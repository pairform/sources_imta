function getElementAtCoordinates(x, y, height, width, nomPage, selecteurs) {
	x *= (window.innerWidth / width);
	y *= (window.innerHeight / height);

	selecteurs = selecteurs.split(",");
	
	var initial_element = document.elementFromPoint(x,y);
	var element = initial_element;
	
	removeSquaringFromEveryElement(selecteurs);
	
	while(element.nodeName !== "BODY" && !matchedSelector(element, selecteurs)){
		element = element.parentNode;
	}

	if (element.nodeName !== "BODY") {
		var matched_selector = matchedSelector(element, selecteurs);
		
		applySquaringOnElement(element);
		
		var elements_with_same_selector = document.querySelectorAll(matched_selector);
		for (var index = 0; index < elements_with_same_selector.length; index++) {
			if (element === elements_with_same_selector[index]) {
				android.onClickedLireMessage( matched_selector + ':' + index + ':' + nomPage);
				break;
			}
		}
	}
}


function matchedSelector (element, selecteurs) {
	for (var i = 0; i < selecteurs.length; i++) {
		if (element.matches(selecteurs[i]))
			return selecteurs[i];
	}
	return null;
}


function removeSquaringFromEveryElement(selecteurs) {    
	for (var i = 0; i < selecteurs.length; i++) {
		var elements_with_same_selector = document.querySelectorAll(selecteurs[i]);
		for (var index = 0; index < elements_with_same_selector.length; index++) {
			if ( hasClass(elements_with_same_selector[index],'selected')){
				removeClass(elements_with_same_selector[index],'selected');
			}
		}
	}
}


function applySquaringOnElement(element) {
	element.className += ' selected';
}


function putBadge(tag, num_occurence, nbMessages, messageslus) {    
	var elements = document.querySelectorAll(tag);
	
	if ( !hasClass(elements[num_occurence],'hasComment') )
		elements[num_occurence].className +=  ' hasComment';
	
	elements[num_occurence].setAttribute('data-count', nbMessages);
	if ( messageslus )
		elements[num_occurence].className +=  ' readed';
	else {
		if (hasClass(elements[num_occurence],'readed')) {
			removeClass(elements[num_occurence],'readed');
		}
	};
}


function hasClass(element, cls) {
	return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}


function removeClass(element,cls) {
	if (hasClass(element,cls)) {
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		element.className=element.className.replace(reg,' ');
	}
}