package cape.pairform.modele;

import cape.pairform.controleurs.services.ConnexionServeurService;
import cape.pairform.controleurs.services.EnvoieMessagesService;
import cape.pairform.controleurs.services.RecuperateurMessagesService;

public interface Constantes {
	// id de la ressource initiale et de ses capsules ; elles doivent être téléchargées lors de l'initialisation de l'application
	// Dev : C2I D4
	//public static final String ID_RESSOURCE_INITIALE = "1";
	//public static final String[] ID_CAPSULES_INITIALES = {"49"};
	// Prod : Manuel Android
	public static final String ID_RESSOURCE_INITIALE = "1";
	public static final String[] ID_CAPSULES_INITIALES = {"193"};
	
	public static final String OS = "and";						// système d'exploitation : Android
	public static final String CLEF_OS = "os";					// clef système d'exploitation
	public static final String CLEF_VERSION = "version";		// clef version de l'application
	public static final String VERSION_NAME = "1.2.200";		// numéro de version de l'application

	/* 
	 * /!\ mise en prod :
	 * 
	 * - s'il y a eu des modifications de la BDD locale, ajouter des requetes SQL pour permettre la migration de la BDD
	 * - vérifier que la mise à jour de l'ancienne version vers la nouvelle se déroule bien
	 * 		mettre à jour BDD.java :
	 * 			- mettreAJourBDD()
	 * 			- BDD()
	 * - modifier version dans :
	 * 		- AndroidManifest.xml
	 * 		- Constantes.java 
	 *		- switch des fonctions mettreAJourBDD() et BDD()
	 * - modifier constantes SERVEUR_NODE_URL
	 * - ajouter un TAG svn : svn copy https://campus.mines-nantes.fr/CAPE_PairForm/trunk/android/SupCast https://campus.mines-nantes.fr/CAPE_PairForm/tag/android/PairForm_v1.2.XXX -m "tag android v1.2.XXX - ..."
	 * 
	 */
	//public static final String SERVEUR_NODE_URL = "http://imedia.emn.fr:3030/";							// URL du serveur Node.js en dev
	public static final String SERVEUR_NODE_URL = "https://www.pairform.fr/node/";					// URL du serveur Node.js en prod
	//public static final String SERVEUR_NODE_URL = "https://www.pairform.fr/beta/";						// URL du serveur Node.js en prod beta
	public static final String WEB_SERVICES_NODE_URL = SERVEUR_NODE_URL + "webServices/";				// URL des web services Node.js en dev
	public static final int METHODE_REQUETE = 0;
	public static final int URL_REQUETE = 1;

	public static final String[] URL_WEB_SERVICE_LISTER_RESSOURCE_TELECHARGEABLE = {"GET", WEB_SERVICES_NODE_URL + "ressource/liste"};					// URL du web service listant toutes les ressources téléchargeables
	public static final String[] URL_WEB_SERVICE_RECUPERER_INFOS_RESSOURCE = {"GET", WEB_SERVICES_NODE_URL + "ressource"};								// URL du web service récupérant les infos sur une ressource (nom, description, logo, infos sur les capsules, profil d'apprentissage, etc.)
	
	public static final String[] URL_WEB_SERVICE_RECUPERER_MESSAGE = {"GET", WEB_SERVICES_NODE_URL + "message"};										// URL du web service récupérant les messages d'une page, d'une sous partie d'une page ou d'une ressource
	public static final String[] URL_WEB_SERVICE_ENREGISTRER_MESSAGE = {"PUT", WEB_SERVICES_NODE_URL + "message"};										// URL du web service qui envoie un nouveau message pour l'enregistrer sur la BDD du serveur
	public static final String[] URL_WEB_SERVICE_SUPPRIMER_MESSAGE = {"POST", WEB_SERVICES_NODE_URL + "message/supprimer"};								// URL du web service qui supprime un message de la BDD du serveur
	public static final String[] URL_WEB_SERVICE_MODIFIER_UTILITE_MESSAGE = {"POST", WEB_SERVICES_NODE_URL + "message/voter"};							// URL du web service modifiant l'utilité d'un message
	public static final String[] URL_WEB_SERVICE_AJOUTER_TAGS_MESSAGE = {"POST", WEB_SERVICES_NODE_URL + "message/tags"};								// URL du web service ajoutant des tags à un message
	public static final String[] URL_WEB_SERVICE_TERMINER_DEFI = {"POST", WEB_SERVICES_NODE_URL + "message/defi/terminer"};								// URL du web service terminant un défi en cours
	public static final String[] URL_WEB_SERVICE_VALIDER_REPONSE_DEFI = {"POST", WEB_SERVICES_NODE_URL + "message/defi/valider"};						// URL du web service qui déclare une réponse à un défi comme valide
	public static final String[] URL_WEB_SERVICE_DONNER_MEDAILLE_MESSAGE = {"POST", WEB_SERVICES_NODE_URL + "message/donnerMedaille"};					// URL du web service permettant à un expert d'attribuer une medaille à un message
	public static final String[] URL_WEB_SERVICE_MODIFIER_LANGUE_MESSAGE = {"POST", WEB_SERVICES_NODE_URL + "message/modifierLangue"};	
	public static final String[] URL_WEB_SERVICE_RECUPERER_INFOS_PJS = {"GET", WEB_SERVICES_NODE_URL + "informationsGeneral"};							// URL du web service récupérant les infos sur les pieces jointes (Nombre maximal, taille max, type authorisé, ...)
	public static final String[] URL_WEB_SERVICE_RECUPERER_PJ = {"GET", WEB_SERVICES_NODE_URL + "recupererPieceJointe"};								// URL du web service permettant de recupererune thumbnail

	public static final String[] URL_WEB_SERVICE_CREER_COMPTE = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/enregistrer"};							// URL du web service qui créé un nouveau compte
	public static final String[] URL_WEB_SERVICE_CONNEXION = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/login"};										// URL du web service connectant / identifiant l'utilisateur
	public static final String[] URL_WEB_SERVICE_DECONNEXION = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/logout"};									// URL du web service déconnectant l'utilisateur
	public static final String[] URL_WEB_SERVICE_EDITER_COMPTE = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/editer"};								// URL du web service modifiant les informations du compte
	public static final String[] URL_WEB_SERVICE_EDITER_AVATAR = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/avatar"};								// URL du web service modifiant l'avatar du compte
	public static final String[] URL_WEB_SERVICE_EDITER_LANGUES = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/editerLangues"};						// URL du web service modifiants les langues de l'utilisateur
	public static final String[] URL_WEB_SERVICE_ACTIVER_EMAIL_NOTIFICATION = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/notification/mail"};		// URL du web service permettant d'activer / désactiver l'email de notification d'une ressource
	public static final String[] URL_WEB_SERVICE_VERIFIER_ROLE = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/categorie/verifier"};					// URL du web service permettant de vérifier si l'utilisateur à un rôle dans une ressource, le cas échéant, crée le rôle
	public static final String[] URL_WEB_SERVICE_CHANGER_ROLE = {"POST", WEB_SERVICES_NODE_URL + "utilisateur/role"};									// URL du web service attribuant un rôle à un utilisateur dans une ressource
	public static final String[] URL_WEB_SERVICE_RECUPERER_UTILISATEURS = {"GET", WEB_SERVICES_NODE_URL + "utilisateur/liste"};							// URL du web service récupérant la liste de tout les utilisateurs de l'application
	public static final String[] URL_WEB_SERVICE_AFFICHER_PROFIL = {"GET", WEB_SERVICES_NODE_URL + "utilisateur/profil"};								// URL du web service récupérant les informations sur un utilisateur (son profil)
	public static final String[] URL_WEB_SERVICE_RECUPERER_OPENBADGES_ATTRIBUABLES = {"GET", WEB_SERVICES_NODE_URL + "utilisateur/openbadge/attribuables"};	// URL du web service récupérant la liste des badges attribuables (par un utilisateur donné) dans une ressource
	public static final String[] URL_WEB_SERVICE_ATTRIBUER_OPENBADGE = {"PUT", WEB_SERVICES_NODE_URL + "utilisateur/openbadge"};						// URL du web service attribuant un badge à un utilisateur dans une ressource
	
	public static final String[] URL_WEB_SERVICE_AFFICHER_CERCLES = {"GET", WEB_SERVICES_NODE_URL + "reseau/liste"};									// URL du web service récupérant les cercles et classes d'un utilisateur
	public static final String[] URL_WEB_SERVICE_CREER_CERCLE = {"PUT", WEB_SERVICES_NODE_URL + "reseau/cercle"};										// URL du web service permettant de creer un cercle
	public static final String[] URL_WEB_SERVICE_CREER_CLASSE = {"PUT", WEB_SERVICES_NODE_URL + "reseau/classe"};										// URL du web service permettant de creer une classe
	public static final String[] URL_WEB_SERVICE_REJOINDRE_CLASSE = {"PUT", WEB_SERVICES_NODE_URL + "reseau/classe/utilisateur"};						// URL du web service permettant à un utilisateur de rejoindre une classe
	public static final String[] URL_WEB_SERVICE_SUPPRIMER_CERCLE = {"DELETE", WEB_SERVICES_NODE_URL + "reseau"};										// URL du web service permettant de supprimer un cercle
	public static final String[] URL_WEB_SERVICE_SUPPRIMER_MEMBRE_CERCLE = {"DELETE", WEB_SERVICES_NODE_URL + "reseau/utilisateur"};					// URL du web service permettant de supprimer un membre d'un cercle
	public static final String[] URL_WEB_SERVICE_AJOUTER_MEMBRE_CERCLE = {"PUT", WEB_SERVICES_NODE_URL + "reseau/utilisateur"};							// URL du web service permettant d'ajouter un membre à un cercle

	public static final String URL_FICHIER_CGU = SERVEUR_NODE_URL + "cgu/reseau_social";																// URL web vers le fichier html stockant les CGU - un paramètre (optionnel) de langue peut être ajouté (ex: /fr, /en, etc.)
	
	
	/*
	 *  clefs utilisées par les fichiers de preferences, les webservices et les Intent
	 */
	// retour JSON des webservices
	public static final String CLEF_WEBSERVICE_CLEF_RETOUR = "status";
	public static final String CLEF_WEBSERVICE_RETOUR_OK = "ok";
	public static final String CLEF_WEBSERVICE_RETOUR_UP = "up";
	public static final String CLEF_WEBSERVICE_CLEF_ERREUR = "message";
	public static final String CLEF_WEBSERVICE_CLEF_DATA = "data";
	/* voué à disparaitre : */
	public static final String CLEF_WEBSERVICE_CLEF_DATAS = "datas";
	// clefs générales
	public static final String CLEF_NOM = "nom";
	public static final String CLEF_NOM_COURT = "nom_court";
	public static final String CLEF_NOM_LONG = "nom_long";
	public static final String CLEF_DESCRIPTION= "description";
	public static final String CLEF_LOGO = "logo";
	public static final String CLEF_URL_LOGO = "url_logo";
	public static final String CLEF_DATE_CREATION = "date_creation";
	public static final String CLEF_DATE_EDITION = "date_edition";
	public static final String CLEF_DATE_GAGNE = "date_gagne";
	public static final String CLEF_URL_AVATAR = "avatar_url";
	public static final String CLEF_TRANSVERSALE = "transversale";
	public static final String CLEF_COOKIE_SESSION = "cookie_sessions";
	public static final String CLEF_COOKIE_SESSION_SIG = "cookie_sessions_sig";
	// ressource
	public static final String CLEF_RESSOURCE = "ressource";
	public static final String CLEF_RESSOURCES = "ressources";
	public static final String CLEF_ID_RESSOURCE = "id_ressource";
	public static final String CLEF_NOM_COURT_ESPACE = "espace_nom_court";
	public static final String CLEF_NOM_LONG_ESPACE = "espace_nom_long";
	public static final String CLEF_THEME = "theme";
	public static final String CLEF_ID_USAGE = "id_usage";
	/* voué à disparaitre : */
	public static final String CLEF_ID_RESSOURCES_TELECHARGEES = "capsules_utilisateur_mobile";
	// capsule
	public static final String CLEF_CAPSULES = "capsules";
	public static final String CLEF_ID_CAPSULE = "id_capsule";
	public static final String CLEF_ID_VISIBILITE = "id_visibilite";
	public static final String CLEF_URL_MOBILE = "url_mobile";
	public static final String CLEF_LICENCE = "licence";
	public static final String CLEF_TAILLE = "taille";
	public static final String CLEF_NOMS_AUTEURS = "auteurs";
	public static final String CLEF_SELECTEURS = "selecteurs";
	public static final String CLEF_FILTRE_CAPSULES = "filtre_timestamp";
	public static final String CLEF_NOM_PAGE_CAPSULE = "nom_page_capsule";
	public static final String CLEF_LISTE_NOM_PAGE_CAPSULE= "liste_nom_page_capsule";
	// message
	public static final String CLEF_ID_MESSAGE = "id_message";
	public static final String CLEF_LISTE_MESSAGES = "messages";
	public static final String CLEF_ID_MESSAGE_PARENT = "id_message_parent";
	public static final String CLEF_ID_PARENT_MESSAGE = "id_parent";
	public static final String CLEF_CONTENU_MESSAGE = "contenu";
	public static final String CLEF_SOMME_VOTES_MESSAGE = "somme_votes";
	public static final String CLEF_UTILISATEUR_A_VOTE_MESSAGE = "utilisateur_a_vote";
	public static final String CLEF_MODIF_UTILITE_MESSAGE = "up";
	public static final String CLEF_DEFI_MESSAGE = "est_defi";
	public static final String CLEF_REPONSE_DEFI__VALIDE_MESSAGE = "defi_valide";
	public static final String CLEF_NOM_PAGE_MESSAGE_DEFAUT = "";
	public static final String CLEF_NOM_PAGE_MESSAGE = "nom_page";
	public static final String CLEF_NOM_TAG_PAGE_MESSAGE = "nom_tag";
	public static final String CLEF_TAG_MESSAGE = "tag";
	public static final String CLEF_TAGS_MESSAGE = "tags";
	public static final String CLEF_NOMS_AUTEURS_TAGS_MESSAGE = "noms_auteurs_tags";
	public static final String CLEF_NUM_OCCURENCE_TAG_PAGE_MESSAGE = "num_occurence";
	public static final String CLEF_SUPPRIME_PAR_MESSAGE = "supprime_par";
	public static final String CLEF_DATE_CREATION_MESSAGE = "date_creation";
	public static final String CLEF_DATE_MODIF_MESSAGE = "date_modification";
	public static final String CLEF_MEDAILLE_MESSAGE = "medaille";
	public static final String CLEF_ID_AUTEUR_MESSAGE = "id_auteur";
	public static final String CLEF_ID_ROLE_AUTEUR_MESSAGE = "id_role_auteur";
	public static final String CLEF_PSEUDO_AUTEUR_MESSAGE = "pseudo_auteur";
	public static final String CLEF_URL_AVATAR_AUTEUR_MESSAGE = "url_avatar_auteur";
	public static final String CLEF_VISIBILITE_MESSAGE = "visibilite";
	public static final String CLEF_VISIBILITE_RESTREINTE_MESSAGE = "prive";
	public static final String CLEF_MODERATEUR_MESSAGE = "id_user_moderator";
	/* voué à disparaitre : */
	public static final String CLEF_WEBSERVICE_AUTEUR_MESSAGE = "id_user_op";
	public static final String CLEF_WEBSERVICE_IDLANGUE_MESSAGE = "langue";
	// geolocation
	public static final String CLEF_GEO_LATITUDE = "geo_latitude";
	public static final String CLEF_GEO_LONGITUDE = "geo_longitude";	
	// pièces jointes
	public static final String CLEF_NOM_ORIGINAL_PJ = "nom_original_pj";
	public static final String CLEF_NOM_SERVEUR_PJ = "nom_serveur_pj";	
	public static final String CLEF_EXTENSION_PJ = "extension_pj";
	public static final String CLEF_TAILLE_PJ = "taille_pj";
	public static final String CLEF_WEBSERVICE_PIECES_JOINTES_MESSAGE = "pieces_jointes";
	public static final String CLEF_WEBSERVICE_DATA_PJ = "dataPJ";
	public static final String CLEF_WEBSERVICE_DATA_PJ_INFORMATION = "pj_data";	
	public static final String CLEF_WEBSERVICE_PJ_NOM_SERVEUR = "name";
	public static final String CLEF_WEBSERVICE_PJ_TAILLE = "size";
	public static final String CLEF_WEBSERVICE_PJ_EXTENSION = "extension";
	public static final String CLEF_WEBSERVICE_PJ_NB_MAX = "nb_pj";
	public static final String CLEF_WEBSERVICE_PJ_MAX_TAILLE = "max_taille";
	public static final String CLEF_WEBSERVICE_PJ_THUMBNAIL_TAILLE = "thumbnail_taille";
	public static final String CLEF_WEBSERVICE_PJ_FORMAT = "format";
	public static final String CLEF_WEBSERVICE_PJ_FORMAT_TYPE = "type";
	public static final String CLEF_WEBSERVICE_PJ_FORMAT_VALEUR = "valeur";
	public static final String CLEF_WEBSERVICE_PJ_FORMAT_PHOTO = "Photo";
	public static final String CLEF_WEBSERVICE_PJ_FORMAT_VIDEO = "Video";
	public static final String CLEF_WEBSERVICE_PJ_FORMAT_FICHIER = "Fichier";
	public static final String CLEF_WEBSERVICE_PJ_FORMAT_SON = "Son";	
	//Clé requete serveur recupération pièces jointes
	public static final String CLEF_THUMBNAIL_NOM_FICHIER = "-thumbnail."; //Clé inséré dans le nom de la piece jointe par default pour différencier la preview de la piece jointe grandeur nature
	public static final String CLEF_TYPE_PJ = "type";
	// utilisateur, connexion et création de compte
	public static final String CLEF_ID_UTILISATEUR = "id_utilisateur";
	public static final String CLEF_PSEUDO_UTILISATEUR = "username";
	public static final String CLEF_PASSWORD_UTILISATEUR = "password";	
	public static final int CLEF_AUTO_LOGGIN = 0;
	public static final int CLEF_MANUAL_LOGGIN = 1;
	public static final int CLEF_NO_LOGGIN = 2;
	public static final String CLEF_CHOIX_CONNEXION = "choix_connexion";	
	public static final String CLEF_NOM_UTILISATEUR = "name";
	public static final String CLEF_ETABLISSEMENT_UTILISATEUR = "etablissement";
	public static final String CLEF_NOTIFICATION_EMAIL = "notification_mail";
	public static final String CLEF_MOT_DE_PASSE_UTILISATEUR = "password";
	public static final String CLEF_CONFIRMATION_MOT_DE_PASSE_UTILISATEUR = "password2";
	public static final String CLEF_ANCIEN_MOT_DE_PASSE_UTILISATEUR = "current_password";
	public static final String CLEF_LISTE_UTILISATEURS = "profils";
	public static final String CLEF_EMAIL_UTILISATEUR = "email";
	public static final String CLEF_ROLE_UTILISATEUR = "rank";
	public static final String CLEF_ID_ROLE_UTILISATEUR = "id_categorie";
	/* voué à disparaitre : */
	public static final String CLEF_URL_UTILISATEUR = "image";
	// profil utilisateur
	public static final String CLEF_SUCCES = "succes";
	public static final String CLEF_NB_SUCCES_TOTAL = "nb_succes";
	public static final String CLEF_NB_SUCCES_GAGNE = "nb_succes_gagnes";
	public static final String CLEF_SUCCES_DEBLOQUE = "succes_est_gagne";
	public static final String CLEF_SCORE_UTILISATEUR_DANS_RESSOURCE = "score";
	public static final String CLEF_AVATAR_UTILISATEUR = "avatar";
	public static final String CLEF_TAILLE_AVATAR_UTILISATEUR = "erreur_taille_avatar";
	public static final String CLEF_LOGO_SUCCES = "nom_logo";
	public static final String CLEF_EMETTEUR_OPENBADGE = "emetteur";
	public static final String CLEF_ID_OPENBADGE = "id_openbadge";
	public static final String CLEF_OPENBADGES = "openbadges";
	public static final String CLEF_NB_OPENBADGES = "nb_openbadges";
	public static final String CLEF_ID_UTILISATEUR_CIBLE = "id_utilisateur_cible";
	public static final String CLEF_ID_ROLE_TEMP = "id_categorie_temp";
	// cercle et classe
	public static final String CLEF_ID_CERCLE = "id_collection";
	public static final String CLEF_CLEF_CLASSE = "cle";
	public static final String CLEF_LISTE_CERCLES = "cercles";
	public static final String CLEF_LISTE_CLASSES = "classes";
	public static final String CLEF_LISTE_CLASSES_REJOINTES = "classesRejointes";
	public static final String CLEF_CLE_INVALIDE = "cle_invalide";
	public static final String CLEF_ID_UTILISATEUR_CONCERNE = "id_utilisateur_concerne";
	public static final String CLEF_UTILISATEUR_DEJA_MEMBRE = "utilisateur_deja_membre";
	// gestion des préférences des langues
	public static final String CLEF_ID_LANGUE = "id_langue";
	public static final String CLEF_ID_LANGUE_RESSOURCE = "langue";
	public static final String CLEF_LANGUE_UTILISATEUR = "langue_utilisateur";
	public static final String CLEF_ID_LANGUE_UTILISATEUR = "langue";
	public static final String CLEF_LANGUE_APPLICATION = "langue_application";
	public static final String CLEF_GEOLOCALISATION_MESSAGE = "geolocalisation_message";
	public static final String CLEF_LANGUE_PRINCIPALE = "langue_principale";
	public static final String CLEF_AUTRES_LANGUES = "autres_langues";
	public static final String CLEF_LANGUES_AFFICHAGE = "langues_affichage";
	public static final String CLEF_WEBSERVICE_LANGUE_PRINCIPALE = "langue_principale";
	public static final String CLEF_WEBSERVICE_AUTRES_LANGUES = "autres_langues";
	public static final String CLEF_WEBSERVICE_CODE_LANGUE = "code_langue";
	public static final String CLEF_WEBSERVICE_LANGUE_APPLICATION = "langue_app";

	// clef des messages d'erreurs des webservices
	public static final String CLEF_ERREUR_WEBSERVICE_PSEUDO_UTILISE = "ws_erreur_nom_utilise";
	public static final String CLEF_ERREUR_WEBSERVICE_CHAMP_VIDE = "Missing credentials";
	public static final String CLEF_ERREUR_WEBSERVICE_MOT_DE_PASSE_CONFIRMATION = "ws_erreur_saisie_mot_de_passe_confirmation";
	public static final String CLEF_ERREUR_WEBSERVICE_EMAIL_UTILISE = "ws_erreur_mail_utilise";

	
	public static final String REPERTOIRE_CAPSULE = "capsules_pairform";									// répertoire contenant les fichiers des capsules téléchargées
	public static final String REPERTOIRE_RESSOURCE = "ressources_pairform";								// répertoire contenant les fichiers des ressources téléchargées
	public static final String REPERTOIRE_AVATAR = "avatars_pairform";										// répertoire contenant les avatars téléchargées
	public static final String REPERTOIRE_PJS = "pjs_pairform";												// répertoire contenant les thumbnails de pjs
	public static final String REPERTOIRE_PJS_HORS_LIGNE = "pjs_pairform_message_horsligne";				// répertoire contenant les thumbnails de pjs quand l'application est hors-ligne
	public static final String REPERTOIRE_SUCCES = "succes";												// repertoire stockant les images des succès
	public static final String FICHIER_APPLICATION_PARAMETRES = "application_parametres";					// fichier SharedPreferences stockant les parametres de l'application (si c'est le premier lancement de l'apply, cookie de session, etc.) 
	public static final String FICHIER_INFOS_AIDES_CONTEXTUELLES = "aides_contextuelles";					// fichier SharedPreferences stockant les informations sur l'aide contextuelle sous forme : clef écran (clef) / "true" si c'est le premier lancement de l'écran, "false" sinon (valeur) ; CLEF_AIDE_CONTEXTUELLE (clef) / id aide écran actuellement visible (valeur) 
	public static final String FICHIER_INFOS_UTILISATEUR_CONNECTE = "utilisateur_connecte";					// fichier SharedPreferences stockant les informations sur l'utilisateur actuellement connecté (id, email, établissement, role dans chaque ressource, affichage des noms et prenoms, etc.)
	public static final String FICHIER_LISTE_RESSOURCES_MISES_A_JOUR = "liste_ressources_mises_a_jour";		// fichier SharedPreferences stockant la liste des ressources ayant une mise à jour de disponible sous forme : id ressource (clef) / URL de téléchargement de la ressource (value)
	public static final String FICHIER_LISTE_URL_AVATAR = "liste_url_avatar";								// fichier SharedPreferences stockant la liste des avatars des utilisateur sous forme : clef (URL) / valeur (pseudo utilisateur)
	public static final String FICHIER_LISTE_CHEMIN_THUMBNAILS = "liste_thumbnails";						// fichier SharedPreferences stockant la liste des thumbnails présent sur le disque 
	public static final String FICHIER_A_PROPOS_INFOS_PAIRFORM = "informations_pairform.html";				// fichier html stockant une description de l'application et des infos sur celle-ci
	public static final String FICHIER_SUCCES = "succes.png";												// image associée à un succès n'ayant pas d'image sur le smartphone 
	public static final String FICHIER_INFOS_PJ = "pj_informations";										// Nom du fichier qui va contenir dans les preferences les informations des pjs 


	public static final String ONGLET_SELECTIONNE = "onglet_selectionne";		// onglet selectionné dans un écran affichant plusieurs onglets


	public static final int DUREE_VIBRATION_CLIC_LONG = 15;						// durée de vibration d'un clic long  dans l'application (en milliseconde)


	public static final int NB_TAGS_MAX = 5;									// nombre maximum de tags associé à un message


	public static final int TAILLE_INEXISTANTE = -1;							// valeur pas défaut de la taille (nb Mo) d'une capsule dans le cas ou cette taille n'est pas renseignée


	// ordre de tri des résultats retournés par la BDD
	public static final String CLASSEMENT_ASCENDANT = " ASC";
	public static final String CLASSEMENT_DESCENDANT = " DESC";


	// gestion de l'utilité
	public static final String UTILISATEUR_A_VOTE_PLUS = "1";
	public static final String UTILISATEUR_A_VOTE_MOINS = "-1";
	public static final String UTILISATEUR_A_PAS_VOTE = "0";


	// gestion des médailles
	public static final String MEDAILLE_OR = "or";
	public static final String MEDAILLE_ARGENT = "argent";
	public static final String MEDAILLE_BRONZE = "bronze";


	// gestion des défis
	public static final int DEFI_EN_COURS = 1;
	public static final int DEFI_FINIS = 2;


	// gestion de l'affichage des réponses des messages
	public static final String REPONSES_VISIBLES = "reponses_visibles";


	// id des rôles utilisateurs
	public static final int ID_LECTEUR = 0;
	public static final int ID_CONTRIBUTEUR= 1;
	public static final int ID_COLLABORATEUR = 2;
	public static final int ID_ANIMATEUR = 3;
	public static final int ID_EXPERT = 4;
	public static final int ID_ADMIN = 5;


	// id des visibilités des capsules
	public static final String ID_VISIBILITE_ACCESSIBLE = "1";
	public static final String ID_VISIBILITE_INACCESSIBLE= "2";


	// gestion de l'aide contextuelle
	// clef général
	public static final String CLEF_AIDE_CONTEXTUELLE = "aide_contextuelle";
	// clefs écran
	public static final String CLEF_ECRAN_ACCUEIL = "aide_ecran_accueil";
	public static final String CLEF_ECRAN_MESSAGES = "aide_ecran_messages";
	public static final String CLEF_ECRAN_MESSAGES_TRANSVERSALE = "aide_ecran_messages_transversale";
	public static final String CLEF_ECRAN_PAGE_CAPSULE = "aide_ecran_page_capsule";
	public static final String CLEF_ECRAN_CERCLES = "aide_ecran_cercles";
	public static final String CLEF_ECRAN_CARTE = "aide_ecran_carte";
	public static final String CLEF_ECRAN_RESSOURCE = "aide_ecran_ressource";
	// id d'aides écran
	public static final int AIDE_INEXISTANTE = -1;
	public static final int AIDE_ECRAN_ACCUEIL = 0;
	public static final int AIDE_ECRAN_MESSAGES = 1;
	public static final int AIDE_ECRAN_MESSAGES_TRANSVERSALE = 2;
	public static final int AIDE_ECRAN_PAGE_CAPSULE = 3;
	public static final int AIDE_ECRAN_CERCLES = 4;
	public static final int AIDE_ECRAN_CARTE = 5;
	public static final int AIDE_ECRAN_RESSOURCE = 6;

	// gestion des intents et clefs associées
	public static final String ACTION_INTENT_CONNEXION = ConnexionServeurService.class.getName();		// action associé à l'intent renvoyé par la classe ConnexionServeurService et intercepté par un BroadcastReceiver
	public static final String ACTION_INTENT_RECUPERER_MESSAGES = RecuperateurMessagesService.class.getName();		// action associé à l'intent renvoyé par la classe RecuperateurMessagesService et intercepté par un BroadcastReceiver de la classe LectureMessagesFragment
	public static final String ACTION_INTENT_ENVOYER_MESSAGES = EnvoieMessagesService.class.getName();		// action associé à l'intent renvoyé par la classe EnvoieMessagesServices et intercepté par un BroadcastReceiver de la classe LectureMessagesFragment
	public static final String CLEF_INTENT_RECUPERER_MESSAGES = "intent_messages_recuperes";						// clef associée au contenu du retour JSON contenant les nouveaux messages
	public static final String CLEF_INTENT_UTILISATEUR_CONNECTE = "intent_utilisateur_connecte";						// clef associée au contenu du retour JSON contenant les informations de l'utilisateur
	public static final String CLEF_INTENT_ENVOYER_MESSAGES = "intent_utilisateur_envoie";						// clef associée au contenu du retour JSON contenant les informations de l'utilisateur

	
	public static final int CLEF_INTENT_MODIFIER_AVATAR = 0;														// clef associée à l'intent de modification de l'avatar de utilisateur

}
