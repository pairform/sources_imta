package cape.pairform.modele;

import java.util.ArrayList;


/*
 * Cercle ou classe
 */
public class Cercle {
	
	private String nomCercle;				// nom du cercle
	private String idCercle;				// id du cercle
	private boolean isCreateur;				// true si le cercle a été créé par l'utilisateur
	private boolean isClasse;				// true si le cercle est une classe
	private String clefClasse = null;		// si le cercle est une classe, clef d'accès permettant à un utilisateur de s'ajouter dans la classe
	private ArrayList<Utilisateur> listeMembresCercle = new ArrayList<Utilisateur>();		// liste des membres appartenant au cercle
	
		
	public Cercle(String nomCercle, String idCercle, boolean isClasse, boolean isCreateur) {
		this.nomCercle = nomCercle;
		this.idCercle = idCercle;
		this.isClasse = isClasse;
		this.isCreateur = isCreateur;
	}

	
	public String getNomCercle() {
		return nomCercle;
	}
	
	public void setNomCercle(String nomCercle) {
		this.nomCercle = nomCercle;
	}
	
	public String getIdCercle() {
		return idCercle;
	}
	
	public void setIdCercle(String idCercle) {
		this.idCercle = idCercle;
	}

	public boolean isCreateur() {
		return isCreateur;
	}
	
	public boolean isClasse() {
		return isClasse;
	}
	
	public String getClefClasse() {
		return clefClasse;
	}
	
	public void setClefClasse(String clefClasse) {
		this.clefClasse = clefClasse;
	}
	
	public void setListeMembresCercle(ArrayList<Utilisateur> listeMembresCercle) {
		this.listeMembresCercle = listeMembresCercle;
	}

	public ArrayList<Utilisateur> getListeMembresCercle() {
		return listeMembresCercle;
	}
	
	public void addMembreCercle(Utilisateur membreCercle) {
		this.listeMembresCercle.add(membreCercle);
	}
}
