package cape.pairform.modele;

public enum PieceJointeType {
	PHOTO,
	VIDEO;
	
	/**
	 * Méthode renvoyant l'extension correspondant au nom du fichier passé en paramètre
	 * Elle se base sur la présence du "."
	 * @param name Le nom du ficher
	 * @return l'extension situé derrière le "."
	 */
	public static String stringAExtension(String name){
		return name.substring(name.lastIndexOf(".")+1);
	}
}
