package cape.pairform.modele;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ListView;


/*
 * ListView personnalisée, la listView peut être positionnée dans un scrollview sans problème d'affichage
 * (avec une ListView classique, seul la première ligne est affichée)
 * 
 * Attention : peut causer des ralentissements s'il y a un grand nombre d'élément dans la ListView
 */
public class ListViewExtensible extends ListView {
	
	boolean extensible = false;			// false : la ListViewExtensible se comporte comme une ListView classique ; true : la ListViewExtensible peut être positionné dans un scrollview sans problème d'affichage
	

	public ListViewExtensible(Context context) {
		super(context);
	}
	
	public ListViewExtensible(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ListViewExtensible(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    
    public void setExtensible(boolean extensible) {
        this.extensible = extensible;
    }
    
    
    public boolean isExtensible() {
        return extensible;
    }
    
    
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // si la ListViewExtensible est extensible
        if ( isExtensible() ) {
        	// on mesure la hauteur
            int height = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, height);
            // on attribut la hauteur mesurée à la ListViewExtensible
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();
            
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}

