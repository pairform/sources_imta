package cape.pairform.modele;

import java.util.ArrayList;
import java.util.HashMap;


/*
 * classe représentant les informations de profil d'un utilisateur
 */
public class ProfilUtilisateur {

	private String idUtilisateur;					// id de l'utilisateur
	private String nomUtilisateur;					// pseudo ou nom-prenom de l'utilisateur
	private String etablissement;					// etablissement de l'utilisateur (école, entreprise, ...)
	private String urlAvatar;						// url de l'avatar de l'utilisateur
	private String langue;							// langue de l'utilisateur
	private ArrayList<HashMap<String,String>> listeRessources = new ArrayList<HashMap<String,String>>();	// liste de toutes les ressources utilisées par l'utilisateur
	private int nbSuccesDebloques;					// nombre de succés débloqués
	private int nbSuccesTotal;						// nombre total de succès existant
	private ArrayList<HashMap<String,String>> listeSucces = new ArrayList<HashMap<String,String>>();		// liste de tout les succès existants
	private int nbOpenBadges;							// nombre de badges gagnés
	private ArrayList<HashMap<String,String>> listeOpenBadges = new ArrayList<HashMap<String,String>>();		// liste de tout les badges gagnés	
	
	
	public ProfilUtilisateur(String idUtilisateur, String nomUtilisateur, String etablissement, String urlAvatar, String langue) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.nomUtilisateur = nomUtilisateur;
		this.etablissement = etablissement;
		this.urlAvatar = urlAvatar;
		this.langue = langue;
	}
	

	public String getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}

	public String getEtablissement() {
		return etablissement;
	}

	public String getUrlAvatar() {
		return urlAvatar;
	}

	public void setUrlAvatar(String urlAvatar) {
		this.urlAvatar = urlAvatar;
	}

	public void setEtablissement(String etablissement) {
		this.etablissement = etablissement;
	}

	public String getLangue() {
		return langue;
	}

	public void setLangue(String langue) {
		this.langue = langue;
	}
	
	public ArrayList<HashMap<String,String>> getListeRessources() {
		return listeRessources;
	}

	public void addRessource(HashMap<String,String> ressource) {
		this.listeRessources.add(ressource);
	}

	public int getNbSuccesDebloques() {
		return nbSuccesDebloques;
	}

	public void setNbSuccesDebloques(int nbSuccesDebloques) {
		this.nbSuccesDebloques = nbSuccesDebloques;
	}

	public int getNbSuccesTotal() {
		return nbSuccesTotal;
	}

	public void setNbSuccesTotal(int nbSuccesTotal) {
		this.nbSuccesTotal = nbSuccesTotal;
	}
	
	public ArrayList<HashMap<String,String>> getListeSucces() {
		return listeSucces;
	}

	public void addSucces(HashMap<String,String> succes) {
		this.listeSucces.add(succes);
	}

	public int getNbOpenBadges() {
		return nbOpenBadges;
	}

	public void setNbOpenBadges(int nbOpenBadges) {
		this.nbOpenBadges = nbOpenBadges;
	}

	public ArrayList<HashMap<String, String>> getListeOpenBadges() {
		return listeOpenBadges;
	}

	public void addBadge(HashMap<String,String> badge) {
		this.listeOpenBadges.add(badge);
	}
}
