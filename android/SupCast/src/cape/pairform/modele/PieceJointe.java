package cape.pairform.modele;

import java.io.File;
import java.io.Serializable;


public class PieceJointe implements Serializable {
	private static final long serialVersionUID = 1L;

	protected String nomOriginal;          	// nom d'un fichier joint sans l'extension
	protected String nomServeur;        	// nom du fichier sur le serveur
	protected String extension;    			// extension d'un fichier joint
	protected String idMessage;      		// id d'un objet Message auquel un fichier est joint 
	protected long taille;					// taille d'un fichier
	protected String cheminVersThumbnail;
	protected PieceJointeType type;
	protected File file;

	// constructeur pour une piéce jointe avant l'envoi
	public PieceJointe(String nom) {
		this.nomOriginal = nom;
		this.extension = "";
		this.taille = -1;
		this.cheminVersThumbnail = null;
		this.type = null;
		this.file = null;
	}

	// constructeur pour une piéce jointe aprés l'envoi
	public PieceJointe(String idMessage, String nom_original, String nom_serveur, String extension, String clientPathToThumbnail, int taille) {
		this.nomOriginal = nom_original;
		this.nomServeur = nom_serveur;
		this.extension = extension;
		this.taille = taille;
		this.idMessage = idMessage;
		this.cheminVersThumbnail = clientPathToThumbnail;
		this.type = null;
		this.file = null;
	}

	/**
	 * renvoie le nom original de la PJ tel qu'il a été défini par l'utilisateur
	 * @return le nom original de la PJ tel qu'il a été défini par l'utilisateur
	 */
	public String getNomOriginal() {
		return nomOriginal;
	}

	/**
	 * modifie le nom original de la PJ défini par l'utilisateur
	 */
	public void setNomOriginal(String nom) {
		this.nomOriginal = nom;
	}	

	/**
	 * renvoie le nom de la PJ aléatoirement aloué par le serveur
	 * @return le nom de la PJ aléatoirement aloué par le serveur
	 */
	public String getNomServeur() {
		return nomServeur;
	}	

	public void setNomServeur(String nom){
		this.nomServeur = nom;
	}

	public String getExtension() {
		if(extension == null)
			if(file != null)
				extension = PieceJointeType.stringAExtension(file.getName());

		return extension;
	}		

	public void setExtension(String ext){
		this.extension = ext;
	}

	public long getTaille() {
		if(file != null)
			return  file.length();
		else
			return taille;
	}

	public void setTaille(long taille) {
		this.taille = taille;
	}

	public String getIdMessage() {
		return this.idMessage;
	}

	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}

	public String getCheminVersThumbnail() {
		return this.cheminVersThumbnail;
	}

	public void setCheminVersThumbnail(String donnee){
		this.cheminVersThumbnail = donnee;
	}

	public PieceJointeType getType() {
		return this.type;
	}

	public void setType(PieceJointeType type) {
		this.type = type;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String toString(){
		return "Nom orginal : " + this.getNomOriginal()+
			"\n extension : "+this.getExtension()+
			"\n taille : "+this.getTaille()+
			"\n Chemin : " + this.getCheminVersThumbnail();
	}
}

