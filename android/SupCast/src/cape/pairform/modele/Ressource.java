package cape.pairform.modele;

import java.util.ArrayList;


public class Ressource {
	
	private String id;
	private String nomCourt;
	private String nomLong;
	private String urlLogo;
	private String nomEspace;
	private String description;
	private String theme;
	private String idLangue;
	private int idUsage;
	private long dateCreation;
	private long dateEdition;
	private ArrayList<Capsule>  listeCapsules = new ArrayList<Capsule>();
	
	
	public Ressource(String id, String nomCourt, String nomLong, String urlLogo, String nomEspace, String description, String theme,
			String idLangue, int idUsage, long dateCreation, long dateEdition, ArrayList<Capsule> listeCapsules) {
		this.id = id;
		this.nomCourt = nomCourt;
		this.nomLong = nomLong;
		this.urlLogo = urlLogo;
		this.nomEspace = nomEspace;
		this.description = description;
		this.theme = theme;
		this.idLangue = idLangue;
		this.idUsage = idUsage;
		this.dateCreation = dateCreation;
		this.dateEdition = dateEdition;
		this.listeCapsules = listeCapsules;
	}
	

	/*
	 * Getters et setters
	 */

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getNomCourt() {
		return nomCourt;
	}
	
	public void setNomCourt(String nomCourt) {
		this.nomCourt = nomCourt;
	}
	
	public String getNomLong() {
		return nomLong;
	}
	
	public void setNomLong(String nomLong) {
		this.nomLong = nomLong;
	}
	
	public String getUrlLogo() {
		return urlLogo;
	}
	
	public void setUrlLogo(String urlLogo) {
		this.urlLogo = urlLogo;
	}
	
	public String getNomEspace() {
		return nomEspace;
	}
	
	public void setNomEspace(String nomEspace) {
		this.nomEspace = nomEspace;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getTheme() {
		return theme;
	}
	
	public void setTheme(String theme) {
		this.theme = theme;
	}
	
	public String getIdLangue() {
		return idLangue;
	}
	
	public void setIdLangue(String idLangue) {
		this.idLangue = idLangue;
	}
	
	public int getIdUsage() {
		return idUsage;
	}
	
	public void setIdUsage(int idUsage) {
		this.idUsage = idUsage;
	}
	
	public long getDateCreation() {
		return dateCreation;
	}
	
	public void setDateCreation(long dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	public long getDateEdition() {
		return dateEdition;
	}
	
	public void setDateEdition(long dateEdition) {
		this.dateEdition = dateEdition;
	}

	public ArrayList<Capsule> getListeCapsules() {
		return listeCapsules;
	}
	
	public void setListeCapsules(ArrayList<Capsule> listeCapsules) {
		this.listeCapsules = listeCapsules;
	}

	public int getNbCapsulesTelechargees() {
		int nbCapsuleTelecharge = 0;
		
		for (Capsule capsule : listeCapsules) {
			if (capsule.isTelechargee())
				nbCapsuleTelecharge++;					
		}
		
		return nbCapsuleTelecharge;
	}
}
