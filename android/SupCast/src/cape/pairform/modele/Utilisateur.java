package cape.pairform.modele;


/*
 * utilisateur de PairForm
 */
public class Utilisateur {
	
	private String id;				// id de l'utilisateur
	private String pseudo;			// pseudo de l'utilisateur
	private String urlAvatar;		// url de l'avatar de l'utilisateur
	
	public Utilisateur(String id, String pseudo, String urlAvatar) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.urlAvatar = urlAvatar;
	}
	
	/*
	 * Getters et setters
	 */

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getUrlAvatar() {
		return urlAvatar;
	}

	public void setUrlAvatar(String urlAvatar) {
		this.urlAvatar = urlAvatar;
	}
}
