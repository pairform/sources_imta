package cape.pairform.modele;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;


/*
 * GridView personnalisée, la gridView peut être positionnée dans un scrollview sans problème d'affichage
 * (avec une gridview classique, seul la première ligne est affichée)
 * 
 * Attention : peut causer des ralentissements s'il y a un grand nombre d'élément dans la gridview
 */
public class GridViewExtensible extends GridView {
	
	boolean extensible = false;			// true : la GridViewExtensible se comporte comme une GridView classique ; false : la GridViewExtensible peut être positionné dans un scrollview sans problème d'affichage
	

	public GridViewExtensible(Context context) {
		super(context);
	}
	
	public GridViewExtensible(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridViewExtensible(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    
    public void setExtensible(boolean extensible) {
        this.extensible = extensible;
    }
    
    
    public boolean isExtensible() {
        return extensible;
    }
    
    
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {        
        // si la GridViewExtensible est extensible
        if ( isExtensible() ) {
        	// on mesure la hauteur
            int height = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, height);
            
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
