package cape.pairform.modele;

import java.util.ArrayList;


// représente l'arborescence des chapitres et pages d'une ressource, chaque chapitre peut contenir des pages ou des sous-chapitres ou les 2
public class ChapitreCapsule {
	
	//private static final String LOG_TAG = ParserXmlRessource.class.getSimpleName();	// nom de la classe, utilisé comme identifiant pour les logs
	
	private boolean chapitreRacine = false;							// vaut true si le chapitre n'a pas de père (c'est donc le noeud racine de l'arborescence des chapitres)
	private ArrayList<Object> listePagesEtChapitresFils;			// liste des pages et des chapitres contenus dans le chapitre.
	private String titreChapitre;									// titre du chapitre

		
	public ChapitreCapsule(String titreChapitre) {
		this.titreChapitre = titreChapitre;
		listePagesEtChapitresFils = new ArrayList<Object>();
		//Log.i(LOG_TAG, "création chapitre : " + titreChapitre);
	}
	public ChapitreCapsule(String titreChapitre, boolean chapitreRacine) {
		this.titreChapitre = titreChapitre;
		this.chapitreRacine = chapitreRacine;
		listePagesEtChapitresFils = new ArrayList<Object>();
		//Log.i(LOG_TAG, "création chapitre racine : " + titreChapitre);
	}
	

	public boolean estLeChapitreRacine() {
		return chapitreRacine;
	}
	
	public void addChapitreFils(ChapitreCapsule chapitreFils) {
		listePagesEtChapitresFils.add(chapitreFils);
		//Log.i(LOG_TAG, "ajout chapitre fils dans chapitre : " + titreChapitre);
	}

	public void addPageFils(String titrePage, String nomPage) {
		listePagesEtChapitresFils.add( new PageCapsule(titrePage, nomPage));
		//Log.i(LOG_TAG, "ajout page dans chapitre : " + titreChapitre);
	}
	
	public ArrayList<Object> getListePagesEtChapitresFils() {
		return listePagesEtChapitresFils;
	}
		
	public String getTitreChapitre() {
		return titreChapitre;
	}
}
