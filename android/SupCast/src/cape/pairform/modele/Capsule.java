package cape.pairform.modele;


public class Capsule {
	
	private String id;
	private String idRessource;
	private String idVisibilite;
	private String nomCourt;
	private String nomLong;
	private String urlMobile;
	private String description;
	private String licence;
	private int taille;
	private String auteurs;
	private long dateCreation;
	private long dateEdition;
	private boolean telechargee;
	private String selecteurs;
		
	
	public Capsule(String id, String idRessource, String idVisibilite, String nomCourt, String nomLong, String urlMobile, String description,
			String licence, int taille, String auteurs, long dateCreation, long dateEdition, boolean telechargee, String selecteurs) {
		this.id = id;
		this.idRessource = idRessource;
		this.idVisibilite = idVisibilite;
		this.nomCourt = nomCourt;
		this.nomLong = nomLong;
		this.urlMobile = urlMobile;
		this.description = description;
		this.licence = licence;
		this.taille = taille;
		this.auteurs = auteurs;
		this.dateCreation = dateCreation;
		this.dateEdition = dateEdition;
		this.telechargee = telechargee;
		this.selecteurs = selecteurs;
	}

	/*
	 * Getters et setters
	 */
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getIdRessource() {
		return idRessource;
	}
	
	public void setIdRessource(String idRessource) {
		this.idRessource = idRessource;
	}
	
	public String getIdVisibilite() {
		return idVisibilite;
	}
	
	public void setIdVisibilite(String idVisibilite) {
		this.idVisibilite = idVisibilite;
	}
	
	public String getNomCourt() {
		return nomCourt;
	}
	
	public void setNomCourt(String nomCourt) {
		this.nomCourt = nomCourt;
	}
	
	public String getNomLong() {
		return nomLong;
	}
	
	public void setNomLong(String nomLong) {
		this.nomLong = nomLong;
	}
	
	public String getUrlMobile() {
		return urlMobile;
	}
	
	public void setUrlMobile(String urlMobile) {
		this.urlMobile = urlMobile;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getLicence() {
		return licence;
	}
	
	public void setLicence(String licence) {
		this.licence = licence;
	}
	
	public int getTaille() {
		return taille;
	}
	
	public void setTaille(int taille) {
		this.taille = taille;
	}
	
	public String getAuteurs() {
		return auteurs;
	}
	
	public void setAuteurs(String auteurs) {
		this.auteurs = auteurs;
	}
	
	public long getDateCreation() {
		return dateCreation;
	}
	
	public void setDateCreation(long dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	public long getDateEdition() {
		return dateEdition;
	}
	
	public void setDateEdition(long dateEdition) {
		this.dateEdition = dateEdition;
	}

	public boolean isTelechargee() {
		return telechargee;
	}

	public void setTelechargee(boolean telechargee) {
		this.telechargee = telechargee;
	}

	public String getSelecteurs() {
		return selecteurs;
	}

	public void setSelecteurs(String selecteurs) {
		this.selecteurs = selecteurs;
	}
}
