package cape.pairform.modele;

import java.util.ArrayList;
import java.util.HashSet;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Classe singletonné et entierement static car si on vient à changer un type lors de la demande du serveur, le client en est averti immédiatement et l'instance unique permet de répandre cette logique dans tous le code
 * @author vincent
 *
 */
public class PieceJointeInformation {

	private static PieceJointeInformation singleton;
	private SharedPreferences fichierSharedPreferences;
	private SharedPreferences.Editor editorFichierSharedPreferences;
	//Les valeurs inscrit en dure sont simplement les valeurs par défaults
	private int nombreMax = 5; 
	private long tailleMax = 15000000; 							//En octet
	private int thumbnailTaille = 50;
	private ArrayList<String> formatsPhotoUpload;				// liste des formats des PJ photos pour l'envoi de message
	private ArrayList<String> formatsVideoUpload;				// liste des formats des PJ vidéos pour l'envoi de message
	private String[] FORMATS_PHOTO_DOWNLOAD = {"jpg","png"};	// liste des formats des PJ photos pour les messages recus
	private String[] FORMATS_VIDEO_DOWNLOAD = {"mp4"};			// liste des formats des PJ videos pour les messages recus

	/**
	 * Constructeur du singleton
	 * @param context
	 * @return L'instance unique de la classe
	 */
	public static PieceJointeInformation getInstance(Context context){

		if(singleton == null){
			singleton = new PieceJointeInformation(context);
		}

		return singleton;
	}

	
	/**
	 * Constructeur de l'objet
	 * @param context
	 */
	private PieceJointeInformation(Context context) {
		formatsPhotoUpload = new ArrayList<String>();
		formatsVideoUpload = new ArrayList<String>();
		fichierSharedPreferences = context.getSharedPreferences(Constantes.FICHIER_INFOS_PJ, Context.MODE_PRIVATE);
		editorFichierSharedPreferences = fichierSharedPreferences.edit();

		//On récupère des préférences les différentes informations pour paramètrer la fonctionnalités de pj
		nombreMax = fichierSharedPreferences.getInt(Constantes.CLEF_WEBSERVICE_PJ_NB_MAX, nombreMax);
		tailleMax = fichierSharedPreferences.getLong(Constantes.CLEF_WEBSERVICE_PJ_MAX_TAILLE, tailleMax);
		thumbnailTaille = fichierSharedPreferences.getInt(Constantes.CLEF_WEBSERVICE_PJ_THUMBNAIL_TAILLE, thumbnailTaille);
		formatsPhotoUpload = new ArrayList<String>(fichierSharedPreferences.getStringSet(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_PHOTO, new HashSet<String>()));
		formatsVideoUpload = new ArrayList<String>(fichierSharedPreferences.getStringSet(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_VIDEO, new HashSet<String>()));
	}

	
	/**
	 * Renvoie le nombre maximum de pj autorisé pour un message
	 * @return le nombre
	 */
	public int getNombreMax() {
		return nombreMax;
	}

	
	/**
	 * Spécifie le nombre de pj maximum pour un message
	 * ATTENTION : il faut appelé la méthode "save" afin de garder en mémoire ce choix
	 * @param nombreMax
	 */
	public void setNombreMax(int nombreMax) {
		this.nombreMax = nombreMax;
	}

	
	/**
	 * Renvoie la taille maximal des pjs autorisé pour un message
	 * @return la taille
	 */
	public long getTailleMax() {
		return tailleMax;
	}

	
	/**
	 * Spécifie la taille maximal des pjs pour un message
	 * ATTENTION : il faut appelé la méthode "save" afin de garder en mémoire ce choix
	 * @param tailleMax
	 */
	public void setTailleMax(long tailleMax) {
		this.tailleMax = tailleMax;
	}

	
	/**
	 * Renvoie la taille du thumbnail à generer
	 * @return la taille
	 */
	public int getThumbnailTaille() {
		return thumbnailTaille;
	}

	
	/**
	 * Spécifie la taille des thumbnails
	 * ATTENTION : il faut appelé la méthode "save" afin de garder en mémoire ce choix
	 * @param thumbnailTaille
	 */
	public void setThumbnailTaille(int thumbnailTaille) {
		this.thumbnailTaille = thumbnailTaille;
	}

	
	/**
	 * Ajoute un nouveau format de photo autorisé
	 * ATTENTION : il faut appelé la méthode "save" afin de garder en mémoire ce choix
	 * @param format Un string du format (png, jpg, ...)
	 */
	public void addFormatPhotoUpload(String format){
		formatsPhotoUpload.add(format);
	}

	
	/**
	 * Ajoute un nouveau format de video autorisé
	 * ATTENTION : il faut appelé la méthode "save" afin de garder en mémoire ce choix
	 * @param format Un string du format (mp4, ...)
	 */
	public void addFormatVideoUpload(String format){
		formatsVideoUpload.add(format);
	}
	
	
	/**
	 * Méthode renvoyant le type d'une extension pour l'envoi de message
	 * @param extension Un string de l'extension
	 * @return Le type de l'extension : photo, vidéo, ... . Renvoi null si aucun type ne correspond
	 */
	public PieceJointeType typeExtensionUpload(String extension){		
		if(extensionPhotoAutoriseUpload(extension))
			return PieceJointeType.PHOTO;
		else if(extensionVideoAutoriseUpload(extension))
			return PieceJointeType.VIDEO;
		
		return null;
	}
	
	
	/**
	 * Méthode renvoyant le type d'une extension pour les messages recus
	 * @param extension Un string de l'extension
	 * @return Le type de l'extension : photo, vidéo, ... . Renvoi null si aucun type ne correspond
	 */
	public PieceJointeType typeExtensionDownload(String extension){		
		if(extensionPhotoAutoriseDownload(extension))
			return PieceJointeType.PHOTO;
		else if(extensionVideoAutoriseDownload(extension))
			return PieceJointeType.VIDEO;
		
		return null;
	}
	
	
	/**
	 * Méthode permettant de savoir si une extension correspond au type Photo pour l'envoi de message
	 * @param extension Un string de l'extension
	 * @return Un booléen : true si photo et false sinon
	 */
	public boolean extensionPhotoAutoriseUpload(String extension){
		return formatsPhotoUpload.contains(extension);
	}
	
	
	/**
	 * Méthode permettant de savoir si une extension correspond au type Video pour l'envoi de message
	 * @param extension Un string de l'extension
	 * @return Un booléen : true si video et false sinon
	 */
	public boolean extensionVideoAutoriseUpload(String extension){
		return formatsVideoUpload.contains(extension);
	}
	
	
	/**
	 * Méthode permettant de savoir si une extension correspond au type Photo pour les messages recus
	 * @param extension Un string de l'extension
	 * @return Un booléen : true si photo et false sinon
	 */
	public boolean extensionPhotoAutoriseDownload(String extension){
		for (String format: FORMATS_PHOTO_DOWNLOAD)
			if (format.equals(extension))
				return true;
		return false;
	}
	
	
	/**
	 * Méthode permettant de savoir si une extension correspond au type Video pour les messages recus
	 * @param extension Un string de l'extension
	 * @return Un booléen : true si video et false sinon
	 */
	public boolean extensionVideoAutoriseDownload(String extension){
		for (String format: FORMATS_VIDEO_DOWNLOAD)
			if (format.equals(extension))
				return true;
		return false;
	}

	
	/**
	 * Méthode qui enelève les données de la classe afin de les reinitialiser
	 */
	public void vider(){
		formatsPhotoUpload.clear();
		formatsVideoUpload.clear();
	}
	
	
	/**
	 * Méthode sauvant dans les preferences les données sur les pj
	 */
	public void save() {		
		editorFichierSharedPreferences.putInt(Constantes.CLEF_WEBSERVICE_PJ_NB_MAX, nombreMax);
		editorFichierSharedPreferences.putLong(Constantes.CLEF_WEBSERVICE_PJ_MAX_TAILLE, tailleMax);
		editorFichierSharedPreferences.putInt(Constantes.CLEF_WEBSERVICE_PJ_THUMBNAIL_TAILLE, thumbnailTaille);
		editorFichierSharedPreferences.putStringSet(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_PHOTO, new HashSet<String>(formatsPhotoUpload));
		editorFichierSharedPreferences.putStringSet(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_VIDEO, new HashSet<String>(formatsVideoUpload));
		
		editorFichierSharedPreferences.commit();
	}
}
