package cape.pairform.modele.bdd;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Cette classe est package protected. C'est à dire que seul les classes 
 * présentes dans ce package peuvent l'instancier.
 * Cette classe singletonné permet de n'avoir en tous temps qu'une seule 
 * et unique instantiation des variables communes à toutes les tables de base de données
 * @author vincent
 *
 */
class BDDCore {
	
	private static BDDCore instance;
	public SQLiteDatabase bdd;	 
	private BDD pairformBDD;
	public Context context;
	public int versionApplication;
	
	static BDDCore getInstance(Context context){
		if(instance == null)
			instance = new BDDCore(context);
		return instance;
	}
	
	private BDDCore(Context context){
		this.context = context;
		try {
			versionApplication = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		pairformBDD = new BDD(context, null, versionApplication);
		bdd = pairformBDD.getWritableDatabase();
	}
}
