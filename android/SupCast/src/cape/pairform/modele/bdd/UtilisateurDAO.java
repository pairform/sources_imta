package cape.pairform.modele.bdd;

import java.util.ArrayList;
import cape.pairform.modele.Utilisateur;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


/*
 * classe d'accès à la table Utilisateur
 */
public class UtilisateurDAO implements ReferencesBDD {
	
	private static final String[] LISTE_COLONNES_TABLE_UTILISATEUR = {
		COLONNE_ID_UTILISATEUR, COLONNE_PSEUDO_UTILISATEUR, COLONNE_URL_AVATAR_UTILISATEUR
	};

	private BDDCore bddCore;
	private ContentValues valeursUtilisateur;
	private Cursor curseur;
	private ArrayList<Utilisateur> listeUtilisateurs;
	
	
	UtilisateurDAO(Context context){
		bddCore = BDDCore.getInstance(context);
	}
	
	
	public void close(){
		if (curseur != null) {
			curseur.close();
		}
	}
	
	
	public void insertOrUpdateUtilisateur(Utilisateur utilisateurCourant) {
		curseur = getUtilisateurById(utilisateurCourant.getId());
		
		if (curseur.moveToFirst()) {
			updateUtilisateur(utilisateurCourant);
		} else {
			insertUtilisateur(utilisateurCourant);
		}
	}
	
	
	public long insertUtilisateur(Utilisateur utilisateurCourant){
		valeursUtilisateur = new ContentValues();			
		valeursUtilisateur.put(COLONNE_ID_UTILISATEUR, utilisateurCourant.getId());
		valeursUtilisateur.put(COLONNE_PSEUDO_UTILISATEUR, utilisateurCourant.getPseudo());
		valeursUtilisateur.put(COLONNE_URL_AVATAR_UTILISATEUR, utilisateurCourant.getUrlAvatar());

    	return bddCore.bdd.insert(TABLE_UTILISATEUR, null, valeursUtilisateur);
	}
	
	
	public long updateUtilisateur(Utilisateur utilisateurCourant) {
		//SI avatar url est null (seul valeur mis à jour) alors on quitte
		if(utilisateurCourant.getUrlAvatar() == null)
			return 0;
		
		
		valeursUtilisateur = new ContentValues();
		valeursUtilisateur.put(COLONNE_URL_AVATAR_UTILISATEUR, utilisateurCourant.getUrlAvatar());

    	return bddCore.bdd.update(TABLE_UTILISATEUR, valeursUtilisateur, COLONNE_ID_UTILISATEUR + " = '" + utilisateurCourant.getId() + "'", null);
	}
	
	
	// récupère tout les utilisateurs ayant écrit des messages
	public ArrayList<Utilisateur> getAllUtilisateurs() {		
		curseur = bddCore.bdd.query(
			TABLE_UTILISATEUR,
			LISTE_COLONNES_TABLE_UTILISATEUR,
			null,
			null, 
			null, 
			null, 
			null
		);
		
		listeUtilisateurs = new ArrayList<Utilisateur>();		
		if ( curseur.moveToFirst() ) {
			do {										
				listeUtilisateurs.add( new Utilisateur(
					curseur.getString(NUM_COLONNE_ID),
					curseur.getString(NUM_COLONNE_PSEUDO_UTILISATEUR),
					curseur.getString(NUM_COLONNE_URL_AVATAR_UTILISATEUR)
				));
			} while ( curseur.moveToNext() );
		}
		
		return 	listeUtilisateurs;
	}
	
	
	// récupère un utilisateur par son id
	public Cursor getUtilisateurById(String id) {
		curseur = bddCore.bdd.query(
			TABLE_UTILISATEUR,
			LISTE_COLONNES_TABLE_UTILISATEUR,
			COLONNE_ID_UTILISATEUR + " = '" + id + "'",
			null, 
			null, 
			null, 
			null
		);
		
		return curseur;
	}
}