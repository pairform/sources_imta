package cape.pairform.modele.bdd;

import java.util.ArrayList;
import java.util.HashMap;
import cape.pairform.modele.Cercle;
import cape.pairform.modele.Message;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.Tag;
import cape.pairform.modele.Utilisateur;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


/*
 * classe d'accès à la table Message
 */
public class MessageDAO implements ReferencesBDD {

	private static final String[] LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR = {
			// colonne de la table message
			COLONNE_ID_MESSAGE, COLONNE_ID_CAPSULE_MESSAGE, COLONNE_ID_AUTEUR, COLONNE_ID_ROLE_AUTEUR, COLONNE_NOM_PAGE, 
			COLONNE_NOM_TAG_PAGE, COLONNE_NUM_OCCURENCE_TAG_PAGE, COLONNE_CONTENU, COLONNE_ID_PARENT, COLONNE_UTILITE,
			COLONNE_MEDAILLE, COLONNE_VOTE_UTILITE, COLONNE_DEFI, COLONNE_REPONSE_DEFI_VALIDE, COLONNE_LU, COLONNE_SUPPRIME_PAR, COLONNE_ID_LANGUE_MESSAGE, 
			COLONNE_VISIBILITE_MESSAGE, COLONNE_DATE_CREATION_MESSAGE, COLONNE_DATE_EDITION_MESSAGE, COLONNE_GEO_LATITUDE, COLONNE_GEO_LONGITUDE,
			// colonne de la table utilisateur
			COLONNE_ID_UTILISATEUR, COLONNE_PSEUDO_UTILISATEUR, COLONNE_URL_AVATAR_UTILISATEUR
	};

	private static final String[] LISTE_COLONNES_TABLE_MESSAGE = {
			COLONNE_ID_MESSAGE, COLONNE_ID_CAPSULE_MESSAGE, COLONNE_ID_AUTEUR, COLONNE_ID_ROLE_AUTEUR, COLONNE_NOM_PAGE, 
			COLONNE_NOM_TAG_PAGE, COLONNE_NUM_OCCURENCE_TAG_PAGE, COLONNE_CONTENU, COLONNE_ID_PARENT, 
			COLONNE_UTILITE, COLONNE_MEDAILLE, COLONNE_VOTE_UTILITE, COLONNE_DEFI, COLONNE_REPONSE_DEFI_VALIDE,
			COLONNE_LU, COLONNE_SUPPRIME_PAR, COLONNE_ID_LANGUE_MESSAGE, COLONNE_VISIBILITE_MESSAGE, COLONNE_DATE_CREATION_MESSAGE, 
			COLONNE_DATE_EDITION_MESSAGE, COLONNE_GEO_LATITUDE, COLONNE_GEO_LONGITUDE
	};

	private BDDCore bddCore;

	private UtilisateurDAO utilisateurDAO;
	private TagDAO tagDAO;
	private PiecesJointesDAO pjsDAO;
	private ContentValues valeursMessage;
	private Cursor curseur;
	private ArrayList<Message> listeMessages;
	private HashMap<String, String> listeNbMessagesNonLus;
	private Message messageCourant;
	private String filtre;

	
	MessageDAO(Context context) {
		bddCore = BDDCore.getInstance(context);
		bddCore.bdd.execSQL("PRAGMA foreign_keys=ON");		// permet la suppression SQL en cascade via les clefs étrangères
		tagDAO = new TagDAO(bddCore.context);
		pjsDAO = new PiecesJointesDAO(bddCore.context);
	}

	
	public void close() {
		tagDAO.close();
		pjsDAO.close();
		if (curseur != null) {
			curseur.close();
		}
	}


	// si le message existe, on le met à jour, sinon, on l'ajout dans la BDD
	public void insertOrUpdateMessage(Message messageCourant) {
		if (curseur != null) {
			curseur.close();
		}

		// récupération du message, s'il existe
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE,
			LISTE_COLONNES_TABLE_MESSAGE,
			COLONNE_ID_MESSAGE +" = '"+ messageCourant.getId() +"'",
			null, 
			null, 
			null, 
			null
		);

		// si le message existe déjà en BDD
		if (curseur.moveToFirst()) {
			updateMessage(messageCourant);
		} else {
			insertMessage(messageCourant);
		}
	}


	// ajoute un nouveau message dans la BDD
	public long insertMessage(Message messageCourant){		
		valeursMessage = new ContentValues();
		valeursMessage.put(COLONNE_ID_MESSAGE, messageCourant.getId());
		valeursMessage.put(COLONNE_ID_CAPSULE_MESSAGE, messageCourant.getIdCapsule());
		valeursMessage.put(COLONNE_ID_AUTEUR, messageCourant.getAuteur().getId());
		valeursMessage.put(COLONNE_ID_ROLE_AUTEUR, messageCourant.getIdRoleAuteur());
		valeursMessage.put(COLONNE_NOM_PAGE, messageCourant.getNomPage());
		valeursMessage.put(COLONNE_NOM_TAG_PAGE, messageCourant.getNomTagPage());
		valeursMessage.put(COLONNE_NUM_OCCURENCE_TAG_PAGE, messageCourant.getNumOccurenceTagPage());
		valeursMessage.put(COLONNE_CONTENU, messageCourant.getContenu());
		valeursMessage.put(COLONNE_ID_PARENT, messageCourant.getIdParent());
		valeursMessage.put(COLONNE_UTILITE, messageCourant.getUtilite());
		valeursMessage.put(COLONNE_MEDAILLE, messageCourant.getMedaille());
		valeursMessage.put(COLONNE_VOTE_UTILITE, messageCourant.getVoteUtilite());
		valeursMessage.put(COLONNE_DEFI, messageCourant.getDefi());
		valeursMessage.put(COLONNE_REPONSE_DEFI_VALIDE, String.valueOf( messageCourant.isReponseDefiValide() ));
		valeursMessage.put(COLONNE_LU, String.valueOf( messageCourant.isLu() ));
		valeursMessage.put(COLONNE_SUPPRIME_PAR, messageCourant.getSupprimePar());
		valeursMessage.put(COLONNE_ID_LANGUE_MESSAGE, messageCourant.getIdLangue());
		valeursMessage.put(COLONNE_DATE_CREATION_MESSAGE, messageCourant.getDateCreation());
		valeursMessage.put(COLONNE_DATE_EDITION_MESSAGE, messageCourant.getDateEdition());
		valeursMessage.put(COLONNE_VISIBILITE_RESTREINTE, messageCourant.getVisibiliteRestreinte());
		valeursMessage.put(COLONNE_GEO_LATITUDE, messageCourant.getGeoLatitude());
		valeursMessage.put(COLONNE_GEO_LONGITUDE, messageCourant.getGeoLongitude());

		if (messageCourant.getVisibilite() != null)
			valeursMessage.put(COLONNE_VISIBILITE_MESSAGE, messageCourant.getVisibilite());			
		
		utilisateurDAO = new UtilisateurDAO(bddCore.context);
		utilisateurDAO.insertOrUpdateUtilisateur( messageCourant.getAuteur() );
		utilisateurDAO.close();

		long result = bddCore.bdd.insert(TABLE_MESSAGE, null, valeursMessage);

		for(Tag tagCourant : messageCourant.getListeTags()) {
			tagDAO.insertTag(tagCourant);
		}

		//IMPORTANT : on insert le message avec les pj pour les foreigns constraintes
		for(PieceJointe pj : messageCourant.getPiecesJointes()){
			pjsDAO.insertPieceJointe(pj);
		}

		return result;
	}

	
	// met à jour un message
	public long updateMessage(Message messageCourant) {
		valeursMessage = new ContentValues();
		valeursMessage.put(COLONNE_NOM_PAGE, messageCourant.getNomPage());
		valeursMessage.put(COLONNE_NOM_TAG_PAGE, messageCourant.getNomTagPage());
		valeursMessage.put(COLONNE_NUM_OCCURENCE_TAG_PAGE, messageCourant.getNumOccurenceTagPage());
		valeursMessage.put(COLONNE_UTILITE, messageCourant.getUtilite());
		valeursMessage.put(COLONNE_MEDAILLE, messageCourant.getMedaille());
		valeursMessage.put(COLONNE_VOTE_UTILITE, messageCourant.getVoteUtilite());
		valeursMessage.put(COLONNE_DEFI, messageCourant.getDefi());
		valeursMessage.put(COLONNE_REPONSE_DEFI_VALIDE, String.valueOf( messageCourant.isReponseDefiValide() ));
		valeursMessage.put(COLONNE_SUPPRIME_PAR, messageCourant.getSupprimePar());
		valeursMessage.put(COLONNE_ID_LANGUE_MESSAGE, messageCourant.getIdLangue());
		valeursMessage.put(COLONNE_DATE_CREATION_MESSAGE, messageCourant.getDateCreation());
		valeursMessage.put(COLONNE_DATE_EDITION_MESSAGE, messageCourant.getDateEdition());
		valeursMessage.put(COLONNE_VISIBILITE_RESTREINTE, messageCourant.getVisibiliteRestreinte());
		valeursMessage.put(COLONNE_GEO_LATITUDE, messageCourant.getGeoLatitude());
		valeursMessage.put(COLONNE_GEO_LONGITUDE, messageCourant.getGeoLongitude());

		for(Tag tagCourant : messageCourant.getListeTags()) {
			// si le tag n'est pas déjà en BDD
			if( !tagDAO.getTagByNomEtIdMessage(messageCourant.getId(), tagCourant.getNomTag()).moveToFirst() ) {
				tagDAO.insertTag(tagCourant);
			}
		}

		for(PieceJointe pj : messageCourant.getPiecesJointes()){
			pjsDAO.updatePieceJointe(pj);
		}

		return bddCore.bdd.update(
			TABLE_MESSAGE, 
			valeursMessage,
			COLONNE_ID_MESSAGE +" = '"+ messageCourant.getId() +"'",
			null
		);
	}
	

	// met à jour un message
	public long updateMessageIDAndParentID(String ancienID, String nouvelID, String parentID) {
		//On ne peut modifier la clé primaire
		//Donc je recupere l'ancien msg
		//Je change son id
		//Je le supprime de la base de donnée
		//Je le réinsert avec le bonne id
		Message msg = this.getMessageById(ancienID);
		msg.setId(nouvelID);
		msg.setIdParent(parentID);

		deleteMessageId(ancienID);

		//On ne veut pas réinsérer les pj et tag (fait par la methode d'insertion du message)
		msg.setPiecesJointes(new ArrayList<PieceJointe>());
		msg.setListeTags(new ArrayList<Tag>());

		insertMessage(msg);

		tagDAO.updateTagID(ancienID, nouvelID);
		pjsDAO.updatePieceJointeID(ancienID, nouvelID);

		return 1;
	}
	

	// met à jour des messages non-lus en messages lus
	public long updateMessagesNonLus(String idCapsule, String nomPage, String nomTagPage, String numOccurenceTagPage) {
		valeursMessage = new ContentValues();
		valeursMessage.put(COLONNE_LU, String.valueOf( true ));

		return bddCore.bdd.update(
			TABLE_MESSAGE, 
			valeursMessage,
			COLONNE_ID_CAPSULE_MESSAGE +" = '"+ idCapsule +"' AND "+ COLONNE_NOM_PAGE +" = '"+ nomPage +"' AND "+ COLONNE_NOM_TAG_PAGE +" = '"+ nomTagPage +"' AND "+ COLONNE_NUM_OCCURENCE_TAG_PAGE +" = "+ numOccurenceTagPage +" AND "+ COLONNE_LU +" = 'false'",
			null
		);
	}
	

	// supprime un message
	public long deleteMessageId(String id){
		return bddCore.bdd.delete(TABLE_MESSAGE, COLONNE_ID_MESSAGE + " = '" + id + "'", null);
	}
	
	
	// supprime tout les messages d'une capsule (ses tags, etc. sont supprimés en cascade)
	public void deleteMessagesByCapsule(String idCapsule) {
		bddCore.bdd.delete(
			TABLE_MESSAGE, 
			COLONNE_ID_CAPSULE_MESSAGE + " = '" + idCapsule + "'", 
			null
		);
	}


	// récupère les messages liés à la capsule (qui ne sont pas tout les messages de la capsule) ou lié à la page d'une capsule ou lié à un grain / OA d'une page
	public ArrayList<Message> getMessages(String idCapsule, String nomPage, String nomTagPage, String numOccurenceTagPage, String ordre, String languesAffichees) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR, 
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_ID_CAPSULE_MESSAGE +" = '"+ idCapsule +"' AND "+
				COLONNE_NOM_PAGE +" = '"+ nomPage +"' AND "+
				COLONNE_NOM_TAG_PAGE +" = '"+ nomTagPage +"' AND "+
				COLONNE_NUM_OCCURENCE_TAG_PAGE +" = "+ numOccurenceTagPage +" AND "+
				COLONNE_ID_PARENT +" = '0' AND "+
				COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +")",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE + ordre
		);

		construireListeMessages();

		return listeMessages;
	}

	// récupère les messages geolocalisé liés à la capsule (qui ne sont pas tout les messages de la capsule) ou lié à la page d'une capsule ou lié à un grain / OA d'une page
	public ArrayList<Message> getMessagesGeolocalise(String idCapsule, String nomPage, String nomTagPage, String numOccurenceTagPage, String ordre, String languesAffichees) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR, 
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_ID_CAPSULE_MESSAGE +" = '"+ idCapsule +"' AND "+ 
			COLONNE_NOM_PAGE +" = '"+ nomPage +"' AND " + 
				COLONNE_NOM_TAG_PAGE +" = '"+ nomTagPage +"' AND "+ 
				COLONNE_NUM_OCCURENCE_TAG_PAGE +" = "+ numOccurenceTagPage +" AND "+
				COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +") AND " +
				COLONNE_GEO_LATITUDE + " != '0.0' AND " +
				COLONNE_GEO_LONGITUDE +" != '0.0'",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE + ordre
		);

		construireListeMessages();

		return listeMessages;
	}

	// récupère tout les messages (réponses comprises)
	public ArrayList<Message> getAllMessagesGeolocalise(String languesAffichees) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR, 
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +") AND " +
			COLONNE_GEO_LATITUDE + " != '0.0' AND " +
			COLONNE_GEO_LONGITUDE +" != '0.0'",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE +" DESC"
		);

		construireListeMessages();

		return listeMessages;
	}

	// récupère tout les messages (réponses comprises)
	public ArrayList<Message> getAllMessages(String languesAffichees) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR, 
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +")",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE +" DESC"
		);

		construireListeMessages();

		return listeMessages;
	}

	// Méthode renvoyant tous les messages avec un id négatif (= message non envoyés au serveur)
	public ArrayList<Message> getMessagesAsynchrone() {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR, 
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			null,
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE + " ASC"
		);

		construireListeMessagesAsynchrone();

		return listeMessages;
	}

	// récupère les réponses à un message
	public ArrayList<Message> getMessagesFils(String idParent, String ordre, String languesAffichees) {		
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR,  
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_ID_PARENT +" = '"+ idParent +"' AND "+ COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +")",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE + ordre
		);

		construireListeMessages();

		return listeMessages;
	}


	// récupère tout les messages d'un utilisateur
	public ArrayList<Message> getMessagesByUtilisateur(String idUtilisateur, String languesAffichees) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR,  
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_ID_UTILISATEUR +" = '"+ idUtilisateur +"'",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE +" DESC"
		);	

		construireListeMessages();

		return listeMessages;
	}


	// récupère tout les messages du cercle d'un utilisateur
	public ArrayList<Message> getMessagesByCercle(Cercle cercleCourant, String languesAffichees) {
		String where = null;

		for (Utilisateur membre : cercleCourant.getListeMembresCercle()) {
			if (where == null) {
				where = COLONNE_ID_UTILISATEUR +" = '"+ membre.getId() +"'";
			} else {
				where += " OR "+ COLONNE_ID_UTILISATEUR +" = '"+ membre.getId() +"'";
			}
		}

		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR,  
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			where +" AND "+ COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +")",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE +" DESC"
		);

		construireListeMessages();

		return listeMessages;
	}


	// récupère tout les messages contenant un tag donné
	public ArrayList<Message> getMessagesByTag(String nomTag, String languesAffichees) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR +" JOIN "+ TABLE_TAG +" ON "+ COLONNE_ID_MESSAGE +" = "+ COLONNE_ID_MESSAGE_TAG,
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_NOM_TAG +" = '"+ nomTag +"' AND "+ COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +")",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE +" DESC"
		);

		construireListeMessages();

		return listeMessages;
	}


	// récupère tout les messages d'une capsule
	public ArrayList<Message> getMessagesByCapsule(String idCapsule, String languesAffichees) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_UTILISATEUR +" ON "+ COLONNE_ID_AUTEUR +" = "+ COLONNE_ID_UTILISATEUR,
			LISTE_COLONNES_TABLES_MESSAGE_UTILISATEUR,
			COLONNE_ID_CAPSULE_MESSAGE +" = '"+ idCapsule +"' AND "+ COLONNE_ID_LANGUE_MESSAGE +" IN ("+ languesAffichees +")",
			null, 
			null, 
			null, 
			COLONNE_DATE_CREATION_MESSAGE +" DESC"
		);

		construireListeMessages();

		return listeMessages;
	}


	// récupère un message par son id
	public Message getMessageById(String id) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE,
			LISTE_COLONNES_TABLE_MESSAGE,
			COLONNE_ID_MESSAGE +" = '"+ id +"'",
			null, 
			null, 
			null, 
			null
		);

		if ( curseur.moveToFirst() ) {
			messageCourant = new Message(
				curseur.getString( NUM_COLONNE_ID ),
				curseur.getString( NUM_COLONNE_ID_CAPSULE_MESSAGE ),
				curseur.getString( curseur.getColumnIndex(COLONNE_ID_AUTEUR) ),
				curseur.getString( NUM_COLONNE_ID_ROLE_AUTEUR ),
				null,
				null,
				curseur.getString( NUM_COLONNE_NOM_PAGE ),
				curseur.getString( NUM_COLONNE_NOM_TAG_PAGE ),
				curseur.getInt( NUM_COLONNE_NUM_OCCURENCE_TAG_PAGE ),
				curseur.getString( NUM_COLONNE_CONTENU ),
				curseur.getString( NUM_COLONNE_ID_PARENT ),
				curseur.getInt( NUM_COLONNE_UTILITE ),
				curseur.getString( NUM_COLONNE_MEDAILLE ),
				curseur.getString( NUM_COLONNE_VOTE_UTILITE ),
				curseur.getInt( NUM_COLONNE_DEFI ),
				Boolean.parseBoolean(curseur.getString( NUM_COLONNE_REPONSE_DEFI_VALIDE )),
				Boolean.parseBoolean(curseur.getString( NUM_COLONNE_LU )),
				curseur.getString( NUM_COLONNE_SUPPRIME_PAR ),
				curseur.getString( NUM_COLONNE_ID_LANGUE_MESSAGE ),
				curseur.getLong( NUM_COLONNE_DATE_CREATION_MESSAGE ),
				curseur.getLong( NUM_COLONNE_DATE_EDITION_MESSAGE ),
				curseur.getString( NUM_COLONNE_GEO_LATITUDE ),
				curseur.getString( NUM_COLONNE_GEO_LONGITUDE ),
				Boolean.parseBoolean(curseur.getString( NUM_COLONNE_VISIBILITE_RESTREINTE ))
			);

			messageCourant.setListeTags( tagDAO.getTagsByIdMessage(messageCourant.getId()));		
			messageCourant.setPiecesJointes(pjsDAO.getPiecesJointesByIdMessage(messageCourant.getId()));
		}

		return messageCourant;
	}


	// récupère le nombre de réponses à un message
	public int getNbMessagesFils(String idParent) {		
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE,
			new String[] {COLONNE_ID_MESSAGE, COLONNE_ID_PARENT},
			COLONNE_ID_PARENT +" = '"+ idParent +"'",
			null, 
			null, 
			null, 
			null
		);

		return curseur.getCount();
	}


	// récupère le nombre total de messages non-lus de chaque ressource
	// la variable "lus" vaut true si l'on souhait récupérer les messages lus, false pour les messages non-lus
	public Cursor getNbMessagesByRessources(boolean lus) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_CAPSULE +" ON "+ COLONNE_ID_CAPSULE +" = "+ COLONNE_ID_CAPSULE_MESSAGE,
			new String[] {COLONNE_ID_RESSOURCE_CAPSULE, "COUNT("+ COLONNE_LU +") AS "+ COLONNE_LU },
			COLONNE_LU +" = '"+ lus + "'",
			null, 
			COLONNE_ID_RESSOURCE_CAPSULE, 
			null, 
			null
		);

		return curseur;
	}


	// récupère le nombre total de messages non-lus ou lus de chaque capsule
	// la variable "lus" vaut true si l'on souhait récupérer les messages lus, false pour les messages non-lus
	public Cursor getNbMessagesByCapsules(String idRessource, boolean lus) {
		// si un id de ressource a été précisé, on filtre les capsules avec cette ressource
		if (idRessource != null)
			filtre = " AND "+ COLONNE_ID_RESSOURCE_CAPSULE +" = "+ idRessource;

		curseur = bddCore.bdd.query(
			TABLE_MESSAGE +" JOIN "+ TABLE_CAPSULE +" ON "+ COLONNE_ID_CAPSULE +" = "+ COLONNE_ID_CAPSULE_MESSAGE,
			new String[] {COLONNE_ID_CAPSULE_MESSAGE, "COUNT("+ COLONNE_LU +") AS "+ COLONNE_LU },
			COLONNE_LU +" = '"+ lus + "'" + filtre,
			null, 
			COLONNE_ID_CAPSULE_MESSAGE, 
			null, 
			null
		);

		return curseur;
	}

	
	// récupère le nombre de messages non-lus ou lus sur le sommaire de la capsule et sur/dans chacune de ses pages
	// la variable "lus" vaut true si l'on souhait récupérer les messages lus, false pour les messages non-lus
	public HashMap<String, String> getNbMessagesByCapsule(String idCapsule, boolean lus) {
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE,
			new String[] {COLONNE_ID_CAPSULE_MESSAGE, COLONNE_NOM_PAGE, "COUNT("+ COLONNE_LU +") AS "+ COLONNE_LU },
			COLONNE_ID_CAPSULE_MESSAGE +" = '"+ idCapsule +"' AND "+ COLONNE_LU +" = '"+ lus + "'",
			null, 
			COLONNE_NOM_PAGE, 
			null, 
			null
		);

		// on passe les informations du curseur dans une hashmap
		listeNbMessagesNonLus = new HashMap<String, String>();
		while (curseur.moveToNext()) {
			listeNbMessagesNonLus.put(curseur.getString( curseur.getColumnIndex(COLONNE_NOM_PAGE) ), curseur.getString( curseur.getColumnIndex(COLONNE_LU) ));
		}

		return listeNbMessagesNonLus;
	}


	// récupère le nombre de messages non-lus sur chaque grains/OA de la page d'une capsule (s'il y en a)
	// la variable "lus" vaut true si l'on souhait récupérer les messages lus, false pour les messages non-lus
	public Cursor getNbMessagesByGrain(String idCapsule, String nomPage, boolean lus) {				
		curseur = bddCore.bdd.query(
			TABLE_MESSAGE,
			new String[] {COLONNE_ID_CAPSULE_MESSAGE, COLONNE_NOM_PAGE, COLONNE_NOM_TAG_PAGE, COLONNE_NUM_OCCURENCE_TAG_PAGE, "COUNT("+ COLONNE_LU +") AS "+ COLONNE_LU },
			COLONNE_ID_CAPSULE_MESSAGE +" = '"+ idCapsule +"' AND "+ COLONNE_NOM_PAGE +" = '"+ nomPage +"' AND "+ COLONNE_LU +" = '"+ lus + "'",
			null, 
			COLONNE_NOM_TAG_PAGE +", "+ COLONNE_NUM_OCCURENCE_TAG_PAGE,
			null, 
			null
		);

		return curseur;
	}

	
	// construit une liste de messages (une ArrayList<Message>) à partir d'un curseur
	private void construireListeMessages() {
		listeMessages = new ArrayList<Message>();
		if ( curseur.moveToFirst() ) {
			do {
				messageCourant = new Message(
					curseur.getString( NUM_COLONNE_ID ),
					curseur.getString( NUM_COLONNE_ID_CAPSULE_MESSAGE ),
					curseur.getString( curseur.getColumnIndex(COLONNE_ID_AUTEUR) ),
					curseur.getString( NUM_COLONNE_ID_ROLE_AUTEUR ),
					curseur.getString( curseur.getColumnIndex(COLONNE_PSEUDO_UTILISATEUR) ),
					curseur.getString( curseur.getColumnIndex(COLONNE_URL_AVATAR_UTILISATEUR) ),
					curseur.getString( NUM_COLONNE_NOM_PAGE ),
					curseur.getString( NUM_COLONNE_NOM_TAG_PAGE ),
					curseur.getInt( NUM_COLONNE_NUM_OCCURENCE_TAG_PAGE ),
					curseur.getString( NUM_COLONNE_CONTENU ),
					curseur.getString( NUM_COLONNE_ID_PARENT ),
					curseur.getInt( NUM_COLONNE_UTILITE ),
					curseur.getString( NUM_COLONNE_MEDAILLE ),
					curseur.getString( NUM_COLONNE_VOTE_UTILITE ),
					curseur.getInt( NUM_COLONNE_DEFI ),
					Boolean.parseBoolean(curseur.getString( NUM_COLONNE_REPONSE_DEFI_VALIDE )),
					Boolean.parseBoolean(curseur.getString( NUM_COLONNE_LU )),
					curseur.getString( NUM_COLONNE_SUPPRIME_PAR ),
					curseur.getString( NUM_COLONNE_ID_LANGUE_MESSAGE ),
					curseur.getLong( NUM_COLONNE_DATE_CREATION_MESSAGE ),
					curseur.getLong( NUM_COLONNE_DATE_EDITION_MESSAGE ),
					curseur.getString( NUM_COLONNE_GEO_LATITUDE ),
					curseur.getString( NUM_COLONNE_GEO_LONGITUDE ),
					Boolean.parseBoolean(curseur.getString( NUM_COLONNE_VISIBILITE_RESTREINTE ))
				);

				messageCourant.setListeTags( tagDAO.getTagsByIdMessage(messageCourant.getId()));
				messageCourant.setPiecesJointes(pjsDAO.getPiecesJointesByIdMessage(messageCourant.getId()));

				listeMessages.add(messageCourant);
			} while (curseur.moveToNext());
		}
	}

	
	// construit une liste de messages (une ArrayList<Message>) à partir d'un curseur si l'id du message est négatif
	private void construireListeMessagesAsynchrone() {
		listeMessages = new ArrayList<Message>();
		if ( curseur.moveToFirst() ) {
			do {
				if(Long.valueOf(curseur.getString( NUM_COLONNE_ID )) < 0){					
					messageCourant = new Message(
						curseur.getString( NUM_COLONNE_ID ),
						curseur.getString( NUM_COLONNE_ID_CAPSULE_MESSAGE ),
						curseur.getString( curseur.getColumnIndex(COLONNE_ID_AUTEUR) ),
						curseur.getString( NUM_COLONNE_ID_ROLE_AUTEUR ),
						curseur.getString( curseur.getColumnIndex(COLONNE_PSEUDO_UTILISATEUR) ),
						curseur.getString( curseur.getColumnIndex(COLONNE_URL_AVATAR_UTILISATEUR) ),
						curseur.getString( NUM_COLONNE_NOM_PAGE ),
						curseur.getString( NUM_COLONNE_NOM_TAG_PAGE ),
						curseur.getInt( NUM_COLONNE_NUM_OCCURENCE_TAG_PAGE ),
						curseur.getString( NUM_COLONNE_CONTENU ),
						curseur.getString( NUM_COLONNE_ID_PARENT ),
						curseur.getInt( NUM_COLONNE_UTILITE ),
						curseur.getString( NUM_COLONNE_MEDAILLE ),
						curseur.getString( NUM_COLONNE_VOTE_UTILITE ),
						curseur.getInt( NUM_COLONNE_DEFI ),
						Boolean.parseBoolean(curseur.getString( NUM_COLONNE_REPONSE_DEFI_VALIDE )),
						Boolean.parseBoolean(curseur.getString( NUM_COLONNE_LU )),
						curseur.getString( NUM_COLONNE_SUPPRIME_PAR ),
						curseur.getString( NUM_COLONNE_ID_LANGUE_MESSAGE ),
						curseur.getLong( NUM_COLONNE_DATE_CREATION_MESSAGE ),
						curseur.getLong( NUM_COLONNE_DATE_EDITION_MESSAGE ),
						curseur.getString( NUM_COLONNE_GEO_LATITUDE ),
						curseur.getString( NUM_COLONNE_GEO_LONGITUDE ),
						Boolean.parseBoolean(curseur.getString( NUM_COLONNE_VISIBILITE_RESTREINTE ))
					);
					
					messageCourant.setVisibilite( curseur.getString( NUM_COLONNE_VISIBILITE_MESSAGE ) );
					messageCourant.setListeTags( tagDAO.getTagsByIdMessage(messageCourant.getId()));
					messageCourant.setPiecesJointes(pjsDAO.getPiecesJointesByIdMessage(messageCourant.getId()));

					listeMessages.add(messageCourant);
				}
			} while (curseur.moveToNext());
		}
	}
}
