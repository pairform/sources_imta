package cape.pairform.modele.bdd;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import cape.pairform.modele.PieceJointe;


/*
 * classe d'accès à la table PiecesJointes
 */
public class PiecesJointesDAO implements ReferencesBDD{

	private static final String[] LISTE_COLONNES_TABLE_PIECES_JOINTES = {
		COLONNE_PIECE_JOINTE_ID_MESSAGE,
		COLONNE_PIECE_JOINTE_NOM_ORIGINAL,
		COLONNE_PIECE_JOINTE_NOM_SERVEUR,
		COLONNE_PIECE_JOINTE_EXTENSION,
		COLONNE_PIECE_JOINTE_DONNEES_URL,
		COLONNE_PIECE_JOINTE_TAILLE
	};

	private ContentValues valeursPiecesJointes;
	private ArrayList<PieceJointe> listePiecesJointes;
	private Cursor cursor;
	private BDDCore bddCore;

	
	PiecesJointesDAO(Context context){
		bddCore = BDDCore.getInstance(context);
	}

	
	public void close(){
		if (cursor != null)
			cursor.close();
	}

	
	// si la pj existe, on le met à jour, sinon, on l'ajout dans la BDD
	public void insertOrUpdatePJ(PieceJointe pj) {
		//J'instancie un NOUVEAU curseur.
		//Si il y a plusieurs thread sur la base de données en même temps on pourrait corrompre le curseur
		Cursor curseur;

		//on récupère la pj si elle existe déjà
		if( pj.getNomServeur() == null || pj.getNomServeur().equals(""))
			curseur = getCurseurPieceJointeByNomOriginalEtIdMessage(pj.getIdMessage(), pj.getNomOriginal());
		else
			curseur = getCurseurPieceJointeByNomServeurEtIdMessage(pj.getIdMessage(), pj.getNomServeur());

		// si le message existe déjà en BDD
		if (curseur.moveToFirst())
			updatePieceJointe(pj);
		else
			insertPieceJointe(pj);
		
		curseur.close();
	}

	
	//Met à jour une pj dans la bdd
	public long updatePieceJointe(PieceJointe pj){
		valeursPiecesJointes = new ContentValues();			
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_NOM_ORIGINAL, pj.getNomOriginal());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_EXTENSION, pj.getExtension());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_DONNEES_URL, pj.getCheminVersThumbnail());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_TAILLE, pj.getTaille());

		String whereRequete;

		if( pj.getNomServeur() == null || pj.getNomServeur().equals(""))
			whereRequete = COLONNE_PIECE_JOINTE_ID_MESSAGE +" = '"+ pj.getIdMessage() +"' AND "+ COLONNE_PIECE_JOINTE_NOM_ORIGINAL +" = '"+ pj.getNomOriginal() +"'";
		else
			whereRequete = COLONNE_PIECE_JOINTE_ID_MESSAGE +" = '"+ pj.getIdMessage() +"' AND "+ COLONNE_PIECE_JOINTE_NOM_SERVEUR + " = '"+ pj.getNomServeur() +"'";

		return bddCore.bdd.update(TABLE_PIECES_JOINTES, valeursPiecesJointes, whereRequete, null);
	}

	
	//Supprime toutes les piece jointe d'un message
	public long deletePieceJointeAvecMessageID(String id){
		return bddCore.bdd.delete(TABLE_PIECES_JOINTES, COLONNE_PIECE_JOINTE_ID_MESSAGE + " = '" + id + "'", null);
	}

	
	public long updatePieceJointeID(String ancienID, String nouvelID){

		//Je recupère les pjs du message
		ArrayList<PieceJointe> pjs = this.getPiecesJointesByIdMessage(ancienID);
		//je supprime les donnée en base de donnée
		deletePieceJointeAvecMessageID(ancienID);

		//Je modifie et reinsert les donnée récupérer avec le bonne id
		for(PieceJointe pj : pjs){
			pj.setIdMessage(nouvelID);
			insertPieceJointe(pj);
		}

		return 1;
	}

	
	// insère une pj dans la BDD
	public long insertPieceJointe(PieceJointe pj){
		valeursPiecesJointes = new ContentValues();			
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_ID_MESSAGE, pj.getIdMessage());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_NOM_ORIGINAL, pj.getNomOriginal());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_NOM_SERVEUR, pj.getNomServeur());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_EXTENSION, pj.getExtension());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_DONNEES_URL, pj.getCheminVersThumbnail());
		valeursPiecesJointes.put(COLONNE_PIECE_JOINTE_TAILLE, pj.getTaille());

		return bddCore.bdd.insert(TABLE_PIECES_JOINTES, null, valeursPiecesJointes);
	}

	
	public ArrayList<PieceJointe> getPiecesJointesByIdMessage(String idMessage) {		
		cursor = bddCore.bdd.query(
			TABLE_PIECES_JOINTES,
			LISTE_COLONNES_TABLE_PIECES_JOINTES,
			COLONNE_PIECE_JOINTE_ID_MESSAGE + " = '" + idMessage + "'",
			null, 
			null, 
			null, 
			null
		);

		listePiecesJointes = new ArrayList<PieceJointe>();
		if ( cursor.moveToFirst() ) {
			do {									
				listePiecesJointes.add( new PieceJointe(
					cursor.getString(cursor.getColumnIndex(COLONNE_PIECE_JOINTE_ID_MESSAGE)),
					cursor.getString(cursor.getColumnIndex(COLONNE_PIECE_JOINTE_NOM_ORIGINAL)),
					cursor.getString(cursor.getColumnIndex(COLONNE_PIECE_JOINTE_NOM_SERVEUR)),
					cursor.getString(cursor.getColumnIndex(COLONNE_PIECE_JOINTE_EXTENSION)),
					cursor.getString(cursor.getColumnIndex(COLONNE_PIECE_JOINTE_DONNEES_URL)),
					Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLONNE_PIECE_JOINTE_TAILLE)))
				));
			} while ( cursor.moveToNext() );
		}

		return listePiecesJointes; 
	}

	
	public Cursor getCurseurPieceJointeByNomServeurEtIdMessage(String idMessage, String nomServeur) {		
		cursor = bddCore.bdd.query(
			TABLE_PIECES_JOINTES,
			LISTE_COLONNES_TABLE_PIECES_JOINTES,
			COLONNE_PIECE_JOINTE_ID_MESSAGE + " = '" + idMessage + "' AND " + COLONNE_PIECE_JOINTE_NOM_SERVEUR + " = '" + nomServeur + "'",
			null, 
			null, 
			null, 
			null
		);

		return cursor; 
	}

	
	public Cursor getCurseurPieceJointeByNomOriginalEtIdMessage(String idMessage, String nomOriginal) {		
		cursor = bddCore.bdd.query(
			TABLE_PIECES_JOINTES,
			LISTE_COLONNES_TABLE_PIECES_JOINTES,
			COLONNE_PIECE_JOINTE_ID_MESSAGE + " = '" + idMessage + "' AND " + COLONNE_PIECE_JOINTE_NOM_ORIGINAL + " = '" + nomOriginal + "'",
			null, 
			null, 
			null, 
			null
		);

		return cursor; 
	}

}
