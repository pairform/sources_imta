package cape.pairform.modele.bdd;


/*
 * Constantes utilisées avec la BDD, représente les colonnes et les numéros de colonnes des tables de la BDD
 */
public interface ReferencesBDD {
	
	public static final int NUM_COLONNE_ID = 0;
	
	
	/*
	 *  reference table ressource
	 */
	public static final String TABLE_RESSOURCE = "ressource";

	public static final int NUM_COLONNE_NOM_COURT_RESSOURCE = 1;
	public static final int NUM_COLONNE_NOM_LONG_RESSOURCE = 2;
	public static final int NUM_COLONNE_URL_LOGO = 3;
	public static final int NUM_COLONNE_NOM_ESPACE = 4;
	public static final int NUM_COLONNE_DESCRIPTION_RESSOURCE = 5;
	public static final int NUM_COLONNE_THEME = 6;
	public static final int NUM_COLONNE_ID_LANGUE_RESSOURCE = 7;
	public static final int NUM_COLONNE_ID_USAGE = 8;
	public static final int NUM_COLONNE_DATE_CREATION_RESSOURCE = 9;
	public static final int NUM_COLONNE_DATE_EDITION_RESSOURCE = 10;
	
	public static final String COLONNE_ID_RESSOURCE = "_id_ressource";
	public static final String COLONNE_NOM_COURT_RESSOURCE = "nom_court_ressource";
	public static final String COLONNE_NOM_LONG_RESSOURCE = "nom_long_ressource";
	public static final String COLONNE_URL_LOGO = "url_logo";
	public static final String COLONNE_NOM_ESPACE = "nom_espace";
	public static final String COLONNE_DESCRIPTION_RESSOURCE = "description_ressource";
	public static final String COLONNE_THEME = "theme";
	public static final String COLONNE_ID_LANGUE_RESSOURCE = "id_langue_ressource";
	public static final String COLONNE_ID_USAGE = "id_usage";
	public static final String COLONNE_DATE_CREATION_RESSOURCE = "date_creation_ressource";
	public static final String COLONNE_DATE_EDITION_RESSOURCE = "date_edition_ressource";
	
	
	/*
	 *  reference table capsule
	 */
	public static final String TABLE_CAPSULE = "capsule";

	public static final int NUM_COLONNE_RESSOURCE_CAPSULE = 1;
	public static final int NUM_COLONNE_ID_VISIBILITE = 2;
	public static final int NUM_COLONNE_NOM_COURT_CAPSULE = 3;
	public static final int NUM_COLONNE_NOM_LONG_CAPSULE = 4;
	public static final int NUM_COLONNE_URL_MOBILE = 5;
	public static final int NUM_COLONNE_DESCRIPTION_CAPSULE = 6;
	public static final int NUM_COLONNE_LICENCE = 7;
	public static final int NUM_COLONNE_TAILLE = 8;
	public static final int NUM_COLONNE_AUTEURS = 9;
	public static final int NUM_COLONNE_DATE_CREATION_CAPSULE = 10;
	public static final int NUM_COLONNE_DATE_EDITION_CAPSULE = 11;
	public static final int NUM_COLONNE_EST_TELECHARGEE_CAPSULE = 12;
	public static final int NUM_COLONNE_SELECTEURS_CAPSULE = 13;
	
	public static final String COLONNE_ID_CAPSULE = "_id_capsule";
	public static final String COLONNE_ID_RESSOURCE_CAPSULE = "id_ressource_capsule";
	public static final String COLONNE_ID_VISIBILITE = "id_visibilite";
	public static final String COLONNE_NOM_COURT_CAPSULE = "nom_court_capsule";
	public static final String COLONNE_NOM_LONG_CAPSULE = "nom_long_capsule";
	public static final String COLONNE_URL_MOBILE = "url_mobile";
	public static final String COLONNE_DESCRIPTION_CAPSULE = "description_capsule";
	public static final String COLONNE_LICENCE = "licence";
	public static final String COLONNE_TAILLE = "taille";
	public static final String COLONNE_AUTEURS = "auteurs";
	public static final String COLONNE_DATE_CREATION_CAPSULE = "date_creation_capsule";
	public static final String COLONNE_DATE_EDITION_CAPSULE = "date_edition_capsule";
	public static final String COLONNE_EST_TELECHARGEE_CAPSULE = "capsule_est_telechargee";
	public static final String COLONNE_SELECTEURS_CAPSULE = "selecteurs";
	
	
	/*
	 *  reference table message
	 */
	public static final String TABLE_MESSAGE = "message";
	
	public static final int NUM_COLONNE_ID_CAPSULE_MESSAGE = 1;
	public static final int NUM_COLONNE_ID_AUTEUR = 2;
	public static final int NUM_COLONNE_ID_ROLE_AUTEUR = 3;
	public static final int NUM_COLONNE_NOM_PAGE = 4;
	public static final int NUM_COLONNE_NOM_TAG_PAGE = 5;
	public static final int NUM_COLONNE_NUM_OCCURENCE_TAG_PAGE = 6;
	public static final int NUM_COLONNE_CONTENU = 7;
	public static final int NUM_COLONNE_ID_PARENT = 8;
	public static final int NUM_COLONNE_UTILITE = 9;
	public static final int NUM_COLONNE_MEDAILLE = 10;
	public static final int NUM_COLONNE_VOTE_UTILITE = 11;
	public static final int NUM_COLONNE_DEFI = 12;
	public static final int NUM_COLONNE_REPONSE_DEFI_VALIDE = 13;
	public static final int NUM_COLONNE_LU = 14;
	public static final int NUM_COLONNE_SUPPRIME_PAR = 15;
	public static final int NUM_COLONNE_ID_LANGUE_MESSAGE = 16;
	public static final int NUM_COLONNE_VISIBILITE_MESSAGE = 17;
	public static final int NUM_COLONNE_DATE_CREATION_MESSAGE = 18;
	public static final int NUM_COLONNE_DATE_EDITION_MESSAGE = 19;
	public static final int NUM_COLONNE_GEO_LATITUDE = 20;
	public static final int NUM_COLONNE_GEO_LONGITUDE = 21;
	public static final int NUM_COLONNE_VISIBILITE_RESTREINTE = 18;

	public static final String COLONNE_ID_MESSAGE = "_id_message";
	public static final String COLONNE_ID_CAPSULE_MESSAGE = "id_capsule_message";
	public static final String COLONNE_ID_AUTEUR = "id_auteur";
	public static final String COLONNE_ID_ROLE_AUTEUR = "role_auteur";
	public static final String COLONNE_NOM_PAGE = "nom_page";
	public static final String COLONNE_NOM_TAG_PAGE = "nom_tag_page";
	public static final String COLONNE_NUM_OCCURENCE_TAG_PAGE = "num_occurence_tag_page";
	public static final String COLONNE_CONTENU = "contenu";
	public static final String COLONNE_ID_PARENT = "id_parent";
	public static final String COLONNE_UTILITE = "utilite";
	public static final String COLONNE_MEDAILLE = "medaille";
	public static final String COLONNE_VOTE_UTILITE = "vote_utilite";
	public static final String COLONNE_DEFI = "defi";
	public static final String COLONNE_REPONSE_DEFI_VALIDE = "reponse_defi_valide";
	public static final String COLONNE_LU = "lu";
	public static final String COLONNE_SUPPRIME_PAR = "supprime_par";
	public static final String COLONNE_ID_LANGUE_MESSAGE = "id_langue_message";
	public static final String COLONNE_VISIBILITE_MESSAGE = "visibilite_message";
	public static final String COLONNE_DATE_CREATION_MESSAGE = "date_creation_message";
	public static final String COLONNE_DATE_EDITION_MESSAGE = "date_edition_message";
	public static final String COLONNE_GEO_LATITUDE = "geo_latitude";
	public static final String COLONNE_GEO_LONGITUDE = "geo_longitude";
	public static final String COLONNE_VISIBILITE_RESTREINTE = "prive";
	
	/*
	 * reference table piece jointe
	 */
	public static final String TABLE_PIECES_JOINTES = "piece_jointe";

	public static final int NUM_COLONNE_PIECE_JOINTE_ID = 1;
	public static final int NUM_COLONNE_PIECE_JOINTE_ID_MESSAGE = 2;
	public static final int NUM_COLONNE_PIECE_JOINTE_NOM_ORIGINAL = 3;
	public static final int NUM_COLONNE_PIECE_JOINTE_NOM_SERVEUR = 4;
	public static final int NUM_COLONNE_PIECE_JOINTE_EXTENTION = 5;
	public static final int NUM_COLONNE_PIECE_JOINTE_DONNEES_URL = 6;
	public static final int NUM_COLONNE_PIECE_JOINTE_TAILLE = 7;
	
	//TODO
	public static final String COLONNE_PIECE_JOINTE_ID = "id_pj";
	public static final String COLONNE_PIECE_JOINTE_ID_MESSAGE = "id_message";
	public static final String COLONNE_PIECE_JOINTE_NOM_ORIGINAL = "nom_original";
	public static final String COLONNE_PIECE_JOINTE_NOM_SERVEUR = "nom_serveur";
	public static final String COLONNE_PIECE_JOINTE_EXTENSION = "extension";
	public static final String COLONNE_PIECE_JOINTE_DONNEES_URL = "chemin_thumbnail";
	public static final String COLONNE_PIECE_JOINTE_TAILLE = "taille";

	
	/*
	 *  reference table tag
	 */
	public static final String TABLE_TAG = "tag";

	public static final int NUM_COLONNE_ID_MESSAGE_TAG = 1;
	public static final int NUM_COLONNE_NOM_TAG = 2;
	public static final int NUM_COLONNE_PSEUDO_AUTEUR = 3;

	public static final String COLONNE_ID_TAG = "_id_tag";
	public static final String COLONNE_ID_MESSAGE_TAG = "id_message_tag";
	public static final String COLONNE_NOM_TAG = "nom_tag";
	public static final String COLONNE_PSEUDO_AUTEUR = "pseudo_auteur";
	
	
	/*
	 *  reference table utilisateur
	 */
	public static final String TABLE_UTILISATEUR = "utilisateur";

	public static final int NUM_COLONNE_PSEUDO_UTILISATEUR = 1;
	public static final int NUM_COLONNE_URL_AVATAR_UTILISATEUR = 2;

	public static final String COLONNE_ID_UTILISATEUR = "_id_utilisateur";
	public static final String COLONNE_PSEUDO_UTILISATEUR = "pseudo_utilisateur";
	public static final String COLONNE_URL_AVATAR_UTILISATEUR = "url_avatar_utilisateur";

}