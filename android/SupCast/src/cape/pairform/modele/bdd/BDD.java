package cape.pairform.modele.bdd;

import java.io.File;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * BDD locale de l'application contenant les tables Message et Utilisateur
 */
public class BDD extends SQLiteOpenHelper implements ReferencesBDD {

	private static final String LOG_TAG = Utiles.class.getSimpleName();	
	public static final String NOM_BDD = "bdd_supcast.db";
	
	
	// table contenant les ressources de l'application
	private static final String CREATE_TABLE_RESSOURCE = "CREATE TABLE "+ TABLE_RESSOURCE +" ("+
		COLONNE_ID_RESSOURCE +" TEXT NOT NULL PRIMARY KEY, "+
		COLONNE_NOM_COURT_RESSOURCE +" TEXT NOT NULL, "+
		COLONNE_NOM_LONG_RESSOURCE +" TEXT NOT NULL, "+
		COLONNE_URL_LOGO +" TEXT NOT NULL, "+
		COLONNE_NOM_ESPACE +" TEXT NOT NULL, "+
		COLONNE_DESCRIPTION_RESSOURCE +" TEXT NOT NULL, "+
		COLONNE_THEME +" TEXT NOT NULL, "+
		COLONNE_ID_LANGUE_RESSOURCE +" TEXT NOT NULL, "+
		COLONNE_ID_USAGE +" INTEGER NOT NULL, "+
		COLONNE_DATE_CREATION_RESSOURCE +" TEXT NOT NULL, "+
		COLONNE_DATE_EDITION_RESSOURCE +" TEXT);";
	
	// table contenant les capsules des ressources de l'application
	private static final String CREATE_TABLE_CAPSULE = "CREATE TABLE "+ TABLE_CAPSULE +" ("+
		COLONNE_ID_CAPSULE +" TEXT NOT NULL PRIMARY KEY, "+
		COLONNE_ID_RESSOURCE_CAPSULE +" TEXT NOT NULL, "+
		COLONNE_ID_VISIBILITE +" TEXT NOT NULL, "+
		COLONNE_NOM_COURT_CAPSULE +" TEXT NOT NULL, "+
		COLONNE_NOM_LONG_CAPSULE +" TEXT NOT NULL, "+
		COLONNE_URL_MOBILE +" TEXT NOT NULL, "+
		COLONNE_DESCRIPTION_CAPSULE +" TEXT NOT NULL, "+
		COLONNE_LICENCE +" TEXT NOT NULL, "+
		COLONNE_TAILLE +" INTEGER NOT NULL, "+
		COLONNE_AUTEURS +" TEXT NOT NULL, "+
		COLONNE_DATE_CREATION_CAPSULE +" TEXT NOT NULL, "+
		COLONNE_DATE_EDITION_CAPSULE +" TEXT, "+
		COLONNE_EST_TELECHARGEE_CAPSULE +" TEXT NOT NULL, "+
		COLONNE_SELECTEURS_CAPSULE +" TEXT NOT NULL, "+
		"FOREIGN KEY("+ COLONNE_ID_RESSOURCE_CAPSULE +") REFERENCES "+ TABLE_RESSOURCE +"("+ COLONNE_ID_RESSOURCE +") ON DELETE CASCADE );";
	
	// Table contenant les messages de l'application 
	private static final String CREATE_TABLE_MESSAGE = "CREATE TABLE "+ TABLE_MESSAGE +" ("+
		COLONNE_ID_MESSAGE +" TEXT NOT NULL PRIMARY KEY, "+
		COLONNE_ID_CAPSULE_MESSAGE +" TEXT NOT NULL, "+ 
		COLONNE_ID_AUTEUR +" TEXT NOT NULL, "+ 
		COLONNE_ID_ROLE_AUTEUR +" TEXT NOT NULL, "+
		COLONNE_NOM_PAGE +" TEXT, "	+
		COLONNE_NOM_TAG_PAGE +" TEXT, "+ 
		COLONNE_NUM_OCCURENCE_TAG_PAGE +" INTEGER, "+ 
		COLONNE_CONTENU +" TEXT NOT NULL, "+ 
		COLONNE_ID_PARENT +" TEXT, "+ 
		COLONNE_UTILITE +" INTEGER, "+
		COLONNE_MEDAILLE +" TEXT, "+ 
		COLONNE_VOTE_UTILITE +" TEXT, "+
		COLONNE_DEFI +" INTEGER, "+
		COLONNE_REPONSE_DEFI_VALIDE +" TEXT, "+
		COLONNE_LU +" TEXT, "+
		COLONNE_SUPPRIME_PAR +" TEXT, "+
		COLONNE_ID_LANGUE_MESSAGE +" TEXT NOT NULL, "+
		COLONNE_VISIBILITE_MESSAGE +" TEXT, "+
		COLONNE_DATE_CREATION_MESSAGE +" TEXT NOT NULL, "+
		COLONNE_DATE_EDITION_MESSAGE +" TEXT, "+
		COLONNE_GEO_LATITUDE + " TEXT DEFAULT '0.0', " +
		COLONNE_GEO_LONGITUDE + " TEXT DEFAULT '0.0', " +
		COLONNE_VISIBILITE_RESTREINTE +" TEXT NOT NULL, "+
		"FOREIGN KEY("+ COLONNE_ID_CAPSULE_MESSAGE +") REFERENCES "+ TABLE_CAPSULE +"("+ COLONNE_ID_CAPSULE +") ON DELETE CASCADE );";
	
	// table contenant les pieces jointes des messages de la table TABLE_MESSAGE
	private static final String CREATE_TABLE_PIECES_JOINTES = "CREATE TABLE " + TABLE_PIECES_JOINTES + " (" +
		COLONNE_PIECE_JOINTE_ID_MESSAGE + " TEXT NOT NULL, " +
		COLONNE_PIECE_JOINTE_NOM_ORIGINAL + " TEXT NOT NULL, " +
		COLONNE_PIECE_JOINTE_NOM_SERVEUR + " TEXT, " +
		COLONNE_PIECE_JOINTE_TAILLE + " TEXT NOT NULL, " +
		COLONNE_PIECE_JOINTE_EXTENSION + " TEXT NOT NULL, " +
		COLONNE_PIECE_JOINTE_DONNEES_URL + " TEXT, " +
		"PRIMARY KEY ("	+ COLONNE_PIECE_JOINTE_ID_MESSAGE + ", " + COLONNE_PIECE_JOINTE_NOM_SERVEUR + "), " + 
		"FOREIGN KEY (" + COLONNE_PIECE_JOINTE_ID_MESSAGE + ") REFERENCES "
			+ TABLE_MESSAGE + "(" + COLONNE_ID_MESSAGE + ") ON DELETE CASCADE);";
	
	// table contenant les tags des messages de la table TABLE_MESSAGE
	private static final String CREATE_TABLE_TAG = "CREATE TABLE "+ TABLE_TAG +" ("+
		COLONNE_ID_TAG +" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "+
		COLONNE_ID_MESSAGE_TAG +" TEXT NOT NULL, "+
		COLONNE_NOM_TAG +" TEXT NOT NULL, "+
		COLONNE_PSEUDO_AUTEUR +" TEXT NOT NULL, "+
		"FOREIGN KEY("+ COLONNE_ID_MESSAGE_TAG +") REFERENCES "+ TABLE_MESSAGE +"("+ COLONNE_ID_MESSAGE +") ON DELETE CASCADE );";
	
	// table contenant les utilisateurs de l'application
	private static final String CREATE_TABLE_UTILISATEUR = "CREATE TABLE "+ TABLE_UTILISATEUR +" ("+
		COLONNE_ID_UTILISATEUR +" TEXT NOT NULL PRIMARY KEY, "+
		COLONNE_PSEUDO_UTILISATEUR +" TEXT NOT NULL, "+
		COLONNE_URL_AVATAR_UTILISATEUR +" TEXT NOT NULL);";
	
	@Override
	public void onCreate(SQLiteDatabase bdd) {
		bdd.execSQL(CREATE_TABLE_MESSAGE);
		bdd.execSQL(CREATE_TABLE_TAG);
		bdd.execSQL(CREATE_TABLE_UTILISATEUR);
		bdd.execSQL(CREATE_TABLE_RESSOURCE);
		bdd.execSQL(CREATE_TABLE_CAPSULE);
		bdd.execSQL(CREATE_TABLE_PIECES_JOINTES);
	}
	
	public BDD(Context context, CursorFactory factory, int versionBDD) {
		super(context, NOM_BDD, factory, versionBDD);
		
		// suppression des répertoires messages, capsules et ressources, elles sont trop différentes de l'existant
		if(versionBDD < 12003) {
			Log.v(LOG_TAG, "Mise à jour version : "+ versionBDD);
			File file = new File( context.getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath());
			Utiles.supprimerRepertoire(file);		
			file = new File( context.getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath());
			Utiles.supprimerRepertoire(file);		
			file = new File( context.getDir(Constantes.REPERTOIRE_PJS_HORS_LIGNE, Context.MODE_PRIVATE).getPath());
			Utiles.supprimerRepertoire(file);
			file = new File( context.getDir(Constantes.REPERTOIRE_PJS, Context.MODE_PRIVATE).getPath());
			Utiles.supprimerRepertoire(file);
		}
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase bdd, int oldVersion, int newVersion) {
		mettreAJourBDD(bdd, oldVersion);
	}
	
	private void mettreAJourBDD(SQLiteDatabase bdd, int version) {
		// mise à jour des tables selon la version de PairFormm présente sur le mobile
		switch (version) {
			case 12200:
				// pas de mise à jour de la BDD
				Log.v(LOG_TAG, "BDD à jour - version :"+ version);
				break;
			case 12101:
			case 12100:
				// mise à jour de la table Capsule (ajout d'une nouvelle colonne "selecteurs" + insertion d'une valeur par défaut)
				Log.v(LOG_TAG, "Mise à jour BDD version : "+ version);
				bdd.execSQL("ALTER TABLE "+ TABLE_MESSAGE +" ADD COLUMN "+ COLONNE_VISIBILITE_RESTREINTE +" TEXT NOT NULL DEFAULT '0';");
				//mettreAJourBDD(bdd, ...);
				break;
			case 12003:
				// mise à jour de la table Capsule (ajout d'une nouvelle colonne "selecteurs" + insertion d'une valeur par défaut)
				Log.v(LOG_TAG, "Mise à jour BDD version : "+ version);
				bdd.execSQL("ALTER TABLE "+ TABLE_CAPSULE +" ADD COLUMN "+ COLONNE_SELECTEURS_CAPSULE +" TEXT NOT NULL DEFAULT '.mainContent_co p, .mainContent_ti, .mainContent_co .resInFlow';");
				mettreAJourBDD(bdd, 12101);
				break;
			default:
				// suppression des tables, elles sont trop différentes de l'existant
				Log.v(LOG_TAG, "réinitialisation BDD");
				bdd.execSQL("DROP TABLE IF EXISTS "+ TABLE_MESSAGE +";");
				bdd.execSQL("DROP TABLE IF EXISTS "+ TABLE_TAG +";");
				bdd.execSQL("DROP TABLE IF EXISTS "+ TABLE_PIECES_JOINTES +";");
				bdd.execSQL("DROP TABLE IF EXISTS "+ TABLE_UTILISATEUR +";");
				bdd.execSQL("DROP TABLE IF EXISTS "+ TABLE_CAPSULE +";");
				bdd.execSQL("DROP TABLE IF EXISTS "+ TABLE_RESSOURCE +";");				
				onCreate(bdd);
				break;
		}
	}

}
