package cape.pairform.modele.bdd;

import android.content.Context;

/**
 * Classe Factory qui permet de regrouper les différentes instances possible pour chaque table de la base de données.
 * Il est important de toujours passer par là pour centraliser le fonctionnement de la base de données
 * Les constructeurs des différentes tables (TagDAO, ...) sont package protected. Ainsi il est impossible de les instancier hors de cette classe (ou dans le package)
 * 
 * @author vincent
 *
 */
public class BDDFactory {
	
	/**
	 * Fonction retournant un objet vers la table PiecesJointesDAO
	 * @param context Le context général ou spécifique de l'activity
	 * @return Une nouvelle instance PiecesJointesDAO
	 */
	public static PiecesJointesDAO getPieceJointeDAO(Context context){
		return new PiecesJointesDAO(context);
	}
	
	/**
	 * Fonction retournant un objet vers la table CapsuleDAO
	 * @param context Le context général ou spécifique de l'activity
	 * @return Une nouvelle instance CapsuleDAO
	 */
	public static CapsuleDAO getCapsuleDAO(Context context){
		return new CapsuleDAO(context);
	}
	
	/**
	 * Fonction retournant un objet vers la table MessageDAO
	 * @param context Le context général ou spécifique de l'activity
	 * @return Une nouvelle instance MessageDAO
	 */
	public static MessageDAO getMessageDAO(Context context){
		return new MessageDAO(context);
	}
	
	/**
	 * Fonction retournant un objet vers la table UtilisateurDAO
	 * @param context Le context général ou spécifique de l'activity
	 * @return Une nouvelle instance UtilisateurDAO
	 */
	public static UtilisateurDAO getUtilisateurDAO(Context context){
		return new UtilisateurDAO(context);
	}	
	
	/**
	 * Fonction retournant un objet vers la table TagDAO
	 * @param context Le context général ou spécifique de l'activity
	 * @return Une nouvelle instance TagDAO
	 */
	public static TagDAO getTagDAO(Context context){
		return new TagDAO(context);
	}
	
	/**
	 * Fonction retournant un objet vers la table RessourceDAO
	 * @param context Le context général ou spécifique de l'activity
	 * @return Une nouvelle instance RessourceDAO
	 */
	public static RessourceDAO getRessourceDAO(Context context){
		return new RessourceDAO(context);
	}
}
