package cape.pairform.modele.bdd;

import java.util.ArrayList;
import cape.pairform.modele.Ressource;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


/*
 * classe d'accès à la table Ressource
 */
public class RessourceDAO implements ReferencesBDD {
	
	private static final String[] LISTE_COLONNES_TABLE_RESSOURCE = {
		COLONNE_ID_RESSOURCE, COLONNE_NOM_COURT_RESSOURCE, COLONNE_NOM_LONG_RESSOURCE, COLONNE_URL_LOGO, COLONNE_NOM_ESPACE, COLONNE_DESCRIPTION_RESSOURCE,
		COLONNE_THEME, COLONNE_ID_LANGUE_RESSOURCE, COLONNE_ID_USAGE, COLONNE_DATE_CREATION_RESSOURCE, COLONNE_DATE_EDITION_RESSOURCE
	};

	private BDDCore bddCore;
	private ContentValues valeursRessource;
	private Cursor curseur;
	private CapsuleDAO capsuleDAO;
	private Ressource ressourceCourante;
	private ArrayList<Ressource> listeRessources;
	
	
	RessourceDAO(Context context) {
		bddCore = BDDCore.getInstance(context);
		bddCore.bdd.execSQL("PRAGMA foreign_keys=ON");		// permet la suppression SQL en cascade via les clefs étrangères
		capsuleDAO = new CapsuleDAO(bddCore.context);
	}
 
	
	public void close() {
		capsuleDAO.close();
		if (curseur != null) {
			curseur.close();
		}
	}
	
	
	// si la ressource existe déjà en BDD, on la met à jour, sinon, on l'insert en BDD
	public void insertOrUpdateRessource(Ressource ressourceCourante) {
		if ( isRessource(ressourceCourante.getId()) ) {
			updateRessource(ressourceCourante);
		} else {
			insertRessource(ressourceCourante);
		}
	}
	
	
	public long insertRessource(Ressource ressourceCourante){
		valeursRessource = new ContentValues();
		valeursRessource.put(COLONNE_ID_RESSOURCE, ressourceCourante.getId());
		valeursRessource.put(COLONNE_NOM_COURT_RESSOURCE, ressourceCourante.getNomCourt());
		valeursRessource.put(COLONNE_NOM_LONG_RESSOURCE, ressourceCourante.getNomLong());
		valeursRessource.put(COLONNE_URL_LOGO, ressourceCourante.getUrlLogo());
		valeursRessource.put(COLONNE_NOM_ESPACE, ressourceCourante.getNomEspace());
		valeursRessource.put(COLONNE_DESCRIPTION_RESSOURCE, ressourceCourante.getDescription());
		valeursRessource.put(COLONNE_THEME, ressourceCourante.getTheme());
		valeursRessource.put(COLONNE_ID_LANGUE_RESSOURCE, ressourceCourante.getIdLangue());
		valeursRessource.put(COLONNE_ID_USAGE, ressourceCourante.getIdUsage());
		valeursRessource.put(COLONNE_DATE_CREATION_RESSOURCE, ressourceCourante.getDateCreation());
		valeursRessource.put(COLONNE_DATE_EDITION_RESSOURCE, ressourceCourante.getDateEdition());
		
    	return bddCore.bdd.insert(TABLE_RESSOURCE, null, valeursRessource);
	}
	
	
	public long updateRessource(Ressource ressourceCourante) {
		valeursRessource = new ContentValues();
		valeursRessource.put(COLONNE_ID_RESSOURCE, ressourceCourante.getId());
		valeursRessource.put(COLONNE_NOM_COURT_RESSOURCE, ressourceCourante.getNomCourt());
		valeursRessource.put(COLONNE_NOM_LONG_RESSOURCE, ressourceCourante.getNomLong());
		valeursRessource.put(COLONNE_URL_LOGO, ressourceCourante.getUrlLogo());
		valeursRessource.put(COLONNE_DESCRIPTION_RESSOURCE, ressourceCourante.getDescription());
		valeursRessource.put(COLONNE_DATE_EDITION_RESSOURCE, ressourceCourante.getDateEdition());

    	return bddCore.bdd.update(TABLE_RESSOURCE, valeursRessource, COLONNE_ID_RESSOURCE + " = '" + ressourceCourante.getId() + "'", null);
	}
	
	
	// supprime une ressource (ses capsules, les messages des capsules, etc. sont supprimés en cascade)
	public void deleteRessource(String idRessource) {
		bddCore.bdd.delete(
			TABLE_RESSOURCE, 
			COLONNE_ID_RESSOURCE + " = '" + idRessource + "'", 
			null
		);
	}
	
	
	// récupère toutes les ressources et leurs capsules
	public ArrayList<Ressource> getAllRessources() {		
		curseur = bddCore.bdd.query(
			TABLE_RESSOURCE,
			LISTE_COLONNES_TABLE_RESSOURCE,
			null,
			null, 
			null, 
			null, 
			null
		);
		
		listeRessources = new ArrayList<Ressource>();
		if ( curseur.moveToFirst() ) {
			do {
				ressourceCourante = new Ressource(
					curseur.getString(NUM_COLONNE_ID),
					curseur.getString(NUM_COLONNE_NOM_COURT_RESSOURCE),
					curseur.getString(NUM_COLONNE_NOM_LONG_RESSOURCE),
					curseur.getString(NUM_COLONNE_URL_LOGO),
					curseur.getString(NUM_COLONNE_NOM_ESPACE),
					curseur.getString(NUM_COLONNE_DESCRIPTION_RESSOURCE),
					curseur.getString(NUM_COLONNE_THEME),
					curseur.getString(NUM_COLONNE_ID_LANGUE_RESSOURCE),
					curseur.getInt(NUM_COLONNE_ID_USAGE),
					curseur.getLong(NUM_COLONNE_DATE_CREATION_RESSOURCE),
					curseur.getLong(NUM_COLONNE_DATE_EDITION_RESSOURCE),
					null
				);

				ressourceCourante.setListeCapsules( capsuleDAO.getCapsulesByRessource(ressourceCourante.getId()));
				
				listeRessources.add(ressourceCourante);
			} while ( curseur.moveToNext() );
		}
		
		return 	listeRessources;
	}
	
	
	// récupère une ressource par son id
	public Ressource getRessourceById(String id) {
		curseur = bddCore.bdd.query(
			TABLE_RESSOURCE,
			LISTE_COLONNES_TABLE_RESSOURCE,
			COLONNE_ID_RESSOURCE + " = '" + id + "'",
			null, 
			null, 
			null, 
			null
		);
		
		if ( curseur.moveToFirst() ) {
			ressourceCourante = new Ressource(
				curseur.getString(NUM_COLONNE_ID),
				curseur.getString(NUM_COLONNE_NOM_COURT_RESSOURCE),
				curseur.getString(NUM_COLONNE_NOM_LONG_RESSOURCE),
				curseur.getString(NUM_COLONNE_URL_LOGO),
				curseur.getString(NUM_COLONNE_NOM_ESPACE),
				curseur.getString(NUM_COLONNE_DESCRIPTION_RESSOURCE),
				curseur.getString(NUM_COLONNE_THEME),
				curseur.getString(NUM_COLONNE_ID_LANGUE_RESSOURCE),
				curseur.getInt(NUM_COLONNE_ID_USAGE),
				curseur.getLong(NUM_COLONNE_DATE_CREATION_RESSOURCE),
				curseur.getLong(NUM_COLONNE_DATE_EDITION_RESSOURCE),
				null
			);
			ressourceCourante.setListeCapsules( capsuleDAO.getCapsulesByRessource(ressourceCourante.getId()));
		} else {
			ressourceCourante = null;
		}
		
		return ressourceCourante;
	}
	
	
	// récupère la langue d'une ressource
	public String getLangueRessource(String idRessource) {
		String idLangue = null;
		
		curseur = bddCore.bdd.query(
			TABLE_RESSOURCE,
			LISTE_COLONNES_TABLE_RESSOURCE,
			COLONNE_ID_RESSOURCE + " = '" + idRessource + "'",
			null, 
			null, 
			null, 
			null
		);
		
		if ( curseur.moveToFirst() ) {
			idLangue = curseur.getString(NUM_COLONNE_ID_LANGUE_RESSOURCE);
		}		
		return idLangue;
	}
	
	
	// test si la ressource existe en BDD
	public boolean isRessource(String id) {
		curseur = bddCore.bdd.query(
			TABLE_RESSOURCE,
			LISTE_COLONNES_TABLE_RESSOURCE,
			COLONNE_ID_RESSOURCE + " = '" + id + "'",
			null, 
			null, 
			null, 
			null
		);
		
		return curseur.moveToFirst();
	}
}