package cape.pairform.modele.bdd;

import java.util.ArrayList;
import java.util.HashMap;
import cape.pairform.modele.Capsule;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


/*
 * classe d'accès à la table Capsule
 */
public class CapsuleDAO implements ReferencesBDD {
	
	private static final String[] LISTE_COLONNES_TABLE_CAPSULE = {
		COLONNE_ID_CAPSULE, COLONNE_ID_RESSOURCE_CAPSULE, COLONNE_ID_VISIBILITE, COLONNE_NOM_COURT_CAPSULE, COLONNE_NOM_LONG_CAPSULE,
		COLONNE_URL_MOBILE, COLONNE_DESCRIPTION_CAPSULE, COLONNE_LICENCE, COLONNE_TAILLE, COLONNE_AUTEURS, COLONNE_DATE_CREATION_CAPSULE,
		COLONNE_DATE_EDITION_CAPSULE, COLONNE_EST_TELECHARGEE_CAPSULE, COLONNE_SELECTEURS_CAPSULE
	};
	
	private ContentValues valeursCapsule;
	private Cursor curseur;
	private ArrayList<Capsule> listeCapsules;
	private BDDCore bddCore;
	
	
	CapsuleDAO(Context context) {
		bddCore = BDDCore.getInstance(context);
		bddCore.bdd.execSQL("PRAGMA foreign_keys=ON");	// permet la suppression SQL en cascade via les clefs étrangères
	}

	
	public void close() {
		if (curseur != null) {
			curseur.close();
		}
	}
	
	
	public void insertOrUpdateCapsule(Capsule capsuleCourant) {
		curseur = getCapsuleById(capsuleCourant.getId());
		
		if (curseur.moveToFirst()) {
			updateCapsule(capsuleCourant);
		} else {
			insertCapsule(capsuleCourant);
		}
	}
	
	
	public long insertCapsule(Capsule capsuleCourant){		
		valeursCapsule = new ContentValues();
		valeursCapsule.put(COLONNE_ID_CAPSULE, capsuleCourant.getId());
		valeursCapsule.put(COLONNE_ID_RESSOURCE_CAPSULE, capsuleCourant.getIdRessource());
		valeursCapsule.put(COLONNE_ID_VISIBILITE, capsuleCourant.getIdVisibilite());
		valeursCapsule.put(COLONNE_NOM_COURT_CAPSULE, capsuleCourant.getNomCourt());
		valeursCapsule.put(COLONNE_NOM_LONG_CAPSULE, capsuleCourant.getNomLong());
		valeursCapsule.put(COLONNE_URL_MOBILE, capsuleCourant.getUrlMobile());
		valeursCapsule.put(COLONNE_DESCRIPTION_CAPSULE, capsuleCourant.getDescription());
		valeursCapsule.put(COLONNE_LICENCE, capsuleCourant.getLicence());
		valeursCapsule.put(COLONNE_TAILLE, capsuleCourant.getTaille());
		valeursCapsule.put(COLONNE_AUTEURS, capsuleCourant.getAuteurs());
		valeursCapsule.put(COLONNE_DATE_CREATION_CAPSULE, capsuleCourant.getDateCreation());
		valeursCapsule.put(COLONNE_DATE_EDITION_CAPSULE, capsuleCourant.getDateEdition());
		valeursCapsule.put(COLONNE_EST_TELECHARGEE_CAPSULE, String.valueOf(capsuleCourant.isTelechargee()));
		valeursCapsule.put(COLONNE_SELECTEURS_CAPSULE, capsuleCourant.getSelecteurs());
		
    	return bddCore.bdd.insert(TABLE_CAPSULE, null, valeursCapsule);
	}
	
	
	public long updateCapsule(Capsule capsuleCourant) {
		valeursCapsule = new ContentValues();
		valeursCapsule.put(COLONNE_ID_RESSOURCE_CAPSULE, capsuleCourant.getIdRessource());
		valeursCapsule.put(COLONNE_ID_VISIBILITE, capsuleCourant.getIdVisibilite());
		valeursCapsule.put(COLONNE_NOM_COURT_CAPSULE, capsuleCourant.getNomCourt());
		valeursCapsule.put(COLONNE_NOM_LONG_CAPSULE, capsuleCourant.getNomLong());
		valeursCapsule.put(COLONNE_URL_MOBILE, capsuleCourant.getUrlMobile());
		valeursCapsule.put(COLONNE_DESCRIPTION_CAPSULE, capsuleCourant.getDescription());
		valeursCapsule.put(COLONNE_LICENCE, capsuleCourant.getLicence());
		valeursCapsule.put(COLONNE_TAILLE, capsuleCourant.getTaille());
		valeursCapsule.put(COLONNE_AUTEURS, capsuleCourant.getAuteurs());
		valeursCapsule.put(COLONNE_DATE_EDITION_CAPSULE, capsuleCourant.getDateEdition());
		valeursCapsule.put(COLONNE_EST_TELECHARGEE_CAPSULE, String.valueOf(capsuleCourant.isTelechargee()));
		valeursCapsule.put(COLONNE_SELECTEURS_CAPSULE, capsuleCourant.getSelecteurs());
		
    	return bddCore.bdd.update(TABLE_CAPSULE, valeursCapsule, COLONNE_ID_CAPSULE + " = '" + capsuleCourant.getId() + "'", null);
	}
	
	
	// récupère toutes les Capsules
	public ArrayList<Capsule> getAllCapsules() {		
		curseur = bddCore.bdd.query(
			TABLE_CAPSULE,
			LISTE_COLONNES_TABLE_CAPSULE,
			null,
			null, 
			null, 
			null, 
			null
		);		
		construireListeCapsules();
		
		return 	listeCapsules;
	}
	
	
	// récupère touts les id des Capsules
	public ArrayList<String> getAllIdCapsules() {		
		curseur = bddCore.bdd.query(
			TABLE_CAPSULE,
			new String[]{COLONNE_ID_CAPSULE},
			null,
			null, 
			null, 
			null, 
			null
		);		
		
		ArrayList<String> listeIdCapsules = new ArrayList<String>();
		if ( curseur.moveToFirst() ) {
			do {
				listeIdCapsules.add( curseur.getString(NUM_COLONNE_ID) );
			} while ( curseur.moveToNext() );
		}
			
		return 	listeIdCapsules;
	}
	
	
	// récupère la liste des Capsules d'une ressource
	public ArrayList<Capsule>  getCapsulesByRessource(String idRessource) {
		curseur = bddCore.bdd.query(
			TABLE_CAPSULE,
			LISTE_COLONNES_TABLE_CAPSULE,
			COLONNE_ID_RESSOURCE_CAPSULE + " = '" + idRessource + "'",
			null, 
			null, 
			null, 
			null
		);		
		construireListeCapsules();
		
		return 	listeCapsules;
	}
	
	
	// récupère une Capsule par son id
	public Cursor getCapsuleById(String id) {
		curseur = bddCore.bdd.query(
			TABLE_CAPSULE,
			LISTE_COLONNES_TABLE_CAPSULE,
			COLONNE_ID_CAPSULE + " = '" + id + "'",
			null, 
			null, 
			null, 
			null
		);
		
		return curseur;
	}
	
	// récupère une liste clef (id de la capsule) / valeur (timestamp de la plus vieille date d'édition parmi les messages de la capsule) ; Cette liste permet de mettre à jour les messages en bdd locale des capsules téléchargées
	public HashMap<String,Long> getListeTimestampsMessagesCapsulesTelechargees() {
		curseur = bddCore.bdd.query(
			TABLE_CAPSULE +" LEFT JOIN "+ TABLE_MESSAGE +" ON "+ COLONNE_ID_CAPSULE +" = "+ COLONNE_ID_CAPSULE_MESSAGE,
			new String[]{COLONNE_ID_CAPSULE, "MAX("+ COLONNE_DATE_EDITION_MESSAGE +") AS "+ COLONNE_DATE_EDITION_MESSAGE, COLONNE_DATE_CREATION_CAPSULE},
			COLONNE_EST_TELECHARGEE_CAPSULE +" = 'true'",
			null, 
			COLONNE_ID_CAPSULE_MESSAGE, 
			null, 
			null
		);
		
		HashMap<String,Long> listeTimestampsMessagesCapsules = new HashMap<String,Long>();
		if ( curseur.moveToFirst() ) {
			do {				
				listeTimestampsMessagesCapsules.put(curseur.getString(NUM_COLONNE_ID), curseur.getLong( curseur.getColumnIndex(COLONNE_DATE_EDITION_MESSAGE) ));
			} while ( curseur.moveToNext() );
		}
		return 	listeTimestampsMessagesCapsules;
	}
	
	
	// récupère les sélecteurs d'une capsule
	public String getSelecteursCapsuleById(String id) {		
		curseur = bddCore.bdd.query(
			TABLE_CAPSULE,
			new String[]{COLONNE_SELECTEURS_CAPSULE},
			COLONNE_ID_CAPSULE + " = '" + id + "'",
			null, 
			null, 
			null, 
			null
		);		
		
		String selecteurs = "";
		if ( curseur.moveToFirst() )
			selecteurs = curseur.getString( curseur.getColumnIndex(COLONNE_SELECTEURS_CAPSULE));
			
		return 	selecteurs;
	}
		
		
	// construit une liste de capsules (une ArrayList<Capsule>) à partir d'un curseur
	private void construireListeCapsules() {
		listeCapsules = new ArrayList<Capsule>();
		if ( curseur.moveToFirst() ) {
			do {
				listeCapsules.add( new Capsule(
					curseur.getString(NUM_COLONNE_ID),
					curseur.getString(NUM_COLONNE_RESSOURCE_CAPSULE),
					curseur.getString(NUM_COLONNE_ID_VISIBILITE),
					curseur.getString(NUM_COLONNE_NOM_COURT_CAPSULE),
					curseur.getString(NUM_COLONNE_NOM_LONG_CAPSULE),
					curseur.getString(NUM_COLONNE_URL_MOBILE),
					curseur.getString(NUM_COLONNE_DESCRIPTION_CAPSULE),
					curseur.getString(NUM_COLONNE_LICENCE),
					curseur.getInt(NUM_COLONNE_TAILLE),
					curseur.getString(NUM_COLONNE_AUTEURS),
					curseur.getLong(NUM_COLONNE_DATE_CREATION_CAPSULE),
					curseur.getLong(NUM_COLONNE_DATE_EDITION_CAPSULE),
					Boolean.parseBoolean( curseur.getString(NUM_COLONNE_EST_TELECHARGEE_CAPSULE) ),
					curseur.getString(NUM_COLONNE_SELECTEURS_CAPSULE)
				));
			} while ( curseur.moveToNext() );
		}
	}
}