package cape.pairform.modele.bdd;

import java.util.ArrayList;
import cape.pairform.modele.Tag;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


/*
 * classe d'accès à la table Tag
 */
public class TagDAO implements ReferencesBDD {

	private static final String[] LISTE_COLONNES_TABLE_TAG = {
		COLONNE_ID_MESSAGE_TAG, COLONNE_NOM_TAG, COLONNE_PSEUDO_AUTEUR
	};

	private BDDCore bddCore;
	private ContentValues valeursTag;
	private Cursor curseur;
	private ArrayList<String> listeNomTags;
	private ArrayList<Tag> listeTags;
	
	
	TagDAO(Context context){
		bddCore = BDDCore.getInstance(context);
	}
 
	public void close(){
		if (curseur != null) {
			curseur.close();
		}
	}
	
	public long insertTag(Tag tagCourant){
		valeursTag = new ContentValues();			
		valeursTag.put(COLONNE_ID_MESSAGE_TAG, tagCourant.getIdMessage());
		valeursTag.put(COLONNE_NOM_TAG, tagCourant.getNomTag());
		valeursTag.put(COLONNE_PSEUDO_AUTEUR, tagCourant.getPseudoAuteur());

    	return bddCore.bdd.insert(TABLE_TAG, null, valeursTag);
	}
	
	public long updateTagID(String ancienID, String nouvelID){
		ContentValues valeursTag = new ContentValues();
		valeursTag.put(COLONNE_ID_MESSAGE_TAG, nouvelID);

		long result =  bddCore.bdd.update(
				TABLE_TAG, 
				valeursTag,
				COLONNE_ID_MESSAGE_TAG +" = '"+ ancienID +"'",
				null
				);
		
		return result;
	}
	
	// récupère tout les noms de tag contenus dans les messages en supprimant les doublons 
	public ArrayList<String> getAllNomsTags() {		
		curseur = bddCore.bdd.query(
			TABLE_TAG,
			new String[] {COLONNE_NOM_TAG},
			null,
			null, 
			COLONNE_NOM_TAG, 
			null, 
			null
		);
		
		listeNomTags = new ArrayList<String>();
		if ( curseur.moveToFirst() ) {
			do {										
				listeNomTags.add( curseur.getString( curseur.getColumnIndex(COLONNE_NOM_TAG) ) );
			} while ( curseur.moveToNext() );
		}
		
		return listeNomTags;
	}
	
	// récupère la liste de tags d'un message
	public ArrayList<Tag> getTagsByIdMessage(String idMessage) {		
		curseur = bddCore.bdd.query(
			TABLE_TAG,
			LISTE_COLONNES_TABLE_TAG,
			COLONNE_ID_MESSAGE_TAG + " = '" + idMessage + "'",
			null, 
			null, 
			null, 
			null
		);
		
		listeTags = new ArrayList<Tag>();
		if ( curseur.moveToFirst() ) {
			do {									
				listeTags.add( new Tag(
					curseur.getString(curseur.getColumnIndex(COLONNE_ID_MESSAGE_TAG)),
					curseur.getString(curseur.getColumnIndex(COLONNE_NOM_TAG)),
					curseur.getString(curseur.getColumnIndex(COLONNE_PSEUDO_AUTEUR))
				));
			} while ( curseur.moveToNext() );
		}
		
		return listeTags; 
	}
	
	// récupère un tag par son nom et l'id du message auquel il est associé
	public Cursor getTagByNomEtIdMessage(String idMessage, String nomTag) {		
		curseur = bddCore.bdd.query(
			TABLE_TAG,
			LISTE_COLONNES_TABLE_TAG,
			COLONNE_ID_MESSAGE_TAG + " = '" + idMessage + "' AND " + COLONNE_NOM_TAG + " = '" + nomTag + "'",
			null, 
			null, 
			null, 
			null
		);
		
		return curseur; 
	}
}
