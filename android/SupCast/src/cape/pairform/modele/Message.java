package cape.pairform.modele;

import java.util.ArrayList;


public class Message {

	private String id;
	private Utilisateur auteur;
	private String idRoleAuteur;
	private String idCapsule;
	private String nomPage;
	private String nomTagPage;
	private int numOccurenceTagPage;
	private String contenu;
	private ArrayList<Tag> listeTags = new ArrayList<Tag>();
	private String idParent;
	private int utilite;
	private String medaille;
	private String voteUtilite;
	private int defi;
	private boolean reponseDefiValide;
	private boolean lu;
	private String supprimePar;
	private String idLangue;
	private String visibilite;
	private boolean visibilite_restreinte;
	private long dateCreation;
	private long dateEdition;
	private ArrayList<PieceJointe> pjs;
	private double geoLatitude;
	private double geoLongitude;

	
	public Message(String id, String idCapsule, String idAuteur, String idRoleAuteur, String pseudoAuteur, 
			String urlAvatarAuteur, String nomPage, String nomTagPage, int numOccurenceTagPage, String contenu, 
			String idParent, int utilite, String medaille, String voteUtilite, int defi, 
			boolean reponseDefiValide, boolean lu, String supprimePar, String idLangue, long dateCreation, long dateEdition,
			String latitude, String longitude, boolean visibilite_restreinte) {
		this.id = id;
		this.auteur = new Utilisateur(idAuteur, pseudoAuteur, urlAvatarAuteur);
		this.idRoleAuteur = idRoleAuteur;
		this.idCapsule = idCapsule;
		this.nomPage = nomPage;
		this.nomTagPage = nomTagPage;
		this.numOccurenceTagPage = numOccurenceTagPage;
		this.contenu = contenu;
		this.idParent = idParent;
		this.utilite = utilite;
		this.medaille = medaille;
		this.voteUtilite = voteUtilite;
		this.defi = defi;
		this.reponseDefiValide = reponseDefiValide;
		this.lu = lu;
		this.supprimePar = supprimePar;
		this.idLangue = idLangue;
		this.dateCreation = dateCreation;
		this.dateEdition = dateEdition;
		this.visibilite_restreinte = visibilite_restreinte;

		this.pjs = new ArrayList<PieceJointe>();

		if(!latitude.isEmpty() && !longitude.isEmpty()){
			this.setGeoLatitude(Double.valueOf(latitude));
			this.setGeoLongitude(Double.valueOf(longitude));
		}
	}

	// nouveau message fraichement écrit et ne contenant que les infos dispo avant l'envoi du nouveau message au serveur
	public Message(String id, String idCapsule, String idAuteur, String pseudo, String urlAvatar, String idRoleAuteur, 
			String contenu, String idLangue, String nomPage, String nomTagPage, int numOccurenceTagPage,
			String idParent, int defi, String latitude, String longitude, long dateCreation, boolean visibilite_restreinte){
		this.id = id;
		this.auteur = new Utilisateur(idAuteur, pseudo, urlAvatar);
		this.idRoleAuteur = idRoleAuteur;
		this.idCapsule = idCapsule;
		this.nomPage = nomPage;
		this.nomTagPage = nomTagPage;
		this.numOccurenceTagPage = numOccurenceTagPage;
		this.contenu = contenu;
		this.idParent = idParent;
		this.utilite = 0;
		this.medaille = "";
		this.voteUtilite = "0";
		this.defi = defi;
		this.reponseDefiValide = false;
		this.lu = true;
		this.supprimePar = "0";
		this.idLangue = idLangue;
		this.dateCreation = dateCreation;
		this.visibilite_restreinte = visibilite_restreinte;
		this.dateEdition = 0;

		this.pjs = new ArrayList<PieceJointe>();
		if(!latitude.isEmpty() && !longitude.isEmpty()){
			this.setGeoLatitude(Double.valueOf(latitude));
			this.setGeoLongitude(Double.valueOf(longitude));
		}
	}

	/*
	 * Getters et setters
	 */

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Utilisateur getAuteur() {
		return auteur;
	}

	public void setAuteur(Utilisateur auteur) {
		this.auteur = auteur;
	}

	public String getIdRoleAuteur() {
		return idRoleAuteur;
	}

	public void setIdRoleAuteur(String idRoleAuteur) {
		this.idRoleAuteur = idRoleAuteur;
	}

	public String getIdCapsule() {
		return idCapsule;
	}

	public void setIdCapsule(String idCapsule) {
		this.idCapsule = idCapsule;
	}

	public String getNomPage() {
		return nomPage;
	}

	public void setNomPage(String nomPage) {
		this.nomPage = nomPage;
	}

	public String getNomTagPage() {
		return nomTagPage;
	}

	public void setNomTagPage(String nomTagPage) {
		this.nomTagPage = nomTagPage;
	}


	public int getNumOccurenceTagPage() {
		return numOccurenceTagPage;
	}

	public void setNumOccurenceTagPage(int numOccurenceTagPage) {
		this.numOccurenceTagPage = numOccurenceTagPage;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public ArrayList<Tag> getListeTags() {
		return listeTags;
	}

	public ArrayList<String> getNomTag(){
		ArrayList<String> name = new ArrayList<String>();

		for(int i = 0; i < listeTags.size(); i++)
			name.add(listeTags.get(i).getNomTag());

		return name;
	}

	public void setListeTags(ArrayList<Tag> listeTags) {
		this.listeTags = listeTags;
	}

	public void addTag(Tag tag) {
		this.listeTags.add(tag);
	}

	public void addTag(String nomTag, String pseudoAuteur) {
		this.listeTags.add( new Tag(this.id, nomTag, pseudoAuteur) );
	}

	public String getIdParent() {
		return idParent;
	}

	public void setIdParent(String idParent) {
		this.idParent = idParent;
	}

	public int getUtilite() {
		return utilite;
	}

	public void setUtilite(int utilite) {
		this.utilite = utilite;
	}

	public String getMedaille() {
		return medaille;
	}

	public void setMedaille(String medaille) {
		this.medaille = medaille;
	}

	public String getVoteUtilite() {
		return voteUtilite;
	}

	public void setVoteUtilite(String voteUtilite) {
		this.voteUtilite = voteUtilite;
	}

	public int getDefi() {
		return defi;
	}

	public void setDefi(int defi) {
		this.defi = defi;
	}

	public boolean isReponseDefiValide() {
		return reponseDefiValide;
	}

	public void setReponseDefiValide(boolean reponseDefiValide) {
		this.reponseDefiValide = reponseDefiValide;
	}

	public boolean isLu() {
		return lu;
	}

	public void setLu(boolean lu) {
		this.lu = lu;
	}

	public String getSupprimePar() {
		return supprimePar;
	}

	public void setSupprimePar(String supprimePar) {
		this.supprimePar = supprimePar;
	}

	public String getIdLangue() {
		return idLangue;
	}

	public void setIdLangue(String idLangue) {
		this.idLangue = idLangue;
	}
	
	public String getVisibilite() {
		return visibilite;
	}

	public void setVisibilite(String visibilite) {
		this.visibilite = visibilite;
	}

	public boolean getVisibiliteRestreinte() {
		return visibilite_restreinte;
	}

	public void setVisibiliteRestreinte(boolean visibilite_restreinte) {
		this.visibilite_restreinte = visibilite_restreinte;
	}

	public long getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(long dateCreation) {
		this.dateCreation = dateCreation;
	}

	public long getDateEdition() {
		return dateEdition;
	}

	public void setDateEdition(long dateEdition) {
		this.dateEdition = dateEdition;
	}

	public void ajoutePJ(PieceJointe pj){
		pjs.add(pj);
	}

	public ArrayList<PieceJointe> getPiecesJointes(){
		return this.pjs;
	}

	public void setPiecesJointes(ArrayList<PieceJointe> pjs){
		this.pjs = pjs;
	}

	public double getGeoLatitude() {
		return geoLatitude;
	}


	public void setGeoLatitude(double geoLatitude) {
		this.geoLatitude = geoLatitude;
	}


	public double getGeoLongitude() {
		return geoLongitude;
	}


	public void setGeoLongitude(double geoLongitude) {
		this.geoLongitude = geoLongitude;
	}
}
