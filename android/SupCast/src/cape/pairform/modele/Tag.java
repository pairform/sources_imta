package cape.pairform.modele;


public class Tag {

	private String idMessage;
	private String nomTag;
	private String pseudoAuteur;


	public Tag(String idMessage, String nomTag, String pseudoAuteur) {
		super();
		this.idMessage = idMessage;
		this.nomTag = nomTag;
		this.pseudoAuteur = pseudoAuteur;
	}


	/*
	 * Getters et setters
	 */

	public String getIdMessage() {
		return idMessage;
	}


	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}


	public String getNomTag() {
		return nomTag;
	}


	public void setNomTag(String nomTag) {
		this.nomTag = nomTag;
	}


	public String getPseudoAuteur() {
		return pseudoAuteur;
	}


	public void setPseudoAuteur(String pseudoAuteur) {
		this.pseudoAuteur = pseudoAuteur;
	}

	public String toString() {
		return "Auteur : " + pseudoAuteur + "\n"
				+ "idMessage : " + idMessage + "\n"
				+ "Tag : " + nomTag;		
	}
}
