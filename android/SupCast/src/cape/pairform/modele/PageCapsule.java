package cape.pairform.modele;


/*
 *  représente une page d'une ressource
 */
public class PageCapsule {
		
	private String titrePage;			// le titre de la page
	private String urlRelativePage;		// l'url relative où se trouve la page (ex: co/nom_page.html)
	
		
	public PageCapsule(String titrePage, String urlRelativePage) {
		this.titrePage = titrePage;
		this.urlRelativePage = urlRelativePage;
	}
	

	public String getTitrePage() {
		return titrePage;
	}
	
	public String getUrlRelativePage() {
		return urlRelativePage;
	}
}
