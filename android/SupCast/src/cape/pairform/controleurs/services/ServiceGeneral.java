package cape.pairform.controleurs.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.controleurs.services.communication.IMessage;
import cape.pairform.controleurs.services.communication.IServeurConnexion;
import cape.pairform.modele.Constantes;

/*
                      __------__
                    /~          ~\
                   |    //^\//^\|         Oh..My great god ...     
                 /~~\  ||  o| |o|:~\       Please grant me many many
                | |6   ||___|_|_||:|    /  error .. I want to give a clean
                 \__.  /      o  \/'       code for the new startup.
                  |   (       O   )        Amen! ^^
         /~~~~\    `\  \         /
        | |~~\ |     )  ~------~`\
       /' |  | |   /     ____ /~~~)\
      (_/'   | | |     /'    |    ( |
             | | |     \    /   __)/ \
             \  \ \      \/    /' \   `\
               \  \|\        /   | |\___|
                 \ |  \____/     | |
                 /^~>  \        _/ <
                |  |         \       \
                |  | \        \        \
                -^-\  \       |        )
                     `\_______/^\______/-jurcy
 */
/**
 * Service permettant de centraliser tous les accès réseaux
 */
public class ServiceGeneral extends ServiceAvecCommunication {

	private static boolean enFonctionnement = false; //Booléen représentant l'état du service
	private BroadcastReceiver brRecuperationMessage;
	private BroadcastReceiver brEnvoiMessage;
	private BroadcastReceiver brConnexion;

	public class ServiceGeneralBinder extends Binder {
		public ServiceGeneral getService() {
			return ServiceGeneral.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.v("TAG", "ServiceGeneral - onBind");
		return new ServiceGeneralBinder();
	}

	@Override
	public void onCreate() {
		Log.v("TAG", "ServiceGeneral - onCreate");
		super.onCreate();
		enFonctionnement = true;

		/*
		 * On défini le comportement de base du broadcast lors de la récupération des messages
		 */
		if(brRecuperationMessage == null){
			brRecuperationMessage = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					if( intent.getAction().equals( Constantes.ACTION_INTENT_RECUPERER_MESSAGES ) ) {
						// si la requete http s'est mal exécuté, intent.getStringExtra(Constantes.CLEF_INTENT_RECUPERER_MESSAGES) vaut null		    	    		
						for(IMessage obj : clientMessage)
							obj.messageMiseAJour(intent.getStringExtra(Constantes.CLEF_INTENT_RECUPERER_MESSAGES) != null);
					}
				}
			};
			registerReceiver(brRecuperationMessage, new IntentFilter( Constantes.ACTION_INTENT_RECUPERER_MESSAGES ));
		}
		/*
		 * Comportement par défaut du broadcast pour la gestion de l'envoi de message
		 */
		if(brEnvoiMessage == null){
			brEnvoiMessage = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					if( intent.getAction().equals( Constantes.ACTION_INTENT_ENVOYER_MESSAGES ) ) {
						mettreAJourMessage("-1");
					}
				}
			};
			registerReceiver(brEnvoiMessage, new IntentFilter( Constantes.ACTION_INTENT_ENVOYER_MESSAGES ));
		}
	}

	@Override
	public void onDestroy() {
		Log.v("TAG", "ServiceGeneral - onDestroy");
		super.onDestroy();
		//unregister tous les broadcast utilisé
		try{
			unregisterReceiver( brEnvoiMessage );
			unregisterReceiver( brRecuperationMessage );
			unregisterReceiver( brConnexion );
		}catch(IllegalArgumentException e){}
		enFonctionnement = false;
	}

	@Override
	public void onLowMemory() {
		// TODO Auto-generated method stub
		super.onLowMemory();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.v("TAG", "ServiceGeneral - onStartCommand");
		return START_STICKY;
	}

	public static boolean isRunning() {
		return enFonctionnement;
	}

	/**
	 * Méthode permettant de lancer la récupération des messages pour une capsule ou tous les messages
	 * ATTENTION : Ceci devrait être le comportement nomal mais la code pré existant ne tient pas compte de la capsule et récupère TOUS les messages
	 * @param idCapsule La capsule qui va être mise à jour (n'importe quoi fait l'affaire car paramètre pas utilisé pour le moment)
	 */
	public void mettreAJourMessage(String idCapsule){
		if(internetActif()){
			Intent intentCourante = new Intent(this, RecuperateurMessagesService.class);
			intentCourante.addFlags(Intent.FLAG_FROM_BACKGROUND);
			intentCourante.putExtra(Constantes.CLEF_ID_CAPSULE, idCapsule);
			//intentCourante.putExtra(Constantes.CLEF_FILTRE_CAPSULES, idCapsule);
			intentCourante.putExtra(Constantes.ACTION_INTENT_RECUPERER_MESSAGES, true);
			startService(intentCourante);
		}
	}

	/**
	 * Méthode permettant de lancer la connexion automatique si elle est activé par l'utilisateur
	 */
	protected void autoLoggin(){
		if(estLieAuServeur){
			return;
		}

		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, Context.MODE_PRIVATE);
		String loggin = fichierSharedPreferences.getString(Constantes.CLEF_PSEUDO_UTILISATEUR, null);
		String motDePasse = fichierSharedPreferences.getString(Constantes.CLEF_PASSWORD_UTILISATEUR, null);
		boolean autoLoggin = fichierSharedPreferences.getInt(Constantes.CLEF_CHOIX_CONNEXION, -1) == Constantes.CLEF_AUTO_LOGGIN;

		//Auto loggin
		if(loggin != null && motDePasse != null && autoLoggin){
			connecteUtilisateur(null, loggin, motDePasse);
		}
	}

	/**
	 * Permet de lancer l'envoi des messages au serveur
	 * Cette méthode peut être appelé en tous temps mais ne se déclenchera que SI l'utilisateur est connectée
	 */
	public void envoyerMessageAsynchrone(){

		if(internetActive && estLieAuServeur){
			Log.v("TAG2", "Service envoie message a synch");
			Intent intentCourante = new Intent(this, EnvoieMessagesService.class);
			intentCourante.addFlags(Intent.FLAG_FROM_BACKGROUND);
			startService(intentCourante);
		}else if (internetActive && !estLieAuServeur){
			autoLoggin();
		}
	}

	/**
	 * Méthode permettant de connecter l'utilisateur 
	 * @param listener Une interface d'écoute de la requete de connexion au serveur
	 * @param loggin Le login de l'utilisateur
	 * @param motDePasse Le mot de passe de l'utilisateur
	 */
	public void connecteUtilisateur(final OnConnectedListener listener, String loggin, String motDePasse) {
		/*
		 * La définition du broadcast est ici et non dans le onCreate à cause de l'ancien code
		 * L'interface d'écoute doit être parmis la définition du broadcast, le code pré-service, dépend de cette interface
		 */
		if(brConnexion == null){

			brConnexion = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					if( intent.getAction().equals( Constantes.ACTION_INTENT_CONNEXION ) ) {

						boolean resultat = false;
						int res = 0;

						if(intent.getStringExtra(Constantes.CLEF_INTENT_UTILISATEUR_CONNECTE).equals("-1")){
							res = -1;
						}else if(intent.getStringExtra(Constantes.CLEF_INTENT_UTILISATEUR_CONNECTE).equals("-2")){
							res = -2;
						}else{
							resultat = true;
							res = 0;
						}

						//L'utilisateur est connecté
						if(resultat){
							estLieAuServeur = true;
							envoyerMessageAsynchrone();
						}else{
							//L'utilisateur est déconnecté
							estLieAuServeur = false;
						}

						if(listener != null)
							listener.onConnected(resultat);

						for(IServeurConnexion obj : clientConnexion)
							obj.enregistrement(res);
					}
				}
			};


			registerReceiver(brConnexion, new IntentFilter( Constantes.ACTION_INTENT_CONNEXION ));
		}

		Intent intentCourante = new Intent(this, ConnexionServeurService.class);
		intentCourante.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intentCourante.putExtra(Constantes.CLEF_PSEUDO_UTILISATEUR, loggin);
		intentCourante.putExtra(Constantes.CLEF_PASSWORD_UTILISATEUR, motDePasse);
		startService(intentCourante);
	}
}
