package cape.pairform.controleurs.services;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Service permettant d'activer / désactiver l'email de notification d'une capsule
 */
public class ActivateurEmailNotificationService extends IntentService {
	
	private SharedPreferences.Editor editorFichierSharedPreferences;			// permet d'éditer les fichiers des préférences
	private String stringJSON;													// contenu d'une réponse JSON d'une requête http
	
	
	public ActivateurEmailNotificationService() {
		super(ActivateurEmailNotificationService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {
		boolean isChecked = intent.getBooleanExtra(Constantes.CLEF_NOTIFICATION_EMAIL, false);
		String idCapsule = intent.getStringExtra(Constantes.CLEF_ID_CAPSULE);
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CAPSULE, idCapsule) );		// id de la capsule
        } catch (Exception e) {
        	e.printStackTrace();
        }
        	
        // Envoie d'une requete HTTP permettant d'activer / désactiver l'email de notification d'une capsule
        stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_ACTIVER_EMAIL_NOTIFICATION, parametresRequete, this);
        
        // on parse le JSON
        if (stringJSON != null) {
        	String erreur = ParserJSON.parserRetourActiverEmailNotification(stringJSON);
                
	        if (erreur == null) {	            
		    	// on récupère le fichier de préfèrences stockant les informations sur utilisateur connecté et on le met à jour
		    	editorFichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).edit();		    	
				editorFichierSharedPreferences.putBoolean(Constantes.CLEF_NOTIFICATION_EMAIL + "_" + idCapsule, isChecked);
				editorFichierSharedPreferences.commit();
	        }
        }
	}
}
