package cape.pairform.controleurs.services;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Message;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.CapsuleDAO;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.PiecesJointesDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/*
 * Service récupèrant les nouveaux messages et les messages modifiés. 
 * Le service peut renvoyer une Intent pour indiquer qu'il a fini de récupérer les nouveaux messages
 */
public class RecuperateurMessagesService extends IntentService {
	
	private SharedPreferences fichierSharedPreferences;								// permet l'accès aux fichiers des préférences
	private SharedPreferences pjSharedPreferences;									// permet l'accès aux pjs des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;				// permet d'éditer les fichiers des préférences
	private SharedPreferences.Editor editorPjSharedPreferences;				// permet d'éditer les pjs des préférences
	private ArrayList<NameValuePair> parametresRequete;								// paramétres d'une requête Post
	private String stringJSON = null;												// contenu d'une réponse JSON d'une requête http
	private JSONObject jsonListeTimestampsCapsules;									// représentation JSON d'une liste clef(id_capsule) / valeur(le dernier timestamp parmis ceux des messages de la capsule)
	private ArrayList<Message> listeMessages = new ArrayList<Message>();			// liste des messages récupérés
	private MessageDAO messageDAO;													// permet l'accès à la table Message
	private PiecesJointesDAO pjsDAO;
	private String nomAvatarCourant;												// nom de l'avatar de l'auteur du message courant
	private String localisation;													// localisation ou sera stocké l'avatar courant
	
	public RecuperateurMessagesService() {
		super(RecuperateurMessagesService.class.getName());
	}
	
	@Override
	protected void onHandleIntent(final Intent intent) {
		// si elle existe, on récupère dans l'intent une liste avec pour chaque capsule : clef (id de la capsule) / valeur (timestamp de la plus vieille date d'édition parmi les messages de la capsule)
		// sinon, on récupère la liste en BDD locale
		if( intent.hasExtra(Constantes.CLEF_FILTRE_CAPSULES) ) {
			jsonListeTimestampsCapsules = new JSONObject( (HashMap<String,String>) intent.getSerializableExtra(Constantes.CLEF_FILTRE_CAPSULES) );
		} else {
			CapsuleDAO capsuleDAO = BDDFactory.getCapsuleDAO(this.getApplicationContext());
			jsonListeTimestampsCapsules = new JSONObject( capsuleDAO.getListeTimestampsMessagesCapsulesTelechargees() );
			capsuleDAO.close();
		}
		// création et configuration des paramètres d'une requête http
		parametresRequete = new ArrayList<NameValuePair>();
		parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_LANGUES_AFFICHAGE, Utiles.StringListeLanguesUtilisateur(this)) );		// langues des messages des préférences
		parametresRequete.add( new BasicNameValuePair(																						// capsules pour lesquelles on veut récupérer les messages
			Constantes.CLEF_FILTRE_CAPSULES,
			jsonListeTimestampsCapsules.toString()
		));
		
		// Envoie d'une requete HTTP récupérant les messages
		stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECUPERER_MESSAGE, parametresRequete, this);

		// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas messagesJSON
		if ( stringJSON != null ) {
			enregistrerMessages();
		}

		// si le service doit indiquer qu'il a finit de récupérer les nouveaux messages
		if ( intent.getBooleanExtra(Constantes.ACTION_INTENT_RECUPERER_MESSAGES, false)) {
			Intent intentRetour = new Intent( Constantes.ACTION_INTENT_RECUPERER_MESSAGES );
			intentRetour.putExtra(Constantes.CLEF_INTENT_RECUPERER_MESSAGES, stringJSON);
			sendBroadcast(intentRetour);		    		
		}
	}

	// Enregistrement des messages en BDD
	private void enregistrerMessages() {
		try {
			// on parse la liste des messages qui vont contenir chacun leur pjs
			listeMessages = ParserJSON.parserMessages( new JSONObject(stringJSON) );
		} catch (Exception e) {
			e.printStackTrace();
		}    

		messageDAO = BDDFactory.getMessageDAO( getApplicationContext() );
		pjsDAO = BDDFactory.getPieceJointeDAO(getApplicationContext());

		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0);
		editorFichierSharedPreferences = fichierSharedPreferences.edit();	

		// on insert les messages en BDD et on télécharge les avatars des auteurs des messages
		for (Message messageCourant : listeMessages) {    		
			nomAvatarCourant = messageCourant.getAuteur().getId();
			localisation = getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + nomAvatarCourant;

			// si l'avatar n'a pas déjà été téléchargé
			if ( !fichierSharedPreferences.contains( messageCourant.getAuteur().getUrlAvatar() ) ) {
				Utiles.telechargerFichier(messageCourant.getAuteur().getUrlAvatar(), localisation);

				editorFichierSharedPreferences.putString(messageCourant.getAuteur().getUrlAvatar(), messageCourant.getAuteur().getPseudo() );
				editorFichierSharedPreferences.commit();
			}
			messageCourant.getAuteur().setUrlAvatar( localisation );

			//Pour toutes les pjs du messages on télécharge ces pieces jointes sous formes de thumbnails
			for(PieceJointe pj : messageCourant.getPiecesJointes()){

				String cheminPj = getDir(Constantes.REPERTOIRE_PJS, Context.MODE_PRIVATE).getPath();
				pjSharedPreferences = getSharedPreferences(Constantes.FICHIER_LISTE_CHEMIN_THUMBNAILS, Context.MODE_PRIVATE);
				editorPjSharedPreferences = pjSharedPreferences.edit();	

				//-> Je sauve sur disque + sauve pj avec url vers tof
				if ( !pjSharedPreferences.contains(pj.getNomServeur())) {

					byte[] image = Utiles.telechargerThumbnail(pj, this); 
					Utiles.enregistrerFichier(cheminPj, pj.getNomServeur(), image);

					editorPjSharedPreferences.putString(pj.getNomServeur(), "");
					editorPjSharedPreferences.commit();
				}

				pj.setCheminVersThumbnail(cheminPj+"/"+pj.getNomServeur());

			}

			//delete old file
			ArrayList<PieceJointe> pjsAncienne = pjsDAO.getPiecesJointesByIdMessage(messageCourant.getId());

			//Si on rentre dans la boucle c'est qu'on avait déjà cette pj du à l'envoie asynchrone
			//La pj est alors une copie complete de celle qu'on veut envoyé. Son but est fini donc on la supprime du terminal
			for(PieceJointe pj : pjsAncienne){
				Utiles.supprimerRepertoire(new File(pj.getCheminVersThumbnail()));
			}

			messageDAO.insertOrUpdateMessage(messageCourant);
		}

		messageDAO.close();
	}
}
