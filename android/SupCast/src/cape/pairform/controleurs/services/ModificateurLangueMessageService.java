package cape.pairform.controleurs.services;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Service permettant de modifier la langue d'un message
 */
public class ModificateurLangueMessageService extends IntentService {

	public ModificateurLangueMessageService() {
		super(ModificateurLangueMessageService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {		
		String idMessage = intent.getStringExtra(Constantes.CLEF_ID_MESSAGE);
		String idLangue = intent.getStringExtra(Constantes.CLEF_WEBSERVICE_IDLANGUE_MESSAGE);
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE, idMessage) );
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_WEBSERVICE_IDLANGUE_MESSAGE, idLangue) );
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
            	
        // Envoie d'une requete HTTP modifiant la langue d'un message
    	Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_MODIFIER_LANGUE_MESSAGE, parametresRequete, this);
	}
}