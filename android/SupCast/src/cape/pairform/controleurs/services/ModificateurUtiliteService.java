package cape.pairform.controleurs.services;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;
import android.app.IntentService;
import android.content.Intent;


/*
 * Service modifiant l'utilité d'un message (ajoute ou soustraie 1)
 */
public class ModificateurUtiliteService extends IntentService {

	public ModificateurUtiliteService() {
		super(ModificateurUtiliteService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {
		String idMessage = intent.getStringExtra(Constantes.CLEF_ID_MESSAGE);
		String variationUtilite = String.valueOf( intent.getBooleanExtra(Constantes.CLEF_MODIF_UTILITE_MESSAGE, false) );
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE, idMessage) );
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_MODIF_UTILITE_MESSAGE, variationUtilite) );
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
        // Envoie d'une requete HTTP modifiant l'utilité d'un message
    	Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_MODIFIER_UTILITE_MESSAGE, parametresRequete, this);
	}
}
