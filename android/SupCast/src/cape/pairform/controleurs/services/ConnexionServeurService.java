package cape.pairform.controleurs.services;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.CapsuleDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Base64;


/*
 * Service récupèrant les nouveaux messages et les messages modifiés. 
 * Le service peut renvoyer une Intent pour indiquer qu'il a fini de récupérer les nouveaux messages
 */
public class ConnexionServeurService extends IntentService {

	private SharedPreferences fichierSharedPreferences;								// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;				// permet d'éditer les fichiers des préférences
	private String stringJSON = null;												// contenu d'une réponse JSON d'une requête http
	private String login, motDePasse;
	private HashMap<String,String> infosUtilisateur = null;

	public ConnexionServeurService() {
		super(ConnexionServeurService.class.getName());
	}

	@Override
	protected void onHandleIntent(final Intent intent) {

		String resultat = null;

		//On vérifie qu'on a bien les paramètre qui permettent de se logger.
		if( intent.hasExtra(Constantes.CLEF_PSEUDO_UTILISATEUR)  && intent.hasExtra(Constantes.CLEF_PASSWORD_UTILISATEUR)) {
			login =  intent.getStringExtra(Constantes.CLEF_PSEUDO_UTILISATEUR);
			motDePasse =  intent.getStringExtra(Constantes.CLEF_PASSWORD_UTILISATEUR);
		} else {
			return;
		}
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

		// préparation de l'encodage du mot de passe
		byte[] mdpBytes = null;
		try {
			mdpBytes = motDePasse.getBytes(HTTP.UTF_8);

			// récupération de la liste des id des capsules téléchargées
			CapsuleDAO capsuleDAO = BDDFactory.getCapsuleDAO(getApplicationContext());
			JSONArray jsonListeIdCapsules = new JSONArray(capsuleDAO.getAllIdCapsules());
			capsuleDAO.close();

			parametresRequete.add( new BasicNameValuePair(					// pseudo
				Constantes.CLEF_PSEUDO_UTILISATEUR,
				login
			));
			parametresRequete.add( new BasicNameValuePair(					// password encoder en base64
				Constantes.CLEF_MOT_DE_PASSE_UTILISATEUR,
				Base64.encodeToString(mdpBytes, Base64.DEFAULT)
			));
			parametresRequete.add( new BasicNameValuePair(					// liste des capsules actuelllement téléchargées sur le mobile
				Constantes.CLEF_ID_RESSOURCES_TELECHARGEES,
				jsonListeIdCapsules.toString()
			));
		} catch (Exception e) {
			e.printStackTrace();        	
		}
		// Envoie d'une requete HTTP connectant l'utilisateur
		stringJSON = Utiles.envoyerRequeteHttp(Constantes.URL_WEB_SERVICE_CONNEXION, parametresRequete, this);
		resultat = stringJSON;

		// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas stringJSON
		if ( stringJSON != null ) {
			//On enregistre les données sur les piece jointes en mémoire à travers un objet static
			ParserJSON.parserInformationsPieceJointe(stringJSON, this.getApplicationContext()).save();
			infosUtilisateur = ParserJSON.parserRetourConnexion( stringJSON );
		}

		if ( stringJSON == null ) {
			resultat = "-1";
		} else if (infosUtilisateur == null) {
			resultat = "-2";
		} else {
			// on récupère le fichier de préfèrences stockant les infos sur l'utilisateur et on y stocke les infos de l'utilisateur connecté (id, email, établissement, rôle dans chaque ressource, etc.)
			fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, Context.MODE_PRIVATE);
			editorFichierSharedPreferences = fichierSharedPreferences.edit();

			// pour chaque info sur l'utilisateur
			for( String clefInfos : infosUtilisateur.keySet() ) {
				// Si la clef correspond à l'info sur l'activation / désactivation de l'email de notification d'une ressource
				if( clefInfos.startsWith( Constantes.CLEF_NOTIFICATION_EMAIL ) ) {
					if (infosUtilisateur.get(clefInfos).equals("1") ) {
						editorFichierSharedPreferences.putBoolean(clefInfos, true);
					} else {
						editorFichierSharedPreferences.putBoolean(clefInfos, false);
					}
					// sinon si la clef correspond à une autre langue de l'utilisateur
				} else if( clefInfos.startsWith( Constantes.CLEF_AUTRES_LANGUES ) ) {
					editorFichierSharedPreferences.putBoolean(clefInfos, true);
				} else {
					editorFichierSharedPreferences.putString(clefInfos, infosUtilisateur.get(clefInfos));
				}
			}
			editorFichierSharedPreferences.commit();
		}

		Intent intentRetour = new Intent( Constantes.ACTION_INTENT_CONNEXION );
		intentRetour.putExtra(Constantes.CLEF_INTENT_UTILISATEUR_CONNECTE, resultat);
		sendBroadcast(intentRetour);  		
	}
}
