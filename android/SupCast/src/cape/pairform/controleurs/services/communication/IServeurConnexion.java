package cape.pairform.controleurs.services.communication;

public interface IServeurConnexion {
	/**
	 * Méthode appelé sur la réussite et l'échec de la connexion de l'utilisateur au serveur
	 * @param success -1 si pas de serveur, -2 si mauvais identifiant, 0 si ok autre (retour serveur)
	 */
	public void enregistrement(int success);

	/**
	 * Méthode appelé à tous changement matériel sur la connection.
	 * Si WIFI se connecte au reseau ou si puce data se connecte
	 * @param activate
	 */
	public void internetConnexion(boolean activate);
}
