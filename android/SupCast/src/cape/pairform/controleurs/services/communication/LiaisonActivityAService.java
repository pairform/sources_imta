package cape.pairform.controleurs.services.communication;

import java.util.HashMap;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import cape.pairform.controleurs.services.ServiceGeneral;

/*
 * 
 *                 /^\/^\
                  _|__|  O|
         \/     /~     \_/ \
          \____|__________/  \
                 \_______      \
                         `\     \                 \
                           |     |                  \
                          /      /                    \
                         /     /                       \
                       /      /                         \ \
                      /     /                            \  \
                    /     /             _----_            \   \
                   /     /           _-~      ~-_         |   |
                  (      (        _-~    _--_    ~-_     _/   |
                   \      ~-____-~    _-~    ~-_    ~-_-~    /
                     ~-_           _-~          ~-_       _-~
                        ~--______-~                ~-___-~


 * 
 * 
 */

/**
 * Classe permet de gérer les liaisons avec le service.
 * Toutes les liaisons doivent passer par cette classe.
 * Elle permet d'éviter la duplication de code et d'avoir une entrée général (type proxy) afin de savoir en tous temps qui écoute le service
 * @author vincent
 *
 */
public class LiaisonActivityAService {

	private HashMap<Context, ServiceConnection> activitéLieService;
	private HashMap<Context, Boolean> activiteEstLie;
	private static LiaisonActivityAService singleton;
	private boolean serviceDemarre = false;
	ServiceConnection tmp;

	public static LiaisonActivityAService getInstance(){
		if (singleton == null)
			singleton = new LiaisonActivityAService();

		return singleton;
	}

	private LiaisonActivityAService() {
		activitéLieService = new HashMap<Context, ServiceConnection>();
		activiteEstLie = new HashMap<Context, Boolean>();
	}

	public void addConnection(Context context, ServiceConnection connection){
		activitéLieService.put(context, connection);
		activiteEstLie.put(context, false);
		tmp = connection;
	}

	public void removeConnection(Context context) {
		activitéLieService.remove(context);
		activiteEstLie.remove(context);
	}

	public void bindActiviteService(Context context) {
		if(!serviceDemarre){
			context.startService(new Intent(context, ServiceGeneral.class));
			serviceDemarre = true;
		}
		
		if(activiteEstLie.get(context) == null || !activiteEstLie.get(context)){
			context.bindService(new Intent(context, ServiceGeneral.class), tmp, Context.BIND_NOT_FOREGROUND);
			activiteEstLie.put(context, true);
		}
	}

	public void unbindActiviteService(Context context) {
		if(activiteEstLie.get(context) != null && activitéLieService.get(context) != null){
			context.unbindService(tmp);
			activiteEstLie.put(context, false);
		}

	}
}
