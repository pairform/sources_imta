package cape.pairform.controleurs.services.communication;

public interface IMessage {
	/**
	 * Méthode appelé dès qu'une mise à jours des messages vient d'être effectuée
	 * @param reussite : Booléen représentant le succès de la récupération
	 */
	public void messageMiseAJour(boolean reussite);
	
}
