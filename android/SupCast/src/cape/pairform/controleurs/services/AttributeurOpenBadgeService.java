package cape.pairform.controleurs.services;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Service permettant d'attribuer un badge à un utilisateur dans une ressource
 */
public class AttributeurOpenBadgeService extends IntentService {

	public AttributeurOpenBadgeService() {
		super(AttributeurOpenBadgeService.class.getName());
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {		
		String idBadge = intent.getStringExtra(Constantes.CLEF_ID_OPENBADGE);
		String idUtilisateurCible = intent.getStringExtra(Constantes.CLEF_ID_UTILISATEUR);
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_OPENBADGE, idBadge) );
       		parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_UTILISATEUR_CIBLE, idUtilisateurCible) );
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
            	
        // Envoie d'une requete HTTP attribuant un badge
    	Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_ATTRIBUER_OPENBADGE, parametresRequete, this);
	}
}
