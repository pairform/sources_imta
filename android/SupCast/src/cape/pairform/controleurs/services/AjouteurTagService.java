package cape.pairform.controleurs.services;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Service ajoutant des tags à un message
 */
public class AjouteurTagService extends IntentService {

	public AjouteurTagService() {
		super(AjouteurTagService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {
		String idMessage = intent.getStringExtra( Constantes.CLEF_ID_MESSAGE );
		ArrayList<String> listeTags = intent.getStringArrayListExtra( Constantes.CLEF_TAGS_MESSAGE );
    	JSONArray jsonListeTags = new JSONArray(listeTags);
    	
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE, idMessage) );		// id du message sur lequel ajouter les tags
    		parametresRequete.add( new BasicNameValuePair(												// tags à ajouter au message
    			Constantes.CLEF_TAGS_MESSAGE,
    			jsonListeTags.toString())
    		);
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
        
        // Envoie d'une requete HTTP ajoutant des tags
        Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AJOUTER_TAGS_MESSAGE, parametresRequete, this);
	}
}
