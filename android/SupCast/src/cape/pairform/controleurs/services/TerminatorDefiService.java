package cape.pairform.controleurs.services;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * I'll be back...
 * 
 * Service modifiant l'état du défi, il devient terminer.
 */
public class TerminatorDefiService extends IntentService {

	public TerminatorDefiService() {
		super(TerminatorDefiService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {
		String idMessage = intent.getStringExtra(Constantes.CLEF_ID_MESSAGE);
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE, idMessage) );
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
    	
        // Envoie d'une requete HTTP terminant un défi
    	Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_TERMINER_DEFI, parametresRequete, this);
	}
}
