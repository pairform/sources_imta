package cape.pairform.controleurs.services;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Service modifiant la validité de la réponse à un défi, s'il était valide il devient invalide (et vice versa)
 */
public class ValidateurReponseDefiService extends IntentService {

	public ValidateurReponseDefiService() {
		super(ValidateurReponseDefiService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {		
		String idMessage = intent.getStringExtra(Constantes.CLEF_ID_MESSAGE);
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE, idMessage) );
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
    	
        // Envoie d'une requete HTTP modifiant la validité de la réponse à un défi
    	Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_VALIDER_REPONSE_DEFI, parametresRequete, this);
	}
}
