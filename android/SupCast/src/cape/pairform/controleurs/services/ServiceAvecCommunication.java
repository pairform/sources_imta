package cape.pairform.controleurs.services;

import java.util.ArrayList;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import cape.pairform.controleurs.services.communication.IMessage;
import cape.pairform.controleurs.services.communication.IServeurConnexion;
import cape.pairform.modele.Constantes;


/**
 * Classe abstraite qui va contenir une partier du code du service afin de ne pas tous regrouper en une classe
 * Elle va contenir tous ce qui touche les communications:
 * - Interface
 * - Internet
 * @author vincent
 *
 */

public abstract class ServiceAvecCommunication extends Service {

	protected ArrayList<IServeurConnexion> clientConnexion = new ArrayList<IServeurConnexion>();
	protected ArrayList<IMessage> clientMessage = new ArrayList<IMessage>();
	protected BroadcastReceiver connexionReceiver; //Broadcast ecoutant les modifications de réseau internet
	protected SharedPreferences fichierSharedPreferences;						
	protected SharedPreferences.Editor editorFichierSharedPreferences;
	/*
	 * Variable de classe pour gérer les différentes fonctionnalitées du service:
	 * - connexion au serveur
	 * - internet activé
	 * - travail en cours 
	 * - ...
	 */
	protected boolean internetActive = false;
	protected boolean estLieAuServeur = false; //Connecté réellement au serveur

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		/*
		 * Inscription du service pour écouter toutes les modifications réalisées sur le réseau.
		 */
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

		if(connexionReceiver == null){
			connexionReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					ConnectivityManager cm = (ConnectivityManager) context
							.getSystemService(Context.CONNECTIVITY_SERVICE);
					NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
					boolean isConnected = activeNetwork != null
							&& activeNetwork.isConnectedOrConnecting();

					for(IServeurConnexion obj : clientConnexion)
						obj.internetConnexion(isConnected);
					//Cette condition vérifie si le réseau est lancé pas si 
					//internet est fonctionnel. Exemple dans un point relai 
					//d'hotel ou on doit s'enregistrer avant d'avoir internet
					if (isConnected) {
						internetActive = true;
						autoLoggin();
					} else {
						internetActive = false;
						estLieAuServeur = false;
					}
				}
			};
			registerReceiver(connexionReceiver, filter);
		}
	}

	protected abstract void autoLoggin();

	public boolean internetActif(){
		return internetActive;
	}

	public boolean estLieAuServeur(){
		return estLieAuServeur;
	}

	public void deconnectUtilisateur(){
		this.estLieAuServeur = false;
	}

	/**
	 * Méthode permettant de savoir si il y a un utilisateur en mémoire et si il est en connection automatique. 
	 * Si oui il est considérer comme connecté tous le temps (principe de transparence) sinon le bouton de connexion lui permet de se connecter
	 * @return
	 */
	public boolean utilisateurEnregistre(){
		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, Context.MODE_PRIVATE);
		String idUser = fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null);
		boolean autoLoggin = fichierSharedPreferences.getInt(Constantes.CLEF_CHOIX_CONNEXION, -1) == Constantes.CLEF_AUTO_LOGGIN;
		
		if(estLieAuServeur){
			return true;
		}else if(idUser != null && autoLoggin)
			return true;
		
		return false;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		try{
			unregisterReceiver(connexionReceiver);
		}catch(IllegalArgumentException e){}
	}

	/**
	 * Méthode pour lié les interfaces de communications entre activités et le service
	 */
	public void inscrireConnection(IServeurConnexion conn){
		if(!clientConnexion.contains(conn))
			clientConnexion.add(conn);
	}

	public void desinscrireConnection(IServeurConnexion conn){
		clientConnexion.remove(conn);
	}

	public void inscrireMessage(IMessage conn){
		if(!clientMessage.contains(conn))
			clientMessage.add(conn);
	}

	public void desinscrireMessage(IMessage conn){
		clientMessage.remove(conn);
	}
}
