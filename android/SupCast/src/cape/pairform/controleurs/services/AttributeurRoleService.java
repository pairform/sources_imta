package cape.pairform.controleurs.services;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Service permettant d'attribuer un rôle à un utilisateur dans une ressource
 */
public class AttributeurRoleService extends IntentService {

	public AttributeurRoleService() {
		super(AttributeurRoleService.class.getName());
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {		
		String idRessource = intent.getStringExtra(Constantes.CLEF_ID_RESSOURCE);
		String idUtilisateurCible = intent.getStringExtra(Constantes.CLEF_ID_UTILISATEUR);
		String idRoleTemp = intent.getStringExtra(Constantes.CLEF_ID_ROLE_UTILISATEUR);
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_RESSOURCE, idRessource) );
       		parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_UTILISATEUR_CIBLE, idUtilisateurCible) );
       		parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_ROLE_TEMP, idRoleTemp) );
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
            	
        // Envoie d'une requete HTTP attribuant un rôle
    	Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_CHANGER_ROLE, parametresRequete, this);
	}
}
