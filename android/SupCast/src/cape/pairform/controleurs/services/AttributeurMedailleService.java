package cape.pairform.controleurs.services;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Service permettant d'attribuer ou de supprimer une médaille à un message
 */
public class AttributeurMedailleService extends IntentService {

	public AttributeurMedailleService() {
		super(AttributeurMedailleService.class.getName());
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {		
		String idMessage = intent.getStringExtra(Constantes.CLEF_ID_MESSAGE);
		String medaille = intent.getStringExtra(Constantes.CLEF_MEDAILLE_MESSAGE);
		
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE, idMessage) );
        	// si on supprime une médaille
        	if ( !medaille.equals("") ) {
        		parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_MEDAILLE_MESSAGE, medaille) );
        	}
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
            	
        // Envoie d'une requete HTTP attribuant une médaille à un message
    	Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_DONNER_MEDAILLE_MESSAGE, parametresRequete, this);
	}
}
