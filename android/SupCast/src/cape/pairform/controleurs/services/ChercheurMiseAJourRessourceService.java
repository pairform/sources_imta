package cape.pairform.controleurs.services;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Service recherchant la disponibilité d'une mise à jour pour une ressource
 */
public class ChercheurMiseAJourRessourceService extends IntentService {

	private SharedPreferences fichierSharedPreferences;							// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;			// permet d'éditer les fichiers des préférences
	private String idRessource;													// id de la ressource courante
	private String stringJSON;													// contenu d'une réponse JSON d'une requête http
	private String dateMiseAJour = null;										// date de la dernière mise à jour de la ressource (récupèré par le web service)
	
	
	public ChercheurMiseAJourRessourceService() {
		super(ChercheurMiseAJourRessourceService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {/*
		idRessource = intent.getStringExtra( Constantes.CLEF_ID_RESSOURCE );
    	
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_RESSOURCE, idRessource) );		// id de la ressource pour laquelle on recherche la disponibilité d'une mise à jour
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
        
        // Envoie d'une requete HTTP recherchant la disponibilité d'une mise à jour  pour une ressource
        stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECHERCHER_MISE_A_JOUR, parametresRequete);
        
        // on parse le JSON
        if (stringJSON != null) {
        	dateMiseAJour = ParserJSON.parserRetourRechercheMiseAJourRessource(stringJSON);
        } 	
    	fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_RESSOURCES_TELECHARGEES, 0);
    	
    	// si la date de la dernière mise à jour est différentes de celle stocké dans le fichier de préférences
    	if ( !fichierSharedPreferences.getString(idRessource + Constantes.CLEF_DATE_EDITION, "").equals(dateMiseAJour) ) {
    		// on actualise la date de mise à jour
    		editorFichierSharedPreferences = fichierSharedPreferences.edit();
    		editorFichierSharedPreferences.putString(idRessource + Constantes.CLEF_DATE_EDITION, dateMiseAJour);
    		editorFichierSharedPreferences.commit();
    		
    		// on stocke l'id et l'url où télécharger la ressource mis à jour dans un fichier de préférences
    		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_LISTE_RESSOURCES_MISES_A_JOUR, 0);
    		editorFichierSharedPreferences = fichierSharedPreferences.edit();
    		editorFichierSharedPreferences.putString(
    			idRessource, 
    			getSharedPreferences(Constantes.FICHIER_INFOS_RESSOURCES_TELECHARGEES, 0).getString(idRessource + Constantes.CLEF_URL_RESSOURCE, "")
    		);
    		editorFichierSharedPreferences.commit();    		
    	}*/
	}
}
