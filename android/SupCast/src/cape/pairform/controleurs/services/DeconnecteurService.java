package cape.pairform.controleurs.services;

import android.app.IntentService;
import android.content.Intent;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Service permettant de déconnecter l'utilisateur
 */
public class DeconnecteurService extends IntentService {
	
	public DeconnecteurService() {
		super(DeconnecteurService.class.getName());
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {            	
        // Envoie d'une requete HTTP permettant de choisir déconnecter l'utilisateur
        Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_DECONNEXION, null, this);
	}
}
