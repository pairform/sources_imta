package cape.pairform.controleurs.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Message;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.PieceJointeInformation;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.PiecesJointesDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Service permettant d'envoyer tous les messages non synchronisé avec le serveur. 
 * Le service renvoi une Intent pour indiquer qu'il a fini l'envoi des nouveaux messages
 */
public class EnvoieMessagesService extends IntentService {

	private ArrayList<NameValuePair> parametresRequete;			// paramétres d'une requête Post
	private String stringJSON = null;							// contenu d'une réponse JSON d'une requête http
	private HashMap<String, String> tableauConversionID;		// Tableau de conversion contenant les anciens id mappé avec les nouveaux
	private String idUser;										// ID de l'utilisateur actuel
	private MessageDAO messageDAO;
	private PiecesJointesDAO pjsDAO;

	public EnvoieMessagesService() {
		super(EnvoieMessagesService.class.getName());
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
		messageDAO = BDDFactory.getMessageDAO(getApplicationContext());
		pjsDAO = BDDFactory.getPieceJointeDAO(getApplicationContext());
		tableauConversionID = new HashMap<String, String>();
		SharedPreferences fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, Context.MODE_PRIVATE);
		idUser = fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null);
		ArrayList<Message> messagesAEnvoyer = messageDAO.getMessagesAsynchrone();

		//Pour chaque message
		for(int i = 0; i < messagesAEnvoyer.size(); i++){
			Message message = messagesAEnvoyer.get(i);

			//Si l'utilisateur actuel est celui qui l'a écrit
			if(!message.getAuteur().getId().equals(idUser))
				continue;

			//Si on a un id de message parent négtif
			if(Long.valueOf(message.getIdParent()) < 0){
				//Ca veut dire qu'on a répondu à un message qui n'est pas envoyé
				//Donc il DOIT etre dans le tableau de conversion.
				if(tableauConversionID.containsKey(message.getIdParent())){
					message.setIdParent(tableauConversionID.get(message.getIdParent()));
				}else{
					//Si il ne l'est pas c'est qu'il y a une erreur ...
					//Donc on passe à la suivante
					continue;
				}
			}

			// création et configuration des paramètres d'une requête http
			messageAParametre(message);

			//Recupe les pjs
			//Fais les thumbnails et les noms de thumbnails
			//envoie la requete
			ArrayList<PieceJointe> pjServeur = null;

			if(message.getPiecesJointes().size() > 0){
				pjServeur = new ArrayList<PieceJointe>();   

				//Pour toutes les pièces jointes
				for(int j = 0; j < message.getPiecesJointes().size(); j++){
					//On va créer une thumbnail pour chaque pj
					
					PieceJointe pj = message.getPiecesJointes().get(j);
					String nom = "pj-" + j + "." + pj.getExtension().toLowerCase(Locale.FRANCE).trim();
					String nomThumbnail = "pj-" + j + Constantes.CLEF_THUMBNAIL_NOM_FICHIER + pj.getExtension().toLowerCase(Locale.FRANCE).trim();

					pj.setNomOriginal(nom);
					PieceJointe thumbnail = new PieceJointe(nomThumbnail);
					thumbnail.setExtension(pj.getExtension());
					thumbnail.setFile(pj.getFile());
					thumbnail.setTaille(pj.getTaille());
					thumbnail.setCheminVersThumbnail(pj.getCheminVersThumbnail());

					//Et on les ajoutes au pj a envoyer
					pjServeur.add(thumbnail);
					pjServeur.add(pj);
				}

				stringJSON = Utiles.envoyerMessagePiecesJointes(this, Constantes.URL_WEB_SERVICE_ENREGISTRER_MESSAGE, parametresRequete, pjServeur, PieceJointeInformation.getInstance(getApplicationContext()).getThumbnailTaille());
			} else {
				// Envoie d'une requete HTTP enregistrant un message
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_ENREGISTRER_MESSAGE, parametresRequete, this);
			}

			// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas messagesJSON
			if ( stringJSON != null ) {
				HashMap<String, String> retour = ParserJSON.parserRetourMessage(stringJSON, message.getPiecesJointes());

				if(retour.containsValue(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR) || retour.get(Constantes.CLEF_ID_MESSAGE) == null){
					continue;
				}else{

					String idMessage = retour.get(Constantes.CLEF_ID_MESSAGE);

					//On l'ajoute au tab de conversion au cas ou on envoie un message dont ce message soit le parent pour avoir le vrai id du serveur
					tableauConversionID.put(message.getId(), idMessage);

					//On a de nouvelle données donc on change les anciennes
					messageDAO.updateMessageIDAndParentID(message.getId(), idMessage, message.getIdParent());


					if(message.getPiecesJointes().size() > 0){
						//Cas special, le nom serveur etant une partie de la clé on doit supprimer et re-insérer la pj
						pjsDAO.deletePieceJointeAvecMessageID(idMessage);

						//Pour toutes les pj recup on les ajoutes dans la bdd
						for(PieceJointe pj : message.getPiecesJointes()){
							pjsDAO.insertPieceJointe(pj);
						}
					}
				}
			} else
				continue;
		}

		messageDAO.close();
		pjsDAO.close();

		Intent intentRetour = new Intent( Constantes.ACTION_INTENT_ENVOYER_MESSAGES );
		intentRetour.putExtra(Constantes.CLEF_INTENT_ENVOYER_MESSAGES, stringJSON);
		sendBroadcast(intentRetour);  		
	}

	//Méthode insérant dans la requete les données du message
	private void messageAParametre(Message message){
		try {
			parametresRequete = new ArrayList<NameValuePair>();
			parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CAPSULE, message.getIdCapsule()) );							// id de la capsule sur laquelle est publié le message
			parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_CONTENU_MESSAGE, message.getContenu()) );							// texte du message publié 
			parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_LANGUE, message.getIdLangue()) );								// langue du message publié
			parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_DATE_CREATION, String.valueOf(message.getDateCreation())) );
						
			// si le nouveau message est publié sur une page
			if ( message.getNomPage() != null && !message.getNomPage().trim().isEmpty()) {													// getNomPage() vaut null si le message est publié sur la capsule
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_NOM_PAGE_MESSAGE, message.getNomPage() ));					// nom de la page sur laquelle est publié le message
			} else {
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_NOM_PAGE_MESSAGE, Constantes.CLEF_NOM_PAGE_MESSAGE_DEFAUT ));
			}
			// si le nouveau message est publié sur un grain / OA
			if ( message.getNomTagPage() != null ) {
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_NOM_TAG_PAGE_MESSAGE, message.getNomTagPage()) );										// nom de la balise sur laquelle est publié le message
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_NUM_OCCURENCE_TAG_PAGE_MESSAGE, String.valueOf(message.getNumOccurenceTagPage())) );	// occurence dans la page de la balise sur laquelle est publié le message
			}

			// si le nouveau message est une réponse, id du message parent
			if ( !message.getIdParent().equals("0") ) { 
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE_PARENT, message.getIdParent()) );	
			}
			// si le nouveau message est un défi
			boolean estDefi = message.getDefi() == 1;
			if (estDefi) {
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_DEFI_MESSAGE, String.valueOf(estDefi)) );
			}
			// si des tags sont associés au nouveau message
			if ( !message.getListeTags().isEmpty() ) {
				JSONArray jsonListeTags = new JSONArray(message.getNomTag());
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_TAGS_MESSAGE, jsonListeTags.toString()) );
			}

			//On ajoute la position GPS récupéré
			if(message.getGeoLatitude() != 0 && message.getGeoLongitude() != 0){
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_GEO_LATITUDE, String.valueOf(message.getGeoLatitude())) );
				parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_GEO_LONGITUDE, String.valueOf(message.getGeoLongitude())) );
			}
			// si des cercles sont associés au nouveau message (visibilité) - les réponses n'ont pas de visibilité car celle-ci est identique au message père
			if ( message.getVisibilite() != null ) {
	    		parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_VISIBILITE_MESSAGE, message.getVisibilite()) );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
