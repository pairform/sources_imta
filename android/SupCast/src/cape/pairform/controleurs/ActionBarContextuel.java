package cape.pairform.controleurs;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ActionMode.Callback;


/*
 * actionbar contextuele remplacant l'actionbar
 */
public class ActionBarContextuel implements Callback {

		// appelé quand actionMode est créé ; startActionMode() a été appelé
	    @Override
	    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {	        
	        return true;
	    }
	    
	    // appelé chaque fois que action mode est affiché (et aussi aprés onCreateActionMode)
	    @Override
	    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
	        return false;
	    }
	    
	    @Override
	    public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
	    	return false;
	    }

	    @Override
	    public void onDestroyActionMode(ActionMode mode) {
	    }
	}