package cape.pairform.controleurs.activities;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import cape.pairform.R;
import cape.pairform.controleurs.adapters.ListeUtilisateursAdapter;
import cape.pairform.controleurs.dialogs.ConnexionDialog;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.modele.Cercle;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Utilisateur;
import cape.pairform.modele.bdd.ReferencesBDD;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Ecran présentant la liste des cercles de l'utilisateur (et classe, s'il est expert)
 */
public class EcranCerclesActivity extends ActivityMere implements ReferencesBDD {

	private Activity activityParent;
	private ProgressDialog progressDialog;						// fenêtre de chargement
	private SharedPreferences fichierSharedPreferences;			// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;		// permet d'éditer les fichiers des préférences	
	private Intent intentCourante;								// intent courante
	private String stringJSON;									// contenu d'une réponse JSON d'une requête http
	private AlertDialog.Builder dialogBuilder;					// fenêtre courante
	private ConnexionDialog connexionDialog;					// fenêtre de connexion
	private RelativeLayout layoutDialogCreerCercle;				// layout de la fenêtre de création d'un nouveau cercle
	private LinearLayout layoutDialogRejoindreClasse;			// layout de la fenêtre permettant de rejoindre une classe
	private String erreurRetourJSON;							// erreur retourné par un web service
	private TextView membreView;								// view représentant un utilisateur membre d'un cercle
	private RelativeLayout cercleLayout;						// layout représentant un cercle (composé d'un nom, d'une liste de membres, etc.)
	private int idlayoutlisteCercles;							// ressource id de l'id du latout d'une liste de cercles (cercles, classes créées ou classes rejointes)
	private ArrayList<Cercle> listeCercles;						// liste des cercles et des classes de l'utilisateur
	private BitmapDrawable avatarCourant;						// avatar courant sous forme de drawable
	private Bitmap avatarBitmap;								// avatar courant sous forme de bitmap
	private ListeUtilisateursAdapter adapterListeUtilisateurs;	// adapter personnalisé associé à une liste d'utilisateur
	private ArrayList<Utilisateur> listeUtilisateurs = null;	// liste des utilisateurs de l'application
	private boolean utilisateurEstExpert = false;				// vaut true si l'utilisateur connecté est expert (ou plus) dans une ressource

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.title_cercles);
		setContentView(R.layout.layout_ecran_cercles);
		activityParent = this;

		// création d'une fenêtre de chargement
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage( getString(R.string.label_chargement) );
		progressDialog.show();

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(this, Constantes.CLEF_ECRAN_CERCLES, Constantes.AIDE_ECRAN_CERCLES);

		// on récupère le fichier de préfèrences stockant les informations sur l'utilisateur connecté
		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);
		editorFichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0).edit();

		// récupère les cercles d'un utilisateur (via le web service associé)
		recupererCercles();

		// on vérifie si l'utilisateur connecté est expert (ou plus) dans une ressource
		for ( String idRessourceCourante : fichierSharedPreferences.getAll().keySet() ) {
			try {
				// idRessourceCourante doit contenir  l'id d'une ressource utilisé par l'utilisateur
				// si le contenu de "idRessourceCourante" est un élément du fichier de préférence autres qu'un rôle, une exception est déclanchée, la boucle passe ensuite à l'itération suivante
				Integer.parseInt( idRessourceCourante );
				// si l'utilisateur est un expert ou mieux (ex: un admin)
				if ( Integer.parseInt( fichierSharedPreferences.getString(idRessourceCourante, null) ) >= Constantes.ID_EXPERT ) {
					utilisateurEstExpert = true;
				}
			} catch (Exception e) {}
		}

		// si l'utilisateur connecté n'est pas au moins expert dans une ressource, on masque la liste des classes créées
		if ( !utilisateurEstExpert ) {
			((TextView) findViewById(R.id.titre_liste_classes)).setVisibility( View.GONE );
			((LinearLayout) findViewById(R.id.liste_classes)).setVisibility( View.GONE );
		}

		// configuration du bouton de création d'un cercle
		((Button) findViewById(R.id.creer_cercle)).setText(getString(R.string.button_creer_un_cercle) + "...");
		((Button) findViewById(R.id.creer_cercle)).setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				// ouverture d'une fenêtre permettant de créer un cercle (ou une classe)
				layoutDialogCreerCercle = (RelativeLayout) getLayoutInflater().inflate(R.layout.layout_dialog_creation_cercle, null);

				TextView textClasse = (TextView) layoutDialogCreerCercle.findViewById(R.id.classe);
				textClasse.setText(textClasse.getText() + " :");

				// si l'utilisateur connecté n'est pas au moins expert dans une ressource, on masque la liste des classes
				if ( !utilisateurEstExpert ) {
					((Switch) layoutDialogCreerCercle.findViewById(R.id.button_classe)).setVisibility( View.GONE );
					textClasse.setVisibility( View.GONE );
				}

				dialogBuilder = new AlertDialog.Builder( activityParent );
				dialogBuilder.setTitle(R.string.title_creer_cercle)
					.setView(layoutDialogCreerCercle)
					.setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					})
					.setPositiveButton(R.string.button_ajouter, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							if ( ((Switch) layoutDialogCreerCercle.findViewById(R.id.button_classe)).isChecked() ) {
								// ajout d'une classe
								creerCercle(Constantes.URL_WEB_SERVICE_CREER_CLASSE, ((EditText) layoutDialogCreerCercle.findViewById(R.id.nom_cercle)).getText().toString() );
							} else {
								// ajout d'un cercle
								creerCercle(Constantes.URL_WEB_SERVICE_CREER_CERCLE, ((EditText) layoutDialogCreerCercle.findViewById(R.id.nom_cercle)).getText().toString());
							}
						}
					})
					.show();
			}
		});

		// configuration du bouton permettant de rejoindre une classe
		((Button) findViewById(R.id.rejoindre_classe)).setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				// ouverture d'une fenêtre permettant de rejoindre une classe
				layoutDialogRejoindreClasse = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_dialog_rejoindre_classe, null);

				dialogBuilder = new AlertDialog.Builder( activityParent );
				dialogBuilder.setTitle(R.string.title_rejoindre_classe)
					.setView(layoutDialogRejoindreClasse)
					.setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					})
					.setPositiveButton(R.string.button_rejoindre, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							rejoindreClasse( ((EditText) layoutDialogRejoindreClasse.findViewById(R.id.clef_classe)).getText().toString() );
						}
					})
					.show();
			}
		});
	}


	// récupère les cercles d'un utilisateur (via le web service associé)
	private void recupererCercles() {
		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {	    		
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(											// id de l'utilisateur
						Constantes.CLEF_ID_UTILISATEUR,
						fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null)
					));
				} catch (Exception e) {
					e.printStackTrace();
				}
				// Envoie d'une requete HTTP récupérant les cercles et classes de l'utilisateur
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AFFICHER_CERCLES, parametresRequete, activityParent);
				// création de la liste des avatars qui sera a téléchargé
				ArrayList<Utilisateur> listeMembresSansAvatar = new ArrayList<Utilisateur>();

				// si la requete http s'est mal exécuté, stringJSON vaut null
				if ( stringJSON != null ) {
					listeCercles = ParserJSON.parserCercles(stringJSON);

					// pour chaque cercle
					for(Cercle cercleCourant : listeCercles) {
						// pour chaque utilisateur membre du cercle
						for ( Utilisateur membreCourant : cercleCourant.getListeMembresCercle() ) {
							// si l'avatar n'a pas déjà été téléchargé
							if ( !getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0).contains( membreCourant.getUrlAvatar() ) ) {	
								listeMembresSansAvatar.add( membreCourant );
							}  else {
								membreCourant.setUrlAvatar( getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + membreCourant.getId() );
							}
						}
					}
				}

				runOnUiThread( new Runnable() {
					public void run() {
						// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
						progressDialog.dismiss();
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON != null ) {
							for(Cercle cercleCourant : listeCercles) {
								// on affiche les cercles et classes
								afficherCercle(cercleCourant);
							}
						} else {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
						}	
					}
				});

				// on télécharge les avatars des utilisateurs
				for ( Utilisateur membreCourant : listeMembresSansAvatar ) {
					// si l'avatar n'a pas déjà été téléchargé
					if ( !getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0).contains( membreCourant.getUrlAvatar() ) ) {	
						telechargerAvatar( membreCourant );
					}
				}
			}
		});

		thread.start();
	}


	// affiche les cercles et les classes de l'utilisateur
	private void afficherCercle(Cercle cercleCourant) {
		cercleLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.layout_cercle, null);
		cercleLayout.setTag( cercleCourant.getIdCercle() );														// on stock l'id du cercle dans le cercleLayout
		cercleLayout.findViewById(R.id.supprimer_cercle).setTag( String.valueOf(cercleCourant.isClasse()) );	// on stock dans le bouton supprimer le fait que le cercle soit une classe ou non
		cercleLayout.findViewById(R.id.nom_cercle).setTag( String.valueOf(cercleCourant.isCreateur()) );		// on stock dans le textview le fait que le cercle ait été créé par l'utilisateur ou non

		// si l'utilisateur n'est pas le createur du cercle (c'est une classe rejointe)
		if ( !cercleCourant.isCreateur() ) {
			// on masque les boutons de gestion du cercle
			cercleLayout.findViewById(R.id.supprimer_cercle).setVisibility(View.INVISIBLE);
			cercleLayout.findViewById(R.id.ajouter_membre_cercle).setVisibility(View.INVISIBLE);
		} else {
			// gestion du clic sur le bouton de suppression du cercle
			((ImageView) cercleLayout.findViewById(R.id.supprimer_cercle)).setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View boutonSupprimerCercle) {
					// suppression du cercle
					supprimerCercle(
						(String) ((RelativeLayout) boutonSupprimerCercle.getParent()).getTag(),		// id du cercle à supprimer
						(RelativeLayout) boutonSupprimerCercle.getParent(),							// cercleLayout à supprimer
						Boolean.parseBoolean( (String) boutonSupprimerCercle.getTag() ),			// vaut true si le cercle est une classe false sinon
						Boolean.parseBoolean(														// vaut true si le cercle a été créé par l'utilisateur
							(String) (((RelativeLayout) boutonSupprimerCercle.getParent()).findViewById(R.id.nom_cercle)).getTag()
						)
					);
				}
			});

			// gestion du clic sur le bouton d'ajout d'un membre à un cercle
			((ImageView) cercleLayout.findViewById(R.id.ajouter_membre_cercle)).setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View boutonAjouterMembre) {
					// on ouvre une fenetre contenant la liste utilisateurs
					// si la liste d'utilisateur n'existe pas
					if (listeUtilisateurs == null) {
						// récupération de tous les utilisateurs
						recupererListeUtilisateur(
							(String) ((RelativeLayout) boutonAjouterMembre.getParent()).getTag(),		// id du cercle où ajouter un membre
							(RelativeLayout) boutonAjouterMembre.getParent()							// cercleLayout où ajouter un membre
						);					
					} else {
						// on affiche la liste des utilisateurs dans une fenetre
						afficherListeUtilisateur(
							(String) ((RelativeLayout) boutonAjouterMembre.getParent()).getTag(),		// id du cercle où ajouter un membre
							(RelativeLayout) boutonAjouterMembre.getParent()							// cercleLayout où ajouter un membre
						);
					}
				}
			});
		}

		// pour chaque utilisateur membre du cercle
		for ( Utilisateur membreCourant : cercleCourant.getListeMembresCercle() ) {
			// création et configuration de la view correspondant à l'utilisateur
			membreView = (TextView) getLayoutInflater().inflate(R.layout.element_liste_membre_cercle, null);
			membreView.setText( membreCourant.getPseudo() );
			membreView.setTag( membreCourant.getId() );					// on stock l'id de l'utilisateur dans la vue

			try {
				avatarBitmap = BitmapFactory.decodeFile( getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + membreCourant.getId() );

				// si l'avatar a été téléchargé, on récupère le CompoundDrawable sous forme de BitmapDrawable
				if (avatarBitmap != null ) {
					avatarCourant = new BitmapDrawable(
						getResources(),
						avatarBitmap
					);			
					membreView.setCompoundDrawablesWithIntrinsicBounds(null, avatarCourant, null, null);
				}
			} catch(Exception e) {}

			// si l'utilisateur est le createur du cercle (c'est une classe ou un cercle créé)
			if ( cercleCourant.isCreateur() ) {
				// gestion du clic long sur le membre du cercle
				membreView.setOnLongClickListener( new OnLongClickListener() {
					@Override
					public boolean onLongClick(View membreCercleView) {
						// le téléphone vibre pour signaler la prise en compte du clic long
						((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(Constantes.DUREE_VIBRATION_CLIC_LONG);

						// on supprime le membre du cercle
						supprimerMembreCercle(
							(LinearLayout) membreCercleView.getParent(),												// layout de la liste des membre du cercle courant
							membreCercleView,																			// view du membre à supprimer
							(String) ((RelativeLayout) membreCercleView.getParent().getParent().getParent()).getTag()	// id du cercle où supprimer un membre
						);
						return false;
					}
				});
			}
			// ajout de l'utilisateur dans la liste des membres du cercle
			((LinearLayout) cercleLayout.findViewById(R.id.liste_membres_cercle)).addView(membreView);
		}

		// si le cercle est une classe
		if ( cercleCourant.isClasse() ) {
			// le nom de la classe est suivi de la clef permettant à un utilisateur de s'ajouter à la liste des membres
			((TextView) cercleLayout.findViewById(R.id.nom_cercle)).setText(
				cercleCourant.getNomCercle() + " - " + getResources().getText(R.string.label_clef) + " : " + cercleCourant.getClefClasse()
			);
			// si l'utilisateur a créé la classe
			if ( cercleCourant.isCreateur() ) {
				idlayoutlisteCercles = R.id.liste_classes;
			} else {
				idlayoutlisteCercles = R.id.liste_classes_rejointes;
			}
		} else {
			// le cercle n'est pas une classe
			((TextView) cercleLayout.findViewById(R.id.nom_cercle)).setText( cercleCourant.getNomCercle() );
			idlayoutlisteCercles = R.id.liste_cercles;
		}

		// s'il la liste des cercles/classes/classes rejointes est vides (un TextView affichant un message d'infos est alors présent dans la liste)
		if ( ((LinearLayout) findViewById(idlayoutlisteCercles)).getChildCount() == 1 ) {
			// on supprime le TextView
			((LinearLayout) findViewById(idlayoutlisteCercles)).getChildAt(0).setVisibility( View.GONE );
		}

		((LinearLayout) findViewById(idlayoutlisteCercles)).addView(cercleLayout);
	}


	// récupère la liste des utilisateur de l'application
	private void recupererListeUtilisateur(final String idCercleCourant, final RelativeLayout cercleLayoutCourant) {		
		// création d'une fenêtre de chargement
		progressDialog = new ProgressDialog( activityParent );
		progressDialog.setMessage( getString(R.string.label_chargement) );
		progressDialog.show();

		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {
				// Envoie d'une requete HTTP récupérant les infos sur les ressources pouvant être téléchargé
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECUPERER_UTILISATEURS, null, activityParent);
				// création de la liste des avatars qui sera a téléchargé
				ArrayList<Utilisateur> listeUtilisateursSansAvatar = new ArrayList<Utilisateur>();

				// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas utilisateursJSON
				JSONObject utilisateursJSON = null;
				if ( stringJSON != null ) {
					// transformation de la réponse en objet JSON
					try {
						utilisateursJSON = new JSONObject(stringJSON);
					} catch (Exception e) {
						e.printStackTrace();
					}

					// on parse la liste des utilisateurs
					listeUtilisateurs = ParserJSON.parserUtilisateurs(utilisateursJSON);

					// pour chaque utilisateur
					for ( Utilisateur utilisateurCourant : listeUtilisateurs ) {
						// si l'avatar n'a pas déjà été téléchargé
						if ( !getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0).contains( utilisateurCourant.getUrlAvatar() ) ) {	
							listeUtilisateursSansAvatar.add( utilisateurCourant );
						} else {
							utilisateurCourant.setUrlAvatar( getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + utilisateurCourant.getId() );
						}
					}
				}

				runOnUiThread(new Runnable() {
					public void run() {
						// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
						progressDialog.dismiss();
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON != null ) {
							// on affiche la liste des utilisateurs dans une fenetre
							afficherListeUtilisateur(idCercleCourant, cercleLayoutCourant);
						} else {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
						}
					}
				});

				// on télécharge les avatars des utilisateurs
				for ( Utilisateur utilisateurCourant : listeUtilisateursSansAvatar ) {
					// si l'avatar n'a pas déjà été téléchargé
					if ( !getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0).contains( utilisateurCourant.getUrlAvatar() ) ) {	
						telechargerAvatar( utilisateurCourant );
					}
				}
			}
		});

		thread.start();
	}


	// affiche dans une fenêtre la liste des utilisateurs de l'application
	private void afficherListeUtilisateur(final String idCercleCourant, final RelativeLayout cercleLayoutCourant) {		
		adapterListeUtilisateurs = new ListeUtilisateursAdapter(
			activityParent,
			R.layout.element_liste_utilisateurs,
			listeUtilisateurs
		);

		// création de la fenêtre contenant la liste des utilisateur
		dialogBuilder = new AlertDialog.Builder( activityParent );
		dialogBuilder.setTitle(R.string.title_liste_utilisateurs)
		.setAdapter(adapterListeUtilisateurs, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				// ajout d'un membre dans un cercle
				ajouterMembreCercle(
					idCercleCourant,
					cercleLayoutCourant,
					listeUtilisateurs.get( itemSelectionne )		// nouvelle utilisateur ajouté au cercle
				);
			}
		})
		.setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		})
		.show();
	}


	// ajoute un membre dans un cercle
	private void ajouterMembreCercle(final String IdCercleCourant, final RelativeLayout cercleLayoutCourant, final Utilisateur nouveauMembreCercle) {
		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {	    		
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CERCLE, IdCercleCourant));								// id du cercle
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_UTILISATEUR_CONCERNE, nouveauMembreCercle.getId()));	// id de l'utilisateur ajouté au cercle
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP permettant l'ajout d'un membre dans un crecle
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AJOUTER_MEMBRE_CERCLE, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null
				if ( stringJSON != null ) {
					erreurRetourJSON = ParserJSON.parserRetourAjoutMembreCercle(stringJSON);
				}	        	

				runOnUiThread(new Runnable() {
					public void run() {	    				
						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);

						} else if (erreurRetourJSON != null) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
						} else {
							// création et configuration de la view correspondant au nouveau membre
							membreView = (TextView) getLayoutInflater().inflate(R.layout.element_liste_membre_cercle, null);
							membreView.setText( nouveauMembreCercle.getPseudo() );
							membreView.setTag( nouveauMembreCercle.getId() );			// on stock l'id de l'utilisateur dans la vue

							try {
								avatarBitmap = BitmapFactory.decodeFile( getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + nouveauMembreCercle.getId() );

								// si l'avatar a été téléchargé, on récupère le CompoundDrawable sous forme de BitmapDrawable
								if (avatarBitmap != null ) {
									avatarCourant = new BitmapDrawable(
										getResources(),
										avatarBitmap
									);			
									membreView.setCompoundDrawablesWithIntrinsicBounds(null, avatarCourant, null, null);
								}
							} catch(Exception e) {}

							// gestion du clic long sur le membre du cercle
							membreView.setOnLongClickListener( new OnLongClickListener() {
								@Override
								public boolean onLongClick(View membreCercleView) {
									// le téléphone vibre pour signaler la prise en compte du clic long
									((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(Constantes.DUREE_VIBRATION_CLIC_LONG);

									// on supprime le membre du cercle
									supprimerMembreCercle(
										(LinearLayout) membreCercleView.getParent(),												// layout de la liste des membre du cercle courant
										membreCercleView,																			// view du membre à supprimer
										(String) ((RelativeLayout) membreCercleView.getParent().getParent().getParent()).getTag()	// id du cercle où supprimer un membre
									);
									return false;
								}
							});

							// ajout de l'utilisateur dans la liste des membres du cercle
							((LinearLayout) cercleLayoutCourant.findViewById(R.id.liste_membres_cercle)).addView(membreView);
						}
						// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
						progressDialog.dismiss();
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);	
					}
				});
			}
		});

		thread.start();
	}


	// supprime un membre d'un cercle
	private void supprimerMembreCercle(final LinearLayout listeMembresLayoutCourant, final View membreCercleView, final String IdCercleCourant) {
		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {	    		
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CERCLE, IdCercleCourant));										// id du cercle où supprimer un membre
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_UTILISATEUR_CONCERNE, (String) membreCercleView.getTag()));	// id du membre à supprimer
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP supprimant un membre d'un cercle
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_SUPPRIMER_MEMBRE_CERCLE, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null
				if ( stringJSON != null ) {
					erreurRetourJSON = ParserJSON.parserRetourSuppressionMembreCercle(stringJSON);
				}	        	

				runOnUiThread(new Runnable() {
					public void run() {
						// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
						progressDialog.dismiss();
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);

						} else if (erreurRetourJSON != null) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);

						} else {
							listeMembresLayoutCourant.removeView(membreCercleView);
						}	
					}
				});
			}
		});

		thread.start();
	}


	// créé un cercle
	// ajoute le cercle ou la classe
	private void creerCercle(final String[] urlWebService, final String nomNouveauCercle) {
		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {	    		
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_NOM, nomNouveauCercle));	// nom du nouveau cercle
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP créant un cercle ou une classe
				stringJSON = Utiles.envoyerRequeteHttp( urlWebService, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null
				if ( stringJSON != null ) {
					ParserJSON.parserRetourCreationCercle(stringJSON);
				}	        	

				runOnUiThread(new Runnable() {
					public void run() {	    				
						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);

						} else {
							// suppression de l'affichage des cercles (on ne supprime pas le premier élement contenant un TextView d'infos)
							((LinearLayout) findViewById(R.id.liste_cercles)).removeViews( 1, ((LinearLayout) findViewById(R.id.liste_cercles)).getChildCount() - 1 );
							((LinearLayout) findViewById(R.id.liste_classes)).removeViews( 1, ((LinearLayout) findViewById(R.id.liste_classes)).getChildCount() - 1 );
							((LinearLayout) findViewById(R.id.liste_classes_rejointes)).removeViews( 1, ((LinearLayout) findViewById(R.id.liste_classes_rejointes)).getChildCount() - 1 );
							// récupère les cercles d'un utilisateur (via le web service associé) et les affiche
							recupererCercles();
						}
					}
				});
			}
		});

		thread.start();
	}



	// rejoins une classe créé par un autre utilisateur
	private void rejoindreClasse(final String clefClasse) {
		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {	    		
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_CLEF_CLASSE, clefClasse));	// clef de la classe
					parametresRequete.add( new BasicNameValuePair(												// id de l'utilisateur connecté
						Constantes.CLEF_ID_UTILISATEUR,
						fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null)
					));
					parametresRequete.add( new BasicNameValuePair(											// pseudo de l'utilisateur connecté
						Constantes.CLEF_PSEUDO_UTILISATEUR,
						fichierSharedPreferences.getString(Constantes.CLEF_PSEUDO_UTILISATEUR, null)
					));
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP permettant à l'utilisateur de rejoindre une classe
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_REJOINDRE_CLASSE, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null
				if ( stringJSON != null ) {
					erreurRetourJSON = ParserJSON.parserRetourRejoindreClasse(stringJSON);
				}

				runOnUiThread(new Runnable() {
					public void run() {
						// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
						progressDialog.dismiss();
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
						} else if (erreurRetourJSON != null) {
							if( erreurRetourJSON.equals(Constantes.CLEF_CLE_INVALIDE) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_cle_invalide, R.string.label_erreur_saisie_cle_invalide, activityParent);

							} else if( erreurRetourJSON.equals(Constantes.CLEF_UTILISATEUR_DEJA_MEMBRE) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_utilisateur_deja_membre, R.string.label_erreur_utilisateur_connecte_deja_membre, activityParent);

							} else {
								Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
							}
						}  else {
							// suppression de l'affichage des cercles (on ne supprime pas le premier élement contenant un TextView d'infos)
							((LinearLayout) findViewById(R.id.liste_cercles)).removeViews( 1, ((LinearLayout) findViewById(R.id.liste_cercles)).getChildCount() - 1 );
							((LinearLayout) findViewById(R.id.liste_classes)).removeViews( 1, ((LinearLayout) findViewById(R.id.liste_classes)).getChildCount() - 1 );
							((LinearLayout) findViewById(R.id.liste_classes_rejointes)).removeViews( 1, ((LinearLayout) findViewById(R.id.liste_classes_rejointes)).getChildCount() - 1 );
							// récupère les cercles d'un utilisateur (via le web service associé) et les affiche
							recupererCercles();
						}
					}
				});
			}
		});

		thread.start();
	}



	// supprime un cercle
	private void supprimerCercle(final String idCercleASupprimer, final RelativeLayout cercleLayoutCourant, final boolean isClasse, final boolean isCreateur) {
		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {	    		
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CERCLE, idCercleASupprimer));	// id du cercle à supprimer
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP supprimant un cercle
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_SUPPRIMER_CERCLE, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null
				if ( stringJSON != null ) {
					erreurRetourJSON = ParserJSON.parserRetourSuppressionCercle(stringJSON);
				}	        	

				runOnUiThread(new Runnable() {
					public void run() {
						// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
						progressDialog.dismiss();
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);

						} else if (erreurRetourJSON != null) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);

						} else {
							if ( isClasse && isCreateur) {						// si le cercle est une classe créée
								idlayoutlisteCercles = R.id.liste_classes;
							} else if( isClasse && !isCreateur ) {				// si le cercle est une classe rejointe
								idlayoutlisteCercles = R.id.liste_classes_rejointes;
							} else {											// si le cercle n'est pas une classe
								idlayoutlisteCercles = R.id.liste_cercles;
							}
							// suppression du cercle
							((LinearLayout) findViewById(idlayoutlisteCercles)).removeView( cercleLayoutCourant );
							// s'il n'y a plus de classes dasn la liste, on affiche un message d'infos
							if ( ((LinearLayout) findViewById(idlayoutlisteCercles)).getChildCount() == 1 ) {
								((LinearLayout) findViewById(idlayoutlisteCercles)).getChildAt(0).setVisibility( View.VISIBLE );
							} 
						}	
					}
				});
			}
		});

		thread.start();
	}


	// télécharge l'avatar d'un utilisateur
	// télécharge un avatar
	private void telechargerAvatar(Utilisateur utilisateur) {
		Utiles.telechargerFichier(
			utilisateur.getUrlAvatar(),
			getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + utilisateur.getId()
		);
		// ajout de l'avatar à la liste des avatars téléchargés
		editorFichierSharedPreferences.putString( utilisateur.getUrlAvatar(), utilisateur.getPseudo() );
		editorFichierSharedPreferences.commit();
	}

	// création du menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_ecran_cercles, menu);
		return true;
	}

	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.button_sliding_menu:	// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
			slidingMenu.toggle();
			return true;
		case R.id.button_profil:
			// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
			if ( fichierSharedPreferences.contains(Constantes.CLEF_ID_UTILISATEUR) ) {
				// test la disponibilité de l'accès Internet
				if ( !Utiles.reseauDisponible(this) ) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, this);
				} else { // On peut utiliser la connection Internet
					// lancement de l'écran de profil
					intentCourante = new Intent(this, EcranProfilActivity.class);
					intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null));
					startActivity(intentCourante);
				}

			} else {
				// ouverture de la fenêtre de connexion
				connexionDialog = new ConnexionDialog(this, service);
				connexionDialog.setOnConnectedListener( new OnConnectedListener() {
					@Override
					public void onConnected(boolean success) {
						// lancement de l'écran de profil
						if(success){
							intentCourante = new Intent(activityParent, EcranProfilActivity.class);
							intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null));
							startActivity(intentCourante);
						}
					}			    		
				});
				connexionDialog.show();
				//TODO: vous devez etre connecté pour voir votre profil
			}
			break;
		}
		return false;
	}

	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
