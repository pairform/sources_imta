package cape.pairform.controleurs.activities;

import java.util.Locale;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.Spinner;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Ecran permettant de modifier les préférences de l'application
 */
public class EcranPreferencesActivity extends ActivityMere {

	private static final String LOG_TAG = Utiles.class.getSimpleName();
	
	private Context contextCourant;								// context courant
	private SharedPreferences fichierSharedPreferences;			// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;	// permet d'éditer les fichiers des préférences
	private Spinner langueSpinnerPreferences;					// view représentant la liste des langues
	private String[] tabCodesLangues;							// Tableau de String contenant les codes des langues
	private String langueActuel;								// Langue actuel de l'utilisateur
	private Locale locale;										// Gére le language de l'application
	private Configuration config;								// Gere la configuration de l'application
	private Intent intent;										// Intent pour lancer une Activité
	private CheckedTextView geolocaliserMessageCheckedTextView; // checkbox pour représenter l'activation de la geolocalisation des messages

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.title_preferences);
        setContentView(R.layout.layout_ecran_preferences);
        contextCourant = this;
        
        // modification du titre
        this.setTitle(R.string.title_preferences);
        
        // on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
        Utiles.determinerAideContextuelle(this, null, Constantes.AIDE_INEXISTANTE);
        
        // on récupère le fichier de préfèrences stockant les informations sur utilisateur connecté
        fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, 0);
        editorFichierSharedPreferences = fichierSharedPreferences.edit();
        
        // Initialisation de la liste des langues de l'application
        langueSpinnerPreferences = (Spinner) findViewById(R.id.langue_spinner_preferences);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.langue_true_values_string, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		langueSpinnerPreferences.setAdapter(adapter);
		
		// Selection de la langue de l'application (Anglais par défaut)
		int indexLangue = Utiles.tabIndexLangueAvecCode(fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_APPLICATION, "en"), this);
		langueSpinnerPreferences.setSelection(indexLangue);
		
		// Action lors de la selection d'une langue
		langueSpinnerPreferences.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		        // on recupere la langue selectionnée
	        	langueActuel = fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_APPLICATION, "en");
	        	tabCodesLangues = getResources().getStringArray(R.array.langue_values_code);
	        	
	        	Log.i(LOG_TAG, "langue : " + langueActuel);
	        	
	        	if(!tabCodesLangues[position].equals(langueActuel)) {	        		
	        		// on met à jour la langue de l'utilisateur dans le fichier des préférences
	        		editorFichierSharedPreferences.putString(Constantes.CLEF_LANGUE_APPLICATION, tabCodesLangues[position]);
	        		editorFichierSharedPreferences.commit();
	        		
	        		// on modifie la langue de l'application
	        		locale = new Locale(tabCodesLangues[position]); 
	        		Locale.setDefault(locale);
	        		config = new Configuration();
	        		config.locale = locale;
	        		getApplicationContext().getResources().updateConfiguration(config, null);
	        		
	        		Toast.makeText(contextCourant, R.string.label_modification_langue_application_reussi, Toast.LENGTH_LONG).show();
	        		
	        		// on relance l'activité en vidant la pile et en y mettant l'ecran d'accueil
	        		intent = new Intent(EcranPreferencesActivity.this, EcranTableauDeBordActivity.class);
	        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
	        	    startActivity(intent);
	        	}
		        
		    }
		    public void onNothingSelected(AdapterView<?> parentView) {}
		});
	
		//Initialisation de la geolocalisation
 		geolocaliserMessageCheckedTextView = (CheckedTextView)findViewById(R.id.geolocalisation_checktextview_preferences);
 		//Par défault la geolocalisation est desactivé COMME sur IOS
 		geolocaliserMessageCheckedTextView.setChecked(fichierSharedPreferences.getBoolean(Constantes.CLEF_GEOLOCALISATION_MESSAGE, false));
 		
		//Action lors de la selection de la CheckedTextView de geolocalisation
 		//Le composant CheckedTextView est fait pour les listes donc pas cochable hors liste par défault.
 		//Mais c'est le seul à intégrer JOLIEMENT du texte à gauche de la checkbox
 		//Donc je redéfinie son comportement 
 		geolocaliserMessageCheckedTextView.setOnClickListener(new View.OnClickListener() {
 	        public void onClick(View v)
 	        {
 	        	//Simulation du comportement normale d'une checkbox
 	        	((CheckedTextView) v).toggle();
 	        	//on enregistre le choix utilisateur
 	        	editorFichierSharedPreferences.putBoolean(Constantes.CLEF_GEOLOCALISATION_MESSAGE, geolocaliserMessageCheckedTextView.isChecked());
 	        	editorFichierSharedPreferences.commit();
 	        }
 		});
	}
	
	
	// création du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ecran_defaut, menu);
        return true;
    }
    
    
    // gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		switch (item.getItemId()) {
 			case R.id.button_sliding_menu:	
 				// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
 				slidingMenu.toggle();
 				return true;
 		}
 		return false;
 	}
    
 	
 	// gestion du changement d'orientation
 	@Override
  	public void onConfigurationChanged(Configuration newConfig) {
 		super.onConfigurationChanged(newConfig);
 	}
}
