package cape.pairform.controleurs.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import cape.pairform.R;
import cape.pairform.controleurs.fragments.LectureMessagesTransversaleFragment;
import cape.pairform.controleurs.fragments.LectureMessagesTransversaleFragment.LectureMessagesTransversaleFragmentListener;
import cape.pairform.modele.Constantes;

/*
 * Ecran affichant la liste des messages de toutes les ressources (avec possibilité de les filtrer)
 */
public class EcranVueTransversaleActivity extends ActivityMere implements LectureMessagesTransversaleFragmentListener {
	
	private Bundle bundle;														// bundle contenant les informations à transmettre au fragment
	private Fragment lectureMessagesTransversaleFragment;						// Fragment associé à la liste des messages de la ressource ou d'une page de la ressource ou une sous partie de la page
	private FragmentTransaction fragmentTransaction;							// class permettant de réaliser une transition entre 2 fragments
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.title_vue_transversale);

        // création d'un Bundle contenant les infos avec lesquelles filtrer les messages ; s'il n'y a pas d'infos, il n'y a pas de filtrage et les valeur du bundle sont null
        bundle = new Bundle();
        bundle.putString(
        	Constantes.CLEF_ID_UTILISATEUR,
        	getIntent().getStringExtra(Constantes.CLEF_ID_UTILISATEUR)
        );
        bundle.putString(
        	Constantes.CLEF_TAG_MESSAGE,
        	getIntent().getStringExtra(Constantes.CLEF_TAG_MESSAGE)
        );
        
        // création d'un fragment représentant l'arborescence des chapitres la ressource
        lectureMessagesTransversaleFragment = Fragment.instantiate(this, LectureMessagesTransversaleFragment.class.getName(), bundle);
		// lancement d'une transition pour afficher arborescenceRessourceFragment
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.add( android.R.id.content, lectureMessagesTransversaleFragment );
		fragmentTransaction.attach(lectureMessagesTransversaleFragment);
		fragmentTransaction.commit();
	}
    
    @Override
    public void modifierElementConnexionSlidingMenu() {
    	// modifie l'élément connexion du slidingMenu après une connexion
    	slidingMenu.modifierElementConnexion();
    }
    
	// création du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ecran_vue_transversal, menu);
        return true;
    }
    
    // gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		switch (item.getItemId()) {
 			case R.id.button_sliding_menu:	// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
 				slidingMenu.toggle();
 				return true;
 		}
 		return false;
 	}
 	
 	// gestion du changement d'orientation
 	@Override
   	public void onConfigurationChanged(Configuration newConfig) {
  		super.onConfigurationChanged(newConfig);
  	}
}
