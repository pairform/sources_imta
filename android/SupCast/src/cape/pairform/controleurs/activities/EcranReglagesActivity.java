package cape.pairform.controleurs.activities;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.dialogs.ConnexionDialog;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.controleurs.services.ActivateurEmailNotificationService;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.Ressource;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.RessourceDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Ecran permettant de modifier les informations du compte de l'utilisateur connecté
 */
public class EcranReglagesActivity extends ActivityMere {

	private Intent intentCourante;								// intent courante
	private Context contextCourant;								// context courant
	private RessourceDAO ressourceDAO;							// permet l'accès à la table Ressource
	private ProgressDialog progressDialog;						// fenêtre de chargementt
	private SharedPreferences fichierSharedPreferences;			// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;	// permet d'éditer les fichiers des préférences
	private ConnexionDialog connexionDialog;					// fenêtre de connexion
	private ImageView avatarView;								// view représentant l'avatar de l'utilisateur
	private Switch notificationEmailView;						// view représentant le switch permettant d'activer / désactiver la notification d'email
	private Spinner langueSpinnerProfil;						// view représentant la liste des langues
	private LinearLayout emailNotificationLayout;				// layout contenant les switch d'activation / désactivation de l'email de notification de chaque capsule
	private RelativeLayout capsuleLayout;						// layout représentant une capsule : contient le switch d'activation / désactivation de l'email de notification pour la capsule
	private String stringJSON;									// contenu d'une réponse JSON d'une requête http
	private String erreursModificationCompte = null;			// erreur retourné par le web service de modification du compte
	private int idLanguePrincipaleSelectionnee;					// l'id de la langue principale selectionnee
	private String codeLangueSelectionnee;						// code de la langue selectionnee
	private boolean[] itemsSelectionnes;						// Les items selectionnés
	private ArrayList<String> listeNomsAutresLangues;
	private String codeLangueAppActuelle;						// Code de la langue de l'application actuelle
	private Locale locale;										// Gére le language de l'application
	private Configuration config;								// Gére la configuration de l'application
	private InputStream inputStream;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.title_modification_profil);
		setContentView(R.layout.layout_ecran_reglages);
		contextCourant = this;

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(this, null, Constantes.AIDE_INEXISTANTE);

		// on récupère le fichier de préfèrences stockant les informations sur utilisateur connecté
		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);
		editorFichierSharedPreferences = fichierSharedPreferences.edit();

		// Modification des textes
		((TextView) findViewById(R.id.label_langue_principale)).setText( this.getText(R.string.label_langue_principale) + " : " );
		((TextView) findViewById(R.id.label_autres_langues)).setText( this.getText(R.string.label_autres_langues) + " : " );
		((Button) findViewById(R.id.button_choix_autres_langues)).setText( this.getText(R.string.button_choisir) + "..." );

		// gestion des champs textes (email, établissement, mot de passe, etc.)
		((EditText) findViewById(R.id.etablissement_utilisateur)).setText( fichierSharedPreferences.getString(Constantes.CLEF_ETABLISSEMENT_UTILISATEUR, "") );

		// Action du bouton pour enregistrer les modifications
		((Button) findViewById(R.id.button_modifier)).setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View view) {
				// test la disponibilité de l'accès Internet
				if ( !Utiles.reseauDisponible(contextCourant) ) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, contextCourant);
				} else { // On peut utiliser la connection Internet
					editerCompte();	
				}
			}        	
		});

		// gestion de la liste des langues
		langueSpinnerProfil = (Spinner) findViewById(R.id.langue_spinner_profil);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.langue_true_values_string, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		langueSpinnerProfil.setAdapter(adapter);

		// Permet de selectionner la langue principale de l'utilisateur (anglais par défaut)
		codeLangueSelectionnee = fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_PRINCIPALE, "en");
		int indexLanguePrincipale = Utiles.tabIndexLangueAvecCode(codeLangueSelectionnee, this);
		langueSpinnerProfil.setSelection(indexLanguePrincipale, false);
		idLanguePrincipaleSelectionnee = Utiles.idLangueAvecTabIndex(indexLanguePrincipale, this);

		// Action lors de la selection d'une langue
		langueSpinnerProfil.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				idLanguePrincipaleSelectionnee = Utiles.idLangueAvecTabIndex(position, contextCourant);
				codeLangueSelectionnee = Utiles.codeLangueAvecId(String.valueOf(idLanguePrincipaleSelectionnee), contextCourant);

				// Mise à jour des langues de l'utilisateurs dans le fichier de préférences associés
				editorFichierSharedPreferences.putString(Constantes.CLEF_LANGUE_PRINCIPALE, codeLangueSelectionnee);
				editorFichierSharedPreferences.putBoolean(Constantes.CLEF_AUTRES_LANGUES + "_" + codeLangueSelectionnee, false);
				editorFichierSharedPreferences.commit();

				modifierLanguesUtilisateur();
				afficherDialogChangementLangueApp();
			}
			public void onNothingSelected(AdapterView<?> parentView) {}
		});

		// Action du bouton pour modifier les autres langues de l'utilisateur
		((Button) findViewById(R.id.button_choix_autres_langues)).setOnClickListener( new OnClickListener() {		 	
			@Override
			public void onClick(View view) {				
				// Liste des noms des langues
				listeNomsAutresLangues = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.langue_true_values_string)));
				listeNomsAutresLangues.remove(Utiles.nomVraiLangueAvecCode(fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_PRINCIPALE, ""), contextCourant));

				itemsSelectionnes = new boolean[listeNomsAutresLangues.size()];

				for(int i=0; i<listeNomsAutresLangues.size(); i++) {
					boolean lg = fichierSharedPreferences.getBoolean(Constantes.CLEF_AUTRES_LANGUES + "_" + Utiles.codeLangueAvecVraiNom(listeNomsAutresLangues.get(i), contextCourant) , false);
					itemsSelectionnes[i] = lg;
				}

				AlertDialog.Builder builder = new AlertDialog.Builder(contextCourant);

				builder.setTitle(getString(R.string.title_autres_langues) + " :")
				.setMultiChoiceItems(listeNomsAutresLangues.toArray(new CharSequence[listeNomsAutresLangues.size()]), itemsSelectionnes, new DialogInterface.OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						if (isChecked) {
							itemsSelectionnes[which] = true;
						} else {
							itemsSelectionnes[which] = false;
						}
					}
				})
				.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

						for(int i=0;i<itemsSelectionnes.length;i++) {
							editorFichierSharedPreferences.putBoolean(Constantes.CLEF_AUTRES_LANGUES + "_" + Utiles.codeLangueAvecVraiNom(listeNomsAutresLangues.get(i), contextCourant), itemsSelectionnes[i]);
						}
						editorFichierSharedPreferences.commit();
						modifierLanguesUtilisateur();
					}
				})
				.setNegativeButton(getString(R.string.button_annuler), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				})
				.show();
			}
		});

		// gestion de l'avatar 
		avatarView = (ImageView) findViewById(R.id.avatar_utilisateur);        
		avatarView.setImageURI( 
				Uri.parse( getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, "") )
				);
		avatarView.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View view) {
				editerAvatar();
			}        	
		});

		emailNotificationLayout = (LinearLayout) findViewById(R.id.layout_activer_email_notification);

		ressourceDAO = BDDFactory.getRessourceDAO(this.getApplicationContext());

		// pour chaque capsule téléchargée
		for (Ressource ressourceCourante : ressourceDAO.getAllRessources()) {
			for (final Capsule capsuleCourante : ressourceCourante.getListeCapsules()) {
				capsuleLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.capsule_ecran_reglages, null);

				// ajout du nom de la capsule courante et du logo de la ressource associée
				((TextView) capsuleLayout.findViewById(R.id.description_capsule)).setText( capsuleCourante.getNomCourt() );
				((TextView) capsuleLayout.findViewById(R.id.description_capsule)).setCompoundDrawablesWithIntrinsicBounds(
						new BitmapDrawable(
								getResources(),BitmapFactory.decodeFile( ressourceCourante.getUrlLogo() )
								),
						null,
						null,
						null
						);

				// gestion du bouton permettant d'activer / désactiver l'email de notification pour la capsule courante
				notificationEmailView = (Switch) capsuleLayout.findViewById(R.id.button_activer_email_notification);
				notificationEmailView.setChecked( fichierSharedPreferences.getBoolean(Constantes.CLEF_NOTIFICATION_EMAIL + "_" + capsuleCourante.getId(), false) );
				notificationEmailView.setOnCheckedChangeListener( new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						intentCourante = new Intent(contextCourant, ActivateurEmailNotificationService.class);
						intentCourante.putExtra(Constantes.CLEF_NOTIFICATION_EMAIL, isChecked);
						intentCourante.putExtra(Constantes.CLEF_ID_CAPSULE, capsuleCourante.getId());
						startService(intentCourante);
					}
				});

				emailNotificationLayout.addView(capsuleLayout);
			}
		}
		ressourceDAO.close();
	}


	// Affiche une fenetre permettant d'appliquer la nouvelle langue de l'utilisateur sur l'interface de l'application 
	private void afficherDialogChangementLangueApp() {		
		// on sauvegarde la langue de l'application actuelle
		codeLangueAppActuelle = getString(R.string.locale_language);  
		// on modifie la langue de l'application pour pouvoir afficher la boite ci dessous dans la langue sélectionnée
		editerLangueApp(codeLangueSelectionnee);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// configuration et ouverture de la fenêtre
		builder.setTitle(getString(R.string.title_langue_application))
		.setMessage(R.string.label_changer_langue_app)
		.setPositiveButton(R.string.button_oui, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				// on met à jour la langue de l'utilisateur dans le fichier des préférences
				editorFichierSharedPreferences.putString(Constantes.CLEF_LANGUE_APPLICATION, codeLangueSelectionnee);
				editorFichierSharedPreferences.commit();

				Toast.makeText(contextCourant, R.string.label_modification_langue_application_reussi, Toast.LENGTH_LONG).show();

				// on relance l'activité en vidant la pile et en y mettant l'ecran d'accueil
				intentCourante = new Intent(contextCourant, EcranTableauDeBordActivity.class);
				intentCourante.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intentCourante);

				intentCourante = new Intent(EcranReglagesActivity.this, EcranReglagesActivity.class);
				startActivity(intentCourante);
			}
		})
		.setNegativeButton(R.string.button_non, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				editerLangueApp(codeLangueAppActuelle);
			}
		})
		.show();


	}


	// edite la langue de l'interface de l'application
	private void editerLangueApp(String codeLangue) {
		// on modifie la langue de l'application
		locale = new Locale(codeLangue); 
		Locale.setDefault(locale);
		config = new Configuration();
		config.locale = locale;
		getApplicationContext().getResources().updateConfiguration(config, null);
	}


	// Modifie les langues de l'utilisateur
	private void modifierLanguesUtilisateur() {		
		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(											// id de la langue principale de l'utilisateur
						Constantes.CLEF_LANGUE_PRINCIPALE,
						String.valueOf(Utiles.idLanguePrincipale(contextCourant))
					));
					parametresRequete.add( new BasicNameValuePair(											// liste des autres langues de l'utilisateur
						Constantes.CLEF_AUTRES_LANGUES,
						(new JSONArray(Utiles.listeIdAutresLangues(contextCourant))).toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// id de l'utilisateur
						Constantes.CLEF_ID_UTILISATEUR, 
						fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null)
					));
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP pour modifier le compte utilisateur 
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_EDITER_LANGUES, parametresRequete, contextCourant);
			}
		});

		thread.start();
	}


	// modifie les informations de compte de l'utilisateur connecté
	private void editerCompte() {
		// création d'une fenêtre de chargement
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage( getString(R.string.label_chargement) );
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(											// établissement de l'utilisateur
							Constantes.CLEF_ETABLISSEMENT_UTILISATEUR, 
							((EditText) findViewById(R.id.etablissement_utilisateur)).getText().toString()
							));
					parametresRequete.add( new BasicNameValuePair(											// ancien password encoder en base64
							Constantes.CLEF_ANCIEN_MOT_DE_PASSE_UTILISATEUR, 
							((EditText) findViewById(R.id.ancien_mot_de_passe)).getText().toString()
							));
					parametresRequete.add( new BasicNameValuePair(											// nouveau password encoder en base64
							Constantes.CLEF_MOT_DE_PASSE_UTILISATEUR, 
							((EditText) findViewById(R.id.nouveau_mot_de_passe)).getText().toString()
							));
					parametresRequete.add( new BasicNameValuePair(											// confirmation nouveau password encoder en base64
							Constantes.CLEF_CONFIRMATION_MOT_DE_PASSE_UTILISATEUR, 
							((EditText) findViewById(R.id.confirmation_nouveau_mot_de_passe)).getText().toString()
							));
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP pour modifier le compte utilisateur 
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_EDITER_COMPTE, parametresRequete, contextCourant);

				// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas stringJSON
				if ( stringJSON != null ) {
					erreursModificationCompte = ParserJSON.parserRetourModificationCompte( stringJSON );
				}

				runOnUiThread(new Runnable() {
					public void run() {
						progressDialog.dismiss();	    				

						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, contextCourant);

						} else if ( erreursModificationCompte != null ) {	    	        			    	        		
							if ( erreursModificationCompte.equals(Constantes.CLEF_CONFIRMATION_MOT_DE_PASSE_UTILISATEUR) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_saisie, R.string.label_erreur_saisie_mot_de_passe_confirmation, contextCourant);

							} else if ( erreursModificationCompte.equals(Constantes.CLEF_MOT_DE_PASSE_UTILISATEUR) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_saisie, R.string.label_erreur_saisie_mot_de_passe, contextCourant);

							} else if ( erreursModificationCompte.equals(Constantes.CLEF_ANCIEN_MOT_DE_PASSE_UTILISATEUR) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_saisie, R.string.label_erreur_saisie_mot_de_passe_ancien, contextCourant);

							} else {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_saisie, R.string.title_erreur_saisie, contextCourant);

							}
						} else {
							Toast.makeText(contextCourant, R.string.label_modification_de_compte_reussi, Toast.LENGTH_LONG).show();

							editorFichierSharedPreferences.putString(
									Constantes.CLEF_ETABLISSEMENT_UTILISATEUR,
									((EditText) findViewById(R.id.etablissement_utilisateur)).getText().toString()
									);
							editorFichierSharedPreferences.commit();
						}
					}
				});	        	
			}
		});

		thread.start();
	}


	// modifie l'avatar de l'utilisateur connecté
	private void editerAvatar() {
		intentCourante = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		// on vérifie qu'il existe des applications qui puissent recevoir l'intent
		if (intentCourante.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intentCourante, Constantes.CLEF_INTENT_MODIFIER_AVATAR);
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, final Intent datas) {
		super.onActivityResult(requestCode, resultCode, datas);

		// si l'appel à onActivityResult correspond à la modification de l'avatar
		if (requestCode == Constantes.CLEF_INTENT_MODIFIER_AVATAR && resultCode == Activity.RESULT_OK) {
			// création d'une fenêtre de chargement
			progressDialog = new ProgressDialog(this);
			progressDialog.setMessage( getString(R.string.label_chargement) );
			progressDialog.show();

			// création d'un nouveau Thread
			Thread thread = new Thread(new Runnable() {
				public void run() {	    		
					// création et configuration des paramètres d'une requête http
					ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

					try {
						inputStream = getContentResolver().openInputStream( datas.getData() );
						ByteArrayOutputStream baos = new ByteArrayOutputStream();

						try { 
							byte[] buffer = new byte[1024];  
							int n;  
							while (-1 != (n = inputStream.read(buffer)))  
								baos.write(buffer, 0, n);  
						} catch (Exception e) {  
							e.printStackTrace();  
						}

						parametresRequete.add( new BasicNameValuePair(					// nouvelle avatar de l'utilisateur
							Constantes.CLEF_AVATAR_UTILISATEUR, 
							Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)
						));
					} catch (Exception e) {
						e.printStackTrace();        	
					}
					// Envoie d'une requete HTTP pour modifier le compte utilisateur 
					stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_EDITER_AVATAR, parametresRequete, contextCourant);

					// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas stringJSON
					if ( stringJSON != null ) {
						erreursModificationCompte = ParserJSON.parserRetourModificationAvatarCompte( stringJSON );
					}

					runOnUiThread(new Runnable() {
						public void run() {
							progressDialog.dismiss();	    				

							// si la requete http s'est mal exécuté, stringJSON vaut null
							if ( stringJSON == null ) {
								Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, contextCourant);

							} else if (erreursModificationCompte != null) {
								if ( erreursModificationCompte.equals(Constantes.CLEF_TAILLE_AVATAR_UTILISATEUR) ) {
									Utiles.afficherInfoAlertDialog(R.string.title_erreur_taille_avatar, R.string.label_erreur_taille_avatar, contextCourant);		    						
								} else {
									Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, contextCourant);		    						
								}

							} else {
								try {
									inputStream = getContentResolver().openInputStream( datas.getData() );
									avatarView.setImageBitmap( BitmapFactory.decodeStream(inputStream) );

									Toast.makeText(contextCourant, R.string.label_avatar_mis_a_jour, Toast.LENGTH_LONG).show();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					});	        	
				}
			});

			thread.start();
		}
	}


	// création du menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_ecran_reglages, menu);
		return true;
	}


	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.button_sliding_menu:	// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
			slidingMenu.toggle();
			return true;
		case R.id.button_profil:
			// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
			if ( fichierSharedPreferences.contains(Constantes.CLEF_ID_UTILISATEUR) ) {
				// test la disponibilité de l'accès Internet
				if ( !Utiles.reseauDisponible(this) ) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, this);
				} else { // On peut utiliser la connection Internet
					// lancement de l'écran de profil
					intentCourante = new Intent(this, EcranProfilActivity.class);
					intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null));
					startActivity(intentCourante);
				}

			} else {
				// ouverture de la fenêtre de connexion
				connexionDialog = new ConnexionDialog(this, service);
				connexionDialog.setOnConnectedListener( new OnConnectedListener() {
					@Override
					public void onConnected(boolean success) {
						// lancement de l'écran de profil
						if(success){
							intentCourante = new Intent(contextCourant, EcranProfilActivity.class);
							intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null));
							startActivity(intentCourante);
						}
					}			    		
				});
				connexionDialog.show();
				//TODO: vous devez etre connecté pour voir votre profil
			}
			break;
		}
		return false;
	}


	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
