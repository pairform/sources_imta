package cape.pairform.controleurs.activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.SlidingMenuSupCast;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.controleurs.services.communication.IMessage;
import cape.pairform.controleurs.services.communication.IServeurConnexion;
import cape.pairform.controleurs.services.communication.LiaisonActivityAService;
import cape.pairform.utiles.Utiles;

public abstract class ActivityMere extends FragmentActivity implements IServeurConnexion, IMessage{

	protected SlidingMenuSupCast slidingMenu;							// menu déroulant
	public static ServiceGeneral service;
	private LiaisonActivityAService liaisonServiveActivity = LiaisonActivityAService.getInstance();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		// création du menu déroulant
		slidingMenu = new SlidingMenuSupCast(ActivityMere.this);

		if(service == null) {		
			liaisonServiveActivity.addConnection(this , new ServiceConnection() {
				public void onServiceConnected(ComponentName className, IBinder s) {
					service = ((ServiceGeneral.ServiceGeneralBinder)s).getService();

					service.inscrireConnection(ActivityMere.this);
					service.inscrireMessage(ActivityMere.this);
					
					slidingMenu.showMenu(service);

					//Méthode appelé et customizé dans les classes filles
					onServiceBound(service);
				}

				public void onServiceDisconnected(ComponentName className) {
					service = null;
				}
			});
		} else {
			slidingMenu.showMenu(service);
			onServiceBound(service);
		}
	}

	@Override
	protected void onResume() {
		((ApplicationController)getApplicationContext()).setActivityCourante(this);
		super.onResume();
		if(service == null)
			lieAuService(this);
		else {
			service.inscrireConnection(this);
			service.inscrireMessage(this);
			if(service.estLieAuServeur()) {
				slidingMenu.modifierElementConnexion();
			} else {
				slidingMenu.modifierElementDeconnexion();
			}
		}
	}

	@Override
	protected void onPause() {
		if(service != null) {
			service.desinscrireConnection(ActivityMere.this);
			service.desinscrireMessage(ActivityMere.this);
		}
		super.onPause();
		/*if(service != null)
			delieAuService(this);*/
	}

	@Override
	protected void onDestroy() {
		//liaisonServiveActivity.removeConnection(this);
		super.onDestroy();
	}

	/**
	 * Méthode appelé dès que le service est lié à l'activity
	 * @param service
	 */
	protected void onServiceBound(ServiceGeneral service) {

	};

	protected void lieAuService(Context c) {
		liaisonServiveActivity.bindActiviteService(this);
	}

	protected void delieAuService(Context c){
		service.desinscrireConnection(ActivityMere.this);
		service.desinscrireMessage(ActivityMere.this);
		liaisonServiveActivity.unbindActiviteService(this);
	}

	@Override
	public void enregistrement(int success) {
		Activity activiteCourante = ((ApplicationController)getApplicationContext()).getActivityCourante();

		boolean resultat = false;
		if(success == -1)
			Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activiteCourante);
		else if(success == -2)
			Utiles.afficherInfoAlertDialog(R.string.title_erreur_identifiants, R.string.label_erreur_saisie_utilisateur_inexistant,  activiteCourante);
		else if(success == 0)
			resultat = true;

		if(resultat){
			slidingMenu.modifierElementConnexion();
			Toast.makeText(getApplicationContext(), R.string.label_connexion_reussi, Toast.LENGTH_LONG).show();
		}else{
			slidingMenu.modifierElementDeconnexion();
		}
	}

	@Override
	public void internetConnexion(boolean activate) {
		/*
		 * Mettre action qu'on veut répété par chaque activité lors de la récupération du reseau
		 * Appeler le code dans la méthode même nom overrider dans la classe fille avec  : 
		 * super.internetConnexion(activate);
		 */
	}

	@Override
	public void messageMiseAJour(boolean reussite) {
		//Exemple de travail possible dans cette méthode dans les activité : mise à jours des messages non lu		
	}
}
