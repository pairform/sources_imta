package cape.pairform.controleurs.activities;

import java.lang.ref.WeakReference;
import java.util.Locale;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import cape.pairform.R;
import cape.pairform.controleurs.dialogs.CreationCompteDialog;
import cape.pairform.controleurs.fragments.GrilleRessourcesFragment;
import cape.pairform.controleurs.fragments.GrilleRessourcesFragment.GrilleRessourcesFragmentListener;
import cape.pairform.controleurs.fragments.RessourceFragment;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.modele.Constantes;


/*
 *       ()_()
 *       (o.o)        @
 *      /('"')\     @@ @@
 *--------"-"------@  @  @
 *-----------------@@   @
 *                @  @@
 * 
 * 
 * Ecran d'accueil sur lequel arrive l'utilisateur au lancement de l'application
 */
public class EcranTableauDeBordActivity extends ActivityMere implements GrilleRessourcesFragmentListener {

	private SharedPreferences fichierSharedPreferences;				// permet l'accès aux fichiers des préférences
	private String languePrefApplication;							// chaine contenant la langue de l'application des préférences
	private String langueApplication;								// chaine contenant la langue de l'application
	private Bundle bundle;											// Bundle contenant les données (à transmettre) entre l'activity et ses fragments
	private FragmentTransaction fragmentTransaction;				// class permettant de réaliser une transition entre 2 fragments
	private Fragment grilleRessourcesFragment = null;				// Fragment associé au à la grille des ressources téléchargées
	private Fragment ressourceFragment = null;						// Fragment associé à une ressource
	//Référence faible vers cette activité pour savoir quand on commit les fragment en asynchrone, si l'activité est tjr ok
	//Ceci est du au lancement du fragment quand le service est lié
	//Ceci étant la première activity c'est obligatoire car le service se lance
	//pour toutes les autres le problème ne se présente que si le service est tué puis relancé !!
	private static WeakReference<EcranTableauDeBordActivity> wrActivity = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		wrActivity = new WeakReference<EcranTableauDeBordActivity>(this);

		// on récupère le fichier de préfèrences stockant les paramètres de l'application
		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, 0);

		// Actualisation de la langue de l'application
		languePrefApplication = fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_APPLICATION, null);
		if(languePrefApplication != null) {			
			langueApplication = getResources().getString(R.string.locale_language);
			
			if(!langueApplication.equals(languePrefApplication)) {				
				// on modifie la langue de l'application
				Locale locale = new Locale(languePrefApplication); 
				Locale.setDefault(locale);
				Configuration config = new Configuration();
				config.locale = locale;
				getApplicationContext().getResources().updateConfiguration(config, null);
			}
		}

		// Verification si changement de langue depuis création de compte
		if(getIntent().getBooleanExtra("createAccount", false)) {
			// Affichage du formulaire de création de compte
			CreationCompteDialog creationCompteAlertDialog = new CreationCompteDialog(this, getIntent().getStringArrayExtra("sauvegardeChamps"));    		
			AlertDialog creationCompteDialog = creationCompteAlertDialog.create();

			// On transmet le AlertDialog au AlertDialog.Buidler pour pouvoir fermer la fenetre en dehors d'un clic sur un bouton
			creationCompteAlertDialog.setDialog(creationCompteDialog);
			creationCompteDialog.show();
		}		

		// mise à jour du titre de l'écran, il n'est pas défini dans le manifest pour éviter un changement de nom de l'application
		getActionBar().setTitle(R.string.title_grille_ressources);
		
		//Le constructeur parent doit être appelé en dernier.
		//C'est celui de la classe abstraite mere qui lance la fonction onServiceBound qui permet de savoir de manière asynchrone quand le service est lié
		super.onCreate(savedInstanceState);
	}
	

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	

	// méthode de l'interface GrilleRessourcesFragmentListener (class interne de GrilleRessourcesFragment.java)
	// déclanché quand l'utilisateur sélectionne une ressource dans la liste des ressources téléchargées
	@Override
	public void onSelectedRessource(String idRessource) {
		// création d'un Bundle contenant l'id de la ressource que l'utilisateur consulte
		bundle = new Bundle();
		bundle.putString(
			Constantes.CLEF_ID_RESSOURCE,
			idRessource
		);

		// création d'un fragment représentant une ressource
		ressourceFragment = Fragment.instantiate(this, RessourceFragment.class.getName(), bundle );

		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.replace( android.R.id.content, ressourceFragment );
		fragmentTransaction.commit();
	}
	

	// création du menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_ecran_tableau_de_bord, menu);
		return true;
	}
	

	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.button_sliding_menu:	
			// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
			slidingMenu.toggle();
			return true;
		}
		return false;
	}
	

	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	

	@Override
	protected void onServiceBound(ServiceGeneral service) {
		super.onServiceBound(service);
		if ((wrActivity.get() != null) && (wrActivity.get().isFinishing() != true)) {
			// création d'un fragment représentant la liste des ressources téléchargées
			grilleRessourcesFragment = Fragment.instantiate(this, GrilleRessourcesFragment.class.getName());
			fragmentTransaction = wrActivity.get().getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace( android.R.id.content, grilleRessourcesFragment );
			fragmentTransaction.commit();
		}
	}
}
