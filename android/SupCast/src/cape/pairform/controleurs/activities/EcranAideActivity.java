package cape.pairform.controleurs.activities;

import cape.pairform.R;
import cape.pairform.controleurs.adapters.ListeAidesPagerAdapter;
import cape.pairform.modele.Constantes;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.Toast;


/*
 * Ecran affichant une aide en fonction de l'ecran ou se trouve l'utilisateur
 */
public class EcranAideActivity extends Activity {

	/* Images des aides contextuelles */
	private static final int[] LISTE_IMAGES_AIDES_ECRAN_ACCUEIL = {					// liste des images d'aide associées à l'écran d'acceuil
		R.drawable.aide_ecran_accueil_ouvrir_menu_deroulant,
		R.drawable.aide_ecran_accueil_selectionner_capsule,
		R.drawable.aide_ecran_accueil_supprimer_capsule
	};
	private static final int[] LISTE_IMAGES_AIDES_ECRAN_MESSAGES = {				// liste des images d'aide associées à l'écran de lecture des messages 
		R.drawable.aide_ecran_messages_afficher_menu_contextuelle,
		R.drawable.aide_ecran_messages_afficher_profil,
		R.drawable.aide_ecran_messages_ecrire_reponse,
		R.drawable.aide_ecran_messages_afficher_reponses,
		R.drawable.aide_ecran_messages_masquer_reponses,
		R.drawable.aide_ecran_messages_description_icones_defi,
		R.drawable.aide_ecran_messages_filtrer_messages_par_tag,
		R.drawable.aide_ecran_messages_voter_utilite_message
	};
	private static final int[] LISTE_IMAGES_AIDES_ECRAN_MESSAGES_TRANSVERSALE = {	// liste des images d'aide associées à l'écran de lecture des messages en vue transversale
		R.drawable.aide_ecran_messages_transversal_afficher_contexte_message,
		R.drawable.aide_ecran_messages_transversal_afficher_menu_contextuel,
		R.drawable.aide_ecran_messages_transversal_afficher_profil,
		R.drawable.aide_ecran_messages_transversal_description_icones_defi,
		R.drawable.aide_ecran_messages_transversal_filtrer_messages_par_tag,
		R.drawable.aide_ecran_messages_transversal_voter_utilite_message
	};
	private static final int[] LISTE_IMAGES_AIDES_ECRAN_PAGE_CAPSULE = {			// liste des images d'aide associées à l'écran représantant une page d'une capsule
		R.drawable.aide_ecran_page_capsule_changer_page,
		R.drawable.aide_ecran_page_capsule_lire_message
	};
	private static final int[] LISTE_IMAGES_AIDES_ECRAN_CERCLES = {					// liste des images d'aide associées à l'écran de gestion des cercles
		R.drawable.aide_ecran_cercles_ajouter_utilisateur,
		R.drawable.aide_ecran_cercles_suppression_utilisateur,
		R.drawable.aide_ecran_cercles_supprimer_cercle
	};
	private static final int[] LISTE_IMAGES_AIDES_ECRAN_CARTE = {					// liste des images d'aide associées à l'écran de affichant la cartographie des messages
		R.drawable.aide_ecran_carte_afficher_message
	};
	private static final int[] LISTE_IMAGES_AIDES_ECRAN_RESSOURCE = {				// liste des images d'aide associées à l'écran présentant une ressource
		R.drawable.aide_ecran_ressource_afficher_description,
		R.drawable.aide_ecran_ressource_ouvrir_capsule,
		R.drawable.aide_ecran_ressource_telecharger_capsule
	};
	
	/* Textes des aides contextuelles */
	private static final int[] LISTE_TEXTES_AIDES_ECRAN_ACCUEIL = {					// liste des textes d'aide associées à l'écran d'acceuil
		R.string.label_aide_ecran_accueil_ouvrir_menu_deroulant,
		R.string.label_aide_ecran_accueil_selectionner_ressource,
		R.string.label_aide_ecran_accueil_supprimer_ressource
	};
	private static final int[] LISTE_TEXTES_AIDES_ECRAN_MESSAGES = {				// liste des textes d'aide associées à l'écran de lecture des messages 
		R.string.label_aide_ecran_messages_afficher_menu_contextuelle,
		R.string.label_aide_ecran_messages_afficher_profil,
		R.string.label_aide_ecran_messages_ecrire_reponse,
		R.string.label_aide_ecran_messages_afficher_reponses,
		R.string.label_aide_ecran_messages_masquer_reponses,
		R.string.label_aide_ecran_messages_description_icones_defi,
		R.string.label_aide_ecran_messages_filtrer_messages_par_tag,
		R.string.label_aide_ecran_messages_voter_utilite_message
	};
	private static final int[] LISTE_TEXTES_AIDES_ECRAN_MESSAGES_TRANSVERSALE = {	// liste des textes d'aide associées à l'écran de lecture des messages en vue transversale
		R.string.label_aide_ecran_messages_transversal_afficher_contexte_message,
		R.string.label_aide_ecran_messages_transversal_afficher_menu_contextuel,
		R.string.label_aide_ecran_messages_transversal_afficher_profil,
		R.string.label_aide_ecran_messages_transversal_description_icones_defi,
		R.string.label_aide_ecran_messages_transversal_filtrer_messages_par_tag,
		R.string.label_aide_ecran_messages_transversal_voter_utilite_message
	};
	private static final int[] LISTE_TEXTES_AIDES_ECRAN_PAGE_CAPSULE = {			// liste des textes d'aide associées à l'écran représantant une page d'une capsule
		R.string.label_aide_ecran_page_capsule_changer_page,
		R.string.label_aide_ecran_page_capsule_lire_message
	};
	private static final int[] LISTE_TEXTES_AIDES_ECRAN_CERCLES = {					// liste des textes d'aide associées à l'écran de gestion des cercles
		R.string.label_aide_ecran_cercles_ajouter_utilisateur,
		R.string.label_aide_ecran_cercles_suppression_utilisateur,
		R.string.label_aide_ecran_cercles_supprimer_cercle
	};
	private static final int[] LISTE_TEXTES_AIDES_ECRAN_CARTE = {					// liste des textes d'aide associées à l'écran de affichant la cartographie des messages
		R.string.label_aide_ecran_carte_afficher_message
	};
	private static final int[] LISTE_TEXTES_AIDES_ECRAN_RESSOURCE = {				// liste des textes d'aide associées à l'écran présentant une ressource
		R.string.label_aide_ecran_ressource_afficher_description,
		R.string.label_aide_ecran_ressource_ouvrir_capsule,
		R.string.label_aide_ecran_ressource_telecharger_capsule
	};

	private int[] listeImagesAides;								// liste des images des aides contextuelles affichées à l'utilisateur
	private int[] listeTextesAides;								// liste des images des aides contextuelles affichées à l'utilisateur
	private int indiceAideContextuelle;
	private ViewPager viewPager;								// contient la liste des aides, permet le parcoure de la liste avec des swipes
	private ListeAidesPagerAdapter listeAidesPagerAdapter;		// adapter personnalisée gérant l'aide contextuelle
		
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_ecran_aide);
        
        indiceAideContextuelle = getSharedPreferences(Constantes.FICHIER_INFOS_AIDES_CONTEXTUELLES, 0).getInt(Constantes.CLEF_AIDE_CONTEXTUELLE, -1);

		// On affiche la liste des aides correspondant à l'écran d'ou vient l'utilisateur
		switch( indiceAideContextuelle ) {
			case Constantes.AIDE_ECRAN_ACCUEIL:
				listeImagesAides = LISTE_IMAGES_AIDES_ECRAN_ACCUEIL;
				listeTextesAides = LISTE_TEXTES_AIDES_ECRAN_ACCUEIL;
				break;
			case Constantes.AIDE_ECRAN_MESSAGES:
				listeImagesAides = LISTE_IMAGES_AIDES_ECRAN_MESSAGES;
				listeTextesAides = LISTE_TEXTES_AIDES_ECRAN_MESSAGES;
				break;
			case Constantes.AIDE_ECRAN_MESSAGES_TRANSVERSALE:
				listeImagesAides = LISTE_IMAGES_AIDES_ECRAN_MESSAGES_TRANSVERSALE;
				listeTextesAides = LISTE_TEXTES_AIDES_ECRAN_MESSAGES_TRANSVERSALE;
				break;
			case Constantes.AIDE_ECRAN_PAGE_CAPSULE:
				listeImagesAides = LISTE_IMAGES_AIDES_ECRAN_PAGE_CAPSULE;
				listeTextesAides = LISTE_TEXTES_AIDES_ECRAN_PAGE_CAPSULE;
				break;
			case Constantes.AIDE_ECRAN_CERCLES:
				listeImagesAides = LISTE_IMAGES_AIDES_ECRAN_CERCLES;
				listeTextesAides = LISTE_TEXTES_AIDES_ECRAN_CERCLES;
				break;
			case Constantes.AIDE_ECRAN_CARTE:
				listeImagesAides = LISTE_IMAGES_AIDES_ECRAN_CARTE;
				listeTextesAides = LISTE_TEXTES_AIDES_ECRAN_CARTE;
				break;
			case Constantes.AIDE_ECRAN_RESSOURCE:
				listeImagesAides = LISTE_IMAGES_AIDES_ECRAN_RESSOURCE;
				listeTextesAides = LISTE_TEXTES_AIDES_ECRAN_RESSOURCE;
				break;
			default:
				Toast.makeText(this, R.string.label_aide_contextuelle_inexistante, Toast.LENGTH_LONG).show();
				onBackPressed();
				break;
		}

		if ( indiceAideContextuelle >= 0 ) {
	        // on masque l'action bar
	        getActionBar().hide();
	                
	        viewPager = (ViewPager) findViewById(R.id.layout_aide);
	        
	        listeAidesPagerAdapter = new ListeAidesPagerAdapter (
				this,
				listeImagesAides,
				listeTextesAides,
				viewPager
			);
			// mise à jour du viewpager avec l'adapter contenant la liste des pages,
			viewPager.setAdapter(listeAidesPagerAdapter);
		}
	}
}
