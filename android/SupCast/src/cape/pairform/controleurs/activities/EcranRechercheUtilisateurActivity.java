package cape.pairform.controleurs.activities;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import cape.pairform.R;
import cape.pairform.controleurs.adapters.ListeUtilisateursAdapter;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Utilisateur;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Ecran permettant de rechercher un utilisateur parmis tout ceux qui sont inscrit sur l'application
 */
public class EcranRechercheUtilisateurActivity extends ActivityMere {

	private ProgressDialog progressDialog;						// fenêtre de chargement
	private ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();			// liste des utilisateurs
	private ArrayList<Utilisateur> listeUtilisateursSansAvatar = new ArrayList<Utilisateur>();	// liste des utilisateurs dont l'avatar n'est pas téléchargé
	private ArrayList<String> listePseudoUtilisateurs = new ArrayList<String>();				// liste des pseudo des utilisateurs
	private HashMap<String,String> listeIdUtilisateurs = new HashMap<String,String>();			// liste des id des utilisateur : clef = nom utilisateur ; valeur = id utilisateur
	private ListView listeViewUtilisateurs;
	private ListeUtilisateursAdapter adapter;					// adapter personnalisé utilisé avec listViewUtilisateurs
	private AutoCompleteTextView boutonRecherche;
	private ArrayAdapter<String> rechercheAdapter;
	private String stringJSON;									// contenu d'une réponse JSON d'une requête http
	private Activity activityParent;							// activity parent
	private Intent intentCourante;								// intent courante

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActionBar().setTitle(R.string.title_liste_utilisateurs);
		setContentView(R.layout.layout_ecran_recherche_utilisateur);		
		activityParent = this;

		// on bloque l'orientation de l'écran dasn son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		}
		else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}

		// création d'une fenêtre de chargement
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage( getString(R.string.label_chargement) );
		progressDialog.show();

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(this, null, Constantes.AIDE_INEXISTANTE);

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {
				// Envoie d'une requete HTTP récupérant les infos sur les ressources pouvant être téléchargé
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECUPERER_UTILISATEURS, null, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas utilisateursJSON
				JSONObject utilisateursJSON = null;
				if ( stringJSON != null ) {
					// transformation de la réponse en objet JSON
					try {
						utilisateursJSON = new JSONObject(stringJSON);
					} catch (Exception e) {
						e.printStackTrace();
					}

					// on parse la liste des utilisateurs
					listeUtilisateurs = ParserJSON.parserUtilisateurs(utilisateursJSON);

					// récupèration du fichier de préférences contenant la liste des avatars téléchargés
					SharedPreferences fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0);
					SharedPreferences.Editor editorFichierSharedPreferences = fichierSharedPreferences.edit();	

					// on ajoute chaque utilisateur dans la listedes pseudo et des id
					for (Utilisateur utilisateurCourant : listeUtilisateurs) {
						listePseudoUtilisateurs.add(utilisateurCourant.getPseudo());
						listeIdUtilisateurs.put(utilisateurCourant.getPseudo(), utilisateurCourant.getId());

						// si l'avatar n'a pas déjà été téléchargé
						if ( !fichierSharedPreferences.contains( utilisateurCourant.getUrlAvatar() ) ) {
							listeUtilisateursSansAvatar.add( utilisateurCourant );
						} else {
							utilisateurCourant.setUrlAvatar( getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + utilisateurCourant.getId() );
						}
					}

					runOnUiThread(new Runnable() {
						public void run() {
							// si la requete http s'est mal exécuté, stringJSON vaut null
							if ( stringJSON != null ) {
								// on affiche les utilisateurs
								afficherUtilisateurs();
							} else {
								progressDialog.dismiss();
								// on désactive l'orientation forcé de l'écran
								setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
								Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
							}
						}
					});

					// on télécharge les avatars des utilisateurs
					for (Utilisateur utilisateurCourant : listeUtilisateursSansAvatar) {
						String localisationAvatar = getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + utilisateurCourant.getId();

						// si l'avatar n'a pas déjà été téléchargé
						if ( !fichierSharedPreferences.contains( utilisateurCourant.getUrlAvatar() ) ) {	    			
							Utiles.telechargerFichier(utilisateurCourant.getUrlAvatar(), localisationAvatar);

							editorFichierSharedPreferences.putString(utilisateurCourant.getUrlAvatar(), utilisateurCourant.getPseudo());
							editorFichierSharedPreferences.commit();
						}
					}
				}
			}
		});

		thread.start();        
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	// affiche les utilisateurs de l'application
	private void afficherUtilisateurs() {    	
		adapter = new ListeUtilisateursAdapter(
				this,
				R.layout.element_liste_utilisateurs,
				R.color.gris_tres_sombre,
				listeUtilisateurs
				);

		listeViewUtilisateurs = (ListView) findViewById(R.id.liste_utilisateurs);
		listeViewUtilisateurs.setAdapter(adapter);

		listeViewUtilisateurs.setOnItemClickListener( new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// lancement de l'écran affichant le profil de l'utilisateur
				lancerEcranProfil((String) view.getTag());
			}    		
		});

		rechercheAdapter = new ArrayAdapter<String>(this, R.layout.element_recherche, listePseudoUtilisateurs);
		boutonRecherche.setAdapter(rechercheAdapter);

		progressDialog.dismiss();

		// on désactive l'orientation forcé de l'écran
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	}


	// création du menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_ecran_recherche_utilisateur, menu);
		// gestion de la recherche d'un utilisateur via le bouton de recherche
		boutonRecherche = (AutoCompleteTextView) menu.findItem(R.id.button_chercher_utilisateur).getActionView();
		//boutonRecherche.setCompletionHint( getResources().getText(R.string.button_chercher_utilisateur) );
		boutonRecherche.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		boutonRecherche.setLayoutParams( new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT) );
		boutonRecherche.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);

		boutonRecherche.setOnItemClickListener( new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				lancerEcranProfil( listeIdUtilisateurs.get(((TextView) view).getText()) );
			}        	
		});
		boutonRecherche.setOnEditorActionListener( new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView autoCompleteTextView, int actionId, KeyEvent event) {
				if (listeIdUtilisateurs.containsKey( autoCompleteTextView.getText().toString() )) {
					lancerEcranProfil( listeIdUtilisateurs.get(autoCompleteTextView.getText().toString()) );
					return true;
				} else {
					return false;
				}
			}        	
		});

		return true;
	}


	// lancer l'écran affichant le profil de l'utilisateur
	private void lancerEcranProfil(String idUtilisateur) {
		// test la disponibilité de l'accès Internet
		if (!Utiles.reseauDisponible(activityParent)) {
			Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, activityParent);
		} else { // On peut utiliser la connection Internet
			intentCourante = new Intent(activityParent, EcranProfilActivity.class);
			intentCourante.putExtra( Constantes.CLEF_ID_UTILISATEUR, idUtilisateur);
			activityParent.startActivity(intentCourante);
		}
	}


	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.button_sliding_menu:	
			// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
			slidingMenu.toggle();
			return true;
		}
		return false;
	}

	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
