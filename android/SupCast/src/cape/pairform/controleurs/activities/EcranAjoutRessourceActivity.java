package cape.pairform.controleurs.activities;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import cape.pairform.R;
import cape.pairform.controleurs.fragments.AjoutRessourceFragment;
import cape.pairform.controleurs.fragments.AjoutRessourceFragment.TabAjoutRessourceFragmentListener;
import cape.pairform.controleurs.fragments.RessourceFragment;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Ressource;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;

/*
 * Ecran affichant 2 onglets (thèmes / espace) contenant la liste des ressources téléchargeables
 */
public class EcranAjoutRessourceActivity extends ActivityMere implements TabAjoutRessourceFragmentListener {
	
	private ProgressDialog progressDialog;					// fenêtre de chargement
	private ArrayList<Ressource> listeRessources = new ArrayList<Ressource>();	// les ressources pouvant être téléchargées par l'utilisateur
	private String languesAffichees;						// chaine des langues des messages à afficher
	private Bundle bundle;
	private FragmentTransaction fragmentTransaction;		// class permettant de réaliser une transition entre 2 fragments
    private Fragment ressourceFragment;						// Fragment associé à une ressource
    private Fragment ajoutRessourceFragment;				// Fragment associé à la grille des ressources à télécharger

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.title_ajouter_ressource);
        
        // on bloque l'orientation de l'écran dans son orientation actuelle
    	if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
    	   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    	}
    	else {
    	   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
    	}
        
        // création d'une fenêtre de chargement
    	progressDialog = new ProgressDialog(this);
	    progressDialog.setMessage( getString(R.string.label_chargement) );
	    progressDialog.show();
	    
        // on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
        Utiles.determinerAideContextuelle(this, null, Constantes.AIDE_INEXISTANTE);
        
        // on recupere les preferences sur les langues
        languesAffichees = Utiles.StringListeLanguesUtilisateur(this);
        
        // création d'un nouveau Thread
	    Thread thread = new Thread(new Runnable() {
	    	public void run() {	    		
	    		// création et configuration des paramètres d'une requête http
	    		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
	        	
	            try {
	            	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_LANGUES_AFFICHAGE, languesAffichees) );	// langues de l'affichage des ressources des préférences
	            	parametresRequete.add( new BasicNameValuePair(															// langues de l'application
	            		Constantes.CLEF_WEBSERVICE_LANGUE_APPLICATION,
	            		Utiles.idLangueAvecCode(getResources().getString(R.string.locale_language),
	            		getApplicationContext())
	            	));
	            } catch (Exception e) {
	            	e.printStackTrace();        	
	            }
	            // Envoie d'une requete HTTP récupérant les infos sur les ressources pouvant être téléchargé
	        	String stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_LISTER_RESSOURCE_TELECHARGEABLE, parametresRequete, getApplicationContext());

	        	// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas ressourcesJSON
	        	if ( stringJSON != null ) {
		            // transformation de la réponse en objet JSON
	        		JSONObject ressourcesJSON = null;
		            try {
		            	ressourcesJSON = new JSONObject(stringJSON);
		            } catch (Exception e) {
		            	e.printStackTrace();
		            }  
		            listeRessources = ParserJSON.parserRessources(ressourcesJSON);
		        }
	        		        	
	        	runOnUiThread(new Runnable() {
	    			public void run() {
	    				// on affiche les ressources pouvant être téléchargé
	    	        	afficherRessources();
	    	        }
	    	    });
	    	}
	    });
	    
	    thread.start();
	}
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    }
    
    // afficher les ressources pouvant être téléchargées dans les onglets (représentés par des Fragments)
    private void afficherRessources() {
    	if ( !listeRessources.isEmpty() ) {    		
    		// création d'un fragment représentant la grille des ressources à télécharger
    		ajoutRessourceFragment = Fragment.instantiate(this, AjoutRessourceFragment.class.getName());
    		
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
    		fragmentTransaction.replace( android.R.id.content, ajoutRessourceFragment );
    		fragmentTransaction.commit();
    		
    		// on désactive l'orientation forcé de l'écran et on ferme la fenêtre de chargement
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    		progressDialog.dismiss();
    	} else {
			Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, this);
        }
    }

 	// méthode de l'interface TabAjoutRessourceFragmentListener (class interne de TabAjoutRessourceFragment.java) - permet de récupérer la liste des ressources que l'utilisateur peut télécharger
	@Override
	public ArrayList<Ressource> getListeRessources() {
        return listeRessources;
	}

	// méthode de l'interface TabAjoutRessourceFragmentListener (class interne de TabAjoutRessourceFragment.java) - se déclenche quand l'utilisateur sélectionne une ressource dans une des grille des ressources
	@Override
	public void onSelectedRessource(String idRessource) {
		// création d'un Bundle contenant l'id de la ressource que l'utilisateur consulte
		bundle = new Bundle();
		bundle.putString(
        	Constantes.CLEF_ID_RESSOURCE,
        	idRessource
        );
        
        // création d'un fragment représentant une ressource
        ressourceFragment = Fragment.instantiate(this, RessourceFragment.class.getName(), bundle );
		
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.replace( android.R.id.content, ressourceFragment );
		fragmentTransaction.commit();
	}
    
    
    // création du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ecran_ajout_ressource, menu);
        return true;
    }
    
    
    // gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		switch (item.getItemId()) {
 			case R.id.button_sliding_menu:	// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
 				slidingMenu.toggle();
 				return true;
 		}
 		return false;
 	}
 	
 	
 	// gestion du changement d'orientation
 	@Override
 	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
