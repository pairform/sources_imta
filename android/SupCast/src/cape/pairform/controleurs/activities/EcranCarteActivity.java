package cape.pairform.controleurs.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import cape.pairform.R;
import cape.pairform.controleurs.fragments.CarteFragment;

/*
 * Ecran d'affichage de la carte avec les messages geolocalisés 
 */
public class EcranCarteActivity extends FragmentActivity {
	
	private CarteFragment carteFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_ecran_carte);
		
		if (findViewById(R.id.fragment_container) != null) {
			//Si la vue est restauré de l'état précédent on ne fait rien car on dédoublerait le fragment
            if (savedInstanceState != null) {
                return;
            }

            carteFragment = new CarteFragment();
            //On envoi les arguments passés à l'activité au fragment
            carteFragment.setArguments(getIntent().getExtras());
            
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, carteFragment).commit();
        }
	}

}