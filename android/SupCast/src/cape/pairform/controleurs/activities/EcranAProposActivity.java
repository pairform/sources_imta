 package cape.pairform.controleurs.activities;

import android.app.ActionBar;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import cape.pairform.R;
import cape.pairform.controleurs.ActionBarTabListener;
import cape.pairform.controleurs.fragments.AProposFragment;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.controleurs.services.communication.LiaisonActivityAService;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;

/*
 * Ecran affichant une description de l'application et des infos sur celle-ci
 */
public class EcranAProposActivity extends ActivityMere {
	
	private Bundle bundleRessources = new Bundle();		// Bundle trasnmit aux fragments contenant les infos qu'ils doivent afficher
	/*
 	 * *****************************
 	 * Code de gestion du service
 	 * *****************************
 	 */
 	private ServiceGeneral service;
	private LiaisonActivityAService liaisonServiveActivity = LiaisonActivityAService.getInstance();
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.title_a_propos);
        
        liaisonServiveActivity.addConnection(this , new ServiceConnection() {
    	    public void onServiceConnected(ComponentName className, IBinder s) {
    	    	service = ((ServiceGeneral.ServiceGeneralBinder)s).getService();
    	    }

    	    public void onServiceDisconnected(ComponentName className) {
    	    	service = null;
    	    }
    	});
        
        liaisonServiveActivity.bindActiviteService(this);
        
        // on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
        Utiles.determinerAideContextuelle(this, null, Constantes.AIDE_INEXISTANTE);
        		
		// configuration de l'actionBar (suite) : ajout des Tabs (onglets)
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        getActionBar().addTab (
        	getActionBar().newTab()
        		.setText( R.string.button_cgu )
        		.setTabListener( new ActionBarTabListener( this, AProposFragment.class.getName(), bundleRessources, Constantes.URL_FICHIER_CGU +"/"+ getString(R.string.locale_language) ) )
        );
        getActionBar().addTab (
        	getActionBar().newTab()
        		.setText( R.string.button_infos_pairform )
        		.setTabListener( new ActionBarTabListener( this, AProposFragment.class.getName(), bundleRessources, "file:///android_asset/"+ getString(R.string.locale_language) +"/"+ Constantes.FICHIER_A_PROPOS_INFOS_PAIRFORM ) )
        );
	}
	
	@Override
	protected void onDestroy() {
		liaisonServiveActivity.removeConnection(this);
		liaisonServiveActivity.unbindActiviteService(this);
		super.onDestroy();
	}
	
	// création du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ecran_defaut, menu);
        return true;
    }
    
    
    // gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		switch (item.getItemId()) {
 			case R.id.button_sliding_menu:	
 				// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
 				slidingMenu.toggle();
 				return true;
 		}
 		return false;
 	}
    
 	
 	// gestion du changement d'orientation
 	@Override
  	public void onConfigurationChanged(Configuration newConfig) {
 		super.onConfigurationChanged(newConfig);
 	}
}
