package cape.pairform.controleurs.activities;

import java.net.MalformedURLException;
import org.piwik.sdk.Piwik;
import org.piwik.sdk.Tracker;
import android.app.Activity;
import android.app.Application;


public class ApplicationController extends Application {

	//private static ServiceGeneral service;
	//private LiaisonActivityAService liaisonServiveActivity = LiaisonActivityAService.getInstance();
	
	private Activity activityCourante;
	private Tracker piwikTracker;
	private Piwik piwikInstance;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		/*liaisonServiveActivity.addConnection(this, new ServiceConnection() {
			public void onServiceConnected(ComponentName className, IBinder s) {
				service = ((ServiceGeneral.ServiceGeneralBinder)s).getService();
			}

			public void onServiceDisconnected(ComponentName className) {
				service = null;
			}
		});*/
	}

	
	public Activity getActivityCourante() {
		return activityCourante;
	}

	
	public void setActivityCourante(Activity acivityCourante) {
		this.activityCourante = acivityCourante;
	}
	
	
	/* 
	 * doc Piwik android
	 * https://github.com/piwik/piwik-sdk-android
	 */
	public synchronized Tracker getTracker() {
        if (piwikTracker != null) {
            return piwikTracker;
        }

        try {
        	piwikInstance = Piwik.getInstance(this);
            piwikTracker = piwikInstance.newTracker("http://imedia.emn.fr/piwik/piwik/", 3);
        } catch (MalformedURLException e) {
        	e.printStackTrace();
            return null;
        }

        return piwikTracker;
    }
	
	
	/*public ServiceGeneral getService(){
		return service;
	}*/
}
