package cape.pairform.controleurs.activities;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.adapters.ProfilPagerAdapter;
import cape.pairform.controleurs.dialogs.ConnexionDialog;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.modele.Cercle;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.ProfilUtilisateur;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Ecran présentant le profil d'un utilisateur (nom, avatar, succès, ressources, etc.)
 */
public class EcranProfilActivity extends ActivityMere {

	private ProgressDialog progressDialog;									// fenêtre de chargement
	private Intent intentCourante;											// intent courante
	private Activity activityParent;										// activity parent
	private SharedPreferences fichierSharedPreferences;						// permet l'accès aux fichiers des préférences
	private ConnexionDialog connexionDialog;								// fenêtre de connexion
	private String stringJSON;												// contenu d'une réponse JSON d'une requête http
	private String idUtilisateur;											// id de l'utilisateur dont on consulte le profil
	private ProfilUtilisateur profilUtilisateur;
	private ViewPager viewPagerProfil;										// view contenant dans des tabs des ressources, des succès et des badges d'un utilisateur
	private ProfilPagerAdapter profilPagerAdapter;							// adapter personnalisée gérant les tabs contenant les ressources, les succès et les badges d'un utilisateur
	private Button buttonMessages;											// bouton affichant les messages d'un utilisateur ou ses propores messages (s'il est sur son propre profil)
	private Button buttonCercles;											// bouton permettant d'ajouter un utilisateur à un cercle ou d'afficher les cercles de l'utilisateur (s'il est sur son propre profil)
	private AlertDialog.Builder dialogBuilder;								// builder de gestion de la fenêtre courante
	private ArrayList<Cercle> listeCercles;									// liste des cercles de l'utilisateur connecté
	private ArrayList<Cercle> listeCerclesAffiches = new ArrayList<Cercle>();	// liste des cercles que l'utilisateur connecté a créé (donc, pas les classes rejointes)
	private ArrayList<String> listeNomCercles = new ArrayList<String>();	// liste du nom des cercles de l'utilisateur
	private ArrayAdapter<String> listeCerclesAdapter;						// adapter associé à une liste de cercles
	private String erreurRetourJSON;										// erreur retourné par un web service


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.title_profil);
		setContentView(R.layout.layout_ecran_profil);
		activityParent = this;

		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}

		// création d'une fenêtre de chargement
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage( getString(R.string.label_chargement) );
		progressDialog.show();

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(this, null, Constantes.AIDE_INEXISTANTE);

		// on récupère le fichier de préfèrences stockant les informations sur l'utilisateur connecté
		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);

		idUtilisateur = getIntent().getStringExtra(Constantes.CLEF_ID_UTILISATEUR);

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(													// langues de l'application
						Constantes.CLEF_WEBSERVICE_LANGUE_APPLICATION,
						Utiles.idLangueAvecCode(getResources().getString(R.string.locale_language), getApplicationContext()))
					);
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_UTILISATEUR, idUtilisateur));	// id de l'utilisateur
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP récupérant les infos de profil de l'utilisateur
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AFFICHER_PROFIL, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas profilJSON
				JSONObject profilJSON = null;
				if ( stringJSON != null ) {
					// transformation de la réponse en objet JSON
					try {
						profilJSON = new JSONObject(stringJSON);
					} catch (Exception e) {
						e.printStackTrace();
					}

					// on parse le profil utilisateur
					profilUtilisateur = ParserJSON.parserProfil(profilJSON, idUtilisateur);

					// si l'avatar n'a pas déjà été téléchargé
					if ( !fichierSharedPreferences.contains( profilUtilisateur.getUrlAvatar() ) ) {
						String cheminAccesAvatar = getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + idUtilisateur;
						Utiles.telechargerFichier(profilUtilisateur.getUrlAvatar(), cheminAccesAvatar);

						SharedPreferences.Editor editorFichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_LISTE_URL_AVATAR, 0).edit();

						editorFichierSharedPreferences.putString(profilUtilisateur.getUrlAvatar(), profilUtilisateur.getNomUtilisateur());
						editorFichierSharedPreferences.commit();
					}
				}

				runOnUiThread(new Runnable() {
					public void run() {
						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON != null ) {
							// on affiche les infos sur l'utilisateur
							afficherProfil();
						} else {
							// on ferme la fenetre de chargement et on désactive l'orientation forcé de l'écran
							progressDialog.dismiss();
							setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
						}
					}
				});
			}
		});

		thread.start();
	}


	// affiche les infos sur l'utilisateur
	private void afficherProfil() {
		// avatar de l'utilisateur
		((ImageView) findViewById(R.id.avatar_utilisateur)).setImageURI( 
			Uri.parse( getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + idUtilisateur )
		);
		// établissement de l'utilisateur
		((TextView) findViewById(R.id.etablissement_utilisateur)).setText( profilUtilisateur.getEtablissement() );

		// drapeau de la langue de l'utilisateur
		int idDrapeauImage = getResources().getIdentifier("cape.pairform:drawable/" + "icone_drapeau_" + profilUtilisateur.getLangue(), null, null);
		((ImageView) findViewById(R.id.langue_utilisateur)).setImageResource(idDrapeauImage);

		// pseudo de l'utilisateur
		((TextView) findViewById(R.id.nom_utilisateur)).setText( profilUtilisateur.getNomUtilisateur() );

		// configuration du bouton messages
		buttonMessages = (Button) findViewById(R.id.messages_utilisateur);
		// si l'utilisateur regarde son propre profil (il est donc identifié)
		if ( idUtilisateur.equals(fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, "")) ) {
			buttonMessages.setText(R.string.button_mes_messages);
		} else {
			buttonMessages.setText(R.string.button_ses_messages);
		}
		buttonMessages.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				// lancement de l'ecran de lecture des messages de l'utilisateur en vue transversal
				intentCourante = new Intent(activityParent, EcranVueTransversaleActivity.class);
				intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, idUtilisateur);
				startActivity(intentCourante);
			}
		});

		// configuration du bouton cercles
		buttonCercles = (Button) findViewById(R.id.cercles_utilisateur);
		// si l'utilisateur regarde son propre profil (il est donc identifié)
		if ( idUtilisateur.equals(fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, "")) ) {
			buttonCercles.setText(R.string.button_cercles);
			buttonCercles.setOnClickListener( new OnClickListener() {
				public void onClick(View v) {
					// lancement de l'ecran de gestion des cercles / classes
					intentCourante = new Intent(activityParent, EcranCerclesActivity.class);
					startActivity(intentCourante);
				}
			});

		} else {
			buttonCercles.setText(R.string.button_ajouter_a_cercle);
			buttonCercles.setOnClickListener( new OnClickListener() {
				public void onClick(View v) {
					// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
					if ( activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).contains(Constantes.CLEF_ID_UTILISATEUR) ) {
						recupererListeCercles();
					} else {
						// ouverture de la fenêtre de connexion
						connexionDialog = new ConnexionDialog( activityParent, service);
						connexionDialog.setOnConnectedListener( new OnConnectedListener() {
							@Override
							public void onConnected(boolean success) {
								if(success)
									recupererListeCercles();
							}		    		
						});
						connexionDialog.show();
						//TODO: vous devez etre connecté pour les cercles
					}
				}
			});
		}
		
		viewPagerProfil = (ViewPager) findViewById(R.id.view_pager_profil);

		profilPagerAdapter = new ProfilPagerAdapter(
			this,
			viewPagerProfil,
			profilUtilisateur
		);
		
		// mise à jour du viewpager avec l'adapter contenant la liste des pages,
		viewPagerProfil.setAdapter(profilPagerAdapter);
		viewPagerProfil.setCurrentItem(1);					// l'écrans s'ouvre avec le  tab des succès de l'utilisateur
	
		progressDialog.dismiss();

		// on désactive l'orientation forcé de l'écran
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	}


	// récupère la liste des cercle de l'utilisateur connecté
	private void recupererListeCercles() {
		// test la disponibilité de l'accès Internet
		if ( !Utiles.reseauDisponible( this ) ) {
			Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, this);
		} else { // On peut utiliser la connection Internet
			// on ouvre une fenetre contenant la liste des cercles
			// si la liste des cercles n'existe pas
			if (listeCercles == null) {
				// récupération de tous les cercles de l'utilisateur
				// création d'une fenêtre de chargement
				progressDialog = new ProgressDialog( this );
				progressDialog.setMessage( getString(R.string.label_chargement) );
				progressDialog.show();

				// on bloque l'orientation de l'écran dans son orientation actuelle
				if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
				} else {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
				}
				// création d'un nouveau Thread
				Thread thread = new Thread(new Runnable() {
					public void run() {
						// Envoie d'une requete HTTP récupérant les cercles et classes de l'utilisateur
						final String stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AFFICHER_CERCLES, null, activityParent);

						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON != null ) {
							listeCercles = ParserJSON.parserCercles(stringJSON);

							// si l'utilisateur n'a pas de cercles, on ajoute un message d'infos
							if( listeCercles.isEmpty() ) {
								listeNomCercles.add( getResources().getString(R.string.label_liste_cercles_vide) );
							} else {
								// pour chaque cercle de l'utilisateur
								for (Cercle cercleCourant : listeCercles) {
									// si le cercle n'est pas une classe rejointes
									if( cercleCourant.isCreateur() ) {
										listeNomCercles.add( cercleCourant.getNomCercle() );
										listeCerclesAffiches.add( cercleCourant );
									}
								}
							}
						}
						runOnUiThread( new Runnable() {
							public void run() {
								// si la requete http s'est mal exécuté, stringJSON vaut null
								if ( stringJSON != null ) {
									// on affiche les cercles et classes
									listeCerclesAdapter = new ArrayAdapter<String>(activityParent, R.layout.element_liste_defaut, listeNomCercles);
									ouvrirFenetreListeCercles();
								}
								// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
								progressDialog.dismiss();
								activityParent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);	
							}
						});
					}
				});

				thread.start();
			} else {
				ouvrirFenetreListeCercles();
			}
		}
	}


	// ouvre une fenetre contenant la liste des cercles de l'utilisateur
	private void ouvrirFenetreListeCercles() {
		// création de la fenêtre contenant la liste des utilisateur
		dialogBuilder = new AlertDialog.Builder(activityParent);
		dialogBuilder.setTitle(R.string.title_cercles)
		.setAdapter(listeCerclesAdapter, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				ajouterMembreCercle( listeCerclesAffiches.get(itemSelectionne).getIdCercle(), idUtilisateur);
			}
		})
		.setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		})
		.show();
	}


	// ajoute un membre dans un cercle
	private void ajouterMembreCercle(final String IdCercleCourant, final String idNouveauMembreCercle) {
		// on bloque l'orientation de l'écran dans son orientation actuelle
		if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {	    		
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CERCLE, IdCercleCourant));					// id du cercle
					parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_UTILISATEUR, idNouveauMembreCercle));		// id de l'utilisateur ajouté au cercle
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP permettant l'ajout d'un membre dans un crecle
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AJOUTER_MEMBRE_CERCLE, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null
				if ( stringJSON != null ) {
					erreurRetourJSON = ParserJSON.parserRetourAjoutMembreCercle(stringJSON);
				}	        	

				runOnUiThread(new Runnable() {
					public void run() {	    				
						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);

						} else if (erreurRetourJSON != null) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
						} else {
							Toast.makeText(activityParent, R.string.label_membre_ajoute_au_cercle, Toast.LENGTH_LONG).show();
						}
						// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
						progressDialog.dismiss();
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);	
					}
				});
			}
		});

		thread.start();
	}


	// création du menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_ecran_profil, menu);

		// si l'utilisateur regarde son propre profil (il est donc identifié)
		if ( idUtilisateur.equals(fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, "")) ) {
			menu.findItem(R.id.button_reglages).setVisible(true);
		} else {
			menu.findItem(R.id.button_reglages).setVisible(false);
		}

		return true;
	}


	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.button_sliding_menu:	// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
			slidingMenu.toggle();
			return true;
		case R.id.button_reglages:
			// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
			if ( fichierSharedPreferences.contains(Constantes.CLEF_ID_UTILISATEUR) ) {
				// lancement de l'écran de réglages
				intentCourante = new Intent(this, EcranReglagesActivity.class);
				startActivity(intentCourante);
			} else {
				// ouverture de la fenêtre de connexion
				connexionDialog = new ConnexionDialog(this, service);
				connexionDialog.setOnConnectedListener( new OnConnectedListener() {
					@Override
					public void onConnected(boolean success) {
						// lancement de l'écran de réglages
						if(success){
							intentCourante = new Intent(activityParent, EcranReglagesActivity.class);
							startActivity(intentCourante);
						}
					}		    		
				});
				connexionDialog.show();
				//TODO: vous devez etre connecté pour voir les reglages
			}
			break;
		}
		return false;
	}


	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
