package cape.pairform.controleurs.activities;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.ListIterator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import cape.pairform.R;
import cape.pairform.controleurs.fragments.LectureMessagesFragment;
import cape.pairform.controleurs.fragments.PagesCapsuleFragment;
import cape.pairform.controleurs.fragments.SommaireCapsuleFragment;
import cape.pairform.controleurs.fragments.SommaireCapsuleFragment.SommaireCapsuleFragmentListener;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.modele.ChapitreCapsule;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.PageCapsule;
import cape.pairform.utiles.ParserXmlCapsule;


/*
 * liste des Ecrans affichant le contenu d'une ressource (sommaire, lecture des pages, messages contenu dans la ressources, ...)
 */
public class EcranCapsuleActivity extends ActivityMere implements SommaireCapsuleFragmentListener {

	private String idCapsule;													// id de la capsule courante
	private String nomPageCapsule;												// nom de la page que l'utilisateur veut ouvrir
	private Intent intentCourante;												// intent courant
	private Bundle bundleCapsule;												// Bundle contenant les données (à transmettre) sur la capsule en court
	private Bundle bundlePagesCapsule;											// Bundle contenant les données (à transmettre) sur les pages de la capsule en court
	private Bundle bundleMessage;												// Bundle contenant les données (à transmettre) sur la position d'où l'utilisateur publie son message
	private Fragment sommaireCapsuleFragment = null;							// Fragment associé au sommaire des chapitres de la capsule
	private Fragment pagesCapsuleFragmentCourant = null;						// Fragment associé aux pages de la capsule
	private Fragment lectureMessagesFragment = null;							// Fragment associé à la liste des messages de la capsule ou d'une page de la capsule ou une sous partie de la page
	private FragmentTransaction fragmentTransaction;							// class permettant de réaliser une transition entre 2 fragments
	private ArrayList<String> listeNomPagesCapsule = new ArrayList<String>();	// liste contenant le nom de toutes les pages de la capsule
	private Object filsCourant;													// chapitre ou page courant de la capsule
	private ChapitreCapsule chapitreRacine;		

	//Référence faible vers cette activité pour savoir quand on commit les fragment en asynchrone, si l'activité est tjr ok
	//Ceci est du au lancement du fragment quand le service est lié
	private static WeakReference<EcranCapsuleActivity> wrActivity = null;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		wrActivity = new WeakReference<EcranCapsuleActivity>(this);

		idCapsule = getIntent().getStringExtra(Constantes.CLEF_ID_CAPSULE);
		nomPageCapsule = getIntent().getStringExtra(Constantes.CLEF_NOM_PAGE_CAPSULE);

		// on lance le service de recherche de la disponibilité d'une mise à jour pour la ressource
		/*intentCourante = new Intent(this, ChercheurMiseAJourRessourceService.class);
    	intentCourante.putExtra(Constantes.CLEF_ID_RESSOURCE, idCapsule);
		startService(intentCourante);*/

		// création d'un Bundle contenant l'id de la capsule que l'utilisateur consulte
		bundleCapsule = new Bundle();
		bundleCapsule.putString(
			Constantes.CLEF_ID_CAPSULE,
			idCapsule
		);	        

		if (nomPageCapsule == null) {
			nomPageCapsule = "";
		}

		//Le constructeur parent doit être appelé en dernier.
		//C'est celui de la classe abstraite mere qui lance la fonction onServiceBound qui permet de savoir de manière asynchrone quand le service est lié
		super.onCreate(savedInstanceState);
	}

	
	@Override
	protected void onServiceBound(ServiceGeneral service) {
		super.onServiceBound(service);
		
		if ((wrActivity.get() != null) && (wrActivity.get().isFinishing() != true)) {			
			// si le nom de page est vide (= l'utilisateur arrive de l'écran vue transversal ; le message sur lequel il a cliqué est sur le sommaire de la capsule, pas sur une page) 
			// ou
			// si le nom de page n'existe pas (= l'utilisateur arrive de l'écran d'acceuil)
			if ( nomPageCapsule.equals("")) {
				// création d'un fragment représentant l'arborescence des chapitres la capsule
				sommaireCapsuleFragment = Fragment.instantiate(this, SommaireCapsuleFragment.class.getName(), bundleCapsule );
				// lancement d'une transition pour afficher sommaireCapsuleFragment
				fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.replace( android.R.id.content, sommaireCapsuleFragment );
				fragmentTransaction.commit();
			} else {	// sinon, l'utilisateur arrive de la vue transversal
				// ouverture de la page contenu dans nomPageCapsule
				// création d'un nouveau Thread
				Thread thread = new Thread(new Runnable() {
					public void run() {
						// parsing du fichier XML contenant l'arborescence des chapitres et des pages de la capsule
						File file = new File(getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() +"/"+idCapsule + "/co/outline.xml");

						ParserXmlCapsule parserXml = new ParserXmlCapsule();
						try {
							// à la fin du parsing, on récupère le chapitre racine
							chapitreRacine = parserXml.parse(file);
						} catch (Exception e) {
							e.printStackTrace();
						}

						// execution dans le UI thread
						runOnUiThread(new Runnable() {
							public void run() {
								onSelectedPage(nomPageCapsule, chapitreRacine);
							}
						});
					}
				});

				thread.start();
			}
		}

	}

	
	// méthode de l'interface SommaireCapsuleFragmentListener (class interne de sommaireCapsuleFragment.java)
	// déclanché quand l'utilisateur sélectionne une page dans l'arborescence des chapitres de la capsule
	@Override
	public void onSelectedPage(String nomPageCourante, ChapitreCapsule chapitreRacine) {
		// si la liste des noms de toutes les pages est vide (c'est à dire, l'utilisateur n'a pas encore lancé au moins 1 fois le fragment permettant la lecture des pages)
		if (listeNomPagesCapsule.isEmpty()) {
			// on remplit la liste des noms de toutes les pages de la capsule
			remplirListePageCapsule(chapitreRacine);
		}

		// si le bundle n'a pas déjà été créé
		if(bundlePagesCapsule == null) {
			// création d'un Bundle contenant le nom de la capsule et de la page que l'utilisateur consulte et la liste des noms de toutes les pages
			bundlePagesCapsule = new Bundle();
			bundlePagesCapsule.putString(
				Constantes.CLEF_ID_CAPSULE,
				idCapsule
			);
			bundlePagesCapsule.putStringArrayList(
				Constantes.CLEF_LISTE_NOM_PAGE_CAPSULE,
				listeNomPagesCapsule						// liste des noms de toutes les pages
			);
		}
		bundlePagesCapsule.putString(
			Constantes.CLEF_NOM_PAGE_CAPSULE,
			nomPageCourante									// nom de la page consulté
		);

		// lancement d'une transition cachant l'arborescence des chapitres pour afficher la page choisi par l'utilisateur
		pagesCapsuleFragmentCourant = Fragment.instantiate(this, PagesCapsuleFragment.class.getName(), bundlePagesCapsule );
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.replace( android.R.id.content, pagesCapsuleFragmentCourant);
		fragmentTransaction.commitAllowingStateLoss();
	}

	
	// remplit la liste des noms de toutes les pages de la capsule en parcourant l'arborescence des chapitres récursivement
	private void remplirListePageCapsule(ChapitreCapsule chapitreCourant) {
		ListIterator<Object> iterateur;

		// pour tout les chapitres et les pages fils du chapitre courant
		for (iterateur = chapitreCourant.getListePagesEtChapitresFils().listIterator(); iterateur.hasNext();) {	
			filsCourant = iterateur.next();

			// si le fils du chapitre courant est un chapitre
			if( filsCourant.getClass() == ChapitreCapsule.class ) {
				// on recommence l'opération (récursivité), le chapitre fils devient le chapitre courant
				remplirListePageCapsule( (ChapitreCapsule) filsCourant );

			} else {	// si le fils du chapitre courant est une page
				// on ajoute la page à la liste de s noms de toutes les pages de la capsuleC
				listeNomPagesCapsule.add( ((PageCapsule) filsCourant).getUrlRelativePage() );				
			}				
		}		
	}

	
	@JavascriptInterface
	public void onClickedLireMessage(String retourJS) {
		afficherEcranLireMessage(
			retourJS.substring( retourJS.lastIndexOf(':') + 1 ), 
			retourJS.substring( 0, retourJS.indexOf(':') ), 
			retourJS.substring( retourJS.indexOf(':') + 1, retourJS.lastIndexOf(':') )
		);
	}


	@Override
	public void onClickedLireMessageButton() {
		afficherEcranLireMessage("", "", "0");
	}


	private void afficherEcranLireMessage(String nomPage, String nomTagPage, String numOccurenceTagPage) {		
		// création d'un Bundle contenant le nom de la page que l'utilisateur consulte et l'id de la capsuleC
		bundleMessage = new Bundle();
		bundleMessage.putString(
			Constantes.CLEF_ID_CAPSULE,
			idCapsule
		);
		bundleMessage.putString(
			Constantes.CLEF_NOM_PAGE_CAPSULE,
			nomPage
		);
		bundleMessage.putString(
			Constantes.CLEF_NOM_TAG_PAGE_MESSAGE,
			nomTagPage
		);
		bundleMessage.putString(
			Constantes.CLEF_NUM_OCCURENCE_TAG_PAGE_MESSAGE,
			numOccurenceTagPage
		);

		// lancement d'une transition cachant l'arborescence des chapitres ou la page et affichant l'ecran d'écriture d'un message
		lectureMessagesFragment = Fragment.instantiate(this, LectureMessagesFragment.class.getName(), bundleMessage );
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.replace( android.R.id.content, lectureMessagesFragment);
		fragmentTransaction.commit();
	}

	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	
	// création du menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_ecran_capsule, menu);
		return true;
	}

	
	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.button_accueil:
			// lancement de l'écran d'accueil
			intentCourante = new Intent(this, EcranTableauDeBordActivity.class);
			startActivity(intentCourante);
			return true;
		case R.id.button_sommaire_capsule:
			// retour au sommaire de la capsule 				
			// lancement d'une transition qui masque les fragments précédemment affichés
			sommaireCapsuleFragment = Fragment.instantiate(this, SommaireCapsuleFragment.class.getName(), bundleCapsule );		        
			fragmentTransaction = getSupportFragmentManager().beginTransaction();
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.replace( android.R.id.content, sommaireCapsuleFragment );
			fragmentTransaction.commit();
			return true;
		case R.id.button_sliding_menu:	// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
			slidingMenu.toggle();
			return true;
		}
		return false;
	}

	
	//Important pour que les segments aient accès au retour d'acitivité qu'ils lancent 
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	
	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
