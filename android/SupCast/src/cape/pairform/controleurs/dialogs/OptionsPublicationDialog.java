package cape.pairform.controleurs.dialogs;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.modele.Cercle;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Fenêtre permettant de gérer les options de publication d'un nouveau message
 */
public class OptionsPublicationDialog extends AlertDialog.Builder {

	private Activity activityParent;											// activity parent
	private ProgressDialog progressDialog;										// fenêtre de chargement
	private int idRoleUtilisateur;												// id du rôle de l'utilisateur dans la ressource dans laquelle l'utilisateur écrit un message
	private ScrollView OptionsPublicationLayout;								// layout de la fenêtre
	private LinearLayout tagsLayout;											// layout contenant les tags associés au nouveau message
	private RelativeLayout tagLayout;											// layout d'un tag associé au message
	private LinearLayout cerclesLayout;											// layout contenant les cercles associés au nouveau message
	private RelativeLayout cercleLayout;										// layout d'un cercle associé au message
	private Switch switchDefi;													// view associé au défi
	private ArrayList<CharSequence> listeTags = new ArrayList<CharSequence>();	// liste des tags associés au nouveau message
	private ArrayList<Cercle> listeCercles;										// liste des cercles associés au nouveau message
	private ArrayList<Cercle> listeCerclesUtilisateur;							// liste de tout les cercles de l'utilisateur
	private ArrayList<String> listeNomCerclesUtilisateur = new ArrayList<String>();		// liste du nom des cercles de l'utilisateur
	private ArrayAdapter<String> listeCerclesAdapter;							// adapter associé à une liste de cercles
	private CheckedTextView geolocaliserMessageCheckedTextView; 				// checkbox pour représenter l'activation de la geolocalisation des messages
	
	//Static car 1 seul objet service donc static limite l'utilisation mémoire
	private static ServiceGeneral service;
	
	
	public OptionsPublicationDialog(Activity activityParent, ServiceGeneral _service, int idRoleUtilisateur, ArrayList<CharSequence> listeTagsNouveauMessage, ArrayList<Cercle> listeCerclesNouveauMessage, boolean estDefi, boolean estReponse, boolean geolocalise) {
		super(activityParent);
		this.activityParent = activityParent;
		service = _service;
		this.idRoleUtilisateur = idRoleUtilisateur;
		this.listeTags = listeTagsNouveauMessage;
		this.listeCercles = listeCerclesNouveauMessage;
		
		OptionsPublicationLayout = (ScrollView) activityParent.getLayoutInflater().inflate(R.layout.layout_dialog_options_publication, null);
 		tagsLayout = (LinearLayout) OptionsPublicationLayout.findViewById(R.id.layout_ajouter_tag);
 		cerclesLayout = (LinearLayout) OptionsPublicationLayout.findViewById(R.id.layout_ajouter_cercle);
 		switchDefi = (Switch) OptionsPublicationLayout.findViewById(R.id.button_defi);
 		geolocaliserMessageCheckedTextView = (CheckedTextView)OptionsPublicationLayout.findViewById(R.id.geolocalisation_message);

 		geolocaliserMessageCheckedTextView.setChecked(geolocalise);
 		
 		//Le composant CheckedTextView est fait pour les listes donc pas cochable hors liste par défault.
 		//Mais c'est le seul à intégrer JOLIEMENT du texte à gauche de la checkbox
 		//Donc je redéfinie son comportement 
 		geolocaliserMessageCheckedTextView.setOnClickListener(new View.OnClickListener() {
 	        public void onClick(View v) {
 	            ((CheckedTextView) v).toggle();
 	        }
 	    });
 		
 		// si le message est une réponse
		if( estReponse) {
			cerclesLayout.findViewById(R.id.ajouter_cercle).setVisibility( View.GONE );													// on masque le bouton permettant de modifier la visibilité
			((TextView) cerclesLayout.findViewById(R.id.label_visibilite_public)).setText(R.string.label_visibilite_message_parent);	// on met à jour le texte pour indiquer que la visibilité d'une réponse est celle du message parent
		}
 		
 		// si l'utilisateur est ni un expert, ni un admin OU si le message est une réponse
		if( idRoleUtilisateur < Constantes.ID_EXPERT || estReponse) {
			switchDefi.setVisibility( View.GONE );													// on masque le bouton permettant de choisir si le message est un défi ou non
			OptionsPublicationLayout.findViewById(R.id.titre_defi).setVisibility( View.GONE );		// on masque le titre de défi
		} else {
			switchDefi.setChecked(estDefi); 				// sinon, on initialise la valeur du défi
		}

 		// initialisation des tags
 		for(CharSequence tag : listeTags) {
 			// ajout du tag
			tagLayout = (RelativeLayout) activityParent.getLayoutInflater().inflate( R.layout.element_options_publication, OptionsPublicationLayout, false );
			
			((TextView) tagLayout.findViewById(R.id.nom_element)).setText(tag);
			
			// configuration du bouton permettant de supprimer le tag de la liste de tags
			((ImageButton) tagLayout.findViewById(R.id.button_supprimer_element)).setOnClickListener(new OnClickListener () {
				@Override
				public void onClick(View buttonSupprimerTag) {
					tagsLayout.removeView( (View) buttonSupprimerTag.getParent() ); 	// suppression du tag dans le layout 
					listeTags.remove( 													// suppression du tag dans la liste des tags
						((TextView) ((View) buttonSupprimerTag.getParent()).findViewById(R.id.nom_element)).getText()
					);
				}
			});
			
			tagsLayout.addView(tagLayout, tagsLayout.getChildCount() - 1);
 		}		
		 		
	 	// bouton d'ajout de tags, l'utilisateur doit être collaborateur, animateur, expert ou admin pour pouvoir ajouter des tags
		((EditText) OptionsPublicationLayout.findViewById(R.id.nouveau_tag)).setOnEditorActionListener( new OnEditorActionListener () {
			@Override
			public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
				ajouterTag( textView.getText().toString() );
				return true;
			}
		});

 		// initialisation des cercles
 		for(Cercle cercle : listeCercles) {
 			// ajout du tag
			cercleLayout = (RelativeLayout) activityParent.getLayoutInflater().inflate( R.layout.element_options_publication, OptionsPublicationLayout, false );
			
			((TextView) cercleLayout.findViewById(R.id.nom_element)).setText(cercle.getNomCercle());
			cercleLayout.findViewById(R.id.button_supprimer_element).setTag(cercle);
			
			// configuration du bouton permettant de supprimer le cercle de la liste de cercles
			((ImageButton) cercleLayout.findViewById(R.id.button_supprimer_element)).setOnClickListener(new OnClickListener () {
				@Override
				public void onClick(View buttonSupprimerCercle) {
					cerclesLayout.removeView( (View) buttonSupprimerCercle.getParent() ); 		// suppression du cercle dans le layout 
					listeCercles.remove( (Cercle) ((View) buttonSupprimerCercle).getTag() );	// suppression du cercle dans la liste des cercles
					
					// s'il n'y a plus de cercle, on affiche le texte indiquant que le message sera publique
					if (listeCercles.isEmpty())
						cerclesLayout.findViewById(R.id.label_visibilite_public).setVisibility(View.VISIBLE);
				}
			});
			
			cerclesLayout.addView(cercleLayout, cerclesLayout.getChildCount() - 1);
 		}
 		
 		// s'il y a des cercles, on masque le texte indiquant que le message sera publique
 		if(!listeCercles.isEmpty())
 			cerclesLayout.findViewById(R.id.label_visibilite_public).setVisibility(View.GONE);

	 	// bouton d'ajout de cercles
		((Button) OptionsPublicationLayout.findViewById(R.id.ajouter_cercle)).setOnClickListener( new OnClickListener () {
			@Override
			public void onClick(View v) {
				recupererListeCercles();
			}

		});
 	
		setView(OptionsPublicationLayout);
		
		setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
	    	public void onClick(DialogInterface dialog, int id) {
	        	dialog.cancel();
	        }
	    });
	}
	
	
	// ajoute un tag au message
	private void ajouterTag(String tag) {		
		// si l'utilisateur n'est pas collaborateur, animateur, expert ou admin
		if ( idRoleUtilisateur < Constantes.ID_COLLABORATEUR ) {
			Toast.makeText(activityParent, R.string.label_fonctionnalite_indisponible_collaborateur, Toast.LENGTH_LONG).show();
		}
		// sinon, s'il y a 5 tags ou plus d'associés au message ; il ne peut pas y avoir plus de 5 tags associé à un message
		else if ( tagsLayout.getChildCount() - 1 == Constantes.NB_TAGS_MAX ) {		// -1 car il ne faut pas prendre en compte l'EditText
			Toast.makeText(activityParent, R.string.label_nb_tag_max, Toast.LENGTH_LONG).show();
		}	// sinon, si le tag n'est pas une chaine de caractères vides 
		else if( !tag.trim().isEmpty() ) {
			// ajout du nouveau tag
			tagLayout = (RelativeLayout) activityParent.getLayoutInflater().inflate( R.layout.element_options_publication, OptionsPublicationLayout, false );
			
			((TextView) tagLayout.findViewById(R.id.nom_element)).setText( tag.trim() );
			
			// configuration du bouton permettant de supprimer le tag de la liste de tags
			((ImageButton) tagLayout.findViewById(R.id.button_supprimer_element)).setOnClickListener(new OnClickListener () {
				@Override
				public void onClick(View buttonSupprimerTag) {
					tagsLayout.removeView( (View) buttonSupprimerTag.getParent() ); 	// suppression du tag dans le layout 
					listeTags.remove( 													// suppression du tag dans la liste des tags
						((TextView) ((View) buttonSupprimerTag.getParent()).findViewById(R.id.nom_element)).getText()
					);
				}
			});
			
			((EditText) OptionsPublicationLayout.findViewById(R.id.nouveau_tag)).getText().clear();
			tagsLayout.addView(tagLayout, tagsLayout.getChildCount() - 1);
			listeTags.add( ((TextView) tagLayout.findViewById(R.id.nom_element)).getText() );
		}
	}


	// récupère la liste des cercles de l'utilisateur
	private void recupererListeCercles() {
		// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
		if ( activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).contains(Constantes.CLEF_ID_UTILISATEUR) ) {
			// test la disponibilité de l'accès Internet
			if ( !Utiles.reseauDisponible( activityParent ) ) {
				Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, activityParent);
			} else { // On peut utiliser la connection Internet
				// on ouvre une fenetre contenant la liste des cercles
				// si la liste des cercles n'existe pas
				if (listeCerclesUtilisateur == null) {
					// récupération de tous les cercles de l'utilisateur
					// création d'une fenêtre de chargement
					progressDialog = new ProgressDialog( activityParent );
					progressDialog.setMessage( activityParent.getString(R.string.label_chargement) );
					progressDialog.show();

					// on bloque l'orientation de l'écran dans son orientation actuelle
					if ( activityParent.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
						activityParent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
					} else {
						activityParent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
					}
					// création d'un nouveau Thread
					Thread thread = new Thread(new Runnable() {
						public void run() {
							// Envoie d'une requete HTTP récupérant les cercles et classes de l'utilisateur
							final String stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AFFICHER_CERCLES, null, activityParent);

							// si la requete http s'est mal exécuté, stringJSON vaut null
							if ( stringJSON != null ) {
								listeCerclesUtilisateur = ParserJSON.parserCercles(stringJSON);
							}

							for (Cercle cercleCourant : listeCerclesUtilisateur) {
								listeNomCerclesUtilisateur.add( cercleCourant.getNomCercle() );
							}

							activityParent.runOnUiThread( new Runnable() {
								public void run() {
									// si la requete http s'est mal exécuté, stringJSON vaut null
									if ( stringJSON != null ) {
										// on affiche les cercles et classes
										listeCerclesAdapter = new ArrayAdapter<String>(activityParent, R.layout.element_liste_defaut, listeNomCerclesUtilisateur);
										ouvrirFenetreListeCercles();
									}
									// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
									progressDialog.dismiss();
									activityParent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);	
								}
							});
						}
					});

					thread.start();					
				} else {
					ouvrirFenetreListeCercles();
				}
			}
		} else {
			// ouverture de la fenêtre de connexion
			ConnexionDialog connexionDialog = new ConnexionDialog( activityParent, service);
			connexionDialog.setOnConnectedListener( new OnConnectedListener() {
				@Override
				public void onConnected(boolean success) {
					if (success)
						recupererListeCercles();
				}
			});
			connexionDialog.show();
			//Vous devez être connecté pour choisir un cercle 
		}
	}

	
	// ouvre une fenetre contenant la liste des cercles de l'utilisateur
	private void ouvrirFenetreListeCercles() {
		// création de la fenêtre contenant la liste des utilisateur
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activityParent);
		dialogBuilder.setAdapter(listeCerclesAdapter, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				ajouterCercle( listeCerclesUtilisateur.get(itemSelectionne) );
			}
		})
		.show();
	}
	
	
	// étend la  visibilité à un cercle
	private void ajouterCercle(Cercle cercle) {		
		// si le cercle n'a pas déjà été ajouté
		if( !listeCercles.contains(cercle) ) {
			// ajout du nouveau cercle
			cercleLayout = (RelativeLayout) activityParent.getLayoutInflater().inflate( R.layout.element_options_publication, OptionsPublicationLayout, false );
			
			((TextView) cercleLayout.findViewById(R.id.nom_element)).setText( cercle.getNomCercle() );
			
			// configuration du bouton permettant de supprimer le cercle de la liste de cercles
			((ImageButton) cercleLayout.findViewById(R.id.button_supprimer_element)).setOnClickListener(new OnClickListener () {
				@Override
				public void onClick(View buttonSupprimerCercle) {
					cerclesLayout.removeView( (View) buttonSupprimerCercle.getParent() ); 		// suppression du cercle dans le layout 
					listeCercles.remove( (Cercle) ((View) buttonSupprimerCercle).getTag() );	// suppression du cercle dans la liste des cercles
					
					// s'il n'y a plus de cercle, on affiche le texte indiquant que le message sera publique
					if (listeCercles.isEmpty())
						cerclesLayout.findViewById(R.id.label_visibilite_public).setVisibility(View.VISIBLE);
				}
			});
			// on masque le texte indiquant que le message sera publique
			cerclesLayout.findViewById(R.id.label_visibilite_public).setVisibility(View.GONE);
			
			cerclesLayout.addView(cercleLayout, cerclesLayout.getChildCount() - 1);
			listeCercles.add( cercle );
		}
	}
	
	
	public ArrayList<CharSequence> getListeTags() {
		return listeTags;
	}
	
	
	public ArrayList<Cercle> getListeCercles() {
		return listeCercles;
	}
	
	
	public boolean estDefi() {
		return switchDefi.isChecked();
	}
	
	public boolean geolocaliserMessage(){
		return geolocaliserMessageCheckedTextView.isChecked();
	}
}
