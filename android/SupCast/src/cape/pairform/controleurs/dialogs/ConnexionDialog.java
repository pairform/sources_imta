package cape.pairform.controleurs.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import cape.pairform.R;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.controleurs.services.communication.IServeurConnexion;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/*
 * Fenêtre permettant de se connecter à l'application
 */
public class ConnexionDialog extends AlertDialog.Builder implements IServeurConnexion {

	private ProgressDialog progressDialog;									// fenêtre de chargement
	private Activity activityParent;										// activity parent
	private LinearLayout connexionLayout;									// layout du contenu de la fenêtre de connexion
	private EditText pseudoView;											// pseudo de l'utilisateur, utilisé pour la connexion
	private EditText motDePasseView;										// mot de passe de l'utilisateur, utilisé pour la connexion
	private SharedPreferences fichierSharedPreferences;						// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;		// permet d'éditer les fichiers des préférences
	private OnConnectedListener onConnectedListener = null;
	//Quand au pourquoi du static allez voir la classe slidingmenuSupCast.jaa
	private static ServiceGeneral service;

	public ConnexionDialog(Activity activityParent, ServiceGeneral _service) {
		super(activityParent);
		this.activityParent = activityParent;
		service = _service;

		connexionLayout = (LinearLayout)((LayoutInflater) activityParent.getSystemService( Context.LAYOUT_INFLATER_SERVICE )).inflate(R.layout.layout_dialog_connexion, null);
		pseudoView = (EditText) connexionLayout.findViewById(R.id.pseudo_utilisateur);
		motDePasseView = (EditText) connexionLayout.findViewById(R.id.mot_de_passe);

		setTitle(R.string.button_connexion);
		setView(connexionLayout);
		configurerBoutons();
	}

	// configure les boutons de la fenêtres de connexion
	private void configurerBoutons() {
		setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				fichierSharedPreferences = activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, Context.MODE_PRIVATE);
				editorFichierSharedPreferences = fichierSharedPreferences.edit();
				editorFichierSharedPreferences.putInt(Constantes.CLEF_CHOIX_CONNEXION, Constantes.CLEF_NO_LOGGIN);
			}
		});

		// Lancer la connexion
		setPositiveButton(R.string.button_connexion, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();

				// test la disponibilité de l'accès Internet
				if ( !service.internetActif() ) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, activityParent);
				} else { // On peut utiliser la connection Internet
					// si le mot de passe ou le login n'a pas été renseigné
					if(motDePasseView.getText().toString().isEmpty() || pseudoView.getText().toString().isEmpty()) {
						Utiles.afficherInfoAlertDialog(R.string.title_erreur_identifiants, R.string.label_erreur_saisie_utilisateur_inexistant, activityParent);
					} else {
						connecterUtilisateur();
					}
				}
			}
		});

		// creer un nouveau compte PairForm
		setNeutralButton(R.string.button_creer_un_compte, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {	    		
				dialog.cancel();
				CreationCompteDialog creationCompteAlertDialog = new CreationCompteDialog(activityParent);
				AlertDialog creationCompteDialog = creationCompteAlertDialog.create();

				// On transmet le AlertDialog au AlertDialog.Buidler pour pouvoir fermer la fenetre en dehors d'un clic sur un bouton
				creationCompteAlertDialog.setDialog(creationCompteDialog);
				creationCompteDialog.show();
			}
		});
	}

	// connecte l'utilisateur à PairForm
	private void connecterUtilisateur() {
		// création d'une fenêtre de chargement
		try{
			progressDialog = new ProgressDialog(activityParent);
			progressDialog.setMessage( activityParent.getString(R.string.label_chargement) );
			progressDialog.show();
		}catch(Exception e){
			e.printStackTrace();
		}
		service.inscrireConnection(this);
		service.connecteUtilisateur(null, pseudoView.getText().toString(), motDePasseView.getText().toString());
	}

	public void setOnConnectedListener(OnConnectedListener onConnectedListener) {
		this.onConnectedListener = onConnectedListener;
	}

	// listener de la classe ConnexionDialog
	public interface OnConnectedListener {
		// la méthode se déclanche quand l'utilisateur est connecté
		public void onConnected(boolean success);
	}

	@Override
	public void enregistrement(int success) {
		boolean check =  ((CheckBox) connexionLayout.findViewById(R.id.autoLoggin)).isChecked();
		boolean connecte = (success == 0);
		fichierSharedPreferences = activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, Context.MODE_PRIVATE);
		editorFichierSharedPreferences = fichierSharedPreferences.edit();

		//Si la connection à réussie ET l'utilisateur veut se connecter automatiquement
		if(connecte && check){
			editorFichierSharedPreferences.putInt(Constantes.CLEF_CHOIX_CONNEXION, Constantes.CLEF_AUTO_LOGGIN);
			editorFichierSharedPreferences.putString(Constantes.CLEF_PASSWORD_UTILISATEUR, motDePasseView.getText().toString());
		}

		if(!check){
			editorFichierSharedPreferences.putInt(Constantes.CLEF_CHOIX_CONNEXION, Constantes.CLEF_MANUAL_LOGGIN);
		}

		editorFichierSharedPreferences.commit();
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.hide();

		service.desinscrireConnection(this);

		if(onConnectedListener != null)
			onConnectedListener.onConnected(connecte);

	}

	@Override
	public void internetConnexion(boolean activate) {}
}
