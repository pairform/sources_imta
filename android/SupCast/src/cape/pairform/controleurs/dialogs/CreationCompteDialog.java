package cape.pairform.controleurs.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.activities.EcranTableauDeBordActivity;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Fenêtre permettant de créer un nouveau compte
 */
public class CreationCompteDialog extends AlertDialog.Builder {

	private ProgressDialog progressDialog;						// fenêtre de chargement
	private Activity activityParent;							// activity parent
	private ScrollView creationCompteLayout;
	private String erreursCreationCompte = null;				// erreur retourné par le web service de création de compte
	private String stringJSON;									// contenu d'une réponse JSON d'une requête http
	private EditText pseudoUtilisateur;
	private EditText nomUtilisateur;							// nom et prénom de l'utilisateur
	private EditText emailUtilisateur;
	private EditText motDePasseUtilisateur;
	private EditText motDePasseConfirmationUtilisateur;
	private EditText etablissementUtilisateur;
	private TextView labelLanguePrincipale;
	private Button buttonChoixAutresLangues;
	private Spinner listeLangues;
	private int idLangueSelectionnee;							// l'indice de la langue principale selectionnee
	private SharedPreferences fichierSharedPreferences;			// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;	// permet d'éditer les fichiers des préférences
	private ArrayList<String> listeIdAutresLangues;				// Liste d'ID des autres langues
	private ArrayList<String> listeNomsAutresLangues;			// Liste de noms des autres langues
	private boolean[] itemsSelectionnes;						// Les items selectionnés pour les autres langues
	private JSONArray listeAutresLangues;						// Liste des autres langues sur le format JSON
	private String id_utilisateur;								// id de l'utilisateur apres création de compte
	private Locale locale;										// Gére le language de l'application
	private Configuration config;								// Gere la configuration de l'application
	private String codeLangueAppActuelle;						// Code de la langue de l'application actuelle
	private Intent intent;										// Intent pour lancer une Activité
	private String[] sauvegardeChamps;							// Tableau de strings permettant de sauvegarder les champs si changement de langue
	private AlertDialog creationCompteDialog;


	public CreationCompteDialog(Activity activityParent) {
		super(activityParent);
		initialisation(activityParent);
	}

	public CreationCompteDialog(Activity activityParent, String[] sauvegarde) {
		super(activityParent);
		initialisation(activityParent);

		// Recuperation des Champs sauvegardés
		nomUtilisateur.setText(sauvegarde[0]);
		pseudoUtilisateur.setText(sauvegarde[1]);
		etablissementUtilisateur.setText(sauvegarde[2]);
		emailUtilisateur.setText(sauvegarde[3]);
		motDePasseUtilisateur.setText(sauvegarde[4]);
		motDePasseConfirmationUtilisateur.setText(sauvegarde[5]);
	}

	private void initialisation(Activity activityParent) {
		this.activityParent = activityParent;

		creationCompteLayout = (ScrollView)((LayoutInflater) activityParent.getSystemService( Context.LAYOUT_INFLATER_SERVICE )).inflate(R.layout.layout_dialog_creation_compte, null);

		setView(creationCompteLayout);

		pseudoUtilisateur = (EditText) creationCompteLayout.findViewById(R.id.pseudo_utilisateur);
		nomUtilisateur = (EditText) creationCompteLayout.findViewById(R.id.nom_utilisateur);
		emailUtilisateur = (EditText) creationCompteLayout.findViewById(R.id.email_utilisateur);
		motDePasseUtilisateur = (EditText) creationCompteLayout.findViewById(R.id.mot_de_passe);
		motDePasseConfirmationUtilisateur = (EditText) creationCompteLayout.findViewById(R.id.confirmation_mot_de_passe);
		etablissementUtilisateur = (EditText) creationCompteLayout.findViewById(R.id.etablissement_utilisateur);
		labelLanguePrincipale = (TextView) creationCompteLayout.findViewById(R.id.label_langue_principale);
		buttonChoixAutresLangues = (Button) creationCompteLayout.findViewById(R.id.button_choix_autres_langues);

		// maj des textes
		setContent();

		creerListeLangues();
		creerBoutons();

		// Action du bouton pour modifier les autres langues de l'utilisateur
		buttonChoixAutresLangues.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View view) {
				afficherAutresLangues();
			}
		});
	}


	// maj des textes
	private void setContent() {
		this.setTitle(activityParent.getString(R.string.title_nouveau_compte));
		this.setPositiveButton(activityParent.getString(R.string.button_creer_le_compte), null);
		this.setNegativeButton(activityParent.getString(R.string.button_annuler), null);

		pseudoUtilisateur.setHint( pseudoUtilisateur.getHint() + " * (4+)" );
		nomUtilisateur.setHint( nomUtilisateur.getHint() + " *" );		
		emailUtilisateur.setHint( emailUtilisateur.getHint() + " *" );
		motDePasseUtilisateur.setHint( motDePasseUtilisateur.getHint() + " * (6+)" );
		motDePasseConfirmationUtilisateur.setHint( motDePasseConfirmationUtilisateur.getHint() + " *" );

		labelLanguePrincipale.setText(labelLanguePrincipale.getText() + " :");
		buttonChoixAutresLangues.setText(buttonChoixAutresLangues.getText() + "...");
	}


	// affiche les autres langues
	private void afficherAutresLangues() {
		AlertDialog.Builder builder = new AlertDialog.Builder(activityParent);

		builder.setTitle(activityParent.getString(R.string.title_autres_langues) + " :")
		.setMultiChoiceItems(listeNomsAutresLangues.toArray(new CharSequence[listeNomsAutresLangues.size()]), itemsSelectionnes, new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if (isChecked) {
					itemsSelectionnes[which] = true;
				} else {
					itemsSelectionnes[which] = false;
				}
			}
		})
		.setPositiveButton(activityParent.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		})
		.setNegativeButton(activityParent.getString(R.string.button_annuler), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		})
		.show();
	}


	//
	private void afficherDialogChangementLangueApp() {
		// on sauvegarde la langue de l'application actuelle
		codeLangueAppActuelle = activityParent.getString(R.string.locale_language);  
		// on modifie la langue de l'application
		editerLangueApp(Utiles.codeLangueAvecId(String.valueOf(idLangueSelectionnee), activityParent));

		AlertDialog.Builder builder = new AlertDialog.Builder(activityParent);

		builder.setTitle(activityParent.getString(R.string.title_langue_application))
		.setMessage(R.string.label_changer_langue_app)
		.setPositiveButton(R.string.button_oui, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				// on met à jour la langue de l'utilisateur dans le fichier des préférences
				editorFichierSharedPreferences.putString(Constantes.CLEF_LANGUE_APPLICATION, Utiles.codeLangueAvecId(String.valueOf(idLangueSelectionnee), activityParent));
				editorFichierSharedPreferences.commit();

				// on sauvegarde les champs
				sauvegardeChamps = new String[6];
				sauvegardeChamps[0] = nomUtilisateur.getText().toString();
				sauvegardeChamps[1] = pseudoUtilisateur.getText().toString();
				sauvegardeChamps[2] = etablissementUtilisateur.getText().toString();
				sauvegardeChamps[3] = emailUtilisateur.getText().toString();
				sauvegardeChamps[4] = motDePasseUtilisateur.getText().toString();
				sauvegardeChamps[5] = motDePasseConfirmationUtilisateur.getText().toString();

				Toast.makeText(activityParent, R.string.label_modification_langue_application_reussi, Toast.LENGTH_LONG).show();

				// on relance l'activité en vidant la pile et en y mettant l'ecran d'accueil
				intent = new Intent(activityParent, EcranTableauDeBordActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.putExtra("createAccount", true);
				intent.putExtra("sauvegardeChamps", sauvegardeChamps);

				creationCompteDialog.cancel();
				activityParent.startActivity(intent);
			}
		})
		.setNegativeButton(R.string.button_non, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				editerLangueApp(codeLangueAppActuelle);
			}
		})
		.show();
	}


	private void editerLangueApp(String codeLangue) {
		// on modifie la langue de l'application
		locale = new Locale(codeLangue); 
		Locale.setDefault(locale);
		config = new Configuration();
		config.locale = locale;
		activityParent.getApplicationContext().getResources().updateConfiguration(config, null);
	}

	private void creerListeLangues() {
		listeLangues = (Spinner) creationCompteLayout.findViewById(R.id.langue_principale_spinner);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activityParent, R.array.langue_true_values_string, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		listeLangues.setAdapter(adapter);

		// Recuperation du fichier des preferences
		fichierSharedPreferences = activityParent.getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, 0);
		editorFichierSharedPreferences = fichierSharedPreferences.edit();
		String languePrefApplication = fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_APPLICATION, "en");

		// Permet de selectionner la langue du telephone (anglais par défaut)
		listeLangues.setSelection(Utiles.tabIndexLangueAvecCode(languePrefApplication, activityParent));
		idLangueSelectionnee = Integer.parseInt(Utiles.idLangueAvecCode(languePrefApplication, activityParent));

		// Initialisation des paramètres utiles aux langues
		listeIdAutresLangues = new ArrayList<String>(Arrays.asList(activityParent.getResources().getStringArray(R.array.langue_values_id)));
		listeNomsAutresLangues = new ArrayList<String>(Arrays.asList(activityParent.getResources().getStringArray(R.array.langue_true_values_string)));
		listeIdAutresLangues.remove(String.valueOf(idLangueSelectionnee));
		listeNomsAutresLangues.remove(Utiles.nomVraiLangueAvecId(String.valueOf(idLangueSelectionnee), activityParent));
		itemsSelectionnes = new boolean[listeIdAutresLangues.size()];
		for(int i=0; i<listeIdAutresLangues.size(); i++) {
			itemsSelectionnes[i] = false;
		}

		// Action lors de la selection d'une langue
		listeLangues.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				if(idLangueSelectionnee != Utiles.idLangueAvecTabIndex(position, activityParent)) {
					listeIdAutresLangues.add(String.valueOf(idLangueSelectionnee));
					listeNomsAutresLangues.add(Utiles.nomVraiLangueAvecId(String.valueOf(idLangueSelectionnee), activityParent));
					idLangueSelectionnee = Utiles.idLangueAvecTabIndex(position, activityParent);
					listeIdAutresLangues.remove(String.valueOf(idLangueSelectionnee));
					listeNomsAutresLangues.remove(Utiles.nomVraiLangueAvecId(String.valueOf(idLangueSelectionnee), activityParent));
					for(int i=0; i<listeIdAutresLangues.size(); i++) {
						itemsSelectionnes[i] = false;
					}
					afficherDialogChangementLangueApp();
				}
			}
			public void onNothingSelected(AdapterView<?> parentView) {}
		});
	}


	private void creerBoutons() {
		setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		setPositiveButton(R.string.button_creer_le_compte, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				// ouvrir la fenêtre affichant les CGU pour validation
				validerCGU();
			}
		});
	}


	// ouvre la fenêtre affichant les CGU pour validation
	private void validerCGU() {
		AlertDialog.Builder builder = new AlertDialog.Builder(activityParent);

		WebView cgu = new WebView( activityParent );
		cgu.loadUrl(Constantes.URL_FICHIER_CGU +"/"+ activityParent.getString(R.string.locale_language));

		builder.setTitle(R.string.button_creer_un_compte)
		.setView(cgu)
		.setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		})
		.setPositiveButton(R.string.button_accepter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();

				// test la disponibilité de l'accès Internet
				if ( !Utiles.reseauDisponible(activityParent) ) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, activityParent);
				} else { // On peut utiliser la connection Internet
					creerCompte();	
				}
			}
		})
		.show();
	}


	private void creerCompte() {
		// création d'une fenêtre de chargement
		progressDialog = new ProgressDialog(activityParent);
		progressDialog.setMessage( activityParent.getString(R.string.label_chargement) );
		progressDialog.show();

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(											// pseudo
						Constantes.CLEF_PSEUDO_UTILISATEUR, 
						pseudoUtilisateur.getText().toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// nom et prénom
						Constantes.CLEF_NOM_UTILISATEUR, 
						nomUtilisateur.getText().toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// établissement de l'utilisateur
						Constantes.CLEF_ETABLISSEMENT_UTILISATEUR, 
						((EditText) creationCompteLayout.findViewById(R.id.etablissement_utilisateur) ).getText().toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// password encoder
						Constantes.CLEF_MOT_DE_PASSE_UTILISATEUR, 
						motDePasseUtilisateur.getText().toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// confirmation password
						Constantes.CLEF_CONFIRMATION_MOT_DE_PASSE_UTILISATEUR, 
						motDePasseConfirmationUtilisateur.getText().toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// email
						Constantes.CLEF_EMAIL_UTILISATEUR, 
						emailUtilisateur.getText().toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// langue
						Constantes.CLEF_ID_LANGUE_UTILISATEUR, 
						String.valueOf(idLangueSelectionnee)
					));
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP pour créer un nouveau compte utilisateur 
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_CREER_COMPTE, parametresRequete, activityParent);

				// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas stringJSON
				if ( stringJSON != null ) {
					erreursCreationCompte = ParserJSON.parserRetourCreationCompte( stringJSON );
				}

				activityParent.runOnUiThread(new Runnable() {
	    			public void run() {
						progressDialog.dismiss();	    				
		
						// si la requete http s'est mal exécuté, stringJSON vaut null
						if ( stringJSON == null ) {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
		
						} else if ( erreursCreationCompte != null ) {	    	        			    	        		
							if ( erreursCreationCompte.equals(Constantes.CLEF_ERREUR_WEBSERVICE_PSEUDO_UTILISE) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_pseudo_indisponible, R.string.label_erreur_pseudo_indisponible, activityParent);
		
							} else if ( erreursCreationCompte.equals(Constantes.CLEF_ERREUR_WEBSERVICE_CHAMP_VIDE) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_saisie, R.string.label_erreur_saisie_champ_vide, activityParent);
		
							} else if ( erreursCreationCompte.equals(Constantes.CLEF_ERREUR_WEBSERVICE_MOT_DE_PASSE_CONFIRMATION) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_saisie, R.string.label_erreur_saisie_mot_de_passe_confirmation, activityParent);
		
							} else if ( erreursCreationCompte.equals(Constantes.CLEF_ERREUR_WEBSERVICE_EMAIL_UTILISE) ) {
								Utiles.afficherInfoAlertDialog(R.string.title_email_indisponible, R.string.label_erreur_email_indisponible, activityParent);
		
							} else {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_saisie, R.string.title_erreur_saisie, activityParent);
		
							}
						} else {
							Toast.makeText(activityParent, R.string.label_creation_de_compte_reussi, Toast.LENGTH_LONG).show();
							id_utilisateur = ParserJSON.parserIdUtilisateurRetourCreationCompte(stringJSON);
							sauvegarderLanguesUtilisateur();
						}
	    			}
				});
			}
		});

		thread.start();
	}


	// Sauvegarder les langues de l'utilisateur
	private void sauvegarderLanguesUtilisateur() {

		// Recuperation des autres langues de l'utilisateur
		ArrayList<String> tmpListeIdAutresLangues = new ArrayList<String>();
		for(int i=0; i< listeIdAutresLangues.size(); i++) {
			if(itemsSelectionnes[i]) {
				tmpListeIdAutresLangues.add(listeIdAutresLangues.get(i));
			}
		}
		listeAutresLangues = new JSONArray(tmpListeIdAutresLangues);

		// création d'un nouveau Thread
		Thread thread = new Thread(new Runnable() {
			public void run() {
				// création et configuration des paramètres d'une requête http
				ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

				try {
					parametresRequete.add( new BasicNameValuePair(											// id de la langue principale de l'utilisateur
						Constantes.CLEF_LANGUE_PRINCIPALE,
						String.valueOf(idLangueSelectionnee)
					));
					parametresRequete.add( new BasicNameValuePair(											// liste des autres langues de l'utilisateur
						Constantes.CLEF_AUTRES_LANGUES,
						listeAutresLangues.toString()
					));
					parametresRequete.add( new BasicNameValuePair(											// id de l'utilisateur
						Constantes.CLEF_ID_UTILISATEUR, 
						id_utilisateur
					));
				} catch (Exception e) {
					e.printStackTrace();        	
				}
				// Envoie d'une requete HTTP pour modifier le compte utilisateur 
				stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_EDITER_LANGUES, parametresRequete, activityParent);
			}
		});

		thread.start();
	}


	// On transmet le AlertDialog au AlertDialog.Buidler pour pouvoir fermer la fenetre en dehors d'un clic sur un bouton
	public void setDialog(AlertDialog creationCompteDialog) {
		this.creationCompteDialog = creationCompteDialog;
	}
}
