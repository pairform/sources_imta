package cape.pairform.controleurs;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.activities.EcranAProposActivity;
import cape.pairform.controleurs.activities.EcranAideActivity;
import cape.pairform.controleurs.activities.EcranAjoutRessourceActivity;
import cape.pairform.controleurs.activities.EcranPreferencesActivity;
import cape.pairform.controleurs.activities.EcranProfilActivity;
import cape.pairform.controleurs.activities.EcranRechercheUtilisateurActivity;
import cape.pairform.controleurs.activities.EcranReglagesActivity;
import cape.pairform.controleurs.activities.EcranTableauDeBordActivity;
import cape.pairform.controleurs.activities.EcranVueTransversaleActivity;
import cape.pairform.controleurs.adapters.ListWithCompoundDrawableAdapter;
import cape.pairform.controleurs.dialogs.ConnexionDialog;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.controleurs.services.DeconnecteurService;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.modele.Constantes;
import cape.pairform.utiles.Utiles;


/* 
 * menu déroulant affichant une liste de boutons permettant de naviguer dans l'application
 * pour plus d'informations sur la library SlidingMenu : https://github.com/jfeinstein10/SlidingMenu
 */
public class SlidingMenuSupCast {

	// tableaux contenant les ressources-id des textes contenu dans les parties "Ressources", "Messages", "utilisateurs" et "Autres"
	private static final int[] CODE_TEXTE_LISTE_RESOURCES = {R.string.button_grille_ressources, R.string.button_ajouter_ressource};
	private int[] code_texte_liste_utilisateur;
	private static final int[] CODE_TEXTE_LISTE_MESSAGES = {R.string.button_tout_les_messages};
	private static final int[] CODE_TEXTE_LISTE_AUTRES = {R.string.button_preferences,R.string.button_aide, R.string.button_a_propos};
	// tableaux contenant les ressources-id des icones contenu dans les parties "Ressources", "utilisateurs", "Messages" et "Autres"
	private static final int[] CODE_ICONE_LISTE_RESSOURCES = {R.drawable.icone_grille_ressources, R.drawable.icone_ajouter_ressource};
	private static final int[] CODE_ICONE_LISTE_UTILISATEURS = {R.drawable.icone_profil, R.drawable.icone_chercher, R.drawable.icone_connexion};
	private static final int[] CODE_ICONE_LISTE_MESSAGES = {R.drawable.icone_tout_les_messages};
	private static final int[] CODE_ICONE_LISTE_AUTRES = {R.drawable.icone_preferences, R.drawable.icone_aide, R.drawable.icone_infos};

	private SlidingMenu slidingMenu;						// le menu déroulant en lui-même (provient de la library SlidingMenu : https://github.com/jfeinstein10/SlidingMenu)
	private ListView listeElements;							// ListView contenant la liste des boutons du menu déroulant
	private ListWithCompoundDrawableAdapter adapter;		// adapter personnalisé utilisé avec listElements
	private TextView headerView;							// titre des listView
	private LinearLayout slidingMenuLayout;					// layout du menu déroulant
	private OnItemClickListener listViewListener;			// listener associé aux listeView
	private Activity activityParent;						// activity parent à qui appartient le menu déroulant
	private Intent intentCourante;							// intent courante
	private ConnexionDialog connexionDialog;				// fenêtre de connexion
	private SharedPreferences fichierSharedPreferences;					// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;	// permet d'éditer les fichiers des préférences
	//Important d'utiliser static.
	//Permet d'utiliser moins d'espace mémoire
	//Le service est unique, il n'y a 'une instance quoi qu'il arrive qui fonctionne
	//Donc le mettre static permet de n'avoir, pour chaque objet menu qu'une seul référence vers le service :-)
	private static ServiceGeneral service;

	public SlidingMenuSupCast(Activity activity) {
		this.activityParent = activity;
		/*
		 *  création et configuration du menu déroulant
		 */

		// récupération du layout du menu déroulant
		slidingMenuLayout = new LinearLayout(activityParent);
		activityParent.getLayoutInflater().inflate(R.layout.layout_sliding_menu, slidingMenuLayout);

		slidingMenu = new SlidingMenu(activityParent);
		slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);				// un swipe sur le bord de l'écran permet d'affcher le menu déroulant
		slidingMenu.setBehindOffsetRes(R.dimen.largeur_restante_ecran);				// choix de la dimension d'écran (de l'activity à laquelle est attaché le menu déroulant) restante visible
		slidingMenu.setMode(SlidingMenu.RIGHT);        								// le menu déroulant apparait sur le bord droit de l'écran
		slidingMenu.setFadeDegree(0.35f);											// choix de l'ombrage s'affichant au dessus du menu déroulant pendant son animation d'apparition à l'écran
		slidingMenu.attachToActivity(activityParent, SlidingMenu.SLIDING_WINDOW); 	// toute l'activity "glisse" sur le côté, action bar compris

		slidingMenu.setMenu(slidingMenuLayout);

		
	}

	//Méthode permettant d'afficher le menu
	public void showMenu(ServiceGeneral service){
		this.service = service;
		
		/*
		 * création et configuration des listes contenus dans le menu déroulant
		 */

		// création du listener associé au liste
		listViewListener = new OnItemClickListener() {
			// déclenche une action lors d'un clic sur un élément d'une liste
			public void onItemClick(AdapterView<?> parent, View elementListe, int position, long id) {            	
				declencherActionSurClic( elementListe );
			}
		};

		// création et configuration de la list des boutons de la partie "Ressources"
		adapter = new ListWithCompoundDrawableAdapter(
			activityParent,
			CODE_TEXTE_LISTE_RESOURCES,
			CODE_ICONE_LISTE_RESSOURCES,
			R.layout.element_liste_menu,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);
		headerView = (TextView) activityParent.getLayoutInflater().inflate(R.layout.titre_sliding_menu, null);
		headerView.setText(R.string.title_ressources);
		headerView.setClickable(true);

		listeElements = (ListView) slidingMenuLayout.findViewById(R.id.liste_ressources);
		listeElements.addHeaderView(headerView);
		listeElements.setOnItemClickListener(listViewListener);
		listeElements.setAdapter(adapter);

		// création et configuration de la list des boutons de la partie "Utilisateur"
		// on récupère le fichier de préfèrences stockant les infos sur l'utilisateur connecté
		fichierSharedPreferences = activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);

		// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
		if ( service.utilisateurEnregistre() ) {
			code_texte_liste_utilisateur = new int[] {R.string.button_profil, R.string.button_chercher_utilisateur, R.string.button_deconnexion};
		} else {
			code_texte_liste_utilisateur = new int[] {R.string.button_profil, R.string.button_chercher_utilisateur, R.string.button_connexion};
		}

		adapter = new ListWithCompoundDrawableAdapter (
			activityParent,
			code_texte_liste_utilisateur,
			CODE_ICONE_LISTE_UTILISATEURS,
			R.layout.element_liste_menu,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);
		headerView = (TextView) activityParent.getLayoutInflater().inflate(R.layout.titre_sliding_menu, null);
		headerView.setText(R.string.title_utilisateurs);
		headerView.setClickable(true);

		listeElements = (ListView) slidingMenuLayout.findViewById(R.id.liste_profils);
		listeElements.addHeaderView(headerView);
		listeElements.setOnItemClickListener(listViewListener);
		listeElements.setAdapter(adapter);

		// création et configuration de la list des boutons de la partie "Messages"
		adapter = new ListWithCompoundDrawableAdapter(
			activityParent,
			CODE_TEXTE_LISTE_MESSAGES,
			CODE_ICONE_LISTE_MESSAGES,
			R.layout.element_liste_menu,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);
		headerView = (TextView) activityParent.getLayoutInflater().inflate(R.layout.titre_sliding_menu, null);
		headerView.setText(R.string.title_messages);
		headerView.setClickable(true);

		listeElements = (ListView) slidingMenuLayout.findViewById(R.id.liste_messages);
		listeElements.addHeaderView(headerView);
		listeElements.setOnItemClickListener(listViewListener);
		listeElements.setAdapter(adapter);

		// création et configuration de la list des boutons de la partie "Outils"
		adapter = new ListWithCompoundDrawableAdapter(
			activityParent,
			CODE_TEXTE_LISTE_AUTRES,
			CODE_ICONE_LISTE_AUTRES,
			R.layout.element_liste_menu,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);
		headerView = (TextView) activityParent.getLayoutInflater().inflate(R.layout.titre_sliding_menu, null);
		headerView.setText(R.string.title_autres);
		headerView.setClickable(true);

		listeElements = (ListView) slidingMenuLayout.findViewById(R.id.liste_autres);
		listeElements.addHeaderView(headerView);
		listeElements.setOnItemClickListener(listViewListener);
		listeElements.setAdapter(adapter);	
	}
	
	// déclenche l'ouverture du menu déroulant si celui-ci est "fermé" (et vice versa s'il est "ouvert")
	public void toggle() {
		slidingMenu.toggle();
	}

	// modifie l'élément connexion
	public void modifierElementConnexion() {
		code_texte_liste_utilisateur = new int[] {R.string.button_profil, R.string.button_chercher_utilisateur, R.string.button_deconnexion};

		adapter = new ListWithCompoundDrawableAdapter (
			activityParent,
			code_texte_liste_utilisateur,
			CODE_ICONE_LISTE_UTILISATEURS,
			R.layout.element_liste_menu,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);

		listeElements = (ListView) slidingMenuLayout.findViewById(R.id.liste_profils);
		listeElements.setAdapter(adapter);
	}

	// modifie l'élément deconnexion
	public void modifierElementDeconnexion() {
		code_texte_liste_utilisateur = new int[] {R.string.button_profil, R.string.button_chercher_utilisateur, R.string.button_connexion};

		adapter = new ListWithCompoundDrawableAdapter (
			activityParent,
			code_texte_liste_utilisateur,
			CODE_ICONE_LISTE_UTILISATEURS,
			R.layout.element_liste_menu,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);

		listeElements = (ListView) slidingMenuLayout.findViewById(R.id.liste_profils);
		listeElements.setAdapter(adapter);
	}


	// déclenche une action lors d'un clic sur un élément du slidingMenu
	private void declencherActionSurClic (View elementListe) {
		switch ( elementListe.getId() ) {
		case R.string.button_grille_ressources:
			// lancement de l'écran affichant les ressources téléchargéess par l'utilisateur
			activityParent.startActivity( new Intent(activityParent, EcranTableauDeBordActivity.class) );
			break;
		case R.string.button_ajouter_ressource:
			// test la disponibilité de l'accès Internet
			if ( !service.internetActif() ) {
				Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, activityParent);
			} else { // On peut utiliser la connection Internet
				// lancement de l'écran d'ajout de nouvelles ressources
				activityParent.startActivity( new Intent(activityParent, EcranAjoutRessourceActivity.class) );
			}
			break;
		case R.string.button_profil:
			// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
			if ( service.estLieAuServeur() ) {
				// test la disponibilité de l'accès Internet
				if ( !service.internetActif()) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, activityParent);
				} else { // On peut utiliser la connection Internet
					// lancement de l'écran affichant le profil de l'utilisateur
					intentCourante = new Intent(activityParent, EcranProfilActivity.class);
					intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null));
					activityParent.startActivity(intentCourante);
				}
			} else {
				//TODO: vérif fonctionnelle
				// ouverture de la fenêtre de connexion
				connexionDialog = new ConnexionDialog(activityParent, service);
				connexionDialog.setOnConnectedListener( new OnConnectedListener() {
					@Override
					public void onConnected(boolean success) {
						// lancement de l'écran affichant le profil de l'utilisateur
						if(success){
							intentCourante = new Intent(activityParent, EcranProfilActivity.class);
							intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null));
							activityParent.startActivity(intentCourante);
						}
					}
				});
				connexionDialog.show();
			}
			break;
		case R.string.button_chercher_utilisateur:
			// test la disponibilité de l'accès Internet
			if ( !service.internetActif() ) {
				Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, activityParent);
			} else { // On peut utiliser la connection Internet
				// lancement de l'ecran de recherche d'un utilisateur
				activityParent.startActivity( new Intent(activityParent, EcranRechercheUtilisateurActivity.class) );
			}
			break;
		case R.string.button_connexion:
			toggle();

			//TODO: vérif fonctionnelle

			// ouverture de la fenêtre de connexion
			connexionDialog = new ConnexionDialog(activityParent, service);
			connexionDialog.setOnConnectedListener( new OnConnectedListener() {
				@Override
				public void onConnected(boolean success) {
					// l'utilisateur est connecté, on modifie l'élément connexion
					if(success)
						modifierElementConnexion();
				}			    		
			});
			connexionDialog.show();
			break;
		case R.string.button_deconnexion:
			// deconnexion de l'utilisateur, suppression de ses informations dans le fichier des préférences
			editorFichierSharedPreferences = fichierSharedPreferences.edit();
			editorFichierSharedPreferences.clear();
			editorFichierSharedPreferences.putInt(Constantes.CLEF_CHOIX_CONNEXION, Constantes.CLEF_MANUAL_LOGGIN);
			editorFichierSharedPreferences.commit();
			
			intentCourante = new Intent(activityParent, DeconnecteurService.class);
			activityParent.startService(intentCourante);

			service.deconnectUtilisateur();
			
			// lancement de l'écran affichant les ressources téléchargéess par l'utilisateur
			activityParent.startActivity( new Intent(activityParent, EcranTableauDeBordActivity.class) );
			Toast.makeText(activityParent, R.string.label_deconnexion_reussi, Toast.LENGTH_LONG).show();
			break;
		case R.string.button_tout_les_messages:
			// lancement de l'ecran de lecture de messages en vue transversal
			activityParent.startActivity( new Intent(activityParent, EcranVueTransversaleActivity.class) );
			break;
		case R.string.button_reglages:
			// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
			if ( service.estLieAuServeur() ) {
				// lancement de l'écran de réglages
				activityParent.startActivity( new Intent(activityParent, EcranReglagesActivity.class) );
			} else {
				// ouverture de la fenêtre de connexion
				connexionDialog = new ConnexionDialog(activityParent, service);
				connexionDialog.setOnConnectedListener( new OnConnectedListener() {
					@Override
					public void onConnected(boolean success) {
						// lancement de l'écran de réglages
						if(success)
							activityParent.startActivity( new Intent(activityParent, EcranReglagesActivity.class) );
					}			    		
				});
				connexionDialog.show();
			}
			break;
		case R.string.button_preferences:
			// lancement de l'ecran des preferences
			activityParent.startActivity( new Intent(activityParent, EcranPreferencesActivity.class) );
			break;
		case R.string.button_aide:
			// lancement de l'ecran d'aide
			activityParent.startActivity( new Intent(activityParent, EcranAideActivity.class) );
			break;
		case R.string.button_a_propos:
			// lancement de l'ecran d'information
			activityParent.startActivity( new Intent(activityParent, EcranAProposActivity.class) );
			break;
		}
	}
}
