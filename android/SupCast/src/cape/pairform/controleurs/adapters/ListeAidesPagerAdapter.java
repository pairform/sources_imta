package cape.pairform.controleurs.adapters;

import cape.pairform.R;
import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


/*
 * Adapter personnalisé permettant l'affichage horizontal d'une liste de fenêtre d'aide
 * On peut passer d'une aide à une autre par un swipe
 */
public class ListeAidesPagerAdapter extends PagerAdapter {

	private int[] listeImagesAides;							// liste des images des aides contextuelles affichées à l'utilisateur
	private int[] listeTextesAides;							// liste des images des aides contextuelles affichées à l'utilisateur
	private ViewPager viewPager;							// contient la liste des pages, permet le parcoure de la liste avec des swipes
	private LinearLayout aide;								// une page de la ressource sous forme de WebView
	private Activity activityParent;
	
	
	public ListeAidesPagerAdapter(Activity activityParent, int[] listeImagesAides, int[] listeTextesAides, ViewPager viewPager) {
		this.activityParent = activityParent;
		this.listeImagesAides = listeImagesAides;
		this.listeTextesAides = listeTextesAides;
		this.viewPager = viewPager;
	}
	
	
	@Override
	public void destroyItem (ViewGroup container, int position, Object aideLayout) {
		container.removeView((LinearLayout) aideLayout);
	}
	
	
	@Override
	public int getCount() {		
		return listeImagesAides.length;
	}

	
	@Override
	public Object instantiateItem (ViewGroup container, int position) {
		// création d'une nouvelle aide chargé
		aide = (LinearLayout) activityParent.getLayoutInflater().inflate( R.layout.aide, viewPager, false );
		
		((ImageView) aide.findViewById(R.id.image_aide)).setImageResource( listeImagesAides[position] );				// image de l'aide
		((TextView) aide.findViewById(R.id.description_aide)).setText( listeTextesAides[position] );					// texte de l'aide
		((TextView) aide.findViewById(R.id.position_aide)).setText(++position + "/" + listeImagesAides.length);			// numéro de l'aide
		
		((Button) aide.findViewById(R.id.button_fermer)).setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View buttonFermer) {
				// on revient à l'écran précédent
				activityParent.onBackPressed();
			}			
		});
		
		container.addView( aide );
		
		return aide;
	}
	
	
	@Override
	public boolean isViewFromObject(View view, Object aideLayout) {
		return view == ((LinearLayout) aideLayout);
	}
}
