package cape.pairform.controleurs.adapters;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import cape.pairform.utiles.Utiles;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


/*
 * Adapter personnalisé permettant l'affichage d'une liste (ou grille) de TextView possédant un CompoundDrawable en tant qu'icône
 */
public class ListWithCompoundDrawableAdapter extends ArrayAdapter<String> {
	public static final int COMPOUND_DRAWABLE_DROITE = 1;		// le CompoundDrawable est à droite du texte
	public static final int COMPOUND_DRAWABLE_GAUCHE = 2;		// le CompoundDrawable est à gauche du texte
	public static final int COMPOUND_DRAWABLE_HAUT = 3;			// le CompoundDrawable est au dessus du texte
	public static final int COMPOUND_DRAWABLE_BAS = 4;			// le CompoundDrawable est en dessous du texte
	
	private final Activity activityParent;							// activityParent de l'activity parent
	private int[] codeTexteElements;								// tableaux contenant les ressources-id des textes
	private int[] codeCompoundDrawableElements;						// tableaux contenant les ressources-id des CompoundDrawables
	private ArrayList<String> texteElements;						// tableaux contenant les textes
	private ArrayList<BitmapDrawable> compoundDrawableElements;		// tableaux contenant les CompoundDrawables sous forme de BitmapDrawable
	private ArrayList<String> urlCompoundDrawableElements;			// tableaux contenant les CompoundDrawables sous forme d'URL web
	private ArrayList<String> listeIdLangues;						// tableaux contenant une liste d'id langue associée aux CompoundDrawables ; utilisé si l'icone doit contenir le drapeau d'une langue
	private LayoutInflater inflater;
	private int layoutElement;										// le layout du TextView
	private TextView elementListe;									// element de la liste (ou grille) possédant un CompoundDrawable
	private HashMap<String,TextView> listeElementListe;				// map des elements de la liste (ou grille) possédant un CompoundDrawable
	private int compoundDrawablePosition;							// position du CompoundDrawable (droite, gauche, haut, bas)
	private boolean textSousFormeDeCode = false;					// true si le texte du TextView est transmis via sa ressources-id
	private boolean textSousFormeDeString = false;					// true si le texte du TextView est transmis directement sous forme de texte (String)
	private boolean compoundDrawableSousFormeDeCode = false;		// true si le CompoundDrawable du TextView est transmis via sa ressources-id
	private boolean compoundDrawableSousFormeDeBitmapDrawable = false;	// true si le CompoundDrawable du TextView est transmis via un BitmapDrawable
	private boolean compoundDrawableSousFormeDUrl = false;			// true si le CompoundDrawable du TextView est transmis via une URL web
	private TransformateurIconeTask transformateurIconeTask;

	
	// constructeur utilisant des ressources-id pour les textes et les CompoundDrawable
	public ListWithCompoundDrawableAdapter(Activity activityParent, int[] codeTexteElements, int[] codeCompoundDrawableElements, int layoutElement, int compoundDrawablePosition) {
		super(activityParent, layoutElement, new String[codeTexteElements.length]);
		this.activityParent = activityParent;
		this.codeTexteElements = codeTexteElements;
		this.codeCompoundDrawableElements = codeCompoundDrawableElements;
		this.layoutElement = layoutElement;
		this.compoundDrawablePosition = compoundDrawablePosition;
		
		this.textSousFormeDeCode = true;
		this.compoundDrawableSousFormeDeCode = true;
	}
	
	// constructeur utilisant des String pour les textes et des BitmapDrawable pour les CompoundDrawable
	public ListWithCompoundDrawableAdapter(Activity activityParent, ArrayList<String> texteElements, ArrayList<BitmapDrawable> compoundDrawableElements, int layoutElement, int compoundDrawablePosition) {
		super(activityParent, layoutElement, texteElements);
		this.activityParent = activityParent;
		this.texteElements = texteElements;
		this.compoundDrawableElements = compoundDrawableElements;
		this.layoutElement = layoutElement;
		this.compoundDrawablePosition = compoundDrawablePosition;
		
		this.textSousFormeDeString = true;
		this.compoundDrawableSousFormeDeBitmapDrawable = true;
	}
	
	// constructeur utilisant des String pour les textes et des URLs web pour les CompoundDrawable - fonctionne spécifiquement avec les logos des ressources
	public ListWithCompoundDrawableAdapter(Activity activityParent, ArrayList<String> texteElements, ArrayList<String> urlCompoundDrawableElements, int layoutElement, int compoundDrawablePosition, ArrayList<String> listeIdLangues) {
		super(activityParent, layoutElement, texteElements);
		this.activityParent = activityParent;
		this.texteElements = texteElements;
		this.urlCompoundDrawableElements = urlCompoundDrawableElements;
		this.layoutElement = layoutElement;
		this.compoundDrawablePosition = compoundDrawablePosition;
		this.listeIdLangues = listeIdLangues;
		
		this.textSousFormeDeString = true;
		this.compoundDrawableSousFormeDUrl = true;
		
		listeElementListe = new HashMap<String,TextView>();
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		inflater = (LayoutInflater) activityParent.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	    elementListe = (TextView) inflater.inflate(layoutElement, parent, false);
	    
	    // attribution du texte de l'élément
	    if (textSousFormeDeCode) {
		    elementListe.setText( codeTexteElements[position] );
		    elementListe.setId( codeTexteElements[position] );		// la ressource-id du texte est utilisé comme id pour chaque view. C'est cette id qui est utilisé dans la méthode onItemClick() du OnItemClickListener des listview 
		    		    
	    } else if (textSousFormeDeString) {
		    elementListe.setText( texteElements.get(position) );		    
		    elementListe.setTag( texteElements.get(position) );		// le texte est utilisé comme tag pour chaque view. C'est ce tag qui est utilisé dans la méthode onItemClick() du OnItemClickListener des listview 
		}
	    
	    // attribution du CompoundDrawable de l'élément
	    if (compoundDrawableSousFormeDeCode) {
	    	switch (compoundDrawablePosition) {	// on positionne le CompoundDrawable au bonne endroit (gauche, droite, haut, bas)
		    	case COMPOUND_DRAWABLE_GAUCHE :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( codeCompoundDrawableElements[position], 0, 0, 0 );
		    		break;
		    	case COMPOUND_DRAWABLE_HAUT :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( 0, codeCompoundDrawableElements[position], 0, 0 );
		    		break;
		    	case COMPOUND_DRAWABLE_DROITE :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( 0, 0, codeCompoundDrawableElements[position],0 );
		    		break;
		    	case COMPOUND_DRAWABLE_BAS :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, codeCompoundDrawableElements[position] );
		    		break;
	    	}
	    } else if (compoundDrawableSousFormeDeBitmapDrawable) {
	    	switch (compoundDrawablePosition) { // on positionne le CompoundDrawable au bonne endroit (gauche, droite, haut, bas)
		    	case COMPOUND_DRAWABLE_GAUCHE :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( compoundDrawableElements.get(position), null, null, null );
		    		break;
		    	case COMPOUND_DRAWABLE_HAUT :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( null, compoundDrawableElements.get(position), null, null );
		    		break;
		    	case COMPOUND_DRAWABLE_DROITE :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( null, null, compoundDrawableElements.get(position),null );
		    		break;
		    	case COMPOUND_DRAWABLE_BAS :
		    	    elementListe.setCompoundDrawablesWithIntrinsicBounds( null, null, null, compoundDrawableElements.get(position) );
		    		break;
		    }
	    } else if (compoundDrawableSousFormeDUrl) {
	    	listeElementListe.put(String.valueOf(position), elementListe);
	    	transformateurIconeTask = new TransformateurIconeTask();
    		transformateurIconeTask.execute( Integer.valueOf(position) );
	    }
	    
	    return elementListe;
	}
	
	
	/*
	 * classe interne permettant le téléchargement et la transformation d'icones en BitmapDrawable avec ajout de l'icone drapeau de la langue de la ressource
	 */
	private class TransformateurIconeTask extends AsyncTask<Integer, Void, Bitmap> {
		private Canvas canvas;
		private URL urlLogoCourant;
	    private Bitmap iconeResource;									// icone d'une ressource
	    private Bitmap iconeLangueRessource;							// icone d'indication de la langue de la ressource
	    private String codelangueRessource;
	    private int positionCourante;
		
		@Override
	    protected Bitmap doInBackground(Integer... tabPositions) {			
			positionCourante = tabPositions[0].intValue();
			
	    	try {
				// on duplique l'icone pour pouvoir la modifier
				urlLogoCourant = new URL ( urlCompoundDrawableElements.get(positionCourante) );
				iconeResource = BitmapFactory.decodeStream(urlLogoCourant.openStream()).copy(Bitmap.Config.ARGB_8888, true);
				
				// Recuperation de l'icone drapeau associé à la langue de la ressource
				codelangueRessource = listeIdLangues.get(positionCourante);
				codelangueRessource = Utiles.codeLangueAvecId(codelangueRessource, activityParent);
				iconeLangueRessource = BitmapFactory.decodeResource(
					activityParent.getResources(), 
					activityParent.getResources().getIdentifier("icone_drapeau_rond_" + codelangueRessource, "drawable", activityParent.getPackageName())
				);
				
				canvas = new Canvas( iconeResource );
		        canvas.drawBitmap(iconeLangueRessource, 
		        	iconeResource.getWidth() - iconeLangueRessource.getWidth(),
		        	iconeResource.getHeight() - iconeLangueRessource.getHeight(), 
		        	null
		        );
		        
	            return iconeResource;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	    
		@Override
	    protected void onPostExecute(Bitmap icone) {				    	
	    	switch (compoundDrawablePosition) { // on positionne le CompoundDrawable au bonne endroit (gauche, droite, haut, bas)
		    	case COMPOUND_DRAWABLE_GAUCHE :
		    	    listeElementListe.get( String.valueOf(positionCourante) ).setCompoundDrawablesWithIntrinsicBounds( new BitmapDrawable(activityParent.getResources(), icone), null, null, null );
		    		break;
		    	case COMPOUND_DRAWABLE_HAUT :
		    	    listeElementListe.get( String.valueOf(positionCourante) ).setCompoundDrawablesWithIntrinsicBounds( null, new BitmapDrawable(activityParent.getResources(), icone), null, null );
		    		break;
		    	case COMPOUND_DRAWABLE_DROITE :
		    	    listeElementListe.get( String.valueOf(positionCourante) ).setCompoundDrawablesWithIntrinsicBounds( null, null, new BitmapDrawable(activityParent.getResources(), icone),null );
		    		break;
		    	case COMPOUND_DRAWABLE_BAS :
		    	    listeElementListe.get( String.valueOf(positionCourante) ).setCompoundDrawablesWithIntrinsicBounds( null, null, null, new BitmapDrawable(activityParent.getResources(), icone) );
		    		break;
		    }
	    }
	}
}