package cape.pairform.controleurs.adapters;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.protocol.HTTP;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import cape.pairform.R;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.ReferencesBDD;
import cape.pairform.utiles.Utiles;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/*
 * Adapter personnalisé permettant l'affichage horizontal d'une liste de webview
 * On peut passer d'une webview à une autre par un swipe
 */
public class ListeWebViewPagerAdapter extends PagerAdapter implements ReferencesBDD {

	private Intent intentCourante;												// intent courant
	private ArrayList<String> listeNomPagesCapsule = new ArrayList<String>();	// liste des noms de toutes les pages de la Capsule
	private String idCapsule;													// id de la capsule consultée par l'utilisateur
	private String selecteursCapsule;											// selecteurs HTML/CSS de la capsule consultée par l'utilisateur
	private ViewPager viewPager;												// contient la liste des pages, permet le parcour de la liste avec des swipes
	private WebView pageCapsule;												// une page de la Capsule sous forme de WebView
	private GestureDetectorCompat gestureDetector;
	private MessageDAO messageDAO;												// permet l'accès à la table Message
	private Cursor cursor;
	private String fonctionsJS;													// String contenant une liste de fonctions JS a executer dans la page d'une capsule
	private HashMap<String,String> mapTagOccurence;								// hashmap contenant les couples "nom tag page / occurence tag page" représentant un grain / OA d'une page
	private Activity activityParent;											// activity parent
	
	
	public ListeWebViewPagerAdapter(Activity activityParent, ArrayList<String> listeNomPagesCapsule, String idCapsule, String selecteursCapsule, ViewPager viewPager) {
		this.activityParent = activityParent;
		this.listeNomPagesCapsule = listeNomPagesCapsule;
		this.idCapsule = idCapsule;
		this.selecteursCapsule = selecteursCapsule;
		this.viewPager = viewPager;
	}
	
	
	@Override
	public void destroyItem (ViewGroup container, int position, Object object) {
		container.removeView((WebView) object);
	}
	
	
	@Override
	public int getCount() {		
		return listeNomPagesCapsule.size();
	}

	
	@Override
	public Object instantiateItem (ViewGroup container, final int position) {
		// création d'une nouvelle webview chargé avec l'uri d'une page
		pageCapsule = (WebView) activityParent.getLayoutInflater().inflate( R.layout.page_capsule, viewPager, false );
		pageCapsule.getSettings().setJavaScriptEnabled(true);			// activation du javascript
		pageCapsule.addJavascriptInterface(activityParent, "android");	// ajout de la classe d'interface du JS, le 2ème paramètre ("Android") représente le namespace JS permettant d'appeler les fonctions de WebViewJSInterface
		
		// on ajoute du javascript et du css dans la page
		inclureJSetCSS(
			activityParent.getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() +"/"+ idCapsule +"/"+ listeNomPagesCapsule.get(position)
		);

		// désactivation du menu contextuel (surcharge du clic long)
		pageCapsule.setOnLongClickListener(new OnLongClickListener() { 
	        @Override 
	        public boolean onLongClick(View v) {return true;} 
	    });		
		
		// création du listener gérant les clics sur la page
		gestureDetector = new GestureDetectorCompat(
			activityParent,
			new PageOnGestureListener( pageCapsule, position )
		);
		pageCapsule.setTag( gestureDetector) ;
		pageCapsule.setOnTouchListener( new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				return ((GestureDetectorCompat) view.getTag()).onTouchEvent(event);
			}			
		});
		
		//configuration d'un client permettant d'interagir avec la webView
		pageCapsule.setWebViewClient(new WebViewClient() {  
		    @Override
		    public void onPageFinished (WebView webView, String url) {
		    	fonctionsJS = "javascript:";
		    	mapTagOccurence = new HashMap<String,String>();
	    		messageDAO = BDDFactory.getMessageDAO(activityParent.getApplicationContext());
	    		
		    	// récupération du nombre de messages non lu sur chaque grains / OA de la page
	    		cursor = messageDAO.getNbMessagesByGrain(idCapsule, listeNomPagesCapsule.get(position), false);
	    		
	    		// construction des fonctions JS permettant d'indiquer le nombre de messages non lus sur chaque grains / OA
	    		while (cursor.moveToNext()) {
	    			fonctionsJS += "putBadge('"+ 
	    				cursor.getString( cursor.getColumnIndex(COLONNE_NOM_TAG_PAGE) ) +"',"+ 
	    				cursor.getString( cursor.getColumnIndex(COLONNE_NUM_OCCURENCE_TAG_PAGE) ) +","+ 
	    				cursor.getString( cursor.getColumnIndex(COLONNE_LU) ) +","+ 
	    				false +			// true, signifie que les messages sont non-lus
	    			");";
	    			// on stock dans une hashmap les grains / OA possédant des messages non lus
	    			mapTagOccurence.put(
    					cursor.getString( cursor.getColumnIndex(COLONNE_NOM_TAG_PAGE) ), 
    					cursor.getString( cursor.getColumnIndex(COLONNE_NUM_OCCURENCE_TAG_PAGE) )
    				);
	    		}
	    		
	    		// récupération des messages lu sur chaque grains / OA de la page
	    		cursor = messageDAO.getNbMessagesByGrain(idCapsule, listeNomPagesCapsule.get(position), true);
	    		
	    		// construction des fonctions JS permettant d'indiquer les grains / OA possédant uniquement des messages lus
	    		while (cursor.moveToNext()) {
	    			// si le grain / OA ne posséde pas de message non-lu
	    			if ( mapTagOccurence.get(cursor.getString( cursor.getColumnIndex(COLONNE_NOM_TAG_PAGE) )) != cursor.getString( cursor.getColumnIndex(COLONNE_NUM_OCCURENCE_TAG_PAGE) ) ) {
	    				fonctionsJS += "putBadge('"+ 
		    				cursor.getString( cursor.getColumnIndex(COLONNE_NOM_TAG_PAGE) ) +"',"+ 
		    				cursor.getString( cursor.getColumnIndex(COLONNE_NUM_OCCURENCE_TAG_PAGE) ) +","+ 
		    				cursor.getString( cursor.getColumnIndex(COLONNE_LU) ) +","+ 
		    				true +			// true, signifie que les messages sont lus
		    			");";
		    		}
	    		}
	    		
	    		webView.loadUrl(fonctionsJS);
	    		
	    		cursor.close();
	    		messageDAO.close();
		    }
		    
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView webView, String url) {		// permet la prise en compte du clic sur des urls contenues dans la webView
		    	// Si l'url commence par file://, on ne la prend pas en compte (gestion des pages web insérées dans d'autre page web, cf. iframe)
		    	if (!url.startsWith("file://")) {
		    		intentCourante = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					
					// on vérifie qu'il existe des applications qui puissent recevoir l'intent
					if (intentCourante.resolveActivity(activityParent.getPackageManager()) != null) {
						activityParent.startActivity(intentCourante);
					}
					return true;
		    	}
				return false;
		    }
		});
		
		// chargement de la page
		pageCapsule.loadUrl( "file://"+ activityParent.getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() +"/"+ idCapsule +"/"+ listeNomPagesCapsule.get(position) );
		container.addView( pageCapsule );		
		
		return pageCapsule;
	}
	
	
	@Override
	public boolean isViewFromObject(View view, Object webView) {
		return view == ((WebView) webView);
	}

	
	// ajoute du javascript et du css dans la page courante
	private void inclureJSetCSS(String localisation) {
		// récupération du javascript et du css a inclure dans la page .html
        String gestionMessagesCSS= "", gestionMessagesJS = "";
        try{
        	// le contenu du fichier javascript est déposé dans une chaine de caractère
            InputStream is = activityParent.getAssets().open("gestionMessages.js");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            
            while (( line = br.readLine()) != null) {
            	gestionMessagesJS += line;
            }
            
            // le contenu du fichier css est déposé dans une chaine de caractère
            is = activityParent.getAssets().open("gestionMessages.css");
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
            
            while (( line = br.readLine()) != null) {
            	gestionMessagesCSS += line;
            }
            
            is.close();
            br.close();

			// ajout du css et du javascript dans la balise head du fichier .html
			File htmlFile = new File(localisation);                		
			Document docHtml = Jsoup.parse(htmlFile, HTTP.UTF_8);
			               			
			docHtml.head().append("<style>"+ gestionMessagesCSS +"</style>");
			docHtml.head().append("<script>"+ gestionMessagesJS +"</script>");
			
    		Utiles.enregistrerFichier(new ByteArrayInputStream(docHtml.html().getBytes()), localisation);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	
	private class PageOnGestureListener implements GestureDetector.OnGestureListener {		
		private WebView pageCapsuleCourante;		// une page de la Capsule sous forme de WebView
		private int positionCourante;				// position de la page courante dans listeNomPagesCapsule
  		
  		public PageOnGestureListener(WebView pageCapsuleCourante, int positionCourante) {
  			this.pageCapsuleCourante = pageCapsuleCourante;
  			this.positionCourante = positionCourante;
  		}
		  		
		@Override
		public void onLongPress(MotionEvent event) {			
			// on déclenche une fonction JS qui détermine sur quel grains / OA l'utilisateur a cliqué et qui affiche les messages lié à ce grain / OA
			pageCapsuleCourante.loadUrl("javascript:getElementAtCoordinates("+
					event.getX() +","+ 
					event.getY() +","+ 
					pageCapsuleCourante.getHeight() +","+
					pageCapsuleCourante.getWidth() +  ","+
					"'"+ listeNomPagesCapsule.get(positionCourante) +"',"+
					"'"+ selecteursCapsule +"'"+ 
				");"
			);
		}
  		
		@Override
		public boolean onSingleTapUp(MotionEvent event) {return false;}
		@Override
		public boolean onDown(MotionEvent event) {return false;}							
		@Override
		public boolean onFling(MotionEvent downEvent, MotionEvent moveEvent, float arg2, float arg3) {return false;}        			
		@Override
		public boolean onScroll(MotionEvent startEvent, MotionEvent moveEvent, float arg2, float arg3) {return false;}
		@Override
		public void onShowPress(MotionEvent event) {}
	}
}
