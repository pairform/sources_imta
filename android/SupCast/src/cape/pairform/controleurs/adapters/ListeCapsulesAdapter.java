package cape.pairform.controleurs.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import cape.pairform.R;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.Constantes;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


/*
 * adapter personnalisé permettant l'affichage de la liste des capsules d'une ressource
 */
public class ListeCapsulesAdapter extends ArrayAdapter<String> {
	
	private final Context context;													// context de l'activity parent
	private LayoutInflater inflater;
	private RelativeLayout capsuleLayout;											// layout d'une capsule de la liste
	private ArrayList<Capsule> listeCapsules = new ArrayList<Capsule>();			// liste des capsules d'une ressource
	private HashMap<String,String> listeNbMessagesNonLus = new HashMap<String,String>();	// liste du nb de messages non-lus sur chaque capsule de la ressource courante
	private HashMap<String,String> listeNbMessagesLus = new HashMap<String,String>();		// liste du nb de messages lus sur chaque capsule de la ressource courante
	private String idCapsuleCourante;												// id de la capsule courante
	
	
	public ListeCapsulesAdapter(Context context, ArrayList<Capsule> listeCapsules, HashMap<String,String> listeNbMessagesNonLus, HashMap<String,String> listeNbMessagesLus) {
		super(context, R.layout.element_liste_capsule, new String[listeCapsules.size()]);
		this.context = context;
		this.listeCapsules = listeCapsules;
		this.listeNbMessagesNonLus = listeNbMessagesNonLus;
		this.listeNbMessagesLus = listeNbMessagesLus;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    capsuleLayout = (RelativeLayout) inflater.inflate(R.layout.element_liste_capsule, parent, false);
	    
	    // ajout de l'id de la capsule dans le tag de la view - permet de savoir sur quelle capsule l'utilisateur à cliquée
	    capsuleLayout.setTag( listeCapsules.get(position) );
	    	    
	    // si la capsule est accessible sur mobile (si elle n'est pas accessible, cela veut dire que la capsule est soit en construction, soit qu'il n'existe pas de version mobile)
	    if( !listeCapsules.get(position).getUrlMobile().isEmpty()) {
		    idCapsuleCourante = listeCapsules.get(position).getId();
		    
		    // auteurs, nom long, description, etc. de la capsule
		    ((TextView) capsuleLayout.findViewById(R.id.nom_long)).setText( listeCapsules.get(position).getNomLong() );
		    ((TextView) capsuleLayout.findViewById(R.id.description)).setText( listeCapsules.get(position).getDescription() );
		    ((TextView) capsuleLayout.findViewById(R.id.auteurs)).setText( listeCapsules.get(position).getAuteurs() );
		    ((TextView) capsuleLayout.findViewById(R.id.licence)).setText(
		    	context.getResources().getText(R.string.label_licence) +" : "+ listeCapsules.get(position).getLicence()
		    );
		    ((TextView) capsuleLayout.findViewById(R.id.poids)).setText(
		    	listeCapsules.get(position).getTaille() + " Mo"
		    );
		    
		    // formatage des dates (timestamps vers JJ/MM/AAA)
			// * 1000 : permet de transformer le timestamp (qui est en second) en millisecond
			SimpleDateFormat dateFormat = new SimpleDateFormat( context.getString(R.string.android_format_date), Locale.US );
	
			((TextView) capsuleLayout.findViewById(R.id.date_creation)).setText(
				context.getResources().getText(R.string.label_date_creation) +" : "+ dateFormat.format( new Date( listeCapsules.get(position).getDateCreation() * 1000 ) )
			);
			((TextView) capsuleLayout.findViewById(R.id.date_edition)).setText(
				context.getResources().getText(R.string.label_date_edition) +" : "+ dateFormat.format( new Date( listeCapsules.get(position).getDateEdition() * 1000 ) )
			);
		    		    	    
		    // si la capsule contient des messages non-lus
		    if ( listeNbMessagesNonLus.get(idCapsuleCourante) != String.valueOf(0) && listeNbMessagesNonLus.get(idCapsuleCourante) != null) {	    	
		    	// Ajout d'une icone indiquant le nombre de messages non-lus dans la capsule
			    ((TextView) capsuleLayout.findViewById(R.id.nb_messages)).setText(
			    	listeNbMessagesNonLus.get(idCapsuleCourante)
			    );
			    ((TextView) capsuleLayout.findViewById(R.id.nb_messages)).setBackgroundResource(R.drawable.icone_messages_non_lus);
		  	}	// sinon, si la capsule contient des messages lus
		    else if ( listeNbMessagesLus.get(idCapsuleCourante) != String.valueOf(0) && listeNbMessagesLus.get(idCapsuleCourante) != null) {	    	
		    	// Ajout d'une icone indiquant le nombre de messages lus dans la capsule
			    ((TextView) capsuleLayout.findViewById(R.id.nb_messages)).setText(
			    	listeNbMessagesLus.get(idCapsuleCourante)
			    );
			    ((TextView) capsuleLayout.findViewById(R.id.nb_messages)).setBackgroundResource(R.drawable.icone_messages_lus);
		  	}	// sinon, si la capsule n'est pas une capsule téléchargé
		 	else if ( !listeCapsules.get(position).isTelechargee() ) {
		    	((ImageView) capsuleLayout.findViewById(R.id.bouton_telecharger_capsule)).setVisibility(View.VISIBLE);		// on affiche le bouton de téléchargement
		    	
		    	// si la capsule a une visibilité innacessible (= accès résérvé à certains utilisateurs), on affiche une icone de verrou
		    	if ( listeCapsules.get(position).getIdVisibilite().equals(Constantes.ID_VISIBILITE_INACCESSIBLE) ) {
		    		((ImageView) capsuleLayout.findViewById(R.id.bouton_telecharger_capsule)).setImageResource(R.drawable.icone_verouiller);
		    	}
		    }
			
		    // configuration du bouton d'informations sur la capsule
		    ((ImageView) capsuleLayout.findViewById(R.id.bouton_infos_capsule)).setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View boutonInfos) {					
					// affiche ou masque les informatons sur une capsule
					if ( ((LinearLayout) capsuleLayout.findViewById(R.id.infos_capsule)).getVisibility() == View.GONE) {
						Toast.makeText(context, "visible", Toast.LENGTH_SHORT).show();
						((LinearLayout) capsuleLayout.findViewById(R.id.infos_capsule)).setVisibility(View.VISIBLE);
					} else {
						Toast.makeText(context, "gone", Toast.LENGTH_SHORT).show();
						((LinearLayout) capsuleLayout.findViewById(R.id.infos_capsule)).setVisibility(View.GONE);
					}
				}	    
		    });
	    } else {
		    ((TextView) capsuleLayout.findViewById(R.id.nom_long)).setText( listeCapsules.get(position).getNomLong() );
		    ((TextView) capsuleLayout.findViewById(R.id.nom_long)).setTextColor( context.getResources().getColor(R.color.gris_sombre) );
		    ((ImageView) capsuleLayout.findViewById(R.id.bouton_telecharger_capsule)).setVisibility(View.INVISIBLE);						// modifi la visibilité du bouton de téléchargement pour ajuster la position des autres éléments
	    }
	    
	    return capsuleLayout;
	}

}
