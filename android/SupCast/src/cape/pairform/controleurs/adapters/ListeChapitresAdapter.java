package cape.pairform.controleurs.adapters;

import java.util.ArrayList;

import cape.pairform.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


/*
 * adapter personnalisé permettant l'affichage des chapitres et des pages de l'arborescence des chapitres et pages d'une ressource
 */
public class ListeChapitresAdapter extends ArrayAdapter<String> {
	
	private final Context context;													// context de l'activity parent
	private LayoutInflater inflater;
	private LinearLayout elementListe;												// element de la liste (représente un chapitre ou une page)
	private ArrayList<String> listeTitresElements = new ArrayList<String>();		// liste des titres des chapitres et des pages de l'arborescence
	private ArrayList<Boolean> listeEstChapitre = new ArrayList<Boolean>();			// liste associé à listeTitresElements, contient true pour les chapitres, false pour les pages
	private ArrayList<String> listeNbMessagesNonLus = new ArrayList<String>();		// liste associé à listeTitresElements, la somme du nombre de messages non lus dans une page ou un chapitres (et les chapitres et pages qu'il contient)
	private ArrayList<String> listeNbMessagesLus = new ArrayList<String>();		// liste associé à listeTitresElements, la somme du nombre de messages lus dans une page ou un chapitres (et les chapitres et pages qu'il contient)
	
	
	public ListeChapitresAdapter(Context context) {
		super(context, R.layout.element_liste_chapitre_capsule);
		this.context = context;
	}
	
	
	public void add (String titreElement, boolean estChapitre, String nbMessagesNonLus, String nbMessagesLus) {
		super.add(titreElement);
		listeTitresElements.add(titreElement);
		listeEstChapitre.add( Boolean.valueOf(estChapitre) );
		listeNbMessagesNonLus.add( nbMessagesNonLus );
		listeNbMessagesLus.add( nbMessagesLus );
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    elementListe = (LinearLayout) inflater.inflate(R.layout.element_liste_chapitre_capsule, parent, false);
	    
	    ((TextView) elementListe.findViewById(R.id.titre)).setText( listeTitresElements.get(position) );	// titre du chapitre ou de la page
	    
	    // si le chapitre ou la pages courante contient des messages non-lus
	    if ( listeNbMessagesNonLus.get(position) != (String.valueOf(0)) && listeNbMessagesNonLus.get(position) != null ) {
	    	// si le chapitre ou la pages courante contient des messages non-lus
		    ((TextView) elementListe.findViewById(R.id.nb_messages)).setText( listeNbMessagesNonLus.get(position) );	// nb messages non-lus dans la page ou le chapitre (et les chapitres et pages qu'il contient)
		    ((TextView) elementListe.findViewById(R.id.nb_messages)).setBackgroundResource(R.drawable.icone_messages_non_lus);
		    
	  	} // sinon si le chapitre ou la pages courante contient des messages lus 
	    else if ( listeNbMessagesLus.get(position) != (String.valueOf(0)) && listeNbMessagesLus.get(position) != null ) {
		    ((TextView) elementListe.findViewById(R.id.nb_messages)).setText( listeNbMessagesLus.get(position) );				// nb messages lus dans la page ou le chapitre (et les chapitres et pages qu'il contient)
	    	((TextView) elementListe.findViewById(R.id.nb_messages)).setBackgroundResource(R.drawable.icone_messages_lus);
	  	}
	    
	    // si l'element est un chapitre
	    if ( listeEstChapitre.get(position) ) {
	    	((TextView) elementListe.findViewById(R.id.titre)).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icone_presence_chapitres_fils, 0);
	    }

	    return elementListe;
	}

}
