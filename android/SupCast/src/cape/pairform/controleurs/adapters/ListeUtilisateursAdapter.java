package cape.pairform.controleurs.adapters;

import java.util.ArrayList;

import cape.pairform.R;
import cape.pairform.modele.Utilisateur;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


/*
 * adapter personnalisé permettant l'affichage d'une liste d'utilisateurs (TextView (pseudo) associé à un CompoundDrawable (avatar))
 */
public class ListeUtilisateursAdapter extends ArrayAdapter<String> {
	
	private final Context context;									// context de l'activity parent
	private ArrayList<Utilisateur> listeUtilisateurs;				// la liste des utilisateurs à afficher
	private LayoutInflater inflater;
	private TextView elementListe;									// element de la liste possédant un CompoundDrawable
	private int layoutElement;										// ressource id du layout à utilisé pour chaque élément de la liste d'utilisateurs
	private int colorRessource;										// ressource id de la couleur du texte
	private boolean nouvelleCouleurText = false;					// true si le text doit avoir une couleur particulière, false sinon (il aura la couleur par défaut du thème)
	
	
	public ListeUtilisateursAdapter(Context context, int layoutElement, int colorRessource, ArrayList<Utilisateur> listeUtilisateurs) {
		super(context, R.layout.element_liste_utilisateurs, new String[listeUtilisateurs.size()]);
		this.context = context;
		this.listeUtilisateurs = listeUtilisateurs;
		this.layoutElement = layoutElement;
		this.colorRessource = colorRessource;
		nouvelleCouleurText = true;
	}
	
	public ListeUtilisateursAdapter(Context context, int layoutElement, ArrayList<Utilisateur> listeUtilisateurs) {
		super(context, R.layout.element_liste_utilisateurs, new String[listeUtilisateurs.size()]);
		this.context = context;
		this.listeUtilisateurs = listeUtilisateurs;
		this.layoutElement = layoutElement;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    elementListe = (TextView) inflater.inflate(layoutElement, parent, false);
	    
	    if (nouvelleCouleurText) {
		    elementListe.setTextColor( colorRessource );	
	    }
	    elementListe.setText( listeUtilisateurs.get(position).getPseudo() );
		elementListe.setTag( listeUtilisateurs.get(position).getId() );		// l'id de l'utilisateur est utilisé comme tag. C'est ce tag qui est utilisé dans la méthode onItemClick() du OnItemClickListener des listview 
		
		try {
			Bitmap avatarBitmap = BitmapFactory.decodeFile( listeUtilisateurs.get(position).getUrlAvatar() );
		
	    	// si l'avatar a été téléchargé, on récupère le CompoundDrawable sous forme de BitmapDrawable
			if (avatarBitmap != null ) {
				BitmapDrawable drawable = new BitmapDrawable(
					context.getResources(),
					avatarBitmap
				);
				
				elementListe.setCompoundDrawablesWithIntrinsicBounds( drawable, null, null, null );    
			}
		} catch(Exception e) {}
		
	    return elementListe;
	}

}
