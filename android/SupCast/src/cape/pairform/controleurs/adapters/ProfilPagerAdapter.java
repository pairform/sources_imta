package cape.pairform.controleurs.adapters;

import cape.pairform.R;
import cape.pairform.controleurs.services.AttributeurOpenBadgeService;
import cape.pairform.controleurs.services.AttributeurRoleService;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.ProfilUtilisateur;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;
import android.widget.Toast;


/*
 * Adapter personnalisé permettant l'affichage dans des tabs des ressources, des succès et des badges d'un utilisateur
 * On peut passer de l'un à l'autre par un swipe
 */
public class ProfilPagerAdapter extends PagerAdapter {

	private static final String[] CLEF_RESSOURCES = {Constantes.CLEF_ID_RESSOURCE, Constantes.CLEF_NOM_COURT, Constantes.CLEF_SCORE_UTILISATEUR_DANS_RESSOURCE, Constantes.CLEF_ID_ROLE_UTILISATEUR};
	private static final int[] LAYOUT_ID_RESSOURCES = {R.id.image_ressource, R.id.nom_ressource, R.id.score, R.id.role_utilisateur};
	private static final String[] CLEFS_SUCCES = {Constantes.CLEF_LOGO_SUCCES, Constantes.CLEF_NOM, Constantes.CLEF_DESCRIPTION};
	private static final int[] LAYOUT_ID_SUCCES = {R.id.image_succes, R.id.nom_succes, R.id.description_succes};
	private static final String[] CLEFS_OPENBADGES = {Constantes.CLEF_URL_LOGO, Constantes.CLEF_NOM, Constantes.CLEF_DESCRIPTION, Constantes.CLEF_EMETTEUR_OPENBADGE, Constantes.CLEF_DATE_GAGNE};
	private static final int[] LAYOUT_ID_OPENBADGES = {R.id.image_openbadge, R.id.nom_openbadge, R.id.description_openbadge, R.id.emetteur, R.id.date_gagne};
	private static final int[] CODE_TEXTE_ACTIONS_EXPERT_SUR_RESSOURCE = {R.string.button_nommer_a_un_role, R.string.button_attribuer_badge};
	
	private Intent intentCourante;							// intent courante
	private Activity activityParent;						// activity parent
	private ViewPager viewPager;
	private ProfilUtilisateur profilUtilisateur;
	private HashMap< String, ArrayList<HashMap<String,String>> > listeOpenBadges;	// liste des badge qu'un utilisateur peut attribuer dans chaque ressource
	private String idRessourceCourante;
	private GridView layoutTabRessources;					// tab contenant la liste des ressources de l'utilisateur
	private ListView layoutTabSucces;						// tab contenant la liste des succes de l'utilisateur
	private ListView layoutTabOpenBadges;					// tab contenant la liste des badges de l'utilisateur
	private ViewGroup layoutTabSelectionne;					// tab actuellement affiché
	private SimpleAdapter succesAdapter;					// adapter de la liste des succès
	private SimpleAdapter openBadgesAdapter;				// adapter de la liste des badgess
	private SimpleAdapter ressourcesAdapter;				// adapter de la liste des ressources
	private Drawable imageSucces;
	private InputStream inputStream;
	private AlertDialog.Builder dialogBuilder;				// fenêtre courante
	private ArrayAdapter<String> dialogAdapter;				// adapter de la fenêtre courante
	private TransformateurIconeTask transformateurIconeTask;
	private String stringJSON;								// contenu d'une réponse JSON d'une requête http
	private ProgressDialog progressDialog;					// fenêtre de chargement
	private SimpleDateFormat dateFormat;
	
	
	public ProfilPagerAdapter(Activity activityParent, ViewPager viewPager, ProfilUtilisateur profilUtilisateur) {
		this.activityParent = activityParent;
		this.viewPager = viewPager;
		this.profilUtilisateur = profilUtilisateur;
	}

	
	@Override
	public CharSequence getPageTitle(int position) {		
	    switch (position) {
	        case 0:		// liste des ressources de l'utilisateur
	            return activityParent.getString(R.string.title_ressources_maj);
	        case 1:		// liste des succès de l'utilisateur
	        	return activityParent.getString(R.string.title_succes_maj) + " (" + profilUtilisateur.getNbSuccesDebloques() + "/" + profilUtilisateur.getNbSuccesTotal() + ")";
	        case 2:		// liste des badge de l'utilisateur
	            return activityParent.getString(R.string.title_openbadges_maj) + " (" + profilUtilisateur.getNbOpenBadges() + ")";
	    }	 
	    return null;
	}
	
	
	@Override
	public Object instantiateItem (ViewGroup container, int position) {
		// selon la position, on affiche le bon tab
		switch (position) {
			case 0:		// liste des ressources de l'utilisateur
				layoutTabRessources = ((GridView) activityParent.getLayoutInflater().inflate( R.layout.grille_ressources_profil, viewPager, false ));
				
				// si l'utilisateur possède des ressources 
				if ( !profilUtilisateur.getListeRessources().isEmpty() ) {
					ressourcesAdapter = new SimpleAdapter(
						activityParent,
						profilUtilisateur.getListeRessources(),
						R.layout.ressource_ecran_profil,
						CLEF_RESSOURCES,
						LAYOUT_ID_RESSOURCES
					);
		
					ressourcesAdapter.setViewBinder(new ViewBinder() {
						@Override
						public boolean setViewValue(View view, Object value, String stringValue) {
							if (view instanceof TextView) {	            		
								if(((TextView) view).getId() == R.id.role_utilisateur)		// si le TextView correspond au rôle de l'utilisateur dans la ressource
									((TextView) view).setText( activityParent.getString(
										activityParent.getResources().getIdentifier("label_utilisateur_" + stringValue, "string", activityParent.getPackageName())
									));
								else if(((TextView) view).getId() == R.id.score)			// si le TextView correspond au score de l'utilisateur dans la ressource
									((TextView) view).setText(
										activityParent.getString(R.string.label_score) + " : " + stringValue + " " + activityParent.getString(R.string.label_pts)
									); 
								else
									((TextView) view).setText(stringValue);
							} else if (view instanceof ImageView) {
								// logo de la ressource, stringValue correspond ici à l'id de la ressource
								File logo = new File( activityParent.getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + stringValue + "/" + Constantes.CLEF_LOGO );
		
								// si la ressource est téléchargé sur le mobile, on récupère l'icone
								if ( logo.exists() ) {
									((ImageView) view).setImageURI( Uri.parse(logo.getPath() ));
								}
							}
							return true;
						}
					});
					
					layoutTabRessources.setAdapter(ressourcesAdapter);
					
					layoutTabRessources.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view, final int positionRessource, long id) {
							idRessourceCourante = profilUtilisateur.getListeRessources().get(positionRessource).get(Constantes.CLEF_ID_RESSOURCE);
							
							// si l'utilisateur est un expert ou un admin
							if(Integer.parseInt( activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_RESSOURCE + idRessourceCourante, "0") ) >= Constantes.ID_EXPERT) {
								// on affiche les actions que l'utilisateur peut faire sur la ressource (attribuer un badge, etc.)
								dialogAdapter = new ArrayAdapter<String>(
									activityParent,
									R.layout.element_liste_defaut,
									new String[] {activityParent.getString(R.string.button_nommer_a_un_role), activityParent.getString(R.string.button_attribuer_badge)}
								);
								
								dialogBuilder = new AlertDialog.Builder(activityParent);
								dialogBuilder.setTitle(activityParent.getString(R.string.title_voulez_vous) + " :")
									.setAdapter(dialogAdapter, new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int itemSelectionne) {
											dialog.cancel();
											
											switch( CODE_TEXTE_ACTIONS_EXPERT_SUR_RESSOURCE[itemSelectionne] ) {
												// nommer l'utilisateur à un rôle (expert, animateur, etc.)
												case R.string.button_nommer_a_un_role:
													dialog.cancel();
													nommerRole(idRessourceCourante, positionRessource);
													break;
												// attribuer un badge
												case R.string.button_attribuer_badge:
													dialog.cancel();
													attribuerBadge(idRessourceCourante);
													break;
											}
										}
									})
									.show();								
							}
						}
					});
				}
				
				layoutTabSelectionne = layoutTabRessources;
				break;

			case 1:		// liste des succès de l'utilisateur
				layoutTabSucces = new ListView(activityParent);
				
				succesAdapter = new SimpleAdapter(
					activityParent,
					profilUtilisateur.getListeSucces(),
					R.layout.succes,
					CLEFS_SUCCES,
					LAYOUT_ID_SUCCES
				);

				succesAdapter.setViewBinder(new ViewBinder() {
					@Override
					public boolean setViewValue(View view, Object value, String stringValue) {       	
						if (view instanceof ImageView) {
							inputStream = null;
							try {
								// si l'image d'un succès n'existe pas, on prend l'image de succès par défaut (différente de l'image d'un succès non-débloqué)
								if ( !stringValue.isEmpty() ) {
									inputStream = activityParent.getAssets().open(Constantes.REPERTOIRE_SUCCES + "/" + stringValue + ".png");
								} else {
									inputStream = activityParent.getAssets().open(Constantes.REPERTOIRE_SUCCES + "/" + Constantes.FICHIER_SUCCES);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}

							imageSucces = Drawable.createFromStream(inputStream, null);            		
							((ImageView) view).setImageDrawable(imageSucces);

						} else if (view instanceof TextView) {
							((TextView) view).setText(stringValue);                    
						}
						return true;
					}
				});
				
				layoutTabSucces.setAdapter(succesAdapter);
				
				layoutTabSelectionne = layoutTabSucces;
				break;

			case 2:		// liste des badges de l'utilisateur
				layoutTabOpenBadges = new ListView(activityParent);
				dateFormat = new SimpleDateFormat(activityParent.getString(R.string.android_format_date), Locale.US);
				
				openBadgesAdapter = new SimpleAdapter(
					activityParent,
					profilUtilisateur.getListeOpenBadges(),
					R.layout.openbadge,
					CLEFS_OPENBADGES,
					LAYOUT_ID_OPENBADGES
				);

				openBadgesAdapter.setViewBinder(new ViewBinder() {
					@Override
					public boolean setViewValue(View view, Object value, String stringValue) {       	
						if (view instanceof ImageView) {
					    	transformateurIconeTask = new TransformateurIconeTask();
				    		transformateurIconeTask.execute(stringValue, view);
				    		
						} else if (view.getId() == R.id.emetteur) {
							((TextView) view).setText(activityParent.getString(R.string.label_donne_par) +" : "+ stringValue);

						} else if (view.getId() == R.id.date_gagne) {
							// date d'attribution du badge
							Date dateCreation = new Date( Long.parseLong(stringValue) * 1000 );		// * 1000 : permet de transformer le timestamp (qui est en second) en millisecond
							((TextView) view).setText( dateFormat.format(dateCreation) );
							
						} else if (view instanceof TextView) {
							((TextView) view).setText(stringValue);
							
						}
						return true;
					}
				});
				
				layoutTabOpenBadges.setAdapter(openBadgesAdapter);
				
				layoutTabSelectionne = layoutTabOpenBadges;
				break;
		}
		container.addView( layoutTabSelectionne );
		
		return layoutTabSelectionne;
	}
	
	
	// nomme l'utilisateur à un rôle sur la ressource séléctionnée
	private void nommerRole(final String idRessourceSelectionne, final int positionRessource) {
		// on affiche la liste des rôles
		dialogAdapter = new ArrayAdapter<String>(
			activityParent,
			R.layout.element_liste_defaut,
			new String[] {activityParent.getString(R.string.label_utilisateur_4), activityParent.getString(R.string.label_utilisateur_3), activityParent.getString(R.string.label_utilisateur_2), activityParent.getString(R.string.label_utilisateur_1)}
		);
		
		dialogBuilder = new AlertDialog.Builder(activityParent);
		dialogBuilder.setTitle(activityParent.getString(R.string.button_nommer_a_un_role))
			.setAdapter(dialogAdapter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int itemSelectionne) {
					dialog.cancel();

					// on lance le service qui gére l'attribution des rôles en spécifiant le rôle, l'utilisateur et la ressource concernés
					intentCourante = new Intent(activityParent, AttributeurRoleService.class);
					intentCourante.putExtra(Constantes.CLEF_ID_RESSOURCE, idRessourceSelectionne);
					intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, profilUtilisateur.getIdUtilisateur());

					switch(itemSelectionne) {
						case 0:
							intentCourante.putExtra(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_EXPERT));
							profilUtilisateur.getListeRessources().get(positionRessource).put(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_EXPERT));
							break;
						case 1:
							intentCourante.putExtra(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_ANIMATEUR));
							profilUtilisateur.getListeRessources().get(positionRessource).put(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_ANIMATEUR));
							break;
						case 2:
							intentCourante.putExtra(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_COLLABORATEUR));
							profilUtilisateur.getListeRessources().get(positionRessource).put(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_COLLABORATEUR));
							break;
						case 3:
							intentCourante.putExtra(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_CONTRIBUTEUR));
							profilUtilisateur.getListeRessources().get(positionRessource).put(Constantes.CLEF_ID_ROLE_UTILISATEUR, String.valueOf(Constantes.ID_CONTRIBUTEUR));
							break;					
					}
					activityParent.startService(intentCourante);

					// mise à jour du rôle dans l'interface
					ressourcesAdapter = new SimpleAdapter(
						activityParent,
						profilUtilisateur.getListeRessources(),
						R.layout.ressource_ecran_profil,
						CLEF_RESSOURCES,
						LAYOUT_ID_RESSOURCES
					);
					//layoutTabRessources.setAdapter(ressourcesAdapter);
				}
			})
			.show();
	}
	
	
	// attribue un badge à l'utilisateur sur la ressource séléctionnée
	private void attribuerBadge(final String idRessourceSelectionne) {
		// si ce n'est pas déjà fait, on récupére de la liste des badges
		if(listeOpenBadges == null) {
			// on bloque l'orientation de l'écran dans son orientation actuelle
			if ( activityParent.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
				activityParent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
			} else {
				activityParent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
	
			// création d'une fenêtre de chargement
			progressDialog = new ProgressDialog(activityParent);
			progressDialog.setMessage( activityParent.getString(R.string.label_chargement) );
			progressDialog.show();
					
			// création d'un nouveau Thread
			Thread thread = new Thread(new Runnable() {
				public void run() {
					// création et configuration des paramètres d'une requête http
					ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
	
					try {
						parametresRequete.add( new BasicNameValuePair(															// langue de l'application
							Constantes.CLEF_WEBSERVICE_LANGUE_APPLICATION,
							Utiles.idLangueAvecCode(activityParent.getResources().getString(R.string.locale_language), activityParent.getApplicationContext()))
						);
						parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_RESSOURCE, idRessourceSelectionne));	// id de la ressource selectionné
					} catch (Exception e) {
						e.printStackTrace();        	
					}
					// Envoie d'une requete HTTP récupérant les infos de profil de l'utilisateur
					stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECUPERER_OPENBADGES_ATTRIBUABLES, parametresRequete, activityParent);
	
					// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas profilJSON
					JSONObject listeBadgesJSON = null;
					if ( stringJSON != null ) {
						// transformation de la réponse en objet JSON
						try {
							listeBadgesJSON = new JSONObject(stringJSON);
						} catch (Exception e) {
							e.printStackTrace();
						}
	
						// on parse la liste des badges
						listeOpenBadges = ParserJSON.parserOpenBadges(listeBadgesJSON);
					}
	
					activityParent.runOnUiThread(new Runnable() {
						public void run() {
							// on ferme la fenetre de chargement et on désactive l'orientation forcé de l'écran
							progressDialog.dismiss();
							activityParent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
							
							// si la requete http s'est mal exécuté, stringJSON vaut null
							if ( stringJSON != null ) {
								afficherListeOpenBadges(idRessourceSelectionne);
							} else {
								Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, activityParent);
							}
						}
					});
				}
			});
	
			thread.start();
		} else {
			afficherListeOpenBadges(idRessourceSelectionne);
		}
	}

	
	// afficher la liste des badges attribualbles par un utilisateur dans une ressource
	private void afficherListeOpenBadges(final String idRessourceSelectionne) {
		// s'il y a des openbadges sur cette ressource
		if ( listeOpenBadges.get(idRessourceSelectionne) != null ) {
			// on affiche la liste des badge de la resource
			openBadgesAdapter = new SimpleAdapter(
				activityParent,
				listeOpenBadges.get(idRessourceSelectionne),
				R.layout.openbadge,
				CLEFS_OPENBADGES,
				LAYOUT_ID_OPENBADGES
			);

			openBadgesAdapter.setViewBinder(new ViewBinder() {
				@Override
				public boolean setViewValue(View view, Object value, String stringValue) {       	
					if (view instanceof ImageView) {
				    	transformateurIconeTask = new TransformateurIconeTask();
			    		transformateurIconeTask.execute(stringValue, view);		    		
					} else if (view.getId() == R.id.emetteur) {
						// on affiche pas l'émetteur
					} else if (view.getId() == R.id.date_gagne) {
						// il n'y a pas encore de date
					} else if (view instanceof TextView) {
						((TextView) view).setText(stringValue);
					}
					return true;
				}
			});
			
			dialogBuilder = new AlertDialog.Builder(activityParent);
			dialogBuilder.setTitle(activityParent.getString(R.string.button_attribuer_badge))
				.setAdapter(openBadgesAdapter, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int itemSelectionne) {
						dialog.cancel();

						// on lance le service qui gére l'attribution des badges
						intentCourante = new Intent(activityParent, AttributeurOpenBadgeService.class);
						intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, profilUtilisateur.getIdUtilisateur());
						intentCourante.putExtra(Constantes.CLEF_ID_OPENBADGE, listeOpenBadges.get(idRessourceSelectionne).get(itemSelectionne).get(Constantes.CLEF_ID_OPENBADGE));
						
						activityParent.startService(intentCourante);

						// mise à jour de l'interface : on ajoute le nouveau badge dans la liste des badge de l'utilisateur
						listeOpenBadges.get(idRessourceSelectionne).get(itemSelectionne).put(
							Constantes.CLEF_DATE_GAGNE,
							String.valueOf( Calendar.getInstance().getTimeInMillis() / 1000 )
						);
						profilUtilisateur.getListeOpenBadges().add( listeOpenBadges.get(idRessourceSelectionne).get(itemSelectionne) );
					}
				})
				.show();
		} else {
			Toast.makeText(activityParent, R.string.label_aucun_openbadge_sur_ressource, Toast.LENGTH_LONG).show();
		}
	}
	
	
	/*
	 * classe interne permettant le téléchargement et la transformation d'icones en BitmapDrawable
	 */
	private class TransformateurIconeTask extends AsyncTask<Object, Void, Bitmap> {
		private URL urlLogoCourant;
	    private Bitmap iconeBadge;
	    private String urlLogo;
	    private ImageView imageView;
		
		@Override
	    protected Bitmap doInBackground(Object... tabPositions) {
			urlLogo = (String) tabPositions[0];		
			imageView = (ImageView) tabPositions[1];
			
	    	try {
				urlLogoCourant = new URL ( urlLogo );
				iconeBadge = BitmapFactory.decodeStream(urlLogoCourant.openStream()).copy(Bitmap.Config.ARGB_8888, true);
		        
	            return iconeBadge;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	    
		@Override
	    protected void onPostExecute(Bitmap icone) {				    	
			imageView.setImageBitmap(icone);
	    }
	}
	
	
	@Override
	public int getCount() {		
		return 3;
	}
	
	
	@Override
	public boolean isViewFromObject(View view, Object layout) {
		return view == ((ViewGroup) layout);
	}
	
	
	@Override
	public void destroyItem (ViewGroup container, int position, Object layout) {
		container.removeView((ViewGroup) layout);
	}
}