package cape.pairform.controleurs.adapters;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import cape.pairform.R;
import cape.pairform.controleurs.activities.EcranCapsuleActivity;
import cape.pairform.controleurs.activities.EcranTableauDeBordActivity;
import cape.pairform.controleurs.services.RecuperateurMessagesService;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Ressource;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.CapsuleDAO;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.RessourceDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;


/*
 * Adapter personnalisé permettant l'affichage dans des tabs des infos d'une ressource et de des capsules de cette ressource
 * On peut passer de l'un à l'autre par un swipe
 */
public class RessourcePagerAdapter extends PagerAdapter {

	private static final String LOG_TAG = RessourcePagerAdapter.class.getSimpleName();
	
	private Ressource ressourceCourante;						// ressource sélectionnée par l'utilisateur
	private HashMap<String,String> listeNbMessagesNonLus = new HashMap<String,String>();	// liste du nb de messages non-lus sur chaque capsule de la ressource sélectionnée
	private HashMap<String,String> listeNbMessagesLus = new HashMap<String,String>();		// liste du nb de messages lus sur chaque capsule de la ressource sélectionnée
	private Activity activityParent;
	private ViewPager viewPager;
	private ScrollView layoutTabInfos;							// tab contenant les infos sur la ressource
	private ListView layoutTabCapsules;							// tab contennat la liste des capsules de la ressource
	private ViewGroup layoutTabSelectionne;						// tab actuellement affiché
	private ListeCapsulesAdapter adapterListeCapsules;			// adapter de la listView layoutTabCapsules
    private DownloadManager downloadManager = null;				// gestionnaire de téléchargement
	private DownloadManager.Request request;					// requete définissant ce qui doit être téléchargé et le contenu de la notification de téléchargement
	private BroadcastReceiver downloadBroadcastReceiver;		// broadcastreceiver se déclenchant à la fin du téléchargement
	private long downloadReference;								// identifiant du téléchargement
	private AlertDialog.Builder dialogSupprimerCapsule;			// fenêtre de dialog demandant validation avant de supprimer une capsule
	
	
	public RessourcePagerAdapter(Activity activityParent, ViewPager viewPager, Ressource ressourceCourante, HashMap<String,String> listeNbMessagesNonLus, HashMap<String,String> listeNbMessagesLus) {
		this.activityParent = activityParent;
		this.viewPager = viewPager;
		this.ressourceCourante = ressourceCourante;
		this.listeNbMessagesNonLus = listeNbMessagesNonLus;
		this.listeNbMessagesLus = listeNbMessagesLus;
	}

	
	@Override
	public CharSequence getPageTitle(int position) {		
	    switch (position) {
	        case 0:		// liste des capsules de la ressource
	            return activityParent.getString(R.string.title_capsules_maj);
	        case 1:		// infos sur la ressource
	            return activityParent.getString(R.string.title_informations_maj);
	    }	 
	    return null;
	}
	
	
	@Override
	public Object instantiateItem (ViewGroup container, int position) {
		// selon la position, on affiche le bon tab
		switch (position) {
			case 0:		// liste des capsules de la ressource
				layoutTabCapsules = (ListView) activityParent.getLayoutInflater().inflate( R.layout.layout_tab_liste_capsules, viewPager, false );

				// création d'un listener se déclenchant lors d'un clic sur un item de la listView
				layoutTabCapsules.setOnItemClickListener( new OnItemClickListener () {
					@Override
					public void onItemClick(AdapterView<?> parent, View capsuleLayout, int position, long id) {
						// si la capsule est accessible sur mobile (si elle n'est pas accessible, cela veut dire que la capsule est soit en construction, soit qu'il n'existe pas de version mobile)
						if ( !((Capsule) capsuleLayout.getTag()).getUrlMobile().isEmpty() ) {
							// si la capsule est déjà téléchargée
							if ( ((Capsule) capsuleLayout.getTag()).isTelechargee() ) {
								// lancement de l'écran affichant le sommaire d'une capsule
								Intent intentCourante = new Intent(activityParent, EcranCapsuleActivity.class);
					        	intentCourante.putExtra(Constantes.CLEF_ID_CAPSULE, ((Capsule) capsuleLayout.getTag()).getId());
					        	activityParent.startActivity(intentCourante);
							
							}	// sinon, si la capsule n'a pas une visibilité innacessible (= accès résérvé à certains utilisateurs), on la télécharge
							else if ( !((Capsule) capsuleLayout.getTag()).getIdVisibilite().equals(Constantes.ID_VISIBILITE_INACCESSIBLE) ) {
								telechargerCapsule( (Capsule) capsuleLayout.getTag() );
							}
						} else {
    						Toast.makeText(activityParent, activityParent.getString(R.string.label_capsule_uniquement_web), Toast.LENGTH_LONG).show();							
						}
					}        	
		        });
				// création d'un listener se déclenchant lors d'un clic long sur un item de la listView
				layoutTabCapsules.setOnItemLongClickListener( new OnItemLongClickListener () {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent, View capsuleLayout, int position, long id) {
						// si la capsule est téléchargée
						if ( ((Capsule) capsuleLayout.getTag()).isTelechargee() ) {
							// ouverture de la fenetre de dialog de validation de la suppression d'une capsule
							ouvrirDialogSupprimerCapsule( (Capsule) capsuleLayout.getTag() );
						}
						return true;
					}        	
		        });
				
				adapterListeCapsules = new ListeCapsulesAdapter(
					activityParent,
					ressourceCourante.getListeCapsules(),
					listeNbMessagesNonLus,
					listeNbMessagesLus
				);
				layoutTabCapsules.setAdapter(adapterListeCapsules);
				
				layoutTabSelectionne = layoutTabCapsules;
				break;

			case 1:		// infos sur la ressource
				layoutTabInfos = (ScrollView) activityParent.getLayoutInflater().inflate( R.layout.layout_tab_infos_ressource, viewPager, false );
				
				((TextView) layoutTabInfos.findViewById(R.id.theme_ressource)).setText(
					activityParent.getString(R.string.label_theme) +" : "+ ressourceCourante.getTheme()
				);
				((TextView) layoutTabInfos.findViewById(R.id.espace_ressource)).setText( ressourceCourante.getNomEspace() );
				((TextView) layoutTabInfos.findViewById(R.id.description_ressource)).setText( ressourceCourante.getDescription() );
				
				// formatage des dates (timestamps vers JJ/MM/AAA)
				// * 1000 : permet de transformer le timestamp (qui est en second) en millisecond
				SimpleDateFormat dateFormat = new SimpleDateFormat( activityParent.getString(R.string.android_format_date), Locale.US );

				((TextView) layoutTabInfos.findViewById(R.id.date_creation)).setText(
					activityParent.getString(R.string.label_date_creation) +" : "+ dateFormat.format( new Date( ressourceCourante.getDateCreation() * 1000 ) )
				);
				((TextView) layoutTabInfos.findViewById(R.id.date_edition)).setText(
					activityParent.getString(R.string.label_date_edition) +" : "+ dateFormat.format( new Date( ressourceCourante.getDateEdition() * 1000 ) )
				);
				
				layoutTabSelectionne = layoutTabInfos;
				break;
		}
		container.addView( layoutTabSelectionne );
		
		return layoutTabSelectionne;
	}
    
    
    // télécharge la capsule séléctionnée
    private void telechargerCapsule(final Capsule capsuleCourante) {
		Log.i(LOG_TAG, "fichier à télécharger : " + capsuleCourante.getUrlMobile() + ".zip");

    	if (downloadManager == null) {
    		downloadManager = (DownloadManager) activityParent.getSystemService(Context.DOWNLOAD_SERVICE);
    	}
    	
		downloadBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// si l'identifiant correspond à celui du téléchargement courant
				if(downloadReference == intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)) {					
					int status = Utiles.verifierStatusDownloadManager(downloadManager, downloadReference);
					
					// si le téléchargement a échouer
					if (status == DownloadManager.STATUS_FAILED) {
						Utiles.afficherInfoAlertDialog(R.string.title_erreur_telechargement, R.string.label_erreur_telechargement_nouvelle_capsule, activityParent);
						downloadManager.remove(downloadReference);
					} else if (status == DownloadManager.STATUS_SUCCESSFUL) {		// si le téléchargement a réussi
						// création d'un nouveau Thread
					    Thread thread = new Thread(new Runnable() {
					    	public void run() {
					    		RessourceDAO ressourceDAO = BDDFactory.getRessourceDAO(activityParent.getApplicationContext());
					    		CapsuleDAO capsuleDAO = BDDFactory.getCapsuleDAO(activityParent.getApplicationContext());
								
					    		// si la ressource n'exite pas déjà en BDD (aucune de ses capsules n'a déjà été téléchargées)
					    		if ( !ressourceDAO.isRessource(ressourceCourante.getId()) ) {
									// création du répertoire associé à la ressource, il contient les fichiers de la ressource (logo, capsules, etc.)
						        	File repertoireRessource = new File( activityParent.getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + ressourceCourante.getId() );
						        	repertoireRessource.mkdirs();
						        	
						    		// on télécharge l'icone de la ressource
						    		Utiles.telechargerFichier (
						    			Constantes.SERVEUR_NODE_URL + ressourceCourante.getUrlLogo(),
						    			activityParent.getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + ressourceCourante.getId() + "/" + Constantes.CLEF_LOGO
						    		);
						    		
						    		// mise à jour de l'emplacement du logo pour pointer vers le logo venant d'être télécharger en local
						    		ressourceCourante.setUrlLogo( activityParent.getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + ressourceCourante.getId() + "/" + Constantes.CLEF_LOGO );
						        	
						        	// ajout de la ressource en BDD
									ressourceDAO.insertRessource(ressourceCourante);
									
									// ajout des capsules de la ressource en BDD
									for (Capsule capsule : ressourceCourante.getListeCapsules()) {
										if ( capsuleCourante.getId().equals(capsule.getId()) )
											capsule.setTelechargee(true);
											
										capsuleDAO.insertCapsule(capsule);
									}
						    	} else {
									capsuleCourante.setTelechargee(true);
									capsuleDAO.updateCapsule(capsuleCourante);
						    	}
								ressourceDAO.close();
								capsuleDAO.close();
					        	
								try {
									// on dezipe la capsule téléchargée dans le repertoire dédié
									Utiles.dezipperCapsule (
										(FileInputStream) new ParcelFileDescriptor.AutoCloseInputStream ( downloadManager.openDownloadedFile(downloadReference) ),
										activityParent.getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() +"/"+ capsuleCourante.getId(),
										capsuleCourante.getUrlMobile()
									);
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								}
								
								downloadManager.remove(downloadReference);
															
								// télécharge les messages des nouvelles capsules et les enregistrent en BDD
						    	telechargerNouveauxMessages(capsuleCourante.getId(), capsuleCourante.getDateCreation());

								String idUtilisateur = activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, null);
								// si l'utilisateur est connecté  (l'id est stocké dans un fichier de préférence)
								if (idUtilisateur != null) {
									recupererRoleUtilisateur(ressourceCourante.getId(), capsuleCourante.getId(), idUtilisateur);
								}
								
			    				// on met à jour l'interface				    			        	
	    			        	adapterListeCapsules = new ListeCapsulesAdapter(
	    							activityParent,
	    							ressourceCourante.getListeCapsules(),
	    							listeNbMessagesNonLus,
	    							listeNbMessagesLus
	    						);
	    			        	
			    	        	activityParent.runOnUiThread(new Runnable() {
			    	    			public void run() {
			    						((ListView) layoutTabCapsules.findViewById(R.id.liste_capsules)).setAdapter(adapterListeCapsules);
			    						
			    						// On informe l'utilisateur de la réussite du téléchargement
			    						Toast.makeText(activityParent, capsuleCourante.getNomCourt() + " " + activityParent.getString(R.string.label_telechargement_nouvelle_capsule_fini), Toast.LENGTH_LONG).show();
			    	    	        }
			    	    	    });
			    				activityParent.unregisterReceiver( downloadBroadcastReceiver );
			    	    	}
			    	    });
			    	    
			    	    thread.start();
					}
				}
			}
		};
    	activityParent.registerReceiver( downloadBroadcastReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE) );
    	
    	// création et définition de la requete, affichage d'une notification pendant le téléchargement
    	request = new DownloadManager.Request( Uri.parse( capsuleCourante.getUrlMobile() + ".zip" ) );
    	request.addRequestHeader("Cookie", Utiles.getCookieSession(activityParent, Constantes.CLEF_COOKIE_SESSION) +" "+ Utiles.getCookieSession(activityParent, Constantes.CLEF_COOKIE_SESSION_SIG));
    	request.setTitle( activityParent.getString(R.string.app_name) );
		request.setDescription( activityParent.getString(R.string.label_telechargement_nouvelle_capsule) + " " + capsuleCourante.getNomCourt() + "." );
		request.setDestinationInExternalFilesDir(activityParent, Environment.DIRECTORY_DOWNLOADS, capsuleCourante.getNomCourt() + ".zip");
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
		
		// ajout de la requete à la pile des téléchargement du downloadmanager
		downloadReference = downloadManager.enqueue(request);
		
		// On informe l'utilisateur du lancement du téléchargement
		Toast.makeText(activityParent, activityParent.getString(R.string.label_telechargement_nouvelle_capsule) + " " + capsuleCourante.getNomCourt() + ".", Toast.LENGTH_LONG).show();
    }
    
    
    // télécharge les nouveaux messages et les enregistre en BDD
    private void telechargerNouveauxMessages(String idCapsule, long dateCreationCapsule) {
    	HashMap<String,Long> listeTimestampsCapsules = new HashMap<String,Long>();
    	listeTimestampsCapsules.put(idCapsule, dateCreationCapsule);
    	
		if (Utiles.reseauDisponible(activityParent)) {	    	
			Intent intentCourante = new Intent(activityParent, RecuperateurMessagesService.class);
	    	
	    	if (listeTimestampsCapsules != null)
	    		intentCourante.putExtra(Constantes.CLEF_FILTRE_CAPSULES, listeTimestampsCapsules);		// listeTimestampsCapsules représente une liste clef (id de la capsule) / valeur (timestamp de la plus vieille date d'édition parmi les messages de la capsule)
	    	
	    	activityParent.startService(intentCourante);
        }
    }
	
	
	// récupère le role de l'utilisateur
	private void recupererRoleUtilisateur(String idRessource, String idCapsule, String idUtilisateur) {
		// création et configuration des paramètres d'une requête http
		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
    	
        try {
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_RESSOURCE, idRessource));		// id de la ressource de la capsule téléchargée
        	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CAPSULE, idCapsule));			// id de la capsule téléchargée
        } catch (Exception e) {
        	e.printStackTrace();        	
        }
        // Envoie d'une requete HTTP connectant l'utilisateur
    	String stringJSON = Utiles.envoyerRequeteHttp(Constantes.URL_WEB_SERVICE_VERIFIER_ROLE, parametresRequete, activityParent);
    	
    	// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas stringJSON
    	if ( stringJSON != null ) {
    		HashMap<String, String> infosUtilisateur = ParserJSON.parserRetourRecuperationRole( stringJSON );
    		
    		if (infosUtilisateur != null) {
    			// on récupère le fichier de préfèrences stockant les infos sur l'utilisateur et on y stocke les infos de l'utilisateur connecté (id, email, espace, rôle dans chaque ressource, etc.)
    			SharedPreferences fichierSharedPreferences = activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);
    			SharedPreferences.Editor editorFichierSharedPreferences = fichierSharedPreferences.edit();
	        	
	        	// pour chaque info sur l'utilisateur
	        	for( String clefInfos : infosUtilisateur.keySet() ) {
	        		// si la clef correspond à l'info sur l'activation / désactivation de l'email de notification d'une ressource
	        		if( clefInfos.startsWith( Constantes.CLEF_NOTIFICATION_EMAIL ) ) {
	        			if (infosUtilisateur.get(clefInfos).equals("1") ) {
	        				editorFichierSharedPreferences.putBoolean(clefInfos, true);
	        			} else {
	        				editorFichierSharedPreferences.putBoolean(clefInfos, false);
	        			}
	        		} else {
	        			editorFichierSharedPreferences.putString(clefInfos, infosUtilisateur.get(clefInfos));
	        		}
	        	}
        		editorFichierSharedPreferences.commit();
    		}
    	}
	}
    

	// ouvrir la fenetre de dialog de validation de la suppression d'une capsule
    private void ouvrirDialogSupprimerCapsule(final Capsule capsuleASupprimee) {
    	dialogSupprimerCapsule = new AlertDialog.Builder(activityParent);
        
    	dialogSupprimerCapsule.setTitle( R.string.title_supprimer_capsule )
    		.setMessage( R.string.label_supprimer_capsule )
    	    .setPositiveButton(
   	    		R.string.button_supprimer,
    	    	new DialogInterface.OnClickListener() {
	    	    	public void onClick(DialogInterface dialog, int id) {
	    	    		// on supprime la capsule sélectionné
	    	    		supprimerCapsule( capsuleASupprimee );
	    	    		dialog.cancel();
	    	        }
    	    })
    	    .setNegativeButton(
   	    		R.string.button_annuler,
    	    	new DialogInterface.OnClickListener() {
	    	    	public void onClick(DialogInterface dialog, int id) {
	    	    		dialog.cancel();
	    	        }
    	    })
    	    .show();
    }
    
    
    // supprime une capsule
    private void supprimerCapsule(final Capsule capsuleASupprimee) {
    	// création d'un nouveau Thread
	    Thread thread = new Thread(new Runnable() {
	    	public void run() {
	    		// on supprime le répertoires contenant la capsule sélectionné
    			File file = new File( activityParent.getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() + "/" + capsuleASupprimee.getId() );
    			Utiles.supprimerRepertoire(file);

	    		RessourceDAO ressourceDAO = BDDFactory.getRessourceDAO( activityParent.getApplicationContext() );
	    		
    			// si c'est la seule capsule téléchargée de la ressource, on supprime la ressource
    			if( ressourceCourante.getNbCapsulesTelechargees() == 1 ) {
    				// on supprime le répertoire associé à la ressource
    	    		file = new File( activityParent.getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + ressourceCourante.getId() );
        			Utiles.supprimerRepertoire(file);
        			
    				// on supprime la ressource de la BDD locale (la capsule et ses messages sont aussi supprimés)
    	        	ressourceDAO.deleteRessource( ressourceCourante.getId() );
    	        	
    				// redirection de l'utilisatur sur l'écran affichant les ressources téléchargéess par l'utilisateur
    				activityParent.startActivity( new Intent(activityParent, EcranTableauDeBordActivity.class) );
    			} else {
    				// on indique la capsule comme non-téléchargée
    				capsuleASupprimee.setTelechargee(false);
    				CapsuleDAO capsuleDAO = BDDFactory.getCapsuleDAO( activityParent.getApplicationContext() );
		        	capsuleDAO.updateCapsule(capsuleASupprimee);
		        	capsuleDAO.close();
		        	
    				// on supprime tout les messages de la capsule de la BDD locale (les tags associés, etc. sont aussi supprimés)
    				MessageDAO messageDAO = BDDFactory.getMessageDAO( activityParent.getApplicationContext() );
		        	messageDAO.deleteMessagesByCapsule( capsuleASupprimee.getId() );
		        	messageDAO.close();
		        	
		        	ressourceCourante = ressourceDAO.getRessourceById( ressourceCourante.getId() );
		        	listeNbMessagesNonLus.remove(capsuleASupprimee.getId());
		        	listeNbMessagesLus.remove(capsuleASupprimee.getId());
		        	
		        	// mise à jour de l'adapter pour prendre en compte la suppression
		        	adapterListeCapsules = new ListeCapsulesAdapter(
						activityParent,
						ressourceCourante.getListeCapsules(),
						listeNbMessagesNonLus,
						listeNbMessagesLus
					);
	    			
		        	activityParent.runOnUiThread(new Runnable() {
		    			public void run() {
		    				// Mise à jour interface		    				
							((ListView) layoutTabCapsules.findViewById(R.id.liste_capsules)).setAdapter(adapterListeCapsules);							
		    			}
		    	    });
	        	}
	        	ressourceDAO.close();
	    	}
	    });

	    thread.start();
    }
	
	
	@Override
	public int getCount() {		
		return 2;
	}
	
	
	@Override
	public boolean isViewFromObject(View view, Object layout) {
		return view == ((ViewGroup) layout);
	}
	
	
	@Override
	public void destroyItem (ViewGroup container, int position, Object layout) {
		container.removeView((ViewGroup) layout);
	}
}