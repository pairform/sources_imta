package cape.pairform.controleurs;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ActionBar.Tab;
import android.os.Bundle;
import cape.pairform.modele.Constantes;


/*
 * listener associé aux onglets de l'actionbar
 */
public class ActionBarTabListener implements ActionBar.TabListener {
	private Fragment fragmentCourant;			// Fragment actuellement visible par l'utilisateur
	private final Activity activityParent;		// instance de l'activity parent
	private final String nomFragment;			// nom du fragment courant
	private final Bundle bundleFragment;		// Bundle transmis au fragment par l'activity parent
	private final String ongletSelectionne;		// onglet selectionné dans l'actionbar

	
	public ActionBarTabListener( Activity activity, String nomFragment, Bundle bundleFragment, String ongletSelectionne ) {
		this.activityParent = activity;
		this.nomFragment = nomFragment;
		this.bundleFragment = bundleFragment;
		this.ongletSelectionne = ongletSelectionne;
	}

	
	@Override
	public void onTabSelected( Tab tab, FragmentTransaction fragmentTransaction ) {
		// selection d'un nouvel onglet
		if (fragmentCourant == null) {
			// ajout de l'onglet selectionné dans le bundle
			if ( bundleFragment != null ) {
				bundleFragment.putString(Constantes.ONGLET_SELECTIONNE, ongletSelectionne);
			}
			fragmentCourant = Fragment.instantiate( activityParent, nomFragment, bundleFragment );
			fragmentTransaction.add( android.R.id.content, fragmentCourant );
		} else {
			fragmentTransaction.attach(fragmentCourant);
		}
	}

	
	@Override
	public void onTabUnselected( Tab tab, FragmentTransaction fragmentTransaction ) {
		// selection d'un nouvel onglet, l'ancien est dé-selectionné
		if (fragmentCourant != null) {				
			fragmentTransaction.detach( fragmentCourant );				
		}
	}
	

	@Override
	public void onTabReselected( Tab tab, FragmentTransaction fragmentTransaction ) {}
}
