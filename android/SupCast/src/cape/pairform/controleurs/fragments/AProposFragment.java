package cape.pairform.controleurs.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import cape.pairform.modele.Constantes;


/*
 * Onglet affichant une description de l'application ou des infos sur celle-ci
 */
public class AProposFragment extends Fragment {
	
	private WebView aProposView;						// webview contenant un fichier html d'information
	

	@Override
	public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // on charge un fichier html contenu dans les assets
		aProposView = new WebView( getActivity() );
		aProposView.loadUrl( getArguments().getString(Constantes.ONGLET_SELECTIONNE, "") );
	}
	

	@Override
	public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		return aProposView;
	}
}
