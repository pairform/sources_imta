package cape.pairform.controleurs.fragments;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import org.apache.http.protocol.HTTP;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.ActionBarContextuel;
import cape.pairform.controleurs.activities.ActivityMere;
import cape.pairform.controleurs.activities.EcranCapsuleActivity;
import cape.pairform.controleurs.activities.EcranCarteActivity;
import cape.pairform.controleurs.activities.EcranProfilActivity;
import cape.pairform.controleurs.adapters.ListWithCompoundDrawableAdapter;
import cape.pairform.controleurs.adapters.ListeUtilisateursAdapter;
import cape.pairform.controleurs.dialogs.ConnexionDialog;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.controleurs.services.ModificateurLangueMessageService;
import cape.pairform.controleurs.services.ModificateurUtiliteService;
import cape.pairform.controleurs.services.RecuperateurMessagesService;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.controleurs.services.TerminatorDefiService;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.Cercle;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Message;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.PieceJointeInformation;
import cape.pairform.modele.Ressource;
import cape.pairform.modele.Tag;
import cape.pairform.modele.Utilisateur;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.ReferencesBDD;
import cape.pairform.modele.bdd.RessourceDAO;
import cape.pairform.modele.bdd.TagDAO;
import cape.pairform.modele.bdd.UtilisateurDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * Fragment associé à l'écran d'affichage des messages de toutes les ressources 
 */
public class LectureMessagesTransversaleFragment extends Fragment implements ReferencesBDD {

	private static final int[] CODE_TEXTE_LISTE_FILTRES = {R.string.button_capsule, R.string.button_utilisateur, R.string.button_reseau, R.string.button_tag};
	private static final int[] CODE_ICONE_LISTE_FILTRES = {R.drawable.icone_ressource_gris, R.drawable.icone_profil_gris, R.drawable.icone_cercles_gris, R.drawable.icone_tag_gris};
	private final int MESSAGE_CHOISI_SUR_CARTE = 9; 	   // requestCode pour le retour dela carte si l'utilisateur a choisi un message avec lequel il désire intéragir

	
	private Intent intentCourante;												// Intent permettant de transmettre des paramètres à une nouvelle activity ou un nouveau service
	private String idUtilisateurCourant;										// l'id de l'utilisateur actuellement connecté
	private MessageDAO messageDAO;												// permet l'accès à la table Message
	private UtilisateurDAO utilisateurDAO;										// permet l'accès à la table Utilisateur
	private TagDAO tagDAO;														// permet l'accès à la table Tag
	private RessourceDAO ressourceDAO;											// permet l'accès à la table Ressource
	private Message messageCourant;
	private ArrayList<Message> listeMessages = new ArrayList<Message>();		// liste de tout les messages affichés à l'écran
	private ArrayList<Message> listeNouveauxMessages;							// liste des nouveaux messages a afficher à l'écran
	private ScrollView listeMessagesScrollView;
	private LinearLayout listeMessagesLayout;
	private RelativeLayout messageLayout;
	private GestureDetectorCompat gestureDetector;
	private ImageView utilitePlus;												// icone représentant le vote positif sur un message
	private ImageView utiliteMoins;												// icone représentant le vote négatif sur un message
	private TextView utilite;													// utilité du message courant
	private boolean utiliteVariationCourante;									// variation de l'utilité du message courant
	private RelativeLayout voteUtilite;											// layout contenant l'utilité d'un message
	private Button tagCourantButton;											// bouton représentant le tag courant du message courant
	private LayoutParams layoutParams;
	private SimpleDateFormat dateFormat;
	private Date dateCreation;
	private static final int MIN_DISTANCE = 200;
	private float downX, upX, deltaX;
	private ConnexionDialog connexionDialog;									// fenêtre de connexion
	private ActionBarContextuel actionBarContextuel = new ActionBarContextuel();
	private ActionMode actionMode;
	private ProgressDialog progressDialog;										// fenêtre de chargement
	private ListWithCompoundDrawableAdapter adapterFiltre;						// adapter personnalisé associé à des filtres
	private ListeUtilisateursAdapter adapterListeUtilisateurs;					// adapter personnalisé associé à une liste d'utilisateur
	private ArrayAdapter<String> listeTagsAdapter;								// adapter associé à une liste de tags
	private ArrayAdapter<String> listeCerclesAdapter;							// adapter associé à une liste de cercles
	private ArrayList<String> listeIdCapsules = new ArrayList<String>();		// liste des id des ressources téléchargées
	private ArrayList<String> listeNomsCapsules = new ArrayList<String>();		// liste des noms des capsules, utilisé pour filtrer les messages par capsule
	private ArrayList<BitmapDrawable> listeIcones = new ArrayList<BitmapDrawable>();	// liste des icônes des ressources, utilisé pour filtrer les messages par ressource
	private ArrayList<Utilisateur> listeUtilisateurs;							// liste des utilisateurs de l'application
	private ArrayList<String> listeNomTags;										// liste des tags contenus dans les messages de l'application
	private ArrayList<Cercle> listeCercles;										// liste des cercles de l'utilisateur
	private ArrayList<String> listeNomCercles = new ArrayList<String>();		// liste du nom des cercles de l'utilisateur
	private ArrayList<String> listeLiens;										// liste des url contenu dans un message
	private HashMap<String,String> mapUriIcones = new HashMap<String, String>();// hashmap contenant la liste des URI des icones des ressources téléchargées, la clef correspond à l'id de la capsules
	private HashMap<String,String> mapNomsCapsules = new HashMap<String, String>();// hashmap contenant la liste des URI des icones des ressources téléchargées, la clef correspond à l'id de la capsules
	private AlertDialog.Builder dialogBuilder;									// fenêtre courante
	private BroadcastReceiver broadcastReceiverRecupererMessages = null;		// broadcastReceiverRecupererMessages récupèrant l'intent renvoyé par RecuperateurMessagesService
	private LectureMessagesTransversaleFragmentListener lectureMessagesTransversaleFragmentListener;
	private String languesAffichees;											// liste des langues des messages à afficher
	private int[] listeTextesLangues = null;									// liste des ressource id associés aux textes des langues
	private int[] listeIconesLangues = null;									// liste des ressource id associés aux icones des langues

	//Static car 1 seul objet service donc static limite l'utilisation mémoire
	private static ServiceGeneral service;

	private ProgressDialog telechargementDialog;	
	//HashMap qui va contenir les données renvoyé du serveur trié selon le nom serveur qui est unique
	private static HashMap<String, byte[]> donneeServeur = new HashMap<String, byte[]>();
	private File ancienFichierTmpAffichagePJGallerie; 
	//Hashmap qui va contenir certaines bitmaps des imageviews des vus de messages
	//C'est bitmap on la particularité d'etre trop grosse pour etre afficvhé. Donc on doit les scaler
	//Le faisant dans un thread pour garder une ui responsive, il nous faut garder une reference d'ou les objets ci dessous
	//Voir la méthode : ajouterPiecesJointesAuMessage pour plus de details
	private HashMap<ImageView, Bitmap> imageThumbnailStore = new HashMap<ImageView, Bitmap>();
	//Contient les chemin vers les bitmaps pour chaque imageview
	private HashMap<ImageView, String> cheminThumbnailStore = new HashMap<ImageView, String>();


	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		service = ((ActivityMere)getActivity()).service;

		// création de l'interface permettant de communiquer avec l'activity parent
		lectureMessagesTransversaleFragmentListener = (LectureMessagesTransversaleFragmentListener) getActivity();

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(getActivity(), Constantes.CLEF_ECRAN_MESSAGES_TRANSVERSALE, Constantes.AIDE_ECRAN_MESSAGES_TRANSVERSALE);

		// on recupere les preferences sur les langues
		languesAffichees = Utiles.StringListeLanguesUtilisateur( getActivity() );

		// création d'une fenêtre de chargement
		progressDialog = new ProgressDialog(getActivity());
		progressDialog.setMessage( getString(R.string.label_chargement) );
		progressDialog.show();

		ressourceDAO = BDDFactory.getRessourceDAO(getActivity().getApplicationContext());
		messageDAO = BDDFactory.getMessageDAO(getActivity().getApplicationContext());
		tagDAO = BDDFactory.getTagDAO(getActivity().getApplicationContext());

		// pour chaque capsules téléchargées
		for (Ressource ressourceCourante : ressourceDAO.getAllRessources()) {
			for (Capsule capsuleCourante : ressourceCourante.getListeCapsules()) {
				// on stock l'id et le nom de chaque capsules et l'URI du logo la ressource associée
				listeIdCapsules.add( capsuleCourante.getId() );
				listeNomsCapsules.add( capsuleCourante.getNomCourt() );

				listeIcones.add( new BitmapDrawable(
					getResources(),
					BitmapFactory.decodeFile( ressourceCourante.getUrlLogo() )
				));

				// on stock l'id (clef) et le nom (valeur) de la capsule dans une hashmap
				mapNomsCapsules.put(
					capsuleCourante.getId(),
					capsuleCourante.getNomCourt()
				);

				// on stock l'id de la capsule (clef) et l'uri du logo la ressource associée (valeur) dans une hashmap
				mapUriIcones.put(
					capsuleCourante.getId(),
					ressourceCourante.getUrlLogo()
				);
			}
		}
		ressourceDAO.close();

		// active la possibilité (pour le fragment) de manipuler le menu de l'activity parent
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup rootView, Bundle savedInstanceState) {
		listeMessagesScrollView = (ScrollView) inflater.inflate( R.layout.layout_ecran_lire_messages_trans, rootView, false );		
		listeMessagesLayout = (LinearLayout) listeMessagesScrollView.findViewById(R.id.liste_messages);

		// si l'ecran de vue transversal a été ouvert via l'écran de lecture des messages, on filtre les messages avec le tag
		if ( getArguments().getString(Constantes.CLEF_TAG_MESSAGE) != null ) {
			listeNouveauxMessages = messageDAO.getMessagesByTag( getArguments().getString(Constantes.CLEF_TAG_MESSAGE), languesAffichees );
		}
		// sinon, si l'ecran de vue transversal a été ouvert via le profil d'un utilisateur, on filtre les messages avec l'id utilisateur
		else if ( getArguments().getString(Constantes.CLEF_ID_UTILISATEUR) != null ) {
			listeNouveauxMessages = messageDAO.getMessagesByUtilisateur( getArguments().getString(Constantes.CLEF_ID_UTILISATEUR), languesAffichees );

		} else {			
			listeNouveauxMessages = messageDAO.getAllMessages(languesAffichees);
		}

		afficherMessages();
		progressDialog.dismiss();
		telechargementDialog = new ProgressDialog(getActivity());


		return listeMessagesScrollView;
	}

	// affiche une liste de messages à l'écran
	public void afficherMessages() {
		// s'il y a des messages
		if ( !listeNouveauxMessages.isEmpty() ) {
			listeMessagesLayout.removeAllViews();

			idUtilisateurCourant = getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, null);

			for( Message message : listeNouveauxMessages ) {
				messageCourant = message;				

				// si le message n'est pas une réponse
				if ( messageCourant.getIdParent().equals("0") ) {
					// on ajoute le message à l'écran
					messageLayout = (RelativeLayout) getActivity().getLayoutInflater().inflate(R.layout.message, listeMessagesLayout, false);			
					ajouterMessage(listeMessagesLayout.getChildCount(), listeMessagesLayout.getChildCount());

					listeMessages.add(messageCourant);

				} else {	// sinon, le message est une réponse
					// récupération du message parent du message courant
					message = messageDAO.getMessageById( messageCourant.getIdParent() );

					// si le message parent n'est pas un défi non-clos
					if ( message.getDefi() != Constantes.DEFI_EN_COURS || message.getAuteur().getId().equals(idUtilisateurCourant) ) {
						// on ajoute la réponse à l'écran
						messageLayout = (RelativeLayout) getActivity().getLayoutInflater().inflate(R.layout.message, listeMessagesLayout, false);			
						ajouterMessage(listeMessagesLayout.getChildCount(), listeMessagesLayout.getChildCount());

						listeMessages.add(messageCourant);
					}
				}
			}

			//On lance la méthode qui va gérer l'affichage des images
			calculEtAfficheLesThumbnails();
		}
	}


	// configure un message et l'ajout à la liste des messages
	private void ajouterMessage(int positionMessage, int position) {
		// date de creation du message
		dateFormat = new SimpleDateFormat(getString(R.string.android_format_date), Locale.US);
		dateCreation = new Date( messageCourant.getDateCreation() * 1000 );		// * 1000 : permet de transformer le timestamp (qui est en second) en millisecond
		((TextView) messageLayout.findViewById(R.id.date_message)).setText( dateFormat.format(dateCreation) );
		// id de l'auteur du message
		messageLayout.findViewById(R.id.pseudo_utilisateur).setTag( messageCourant.getAuteur().getId() );
		// role de l'auteur
		((TextView) messageLayout.findViewById(R.id.role_utilisateur)).setText( getString(
			getResources().getIdentifier("label_utilisateur_" + messageCourant.getIdRoleAuteur(), "string", getActivity().getPackageName())
		) );

		// pseudo de l'auteur du message
		((TextView) messageLayout.findViewById(R.id.pseudo_utilisateur)).setText( messageCourant.getAuteur().getPseudo() );

		//changement de la couleur du text si le message est synchro au serveur
		if(Long.valueOf(messageCourant.getId()) < 0){
			((TextView) messageLayout.findViewById(R.id.pseudo_utilisateur)).setTextColor(Color.rgb(255, 140, 0));
		}else{
			((TextView) messageLayout.findViewById(R.id.pseudo_utilisateur)).setTextColor(getResources().getColor(R.color.gris_tres_sombre));
		}

		// contenu du message
		// si le message n'a pas été supprimé (id utilisateur == "0"), on affiche le contenu du message
		if ( messageCourant.getSupprimePar().equals("0") ) {
			((TextView) messageLayout.findViewById(R.id.contenu_message)).setText( messageCourant.getContenu() );
		} else {
			((TextView) messageLayout.findViewById(R.id.contenu_message)).setText(R.string.label_message_supprime);
			((TextView) messageLayout.findViewById(R.id.contenu_message)).setTypeface( Typeface.defaultFromStyle(Typeface.ITALIC) );
			((LinearLayout) messageLayout.findViewById(R.id.tags_message)).setVisibility(View.GONE);
			((LinearLayout) messageLayout.findViewById(R.id.pjs_message)).setVisibility(View.GONE);
		}
		// id du message
		messageLayout.findViewById(R.id.contenu_message).setTag( messageCourant.getId() );
		// id du message parent
		messageLayout.setTag( messageCourant.getIdParent() );
		// index du tableau de message listeMessages
		messageLayout.findViewById(R.id.date_message).setTag( Integer.valueOf(listeMessages.size()) );

		// medaille
		if ( messageCourant.getMedaille().equals(Constantes.MEDAILLE_BRONZE) ) {
			((ImageView) messageLayout.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_bronze);			
		} else if( messageCourant.getMedaille().equals(Constantes.MEDAILLE_ARGENT) ) {
			((ImageView) messageLayout.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_argent);			
		} else if( messageCourant.getMedaille().equals(Constantes.MEDAILLE_OR) ) {
			((ImageView) messageLayout.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_or);			
		}

		// défi (en cours ou finis)
		if ( messageCourant.getDefi() == Constantes.DEFI_EN_COURS ) {
			((ImageView) messageLayout.findViewById(R.id.defi)).setImageResource( R.drawable.icone_defi_pas_fini );
		} else if ( messageCourant.getDefi() == Constantes.DEFI_FINIS ) {
			((ImageView) messageLayout.findViewById(R.id.defi)).setImageResource( R.drawable.icone_defi_fini );
		}

		// langue du message
		// si l'utilisateur est un expert, un admin, ou le posteur du message
		if(messageCourant.getAuteur().getId().equals(idUtilisateurCourant) || Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(messageCourant.getIdCapsule(), "0") ) >= Constantes.ID_EXPERT) {
			int id = getResources().getIdentifier("cape.pairform:drawable/icone_drapeau_" + Utiles.codeLangueAvecId(messageCourant.getIdLangue(), getActivity()), null, null);
			((ImageView) messageLayout.findViewById(R.id.langue_message)).setImageResource(id);
			messageLayout.findViewById(R.id.langue_message).setTag( Integer.valueOf(listeMessages.size()) );
		}

		// Action lors deu click sur le drapeau d'un message
		((ImageView) messageLayout.findViewById(R.id.langue_message)).setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View imageView) {
				messageCourant = listeMessages.get(((Integer) imageView.getTag()).intValue());
				OuvrirFenetreLangues();
			}			
		});

		// réponse valide à un défi
		if ( messageCourant.isReponseDefiValide() ) {
			((ImageView) messageLayout.findViewById(R.id.defi)).setImageResource( R.drawable.icone_reponse_defi_valide );
		}

		// icone de la ressource
		((ImageView) messageLayout.findViewById(R.id.icone_ressource)).setImageURI( Uri.parse(mapUriIcones.get(messageCourant.getIdCapsule())) );
		messageLayout.findViewById(R.id.icone_ressource).setTag( Integer.valueOf(listeMessages.size()) );

		layoutParams = messageLayout.findViewById(R.id.icone_ressource).getLayoutParams();
		layoutParams.height = (int) getResources().getDimension(R.dimen.taille_icone);
		layoutParams.width = (int) getResources().getDimension(R.dimen.taille_icone);
		messageLayout.findViewById(R.id.icone_ressource).setLayoutParams(layoutParams);

		((ImageView) messageLayout.findViewById(R.id.icone_ressource)).setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View imageView) {
				lancerEcranRessource( ((Integer) imageView.getTag()).intValue() );
			}			
		});

		// si le message n'a pas été supprimé
		if ( messageCourant.getSupprimePar().equals("0") ) {
			for ( Tag tagCourant : messageCourant.getListeTags() ) {
				tagCourantButton = (Button) getActivity().getLayoutInflater().inflate( R.layout.tag, messageLayout, false );
				tagCourantButton.setText( tagCourant.getNomTag() );
				// configuration du click sur un tag : créé une redirection vers la vue transversal filtrée avec le tag
				tagCourantButton.setOnClickListener( new OnClickListener() {
					@Override
					public void onClick(View tagView) {
						// création d'une fenêtre de chargement
						progressDialog = new ProgressDialog(getActivity());
						progressDialog.setMessage( getString(R.string.label_chargement) );
						progressDialog.show();

						// récupération et affichage des messages contenant le tag séléctionné
						listeNouveauxMessages = messageDAO.getMessagesByTag( (String) ((TextView) tagView).getText(), languesAffichees  );

						listeMessagesLayout.removeAllViews();
						listeMessages.clear();

						afficherMessages();
						progressDialog.dismiss();
					}
				});

				((LinearLayout) messageLayout.findViewById(R.id.tags_message)).addView(tagCourantButton);
			}
		}

		ajouterPiecesJointesAuMessage();

		// avatar de l'utilisateur
		((ImageView) messageLayout.findViewById(R.id.avatar_utilisateur)).setImageURI( Uri.parse(messageCourant.getAuteur().getUrlAvatar()) );
		((ImageView) messageLayout.findViewById(R.id.avatar_utilisateur)).setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(final View avatarView) {
				// test la disponibilité de l'accès Internet
				if (!Utiles.reseauDisponible( getActivity() )) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
				} else { // On peut utiliser la connection Internet
					// lancement de l'écran affichant le profil de l'utilisateur
					intentCourante = new Intent(getActivity(), EcranProfilActivity.class);
					intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, (String) ((View) avatarView.getParent()).findViewById(R.id.pseudo_utilisateur).getTag());
					startActivity(intentCourante);
				}
			}			
		});

		// utilite du message
		messageLayout.findViewById(R.id.utilite).setTag( messageCourant.getVoteUtilite() );            			

		// si l'utilisateur est connecté
		if( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).contains(Constantes.CLEF_ID_UTILISATEUR) ) {
			// si l'utilisateur est l'auteur du message
			if ( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, "").equals(messageLayout.findViewById(R.id.pseudo_utilisateur).getTag()) ) {
				// on assombrit les icones pour indiquer leur état désactivé
				messageLayout.findViewById(R.id.augmenter_utilite).setAlpha(0.25f);
				messageLayout.findViewById(R.id.baisser_utilite).setAlpha(0.25f);
			}// si l'utilisateur a déjà voté positivement le message, sinon, s'il à déjà voté négativement
			else if( messageCourant.getVoteUtilite().equals(Constantes.UTILISATEUR_A_VOTE_PLUS) ) {
				utilitePlus = (ImageView) messageLayout.findViewById(R.id.augmenter_utilite);
				utilitePlus.setImageResource(R.drawable.icone_utilite_plus_vert);

			} else if( messageCourant.getVoteUtilite().equals(Constantes.UTILISATEUR_A_VOTE_MOINS) ) {
				utiliteMoins = (ImageView) messageLayout.findViewById(R.id.baisser_utilite);
				utiliteMoins.setImageResource(R.drawable.icone_utilite_moins_rouge);
			}
		}

		messageLayout.findViewById(R.id.utilite_message).setTag( Integer.valueOf(messageCourant.getUtilite()) );
		((TextView) messageLayout.findViewById(R.id.utilite_message)).setText( String.valueOf(messageCourant.getUtilite()) );

		// création des listeners
		gestureDetector = new GestureDetectorCompat(
			getActivity(),
			new MessageOnGestureListener()
		);
		gestureDetector.setOnDoubleTapListener( new MessageOnDoubleTapListener(
			listeMessages.size(),
			messageCourant.getId(), 
			messageCourant.getIdParent(),
			position										// si le message est un réponse, position représente la position du message parent, sinon, c'est la position du message
		));
		messageLayout.findViewById(R.id.role_utilisateur).setTag(gestureDetector);

		messageLayout.setOnTouchListener( new OnTouchListener() {                			
			public boolean onTouch(View view, MotionEvent event) {
				voterUtilite(
					view, 
					((Integer) view.findViewById(R.id.icone_ressource).getTag()).intValue(),	// index du message courant
					event
				);
				return ((GestureDetectorCompat) view.findViewById(R.id.role_utilisateur).getTag()).onTouchEvent(event);    	            			
			}
		});

		// ajout du message à la liste des messages
		listeMessagesLayout.addView(messageLayout, positionMessage);
	}

	
	// ajoute la thumbnail des PJ dans le layout du message
	private void ajouterPiecesJointesAuMessage() {
		// pour chaque pièce jointe du message courant
		for(PieceJointe pieceJointe : messageCourant.getPiecesJointes()) {			
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics()),
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics())
			);

			layoutParams.setMargins(0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()), 0);

			ImageView imageView = new ImageView(this.getActivity());
			imageView.setLayoutParams(layoutParams);
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setTag(pieceJointe);
			
			//On ajoute une référence de l'imageview pour la modifier plus tard lors de la récupération de l'image correspondant au chemin stocké aussi
			cheminThumbnailStore.put(imageView, pieceJointe.getCheminVersThumbnail());
			
			//Sur le click de l'image 
			imageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View imageView) {
					ouvrirPieceJointe( (PieceJointe) imageView.getTag() );
				}
			});

			((LinearLayout) messageLayout.findViewById(R.id.pjs_message)).addView(imageView);
		}
	}
	
	
	// télécharge et affiche la pièce jointe sur laquelle l'utilisateur vient de cliquer (dans PairForm pour les vidéos et photos, en dehors pour les doc (PDF, .doc, etc.) 
	private void ouvrirPieceJointe(final PieceJointe pieceJointe) {
		//Si on a PAS le reseau
		if (!Utiles.reseauDisponible( getActivity() )) {
			//UN seul choix d'affichage : la pj asynchrone
			//Si la pj est asynchrone alors les données sont dans un fichier a l'adresse du thumbnail
			if(pieceJointe.getNomServeur() == null || pieceJointe.getNomServeur().equals("")) {

				telechargementDialog.setMessage(getString(R.string.label_dialog_recuperation));
				telechargementDialog.show();

				new Thread(new Runnable() {
					public void run() {
						//On récupère la pj
						ancienFichierTmpAffichagePJGallerie = new File(getActivity().getExternalCacheDir().getAbsolutePath(), "tmp." + pieceJointe.getExtension());
						try {
							Utiles.copieFichierVersFichier(new File(pieceJointe.getCheminVersThumbnail()), ancienFichierTmpAffichagePJGallerie);
						} catch (IOException e) {
							e.printStackTrace();
						}

						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								telechargementDialog.hide();
								
								String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(ancienFichierTmpAffichagePJGallerie).toString());
								String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
								
								// si la pj est une image ou photo
								if( PieceJointeInformation.getInstance(getActivity()).extensionPhotoAutoriseDownload(pieceJointe.getExtension()) /*||
										PieceJointeInformation.getInstance(getActivity()).extensionPhotoAutoriseDownload(pieceJointe.getExtension())*/ ) {
									//On affiche la pj dans la gallery
									Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);	
									myIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									startActivity(myIntent);
								}
								// sinon, la pj est un doc 
								else {
									/*Intent sendIntent = new Intent(Intent.ACTION_SEND);									
									sendIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									Intent chooserIntent = Intent.createChooser(sendIntent, getResources().getString(R.string.title_ouvrir_avec));
									
									// s'il y a une application qui peut ouvrir le doc
									if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
										startActivity(chooserIntent);
									} else {*/
										Toast.makeText(getActivity(), R.string.label_format_pj_pas_lisible, Toast.LENGTH_LONG).show();
									//}
								}
							}
						});
					}
				}).start();
			} else {
				//Sinon on prévient que pas de reseau
				Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
			}
		} else {
			//SI on a le reseau
			telechargementDialog.setMessage(getString(R.string.label_dialog_telechargement));
			telechargementDialog.show();

			new Thread(new Runnable() {
				public void run() {

					//Est ce que l'image est déjà téléchargé ?
					boolean contientDonnee =  donneeServeur.containsKey(pieceJointe.getNomServeur());

					//On récupère les données
					final byte[] data = contientDonnee ? donneeServeur.get(pieceJointe.getNomServeur()) : Utiles.telechargerPj(pieceJointe, getActivity());

					//Si on a télécharger les données alors on les ajoutes dans les images téléchargées
					if(!contientDonnee)
						donneeServeur.put(pieceJointe.getNomServeur(), data);

					getActivity().runOnUiThread(new Runnable() {
						public void run() {							
							//Si on a les informations de l'image
							if(data != null && data.length > 0){
								telechargementDialog.hide();

								//On met la pj dans le fichier par défaut
								ancienFichierTmpAffichagePJGallerie = Utiles.enregistrerFichier(getActivity().getExternalCacheDir().getAbsolutePath(), pieceJointe.getNomServeur(), data);
								
								String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(ancienFichierTmpAffichagePJGallerie).toString());
								String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
								
								// si la pj est une image/photos ou une vidéo
								if( PieceJointeInformation.getInstance(getActivity()).extensionPhotoAutoriseDownload(pieceJointe.getExtension()) /*|| 
										PieceJointeInformation.getInstance(getActivity()).extensionPhotoAutoriseDownload(pieceJointe.getExtension())*/ ) {
									Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.fromFile(ancienFichierTmpAffichagePJGallerie));
									myIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									startActivity(myIntent);
								}
								// sinon, la pj est un doc 
								else {
									/*Intent sendIntent = new Intent(Intent.ACTION_SEND);									
									sendIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									Intent chooserIntent = Intent.createChooser(sendIntent, getResources().getString(R.string.title_ouvrir_avec));
									
									// s'il y a une application qui peut ouvrir le doc
									if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
										startActivity(chooserIntent);
									} else {*/
										Toast.makeText(getActivity(), R.string.label_format_pj_pas_lisible, Toast.LENGTH_LONG).show();
									//}
								}
							}
						}
					});
				}
			}).start();
		}
	}
	

	// Méthode permettant de récupérer les images dans le téléphone et de les ajouter dans leur imageView
	private void calculEtAfficheLesThumbnails(){

		for(final ImageView view : cheminThumbnailStore.keySet()){

			//Si on a déjà calculer le thumbnail on ne le recalcul pas
			if(imageThumbnailStore.containsValue(view)) continue;

			new Thread(new Runnable() {
				public void run() {

					Bitmap  bmp = Utiles.fichierVersBitmap(cheminThumbnailStore.get(view));

					//Sur un android S4 une image > 4096*4096 ne se chargent pas dans une imageview pour cause de mémoire opengl.
					//720 est un minimum pour les vieux téléphone après des recherches
					//Il n'y a pas de moyen efficace actuellement pour récupérer la taille maximal allouable pour un bitmap
					if (bmp != null){
						if(bmp.getWidth() > 720 || bmp.getHeight() > 720) {
							//On redimenssione la taille pour pouvoir afficher le thumbnail
							bmp = Utiles.scaleBitmap(bmp, PieceJointeInformation.getInstance(getActivity()).getThumbnailTaille());
						}
					}

					imageThumbnailStore.put(view, bmp);

					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							view.setImageBitmap(imageThumbnailStore.get(view));
						}
					});
				}
			}).start();
		}		
	}

	
	// si le mouvement (décrit par le parametre event) correspond à un vote, on modifie le vote d'utilité
	private void voterUtilite(final View view, int indexMessageCourant, final MotionEvent event) {
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			downX = event.getX();

		case MotionEvent.ACTION_UP:		        	
			upX = event.getX();
			deltaX = downX - upX;

			if ( Math.abs(deltaX) > MIN_DISTANCE ) {
				// s'il n'y a pas de réseau internet
				if ( !Utiles.reseauDisponible(getActivity()) ) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());

				} // si l'utilisateur n'est pas connecté (l'id est stocké dans un fichier de préférence) 
				else if( !getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).contains(Constantes.CLEF_ID_UTILISATEUR) ) {
					// ouverture de la fenêtre de connexion
					connexionDialog = new ConnexionDialog(getActivity(), service);
					connexionDialog.setOnConnectedListener( new OnConnectedListener() {
						@Override
						public void onConnected(boolean success) {
							// modifie l'élément connexion du slidingMenu après une connexion
							if(success)
								lectureMessagesTransversaleFragmentListener.modifierElementConnexionSlidingMenu();
						}			    		
					});
					connexionDialog.show();
				} // si l'utilisateur n'est pas au minimum contributeur dans la ressource 
				else if ( Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(
						listeMessages.get(indexMessageCourant).getIdCapsule(), "0"
					)) < Constantes.ID_CONTRIBUTEUR ) {
						Toast.makeText(getActivity(), R.string.label_fonctionnalite_indisponible_contributeur, Toast.LENGTH_LONG).show();	            			

				}// si l'utilisateur n'essaye pas de voter pour son propre message  : si l'id de l'utilisateur connecté est égal à l'id de l'auteur du message
				else if ( !getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, "").equals( view.findViewById(R.id.pseudo_utilisateur).getTag() ) ) {		
					utilitePlus = (ImageView) view.findViewById(R.id.augmenter_utilite);
					utiliteMoins = (ImageView) view.findViewById(R.id.baisser_utilite);
					utilite = (TextView) view.findViewById(R.id.utilite_message);
					voteUtilite = (RelativeLayout) view.findViewById(R.id.utilite);

					// si l'utilisateur veut faire un vote positif et qu'il n'avait pas déjà voter positivement ce message
					if ( deltaX < 0 && !voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_VOTE_PLUS) ) {
						utilitePlus.setImageResource(R.drawable.icone_utilite_plus_vert);
						utiliteMoins.setImageResource(R.drawable.icone_utilite_moins);

						// si l'utilisateur veut faire un vote positif et qu'il n'a pas encore voté ce message
						if (voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_PAS_VOTE)) {
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() + 1) ));
						} else { 	// sinon l'utilisateur veut faire un vote positif et il avait voté négativement ce message
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() + 2) ));    		            		                		
						}
						utilite.setText( ((Integer) utilite.getTag()).toString() );
						voteUtilite.setTag(Constantes.UTILISATEUR_A_VOTE_PLUS);

						mettreAJourUtiliteMessage(
							(String) view.findViewById(R.id.contenu_message).getTag(),
							((Integer) view.findViewById(R.id.date_message).getTag()).intValue(),
							((Integer) utilite.getTag()).intValue(),
							true,
							Constantes.UTILISATEUR_A_VOTE_PLUS
						);

					} // si l'utilisateur veut faire un vote négatif et qu'il n'avait pas déjà voter négativement ce message
					else if ( deltaX > 0 && !voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_VOTE_MOINS) ) {
						utiliteMoins.setImageResource(R.drawable.icone_utilite_moins_rouge);
						utilitePlus.setImageResource(R.drawable.icone_utilite_plus);

						// si l'utilisateur veut faire un vote négatif et qu'il n'a pas encore voté ce message
						if (voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_PAS_VOTE)) {
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() - 1) ));
						} else { 	// sinon l'utilisateur veut faire un vote négatif et il n'avait voté positivement ce message
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() - 2) ));
						}
						utilite.setText( ((Integer) utilite.getTag()).toString() );
						voteUtilite.setTag(Constantes.UTILISATEUR_A_VOTE_MOINS);

						mettreAJourUtiliteMessage(
							(String) view.findViewById(R.id.contenu_message).getTag(),
							((Integer) view.findViewById(R.id.date_message).getTag()).intValue(), 
							((Integer) utilite.getTag()).intValue(),
							false,
							Constantes.UTILISATEUR_A_VOTE_MOINS
						);

					} // si l'utilisateur veut annuler son vote
					else {
						// si l'utilisateur veut annuler un vote négatif
						if ( deltaX > 0 ) {
							utiliteMoins.setImageResource(R.drawable.icone_utilite_moins);
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() + 1) ));		                	
							utiliteVariationCourante = false;
						} // l'utilisateur veut annuler un vote positif 
						else {
							utilitePlus.setImageResource(R.drawable.icone_utilite_plus);
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() - 1) ));
							utiliteVariationCourante = true;
						}

						utilite.setText( ((Integer) utilite.getTag()).toString() );
						voteUtilite.setTag(Constantes.UTILISATEUR_A_PAS_VOTE);

						mettreAJourUtiliteMessage(
							(String) view.findViewById(R.id.contenu_message).getTag(),
							((Integer) view.findViewById(R.id.date_message).getTag()).intValue(), 
							((Integer) utilite.getTag()).intValue(),
							utiliteVariationCourante,
							Constantes.UTILISATEUR_A_PAS_VOTE
						);
					}
				}
			}
		}
	}


	private void mettreAJourUtiliteMessage(String idMessage, int indexMessage, int utilite, boolean utiliteVariation, String voteUtilite) {
		messageCourant = listeMessages.get(indexMessage);
		messageCourant.setUtilite(utilite);
		messageCourant.setVoteUtilite(voteUtilite);
		messageDAO.updateMessage(messageCourant);

		intentCourante = new Intent(getActivity(), ModificateurUtiliteService.class);
		intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, idMessage);
		intentCourante.putExtra(Constantes.CLEF_MODIF_UTILITE_MESSAGE, utiliteVariation);
		getActivity().startService(intentCourante);		
	}


	// lancer l'activity EcranRessourceActivity
	private void lancerEcranRessource(int indexMessage) {
		// lancement de l'écran ressources, on transmet l'id de la ressource et le nom de la page
		intentCourante = new Intent(getActivity(), EcranCapsuleActivity.class);
		intentCourante.putExtra(Constantes.CLEF_ID_CAPSULE, listeMessages.get(indexMessage).getIdCapsule());
		intentCourante.putExtra(Constantes.CLEF_NOM_PAGE_CAPSULE, listeMessages.get(indexMessage).getNomPage());
		startActivity(intentCourante);
	}


	// télécharge les nouveaux messages et les enregistre en BDD
	private void telechargerNouveauxMessages() {
		// test la disponibilité de l'accès Internet
		if (!Utiles.reseauDisponible( getActivity() )) {
			Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
		} else { // On peut utiliser la connection Internet
			// création d'une fenêtre de chargement
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage( getString(R.string.label_chargement) );
			progressDialog.show();

			if( broadcastReceiverRecupererMessages == null ) {
				// création du broadcastReceiverRecupererMessages, il récupère l'intent renvoyé par RecuperateurMessagesService
				broadcastReceiverRecupererMessages = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						// si la requete http s'est mal exécuté, intent.getStringExtra(Constantes.CLEF_INTENT_RECUPERER_MESSAGES) vaut null
						if ( intent.getStringExtra(Constantes.CLEF_INTENT_RECUPERER_MESSAGES) != null ) {
							// on affiche les nouveaux messages
							listeMessagesLayout.removeAllViews();
							listeMessages.clear();
							listeNouveauxMessages = messageDAO.getAllMessages(languesAffichees);
							afficherMessages();
						} else {
							Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, getActivity());						        
						}
						progressDialog.dismiss();
					}
				};

				getActivity().registerReceiver(broadcastReceiverRecupererMessages, new IntentFilter( Constantes.ACTION_INTENT_RECUPERER_MESSAGES ));
			}
			// on démarre le service de récupération des nouveaux messages
			intentCourante = new Intent(getActivity(), RecuperateurMessagesService.class);
			intentCourante.putExtra(Constantes.ACTION_INTENT_RECUPERER_MESSAGES, true);
			getActivity().startService(intentCourante);
		}
	}


	@Override
	public void onPrepareOptionsMenu (Menu menu) {
		// mise à jour du menu (partagé par plusieurs fragments)
		menu.findItem(R.id.button_filtrer_messages).setVisible(true);
		menu.findItem(R.id.button_carte).setVisible(true);
	}


	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) { 		
		switch (item.getItemId()) {
		case R.id.button_recuperer_nouveaux_messages:
			telechargerNouveauxMessages();
			return true;
		case R.id.button_filtrer_messages:
			adapterFiltre = new ListWithCompoundDrawableAdapter (
				getActivity(),
				CODE_TEXTE_LISTE_FILTRES,
				CODE_ICONE_LISTE_FILTRES,
				R.layout.element_liste_filtre,
				ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
			);

			dialogBuilder = new AlertDialog.Builder(getActivity());
			dialogBuilder.setTitle(getString(R.string.title_filtrer) + " :")
				.setAdapter(adapterFiltre, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int itemSelectionne) {
						dialog.cancel();
	
						dialogBuilder = new AlertDialog.Builder(getActivity());
						dialogBuilder.setTitle(getString(R.string.title_filtrer) + " :");
	
						switch( CODE_TEXTE_LISTE_FILTRES[itemSelectionne] ) {
						// filtrer les messages par capsule
						case R.string.button_capsule:
							filtrerParCapsule();
							break;
							// filtrer les messages par utilisateur
						case R.string.button_utilisateur:
							filtrerParUtilisateur();
							break;
							// filtrer les messages par reseau
						case R.string.button_reseau:
							filtrerParCercle();
							break;
							// filtrer les messages par tag
						case R.string.button_tag:
							filtrerParTag();
							break;
						}
					}
				})
				.show();

			return true;
		case R.id.button_carte:
			Bundle bundleMessage = new Bundle();
			bundleMessage.putString(
				Constantes.CLEF_TRANSVERSALE,
				Constantes.CLEF_TRANSVERSALE
			);
			Intent i = new Intent(getActivity(), EcranCarteActivity.class);
			i.putExtras(bundleMessage);
			//Ne pas mettre getActivity avant car empeche le retour du result
			startActivityForResult(i, MESSAGE_CHOISI_SUR_CARTE);
			return true;
		}

		return false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode){
		case MESSAGE_CHOISI_SUR_CARTE:

			//Morceaux de code appelé dès qu'un utilisateur choisi un message dans la vu de carte
			//On scroll la liste jusqu'au message
			if (data != null && data.hasExtra("id_message")) {
				scrollListeJusquaMessage(data.getStringExtra("id_message"));
			}
			break;
		}
	}
	
	//Méthode permetant de scroller dans la liste des message jusqu'a celui passé en parametre.
	//Cette méthode utilise l'id du message et le compare à celui contenu dans le tag de la vue du contenu message
	private void scrollListeJusquaMessage(String id){
		int nombreMessage = listeMessagesLayout.getChildCount();

		for (int i=0; i < nombreMessage; i++){
			final View v = listeMessagesLayout.getChildAt(i);
			String idMessage = (String)v.findViewById(R.id.contenu_message).getTag();

			if(idMessage.equals(id)){
				new Handler().post(new Runnable() {
					@Override
					public void run() {
						listeMessagesScrollView.scrollTo(0, v.getTop());
					}
				});
				break;
			}
		}
	}

	// ouvre une fenêtre permettant de choisir une capsule avec laquelle filtrer les messages
	private void filtrerParCapsule() {
		// on ouvre une fenetre contenant la liste des capsules téléchargées sur le mobile
		adapterFiltre = new ListWithCompoundDrawableAdapter(
			getActivity(),
			listeNomsCapsules,
			listeIcones,
			R.layout.element_liste_filtre,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);

		dialogBuilder.setAdapter(adapterFiltre, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				// création d'une fenêtre de chargement
				progressDialog = new ProgressDialog(getActivity());
				progressDialog.setMessage( getString(R.string.label_chargement) );
				progressDialog.show();
				dialog.cancel();

				listeNouveauxMessages = messageDAO.getMessagesByCapsule( listeIdCapsules.get(itemSelectionne), languesAffichees );
				listeMessagesLayout.removeAllViews();
				listeMessages.clear();

				afficherMessages();
				progressDialog.dismiss();
			}
		})
		.show();
	}


	// ouvre une fenêtre permettant de choisir un utilisateur avec lequelle filtrer les messages
	private void filtrerParUtilisateur() {
		// on ouvre une fenetre contenant la liste utilisateurs
		// si la liste d'utilisateur n'existe pas
		if (listeUtilisateurs == null) {
			utilisateurDAO = BDDFactory.getUtilisateurDAO(getActivity().getApplicationContext());

			// récupération de tous les utilisateurs ayant publié un message
			listeUtilisateurs = utilisateurDAO.getAllUtilisateurs();

			adapterListeUtilisateurs = new ListeUtilisateursAdapter(
				getActivity(),
				R.layout.element_liste_filtre,
				listeUtilisateurs
			);

			utilisateurDAO.close();
		}
		// création de la fenêtre contenant la liste des utilisateur
		dialogBuilder.setAdapter(adapterListeUtilisateurs, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				// création d'une fenêtre de chargement
				progressDialog = new ProgressDialog(getActivity());
				progressDialog.setMessage( getString(R.string.label_chargement) );
				progressDialog.show();
				dialog.cancel();

				// récupération et affichage des messages de l'utilisateur séléctionné
				listeNouveauxMessages = messageDAO.getMessagesByUtilisateur( listeUtilisateurs.get(itemSelectionne).getId(), languesAffichees );

				listeMessagesLayout.removeAllViews();
				listeMessages.clear();

				afficherMessages();
				progressDialog.dismiss();
			}
		})
		.show();
	}


	// ouvre une fenêtre permettant de choisir un cercle avec lequelle filtrer les messages
	private void filtrerParCercle() {
		// si un utilisateur est connecté (l'id est stocké dans un fichier de préférence)
		if ( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).contains(Constantes.CLEF_ID_UTILISATEUR) ) {
			// test la disponibilité de l'accès Internet
			if ( !Utiles.reseauDisponible( getActivity() ) ) {
				Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
			} else { // On peut utiliser la connection Internet
				// on ouvre une fenetre contenant la liste des cercles
				// si la liste des cercles n'existe pas
				if (listeCercles == null) {
					// récupération de tous les cercles de l'utilisateur
					// création d'une fenêtre de chargement
					progressDialog = new ProgressDialog( getActivity() );
					progressDialog.setMessage( getString(R.string.label_chargement) );
					progressDialog.show();

					// on bloque l'orientation de l'écran dans son orientation actuelle
					if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
						getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
					} else {
						getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
					}
					// création d'un nouveau Thread
					Thread thread = new Thread(new Runnable() {
						public void run() {
							// Envoie d'une requete HTTP récupérant les cercles et classes de l'utilisateur
							final String stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_AFFICHER_CERCLES, null, getActivity());

							// si la requete http s'est mal exécuté, stringJSON vaut null
							if ( stringJSON != null ) {
								listeCercles = ParserJSON.parserCercles(stringJSON);
							}

							for (Cercle cercleCourant : listeCercles) {
								listeNomCercles.add( cercleCourant.getNomCercle() );
							}

							getActivity().runOnUiThread( new Runnable() {
								public void run() {
									// si la requete http s'est mal exécuté, stringJSON vaut null
									if ( stringJSON != null ) {
										// on affiche les cercles et classes
										listeCerclesAdapter = new ArrayAdapter<String>(getActivity(), R.layout.element_liste_defaut, listeNomCercles);
										ouvrirFenetreListeCercles();
									}
									// on ferme la fenetre de chargement et on désactive l'orientation forcée de l'écran
									progressDialog.dismiss();
									getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);	
								}
							});
						}
					});

					thread.start();					
				} else {
					ouvrirFenetreListeCercles();
				}
			}
		} else {
			// ouverture de la fenêtre de connexion
			connexionDialog = new ConnexionDialog( getActivity(), service);
			connexionDialog.setOnConnectedListener( new OnConnectedListener() {
				@Override
				public void onConnected(boolean success) {
					if (success)
						filtrerParCercle();
				}
			});
			connexionDialog.show();
			//Vous devez être connecté pour choisir un cercle 
		}
	}


	// ouvre une fenêtre permettant de choisir un tag avec lequelle filtrer les messages
	private void filtrerParTag() {
		// on ouvre une fenetre contenant la liste des tags
		// si la liste des noms tags n'existe pas
		if (listeNomTags == null) {
			// récupération de tous les tags contenus dans les message
			listeNomTags = tagDAO.getAllNomsTags();

			listeTagsAdapter = new ArrayAdapter<String>(getActivity(), R.layout.element_liste_defaut, listeNomTags);
		}
		// création de la fenêtre contenant la liste des utilisateur
		dialogBuilder.setAdapter(listeTagsAdapter, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				// création d'une fenêtre de chargement
				progressDialog = new ProgressDialog(getActivity());
				progressDialog.setMessage( getString(R.string.label_chargement) );
				progressDialog.show();
				dialog.cancel();

				// récupération et affichage des messages contenant le tag séléctionné
				listeNouveauxMessages = messageDAO.getMessagesByTag( listeNomTags.get(itemSelectionne), languesAffichees );

				listeMessagesLayout.removeAllViews();
				listeMessages.clear();

				afficherMessages();
				progressDialog.dismiss();
			}
		})
		.show();
	}

	// ouvre une fenetre contenant la liste des cercles de l'utilisateur
	private void ouvrirFenetreListeCercles() {
		// création de la fenêtre contenant la liste des utilisateur
		dialogBuilder.setAdapter(listeCerclesAdapter, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				// création d'une fenêtre de chargement
				progressDialog = new ProgressDialog(getActivity());
				progressDialog.setMessage( getString(R.string.label_chargement) );
				progressDialog.show();
				dialog.cancel();

				// récupération et affichage des messages contenant le tag séléctionné
				listeNouveauxMessages = messageDAO.getMessagesByCercle( listeCercles.get(itemSelectionne), languesAffichees );

				listeMessagesLayout.removeAllViews();
				listeMessages.clear();

				afficherMessages();
				progressDialog.dismiss();
			}
		})
		.show();
	}


	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		if ( broadcastReceiverRecupererMessages != null ) {
			getActivity().unregisterReceiver( broadcastReceiverRecupererMessages );
		}
		messageDAO.close();
		tagDAO.close();
	}

	@Override
	public void onResume() {
		super.onResume();

		if(ancienFichierTmpAffichagePJGallerie != null)
			ancienFichierTmpAffichagePJGallerie.delete();
	}

	@Override
	public void onDetach() {
		super.onDetach();

		if(ancienFichierTmpAffichagePJGallerie != null)
			ancienFichierTmpAffichagePJGallerie.delete();

		if(progressDialog != null)
			progressDialog.dismiss();

		if(telechargementDialog != null)
			telechargementDialog.dismiss();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		donneeServeur.clear();
	}

	// interface permettant de communiquer avec l'activity parent
	public interface LectureMessagesTransversaleFragmentListener {
		// modifie l'élément connexion du slidingMenu après une connexion
		public void modifierElementConnexionSlidingMenu();
	}


	/* ***********************************************************
	 * 
	 * Listeners réagissant aux interactions avec les messages (clic, clic long, swipe, etc.)
	 * 
	 * **********************************************************/

	private class MessageOnGestureListener implements GestureDetector.OnGestureListener {

		public MessageOnGestureListener() {}

		@Override
		public void onLongPress(MotionEvent event) {}		
		@Override
		public boolean onDown(MotionEvent event) {return true;}							
		@Override
		public boolean onFling(MotionEvent downEvent, MotionEvent moveEvent, float arg2, float arg3) {return false;}        			
		@Override
		public boolean onScroll(MotionEvent startEvent, MotionEvent moveEvent, float arg2, float arg3) {return false;}
		@Override
		public void onShowPress(MotionEvent event) {}
		@Override
		public boolean onSingleTapUp(MotionEvent event) {return false;}
	}


	private class MessageOnDoubleTapListener implements GestureDetector.OnDoubleTapListener {
		private int indexMessage;			// index du message dans la liste des messages listeMessages
		private String idMessage;			// id du message sur lequel l'utilisateur à cliquer

		public MessageOnDoubleTapListener(int indexMessage, String idMessage, String idMessageParent, int positionMessage) {
			this.indexMessage = indexMessage;
			this.idMessage = idMessage;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent event) {
			// on affiche le menu contextuel
			actionMode = getActivity().startActionMode(actionBarContextuel);    		
			actionMode.getMenuInflater().inflate(R.menu.menu_contextuel_ecran_messages, actionMode.getMenu());

			// on désactive certaines fonctionnalité du menu contextuel
			actionMode.getMenu().findItem(R.id.button_valider_reponse_defi).setVisible(false);
			actionMode.getMenu().findItem(R.id.button_attribuer_medaille).setVisible(false);
			//actionMode.getMenu().findItem(R.id.button_changer_langue_message).setVisible(false);
			actionMode.getMenu().findItem(R.id.button_ajouter_tag).setVisible(false);
			actionMode.getMenu().findItem(R.id.button_supprimer_message).setVisible(false);
			actionMode.getMenu().findItem(R.id.button_repondre).setVisible(false);

			// récupération de l'id de l'utilisateur et du message courant
			String idUtilisateurCourant = getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, null);
			messageCourant = listeMessages.get(indexMessage);

			// on récupère les liens contenues dans le message
			listeLiens = Utiles.obtenirLiens( messageCourant.getContenu() );

			// configuration du clic sur le bouton obtenir liens
			// s'il n'y a pas de liens dans le message
			if ( listeLiens.isEmpty() ) {
				actionMode.getMenu().findItem(R.id.button_obtenir_liens).setVisible(false);
			} else {
				actionMode.getMenu().findItem(R.id.button_obtenir_liens).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {       		
						TextView textViewLiens = (TextView) getActivity().getLayoutInflater().inflate(R.layout.layout_dialog_texte, null);

						for (String lien : listeLiens) {
							textViewLiens.setText( textViewLiens.getText() + "\n" + lien + "\n");
						}

						// ouverture d'une fenêtre contenant les liens
						dialogBuilder = new AlertDialog.Builder(getActivity());
						dialogBuilder.setTitle(R.string.title_liens)
						.setView(textViewLiens)
						.show();

						actionMode.finish();
						return false;
					}  	        		
				});
			}

			// configuration du clic sur le bouton changer la langue du message
			// si l'utilisateur est un expert, un admin, ou le posteur du message
			if(messageCourant.getAuteur().getId().equals(idUtilisateurCourant) || Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(messageCourant.getIdCapsule(), "0") ) >= Constantes.ID_EXPERT) {
				actionMode.getMenu().findItem(R.id.button_changer_langue_message).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						OuvrirFenetreLangues();       		
						return false;
					}  	        		
				});
			} else {
				actionMode.getMenu().findItem(R.id.button_changer_langue_message).setVisible(false);
			}

			// configuration du clic sur le bouton affichant les informations du message
			actionMode.getMenu().findItem(R.id.button_infos).setOnMenuItemClickListener( new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					OuvrirFenetreInfos();
					return false;
				}  	        		
			}); 

			// configuration du bouton pour terminer un défi (invisible si le message n'est pas un défi)
			// si le message n'est pas un défi en cours OU si le message est un défi en cour mais que l'utilisateur n'est pas l'auteur du défi)
			if ( messageCourant.getDefi() != Constantes.DEFI_EN_COURS || (messageCourant.getDefi() == Constantes.DEFI_EN_COURS && !messageCourant.getAuteur().getId().equals(idUtilisateurCourant)) ) {
				actionMode.getMenu().findItem(R.id.button_terminer_defi).setVisible(false);
			} else {
				actionMode.getMenu().findItem(R.id.button_terminer_defi).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						messageCourant.setDefi(Constantes.DEFI_FINIS);
						messageDAO.updateMessage(messageCourant);

						intentCourante = new Intent(getActivity(), TerminatorDefiService.class);
						intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, idMessage);
						getActivity().startService(intentCourante);

						actionMode.finish();
						return false;
					}  	        		
				});
			}

			// configuration du clic sur le bouton partager contenu du message
			// si le message a été supprimé (id utilisateur != "0")
			if ( !messageCourant.getSupprimePar().equals("0") ) {
				actionMode.getMenu().findItem(R.id.button_partager_message).setVisible(false);
			} else {
				// création du sous-menu affichant les possibilités (réseaux sociaux) de partage du message
				intentCourante = new Intent(Intent.ACTION_SEND);
				intentCourante.setType( HTTP.PLAIN_TEXT_TYPE );
				intentCourante.putExtra(
						Intent.EXTRA_TEXT,
						// mise en forme du texte à afficher
						"#" + getResources().getString(R.string.app_name) +
						" #" + mapNomsCapsules.get( messageCourant.getIdCapsule() ) +
						" >" + messageCourant.getAuteur().getPseudo() + " : " + messageCourant.getContenu()
						);

				ShareActionProvider shareActionProvider = (ShareActionProvider) actionMode.getMenu().findItem(R.id.button_partager_message).getActionProvider();
				shareActionProvider.setShareIntent(intentCourante);
			}

			return true;
		}


		// ouvre la fenêtre affichant les informations sur le message
		private void OuvrirFenetreInfos() {        		
			TextView textViewTags = (TextView) getActivity().getLayoutInflater().inflate(R.layout.layout_dialog_texte, null);

			dateCreation = new Date( messageCourant.getDateCreation() * 1000 );
			dateFormat = new SimpleDateFormat(getString(R.string.android_format_date), Locale.US);
			String date = dateFormat.format(dateCreation);
			dateFormat = new SimpleDateFormat(getString(R.string.android_format_heure), Locale.US);
			String heure = dateFormat.format(dateCreation);

			String content = "";
			content += getString(R.string.label_message_poste_le) + " " + date + " " + getString(R.string.label_a) + " " + heure + "\n";
			content += getString(R.string.label_langue) + " : " + Utiles.nomLangueAvecId(String.valueOf(messageCourant.getIdLangue()), getActivity());

			// Affichage des tags
			// si l'utilisateur est un expert ou un admin
			if (!(Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(messageCourant.getIdCapsule(), "0") ) < Constantes.ID_EXPERT )) {
				if ( !messageCourant.getListeTags().isEmpty() ) {
					content += "\n" + getString(R.string.label_auteurs_tag) + " :";
					// pour chaque tags contenues dans le message
					for (Tag tagCourant : messageCourant.getListeTags()) {
						// on affiche pour chaque tag le pseudo de l'auteur associé
						content += "\n" + "- " + tagCourant.getNomTag() + " : " + tagCourant.getPseudoAuteur();
					}
				}
			}

			textViewTags.setText(content);

			// ouverture d'une fenêtre contenant les infos sur les tags
			dialogBuilder = new AlertDialog.Builder(getActivity());
			dialogBuilder.setTitle(R.string.title_informations)
			.setView(textViewTags)
			.show();

			actionMode.finish();
		}

		@Override
		public boolean onDoubleTap(MotionEvent event) {return true;}
		@Override
		public boolean onDoubleTapEvent(MotionEvent event) {return false;}
	}


	// ouvre la fenêtre de modification de la langue d'un message
	private void OuvrirFenetreLangues() {
		listeTextesLangues = Utiles.idArrayWithRIdArray(getActivity(), R.array.langue_true_values_string);
		listeIconesLangues = Utiles.idArrayIconesLangues(getActivity());

		ListWithCompoundDrawableAdapter adapterLangues = new ListWithCompoundDrawableAdapter(
				getActivity(),
				listeTextesLangues,
				listeIconesLangues,
				R.layout.element_liste_utilisateurs,
				ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
				);

		dialogBuilder = new AlertDialog.Builder( getActivity() );
		dialogBuilder.setTitle(R.string.label_langue_du_message)
		.setAdapter(adapterLangues, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				modifierLangue(Utiles.idLangueAvecTabIndex(itemSelectionne, getActivity()));
				dialog.cancel();
				if(actionMode != null)
					actionMode.finish();
			}
		})
		.show();    		
	}


	// modification de la langue d'un message
	private void modifierLangue(int langueID) {
		messageCourant.setIdLangue( String.valueOf(langueID) );
		messageDAO.updateMessage(messageCourant);

		intentCourante = new Intent(getActivity(), ModificateurLangueMessageService.class);
		intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, messageCourant.getId());
		intentCourante.putExtra(Constantes.CLEF_WEBSERVICE_IDLANGUE_MESSAGE, String.valueOf(langueID));			
		getActivity().startService(intentCourante);

		// Actualise la liste des messages
		listeMessagesLayout.removeAllViews();
		listeMessages.clear();
		afficherMessages();
	}
}
