package cape.pairform.controleurs.fragments;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.ShareActionProvider;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import cape.pairform.R;
import cape.pairform.controleurs.ActionBarContextuel;
import cape.pairform.controleurs.activities.ActivityMere;
import cape.pairform.controleurs.activities.EcranCarteActivity;
import cape.pairform.controleurs.activities.EcranProfilActivity;
import cape.pairform.controleurs.activities.EcranVueTransversaleActivity;
import cape.pairform.controleurs.adapters.ListWithCompoundDrawableAdapter;
import cape.pairform.controleurs.dialogs.ConnexionDialog;
import cape.pairform.controleurs.dialogs.ConnexionDialog.OnConnectedListener;
import cape.pairform.controleurs.dialogs.OptionsPublicationDialog;
import cape.pairform.controleurs.services.AjouteurTagService;
import cape.pairform.controleurs.services.AttributeurMedailleService;
import cape.pairform.controleurs.services.ModificateurLangueMessageService;
import cape.pairform.controleurs.services.ModificateurUtiliteService;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.controleurs.services.TerminatorDefiService;
import cape.pairform.controleurs.services.ValidateurReponseDefiService;
import cape.pairform.controleurs.services.communication.IMessage;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.Cercle;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Message;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.PieceJointeInformation;
import cape.pairform.modele.PieceJointeType;
import cape.pairform.modele.Tag;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.CapsuleDAO;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.ReferencesBDD;
import cape.pairform.modele.bdd.RessourceDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;
import cape.pairform.utiles.modules.geolocalisation.GeolocalisationInterface;
import cape.pairform.utiles.modules.geolocalisation.GeolocalisationModule;


/*
 * ecran de lecture des messages d'une capsule (sur la capsule, sur une page de la capsule ou sur des OA d'une page)
 */
public class LectureMessagesFragment extends Fragment implements ReferencesBDD, IMessage {

	private static final int[] CODE_TEXTE_LISTE_PIECES_JOINTES = {R.string.button_prendre_photo, R.string.button_ajouter_image, R.string.button_prendre_video, R.string.button_ajouter_video};
	private static final int[] CODE_ICONE_LISTE_PIECES_JOINTES = {R.drawable.icone_prendre_photo_gris, R.drawable.icone_ajouter_image_gris, R.drawable.icone_prendre_photo_gris, R.drawable.icone_ajouter_video_gris};

	private Intent intentCourante;							// Intent permettant de transmettre des paramètres à une nouvelle activity ou un nouveau service
	private String stringJSON;								// contenu d'une réponse JSON d'une requête http
	private MessageDAO messageDAO;
	private CapsuleDAO capsuleDAO;
	private RessourceDAO ressourceDAO;
	private Cursor curseur;
	private Capsule capsuleCourante;						// capsule actuellement consulté par l'utilisateur
	private int nbMessageFils;
	private Message messageCourant;
	private ArrayList<Message> listeMessages = new ArrayList<Message>();						// liste de tout les messages affichés à l'écran
	private ArrayList<Message> listeNouveauxMessages;		// liste des nouveaux messages a afficher à l'écran
	private ArrayList<CharSequence> listeTagsNouveauMessage = new ArrayList<CharSequence>();	// liste des tags associés au nouveau message
	private ArrayList<Cercle> listeCerclesNouveauMessage = new ArrayList<Cercle>();				// liste des tags associés au nouveau message
	private LinearLayout layoutEcranLectureMessages;		// layout de l'écran
	private ScrollView listeMessagesScrollView;				// scrollview contenant la liste des messages
	private LinearLayout listeMessagesLayout;				// layout contenant la liste des messages
	private RelativeLayout messageLayout;					// layout d'un message
	private Spinner spinnerLangues;							// spinner représentant la liste des langues
	private String nomPage = "";							// nom de la page sur laquelle se trouve les messages (vaut "" quand on affiche les messages sur la capsule)
	private String idLangueRessource;						// id de la langue de la ressource courante
	private String idCapsule;								// id de la capsule actuellement ouverte
	private String nomTagPage;								// tag sur lequel se trouve les messages à afficher
	private String numOccurenceTagPage;						// occurence du tag sur lequel se trouve les messages à afficher
	private String idParentCourant;							// id du message parent;
	private GestureDetectorCompat gestureDetector;
	private ImageView utilitePlus;							// icone représentant le vote positif sur un message
	private ImageView utiliteMoins;							// icone représentant le vote négatif sur un message
	private TextView utilite;								// utilité du message courant
	private boolean utiliteVariationCourante;				// variation de l'utilité du message courant
	private RelativeLayout voteUtilite;						// layout contenant l'utilité d'un message
	private Button tagCourantButton;						// tag courant du message courant
	private int positionMessageCourante;
	private SimpleDateFormat dateFormat;
	private Date dateCreation;
	private static final int MIN_DISTANCE = 200;
	private float downX, upX, deltaX;
	private EditText ajouterTagView;						// nouveau tag à ajouter à un message donné
	private ArrayList<String> listeNomTags = new ArrayList<String>();	// liste du nom des tags associés au message
	private int[] listeTextesMedailles = null;				// liste des capsule id associés aux textes des médailes (or, argent, bronze, supprimer la médaille)
	private int[] listeIconesMedailles = null;				// liste des capsule id associés aux icones des médailes
	private int[] listeTextesLangues = null;				// liste des capsule id associés aux textes des langues
	private int[] listeIconesLangues = null;				// liste des capsule id associés aux icones des langues
	private ArrayList<String> listeLiens;					// liste des url contenu dans un message
	private ArrayAdapter<CharSequence> adapterLanguesNouveauMessage;	// adapter lié à la liste des langues d'un nouveau message
	private String erreursSuppressionMessage = null;		// erreur retourné par le web service de création de compte
	private ConnexionDialog connexionDialog;				// fenêtre de connexion
	private OptionsPublicationDialog optionsPublicationDialog;			// fenêtre affichant les options de publication (tags, visibilité, défi, etc.)
	private AlertDialog.Builder dialogBuilder;				// fenêtre courante
	private ProgressDialog progressDialog;					// fenêtre de chargement
	private ActionBarContextuel actionBarContextuel = new ActionBarContextuel();
	private ActionMode actionMode;
	private String languesAffichees;						// chaine des langues des messages à afficher
	private String idUtilisateurCourant;					// identifiant de l'utilisateur courant
	private String idLangueNouveauMessage;					// id de la langue selectionnée pour le nouveau message
	private String idMessageParentNouveauMessage = "0";		// id du message parent du nouveau message
	private EditText texteNouveauMessage;					// texte d'un nouveau message
	private boolean NouveauMessageEstDefi = false;			// true si le nouveau message est un defi, false sinon
	private GeolocalisationModule geoMod;
	private Location locationCourante = null;
	private boolean geolocaliserMessage = false;
	private static HashMap<String, byte[]> donneeServeur = new HashMap<String, byte[]>();	//HashMap qui va contenir les données renvoyé du serveur trié selon le nom serveur qui est unique
	private File ancienFichierTmpAffichagePJGallerie; 
	private ProgressDialog telechargementDialog;
	private Uri uriPJ;
	private final int REQUETE_CAPTURE_IMAGE = 1;   			// requestCode de activity qui est lancé pour faire la photo 
	private final int REQUETE_CAPTURE_VIDEO = 2;   			// requestCode de activity qui est lancé pour faire la video 
	private final int REQUETE_CHOIX_IMAGE = 3; 	   			// requestCode de activity que est lancé pour choisir une image dans file chooser
	private final int REQUETE_CHOIX_VIDEO = 4; 	   			// requestCode de activity que est lancé pour choisir une video dans file chooser
	private final int MESSAGE_CHOISI_SUR_CARTE = 9;			// requestCode pour le retour dela carte si l'utilisateur a choisi un message avec lequel il désire intéragir

	private ArrayList<PieceJointe> piecesJointesMessage = new ArrayList<PieceJointe>();
	//Hashmap qui va contenir certaines bitmaps des imageviews des vus de messages
	//C'est bitmap on la particularité d'etre trop grosse pour etre afficvhé. Donc on doit les scaler
	//Le faisant dans un thread pour garder une ui responsive, il nous faut garder une reference d'ou les objets ci dessous
	//Voir la méthode : ajouterPiecesJointesAuMessage pour plus de details
	private HashMap<ImageView, Bitmap> imageThumbnailStore = new HashMap<ImageView, Bitmap>();
	//Contient les chemin vers les bitmaps pour chaque imageview
	private HashMap<ImageView, String> cheminThumbnailStore = new HashMap<ImageView, String>();

	//Static car 1 seul objet service donc static limite l'utilisation mémoire
	private static ServiceGeneral service;

	
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		service = ((ActivityMere)getActivity()).service;

		// récupération de l'id de l'utilisateur courant
		idUtilisateurCourant = getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, null);

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(getActivity(), Constantes.CLEF_ECRAN_MESSAGES, Constantes.AIDE_ECRAN_MESSAGES);

		// on recupere les preferences sur les langues
		languesAffichees = Utiles.StringListeLanguesUtilisateur( getActivity() );

		nomPage = getArguments().getString(Constantes.CLEF_NOM_PAGE_CAPSULE, "");
		idCapsule = getArguments().getString(Constantes.CLEF_ID_CAPSULE);
		nomTagPage = getArguments().getString(Constantes.CLEF_NOM_TAG_PAGE_MESSAGE);
		numOccurenceTagPage = getArguments().getString(Constantes.CLEF_NUM_OCCURENCE_TAG_PAGE_MESSAGE);

		messageDAO = BDDFactory.getMessageDAO(getActivity().getApplicationContext());

		// on récupère les informations sur la capsule
		capsuleDAO = BDDFactory.getCapsuleDAO(getActivity().getApplicationContext());
		curseur = capsuleDAO.getCapsuleById( idCapsule );

		if ( curseur.moveToFirst() ) {
			capsuleCourante = new Capsule(
				curseur.getString(NUM_COLONNE_ID),
				curseur.getString(NUM_COLONNE_RESSOURCE_CAPSULE),
				curseur.getString(NUM_COLONNE_ID_VISIBILITE),
				curseur.getString(NUM_COLONNE_NOM_COURT_CAPSULE),
				curseur.getString(NUM_COLONNE_NOM_LONG_CAPSULE),
				curseur.getString(NUM_COLONNE_URL_MOBILE),
				curseur.getString(NUM_COLONNE_DESCRIPTION_CAPSULE),
				curseur.getString(NUM_COLONNE_LICENCE),
				curseur.getInt(NUM_COLONNE_TAILLE),
				curseur.getString(NUM_COLONNE_AUTEURS),
				curseur.getLong(NUM_COLONNE_DATE_CREATION_CAPSULE),
				curseur.getLong(NUM_COLONNE_DATE_EDITION_CAPSULE),
				Boolean.parseBoolean( curseur.getString(NUM_COLONNE_EST_TELECHARGEE_CAPSULE) ),
				curseur.getString(NUM_COLONNE_SELECTEURS_CAPSULE)
			);
		}

		// récupération de la langue de la ressource
		ressourceDAO = BDDFactory.getRessourceDAO(getActivity().getApplicationContext());
		idLangueRessource = ressourceDAO.getLangueRessource(capsuleCourante.getIdRessource() );

		curseur.close();
		capsuleDAO.close();
		ressourceDAO.close();

		// active la possibilité (pour le fragment) de manipuler le menu de l'activity parent
		setHasOptionsMenu(true);
	}

	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		service.desinscrireMessage(this);
	}

	
	@Override
	public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		service.inscrireMessage(this);

		//On initialise le booléen controlant la geolocalisation selon la valeur dans les préférences
		geolocaliserMessage = this.getActivity().getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, Context.MODE_PRIVATE).getBoolean(Constantes.CLEF_GEOLOCALISATION_MESSAGE, false);

		// création de la vue
		layoutEcranLectureMessages = (LinearLayout) inflater.inflate( R.layout.layout_ecran_lire_messages, container, false );
		listeMessagesScrollView = (ScrollView) layoutEcranLectureMessages.findViewById(R.id.scrollview_liste_messages);		
		listeMessagesLayout = (LinearLayout) layoutEcranLectureMessages.findViewById(R.id.liste_messages);

		// texte d'un nouveau message
		texteNouveauMessage = (EditText) layoutEcranLectureMessages.findViewById(R.id.nouveau_message);

		//On rajoute un évènement pour lancer la geolocalisation à l'écriture du message
		texteNouveauMessage.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus && geolocaliserMessage){
					lancerGeolocalisation();
				}
			}
		});

		// gestion de la liste des langues
		spinnerLangues = (Spinner) layoutEcranLectureMessages.findViewById(R.id.spinner_langues);
		adapterLanguesNouveauMessage = ArrayAdapter.createFromResource(inflater.getContext(), R.array.langue_true_values_string, android.R.layout.simple_spinner_item);
		adapterLanguesNouveauMessage.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerLangues.setAdapter(adapterLanguesNouveauMessage);

		// Recuperation de l'id de la langue de la ressource actuelle
		String codeLangueCapsule = Utiles.codeLangueAvecId(idLangueRessource, getActivity());

		// Permet de selectionner la langue principale de l'utilisateur (anglais par défaut)
		spinnerLangues.setSelection(Utiles.tabIndexLangueAvecCode(codeLangueCapsule, getActivity()));
		idLangueNouveauMessage = Utiles.idLangueAvecCode(codeLangueCapsule, getActivity());

		// Action lors de la selection d'une langue
		spinnerLangues.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				idLangueNouveauMessage = String.valueOf(Utiles.idLangueAvecTabIndex(position, getActivity()));
			}
			public void onNothingSelected(AdapterView<?> parentView) {}
		});

		// bouton de publication
		((ImageButton) layoutEcranLectureMessages.findViewById(R.id.button_publier_message)).setOnClickListener(new OnClickListener () {
			@Override
			public void onClick(View view) {	// test la disponibilité de l'accès Internet
				// si le texte du message est vide
				if ( ((EditText) layoutEcranLectureMessages.findViewById(R.id.nouveau_message) ).getText().toString().trim().isEmpty() ) {
					Toast.makeText(getActivity(), R.string.label_erreur_saisie_message, Toast.LENGTH_LONG).show();
				} else {

					//Si on a pas de reseau on ajoute le message dans la file d'attente et on l'envoie  la prochaine connection
					if(!service.internetActif()) {
						publierMessageVerificationGeolloc();
					} else{
						//Si on a du reseau et lié au serveur
						if(service.estLieAuServeur())
							publierMessageVerificationGeolloc();
						else {
							// ouverture de la fenêtre de connexion
							connexionDialog = new ConnexionDialog(getActivity(), service);
							connexionDialog.setOnConnectedListener( new OnConnectedListener() {
								@Override
								public void onConnected(boolean success) {
									publierMessageVerificationGeolloc();
								}
							});
							connexionDialog.show();
						}
					}
				}
			}
		});

		// bouton d'options de publication
		((ImageButton) layoutEcranLectureMessages.findViewById(R.id.button_option_message)).setOnClickListener(new OnClickListener () {
			@Override
			public void onClick(View view) {						
				// ouverture de la fenêtre de connexion
				optionsPublicationDialog = new OptionsPublicationDialog(
					getActivity(),
					service,
					Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ),
					listeTagsNouveauMessage,
					listeCerclesNouveauMessage,
					NouveauMessageEstDefi,
					!idMessageParentNouveauMessage.equals("0"),		// true si le message est une réponse, false sinon
					geolocaliserMessage
				);
				optionsPublicationDialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						listeTagsNouveauMessage = optionsPublicationDialog.getListeTags();
						listeCerclesNouveauMessage = optionsPublicationDialog.getListeCercles();
						NouveauMessageEstDefi = optionsPublicationDialog.estDefi();
						geolocaliserMessage = optionsPublicationDialog.geolocaliserMessage();

						if(geolocaliserMessage) {
							if(locationCourante == null)
								lancerGeolocalisation();
						} else {
							locationCourante = null;
						}
						dialog.cancel();			    		
					}
				});
				optionsPublicationDialog.show();
			}
		});

		// bouton d'options d'ajout de pieces jointes
		((ImageButton) layoutEcranLectureMessages.findViewById(R.id.button_photo_message)).setOnClickListener(new OnClickListener () {
			@Override
			public void onClick(View view) {
				ListWithCompoundDrawableAdapter adapterPiecesJointes = new ListWithCompoundDrawableAdapter (
					getActivity(),
					CODE_TEXTE_LISTE_PIECES_JOINTES,
					CODE_ICONE_LISTE_PIECES_JOINTES,
					R.layout.element_liste_filtre,
					ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
				);
				 
				dialogBuilder = new AlertDialog.Builder(getActivity());
				dialogBuilder.setTitle(getString(R.string.title_dialog_ajout_pj))
					.setAdapter(adapterPiecesJointes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int itemSelectionne) {
							dialog.cancel();
	
							switch( CODE_TEXTE_LISTE_PIECES_JOINTES[itemSelectionne] ) {
								case R.string.button_prendre_photo:
									priseDePhoto();
									break;
								case R.string.button_ajouter_image:
									ouvreAjoutChoixPieceJointe(REQUETE_CHOIX_IMAGE);
									break;
								case R.string.button_prendre_video:
									priseDeVideo();
									break;
								case R.string.button_ajouter_video:
									ouvreAjoutChoixPieceJointe(REQUETE_CHOIX_VIDEO);
									break;
							}
						}
					})
					.show();
			}
		});

		telechargementDialog = new ProgressDialog(getActivity());


		return layoutEcranLectureMessages;
	}

	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// lancement du téléchargement des nouveaux messages
		telechargerNouveauxMessages();		
	}

	
	private void lancerGeolocalisation() {
		geoMod = GeolocalisationModule.getInstance(new GeolocalisationInterface() {

			@Override
			public void recupérationDeLaPositionReussie(Location location) {
				locationCourante = location;
			}

			@Override
			public void recupérationDeLaPositionEchoue(int flag, String provider, boolean erreurCritique) {
				if(erreurCritique){
					Utiles.afficherInfoAlertDialog(R.string.title_localisation_indisponible, R.string.label_erreur_localisation_indisponible, getActivity());
				}
			}
		}, getActivity().getApplicationContext());
	}

	
	@Override
	public void onResume () {
		super.onResume();

		if(ancienFichierTmpAffichagePJGallerie != null)
			ancienFichierTmpAffichagePJGallerie.delete();

		// récupère et affiche les messages
		afficherMessages();
	}

	
	@Override
	public void onPause() {
		super.onPause();

		//On arrete la recuparation de la position si elle est lancé
		if(geoMod != null)
			geoMod.stopLocalisation();
	}

	
	// affiche une liste de messages à l'écran
	public void afficherMessages() {
		if(getActivity() == null)
			return;

		listeNouveauxMessages = messageDAO.getMessages(idCapsule, nomPage, nomTagPage, numOccurenceTagPage, Constantes.CLASSEMENT_DESCENDANT, languesAffichees);

		// s'il y a des messages
		if ( !listeNouveauxMessages.isEmpty() ) {
			listeMessagesLayout.removeAllViews();

			// pour chaque message
			for (Message message : listeNouveauxMessages) {
				messageCourant = message;

				messageLayout = (RelativeLayout) getActivity().getLayoutInflater().inflate(R.layout.message, listeMessagesLayout, false);
				ajouterMessage(listeMessagesLayout.getChildCount(), listeMessagesLayout.getChildCount());

				listeMessages.add(messageCourant);
			}
			//On lance la méthode qui va gérer l'affichage des images
			calculEtAfficheLesThumbnails();
		}
		// on indique les messages et leurs réponses comme lus
		messageDAO.updateMessagesNonLus(idCapsule, nomPage, nomTagPage, numOccurenceTagPage);
	}

	
	// configure un message et l'ajoute à la liste des messages
	private void ajouterMessage(int positionMessage, int position) {
		// date de creation du message
		dateFormat = new SimpleDateFormat(getString(R.string.android_format_date), Locale.US);
		dateCreation = new Date( messageCourant.getDateCreation() * 1000 );		// * 1000 : permet de transformer le timestamp (qui est en second) en millisecond
		((TextView) messageLayout.findViewById(R.id.date_message)).setText( dateFormat.format(dateCreation) );
		// id de l'auteur du message
		messageLayout.findViewById(R.id.pseudo_utilisateur).setTag( messageCourant.getAuteur().getId() );
		// role de l'auteur pour traduction
		((TextView) messageLayout.findViewById(R.id.role_utilisateur)).setText( getString(
			getResources().getIdentifier("label_utilisateur_" + messageCourant.getIdRoleAuteur(), "string", getActivity().getPackageName())
		) );

		// pseudo de l'auteur du message
		((TextView) messageLayout.findViewById(R.id.pseudo_utilisateur)).setText( messageCourant.getAuteur().getPseudo() );

		// affichage d'une roue de chargement si le message n'est pas synchro au serveur
		if(Long.valueOf(messageCourant.getId()) < 0) {
			((ProgressBar) messageLayout.findViewById(R.id.progress_bar)).setVisibility(View.VISIBLE);;
		}

		// contenu du message
		// si le message n'a pas été supprimé (id utilisateur == "0"), on affiche le contenu du message
		if ( messageCourant.getSupprimePar().equals("0") ) {
			((TextView) messageLayout.findViewById(R.id.contenu_message)).setText( messageCourant.getContenu() );
		} else {
			((TextView) messageLayout.findViewById(R.id.contenu_message)).setText(R.string.label_message_supprime);
			((TextView) messageLayout.findViewById(R.id.contenu_message)).setTypeface( Typeface.defaultFromStyle(Typeface.ITALIC) );
			((LinearLayout) messageLayout.findViewById(R.id.tags_message)).setVisibility(View.GONE);
			((LinearLayout) messageLayout.findViewById(R.id.pjs_message)).setVisibility(View.GONE);
		}

		// id du message
		messageLayout.findViewById(R.id.contenu_message).setTag( messageCourant.getId() );
		// id du message parent
		messageLayout.setTag( messageCourant.getIdParent() );
		// index du tableau de message listeMessages
		messageLayout.findViewById(R.id.date_message).setTag( Integer.valueOf(listeMessages.size()) );

		// medaille
		if ( messageCourant.getMedaille().equals(Constantes.MEDAILLE_BRONZE) ) {
			((ImageView) messageLayout.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_bronze);			
		} else if( messageCourant.getMedaille().equals(Constantes.MEDAILLE_ARGENT) ) {
			((ImageView) messageLayout.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_argent);			
		} else if( messageCourant.getMedaille().equals(Constantes.MEDAILLE_OR) ) {
			((ImageView) messageLayout.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_or);			
		}

		// défi (en cours ou finis)
		if ( messageCourant.getDefi() == Constantes.DEFI_EN_COURS ) {
			((ImageView) messageLayout.findViewById(R.id.defi)).setImageResource( R.drawable.icone_defi_pas_fini );
		} else if ( messageCourant.getDefi() == Constantes.DEFI_FINIS ) {
			((ImageView) messageLayout.findViewById(R.id.defi)).setImageResource( R.drawable.icone_defi_fini );
		}

		// réponse valide à un défi
		if ( messageCourant.isReponseDefiValide() ) {
			((ImageView) messageLayout.findViewById(R.id.defi)).setImageResource( R.drawable.icone_reponse_defi_valide );
		}

		// langue du message
		// si l'utilisateur est un expert, un admin, ou le posteur du message
		if(messageCourant.getAuteur().getId().equals(idUtilisateurCourant) || Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) >= Constantes.ID_EXPERT) {
			int id = getResources().getIdentifier("cape.pairform:drawable/icone_drapeau_" + Utiles.codeLangueAvecId(messageCourant.getIdLangue(), this.getActivity()), null, null);
			((ImageView) messageLayout.findViewById(R.id.langue_message)).setImageResource(id);
			messageLayout.findViewById(R.id.langue_message).setTag( Integer.valueOf(listeMessages.size()) );
		}

		// Action lors d'un click sur le drapeau d'un message
		((ImageView) messageLayout.findViewById(R.id.langue_message)).setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View imageView) {
				messageCourant = listeMessages.get(((Integer) imageView.getTag()).intValue());
				OuvrirFenetreLangues();
			}			
		});

		// présence de réponses
		nbMessageFils = messageDAO.getNbMessagesFils(messageCourant.getId());
		if (nbMessageFils > 0) {
			messageLayout.setBackgroundResource(R.drawable.arriere_plan_message_avec_reponses);
		}

		// si le message n'a pas été supprimé
		for (Tag tagCourant : messageCourant.getListeTags()) {
			tagCourantButton = (Button) getActivity().getLayoutInflater().inflate( R.layout.tag, messageLayout, false );
			tagCourantButton.setText( tagCourant.getNomTag() );
			// configuration du click sur un tag : créé une redirection vers la vue transversal filtrée avec le tag
			tagCourantButton.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View tagView) {
					intentCourante = new Intent(getActivity(), EcranVueTransversaleActivity.class);
					intentCourante.putExtra(Constantes.CLEF_TAG_MESSAGE, (String) ((TextView) tagView).getText() );
					getActivity().startActivity(intentCourante);
				}
			});

			((LinearLayout) messageLayout.findViewById(R.id.tags_message)).addView(tagCourantButton);
		}

		ajouterPiecesJointesAuMessage();

		// avatar de l'utilisateur
		((ImageView) messageLayout.findViewById(R.id.avatar_utilisateur)).setImageURI( Uri.parse(messageCourant.getAuteur().getUrlAvatar()) );
		((ImageView) messageLayout.findViewById(R.id.avatar_utilisateur)).setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(final View avatarView) {
				// test la disponibilité de l'accès Internet
				if (!Utiles.reseauDisponible( getActivity() )) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
				} else { // On peut utiliser la connection Internet
					// lancement de l'écran affichant le profil de l'utilisateur
					intentCourante = new Intent(getActivity(), EcranProfilActivity.class);
					intentCourante.putExtra(Constantes.CLEF_ID_UTILISATEUR, (String) ((View) avatarView.getParent()).findViewById(R.id.pseudo_utilisateur).getTag());
					startActivity(intentCourante);
				}
			}			
		});

		// utilite du message
		messageLayout.findViewById(R.id.utilite).setTag( messageCourant.getVoteUtilite() );            			

		// si l'utilisateur est connecté
		if( service.estLieAuServeur() ) {
			// si l'utilisateur est l'auteur du message
			if ( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, "").equals(messageLayout.findViewById(R.id.pseudo_utilisateur).getTag()) ) {
				messageLayout.findViewById(R.id.augmenter_utilite).setAlpha(0.25f);
				messageLayout.findViewById(R.id.baisser_utilite).setAlpha(0.25f);
			}// si l'utilisateur a déjà voté positivement le message, sinon, s'il à déjà voté négativement
			else if( messageCourant.getVoteUtilite().equals(Constantes.UTILISATEUR_A_VOTE_PLUS) ) {
				utilitePlus = (ImageView) messageLayout.findViewById(R.id.augmenter_utilite);
				utilitePlus.setImageResource(R.drawable.icone_utilite_plus_vert);

			} else if( messageCourant.getVoteUtilite().equals(Constantes.UTILISATEUR_A_VOTE_MOINS) ) {
				utiliteMoins = (ImageView) messageLayout.findViewById(R.id.baisser_utilite);
				utiliteMoins.setImageResource(R.drawable.icone_utilite_moins_rouge);
			}
		}

		messageLayout.findViewById(R.id.utilite_message).setTag( Integer.valueOf(messageCourant.getUtilite()) );
		((TextView) messageLayout.findViewById(R.id.utilite_message)).setText( String.valueOf(messageCourant.getUtilite()) );

		// création des listeners
		gestureDetector = new GestureDetectorCompat(
			getActivity(),
			new MessageOnGestureListener(
				messageCourant.getId(), 
				messageCourant.getIdParent(),
				messageCourant.getAuteur().getPseudo()
			)
		);
		gestureDetector.setOnDoubleTapListener( new MessageOnDoubleTapListener( 
			listeMessages.size(),
			messageCourant.getId(), 
			messageCourant.getIdParent(),
			position,										// si le message est une réponse, position représente la position du message parent, sinon, c'est la position du message
			positionMessage
		));		
		messageLayout.findViewById(R.id.role_utilisateur).setTag(gestureDetector);

		// listener associé au vote (util / inutil)
		messageLayout.setOnTouchListener( new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				voterUtilite(view, event);
				return ((GestureDetectorCompat) view.findViewById(R.id.role_utilisateur).getTag()).onTouchEvent(event);    	            			
			}			
		});

		// ajout du message à la liste des messages
		listeMessagesLayout.addView(messageLayout, positionMessage);
	}

	
	//Méthode qui ajoute les pieces jointes dans la vue pour le message courant et qui defini le comportement au click de l'image
	private void ajouterPiecesJointesAuMessage() {
		// pour chaque pièce jointe du message courant
		for(PieceJointe pieceJointe : messageCourant.getPiecesJointes()) {			
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics()),
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics())
			);

			layoutParams.setMargins(0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()), 0);

			ImageView imageView = new ImageView(this.getActivity());
			imageView.setLayoutParams(layoutParams);
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setTag(pieceJointe);
			
			//On ajoute une référence de l'imageview pour la modifier plus tard lors de la récupération de l'image correspondant au chemin stocké aussi
			cheminThumbnailStore.put(imageView, pieceJointe.getCheminVersThumbnail());
			
			//Sur le click de l'image 
			imageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View imageView) {
					ouvrirPieceJointe( (PieceJointe) imageView.getTag() );
				}
			});

			((LinearLayout) messageLayout.findViewById(R.id.pjs_message)).addView(imageView);
		}
	}
	
	
	// télécharge et affiche la pièce jointe sur laquelle l'utilisateur vient de cliquer (dans PairForm pour les vidéos et photos, en dehors pour les doc (PDF, .doc, etc.) 
	private void ouvrirPieceJointe(final PieceJointe pieceJointe) {
		//Si on a PAS le reseau
		if (!Utiles.reseauDisponible( getActivity() )) {
			//UN seul choix d'affichage : la pj asynchrone
			//Si la pj est asynchrone alors les données sont dans un fichier a l'adresse du thumbnail
			if(pieceJointe.getNomServeur() == null || pieceJointe.getNomServeur().equals("")) {

				telechargementDialog.setMessage(getString(R.string.label_dialog_recuperation));
				telechargementDialog.show();

				new Thread(new Runnable() {
					public void run() {
						//On récupère la pj
						ancienFichierTmpAffichagePJGallerie = new File(getActivity().getExternalCacheDir().getAbsolutePath(), "tmp." + pieceJointe.getExtension());
						try {
							Utiles.copieFichierVersFichier(new File(pieceJointe.getCheminVersThumbnail()), ancienFichierTmpAffichagePJGallerie);
						} catch (IOException e) {
							e.printStackTrace();
						}

						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								telechargementDialog.hide();
								
								String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(ancienFichierTmpAffichagePJGallerie).toString());
								String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
								
								// si la pj est une image/photos ou une vidéo
								if( PieceJointeInformation.getInstance(getActivity()).extensionPhotoAutoriseDownload(pieceJointe.getExtension()) ||
										PieceJointeInformation.getInstance(getActivity()).extensionVideoAutoriseDownload(pieceJointe.getExtension()) ) {
									//On affiche la pj dans la gallery
									Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);	
									myIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									startActivity(myIntent);
								}
								// sinon, la pj est un doc 
								else {
									Intent sendIntent = new Intent(Intent.ACTION_SEND);									
									sendIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									Intent chooserIntent = Intent.createChooser(sendIntent, getResources().getString(R.string.title_ouvrir_avec));
									
									// s'il y a une application qui peut ouvrir le doc
									if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
										startActivity(chooserIntent);
									} else {
										Toast.makeText(getActivity(), R.string.label_format_pj_pas_lisible, Toast.LENGTH_LONG).show();
									}
								}
							}
						});
					}
				}).start();
			} else {
				//Sinon on prévient que pas de reseau
				Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
			}
		} else {
			//SI on a le reseau
			telechargementDialog.setMessage(getString(R.string.label_dialog_telechargement));
			telechargementDialog.show();

			new Thread(new Runnable() {
				public void run() {

					//Est ce que l'image est déjà téléchargé ?
					boolean contientDonnee =  donneeServeur.containsKey(pieceJointe.getNomServeur());

					//On récupère les données
					final byte[] data = contientDonnee ? donneeServeur.get(pieceJointe.getNomServeur()) : Utiles.telechargerPj(pieceJointe, getActivity());

					//Si on a télécharger les données alors on les ajoutes dans les images téléchargées
					if(!contientDonnee)
						donneeServeur.put(pieceJointe.getNomServeur(), data);

					getActivity().runOnUiThread(new Runnable() {
						public void run() {							
							//Si on a les informations de l'image
							if(data != null && data.length > 0){
								telechargementDialog.hide();

								//On met la pj dans le fichier par défaut
								ancienFichierTmpAffichagePJGallerie = Utiles.enregistrerFichier(getActivity().getExternalCacheDir().getAbsolutePath(), pieceJointe.getNomServeur(), data);
								
								String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(ancienFichierTmpAffichagePJGallerie).toString());
								String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
								
								Log.i("test", "pieceJointe.getExtension() : "+ pieceJointe.getExtension());
								
								// si la pj est une image/photos ou une vidéo
								if( PieceJointeInformation.getInstance(getActivity()).extensionPhotoAutoriseDownload(pieceJointe.getExtension()) || 
										PieceJointeInformation.getInstance(getActivity()).extensionVideoAutoriseDownload(pieceJointe.getExtension()) ) {
									Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.fromFile(ancienFichierTmpAffichagePJGallerie));
									myIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									startActivity(myIntent);
								}
								// sinon, la pj est un doc 
								else {
									Intent sendIntent = new Intent(Intent.ACTION_SEND);									
									sendIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie), mimetype);
									
									Intent chooserIntent = Intent.createChooser(sendIntent, getResources().getString(R.string.title_ouvrir_avec));
									
									// s'il y a une application qui peut ouvrir le doc
									if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
										startActivity(chooserIntent);
									} else {
										Toast.makeText(getActivity(), R.string.label_format_pj_pas_lisible, Toast.LENGTH_LONG).show();
									}
								}
							}
						}
					});
				}
			}).start();
		}
	}

	
	// Méthode permettant de récupérer les images dans le téléphone et de les ajouter dans leur imageView
	private void calculEtAfficheLesThumbnails() {
		//Pour toutes les imageview stockées
		for(final ImageView view : cheminThumbnailStore.keySet()){
			//Si on a déjà calculer le thumbnail on ne le recalcul pas
			if(imageThumbnailStore.containsValue(view)) continue;

			new Thread(new Runnable() {
				public void run() {
					//On récupère le bitmap à l'url
					Bitmap  bmp = Utiles.fichierVersBitmap(cheminThumbnailStore.get(view));

					//Sur un android S4 une image > 4096*4096 ne se chargent pas dans une imageview pour cause de mémoire opengl.
					//720 est un minimum pour les vieux téléphone après des recherches
					//Il n'y a pas de moyen efficace actuellement pour récupérer la taille maximal allouable pour un bitmap
					if (bmp != null){
						if(bmp.getWidth() > 720 || bmp.getHeight() > 720){

							//On redimenssione la taille pour pouvoir afficher le thumbnail
							bmp = Utiles.scaleBitmap(
								bmp,
								PieceJointeInformation.getInstance(getActivity()).getThumbnailTaille()
							);
						}
					}

					//On ajoute l'image en mémoire afin de ne pas la recalculer
					imageThumbnailStore.put(view, bmp);

					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							//On ajoute l'image dans l'imageView
							view.setImageBitmap(imageThumbnailStore.get(view));
						}
					});
				}
			}).start();
		}		
	}

	
	private void voterUtilite(final View view, final MotionEvent event) {
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			downX = event.getX();

		case MotionEvent.ACTION_UP:		        	
			upX = event.getX();
			deltaX = downX - upX;

			if ( Math.abs(deltaX) > MIN_DISTANCE ) {	
				// s'il n'y a pas de réseau internet
				if ( !Utiles.reseauDisponible(getActivity()) ) {
					Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());

				} // si l'utilisateur n'est pas connecté (l'id est stocké dans un fichier de préférence) 
				else if( !service.estLieAuServeur() ) {
					// ouverture de la fenêtre de connexion
					connexionDialog = new ConnexionDialog(getActivity(), service);
					connexionDialog.setOnConnectedListener( new OnConnectedListener() {
						@Override
						public void onConnected(boolean success) {

						}			    		
					});
				} // si l'utilisateur n'est pas au minimum contributeur dans la capsule 
				else if ( Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) < Constantes.ID_CONTRIBUTEUR ) {
					Toast.makeText(getActivity(), R.string.label_fonctionnalite_indisponible_contributeur, Toast.LENGTH_LONG).show();

				} // si l'utilisateur n'essaye pas de voter pour son propre message  : si l'id de l'utilisateur connecté est égal à l'id de l'auteur du message
				else if( !getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, "").equals(view.findViewById(R.id.pseudo_utilisateur).getTag()) ) {		
					utilitePlus = (ImageView) view.findViewById(R.id.augmenter_utilite);
					utiliteMoins = (ImageView) view.findViewById(R.id.baisser_utilite);
					utilite = (TextView) view.findViewById(R.id.utilite_message);
					voteUtilite = (RelativeLayout) view.findViewById(R.id.utilite);

					// si l'utilisateur veut faire un vote positif et qu'il n'avait pas déjà voter positivement ce message
					if ( deltaX < 0 && !voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_VOTE_PLUS) ) {
						utilitePlus.setImageResource(R.drawable.icone_utilite_plus_vert);
						utiliteMoins.setImageResource(R.drawable.icone_utilite_moins);

						// si l'utilisateur veut faire un vote positif et qu'il n'a pas encore voté ce message
						if (voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_PAS_VOTE)) {
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() + 1) ));
						} else { 	// sinon l'utilisateur veut faire un vote positif et il avait voté négativement ce message
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() + 2) ));    		            		                		
						}
						utilite.setText( ((Integer) utilite.getTag()).toString() );
						voteUtilite.setTag(Constantes.UTILISATEUR_A_VOTE_PLUS);

						mettreAJourUtiliteMessage(
							(String) view.findViewById(R.id.contenu_message).getTag(),
							((Integer) view.findViewById(R.id.date_message).getTag()).intValue(),
							((Integer) utilite.getTag()).intValue(),
							true,
							Constantes.UTILISATEUR_A_VOTE_PLUS
						);

					} // si l'utilisateur veut faire un vote négatif et qu'il n'avait pas déjà voter négativement ce message
					else if ( deltaX > 0 && !voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_VOTE_MOINS) ) {
						utiliteMoins.setImageResource(R.drawable.icone_utilite_moins_rouge);
						utilitePlus.setImageResource(R.drawable.icone_utilite_plus);

						// si l'utilisateur veut faire un vote négatif et qu'il n'a pas encore voté ce message
						if (voteUtilite.getTag().equals(Constantes.UTILISATEUR_A_PAS_VOTE)) {
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() - 1) ));
						} else { 	// sinon l'utilisateur veut faire un vote négatif et il n'avait voté positivement ce message
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() - 2) ));
						}
						utilite.setText( ((Integer) utilite.getTag()).toString() );
						voteUtilite.setTag(Constantes.UTILISATEUR_A_VOTE_MOINS);

						mettreAJourUtiliteMessage(
							(String) view.findViewById(R.id.contenu_message).getTag(),
							((Integer) view.findViewById(R.id.date_message).getTag()).intValue(), 
							((Integer) utilite.getTag()).intValue(),
							false,
							Constantes.UTILISATEUR_A_VOTE_MOINS
						);

					} // si l'utilisateur veut annuler son vote
					else {
						// si l'utilisateur veut annuler un vote négatif
						if ( deltaX > 0 ) {
							utiliteMoins.setImageResource(R.drawable.icone_utilite_moins);
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() + 1) ));		                	
							utiliteVariationCourante = false;
						} // l'utilisateur veut annuler un vote positif 
						else {
							utilitePlus.setImageResource(R.drawable.icone_utilite_plus);
							utilite.setTag( Integer.valueOf( (((Integer) utilite.getTag()).intValue() - 1) ));
							utiliteVariationCourante = true;
						}

						utilite.setText( ((Integer) utilite.getTag()).toString() );
						voteUtilite.setTag(Constantes.UTILISATEUR_A_PAS_VOTE);

						mettreAJourUtiliteMessage(
							(String) view.findViewById(R.id.contenu_message).getTag(),
							((Integer) view.findViewById(R.id.date_message).getTag()).intValue(), 
							((Integer) utilite.getTag()).intValue(),
							utiliteVariationCourante,
							Constantes.UTILISATEUR_A_PAS_VOTE
						);
					}
				}
			}
		}
	}

	
	private void mettreAJourUtiliteMessage(String idMessage, int indexMessage, int utilite, boolean utiliteVariation, String voteUtilite) {
		messageCourant = listeMessages.get(indexMessage);
		messageCourant.setUtilite(utilite);
		messageCourant.setVoteUtilite(voteUtilite);
		messageDAO.updateMessage(messageCourant);

		intentCourante = new Intent(getActivity(), ModificateurUtiliteService.class);
		intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, idMessage);
		intentCourante.putExtra(Constantes.CLEF_MODIF_UTILITE_MESSAGE, utiliteVariation);
		getActivity().startService(intentCourante);		
	}

	
	private void afficherReponse(String idMessageParent, int positionMessageParent) {		
		listeNouveauxMessages = messageDAO.getMessagesFils(idMessageParent, Constantes.CLASSEMENT_ASCENDANT, languesAffichees);

		// s'il y a des messages
		if ( !listeNouveauxMessages.isEmpty() ) {
			// modification de l'arriere plan du message parent
			listeMessagesLayout.getChildAt(positionMessageParent).setBackgroundResource(R.drawable.arriere_plan_element_liste);

			// reconfiguration du listener du message parent pour qu'il ferme les réponses si l'utilisateur effectue un double clic
			gestureDetector = (GestureDetectorCompat) listeMessagesLayout.getChildAt(positionMessageParent).findViewById(R.id.role_utilisateur).getTag();
			gestureDetector.setOnDoubleTapListener( new MessageOnDoubleTapListener( 
				((Integer) listeMessagesLayout.getChildAt(positionMessageParent).findViewById(R.id.date_message).getTag()).intValue(),
				(String) listeMessagesLayout.getChildAt(positionMessageParent).findViewById(R.id.contenu_message).getTag(),
				Constantes.REPONSES_VISIBLES,
				positionMessageParent,
				positionMessageParent
			));
			listeMessagesLayout.getChildAt(positionMessageParent).setTag(Constantes.REPONSES_VISIBLES);

			positionMessageCourante = positionMessageParent;

			// ajout des réponses
			for (Message message : listeNouveauxMessages) {
				messageCourant = message;

				messageLayout = (RelativeLayout) getActivity().getLayoutInflater().inflate(R.layout.reponse_message, listeMessagesLayout, false);
				positionMessageCourante++;

				ajouterMessage(positionMessageCourante, positionMessageParent);
				listeMessages.add(messageCourant);
			}

			// reconfiguration des listeners pour prendre en compte le changement de position de chaque message			
			for (int i = positionMessageCourante + 1; i < listeMessagesLayout.getChildCount(); i++) {
				idParentCourant = (String) listeMessagesLayout.getChildAt(i).getTag();

				// si le message à la position courante n'est pas une réponse
				if ( idParentCourant.equals(Constantes.REPONSES_VISIBLES) || idParentCourant.equals("") ) {
					positionMessageCourante = i;
				}

				gestureDetector = (GestureDetectorCompat) listeMessagesLayout.getChildAt(i).findViewById(R.id.role_utilisateur).getTag();
				gestureDetector.setOnDoubleTapListener( new MessageOnDoubleTapListener( 
					((Integer) listeMessagesLayout.getChildAt(i).findViewById(R.id.date_message).getTag()).intValue(),
					(String) listeMessagesLayout.getChildAt(i).findViewById(R.id.contenu_message).getTag(),
					idParentCourant,
					positionMessageCourante,						// si le message est une réponse, position représente la position du message parent, sinon, c'est la position du message
					i
				));
			}


			calculEtAfficheLesThumbnails();
		}
	}

	
	// supprime les réponses d'un message donné
	private void supprimerReponses(String idMessageParent, int positionMessageParent) {		
		// suppression des réponses
		positionMessageCourante = positionMessageParent + 1;
		do {
			listeMessagesLayout.removeViewAt(positionMessageCourante);			// suppression du message du layout
			if ( listeMessagesLayout.getChildAt(positionMessageCourante) == null ) {
				break;
			}
		} while( listeMessagesLayout.getChildAt(positionMessageCourante).getTag().equals(idMessageParent) );

		// reconfiguration du listener du message parent pour prendre en compte la suppression des réponses
		gestureDetector = (GestureDetectorCompat) listeMessagesLayout.getChildAt(positionMessageParent).findViewById(R.id.role_utilisateur).getTag();
		gestureDetector.setOnDoubleTapListener( new MessageOnDoubleTapListener( 
			((Integer) listeMessagesLayout.getChildAt(positionMessageParent).findViewById(R.id.date_message).getTag()).intValue(),
			(String) listeMessagesLayout.getChildAt(positionMessageParent).findViewById(R.id.contenu_message).getTag(),
			"0",
			positionMessageParent,
			positionMessageParent
		));
		listeMessagesLayout.getChildAt(positionMessageParent).setTag("");

		// reconfiguration des listeners pour prendre en compte le changement de position de chaque message			
		for (int i = positionMessageParent + 1; i < listeMessagesLayout.getChildCount(); i++) {
			idParentCourant = (String) listeMessagesLayout.getChildAt(i).getTag();

			// si le message à la position courante n'est pas une réponse
			if ( idParentCourant.equals(Constantes.REPONSES_VISIBLES) || idParentCourant.equals("") ) {
				positionMessageCourante = i;
			}

			gestureDetector = (GestureDetectorCompat) listeMessagesLayout.getChildAt(i).findViewById(R.id.role_utilisateur).getTag();
			gestureDetector.setOnDoubleTapListener( new MessageOnDoubleTapListener( 
				((Integer) listeMessagesLayout.getChildAt(i).findViewById(R.id.date_message).getTag()).intValue(),
				(String) listeMessagesLayout.getChildAt(i).findViewById(R.id.contenu_message).getTag(),
				idParentCourant,
				positionMessageCourante,						// si le message est une réponse, position représente la position du message parent, sinon, c'est la position du message
				i
			));
		}
		// modification de l'arriere plan du message parent
		listeMessagesLayout.getChildAt(positionMessageParent).setBackgroundResource(R.drawable.arriere_plan_message_avec_reponses);

		// repositionnement de l'ecran (le message parent sera le premier élément de la liste à l'écran)
		listeMessagesScrollView.scrollTo(0, listeMessagesLayout.getChildAt(positionMessageParent).getTop());
	}

	
	// Mise à jour du champ d'écriture d'un message
	private void ecrireMessage(String idMessageParent, String auteurMessageSelectionne) {
		idMessageParentNouveauMessage = idMessageParent;

		// si l'utilisateur répond à un message
		if (auteurMessageSelectionne != null) {
			texteNouveauMessage.setText("@"+ auteurMessageSelectionne +",");	// le nom de l'auteur de ce message est ajouté au début du message
			NouveauMessageEstDefi = false;										// une réponse ne peut pas être un défi
		} else {
			texteNouveauMessage.setText("");
		}
	}

	
	/**
	 * 
	 * Méthode à appeler avant d'envoyer un message qui vérifie si la position GPS est bien récupéré
	 */
	public void publierMessageVerificationGeolloc() {
		if(geolocaliserMessage && locationCourante == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

			builder.setTitle(R.string.title_ajout_pj_impossible);

			builder.setMessage(R.string.label_erreur_localisation_pas_recuperer);
			builder.setPositiveButton(R.string.button_oui, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
					
					if(!service.internetActif())
						Toast.makeText(getActivity(), R.string.label_envoie_message_pas_de_reseau, Toast.LENGTH_LONG).show();

					publierMessage();
				}
			});
			builder.setNegativeButton(R.string.button_non, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					return;
				}
			});
			builder.show();

		} else
			publierMessage();
	}
	
	
	// publie le message rédigé par l'utilisateur
	public void publierMessage() {
		// création d'une fenêtre de chargement
		long idMessage = System.currentTimeMillis()/1000; //Cast de milisecond en secondes
		idMessage*=-1;
		SharedPreferences fichierSharedPreferences = getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, Context.MODE_PRIVATE);
		String idRoleAuteur = fichierSharedPreferences.getString(String.valueOf(idCapsule), "");
		String pseudoAuteur = fichierSharedPreferences.getString(Constantes.CLEF_PSEUDO_UTILISATEUR, null);
		String idUtilisateur = fichierSharedPreferences.getString(Constantes.CLEF_ID_UTILISATEUR, null);
		String path = getActivity().getDir(Constantes.REPERTOIRE_PJS_HORS_LIGNE, Context.MODE_PRIVATE).getPath();
		double latitude = 0;
		double longitude = 0;

		//Si on a une position gps
		if(geolocaliserMessage && locationCourante != null){            		  
			latitude = locationCourante.getLatitude();
			longitude = locationCourante.getLongitude();
		}

		//Si on écrit le message sans etre connecté
		if (pseudoAuteur == null){
			Utiles.afficherInfoAlertDialog(R.string.title_alertdialog_compte_manquant, R.string.label_alertdialog_compte_manquant, getActivity());
			return;
		}
		
		//On crée un nouveau message qui sera sauvegardé dans la bdd, temporarirement avec les données qu'on a
		Message message = new Message(
			String.valueOf(idMessage), 
			idCapsule,
			idUtilisateur,
			pseudoAuteur,
			getActivity().getDir(Constantes.REPERTOIRE_AVATAR, Context.MODE_PRIVATE).getPath() + "/" + idUtilisateur,
			idRoleAuteur, 
			texteNouveauMessage.getText().toString().trim(), 
			idLangueNouveauMessage, 
			nomPage, 
			nomTagPage, 
			Integer.valueOf(numOccurenceTagPage), 
			idMessageParentNouveauMessage, 
			(NouveauMessageEstDefi ? 1 : 0), 
			String.valueOf(latitude),
			String.valueOf(longitude),
			new java.util.Date().getTime()/1000,		//Cast de milisecond en secondes
			!idMessageParentNouveauMessage.equals("0")
		);

		message.setLu(true);

		// Ajout des tags
		for(CharSequence tag : listeTagsNouveauMessage){
			message.addTag(tag.toString(), pseudoAuteur);
		}
		// Si le message n'est pas une réponse, on définit la visibilité avec les cercles définis dans les options de publication
		for(Cercle cercle : listeCerclesNouveauMessage) {
			if (message.getVisibilite() == null)
				message.setVisibilite(cercle.getIdCercle());
			else 
				message.setVisibilite(message.getVisibilite() +","+ cercle.getIdCercle());				
		}
		
		//Ajout des pjs
		for(int i = 0; i < piecesJointesMessage.size(); i++){
			PieceJointe pj = piecesJointesMessage.get(i);
			pj.setIdMessage(String.valueOf(idMessage));

			try {
				//Sauvegarde des pjs !!!
				//on créer un fichier unique dans les data de l'appli
				File tmp = new File(path, pj.getIdMessage()+"-"+i+"."+pj.getExtension());
				//On sauvegarde la pj dans le fichier
				Utiles.copieFichierVersFichier(pj.getFile(), tmp);
				pj.setCheminVersThumbnail(tmp.getAbsolutePath());
				pj.setFile(tmp);

			} catch (IOException e) {
				//Tans pis on ne sauve pas ...
				//On compte sur l'utilisateur qui ne supprimera pas la pj dans sa galerie
				e.printStackTrace();
			}

			message.ajoutePJ(pj);
		}

		//On vide les champs
		texteNouveauMessage.setText("");
		idMessageParentNouveauMessage = "0";
		NouveauMessageEstDefi = false;
		listeTagsNouveauMessage.clear();
		piecesJointesMessage.clear();
		((LinearLayout) layoutEcranLectureMessages.findViewById(R.id.pjs_menu_liste)).removeAllViews();

		//Enregistre en base de données
		//	-message
		//	-tag
		//	-pjs
		messageDAO.insertMessage(message);

		//Ajoute au layout le nouveau message
		if(message.getIdParent().equals("0") || message.getPiecesJointes().size() != 0) {
			//C'est une réponse  ou a des pj donc on rafraichis la vue
			messageMiseAJour(true);
		} else {
			//C'est juste un message donc on ne fait que l'ajouter ou lieu de tout recharger
			messageCourant = message;
			messageLayout = (RelativeLayout) getActivity().getLayoutInflater().inflate(R.layout.message, listeMessagesLayout, false);			
			ajouterMessage(0, 0);
			listeMessagesScrollView.fullScroll(ScrollView.FOCUS_UP);
			listeMessages.add(messageCourant);
		}

		//On re-initialise le booléen controlant la geolocalisation selon la valeur dans les préférences
		geolocaliserMessage = this.getActivity().getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, Context.MODE_PRIVATE).getBoolean(Constantes.CLEF_GEOLOCALISATION_MESSAGE, false);

		// on masque le clavier
		((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
		.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

		//Lance l'envoie des message à partir du service
		//Asynchrone -> rafraichis la vu lors de la mise a jours des messages qui va suivre l'envoie
		service.envoyerMessageAsynchrone();
	}

	
	// télécharge les nouveaux messages et les enregistre en BDD
	private void telechargerNouveauxMessages() {    
		service.mettreAJourMessage(idCapsule);
	}

	
	// ouvre la fenêtre de modification de la langue d'un message
	private void OuvrirFenetreLangues() {
		listeTextesLangues = Utiles.idArrayWithRIdArray(getActivity(), R.array.langue_true_values_string);
		listeIconesLangues = Utiles.idArrayIconesLangues(getActivity());

		ListWithCompoundDrawableAdapter adapterLangues = new ListWithCompoundDrawableAdapter(
			getActivity(),
			listeTextesLangues,
			listeIconesLangues,
			R.layout.element_liste_utilisateurs,
			ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		);

		dialogBuilder = new AlertDialog.Builder( getActivity() );
		dialogBuilder.setTitle(R.string.label_langue_du_message)
		.setAdapter(adapterLangues, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				modifierLangue(Utiles.idLangueAvecTabIndex(itemSelectionne, getActivity()));
				dialog.cancel();
				if(actionMode != null)
					actionMode.finish();
			}
		}).show();    		
	}

	
	// modification de la langue d'un message
	private void modifierLangue(int langueID) {
		messageCourant.setIdLangue( String.valueOf(langueID) );
		messageDAO.updateMessage(messageCourant);

		intentCourante = new Intent(getActivity(), ModificateurLangueMessageService.class);
		intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, messageCourant.getId());
		intentCourante.putExtra(Constantes.CLEF_WEBSERVICE_IDLANGUE_MESSAGE, String.valueOf(langueID));			
		getActivity().startService(intentCourante);

		// Actualise la liste des messages
		listeMessagesLayout.removeAllViews();
		listeMessages.clear();
		afficherMessages();
	}

	
	@Override
	public void onPrepareOptionsMenu (Menu menu) {
		// mise à jour du menu (partagé par plusieurs fragments)
		menu.findItem(R.id.button_accueil).setVisible(false);
		menu.findItem(R.id.button_sommaire_capsule).setVisible(true);
		menu.findItem(R.id.button_recuperer_nouveaux_messages).setVisible(true);
		menu.findItem(R.id.button_lire_messages).setVisible(false);
		menu.findItem(R.id.button_ecrire_message).setVisible(true);
		menu.findItem(R.id.button_carte).setVisible(true);
	}

	
	// gestion du menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) { 		
		switch (item.getItemId()) {
		case R.id.button_recuperer_nouveaux_messages:
			telechargerNouveauxMessages();
			return true;
		case R.id.button_ecrire_message:
			ecrireMessage("0", null);		// "0" indique que le message n'est pas une réponse
			return true;
		case R.id.button_carte:
			Bundle bundleMessage = new Bundle();
			bundleMessage.putString(
				Constantes.CLEF_ID_CAPSULE,
				idCapsule
			);
			bundleMessage.putString(
				Constantes.CLEF_NOM_PAGE_CAPSULE,
				nomPage
			);
			bundleMessage.putString(
				Constantes.CLEF_NOM_TAG_PAGE_MESSAGE,
				nomTagPage
			);
			bundleMessage.putString(
				Constantes.CLEF_NUM_OCCURENCE_TAG_PAGE_MESSAGE,
				numOccurenceTagPage
			);
			Intent i = new Intent(getActivity(), EcranCarteActivity.class);
			i.putExtras(bundleMessage);
			//Ne pas mettre getActivity avant car empeche le retour du result
			startActivityForResult(i, MESSAGE_CHOISI_SUR_CARTE);
			return true;
		}
		return false;
	}

	
	// gestion du changement d'orientation
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	
	@Override
	public void onDestroy() {
		messageDAO.close();
		super.onDestroy();
	}

	
	@Override
	public void onDetach() {
		super.onDetach();

		if(ancienFichierTmpAffichagePJGallerie != null)
			ancienFichierTmpAffichagePJGallerie.delete();

		if(progressDialog != null)
			progressDialog.dismiss();

		if(telechargementDialog != null)
			telechargementDialog.dismiss();
	}

	
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		donneeServeur.clear();
	}

	
	// interface permettant de communiquer avec l'activity parent
	public interface LectureMessagesFragmentListener {
		// modifie l'élément connexion du slidingMenu après une connexion
		public void modifierElementConnexionSlidingMenu();
	}


	/* ***********************************************************
	 * 
	 * Listeners réagissant aux interactions avec les messages (clic, clic long, swipe, etc.)
	 * 
	 * **********************************************************/

	private class MessageOnGestureListener implements GestureDetector.OnGestureListener {
		private String idMessage;			// id du message sur lequel l'utilisateur à cliquer
		private String idMessageParent;		// id du parent du message sur lequel l'utilisateur à cliquer
		private String auteurMessage;		// pseudo de l'auteur du message sur lequel l'utilisateur à cliquer

		public MessageOnGestureListener(String idMessage, String idMessageParent, String auteurMessage) {
			this.idMessage = idMessage;
			this.idMessageParent = idMessageParent;
			this.auteurMessage = auteurMessage;
		}

		@Override
		public void onLongPress(MotionEvent event) {
			// le téléphone vibre pour signaler la prise en compte du clic long
			((Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(Constantes.DUREE_VIBRATION_CLIC_LONG);
									
			// fermeture du menu contextuel, s'il existe
			if (actionMode != null) { 
				actionMode.finish();
			}

			if ( idMessageParent.equals("0") || idMessageParent.equals(Constantes.REPONSES_VISIBLES)) {
				ecrireMessage(idMessage, auteurMessage);
			} else {
				ecrireMessage(idMessageParent, auteurMessage);
			}
		}

		@Override
		public boolean onDown(MotionEvent event) {return true;}							
		@Override
		public boolean onFling(MotionEvent downEvent, MotionEvent moveEvent, float arg2, float arg3) {return false;}        			
		@Override
		public boolean onScroll(MotionEvent startEvent, MotionEvent moveEvent, float arg2, float arg3) {return false;}
		@Override
		public void onShowPress(MotionEvent event) {}
		@Override
		public boolean onSingleTapUp(MotionEvent event) {return false;}
	}


	private class MessageOnDoubleTapListener implements GestureDetector.OnDoubleTapListener {
		private int indexMessage;			// index du message dans la liste des messages listeMessages
		private String idMessage;			// id du message sur lequel l'utilisateur à cliquer
		private String idMessageParent;		// id du parent du message sur lequel l'utilisateur à cliquer
		private int position;				// si le message est un parent, position du message dans le layout listeMessagesLayout ; si le message est une réponse, position du message parent dans le layout listeMessagesLayout
		private int positionMessage;		// position du message dans le layout listeMessagesLayout

		public MessageOnDoubleTapListener(int indexMessage, String idMessage, String idMessageParent, int position, int positionMessage) {
			this.indexMessage = indexMessage;
			this.idMessage = idMessage;
			this.idMessageParent = idMessageParent;
			this.position = position;
			this.positionMessage = positionMessage;
		}

		@Override
		public boolean onDoubleTap(MotionEvent event) {
			// si les réponses du message parent (ou du message lui-même si il est le parent) ne sont pas déjà visibles
			if ( idMessageParent.equals("0")) {				
				// si le message n'est pas un défi en cours OU si l'utilisateur courant est l'auteur du message courant
				messageCourant = listeMessages.get(indexMessage);
				if ( messageCourant.getDefi() != Constantes.DEFI_EN_COURS || messageCourant.getAuteur().getId().equals(idUtilisateurCourant)) {
					// on affiche les reponses du message sur lequel l'utilisateur a double-cliqué
					afficherReponse(idMessage, position);
				}

			} else {
				// si l'utilisateur à cliquer sur le message parent
				if (idMessageParent.equals(Constantes.REPONSES_VISIBLES)) {
					// on supprime les reponses du message sur lequel l'utilisateur a double-cliqué
					supprimerReponses(idMessage, position);
				} else { // sinon (il a cliqué sur une réponse)
					// on masque les reponses du message parent du message sur lequel l'utilisateur a double-cliqué
					supprimerReponses(idMessageParent, position);
				}
			}

			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent event) {
			// récupération du message courant    		
			messageCourant = listeMessages.get(indexMessage);

			// on affiche le menu contextuel
			actionMode = getActivity().startActionMode(actionBarContextuel);    		
			actionMode.getMenuInflater().inflate(R.menu.menu_contextuel_ecran_messages, actionMode.getMenu());

			// configuration du clic sur le bouton répondre
			actionMode.getMenu().findItem(R.id.button_repondre).setOnMenuItemClickListener( new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {					
					if ( idMessageParent.equals("0") || idMessageParent.equals(Constantes.REPONSES_VISIBLES)) {
						ecrireMessage(idMessage, messageCourant.getAuteur().getPseudo());
					} else {
						ecrireMessage(idMessageParent, messageCourant.getAuteur().getPseudo());
					}
					actionMode.finish();
					return false;
				}  	        		
			});    		

			// on récupère les liens contenues dans le message
			listeLiens = Utiles.obtenirLiens( messageCourant.getContenu() );

			// configuration du clic sur le bouton obtenir liens
			// s'il n'y a pas de liens dans le message
			if ( listeLiens.isEmpty() ) {
				actionMode.getMenu().findItem(R.id.button_obtenir_liens).setVisible(false);
			} else {
				actionMode.getMenu().findItem(R.id.button_obtenir_liens).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {       		
						TextView textViewLiens = (TextView) getActivity().getLayoutInflater().inflate(R.layout.layout_dialog_texte, null);

						for (String lien : listeLiens) {
							textViewLiens.setText( textViewLiens.getText() + "\n" + lien + "\n");
						}

						// ouverture d'une fenêtre contenant les liens
						dialogBuilder = new AlertDialog.Builder(getActivity());
						dialogBuilder.setTitle(R.string.title_liens)
						.setView(textViewLiens)
						.show();

						actionMode.finish();
						return false;
					}  	        		
				});
			}

			// configuration du clic sur le bouton ajouter tag
			// si l'utilisateur est lecteur ou participant
			if ( Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) < Constantes.ID_COLLABORATEUR ) {
				actionMode.getMenu().findItem(R.id.button_ajouter_tag).setVisible(false);
			} else {
				actionMode.getMenu().findItem(R.id.button_ajouter_tag).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						// s'il y a 5 tags ou plus d'associés au message ; il ne peut pas y avoir plus de 5 tags associés à un message
						if ( ((LinearLayout) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.tags_message)).getChildCount() == Constantes.NB_TAGS_MAX ) {
							Toast.makeText(getActivity(), R.string.label_nb_tag_max, Toast.LENGTH_LONG).show();
						} else {
							ouvrirFenetreAjoutTag();
						}
						return false;
					} 		
				});
			}

			// configuration du clic sur le bouton affichant les informations du message
			actionMode.getMenu().findItem(R.id.button_infos).setOnMenuItemClickListener( new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					OuvrirFenetreInfos();
					return false;
				}  	        		
			});

			// configuration du bouton pour terminer un défi
			// si le message n'est pas un défi en cours OU si le message est un défi en cour mais que l'utilisateur n'est pas l'auteur du défi)
			if ( messageCourant.getDefi() != Constantes.DEFI_EN_COURS || (messageCourant.getDefi() == Constantes.DEFI_EN_COURS && !messageCourant.getAuteur().getId().equals(idUtilisateurCourant)) ) {
				actionMode.getMenu().findItem(R.id.button_terminer_defi).setVisible(false);
			} else {
				actionMode.getMenu().findItem(R.id.button_terminer_defi).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						messageCourant.setDefi(Constantes.DEFI_FINIS);  	        			
						messageDAO.updateMessage( messageCourant );

						intentCourante = new Intent(getActivity(), TerminatorDefiService.class);
						intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, idMessage);
						getActivity().startService(intentCourante);

						((ImageView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.defi)).setImageResource( R.drawable.icone_defi_fini );
						actionMode.finish();
						return false;
					}  	        		
				});
			}

			// configuration du clic sur le bouton de validation d'une réponse à un défi
			// si le message est une réponse (la variable idParent existe et correspond à l'id du message parent)
			if ( !messageCourant.getIdParent().equals("0") ) {
				// récupération du message parent
				Message messageParent = listeMessages.get( (Integer) listeMessagesLayout.getChildAt(position).findViewById(R.id.date_message).getTag() );

				// si le message parent est un défi ET si l'auteur du message parent est l'utilisateur
				if ( messageParent.getDefi() >= Constantes.DEFI_EN_COURS && messageParent.getAuteur().getId().equals(idUtilisateurCourant) ) {
					actionMode.getMenu().findItem(R.id.button_valider_reponse_defi).setOnMenuItemClickListener( new OnMenuItemClickListener() {
						@Override
						public boolean onMenuItemClick(MenuItem item) {
							// on modifie la validité du message, s'il était valide il devient invalide (et vice versa)
							messageCourant.setReponseDefiValide( !messageCourant.isReponseDefiValide() );
							messageDAO.updateMessage( messageCourant );

							intentCourante = new Intent(getActivity(), ValidateurReponseDefiService.class);
							intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, idMessage);
							getActivity().startService(intentCourante);

							// mise à jour de l'icone défi
							if ( messageCourant.isReponseDefiValide() ) {
								((ImageView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.defi)).setImageResource( R.drawable.icone_reponse_defi_valide );
							} else {
								((ImageView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.defi)).setImageResource( 0 );
							}
							actionMode.finish();
							return false;
						}  	        		
					});
				} else {
					actionMode.getMenu().findItem(R.id.button_valider_reponse_defi).setVisible(false);
				}
			} else {
				actionMode.getMenu().findItem(R.id.button_valider_reponse_defi).setVisible(false);
			}

			// configuration du clic sur le bouton attribuer médaille
			// si l'utilisateur n'est ni un expert, ni un admin
			if ( Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) < Constantes.ID_EXPERT ) {
				actionMode.getMenu().findItem(R.id.button_attribuer_medaille).setVisible(false);
			} else {
				actionMode.getMenu().findItem(R.id.button_attribuer_medaille).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						OuvrirFenetreMedailles();		        		
						return false;
					}  	        		
				});
			}

			// configuration du clic sur le bouton changer la langue du message
			// si l'utilisateur est un expert, un admin, ou le posteur du message
			if(messageCourant.getAuteur().getId().equals(idUtilisateurCourant) || Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) >= Constantes.ID_EXPERT) {
				actionMode.getMenu().findItem(R.id.button_changer_langue_message).setOnMenuItemClickListener( new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						OuvrirFenetreLangues();		        		
						return false;
					}  	        		
				});
			} else {
				actionMode.getMenu().findItem(R.id.button_changer_langue_message).setVisible(false);
			}

			// configuration du clic sur le bouton de suppression/retablissement d'un message
			// si l'utilisateur n'est ni un animateur, ni un expert, ni un admin ni l'auteur du message
			if ( !messageCourant.getAuteur().getId().equals(idUtilisateurCourant) && Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) < Constantes.ID_ANIMATEUR ) {
				actionMode.getMenu().findItem(R.id.button_supprimer_message).setVisible(false);
			} else {
				// si le message n'a pas été supprimé (id utilisateur == "0")
				if ( messageCourant.getSupprimePar().equals("0") ) {
					// configuration du clic sur le bouton supprimer un message
					actionMode.getMenu().findItem(R.id.button_supprimer_message).setOnMenuItemClickListener( new OnMenuItemClickListener() {
						@Override
						public boolean onMenuItemClick(MenuItem item) {
							supprimerMessage(false);
							actionMode.finish();
							return false;
						}  	        		
					});
				} else {
					// modification de l'icone et du texte associé pour correspondre à l'annulation de la suppression
					actionMode.getMenu().findItem(R.id.button_supprimer_message).setIcon(R.drawable.icone_retablir_message);
					actionMode.getMenu().findItem(R.id.button_supprimer_message).setTitle(R.string.button_retablir_message);

					// configuration du clic sur le bouton de retablissement d'un message
					actionMode.getMenu().findItem(R.id.button_supprimer_message).setOnMenuItemClickListener( new OnMenuItemClickListener() {
						@Override
						public boolean onMenuItemClick(MenuItem item) {
							supprimerMessage(true);
							actionMode.finish();
							return false;
						}  	        		
					});
				}
			}

			// configuration du clic sur le bouton partager contenu du message
			// si le message a été supprimé (id utilisateur != "0")
			if ( !messageCourant.getSupprimePar().equals("0") ) {
				actionMode.getMenu().findItem(R.id.button_partager_message).setVisible(false);
			} else {
				// création du sous-menu affichant les possibilités (réseaux sociaux) de partage du message
				intentCourante = new Intent(Intent.ACTION_SEND);
				intentCourante.setType( HTTP.PLAIN_TEXT_TYPE );
				intentCourante.putExtra(
						Intent.EXTRA_TEXT,
						// mise en forme du texte à afficher
						"#" + getActivity().getResources().getString(R.string.app_name) +
						" #" + capsuleCourante.getNomCourt() +
						" >" + messageCourant.getAuteur().getPseudo() + " : " +
						messageCourant.getContenu()
						);

				ShareActionProvider shareActionProvider = (ShareActionProvider) actionMode.getMenu().findItem(R.id.button_partager_message).getActionProvider();
				shareActionProvider.setShareIntent(intentCourante);
			}

			return true;
		}


		// ouvre la fenetre d'ajout d'un tag sur un message
		private void ouvrirFenetreAjoutTag() {
			ajouterTagView = (EditText) getActivity().getLayoutInflater().inflate(R.layout.layout_dialog_ajout_tag, listeMessagesLayout, false);

			dialogBuilder = new AlertDialog.Builder( getActivity() );
			dialogBuilder.setTitle(R.string.title_ajouter_tag)
			.setView(ajouterTagView)
			.setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					actionMode.finish();
				}
			})
			.setPositiveButton(R.string.button_ajouter, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					// si le tag n'est pas une chaine de caractères vides 
					if( !ajouterTagView.getText().toString().trim().isEmpty() ) {
						// ajout du tag sur le message courant
						messageCourant.addTag(
							ajouterTagView.getText().toString(), 
							getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_PSEUDO_UTILISATEUR, "")
						);
						messageDAO.updateMessage(messageCourant);

						listeNomTags.clear();
						listeNomTags.add( ajouterTagView.getText().toString() );

						//Si le message est sur le serveur on peut le modifier, sinon on attend juste qu'il soit synchronisé
						if(Long.valueOf(messageCourant.getId()) > 0){
							intentCourante = new Intent(getActivity(), AjouteurTagService.class);
							intentCourante.putExtra( Constantes.CLEF_ID_MESSAGE, messageCourant.getId() );
							intentCourante.putStringArrayListExtra( Constantes.CLEF_TAGS_MESSAGE, listeNomTags );
							getActivity().startService(intentCourante);
						}

						// configuration du tag qui apparaitra à l'écran
						tagCourantButton = (Button) getActivity().getLayoutInflater().inflate( R.layout.tag, null );
						tagCourantButton.setText( ajouterTagView.getText().toString() );
						tagCourantButton.setTag( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_PSEUDO_UTILISATEUR, "") );
						// configuration du click sur un tag : créé une redirection vers la vue transversal filtrée avec le tag
						tagCourantButton.setOnClickListener( new OnClickListener() {
							@Override
							public void onClick(View tagView) {
								intentCourante = new Intent(getActivity(), EcranVueTransversaleActivity.class);
								intentCourante.putExtra(Constantes.CLEF_TAG_MESSAGE, (String) ((TextView) tagView).getText() );
								getActivity().startActivity(intentCourante);
							}
						});

						// mise à jour de la liste des tags du message à l'écran
						((LinearLayout) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.tags_message)).addView(tagCourantButton);
					}
					actionMode.finish();
				}
			})
			.show();
		}

		// ouvre la fenêtre affichant les informations sur le message
		private void OuvrirFenetreInfos() {        		
			TextView textViewInfos = (TextView) getActivity().getLayoutInflater().inflate(R.layout.layout_dialog_texte, null);

			dateCreation = new Date( messageCourant.getDateCreation() * 1000 );
			dateFormat = new SimpleDateFormat(getString(R.string.android_format_date), Locale.US);
			String date = dateFormat.format(dateCreation);
			dateFormat = new SimpleDateFormat(getString(R.string.android_format_heure), Locale.US);
			String heure = dateFormat.format(dateCreation);

			String content = "";
			content += getString(R.string.label_message_poste_le) + " " + date + " " + getString(R.string.label_a) + " " + heure + "\n";
			content += getString(R.string.label_langue) + " : " + Utiles.nomLangueAvecId(String.valueOf(messageCourant.getIdLangue()), getActivity());

			// Affichage des tags
			// si l'utilisateur est un expert ou un admin
			if (!(Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) < Constantes.ID_EXPERT )) {
				if ( !messageCourant.getListeTags().isEmpty() ) {
					content += "\n" + getString(R.string.label_auteurs_tag) + " :";
					// pour chaque tags contenues dans le message
					for (Tag tagCourant : messageCourant.getListeTags()) {
						// on affiche pour chaque tag le pseudo de l'auteur associé
						content += "\n" + "- " + tagCourant.getNomTag() + " : " + tagCourant.getPseudoAuteur();
					}
				}
			}

			textViewInfos.setText(content);

			// ouverture d'une fenêtre contenant les infos sur les tags
			dialogBuilder = new AlertDialog.Builder(getActivity());
			dialogBuilder.setTitle(R.string.title_informations)
			.setView(textViewInfos)
			.show();

			actionMode.finish();
		}


		// ouvre la fenêtre d'attribution / de suppression d'une medaille
		private void OuvrirFenetreMedailles() {    		
			// on configure la fenêtre des médailles, si le message a déjà une médaille, on propose la possibilité de la supprimer
			if ( messageCourant.getMedaille().equals(Constantes.MEDAILLE_BRONZE) ) {		// le message a déjà une médaille de bronze
				listeTextesMedailles = new int[] {R.string.label_supprimer_medaille, R.string.label_medaille_argent, R.string.label_medaille_or};
				listeIconesMedailles = new int[] {R.drawable.icone_pas_de_medaille, R.drawable.icone_medaille_argent, R.drawable.icone_medaille_or};

			} else if ( messageCourant.getMedaille().equals(Constantes.MEDAILLE_ARGENT) ) {		// le message a déjà une médaille d'argent
				listeTextesMedailles = new int[] {R.string.label_supprimer_medaille, R.string.label_medaille_bronze,R.string.label_medaille_or};
				listeIconesMedailles = new int[] {R.drawable.icone_pas_de_medaille, R.drawable.icone_medaille_bronze, R.drawable.icone_medaille_or};

			} else if ( messageCourant.getMedaille().equals(Constantes.MEDAILLE_OR) ) {			// le message a déjà une médaille d'or
				listeTextesMedailles = new int[] {R.string.label_supprimer_medaille, R.string.label_medaille_bronze, R.string.label_medaille_argent};
				listeIconesMedailles = new int[] {R.drawable.icone_pas_de_medaille, R.drawable.icone_medaille_bronze, R.drawable.icone_medaille_argent};

			} else {																			// le message n'a pas encore de médaille
				listeTextesMedailles = new int[] {R.string.label_medaille_bronze, R.string.label_medaille_argent, R.string.label_medaille_or};
				listeIconesMedailles = new int[] {R.drawable.icone_medaille_bronze, R.drawable.icone_medaille_argent, R.drawable.icone_medaille_or};
			}

			ListWithCompoundDrawableAdapter adapterMedaille = new ListWithCompoundDrawableAdapter(
				getActivity(),
				listeTextesMedailles,
				listeIconesMedailles,
				R.layout.element_liste_utilisateurs,
				ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
			);

			dialogBuilder = new AlertDialog.Builder( getActivity() );
			dialogBuilder.setTitle(R.string.title_attribuer_medaille)
			.setAdapter(adapterMedaille, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int itemSelectionne) {		        					
					// ajout / suppression d'une médaille sur le message
					switch( listeTextesMedailles[itemSelectionne] ) {
					case R.string.label_medaille_bronze:
						attribuerMedaille( Constantes.MEDAILLE_BRONZE, R.drawable.icone_medaille_bronze ) ;
						break;
					case R.string.label_medaille_argent:
						attribuerMedaille( Constantes.MEDAILLE_ARGENT, R.drawable.icone_medaille_argent );
						break;
					case R.string.label_medaille_or:
						attribuerMedaille( Constantes.MEDAILLE_OR, R.drawable.icone_medaille_or );
						break;
					case R.string.label_supprimer_medaille:
						attribuerMedaille("", 0) ;
						break;
					}
					dialog.cancel();
					actionMode.finish();
				}
			})
			.show();    		
		}


		// ajout / suppression d'une médaille sur un message
		private void attribuerMedaille(String medaille, int iconeMedaille) {
			messageCourant.setMedaille(medaille);
			messageDAO.updateMessage(messageCourant);

			intentCourante = new Intent(getActivity(), AttributeurMedailleService.class);
			intentCourante.putExtra(Constantes.CLEF_ID_MESSAGE, idMessage);
			intentCourante.putExtra(Constantes.CLEF_MEDAILLE_MESSAGE, medaille);			
			getActivity().startService(intentCourante);

			// ajout / suppression de l'icone de médaille
			((ImageView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.medaille)).setImageResource(iconeMedaille);			
		}


		// supprime un message ou rétabli un message supprimé
		private void supprimerMessage(final boolean estSupprime) {
			// on bloque l'orientation de l'écran dans son orientation actuelle
			if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
			} else {
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}

			// création d'une fenêtre de chargement
			progressDialog = new ProgressDialog( getActivity() );
			progressDialog.setMessage( getString(R.string.label_chargement) );
			progressDialog.show();

			// création d'un nouveau Thread
			Thread thread = new Thread(new Runnable() {
				public void run() {	    		
					// création et configuration des paramètres d'une requête http
					ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

					try {
						parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_MESSAGE, idMessage) );												// id du message à supprimé
						parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_CAPSULE, idCapsule) );												// id de la capsule
						parametresRequete.add( new BasicNameValuePair( Constantes.CLEF_WEBSERVICE_AUTEUR_MESSAGE, messageCourant.getAuteur().getId() ));	// id de l'auteur du message
						parametresRequete.add( new BasicNameValuePair( Constantes.CLEF_SUPPRIME_PAR_MESSAGE, messageCourant.getSupprimePar() ));			// id de l'utilisateur ayant supprimé le message (0 s'il n'a pas été supprimé)
						parametresRequete.add( new BasicNameValuePair(																						// id de l'utilisateur supprimant le message (modérateur)
							Constantes.CLEF_MODERATEUR_MESSAGE,
							getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, null)
						));
					} catch (Exception e) {
						e.printStackTrace();
					}
					// Envoie d'une requete HTTP supprimant un message
					stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_SUPPRIMER_MESSAGE, parametresRequete, getActivity());

					// si la requete http s'est mal exécuté, stringJSON vaut null, donc on parse pas stringJSON
					if ( stringJSON != null ) {
						erreursSuppressionMessage = ParserJSON.parserRetourSuppressionMessage( stringJSON );
					}

					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							// on ferme la fenête de chargement et on désactive l'orientation forcé de l'écran
							progressDialog.dismiss();
							getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

							// si la requete http s'est mal exécuté, stringJSON vaut null
							if ( stringJSON == null ) {
								Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, getActivity());

							} else if ( erreursSuppressionMessage != null ) {
								// si le message n'est pas déjà supprimé
								if ( !estSupprime ) {
									// si le rôle de l'auteur du message est au dessus du rôle de l'utilisateur
									Toast.makeText(getActivity(), R.string.label_fonctionnalite_indisponible_supprimer_message, Toast.LENGTH_LONG).show();
								} else {
									// si le rôle de l'utilisateur ayant supprimé le message est au dessus du rôle de l'utilisateur
									Toast.makeText(getActivity(), R.string.label_fonctionnalite_indisponible_retablir_message, Toast.LENGTH_LONG).show();
								}
							} else {
								// si le message n'est pas déjà supprimé, on le supprime, sinon, on le rétabli
								if ( !estSupprime ) {
									// on modifie le message courant pour indiquer l'id de l'utilisateur ayant supprimé le message
									messageCourant.setSupprimePar( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, null) );
									messageDAO.updateMessage(messageCourant);
									// on modifie le contenu du message pour l'indiquer comme supprimé
									((TextView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.contenu_message)).setText(R.string.label_message_supprime);
									((TextView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.contenu_message)).setTypeface( Typeface.defaultFromStyle(Typeface.ITALIC) );
									((LinearLayout) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.tags_message)).setVisibility(View.GONE);
									((LinearLayout) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.pjs_message)).setVisibility(View.GONE);
								} else {
									// on modifie le message courant pour l'indiquer comme un message "normale"
									messageCourant.setSupprimePar("0");
									messageDAO.updateMessage(messageCourant);

									// on modifie le message pour afficher son contenue et ses tags
									((TextView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.contenu_message)).setText(messageCourant.getContenu());
									((TextView) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.contenu_message)).setTypeface( Typeface.defaultFromStyle(Typeface.NORMAL) );
									((LinearLayout) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.tags_message)).setVisibility(View.VISIBLE);
									((LinearLayout) listeMessagesLayout.getChildAt(positionMessage).findViewById(R.id.pjs_message)).setVisibility(View.VISIBLE);
								}
							}
						}
					});
				}
			});

			thread.start();
		}


		@Override
		public boolean onDoubleTapEvent(MotionEvent event) {return false;}
	}


	/* ***********************************************************
	 * 
	 * Méthode et classes permettant la gestion des pièces jointes
	 * 
	 * **********************************************************/

	/** démarrer activity pour prendre une photo
	 * 
	 */
	public void priseDePhoto() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getString(R.string.app_name));

		if (! mediaStorageDir.exists()) {
	        mediaStorageDir.mkdirs();
	    }

	    long timeStamp = new java.util.Date().getTime();	    
	    File mediaFile = new File(mediaStorageDir.getPath() + "/IMG_"+ timeStamp + ".jpg");
	    uriPJ = Uri.fromFile(mediaFile);
	    
		takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriPJ);

		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUETE_CAPTURE_IMAGE);
		}
	}
	
	
	/** démarrer activity pour prendre une video
	 * 
	 */
	public void priseDeVideo() {	
		Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getString(R.string.app_name));

		if (! mediaStorageDir.exists()) {
	        mediaStorageDir.mkdirs();
	    }

	    long timeStamp = new java.util.Date().getTime();	    
	    File mediaFile = new File(mediaStorageDir.getPath() + "/VIDEO_"+ timeStamp + ".mp4");
	    uriPJ = Uri.fromFile(mediaFile);
	    
		takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriPJ);

		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {			
			startActivityForResult(takePictureIntent, REQUETE_CAPTURE_VIDEO);
		}
	}
	
	
	/** fonction appelée une fois que file chooser activity a retourné un fichier
	 * 
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
				case REQUETE_CHOIX_IMAGE:					
					String[] filePathColumnImage = {MediaStore.Images.Media.DATA};
					Cursor cursorImage = getActivity().getContentResolver().query(intent.getData(), filePathColumnImage, null, null, null);
	
					if (cursorImage.moveToFirst()) {
						int columnIndex = cursorImage.getColumnIndex(filePathColumnImage[0]);
						String filePath = cursorImage.getString(columnIndex);
						String extension = PieceJointeType.stringAExtension(filePath);
						PieceJointeInformation pjInfo = PieceJointeInformation.getInstance(getActivity());
						
						//On vérifie que la pj respecte les exigences du serveur
						//Contrainte de poids
						long poidsActuelPj = 0;
						for(PieceJointe pj : piecesJointesMessage)
							poidsActuelPj += pj.getTaille();
	
						//contrainte de format
						if(!pjInfo.extensionPhotoAutoriseUpload(extension)){
							String message = getString(R.string.label_ajout_pj_mauvais_format) + extension;
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_mauvais_format, message , getActivity());
							return;
						}
	
						//Contrainte de nombre de pj
						if(piecesJointesMessage.size() == pjInfo.getNombreMax()) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_nombre_maximum, R.string.label_ajout_pj_nombre_maximum , getActivity());
							return;
						}
	
						//Vérification que l'image n'est pas un contenu http (possible avec la gallery samsung)
						if(filePath.contains("http")) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.title_ajout_pj_online , getActivity());
							return;
						}
					
						PieceJointe pieceJointe = new PieceJointe(piecesJointesMessage.size() + "");
						pieceJointe.setType(PieceJointeType.PHOTO);
						pieceJointe.setExtension(extension);
						pieceJointe.setFile(new File(filePath));
						pieceJointe.setCheminVersThumbnail(pieceJointe.getFile().getAbsolutePath());
						pieceJointe.setType(pjInfo.typeExtensionUpload(extension));
	
						poidsActuelPj += pieceJointe.getTaille();
	
						//gestion du poids
						//Le poid n'est calculable qu'après qu'on ai créé la piece jointe avec le file
						if(poidsActuelPj >= pjInfo.getTailleMax()){
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_taille_maximum, R.string.label_ajout_pj_taille_maximum , getActivity());
							return;
						}
						
						piecesJointesMessage.add(pieceJointe);
						ajouterElementMenuPJ(pieceJointe);
						
					} else {
						Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.label_ajout_pj_impossible, getActivity());
					}
					cursorImage.close();				
					break;
				case REQUETE_CAPTURE_IMAGE:					
					if (uriPJ != null) {
						String extension = PieceJointeType.stringAExtension(uriPJ.toString());						
						PieceJointeInformation pjInfo = PieceJointeInformation.getInstance(getActivity());
						
						//On vérifie que la pj respecte les exigences du serveur
						//Contrainte de poids
						long poidsActuelPj = 0;
						for(PieceJointe pj : piecesJointesMessage)
							poidsActuelPj += pj.getTaille();
	
						//contrainte de format
						if(!pjInfo.extensionPhotoAutoriseUpload(extension)){
							String message = getString(R.string.label_ajout_pj_mauvais_format) + extension;
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_mauvais_format, message , getActivity());
							return;
						}
	
						//Contrainte de nombre de pj
						if(piecesJointesMessage.size() == pjInfo.getNombreMax()) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_nombre_maximum, R.string.label_ajout_pj_nombre_maximum , getActivity());
							return;
						}
						
						//Vérification que l'image n'est pas un contenu http (possible avec la gallery samsung)
						if(uriPJ.toString().contains("http")) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.title_ajout_pj_online , getActivity());
							return;
						}
					
						PieceJointe pieceJointe = new PieceJointe(piecesJointesMessage.size() + "");
						pieceJointe.setType(PieceJointeType.PHOTO);
						pieceJointe.setExtension(extension);
						pieceJointe.setFile(new File(uriPJ.getPath()));
						pieceJointe.setCheminVersThumbnail(pieceJointe.getFile().getAbsolutePath());
						pieceJointe.setType(pjInfo.typeExtensionUpload(extension));
	
						poidsActuelPj += pieceJointe.getTaille();
	
						//gestion du poids
						//Le poid n'est calculable qu'après qu'on ai créé la piece jointe avec le file
						if(poidsActuelPj >= pjInfo.getTailleMax()){
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_taille_maximum, R.string.label_ajout_pj_taille_maximum , getActivity());
							return;
						}
						
						piecesJointesMessage.add(pieceJointe);
						ajouterElementMenuPJ(pieceJointe);
						
					} else {
						Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.label_ajout_pj_impossible, getActivity());
					}				
					break;					
				case REQUETE_CHOIX_VIDEO:					
					String[] filePathColumnVideo = {MediaStore.Video.Media.DATA};
					Cursor cursorVideo = getActivity().getContentResolver().query(intent.getData(), filePathColumnVideo, null, null, null);
	
					if (cursorVideo.moveToFirst()) {						
						int columnIndex = cursorVideo.getColumnIndex(filePathColumnVideo[0]);
						String filePath = cursorVideo.getString(columnIndex);
						String extension = PieceJointeType.stringAExtension(filePath);
						PieceJointeInformation pjInfo = PieceJointeInformation.getInstance(getActivity());
						
						//On vérifie que la pj respecte les exigences du serveur
						//Contrainte de poids
						long poidsActuelPj = 0;
						for(PieceJointe pj : piecesJointesMessage)
							poidsActuelPj += pj.getTaille();
	
						//contrainte de format
						if(!pjInfo.extensionVideoAutoriseUpload(extension)){
							String message = getString(R.string.label_ajout_pj_mauvais_format) + extension;
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_mauvais_format, message , getActivity());
							return;
						}
	
						//Contrainte de nombre de pj
						if(piecesJointesMessage.size() == pjInfo.getNombreMax()) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_nombre_maximum, R.string.label_ajout_pj_nombre_maximum , getActivity());
							return;
						}
	
						//Vérification que la video n'est pas un contenu http (possible avec la gallery samsung)
						if(filePath.contains("http")) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.title_ajout_pj_online , getActivity());
							return;
						}
					
						PieceJointe pieceJointe = new PieceJointe(piecesJointesMessage.size() + "");
						pieceJointe.setType(PieceJointeType.VIDEO);
						pieceJointe.setExtension(extension);
						pieceJointe.setFile(new File(filePath));
						pieceJointe.setCheminVersThumbnail(pieceJointe.getFile().getAbsolutePath());
						pieceJointe.setType(pjInfo.typeExtensionUpload(extension));
	
						poidsActuelPj += pieceJointe.getTaille();
	
						//gestion du poids
						//Le poid n'est calculable qu'après qu'on ait créé la piece jointe avec le file
						if(poidsActuelPj >= pjInfo.getTailleMax()){
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_taille_maximum, R.string.label_ajout_pj_taille_maximum , getActivity());
							return;
						}
						
						piecesJointesMessage.add(pieceJointe);
						ajouterElementMenuPJ(pieceJointe);
						
					} else {
						Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.label_ajout_pj_impossible, getActivity());
					}
					cursorVideo.close();	
					break;
				case REQUETE_CAPTURE_VIDEO:
					
					Log.i("test", "REQUETE_CAPTURE_VIDEO");
					Log.i("test", "uriPJ : "+ uriPJ);
					
					if (uriPJ != null) {						
						String extension = PieceJointeType.stringAExtension(uriPJ.toString());						
						PieceJointeInformation pjInfo = PieceJointeInformation.getInstance(getActivity());
						
						//On vérifie que la pj respecte les exigences du serveur
						//Contrainte de poids
						long poidsActuelPj = 0;
						for(PieceJointe pj : piecesJointesMessage)
							poidsActuelPj += pj.getTaille();
	
						//contrainte de format
						if(!pjInfo.extensionVideoAutoriseUpload(extension)){
							String message = getString(R.string.label_ajout_pj_mauvais_format) + extension;
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_mauvais_format, message , getActivity());
							return;
						}
	
						//Contrainte de nombre de pj
						if(piecesJointesMessage.size() == pjInfo.getNombreMax()) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_nombre_maximum, R.string.label_ajout_pj_nombre_maximum , getActivity());
							return;
						}
	
						//Vérification que l'image n'est pas un contenu http (possible avec la gallery samsung)
						if(uriPJ.toString().contains("http")) {
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.title_ajout_pj_online , getActivity());
							return;
						}
					
						PieceJointe pieceJointe = new PieceJointe(piecesJointesMessage.size() + "");
						pieceJointe.setType(PieceJointeType.VIDEO);
						pieceJointe.setExtension(extension);
						pieceJointe.setFile(new File(uriPJ.getPath()));
						pieceJointe.setCheminVersThumbnail(pieceJointe.getFile().getAbsolutePath());
						pieceJointe.setType(pjInfo.typeExtensionUpload(extension));
	
						poidsActuelPj += pieceJointe.getTaille();
	
						//gestion du poids
						//Le poid n'est calculable qu'après qu'on ait créé la piece jointe avec le file
						if(poidsActuelPj >= pjInfo.getTailleMax()){
							Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_taille_maximum, R.string.label_ajout_pj_taille_maximum , getActivity());
							return;
						}
						
						piecesJointesMessage.add(pieceJointe);
						ajouterElementMenuPJ(pieceJointe);
						
					} else {
						Utiles.afficherInfoAlertDialog(R.string.title_ajout_pj_impossible, R.string.label_ajout_pj_impossible, getActivity());
					}				
					break;
				case MESSAGE_CHOISI_SUR_CARTE:		
					//Morceaux de code appelé dès qu'un utilisateur choisi un message dans la vu de carte
					//On scroll la liste jusqu'au message
					if (intent != null && intent.hasExtra("id_message")) {
						scrollListeJusquaMessage(intent.getStringExtra("id_message"));
					}
					break;
				default:
					break;
			}
		}    
	}

	//Méthode permetant de scroller dans la liste des message jusqu'a celui passé en parametre.
	//Cette méthode utilise l'id du message et le compare à celui contenu dans le tag de la vue du contenu message
	private void scrollListeJusquaMessage(String id){
		int nombreMessage = listeMessagesLayout.getChildCount();

		for (int i=0; i < nombreMessage; i++){
			final View v = listeMessagesLayout.getChildAt(i);
			String idMessage = (String)v.findViewById(R.id.contenu_message).getTag();

			if(idMessage.equals(id)){
				new Handler().post(new Runnable() {
					@Override
					public void run() {
						listeMessagesScrollView.scrollTo(0, v.getTop());
					}
				});
				break;
			}
		}
	}

	/**
	 * Méthode appelé pour lancer la sélection de l'image dans la gallery
	 * @param choix
	 */
	private void ouvreAjoutChoixPieceJointe(int choix){		
		if (choix == REQUETE_CHOIX_IMAGE) {
			Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			pickIntent.setType("image/*");

			Intent chooserIntent = Intent.createChooser(pickIntent, getResources().getString(R.string.button_ajouter_image));

			startActivityForResult(chooserIntent, REQUETE_CHOIX_IMAGE);
		}
		else if (choix == REQUETE_CHOIX_VIDEO) {
			Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
			pickIntent.setType("video/*");

			Intent chooserIntent = Intent.createChooser(pickIntent, getResources().getString(R.string.button_ajouter_video));			
			
			startActivityForResult(chooserIntent, REQUETE_CHOIX_VIDEO);
		}
	}

	/**
	 * Méthode permettant d'ajouter une vignette piece jointe au formulaire d'envoie de message
	 * @param obj
	 */
	private void ajouterElementMenuPJ(PieceJointe pieceJointe) {

		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
			(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()),
			(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics())
		);

		layoutParams.setMargins(
			(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()), 
			(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()), 
			(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()), 
			(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics())
		);

		final ImageView imageView = new ImageView(getActivity());
		imageView.setLayoutParams(layoutParams);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		
		if (pieceJointe.getType().equals(PieceJointeType.PHOTO)) {
			imageView.setImageBitmap(
				Utiles.scaleBitmap(
					Utiles.fichierVersBitmap(pieceJointe.getFile().getAbsolutePath()),
					(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics())
				)
			);
		} else if (pieceJointe.getType().equals(PieceJointeType.VIDEO)) {
			imageView.setImageBitmap(
				Utiles.scaleBitmap(
					ThumbnailUtils.createVideoThumbnail(pieceJointe.getFile().getAbsolutePath(), MediaStore.Images.Thumbnails.MINI_KIND),
					(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics())
				)
			);
		}

		imageView.setTag(pieceJointe.hashCode());
		//Sur la selection d'une image on propose ça suppression
		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				supprimeHolder((Integer)v.getTag());
				((LinearLayout) layoutEcranLectureMessages.findViewById(R.id.pjs_menu_liste)).removeView(v);
			}

			void supprimeHolder(int tag){
				for(PieceJointe pj : piecesJointesMessage){
					if(pj.hashCode() == tag){
						piecesJointesMessage.remove(pj);
						return;
					}
				}
			}
		});

		((LinearLayout) layoutEcranLectureMessages.findViewById(R.id.pjs_menu_liste)).addView(imageView);
	}

	/*
	 * Méthode appelé de manière asynchrone dès que le service met à jour les messages dans la bdd
	 * @see cape.pairform.controleurs.services.communication.IMessage#messageMiseAJour(boolean)
	 */
	@Override
	public void messageMiseAJour(boolean reussite) {
		if(reussite){
			listeMessagesLayout.removeAllViews();
			listeMessages.clear();
			afficherMessages();
		}
	}


}
