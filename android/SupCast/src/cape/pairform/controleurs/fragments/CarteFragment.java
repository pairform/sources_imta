package cape.pairform.controleurs.fragments;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cape.pairform.R;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Message;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.Tag;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.PiecesJointesDAO;
import cape.pairform.utiles.Utiles;


/*
 * Fragment basé sur le code de google pour afficher une carte
 */
public class CarteFragment extends Fragment implements OnMapReadyCallback{

	private SupportMapFragment carteSupportFragment;
	private String nomPage = "";		
	private String idCapsule;
	private String nomTagPage;
	private String numOccurenceTagPage;
	private MessageDAO messageDAO;
	private ArrayList<Message> messageGeolocalise = null;
	private String languesAffichees;
	private ProgressDialog progressDialog;
	private HashMap<Marker, Message> markeurMessageTab = new HashMap<Marker, Message>();	
	private PiecesJointesDAO pjsDAO; 
	private ProgressDialog telechargementDialog;
	private AlertDialog messageDialog;
	private View messageDialogView;

	//HashMap qui va contenir les données renvoyé du serveur trié selon le nom serveur qui est unique
	private static HashMap<String, byte[]> donneeServeur = new HashMap<String, byte[]>();
	private File ancienFichierTmpAffichagePJGallerie; 
	private boolean estTransversale = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(getActivity(), Constantes.CLEF_ECRAN_CARTE, Constantes.AIDE_ECRAN_CARTE);
		
		if(getArguments().getString(Constantes.CLEF_TRANSVERSALE, "").equals(Constantes.CLEF_TRANSVERSALE)){
			estTransversale = true;
		}else{
			estTransversale = false;
			nomPage = getArguments().getString(Constantes.CLEF_NOM_PAGE_CAPSULE, "");
			idCapsule = getArguments().getString(Constantes.CLEF_ID_CAPSULE);
			nomTagPage = getArguments().getString(Constantes.CLEF_NOM_TAG_PAGE_MESSAGE);
			numOccurenceTagPage = getArguments().getString(Constantes.CLEF_NUM_OCCURENCE_TAG_PAGE_MESSAGE);
		}
		languesAffichees = Utiles.StringListeLanguesUtilisateur( getActivity() );

		messageDAO = BDDFactory.getMessageDAO(getActivity().getApplicationContext());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_carte, null, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		//On insère dans notre fragment le fragment de google pour afficher une carte
		FragmentManager fm = getChildFragmentManager();
		carteSupportFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
		if (carteSupportFragment == null) {
			carteSupportFragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.map, carteSupportFragment).commit();
		}
		//Chargement de la carte asynchrone
		carteSupportFragment.getMapAsync(this);

		//On créer les différentes fenêtre de dialog
		telechargementDialog = new ProgressDialog(getActivity());

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setCancelable(true);
		messageDialogView = getActivity().getLayoutInflater().inflate(R.layout.layout_carte_marker, null);
		builder.setView(messageDialogView);
		messageDialog = builder.create();
		messageDialog.setCanceledOnTouchOutside(true);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onDetach() {
		super.onDetach();

		/*
		 * Pour éviter les fuites mémoires et de mémoires -
		 */
		if(ancienFichierTmpAffichagePJGallerie != null)
			ancienFichierTmpAffichagePJGallerie.delete();

		if(messageDialog != null)
			messageDialog.dismiss();

		if(progressDialog != null)
			progressDialog.dismiss();

		if(telechargementDialog != null)
			telechargementDialog.dismiss();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		//SI on reçoit un avertissement de mémoire on supprime les images téléchargés du serveur
		donneeServeur.clear();
	}

	/*
	 * Méthode async appelée dès que la carte est disponible = affichée
	 */
	@Override
	public void onMapReady(final GoogleMap carte) {
		carte.setMyLocationEnabled(true);

		carte.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng point) {


			}
		});

		/*
		 * Evenement déclenché lors du clic d'un marqueur
		 */
		carte.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {
				//On récupère le message clické
				Message message = markeurMessageTab.get(marker); 

				ajouterMessage(message);
				//On l'affiche dans une pop up
				messageDialog.show();

				return true;
			}
		});

		//Si il n'y aucun message dans le tableau des messages
		//= on n'a pas récupéré de message
		if(messageGeolocalise == null){
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getString(R.string.label_chargement));
			if(!progressDialog.isShowing())
				progressDialog.show();

			//On les récupére
			new Thread(new Runnable() {
				public void run() {
					if(estTransversale)
						messageGeolocalise = messageDAO.getAllMessagesGeolocalise(languesAffichees);
					else
						messageGeolocalise = messageDAO.getMessagesGeolocalise(idCapsule, nomPage, nomTagPage, numOccurenceTagPage, Constantes.CLASSEMENT_DESCENDANT, languesAffichees);

					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							//On les ajoutes en tant que marqueurs sur la carte
							//triMessage();
							afficheMarkeur(carte);
							progressDialog.hide();
						}
					});
				} 
			}).start();
		}
	}

	//Méthode permettant d'ajouter un marqueur sur la carte
	private void afficheMarkeur(GoogleMap carte) {
		for(Message msg: messageGeolocalise){
			if(msg.getGeoLatitude() != 0 || msg.getGeoLongitude() != 0){
				Marker m = carte.addMarker(new MarkerOptions()
						.position(new LatLng(msg.getGeoLatitude(), msg.getGeoLongitude()))
						.title(msg.getAuteur().getPseudo())
						);
				markeurMessageTab.put(m, msg);
			}
		}
	}

	//Méthode permettant d'ajouter les information d'un message sur la pop up de visualisation
	private void ajouterMessage(final Message messageCourant) {
		//On nettoie le layout des pjs des anciennes
		((LinearLayout) messageDialogView.findViewById(R.id.pjs_message)).removeAllViews();

		// date de creation du message
		SimpleDateFormat dateFormat = new SimpleDateFormat(getString(R.string.android_format_date), Locale.US);
		Date dateCreation = new Date( messageCourant.getDateCreation() * 1000 );		// * 1000 : permet de transformer le timestamp (qui est en second) en millisecond
		((TextView) messageDialogView.findViewById(R.id.date_message)).setText( dateFormat.format(dateCreation) );
		// id de l'auteur du message
		messageDialogView.findViewById(R.id.pseudo_utilisateur).setTag( messageCourant.getAuteur().getId() );
		// role de l'auteur pour traduction
		((TextView) messageDialogView.findViewById(R.id.role_utilisateur)).setText( getString(
				getResources().getIdentifier("label_utilisateur_" + messageCourant.getIdRoleAuteur(), "string", getActivity().getPackageName())
				) );

		// pseudo de l'auteur du message
		((TextView) messageDialogView.findViewById(R.id.pseudo_utilisateur)).setText( messageCourant.getAuteur().getPseudo() );

		// contenu du message
		// si le message n'a pas été supprimé (id utilisateur == "0"), on affiche le contenu du message
		if ( messageCourant.getSupprimePar().equals("0") ) {
			((TextView) messageDialogView.findViewById(R.id.contenu_message)).setText( messageCourant.getContenu() );
		} else {
			((TextView) messageDialogView.findViewById(R.id.contenu_message)).setText(R.string.label_message_supprime);
			((TextView) messageDialogView.findViewById(R.id.contenu_message)).setTypeface( Typeface.defaultFromStyle(Typeface.ITALIC) );
		}
		((TextView) messageDialogView.findViewById(R.id.contenu_message)).setMovementMethod(new ScrollingMovementMethod());

		// medaille
		if ( messageCourant.getMedaille().equals(Constantes.MEDAILLE_BRONZE) ) {
			((ImageView) messageDialogView.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_bronze);			
		} else if( messageCourant.getMedaille().equals(Constantes.MEDAILLE_ARGENT) ) {
			((ImageView) messageDialogView.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_argent);			
		} else if( messageCourant.getMedaille().equals(Constantes.MEDAILLE_OR) ) {
			((ImageView) messageDialogView.findViewById(R.id.medaille)).setImageResource(R.drawable.icone_medaille_or);			
		}

		// défi (en cours ou finis)
		if ( messageCourant.getDefi() == Constantes.DEFI_EN_COURS ) {
			((ImageView) messageDialogView.findViewById(R.id.defi)).setImageResource( R.drawable.icone_defi_pas_fini );
		} else if ( messageCourant.getDefi() == Constantes.DEFI_FINIS ) {
			((ImageView) messageDialogView.findViewById(R.id.defi)).setImageResource( R.drawable.icone_defi_fini );
		}

		// réponse valide à un défi
		if ( messageCourant.isReponseDefiValide() ) {
			((ImageView) messageDialogView.findViewById(R.id.defi)).setImageResource( R.drawable.icone_reponse_defi_valide );
		}

		// langue du message
		// si l'utilisateur est un expert, un admin, ou le posteur du message
		String idUtilisateurCourant = getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, null);
		if(messageCourant.getAuteur().getId().equals(idUtilisateurCourant) || Integer.parseInt( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(idCapsule, "0") ) >= Constantes.ID_EXPERT) {
			int id = getResources().getIdentifier("cape.pairform:drawable/icone_drapeau_" + Utiles.codeLangueAvecId(messageCourant.getIdLangue(), this.getActivity()), null, null);
			((ImageView) messageDialogView.findViewById(R.id.langue_message)).setImageResource(id);
		}

		// si le message n'a pas été supprimé
		if ( messageCourant.getSupprimePar().equals("0") ) {
			((LinearLayout) messageDialogView.findViewById(R.id.tags_message)).removeAllViews();
			for (Tag tagCourant : messageCourant.getListeTags()) {
				Button tagCourantButton = (Button) getActivity().getLayoutInflater().inflate( R.layout.tag, null, false );
				tagCourantButton.setText( tagCourant.getNomTag() );
				((LinearLayout) messageDialogView.findViewById(R.id.tags_message)).addView(tagCourantButton);
			}
		}

		ajouterPiecesJointesAuMessage(messageCourant);

		// avatar de l'utilisateur
		((ImageView) messageDialogView.findViewById(R.id.avatar_utilisateur)).setImageURI( Uri.parse(messageCourant.getAuteur().getUrlAvatar()) );

		// utilite du message
		messageDialogView.findViewById(R.id.utilite).setTag( messageCourant.getVoteUtilite() );            			

		// si l'utilisateur est connecté
		if( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).contains(Constantes.CLEF_ID_UTILISATEUR) ) {
			// si l'utilisateur est l'auteur du message
			if ( getActivity().getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0).getString(Constantes.CLEF_ID_UTILISATEUR, "").equals(messageDialogView.findViewById(R.id.pseudo_utilisateur).getTag()) ) {
				messageDialogView.findViewById(R.id.augmenter_utilite).setAlpha(0.25f);
				messageDialogView.findViewById(R.id.baisser_utilite).setAlpha(0.25f);
			}// si l'utilisateur a déjà voté positivement le message, sinon, s'il à déjà voté négativement

			else if( messageCourant.getVoteUtilite().equals(Constantes.UTILISATEUR_A_VOTE_PLUS) ) {
				ImageView utilitePlus = (ImageView) messageDialogView.findViewById(R.id.augmenter_utilite);
				utilitePlus.setImageResource(R.drawable.icone_utilite_plus_vert);
			} else if( messageCourant.getVoteUtilite().equals(Constantes.UTILISATEUR_A_VOTE_MOINS) ) {
				ImageView utiliteMoins = (ImageView) messageDialogView.findViewById(R.id.baisser_utilite);
				utiliteMoins.setImageResource(R.drawable.icone_utilite_moins_rouge);
			}
		}

		messageDialogView.findViewById(R.id.utilite_message).setTag( Integer.valueOf(messageCourant.getUtilite()) );
		((TextView) messageDialogView.findViewById(R.id.utilite_message)).setText( String.valueOf(messageCourant.getUtilite()) );

		messageDialogView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				messageDialog.hide();
				Intent intent = getActivity().getIntent();
				intent.putExtra("id_message", messageCourant.getId());

				//finir activity et envoyer les paramètres d'un message affiché sur marker à activity supérieur 
				getActivity().setResult(Activity.RESULT_OK, intent);
				getActivity().finish();
			}
		});
	}

	//Méthode permettant d'ajouter les information des pjs d'un message sur la pop up de visualisation
	private void ajouterPiecesJointesAuMessage(Message messageCourant) {
		pjsDAO = BDDFactory.getPieceJointeDAO( getActivity().getApplicationContext() );

		ArrayList<PieceJointe> pjs = pjsDAO.getPiecesJointesByIdMessage(messageCourant.getId());

		for(final PieceJointe pj : pjs){

			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics()),
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics())
			);

			lp.setMargins(0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()), 0);

			final ImageView imageView = new ImageView(this.getActivity());
			imageView.setLayoutParams(lp);

			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setImageBitmap(Utiles.fichierVersBitmap(pj.getCheminVersThumbnail()));
			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!Utiles.reseauDisponible( getActivity() )) {
						Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
					} else {
						telechargementDialog.setMessage(getString(R.string.label_dialog_telechargement));
						telechargementDialog.show();

						new Thread(new Runnable() {
							public void run() {

								boolean contientDonnee =  donneeServeur.containsKey(pj.getNomServeur());

								final byte[] data = contientDonnee ? donneeServeur.get(pj.getNomServeur()) : Utiles.telechargerPj(pj, getActivity());

								if(!contientDonnee)
									donneeServeur.put(pj.getNomServeur(), data);

								if(ancienFichierTmpAffichagePJGallerie != null)
									ancienFichierTmpAffichagePJGallerie.delete();

								getActivity().runOnUiThread(new Runnable() {
									public void run() {
										if(data != null && data.length > 0){
											ancienFichierTmpAffichagePJGallerie = Utiles.enregistrerFichier(getActivity().getExternalCacheDir().getAbsolutePath(), pj.getNomServeur(), data);
											Log.d("TAG", "Chemin de mon fichier : " + ancienFichierTmpAffichagePJGallerie.getAbsolutePath());
											Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);

											telechargementDialog.hide();

											String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(ancienFichierTmpAffichagePJGallerie).toString());
											String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
											myIntent.setDataAndType(Uri.fromFile(ancienFichierTmpAffichagePJGallerie),mimetype);
											startActivity(myIntent);	
										}
									}
								});
							}
						}).start();
					}
				}
			});

			((LinearLayout) messageDialogView.findViewById(R.id.pjs_message)).addView(imageView);
		}
	}
}
