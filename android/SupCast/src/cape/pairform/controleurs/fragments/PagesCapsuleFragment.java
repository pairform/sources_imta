package cape.pairform.controleurs.fragments;

import java.util.ArrayList;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import cape.pairform.R;
import cape.pairform.controleurs.adapters.ListeWebViewPagerAdapter;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.CapsuleDAO;
import cape.pairform.utiles.Utiles;


/*
 * Fragment affichant les pages de la capsule
 */
public class PagesCapsuleFragment extends Fragment {
	
	private CapsuleDAO capsuleDAO;
	private ViewPager viewPager;												// contient la liste des pages, permet le parcoure de la liste avec des swipes
	private ListeWebViewPagerAdapter listeWebViewAdapter;						// adapter de la liste des pages (représenté par des Webview)
	private String nomPageCourante;												// nom de la page ouverte par l'utilisateur
	private String idCapsule;													// id de la capsule consultée par l'utilisateur
	private String selecteursCapsule;											// selecteurs HTML/CSS de la capsule consultée par l'utilisateur
	private ArrayList<String> listeNomPagesCapsule = new ArrayList<String>();	// liste des noms de toutes les pages de la capsule
	private int indexPageCourante;												// index de la page que l'utilisateur à ouverte
	private Menu menu;															// menu de l'écran
	
	
	@Override
	public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
        Utiles.determinerAideContextuelle(getActivity(), Constantes.CLEF_ECRAN_PAGE_CAPSULE, Constantes.AIDE_ECRAN_PAGE_CAPSULE);
        
        // on récupère les données transmises par l'activity parent
        idCapsule = getArguments().getString(Constantes.CLEF_ID_CAPSULE);
		nomPageCourante = getArguments().getString(Constantes.CLEF_NOM_PAGE_CAPSULE);
		listeNomPagesCapsule = getArguments().getStringArrayList(Constantes.CLEF_LISTE_NOM_PAGE_CAPSULE);  
		// active la possibilité (pour le fragment) de manipuler le menu de l'activity parent
        setHasOptionsMenu(true);
        
        // récupération des sélécteurs HTML/CSS de la capsule consultée par l'utilisateur
 		capsuleDAO = BDDFactory.getCapsuleDAO(getActivity().getApplicationContext());
 		selecteursCapsule = capsuleDAO.getSelecteursCapsuleById( idCapsule );
 		capsuleDAO.close();
    }
	
	
	@Override
	public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		viewPager = (ViewPager) inflater.inflate( R.layout.layout_ecran_pages_capsule, container, false );
		
		// pour chaque page de la capsule
		for (int i=0; i < listeNomPagesCapsule.size(); i++) {
			// si la page est celle ouverte par l'utilisateur
			if ( listeNomPagesCapsule.get(i).equals(nomPageCourante) ) {
				// on sauvegarde l'index de la page
	        	indexPageCourante = i;
	        }
		}
		
		listeWebViewAdapter = new ListeWebViewPagerAdapter(
			getActivity(),
			listeNomPagesCapsule,
			idCapsule,
			selecteursCapsule,
			viewPager
		);
		// mise à jour du viewpager avec l'adapter contenant la liste des pages, le viewpager est initialisé avec l'index courant pour afficher la page ouverte par l'utilisateur
		viewPager.setAdapter(listeWebViewAdapter);
		viewPager.setCurrentItem(indexPageCourante);
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				nomPageCourante = listeNomPagesCapsule.get(position);
			}
		});
		
		return viewPager;
	}
	
	
	@Override
	public void onPrepareOptionsMenu (Menu menuParent) {
		menu = menuParent;
		// mise à jour du menu (partagé par plusieurs fragments)
		menu.findItem(R.id.button_accueil).setVisible(false);
		menu.findItem(R.id.button_sommaire_capsule).setVisible(true);
		menu.findItem(R.id.button_recuperer_nouveaux_messages).setVisible(false);
		menu.findItem(R.id.button_lire_messages).setVisible(false);
		menu.findItem(R.id.button_ecrire_message).setVisible(false);
	}
	
	
	// gestion du changement d'orientation
	@Override
   	public void onConfigurationChanged(Configuration newConfig) {
  		super.onConfigurationChanged(newConfig);
  	}
}
