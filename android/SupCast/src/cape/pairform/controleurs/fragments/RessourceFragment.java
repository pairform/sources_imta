package cape.pairform.controleurs.fragments;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cape.pairform.R;
import cape.pairform.controleurs.activities.EcranTableauDeBordActivity;
import cape.pairform.controleurs.adapters.RessourcePagerAdapter;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Ressource;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.ReferencesBDD;
import cape.pairform.modele.bdd.RessourceDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * écran affichant les infos sur une ressource et la liste de ses capsules
 */
public class RessourceFragment extends Fragment implements ReferencesBDD {

	private ProgressDialog progressDialog;							// fenêtre de chargement
	private LinearLayout ressourceView;								// layout racine de l'écran
	private ViewPager viewPagerRessource;							// view contenant les infos sur une ressource et la liste de ses capsules, permet le parcoure avec des swipes
	private RessourcePagerAdapter ressourcePagerAdapter;			// adapter personnalisée gérant les infos sur une ressource et la liste de ses capsules
	private String idRessourceCourante;
	private Ressource ressourceCourante;
	private RessourceDAO ressourceDAO;
	private MessageDAO messageDAO;
	private Cursor curseur;
	private HashMap<String,String> listeNbMessagesNonLus = new HashMap<String,String>();	// liste du nb de messages non-lus sur chaque capsule de la ressource courante
	private HashMap<String,String> listeNbMessagesLus = new HashMap<String,String>();		// liste du nb de messages lus sur chaque capsule de la ressource courante
	private Canvas canvas;
    private Bitmap iconeResource;									// icone d'une ressource
    private Bitmap iconeLangueRessource;							// icone d'indication de la langue de la ressource
    private String codelangueRessource;
    private boolean ressourceTelechargee = false;					// true si la ressource est une ressource déjà téléchargée (et donc présente en BDD), false sinon
    private TransformateurIconeTask transformateurIconeTask;
	
    
	@Override
	public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    	// création d'une fenêtre de chargement
    	progressDialog = new ProgressDialog(getActivity());
	    progressDialog.setMessage( getString(R.string.label_chargement) );
	    progressDialog.show();
	    
        // on bloque l'orientation de l'écran dans son orientation actuelle
    	if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
    		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    	}
    	else {
    		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
    	}

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
		Utiles.determinerAideContextuelle(getActivity(), Constantes.CLEF_ECRAN_RESSOURCE, Constantes.AIDE_ECRAN_RESSOURCE);
		
		idRessourceCourante = getArguments().getString(Constantes.CLEF_ID_RESSOURCE);

		ressourceDAO = BDDFactory.getRessourceDAO(getActivity().getApplicationContext());
		ressourceCourante = ressourceDAO.getRessourceById( idRessourceCourante );
    	    			
		// si la ressource a des capsules déjà telechargées
		if (ressourceCourante != null ) {
			ressourceTelechargee = true;
			
	    	// récupération du nombre de messages non-lus de chaque capsule de la ressource
			messageDAO = BDDFactory.getMessageDAO(getActivity().getApplicationContext());
			curseur = messageDAO.getNbMessagesByCapsules( idRessourceCourante, false );
			
			// on passe les informations du curseur dans une hashmap
			while (curseur.moveToNext()) {
				listeNbMessagesNonLus.put(
					curseur.getString( curseur.getColumnIndex(COLONNE_ID_CAPSULE_MESSAGE) ),
					curseur.getString( curseur.getColumnIndex(COLONNE_LU) )
				);
			}
			
			// récupération du nombre de messages lus de chaque capsule de la ressource
			curseur = messageDAO.getNbMessagesByCapsules( idRessourceCourante, true );
			
			// on passe les informations du curseur dans une hashmap
			while (curseur.moveToNext()) {
				listeNbMessagesLus.put(
					curseur.getString( curseur.getColumnIndex(COLONNE_ID_CAPSULE_MESSAGE) ),
					curseur.getString( curseur.getColumnIndex(COLONNE_LU) )
				);
			}
			messageDAO.close();
			curseur.close();
		}
		ressourceDAO.close();
		
        // active la possibilité (pour le fragment) de manipuler le menu de l'activity parent
        setHasOptionsMenu(true);
	}
	
	
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		
		ressourceView = (LinearLayout) inflater.inflate( R.layout.layout_ecran_ressource, container, false );

		// dans le cas d'une ressource non-téléchargée, on fait une requête http pour récupérer des infos sur la ressource
		if (!ressourceTelechargee) {
			recupererInfosRessource();
		} else {	// Sinon, on a déjà les infos, donc on affiche à l'écran la ressource sélectionnée par l'utilisateur
	        // on récupère l'icone de la ressource et on crée une Bitmap correspondant à l'icone de la ressource modifiée
			iconeResource = BitmapFactory.decodeFile( ressourceCourante.getUrlLogo() ).copy(Bitmap.Config.ARGB_8888, true);
			
			// Recuperation de l'icone drapeau associé à la langue de la ressource
			codelangueRessource = ressourceCourante.getIdLangue();
			codelangueRessource = Utiles.codeLangueAvecId(codelangueRessource, getActivity());
			iconeLangueRessource = BitmapFactory.decodeResource(
				getResources(), 
				getResources().getIdentifier("icone_drapeau_rond_" + codelangueRessource, "drawable", getActivity().getPackageName())
			);
			
			canvas = new Canvas( iconeResource );
	        canvas.drawBitmap(iconeLangueRessource, 
	        	iconeResource.getWidth() - iconeLangueRessource.getWidth(),
	        	iconeResource.getHeight() - iconeLangueRessource.getHeight(), 
	        	null
	        );
			
	        ((ImageView) ressourceView.findViewById(R.id.logo_ressource)).setImageDrawable( new BitmapDrawable(getResources(), iconeResource) );
		
	        // on désactive l'orientation forcé de l'écran et la fenetre de chargement
 			getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
 			progressDialog.dismiss();
	     			
			afficherRessourceCourante();
		}
		
		return ressourceView;
	}
	
	
	// récupére des infos sur la ressource
	private void recupererInfosRessource() {	    
    	// création d'un nouveau Thread
	    Thread thread = new Thread(new Runnable() {
	    	private String stringJSON;
	    	
	    	public void run() {
	    		// création et configuration des paramètres d'une requête http
	    		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
	        	
	            try {
	            	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_LANGUES_AFFICHAGE, Utiles.getListeIdLangues(getActivity())));		// langues de l'affichage des ressources des préférences
	            	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_RESSOURCE, idRessourceCourante));								// l'id de la ressource est le premier élément du tableau (cf. fichier Constantes.java)
		            
		            // Envoie d'une requete HTTP récupérant les infos sur les ressources pouvant être téléchargé
		        	stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECUPERER_INFOS_RESSOURCE, parametresRequete, getActivity());
		        	
		        	// si la requete http s'est mal exécutée, stringJSON vaut null, donc on ne parse pas stringJSON
		        	if ( stringJSON != null ) {
			            // transformation de la réponse en objet JSON
		        		JSONObject retourJSON = null;
			           	retourJSON = new JSONObject(stringJSON);
						
			           	ressourceCourante = ParserJSON.parserRessource(retourJSON);
			        }
		        	
	            } catch (Exception e) {
	            	e.printStackTrace();        	
	            }
	            
	        	getActivity().runOnUiThread(new Runnable() {
	    			public void run() {
				        // on désactive l'orientation forcé de l'écran et la fenetre de chargement
						getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
						progressDialog.dismiss();
						
	    				// si la requete http s'est mal exécuté, stringJSON vaut null
	    				if ( stringJSON != null ) {
	    					// si la ressource est une ressource téléchargée, on récupère l'icone en local, sinon, on la récupère via son URL web
	    					transformateurIconeTask = new TransformateurIconeTask();
	    		    		transformateurIconeTask.execute();
	    		    		
	    					afficherRessourceCourante();
		    			} else {	
					        Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, getActivity());
						}
	    	        }
	    	    });
	    	}
	    });
	    
	    thread.start();
    }

	
	// affiche à l'écran la ressource sélectionnée par l'utilisateur
	private void afficherRessourceCourante() {		
        // nom (court et long) de la ressource
		((TextView) ressourceView.findViewById(R.id.nom_court_ressource)).setText( ressourceCourante.getNomCourt() );
		((TextView) ressourceView.findViewById(R.id.nom_long_ressource)).setText( ressourceCourante.getNomLong() );
		
		viewPagerRessource = (ViewPager) ressourceView.findViewById(R.id.view_pager_ressource);

		ressourcePagerAdapter = new RessourcePagerAdapter(
			getActivity(),
			viewPagerRessource,
			ressourceCourante,
			listeNbMessagesNonLus,
			listeNbMessagesLus
		);

		// mise à jour du viewpager avec l'adapter contenant la liste des pages,
        viewPagerRessource.setAdapter(ressourcePagerAdapter);
	}
	
	
	// afichage des boutons du menu, selon l'activity parent
	@Override
	public void onPrepareOptionsMenu (Menu menu) {
		// si l'activity parent est EcranTableauDeBordActivity
		if (getActivity().getClass() == EcranTableauDeBordActivity.class) {			
			// mise à jour du menu (partagé par plusieurs fragments)
			menu.findItem(R.id.button_ajouter_ressource).setVisible(false);
			
	        // s'il n'y a pas de ressources à mettre à jour, on masque le bouton de mise à jour des ressources
	        if ( getActivity().getSharedPreferences(Constantes.FICHIER_LISTE_RESSOURCES_MISES_A_JOUR, 0).getAll().isEmpty() ) {
	        	menu.findItem(R.id.button_mettre_a_jour_ressources).setVisible(false);
	        } else {
	        	menu.findItem(R.id.button_mettre_a_jour_ressources).setVisible(true);
	        }
		} else {	// sinon, l'activity parent est EcranAjoutRessourceActivity
			// mise à jour du menu (partagé par plusieurs fragments)
			menu.findItem(R.id.button_grille_ressources).setVisible(false);
			menu.findItem(R.id.button_filtrer_ressources).setVisible(false);
		}
	}
    
    
    // gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		switch (item.getItemId()) {
			case R.id.button_mettre_a_jour_ressources: 				
				// on ouvre une fenetre contenant la liste des ressources ayant une mise à jour de disponible
				/*ListWithCompoundDrawableAdapter adapter = new ListWithCompoundDrawableAdapter(
					getActivity(),
		    		listeNomRessourcesMisesAJour,
		    		listeIconesRessourcesMisesAJour,
		    		R.layout.element_liste_ressource,
		    		ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		        );
				
				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
				dialogBuilder.setTitle(R.string.title_mettre_a_jour_ressource)
	 				.setAdapter(adapter, new DialogInterface.OnClickListener() {
	 					@Override
	 					public void onClick(DialogInterface dialog, int itemSelectionne) {
	 						idRessourceCourante = listeIdRessourcesMisesAJour.get(itemSelectionne);
	 						mettreAJourRessource( itemSelectionne );
	 					}
	 		     	})
	 		     	.show();*/
				return true;
 		}
 		return false;
 	}
	
	
	// gestion du changement d'orientation
	@Override
  	public void onConfigurationChanged(Configuration newConfig) {
 		super.onConfigurationChanged(newConfig);
 	}
	
	
	/*
	 * classe interne permettant la transformation d'icones en BitmapDrawable avec ajout de l'icone drapeau de la langue de la ressource
	 */
	private class TransformateurIconeTask extends AsyncTask<Integer, Void, Bitmap> {
		private URL urlLogoCourant;
		
		@Override
	    protected Bitmap doInBackground(Integer... tab) {			
	    	try {
				// on duplique l'icone pour pouvoir la modifier
				urlLogoCourant = new URL ( Constantes.SERVEUR_NODE_URL + ressourceCourante.getUrlLogo() );
				iconeResource = BitmapFactory.decodeStream(urlLogoCourant.openStream()).copy(Bitmap.Config.ARGB_8888, true);
				
				// Recuperation de l'icone drapeau associé à la langue de la ressource
				codelangueRessource = ressourceCourante.getIdLangue();
				codelangueRessource = Utiles.codeLangueAvecId(codelangueRessource, getActivity());
				iconeLangueRessource = BitmapFactory.decodeResource(
					getResources(), 
					getResources().getIdentifier("icone_drapeau_rond_" + codelangueRessource, "drawable", getActivity().getPackageName())
				);
				
				canvas = new Canvas( iconeResource );
		        canvas.drawBitmap(iconeLangueRessource, 
		        	iconeResource.getWidth() - iconeLangueRessource.getWidth(),
		        	iconeResource.getHeight() - iconeLangueRessource.getHeight(), 
		        	null
		        );
		        
	            return iconeResource;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	    
		@Override
	    protected void onPostExecute(Bitmap icone) {
	        ((ImageView) ressourceView.findViewById(R.id.logo_ressource)).setImageDrawable( new BitmapDrawable(getResources(), icone) );
		}
	}
}
