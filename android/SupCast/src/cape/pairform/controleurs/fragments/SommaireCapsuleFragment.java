package cape.pairform.controleurs.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import cape.pairform.R;
import cape.pairform.controleurs.adapters.ListeChapitresAdapter;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.ChapitreCapsule;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.ListViewExtensible;
import cape.pairform.modele.PageCapsule;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.CapsuleDAO;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.ReferencesBDD;
import cape.pairform.utiles.ParserXmlCapsule;
import cape.pairform.utiles.Utiles;


/*
 * écran affichant le sommaire d'une capsule (liste des chapitres et pages de la capsule)
 */
public class SommaireCapsuleFragment extends Fragment implements ReferencesBDD {
	
	private Menu actionBarSommaire;								// actionBar de l'ecran de sommaire d'une capsule
	private Capsule capsuleCourante;							// capsule actuellement consulté par l'utilisateur
	private CapsuleDAO capsuleDAO;
	private Cursor curseur;
	private ScrollView arborescenceScrollView;					// view scrollable, layout racine de l'ecran
	private LinearLayout arborescenceLayout;					// layout contenant tous les éléments de l'écran (icone de la capsule, listes des chapitres, etc.)
	private LinearLayout listesChapitresLayout;					// layout contenant l'arborescence des chapitres
	private ListViewExtensible listeCourante;					// listeview courante contenant le nom des chapitres et des pages
	private ListeChapitresAdapter adapterListeChapitres;		// adapter de la listview courante
	private ChapitreCapsule chapitreSelectionneCourant;			// chapitre séléectionné par l'utilisateur, le chapitre racine est le premier chapitreSelectionneCourant
	private PageCapsule pageFilsCourante;						// page courante
	private ChapitreCapsule chapitreFilsCourant;				// chapitre courant
	private ArrayList<Object> listePagesEtChapitresFils;		// liste des chapitres et des pages
	private ArrayList<ChapitreCapsule> listeChapitresSelectionnes = new ArrayList<ChapitreCapsule>(); //liste des chapitres selectionnés dans l'arborescence des chapites
	private int nbMessagesLus;									// nombre de messages lus dans le chapitre (ou la page) courant
	private int nbMessagesNonLus;								// nombre de messages non-lus dans le chapitre (ou la page) courant
	private HashMap<String,String> listeNbMessagesNonLus = new HashMap<String,String>();	// liste des messages non-lus de chaque page d'une capsule
	private HashMap<String,String> listeNbMessagesLus = new HashMap<String,String>();		// liste des messages lus de chaque page d'une capsule
	private SommaireCapsuleFragmentListener sommaireCapsuleFragmentListener;				// interface permettant de communiquer avec l'activity parent
	
	
	@Override
	public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // création de l'interface permettant de communiquer avec l'activity parent
        sommaireCapsuleFragmentListener = (SommaireCapsuleFragmentListener) getActivity();

	    // on récupère les informations sur la capsule
        capsuleDAO = BDDFactory.getCapsuleDAO(getActivity().getApplicationContext());
    	curseur = capsuleDAO.getCapsuleById( getArguments().getString(Constantes.CLEF_ID_CAPSULE) );
    	
    	if ( curseur.moveToFirst() ) {
	    	capsuleCourante = new Capsule(
				curseur.getString(NUM_COLONNE_ID),
				curseur.getString(NUM_COLONNE_RESSOURCE_CAPSULE),
				curseur.getString(NUM_COLONNE_ID_VISIBILITE),
				curseur.getString(NUM_COLONNE_NOM_COURT_CAPSULE),
				curseur.getString(NUM_COLONNE_NOM_LONG_CAPSULE),
				curseur.getString(NUM_COLONNE_URL_MOBILE),
				curseur.getString(NUM_COLONNE_DESCRIPTION_CAPSULE),
				curseur.getString(NUM_COLONNE_LICENCE),
				curseur.getInt(NUM_COLONNE_TAILLE),
				curseur.getString(NUM_COLONNE_AUTEURS),
				curseur.getLong(NUM_COLONNE_DATE_CREATION_CAPSULE),
				curseur.getLong(NUM_COLONNE_DATE_EDITION_CAPSULE),
				Boolean.parseBoolean( curseur.getString(NUM_COLONNE_EST_TELECHARGEE_CAPSULE) ),
				curseur.getString(NUM_COLONNE_SELECTEURS_CAPSULE)
			);
    	}
    	curseur.close();
		capsuleDAO.close();
        
        // Mise à jour du titre de l'action bar avec le nom de la capsule courante
        getActivity().getActionBar().setTitle( capsuleCourante.getNomCourt() );

		// on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
        Utiles.determinerAideContextuelle(getActivity(), null, Constantes.AIDE_INEXISTANTE);
        
        // active la possibilité (pour le fragment) de manipuler le menu de l'activity parent
        setHasOptionsMenu(true);
	}
	
	
	@Override
	public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
	    arborescenceScrollView = (ScrollView) inflater.inflate( R.layout.layout_ecran_sommaire_capsule, container, false );
		arborescenceLayout = (LinearLayout) arborescenceScrollView.getChildAt(0);
		listesChapitresLayout = (LinearLayout) arborescenceLayout.findViewById(R.id.layout_listes_chapitres);
		
		// la liste initiale correspond aux chapitres et pages fils du chapitre racine
		listeCourante = (ListViewExtensible) listesChapitresLayout.findViewById(R.id.liste_parent);
		listeCourante.setExtensible(true);
		listeCourante.setTag( Integer.valueOf(0) );		// initialisation à 0 de la profondeur (chapitre racine)
				
	    // création d'un nouveau Thread
	    Thread thread = new Thread(new Runnable() {
	    	public void run() {
	    		// parsing du fichier XML contenant l'arborescence des chapitres et des pages de la capsule
	    		File file = new File(getActivity().getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() +"/"+ capsuleCourante.getId() + "/co/outline.xml");
	    			    		
	    		ParserXmlCapsule parserXml = new ParserXmlCapsule();
	    		try {
	    			// à la fin du parsing, on récupère le chapitre racine
	    			chapitreSelectionneCourant = parserXml.parse(file);
	    		} catch (Exception e) {
	            	e.printStackTrace();
	    		}
	    		
	    		// récupération du nombre de messages non lu de chaque page de la capsule
	    		MessageDAO messageDAO = BDDFactory.getMessageDAO(getActivity().getApplicationContext());
	    		
	    		listeNbMessagesNonLus = messageDAO.getNbMessagesByCapsule( capsuleCourante.getId(), false );	  		// on récupère le nombre de messages non-lus sur la capsule et sur chacune de ses pages  
	    		listeNbMessagesLus = messageDAO.getNbMessagesByCapsule( capsuleCourante.getId(), true );	  			// on récupère le nombre de messages lus sur la capsule et sur chacune de ses pages  

	    		messageDAO.close();
	    		
	    		// execution dans le UI thread
	        	getActivity().runOnUiThread(new Runnable() {
	    			public void run() {	    				
	    				// nom de l'auteur de la capsule
	    				((TextView) arborescenceLayout.findViewById(R.id.nom_long)).setText( capsuleCourante.getNomLong() );
    			        
	    				// s'il y a des messages lus ou non-lus sur le sommaire de la capsule
	    				if (listeNbMessagesNonLus.get("") != null || listeNbMessagesLus.get("") != null) {
	    			        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG); 
	    			        paint.setTextAlign(Align.CENTER);
	    			        paint.setColor(Color.WHITE); 
	    			        paint.setTextSize(22);
	    			        
	    			        // s'il y a des messages non-lus sur le sommaire de la capsule
		    				if (listeNbMessagesNonLus.get("") != null) {	    					
		    					// Modification de l'icone Lire messages de l'actionbar
		    					// on duplique l'image pour pouvoir rajouté du texte
		    					Bitmap icone = BitmapFactory.decodeResource(getResources(), R.drawable.icone_messages_non_lus).copy(Bitmap.Config.ARGB_8888, true);
		    						    			        
		    			        // mise à jour de l'icone avec une icone de couleur contenant le nombre de messages non-lus
		    			        Canvas canvas = new Canvas(icone);
		    			        canvas.drawText(listeNbMessagesNonLus.get(""), icone.getWidth()*1/2, icone.getHeight()*3/5, paint);	// ajout du nombre de messages non lus
		    						    			        
		    			        if(actionBarSommaire != null)
		    			        	actionBarSommaire.findItem(R.id.button_lire_messages).setIcon( new BitmapDrawable(getResources(), icone) );
		    					
		    				} else if (listeNbMessagesLus.get("") != null) {		// sinon, s'il y a des messages lus sur le sommaire de la capsule
		    					// Modification de l'icone Lire messages de l'actionbar
		    					// on duplique l'image pour pouvoir rajouté du texte
		    					Bitmap icone = BitmapFactory.decodeResource(getResources(), R.drawable.icone_messages_lus).copy(Bitmap.Config.ARGB_8888, true);
		    					
		    					// mise à jour de l'icone avec une icone de couleur contenant le nombre de messages lus
		    			        Canvas canvas = new Canvas(icone);
		    			        canvas.drawText(listeNbMessagesLus.get(""), icone.getWidth()*1/2, icone.getHeight()*3/5, paint);	// ajout du nombre de messages lus
		    			        	    					
		    					if(actionBarSommaire != null)
		    						actionBarSommaire.findItem(R.id.button_lire_messages).setIcon( new BitmapDrawable(getResources(), icone) );
		    				}
	    				}
	    				
	    				configurerListeChapitres();		// configuration de la listview courante (création d'un adapter, d'un listener sur un clic, etc.)
	    	        }
	    	    });
	    	}
	    });
	    
	    thread.start();

		return arborescenceScrollView;
	}
	
	
	// configuration de la listview courante (création d'un adapter, d'un listener sur un clic, etc.)
	private void configurerListeChapitres() {
		// création de l'adapter de la listview courante
		adapterListeChapitres = new ListeChapitresAdapter( getActivity() );
		
		// ajout du chapitre sélectionné à la liste des chapitre séléctionné, puis récupération des chapitres et des pages contenus dans celui-ci
		listeChapitresSelectionnes.add(chapitreSelectionneCourant);
		listePagesEtChapitresFils = chapitreSelectionneCourant.getListePagesEtChapitresFils();
		
		try {
			// pour chaque chapitres et pages contenu dans le chapitre sélectionné
			for (Object filsCourant : listePagesEtChapitresFils) {
				
				// si le fils courant du chapitre sélectionné est un chapitre 
				if( filsCourant.getClass() == ChapitreCapsule.class ) {
					// ajout du titre du chapitre dans l'adapter
					chapitreFilsCourant = (ChapitreCapsule) filsCourant;
					
					nbMessagesNonLus = CompterNbMessages( chapitreFilsCourant, 0, listeNbMessagesNonLus );
					nbMessagesLus = CompterNbMessages( chapitreFilsCourant, 0, listeNbMessagesLus );
	    			
					adapterListeChapitres.add(
						chapitreFilsCourant.getTitreChapitre(),
						true,
						String.valueOf( nbMessagesNonLus ),
						String.valueOf( nbMessagesLus )
					);
				} else {	// si le fils courant du chapitre sélectionné est une page
					// ajout du titre de la page dans l'adapter
					pageFilsCourante = (PageCapsule) filsCourant;
					adapterListeChapitres.add(
						pageFilsCourante.getTitrePage(),
						false,
						listeNbMessagesNonLus.get( pageFilsCourante.getUrlRelativePage() ),
						listeNbMessagesLus.get( pageFilsCourante.getUrlRelativePage() )
					);
				}
			}
		} catch (Exception e) {
        	e.printStackTrace();
		}
        		
		// création d'un listener se déclenchant lors d'un clic sur un item de la listview
		listeCourante.setOnItemClickListener( new OnItemClickListener () {
			@Override
			public void onItemClick(AdapterView<?> parent, View chapitreOuPage, int position, long id) {				
				// pour chaque item de la listview
				for (int i=0; i < parent.getChildCount(); i++) {
					// suppression du background de l'item (suppression de la couleur d'arrière plan de l'item potentiellement précédement séléctionné)
					parent.getChildAt(i).setBackgroundResource( 0 );
				}
				
				chapitreOuPage.setBackgroundResource(R.color.couleur_principale_clair);		// le chapitre sélectionné est coloré différemment des autres items de la listview
				
				// déclenche l'ouverture de la page sélectionné ou du contenu du chapitre sélectionné (une liste de pages et de chapitres)
				afficherPagesEtChapitresFils(position, ((Integer) parent.getTag()).intValue() );
			}        	
        });
		
		listeCourante.setAdapter(adapterListeChapitres);
	}
	
	
	// compte récursivement le nombre de messages non-lus/lus dans un chapitre
	private int CompterNbMessages(ChapitreCapsule chapitreCourant, int nbMessages, HashMap<String,String> listeNbMessages) {
		for (Object filsCourant : chapitreCourant.getListePagesEtChapitresFils()) {			
			// si le fils courant du chapitre sélectionné est un chapitre 
			if( filsCourant.getClass() == ChapitreCapsule.class ) {
				// ajout du titre du chapitre dans l'adapter
				nbMessages = CompterNbMessages((ChapitreCapsule) filsCourant, nbMessages, listeNbMessages);
								
			} else {	// si le fils courant du chapitre sélectionné est une page
				// ajout du titre de la page dans l'adapter
				if ( listeNbMessages.containsKey( ((PageCapsule) filsCourant).getUrlRelativePage() ) ) {
					nbMessages += Integer.parseInt(
							listeNbMessages.get( ((PageCapsule) filsCourant).getUrlRelativePage() )
					);
				}
			}
		}
		
		return nbMessages;
	}
	
	// déclenche l'ouverture de la page sélectionné ou du contenu du chapitre sélectionné (une liste de pages et de chapitres)
	private void afficherPagesEtChapitresFils(int position, int profondeur) {
		// récupération de la liste des chapitres et pages contenant le chapitre ou la page sélectionné
		listePagesEtChapitresFils = listeChapitresSelectionnes.get(profondeur).getListePagesEtChapitresFils();
		
		// si c'est une page qui a été sélectionné
		if ( listePagesEtChapitresFils.get(position).getClass() == PageCapsule.class ) {
			// on communique à l'activity parent le nom de la page sélectionné pour qu'elle déclanche son ouverture 
			pageFilsCourante = (PageCapsule) listePagesEtChapitresFils.get(position);
			sommaireCapsuleFragmentListener.onSelectedPage(pageFilsCourante.getUrlRelativePage(), listeChapitresSelectionnes.get(0));
		
		} else {	// si c'est un chapitre qui a été sélectionné
			profondeur++;
			
			// si l'utilisateur a sélectionné un chapitre qui n'est pas dans la plus profonde listview
			if ( profondeur < listeChapitresSelectionnes.size() ) {
				// on supprime toutes les listview ayant une plus grande profondeur que la liste dont fait partie le chapitre sélectionné
				listesChapitresLayout.removeViews(0, listeChapitresSelectionnes.size() - profondeur);
				// on supprime les chapitres séléctionnés qui ne le sont plus
				for (int i=listeChapitresSelectionnes.size() - 1; i >= profondeur ; i--) {
					listeChapitresSelectionnes.remove(i);
				}
			}
			
			// création d'une nouvelle listecourante, la profondeur de la liste est précisé dans son tag
			listeCourante = (ListViewExtensible) getActivity().getLayoutInflater().inflate( R.layout.liste_chapitres_capsule, listesChapitresLayout, false );
			listeCourante.setExtensible(true);
			listeCourante.setTag( Integer.valueOf(profondeur) );
			
			// configuration de la listview courante (création d'un adapter, d'un listener sur un clic, etc.)
			chapitreSelectionneCourant = (ChapitreCapsule) listePagesEtChapitresFils.get(position);
			configurerListeChapitres();
					    
			// ajout de la listview courante (et d'un séparateur de listes) dans l'arborescence
			listesChapitresLayout.addView(listeCourante, 0);
			// on repositionne l'écran (la dernière liste sera le premier élément à l'écran)
			arborescenceScrollView.scrollTo(0, listesChapitresLayout.getTop());
		}
	}

	
	@Override
	public void onPrepareOptionsMenu (Menu menu) {		
		// mise à jour du menu (partagé par plusieurs fragments)
		menu.findItem(R.id.button_accueil).setVisible(true);
		menu.findItem(R.id.button_sommaire_capsule).setVisible(false);
		menu.findItem(R.id.button_recuperer_nouveaux_messages).setVisible(false);
		menu.findItem(R.id.button_lire_messages).setVisible(true);
		menu.findItem(R.id.button_lire_messages).setTitle(R.string.button_lire_messages_sur_sommaire);
		menu.findItem(R.id.button_ecrire_message).setVisible(false);
		
		actionBarSommaire = menu;
	}
	
	
	// gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) { 		
 		switch (item.getItemId()) {
 			case R.id.button_lire_messages:
 				sommaireCapsuleFragmentListener.onClickedLireMessageButton();
 				return true;
 		}
		return false;
 	}
	
	
	// gestion du changement d'orientation
	@Override
  	public void onConfigurationChanged(Configuration newConfig) {
 		super.onConfigurationChanged(newConfig);
 	}

  	
  	// interface permettant de communiquer avec l'activity parent
  	public interface SommaireCapsuleFragmentListener {
  		// la méthode se déclanche quand l'utilisateur sélectionne une page dans l'arborescence des chapitres de la capsule
  		public void onSelectedPage(String pageUri, ChapitreCapsule chapitreRacine);
  		// la méthode se déclanche quand l'utilisateur clic sur le bouton permettant de lire un message
  		public void onClickedLireMessageButton();
  	}
}
