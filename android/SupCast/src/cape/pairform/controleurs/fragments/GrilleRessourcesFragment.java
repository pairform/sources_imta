package cape.pairform.controleurs.fragments;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import cape.pairform.R;
import cape.pairform.controleurs.activities.ActivityMere;
import cape.pairform.controleurs.activities.ApplicationController;
import cape.pairform.controleurs.activities.EcranAjoutRessourceActivity;
import cape.pairform.controleurs.adapters.ListWithCompoundDrawableAdapter;
import cape.pairform.controleurs.services.RecuperateurMessagesService;
import cape.pairform.controleurs.services.ServiceGeneral;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.GridViewExtensible;
import cape.pairform.modele.Ressource;
import cape.pairform.modele.bdd.BDDFactory;
import cape.pairform.modele.bdd.CapsuleDAO;
import cape.pairform.modele.bdd.MessageDAO;
import cape.pairform.modele.bdd.ReferencesBDD;
import cape.pairform.modele.bdd.RessourceDAO;
import cape.pairform.utiles.ParserJSON;
import cape.pairform.utiles.Utiles;


/*
 * écran affichant la liste des ressources téléchargées par l'utiliateur
 */
public class GrilleRessourcesFragment extends Fragment implements ReferencesBDD {

	private static final String LOG_TAG = Utiles.class.getSimpleName();
	
	private Intent intentCourante;												// Intent permettant de transmettre des paramètres à une nouvelle activity ou un nouveau service
	private SharedPreferences fichierSharedPreferences;							// permet l'accès aux fichiers des préférences
	private SharedPreferences.Editor editorFichierSharedPreferences;			// permet d'éditer les fichiers des préférences
	private CapsuleDAO capsuleDAO;
	private ProgressDialog progressDialog;										// fenêtre de chargement
	private ListWithCompoundDrawableAdapter adapter;							// adapter personnalisé utilisé avec la GridView "grilleRessources"
	private ArrayList<String> listeIdRessourcesMisesAJour = new ArrayList<String>();					// liste des id des ressources ayant une mise à jour de disponible
	private ArrayList<String> listeNomRessourcesMisesAJour = new ArrayList<String>();					// liste des noms des ressources ayant une mise à jour de disponible
	private ArrayList<BitmapDrawable> listeIconesRessourcesMisesAJour = new ArrayList<BitmapDrawable>();// liste des icônes des ressources ayant une mise à jour de disponible
	private ArrayList<String> listeNomRessources;								// liste des noms des ressources
	private ArrayList<BitmapDrawable> listeIconesRessources;					// liste des icônes des ressources
	private HashMap<String,String> listeNbMessagesNonLus = new HashMap<String,String>();				// liste du total des messages non-lus de chaque ressource
	private HashMap<String,String> listeNbMessagesLus = new HashMap<String,String>();					// liste du total des messages non-lus de chaque ressource
	private GridViewExtensible grilleRessources;								// view représentant les ressources téléchargées sous forme de grille
	private AlertDialog.Builder dialogSupprimerRessource;						// fenêtre de dialog demandant validation avant de supprimer une ressource
	private String idRessourceCourante;											// id d'une ressource
	private int indexRessourceCourante;											// index d'une ressource dans une liste
	private int indexCapsuleCourante = 0;										// index d'une capsule dans une liste
	private Ressource ressourceInitiale = null;									// ressource initiale installée au lancement de l'application
	private Capsule capsuleCourante = null;
	private HashMap<String,Long> listeTimestampsCapsulesInitiales = new HashMap<String,Long>();	// liste clef(id_capsule) / valeur(le dernier timestamp parmis ceux des messages de la capsule)
	private ArrayList<Ressource> listeRessources = new ArrayList<Ressource>();	// liste des ressources téléchargées
	private DownloadManager downloadManager = null;								// gestionnaire de téléchargement
	private DownloadManager.Request request;									// requete définissant ce qui doit être téléchargé et le contenu de la notification de téléchargement
	private BroadcastReceiver downloadBroadcastReceiver;						// broadcastreceiver se déclenchant à la fin du téléchargement
	private long downloadReference;												// identifiant du téléchargement
	private String stringJSON;													// contenu d'une réponse JSON d'une requête http
	private Canvas canvas;
	private Paint paint;
    private Bitmap iconeResource;												// icone d'une ressource
    private Bitmap iconeResourceFinale;											// icone d'une ressource après modification (ajout du nb de messages non-lus et du drapeau de langue)
    private Bitmap iconeNbMessageNonLus;										// icone d'indication du nb de messages nons-lus
    private Bitmap iconeNbMessageLus;											// icone d'indication du nb de messages lus
    private Bitmap iconeLangueRessource;										// icone d'indication de la langue de la ressource
    private String codelangueRessource;
    private GrilleRessourcesFragmentListener grilleRessourcesFragmentListener;	// interface permettant de communiquer avec l'activity parent
    private static ServiceGeneral service;
	
    
    @Override
	public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = ((ActivityMere)getActivity()).service;
        
        // on bloque l'orientation de l'écran dans son orientation actuelle
    	if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)  {
    		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    	}
    	else {
    		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
    	}
    	
    	// tracker piwik
    	((ApplicationController) getActivity().getApplication()).getTracker()
    		.trackScreenView("/"+ this.getClass().getName(), this.getClass().getSimpleName())
    		.trackEvent("/"+ this.getClass().getName(), this.getClass().getSimpleName());

		// création de l'interface permettant de communiquer avec l'activity parent
        grilleRessourcesFragmentListener = (GrilleRessourcesFragmentListener) getActivity();
        
        // on détermine l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran, l'aide se lance)
     	Utiles.determinerAideContextuelle(getActivity(), Constantes.CLEF_ECRAN_ACCUEIL, Constantes.AIDE_ECRAN_ACCUEIL);
        
        // on récupère le fichier de préfèrences stockant les paramètres de l'application
        fichierSharedPreferences = getActivity().getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, 0);
        
        // active la possibilité (pour le fragment) de manipuler le menu de l'activity parent
        setHasOptionsMenu(true);
	}
	
    
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		grilleRessources = (GridViewExtensible) inflater.inflate( R.layout.grille_ressources, container, false );
		
		// si c'est le 1er démarrage de l'application après l'installation
        if (fichierSharedPreferences.getString(Constantes.CLEF_VERSION, null) == null) {
        	// test la disponibilité de l'accès Internet
            if ( !Utiles.reseauDisponible(getActivity()) ) {
            	Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_initialisation_impossible, getActivity());
            } else { // On peut utiliser la connection Internet
            	// création d'une fenêtre de chargement
	        	progressDialog = new ProgressDialog(getActivity());
	    	    progressDialog.setMessage( getString(R.string.label_initialisation_application) );
	    	    progressDialog.setCanceledOnTouchOutside(false);
	    	    progressDialog.show();
            	
	        	// on stock la version de l'application dans le fichier de paramètres 
	        	editorFichierSharedPreferences = fichierSharedPreferences.edit();
	        	editorFichierSharedPreferences.putString(Constantes.CLEF_VERSION, Constantes.VERSION_NAME);
				editorFichierSharedPreferences.commit();
				
				// on stock les informations sur les langues
				String langueApplication = getResources().getString(R.string.locale_language);
				editorFichierSharedPreferences.putString(Constantes.CLEF_LANGUE_APPLICATION, langueApplication);
				editorFichierSharedPreferences.commit();
				
				// tracker piwik
		    	((ApplicationController) getActivity().getApplication()).getTracker().trackAppDownload();
				
				// on récupère les infos sur la ressource initiale (notament l'url où télécharger les capsules qu'elle contient)
	        	recupererInfosRessourceInitiale();
            }
        } else {
        	// création d'une fenêtre de chargement
        	progressDialog = new ProgressDialog(getActivity());
    	    progressDialog.setMessage( getString(R.string.label_chargement) );
    	    progressDialog.show();
    	            	
			// création d'un nouveau Thread
		    Thread thread = new Thread(new Runnable() {
		    	public void run() {
		        	// télécharge les nouveaux messages des capsules téléchargées et les enregistrent en BDD
		    		telechargerNouveauxMessages(null);
		    		
					RessourceDAO ressourceDAO = BDDFactory.getRessourceDAO(getActivity().getApplicationContext());
		    		listeRessources = ressourceDAO.getAllRessources();
		        	ressourceDAO.close();
		        	
					// on récupère le nombre total de messages non-lus des ressources téléchargées
					recupererNbMessagesNonLus();
		        	
		        	getActivity().runOnUiThread(new Runnable() {
		    			public void run() {
		    				// on affiche les ressources téléchargées dans une grille
		    				afficherRessources();
		    				progressDialog.dismiss();

		    		        // on désactive l'orientation forcé de l'écran
		    				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		    	        }
		    	    });
		    	}
		    });
		    
		    thread.start();
        }
        
		return grilleRessources;
	}
    
    
    // récupère les infos sur la ressource initiale (notament l'url où télécharger les capsules qu'elle contient)
    private void recupererInfosRessourceInitiale() {
    	// création d'un nouveau Thread
	    Thread thread = new Thread(new Runnable() {
	    	public void run() {
	    		// création et configuration des paramètres d'une requête http
	    		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();
	    		
	            try {
		        	stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECUPERER_INFOS_PJS, parametresRequete, getActivity());
		        	// si la requete http s'est mal exécutée, stringJSON vaut null, donc on ne parse pas ressourcesJSON
		        	if ( stringJSON != null ) {
						ParserJSON.parserInformationsPieceJointe(stringJSON, getActivity()).save();
		        	}
	    		
		        	stringJSON = "";
	            	parametresRequete.clear();
	            	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_LANGUES_AFFICHAGE, Utiles.getListeIdLangues(getActivity())));		// langues de l'affichage des ressources des préférences
	            	parametresRequete.add( new BasicNameValuePair(Constantes.CLEF_ID_RESSOURCE, Constantes.ID_RESSOURCE_INITIALE));					// l'id de la ressource est le premier élément du tableau (cf. fichier Constantes.java)
		            
		            // Envoie d'une requete HTTP récupérant les infos sur les ressources pouvant être téléchargé
		        	stringJSON = Utiles.envoyerRequeteHttp( Constantes.URL_WEB_SERVICE_RECUPERER_INFOS_RESSOURCE, parametresRequete, getActivity());
		        	
		        	// si la requete http s'est mal exécutée, stringJSON vaut null, donc on ne parse pas ressourcesJSON
		        	if ( stringJSON != null ) {
			            // transformation de la réponse en objet JSON
		        		JSONObject retourJSON = new JSONObject(stringJSON);
						
			           	ressourceInitiale = ParserJSON.parserRessourceInitiale(retourJSON, Constantes.ID_CAPSULES_INITIALES);
			        	listeRessources.add(ressourceInitiale);
		        	}
					
		        	// création du répertoire associé à la ressource, il contient les fichiers de la ressource (logo, capsules, etc.)
		        	File repertoireRessource = new File( getActivity().getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + ressourceInitiale.getId() );
		        	repertoireRessource.mkdirs();
		        	
		    		// on télécharge l'icone de la ressource
		    		Utiles.telechargerFichier (
		    			Constantes.SERVEUR_NODE_URL + ressourceInitiale.getUrlLogo(),
		    			getActivity().getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + ressourceInitiale.getId() + "/" + Constantes.CLEF_LOGO
		    		);
		    		
		    		// mise à jour de l'emplacement du logo pour pointer vers le logo venant d'être télécharger en local
		    		ressourceInitiale.setUrlLogo( getActivity().getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + ressourceInitiale.getId() + "/" + Constantes.CLEF_LOGO );
		        	
		        	// ajout de la ressource en BDD
					RessourceDAO ressourceDAO = BDDFactory.getRessourceDAO(getActivity().getApplicationContext());
		        	ressourceDAO.insertRessource(ressourceInitiale);
		        	ressourceDAO.close();
		        	
	            } catch (Exception e) {
	            	e.printStackTrace();        	
	            }
	            
	        	getActivity().runOnUiThread(new Runnable() {
	    			public void run() {
	    				// si la requete http s'est mal exécuté, stringJSON vaut null
	    				if ( stringJSON != null ) {
		    				// lancement du téléchargement des capsules de la ressource initiale : certaines ressources doivent être présentes dès l'installation
	    					telechargerCapsulesInitiales();
		    			} else {
							progressDialog.dismiss();
					        // on désactive l'orientation forcé de l'écran
							getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	
					        Utiles.afficherInfoAlertDialog(R.string.title_serveur_indisponible, R.string.label_erreur_serveur_indisponible, getActivity());
						}
	    	        }
	    	    });
	    	}
	    });
	    
	    thread.start();
    }
    
    
    // télécharge les capsules de la ressource initiale (certaines ressources et capsules doivent être présentes dès l'installation)
    private void telechargerCapsulesInitiales() {
		capsuleDAO = BDDFactory.getCapsuleDAO(getActivity().getApplicationContext());
		
		capsuleCourante = ressourceInitiale.getListeCapsules().get(indexCapsuleCourante);		
		
		// si la capsule doit être téléchargée
		if (capsuleCourante.isTelechargee()) {
			Log.i(LOG_TAG, "fichier à télécharger : " + capsuleCourante.getUrlMobile() + ".zip");
	
	    	if (downloadManager == null) {
	    		downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
	    	}
	    	if (downloadBroadcastReceiver == null) {
				downloadBroadcastReceiver = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						// si l'identifiant correspond à celui du téléchargement courant
						if(downloadReference == intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)) {					
							int status = Utiles.verifierStatusDownloadManager(downloadManager, downloadReference);
							
							// si le téléchargement a échouer
							if (status == DownloadManager.STATUS_FAILED) {
								Utiles.afficherInfoAlertDialog(R.string.title_erreur_telechargement, R.string.label_erreur_telechargement_nouvelle_capsule, getActivity());
								downloadManager.remove(downloadReference);
							} else if (status == DownloadManager.STATUS_SUCCESSFUL) {		// si le téléchargement a réussi
								try {
									// on dezipe la capsule téléchargée dans le repertoire dédié
									Utiles.dezipperCapsule (
										(FileInputStream) new ParcelFileDescriptor.AutoCloseInputStream ( downloadManager.openDownloadedFile(downloadReference) ),
										getActivity().getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() +"/"+ capsuleCourante.getId(),
										capsuleCourante.getUrlMobile()
									);
									downloadManager.remove(downloadReference);
									
									listeTimestampsCapsulesInitiales.put(capsuleCourante.getId(), capsuleCourante.getDateCreation());
									ajouterCapsuleInitiale();
									
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				};
				getActivity().registerReceiver( downloadBroadcastReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE) );
		    }
			
	    	// création et définition de la requete, affichage d'une notification pendant le téléchargement
	    	request = new DownloadManager.Request( Uri.parse( capsuleCourante.getUrlMobile() + ".zip" ) );
	    	request.setTitle( getString(R.string.app_name) );
			request.setDescription( getString(R.string.label_telechargement_nouvelle_capsule) + " " + capsuleCourante.getNomCourt() + "." );
			request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, capsuleCourante.getNomCourt() + ".zip");
			request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
			
			// ajout de la requete à la pile des téléchargement du downloadmanager
			downloadReference = downloadManager.enqueue(request);
		} else {
			ajouterCapsuleInitiale();
		}
    }
    
    
    // ajoute une capsule de la ressource initiale en BDD
    private void ajouterCapsuleInitiale() {
		capsuleDAO.insertCapsule(capsuleCourante);
		capsuleDAO.close();
		
		indexCapsuleCourante++;
		
		// si toutes les capsules initiales ont été téléchargées
		if (indexCapsuleCourante >= ressourceInitiale.getListeCapsules().size()) {									
			// télécharge les messages des nouvelles capsules et les enregistrent en BDD
	    	telechargerNouveauxMessages(listeTimestampsCapsulesInitiales);
			// on récupère le nombre total de messages non-lus de la ressource
			recupererNbMessagesNonLus();
			// on affiche la ressource téléchargée dans une grille
			afficherRessources();
	        // on désactive l'orientation forcé de l'écran
			getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			progressDialog.dismiss();
			getActivity().unregisterReceiver( downloadBroadcastReceiver );
		} else {									
			// on lance le téléchargement de la capsule suivante
			telechargerCapsulesInitiales();
		}
    }
    
    
    // récupère le nombre total de messages non-lus de chaque ressource
    private void recupererNbMessagesNonLus() {
    	// récupération du nombre des messages non-lus des ressources
		MessageDAO messageDAO = BDDFactory.getMessageDAO(getActivity().getApplicationContext());
		Cursor cursor = messageDAO.getNbMessagesByRessources(false);
		
		// on passe les informations du cursor dans une hashmap
		while (cursor.moveToNext()) {
			listeNbMessagesNonLus.put(cursor.getString( cursor.getColumnIndex(COLONNE_ID_RESSOURCE_CAPSULE) ), cursor.getString( cursor.getColumnIndex(COLONNE_LU) ));
		}
		
		// récupération du nombre des messages lus des ressources
		cursor = messageDAO.getNbMessagesByRessources(true);
		
		// on passe les informations du cursor dans une hashmap
		while (cursor.moveToNext()) {
			listeNbMessagesLus.put(cursor.getString( cursor.getColumnIndex(COLONNE_ID_RESSOURCE_CAPSULE) ), cursor.getString( cursor.getColumnIndex(COLONNE_LU) ));
		}
		
		cursor.close();
		messageDAO.close();
    }
    
    
    // affiche les ressources téléchargés dans une grille
    private void afficherRessources() {        
    	paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        paint.setTextAlign(Align.CENTER);
        paint.setColor(Color.WHITE); 
        paint.setTextSize(22);

        listeNomRessources = new ArrayList<String>();					// liste des noms des ressources
    	listeIconesRessources = new ArrayList<BitmapDrawable>();		// liste des icônes des ressources
    	
		for (Ressource ressourceCourante : listeRessources) {
			// on stock le nom et l'URI des icônes de chaque ressource dans des listes
			listeNomRessources.add( ressourceCourante.getNomCourt() );
			
			// on duplique l'icone pour pouvoir la modifier
			iconeResource = BitmapFactory.decodeFile( ressourceCourante.getUrlLogo() ).copy(Bitmap.Config.ARGB_8888, true);
			iconeNbMessageNonLus = BitmapFactory.decodeResource(getResources(), R.drawable.icone_messages_non_lus).copy(Bitmap.Config.ARGB_8888, true);
			iconeNbMessageLus = BitmapFactory.decodeResource(getResources(), R.drawable.icone_messages_lus).copy(Bitmap.Config.ARGB_8888, true);
			iconeResourceFinale = Bitmap.createBitmap(									// création de l'icone finale avec les bonnes dimensions
				iconeResource.getWidth() + iconeNbMessageLus.getWidth(), 
				iconeResource.getHeight() + iconeNbMessageLus.getHeight()/2,
				Bitmap.Config.ARGB_8888
			);
			
			// Recuperation de l'icone drapeau associé à la langue de la ressource
			codelangueRessource = ressourceCourante.getIdLangue();
			codelangueRessource = Utiles.codeLangueAvecId(codelangueRessource, getActivity());
			iconeLangueRessource = BitmapFactory.decodeResource(
				getResources(), 
				getResources().getIdentifier("icone_drapeau_rond_" + codelangueRessource, "drawable", getActivity().getPackageName())
			);

			canvas = new Canvas( iconeResourceFinale );
	        canvas.drawBitmap(iconeResource, iconeNbMessageLus.getWidth()/2, iconeNbMessageLus.getHeight()/2, null);	// ajout de l'icone de la ressource
	        
	        canvas.drawBitmap(
	        	iconeLangueRessource,
	        	iconeResource.getWidth(),
	        	iconeResource.getHeight(),
	        	null
	        );
	        
			// si la ressource contient des messages non-lus
			if( listeNbMessagesNonLus.get( ressourceCourante.getId() ) != null ) {
		        canvas.drawBitmap(				// ajout de l'icone de messages non-lus
		        	iconeNbMessageNonLus, 
		        	iconeResource.getWidth(), 
		        	0, 
		        	null
		        );
		        canvas.drawText(				// ajout du nombre de messages non lus
		        	listeNbMessagesNonLus.get( ressourceCourante.getId() ), 
		        	iconeResource.getWidth() + iconeNbMessageNonLus.getWidth()*1/2, 
		        	iconeNbMessageNonLus.getHeight()*3/5,
		        	paint
		        );
			} else if( listeNbMessagesLus.get( ressourceCourante.getId() ) != null ) {
		        canvas.drawBitmap(				// ajout de l'icone de messages lus
		        	iconeNbMessageLus, 
		        	iconeResource.getWidth(), 
		        	0, 
		        	null
		        );
		        canvas.drawText(				// ajout du nombre de messages non lus
		        	listeNbMessagesLus.get( ressourceCourante.getId() ), 
		        	iconeResource.getWidth() + iconeNbMessageLus.getWidth()*1/2, 
		        	iconeNbMessageLus.getHeight()*3/5,
		        	paint
		        );
			}
			
			// ajout de l'icone modifié à la liste des icones
			listeIconesRessources.add( new BitmapDrawable(getResources(), iconeResourceFinale) );
			
			// si la ressource à une mise à jour de disponible
	        if ( getActivity().getSharedPreferences(Constantes.FICHIER_LISTE_RESSOURCES_MISES_A_JOUR, 0).contains( ressourceCourante.getId() ) ) {
	        	// on stock l'id, le nom et l'URI des icônes de chaque ressource dans des listes
	        	listeIdRessourcesMisesAJour.add( ressourceCourante.getId() );
				listeNomRessourcesMisesAJour.add( ressourceCourante.getNomCourt() );
				listeIconesRessourcesMisesAJour.add( new BitmapDrawable(getResources(), iconeResource) );
	        }
		}
		
		// création et configuration de la view représentant les ressources téléchargées sous forme de grille
        grilleRessources.setOnItemClickListener( new OnItemClickListener () {
			@Override
			public void onItemClick(AdapterView<?> parent, View ressourceView, int position, long id) {
				// si l'utilisateur clique sur un item (=une ressource) de la GridView, on lance l'écran ressources, on transmet l'id de la ressource
				grilleRessourcesFragmentListener.onSelectedRessource( listeRessources.get(position).getId() );
			}
        });
        grilleRessources.setOnItemLongClickListener( new OnItemLongClickListener () {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View ressourceView, int position, long id) {
				// si l'utilisateur fait un clique long sur un item (=une ressource) de la GridView, on supprime la ressource
				// le téléphone vibre pour signaler la prise en compte du clic long
				Vibrator vibrateur = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
				vibrateur.vibrate(Constantes.DUREE_VIBRATION_CLIC_LONG);
				
				indexRessourceCourante = position;
				// ouverture de la fenetre de dialog de validation de la suppression
				ouvrirDialogSupprimerRessource();
				
				return true;
			}        	
        });
        
        // mise à jour de la GridView "grilleRessources" avec son adapter
        mettreAjourAdapter();
    }
    
    
    // télécharge les nouveaux messages et les enregistre en BDD
    private void telechargerNouveauxMessages(HashMap<String,Long> listeTimestampsCapsules) {    	
		if (Utiles.reseauDisponible(getActivity())) {	    	
	    	intentCourante = new Intent(getActivity(), RecuperateurMessagesService.class);
	    	
	    	if (listeTimestampsCapsules != null)
	    		intentCourante.putExtra(Constantes.CLEF_FILTRE_CAPSULES, listeTimestampsCapsules);		// listeTimestampsCapsules représente une liste clef (id de la capsule) / valeur (timestamp de la plus vieille date d'édition parmi les messages de la capsule)
	    	
	    	getActivity().startService(intentCourante);
        }
    }
    
    
    // mettre à jour de la GridView "grilleRessources" avec son adapter
    private void mettreAjourAdapter() {		
    	adapter = new ListWithCompoundDrawableAdapter(
    		getActivity(),
    		listeNomRessources,
    		listeIconesRessources,
    		R.layout.ressource_grille_ressources,
    		ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_HAUT
        );
        grilleRessources.setAdapter(adapter);
    }
    

	// ouvrir la fenetre de dialog de validation de la suppression d'une ressource
    private void ouvrirDialogSupprimerRessource() {
    	dialogSupprimerRessource = new AlertDialog.Builder(getActivity());
        
    	dialogSupprimerRessource.setTitle( R.string.title_supprimer_ressource )
    		.setMessage( R.string.label_supprimer_ressource )
    	    .setPositiveButton(
   	    		R.string.button_supprimer,
    	    	new DialogInterface.OnClickListener() {
	    	    	public void onClick(DialogInterface dialog, int id) {
	    	    		// on supprime la ressource
	    	    		supprimerRessource();	    	    		
	    	    		dialog.cancel();
	    	        }
    	    })
    	    .setNegativeButton(
   	    		R.string.button_annuler,
    	    	new DialogInterface.OnClickListener() {
	    	    	public void onClick(DialogInterface dialog, int id) {
	    	    		dialog.cancel();
	    	        }
    	    })
    	    .show();
    }
    
    
    // supprimer une ressource
    private void supprimerRessource() {
    	idRessourceCourante = listeRessources.get(indexRessourceCourante).getId();
    	listeNomRessources.remove(indexRessourceCourante);
		listeIconesRessources.remove(indexRessourceCourante);
        // mise à jour de la GridView "grilleRessources" avec son adaptater       
		mettreAjourAdapter(); 
    	
    	// création d'un nouveau Thread
	    Thread thread = new Thread(new Runnable() {
	    	public void run() {
	    		// on supprime les répertoires contenant les capsules de la ressource
	    		for(Capsule capsuleCourante : listeRessources.get(indexRessourceCourante).getListeCapsules()) {
	    			File file = new File( getActivity().getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() + "/" + capsuleCourante.getId() );
	    			Utiles.supprimerRepertoire(file);
	    		}
	    		
	    		// on supprime le répertoire associé à la ressource
	    		File file = new File( getActivity().getDir(Constantes.REPERTOIRE_RESSOURCE, Context.MODE_PRIVATE).getPath() + "/" + listeRessources.get(indexRessourceCourante).getId() );
    			Utiles.supprimerRepertoire(file);
    			
	        	listeRessources.remove(indexRessourceCourante);
	        	
	    		// on supprime la ressource de la BDD locale
	    		RessourceDAO ressourceDAO = BDDFactory.getRessourceDAO(getActivity().getApplicationContext());
	        	ressourceDAO.deleteRessource(idRessourceCourante);
	        	ressourceDAO.close();	        	
	    	}
	    });

	    thread.start();		
    }
 	
 	
 	// met à jour une ressource, elle est téléchargé à nouveau, les anciens fichiers prennent la place des nouveaux
    private void mettreAJourRessource(final int indexListeRessource) {/*
    	// création d'une fenêtre de chargement
    	progressDialog = new ProgressDialog(this);
	    progressDialog.setMessage( getString(R.string.label_chargement) );
	    progressDialog.show();
	    
    	fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_INFOS_RESSOURCES_TELECHARGEES, 0);
    	
    	Log.i(LOG_TAG, "fichier à télécharger : " + fichierSharedPreferences.getString(idRessourceCourante + Constantes.CLEF_URL_RESSOURCE, "") + ".zip");
		
    	if (downloadManager == null) {
    		downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
    	}
		downloadBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// si l'identifiant correspond à celui du téléchargement courant
				if ( downloadReference == intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1) ) {					
					int status = Utiles.verifierStatusDownloadManager(downloadManager, downloadReference);
					
					// si le téléchargement a échouer
					if (status == DownloadManager.STATUS_FAILED) {
						Utiles.afficherInfoAlertDialog(R.string.title_erreur_mise_a_jour_ressource, R.string.label_erreur_mise_a_jour_ressource, getActivity());
						downloadManager.remove(downloadReference);
					} else if (status == DownloadManager.STATUS_SUCCESSFUL) {		// si le téléchargement a réussi
						try {
							// on dezipe la ressource téléchargé dans le repertoire dédié
							Utiles.dezipperRessource (
								(FileInputStream) new ParcelFileDescriptor.AutoCloseInputStream ( downloadManager.openDownloadedFile(downloadReference) ),
								getDir(Constantes.REPERTOIRE_CAPSULE, Context.MODE_PRIVATE).getPath() + "/" + idRessourceCourante,
								fichierSharedPreferences.getString(idRessourceCourante + Constantes.CLEF_URL_RESSOURCE, "")
							);
							
		    		        // on désactive l'orientation forcé de l'écran et on ferme la fenêtre de chargement
		    		        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		    				progressDialog.dismiss();
							
		    				// on supprime la ressource du fichier de préférences et des listes stockant les resources à mettre à jour
		    	    		fichierSharedPreferences = getSharedPreferences(Constantes.FICHIER_LISTE_RESSOURCES_MISES_A_JOUR, 0);
		    	    		editorFichierSharedPreferences = fichierSharedPreferences.edit();
		    	    		editorFichierSharedPreferences.remove(idRessourceCourante);
		    	    		editorFichierSharedPreferences.commit();
		    	    		
		 		    		listeNomRessourcesMisesAJour.remove(indexListeRessource);
		 		    		listeIconesRessourcesMisesAJour.remove(indexListeRessource);
		 		    		listeIdRessourcesMisesAJour.remove(indexListeRessource);

	    	    			// on désactive le broadcast receiver
	    	    			getActivity().unregisterReceiver( downloadBroadcastReceiver );
	    	    			
		 		    		// s'il n'y a plus de ressource à mettre à jour
		    	    		if ( fichierSharedPreferences.getAll().isEmpty() ) {
		    	    			// on masque le bouton de mise à jour
		    	    			menuAccueil.findItem(R.id.button_mettre_a_jour_ressources).setVisible(false);
		    	    		}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		};
		registerReceiver( downloadBroadcastReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE) );
				
    	// création et définition de la requete, affichage d'une notification pendant le téléchargement
    	request = new DownloadManager.Request( Uri.parse( fichierSharedPreferences.getString(idRessourceCourante + Constantes.CLEF_URL_RESSOURCE, "") + ".zip" ) );
    	request.setTitle( getString(R.string.app_name) );
		request.setDescription( getString(R.string.label_mise_a_jour_ressource) + " " + fichierSharedPreferences.getString(idRessourceCourante + Constantes.CLEF_NOM_COURT_RESSOURCE, "") + "." );
		request.setDestinationInExternalFilesDir(this, Environment.DIRECTORY_DOWNLOADS, fichierSharedPreferences.getString(idRessourceCourante + Constantes.CLEF_NOM_COURT_RESSOURCE, "") + ".zip");
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
		
		// ajout de la requete à la pile des téléchargement du downloadmanager
		downloadReference = downloadManager.enqueue(request);*/
    }
    
	
	@Override
	public void onPrepareOptionsMenu (Menu menu) {		
		// mise à jour du menu (partagé par plusieurs fragments)
		menu.findItem(R.id.button_ajouter_ressource).setVisible(true);
		
        // s'il n'y a pas de ressources à mettre à jour, on masque le bouton de mise à jour des ressources
        if ( getActivity().getSharedPreferences(Constantes.FICHIER_LISTE_RESSOURCES_MISES_A_JOUR, 0).getAll().isEmpty() ) {
        	menu.findItem(R.id.button_mettre_a_jour_ressources).setVisible(false);
        } else {
        	menu.findItem(R.id.button_mettre_a_jour_ressources).setVisible(true);
        }
	}
    
    
    // gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		switch (item.getItemId()) {
			case R.id.button_ajouter_ressource: 				
				// test la disponibilité de l'accès Internet
		        if (!Utiles.reseauDisponible(getActivity())) {
		        	Utiles.afficherInfoAlertDialog(R.string.title_reseau_indisponible, R.string.label_erreur_reseau_indisponible, getActivity());
		        } else { // On peut utiliser la connection Internet
		        	// lancement de l'écran d'ajout de nouvelles ressources
					startActivity( new Intent(getActivity(), EcranAjoutRessourceActivity.class) );
		        }
		        return true;
			case R.id.button_mettre_a_jour_ressources: 				
				// on ouvre une fenetre contenant la liste des ressources ayant une mise à jour de disponible
				ListWithCompoundDrawableAdapter adapter = new ListWithCompoundDrawableAdapter(
					getActivity(),
		    		listeNomRessourcesMisesAJour,
		    		listeIconesRessourcesMisesAJour,
		    		R.layout.element_liste_ressource,
		    		ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_GAUCHE
		        );
				
				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
				dialogBuilder.setTitle(R.string.title_mettre_a_jour_ressource)
	 				.setAdapter(adapter, new DialogInterface.OnClickListener() {
	 					@Override
	 					public void onClick(DialogInterface dialog, int itemSelectionne) {
	 						idRessourceCourante = listeIdRessourcesMisesAJour.get(itemSelectionne);
	 						mettreAJourRessource( itemSelectionne );
	 					}
	 		     	})
	 		     	.show();
				return true;
 		}
 		return false;
 	}
	
	// gestion du changement d'orientation
	@Override
  	public void onConfigurationChanged(Configuration newConfig) {
 		super.onConfigurationChanged(newConfig);
 	}
	
  	
  	// interface permettant de communiquer avec l'activity parent
  	public interface GrilleRessourcesFragmentListener {
  		// la méthode se déclenche quand l'utilisateur sélectionne une ressource dans la grille des ressources
  		public void onSelectedRessource(String idRessource);
  	}
}
