package cape.pairform.controleurs.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import cape.pairform.R;
import cape.pairform.controleurs.activities.EcranTableauDeBordActivity;
import cape.pairform.controleurs.adapters.ListWithCompoundDrawableAdapter;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.GridViewExtensible;
import cape.pairform.modele.Ressource;


/*
 * tab affichant un onglet (thèmes ou espace) contenant la liste des ressources téléchargeables
 */
public class AjoutRessourceFragment extends Fragment {
	
	// générale
	private ArrayList<Ressource> listeRessources;																// les ressources pouvant être téléchargées par l'utilisateur
	private ArrayList<String> listeNomsRessources = new ArrayList<String>();									// liste des des noms des ressource
	private ArrayList<String> listeUrlIconesRessources = new ArrayList<String>();								// liste des des URL des icones des ressource
	private ArrayList<String> listeIdLanguesRessources = new ArrayList<String>();								// liste des des id des langues des ressource
	// par thème
	private HashMap<String, Integer> mapThemes = new HashMap<String, Integer>();								// map contenant tous les thèmes ; clef = nom d'un thème, valeur = index de la grille de ressources associé au thème dans themesListeRessources
	private ArrayList<String> listeThemes = new ArrayList<String>();											// liste des thèmes
	private ArrayList<ArrayList<Ressource>> themesListeRessources = new ArrayList<ArrayList<Ressource>>();		// liste des listes de ressource (une liste par thème)
	private ArrayList<ArrayList<String>> themesListeNomsRessources = new ArrayList<ArrayList<String>>();		// liste des listes des noms des ressource (une liste par thème)
	private ArrayList<ArrayList<String>> themesListeUrlIconesRessources = new ArrayList<ArrayList<String>>();	// liste des listes des URL des icones des ressource (une liste par thème)
	private ArrayList<ArrayList<String>> themesListeIdLanguesRessources = new ArrayList<ArrayList<String>>();	// liste des listes des id des langues des ressource (une liste par thème)
	// par espace
	private HashMap<String, Integer> mapEspaces = new HashMap<String, Integer>();								// map contenant tous les espaces ; clef = nom d'un espace, valeur = index de la grille de ressources associé à l'espace dans themesListeRessources
	private ArrayList<String> listeEspaces = new ArrayList<String>();											// liste des espaces
	private ArrayList<ArrayList<Ressource>> espacesListeRessources = new ArrayList<ArrayList<Ressource>>();		// liste des listes de ressource (une liste par espace)
	private ArrayList<ArrayList<String>> espacesListeNomsRessources = new ArrayList<ArrayList<String>>();		// liste des listes des noms des ressource (une liste par espace)
	private ArrayList<ArrayList<String>> espacesListeUrlIconesRessources = new ArrayList<ArrayList<String>>();	// liste des listes des URL des icones des ressource (une liste par espace)
	private ArrayList<ArrayList<String>> espacesListeIdLanguesRessources = new ArrayList<ArrayList<String>>();	// liste des listes des id des langues des ressource (une liste par espace)
	
	private GridViewExtensible gridViewRessources;							// view contenant la grille des ressources pouvant être téléchargées
	private ListWithCompoundDrawableAdapter adapter;						// adapter d'une grille de ressources
	private ArrayAdapter<String> adapterFiltre;								// adapter de la liste de filtres (filtrant la grille de ressources)
	private AlertDialog.Builder dialogBuilder;								// fenêtre courante
	private BroadcastReceiver downloadBroadcastReceiver;
	private TabAjoutRessourceFragmentListener tabAjoutRessourceFragmentListener;
	
		
	@Override
	public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        tabAjoutRessourceFragmentListener = (TabAjoutRessourceFragmentListener) getActivity();	// création de l'interface permettant de communiquer avec l'activity parent
        listeRessources = tabAjoutRessourceFragmentListener.getListeRessources();				// récupération de la liste des ressources
		        
        // on trie chaque ressources téléchargeables
        for (Ressource ressourceCourante : listeRessources) {        	
        	// si le theme n'est pas encore dans la liste des thèmes
        	if ( !mapThemes.containsKey( ressourceCourante.getTheme() ) ) {        		
        		// on ajoute le theme à la liste des themes
        		listeThemes.add(ressourceCourante.getTheme());
        		mapThemes.put( ressourceCourante.getTheme() , Integer.valueOf(themesListeRessources.size()) );
        		
        		themesListeRessources.add( new ArrayList<Ressource>() );
        		themesListeNomsRessources.add( new ArrayList<String>() );
        		themesListeUrlIconesRessources.add( new ArrayList<String>() );
        		themesListeIdLanguesRessources.add( new ArrayList<String>() );
        	}        	
        	// si l'espace n'est pas encore dans la liste des espaces
        	if ( !mapEspaces.containsKey( ressourceCourante.getNomEspace() ) ) {
        		// on ajoute le Espace à la liste des Espaces
        		listeEspaces.add(ressourceCourante.getNomEspace());
        		mapEspaces.put( ressourceCourante.getNomEspace() , Integer.valueOf(espacesListeRessources.size()) );
        		
        		espacesListeRessources.add( new ArrayList<Ressource>() );
        		espacesListeNomsRessources.add( new ArrayList<String>() );
        		espacesListeUrlIconesRessources.add( new ArrayList<String>() );
        		espacesListeIdLanguesRessources.add( new ArrayList<String>() );
        	}

        	// ajout des infos sur la ressources dans les listes générales
    		listeNomsRessources.add( ressourceCourante.getNomCourt() );
    		listeUrlIconesRessources.add( Constantes.SERVEUR_NODE_URL + ressourceCourante.getUrlLogo() );
    		listeIdLanguesRessources.add( ressourceCourante.getIdLangue() );
    		
    		// ajout des infos sur la ressources dans les listes pour filtrer par theme
    		themesListeRessources.get( mapThemes.get( ressourceCourante.getTheme() ).intValue() ).add( ressourceCourante );
        	themesListeNomsRessources.get( mapThemes.get( ressourceCourante.getTheme() ).intValue() ).add( ressourceCourante.getNomCourt() );
        	themesListeUrlIconesRessources.get( mapThemes.get( ressourceCourante.getTheme() ).intValue() ).add( Constantes.SERVEUR_NODE_URL + ressourceCourante.getUrlLogo() );
        	themesListeIdLanguesRessources.get( mapThemes.get( ressourceCourante.getTheme() ).intValue() ).add( ressourceCourante.getIdLangue() );
        	
    		// ajout des infos sur la ressources dans les listes pour filtrer par espace
    		espacesListeRessources.get( mapEspaces.get( ressourceCourante.getNomEspace() ).intValue() ).add( ressourceCourante );
        	espacesListeNomsRessources.get( mapEspaces.get( ressourceCourante.getNomEspace() ).intValue() ).add( ressourceCourante.getNomCourt() );
        	espacesListeUrlIconesRessources.get( mapEspaces.get( ressourceCourante.getNomEspace() ).intValue() ).add( Constantes.SERVEUR_NODE_URL + ressourceCourante.getUrlLogo() );
        	espacesListeIdLanguesRessources.get( mapEspaces.get( ressourceCourante.getNomEspace() ).intValue() ).add( ressourceCourante.getIdLangue() );
        }
        // active la possibilité (pour le fragment) de manipuler le menu de l'activity parent
        setHasOptionsMenu(true);
	}
	
	
	@Override
	public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		adapter = new ListWithCompoundDrawableAdapter(
    		getActivity(),
    		listeNomsRessources,
    		listeUrlIconesRessources,
    		R.layout.ressource_grille_ressources,
    		ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_HAUT,
    		listeIdLanguesRessources
        );
		
		// création de la grille de ressources
		gridViewRessources = (GridViewExtensible) inflater.inflate( R.layout.grille_ressources, container, false );
		gridViewRessources.setAdapter(adapter);
		gridViewRessources.setOnItemClickListener( new OnItemClickListener () {
			@Override
			public void onItemClick(AdapterView<?> parent, View ressourceView, int position, long id) {
				// si l'utilisateur clique sur un item (=une ressource) de la GridView, on lance l'écran ressources, on transmet l'id de la ressource
				tabAjoutRessourceFragmentListener.onSelectedRessource(
					listeRessources.get(position).getId()
				);
			}
        });
		
		return gridViewRessources;
	}
	
	
	@Override
	public void onPrepareOptionsMenu (Menu menu) {
		// mise à jour du menu (partagé par plusieurs fragments)
		menu.findItem(R.id.button_filtrer_ressources).setVisible(true);
		menu.findItem(R.id.button_grille_ressources).setVisible(true);
	}
	
	
	// gestion du menu
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) { 		
		switch (item.getItemId()) {
			case R.id.button_grille_ressources:
	        	// lancement de l'écran affichant les ressources téléchargées par l'utilisateur
				startActivity( new Intent(getActivity(), EcranTableauDeBordActivity.class) );
				return true;
	 		case R.id.button_filtrer_ressources:
				adapterFiltre = new ArrayAdapter<String> (
	 				getActivity(),
	 				R.layout.element_liste_defaut,
	 				new String[] {
	 					getResources().getString(R.string.button_themes),
	 					getResources().getString(R.string.button_espaces)
	 				}
	 	    	);
				
				dialogBuilder = new AlertDialog.Builder(getActivity());
				dialogBuilder.setTitle(getString(R.string.title_filtrer) + " :")
					.setAdapter(adapterFiltre, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int themeOuEspace) {
							dialog.cancel();
							
							dialogBuilder = new AlertDialog.Builder(getActivity());
							dialogBuilder.setTitle(getString(R.string.title_filtrer) + " :")
								.setNegativeButton(R.string.button_annuler, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int itemSelectionne) {
										dialog.cancel();
									}
				 	        	});
								
							switch( themeOuEspace ) {
								// filtrer les ressources par thème
								case 0:
									filtrerRessources(listeThemes, themeOuEspace);
									break;
								// filtrer les ressources par espace
								case 1:
									filtrerRessources(listeEspaces, themeOuEspace);
									break;
							}
						}
	 	        	})
	 	        	.show();
	 	        
				return true;
 		}
		return false;
 	}

 	
 	// ouvre une fenêtre permettant de filtrer les ressources
	private void filtrerRessources(ArrayList<String> listeFiltres, final int themeOuEspace) {
		adapterFiltre = new ArrayAdapter<String>(getActivity(), R.layout.element_liste_defaut, listeFiltres);

		// création de la fenêtre contenant la liste des themes ou des espaces
		dialogBuilder.setAdapter(adapterFiltre, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int itemSelectionne) {
				switch( themeOuEspace ) {
					// filtrer les ressources par thème
					case 0:
						adapter = new ListWithCompoundDrawableAdapter(
				    		getActivity(),
				    		themesListeNomsRessources.get(itemSelectionne),
				    		themesListeUrlIconesRessources.get(itemSelectionne),
				    		R.layout.ressource_grille_ressources,
				    		ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_HAUT,
				    		themesListeIdLanguesRessources.get(itemSelectionne)
				        );
						break;
					// filtrer les ressources par espace
					case 1:
						adapter = new ListWithCompoundDrawableAdapter(
				    		getActivity(),
				    		espacesListeNomsRessources.get(itemSelectionne),
				    		espacesListeUrlIconesRessources.get(itemSelectionne),
				    		R.layout.ressource_grille_ressources,
				    		ListWithCompoundDrawableAdapter.COMPOUND_DRAWABLE_HAUT,
				    		espacesListeIdLanguesRessources.get(itemSelectionne)
				        );
						break;
				}
				
				gridViewRessources.setTag( Integer.valueOf(itemSelectionne) );
				gridViewRessources.setAdapter(adapter);
				gridViewRessources.setOnItemClickListener( new OnItemClickListener () {
					@Override
					public void onItemClick(AdapterView<?> parent, View ressourceView, int position, long id) {
						// si l'utilisateur clique sur un item (=une ressource) de la GridView, on lance l'écran ressources, on transmet l'id de la ressource
						switch( themeOuEspace ) {
							// filtrer les ressources par thème
							case 0:
								tabAjoutRessourceFragmentListener.onSelectedRessource(
										themesListeRessources.get(
											((Integer) gridViewRessources.getTag()).intValue()
										).get(position).getId()
									);
								break;
							// filtrer les ressources par espace
							case 1:
								tabAjoutRessourceFragmentListener.onSelectedRessource(
										espacesListeRessources.get(
											((Integer) gridViewRessources.getTag()).intValue()
										).get(position).getId()
									);
								break;
						}
					}
		        });
			}
        })
        .show();
	}
	
	
	// gestion du changement d'orientation
 	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if ( downloadBroadcastReceiver != null ) {
			getActivity().unregisterReceiver( downloadBroadcastReceiver );
		}
	}
	
  	
  	// interface permettant de communiquer avec l'activity parent
  	public interface TabAjoutRessourceFragmentListener {
  		// la méthode permet de récupérer la liste des ressources que l'utilisateur peut télécharger
  		public ArrayList<Ressource> getListeRessources();
  		// la méthode se déclenche quand l'utilisateur sélectionne une ressource dans une des grille des ressources
  		public void onSelectedRessource(String idRessource);
  	}
}
