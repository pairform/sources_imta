package cape.pairform.utiles;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import cape.pairform.modele.Capsule;
import cape.pairform.modele.Cercle;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.Message;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.PieceJointeInformation;
import cape.pairform.modele.ProfilUtilisateur;
import cape.pairform.modele.Ressource;
import cape.pairform.modele.Utilisateur;


public class ParserJSON {

	// parse un JSON contenant la liste des infos sur les ressources téléchargeables (nom, url, description, etc.)
	public static ArrayList<Ressource> parserRessources( JSONObject retourJSON ) {
		ArrayList<Ressource> listeRessources = new ArrayList<Ressource>();

		try {
			// s'il n'y a pas d'erreurs (status == "ok")
			if ( retourJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR).equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) ) {
				JSONArray listeRessourcesJSON = retourJSON.getJSONArray(Constantes.CLEF_RESSOURCES);
				JSONObject ressourcesJSON;

				for (int i=0; i< listeRessourcesJSON.length(); i++) {
					ressourcesJSON = listeRessourcesJSON.getJSONObject(i);

					listeRessources.add( new Ressource(
						ressourcesJSON.getString(Constantes.CLEF_ID_RESSOURCE),
						ressourcesJSON.getString(Constantes.CLEF_NOM_COURT),
						ressourcesJSON.getString(Constantes.CLEF_NOM_LONG),
						ressourcesJSON.getString(Constantes.CLEF_URL_LOGO),
						ressourcesJSON.getString(Constantes.CLEF_NOM_COURT_ESPACE),
						ressourcesJSON.getString(Constantes.CLEF_DESCRIPTION),
						ressourcesJSON.getString(Constantes.CLEF_THEME),
						ressourcesJSON.getString(Constantes.CLEF_ID_LANGUE),
						Integer.parseInt( ressourcesJSON.getString(Constantes.CLEF_ID_USAGE) ),
						Long.parseLong( ressourcesJSON.getString(Constantes.CLEF_DATE_CREATION) ),
						Long.parseLong( ressourcesJSON.getString(Constantes.CLEF_DATE_EDITION) ),
						null
					));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listeRessources;
	}


	// Parse un JSON contenant la liste des infos sur la ressource initiale à télécharger (nom, url, description, infos sur ses capsules, etc.)
	public static Ressource parserRessourceInitiale( JSONObject retourJSON, String[] idCapsulesInitiales ) {
		Ressource ressourceInitiale = null;
		ArrayList<Capsule> listeCapsules = new ArrayList<Capsule>();
		JSONObject capsuleJSONCourante;
		boolean estTelechargee = false;				// true si la capsule fait partie des capsules initiales devant être téléchargées

		try {			
			// s'il n'y a pas d'erreurs (status == "ok")
			if ( retourJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR).equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) ) {
				JSONObject ressourcesJSON = retourJSON.getJSONObject(Constantes.CLEF_RESSOURCE);
				JSONArray listeCapsulesJSON = ressourcesJSON.getJSONArray(Constantes.CLEF_CAPSULES);
				int taille;
				String urlMobile;

				// parcourt de la liste des capsules de la ressource initiale
				for (int i=0; i< listeCapsulesJSON.length(); i++) {
					capsuleJSONCourante = listeCapsulesJSON.getJSONObject(i);

					// si la taille de la capsule n'a pas été défini, elle vaudra un champ par défaut indiquant la non-précense d'une taille
					if( !capsuleJSONCourante.getString(Constantes.CLEF_TAILLE).equals("null") || capsuleJSONCourante.getString(Constantes.CLEF_TAILLE).isEmpty() ) {
						taille = Integer.parseInt( capsuleJSONCourante.getString(Constantes.CLEF_TAILLE) );
					} else {
						taille = Constantes.TAILLE_INEXISTANTE;
					}

					// si l'url est null, on vide le champ (pour uniformiser avec les cas ou l'url est un champ vide)
					if( capsuleJSONCourante.getString(Constantes.CLEF_URL_MOBILE).equals("null") ) {
						urlMobile = "";
					} else {
						urlMobile = capsuleJSONCourante.getString(Constantes.CLEF_URL_MOBILE);
					}

					// on parcoure la liste des id des capsules initiales
					for (String idCapsulesInitiale : idCapsulesInitiales) {
						// si la capsule fait partie des capsules initiales, on l'ajoute à la liste en indiquant qu'elle doit être téléchargée
						if( idCapsulesInitiale.equals(capsuleJSONCourante.getString(Constantes.CLEF_ID_CAPSULE)) ) {
							estTelechargee = true;

							listeCapsules.add( new Capsule(
								capsuleJSONCourante.getString(Constantes.CLEF_ID_CAPSULE),
								ressourcesJSON.getString(Constantes.CLEF_ID_RESSOURCE),
								capsuleJSONCourante.getString(Constantes.CLEF_ID_VISIBILITE),
								capsuleJSONCourante.getString(Constantes.CLEF_NOM_COURT),
								capsuleJSONCourante.getString(Constantes.CLEF_NOM_LONG),
								urlMobile,
								capsuleJSONCourante.getString(Constantes.CLEF_DESCRIPTION),
								capsuleJSONCourante.getString(Constantes.CLEF_LICENCE),
								taille,
								capsuleJSONCourante.getString(Constantes.CLEF_NOMS_AUTEURS),
								Long.parseLong( capsuleJSONCourante.getString(Constantes.CLEF_DATE_CREATION) ),
								Long.parseLong( capsuleJSONCourante.getString(Constantes.CLEF_DATE_EDITION) ),
								estTelechargee,
								capsuleJSONCourante.getString(Constantes.CLEF_SELECTEURS)
							));
							break;
						}
						estTelechargee = false;
					}

					// si la capsule ne fait pas partie des capsules initiales, on l'ajoute à la liste en indiquant qu'elle ne doit pas être téléchargée
					if(!estTelechargee)
						listeCapsules.add( new Capsule(
							capsuleJSONCourante.getString(Constantes.CLEF_ID_CAPSULE),
							ressourcesJSON.getString(Constantes.CLEF_ID_RESSOURCE),
							capsuleJSONCourante.getString(Constantes.CLEF_ID_VISIBILITE),
							capsuleJSONCourante.getString(Constantes.CLEF_NOM_COURT),
							capsuleJSONCourante.getString(Constantes.CLEF_NOM_LONG),
							urlMobile,
							capsuleJSONCourante.getString(Constantes.CLEF_DESCRIPTION),
							capsuleJSONCourante.getString(Constantes.CLEF_LICENCE),
							taille,
							capsuleJSONCourante.getString(Constantes.CLEF_NOMS_AUTEURS),
							Long.parseLong( capsuleJSONCourante.getString(Constantes.CLEF_DATE_CREATION) ),
							Long.parseLong( capsuleJSONCourante.getString(Constantes.CLEF_DATE_EDITION) ),
							estTelechargee,
							capsuleJSONCourante.getString(Constantes.CLEF_SELECTEURS)
						));
				}

				// création de la ressource initiale
				ressourceInitiale = new Ressource(
					ressourcesJSON.getString(Constantes.CLEF_ID_RESSOURCE),
					ressourcesJSON.getString(Constantes.CLEF_NOM_COURT),
					ressourcesJSON.getString(Constantes.CLEF_NOM_LONG),
					ressourcesJSON.getString(Constantes.CLEF_URL_LOGO),
					ressourcesJSON.getString(Constantes.CLEF_NOM_LONG_ESPACE),
					ressourcesJSON.getString(Constantes.CLEF_DESCRIPTION),
					ressourcesJSON.getString(Constantes.CLEF_THEME),
					ressourcesJSON.getString(Constantes.CLEF_ID_LANGUE),
					Integer.parseInt( ressourcesJSON.getString(Constantes.CLEF_ID_USAGE) ),
					Long.parseLong( ressourcesJSON.getString(Constantes.CLEF_DATE_CREATION) ),
					Long.parseLong( ressourcesJSON.getString(Constantes.CLEF_DATE_EDITION) ),
					listeCapsules
				);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ressourceInitiale;
	}


	// Parse un JSON contenant la liste des infos sur une ressource à télécharger (nom, url, description, infos sur ses capsules, etc.)
	public static Ressource parserRessource( JSONObject retourJSON ) {
		Ressource ressource = null;
		ArrayList<Capsule> listeCapsules = new ArrayList<Capsule>();
		JSONObject capsuleJSONCourante;

		try {			
			// s'il n'y a pas d'erreurs (status == "ok")
			if ( retourJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR).equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) ) {
				JSONObject ressourcesJSON = retourJSON.getJSONObject(Constantes.CLEF_RESSOURCE);
				JSONArray listeCapsulesJSON = ressourcesJSON.getJSONArray(Constantes.CLEF_CAPSULES);
				int taille;
				String urlMobile;

				// parcourt de la liste des capsules de la ressource
				for (int i=0; i< listeCapsulesJSON.length(); i++) {
					capsuleJSONCourante = listeCapsulesJSON.getJSONObject(i);

					// si la taille de la capsule n'a pas été défini, elle vaudra -1
					if( !capsuleJSONCourante.getString(Constantes.CLEF_TAILLE).equals("null") || capsuleJSONCourante.getString(Constantes.CLEF_TAILLE).isEmpty()) {
						taille = Integer.parseInt( capsuleJSONCourante.getString(Constantes.CLEF_TAILLE) );
					} else {
						taille = Constantes.TAILLE_INEXISTANTE;
					}
					// si l'url est null, on vide le champ (pour uniformiser avec les cas ou l'url est un champ vide)
					if( capsuleJSONCourante.getString(Constantes.CLEF_URL_MOBILE).equals("null") ) {
						urlMobile = "";
					} else {
						urlMobile = capsuleJSONCourante.getString(Constantes.CLEF_URL_MOBILE);
					}

					listeCapsules.add( new Capsule(
						capsuleJSONCourante.getString(Constantes.CLEF_ID_CAPSULE),
						ressourcesJSON.getString(Constantes.CLEF_ID_RESSOURCE),
						capsuleJSONCourante.getString(Constantes.CLEF_ID_VISIBILITE),
						capsuleJSONCourante.getString(Constantes.CLEF_NOM_COURT),
						capsuleJSONCourante.getString(Constantes.CLEF_NOM_LONG),
						urlMobile,
						capsuleJSONCourante.getString(Constantes.CLEF_DESCRIPTION),
						capsuleJSONCourante.getString(Constantes.CLEF_LICENCE),
						taille,
						capsuleJSONCourante.getString(Constantes.CLEF_NOMS_AUTEURS),
						Long.parseLong( capsuleJSONCourante.getString(Constantes.CLEF_DATE_CREATION) ),
						Long.parseLong( capsuleJSONCourante.getString(Constantes.CLEF_DATE_EDITION) ),
						false,
						capsuleJSONCourante.getString(Constantes.CLEF_SELECTEURS)
					));
				}

				// création de la ressource
				ressource = new Ressource(
					ressourcesJSON.getString(Constantes.CLEF_ID_RESSOURCE),
					ressourcesJSON.getString(Constantes.CLEF_NOM_COURT),
					ressourcesJSON.getString(Constantes.CLEF_NOM_LONG),
					ressourcesJSON.getString(Constantes.CLEF_URL_LOGO),
					ressourcesJSON.getString(Constantes.CLEF_NOM_LONG_ESPACE),
					ressourcesJSON.getString(Constantes.CLEF_DESCRIPTION),
					ressourcesJSON.getString(Constantes.CLEF_THEME),
					ressourcesJSON.getString(Constantes.CLEF_ID_LANGUE),
					Integer.parseInt( ressourcesJSON.getString(Constantes.CLEF_ID_USAGE) ),
					Long.parseLong( ressourcesJSON.getString(Constantes.CLEF_DATE_CREATION) ),
					Long.parseLong( ressourcesJSON.getString(Constantes.CLEF_DATE_EDITION) ),
					listeCapsules
				);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ressource;
	}


	// parse un JSON contenant la liste des messages de chaque page d'une ressource
	public static ArrayList<Message> parserMessages( JSONObject listeMessagesJSON ) {
		ArrayList<Message> listeMessage = new ArrayList<Message>();
		Message messageCourant;
		JSONObject messageJSONCourant;

		try {
			JSONArray messagesJSON = listeMessagesJSON.getJSONArray(Constantes.CLEF_LISTE_MESSAGES);

			for (int i=0; i< messagesJSON.length(); i++) {
				messageJSONCourant = messagesJSON.getJSONObject(i);

				messageCourant = new Message(
					messageJSONCourant.getString(Constantes.CLEF_ID_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_ID_CAPSULE),
					messageJSONCourant.getString(Constantes.CLEF_ID_AUTEUR_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_ID_ROLE_AUTEUR_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_PSEUDO_AUTEUR_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_URL_AVATAR_AUTEUR_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_NOM_PAGE_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_NOM_TAG_PAGE_MESSAGE),
					Integer.parseInt( messageJSONCourant.getString(Constantes.CLEF_NUM_OCCURENCE_TAG_PAGE_MESSAGE) ),
					messageJSONCourant.getString(Constantes.CLEF_CONTENU_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_ID_MESSAGE_PARENT),
					Integer.parseInt( messageJSONCourant.getString(Constantes.CLEF_SOMME_VOTES_MESSAGE) ),
					messageJSONCourant.getString(Constantes.CLEF_MEDAILLE_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_UTILISATEUR_A_VOTE_MESSAGE),
					Integer.parseInt( messageJSONCourant.getString(Constantes.CLEF_DEFI_MESSAGE) ),
					Integer.parseInt( messageJSONCourant.getString(Constantes.CLEF_REPONSE_DEFI__VALIDE_MESSAGE) ) == 1,
					false,
					messageJSONCourant.getString(Constantes.CLEF_SUPPRIME_PAR_MESSAGE),
					messageJSONCourant.getString(Constantes.CLEF_ID_LANGUE),
					Long.parseLong( messageJSONCourant.getString(Constantes.CLEF_DATE_CREATION_MESSAGE) ),
					Long.parseLong( messageJSONCourant.getString(Constantes.CLEF_DATE_MODIF_MESSAGE) ),
					messageJSONCourant.getString(Constantes.CLEF_GEO_LATITUDE),
					messageJSONCourant.getString(Constantes.CLEF_GEO_LONGITUDE),
					Integer.parseInt( messageJSONCourant.getString(Constantes.CLEF_VISIBILITE_RESTREINTE_MESSAGE) ) == 1
				);

				// récupération des tags et des auteurs des tags stockés dans un String (ex: "['tag1', 'tag2']" )
				JSONArray listeTags = new JSONArray( messageJSONCourant.getString(Constantes.CLEF_TAGS_MESSAGE) );
				JSONArray listeAuteursTags = new JSONArray( messageJSONCourant.getString(Constantes.CLEF_NOMS_AUTEURS_TAGS_MESSAGE) );

				for(int j=0; j <listeTags.length(); j++) {
					messageCourant.addTag(
						listeTags.getString(j),
						listeAuteursTags.getString(j)
					);
				}

				listeMessage.add(messageCourant);	

				//recuperation de l'info des pieces jointes			
				JSONArray jsonPiecesJointes = new JSONArray( messageJSONCourant.getString( Constantes.CLEF_WEBSERVICE_PIECES_JOINTES_MESSAGE ) );
				for(int j = 0; j < jsonPiecesJointes.length(); ++j) {

					JSONObject piece = jsonPiecesJointes.getJSONObject(j);

					PieceJointe pj = new PieceJointe(
						messageCourant.getId(), 
						piece.getString(Constantes.CLEF_NOM_ORIGINAL_PJ), 
						piece.getString(Constantes.CLEF_NOM_SERVEUR_PJ), 
						piece.getString(Constantes.CLEF_EXTENSION_PJ), 
						"",
						Integer.parseInt(piece.getString(Constantes.CLEF_TAILLE_PJ))
					);

					messageCourant.ajoutePJ(pj);			
				}

			}			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return listeMessage;
	}

	// parse un JSON contenant la liste des utilisateurs de l'application
	public static ArrayList<Utilisateur> parserUtilisateurs( JSONObject listeUtilisateursJSON ) {
		ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
		Utilisateur utilisateurCourant;
		JSONObject utilisateurJSONCourant;

		try {
			JSONArray utilisateursJSON = listeUtilisateursJSON.getJSONArray(Constantes.CLEF_LISTE_UTILISATEURS);

			for (int i=0; i< utilisateursJSON.length(); i++) {
				utilisateurJSONCourant = utilisateursJSON.getJSONObject(i);

				utilisateurCourant = new Utilisateur(
					utilisateurJSONCourant.getString(Constantes.CLEF_ID_UTILISATEUR),
					utilisateurJSONCourant.getString(Constantes.CLEF_PSEUDO_UTILISATEUR),
					utilisateurJSONCourant.getString(Constantes.CLEF_URL_UTILISATEUR)
				);

				listeUtilisateurs.add(utilisateurCourant);				
			}			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return listeUtilisateurs;
	}


	// parse un JSON contenant la description du profil d'un utilisateur
	public static ProfilUtilisateur parserProfil( JSONObject retourJSON, String idUtilisateur ) {
		ProfilUtilisateur profil = null;
		JSONObject elementCourantJSON;
		HashMap<String,String> elementCourant;

		try {
			// s'il n'y a pas d'erreurs (status == "ok")
			if ( retourJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR).equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) ) {
				JSONObject profilJSON = retourJSON.getJSONObject( Constantes.CLEF_WEBSERVICE_CLEF_DATAS );

				profil = new ProfilUtilisateur(
					idUtilisateur,
					profilJSON.getString(Constantes.CLEF_PSEUDO_UTILISATEUR),
					profilJSON.getString(Constantes.CLEF_ETABLISSEMENT_UTILISATEUR),
					profilJSON.getString(Constantes.CLEF_URL_AVATAR),
					profilJSON.getString(Constantes.CLEF_WEBSERVICE_LANGUE_PRINCIPALE)
				);

				JSONArray listeRessourcesJSON = profilJSON.getJSONArray(Constantes.CLEF_RESSOURCES);

				if (listeRessourcesJSON != null) {
					for (int i=0; i< listeRessourcesJSON.length(); i++) {
						elementCourantJSON = listeRessourcesJSON.getJSONObject(i);

						elementCourant = new HashMap<String,String>();
						elementCourant.put(Constantes.CLEF_ID_RESSOURCE, elementCourantJSON.getString(Constantes.CLEF_ID_RESSOURCE));
						elementCourant.put(Constantes.CLEF_NOM_COURT, elementCourantJSON.getString(Constantes.CLEF_NOM_COURT));
						elementCourant.put(Constantes.CLEF_SCORE_UTILISATEUR_DANS_RESSOURCE, elementCourantJSON.getString(Constantes.CLEF_SCORE_UTILISATEUR_DANS_RESSOURCE));
						elementCourant.put(Constantes.CLEF_ID_ROLE_UTILISATEUR, elementCourantJSON.getString(Constantes.CLEF_ID_ROLE_UTILISATEUR));

						profil.addRessource(elementCourant);
					}			
				}

				JSONArray listeSuccesJSON = profilJSON.getJSONArray(Constantes.CLEF_SUCCES);

				profil.setNbSuccesDebloques( profilJSON.getInt(Constantes.CLEF_NB_SUCCES_GAGNE) );
				profil.setNbSuccesTotal( profilJSON.getInt(Constantes.CLEF_NB_SUCCES_TOTAL));

				for (int j=0; j< listeSuccesJSON.length(); j++) {
					elementCourantJSON = listeSuccesJSON.getJSONObject(j);

					elementCourant = new HashMap<String,String>();
					elementCourant.put(Constantes.CLEF_NOM, elementCourantJSON.getString(Constantes.CLEF_NOM));
					elementCourant.put(Constantes.CLEF_DESCRIPTION, elementCourantJSON.getString(Constantes.CLEF_DESCRIPTION));
					
					// si le sucès est débloqué par l'utilisateur
					if (elementCourantJSON.getInt(Constantes.CLEF_SUCCES_DEBLOQUE) == 1)
						elementCourant.put(Constantes.CLEF_LOGO_SUCCES, elementCourantJSON.getString(Constantes.CLEF_LOGO_SUCCES));

					profil.addSucces(elementCourant);
				}

				JSONArray listeOpenBadgeJSON = profilJSON.getJSONArray(Constantes.CLEF_OPENBADGES);

				profil.setNbOpenBadges( profilJSON.getInt(Constantes.CLEF_NB_OPENBADGES) );

				for (int j=0; j< listeOpenBadgeJSON.length(); j++) {
					elementCourantJSON = listeOpenBadgeJSON.getJSONObject(j);

					elementCourant = new HashMap<String,String>();
					elementCourant.put(Constantes.CLEF_NOM, elementCourantJSON.getString(Constantes.CLEF_NOM));
					elementCourant.put(Constantes.CLEF_DESCRIPTION, elementCourantJSON.getString(Constantes.CLEF_DESCRIPTION));
					elementCourant.put(Constantes.CLEF_EMETTEUR_OPENBADGE, elementCourantJSON.getString(Constantes.CLEF_EMETTEUR_OPENBADGE));
					elementCourant.put(Constantes.CLEF_DATE_GAGNE, elementCourantJSON.getString(Constantes.CLEF_DATE_GAGNE));
					elementCourant.put(Constantes.CLEF_URL_LOGO, elementCourantJSON.getString(Constantes.CLEF_URL_LOGO));

					profil.addBadge(elementCourant);
				}
			}						
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return profil;
	}


	// parse un JSON contenant la liste des badges qu'un utilisateur peut attribuer dans chaque ressource
	public static HashMap< String, ArrayList<HashMap<String,String>> > parserOpenBadges( JSONObject retourJSON ) {
		HashMap< String, ArrayList<HashMap<String,String>> > listeOpenBadges = new HashMap< String, ArrayList<HashMap<String,String>> >();
		JSONObject elementCourantJSON;
		HashMap<String,String> elementCourant;

		try {
			// s'il n'y a pas d'erreurs (status == "ok")
			if ( retourJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR).equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) ) {
				JSONArray listeOpenBadgeJSON = retourJSON.getJSONArray(Constantes.CLEF_WEBSERVICE_CLEF_DATA);

				for (int j=0; j< listeOpenBadgeJSON.length(); j++) {
					elementCourantJSON = listeOpenBadgeJSON.getJSONObject(j);

					elementCourant = new HashMap<String,String>();
					elementCourant.put(Constantes.CLEF_ID_OPENBADGE, elementCourantJSON.getString(Constantes.CLEF_ID_OPENBADGE));
					elementCourant.put(Constantes.CLEF_NOM, elementCourantJSON.getString(Constantes.CLEF_NOM));
					elementCourant.put(Constantes.CLEF_DESCRIPTION, elementCourantJSON.getString(Constantes.CLEF_DESCRIPTION));
					elementCourant.put(Constantes.CLEF_EMETTEUR_OPENBADGE, elementCourantJSON.getString(Constantes.CLEF_EMETTEUR_OPENBADGE));
					elementCourant.put(Constantes.CLEF_URL_LOGO, elementCourantJSON.getString(Constantes.CLEF_URL_LOGO));

					if (listeOpenBadges.containsKey( elementCourantJSON.getString(Constantes.CLEF_ID_RESSOURCE) )) {
						listeOpenBadges.get(elementCourantJSON.getString(Constantes.CLEF_ID_RESSOURCE)).add(elementCourant);
					} else {
						listeOpenBadges.put(
							elementCourantJSON.getString(Constantes.CLEF_ID_RESSOURCE),
							new ArrayList<HashMap<String,String>>()
						);
						listeOpenBadges.get(elementCourantJSON.getString(Constantes.CLEF_ID_RESSOURCE)).add(elementCourant);
					}
				}
			}						
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return listeOpenBadges;
	}


	// parse un JSON contenant les erreurs retourné par un utilisateur rejoignant une classe
	public static String parserRetourRejoindreClasse( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par la suppression d'un membre d'un cercle
	public static String parserRetourSuppressionMembreCercle( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par l'ajout d'un utilisateur à un cercle
	public static String parserRetourAjoutMembreCercle( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par la suppression d'un cercle
	public static String parserRetourSuppressionCercle( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par la suppression d'un message
	public static String parserRetourSuppressionMessage( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par la création de compte
	public static String parserRetourCreationCompte( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant l'identifiant retourné par la création de compte
	public static String parserIdUtilisateurRetourCreationCompte( String stringJSON ) {
		String id_utilisateur = null;

		try {			
			JSONObject codeJSON = new JSONObject(stringJSON);
			id_utilisateur = codeJSON.getString(Constantes.CLEF_ID_UTILISATEUR);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return id_utilisateur;
	}


	// parse un JSON contenant les erreurs retourné par la modification de compte
	public static String parserRetourModificationCompte( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par la modification de l'avatar du compte
	public static String parserRetourModificationAvatarCompte( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par l'enregistrement d'un nouveau message
	public static String parserRetourEnregistrementMessage( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par le changement d'affichage noms-prenoms / pseudo
	public static String parserRetourAfficherNomsPrenoms( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par l'activation / désactivation de l'email de notification d'une ressource
	public static String parserRetourActiverEmailNotification( String stringJSON ) {
		return parserRetourJSON(stringJSON);
	}


	// parse un JSON contenant les erreurs retourné par un webService
	public static String parserRetourJSON( String stringJSON ) {
		String erreur = null;
		try {
			JSONObject codeErreurJSON = new JSONObject(stringJSON);
			String status = codeErreurJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR);

			// s'il y a une erreur (status est à "ko")
			if ( !status.equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) && !status.equals(Constantes.CLEF_WEBSERVICE_RETOUR_UP) ) {
				// s'il y a un message d'erreur spécifique, on le récupère
				if ( codeErreurJSON.has(Constantes.CLEF_WEBSERVICE_CLEF_ERREUR) ) {
					erreur = codeErreurJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_ERREUR);
				} else {
					erreur = "";
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return erreur;
	}

	// parse un JSON contenant les infos sur le message envoyé au serveur
	public static HashMap<String,String> parserRetourMessage( String stringJSON, ArrayList<PieceJointe> pjServeur) {
		HashMap<String, String> infos = new HashMap<String, String>();

		try {
			JSONObject codeRetour = new JSONObject(stringJSON);
			String status = codeRetour.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR);

			// s'il y a une erreur (status est à "ko")
			if ( !status.equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) && !status.equals(Constantes.CLEF_WEBSERVICE_RETOUR_UP) ) {
				infos.put(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR, codeRetour.getString(Constantes.CLEF_WEBSERVICE_CLEF_ERREUR));
			}else{

				String idMessage = codeRetour.getString(Constantes.CLEF_ID_MESSAGE);

				if(idMessage != null){
					infos.put(Constantes.CLEF_ID_MESSAGE, idMessage);
				}

				if(pjServeur.size() > 0){
					JSONObject donneePJJSONObject = codeRetour.getJSONObject(Constantes.CLEF_WEBSERVICE_DATA_PJ);
					if(donneePJJSONObject != null){
						JSONObject donnee = donneePJJSONObject.getJSONObject(Constantes.CLEF_WEBSERVICE_DATA_PJ);
						//Contient tous les noms des pjs ajoutés 
						JSONArray names = donnee.names();
						JSONObject pjJSON;

						if(names != null && names.length() == pjServeur.size()){
							for (PieceJointe pj : pjServeur) {

								String pjName = pj.getNomOriginal();
								pjJSON = donnee.getJSONObject(pjName);

								pj.setIdMessage(idMessage);
								pj.setNomServeur(pjJSON.getString(Constantes.CLEF_WEBSERVICE_PJ_NOM_SERVEUR));
								pj.setExtension(pjJSON.getString(Constantes.CLEF_WEBSERVICE_PJ_EXTENSION));
								pj.setTaille(pjJSON.getInt(Constantes.CLEF_WEBSERVICE_PJ_TAILLE));
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return infos;
	}


	public static PieceJointeInformation parserInformationsPieceJointe(String stringJSON, Context c) {
		PieceJointeInformation info = PieceJointeInformation.getInstance(c);

		try {
			JSONObject codeRetour = new JSONObject(stringJSON);
			String status = codeRetour.getString(Constantes.CLEF_WEBSERVICE_CLEF_RETOUR);

			// s'il y a une erreur (status est à "ko")
			if ( !status.equals(Constantes.CLEF_WEBSERVICE_RETOUR_OK) && !status.equals(Constantes.CLEF_WEBSERVICE_RETOUR_UP) ) {
				
			}else{
				JSONArray datas = codeRetour.getJSONArray(Constantes.CLEF_WEBSERVICE_DATA_PJ_INFORMATION);
				JSONObject data = datas.getJSONObject(0);
				info.vider();
				info.setNombreMax(data.getInt(Constantes.CLEF_WEBSERVICE_PJ_NB_MAX));
				info.setTailleMax(data.getLong(Constantes.CLEF_WEBSERVICE_PJ_MAX_TAILLE));
				info.setThumbnailTaille(data.getInt(Constantes.CLEF_WEBSERVICE_PJ_THUMBNAIL_TAILLE));

				JSONArray format = data.getJSONArray(Constantes.CLEF_WEBSERVICE_PJ_FORMAT);
				for(int i=0; i < format.length(); i++){
					JSONObject row = format.getJSONObject(i);
					String type = row.getString(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_TYPE);

					if(type.equals(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_PHOTO)){
						JSONArray valeurs = row.getJSONArray(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_VALEUR);
						
						for(int j=0; j<valeurs.length(); j++)
							info.addFormatPhotoUpload(valeurs.getString(j));
					}
					else if(type.equals(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_VIDEO)){
						JSONArray valeurs = row.getJSONArray(Constantes.CLEF_WEBSERVICE_PJ_FORMAT_VALEUR);
												
						for(int j=0; j<valeurs.length(); j++)
							info.addFormatVideoUpload(valeurs.getString(j));
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return info;
	}

	// parse un JSON contenant les infos sur l'utilisateur retourné par la connexion
	public static HashMap<String,String> parserRetourConnexion( String stringJSON ) {
		HashMap<String, String> infosUtilisateur = null;

		try {
			JSONObject infosJSON = new JSONObject(stringJSON);
			if ( infosJSON.has(Constantes.CLEF_ID_UTILISATEUR) ) {
				infosUtilisateur = new HashMap<String, String>();
				infosUtilisateur.put(Constantes.CLEF_ID_UTILISATEUR, infosJSON.getString(Constantes.CLEF_ID_UTILISATEUR));								// id de l'utilisateur
				infosUtilisateur.put(Constantes.CLEF_PSEUDO_UTILISATEUR, infosJSON.getString(Constantes.CLEF_PSEUDO_UTILISATEUR));						// pseudo de l'utilisateur
				infosUtilisateur.put(Constantes.CLEF_ETABLISSEMENT_UTILISATEUR, infosJSON.getString(Constantes.CLEF_ETABLISSEMENT_UTILISATEUR));		// etablissement de l'utilisateur
				infosUtilisateur.put(Constantes.CLEF_LANGUE_PRINCIPALE, infosJSON.getString(Constantes.CLEF_WEBSERVICE_LANGUE_PRINCIPALE));				// langue principale de l'utilisateur

				// Recuperation des autres langues de l'utilisateur
				JSONArray listeAutresLangues = infosJSON.getJSONArray(Constantes.CLEF_WEBSERVICE_AUTRES_LANGUES);
				if (listeAutresLangues != null) {
					for (int i=0; i< listeAutresLangues.length(); i++)
						infosUtilisateur.put(Constantes.CLEF_AUTRES_LANGUES + "_" + listeAutresLangues.getString(i), "0");
				}

				// on parcoure une liste de capsule contenant : le role de l'utilisateur dans la ressource associée, et, si l'utilisateur a activé ou non la notification email dans la ressource associée
				JSONObject listeRolesCapsulesJSON = infosJSON.getJSONObject(Constantes.CLEF_ROLE_UTILISATEUR);
				if (listeRolesCapsulesJSON != null) {
					JSONArray indexlisteRolesCapsulesJSON = listeRolesCapsulesJSON.names();

					if (indexlisteRolesCapsulesJSON != null) {
						for (int i=0; i< indexlisteRolesCapsulesJSON.length(); i++) {
							infosUtilisateur.put(	// role de l'utilisateur dans la capsule courante
								listeRolesCapsulesJSON.getJSONObject(indexlisteRolesCapsulesJSON.getString(i)).getString(Constantes.CLEF_ID_CAPSULE),			// id de la capsule
								listeRolesCapsulesJSON.getJSONObject(indexlisteRolesCapsulesJSON.getString(i)).getString(Constantes.CLEF_ID_ROLE_UTILISATEUR)	// id du rôle
							);
							infosUtilisateur.put(	// role de l'utilisateur dans la capsule courante
								Constantes.CLEF_ID_RESSOURCE + listeRolesCapsulesJSON.getJSONObject(indexlisteRolesCapsulesJSON.getString(i)).getString(Constantes.CLEF_ID_RESSOURCE),	// id de la ressource
								listeRolesCapsulesJSON.getJSONObject(indexlisteRolesCapsulesJSON.getString(i)).getString(Constantes.CLEF_ID_ROLE_UTILISATEUR)							// id du rôle
							);
							infosUtilisateur.put(	// réglage d'activation / désactivation de l'email de notification pour la capsule courante
								Constantes.CLEF_NOTIFICATION_EMAIL + "_" + listeRolesCapsulesJSON.getJSONObject(indexlisteRolesCapsulesJSON.getString(i)).getString(Constantes.CLEF_ID_CAPSULE),
								listeRolesCapsulesJSON.getJSONObject(indexlisteRolesCapsulesJSON.getString(i)).getString(Constantes.CLEF_NOTIFICATION_EMAIL)
							);
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return infosUtilisateur;
	}


	// parse un JSON contenant les infos sur le rôle de l'utilisateur dans une ressource venant d'être téléchargée
	public static HashMap<String,String> parserRetourRecuperationRole( String stringJSON ) {
		HashMap<String, String> infosUtilisateur = null;

		try {
			JSONObject infosJSON = new JSONObject(stringJSON);

			if ( infosJSON.has(Constantes.CLEF_ROLE_UTILISATEUR) ) {
				infosUtilisateur = new HashMap<String, String>();
				JSONArray listeRolesRessourcesJSON = infosJSON.getJSONArray(Constantes.CLEF_ROLE_UTILISATEUR);

				for (int i=0; i< listeRolesRessourcesJSON.length(); i++) {
					infosUtilisateur.put(	// role de l'utilisateur dans la ressource courante
						listeRolesRessourcesJSON.getJSONObject(i).getString(Constantes.CLEF_ID_RESSOURCE),			// id de la ressource
						listeRolesRessourcesJSON.getJSONObject(i).getString(Constantes.CLEF_ID_ROLE_UTILISATEUR)	// id du rôle
					);
					infosUtilisateur.put(	// réglage d'activation / désactivation de l'email de notification pour la ressource courante
						Constantes.CLEF_NOTIFICATION_EMAIL + "_" + listeRolesRessourcesJSON.getJSONObject(i).getString(Constantes.CLEF_ID_RESSOURCE),
						listeRolesRessourcesJSON.getJSONObject(i).getString(Constantes.CLEF_NOTIFICATION_EMAIL)
					);
				}				
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return infosUtilisateur;
	}


	// parse un JSON contenant la liste des cercles et des classes de l'utilisateur
	public static ArrayList<Cercle> parserCercles( String stringJSON ) {
		ArrayList<Cercle> listeCercles = new ArrayList<Cercle>();
		Cercle cercleCourant;

		try {
			JSONObject infosJSON = new JSONObject(stringJSON);

			// récupération des cercles
			JSONArray listeCerclesJSON = infosJSON.getJSONArray(Constantes.CLEF_LISTE_CERCLES);

			for (int i=0; i< listeCerclesJSON.length(); i++) {
				cercleCourant = new Cercle (
					listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_NOM),			// nom du cercle	
					listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_ID_CERCLE),		// id du cercle
					false,																		// false, le cercle n'est pas une classe
					true																		// true, l'utilisateur a créé le cercle
				);

				cercleCourant.setListeMembresCercle( parserUtilisateurs( listeCerclesJSON.getJSONObject(i) ) );
				listeCercles.add(cercleCourant);
			}

			// récupération des classes créés par l'utilisateur
			listeCerclesJSON = infosJSON.getJSONArray(Constantes.CLEF_LISTE_CLASSES);

			for (int i=0; i< listeCerclesJSON.length(); i++) {
				cercleCourant = new Cercle (
					listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_NOM),			// nom du cercle	
					listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_ID_CERCLE),		// id du cercle
					true,																		// true, le cercle est une classe
					true																		// true, l'utilisateur a créé le cercle
				);

				cercleCourant.setClefClasse( listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_CLEF_CLASSE) );

				cercleCourant.setListeMembresCercle( parserUtilisateurs( listeCerclesJSON.getJSONObject(i) ) );
				listeCercles.add(cercleCourant);
			}

			// récupération des classes rejointes
			listeCerclesJSON = infosJSON.getJSONArray(Constantes.CLEF_LISTE_CLASSES_REJOINTES);

			for (int i=0; i< listeCerclesJSON.length(); i++) {
				cercleCourant = new Cercle (
					listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_NOM),			// nom du cercle	
					listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_ID_CERCLE),		// id du cercle
					true,																		// true, le cercle est une classe
					false																		// false, l'utilisateur n'a pas créé le cercle
				);

				cercleCourant.setClefClasse( listeCerclesJSON.getJSONObject(i).getString(Constantes.CLEF_CLEF_CLASSE) );

				cercleCourant.setListeMembresCercle( parserUtilisateurs( listeCerclesJSON.getJSONObject(i) ) );
				listeCercles.add(cercleCourant);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return listeCercles;		
	}


	// parse un JSON contenant, soit les erreurs retournées par la création d'un cercle (ou d'une classe), soit la clef d'inscription à la classe
	public static String parserRetourCreationCercle( String stringJSON ) {
		String retourCreationCercle = null;

		try {			
			JSONObject retourJSON = new JSONObject(stringJSON);
			if ( retourJSON.has(Constantes.CLEF_WEBSERVICE_CLEF_ERREUR) ) {
				retourCreationCercle = retourJSON.getString(Constantes.CLEF_WEBSERVICE_CLEF_ERREUR);

			} else if ( retourJSON.has(Constantes.CLEF_CLEF_CLASSE) ) {
				retourCreationCercle = retourJSON.getString(Constantes.CLEF_CLEF_CLASSE);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return retourCreationCercle;		
	}


	// parse un JSON contenant la dernière date de mise à jour d'une ressource
	public static String parserRetourRechercheMiseAJourRessource( String stringJSON ) {
		String retourRechercheMiseAJourRessource = "";

		try {			
			JSONObject retourJSON = new JSONObject(stringJSON);
			retourRechercheMiseAJourRessource = retourJSON.getString(Constantes.CLEF_DATE_EDITION);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return retourRechercheMiseAJourRessource;		
	}
}
