package cape.pairform.utiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import cape.pairform.modele.ChapitreCapsule;

import android.util.Log;
import android.util.Xml;


// class utilisée pour parser les fichiers "outline.xml" permettant d'obtenir le sommaire d'une ressource (l'arborescence des chapitres et pages)
public class ParserXmlCapsule extends DefaultHandler {

	private static final String LOG_TAG = ParserXmlCapsule.class.getSimpleName();	// nom de la classe, utilisé comme identifiant pour les logs
	
	// nom des balises et attributs dans le fichier XML décrivant l'arborescence d'une ressource
	private static final String BALISE_RACINE = "outline";				// balise représentant le chapitre racine
	private static final String BALISE_CHAPITRE = "b";					// balise représentant un chapitre
	private static final String BALISE_PAGE = "l";						// balise représentant une page
	private static final String ATTRIBUT_TITRE = "t";					// titre de la page
	private static final String ATTRIBUT_URL = "u";						// nom du fichier ou se trouve la page
	
	private ChapitreCapsule chapitreCourant;							// le chapitre courant
	private ArrayList<ChapitreCapsule> listeArborescenceChapitre; 	// liste des chapitres, l'index représente la profondeur de l'élément dans l'arborescence xml
	int profondeur;														// profondeur d'un élément dans l'arborescence xml
	
		
	public ChapitreCapsule parse(File fichierXml) {
		Log.i(LOG_TAG, "Lancement parsing XML de l'arborescence d'une ressource");
				
		listeArborescenceChapitre = new ArrayList<ChapitreCapsule>();	
				
		// on parse le XML avec le handler ParserXmlRessource et on retourne le chapitre racine
        try {
            Xml.parse(new FileInputStream(fichierXml), Xml.Encoding.UTF_8, this);            
            return listeArborescenceChapitre.get(0);
            
        } catch (SAXException e) {
        	e.printStackTrace();
        } catch (IOException e) {
        	e.printStackTrace();
        }

        return null;
	}
	
	
	@Override
	public void startElement(String uri, String nomBalise, String qName, Attributes attributs) {
		
		if( nomBalise.equals(BALISE_PAGE) ) {
			// ouverture d'une balise page
			//Log.i(LOG_TAG, "parsing XML : création nouvelle page '" + attributs.getValue(ATTRIBUT_TITRE) + "'");
        	// ajout de la page au chapitre dans elle fait partie 
        	listeArborescenceChapitre.get(profondeur).addPageFils(attributs.getValue(ATTRIBUT_TITRE), "co/"+ attributs.getValue(ATTRIBUT_URL));
			
		} else if ( nomBalise.equals(BALISE_CHAPITRE) ) {
			// ouverture d'une balise chapitre
			//Log.i(LOG_TAG, "parsing XML : création nouveau chapitre '" + attributs.getValue(ATTRIBUT_TITRE) + "'");
        	// création d'un nouveau chapitre, on augmente donc la profondeur
        	chapitreCourant = new ChapitreCapsule(attributs.getValue(ATTRIBUT_TITRE));
        	profondeur++;
        	listeArborescenceChapitre.add(profondeur, chapitreCourant);            	
	        
		} else if( nomBalise.equals(BALISE_RACINE) ) {
			// ouverture de la balise racine
			//Log.i(LOG_TAG, "parsing XML : création chapitre racine");
        	// création du chapitre racine, initialisation de la profondeur
        	chapitreCourant = new ChapitreCapsule(attributs.getValue(ATTRIBUT_TITRE), true);
        	profondeur = 0;
        	listeArborescenceChapitre.add(profondeur, chapitreCourant);
        	
		}		
	}
	
	
	@Override
	public void endElement(String uri, String nomBalise, String qName) {
		if ( nomBalise.equals(BALISE_CHAPITRE) ) {			// fermeture d'une balise chapitre
			// on ajoute le chapitre à la liste des chapitres de son chapitre pére
        	chapitreCourant = listeArborescenceChapitre.get(profondeur);
        	profondeur--;
        	listeArborescenceChapitre.get(profondeur).addChapitreFils(chapitreCourant);
		} 
	}
}
