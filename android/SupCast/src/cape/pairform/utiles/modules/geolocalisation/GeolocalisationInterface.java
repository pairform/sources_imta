package cape.pairform.utiles.modules.geolocalisation;

import android.location.Location;

/**
 * Interface de communication avec le module de gestion des positions GPS
 * @author vincent
 *
 */
public interface GeolocalisationInterface {

	/**
	 * Méthode appelée dès que la position GPS est récupéré 
	 * @param location La nouvelle position
	 */
	public void recupérationDeLaPositionReussie(Location location);

	/**
	 * Cette fonction est déclenché dès que l'état d'un fournisseur de position change
	 * @param flag : Etat du fournisseur
	 * @param provider : String contenant les informations du fournisseur de position qui à déclenché cette fonction
	 * @param erreurCritique : booléen permettant de signaler si la position est irrécupérable
	 */
	public void recupérationDeLaPositionEchoue(int flag, String provider, boolean erreurCritique);
}
