package cape.pairform.utiles.modules.geolocalisation;

import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;

/**
 * Celle classe permet de récupérer la position GPS de l'utilisateur
 * @author vincent
 *
 */
public class GeolocalisationModule {
	//Interface de communication
	private GeolocalisationInterface destinataire;
	private LocationManager locationManager;
	//HashMap contenant les providers qui sont actuellement en échec
	private Set<String> echecFournisseur;
	//Variable d'instance pour singletonner la classe
	private static GeolocalisationModule instance;	
	//Flag représentant un fournisseur de position désactivé
	public static final int FOURNISSEUR_DESACTIVE = 0;
	
	//Constructeur du singleton
	public static GeolocalisationModule getInstance(GeolocalisationInterface destinataire, Context c){
		if(instance == null)
			instance = new GeolocalisationModule(destinataire, c);	
		
		// inscrit l'application à écouter deux fournisseurs : le réseau internet et la puce GPS
		instance.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 600000, instance.locationListener);
		instance.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 600000, instance.locationListener);
		
		return instance;
	}
	
	//Constructeur de la classe
	private GeolocalisationModule(GeolocalisationInterface destinataire, Context c) {
		this.destinataire = destinataire;
		locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
		echecFournisseur = new HashSet<String>();
	}

	/**
	 * Cette méthode désinscrit l'écoute de la mise à jour de la position 
	 */
	public void stopLocalisation(){
		locationManager.removeUpdates(locationListener);
	}
	
	// Listener qui écoute les retour des fournisseurs
	LocationListener locationListener = new LocationListener() {
		//Appelée dès que la position change
		public void onLocationChanged(Location location) {
			// On previens à travers l'interface de communication
			destinataire.recupérationDeLaPositionReussie(location);
		}

		/**
		 * Cette fonction va servir pour gérer l'état des fournisseurs 
		 */
		public void onStatusChanged(String provider, int status, Bundle extras) {	
			switch (status) {
			case LocationProvider.OUT_OF_SERVICE:
			case LocationProvider.TEMPORARILY_UNAVAILABLE:
				echecFournisseur.add(provider);
				break;
			case LocationProvider.AVAILABLE:
				echecFournisseur.remove(provider);
				break;
			default:
				break;
			}
			
			verifiePositionIrrecuperable(provider);
		}

		/**
		 * Méthode appelée dès qu'un fournisseur est disponible
		 */
		public void onProviderEnabled(String provider) {
			echecFournisseur.remove(provider);
		}

		/**
		 * Méthode appelée dès qu'un fournisseur est désactivé par l'utilisateur
		 */
		public void onProviderDisabled(String provider) {
			echecFournisseur.add(provider);
			verifiePositionIrrecuperable(provider);
		}
	};

	/**
	 * Méthode permettant de renvoyer à travers l'interface de communication si tous les fournisseurs en écoute sont inaccessible.
	 * @param provider : un string contenant toutes les informations sur le fournisseur
	 */
	public void verifiePositionIrrecuperable(String provider){
		destinataire.recupérationDeLaPositionEchoue(FOURNISSEUR_DESACTIVE, provider, (echecFournisseur.size()>=locationManager.getProviders(true).size())? true : false );
	}

}
