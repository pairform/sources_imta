package cape.pairform.utiles;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.MediaStore;
import android.util.Log;
import cape.pairform.R;
import cape.pairform.controleurs.activities.EcranAideActivity;
import cape.pairform.modele.Constantes;
import cape.pairform.modele.PieceJointe;
import cape.pairform.modele.PieceJointeInformation;


public class Utiles {

	private static final String LOG_TAG = Utiles.class.getSimpleName();
	public static final int THUMBNAIL_FLAG = 1;
	public static final int FICHIER_FLAG = 2;


	/**
	 * Retourne la disponibilité d'une connection réseau
	 * 
	 * @param context Context
	 * @return reseauDisponible boolean vaut "true" si le réseau est disponible
	 */
	public static boolean reseauDisponible(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivity == null) {
			Log.w(LOG_TAG, "erreur, service indisponible");
		} else {
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null) {
				if (info.isConnected() && info.isAvailable()) {
					Log.i(LOG_TAG, "réseau disponible");
					return true;
				}
			}
		}

		Log.i(LOG_TAG, "réseau indisponible");
		return false;
	}


	/**
	 * Détermine l'aide contextuelle associé à l'écran actuellement affiché
	 * puis, lance l'aide contextuelle associée à l'écran (si c'est la première fois que l'utilisateur ouvre l'écran)
	 * 
	 * @param activityParent Activity activity associé à l'écran actuellement affiché
	 * @param String clefEcran clef associé à l'écran actuellement affiché
	 * @param int idAideEcran id associé à l'aide de l'écran actuellement affiché
	 */
	public static void determinerAideContextuelle(Activity activityParent, String clefEcran, int idAideEcran) {
		SharedPreferences.Editor editorFichierSharedPreferences = activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_AIDES_CONTEXTUELLES, 0).edit();
    	editorFichierSharedPreferences.putInt(Constantes.CLEF_AIDE_CONTEXTUELLE, idAideEcran);		// on spécifie dans le fichier de préférences gérant l'aide contextuelle l'écran actuellement affiché

    	// si l'utilisateur ouvre pour la première fois l'écran
        if ( activityParent.getSharedPreferences(Constantes.FICHIER_INFOS_AIDES_CONTEXTUELLES, 0).getBoolean(clefEcran, true) && clefEcran != null) {
        	// on spécifie dans le fichier de préférences gérant l'aide contextuelle que l'écran a déjà été ouvert
        	editorFichierSharedPreferences.putBoolean(clefEcran, false);

        	// lancement de l'aide contetuelle de l'écran
        	activityParent.startActivity( new Intent(activityParent, EcranAideActivity.class) );
        }
    	editorFichierSharedPreferences.commit();
	}

	
	/**
	 * met à jour le cookie de session
	 * @param List<String> cookiesHeader cookies contenuent dans le header d'une requete
	 */
	private static void setCookieSession(Map<String,List<String>> headers, Context context) {
		// s'il y a des cookies dans le header
		if (headers.containsKey("Set-Cookie")) {			
			List<String> cookiesHeader = headers.get("Set-Cookie");
			
			if( !cookiesHeader.isEmpty() ) {
				for (String cookie : cookiesHeader) {
					SharedPreferences.Editor editorFichierSharedPreferences = context.getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, 0).edit();

					// on stock dans un fichier de preference le cookie de session
					// il y a 2 cookies de session, session.pf et session.pf.sig 
					if(cookie.indexOf("session.pf=") != -1)
						editorFichierSharedPreferences.putString(Constantes.CLEF_COOKIE_SESSION, cookie.substring(0, cookie.indexOf(";") + 1));
					else
						editorFichierSharedPreferences.putString(Constantes.CLEF_COOKIE_SESSION_SIG, cookie.substring(0, cookie.indexOf(";") + 1));						
					
					editorFichierSharedPreferences.commit();
				}
			}
		}
	}

	
	/**
	 * récupère le cookie de session
	 * @param String clef déterminant si on renvoit le cookie session.pf ou le cookie session.pf.sig
	 * @return String cookie de session
	 */
	public static String getCookieSession(Context context, String clef) {		
		return context.getSharedPreferences(Constantes.FICHIER_APPLICATION_PARAMETRES, 0).getString(clef, "");
	}

	
	/**
	 * Envoie une requete http et retourne la réponse (format JSON)
	 * 
	 * @param requete String[] la requete http
	 * @param parametresRequete ArrayList paramêtres de la requête
	 * @return String reponse http au format JSON 
	 */
	public static String envoyerRequeteHttp(String[] requete, ArrayList<NameValuePair> parametresRequete, Context context) {    	    	
		String stringJSON = null;
		HttpURLConnection connexion = null;

		Log.i(LOG_TAG, "Requete http : " + requete[Constantes.METHODE_REQUETE] +" - "+ requete[Constantes.URL_REQUETE]);
		Log.i(LOG_TAG, "parametresRequete : " + parametresRequete);
		Log.i(LOG_TAG, "version : " + Constantes.VERSION_NAME);

		// gestion des sessions avec un cookie contenu dans chaque requete 
		if (CookieHandler.getDefault() == null) {
			CookieHandler.setDefault(new CookieManager());
		}
       
		try {			
			connexion = envoyerRequete(requete[Constantes.METHODE_REQUETE], requete[Constantes.URL_REQUETE], parametresRequete);
			
			// si la requête a été redirigé, on suit la redirection 
			if (connexion.getResponseCode() == 307) {
				Log.i(LOG_TAG, "Redirection requete http : " + connexion.getHeaderField("Location"));
				connexion = envoyerRequete(requete[Constantes.METHODE_REQUETE], connexion.getHeaderField("Location"), parametresRequete);
			}
			setCookieSession( connexion.getHeaderFields(), context );
						
			// récupération du retour Json
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connexion.getInputStream()));
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;

			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line + "\n");
			}
			stringJSON = stringBuilder.toString();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connexion.disconnect();
		}

		Log.i(LOG_TAG, "stringJSON : " + stringJSON);
		return stringJSON;
	}

	/**
	 * construie et envoie une requete http 
	 * 
	 * @param methode_requete String la methode de la requete (GET, POST, etc.)
	 * @param url_methode String l'URL du webservice
	 * @param parametresRequete ArrayList<NameValuePair> les parametres de la requete
	 * @return HttpURLConnection la connexion http
	 */
	private static HttpURLConnection envoyerRequete(String methode_requete, String url_methode, ArrayList<NameValuePair> parametresRequete) {
		HttpURLConnection connexion = null;
		URL url = null;

		try {
			// Ajout des paramametres de la requete dans stringBuilder
			StringBuilder stringBuilder = new StringBuilder();

			// système d'exploitation : Android
			stringBuilder.append(URLEncoder.encode(Constantes.CLEF_OS, HTTP.UTF_8));
			stringBuilder.append("=");
			stringBuilder.append(URLEncoder.encode(Constantes.OS, HTTP.UTF_8));
			stringBuilder.append("&");
			// version de SupCast (ex : 1.0)
			stringBuilder.append(URLEncoder.encode(Constantes.CLEF_VERSION, HTTP.UTF_8));
			stringBuilder.append("=");
			stringBuilder.append(URLEncoder.encode(Constantes.VERSION_NAME, HTTP.UTF_8));

			if (parametresRequete != null) {
				for (NameValuePair parametre : parametresRequete) {
					stringBuilder.append("&");
					stringBuilder.append(URLEncoder.encode(parametre.getName(), HTTP.UTF_8));
					stringBuilder.append("=");
					stringBuilder.append(URLEncoder.encode(parametre.getValue(), HTTP.UTF_8));
				}
			}

			// si la methode est GET ou DELETE, on met les parametres dans l'URL
			if ( methode_requete.equals("GET") || methode_requete.equals("DELETE") ) {               	
				url = new URL(url_methode +"?"+ stringBuilder.toString());
			} else {
				url = new URL(url_methode);
			}
			connexion = (HttpURLConnection) url.openConnection();
			connexion.setInstanceFollowRedirects(true);
			connexion.setDoInput(true);
			connexion.setRequestMethod( methode_requete );

			// si la methode n'est ni GET ni DELETE, on met les paramètres dans la requetes
			if ( !methode_requete.equals("GET") && !methode_requete.equals("DELETE") ) {
				connexion.setDoOutput(true);

				OutputStream outputStream = connexion.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, HTTP.UTF_8));
				writer.write( stringBuilder.toString() );
				writer.flush();
				writer.close();
				outputStream.close();
			}
			connexion.connect();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return connexion;
	}

	/** Envoie sur le serveur un message et des fichiers joints et retourne une réponse JSON
	 * 
	 * @param requete
	 * @param parametresRequete
	 * @param listFichierUri
	 * @param context
	 * @return
	 */   
	public static String envoyerMessagePiecesJointes(Context context, String[] requete, ArrayList<NameValuePair> parametresRequete, List<PieceJointe> piecesJointesList, int thumbnailTaille) {
		String stringJSON = null;
		HttpURLConnection connexion = null;

		Log.i(LOG_TAG, "Requete http : " + requete[Constantes.METHODE_REQUETE] +" - "+ requete[Constantes.URL_REQUETE]);
		Log.i(LOG_TAG, "parametresRequete : " + parametresRequete);
		Log.i(LOG_TAG, "version : " + Constantes.VERSION_NAME);

		// gestion des sessions avec un cookie contenu dans chaque requete 
		if (CookieHandler.getDefault() == null) {
			CookieHandler.setDefault(new CookieManager());
		}

		try {
			HttpEntity reqEntity = obtenirHttpEntityMessagePiecesJointes(context, parametresRequete, piecesJointesList, thumbnailTaille);
			connexion = etablirConnection(requete[Constantes.METHODE_REQUETE], requete[Constantes.URL_REQUETE], reqEntity);
			
			// si la requête a été redirigé, on suit la redirection 
			if (connexion.getResponseCode() == 307) {
				Log.i(LOG_TAG, "Redirection requete http : " + connexion.getHeaderField("Location"));
				connexion = etablirConnection(requete[Constantes.METHODE_REQUETE], connexion.getHeaderField("Location"), reqEntity);
			}
			setCookieSession( connexion.getHeaderFields(), context );
			
			// récupération du retour Json
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connexion.getInputStream()));
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;

			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line + "\n");
			}
			stringJSON = stringBuilder.toString();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connexion.disconnect();
		}

		Log.i(LOG_TAG, "stringJSON : " + stringJSON);
		return stringJSON;
	}
	
	/** construire HttpEntity à partir du contenu d'un message et des pièces jointes  
	 * 
	 * @param parametresRequete
	 * @param piecesJointesList
	 * @param context
	 * @return
	 * @throws IOException
	 */
	public static HttpEntity obtenirHttpEntityMessagePiecesJointes(Context context, List<NameValuePair> parametresRequete, List<PieceJointe> piecesJointesList, int thumbnailTaille) throws IOException {
		MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();

		entityBuilder.setMode( HttpMultipartMode.BROWSER_COMPATIBLE );
		entityBuilder.setCharset(Charset.forName(HTTP.UTF_8));
		entityBuilder.addTextBody(Constantes.CLEF_OS, Constantes.OS);					// système d'exploitation : Android
		entityBuilder.addTextBody(Constantes.CLEF_VERSION, Constantes.VERSION_NAME);	// version de SupCast (ex : 2.3)

		for(int i = 0; i < parametresRequete.size(); ++i) {
			entityBuilder.addTextBody(
				parametresRequete.get(i).getName(),
				parametresRequete.get(i).getValue(),
				ContentType.create(HTTP.PLAIN_TEXT_TYPE, HTTP.UTF_8)
			);	
		}
		
		PieceJointe pieceJointe;
		String filename;
		Bitmap bmp;
		boolean compresser;
		
		for(int i = 0; i < piecesJointesList.size(); ++i) {
			pieceJointe = piecesJointesList.get(i);
			filename = piecesJointesList.get(i).getNomOriginal();
			bmp = null;
			//Si la taille de la pièce jointe est supérieure à 2Mo alors on la compress sinon pas besoin
			compresser = pieceJointe.getTaille() > 2*1000000 ? true : false;
			
			if ( PieceJointeInformation.getInstance(context).extensionPhotoAutoriseDownload(pieceJointe.getExtension()) ) {
				//Si la pj est un thumbnail
				if(filename.contains(Constantes.CLEF_THUMBNAIL_NOM_FICHIER)) {
					bmp = Utiles.scaleBitmap( Utiles.fichierVersBitmap(pieceJointe.getCheminVersThumbnail()), thumbnailTaille );
				} else {
					bmp = Utiles.fichierVersBitmap(pieceJointe.getCheminVersThumbnail()); // on récupère l'image original
				}
				//On ajoute l'image compressée et on converti l'image en byte
				entityBuilder.addPart(															
					filename,
					new ByteArrayBody(Utiles.getImageCompressFromBitmapAndFormat(bmp, pieceJointe.getExtension(), compresser), filename)
				);				
				
			} else if ( PieceJointeInformation.getInstance(context).extensionVideoAutoriseDownload(pieceJointe.getExtension()) ) {
				//Si la pj est un thumbnail
				if(filename.contains(Constantes.CLEF_THUMBNAIL_NOM_FICHIER)) {
					bmp = Utiles.scaleBitmap(
						ThumbnailUtils.createVideoThumbnail(pieceJointe.getCheminVersThumbnail(), MediaStore.Images.Thumbnails.MINI_KIND),
						thumbnailTaille
					);
					//On ajoute l'image compressée et on converti l'image en byte
					entityBuilder.addPart(															
						filename,
						new ByteArrayBody(Utiles.getImageCompressFromBitmapAndFormat(bmp, "jpg", compresser), filename)
					);
				} else {
					//On ajoute la vidéo et on la converti en byte
					File file = new File(pieceJointe.getCheminVersThumbnail());
					byte[] bytes = new byte[(int) file.length()];
					try {
					    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
					    buf.read(bytes, 0, bytes.length);
					    buf.close();
					} catch (Exception e) {
					    e.printStackTrace();
					}
					entityBuilder.addPart(															
						filename,
						new ByteArrayBody(bytes, filename)
					);
				}
			} else {
				return null;
			}
		}

		return entityBuilder.build();    	
	}
	
	/**
	 * Fonction permettant de réduire la taille d'une image et de la renvoyer en byte
	 * @param src L'image source
	 * @param extension : l'extension de l'image pour utiliser la bonne compression
	 * @param compress : booléen permettant de lancer la compression
	 * @return
	 */
	private static byte[] getImageCompressFromBitmapAndFormat(Bitmap src, String extension, boolean compress){
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		
		int facteur = compress ? 80 : 100;
		
		if(extension.toLowerCase(Locale.FRANCE).trim().equals("png"))
			src.compress(Bitmap.CompressFormat.PNG, facteur, os);
		
		else if(extension.toLowerCase(Locale.FRANCE).trim().equals("jpg") || extension.toLowerCase(Locale.FRANCE).trim().equals("jpeg"))
			src.compress(Bitmap.CompressFormat.JPEG, facteur, os);
 
		return os.toByteArray();
	}
	
	/**
	 * Etablit une connexion
	 * @param methode_requete
	 * @param url_methode
	 * @param reqEntity
	 * @return
	 * @throws IOException
	 */
	private static HttpURLConnection etablirConnection(String methode_requete, String url_methode, HttpEntity reqEntity) throws IOException {
		URL url = new URL(url_methode);
		HttpURLConnection connexion = (HttpURLConnection) url.openConnection();
		connexion.setReadTimeout(10000);
		connexion.setConnectTimeout(15000);				
		connexion.setRequestMethod(methode_requete);

		connexion.setUseCaches(false);
		connexion.setDoInput(true);
		connexion.setDoOutput(true);
		connexion.setInstanceFollowRedirects(true);

		connexion.setRequestProperty("Connection", "Keep-Alive");
		connexion.addRequestProperty("Content-length", reqEntity.getContentLength()+"");
		connexion.addRequestProperty(reqEntity.getContentType().getName(), reqEntity.getContentType().getValue());

		OutputStream outputStream = connexion.getOutputStream();
		reqEntity.writeTo(outputStream);
		outputStream.close();
		
		connexion.connect();

		return connexion;
	}    
	
	
	/**
	 * Ouvre une fenêtre de dialogue simple (Titre / message / 1 bouton "Ok")
	 * 
	 * @param titre int ressource-id du titre
	 * @param message int ressource-id du message 
	 * @param context Context Context auquel rattacher la fenêtre de dialoge
	 */
	public static void afficherInfoAlertDialog(int titre, int message, Context context) {
		afficherInfoAlertDialog(titre, context.getString(message), context);
	}
	
	/**
	 * Ouvre une fenêtre de dialogue simple (Titre / message / 1 bouton "Ok")
	 * 
	 * @param titre int ressource-id du titre
	 * @param message String du message 
	 * @param context Context Context auquel rattacher la fenêtre de dialoge
	 */
	public static void afficherInfoAlertDialog(int titre, String message, Context context) {
		if(context == null)return;
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle(titre)
		.setMessage(message)
		.setPositiveButton(string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		})
		.show();
	}


	/**
	 * Supprimer récursivement un répertoire et son contenu
	 * 
	 * @param File fileOrDirectory objet File à supprimer (avec son éventuel contenu)
	 */
	public static void supprimerRepertoire(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				supprimerRepertoire(child);
			}
		}
		//Log.v(LOG_TAG, "delete " + fileOrDirectory.getName());
		fileOrDirectory.delete();
	}


	/**
	 * Dézippe l'archive d'une capsule contenant une arborescence de fichiers et répertoires. Si les fichiers / répertoires existent déjà, ils seront supprimés
	 * On ajoute à tout les documents .html (représentant les pages de la ressource) le javascript et le css permettant d'interagir avec les OA de la page (nb messages non lus, publier un message sur un OA, etc.)
	 * 
	 * @param FileInputStream fileInputStream F ileInputStream de l'archive a dézipper
	 * @param String localisation emplacement où enregistrer l'arborescence de fichiers / répertoires, si l'emplacement n'existe pas, il sera créé
	 * @param Sring url d'où l'on vient de télécharger la capsule
	 */
	public static void dezipperCapsule(FileInputStream fileInputStream, String localisation, String UrlCapsule) {
		Log.v(LOG_TAG, "unzipping to " + localisation);

		int size;
		byte[] buffer = new byte[1024];

		try {
			if ( !localisation.endsWith("/") ) {
				localisation += "/";
			}
			ZipInputStream zin = new ZipInputStream(new BufferedInputStream(fileInputStream, 4096));
			try {
				// dézippe le repertoire racine
				ZipEntry ze = zin.getNextEntry();
				String repertoireRacine = "";

				if ( ze.getName().equals(UrlCapsule.substring(UrlCapsule.lastIndexOf("/") + 1) + "/") ) {		// si le premier répertoire de l'archive correspond au nom de l'archive, on passe au répertoire suivant (= la Capsule contient un répertoire intérmediaire inutil)
					repertoireRacine = ze.getName();
					ze = zin.getNextEntry();
				}
				String path = localisation + ze.getName().substring(repertoireRacine.length());
				File rootFile = new File(path);
				rootFile.mkdirs();

				//Log.v(LOG_TAG, "Unzipping " + path);
				// dézippe le contenu du répertoire racine
				File unzipFile;
				while ((ze = zin.getNextEntry()) != null) {
					path = localisation + ze.getName().substring(repertoireRacine.length());
					unzipFile = new File(path);

					//Log.v(LOG_TAG, "Unzipping " + path);

					if ( ze.isDirectory() ) {
						if( !unzipFile.isDirectory() ) {
							unzipFile.mkdirs();
						}
					} else {
						// création du répertoire parent du fichier (s'il n'existe pas)
						File parentDir = unzipFile.getParentFile();
						if ( null != parentDir ) {
							if ( !parentDir.isDirectory() ) {
								parentDir.mkdirs();
							}
						}

						// dézippe le fichier courant
						FileOutputStream out = new FileOutputStream(unzipFile, false);
						BufferedOutputStream fout = new BufferedOutputStream(out, 4096);
						try {
							while ( (size = zin.read(buffer, 0, 1024)) != -1 ) {
								fout.write(buffer, 0, size);
							}

							zin.closeEntry();
						}
						finally {
							fout.flush();
							fout.close();
						}
					}
				}
			}
			finally {
				zin.close();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Enregistre un fichier
	 * 
	 * @param InputStream inputStream inputstream du fichier à enregistrer.
	 * @param String localisation emplacement où enregistrer le fichier. Exemple : racine/repertoire_fichier/nom_fichier
	 */
	public static void enregistrerFichier(InputStream inputStream, String localisation) {
		File file = new File(localisation);
		FileOutputStream fileOutputStream;
		int size;
		byte[] buffer = new byte[1024];

		try {
			fileOutputStream = new FileOutputStream(file);

			while ( (size = inputStream.read(buffer, 0, 1024)) != -1 ) {
				fileOutputStream.write(buffer, 0, size);
			}
			inputStream.close();
			fileOutputStream.flush();
			fileOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Enregistre un fichier
	 * 
	 * @param InputStream inputStream inputstream du fichier à enregistrer.
	 * @param String localisation emplacement où enregistrer le fichier. Exemple : racine/repertoire_fichier/nom_fichier
	 */
	public static File enregistrerFichier(String cheminDossier, String nomFichier, byte[] donnee) {
		OutputStream output = null;
		File storageFile = null;
		try {
			storageFile = new File(cheminDossier, nomFichier);
			output = new FileOutputStream (storageFile);
			output.write(donnee);
			output.flush();
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storageFile;
	}
	
	/**
	 * Télécharge un fichier à partir de son URL
	 * 
	 * @param String urlFichier url du fichier à télécharger.
	 * @param String localisation chemin ou l'on stockera le fichier. Exemple : racine/repertoire_avatar/nom_avatar
	 */
	public static void telechargerFichier(String urlFichier, String localisation) {
		InputStream input = null;
		OutputStream output = null;

		try {
			URL url = new URL ( urlFichier );
			input = url.openStream();

			File storageFile = new File(localisation);
			output = new FileOutputStream (storageFile);

			byte[] buffer = new byte[1024];
			int bytesRead = 0;
			while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
				output.write(buffer, 0, bytesRead);
			}

			output.close();
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Copie le contenu d'un fichier vers un autre
	 * @param src le fichier source
	 * @param dst la destination
	 * @throws IOException 
	 */
	public static void copieFichierVersFichier(File src, File dst) throws IOException {
	    InputStream in = new FileInputStream(src);
	    OutputStream out = new FileOutputStream(dst);

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}
	
	
	public static byte[] telechargerPj(PieceJointe pj, Context context){
		return recuperePieceJointeServeurByte(Constantes.URL_WEB_SERVICE_RECUPERER_PJ, pj, FICHIER_FLAG, context);
	}
	
	
	public static byte[] telechargerThumbnail(PieceJointe pj, Context context){
		return recuperePieceJointeServeurByte(Constantes.URL_WEB_SERVICE_RECUPERER_PJ, pj, THUMBNAIL_FLAG, context);
	}

	
	public static Bitmap fichierVersBitmap(String chemin){
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		options.inSampleSize = 2;
		Bitmap bmp;
		try{
			bmp = BitmapFactory.decodeFile(chemin, options);
		}catch(Exception e){
			options.inSampleSize = 4;
			bmp = BitmapFactory.decodeFile(chemin, options);
		}
		
		return bmp;
	}
	
	
	public static byte[] bitmapToByte(Bitmap bmp) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(bmp.getByteCount());
		bmp.copyPixelsToBuffer(buffer);
		return buffer.array();
	}
	
	
	public static Bitmap scaleBitmap(Bitmap source, int width){
		int bmWidth=source.getWidth();
		int bmHeight=source.getHeight();
		
		int new_height = (int) Math.floor((double) bmHeight *( (double) width / (double) bmWidth));
		
		return Bitmap.createScaledBitmap(source,width,new_height, true);
	}
	

	private static byte[] recuperePieceJointeServeurByte(String[] requete, PieceJointe pj, int mode, Context context) {		
		HttpURLConnection connexion = null;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		ArrayList<NameValuePair> parametresRequete = new ArrayList<NameValuePair>();

		if(mode == THUMBNAIL_FLAG){
			parametresRequete.add( new BasicNameValuePair(				// capsules pour lesquelles on veut récupérer les messages
				Constantes.CLEF_NOM_SERVEUR_PJ,
				pj.getNomServeur()
			));
			parametresRequete.add( new BasicNameValuePair(				// capsules pour lesquelles on veut récupérer les messages
				Constantes.CLEF_TYPE_PJ,
				"thumbnail"
			));
		}else if(mode == FICHIER_FLAG){
			parametresRequete.add( new BasicNameValuePair(				// capsules pour lesquelles on veut récupérer les messages
				Constantes.CLEF_NOM_SERVEUR_PJ,
				pj.getNomServeur()
			));
		}
		
		Log.i(LOG_TAG, "Requete http : " + requete[Constantes.METHODE_REQUETE] +" - "+ requete[Constantes.URL_REQUETE]);
		Log.i(LOG_TAG, "parametresRequete : " + parametresRequete);
		Log.i(LOG_TAG, "version : " + Constantes.VERSION_NAME);

		// gestion des sessions avec un cookie contenu dans chaque requete 
		if (CookieHandler.getDefault() == null) {
			CookieHandler.setDefault(new CookieManager());
		}

		try {
			connexion = envoyerRequete(requete[Constantes.METHODE_REQUETE], requete[Constantes.URL_REQUETE], parametresRequete);

			// si la requête a été redirigé, on suit la redirection 
			if (connexion.getResponseCode() == 307) {
				Log.i(LOG_TAG, "Redirection requete http : " + connexion.getHeaderField("Location"));
				connexion = envoyerRequete(requete[Constantes.METHODE_REQUETE], connexion.getHeaderField("Location"), parametresRequete);
			}
			setCookieSession( connexion.getHeaderFields(), context );

			byte[] tmpbuffer = new byte[1024];
			int bytesRead = 0;
			InputStream input = connexion.getInputStream();

			while ((bytesRead = input.read(tmpbuffer, 0, tmpbuffer.length)) >= 0) {
				buffer.write(tmpbuffer, 0, bytesRead);
			}

			buffer.close();
			input.close();			

			return  buffer.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connexion.disconnect();
		}

		return null;
	}

	/**
	 * Retourne une liste d'urls à partir d'un texte
	 * @param String text le text duquel on veut extraire les urls
	 * @return ArrayList<String> tableau contenant les urls
	 */
	public static ArrayList<String> obtenirLiens(String text) {
		ArrayList<String> liens = new ArrayList<String>();

		String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		String lien;

		while(matcher.find()) {
			lien = matcher.group();
			if (lien.startsWith("(") && lien.endsWith(")")) {
				lien = lien.substring(1, lien.length() - 1);
			}
			liens.add(lien);
		}

		return liens;
	}


	/**
	 * Vérifie le status du fichier téléchargé via DownloadManager
	 * 
	 * @param DownloadManager downloadManager DownloadManager utilisé
	 * @param long downloadReference id du téléchargement géré par le DownloadManger
	 * @return int status
	 */
	public static int verifierStatusDownloadManager(DownloadManager downloadManager, long downloadReference) {
		//String statusText = "";
		//String reasonText = "";
		int status = -1;

		Query myDownloadQuery = new Query();
		//set the query filter to our previously Enqueued download
		myDownloadQuery.setFilterById(downloadReference);

		//Query the download manager about downloads that have been requested.
		Cursor cursor = downloadManager.query(myDownloadQuery);
		if(cursor.moveToFirst()) {
			//column for status
			int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
			status = cursor.getInt(columnIndex);
			//column for reason code if the download failed or paused
			//int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
			//int reason = cursor.getInt(columnReason);
			//get the download filename
			//int filenameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
			//String filename = cursor.getString(filenameIndex);

			/*switch(status) {
				case DownloadManager.STATUS_FAILED:
					statusText = "STATUS_FAILED";
					switch(reason) {
						case DownloadManager.ERROR_CANNOT_RESUME:
							reasonText = "ERROR_CANNOT_RESUME";
							break;
						case DownloadManager.ERROR_DEVICE_NOT_FOUND:
							reasonText = "ERROR_DEVICE_NOT_FOUND";
							break;
						case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
							reasonText = "ERROR_FILE_ALREADY_EXISTS";
							break;
						case DownloadManager.ERROR_FILE_ERROR:
							reasonText = "ERROR_FILE_ERROR";
							break;
						case DownloadManager.ERROR_HTTP_DATA_ERROR:
							reasonText = "ERROR_HTTP_DATA_ERROR";
							break;
						case DownloadManager.ERROR_INSUFFICIENT_SPACE:
							reasonText = "ERROR_INSUFFICIENT_SPACE";
							break;
						case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
							reasonText = "ERROR_TOO_MANY_REDIRECTS";
							break;
						case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
							reasonText = "ERROR_UNHANDLED_HTTP_CODE";
							break;
						case DownloadManager.ERROR_UNKNOWN:
							reasonText = "ERROR_UNKNOWN";
							break;
					}
					break;
				case DownloadManager.STATUS_PAUSED:
					statusText = "STATUS_PAUSED";
					switch(reason) {
						case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
							reasonText = "PAUSED_QUEUED_FOR_WIFI";
							break;
						case DownloadManager.PAUSED_UNKNOWN:
							reasonText = "PAUSED_UNKNOWN";
							break;
						case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
							reasonText = "PAUSED_WAITING_FOR_NETWORK";
							break;
						case DownloadManager.PAUSED_WAITING_TO_RETRY:
							reasonText = "PAUSED_WAITING_TO_RETRY";
							break;
					}
					break;
				case DownloadManager.STATUS_PENDING:
					statusText = "STATUS_PENDING";
					break;
				case DownloadManager.STATUS_RUNNING:
					statusText = "STATUS_RUNNING";
					break;
				case DownloadManager.STATUS_SUCCESSFUL:
					statusText = "STATUS_SUCCESSFUL";
					reasonText = "Filename:\n" + filename;
					break;
			}*/
		}
		cursor.close();
		return status;
	}

	/**
	 * Récupère les langues disponibles de l'application
	 * 
	 * @param Context context
	 * @return String la liste des langues disponibles
	 */
	public static String getListeIdLangues(Context context) {
		// Recuperation des langues des messages à afficher
		String languesAffichees = "";
		boolean isFirst = true;
		String[] tabIdLangues = context.getResources().getStringArray(R.array.langue_values_id);
		for(int i=0;i<tabIdLangues.length;i++) {
			if(isFirst) {
				languesAffichees += tabIdLangues[i];
				isFirst = false;
			} else
				languesAffichees += "," + tabIdLangues[i];
		}

		return languesAffichees;
	}

	/**
	 * Récupère les langues de l'utilisateur
	 * 
	 * @param Context context
	 * @return String la liste des langues de l'utilisateur
	 */
	public static String StringListeLanguesUtilisateur(Context context) {
		String languesAffichees = "";
		SharedPreferences fichierSharedPreferences = context.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);

		if(fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_PRINCIPALE, null) == null) { // Non connecté
			String[] tabIdLangues = context.getResources().getStringArray(R.array.langue_values_id);

			languesAffichees += tabIdLangues[0];
			for(int i=1;i<tabIdLangues.length;i++) {
				languesAffichees += "," + tabIdLangues[i];
			}
		} 
		else { // Connecté
			languesAffichees += Utiles.idLangueAvecCode(fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_PRINCIPALE,"en"), context);

			String[] tabCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);

			for(int i=0;i<tabCodesLangues.length;i++) {
				if(fichierSharedPreferences.getBoolean(Constantes.CLEF_AUTRES_LANGUES + "_" + tabCodesLangues[i], false)) {
					languesAffichees += "," + Utiles.idLangueAvecCode(tabCodesLangues[i], context);
				}
			}
		}

		return languesAffichees;
	}

	/**
	 * Récupère le nom de la langue à partir de son id
	 * 
	 * @param String id l'id de la langue souhaitée
	 * @param Context context
	 * @return String le nom de la langue
	 */
	public static String nomLangueAvecId(String id, Context context) {
		String langueString = "";
		String[] tabIdLangues = context.getResources().getStringArray(R.array.langue_values_id);
		String[] tabNomsLangues = context.getResources().getStringArray(R.array.langue_values_string);

		for(int i = 0; i<tabIdLangues.length; i++) {
			if(tabIdLangues[i].equals(id)) {
				langueString = tabNomsLangues[i];
				break;
			}
		}

		return langueString;
	}

	/**
	 * Récupère le vrai nom de la langue à partir de son id
	 * 
	 * @param String id l'id de la langue souhaitée
	 * @param Context context
	 * @return String le nom de la langue
	 */
	public static String nomVraiLangueAvecId(String id, Context context) {
		String langueString = "";
		String[] tabIdLangues = context.getResources().getStringArray(R.array.langue_values_id);
		String[] tabNomsLangues = context.getResources().getStringArray(R.array.langue_true_values_string);

		for(int i = 0; i<tabIdLangues.length; i++) {
			if(tabIdLangues[i].equals(id)) {
				langueString = tabNomsLangues[i];
				break;
			}
		}

		return langueString;
	}

	/**
	 * Récupère le nom de la langue à partir de son code
	 * 
	 * @param String code le code de la langue souhaitée
	 * @param Context context
	 * @return String le nom de la langue
	 */
	public static String nomLangueAvecCode(String code, Context context) {
		int i = 0;
		String[] tabCodeLangues = context.getResources().getStringArray(R.array.langue_values_code);
		String[] tabNomsLangues = context.getResources().getStringArray(R.array.langue_values_string);

		for(i = 0; i<tabCodeLangues.length; i++) {
			if(tabCodeLangues[i].equals(code)) {
				break;
			}
		}

		return tabNomsLangues[i];
	}

	/**
	 * Récupère le vrai nom de la langue à partir de son code
	 * 
	 * @param String code le code de la langue souhaitée
	 * @param Context context
	 * @return String le nom de la langue
	 */
	public static String nomVraiLangueAvecCode(String code, Context context) {
		int i = 0;
		String[] tabCodeLangues = context.getResources().getStringArray(R.array.langue_values_code);
		String[] tabTrueNomsLangues = context.getResources().getStringArray(R.array.langue_true_values_string);

		for(i = 0; i<tabCodeLangues.length; i++) {
			if(tabCodeLangues[i].equals(code)) {
				break;
			}
		}

		return tabTrueNomsLangues[i];
	}

	/**
	 * Récupère le code de la langue à partir de son id
	 * 
	 * @param String id l'id de la langue souhaitée
	 * @param Context context
	 * @return String le code de la langue
	 */
	public static String codeLangueAvecId(String id, Context context) {
		int i = 0;
		String[] tabIdLangues = context.getResources().getStringArray(R.array.langue_values_id);
		String[] tabCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);

		for(i = 0; i<tabIdLangues.length; i++) {
			if(tabIdLangues[i].equals(id)) {
				break;
			}
		}

		return tabCodesLangues[i];
	}

	/**
	 * Récupère le code de la langue à partir de son nom
	 * 
	 * @param String nom de la langue souhaitée
	 * @param Context context
	 * @return String le code de la langue
	 */
	public static String codeLangueAvecNom(String nom, Context context) {
		int i = 0;
		String[] tabNomLangues = context.getResources().getStringArray(R.array.langue_values_string);
		String[] tabCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);

		for(i = 0; i<tabNomLangues.length; i++) {
			if(tabNomLangues[i].equals(nom)) {
				break;
			}
		}

		return tabCodesLangues[i];
	}

	/**
	 * Récupère le code de la langue à partir de son vrai nom
	 * 
	 * @param String vrai nom de la langue souhaitée
	 * @param Context context
	 * @return String le code de la langue
	 */
	public static String codeLangueAvecVraiNom(String nom, Context context) {
		int i = 0;
		String[] tabVraiNomLangues = context.getResources().getStringArray(R.array.langue_true_values_string);
		String[] tabCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);

		for(i = 0; i<tabVraiNomLangues.length; i++) {
			if(tabVraiNomLangues[i].equals(nom)) {
				break;
			}
		}

		return tabCodesLangues[i];
	}

	/**
	 * Récupère l'id de la langue à partir de son code
	 * 
	 * @param String code le code de la langue souhaitée
	 * @param Context context
	 * @return String l'id de la langue
	 */
	public static String idLangueAvecCode(String code, Context context) {
		int i = 0;
		String[] tabIdLangues = context.getResources().getStringArray(R.array.langue_values_id);
		String[] tabCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);

		for(i = 0; i<tabCodesLangues.length; i++) {
			if(tabCodesLangues[i].equals(code)) {
				break;
			}
		}

		return tabIdLangues[i];
	}

	/**
	 * Récupère l'index de la langue du tableau des langues à partir de son code
	 * 
	 * @param String code le code de la langue souhaitée
	 * @param Context context
	 * @return int index de la langue
	 */
	public static int tabIndexLangueAvecCode(String code, Context context) {
		int i = 0;
		String[] tabCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);

		for(i = 0; i<tabCodesLangues.length; i++) {
			if(tabCodesLangues[i].equals(code)) {
				break;
			}
		}

		return i;
	}

	/**
	 * Récupère l'indice de la langue à partir de son index de tableau
	 * 
	 * @param int index l'index de tableau de la langue
	 * @param Context context
	 * @return int id de la langue
	 */
	public static int idLangueAvecTabIndex(int index, Context context) {
		String[] tabIdsLangues = context.getResources().getStringArray(R.array.langue_values_id);
		return Integer.parseInt(tabIdsLangues[index]);
	}

	/**
	 * Récupère l'id de la langue principale de l'utilisateur
	 *
	 * @param Context context
	 * @return int id de la langue
	 */
	public static int idLanguePrincipale(Context context) {
		SharedPreferences fichierSharedPreferences = context.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);
		return Integer.parseInt(Utiles.idLangueAvecCode(fichierSharedPreferences.getString(Constantes.CLEF_LANGUE_PRINCIPALE, "en"), context));
	}

	/**
	 * Renvoi une liste d'id des autres langues de l'utilisateur
	 *
	 * @param Context context
	 * @return ArrayList<String> liste d'id des autres langues
	 */
	public static ArrayList<String> listeIdAutresLangues(Context context) {
		ArrayList<String> listeIdAutresLangues = new ArrayList<String>();
		SharedPreferences fichierSharedPreferences = context.getSharedPreferences(Constantes.FICHIER_INFOS_UTILISATEUR_CONNECTE, 0);
		String[] listeCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);

		for(int i=0; i< listeCodesLangues.length; i++) {
			if(fichierSharedPreferences.getBoolean(Constantes.CLEF_AUTRES_LANGUES + "_" + listeCodesLangues[i], false)) {
				listeIdAutresLangues.add(Utiles.idLangueAvecCode(listeCodesLangues[i], context));
			}
		}

		return listeIdAutresLangues;
	}

	/**
	 * Renvoi un tableau contenant les R id du tableau passé en parametre
	 *
	 * @param Context context
	 * @param int id
	 * @return int[] tableau des R id
	 */
	public static int[] idArrayWithRIdArray(Context context, int id) {
		TypedArray ar = context.getResources().obtainTypedArray(id);
		int len = ar.length(); 
		int[] arrayId = new int[len];
		for (int i = 0; i < len; i++)
			arrayId[i] = ar.getResourceId(i, 0);
		ar.recycle();
		return arrayId;
	}

	/**
	 * Renvoi un tableau contenant les R id des icones des langues
	 *
	 * @param Context context
	 * @return int[] tableau des R id des icones des langues
	 */
	public static int[] idArrayIconesLangues(Context context) {
		String[] tabCodesLangues = context.getResources().getStringArray(R.array.langue_values_code);
		int[] arrayId = new int[tabCodesLangues.length];

		for(int i = 0; i<tabCodesLangues.length; i++) {
			arrayId[i] = context.getResources().getIdentifier("drawable/icone_drapeau_" + tabCodesLangues[i], null, context.getPackageName());
		}

		return arrayId;
	}
}
