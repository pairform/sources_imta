angular.module('ui.thumbnail', [])

.provider('ThumbnailService', function() {

  this.defaults = {
    width: 100,
    height: 100
  };

  this.$get = function($q) {
    var defaults = this.defaults;

    return {

      generate: function generate(src, opts) {
        var deferred = $q.defer();

        opts = opts || {};

        this.load(src, opts).loaded.then(
          function success(canvas) {
            if (opts.returnType === 'blob') {
              if (typeof canvas.toBlob !== 'function') {
                return deferred.reject('Your browser doesn\'t support canvas.toBlob yet. Please polyfill it.');
              }

              try {
                canvas.toBlob(function(blob) {
                  // one may use blob-util to get a URL
                  deferred.resolve(blob);
                }, opts.type, opts.encoderOptions);
              } catch (ex) {
                deferred.reject(ex);
              }

            } else {
              if (typeof canvas.toDataURL !== 'function') {
                return deferred.reject('Your browser doesn\'t support canvas.toDataURL yet. Please polyfill it.');
              }

              try {
                var base64 = canvas.toDataURL(opts.type, opts.encoderOptions);
                deferred.resolve(base64);
              } catch (ex) {
                deferred.reject(ex);
              }
            }
          }
        );

        return deferred.promise;
      },

      load: function load(src, opts) {
        var canvas = this.createCanvas(opts);

        return {
          // creation is done
          created: $q.when(canvas),
          // wait for it
          loaded: this.draw(canvas, src, opts)
        };
      },

      draw: function draw(canvas, src, opts) {
        var deferred = $q.defer();

        var ctx = canvas.getContext('2d');

        // it seems that we cannot reuse image instance for drawing
        var item;

        //Default is an image
        if (typeof opts === "undefined" || typeof opts.media_type === "undefined" || opts.media_type === "image") {
          item = new Image();
          item.onload = onMediaLoad;
          item.src = src;
        }
        else if(opts.media_type === "video"){
          item = document.createElement("video"); 
          item.src = src;
          item.addEventListener("loadeddata", onMediaLoad);
          item.currentTime = 4; // video offset
          item.load();
        }
        else {
          var data = '<svg xmlns="http://www.w3.org/2000/svg" width="180" height="212">' +
            '<foreignObject width="100%" height="100%">' +
              '<div xmlns="http://www.w3.org/1999/xhtml" id="pj-file" style="border: 6px #AFAFAF solid;' +
                'font-size: 66px;' +
                'background-color: #FFFFFF;' +
                'border-radius: 13px;' +
                'border-top-left-radius: 46px;' +
                'width: 168px;' +
                'height: 200px;' +
                'cursor: pointer;' +
                'text-align: center;' +
                'line-height: 200px;' +
                'font-family:Helvetica;' +
                'color: #585858;' +
                'box-shadow: 206px 206px 0px -220px #EAEAEA inset;' +
                'text-shadow:0 -2px 8px white;' +
                'overflow:hidden;">' +
                '.' + opts.media_extension +
              '</div>' +
            '</foreignObject>' +
            '</svg>';

            item = new Image();
            item.onload = function() {
              canvas.width = 180;
              canvas.height = 212;
              ctx.drawImage(item, 0, 0);
              // loading is done
              deferred.resolve(canvas);
            };
            item.src = "data:image/svg+xml;base64," + btoa(data);
        }

        function onMediaLoad() {
          var keep_ratio = defaults.keep_ratio;
          if (keep_ratio) {
            // Keep the original dimension, constrained to the wanted height or width
            if (keep_ratio == "height") {
              var width = (this.videoWidth || this.width) * canvas.height / (this.videoHeight || this.height),
                  x = 0;
              if (canvas.width > width) {
                x = (canvas.width - width) / 2;
              }
              else{
                canvas.width = width;
              }
              ctx.drawImage(this, x, 0, width, canvas.height);
            } else if (keep_ratio == "width") {

              var height = (this.videoHeight || this.height) * canvas.width / (this.videoWidth || this.width),
                  y = 0;
              if (canvas.height > height) {
                y = (canvas.height - height) / 2;
              }
              else{
                canvas.height = height;
              }
              ctx.drawImage(this, 0, y, canvas.width, height);
            }
          }
          else
            // designated canvas dimensions should have been set
            ctx.drawImage(this, 0, 0, canvas.width, canvas.height);

          // loading is done
          deferred.resolve(canvas);
        };


        return deferred.promise;
      },

      createCanvas: function createCanvas(opts) {
        var canvas = angular.element('<canvas></canvas>')[0];

        return this.updateCanvas(canvas, opts);
      },

      updateCanvas: function updateCanvas(canvas, opts) {
        opts = opts || {};
        var w = Number(opts.width) || defaults.width;
        var h = Number(opts.height) || defaults.height;

        canvas.width = w;
        canvas.height = h;

        return canvas;
      }

    };

  }; // $get

})

.directive('uiThumbnail', function(ThumbnailService) {

  return {

    restrict: 'E',

    scope: {
      src: '=',
      opts: '='
    },

    link: function link(scope, el, attrs) {
      var promises = ThumbnailService.load(scope.src, scope.opts);

      promises.created.then(
        function created(canvas) {
          // can be appended at this point
          el.append(canvas);
        }
      );

      promises.loaded.then(
        function loaded(canvas) {
          // create watches
          scope.$watch('src', function(newSrc) {
            ThumbnailService.draw(canvas, newSrc);
          });
          scope.$watchCollection('opts', function(newOpts) {
            ThumbnailService.updateCanvas(canvas, newOpts);
            // need to redraw
            ThumbnailService.draw(canvas, scope.src);
          });
        }
      );
    }

  };

})

;