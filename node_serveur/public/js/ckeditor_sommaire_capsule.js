(function (document) {
	getJSON();
	function getJSON() {
		var req = new XMLHttpRequest();
		req.open('GET', '../outline.json', true);
		req.onreadystatechange = function (aEvt) {
			if (req.readyState == 4) {
				if(req.status == 200){
					var json = JSON.parse(req.responseText)

					// s'il y a plus d'une page
					if(json.liste_pages.length > 1) {
						generateTOC(json.liste_pages);
						generateNavigationLinks(json.liste_pages);
					} else {
						var menu = document.querySelector("section.ck_editor #toc");
						menu.style = "display:none;";
						menu = document.querySelector("section.ck_editor .nav-links");
						menu.style = "display:none;";
						menu = document.querySelector("section.ck_editor #toc~.content");
						menu.style = "width:auto";
						
					}
				}
				else{
					console.log("Erreur pendant le chargement de la page.\n");
				}
			};
		}
		req.send(null);
	}
	function generateTOC(liste_pages) {
		var url = document.location.pathname;
		var menu = document.querySelector("#toc nav ul");

		for (var i = 0; i < liste_pages.length; i++) {
			var page = liste_pages[i];
			var li = document.createElement("li"),
			     a = document.createElement("a");
			a.setAttribute('href', page.id +".html");
			a.innerText = page.titre;
			//Si c'est la page active
			if (url.indexOf(page.id) > 0) 
				a.className = "page_active";
			li.appendChild(a);
			menu.appendChild(li);
		}
	}
	function generateNavigationLinks(liste_pages) {
		var url = document.location.pathname;
		var index = liste_pages.findIndex(function (element) {
			return url.indexOf(element.id) > 0;
		})
		//Si on est minimum à la page 1
		if (index > 0 && index < liste_pages.length -1) {
			fillNavigationLink("#nav-link-previous", liste_pages[index - 1])
			fillNavigationLink("#nav-link-next", liste_pages[index + 1])
		}
		else {
			//Si on est a la page 1
			if (index == 0) {
				hideNavigationLink("#nav-link-previous");
				fillNavigationLink("#nav-link-next", liste_pages[index + 1])
			}
			//Si on est à la fin
			if (index == liste_pages.length -1){
				hideNavigationLink("#nav-link-next");
				fillNavigationLink("#nav-link-previous", liste_pages[index - 1])
			}
		} 
	}
	function fillNavigationLink(selector, page) {		
		var menu = document.querySelector(selector);

		menu.setAttribute("href", page.id + '.html');
		menu.innerText = page.titre;
	}
	function hideNavigationLink(selector) {
		var menu = document.querySelector(selector);
		menu.style = "display:none;";
	}
	
	if (!Array.prototype.findIndex) {
	  Object.defineProperty(Array.prototype, 'findIndex', {
	    value: function(predicate) {
	      'use strict';
	      if (this == null) {
	        throw new TypeError('Array.prototype.findIndex called on null or undefined');
	      }
	      if (typeof predicate !== 'function') {
	        throw new TypeError('predicate must be a function');
	      }
	      var list = Object(this);
	      var length = list.length >>> 0;
	      var thisArg = arguments[1];
	      var value;

	      for (var i = 0; i < length; i++) {
	        value = list[i];
	        if (predicate.call(thisArg, value, i, list)) {
	          return i;
	        }
	      }
	      return -1;
	    },
	    enumerable: false,
	    configurable: false,
	    writable: false
	  });
	}
})(document);
