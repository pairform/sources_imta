if (!library)
		   var library = {};

		library.json = {
		   replacer: function(match, pIndent, pKey, pVal, pEnd) {
		      var key = '<span class=json-key>';
		      var val = '<span class=json-value>';
		      var str = '<span class=json-string>';
		      var r = pIndent || '';
		      if (pKey)
		         r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
		      if (pVal)
		         r = r + (pVal[0] == '"' ? str : val) + pVal + '</span>';
		      return r + (pEnd || '');
		      },
		   prettyPrint: function(obj) {
		      var jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
		      return JSON.stringify(obj, null, 3)
		         .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
		         .replace(/</g, '&lt;').replace(/>/g, '&gt;')
		         .replace(jsonLine, library.json.replacer);
		      }
		   };
		var speed_test_app = angular.module('speed_test', [], function($httpProvider) {
  			// Use x-www-form-urlencoded Content-Type
		  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
		 
		  /**
		   * The workhorse; converts an object to x-www-form-urlencoded serialization.
		   * @param {Object} obj
		   * @return {String}
		   */ 
		  var param = function(obj) {
		    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
		      
		    for(name in obj) {
		      value = obj[name];
		        
		      if(value instanceof Array) {
		        for(i=0; i<value.length; ++i) {
		          subValue = value[i];
		          fullSubName = name + '[' + i + ']';
		          innerObj = {};
		          innerObj[fullSubName] = subValue;
		          query += param(innerObj) + '&';
		        }
		      }
		      else if(value instanceof Object) {
		        for(subName in value) {
		          subValue = value[subName];
		          fullSubName = name + '[' + subName + ']';
		          innerObj = {};
		          innerObj[fullSubName] = subValue;
		          query += param(innerObj) + '&';
		        }
		      }
		      else if(value !== undefined && value !== null)
		        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
		    }
		      
		    return query.length ? query.substr(0, query.length - 1) : query;
		  };
		 
		  // Override $http service's default transformRequest
		  $httpProvider.defaults.transformRequest = [function(data) {
		    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
		  }];
		});

		speed_test_app.controller("settings", function ($scope, $http, $timeout, $sce) {
			$scope.interval = 4000;
			$scope.num = 5;
			$scope.param = "";	
			$scope.ws = "";
			$scope.response = "";
			$scope.should_log = false;
			$scope.os = "Web";
			$scope.method = "POST";
			$scope.app_version = "1";
			$scope.idle = true;
			$scope.auth = true;
			// Ne pas utiliser document.location.origin ou autre, parce qu'il n'y a pas l'éventuel chemin intermédiaire (/node par ex)
			$scope.base_url = PF.url_serveur_node || document.location.href.replace("/utils/speed_test","");

			var timer;

			//Récuperation des données externalisées
			$http.get(PF.url_serveur_node + "/private/json/platform_data.json").success(function(data, status) {
				$scope.platforms = data;
				$scope.platform = $scope.platforms[0];
				// $scope.ws_list = ;

              }).error(function(data, status) {
                console.log(data || "Request failed");
            });    


			try{
				var old_scope = JSON.parse(localStorage.speed_test_app_settings);

				for(var prop in old_scope) {
					$scope[prop] = old_scope[prop];
				}

				console.log($scope.platform);
			}
			catch(e){
				toastr.info('Pas de données enregistrées précédemment');
			}

			$scope.handle_auth = function () {
				
				if ($scope.auth) {
					$http.post($scope.base_url + $scope.platform.url + "compte/loginCompte.php",{"username":"Admin"}).success(function(data, status) {
						$scope.platforms = data;
						$scope.platform = $scope.platforms[0];

		              }).error(function(data, status) {
		                console.log(data || "Request failed");
		            });   
				}
				else{

				}
			}
			$scope.update_param = function () {
				var current_ws = $scope.ws;

				var ws_param = $scope.platform.ws_list[current_ws];

				if (typeof(ws_param) != "undefined" && ws_param != {} && ws_param != "")
					$scope.param = JSON.stringify(ws_param);
			}

			$scope.effacer = function () {
				delete(localStorage.speed_test_app_results);
				var scope = angular.element($("[ng-controller=results]")).scope();
				$timeout(function() {
					scope.$apply(function(){
	        			scope.med_arr = [];
	        			scope.med = 0;
	        			scope.results = [];
	        		});
				});

			}
			$scope.stopper = function (){
				//Unset du timer s'il était mis
				if (typeof(timer) != "undefined")
					clearInterval(timer);

				$scope.idle = true;
			}
			$scope.tester = function () {
				//Unset du timer s'il était mis
				if (typeof(timer) != "undefined")
					clearInterval(timer);


				var number_of_times_remaining = $scope.num;

	            //Sauvegarde du scope
	            localStorage.speed_test_app_settings = JSON.stringify({"interval" : $scope.interval, "num" : $scope.num, "platform" : $scope.platform, "param" : $scope.param, "ws" : $scope.ws, "should_log" : $scope.should_log, "os" : $scope.os, "app_version" : $scope.app_version, "auth" : $scope.auth});

	            var param_obj = $scope.os !== "Aucun" ? {"os" : $scope.os.toLowerCase(), "version" : $scope.app_version} : {};

	            if ($scope.param != "")
	            {
	            	try{
	            		params = JSON.parse($scope.param);

	            		for(var prop in params) {
							param_obj[prop] = params[prop];
						}

	            	} catch(e){						
	            		toastr.error("Le paramètre est mal formatté (JSON attendu)");
	            		return;
	            	}
	            }

	            if (!$scope.ws){
	            	toastr.error("Vous devez spécifier obligatoirement un webservice.");
	            	return;
	            }

				$scope.idle = false;

	            timer = setInterval(function () {
	            	var before_timestamp = new Date().getTime();
	            	var request;

	            	switch($scope.method){
	            		case "POST":
	            			request = $http.post($scope.base_url + $scope.platform.url + $scope.ws, param_obj);
	            			break;
	            		case "PUT":
	            			request = $http.put($scope.base_url + $scope.platform.url + $scope.ws, param_obj);
	            			break;
	            		case "GET":
	            			request = $http({
	            				url : $scope.base_url + $scope.platform.url + $scope.ws,
	            				method : "GET",
	            				params : param_obj
	            			});
	            			break;
	            		case "DELETE":
	            			request = $http({
	            				url : $scope.base_url + $scope.platform.url + $scope.ws,
	            				method : "DELETE",
	            				params : param_obj
	            			});
	            			break;
	            		default:
	            			request = $http.post($scope.base_url + $scope.platform.url + $scope.ws, param_obj);
	            			break;

	            	}

	            	request.success(function post_callback (data) {

	            		$scope.response = "Recupération en cours...";
	            		
			        	var after_timestamp = new Date();
			        	var time = after_timestamp.getHours() + ":" + after_timestamp.getMinutes() + ":" + after_timestamp.getSeconds();

			        	var diff = after_timestamp.getTime() - before_timestamp;

	            		
	            		if ($scope.should_log)
	            			console.log(data);

			        	if(data['status'] == 'ok' || typeof(data["status"]) == "undefined"){

			        		toastr.success("Réponse positive du serveur");

			        	}
			        	else{
			        		toastr.info("Réponse négative du serveur");
			        	} 

			        	$timeout(function() {

				        	var scope = angular.element("[ng-controller=results]").scope();

			        		scope.$apply(function(){
			        			scope.med_arr.push(diff);
			        			scope.med += diff;
			        			scope.results.push({"date" : time, "description" : "Temps de requête : ", "elapsed_time" : diff, "req" : $scope.platform.name + " : " + $scope.ws, "param" : $scope.param});
			        		});

								//Enregistrement dans le localStorage
							localStorage.speed_test_app_results = JSON.stringify({"results" : scope.results, "med" : scope.med, "med_arr" : scope.med_arr});
						});

						if (number_of_times_remaining == 0){
							//Affichage de la réponse qu'au dernier coup
	            			$scope.response = $sce.trustAsHtml(library.json.prettyPrint(data));
							$scope.idle = true;
						}
			        })
	            	.error(function (error) {
	            		toastr.error("Le webservice demandé n'existe pas || erreur du serveur.");

						if (number_of_times_remaining == 0)
							$scope.idle = true;
	            	});

	            	if(--number_of_times_remaining == 0){
	            		clearInterval(timer);
	            	}

	            }, $scope.interval);

	        }


	    });

		speed_test_app.controller("results", function ($scope) {
			$scope.results = [];
			$scope.med = 0;
			$scope.med_arr = [];

			try{
				var old_results = JSON.parse(localStorage.speed_test_app_results);

				for(var prop in old_results) {
					$scope[prop] = old_results[prop];
				}
			}
			catch(e){
				toastr.error('Pas de données enregistrées précédemment');
			}
		});
