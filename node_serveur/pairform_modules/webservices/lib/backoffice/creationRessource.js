"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes");


// récupèration des données liées à la création d'une ressource (liste des thèmes, listes des langues ; traduit dans la langue choisi)
exports.getCreationRessource = function(code_langue, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectAllThemes();

	// Récupére toutes les thèmes (traduits dans la langue choisie)
	function selectAllThemes() {
		pairform_dao.theme_dao.selectAllThemes(
			code_langue,
			function (liste_themes) {
				retour_json.liste_themes = liste_themes;
				selectAllUsages();
			},
			callbackError
		);
	}

	// Récupére toutes les usages (traduits dans la langue choisie)
	function selectAllUsages() {
		pairform_dao.usage_dao.selectAllUsages(
			function (liste_usages) {
				retour_json.liste_usages = liste_usages;
				selectAllLangues();
			},
			callbackError
		);
	}

	// Récupére toutes les langues (traduites dans la langue choisie)
	function selectAllLangues() {
		pairform_dao.langue_dao.selectAllLangues(
			code_langue,
			function (liste_langues) {
				retour_json.liste_langues = liste_langues;
				callback(retour_json);
			},
			callbackError
		);
	}
};