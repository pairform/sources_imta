"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes");


// récupèration des données lièes à la page d'attribution des rôles aux utilisateurs d'un espace
exports.getAttributionRolesEspace = function(id_espace, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectRolesByIdEspace();

	function selectRolesByIdEspace() {
		pairform_dao.role_dao.selectRolesByIdEspace(
			id_espace,
			function (liste_roles) {
				retour_json.liste_roles = liste_roles;
				selectEmailMembresByIdRole(0);
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres du groupe Espace ayant un role donné
	function selectEmailMembresByIdRole(index_role) {
		pairform_dao.membre_dao.selectMembresByIdRoleEspace(
			retour_json.liste_roles[index_role].id_role_espace,
			false,
			function (liste_membres) {
				retour_json.liste_roles[index_role].liste_membres_email = liste_membres;
				selectNomDomaineMembresByIdRole(index_role);
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres du groupe Espace ayant un role donné
	function selectNomDomaineMembresByIdRole(index_role) {
		pairform_dao.membre_dao.selectMembresByIdRoleEspace(
			retour_json.liste_roles[index_role].id_role_espace,
			true,
			function (liste_membres) {
				retour_json.liste_roles[index_role].liste_membres_nom_domaine = liste_membres;

				index_role++;
				if (index_role < retour_json.liste_roles.length) {
					selectEmailMembresByIdRole(index_role);
				} else {
					selectEmailMembresByIdEspace();
				}
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres du groupe Espace
	function selectEmailMembresByIdEspace() {
		pairform_dao.membre_dao.selectMembresByIdEspace(
			id_espace,
			false,
			function (liste_membres) {
				retour_json.liste_membres_email = liste_membres;
				selectNomDomaineMembresByIdEspace();
			},
			callbackError
		);
	}

	// Récupère la liste des noms de domaines des membres du groupe Espace (le nom de domaine est compté comme un membre mais peut faire référence à plusieurs utilisateurs)
	function selectNomDomaineMembresByIdEspace() {
		pairform_dao.membre_dao.selectMembresByIdEspace(
			id_espace,
			true,
			function (liste_membres) {
				retour_json.liste_membres_nom_domaine = liste_membres;
				callback(retour_json);
			},
			callbackError
		);
	}
};


// récupèration des données lièes à la page d'attribution des rôles aux utilisateurs d'une ressource
exports.getAttributionRolesRessource = function(id_espace, id_ressource, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectRolesByIdRessource();

	function selectRolesByIdRessource() {
		pairform_dao.role_dao.selectRolesByIdRessource(
			id_ressource,
			function (liste_roles) {
				retour_json.liste_roles = liste_roles;
				selectEmailMembresByIdRole(0);
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres d'un Espace ayant un role donné
	function selectEmailMembresByIdRole(index_role) {
		pairform_dao.membre_dao.selectMembresByIdRoleRessource(
			retour_json.liste_roles[index_role].id_role_ressource,
			false,
			function (liste_membres) {
				retour_json.liste_roles[index_role].liste_membres_email = liste_membres;
				selectNomDomaineMembresByIdRole(index_role);
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres d'un Espace ayant un role donné
	function selectNomDomaineMembresByIdRole(index_role) {
		pairform_dao.membre_dao.selectMembresByIdRoleRessource(
			retour_json.liste_roles[index_role].id_role_ressource,
			true,
			function (liste_membres) {
				retour_json.liste_roles[index_role].liste_membres_nom_domaine = liste_membres;

				index_role++;
				if (index_role < retour_json.liste_roles.length) {
					selectEmailMembresByIdRole(index_role);
				} else {
					selectEmailMembresByIdEspace();
				}
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres du groupe Espace
	function selectEmailMembresByIdEspace() {
		pairform_dao.membre_dao.selectMembresByIdEspace(
			id_espace,
			false,
			function (liste_membres) {
				retour_json.liste_membres_email = liste_membres;
				selectNomDomaineMembresByIdEspace();
			},
			callbackError
		);
	}

	// Récupère la liste des noms de domaines des membres du groupe Espace (le nom de domaine est compté comme un membre mais peut faire référence à plusieurs utilisateurs)
	function selectNomDomaineMembresByIdEspace() {
		pairform_dao.membre_dao.selectMembresByIdEspace(
			id_espace,
			true,
			function (liste_membres) {
				retour_json.liste_membres_nom_domaine = liste_membres;
				callback(retour_json);
			},
			callbackError
		);
	}
};
