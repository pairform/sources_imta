"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes");


// récupèration de la liste des formats de capsule
exports.getAllFormats = function(callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.format_dao.selectAllFormats(
		function (liste_formats) {
			retour_json.liste_formats = liste_formats;
			callback(retour_json);
		},
		callbackError
	);
};