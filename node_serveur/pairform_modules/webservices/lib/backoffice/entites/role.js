"use strict";

var pairform_dao = require("pairform_dao"),
	  	   async = require("async"),
	  	     log = require("metalogger")(),
	  constantes = require("../../constantes");


// Récupération de la liste des roles via son espace
exports.getRolesByIdEspace = function(id_espace, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectRolesByIdEspace();

	function selectRolesByIdEspace() {
		pairform_dao.role_dao.selectRolesByIdEspace(
			id_espace,
			function (liste_roles) {
				retour_json.liste_roles = liste_roles;
				callback(retour_json);
			},
			callbackError
		);
	}
};


// Récupération de la liste des roles via sa ressource
exports.getRolesByIdRessource = function(id_ressource, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectRolesByIdRessource();

	function selectRolesByIdRessource() {
		pairform_dao.role_dao.selectRolesByIdRessource(
			id_ressource,
			function (liste_roles) {
				retour_json.liste_roles = liste_roles;
				callback(retour_json);
			},
			callbackError
		);
	}
};


// Mise à jour de la liste des roles d'un espace
exports.postRolesEspace = function(liste_parametres, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	if(liste_parametres.liste_roles_modifies.length > 0) {				// s'il y a des anciens rôles modifies
		updateRole(0);
	} else if(liste_parametres.liste_roles_supprimes.length > 0) {		// sinon, s'il y a des anciens rôles à supprimer
		updateMembresRoleByIdRole(0);
	} else if(liste_parametres.liste_nouveaux_roles.length > 0) {		// sinon, s'il y a des nouveaux rôles à ajouter
		insertRoleEspace(0);
	} else {
		callback(constantes.RETOUR_JSON_OK);
	}

	// Mise à jour des roles modifiés
	function updateRole(index_role) {
		pairform_dao.role_dao.updateRoleEspace(
			liste_parametres.liste_roles_modifies[index_role].id_role_espace,
			liste_parametres.liste_roles_modifies[index_role].nom,
			liste_parametres.liste_roles_modifies[index_role].editer_espace,
			liste_parametres.liste_roles_modifies[index_role].gerer_groupes,
			liste_parametres.liste_roles_modifies[index_role].gerer_roles_espace,
			liste_parametres.liste_roles_modifies[index_role].gerer_ressources_et_capsules,
			liste_parametres.liste_roles_modifies[index_role].gerer_toutes_ressources_et_capsules,
			liste_parametres.liste_roles_modifies[index_role].gerer_visibilite_toutes_capsules,
			liste_parametres.liste_roles_modifies[index_role].deplacer_capsule,
			function () {
				index_role++;
				// S'il reste des nouveaux rôles à ajouter en BDD
				if (index_role < liste_parametres.liste_roles_modifies.length) {
					updateRole(index_role);
				} else {
					if(liste_parametres.liste_roles_supprimes.length > 0) {				// s'il y a des anciens rôles à supprimer
						updateMembresRoleByIdRole(0);
					} else if(liste_parametres.liste_nouveaux_roles.length > 0) {		// sinon, s'il y a des nouveaux rôles à ajouter
						insertRoleEspace(0);
					} else {
						callback(constantes.RETOUR_JSON_OK);
					}
				}
			},
			callbackError
		);
	}

	// retire un rôle à une liste de membres
	function updateMembresRoleByIdRole(index_role) {
		pairform_dao.membre_dao.updateMembresRoleEspaceByIdRole(
			liste_parametres.liste_roles_supprimes[index_role].id_role_espace,
			null,
			function () {
				index_role++;
				// S'il reste des rôles à retirés à des membres en BDD
				if (index_role < liste_parametres.liste_roles_supprimes.length) {
					updateMembresRoleByIdRole(index_role);
				} else {
					deleteRoles();
				}
			},
			callbackError
		);
	}

	// suppression d'anciens rôles de l'espace
	function deleteRoles() {
		pairform_dao.role_dao.deleteRolesEspace(
			liste_parametres.liste_roles_supprimes,
			function () {
				// S'il y a des nouveaux rôles à ajouter
				if (liste_parametres.liste_nouveaux_roles.length > 0) {
					insertRoleEspace(0);
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}

	// ajout des nouveaux roles dans l'espace
	function insertRoleEspace(index_role) {
		pairform_dao.role_dao.insertRoleEspace(
			liste_parametres.id_espace,
			false,
			liste_parametres.liste_nouveaux_roles[index_role].nom,
			liste_parametres.liste_nouveaux_roles[index_role].editer_espace,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_groupes,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_roles_espace,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_ressources_et_capsules,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_toutes_ressources_et_capsules,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_visibilite_toutes_capsules,
			liste_parametres.liste_nouveaux_roles[index_role].deplacer_capsule,
			function () {
				index_role++;
				// S'il reste des nouveaux rôles à ajouter en BDD
				if (index_role < liste_parametres.liste_nouveaux_roles.length) {
					insertRoleEspace(index_role);
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}
};


// Mise à jour de la liste des roles d'une ressource
exports.postRolesRessource = function(liste_parametres, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	if(liste_parametres.liste_roles_modifies.length > 0) {				// s'il y a des anciens rôles modifies
		updateRole(0);
	} else if(liste_parametres.liste_roles_supprimes.length > 0) {		// sinon, s'il y a des anciens rôles à supprimer
		deleteMembresRoleRessource(0);
	} else if(liste_parametres.liste_nouveaux_roles.length > 0) {		// sinon, s'il y a des nouveaux rôles à ajouter
		insertRoleRessource(0);
	} else {
		callback(constantes.RETOUR_JSON_OK);
	}

	// Mise à jour des roles modifiés
	function updateRole(index_role) {
		pairform_dao.role_dao.updateRoleRessource(
			liste_parametres.liste_roles_modifies[index_role].id_role_ressource,
			liste_parametres.liste_roles_modifies[index_role].nom,
			liste_parametres.liste_roles_modifies[index_role].editer_ressource,
			liste_parametres.liste_roles_modifies[index_role].gerer_open_badge,
			liste_parametres.liste_roles_modifies[index_role].afficher_statistiques,
			liste_parametres.liste_roles_modifies[index_role].gerer_roles_ressource,
			liste_parametres.liste_roles_modifies[index_role].supprimer_ressource,
			liste_parametres.liste_roles_modifies[index_role].gerer_capsule,
			liste_parametres.liste_roles_modifies[index_role].gerer_visibilite,
			liste_parametres.liste_roles_modifies[index_role].gerer_toutes_capsules,
			liste_parametres.liste_roles_modifies[index_role].gerer_visibilite_toutes_capsules_ressource,
			liste_parametres.liste_roles_modifies[index_role].supprimer_messages_capsule,
			liste_parametres.liste_roles_modifies[index_role].supprimer_messages_toutes_capsules,
			function () {
				index_role++;
				// S'il reste des nouveaux rôles à ajouter en BDD
				if (index_role < liste_parametres.liste_roles_modifies.length) {
					updateRole(index_role);
				} else {
					if(liste_parametres.liste_roles_supprimes.length > 0) {				// s'il y a des anciens rôles à supprimer
						deleteMembresRoleRessource(0);
					} else if(liste_parametres.liste_nouveaux_roles.length > 0) {		// sinon, s'il y a des nouveaux rôles à ajouter
						insertRoleRessource(0);
					} else {
						callback(constantes.RETOUR_JSON_OK);
					}
				}
			},
			callbackError
		);
	}

	// retire un rôle à une liste de membres
	function deleteMembresRoleRessource(index_role) {
		pairform_dao.membre_dao.deleteMembresRoleRessource(
			null,
			liste_parametres.liste_roles_supprimes[index_role].id_role_ressource,
			function () {
				index_role++;
				// S'il reste des rôles à retirés à des membres en BDD
				if (index_role < liste_parametres.liste_roles_supprimes.length) {
					deleteMembresRoleRessource(index_role);
				} else {
					deleteRoles();
				}
			},
			callbackError
		);
	}

	// suppression d'anciens rôles de la ressource
	function deleteRoles() {
		pairform_dao.role_dao.deleteRolesRessource(
			liste_parametres.liste_roles_supprimes,
			function () {
				// S'il y a des nouveaux rôles à ajouter
				if (liste_parametres.liste_nouveaux_roles.length > 0) {
					insertRoleRessource(0);
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}

	// ajout des nouveaux roles dans la ressource
	function insertRoleRessource(index_role) {
		pairform_dao.role_dao.insertRoleRessource(
			liste_parametres.id_ressource,
			liste_parametres.liste_nouveaux_roles[index_role].nom,
			liste_parametres.liste_nouveaux_roles[index_role].editer_ressource,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_open_badge,
			liste_parametres.liste_nouveaux_roles[index_role].afficher_statistiques,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_roles_ressource,
			liste_parametres.liste_nouveaux_roles[index_role].supprimer_ressource,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_capsule,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_visibilite,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_toutes_capsules,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_visibilite_toutes_capsules_ressource,
			liste_parametres.liste_nouveaux_roles[index_role].supprimer_messages_capsule,
			liste_parametres.liste_nouveaux_roles[index_role].supprimer_messages_toutes_capsules,
			function () {
				index_role++;
				// S'il reste des nouveaux rôles à ajouter en BDD
				if (index_role < liste_parametres.liste_nouveaux_roles.length) {
					insertRoleRessource(index_role);
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}
};


// Mise à jour de la liste des membres nommé à un rôle dans un espace
exports.postMembresRoleEspace = function(liste_parametres, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	if (liste_parametres.liste_membres_supprimes.length > 0) {			// S'il y a des membres qui ne sont plus nommés
		updateMembresRoleRetire();
	} else if (liste_parametres.liste_nouveaux_membres.length > 0) {	// Sinon, s'il y a des membres nommés
		updateMembresRoleNomme();
	} else {
		callback(constantes.RETOUR_JSON_OK);
	}

	// retire le rôle d'une liste de membres - le paramètre id_role_espace vaut NULL si l'on veut retirer un role à une liste de membres
	function updateMembresRoleRetire() {
		pairform_dao.membre_dao.updateMembresRoleEspace(
			null,
			liste_parametres.liste_membres_supprimes,
			function () {
				// s'il y a des membres nommés
				if (liste_parametres.liste_nouveaux_membres.length > 0) {
					updateMembresRoleNomme();
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}

	// nomme à un rôle une liste de membres
	function updateMembresRoleNomme() {
		pairform_dao.membre_dao.updateMembresRoleEspace(
			liste_parametres.id_role_espace,
			liste_parametres.liste_nouveaux_membres,
			function () {
				callback(constantes.RETOUR_JSON_OK);
			},
			callbackError
		);
	}
};


// Mise à jour de la liste des membres nommé à un rôle dans une ressource
exports.postMembresRoleRessource = function(liste_parametres, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	if (liste_parametres.liste_membres_supprimes.length > 0) {			// S'il y a des membres qui ne sont plus nommés
		deleteMembresRoleRetire();
	} else if (liste_parametres.liste_nouveaux_membres.length > 0) {	// Sinon, s'il y a des membres nommés
		deleteMembresRoleRessourceByIdRessource();
	} else {
		callback(constantes.RETOUR_JSON_OK);
	}

	// retire le rôle d'une liste de membres
	function deleteMembresRoleRetire() {
		pairform_dao.membre_dao.deleteMembresRoleRessource(
			liste_parametres.liste_membres_supprimes,
			liste_parametres.id_role_ressource,
			function () {
				// s'il y a des membres nommés
				if (liste_parametres.liste_nouveaux_membres.length > 0) {
					deleteMembresRoleRessourceByIdRessource();
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}

	// retire l'ancien rôle dasn la ressource des membres nommé à un nouveau rôle
	function deleteMembresRoleRessourceByIdRessource() {
		pairform_dao.membre_dao.deleteMembresRoleRessourceByIdRessource(
			liste_parametres.liste_nouveaux_membres,
			liste_parametres.id_ressource,
			function () {
				insertMembreRoleNomme(0);
			},
			callbackError
		);
	}

	// nomme à un rôle une liste de membres
	function insertMembreRoleNomme(index) {
		pairform_dao.membre_dao.insertMembreRoleRessource(
			liste_parametres.liste_nouveaux_membres[index].id_membre,
			liste_parametres.id_role_ressource,
			function () {
				index++;
				// s'il reste des membres à ajouter
				if (liste_parametres.liste_nouveaux_membres[index]) {
					insertMembreRoleNomme(index);
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}
};

