"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes"),
	  		  fs = require("fs");


// Récupération des données du tableau de bord (liste des espaces, liste des ressources, liste des capsules) dans le cas ou l'utilisateur connecté est l'admin PairForm
exports.getTableauDeBordAdminPairForm = function(callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectAllEspaces();

	function selectAllEspaces() {
		pairform_dao.espace_dao.selectAllEspaces(
			function (liste_espaces) {
				retour_json.liste_espaces = liste_espaces;
				selectAllRessources();
			},
			callbackError
		);
	}

	function selectAllRessources() {
		pairform_dao.ressource_dao.selectAllRessources(
			function (liste_ressources) {
				retour_json.liste_ressources = liste_ressources;
				selectAllCapsules();
			},
			callbackError
		);
	}

	function selectAllCapsules() {
		pairform_dao.capsule_dao.selectAllCapsules(
			function (liste_capsules) {
				retour_json.liste_capsules = liste_capsules;
				callback(retour_json);
			},
			callbackError
		);
	}
};


// Récupération des données du tableau de bord (liste des espaces, liste des ressources, liste des capsules) dans le cas ou l'utilisateur connecté n'est pas l'admin PairForm
exports.getTableauDeBord = function(email, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectEspacesByEmail();

	function selectEspacesByEmail() {
		pairform_dao.espace_dao.selectEspacesByEmail(
			email,
			function (liste_espaces) {
				retour_json.liste_espaces = liste_espaces;

				// s'il y a des espaces dans la liste
				if (liste_espaces.length > 0) {
					selectRessourcesByEspaces(liste_espaces);
				} else {
					retour_json.liste_ressources = [];
					retour_json.liste_capsules = [];
					callback(retour_json);
				}
			},
			callbackError
		);
	}

	function selectRessourcesByEspaces(liste_espaces) {
		pairform_dao.ressource_dao.selectRessourcesByEspaces(
			liste_espaces,
			function (liste_ressources) {
				retour_json.liste_ressources = liste_ressources;

				// s'il y a des ressources dans la liste
				if (liste_ressources.length > 0) {
					selectCapsulesByRessources(liste_ressources);
				} else {
					retour_json.liste_capsules = [];
					callback(retour_json);
				}
			},
			callbackError
		);
	}

	function selectCapsulesByRessources(liste_ressources) {
		pairform_dao.capsule_dao.selectCapsulesByRessources(
			liste_ressources,
			function (liste_capsules) {
				retour_json.liste_capsules = liste_capsules;
				callback(retour_json);
			},
			callbackError
		);
	}
};
