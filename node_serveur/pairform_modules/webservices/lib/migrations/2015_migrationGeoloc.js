var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes");


exports.alterTablesPourGeoloc = function(callback) {

// --
// -- Structure de la table `cape_messages_pieces_jointes`
// --

// CREATE TABLE IF NOT EXISTS `cape_messages_pieces_jointes` (
//   `id_message` int(11) NOT NULL,
//   `id_utilisateur` int(11) NOT NULL,
//   `nom_serveur` varchar(40) character set utf8 collate utf8_unicode_ci NOT NULL,
//   `nom_original` varchar(105) character set utf8 collate utf8_unicode_ci NOT NULL,
//   `extension` varchar(10) character set utf8 collate utf8_unicode_ci NOT NULL,
//   `taille` varchar(14) character set utf8 collate utf8_unicode_ci NOT NULL,
//   `nom_thumbnail` varchar(40) character set utf8 collate utf8_unicode_ci default NULL,
//   PRIMARY KEY  (`id_message`,`nom_serveur`),
//   UNIQUE KEY `nom_thumbnail` (`nom_thumbnail`),
//   UNIQUE KEY `nom_thumbnail_2` (`nom_thumbnail`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


// Changement des types de colonnes pour géo -> décimal

// 	ALTER TABLE  `cape_messages` CHANGE  `geo_latitude`  `geo_latitude` DECIMAL( 10,8 ) NOT NULL ,
// CHANGE  `geo_longitude`  `geo_longitude` DECIMAL( 11,8 ) NOT NULL
};
