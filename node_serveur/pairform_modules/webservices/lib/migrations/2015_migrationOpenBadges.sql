//ALTER TABLE `cape_espaces` ADD `url` VARCHAR( 250 ) NULL AFTER `nom_long` ;

//CREATE TABLE IF NOT EXISTS `cape_openbadges` (
//	`id_openbadge` bigint(20) NOT NULL auto_increment,
//	`id_ressource` bigint(20) NOT NULL,
//	`nom` varchar(250) NOT NULL,
//	`description` varchar(250) NOT NULL,
//	`issuer_externe` varchar(250) default NULL,
//	`date_creation` datetime NOT NULL,
//	`date_edition` datetime default NULL,
//	PRIMARY KEY  (`id_openbadge`),
//	UNIQUE KEY `id_ressource_2` (`id_ressource`,`nom`),
//	KEY `id_ressource` (`id_ressource`)
//) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

//ALTER TABLE `cape_openbadges` ADD CONSTRAINT `cape_openbadges_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`);


//CREATE TABLE IF NOT EXISTS `cape_utilisateurs_openbadges` (
//	`id_openbadge` bigint(20) NOT NULL,
//	`id_utilisateur` bigint(20) NOT NULL,
//	`date_gagne` datetime NOT NULL,
//	UNIQUE KEY `id_openbadge` (`id_openbadge`,`id_utilisateur`),
//	KEY `id_openbadge_2` (`id_openbadge`),
//	KEY `id_utilisateur` (`id_utilisateur`)
//) ENGINE=InnoDB DEFAULT CHARSET=latin1;

//ALTER TABLE `cape_utilisateurs_openbadges`
//	ADD CONSTRAINT `cape_utilisateurs_openbadges_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`),
//	ADD CONSTRAINT `cape_utilisateurs_openbadges_ibfk_1` FOREIGN KEY (`id_openbadge`) REFERENCES `cape_openbadges` (`id_openbadge`);
