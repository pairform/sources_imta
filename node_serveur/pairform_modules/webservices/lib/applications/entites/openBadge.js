"use strict";

var pairform_dao = require("pairform_dao"),
	web_services = require("../../webServices"),
	  constantes = require("../../constantes"),
			  fs = require("fs");


// Récupération de la liste des openbadges via sa ressource
exports.getOpenBadgesByIdRessource = function(id_ressource, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.openbadge_dao.selectOpenBadgesByIdRessource(
			id_ressource,
			function (liste_openbadges) {
				retour_json.liste_openbadges = liste_openbadges;
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	});
};