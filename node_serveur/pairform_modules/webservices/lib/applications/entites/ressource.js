"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		 request = require("request");


// 
exports.verifier = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	pairform_dao.capsule_dao.checkCapsule(
		param.hash_ressource,
		function callbackSuccess (capsule) {
			callback(capsule);
		},
		callbackError
	);
};


// Récupère toutes les ressources accessibles à l'utilisateur
exports.getRessourcesAccessibles = function(param, callback) {
	var id_langue = param.id_langue || "3",
		email = param.email;
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	pairform_dao.ressource_dao.selectRessourcesAccessibles(
		id_langue,
		email,
		function (liste_ressources) {
			callback({
				"status" : "ok", 
				"ressources" : liste_ressources
			});
		},
		callbackError
	);
};


// Récupère les dernières ressources publiés, accessibles à l'utilisateur
exports.getRessourcesRecentes = function(param, callback) {
	var id_langue = param.id_langue || "3",
		email = param.email;
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	pairform_dao.ressource_dao.selectRecentesRessources(
		id_langue,
		email,
		function (liste_ressources) {
			callback({
				"status" : "ok", 
				"ressources" : liste_ressources
			});
		},
		callbackError
	);
};


// Récupération d'un ressource (infos, capsule, profil & obj d'apprentissage, etc.)
exports.getRessource = function(parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var id_ressource = parametres.id_ressource,
		id_utilisateur = parametres.id_utilisateur,
		email = parametres.email,
		id_langue = parametres.id_langue || 3;
	
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError(erreur) {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectRessourceForWeb();

	function selectRessourceForWeb() {
		pairform_dao.ressource_dao.selectRessourceForWeb(
			id_ressource,
			id_langue,
			function (ressource) {
				if (ressource) {
					retour_json.ressource = ressource;
					selectCapsulesByRessource();
				} else {
					callbackError();
				}
			},
			callbackError
		);
	}

	function selectCapsulesByRessource() {
		pairform_dao.capsule_dao.selectCapsulesByRessource(
			id_ressource,
			email,
			function (capsules) {
				retour_json.ressource.capsules = capsules;
				selectNombreFromRessource();
			},
			callbackError
		);
	}

	// récupère le nombre de message déposé sur chaque capsule de la ressource
	function selectNombreFromRessource() {
		pairform_dao.message_dao.selectNombreFromRessource(
			id_utilisateur, 
			id_langue,
			id_ressource,
			function (nb_messages) {
				var nb_messages_objet = {};
				
				for (var i = 0; i < nb_messages.length; i++) {
					nb_messages_objet[nb_messages[i]['id_capsule']] = nb_messages[i]['count'];
				}
				retour_json.ressource.nombre_messages = nb_messages_objet;

				// si l'utilisateur est connecté et si c'est une ressource de formation
				if (id_utilisateur && retour_json.ressource.id_usage == constantes.USAGE_FORMATION) {
					retour_json.ressource.profil_apprentissage = {};
					retour_json.ressource.objectifs_apprentissage = {};
					
					// on récupère les obj et le profil d'apprentissage de l'utilisateur
					getProfilApprentissageByUtilisateurEtRessource(function () {
						getObjectifsApprentissageByUtilisateurEtRessource( function () {
							callback(retour_json);
						});
					});
				}
				else {
					callback(retour_json);
				}
			},
			callbackError
		);
	}

	function getProfilApprentissageByUtilisateurEtRessource(callback) {
		selectQuestionsByUtilisateurEtRessource();

		// Récupère la liste des questions du profil d'apprentissage d'un utilisateur dans une ressource
		function selectQuestionsByUtilisateurEtRessource() {
			pairform_dao.profil_apprentissage_dao.selectQuestionsByUtilisateurEtRessource(
				id_ressource,
				id_utilisateur,
				id_langue,
				function (liste_questions) {
					// s'il y a des questions, on va récupérer les réponses
					if (liste_questions.length > 0) {
						retour_json.ressource.profil_apprentissage.liste_questions = liste_questions;
						selectReponsesByQuestion(0);
					} else {
						// sinon, cela veut dire que l'utilisateur consulte pour la première fois le profil d'apprentisage de la ressource
						// on initialise avec les valeurs par défaut (null) les réponses valides aux questions
						selectIdQuestionsByRessource()
					}
				},
				callbackError
			);
		}

		// Récupère la liste des réponses à une question
		function selectReponsesByQuestion(index_question) {
			pairform_dao.profil_apprentissage_dao.selectReponsesByQuestion(
				retour_json.ressource.profil_apprentissage.liste_questions[index_question].id_question,
				id_langue,
				function (liste_reponses) {
					retour_json.ressource.profil_apprentissage.liste_questions[index_question].liste_reponses = liste_reponses;

					index_question++
					// s'il y a encore une question pour laquelle les réponses n'ont pas été récupérées
					if (index_question < retour_json.ressource.profil_apprentissage.liste_questions.length) {
						selectReponsesByQuestion(index_question);
					} else {
						callback(retour_json);
					}
				},
				callbackError
			);
		}

		// Récupère la liste des id des questions du profil d'apprentissage d'une ressource
		function selectIdQuestionsByRessource() {
			pairform_dao.profil_apprentissage_dao.selectIdQuestionsByRessource(
				id_ressource,
				function (liste_id_questions) {
					//Quick patch :
					if (liste_id_questions)
						insertReponseValideByQuestion(liste_id_questions, 0);
				},
				callbackError
			);
		}

		// créé la reponse par défaut (null) d'un utilisateur pour une question du profil d'apprentissage d'une ressource
		function insertReponseValideByQuestion(liste_id_questions, index_question) {
			pairform_dao.profil_apprentissage_dao.insertReponseValideByQuestion(
				liste_id_questions[index_question].id_question,
				id_utilisateur,
				function () {
					index_question++
					// s'il y a encore une question pour laquelle une réponse par défaut n'a pas été inséré
					if (index_question < liste_id_questions.length) {
						insertReponseValideByQuestion(liste_id_questions, index_question)
					} else {
						selectQuestionsByUtilisateurEtRessource();
					}
				},
				callbackError
			);
		}
	};

	function getObjectifsApprentissageByUtilisateurEtRessource(callback) {
		selectObjectifsApprentissageByUtilisateurEtRessource();

		function selectObjectifsApprentissageByUtilisateurEtRessource() {
			pairform_dao.objectif_apprentissage_dao.selectObjectifsApprentissageByUtilisateurEtRessource(
				id_ressource,
				id_utilisateur,
				function (liste_objectifs_apprentissage) {
					retour_json.ressource.objectifs_apprentissage.liste_objectifs_apprentissage = liste_objectifs_apprentissage;
					selectNbObjectifsValidesByUtilisateurEtRessource();
				},
				callbackError
			);
		}

		// Récupère le nombre d'objectifs d'apprentissage validé par un utilisateur dans une ressource
		function selectNbObjectifsValidesByUtilisateurEtRessource() {
			pairform_dao.objectif_apprentissage_dao.selectNbObjectifsValidesByUtilisateurEtRessource(
				id_ressource,
				id_utilisateur,
				function (nb_objectifs_valides) {
					retour_json.ressource.objectifs_apprentissage.nb_objectifs_valides = nb_objectifs_valides;
					callback(retour_json);
				},
				callbackError
			);
		}
	};
};