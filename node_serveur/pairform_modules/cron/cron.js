var web_services = require("../webservices/lib/webServices"),
		  CONFIG = require("config"),
		  cluster = require("cluster");

// Protection anti-doublon par les différents clusters, verification qu'on est sur la prod, et pas en instance de développement.
if(cluster.isMaster && CONFIG.app.urls.alias_serveur.match(/pairform.fr/)){
	require("mail_resume");
	require("suppression_bas");
	require("mail_inbox");
	require("mail_watch");
}