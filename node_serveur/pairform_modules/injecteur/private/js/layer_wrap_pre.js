// 'use strict';

//Utilisation de notre propre instance de jQuery, sans polluer le DOM
$_pf = window.$_pf || jQuery.noConflict(true);

//Injection de tous les modules
$_pf.get(PF.globals.url.root + 'injecteur/layer', function(layer) {
	$_pf('body').prepend(layer);
