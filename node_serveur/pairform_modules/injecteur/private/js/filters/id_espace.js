
// filtre la liste des ressource pour ne conserver que les ressources d'un espace
pf_injecteur.filter("filtreByIdEspace", function() {
	return function(liste_ressources, id_espace) {
		var liste_ressources_filtres = new Array();

		// si un espace a été sélectionné 
		if (id_espace) {
			for(var i in liste_ressources) {
				// si l'espace sélectionné contient la ressource
				// Ou qu'on est dans le cas d'un nouveau document, sans encore d'espace
				if (id_espace === liste_ressources[i].id_espace || liste_ressources[i].id_espace === null) {
					liste_ressources_filtres.push(liste_ressources[i]);
				}
			}
			return liste_ressources_filtres;

		} else {
			return liste_ressources;
		}
	};
});