"use strict";


/*
 * Controleurs de gestion des rôles (nom et droits associer) de la ressource
 */

// page de gestion des rôles (nom et droits associer) de la ressource
backoffice_controlleurs.controller("controleurGestionRolesRessource", ["$scope", "$http", "toaster", "$routeParams", "$location", "$translate", "connexion", function($scope, $http, toaster, $routeParams, $location, $translate, connexion) {
	var succes_enregistrement, succes_suppression, erreur_requete, erreur_membre_deja_existant;
	var liste_anciens_roles, membres_nommes;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});
	$translate('LABEL.ERREUR.MEMBRE_DEJA_EXISTANT').then(function (traduction) {
		erreur_membre_deja_existant = traduction;
	});
	// traduction titre précédant la liste de membres nommée à un rôle
	$translate('LABEL.ROLE.MEMBRES').then(function (traduction) {
		membres_nommes = traduction;
	});

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;

	/*
	 * Gestion de l'accès aux fonctionnalités en fonction des droits de l'utilisateur connecté
	 * ATTENTION : reporter les changements dans controleursTableauDeBord.js 
	 */
	$scope.aAccesGererRolesRessource = function () {
		var utilisateur_connecte = connexion.getUtilisateurConnecte();
		return utilisateur_connecte.est_admin_pairform ||
			utilisateur_connecte.liste_roles[$routeParams.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			utilisateur_connecte.liste_roles[$routeParams.id_espace][$routeParams.id_ressource][GERER_ROLES_RESSOURCE];
	}


	/********************************************************
	 *					Attribuer un rôle
	 *******************************************************/
	
	// détermine l'index d'un membre dans une liste de membre
	// Rq: liste.indexOf fonctionne pas, AngularJS rajoute la clef du membre dans l'objet membre ; 2 membres identiques de 2 listes différentes apparaissent comme différents car leur clef est différente
	var indexOfMembre = function(membre, liste_membres) {
		for (var i in liste_membres) {
			if (liste_membres[i].nom == membre.nom) {
				return i;
			}
		}
		return -1;
	};

	$scope.attribuerRoles  = function() {
		if ($scope.aAccesGererRolesRessource()) {
			// récupération des informations à afficher sur la page
			$http.get(URL_SERVEUR_NODE +"webServices/espace/" + $routeParams.id_espace +"/ressource/" + $routeParams.id_ressource +"/attributionRoles")
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						$scope.liste_roles = retour_json.liste_roles;
						$scope.liste_membres_email = retour_json.liste_membres_email;
						$scope.liste_membres_nom_domaine = retour_json.liste_membres_nom_domaine;
						$scope.liste_membres_ressource = retour_json.liste_membres_email.concat( retour_json.liste_membres_nom_domaine );

					} else {
						toaster.pop("error", erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http get /webServices/espace/:id_espace/ressource/:id_ressource/attributionRoles");
					toaster.pop("error", erreur_requete);
				});
		}
	}();

	// Actualisation de la liste des membres du role sélectionné (cette liste contient la liste des email et la liste des noms de domaine)
	$scope.rechercherMembreRole = function() {
		$scope.role_selectionne.liste_membres_role = $scope.role_selectionne.liste_membres_email.concat( $scope.role_selectionne.liste_membres_nom_domaine );
		// Mise à jour du titre précédant la liste des membres du role sélectionné
		if($scope.role_selectionne) {
			$scope.membres_nommes = membres_nommes +" "+ $scope.role_selectionne.nom;
		}
	};

	// configuration des listes affichées dans la fenetre  permettant d'ajouter / supprimer les membres du role sélectionné
	$scope.gererMembresRole = function() {	
		// on créé une copie éditable des différentes liste de membres du role sélectionné
		$scope.nouvelle_liste_membres_email = angular.copy( $scope.role_selectionne.liste_membres_email );					// liste des email des membres
		$scope.nouvelle_liste_membres_nom_domaine = angular.copy( $scope.role_selectionne.liste_membres_nom_domaine );		// liste des noms de domaine des membres du role
		$scope.nouvelle_liste_membres_role = $scope.nouvelle_liste_membres_email.concat( $scope.nouvelle_liste_membres_nom_domaine );	// liste de tous les membres du role (concatenation des 2 listes)
	};

	// nomme un nouveau membre à un role
	$scope.nommerMembreRole = function(nouveau_membre) {
		if (nouveau_membre) {
			// si le nouveau membre est un nom de domaine et n'est pas déjà nommé à ce rôle 
			if (nouveau_membre.nom_de_domaine && indexOfMembre(nouveau_membre, $scope.nouvelle_liste_membres_nom_domaine) < 0) {
				$scope.nouvelle_liste_membres_nom_domaine.push(nouveau_membre);
				$scope.nouvelle_liste_membres_role.push(nouveau_membre);

			}	// sinon, si le nouveau membre est un email et n'est pas déjà nommé à ce rôle  
			else if (!nouveau_membre.nom_de_domaine && indexOfMembre(nouveau_membre, $scope.nouvelle_liste_membres_email) < 0) {
				$scope.nouvelle_liste_membres_email.push(nouveau_membre);
				$scope.nouvelle_liste_membres_role.push(nouveau_membre);

			}	// sinon, on affiche un message d'erreur indiquant la précense d'un doublon 
			else {
				toaster.pop("error", erreur_membre_deja_existant);
			}
		}
	};

	// supprime un membre ayant un role
	$scope.supprimerMembreRole = function(membre_a_supprime, liste_membres) {
		liste_membres.splice( liste_membres.indexOf(membre_a_supprime), 1);
		$scope.nouvelle_liste_membres_role.splice( $scope.nouvelle_liste_membres_role.indexOf(membre_a_supprime), 1);
	};

	// Mise à jour des membres du role sélectionné
	$scope.editerMembresRole = function() {
		var liste_membres_supprimes = [];
		var liste_nouveaux_membres = [];
		var membre;

		// on récupère les membres qui ne sont plus présent dans la nouvelle liste de membres
		for (var i in $scope.role_selectionne.liste_membres_role) {
			membre = $scope.role_selectionne.liste_membres_role[i];
			// si le membre de l'ancienne liste n'existe plus dans la nouvelle liste 
			if (indexOfMembre(membre, $scope.nouvelle_liste_membres_role) < 0) {
				liste_membres_supprimes.push(membre);
			}
		}
		// on récupère les nouveaux membres nommé au role selectionné
		for (var i in $scope.nouvelle_liste_membres_role) {
			membre = $scope.nouvelle_liste_membres_role[i];
			// si le membre de la nouvelle liste n'existait pas dans l'ancienne
			if (indexOfMembre(membre, $scope.role_selectionne.liste_membres_role) < 0) {
				liste_nouveaux_membres.push(membre);
			}
		}

		// on edite le role en BDD
		$http.post(URL_SERVEUR_NODE +"webServices/roleRessource/listeMembres", {
				id_espace: $routeParams.id_espace,
				id_ressource: $routeParams.id_ressource,
				id_role_ressource: $scope.role_selectionne.id_role_ressource,
				liste_nouveaux_membres: liste_nouveaux_membres,
				liste_membres_supprimes: liste_membres_supprimes
			})
			.success(function(retour_json){				
				if (retour_json.status === RETOUR_JSON_OK) {
					// mise à jour de l'affichage
					$scope.role_selectionne.liste_membres_email = $scope.nouvelle_liste_membres_email;
					$scope.role_selectionne.liste_membres_nom_domaine = $scope.nouvelle_liste_membres_nom_domaine;
					$scope.rechercherMembreRole();

					var index_membre;
					// Pour chaque nouveaux membres
					for (var i in liste_nouveaux_membres) {
						// Pour chaque rôle de la ressource
						for (var j in $scope.liste_roles) {
							// si le role courant n'est pas le role selectionné
							if($scope.liste_roles[j].id_role_ressource != $scope.role_selectionne.id_role_ressource) {
								// si un nouveau membre avait déjà un role, il ne l'a plus ; on le supprime donc de la liste des membres de son ancien rôle
								index_membre = indexOfMembre(liste_nouveaux_membres[i], $scope.liste_roles[j].liste_membres_email);
								if(index_membre >= 0) {
									$scope.liste_roles[j].liste_membres_email.splice(index_membre, 1);
								}

								index_membre = indexOfMembre(liste_nouveaux_membres[i], $scope.liste_roles[j].liste_membres_nom_domaine);
								if(index_membre >= 0) {
									$scope.liste_roles[j].liste_membres_nom_domaine.splice(index_membre, 1);
								}
							}
						}
					}

					toaster.pop("success", succes_enregistrement);
				} else {
					toaster.pop("error", erreur_requete);
				}
			})
			.error(function(){
				console.log("erreur requete http post /webServices/role/listeMembres");
				toaster.pop("error", erreur_requete);
			});
	};


	/********************************************************
	 *					Définir un rôle
	 *******************************************************/

	// détermine l'index d'un role dans une liste de role
	// Rq: liste.indexOf fonctionne pas, AngularJS rajoute la clef du role dans l'objet role ; 2 roles identiques de 2 listes différentes apparaissent comme différents car leur clef est différente
	var indexOfRole = function(role, liste_roles) {
		for (var i in liste_roles) {
			if (liste_roles[i].id_role_ressource == role.id_role_ressource &&
				liste_roles[i].nom == role.nom &&
				liste_roles[i].editer_ressource == role.editer_ressource &&
				liste_roles[i].gerer_open_badge == role.gerer_open_badge &&
				liste_roles[i].afficher_statistiques == role.afficher_statistiques &&
				liste_roles[i].gerer_roles_ressource == role.gerer_roles_ressource &&
				liste_roles[i].supprimer_ressource == role.supprimer_ressource &&
				liste_roles[i].gerer_capsule == role.gerer_capsule && 
				liste_roles[i].gerer_visibilite == role.gerer_visibilite && 
				liste_roles[i].gerer_toutes_capsules == role.gerer_toutes_capsules && 
				liste_roles[i].gerer_visibilite_toutes_capsules_ressource == role.gerer_visibilite_toutes_capsules_ressource && 
				liste_roles[i].supprimer_messages_capsule == role.supprimer_messages_capsule && 
				liste_roles[i].supprimer_messages_toutes_capsules == role.supprimer_messages_toutes_capsules ) {
					return i;
			}
		}
		return -1;
	};

	// vérifie l'existence d'un role dans une liste via l'id du rôle (le rôle est donc onsidéré comme existant, même s'il a été modifié)
	var indexOfIdRole = function(role, liste_roles) {
		for (var i in liste_roles) {
			if (liste_roles[i].id_role_ressource == role.id_role_ressource) {
				return i;
			}
		}
		return -1;
	};

	$scope.definirRoles = function() {
		if ($scope.aAccesGererRolesRessource()) {
			$scope.formulaire_soumis = false;

			// récupération des informations à afficher sur la page
			$http.get(URL_SERVEUR_NODE +"webServices/espace/" + $routeParams.id_espace +"/ressource/" + $routeParams.id_ressource +"/listeRoles")
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						$scope.liste_roles_a_definir = retour_json.liste_roles;
						liste_anciens_roles = angular.copy($scope.liste_roles_a_definir);
					} else {
						toaster.pop("error", erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http get /webServices/espace/:id_espace/ressource/:id_ressource/listeRoles");
					toaster.pop("error", erreur_requete);
				});
		}
	}();

	$scope.ajouterRole = function() {
		$scope.formulaire_soumis = false;

		var role = {
			editer_ressource: false,
			gerer_open_badge: false,
			afficher_statistiques: false,
			gerer_roles_ressource: false,
			supprimer_ressource: false,
			gerer_capsule: false,
			gerer_visibilite: false,
			gerer_toutes_capsules: false,
			gerer_visibilite_toutes_capsules_ressource: false,
			supprimer_messages_capsule: false,
			supprimer_messages_toutes_capsules: false
		};
		$scope.liste_roles_a_definir.push(role);
	};

	$scope.supprimerRole = function(role) {
		$scope.liste_roles_a_definir.splice( $scope.liste_roles_a_definir.indexOf(role), 1);
	};

	// Mise à jour en BDD des roles de la ressource
	$scope.editerRoles = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			var liste_nouveaux_roles = [], liste_roles_modifies = [], liste_roles_supprimes = [];

			// on parcoure la liste des rôles pour déterminer les nouveaux rôles et les anciens rôles modifiés
			for(var i in $scope.liste_roles_a_definir) {
				// si le role n'existait pas (en BDD), il vient d'être ajouté
				if (!$scope.liste_roles_a_definir[i].id_role_ressource) {
					liste_nouveaux_roles.push($scope.liste_roles_a_definir[i]);
				} else {
					// sinon, le role existait déjà (il est ancien)
					// si le role est différent de ce qu'il était initialement, il a été modifié
					if (indexOfRole($scope.liste_roles_a_definir[i], liste_anciens_roles) < 0) {
						liste_roles_modifies.push($scope.liste_roles_a_definir[i]);
					}
				}
			}

			// on parcoure la liste des anciens rôles pour déterminer les anciens rôles supprimés
			for(var i in liste_anciens_roles) {
				// si le role n'existe plus dans la liste des roles
				if (indexOfIdRole(liste_anciens_roles[i], $scope.liste_roles_a_definir) < 0) {
					liste_roles_supprimes.push(liste_anciens_roles[i]);
				}
			}

			// on edite les rôles (nom et droits associés) de la ressource
			$http.post(URL_SERVEUR_NODE +"webServices/ressource/listeRoles", {
					id_espace: $routeParams.id_espace,
					id_ressource: $routeParams.id_ressource,
					liste_nouveaux_roles: liste_nouveaux_roles,
					liste_roles_modifies: liste_roles_modifies,
					liste_roles_supprimes: liste_roles_supprimes
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						$scope.formulaire_soumis = false;
						toaster.pop("success", succes_enregistrement);
					} else {
						toaster.pop("error", erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http post /webServices/ressource/listeRoles");
					toaster.pop("error", erreur_requete);
				});
		}
	};
}]);