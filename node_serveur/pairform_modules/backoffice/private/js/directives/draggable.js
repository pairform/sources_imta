backoffice_controlleurs.directive('draggable', ["$document", function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {

			function handleDragStart(e) {
				console.log("handleDragStart");
				
				//On empèche l'event de bubbler 
				//Sur un DnD d'un message, ca bubble au btn-bar
				e.stopPropagation();

				var data,
					dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer;

				dataTransfer.effectAllowed = 'move';
				
				//Si on déplace un seul message
				//if (scope.message)
					//data = JSON.stringify(scope.message);
				//Sinon, on déplace tous les messages
				// else
				//	data = JSON.stringify(scope.array_messages_deplacement);

				dataTransfer.setData('text/javascript', data);
				// this/e.target is the source node.
				// this.addClassName('moving');
				scope.$root.$broadcast("pf:dragstart");
			};

			function handleDragEnd(e) {
				console.log("handleDragEnd");
				
				scope.$root.$broadcast("pf:dragend");
			};

			// col.setAttribute('draggable', 'true');  // Enable columns to be draggable.
			element.on('dragstart', handleDragStart);
			element.on('dragend', handleDragEnd);
		}
	}
}]);