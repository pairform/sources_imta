
 //Service permettant de lazy load la carte openlayer
pf_app.service('CarrouselLazyLoad', function () {
	var url = PF.globals.url.public + 'js/angular-carousel.min.js',
		loaded = false;

	return function (callback) {
		if (loaded) 
			callback();
		else
			PF.includeHead(url, function () {
				loaded = true;
				callback();
			});
	}
		
});