function PF () {};

//Global vars
PF.globals = {};
PF.globals.url = {};

PF.globals.url.root = "{{= url_root }}";

PF.globals.url.ws = PF.globals.url.root+"webServices/";
PF.globals.url.views = PF.globals.url.root + "app/";

PF.globals.url.private = PF.globals.url.root + "private/";
PF.globals.url.public = PF.globals.url.root + "public/";
PF.globals.url.res = PF.globals.url.root + "res/";
PF.globals.url.dist = PF.globals.url.root + "dist/";

PF.globals.os = "web";

PF.globals.capsule = {{= capsule }};
PF.globals.status = "{{= status }}";
PF.globals.message = "{{= message }}";

//Sélecteur de ressource par défaut
PF.globals.selector = "{{= selecteurs }}";
PF.globals.selectorRes = "#label-title";

//Types MIME autorisés pour l'upload de fichier sur l'app web
PF.globals.types_mime = "{{= types_mime }}"

PF.includeHead = function (url, callback)
{
	var head = window.document.getElementsByTagName('head')[0];
	var type = url.slice(url.lastIndexOf('.'));
	switch (type)
	{
		case '.js':
		var addedHead = window.document.createElement('script');
		addedHead.setAttribute('src', url);

		if(callback)
		{
			var completed = false;
			addedHead.onload = addedHead.onreadystatechange = function () {
				if (!completed && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
					completed = true;
					callback();
					addedHead.onload = addedHead.onreadystatechange = null;
					head.removeChild(addedHead);
				}
			};
		}
		break;
		case '.css':
		var addedHead = window.document.createElement('link');
		addedHead.setAttribute('href', url);
		addedHead.setAttribute('type', 'text/css');
		addedHead.setAttribute('rel', 'stylesheet');
		break;
		case '.less':
		var addedHead = window.document.createElement('link');
		addedHead.setAttribute('href', url);
		addedHead.setAttribute('type', 'text/css');
		addedHead.setAttribute('rel', 'stylesheet/less');
		break;
		default:
		var addedHead = '<!--[if lte IE 8]>    <script type="text/javascript"      src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>    <sc     // Le code conditionnel qui check si Google Chrome Frame est déjà installé     // Il ouvre une iFrame pour proposer le téléchargement.     window.attachEvent("onload", function() {       CFInstall.check({         mode: "overlay" // the default       });     });    </script>        //La balise indiquant à IE d`utiliser GCF si présent    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->'
		break;

	}
	addedHead.async =true;
	head.appendChild(addedHead);
}

//Récupération du titre de la ressource
PF.getHash = function () {

	var hash_ressource = document.querySelector('meta[name="hash_ressource"]');
	if(hash_ressource)
		return hash_ressource.content;
	else 
		return false;
}

//Le site est-il construit avec Scenari
PF.isScenari = function () {
	//"<meta name='generator' content='* scenari || SCENARI *'>"
	var meta_tag_content = document.querySelector('meta[name="generator"]','head');
	if(!meta_tag_content)
		return false;

	if(meta_tag_content.content.match(/scenari/i))
		return true;
	else
		return false;
}
//Le site est-il dans un cas spécial, ou le sélécteur est défini directement dans la page
PF.isSpecialCase = function () {
	var meta_tag_content = document.querySelector('meta[name="generator"]','head');
	if(!meta_tag_content)
		return false;

	if(meta_tag_content.content == "special")
		return true;
	else
		return false;
}


//Le site est-il généré depuis LaTeX
PF.isFromLatex = function () {
	//"<meta name='generator' content='* scenari || SCENARI *'>"
	var meta_tag_content = document.querySelector('meta[name="generator"]','head');
	if(!meta_tag_content)
		return false;

	if(meta_tag_content.content == "http://www.nongnu.org/elyxer/")
		return true;
	else
		return false;
}


PF.isWebVersion = function () {
	var meta_tag_content = document.querySelector('meta[name="platform"]','head');
	if(!meta_tag_content)
		return true;
	else if(meta_tag_content.content === "web")
		return true;
	else 
		return false;
}

PF.isMobVersion = function () {
	var meta_tag_content = document.querySelector('meta[name="platform"]','head');
	if(!meta_tag_content)
		return false;
	else if(meta_tag_content.content === "mob")
		return true;
	else 
		return false;
}

PF.hasLocalStorage = function (callback_success, callback_error, callback_fix) {
	try {
		PF._localStorage = localStorage;
		//Premier test I/O
		PF._localStorage.setItem("_pf_test", "_pf_test");
		PF._localStorage.removeItem("_pf_test");
		return callback_success();
	} catch(e) {
		try {
			//On tente un polyfill sur le prototype du localStorage
			PF.polyfillLocalStorage(Storage.prototype);
			//On teste à nouveau, après avoir polyfillé le localstorage
			PF._localStorage = localStorage;
			PF._localStorage.setItem("_pf_test", "_pf_test");
			PF._localStorage.removeItem("_pf_test");
			PF.showCompatibilityWarning(0, "Votre navigateur n'est pas pleinement compatible avec PairForm. ");
			return callback_success();
		} catch(e) {
			try {
				//Sinon, on utilise un objet temporaire pour stocker les infos
				PF._localStorage = {};
				PF.polyfillLocalStorage(PF._localStorage);
				PF._localStorage.setItem("_pf_test", "_pf_test");
				PF._localStorage.removeItem("_pf_test");
				PF.showCompatibilityWarning(0, "Votre navigateur n'est pas pleinement compatible avec PairForm. ");
				return callback_success();
			} catch(e) {
				return callback_error(e);
			}
		}
	}
}
PF.polyfillLocalStorage = function (polyfilled_object) {
	// polyfilled_object._setItem = polyfilled_object.setItem;
	polyfilled_object.setItem = function (key, value) { 
		Object.defineProperty(polyfilled_object, key, {
			configurable: true,
			get: function myProperty() {
				return value;
			}
		});
		return value;
		 // PF.__localStorage[key] = String(value); 
	};

	// polyfilled_object._getItem = polyfilled_object.getItem;
	polyfilled_object.getItem = function (key) { 
		
		if (polyfilled_object.hasOwnProperty(key))
			return polyfilled_object[key];
		else return undefined;
	};

	// polyfilled_object._removeItem = polyfilled_object.removeItem;
	polyfilled_object.removeItem = function (key) { 
		
		if (polyfilled_object.hasOwnProperty(key))
			return delete polyfilled_object[key];
		else return false;
	};

	// polyfilled_object._clear = polyfilled_object.clear;
	polyfilled_object.clear = function () { 
		for(var key in polyfilled_object){
			if (polyfilled_object.hasOwnProperty(key))
				delete polyfilled_object[key];
		}
		return true; 
	};

	return polyfilled_object;
}
PF.showCompatibilityWarning = function (top_position, message) {
	document.onreadystatechange = function () {
		if (document.readyState == "complete") {
			var warn = document.createElement("div");
			warn.style.cssText = "position:fixed; z-index: 10000; top:" + (top_position || "0") + "px; right: 0px; background-color: rgba(255, 219, 51, 1); box-shadow: rgba(237, 3, 3, 0.156863) 0px -10px 10px inset; color: rgb(18, 18, 18); font-weight: 600; padding: 8px; border-bottom-left-radius: 20px; background-position: initial initial; background-repeat: initial initial;"; 
			warn.id = "pf-incompatible-warning";
			warn.innerHTML = message || 'Votre navigateur n\'est pas compatible avec PairForm. ';
			var warn_link = document.createElement("a");
			warn_link.href = PF.globals.url.root + "utils/compatibilite";
			warn_link.innerHTML = "Cliquez ici pour plus d\'information";
			warn.appendChild(warn_link);
			document.querySelector('body').appendChild(warn);
			// alert("Les paramètres de votre navigateur ne permettent pas d'utliser PairForm. Cliquez ici pour en savoir plus.");
		}
	}
}

PF.isPairForm = function (callback_launch_app, callback_launch_injecteur, callback_error) {
	if(PF.globals.status === "ko"){
		return callback_error(PF.globals.message);
	}

	var hash_ressource = PF.getHash();

	//Si la capsule est déjà définie par Node
	if(PF.globals.capsule && PF.globals.capsule.id_capsule && !localStorage.flag_inj)
	{
		//rock'n'roll
		callback_launch_app();
	}
	else if(hash_ressource && !localStorage.flag_inj)
	{
		var http = new XMLHttpRequest();
		var url = PF.globals.url.ws + 'ressource/verifier';
		var post = 'hash_ressource=' + hash_ressource;
		http.open("POST", url, true);

		//Send the proper header information along with the request
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		http.onreadystatechange = function() {//Call a function when the state changes.
			if(http.readyState == 4 && http.status == 200) {
				var dataJSON = JSON.parse(http.responseText);
				if(dataJSON.id_capsule)
				{
					if(PF.isWebVersion())
					{
						PF.globals.capsule = dataJSON;
						
						callback_launch_app();
					}
					else 
					{
						// console.log("Enregistrée.");
						callback_launch_app();
					}
				}
				else
				{
					//TODO: Gérer l'importation d'un formulaire de validation et d'upload de la ressource.
					callback_launch_injecteur();
					// if(PF.isWebVersion())
					// {   
					//     alert("Cette ressource n'est pas encore enregistrée sur les serveurs PairForm.");
					//     // console.log("Pas enregistrée.");
					// }
					// else
					// {
					//     alert("Cette ressource n'est pas encore enregistrée sur les serveurs PairForm.");
					//     // console.log("Pas enregistrée.");
					// }
				}
			}
			else if(http.readyState == 4 && http.status == 0){
				alert("L'URL sur laquelle est stockée votre section est différente de celle renseignée sur le backoffice. Vous ne pourrez pas vous authentifier.");
				console.log("ws_erreur_url_mismatch");
			}
		}
		http.send(post);
	}
	else {
		callback_launch_injecteur();
	}
}
PF.hasLocalStorage(function callback_success () {
	PF.includeHead(PF.globals.url.public + "lib/js/require.js", function(){
		PF.includeHead(PF.globals.url.dist + "pf_app.css");
		// PF.includeHead(PF.globals.url.private + "css/dev.css");
		PF.includeHead(PF.globals.url.private + "css/toaster.css");
		PF.includeHead(PF.globals.url.private + "css/angular-loading-bar.css");
		PF.includeHead(PF.globals.url.public + "lib/css/outdatedbrowser.min.css");
		PF.includeHead(PF.globals.url.public + "lib/css/angular-carousel.min.css");
		// PF.includeHead("https://cdn.ravenjs.com/3.7.0/angular/raven.min.js");
		PF.includeHead(PF.globals.url.public + "lib/js/outdated-browser-1.1.0/outdatedbrowser.min.js");
		require.config({
			waitSeconds: 30,
			baseUrl: PF.globals.url.public + 'lib/js/',
			paths: {
				jquery: "jquery-1.9.0",
				jquerycookie: "jquery.cookie",
				jqueryuiposition: "jquery.ui.position",
				// angular: "angular.min",
				angular: "angular-1.4.5.min",
				angular_loading: "angular-loading-bar",
				angular_cookies: "angular-cookies",
				angular_animate: "angular-animate-1.4.5.min",
				angular_carousel: "angular-carousel.min",
				angular_touch: "angular-touch.min",
				angular_img_crop: "angular-img-crop.min",
				angular_toaster: "angular_toaster",
				angular_route: "angular-route.min",
				angular_sanitize: "angular-sanitize.min",
				angular_translate: "angular-translate-2.7.2.min",
				angular_translate_loader: "angular-translate-loader-static-files",
				angular_ui_router:"angular-ui-router.min",
				ng_file_upload:'ng-file-upload.min',
				ng_file_upload_shim:'ng-file-upload-shim.min',
				angular_thumbnail:'angular-thumbnail',
				ngstorage:"ngStorage.min",
				languageManager: "languageManager",
				exif: "exif",
				raven: "https://cdn.ravenjs.com/3.7.0/angular/raven.min",
				// sentry_angular: PF.globals.url.public + "js/sentry-angular",
				// sentry: "raven.min",
				injecteur: PF.globals.url.dist +"pf_injecteur",
				pf_app: PF.globals.url.dist + "pf_app"
			},
			shim: {
				'jquerycookie' : ['jquery'],
				'angular_loading' : ['angular'],
				'angular_cookies' : ['angular'],
				'angular_animate' : ['angular'],
				'angular_carousel' : ['angular', 'angular_touch'],
				'angular_touch' : ['angular'],
				'angular_img_crop' : ['angular'],
				'angular_toaster' : ['angular_animate'],
				'angular_route' : ['angular'],
				'angular_sanitize' : ['angular'],
				'angular_translate' : ['angular'],
				'angular_translate_loader' : ['angular_translate'],
				'angular_ui_router' : ['angular'],
				'ng_file_upload' : ['angular'],
				'angular_thumbnail' : ['angular'],
				'ngstorage' : ['angular'],
				'raven' : ['angular'],
				'main_model' : ["angular",
								"angular_cookies",
								"angular_toaster",
								"angular_translate",
								"angular_translate_loader",
								 "ngstorage"],
				'pf_app' : ["jquery",
									"angular_cookies",
									"angular_loading",
									"angular_carousel",
									"angular_img_crop",
									"angular_toaster",
									"angular_route",
									"angular_sanitize",
									"angular_translate",
									"angular_translate_loader",
									"exif",
									"Raven",
									"ng_file_upload_shim",
									"ng_file_upload",
									"angular_thumbnail",
									"ngstorage"],
				'injecteur' : ["jquery",
								"angular_cookies",
								"angular_loading",
								"angular_img_crop",
								"angular_toaster",
								"angular_route",
								"angular_sanitize",
								"angular_translate",
								"angular_translate_loader",
								"ngstorage"]
			}
		});
		PF.isPairForm(function callback_launch_app () {
			require(['raven'], function (Raven) {
				Raven.config('https://4bc358261036432cbfe36480b0052f61@sentry.io/99551').install();
				require(['pf_app']);
			});
		}, function callback_launch_injecteur () {
			require(['injecteur']);
		}, function callback_error (message) {
			console.log(message);
		});
	});
}, function callback_error (e) {
	PF.showCompatibilityWarning();
});