
var LARGEUR_PANNEAU_PX = "400px",
	LARGEUR_PANNEAU = 400,
	TEMPS_ANIMATION = 200,
	ID_RES_TEST = "51";

//Si on est sur le site vitrine
if(document.querySelector('meta[name="pf_vitrine"]','head'))
	PF.globals.is_vitrine = true;

if (PF.globals.is_vitrine && PF.initVitrine) {
	PF.initVitrine(pf_app);
}
/*
 *	Internationalisation
 */

pf_app.config(["$translateProvider", function ($translateProvider) {
	$translateProvider.useSanitizeValueStrategy('sanitizeParameters');
	$translateProvider.useStaticFilesLoader({
		prefix: PF.globals.url.private + 'json/',
		suffix: '/strings.json'
	});
	//Anglais par défaut, si le navigateur n'a pas la propriété définie
	//userLanguage pour IE
	var code_langue = PF._localStorage["ngStorage-code_langue_app"] ? JSON.parse(PF._localStorage["ngStorage-code_langue_app"])[0] : (window.navigator.userLanguage || window.navigator.language || "en").substr(0, 2);
	
	//S'il n'y a pas de langue compatible
	if (!PF.langue.tableau_inverse[code_langue])
		code_langue = "en";

	$translateProvider.preferredLanguage(code_langue);
	PF._localStorage.setItem("ngStorage-code_langue_app", JSON.stringify([code_langue]));

}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	cfpLoadingBarProvider.includeSpinner = false;
}]).config(['$httpProvider', function($httpProvider) {
	// $httpProvider.defaults.useXDomain = true;
	// $httpProvider.defaults.withCredentials = true;
	// $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]).config(['ThumbnailServiceProvider', function(ThumbnailServiceProvider) {
    ThumbnailServiceProvider.defaults.width = 200;
    ThumbnailServiceProvider.defaults.height = 200;
    ThumbnailServiceProvider.defaults.keep_ratio = "height";
}]);
