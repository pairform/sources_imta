pf_app.directive('pfMenuPartager', function () {
	return {
		restrict: 'E',
		template: 	'<div class="ui-tooltip-bottom comPartager-container">'+
						'<span>{{message_permalien}}</span>'+
					'</div>'
	};
});