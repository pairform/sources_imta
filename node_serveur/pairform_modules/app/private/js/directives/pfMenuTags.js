pf_app.directive('pfMenuTags', function () {
	return {
		restrict: 'E',
		template: '<div class="ui-tooltip-bottom">' +
						'<input ng-model="new_tags" placeholder="Tags séparés par des virgules">' +
						'<div class="btn-action" ng-click="ajouterTags(message, new_tags); new_tags = \'\'; ">{{"button_ajouter" | translate}}</div>' +
					'</div>'
	};
});