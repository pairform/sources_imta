var _show = Slide.prototype.show;

Slide.prototype.show = function overload (a,b,c) {
	var _show_contextual = _show.bind(this,a,b,c);
	_show_contextual();
	
	PF && PF.updateHTML && PF.updateHTML("#slideframe", this.id);
}