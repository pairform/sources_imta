
pf_app.controller('controller_recherche_profil', ["$scope", "$http", function ($scope, $http) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"mode" : 'recherche_profil',
		"defaultLimit" : 15,
		"utilisateurs_array" : []
	};
	angular.extend($scope, defaults);




	$scope.init = function () {
		if (!$scope.loaded) {
			$scope.limit = $scope.defaultLimit;
			var _scope = $scope;
			PF.get('utilisateur/liste', {}, function(data, status, headers, config) {
				var retour = data;
				_scope.utilisateurs_array = retour['profils'];
				_scope.loaded = true;
				$_pf('#profil-recherche-container').on('scroll', function() {
			        if($_pf(this).scrollTop() + $_pf(this).innerHeight() + 20 >= $_pf(this)[0].scrollHeight) {
			            $scope.$apply(function (argument) {
			            	$scope.limit += $scope.defaultLimit; 
			            });
			        }
			    });
			});
		}
	}
	$scope.ajouterUtilisateurDansReseauDepuisRecherche = function (utilisateur) {
		PF.getScopeOfController('controller_reseau').ajouterUtilisateurDansReseauDepuisRecherche(utilisateur);
	}
}]);