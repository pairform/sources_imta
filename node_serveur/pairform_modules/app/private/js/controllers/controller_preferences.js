pf_app.controller('controller_preferences', ["$scope", "toaster", "$localStorage", "$translate", function ($scope, toaster, $localStorage, $translate) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"preferences" : {
			"connexion_automatique" : false,
			"notification_mail" : false,
			"afficher_noms_reels" : false,
			"current_code_langue" : PF._localStorage["ngStorage-code_langue_app"] ? JSON.parse(PF._localStorage["ngStorage-code_langue_app"])[0] : "fr"
		},
		"tableau_labels" : PF.langue.tableau_labels
	};
	angular.extend($scope, defaults);
	angular.extend($scope, $scope.utilisateur_local.rank[$scope.capsule.id_capsule]);

	$scope.init = function () {
		// Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
		if ($scope.utilisateur_local.est_connecte) {
			var _scope = $scope;
			PF.get('utilisateur/preferences', {"id_capsule" : $scope.capsule.id_capsule} , function(data, status, headers, config) {
				var retour = data;
				if (retour["status"] == "ko") {
					//Rien de neuf
					if (retour['message'] == "ws_utilisateur_invalide") {
						$scope.deconnecter("ws_utilisateur_invalide");
					};
				}
				else{
					angular.extend(_scope, retour);
				}
				// _scope.loaded = _scope.id_utilisateur;
				
			});
		}
	}
	$scope.switchNotificationMail = function () {

		var post = {'id_utilisateur' : $scope.utilisateur_local.id_utilisateur, 'id_capsule' : $scope.capsule.id_capsule};
		ga('send', 'event', 'user', 'change_notifications_level', post);

		//On inverse la valeur (cast booléen puis string)
		var notif_mail = $scope.preferences.notification_mail = +!$scope.preferences.notification_mail;

		PF.post('utilisateur/notification/mail',post , function (retour){
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				var alert = notif_mail ? 'web_label_activer' : 'web_label_desactiver';
				toaster.pop('success', "'web_label_notification_par_mail' + '"+alert+ "' + 'web_label_sur_cette_ressource'");
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
			}
		});
	}
	$scope.switchNomsReels = function () {
		$scope.fonctionPasDisponible();
	}
	$scope.selectLangue = function (id_langue) {
		var code_langue = PF.langue.tableau[id_langue] || "en";

		$translate.use(code_langue);
		// Maj de la langue de l'application dans l'utilisateur local
		$localStorage.code_langue_app = [code_langue];
	}

	$scope.reinitialiserMessagesLus = function () {
		delete $localStorage.array_messages_lus;
		toaster.pop('success',"web_label_messages_lus_reinitialises");

		setTimeout(function () {
			location.reload();
		},1000);
	}

	$scope.effacerToutesDonnees = function () {
		$localStorage.$reset();
		PF._localStorage.clear();

		setTimeout(function () {
			location.reload();
		},400);
	}
}]);