//Obligé d'utiliser un bloc run, au cas ou angular n'a pas fini de bootstrapper
//On passe le PF._localStorage injecté en référence aux deux fonctions ci-dessous
//Parce qu'elles ne seront pas forcément capable de récuperer l'injector() de l'app
pf_app.run(["$localStorage", "$compile", function ($localStorage, $compile) {
	// Obligé de mettre un mini-time out, je sais pas pourquoi.
	setTimeout(function () {
		Message.recupererMessages(false, function callback (array_messages) {
			//Important : ajouter des bulles pour tous les liens
			PF.compileLinks(null, true);
			PF.compileContent();
			Message.contextualiserMessage($compile, array_messages);
		}, $localStorage);
	},50);
}]);

angular.element(document).ready(function() {
	PF.init();
	angular.bootstrap(document, ['pf_app']);
	$_pf('div[ng-controller=main_controller]').fadeIn(200);
	outdatedBrowser({
		bgColor: '#f25648',
		color: '#ffffff',
		lowerThan: 'transform',
		languagePath: PF.globals.url.public + 'lib/js/outdated-browser-1.1.0/lang/fr.html'
	});

	// Message.recupererMessages();
	// Message.contextualiserMessage();
});