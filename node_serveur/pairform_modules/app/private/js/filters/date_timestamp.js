
/*
* Filtres
*/

pf_app.filter('date_timestamp', ["$filter", function myDateFormat($filter){
	return function(timestamp){
	//Conversion en millisecondes
	var  tempdate= new Date(timestamp *1000);
	return $filter('date')(tempdate, "dd/MM/yy \à HH:mm");
}
}]);
