function Message (options) {
    this.contenu = "";
    this.date_creation = Math.round(Date.now() / 1000);
    this.date_modification = Math.round(Date.now() / 1000);
    this.defi_valide = "0";
    this.est_defi = "0";
    this.est_lu = "0";
    this.geo_latitude = "";
    this.geo_longitude = "";
    this.id_auteur = "0";
    this.id_capsule = "0";
    this.id_langue = "3";
    this.id_message = "0";
    this.id_message_parent = "";
    this.id_role_auteur = "1";
    this.medaille = "";
    this.nom_auteur = "Anonyme";
    this.nom_page = "";
    this.nom_tag = "";
    this.noms_auteurs_tags = {};
    this.num_occurence = "0";
    this.prive = "0";
    this.pseudo_auteur = "Anonyme";
    this.role_auteur = "Lecteur";
    this.somme_votes = "0";
    this.supprime_par = "0";
    this.tags = JSON.stringify([]);
    this.url_avatar_auteur = PF.globals.url.res + "avatars/defaut.png";
    this.utilisateur_a_vote = "0";
    this.uid_page = "";
    this.uid_oa = "";

	angular.extend(this,options);
	if (this.nom_page == "")
		this.nom_page = "_pf_res";
	if (this.uid_page == "")
		this.uid_page = "_pf_res";
}
//On passe le compile injecté en référence aux deux fonctions ci-dessous
//Parce qu'elles ne seront pas forcément capable de récuperer l'injector() de l'app
Message.contextualiserMessage = function ($compile) {
	//S'il y a des messages à remettre dans le contexte 
	if(window.location.hash)
	{
		//Tableau avec les deux paramètres
		var params = window.location.hash.replace("#/", "").split('-');
		var emplacement = params[0];
		var id_message = params[1];

		//Si c'est sur la ressource
		if (emplacement == "res") {
			displayMessages($(globals.selectorRes), id_message);
			addFocusedItem($(globals.selectorRes));		
		}
		//Si c'est sur la page
		else if (emplacement == "page")
		{
			
			var array_messages = JSON.parse(PF._localStorage["ngStorage-array_messages"])[PF.globals.capsule.id_capsule];
			//Flag 
			var elementFound = false;

			var $compile = $compile || angular.element(document).injector().get('$compile'),
				main_controller_scope = PF.getScopeOfController('main_controller');

			//Dans tous les messages de la page
			$.each(array_messages[PF.globals.nom_page], function (index, message) {
				//Si le message existe
				if (message['id_message'] == id_message)
				{
					//On essaie de retrouver l'élément DOM concerné
					var elementWithMess;

					//En fonction de la facon d'attacher les messages
					//Si on est dans le cas d'objet d'apprentissage identifiés
					if (PF.globals.styleAttacheMessages == "OA") {
						//S'il est attaché à la page
						if (message['uid_oa'] == "")
						{
							elementWithMess = $(PF.globals.selectorPage); //Profeci
						}
						//Sinon, c'est qu'il est sur un grain
						else
						{
							elementWithMess = $('[data-oauid='+message['uid_oa']+']');
						}
					}
					//Si on est dans le cas d'attache automatique sur élément bas niveau
					else{
						if (PF.isScenari() || PF.isFromLatex() || PF.isSpecialCase())
						{
							//S'il est attaché à la page
							if (message['nom_tag'] == "PAGE")
								elementWithMess = $(PF.globals.selectorPage);
							//Sinon, c'est qu'il est sur un grain
							else
								elementWithMess = $(message['nom_tag'] ,PF.globals.selectorContent).get(message['num_occurence']);						
						}
						// else
							// PF.displayAlert(J42R.get('web_label_aucun_objet_apprentissage'), "info");
					}

					//Si on l'a trouvé
					if ($(elementWithMess).length)
					{
						//On l'affiche, et on le met en valeur (true en second parametre, idMessToFocus)
						// Message.afficher($(elementWithMess), id_message);
						// angular.element('[ng-controller=main_]').injector().get('$scope').$apply(function () {

						$(elementWithMess).attr('highlighted',id_message);
						if(main_controller_scope)
							$compile(elementWithMess)(main_controller_scope);
						// });
						// addFocusedItem($(elementWithMess));

				        // $('#tplCo').animate({ 
				        //     scrollTop: $(elementWithMess).position().top 
				        // }, 600);
						elementFound = true;
					}
					
				}
			});

			// if(!elementFound)
				// displayAlert(J42R.get('web_label_erreur_msg_inexistant'),'error');
		}
		
	}
}
//On passe le PF._localStorage injecté en référence aux deux fonctions ci-dessous
//Parce qu'elles ne seront pas forcément capable de récuperer l'injector() de l'app
Message.recupererMessages = function (wipe_cache, callback, $localStorage) {
	
		if(!$localStorage){
			try{
				$localStorage = angular.element(document).injector().get('$localStorage');

			}
			catch(e){
				console.log("Injector not ready : " + e);
			}
		}

		var id_capsule = PF.globals.capsule.id_capsule,
			params,
			wipe_cache = wipe_cache || false;

		//Si on n'a pas encore de message du tout
		//Ou que l'on souhaite supprimer le cache
		if (typeof($localStorage.array_messages) == "undefined" 
			|| typeof($localStorage.array_messages[PF.globals.capsule.id_capsule]) == "undefined"
			|| wipe_cache)
			params = {'id_capsule' : id_capsule, "langues_affichage" : 3};
		//Sinon, on envoie le timestamp le plus récent
		else{
			var filtre_timestamp = {};
			filtre_timestamp[id_capsule] = $localStorage.array_messages[PF.globals.capsule.id_capsule]['_timestamp_last_message'] || 0;
			params = {"filtre_timestamp" : filtre_timestamp, "langues_affichage" : 3};
		}


		PF.get("message",params,function (data, status, headers, config)
		{			
			var array_messages = data;

			Message.importerMessages(wipe_cache, array_messages['messages']);

			//Callback function
			if(typeof(callback) === "function")
			{
				callback();
			}
		});		
}
Message.importerMessages = function  (wipe_cache, nouveaux_messages) {
	//Récuperation du localstorage angular
	var $localStorage = angular.element(document).injector().get('$localStorage');
	// $localStorage.$reset();
	//Création des tableaux de messages dans le PF._localStorage au cas ou
	if (typeof($localStorage.array_messages) == "undefined"){
		$localStorage.array_messages = {};
	}
	
	//Si le localstorage commence a devenir fat, et peut ralentir l'utilisation de la ressource, on purge les messages stockés pour les autres sections
	else if(JSON.stringify($localStorage.array_messages).length > 100000){
		var autres_capsules = Object.keys($localStorage.array_messages);
		for (var i = 0; i < autres_capsules.length; i++) {
			if (autres_capsules[i] != PF.globals.capsule.id_capsule)
				delete $localStorage.array_messages[autres_capsules[i]];
		};
	}
		//Reset du tableau des messages dans le localstorage
		// $localStorage.array_messages = {};
	
	//Si on n'a pas encore de message du tout
	//Ou que l'on souhaite supprimer le cache
	if (typeof($localStorage.array_messages[PF.globals.capsule.id_capsule]) == "undefined"
		|| wipe_cache){
		$localStorage.array_messages[PF.globals.capsule.id_capsule] = {};
	}

	var	current_id_user = PF.globals.utilisateur_local.id_utilisateur,
		array_messages = $localStorage.array_messages[PF.globals.capsule.id_capsule],
		_timestamp_last_message = array_messages['_timestamp_last_message'] || 0,
		array_messages_lus = $localStorage.array_messages_lus = $localStorage.array_messages_lus || [],
		key_id_page = "nom_page";

	//Si on est dans le cas d'objet d'apprentissage identifiés
	if (PF.globals.styleAttacheMessages == "OA") {
		key_id_page = "uid_page";
	}

	//Pour chaque message
	$.each(nouveaux_messages, function () {
		var _this = new Message(this),
			//Présence & index du message dans le localstorage
			index_of_message = (typeof(array_messages[_this[key_id_page]]) == 'undefined') ? -1 : array_messages[_this[key_id_page]].map(function (e) {return(e.id_message)}).indexOf(_this.id_message);

		//Si le message n'est pas encore là
		if (index_of_message == -1)
		{
			//Messages sur la page courante
			//Sinon, message sur les autres pages / ressource
			//Stockage dans le $localstorage
			if(typeof(array_messages[_this[key_id_page]]) == 'undefined')
					array_messages[_this[key_id_page]] = [];

			array_messages[_this[key_id_page]].push(_this);
		}
		//Si le message est déjà enregistré, mais a été modifié depuis
		else if(array_messages[_this[key_id_page]][index_of_message]['date_modification'] < _this.date_modification)
		{
			array_messages[_this[key_id_page]][index_of_message] = _this;
		}

		//Dans tous les cas, on garde en stock le dernier timestamp du message le plus récent
		if(_this.date_modification > _timestamp_last_message)
			_timestamp_last_message = _this.date_modification;
	});

	//On réenregistre le timestamp dans le PF._localStorage
	$localStorage.array_messages[PF.globals.capsule.id_capsule]['_timestamp_last_message'] = _timestamp_last_message;

	//Important : ajouter des bulles pour tous les liens
	var a = $('a[href]:not([href^=#])').filter(function (index, object) {
		var normalized = PF.normaliserURL($(this).attr('href'));
		return typeof(array_messages[normalized]) !== "undefined";
	});
	a.attr('pf-commentable', '');

	var $compile = angular.element(document).injector().get('$compile');
	$compile(a)(PF.getScopeOfController('main_controller'));
}

Message.getSavedSortForType = function (display_type) {
	//Récupération des valeurs actuelles
	var sorts_per_displays = Message.getLocalStorageSortType();

	//On renvoie le mode correspondant
	return sorts_per_displays !== false ? sorts_per_displays[display_type] : 'default';

}
/*
	* LocalStorage sorts_per_displays :
	* {
	*   'transversal' : 'date' || 'votes',
	*   'user' : 'date' || 'votes',
	*   'normal' : 'date' || 'votes'
	* }
	*
	* 'default' quand erreur / rien de trouvé / stocké.
	*/
Message.getLocalStorageSortType = function  () {
	if (typeof(PF._localStorage) != "undefined")
	{
		//S'il n'y a pas encore de type, on met les valeurs par défaut
		if (typeof(PF._localStorage.sorts_per_displays) == 'undefined') {
			var sorts_per_displays = {'transversal' : 'date', 'user' : 'date', 'normal' : 'votes'};
			PF._localStorage.setItem("sorts_per_displays", JSON.stringify(sorts_per_displays));
		}
		else{
			//Récupération de la valeur stockée
			var sorts_per_displays = JSON.parse(PF._localStorage.sorts_per_displays);
		}
		return sorts_per_displays;

	}
	else 
		return false;
}

Message.setSavedSortForType = function (sort_type, display_type) {
	//Récupération des valeurs actuelles
	var sorts_per_displays = getLocalStorageSortType();

	//Si les valeurs sont bien récupérées
	if(sorts_per_displays !== false){
		//On enregistre le type
		sorts_per_displays[display_type] = sort_type;

		PF._localStorage.sorts_per_displays = JSON.stringify(sorts_per_displays);

		return true;
	}
	else 
		return false;
}
Message.getNextSortType = function (current_sort_type) {
	var sorts_array = ['date', 'votes'];

	//On récupère l'index du prochain mode de tri, et si on sort de la limite du tableau, on revient à 0
	var next_index = (sorts_array.indexOf(current_sort_type) +1) >= sorts_array.length ? 0 : sorts_array.indexOf(current_sort_type) +1;

	return sorts_array[next_index];
}
Message.isVisible = function  (message, callbackTrue, callbackFalse) {
	if(message.supprime_par == 0)
	{
		if (typeof(callbackTrue) == "function")
			callbackTrue();
	}
	else{
		//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
		if((message.id_user == "isConnected()") || "isAdmin()" || ("getRank()" >= 3))
		{
			if (typeof(callbackTrue) == "function")
				callbackTrue();
		}
		else{
			if (typeof(callbackFalse) == "function")
				callbackFalse();
		}
	}
}

Message.afficher = function (idMessToFocus, idUser) {
		
	PF.getScopeOfController('controller_messages').afficherMessagesTransversal(idMessToFocus, idUser);

}