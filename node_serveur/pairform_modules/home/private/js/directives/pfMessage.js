pf_app.directive('pfMessage', function () {
	
	return {
		restrict: 'E',
		// templateUrl:  PF.globals.url.views + 'partial/message',
		link: function (scope, element, attr) {
			scope.init = function (message_updated) {
				var msg = message_updated || scope.message;

				element.addClass('rank-' + msg.id_role_auteur);

				if(msg.supprime_par != '0'){
					element.addClass('supprime'); 
				}
				else
					element.removeClass('supprime'); 	

				if((msg.id_message_parent != '') && !scope.transversal)
					element.addClass('reponse'); 
				else
					element.removeClass('reponse');

				if(msg.est_defi >= 1)
					element.addClass('est_defi');
				
				element.data({
					"id_message" : msg.id_message,
					"id_auteur" : msg.id_auteur,
					"id_role_auteur" : msg.id_role_auteur,
					"supprime_par" : msg.supprime_par,
					"id_langue" : msg.id_langue,
					"prive" : msg.prive,
					"est_defi" : msg.est_defi >= 1 ? msg.est_defi : '',
					"defi_valide" : msg.defi_valide == true ? msg.defi_valide : '',
					"id_message_parent" : msg.id_message_parent != '' ? msg.id_message_parent : ''
				});
			}

			//Url pour images
			scope.res_url_root = PF.globals.url.root;
			scope.res_url_private = PF.globals.url.private;
			scope.res_url_public = PF.globals.url.public;

			scope.init();

			scope.$watch('message',function (message_updated) {
				scope.init(message_updated);
			}, true)
		}
	};
});