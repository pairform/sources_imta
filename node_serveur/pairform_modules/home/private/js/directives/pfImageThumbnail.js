/**
* Directive permettant d'afficher une liste horizontal de pieces jointes
* Elle prend en paramètre images qui correspond au tableau des pieces jointes qui seront affichées
*/
pf_app.directive('pfImageThumbnail', ['PJService', function(PJService){

	return {
		restrict: 'E',
		scope: {
			images: "="
		},
		template: 	'<div class="comPjs">' +
						'<div class="comPj" ng-repeat="image in images" ng-init="popup_visible = false">' +
							'<img class="comImage" ng-click="afficherPJ($index)" ng-src="{{image.thumbnail_url}}">' +
						'</div>' +
					'</div>',
		
		link: function ($scope, element, attrs) {
			//Image de chargement
			var defaultImg = PF.globals.url.root + "public/img/loading.svg";

			//ATTENTION : ne pas mettre ces variables dans la variable image car les images serait sauvé dans le localstorage
			function init(){

				//Pour toutes les images on va récupérer les thumbnails 
				angular.forEach($scope.images, function(image){
					//debugger
					// image.thumbnail_url = defaultImg;

					PJService.getThumbnail(image);
				});
			}
			
			/**
			* Affichage des PJ dans le carrousel
			* Fonction locale au scope pour éventuelle modification ultérieure
			* Parametre : index courant de la PJ
			*/
			$scope.afficherPJ = function (index) {
				$scope.$parent.afficherCarrousel($scope.images, index);
			}
			/**
			* Fonction qui va permettre de télécharger la piece jointe en version complète quand la souris sera en hover sur la thumbnail
			* Paramètre : image : une piece jointe du tableau
			*/
			$scope.show = function(image){
				//if($scope.fullImage === undefined){
					$scope.fullImage = defaultImg;

					PJService.getPJ(image).then(function(data){
						$scope.fullImage = data;
					}, function(data){/*Erreur*/});
				//}
			}
			
			//On appel la méthode init par défault
			//On met un timer pour ne pas ajouter du travail a l'ouverture du panneau
			//But : ne pas retarder et ralentir l'interface
			setTimeout(function(){
				init();
			}, 300);
	  	}
	};
}]);