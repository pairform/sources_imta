pf_app.directive('timeline', ["$document", "$filter", function($document, $filter) {
	return {
		restrict: 'E',
		scope: {
			items: "="
		},
		link: function(scope, element, attr) {
			scope.items = scope.items || [];
			function handleDragOver(e) {
				if (e.preventDefault) {
				  e.preventDefault(); // Allows us to drop.
				}

				element.addClass('pf-droppable-hover');

				return false;
			};

			function handleDragEnter(e) {
				element.addClass('pf-droppable-hover');
			};

			function handleDragLeave(e) {
				// this/e.target is previous target element.
				element.removeClass('pf-droppable-hover');
			};

			function handleDrop(e) {
				// this/e.target is current target element.

				if (e.stopPropagation) {
				  e.preventDefault(); // stops the browser from redirecting.
				  e.stopPropagation(); // stops the browser from redirecting.
				}

				if (e.target == element[0] || e.target.parentElement == element[0]) {

					var data_string = e.originalEvent.dataTransfer.getData("text/javascript");
					if (data_string && data_string != "undefined") {
						var item = JSON.parse(data_string);
						

						if(!PF.trouverElementListe(item.$$hashKey, scope.items, "$$hashKey")){
							scope.$apply(function () {
								item.date_diffusion = $filter('date')(new Date(), 'dd/MM/yy');
								scope.items.push(item);
							})
							element.removeClass("pf-droppable pf-droppable-hover");
						}

						// PF.post("message/deplacer", post_data, function (data) {
						// 	var retour = data;
						// 	if (retour['status'] == 'ok')
						// 	{
						// 		scope.$root.$broadcast("pf:dropped", message);
						// 		scope.highlighted = message.id_message;
						// 		scope.afficherDepuisOA(scope.highlighted);
						// 	}
						// 	else{
						// 		scope.$root.$broadcast("pf:dragend");
						// 	}
						// });
					

					}
				}
			};

			scope.$on("pf:dragstart", function () {
				element.on('dragenter', handleDragEnter);
				element.on('dragover', handleDragOver);
				element.on('dragleave', handleDragLeave);
				element.on('drop', handleDrop);
				
				element.addClass("pf-droppable");


			});

			scope.$on("pf:dragend", function () {
				// body...
				element.removeClass("pf-droppable");

				element.off('dragenter');
				element.off('dragover');
				element.off('dragleave');
				element.off('drop');
			});

			scope.orderByDate = function(item) {
			    var parts = item.date_diffusion.split('-');
			    var date = new Date(parseInt(parts[0]), 
			                        parseInt(parts[1]), 
			                        parseInt(parts[2]));

			    return date;
			};


		}
	}
}]);