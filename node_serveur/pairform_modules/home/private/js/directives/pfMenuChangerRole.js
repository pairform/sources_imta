pf_app.directive('pfMenuChangerRole', function () {
	return {
		restrict: 'E',
		template: 	
			'<ul class="rank-upgrade-list">' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 4)">{{"label_user_expert" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 3)">{{"label_user_animateur" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 2)">{{"label_user_collaborateur" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 1)">{{"label_user_participant" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 0)">{{"web_label_revenir_role_normal" | translate}}' +
				'</li>' +
			'</ul>'
	}
});