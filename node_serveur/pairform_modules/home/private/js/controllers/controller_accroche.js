pf_app.controller('controller_accroche', ["$scope", "$localStorage", function ($scope, $localStorage) {
	var defaults = {
		"visible" : false
	};

	angular.extend($scope, defaults);
	
	if (!$localStorage.premiere_visite && !PF.globals.is_vitrine && !/(android|ipad|iphone|ipod|mobile)/i.test(navigator.userAgent||navigator.vendor||window.opera)){
		var _accroche = new PanneauModal({'nom_controller' : 'controller_accroche'});
		_accroche.togglePanneau();
	}

	$scope.remove = function () {
		$localStorage.premiere_visite = true;
		this.panneau.cacherPanneau();
	}
	$scope.fermerEtAfficherConnexion = function () {
		$scope.remove();
		$scope.afficherConnexion("enregistrement-0");
	}
	$scope.fermerEtAfficherPresentation = function () {
		$scope.remove();
		$scope.afficherPresentation();
	}

}]);
