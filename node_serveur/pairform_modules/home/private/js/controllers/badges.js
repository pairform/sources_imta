pf_app.controller('badges', ["$scope", "$routeParams", function ($scope, $routeParams) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"id_utilisateur" : $routeParams.id_utilisateur == "me" ? $scope.utilisateur_local.id_utilisateur : parseInt($routeParams.id_utilisateur),
		"badges" : [],
	};

	angular.extend($scope, defaults);

	$scope.init = function () {
		PF.get('utilisateur/profil', {"id_utilisateur": $scope.id_utilisateur} , function(retour, status, headers, config) {
			if (retour["status"] == "ok") {
				var _utilisateur = retour["datas"];
				
				_utilisateur.id_utilisateur = $scope.id_utilisateur;
				$scope.utilisateur = new Utilisateur(_utilisateur);
			}
		});
	}
	$scope.init();
}]);