pf_app.controller('avatar_upload', ["$scope", "$http", "toaster", "$timeout", "$localStorage", function ($scope, $http, toaster, $timeout, $localStorage) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"utilisateur" : $localStorage.utilisateur_local,
		"avatar" : ''
	};

	angular.extend($scope, defaults);
	$scope.init = function () {
		// $scope.src_avatar = $scope.utilisateur_local.avatar_url;
	}
	// récupération des informations sur le champ "Logo"
	$scope.recupererLogo = function($files) {
	
		// affichage du logo selectionné
		var file_reader = new FileReader();

		file_reader.onload = function (e) {
			// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
			$timeout(function () {
				$scope.src_avatar = e.target.result;
			}, 500);
		};

		file_reader.readAsDataURL($files[0]);
	}

	$scope.uploadAvatar = function (avatar) {

		//Si l'utilisateur essaie de sauvegarder une image vide
		if(avatar && !$scope.src_avatar){
			return;
		}
		if(avatar) {
			avatar = avatar.replace("data:image/png;base64,", "")
		}
		PF.post("utilisateur/avatar", {"avatar" : avatar}, function(data, status, headers, config) {
		// $.ajax({
		   //              url: PF.globals.url.ws + 'utilisateur/avatar',
		   //              type: 'POST',
		   //              async: true,
		   //              cache: false,
		   //              contentType: false,
		   //              processData: false,
		   //              data: form
		   //          }).done(function(data) { 
			if (data['status'] == "ok") {
				
			}
			else if (data['status'] == "up") {
				$scope.utilisateur_local.avatar_url = data["data"]["url"];
			}
			else{
				if (data['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					toaster.pop("error", data['message']);
					// toaster.pop("error", "ws_erreur_contacter_pairform");
			}
		})
	}
}]);
