
pf_app.controller('utilisateurs', ["$scope", "$http", "$element", function ($scope, $http, $element) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"mode" : 'recherche_profil',
		"utilisateurs_array" : []
	};
	angular.extend($scope, defaults);

	$scope.defaultLimit = 10;
	$scope.limit = $scope.defaultLimit;

	$scope.increaseLimit = function () {
		$scope.limit += $scope.defaultLimit;
	}

	$element.on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() + 20 >= $(this)[0].scrollHeight) {
            $scope.increaseLimit();
        }
    });
	$scope.init = function () {
		if (!$scope.loaded) {
			var _scope = $scope;
			PF.get('utilisateur/liste', {}, function(data, status, headers, config) {
				var retour = data;
				_scope.utilisateurs_array = retour['profils'];
				_scope.loaded = true;
			});
		}
	}
	$scope.ajouterUtilisateurDansReseauDepuisRecherche = function (utilisateur) {
		PF.getScopeOfController('reseau').ajouterUtilisateurDansReseauDepuisRecherche(utilisateur);
	}
	$scope.init();
}]);