pf_app.service('MessageService', [function(){
	var routeMessage = "message";
	var url = PF.globals.url.ws + routeMessage;
	
	var service = {
		envoyer: function(data_requete, files, callback_succes, callback_echec){
			PF.request_with_file("put", "message", data_requete, files, callback_succes, callback_echec);
		}
	}

	return service;
}]);