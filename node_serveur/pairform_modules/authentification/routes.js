var web_services = require("../webservices/lib/webServices"),
		  CONFIG = require("config");



//Page autonome de login

// app.get("/login", function (req, res) {		
// 	res.render(__dirname + "/views/login",{
// 		url_to_follow : "",
// 		// unauthorized : true
// 	});
// });


module.exports = function(app, passport) {

	// resetPassword
	app.get("/reset/:token", function(req, res) {
		web_services.authentification.resetPasswordStep2(
			req.params,
			function(retour) {
				//Flag de token invalide :
				//Le token est soit expiré, soit non retrouvé dans la BDD
				var token_expired = retour.status == "ko";
				//On passe ça en paramètre à la view (dossier root views)
				res.render("reset_password", {
					"token_expired" : token_expired,
					"url_serveur_node" : CONFIG.app.urls.serveur_node
				});
			}
		);
	});


	// resetPassword
	app.get("/validation/:token", function(req, res) {
		web_services.authentification.validerEmail(
			req.params,
			function(retour) {
				var context = {
					"url_serveur_node" : CONFIG.app.urls.serveur_node
				};
				//Flag de token invalide :
				//Le token est soit expiré, soit non retrouvé dans la BDD
				if (retour.status == "ko") {
					context.erreur = retour.message;
				}
				else{
					context.utilisateur = retour.data.utilisateur;
				}
				//On passe ça en paramètre à la view (dossier root views)
				res.render("validation_compte", context);
			}
		);
	});


}