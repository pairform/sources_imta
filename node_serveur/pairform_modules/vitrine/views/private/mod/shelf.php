<?php
		require_once("fonctions.php");
			//TODO:Faire le module de construction de la liste de ressources.
			//WebServ : listeApplications.php
			$retour = sendRequest("http://podcast.mines-nantes.fr/SCElgg/elgg-1.8.13/webServices/ressources/listerRessources.php",array('os'=>'web','version'=>'1.5'));
			$messages = sendRequest("http://podcast.mines-nantes.fr/SCElgg/elgg-1.8.13/webServices/messages/recupererNombreMessages.php",array('os'=>'web','version'=>'1.5'));

			$ressources = $retour['ressources'];

			// error_log(print_r($ressources));
			$resParDomaine = array();
			$resParEtablissement = array();
			$sortStyle = isset($_GET['sort']) ? $_GET['sort'] : 'domaine'; //Default

			foreach ($ressources as $key => $res) {
				if(!isset($resParEtablissement[$res['etablissement']]))
				{
					$resParEtablissement[$res['etablissement']] = array();
				}
				array_push($resParEtablissement[$res['etablissement']],$res);

				if(!isset($resParDomaine[$res['theme']]))
				{
					$resParDomaine[$res['theme']] = array();
				}
				array_push($resParDomaine[$res['theme']],$res);
				
			}
			echo '<div id="shelf-wrapper">';

			if($sortStyle == "domaine")
			{
				foreach ($resParDomaine as $key => $domaine) {
					echo '<div class="shelf">
							<div class="res-container">';

					foreach ($domaine as $cle => $res) {
						// error_log(print_r($res,true));
						// echo '<div data-count="'.(array_key_exists($res['id_capsule'], $messages) ? ''.$messages[$res['id_capsule']] : '0').'"><a href="'.$res['url_web'].'"><img class="res-icon" alt="'.$res['nom_court'].'" src="data:image/png;base64,'.$res['icone_blob'].'"><span>'.$res['nom_court'].'</span></a><pre class="ui-tooltip-top">'.$res['description'].'<br><br>Auteurs : '.$res['auteur'].'</pre></div>';
						echo '<div data-count="'.(array_key_exists($res['id_capsule'], $messages) ? ''.$messages[$res['id_capsule']] : '0').'" title="'.$res['description'].'&#013;&#013;Auteurs : '.$res['auteur'].'"><a href="'.$res['url_web'].'"><img class="res-icon" alt="'.$res['nom_court'].'" src="data:image/png;base64,'.$res['icone_blob'].'"><span>'.$res['nom_court'].'</span></a></div>';

			
					}
					echo '</div>
						<div class="shelf-title">'.$key.'</div>
						
					</div>';
				}
			}
			else if($sortStyle == "etablissement")
			{
				foreach ($resParEtablissement as $key => $etablissement) {
					echo '<div class="shelf">
							<div class="res-container">';
					foreach ($etablissement as $cle => $res) {
						echo '<div><a href="'.$res['url_web'].'"><img class="res-icon" alt="'.$res['nom_court'].'" src="data:image/png;base64,'.$res['icone_blob'].'"><span>'.$res['nom_court'].'</span></a></div>';
			
					}
					echo '</div>
						<div class="shelf-title">'.$key.'</div>
						
					</div>';
				}
			}
			echo '</div>';
		?>