<?php
    /*
     * Liste les ressources téléchargeables
    * Paramètres :
    *
    * id_elgg (int) - id de l'utilisateur
    *
    * Retour
    "ressources":[
    {
    "id_ressource":"2",
    "nom_court":"E-diagnostic",
    "nom_long":"E-diag - Cours de sensibilisation aux principes de diagnostic",
    "url_mob":"http://imedia.emn.fr/SupCast/Ressources/E-diag",
    "icone_blob":"iVBORw0KGgoJggg==",
    "description":"Ce cours est une sensibilisation à des techniques de diagnostic de fiabilité et/ou d'erreur dans le contrôle de procédés industriels. Il illustre par de nombreux exemples différentes démarches du diagnostic, en précisant ce que l'on doit diagnostiquer et comment on le fait. Les aspects homme-machine y sont traités. ",
    "taille":"87",
    "etablissement":"UVHC",
    "theme":"Diagnostic",
    "date_release":"2012-03-09",
    "date_update":"2012-11-15"
    },


     */
    
    // error_log("**********************************************");
    // error_log("ListeApplication.php appellé"); 
    // error_log("**********************************************");

    
	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
    
	$os = $_POST['os'];
  $version = $_POST['version'];
  $id_elgg = isset($_POST['id_elgg']) ? $_POST['id_elgg'] : 0;
  

  // error_log(print_r($_POST,true));
  
  $query = sprintf("SELECT 
      `id_ressource`,
      `cape_ressources`.`nom_court`,
      `cape_ressources`.`nom_long`,
      `url`,
      `url_web`,
      `icone`,
      `cape_ressources`.`icone_blob`,
      `description`,
      `visible`,
      `cape_etablissements`.`nom_court` etablissement,
      `cape_etablissements`.`icone_blob` icone_etablissement,
      `cape_themes`.`nom_long` theme,
      `date_update`,
      `date_release`,
      `taille`
      FROM `cape_ressources`, 
      `cape_etablissements`,
      `cape_themes` where 
      `cape_themes`.`id_theme` = `cape_ressources`.`theme` 
      and `cape_etablissements`.`id_etablissement` = `cape_ressources`.`etablissement` ");   
                   
  $resultQuery = mysql_query($query);
  if(!$resultQuery)
  {
    error_log(mysql_error());
    exit();
  }
    
                   
  $rows = array();
  

  
  while ($row = mysql_fetch_assoc($resultQuery)) 
  {
    // error_log('Icone Blob : ' . $row['icone_blob']); 
    //error_log('Ressource : ' . $row['nom']); 
    $visible = $row['visible'];
    
    //error_log('$visible (default) : '.$visible);
        //On checke si la ressource est en développement
    //Visible == false?
    if($visible == 0)
    {
              //Gestion des ressources en développement : récupération de l'ID user
      $req = "SELECT id_ressource FROM cape_utilisateurs_ressources WHERE id_elgg = '$id_elgg'";
      $res = mysql_query($req);
      
      while($row = mysql_fetch_assoc($res))
      {
        //Si il a le droit, on dit à l'application web que la ressource est visible.
        if($row['id_ressource'] == $id_elgg)
          $visible = 1;
          
        //error_log('$visible (modifié) : '.$visible);
      }
    }
    if($visible)
      array_push($rows, array('id_ressource' => $row['id_ressource'],
       'nom_court' => $row['nom_court'],
       'nom_long' => $row['nom_long'],
       'url_mob' => $row['url'],
       'icone_blob' => base64_encode($row['icone_blob']),
       'icone_etablissement' => base64_encode($row['icone_etablissement']),
       'description' => $row['description'],
       'taille' => $row['taille'],
       'etablissement' => $row['etablissement'],
       'theme' => $row['theme'],
        'date_release' => $row['date_release'],
        'date_update' => $row['date_update']));
  }
  // error_log("Rows : " . print_r($rows,true));
  $return = json_encode(array('ressources' => $rows));
  print ($return);
  // error_log(print_r($return,true));

    
        
                             
?>