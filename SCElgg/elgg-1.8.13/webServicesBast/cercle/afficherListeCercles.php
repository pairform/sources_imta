<?php


  //********************************************//
  //*********** afficherListeCercle.php ***********//
  //********************************************//
  
  /* affiche la liste des cercles de l'utilisateur connecte.
   *
   * 
   * 
   *
    {
    "cercle":[
                {
                "id":"4",
                "name":"classe1",
                "owner_guid":"74",
                "site_guid":"1"
                },
                {
                "id":"7",
                "name":"supercollec",
                "members":[
                    {
                    "name":"Admin",
                    "guid":35,
                    "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
                    },
                    {
                    "name":"Azerty",
                    "guid":68,
                    "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
                    }
]
                }
              ] 
    }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  $os = $_POST['os'];
  $version = $_POST['version'];
  
  $user = elgg_get_logged_in_user_entity();

  // Utilisateur pas connecté
  if ( empty($user)) {
    $return = json_encode(array("cercles"=>array()));
  }else{
    $guid = $user->guid;
    $collections = get_user_access_collections ($guid);

    foreach ($collections as &$collection) {

      $members = get_members_of_access_collection($collection->id);
      $members_final = array();
      foreach ($members as &$member) {
        $members_final[] = (object) array('name' => $member->name, 'guid' => $member->guid,'image' => $member->getIcon('small'));
      }

      // Dissociation classe et cercle. avec l'id du cercle/classe/

      $collection_id = $collection->id;
      $requete = mysql_query("SELECT * FROM `cape_classes` WHERE `id_classe` = $collection_id");
      $row_message   = mysql_fetch_assoc($requete);  
           
      // Si resultat vide , c'est un cecle normal 
      if(empty($row_message)){
        $cercles[] = (object) array('id'=>$collection->id,'name'=>$collection->name,'members'=>$members_final);
        $namesCercles[] = $collection->name;
      } else{
        $classes[] = (object) array('id'=>$collection->id,'name'=>$collection->name,'members'=>$members_final,'cle'=>$row_message['cle']);
        $namesClasses[] = $collection->name;
      }
      // On range par odre alphhabetique.
      if ( !empty($namesCercles)) array_multisort($namesCercles,$cercles);
      if ( !empty($namesClasses)) array_multisort($namesClasses,$classes);
    }
    if ( empty($cercles) ) $cercles = array();
    if ( empty($classes) ) $classes = array();
  }

  $return = json_encode(array("cercles"=>$cercles,"classes"=>$classes));
  echo $return;
  

?>