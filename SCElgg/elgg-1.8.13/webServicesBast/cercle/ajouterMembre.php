<?php

  //********************************************//
  //*********** ajouterMembre.php ***********//
  //********************************************//
  
  /* affiche la liste des membre d'un cercle.
   *
   * Paramètres :
   *
   * id_utilisateur (int) - l'id de l'utilisateur
   * id_collection (int) - l'id de la collection
   * 
   * Retour : 
   * {"status":"ok"
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  $os = $_POST['os'];
  $version = $_POST['version'];


  $id_utilisateur = $_POST['id_utilisateur'];
  $id_collection= $_POST['id_collection'];
  if (add_user_to_access_collection ($id_utilisateur, $id_collection)){
    $return = json_encode(array('status' => "ok"));
  }
  else
  {
    $return = json_encode(array('status' => "ko"));
  }
  echo $return;
  

?>