<?php

  //********************************************//
  //*********** creerCercle.php ***********//
  //********************************************//
  
  /* Creer un cercle.
   *
   * Paramètres :
   *
   * nom (string) nom du cercle.
   * 
   * Retour : 
   * {"status":"ok"
   * “message” : “message d’erreur “ // uniquement si status est “ko”
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerSucces.php");
  $os = $_POST['os'];
  $version = $_POST['version'];

  $nom = $_POST['nom'];
  $user = elgg_get_logged_in_user_entity();
  $userGuid = $user->guid;
  
  // Nom identiques
  $collections = get_user_access_collections ($userGuid);
  foreach ($collections as &$collection) {
      if ($collection->name ==  $nom) {
        $return = json_encode(array('status' => "ko" ,'message' => "Ce nom est déjà utilisé."));
        echo $return;
        exit();

      };
  }


  if (create_access_collection  ( $nom, $userGuid )){
    $return = json_encode(array('status' => "ok"));
    gagnerSuccesCercle();
  }
  else
  {
    $return = json_encode(array('status' => "ko" ,'message' => "La création a échoué"));
  }
  echo $return;
  

?>