<?php

   //********************************************//
  //*********** supprimerCercle.php ***********//
  //********************************************//
  
  /* supprimer un cercle ou une classe
   *
   * Paramètres :
   *
   * id_collection (string) id du cercle/classe.
   * 
   * Retour : 
   * {"status":"ok"
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  $os = $_POST['os'];
  $version = $_POST['version'];

  $id_collection = $_POST['id_collection'];
  if(delete_access_collection($id_collection)){
    $return = json_encode(array('status' => "ok"));
  }
  else
  {
    $return = json_encode(array('status' => "ko"));
  }

  mysql_query("DELETE FROM `SCElgg`.`cape_classes` WHERE `cape_classes`.`id_classe` = $id_collection");
  echo $return;
  
?>