<?php

  //********************************************//
  //*********** rejoindreClasse.php ***********//
  //********************************************//
  
  /* rejoindre une classe
   *
   * Paramètres :
   *
   * cle (string) cle de la classe.
   * 
   * Retour : 
   * {"status":"ok"
   * “message” : “message d’erreur “ // uniquement si status est “ko”
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  $os = $_POST['os'];
  $version = $_POST['version'];

  $cle = $_POST['cle'];
  $user = elgg_get_logged_in_user_entity();
  $user_guid = $user->guid;

  $result = mysql_query("SELECT `cape_classes`.`id_classe`,`sce_access_collections`.`name` 
    FROM `cape_classes`,`sce_access_collections` 
    WHERE `cle` = '$cle' 
    AND id_classe = id ");
  $row_message = mysql_fetch_assoc($result);
  
   if($row_message){
      
      $id = $row_message['id_classe'];
      $name = $row_message['name'];
      if ( get_access_collection($id)->owner_guid == $user_guid){
        $return = json_encode(array('status' => "ko" ,'message' => "vous être le propriétaire de cette classe"));
      }else{
        if (add_user_to_access_collection ($user_guid, $id)){
          $return = json_encode(array('status' => "ok" ,'message' => "clé acceptée"));
        }
        else
        {
          $return = json_encode(array('status' => "ko",'message' => "vous êtes déjà dans cette classe"));
        }
      }
      
    }else{
       $return =  json_encode(array('status' => "ko",'message' => "clé invalide"));
    }
  echo $return;
  


?>