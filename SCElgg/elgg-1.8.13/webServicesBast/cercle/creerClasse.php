<?php

  //********************************************//
  //*********** creerClasse.php ***********//
  //********************************************//
  
  /* Creer une classe.
   *
   * Paramètres :
   *
   * nom (string) nom de la classe.
   * 
   * Retour : 
   * {"cle":"dascd"
   * “message” : “message d’erreur “ // uniquement si status est “ko”
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerSucces.php");
  $os = $_POST['os'];
  $version = $_POST['version']; 
  

  $nom = $_POST['nom'];
  $user = elgg_get_logged_in_user_entity();
  $userGuid = $user->guid;
  if (empty($user) || !isset($_POST['nom'])){
    $return = json_encode((object) array('status' => "ko" ,'message' => "La création a échoué"));
    echo $return;
    exit();
  }

  // Nom identiques
  $collections = get_user_access_collections ($userGuid);
  foreach ($collections as &$collection) {
      if ($collection->name ==  $nom) {
        $return = json_encode(array('status' => "ko" ,'message' => "Ce nom est déjà utilisé."));
        echo $return;
        exit();

      };
  }

  
  $id = create_access_collection  ( 
    $nom,
    $userGuid
  );
  //######
  // Generation de cle
  //#######

  $supercle =  chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25));;
  $requete = mysql_query("SELECT * FROM `cape_classes` WHERE `cle` = '$supercle'");
  //$row_message   = mysql_fetch_assoc($requete);  
  while (!$requete){
    $supercle =  chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25)).chr(97 + mt_rand(0, 25));
    $requete = mysql_query("SELECT * FROM `cape_classes` WHERE `cle` = '$supercle'");
    //$row_message   = mysql_fetch_assoc($requete);  
  }
  

  $object = (object) array('status' => "ok" , 'cle' => "$supercle" );
  
  $result = mysql_query("INSERT INTO `cape_classes` (`id_classe`, `cle`) 
         VALUES ($id, '$supercle')");
        
  if(!$result){
     echo json_encode((object) array('status' => "ko" ,'message' => "La création a échoué"));
     exit();
  }
  gagnerSuccesRejoindreClasse();
  $return = json_encode($object);
  echo $return;
  
  

?>