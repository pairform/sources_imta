<?php

   //********************************************//
  //*********** supprimerMembre.php ***********//
  //********************************************//
  
  /* supprimer un membre d'une classe ou cercle.
   *
   * Paramètres :
   *
   * id_utilisateur (int) id de l'utilisateur.
   * id_collection (int) id du cercle/classe.
   * 
   * Retour : 
   * {"status":"ok"
   * “message” : “message d’erreur “ // uniquement si status est “ko”
   * }
   */
  
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  $os = $_POST['os'];
  $version = $_POST['version'];


  $id_utilisateur = $_POST['id_utilisateur'];
  $id_collection = $_POST['id_collection'];
  if (remove_user_from_access_collection ($id_utilisateur, $id_collection )){
    $return = json_encode(array('status' => "ok"));
  }
  else
  {
    $return = json_encode(array('status' => "ko" ,'message' => "problème de connexion"));
  }
  echo $return;
  


  


?>