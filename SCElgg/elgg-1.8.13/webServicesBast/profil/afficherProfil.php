<?php

  //********************************************//
  //*********** afficherProfil.php ***********//
  //********************************************//
  
  /* Affiche le profil d'un membre.
   *
   * Paramètres :
   * id_utilisateur(int) - l'id de l'utilisateur , s'il est absent , prend l'utilisateur connecté
   *
   * 
   *
   *
      {
      "name":"bast",
      "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=medium",
      "email":"email@email.com",
      "etablissement" : "EMN",
      "res":[
              {
              "nom":"E-diagnostic",
              "score":"4",
              "max_score":"1200",
              "categorie":"Participant"
              }
          ],
      "nombreSucces":2,
      "succes":[
                {
                "succes":"Participant",
                "description":"Devenir participant",
                "image":"1.jpg"
                },
                {
                "succes":"SuperCaster 100+",
                "description":"Obtenir plus de 100 points",
                "image":"2.jpg"
                }
      ],
      "succesNonGagnes":[
                {
                "succes":"Petite plume",
                "description":"Ecrire 5 messages",
                "image":"succes_default"
                }
      ],
      "succesDerniers":[
                {
                "image":"image.jpg"
                }
      ],
      "isOwner":"yes"
      }
   * 
   */
  
 
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $os = $_POST['os'];
  $version = $_POST['version'];

  
  // On regarde s'il l'utilateur est loggé pour afficher sa page en tant que owner.
  if ( isset($_POST['id_utilisateur'])) {
    $id_utilisateur = $_POST['id_utilisateur'];
    $user = get_entity($id_utilisateur);
    $isOwner = "no";
    if ( elgg_is_logged_in() ){
      $userLogged= elgg_get_logged_in_user_entity();
      if ( $id_utilisateur == $userLogged->getGUID()){
         $isOwner = "yes";
      }
  }
  }else{
    $user= elgg_get_logged_in_user_entity();
    $isOwner = "yes";
  }


  $userGuid = $user->guid;
	$name = $user->username;
	$image = $user->getIcon('medium');
  $email = $user->get("email"); 

  //Affichage des ressources

	//#####
  // DEBUT REQUETE RES
  //######
  $result = mysql_query("SELECT  `cape_ressources`.`id_ressource`, `cape_ressources`.`nom_court`,`cape_categories`.`nom`,`cape_categories`.`id_categorie`,`score`,`cape_utilisateurs_categorie`.`id_categorie_temp`
    FROM `cape_utilisateurs_categorie`,`cape_ressources`,`cape_categories` 
    WHERE `id_utilisateur` = $userGuid
    AND `cape_ressources`.`id_ressource` = `cape_utilisateurs_categorie`.`id_ressource`
    AND  
      ((`id_categorie_temp` > `cape_utilisateurs_categorie`.`id_categorie` AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie_temp`)
      OR (`id_categorie_temp` <= `cape_utilisateurs_categorie`.`id_categorie` AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie` )
      )
  ");

  if(!$result)
      error_log(mysql_error());
    

  //Pour chaque ressouces
  while($row_message_sc = mysql_fetch_assoc($result))
  {
    $arrayRes[] = (object) array('id_ressource' => $row_message_sc['id_ressource'],'nom' => $row_message_sc['nom_court'],'score' => $row_message_sc['score'],'max_score' => 1200,'categorie' => $row_message_sc['nom'],'id_categorie' => $row_message_sc['id_categorie']);
  }
	
	
  //#####
  // FIN REQUETE RES
  //######


  // Affichage des succès gagnés dans l'ordre logique.

  //#####
  // DEBUT REQUETE succes
  //######



  $result = mysql_query("SELECT * 
  FROM `cape_succes`,`cape_utilisateurs_succes` 
  WHERE `cape_succes`.`id_succes` = `cape_utilisateurs_succes`.`id_succes`
  AND `id_utilisateur` = $userGuid
  ORDER BY place ASC");

  if(!$result)
      error_log(mysql_error());
    

  //Pour chaque message
  while($row_message_sc2 = mysql_fetch_assoc($result))
  {
    $arraySucces[] = (object) array('succes' => $row_message_sc2['nom'],'description' => $row_message_sc2['description'],'image' => $row_message_sc2['image']/*,'image_blob'=> base64_encode($row_message_sc2['image_blob'])*/);
  }
  
  
  //#####
  // FIN REQUETE succes
  //######

  // Affichage des succès non gagnés.

  //#####
  // DEBUT REQUETE succes NON GAGNE
  //######

  $result = mysql_query("SELECT *
  FROM `cape_succes`
  WHERE (id_succes) 
  NOT IN
    (SELECT `cape_succes`.`id_succes`
    FROM `cape_succes`,`cape_utilisateurs_succes` 
    WHERE `cape_succes`.`id_succes` = `cape_utilisateurs_succes`.`id_succes`
    AND `id_utilisateur` = $userGuid)
  ORDER BY place ASC");

  if(!$result)
      error_log(mysql_error());
    

  //Pour chaque message
  while($row_message_sc3 = mysql_fetch_assoc($result))
  {
    $arraySuccesNonGagnes[] = (object) array('succes' => $row_message_sc3['nom'],'description' => $row_message_sc3['description'],'image' => "succes_default");
  }
  
  
  //#####
  // FIN REQUETE succes NON GAGNES
  //######


  //Affichage des succès gagnés en l'ordre chronologique.


  //#####
  // DEBUT REQUETE derniers succes
  //######


  $result = mysql_query("SELECT * 
  FROM `cape_succes`,`cape_utilisateurs_succes` 
  WHERE `cape_succes`.`id_succes` = `cape_utilisateurs_succes`.`id_succes`
  AND `id_utilisateur` = $userGuid
  ORDER BY time DESC");

  if(!$result)
      error_log(mysql_error());
    

  //Pour chaque message
  while($row_message_sc3 = mysql_fetch_assoc($result))
  {
    $arraySuccesDerniers[] = (object) array('image' => $row_message_sc3['image']);
  }
  
  
  //#####
  // FIN REQUETE derniers succes
  //######

  $result = mysql_query("SELECT * FROM `cape_utilisateurs` WHERE `id_elgg` = $userGuid");
  $row_message_sc = mysql_fetch_assoc($result); 
  $etablissement = $row_message_sc['etablissement'];

  $object = (object) array('name' => $name , 'image' => $image ,'email' => $email,'etablissement' => $etablissement, 'res' => $arrayRes,'nombreSucces' => count($arraySucces),'succes' => $arraySucces,'succesNonGagnes'=>$arraySuccesNonGagnes,'succesDerniers'=>$arraySuccesDerniers,'isOwner'=>$isOwner);
  $return = json_encode($object);
	
	print $return;

?>