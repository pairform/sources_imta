<?php

  //********************************************//
  //*********** RegisterCompte.php *************//
  //********************************************//
  
  

  /*
  *
  * Enregistre un compte
  *
  * params :
  *
  * username 
  * password
  * password2
  * etablissement
  * name
  * surnmae
  * email
  *
  * Retour : 
  * {"code":"ok" ou message d'erreur
  * }
  *
  */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $os = $_POST['os'];
  $version = $_POST['version'];
  
  //Elgg values
  $username = $_POST['username'];
  $password = $_POST['password'];
  $email = $_POST['email'];
    
  //SupCast values
  $etablissement = isset($_POST['etablissement']) ? $_POST['etablissement'] : " ";
    
  //$id = $_POST['id_elgg'];
  

  //error_log(print_r($_POST,TRUE));
  $name = strtoupper($_POST['name']).' '.$_POST['surname'];
      
  if (trim($password) == "" || trim($password2) == "" || trim($email) == "" || trim($username) == "") {
    $return = json_encode(array("code" => "empty"));
    // error_log($return);
    die($return);
  }
      
  if (strcmp($password, $password2) != 0) {
    $return = json_encode(array("code" => "password2"));
    // error_log($return);
    die($return);
  }

  //Les adresses mail doivent être uniques
  // if($usedEmail = get_user_by_email($email))
  // {
  //   $return = json_encode(array("code" => "email"));
  //   error_log($return);
  //   die($return);
  //   exit;
  // }

  //On vérifie si le pseudo est disponible
  // if($usedUserName = get_user_by_username($username))
  // {
  //   $return = json_encode(array("code" => "username"));
  //   error_log($return);
  //   die($return);
  //   exit;
  // }

  //Defaults values 
  $friend_guid = 0; 
  $invitecode = "";

  // error_log("Tutto bene");
  //Remettre ça pour utiliser le param d'ELGG : && elgg_get_config('allow_registration')
  //Elgg registration
  if (!$usedEmail && !$usedUserName ) {
    try {
      
      $guid = register_user($username, $password, $name, $email, false, $friend_guid, $invitecode);
      // error_log("GUID : " . $guid);
      if ($guid) {
        $new_user = get_entity($guid);

        // allow plugins to respond to self registration
        // note: To catch all new users, even those created by an admin,
        // register for the create, user event instead.
        // only passing vars that aren't in ElggUser.
        $params = array(
          'user' => $new_user,
          'password' => $password,
          'friend_guid' => $friend_guid,
          'invitecode' => $invitecode
        );

        // @todo should registration be allowed no matter what the plugins return?
        if (!elgg_trigger_plugin_hook('register', 'user', $params, TRUE)) {
          $new_user->delete();
          // @todo this is a generic messages. We could have plugins
          // throw a RegistrationException, but that is very odd
          // for the plugin hooks system.
          
          $return = json_encode(array("code" => "error"));
          // error_log($return);
          die($return);
          exit;
        }
        elgg_clear_sticky_form('register');
        system_message(elgg_echo("registerok", array(elgg_get_site_entity()->name)));

        // if exception thrown, this probably means there is a validation
        // plugin that has disabled the user
        // try {
        //   login($new_user);
        // } catch (LoginException $e) {
        //   // do nothing
        // }

        //error_log('GUID : ' . $guid);
        //SupCast registration
        //On entre l'user dans la base SC avec les infos complémentaires
        $result = mysql_query("INSERT INTO cape_utilisateurs (id_elgg, etablissement) VALUES ('$guid','$etablissement')");
        if($result)
        {
          //WARNING : Ne pas oublier les ressources par défaut.
          //On ne créé plus tous les niveaux par ressources, mais on met quand même les deux par défauts.

          //OPI et C2i D2
          $valuesOutput = "(4,".$guid.",0), (9,".$guid.",0)";

          // //On récupère toutes les ressources, pour insérer le niveau 0 (Lecteur) dans la base pour chaque ressource
          // $result = mysql_query("SELECT id_ressource FROM cape_ressources");
          // if($result)
          // {
          //   $values = "";
          //   //Pour chaque ressource, on construit une insertion pour la base cape_utilisateurs_categorie
          //   while ($row = mysql_fetch_assoc($result)) {
          //     $values .= "(".$row['id_ressource'].",".$guid.",0),";
          //   }
          //   //On sucre la dernière virgule de la chaîne
          //   $valuesOutput = rtrim($values, ",");
          // }
          // else error_log('Recup ressource : ' . mysql_error());

          //error_log("Values Output : " . $valuesOutput);
          //On insère les niveaux 0 dans la base
          // $result = mysql_query("INSERT INTO cape_utilisateurs_categorie (id_ressource, id_utilisateur, id_categorie) VALUES $valuesOutput");
          
          // if ($result) {

          //   //error_log("JSON : " . $return);
          //   $return = json_encode(array("code" => "ok"));
          //   error_log($return);
          //   die($return);
          // }
          // else error_log('Enregistrement ressource / rank : ' . mysql_error());
          
          //error_log("JSON : " . $return);
          $return = json_encode(array("code" => "ok"));
          // error_log($return);
          die($return);
          
        }
        else error_log('Erreur Enregistrement cape_utilisateurs : ' . mysql_error());

      } else {
        //error_log("JSON : " . $return);
        $return = json_encode(array("code" => "registerbad"));
        // error_log($return);
        die($return);
        register_error(elgg_echo("registerbad"));
      }
    } catch (RegistrationException $r) {
      register_error($r->getMessage());

      //error_log("JSON : " . $return);
      $return = json_encode(array("code" => $r->getMessage()));
      // error_log($return);
      die($return);
    }
  } else {

    //error_log("JSON : " . $return);
    $return = json_encode(array("code" => 'registerdisabled'));
    // error_log($return);
    die($return);
    register_error(elgg_echo('registerdisabled'));
  }


?>