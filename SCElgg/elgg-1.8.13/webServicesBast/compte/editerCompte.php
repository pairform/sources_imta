<?php

  //********************************************//
  //************** editerCompte.php *************//
  //********************************************//

  /*
  *
  * Edite le compte
  *
  *  param
  *
  * email 
  * etablissement
  * current_password
  * password
  * password2 
  * 
  * Retour : 
   * {"status":"ok"
   * }
   *
   * ou une indication sur l' erreur a la place.
  */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");


  $os = $_POST['os'];
  $version = $_POST['version'];
  
  $current_password = $_POST['current_password'];
  $password = $_POST['password'];
  $password2 = $_POST['password2'];
  $email = $_POST['email'];
  $etablissement = $_POST['etablissement'];

  $user = elgg_get_logged_in_user_entity();
  if (!$user ) {
    print(json_encode(array('status' => 'ko', 'message' => 'non connect')));
     exit();
  }

 	if ( isset( $_POST['current_password']) && !empty($_POST['current_password'])  )
 	{
       $credentials = array(
        'username' => $user->username,
        'password' => $current_password
      );

      try {
        pam_auth_userpass($credentials);
      } catch (LoginException $e) {
        print(json_encode(array('status' => 'ko', 'message' => 'Mauvais ancien mot de passe.')));
        exit();

      }


      try {
        $result = validate_password($password);
      } catch (RegistrationException $e) {
        print(json_encode(array('status' => 'ko', 'message' => 'Mode de passe non valide.')));
        exit();
      }



      if ($result) {
        if ($password == $password2) {
          $user->salt = generate_random_cleartext_password(); // Reset the salt
          $user->password = generate_user_password($user, $password);
        } else {
          print(json_encode(array('status' => 'ko', 'message' => 'Les mots de passe ne sont pas identiques.')));
          exit();
        }
      } else {
        print(json_encode(array('status' => 'ko', 'message' => 'Mot de passe trop court.')));
        exit();
      }
  }



  if (!is_email_address($email)) {
    print(json_encode(array('status' => 'ko', 'message' => 'Email non valide.')));
    exit();
  }

  if (strcmp($email, $user->email) != 0) {
    if (!get_user_by_email($email)) {
        $user->email = $email;
        
    } else {
      print(json_encode(array('status' => 'ko', 'message' => 'Email déjà utilisé.')));
      exit();
    }
  } 




  // Save final

  if ($user->save()) {

    
  } else {
    print(json_encode(array('status' => 'ko', 'message' => 'Problème réseau')));
    exit();
  }


  // Etablissement

  if ( !isset( $_POST['etablissement']) ){
      print(json_encode(array('status' => 'ko', 'message' => 'Etablissement non renseigné')));
      exit();
  }

  $guid = $user->getGUID();
  $result = mysql_query("SELECT * FROM `cape_utilisateurs` WHERE `id_elgg` = $guid");

  if(!$result){
        print(json_encode(array('status' => 'ko', 'message' => 'Problème réseau')));
        exit();
  }
  while($row_message_sc = mysql_fetch_assoc($result))
  {
    $etablissementBdd = $row_message_sc['etablissement'];
  }
  
  if (strcmp($etablissement, $etablissementBdd) != 0) {
    $result = mysql_query("UPDATE  `SCElgg`.`cape_utilisateurs` SET  `etablissement` =  '$etablissement' WHERE  `cape_utilisateurs`.`id_elgg` = '$guid'");

    if(!$result)
    {
        print(json_encode(array('status' => 'ko', 'message' => 'Probleme d\'enregistrement')));
        exit();
    }
  }
  print(json_encode(array('status' => 'ok')));

?>