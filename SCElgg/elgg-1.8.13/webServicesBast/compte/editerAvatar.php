<?php
 //********************************************//
  //************** editerAvatar.php *************//
  //********************************************//

  /*
  *
  * Edite l'avatar
  *
  *  le param est dans $_FILES['avatar']
  *

  */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerSucces.php");


$guid = elgg_get_logged_in_user_entity()->getGUID();
$owner = get_entity($guid);

if (!$owner || !($owner instanceof ElggUser) || !$owner->canEdit()) {
	register_error(elgg_echo('avatar:upload:fail'));
	error_log("avatar:upload:fail1");
	echo  json_encode(array('status' => "ko",'message' => "l'upload a échoué."));

	exit();
	//forward(REFERER);
}

if ($_FILES['avatar']['error'] != 0) {
	register_error(elgg_echo('avatar:upload:fail'));
	error_log("avatar:upload:fail2");
	echo  json_encode(array('status' => "ko",'message' => "l'upload a échoué."));
	exit();
	//forward(REFERER);
}

$icon_sizes = elgg_get_config('icon_sizes');

// get the images and save their file handlers into an array
// so we can do clean up if one fails.
$files = array();
foreach ($icon_sizes as $name => $size_info) {
	$resized = get_resized_image_from_uploaded_file('avatar', $size_info['w'], $size_info['h'], $size_info['square'], $size_info['upscale']);
	if ($resized) {
		//@todo Make these actual entities.  See exts #348.
		$file = new ElggFile();
		$file->owner_guid = $guid;
		$file->setFilename("profile/{$guid}{$name}.jpg");
		$file->open('write');
		$file->write($resized);
		$file->close();
		$files[] = $file;
	} else {
		// cleanup on fail
		foreach ($files as $file) {
			$file->delete();
		}

		register_error(elgg_echo('avatar:resize:fail'));
		error_log("avatar:resize:fail");
		echo  json_encode(array('status' => "ko",'message' => "échec du redimensionnement, image trop grande."));
		
		exit();
		//forward(REFERER);
	}
}


// reset crop coordinates
$owner->x1 = 0;
$owner->x2 = 0;
$owner->y1 = 0;
$owner->y2 = 0;

$owner->icontime = time();
if (elgg_trigger_event('profileiconupdate', $owner->type, $owner)) {
	//error_log("avatar:upload:success");

	/*$view = 'river/user/default/profileiconupdate';
	elgg_delete_river(array('subject_guid' => $owner->guid, 'view' => $view));
	add_to_river($view, 'update', $owner->guid, $owner->guid);*/
}

echo  json_encode(array('status' => "ok"));
gagnerSuccesAvatar();
//forward(REFERER);

?>
