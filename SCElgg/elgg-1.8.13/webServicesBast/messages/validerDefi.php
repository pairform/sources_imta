<?php

  //********************************************//
  //*********** validerDefi.php ***********//
  //********************************************//
  
  /* Valider une reponse de défi.
   *
   * Paramètres :
   * id_message (int) - l'id du message reponde de défi
   *
   * 
   *
   *
   * Retour : 
   * {"status":"ok"
   * }
   * 
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerPoints.php");



  $id_message = $_POST['id_message'];
  $defi = get_entity($id_message);


  $arrayMedaille = $defi->getAnnotations("defi_valide");
  if ( empty($arrayMedaille)){
    $defi->annotate('defi_valide',"",2);
    $return = json_encode(array('status' => "ok"));
    $defi->time_updated_meta = time();
    $defi->save();
    gagnerPointsReussirDefi($defi,$defi->getOwner());
  }else{
    $defi->deleteAnnotations("defi_valide");
    $return = json_encode(array('status' => "ok"));
    $defi->time_updated_meta = time();
    $defi->save();
  }
  
  echo $return; 
?>