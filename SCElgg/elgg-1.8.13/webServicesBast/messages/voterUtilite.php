<?php

  //********************************************//
  //************* voterUtilite **************//
  //********************************************//
  /*
   * Change l’utilité d’un message
   * 
   * Paramètres : 
   * up : booléen. True pour vote positif, false pour vote négatif.
   * id_message : id du message à traiter
   *
   * Retour : 
   * {"status":"ok"
   * "message" : "message" ( uniquement en cas d'erreur )
   * }
  */
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerPoints.php");

	$os = $_POST['os'];
  	$version = $_POST['version'];

    if(elgg_get_logged_in_user_guid() == 0)
    {
      print("Erreur d'enregistrement du message.");
      exit;
    }
    // Valeurs d'entrees 
    if(isset($_POST['up']))
    {
        $bUp = $_POST['up'] == "true" ? true : false;
        $entity_guid = (int)$_POST['id_message'];
        $entity = get_entity($entity_guid);

        function getTypes($up, $type = '')
        {
            //Si on est dans le cas d'un ajout,
            if($up)
            {
                $typeLike = 'likes';
                $typeLikeQuote = "likes";
            }
            else
            {
                $typeLike = 'dislikes';
                $typeLikeQuote = "dislikes";
            }

            return ($type == "quote" ? $typeLikeQuote : $typeLike);
        }

        //Fonctions de créations / suppression
        function createLike($up, $entity_guid)
        {
            $annotation = create_annotation($entity_guid,
                getTypes($up),
                getTypes($up, "quote"),
                "",
                elgg_get_logged_in_user_guid(),
                2);
            $entity = get_entity($entity_guid);
            //$entity->set("time_updated",time());
            $entity->time_updated_meta = time();
            $entity->save();
            gagnerPointsEvaluerMessage($entity,true);
            if ( $up)  {
                gagnerPointsEvaluerMessagePassif($entity,true,$entity->getOwner());
                gagnerSuccesUtiliteRecu($entity->getOwner());
            }
            else gagnerPointsEvaluerMessagePassif($entity,false,$entity->getOwner());
            gagnerSuccesVote();
        }
        
        function deleteLike($up, $entity_guid)
        {
            $likes = elgg_get_annotations(array(
                'guid' => $entity_guid,
                'annotation_owner_guid' => elgg_get_logged_in_user_guid(),
                'annotation_name' => getTypes($up) ));

            $like = $likes[0];

            if ($like && $like->canEdit())
            {
                $like->delete();
            }
            $entity = get_entity($entity_guid);
            $entity->time_updated_meta = time();
            $entity->save();
            gagnerPointsEvaluerMessage($entity,false);
            if ( $up)  gagnerPointsEvaluerMessagePassif($entity,false,$entity->getOwner());
            else gagnerPointsEvaluerMessagePassif($entity,true,$entity->getOwner());
        }


        // l'utilisateur ne peux pas voter ses propres messages
        if ($entity->owner_guid == elgg_get_logged_in_user_guid()) {
            print(json_encode(array('status' => 'ko', 'message' => 'Vous ne pouvez pas voter vos propres messages')));
            exit();
        }
        // check si l'utilisateur a déjà le même vote
        if (elgg_annotation_exists($entity_guid, getTypes($bUp), elgg_get_logged_in_user_guid()))
        {
            //error_log("Annulation du like");
            deleteLike($bUp, $entity_guid);
        }
        else
        {     //S'il a fait le vote inverse
            if(elgg_annotation_exists($entity_guid, getTypes(!$bUp), elgg_get_logged_in_user_guid()))
            {
                //error_log("Vote inverse");
                deleteLike(!$bUp, $entity_guid);
                createLike($bUp, $entity_guid);
            }
            else
            {
                //error_log("Pas de vote précédent");
                createLike($bUp, $entity_guid);
            }
        }
      }
      print(json_encode(array('status' => 'ok')));
     //error_log("entity_guid : $entity_guid ,typeLike : ".getTypes(!$bUp).", !bUp : !$bUp, user->guid : $user->guid, annotation : $annotation");
      

?>