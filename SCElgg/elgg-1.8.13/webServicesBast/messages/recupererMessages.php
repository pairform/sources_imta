<?php
//error_log('//********************************************//');
//error_log('//*********** recupererMessages.php **********//');
//error_log('//********************************************//');
//Recuperation des valeurs des messages
/*
 * Paramètres :
   * nom_page (string) - nom de la page concernée.
   * id_ressource (int) - id de la ressource concernée.
   * nom_tag ( string ) - nom  du tag ( PAGE ou autre ).
   * num_occurence (int) - le numero de l'occurence de l'objet pedagogique.

     "messages":[
      {
          "id_message":117,
          "owner_guid":"35",
          "owner_username":"Admin",
          "owner_avatar_medium":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1362507644&joindate=1359553679&guid=35&size=medium",
          "owner_rank_id":null,
          "owner_rank_name":null,
          "value":"Test sur l'actualité au 15 janvier",
          "geo_lattitude":"",
          "geo_longitude":"",
          "supprime_par":"0",
          "utilite":0,
          "tags":{
            },
          "reponses":null, ( pareil que pour le message )
          "est_defi": 0 ( pas defi ) 1 ( defi ) 2 ( defi terminé" ,
          "medaille":"or" / "argent" / "bronze"
          "defi_valide" : 0 / 1
      }
 */

include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
include_once("../accomplissement/gagnerSucces.php");
$os      = $_POST['os'];
$version = $_POST['version'];


    
//Impossible d'utiliser cette fonction sans être connecté
if (!elgg_get_logged_in_user_guid())
{
    //Creation d'une session php par Elggme
    //$user   = get_user_by_username("sc_bot");
    //$result = login($user, true);
}
    


// Si l'utilisateur n'a pas encore cette ressource dans sa liste de ressource.
$filtre_ressource = json_decode($_POST['filtre_ressource']);
$user = elgg_get_logged_in_user_entity();
if ( !empty($filtre_ressource) && !empty($user)) {
    $id_utilisateur = $user->getGUID();
    foreach ($filtre_ressource as $id_ressource) {
        $result = mysql_query("SELECT * 
            FROM `cape_utilisateurs_categorie` 
            WHERE `id_utilisateur` = $id_utilisateur
            AND `id_ressource` = $id_ressource");
        if ($result && !mysql_fetch_assoc($result)){
            $result = mysql_query("INSERT INTO  `cape_utilisateurs_categorie` (  `id_ressource` , `id_utilisateur` , `id_categorie` ,  `score` ) 
            VALUES ('$id_ressource', '$id_utilisateur', 0, 0)");
            gagnerSuccesRessource();
        }
    }
}

 

// N'affiche pas les reponse au niveau 1
function getMessage($row_message_sc, $affiche_reponse = false)
{

    //On chope les variables réutilisées souvent, pour éviter les accès au tableau et pour la lisibilité
    $message_id_ressource  = $row_message_sc['id_ressource'];


    // Filtre plusieurs ressources : On ne prend pas les messages qui ne sont pas dans la liste
    $filtre_ressource = json_decode($_POST['filtre_ressource']);
    if (!empty($filtre_ressource) && !in_array($message_id_ressource, $filtre_ressource))  {
        return null;
    }
    

    $message_nom_page      = $row_message_sc['nom_page'];
    $message_nom_tag       = $row_message_sc['nom_tag'];
    $message_num_occurence = $row_message_sc['num_occurence'];
    //Options passées 
    $options               = array(
        'guid' => $row_message_sc['id_message'],
        'type' => 'object'
    );                                                    
    $messages_elgg         = elgg_get_entities($options);
    $message_elgg          = $messages_elgg[0];
    // Recupere l'entity objet 
    $message_entity        = get_entity($row_message_sc['id_message']);
    // Si l'objet est inconnu on arrete.
    if ($messages_elgg == null || $message_entity == null)
        return null;
    // Ne pas afficher si nous n'avons pas l'acces.
    $user = elgg_get_logged_in_user_entity();
    if (!has_access_to_entity($message_entity, $user))
        return null;

    // Ne pas afficher si c'est une reponse d'un defi et que nous ne somme pas l'auteur-expert.


    
    $arrayParent    = $message_entity->getAnnotations("parent");
    if (!empty($arrayParent))
    {
        $parentGuid = $arrayParent[0]->__get("value");
        $parentEntity = get_entity($parentGuid);
        
        if ( !empty($parentEntity)){

            // si le parent est un defi , et que je suis pas l'auteur de ce defi
            $arrayDefi = $parentEntity->getAnnotations("est_defi");
            if (!empty($arrayDefi) && ($parentEntity->getOwnerEntity() != $user)){
                // et que le defi n'est pas fini
                if ( $arrayDefi[0]->__get("value") == 1)    return null;
            }
        }

        
    }
      
    if ( $user ){
         $user_guid = $user->getGUID();
    }

    //On store ces valeurs, parce que les crochets passent mal dans la formation de la requête SQL
    $id_message_temp = $message_elgg->guid;
    $id_owner        = $message_elgg->owner_guid;
    $date            = $message_elgg->time_created;
    $time_updated=(($message_elgg->time_updated)>($message_elgg->time_updated_meta))?($message_elgg->time_updated):("$message_elgg->time_updated_meta");
    //$time_updated    = $message_elgg->time_updated;
    $user            = elgg_get_entities(array(
        "type" => "user",
        "guid" => $id_owner
    ));
    if ( empty($user[0])  )  return null;
    //error_log('IDOwner : '. $id_owner .' : '.print_r($user,true));
    $iconURL         = $user[0]->getIconURL("medium");
    // donne l'utilité
    $likes           = elgg_get_annotations(array(
        'guid' => $id_message_temp,
        'annotation_name' => 'likes'
    ));
    $dislikes        = elgg_get_annotations(array(
        'guid' => $id_message_temp,
        'annotation_name' => 'dislikes'
    ));
    $utile           = count($likes) - count($dislikes);

    $user_a_vote     = 0;
    //Renvoie 1 pour vote positif
    foreach ($likes as $key => $like)
    {
        //error_log("Like guid : " . $like['owner_guid']);
        if ($like['owner_guid'] == $user_guid)
            $user_a_vote = 1;
    }
    //Renvoie -1 pour vote négatif
    foreach ($dislikes as $key => $dislike)
    {
        //error_log("dislike guid : " . $dislike['owner_guid']);
        if ($dislike['owner_guid'] == $user_guid)
            $user_a_vote = -1;
    }
    
    //Tag
    $tags            = $message_entity->getTags();
    if (empty($tags))
     {   $tags = "";}
    else{
        $tagsHash  = (array)json_decode($message_entity->tagsHash);
        foreach ($tags as $value) {
            if ( $tagsHash[$value] ){
                $user_tag =get_entity($tagsHash[$value]);
                $name = $user_tag->__get('name');
                $tags_auteurs[] = $name;
            }else{
                $tags_auteurs[] = null;
            }
            
        }
    }

    
    
    if (empty($tagsHash))
        $tagsHash = "";

    // Defi
    $est_defi = $message_entity->getAnnotations("est_defi");
    if ($est_defi)  
        $est_defi = $est_defi[0]->__get("value");
    else 
        $est_defi = 0;

    // Defi valide
    $array_defi_valide = $message_entity->getAnnotations("defi_valide");
    if (!empty($array_defi_valide)) 
        $defi_valide = 1;
    else 
        $defi_valide = 0;

    // Medaille
    $arrayMedaille         = $message_entity->getAnnotations("medaille");
    if (!empty($arrayMedaille))
    {
        $medaille = $arrayMedaille[0]->__get("value");
    }
    
    // parent d'une reponse
    $arrayParent         = $message_entity->getAnnotations("parent");
    if (!empty($arrayParent))
    {
        $parent = $arrayParent[0]->__get("value");
    }
    
    
    $result_messages_enrichis = mysql_query("SELECT `geo_lattitude`, `geo_longitude`, `supprime_par`, `cape_utilisateurs_categorie`.`id_categorie`, `cape_categories`.`nom`
FROM `cape_messages`,  `cape_utilisateurs_categorie`, `cape_categories` 
WHERE `cape_messages`.`id_message` = $id_message_temp 
AND `cape_utilisateurs_categorie`.`id_ressource` = $message_id_ressource
AND `cape_utilisateurs_categorie`.`id_utilisateur` = $id_owner
AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie`");
    if ($result_messages_enrichis)
    {

        $row_message_enrichis   = mysql_fetch_assoc($result_messages_enrichis);

        //On forme le message
        $array_message = array(
            "id_message" => "$id_message_temp",
            "owner_guid" => $id_owner,
            "owner_username" => (!empty($user[0]->username))?$user[0]->username:$user[0]->name,
            "owner_avatar_medium" => $iconURL,
            "owner_rank_id" => $row_message_enrichis['id_categorie'],
            "owner_rank_name" => $row_message_enrichis['nom'],
            "value" => $message_elgg->description,
            "geo_lattitude" => $row_message_sc['geo_lattitude'],
            "geo_longitude" => $row_message_sc['geo_longitude'],
            "supprime_par" => $row_message_sc['supprime_par'],
            "utilite" => "$utile",
            "tags" => $message_entity->getTags(),
            "tags_auteurs" => $tags_auteurs,
            "est_defi" => $est_defi,
            "defi_valide" => $defi_valide,
            "parent" => "$parent",
            "medaille" => $medaille,
            "id_ressource" => $row_message_sc['id_ressource'],
            "nom_page" => $row_message_sc['nom_page'],
            "nom_tag" => $row_message_sc['nom_tag'],
            "num_occurence" => $row_message_sc['num_occurence'],
            "time_created" => $date,
            "time_updated" => $time_updated,
            "user_a_vote" => "$user_a_vote"
        );
    }
    else 
        error_log(msql_error());

    $arrayFinal    = $array_message;
    return $arrayFinal;
}


$granularite = ''; //globale, ressource ou page

//En fonction de ce que l'on envoie au webservice, on va faire une sélection plus fine des messages
if (isset($_POST['id_ressource']) && isset($_POST['nom_page']) && isset($_POST['nom_tag']) && isset($_POST['num_occurence']))
{
    $id_ressource    = $_POST['id_ressource'];
    $nom_page        = $_POST['nom_page'];
    $granularite     = 'message';
    $result_messages = mysql_query("SELECT * FROM `cape_messages` 
  WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' AND `nom_tag` = '$message_nom_tag' AND `num_occurence` = $message_num_occurence
  ORDER BY `cape_messages`.`id_ressource` ASC,
  `cape_messages`.`nom_page` ASC,
  `cape_messages`.`nom_tag` ASC,
  `cape_messages`.`num_occurence` ASC ");
    if (!$result_messages)
        error_log(mysql_error());
}
else if (isset($_POST['id_ressource']) && isset($_POST['nom_page']))
{
    $id_ressource    = $_POST['id_ressource'];
    $nom_page        = $_POST['nom_page'];
    $granularite     = 'page';
    $result_messages = mysql_query("SELECT * FROM `cape_messages` 
  WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' 
  ORDER BY `cape_messages`.`id_ressource` ASC,
  `cape_messages`.`nom_page` ASC,
  `cape_messages`.`nom_tag` ASC,
  `cape_messages`.`num_occurence` ASC ");
    if (!$result_messages)
        error_log(mysql_error());
}
else if (isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
{
    $id_ressource    = $_POST['id_ressource'];
    $granularite     = 'ressource';
    $result_messages = mysql_query("SELECT * FROM `cape_messages` 
  WHERE `id_ressource` = $id_ressource 
  ORDER BY `cape_messages`.`id_ressource` ASC,
  `cape_messages`.`nom_page` ASC,
  `cape_messages`.`nom_tag` ASC,
  `cape_messages`.`num_occurence` ASC ");
    if (!$result_messages)
        error_log(mysql_error());
}
else if (!isset($_POST['id_ressource']) && !isset($_POST['nom_page']) && !isset($_POST['vue_trans']) && !isset($_POST['id_message']))
{
    $granularite     = 'globale';
    $result_messages = mysql_query("SELECT * FROM `cape_messages` 
  ORDER BY `cape_messages`.`id_ressource` ASC,
  `cape_messages`.`nom_page` ASC,
  `cape_messages`.`nom_tag` ASC,
  `cape_messages`.`num_occurence` ASC ");
    if (!$result_messages)
        error_log(mysql_error());
}
else if (isset($_POST['id_message']))
{
    $id_message = $_POST['id_message'];
    $granularite     = 'globale';
    $result_messages = mysql_query("SELECT * FROM `cape_messages` 
        WHERE `id_message` = $id_message
  ORDER BY `cape_messages`.`id_ressource` ASC,
  `cape_messages`.`nom_page` ASC,
  `cape_messages`.`nom_tag` ASC,
  `cape_messages`.`num_occurence` ASC ");
    if (!$result_messages)
        error_log(mysql_error());
}else{
    $granularite     = 'transversale';
    $result_messages = mysql_query("SELECT  `id_message` , `id_ressource` , `nom_page` , `nom_tag` , `num_occurence` , `geo_lattitude` , `geo_longitude` , `supprime_par`   
  FROM `cape_messages` , `sce_entities`
  WHERE  `cape_messages`.`id_message` = `sce_entities`.`guid`
  ORDER BY `sce_entities`.`time_created` DESC   ");
    if (!$result_messages)
        error_log(mysql_error());
}
// error_log($granularite);
//Pour chaque message
while ($row_message_sc = mysql_fetch_assoc($result_messages))
{
    $message = getMessage($row_message_sc);
    if ($message)
    {
        $arrayFinal[] = $message;
    }
}

//error_log('Array_grains : ' . print_r($array_grains, true));
$return = json_encode(array('messages' => $arrayFinal));
//error_log('Retour JSON : ' . $return);
//Retour JSON
print($return);

?>