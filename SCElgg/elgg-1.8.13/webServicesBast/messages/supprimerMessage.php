<?php

  //********************************************//
  //*********** supprimerMessage.php ***********//
  //********************************************//
  
  /* Inscription du message comme supprimé dans la table cape_messages
   *
   * Paramètres :
   * id_message (int) - id du message à supprimer
   * supprime_par (int) - id du modérateur ayant supprimé le message (defaut : 0 si non-supprimé)
   * id_user_op (int) - id de l'auteur du message
   * id_rank_user_op (int) - id de la categorie de l'user
   * id_user_moderator (int) - id du moderateur du message (triggerer de l'action)
   * id_rank_user_moderator (int) - id de la categorie de ce moderateur
   * id_ressource (int) - id de la ressource concernée
   *
   * Retour : 
   * {"status":"ok"
   * "message" : "message" ( uniquement en cas d'erreur )
   * }
   */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerPoints.php");

	$os = $_POST['os'];
  $version = $_POST['version'];

  //Ids elgg
  $id_message = (int)$_POST['id_message'];
  $supprime_par = (int)$_POST['supprime_par'];

	$id_user_op = (int)$_POST['id_user_op'];
  $id_rank_user_op = (int)$_POST['id_rank_user_op'];
  
  $id_user_moderator = (int)$_POST['id_user_moderator'];
  $id_rank_user_moderator = (int)$_POST['id_rank_user_moderator'];

  $id_ressource = (int)$_POST['id_ressource'];


//Trois cas de figure : 
        
//Le message n'a pas encore été supprimé ;
if($supprime_par == 0)
{
  //On check si le modo est l'op
  //OU que le rang du modo est supérieur ou égal à celui du posteur
  if(($id_user_moderator == $id_user_op) || ($id_rank_user_moderator >= $id_rank_user_op))
  {
    //On ajoute l'enregistrement à la table
    $result = mysql_query("UPDATE cape_messages SET supprime_par ='$id_rank_user_moderator' WHERE id_message='$id_message'");
    
    if($result)
    {
      $entity = get_entity($id_message);
      gagnerPointsSupprimerCommentaire($entity);
      $entity->time_updated_meta = time();
      $entity->save();
      print(  json_encode(array('status' => "ok")));
      exit();
    }
    else error_log(mysql_error());
  }
  else 
    echo json_encode(array('status' => "ko","message" => "Vous n'avez pas l'autorisation de supprimer ce message"));
}

//Le message est déjà supprimé par un autre utilisateur de rang inférieur
//Possibilité d'annuler la suppression
else
{ 
  $result = mysql_query("SELECT id_categorie FROM cape_utilisateurs_categorie WHERE id_utilisateur='$supprime_par' AND id_ressource='$id_ressource'");
  
  if($result)
  {
    $row = mysql_fetch_assoc($result);
    $id_rank_supprime_par = $row[0];
  }
  else error_log(mysql_error());

  //Si le rang du modo est supérieur à celui du modo précédent
  //OU que le modo est le propriétaire du post
  if(($id_rank_user_moderator >= $id_rank_supprime_par) || ($id_user_op == $supprime_par))
  {
    //On ajoute l'enregistrement à la table
    $result = mysql_query("UPDATE cape_messages SET supprime_par = 0 WHERE id_message='$id_message'");
    
    if($result)
    {
      $entity = get_entity($id_message);
      $entity->time_updated_meta = time();
      $entity->save();
      gagnerPointsActiverCommentaire($entity);
      print(  json_encode(array('status' => "ok")));
      exit();
    }
    else error_log(mysql_error());
  }

  //Le message est déjà supprimé par un autre utilisateur de rang supérieur
  else if ($id_rank_user_moderator < $id_rank_supprime_par) 
  {
    $return =  json_encode(array('status' => "ko",'message' => "Problème de suppression"));
  }
}


?>