<?php

  //********************************************//
  //*********** terminerDefi.php ***********//
  //********************************************//
  
  /* Terminer un defi.
   *
   * Paramètres :
   * id_message (int) - l'id du message "defi"
   *
   * Retour : 
   * {"status":"ok"
   * }
   * 
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $id_message = $_POST['id_message'];
  $defi = get_entity($id_message);

  if ($defi->deleteAnnotations('est_defi')){
    $defi->annotate('est_defi',2,2); // 2 pour défi terminé. et 2 pour public.
    $return = json_encode(array('status' => "ok"));
    $defi->time_updated_meta = time();
    $defi->save();
  }
  else
  {
    $return = json_encode(array('status' => "ko" , 'message' => "problème sur defi"));
  }  
  echo $return;

?>