<?php

  //********************************************//
  //************* ajouterTags **************//
  //********************************************//
  /*
   * Ajoute des tags à un message.
   * Paramètres : 
   * tags : string tags séparé par des virgules.
   * id_message : id du message à traiter
   *
   * Retour : 
   * {"status":"ok"
      "message" : "message" ( uniquement en cas d'erreur )
   * }
  */
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");




  $version = $_POST['version'];
  $id_message = mysql_real_escape_string($_POST['id_message']);
  $tags = $_POST['tags'];
 
  if ( empty($tags)){
    $return = json_encode(array('status' => 'ko', 'message' => 'Champs vide'));
    echo $return;
    exit();
  }

  $options = array('type' => 'object', 'guid' => $id_message);  
  $messages_elgg = elgg_get_entities($options);
  $blog = $messages_elgg[0];
  
  $id_user =elgg_get_logged_in_user_guid();

  
  // Recupere les tags
  $newTags = explode(",", $tags);
  $newTagsHash = array();
  foreach ($newTags as $tag) {
    $newTagsHash[$tag] = $id_user;
  }
  


  // TagsHash - liaison entre les tags et les auteurs.
  $oldTagsHash = (array)json_decode($blog->tagsHash);
  if ( empty($oldTagsHash) ){
    $blog->tagsHash = json_encode($newTagsHash);
   }else{
    //if ( !is_array($oldTagsHash)) $oldTagsHash = array($oldTagsHash);
    $blog->tagsHash = json_encode(array_merge($oldTagsHash,$newTagsHash));
   }


   // ajoute les tags
   $oldTags = $blog->tags;
  if ( empty($oldTags)){
     if (count($newTags) > 5) {
       $return = json_encode(array('status' => 'ko', 'message' => 'Limite de 5 tags dépassée'));
        echo $return;
        exit();
     }
     $blog->tags = $newTags;
    
  }else{
    if ( !is_array($oldTags)) $oldTags = array($oldTags);
    if (count(array_merge($oldTags,$newTags)) > 5) {
       $return = json_encode(array('status' => 'ko', 'message' => 'Limite de 5 tags dépassée'));
      echo $return;
      exit();
     }
    $blog->tags = array_merge($oldTags,$newTags);
  }  
  $blog->time_updated_meta = time();

  $return = json_encode(array('status' => 'ok'));
  echo $return;


?>