<?php

  error_log("//********************************************//");
  error_log("//******** EnregistrerMessage.php ************//");
  error_log("//********************************************//");

  /* Enregister un message
   *
   * Paramètres :
   * nom_page (string) - nom de la page concernée.
   * id_ressource (int) - id de la ressource concernée.
   * nom_tag ( string ) - nom  du tag ( PAGE ou autre ).
   * num_occurence (int) - le numero de l'occurence de l'objet pedagogique.
   * id_message_original (int) - id du message original , seulement si c'est une reponse
   * est_defi (void) - indique si le message est un defi.
   * (optionel) visibilite ( tableau int ) - indique les id de visibilité si different de defaut ex [ 4,5,6];
   *
   * Retour : 
   * {"result":"true"
   * }
   * 
   */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerPoints.php");
  include_once("../accomplissement/gagnerSucces.php");

  $os = $_POST['os'];
  $version = $_POST['version'];

  $id_ressource = $_POST['id_ressource'];
  $nom_page = $_POST['nom_page'];

  $nom_tag  = isset($_POST['nom_tag']) ? $_POST['nom_tag'] : 'PAGE';
  $num_occurence  = isset($_POST['num_occurence']) ? (int)$_POST['num_occurence'] : 0;

  //Recuperation des variables envoyé pour le message
  $message = $_POST['message'];


  $tagarray = json_decode($_POST['tag']);
  $visibilitearray = json_decode($_POST['visibilite']);


  $path_message = $id_ressource.'/'.$nom_page.'/'.$nom_tag.'/'.$num_occurence;

  if(!elgg_get_logged_in_user_guid())
    print(json_encode(array('status' => "ko")));

  //Login
  //Test si le blog existe déjà
  //Sinon, on enregistre le blog dans la base
  //On génère un ID sous forme /id_ressource/nom_de_la_page.html/type_de_tag/occurence_du_tag
  //Ensuite, on enregistre le couple de cet ID et l'id Elgg dans la base 


 if(isset($_POST['id_message']))
        {
          $id_message = mysql_real_escape_string($_POST['id_message']);
          $options = array('owner_guid' => elgg_get_logged_in_user_guid(), 'type' => 'object', 'guid' => $id_message);  
          $messages_elgg = elgg_get_entities($options);

          $blog = $messages_elgg[0];

          $blog->description = $message;
          // Heure de modification
          $blog->time_updated = date("dd/mm/yy");
          // Now save the object
          if (!$blog->save()) 
          {
            register_error(elgg_echo("blog:error"));
            //forward($_SERVER['HTTP_REFERER']);
          }
        }
        else
        {
        // Initialise a new ElggObject
          $blog = new ElggObject();
        // Tell the system it's a blog post
          $blog->subtype = "blog";
        // Set its owner to the current user
          $blog->owner_guid = elgg_get_logged_in_user_guid();
        // For now, set its access
          //$blog->access_id = 1; public access

        // Gestion des acces.

          // Creation d'un acces special si plusieurs cercle/classe sont choisis , cf doc.

          //Si le message est une reponse on prend la visibilité du parent
          if ( isset($_POST['id_message_original']) && $_POST['id_message_original'] != "" ) {
            $parent = get_entity($_POST['id_message_original']);
            $blog->access_id = $parent->access_id;
          } else{
            if ( $visibilitearray ){
              $new_access_id = access_plus_parse_access($visibilitearray);
              $blog->access_id = $new_access_id;
            }else{
              $blog->access_id = 2;
            }
            
          }
          


        // Set its title and description appropriately
          $blog->title = $path_message;

          // $blog->container_guid = "35";

          // $blog->site_guid = 1;
        // Description
          $blog->description = $message;

        // Heure de création
          $blog->time_created = time();
  
        // Now let's add tags. We can pass an array directly to the object property! Easy.
          if (is_array($tagarray)) 
          {
            $blog->tags = $tagarray;
          }
        //whether the user wants to allow comments or not on the blog post
          $blog->comments_on = false;
        

  
          error_log(print_r($blog, true));
        // Now save the object
          if (!$blog->save()) 
          {
            register_error(elgg_echo("blog:error"));
            //forward($_SERVER['HTTP_REFERER']);
          }

        //Systeme de reponses
        if ( isset($_POST['id_message_original']) && $_POST['id_message_original'] != "" ) {
          $blog->annotate('parent',$_POST['id_message_original'],2);
        } 
        //systeme de defi
        if ( isset($_POST['est_defi'])){
            $blog->annotate('est_defi',1,2); // 1 pour défi actif. 2 pour annotation public.
        }
       

          //ID Elgg du blog
          $id_message = $blog->guid;
        // Success message
          system_message(elgg_echo("blog:posted"));
        // add to river
          add_to_river('river/object/blog/create', 'create', elgg_get_logged_in_user_guid(), $blog->guid);
        // Remove the blog post cache
          //unset($_SESSION['blogtitle']); unset($_SESSION['blogbody']); unset($_SESSION['blogtags']);
        

          //modfication de l'identifiant du blog créé pour l'idecran
          
          $result = mysql_query("INSERT INTO `cape_messages` (`id_message`, `id_ressource`,`nom_page`,`nom_tag`,`num_occurence`, `geo_lattitude`,`geo_longitude`,`supprime_par`) 
            VALUES ($id_message, $id_ressource,'$nom_page','$nom_tag',$num_occurence, '','', 0)");
          
           //Points
          if ($result){
            gagnerPointsEcrireMessage($blog);
            gagnerSuccesMessage();
            gagnerSuccesRepondre();
            if ( isset($_POST['id_message_original']) && $_POST['id_message_original'] != "" ) {
                $entity = get_entity ($_POST['id_message_original']);
                $guid_owner = $entity->getOwnerGUID();
                gagnerSuccesRecevoirReponses($guid_owner);


                //Participation defi
                $entityGuid = $entity->getGUID();
                $optionsAnnotation               = array(
                'guids'  => $entityGuid,
                'annotation_names' => array('est_defi'),
                'limit' => 0,
                );    
              $annotations = elgg_get_annotations($optionsAnnotation);
                if (!empty($annotations)){
                  gagnerSuccesParticiperDefi();
                }
            }
          }

      if($result)
          print(json_encode(array('status' => "ok")));
        else {
          error_log(mysql_error());
          print(json_encode(array('status' => "ko")));
        }
                  
          
   }    




?>