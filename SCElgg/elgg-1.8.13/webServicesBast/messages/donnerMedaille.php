<?php

  //********************************************//
  //*********** donnerMedaille.php ***********//
  //********************************************//
  
  /* Donner une medaille a un message.
   *
   * Paramètres :
   * type_medaille (string) - le type de medaille : or , argent , bronze.
   * id_message (int) - id du messages
   * 
   *
   *
   * Retour : 
   * {"status":"ok"
   * }
   * 
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerSucces.php");

  $os = $_POST['os'];
  $version = $_POST['version'];

	// type medaille : or , argent , bronze.
  $type_medaille = $_POST['type_medaille'];
  $entity_guid = $_POST['id_message'];
	$entity = get_entity($entity_guid);

  $arrayMedaille = $entity->getAnnotations("medaille");
	if ( !empty($arrayMedaille)){
		$entity->deleteAnnotations("medaille");

    // Si on enleve une medaille on s'arrete ici
    if ( !isset($_POST['type_medaille'])){
      $entity->time_updated_meta = time();
      $entity->save();
      echo json_encode(array('status' => "ok"));
      return;
    }
	}
	if ($entity->annotate ("medaille", $type_medaille,2)){
    $entity->time_updated_meta = time();
    $entity->save();
    $return = json_encode(array('status' => "ok"));
    gagnerSuccesMedaille($entity->getOwnerGUID());
  }
  else
  {
    $return = json_encode(array('status' => "ko"));
  }  
  echo $return;

?>