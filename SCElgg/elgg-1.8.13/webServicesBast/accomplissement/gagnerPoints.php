<?php


  //********************************************//
  //*********** gagnerPoints.php ***********//
  //********************************************//
  
  /* Fait gagner des points à l'utilisateur .
   *
   * Paramètres :
   * 
   * Est a utiliser en tant que library
   * 
   * On appelle les fonctions quand des points peuvent être gagnés à cet endroit.

  gagnerPointsEcrireMessage($entity) // Ecrire un message , $entity du message
  gagnerPointsEvaluerMessage($entity,$bPositif) // Evaluer un message , $entity du message , $bPositif vrai si vote positif
  gagnerPointsSupprimerCommentaire($entity) // Supprimer un message , $entity du message
  gagnerPointsActiverCommentaire($entity) // Activer un message , $entity du message
  gagnerPointsEvaluerMessagePassif($entity,$bPositif,$id_utilisateur) // Recevoir une evalutation de message , $entity du message , $bPositif vrai si vote positif , $id_utilisateur id de l’utilisateur.
  gagnerPointsReussirDefi($entity,$id_utilisateur) // Reussir un défi ,$entity du message ,$id_utilisateur id de l’utilisateur.
   *
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("gagnerSucces.php");
  


  define("PTSPARTICIPANT", 30);
  define("PTSCOLLABORATEUR", 500);
  define("PTSANIMATEUR", 1200);


  //Actions simples , actives
  
  define("PTSECRIREMESSAGE", 10);
  define("PTSEVALUERMESSAGE", 1);
  define("PTSGAGNERSUCCES", 10);

  //Actions omplexes , passives

  define("PTSREUSSIDEFI", 20);
  define("PTSVOTEPOSITIF", 1);
  define("PTSVOTENEGATIF", -1);
  define("PTSCOMMENTAIRESUPPRIME", -30);


  //Actions simples


  function gagnerPointsEcrireMessage($entity){
      if ( !bValid() ) return;
      $id_ressource = getIdRessourceFromEntity($entity);
      gagnePoints($id_ressource,PTSECRIREMESSAGE);
  }

  function gagnerPointsEvaluerMessage($entity,$bPositif){
      if ( !bValid() ) return;
      $id_ressource = getIdRessourceFromEntity($entity);
      if ( $bPositif){
        $pts = PTSEVALUERMESSAGE;
      }else{
        $pts = -PTSEVALUERMESSAGE;
      }
      gagnePoints($id_ressource,$pts);
  }

  // StandBy : ou mettre les points?
  /*function gagnerPointsGagnerSucces(){
      if ( !bValid() ) return;
      gagnePoints($id_ressource,ptsGagnerSucces);
  }*/




  //Actions complexes , passives

  function gagnerPointsSupprimerCommentaire($entity){
      if ( !bValid() ) return;
      $id_ressource = getIdRessourceFromEntity($entity);
      $id_utilisateur = $entity->getOwnerGUID();
      gagnePoints($id_ressource,PTSCOMMENTAIRESUPPRIME,$id_utilisateur);
  }

    function gagnerPointsActiverCommentaire($entity){
      if ( !bValid() ) return;
      $id_ressource = getIdRessourceFromEntity($entity);
      $id_utilisateur = $entity->getOwnerGUID();
      gagnePoints($id_ressource,-PTSCOMMENTAIRESUPPRIME,$id_utilisateur);
  }

  function gagnerPointsEvaluerMessagePassif($entity,$bPositif,$id_utilisateur){
      if ( !bValid() ) return;
      $id_ressource = getIdRessourceFromEntity($entity);
      if ( $bPositif){
        $pts = PTSVOTEPOSITIF;
      }else{
        $pts = PTSVOTENEGATIF;
      }
      
      gagnePoints($id_ressource,$pts,$id_utilisateur);
  }

    function gagnerPointsReussirDefi($entity,$id_utilisateur){
      if ( !bValid() ) return;
      $id_ressource = getIdRessourceFromEntity($entity);
      gagnePoints($id_ressource,PTSREUSSIDEFI,$id_utilisateur);
  }

    
    
  // functions "privates"  

  function bValid(){
    if ( elgg_get_logged_in_user_entity() == null ) return FALSE;
    return TRUE;
  }

  function getIdRessourceFromEntity($entity){
      $entityGuid = $entity->getGUID();

    //#####
    // DEBUT REQUETE getRessource
    //######
    $result = mysql_query("SELECT `id_ressource` 
      FROM `cape_messages` 
      WHERE `id_message`=$entityGuid");

    if(!$result)
        error_log(mysql_error());
      
    $row_message_sc = mysql_fetch_assoc($result);
    if ($row_message_sc == null) return null;
    $id_ressource = $row_message_sc['id_ressource'];
    
    //#####
    // FIN REQUETE  getRessource
    //######
    return $id_ressource;


      
  }

  function gagnePoints($id_ressource,$points,$id_utilisateur=null){

    if ( $id_ressource == null ) return;

    // Si non spécifié , c'est actif , et donc l'utilisateur loggé.
    if ( $id_utilisateur == null ) $id_utilisateur = elgg_get_logged_in_user_entity()->guid ;
    //#####
    // DEBUT REQUETE Ajout de points
    //######


    // Si l'utilisateur n'a pas encore de points dans la ressource on ajoute.

          $result = mysql_query("SELECT * 
            FROM `cape_utilisateurs_categorie` 
            WHERE `id_utilisateur` = $id_utilisateur
            AND `id_ressource` = $id_ressource");
    if (!mysql_fetch_assoc($result)){

      $result = mysql_query("INSERT INTO  `cape_utilisateurs_categorie` (  `id_ressource` , `id_utilisateur` , `id_categorie` ,  `score` ) 
        VALUES ('$id_ressource', '$id_utilisateur', 0, 0)");
    }

    // On ajoute les points

    $result = mysql_query("UPDATE `cape_utilisateurs_categorie`
      SET score = score+$points
      WHERE `id_utilisateur` = $id_utilisateur
      AND `id_ressource` = $id_ressource");

    if(!$result){
      error_log(mysql_error());
    }
        
    //#####
    // FIN REQUETE  Ajout de points
    //######

    gagnerSuccesPoints();
    gagnerSuccesRessource();

   //#####
    // DEBUT REQUETE recuperer points
    //######
    $result = mysql_query("SELECT *
      FROM `cape_utilisateurs_categorie`
      WHERE `id_utilisateur` = $id_utilisateur
      AND `id_ressource` = $id_ressource");

    if(!$result)
        error_log(mysql_error());
      
    while($row_message_sc = mysql_fetch_assoc($result))
    {
      $score = $row_message_sc['score'];
      $id_categorie = $row_message_sc['id_categorie'];
    }

        
    //#####
    // FIN REQUETE  recuperer points
    //######
 
    $old_id_caterogie = $id_categorie;
    if ($id_categorie == 0 & $score >=PTSPARTICIPANT  ) $id_categorie = 1;
    if ($id_categorie == 1 & $score >=PTSCOLLABORATEUR  ) $id_categorie = 2;
    if ($id_categorie == 2 & $score >=PTSANIMATEUR  ) $id_categorie = 3;

    /*if ($id_categorie == 2 & $score < 30  ) $id_categorie = 1;
    if ($id_categorie == 3 & $score < 75  ) $id_categorie = 2;*/

    if ($old_id_caterogie != $id_categorie){
      //#####
      // DEBUT REQUETE changement de status
      //######


      $result = mysql_query("UPDATE `cape_utilisateurs_categorie`
      SET id_categorie = $id_categorie
      WHERE `id_utilisateur` = $id_utilisateur
      AND `id_ressource` = $id_ressource");

      if(!$result)
        error_log(mysql_error());
      

      gagnerSuccesParticipant();
      gagnerSuccesCollaborateur();
      gagnerSuccesAnimateur();

      //#####
      // FIN REQUETE  changement de status
      //######
    }


  }




  
 
 

  

   

    


?>