<?php

	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");

	//Récupérer les données
	$pseudo = $_POST['pseudo'];
	$id = $_POST['id'];
    $IDRESSOURCE = $_POST['idressource'];
	$html_fragment = '';
	$html_fragment = $html_fragment .  '<?xml version="1.0" encoding="UTF-8" ?>';
	
	//On détermine le GUID de l'utilisateur;
	$user = get_user_by_username($pseudo);
	$GUID = $user->guid;
	
	//On détermine si le message a été déjà voté
	$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_votesPertinence WHERE idMessage='%d' AND guidUtilisateur='%d'", $id, $GUID);
	$resultQuery = mysql_query($query);
	
	$count = mysql_fetch_array($resultQuery);
	
	if($count[0] == 0) {
		$dejaVote = "NON";
	}
	else {
		$dejaVote = "OUI";
	}
	
	mysql_free_result($resultQuery);
	
	//On détérmine si le message vient d'être supprimé
	$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_messagesSupprimes WHERE idMessage='%d'", $id);
	$resultQuery = mysql_query($query);
	
	$count = mysql_fetch_array($resultQuery);
	
	if($count[0] == 0) {
		$supprime = "NON";
	}
	else {
		$supprime = "OUI";
	}
	
	mysql_free_result($resultQuery);
	
	$html_fragment = $html_fragment .  '<message>';
	$html_fragment = $html_fragment .  '<dejaVote>';
	$html_fragment = $html_fragment .  $dejaVote;
	$html_fragment = $html_fragment .  '</dejaVote>';
	
	$html_fragment = $html_fragment .  '<supprime>';
	$html_fragment = $html_fragment .  $supprime;
	$html_fragment = $html_fragment .  '</supprime>';
	
	$html_fragment = $html_fragment .  '</message>';
	
	print($html_fragment);
	
?>