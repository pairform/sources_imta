<?php
//
//  postparidBlogRessource.php 
//  SupCast
//
//  créé par Didier PUTMAN
//  Copyright (c) 2012 EMN - CAPE. All rights reserved.
//
// Fonction : Post d’un message sur un blog de la ressource
//


	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");
	//Récupérer les données
	$idBlog = $_POST['idBlog'];
	$pseudo = $_POST['pseudo'];
	$password = $_POST['password'];
	$message = $_POST['message'];
	$IDRESSOURCE = $_POST['idressource'];
	$id = $_POST['id'];	

	//error_log(print_r($_POST, true));

	//Login
	if ($id == 's1ecfz9fex2z1dzsdra78')
	{       
		//Login
		if ($user = authenticate($pseudo,$password)) 
		{
			$result = login($user);
		}
		$entity_guid = $idBlog;
		//On crée le message-commentaire
		$entity = get_entity($entity_guid);
    	$user = get_loggedin_user();
    	$annotation = create_annotation($entity->guid, 'generic_comment', $message, "", $user->guid, $entity->access_id);
		//Logout
		if ($result) 
		{
			logout();	
		}	
	}
?>