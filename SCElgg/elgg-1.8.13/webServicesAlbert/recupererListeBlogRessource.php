<?php
 
	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");
	//Récupérer les données

    $pseudo = $_POST['pseudo'];
	$password = $_POST['password'];
    
    $IDRESSOURCE = $_POST['idressource'];

  

	//Login
	if ($user = authenticate($pseudo,$password)) 
	{
         $result = login($user);
	}
	
	$html_fragment = "";
	
	$html_fragment = $html_fragment . "<?xml version='1.0' encoding='UTF-8'?>
	";
	$html_fragment = $html_fragment . "<!DOCTYPE plist PUBLIC '-//Apple//DTD PLIST 1.0//EN' 'http://www.apple.com/DTDs/PropertyList-1.0.dtd'>
	";
	$html_fragment = $html_fragment . "<plist version='1.0'>
	";
	$html_fragment = $html_fragment . "<array>
	";

	if ($result)
	{
		//On récupère les messages associés
		$user = get_loggedin_user();
        // recupere la liste des blogs 	
		$getListeBlog = sprintf(
"SELECT 
b.`IDECRAN` IDECRAN,
b.`GUID_OE` GUID_OE,
b.`PARENT` PARENT,
b.`TITRE` TITRE,
a.`guid` GUID,
a.`title` TITLE,
a.`description` DESCRIPTION
FROM `CAPE_SUPCAST_BLOG` b
left outer join `{$CONFIG->dbprefix}objects_entity` a
on b.`GUID_OE` = a.`guid`
WHERE b.`IDRESSOURCE` = %d",$IDRESSOURCE
        );
		$getListeBlogQuery = mysql_query($getListeBlog);
		
        // Boucle sur la liste des blogs
		while($listeBlog = mysql_fetch_array($getListeBlogQuery))
		{
			//  
			$idecran  = $listeBlog['IDECRAN']; //IDECRAN du blog
			$GUID_OE  = $listeBlog['GUID_OE']; //GUID_OE du blog
			$parent  = $listeBlog['PARENT']; //PARENT du blog
			$titre  = $listeBlog['TITRE']; //TITRE du blog
			$guid = $listeBlog['GUID']; //IDECRAN
			$title = $listeBlog['TITLE']; //titlre identifiant du blog
			$description  = $listeBlog['DESCRIPTION']; //DESCRIPTION du blog
	     	
         	$html_fragment = $html_fragment . "<dict>
         	";
			$html_fragment = $html_fragment . "<key>idecran</key><string>" . $idecran . "</string>
			";
			$html_fragment = $html_fragment . "<key>guidoe</key><string>" . $GUID_OE . "</string>
			";
			$html_fragment = $html_fragment . "<key>parent</key><string>" . $parent . "</string>
			";
			$html_fragment = $html_fragment . "<key>titre</key><string>" . $titre . "</string>
			";
			$html_fragment = $html_fragment . "<key>guid</key><string>" . $guid . "</string>
			";
			$html_fragment = $html_fragment . "<key>title</key><string>" . $title . "</string>
			";
			$html_fragment = $html_fragment . "<key>description</key><string>" . $description . "</string>
			";

	    	$html_fragment = $html_fragment . "</dict>
	    	";
		
		}
		// the end of the loop
	
	logout();
	
	}

	$html_fragment = $html_fragment . "</array>
	";
	$html_fragment = $html_fragment . "</plist>";
    print ($html_fragment);

?>
