<?php

  //error_log('//********************************************//');
  //error_log('//*********** recupererMessages.php **********//');
  //error_log('//********************************************//');

  //Recuperation des valeurs des messages

        /*  JSON : FAUX POUR L'INSTANT
        {
          id_blog :
          {
            id_message:
            {
              id_message: $
              id_blog: $
              owner_guid: $
              id_ressource: $
              nom_page: $
              nom_tag: $
              num_occurence: $
              value: $

              prop:
              {
                geo_lattitude: $
                geo_longitude: $
                supprime_par: $
              }
            }
            ...
          }
          ...
        }
        */    
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

	$os = $_POST['os'];
  $version = $_POST['version'];
  //$id = $_POST['id_message'];

  //error_log(print_r($_SESSION,true));

  switch ($os) {
    case 'web':

      $array_grains = array();

      $granularite = ''; //globale, ressource ou page

      //En fonction de ce que l'on envoie au webservice, on va faire une sélection plus fine des messages
      if(isset($_POST['id_ressource']) && isset($_POST['nom_page']))
      {
        $id_ressource = $_POST['id_ressource'];
        $nom_page = $_POST['nom_page'];
        $granularite = 'page';

        $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }

      else if(isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
      {
        $id_ressource = $_POST['id_ressource'];
        $granularite = 'ressource';
        $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }


      else if(!isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
      {        
        $granularite = 'globale';
        $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }


      //Pour chaque message
      while($row_message_sc = mysql_fetch_assoc($result_messages))
      {
        //On chope les variables réutilisées souvent, pour éviter les accès au tableau et pour la lisibilité
        $message_id_ressource = $row_message_sc['id_ressource'];
        $message_nom_page = $row_message_sc['nom_page'];
        $message_nom_tag = $row_message_sc['nom_tag'];
        $message_num_occurence = $row_message_sc['num_occurence'];
        
        
        //Options passées 
        $options = array('guid' => $row_message_sc['id_message'], 'type' => 'object');
        
        //Recupération des annotations dans la base ELGG 
        /*
        $options = {'guid' => $row_message_sc['id_message'],
                'type' => $entity_type,
                'subtype' => $entity_subtype,
                'annotation_name' => $name,
                'annotation_value' => $value,
                'annotation_owner_guid' => 0,        
                'limit' => 50,
                'offset' => 0,
                'order_by' => 'n_table.time_created asc',
                'annotation_time_lower' => 0,
                'annotation_time_upper' => 0,
                'owner_guid' => 0};
                */


        $messages_elgg = elgg_get_entities($options);
        $message_elgg = $messages_elgg[0];

        /* Retour elgg_get_entities : 
              [attributes:protected] => Array
                (
                    [guid] => 73
                    [type] => object
                    [subtype] => 4
                    [owner_guid] => 68
                    [site_guid] => 1
                    [container_guid] => 68
                    [access_id] => 1
                    [time_created] => 1363094781
                    [time_updated] => 1363094781
                    [last_action] => 1363094781
                    [enabled] => yes
                    [title] => 7/module_droit/PAGE/0
                    [description] => <p>Test de message sur la page d'introduction de droits d'auteur</p>
                    [tables_split] => 2
                    [tables_loaded] => 2
                )

        */

           
        //On store ces valeurs, parce que les crochets passent mal dans la formation de la requête SQL
        $id_message_temp = $message_elgg->guid;
        $id_owner = $message_elgg->owner_guid;

        
        $result_messages_enrichis = mysql_query("SELECT `geo_lattitude`, `geo_longitude`, `supprime_par`, `sce_users_entity`.`username`, `cape_utilisateurs_categorie`.`id_categorie`, `cape_categories`.`nom`
          FROM `cape_messages`, `sce_users_entity`, `cape_utilisateurs_categorie`, `cape_categories` 
          WHERE `cape_messages`.`id_message` = $id_message_temp 
          AND `sce_users_entity`.`guid` = $id_owner 
          AND `cape_utilisateurs_categorie`.`id_ressource` = $id_ressource
          AND `cape_utilisateurs_categorie`.`id_utilisateur` = $id_owner
          AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie`");

        if($result_messages_enrichis)
        {  
            $row_message = mysql_fetch_assoc($result_messages_enrichis);
            
            //On forme le message
            $array_message = array(
                                "id_message" => $message_elgg->guid,
                                "owner_guid" => $message_elgg->owner_guid,
                                "owner_username" => $row_message['username'],
                                "owner_rank_id" => $row_message['id_categorie'],
                                "owner_rank_name" => $row_message['nom'],
                                "value" => $message_elgg->description,
                                "geo_lattitude" => $row_message['geo_lattitude'],
                                "geo_longitude" => $row_message['geo_longitude'],
                                "supprime_par" => $row_message['supprime_par']
                            );
        }
        else 
          error_log(mysql_error());
      
      //En fonction de la granularité demandé, on génère un retour plus ou moins profond
        switch ($granularite) {
          case 'globale':

            if(!isset($array_grains[$message_id_ressource]))
              $array_grains[$message_id_ressource] = array();

            if(!isset($array_grains[$message_id_ressource][$message_nom_page]))
              $array_grains[$message_id_ressource][$message_nom_page] = array();

            if(!isset($array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag]))
              $array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag] = array();

            if(!isset($array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence]))
              $array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence] = array("id_ressource" => $message_id_ressource,
                                      "nom_page" => $message_nom_page,
                                      "nom_tag" => $message_nom_tag,
                                      "num_occurence" => $message_num_occurence);

            $array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence]['messages'][$message_elgg->guid] =  $array_message; 

            break;

          case 'ressource':
            
            if(!isset($array_grains[$message_nom_page]))
              $array_grains[$message_nom_page] = array();

            if(!isset($array_grains[$message_nom_page][$message_nom_tag]))
              $array_grains[$message_nom_page][$message_nom_tag] = array();

            if(!isset($array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence]))
              $array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence] = array("id_ressource" => $message_id_ressource,
                                      "nom_page" => $message_nom_page,
                                      "nom_tag" => $message_nom_tag,
                                      "num_occurence" => $message_num_occurence);

            $array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence]['messages'][$message_elgg->guid] =  $array_message;

            break;

          case 'page':

            if(!isset($array_grains[$message_nom_tag]))
              $array_grains[$message_nom_tag] = array();

            if(!isset($array_grains[$message_nom_tag][$message_num_occurence]))
              $array_grains[$message_nom_tag][$message_num_occurence] = array("id_ressource" => $message_id_ressource,
                                      "nom_page" => $message_nom_page,
                                      "nom_tag" => $message_nom_tag,
                                      "num_occurence" => $message_num_occurence);
            error_log(print_r($array_message,true));
            $array_grains[$message_nom_tag][$message_num_occurence]['messages'][$message_elgg->guid] =  $array_message;

            break;

          default:
            # La réponse D.
            break;
        }

        $array_grains[$message_nom_tag][$message_num_occurence]['num_messages'] = count($array_grains[$message_nom_tag][$message_num_occurence]['messages']);
      }
      

      //error_log('Array_grains : ' . print_r($array_grains, true));
      $return = json_encode($array_grains);

      //error_log('Retour JSON : ' . $return);

      //Retour JSON
      print($return);
      
      break;
    case 'ios':
      # code...
      break;
    case 'and':
      # code...
      break;
    
    default:
      print('La réponse D');
      break;
  }

?>