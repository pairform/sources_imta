<?php

header("cache-Control: no-cache, must-revalidate");  
header("Expires: Mon, 26 Jul 2011 05:00:00 GMT");

	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");
	//Récupérer les données
	
	$idBlog = $_POST['idBlog'];
	$pseudo = $_POST['pseudo'];
	$password = $_POST['password'];
	$orderBy = $_POST['orderBy'];
	$lastMessage = $_POST['last'];
    $ressourceid = $_POST['ressourceid'];
    $categorie = $_POST['categorie'];
    $elggid = $_POST['elggid'];
	$aRecuperer = $lastMessage + 20;
	
	//error_log("POST = ".print_r($_POST,true));

	//Login authentification
	if ($user = authenticate($pseudo,$password)) {
         
		 $result = login($user);
		
	}
	
	//error_log("user = {$user} ");
	//error_log("result = {$result} ");

	//On verifie que la ressource existe
	$query = sprintf("SELECT `ressourcenom` FROM `CAPE_RESSOURCES` WHERE ressourcevisible=true and ressourceid=%d and ressourceelggid=%d", $ressourceid, $elggid);
    $resultQuery = mysql_query($query);
	
	while ($row = mysql_fetch_assoc($resultQuery)) {
		$ressourcenom = $row['ressourcenom'];
	}
	//error_log("ressourcenom = {$ressourcenom} ");


	//On verifie que le blog existe
	$query = sprintf("SELECT `IDRESSOURCE`,`IDECRAN`,`GUID_OE`,`PARENT`,`TITRE` FROM `CAPE_SUPCAST_BLOG` WHERE IDRESSOURCE=%d and GUID_OE = %d" , $ressourceid, $idBlog);
    $resultQuery = mysql_query($query);
	
	while ($row = mysql_fetch_assoc($resultQuery)) {
		$TITRE = $row['TITRE'];
	}
	//error_log("TITRE = {$TITRE} ");


//	mysql_free_result($resultQuery);
	
  	$entity_guid = $idBlog;
	
	//Ecriture dans fichier
	//$nomFichier = $_POST['nomFichier'];
	//$filename = $nomFichier . '.xml';
	
	//$handle = fopen($filename, 'xb');

	$html_fragment ="";
	$html_fragment = "";
	
	$html_fragment = $html_fragment . "<?xml version='1.0' encoding='UTF-8'?>
	";
	$html_fragment = $html_fragment . "<!DOCTYPE plist PUBLIC '-//Apple//DTD PLIST 1.0//EN' 'http://www.apple.com/DTDs/PropertyList-1.0.dtd'>
	";
	$html_fragment = $html_fragment . "<plist version='1.0'>
	";
	
	$html_fragment = $html_fragment . "<array>
	";
	//Logout
	if ($result) 
	{
		//On récupère les messages associés
		$user = get_loggedin_user();

		//on récupére l'id du dernier message posté sur ce blog
		$getAnnotation = sprintf("SELECT max(id) FROM {$CONFIG->dbprefix}annotations WHERE entity_guid=%d",$entity_guid);
		$getAnnotationQuery = mysql_query($getAnnotation);
		$row = mysql_fetch_array($getAnnotationQuery);
		$lastMessageAssociatedWithAnnotation = $row[0];
		
        //error_log("entity_guid = {$entity_guid} ");
        //error_log("user->guid = {$user->guid} ");

		//on vérifie si l'utilisateur a déjà lu ce blog
		$CountuserLastRead = sprintf("SELECT count(ID_MSG) FROM CAPE_USER_OE WHERE GUID_OE=%d AND GUID_USER=%d",$entity_guid,$user->guid);
    	$CountuserLastReadQuery = mysql_query("$CountuserLastRead");
    	$CountuserLastMessage = mysql_fetch_array($CountuserLastReadQuery);
 		$CountlastMessageReadByUser = $CountuserLastMessage[0];
        //error_log("CountlastMessageReadByUser = {$CountlastMessageReadByUser} ");

		// si blog non lu, on insére une nouvelle ligne avec l'id du dernier message posté sur ce blog
		if (($CountlastMessageReadByUser == "0") || ($CountlastMessageReadByUser == 0)  )
        {
			$addUserMessage=sprintf("INSERT INTO CAPE_USER_OE VALUES (%d,%d,%d)",$user->guid,$entity_guid,$lastMessageAssociatedWithAnnotation);
			mysql_query($addUserMessage);
        }
        else 
		// si blog lu, on met a jour ligne avec l'id du dernier message posté sur ce blog
        {
			//modfication dans la table CAPE_USER_OE de l identifiant du dernier message par l utlisateur pour ce blog
			$updateUser=sprintf("UPDATE CAPE_USER_OE set ID_MSG=%d Where GUID_USER=%d AND GUID_OE=%d",$lastMessageAssociatedWithAnnotation,$user->guid,$entity_guid);
			mysql_query($updateUser);
		}
		
		//finish update_BAO
	    
	    //error_log("entity_guid = {$entity_guid} ");

    	$annotations = get_annotations($entity_guid, "", "", "", "", 0, $aRecuperer, 0, $orderBy);
	
		//On ne retourne que les derniers 20 messages
		for ($i=$lastMessage ; $i < count($annotations) ; $i++) 
		{
		
			$annotation = $annotations[$i]; 
			$supprimePar = "";


			$id = $annotation->id;
	        //error_log("id = {$id} ");
			$text = $annotation->value;
			$userGUID = $annotation->owner_guid;
			$user = get_user($userGUID);
			$username = $user->username;
		    //error_log("username = {$username} ");
	
			//On détermine la catégorie d'utilisateur
			$query = sprintf("SELECT categorie FROM CAPE_utilisateurs WHERE username='%s'", mysql_real_escape_string($username));
			$resultQuery = mysql_query($query);
	
			while ($row = mysql_fetch_assoc($resultQuery)) 
			{
				$categorie = $row['categorie'];
			}
	
			mysql_free_result($resultQuery);
			//error_log("categorie = {$categorie} ");

			
			//On vérifie que le message n'est pas "supprimé"
			$query = sprintf("SELECT supprimePar FROM CAPE_messagesSupprimes WHERE idMessage='%d'", $id);
			$resultQuery = mysql_query($query);
	
			while ($row = mysql_fetch_assoc($resultQuery)) 
			{
				$supprimePar = $row['supprimePar'];
			}
	
			mysql_free_result($resultQuery);
			
			//On calcule le score de pertinence
			$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_votesPertinence WHERE idMessage=%d AND estPertinent='OUI'", $id);
			$resultQuery = mysql_query($query);
			$votesPositifs = mysql_fetch_array($resultQuery);
		
			$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_votesPertinence WHERE idMessage=%d AND estPertinent='NON'", $id);
			$resultQuery = mysql_query($query);
			$votesNegatifs = mysql_fetch_array($resultQuery);

			//On détermine le pseudo associé à "supprimePar"
			$userSupprime = get_user($supprimePar);
			$usernameSupprime = $userSupprime->username;
			//error_log("votesPositifs = ".print_r($votesPositifs,true));
			//error_log("votesNegatifs = ".print_r($votesNegatifs,true));
			//error_log("username = {$username} ");
			//error_log("text = {$text} ");

			if(($username != "") && ($text != "")) 
			{			
				$html_fragment .= "<dict>
<key>user</key>
<string>".$username."</string>

<key>categorie</key>
<string>".$categorie."</string>

".$text."

<key>id</key>
<string>".$id."</string>

<key>supprime</key>
<string>".$usernameSupprime."</string>

<key>votesPositifs</key>
<string>".$votesPositifs[0]."</string>

<key>votesNegatifs</key>
<string>".$votesNegatifs[0]."</string>

</dict>";

			}
		
		}

	logout();
		
	}

	$html_fragment = $html_fragment . "</array>";
	$html_fragment = $html_fragment . "</plist>";
	
	error_log($html_fragment);
    print ($html_fragment);

?>