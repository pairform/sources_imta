<?php
    /*
     * Liste les ressources téléchargeables
     * 
     */
    
	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
    
	$os = $_POST['os'];
    $version = $_POST['version'];
    $id_elgg = $_POST['id_elgg'];
    
    //error_log("**********************************************");
    //error_log("ListeApplication.php appellé"); 
    //error_log("**********************************************");
	
    //error_log(print_r($_POST,true));
    
    $query = sprintf("SELECT 
        `id_ressource`,
        `cape_ressources`.`nom_court`,
        `cape_ressources`.`nom_long`,
        `url`,
        `url_web`,
        `icone`,
        `description`,
        `visible`,
        `cape_etablissements`.`nom_court` etablissement,
        `cape_themes`.`nom_long` theme,
        `date_update`,
        `date_release`,
        `taille`
        FROM `cape_ressources`, 
        `cape_etablissements`,
        `cape_themes` where 
        `cape_themes`.`id_theme` = `cape_ressources`.`theme` 
        and `cape_etablissements`.`id_etablissement` = `cape_ressources`.`etablissement` ");   
                     
    $resultQuery = mysql_query($query);
    error_log(mysql_error());
                     
    $rows = array();
    
    switch ($os) {
        case 'and':
            while ($row = mysql_fetch_assoc($resultQuery)) 
            {
                     
            //error_log('Ressource : ' . $row['nom']); 
            $visible = $row['visible'];
                     
            //error_log('$visible (default) : '.$visible);
            //On checke si la ressource est en développement
            //Visible == false?
            if($visible == 0)
            {
                     //Gestion des ressources en développement : récupération de l'ID user
                     $req = "SELECT id_ressource FROM cape_utilisateurs_ressources WHERE id_elgg = '$id_elgg'";
                     $res = mysql_query($req);
                     
                     while($row = mysql_fetch_assoc($res))
                     {
                        //Si il a le droit, on dit à l'application web que la ressource est visible.
                        if($row['id_ressource'] == $id_elgg)
                            $visible = 1;
                     
                        //error_log('$visible (modifié) : '.$visible);
                     }
            }
            if($visible)
                     array_push($rows, array('id_ressource' => $row['id_ressource'], 'nom_court' => $row['nom_court'], 'nom_long' => $row['nom_long'], 'url' => $row['url'], 'icone' => $row['icone'], 'description' => $row['description'], 'taille' => $row['taille'], 'etablissement' => $row['etablissement'], 'theme' => $row['theme']));
                     
            }
            $return = json_encode($rows);
            print ($return);
            //error_log(print_r($return,true));
            break;
        
        case 'web':
            while ($row = mysql_fetch_assoc($resultQuery)) 
            {
                             
                //error_log('Ressource : ' . $row['nom']); 
                $visible = $row['visible'];
                             
                //error_log('$visible (default) : '.$visible);
                //On checke si la ressource est en développement
                //Visible == false?
                if($visible == 0)
                {
                    //Gestion des ressources en développement : récupération de l'ID user
                    $req = "SELECT id_ressource FROM cape_utilisateurs_ressources WHERE id_elgg = '$id_elgg'";
                    $res = mysql_query($req);
                             
                    while($row = mysql_fetch_assoc($res))
                    {
                        //Si il a le droit, on dit à l'application web que la ressource est visible.
                        if($row['id_ressource'] == $id_elgg)
                            $visible = 1;
                             
                        //error_log('$visible (modifié) : '.$visible);
                    }
                }
                if($visible)
                    array_push($rows, array('id_ressource' => $row['id_ressource'], 'nom_court' => $row['nom_court'], 'nom_long' => $row['nom_long'], 'url_web' => $row['url_web'], 'icone' => $row['icone'], 'description' => $row['description'], 'taille' => $row['taille'], 'etablissement' => $row['etablissement'], 'theme' => $row['theme']));
                             
            }
            $return = json_encode($rows);
            print ($return);
            //error_log(print_r($return,true));
            break;
                             
        case 'ios':
            break;
        
        default:
            print('La réponse D');
            break;
    }
                             
?>
