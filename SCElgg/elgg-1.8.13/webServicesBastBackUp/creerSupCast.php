<?php
	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");
	//Récupérer les données
	$nom = $_POST['nom'];
	$prenom = $_POST['prenom'];
	$email = $_POST['email'];
	$pseudo = $_POST['pseudo'];
	$password = $_POST['password'];
	$html_fragment =$html_fragment . "";
	$html_fragment =$html_fragment . "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	$html_fragment =$html_fragment . "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
	$html_fragment =$html_fragment . "<plist version=\"1.0\">";
	$html_fragment =$html_fragment . "	<dict>";
	$html_fragment =$html_fragment . "		<key>nom</key>";
	$html_fragment =$html_fragment . "		<string>" . $pseudo . "</string>";
	//Les adresses mail doivent être uniques
	$usedEmail = get_user_by_email($email);
	//On vérifie si le pseudo est disponible
	$usedPseudo = get_user_by_username($pseudo);
	$html_fragment =$html_fragment . "		<key>status</key>";
	if(!$usedEmail && !$usedPseudo)
	{
		//Créer utilisateur - appels API Elgg
		$prenomNom = $prenom . ' ' . $nom;
		$guid = register_user($pseudo, $password, $prenomNom, $email);
		uservalidationbyemail_request_validation_supcast($guid); 	
		//On crée l'enregistrement dans la table d'utilisateurs qui gère les niveaux (CAPE_utilisateurs)
		$query = sprintf("INSERT INTO CAPE_USER_SUPCAST (guid) VALUES ('%d')", $guid);
		$resultQuery = mysql_query($query);
		$html_fragment =$html_fragment . "		<string>OK</string>";
	}
	else if($usedPseudo)
	{
		$html_fragment =$html_fragment . "		<string>ErreurPseudo</string>";
	}
	else
	{
		$html_fragment =$html_fragment . "		<string>ErreurEMail</string>";
	}
	$html_fragment =$html_fragment . "	</dict>";
	$html_fragment =$html_fragment . "</plist>";
	print ($html_fragment);
?>