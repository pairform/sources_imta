<?php
	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");
	
	//error_log("On pénètre dans recupererNiveauRes.php ...");
	//Récupérer les données
	$guid = $_POST['guid'];
	$ressourceid = $_POST['ressource'];
	
		
	
	//*********************** Paramètres définis par défaut ****************************
	$scoreMessagePertinent = 1;
	$scoreMessageNonPertinent = -1;
	$scoreMessageSupprime = -2;
	$seuilNoviceApprenti = 8;
	$seuilApprentiMaitre = 30;
	$ancienNiveau = "";
	$html_fragment =  "<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE plist PUBLIC '-//Apple//DTD PLIST 1.0//EN' 'http://www.apple.com/DTDs/PropertyList-1.0.dtd'>
<plist version='1.0'>
 <array>
  <dict>
   <key>guid</key>
   <string>" . $guid . "</string>";
			
	//************************ Gestion du niveau par ressource ****************************
	//error_log("********************************************");
	//error_log("Numéro d'utilisateur : ".$guid);
	//On récupère le niveau de l'utilisateur lors de sa dernière connexion
	$query = "SELECT categorie FROM CAPE_niveau_ressource WHERE GUID='$guid' AND ressourceid='$ressourceid'";
	$res = mysql_query($query);
	//error_log("Requête : ".$query);
	//Si l'utilisateur est déjà dans la table
	if($res)
	{
		//On chope son niveau
		while ($row = mysql_fetch_assoc($res)) {
			$ancienNiveau = $row['categorie'];
		}
	
		//Sinon, on l'insère
		if(!$ancienNiveau)
		{
			//error_log("Utilisateur non présent dans la table");
			//On récupère son niveau global
			$query = "SELECT * FROM CAPE_utilisateurs WHERE guid='$guid'";
			$resultQuery = mysql_query($query);
			if($resultQuery)
			while ($row = mysql_fetch_assoc($resultQuery)) {
				$niveauGlobal = $row['categorie'];
			}
			//error_log("Niveau global : ".$niveauGlobal);
			//On l'associe à la ressource courante
			$query = "INSERT INTO CAPE_niveau_ressource (guid, ressourceid, categorie) VALUES ('$guid','$ressourceid','$niveauGlobal')";
			$res = mysql_query($query);
			
			//error_log("Requête d'enregistrement dans la base : ".$query);
			switch($niveauGlobal)
			{
				case "gardien du temple":
					$ancienNiveau = $niveauGlobal;
					break;
				case "sage":
					$ancienNiveau = $niveauGlobal;
					break;
				case "default":
					$ancienNiveau = "novice";
					break;
			}
			
		}
	}
		
	//error_log("Ancien niveau : ".$ancienNiveau);
	//************************ Calcul du nouveau niveau  ****************************
	
	$query = sprintf("SELECT COUNT(owner_guid) FROM {$CONFIG->dbprefix}annotations WHERE owner_guid='%d'", $guid);
	$resultQuery = mysql_query($query);
	$nombreTotalMessagesCreesParUtilisateur = mysql_fetch_array($resultQuery);
	
	$scoreTotal = 0;
	
	//Pour chaque message créé par l'utilisateur, on calcule son score (vérifier s'il est pertinent ou pas / s'il a été supprimé)
	$annotations = get_annotations(0, "", "", "", "", $guid, $nombreTotalMessagesCreesParUtilisateur[0], 0, "desc");
	
	for ($i=0 ; $i < $nombreTotalMessagesCreesParUtilisateur[0] ; $i++) 
	{
		
		$annotation = $annotations[$i]; 
		$idMessage = $annotation->id;
		
		$difference = 0;
		$scorePertinents = 0;
			
		//Est-il pertinent : calcul de la différence OUI - NON dans la table "CAPE_votesPertinence"
		$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_votesPertinence WHERE idMessage=%d AND estPertinent='OUI'", $idMessage);
		$resultQuery = mysql_query($query);
		$votesPositifs = mysql_fetch_array($resultQuery);
		
		$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_votesPertinence WHERE idMessage=%d AND estPertinent='NON'", $idMessage);
		$resultQuery = mysql_query($query);
		$votesNegatifs = mysql_fetch_array($resultQuery);
		
		$difference = $votesPositifs[0] - $votesNegatifs[0];
		
		if($difference == 0) {
			$scorePertinents = 0;
		}
		
		if($difference > 0) {
			$scorePertinents = $scoreMessagePertinent;
		}
		
		if ($difference < 0) {
			$scorePertinents = $scoreMessageNonPertinent;
		}
		
		$scoreTotal = $scoreTotal + $scorePertinents;
	
		//Est-ce que ce message a été supprimé ?
		$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_messagesSupprimes WHERE idMessage='%d'", $idMessage);
		$resultQuery = mysql_query($query);
		$messageSupprime = mysql_fetch_array($resultQuery);	
		
		$scoreTotal = $scoreTotal + ($scoreMessageSupprime * $messageSupprime[0]);
	
	}
	
	//$html_fragment = $html_fragment . $scoreTotal;
	
	if(($ancienNiveau != "sage") && ($ancienNiveau != "gardien du temple"))
	{
		
		//On détermine le nouveau niveau
		if ($scoreTotal <= ($seuilNoviceApprenti - 2)) {
		
			$nouveauNiveau = "novice";
		
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='novice' WHERE username='%s'", mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "1er IF");
			
		}
		
		if ((($seuilNoviceApprenti - 2) < $scoreTotal) && ($scoreTotal <= ($seuilNoviceApprenti + 2))) {
		
			if ($ancienNiveau == "maître") {
				$nouveauNiveau = "apprenti";
			}
			
			if ($ancienNiveau == "apprenti") {
				$nouveauNiveau = "apprenti";
			}
			
			if ($ancienNiveau == "novice") {
				$nouveauNiveau = "novice";
			}
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='%s' WHERE username='%s'", mysql_real_escape_string($nouveauNiveau), mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "2e IF");
			
		}			
		
		if ((($seuilNoviceApprenti + 2) < $scoreTotal) && ($scoreTotal <= ($seuilApprentiMaitre - 2))) {
			
			$nouveauNiveau = "apprenti";
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='apprenti' WHERE username='%s'", mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "3e IF");
						
		}
		
		if ((($seuilApprentiMaitre - 2) < $scoreTotal) && ($scoreTotal <= ($seuilApprentiMaitre + 2))) {
		
			if ($ancienNiveau == "maître") {
				$nouveauNiveau = "maître";
			}
			
			if ($ancienNiveau == "apprenti") {
				$nouveauNiveau = "apprenti";
			}
			
			if ($ancienNiveau == "novice") {
				$nouveauNiveau = "apprenti";
			}
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='%s' WHERE username='%s'", mysql_real_escape_string($nouveauNiveau), mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "4e IF");
			
		}		
		
		if (($seuilApprentiMaitre + 2) < $scoreTotal) {
			
			$nouveauNiveau = "maître";
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='maître' WHERE username='%s'", mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "5e IF");
			
		}
		
		$html_fragment .=  "
   <key>nouveauNiveau</key>
   <string>".$nouveauNiveau."</string>";
	}
	//Si on est sage ou gardien du temple
	else
	{
		$html_fragment .=  "
   <key>nouveauNiveau</key>
   <string>".$ancienNiveau."</string>";
	}
	
	$html_fragment .=  "
   <key>ancienNiveau</key>
   <string>".$ancienNiveau."</string>";
		
	$html_fragment .=  "
  </dict>
 </array>
</plist>";
	//error_log($html_fragment);
    print ($html_fragment);
?>
