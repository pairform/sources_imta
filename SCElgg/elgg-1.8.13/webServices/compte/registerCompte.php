<?php

  //********************************************//
  //*********** RegisterCompte.php *************//
  //********************************************//
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

	$os = $_POST['os'];
  $version = $_POST['version'];
  //$id = $_POST['id_elgg'];
	
  switch ($os) {
    case 'web':
      //error_log(print_r($_POST,TRUE));
      //Elgg values
      $username = $_POST['username'];
      $password = $_POST['password'];
      $password2 = $_POST['password2']; 
      $email = $_POST['email'];
      $name = $_POST['name'];

      //SupCast values
      $etablissement = $_POST['etablissement'];

      //Defaults values 
      $friend_guid = 0; 
      $invitecode = "";

      //Elgg registration
      if (elgg_get_config('allow_registration')) {
        try {
          if (trim($password) == "" || trim($password2) == "") {
            print(false);
          }

          if (strcmp($password, $password2) != 0) {
            print(false);
          }

          $guid = register_user($username, $password, $name, $email, false, $friend_guid, $invitecode);
          //error_log("GUID : " . $guid);
          if ($guid) {
            $new_user = get_entity($guid);

            // allow plugins to respond to self registration
            // note: To catch all new users, even those created by an admin,
            // register for the create, user event instead.
            // only passing vars that aren't in ElggUser.
            $params = array(
              'user' => $new_user,
              'password' => $password,
              'friend_guid' => $friend_guid,
              'invitecode' => $invitecode
            );

            // @todo should registration be allowed no matter what the plugins return?
            if (!elgg_trigger_plugin_hook('register', 'user', $params, TRUE)) {
              $new_user->delete();
              // @todo this is a generic messages. We could have plugins
              // throw a RegistrationException, but that is very odd
              // for the plugin hooks system.
              print(false);
            }

            elgg_clear_sticky_form('register');
            system_message(elgg_echo("registerok", array(elgg_get_site_entity()->name)));

            // if exception thrown, this probably means there is a validation
            // plugin that has disabled the user
            try {
              login($new_user);
            } catch (LoginException $e) {
              // do nothing
            }

            //error_log('GUID : ' . $guid);
            //SupCast registration
            //On entre l'user dans la base SC avec les infos complémentaires
            $result = mysql_query("INSERT INTO cape_utilisateurs (id_elgg, etablissement) VALUES ('$guid','$etablissement')");
            if($result)
            {
              //On récupère toutes les ressources, pour insérer le niveau 0 (Lecteur) dans la base pour chaque ressource
              $result = mysql_query("SELECT id_ressource FROM cape_ressources");
              if($result)
              {
                $values = "";
                //Pour chaque ressource, on construit une insertion pour la base cape_utilisateurs_categorie
                while ($row = mysql_fetch_assoc($result)) {
                  $values .= "(".$row['id_ressource'].",".$guid.",0),";
                }
                //On sucre la dernière virgule de la chaîne
                $valuesOutput = rtrim($values, ",");
              }
              else error_log('Recup ressource : ' . mysql_error());

              //error_log("Values Output : " . $valuesOutput);
              //On insère les niveaux 0 dans la base
              $result = mysql_query("INSERT INTO cape_utilisateurs_categorie (id_ressource, id_utilisateur, id_categorie) VALUES $valuesOutput");
              
              if ($result) {
                //On retourne le nouvel utilisateur fraichement créé et loggé à SC
                $return = json_encode(array('id' => $guid, 'username' => $username, 'rank' => 0));
                //error_log("JSON : " . $return);
                print($return);
              }
              else error_log('Enregistrement ressource / rank : ' . mysql_error());
            }
            else error_log('Erreur Enregistrement cape_utilisateurs : ' . mysql_error());

          } else {
            register_error(elgg_echo("registerbad"));
          }
        } catch (RegistrationException $r) {
          register_error($r->getMessage());
        }
      } else {
        register_error(elgg_echo('registerdisabled'));
      }

      break;
    case 'ios':
      //error_log(print_r($_POST,TRUE));
      //Elgg values
      $username = $_POST['username'];
      $password = $_POST['password'];
      $email = $_POST['email'];
      $name = strtoupper($_POST['name']).' '.$_POST['surname'];

      //SupCast values
      // $etablissement = $_POST['etablissement'];

      //Les adresses mail doivent être uniques
      if($usedEmail = get_user_by_email($email))
      {
        $return = json_encode(array("code" => "email"));
        error_log($return);
        print($return);
        exit;
      }

      //On vérifie si le pseudo est disponible
      if($usedUserName = get_user_by_username($username))
      {
        $return = json_encode(array("code" => "username"));
        error_log($return);
        print($return);
        exit;
      }

      //Defaults values 
      $friend_guid = 0; 
      $invitecode = "";

      //Elgg registration
      if (!$usedEmail && !$usedUserName && elgg_get_config('allow_registration')) {
        try {
          
          $guid = register_user($username, $password, $name, $email, false, $friend_guid, $invitecode);
          //error_log("GUID : " . $guid);
          if ($guid) {
            $new_user = get_entity($guid);

            // allow plugins to respond to self registration
            // note: To catch all new users, even those created by an admin,
            // register for the create, user event instead.
            // only passing vars that aren't in ElggUser.
            $params = array(
              'user' => $new_user,
              'password' => $password,
              'friend_guid' => $friend_guid,
              'invitecode' => $invitecode
            );

            // @todo should registration be allowed no matter what the plugins return?
            if (!elgg_trigger_plugin_hook('register', 'user', $params, TRUE)) {
              $new_user->delete();
              // @todo this is a generic messages. We could have plugins
              // throw a RegistrationException, but that is very odd
              // for the plugin hooks system.
              
              $return = json_encode(array("code" => "error"));
              error_log($return);
              print($return);
              exit;
            }
            elgg_clear_sticky_form('register');
            system_message(elgg_echo("registerok", array(elgg_get_site_entity()->name)));

            // if exception thrown, this probably means there is a validation
            // plugin that has disabled the user
            try {
              login($new_user);
            } catch (LoginException $e) {
              // do nothing
            }

            //error_log('GUID : ' . $guid);
            //SupCast registration
            //On entre l'user dans la base SC avec les infos complémentaires
            $result = mysql_query("INSERT INTO cape_utilisateurs (id_elgg, etablissement) VALUES ('$guid','$etablissement')");
            if($result)
            {
              //On récupère toutes les ressources, pour insérer le niveau 0 (Lecteur) dans la base pour chaque ressource
              $result = mysql_query("SELECT id_ressource FROM cape_ressources");
              if($result)
              {
                $values = "";
                //Pour chaque ressource, on construit une insertion pour la base cape_utilisateurs_categorie
                while ($row = mysql_fetch_assoc($result)) {
                  $values .= "(".$row['id_ressource'].",".$guid.",0),";
                }
                //On sucre la dernière virgule de la chaîne
                $valuesOutput = rtrim($values, ",");
              }
              else error_log('Recup ressource : ' . mysql_error());

              //error_log("Values Output : " . $valuesOutput);
              //On insère les niveaux 0 dans la base
              $result = mysql_query("INSERT INTO cape_utilisateurs_categorie (id_ressource, id_utilisateur, id_categorie) VALUES $valuesOutput");
              
              if ($result) {
                //On retourne le nouvel utilisateur fraichement créé et loggé à SC
                $return = json_encode(array('id' => $guid, 'username' => $username, 'rank' => 0));
                //error_log("JSON : " . $return);
                $return = json_encode(array("code" => "ok"));
                error_log($return);
                print($return);
                exit;
              }
              else error_log('Enregistrement ressource / rank : ' . mysql_error());
            }
            else error_log('Erreur Enregistrement cape_utilisateurs : ' . mysql_error());

          } else {
            register_error(elgg_echo("registerbad"));
          }
        } catch (RegistrationException $r) {
          register_error($r->getMessage());
        }
      } else {
        register_error(elgg_echo('registerdisabled'));
      }

      break;
    case 'and':
      # code...
      break;
    
    default:
      print('La réponse D');
      break;
  }

?>