<?php

	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");

	// Make sure we're logged in (send us to the front page if not)	
	// Get post data
	$titleparent   = $_POST['titleparent'];
	$title         = $_POST['blogtitle'];
	$body          = $_POST['blogbody'];
	$tags          = $_POST['blogtags'];
	$access        = (int)$_POST['access_id'];
	$comments_on   = true;
	$idecran       = (int)$_POST['idecran'];
	$idecranparent = (int)$_POST['idecranparent'];		
	$pseudo        = $_POST['pseudo'];
    $password      = $_POST['password'];
    $IDRESSOURCE   = (int)$_POST['idressource'];

    $access = 1;
	
	//Login
	if ($user = authenticate($pseudo,$password)) 
	{
         $result = login($user);
	}
    
	if ($result) 
	{
		$user = get_loggedin_user();
        
        $getRessourceDescription = sprintf("SELECT `ressourcetitle` FROM `CAPE_RESSOURCES` where `ressourceid`= %d",$IDRESSOURCE);
    	$getRessourceDescriptionQuery = mysql_query($getRessourceDescription);
		
        // Boucle sur la liste des blogs
		$rowressourcetitle = mysql_fetch_array($getRessourceDescriptionQuery);
		if (!empty($rowressourcetitle)) 
        {
		    $ressourcetitle = $rowressourcetitle['ressourcetitle']; //ressourcetitle de la ressource
	    }
        
	    // On récupère le titre de l'écran
        // On vérifie que le idecran/ressource est dans la table CAPE_SUPCAST_BLOG (liste des blogs) 	
		$getListeBlog = sprintf("SELECT `GUID_OE`,`PARENT`,`TITRE` FROM `CAPE_SUPCAST_BLOG` WHERE `IDRESSOURCE` = %d AND `IDECRAN` =%d",$IDRESSOURCE,$idecran);
		$getListeBlogQuery = mysql_query($getListeBlog);
		
        // Boucle sur la liste des blogs
		$listeBlog = mysql_fetch_array($getListeBlogQuery);
		if (empty($listeBlog) || is_null($listeBlog)) 
		//
		{
    		$insert = true;
        }
        else
        {
		    $insert = false;
		    $GUID_OE = $listeBlog['GUID_OE']; //GUID_OE identifiant du blog
			$PARENT  = $listeBlog['PARENT']; //parent du blog
			$TITRE   = $listeBlog['TITRE']; //titre du blog
			if (empty($GUID_OE) || is_null($GUID_OE) || $GUID_OE =="" || $GUID_OE =="0"  || $GUID_OE ==0 ) 
			{
    			$insert = true;
			}

        }
        
        // Cache to the session

	if ($insert)
	{


		$montitre = "";

		if (!empty($title) && !is_null($title) && ($title != "")) 
		{
			$montitre = $title;
		}
		if (!empty($titleparent) && !is_null($titleparent) && ($titleparent != ""))
		{
			$montitre = $titleparent . " -> " . $montitre;
		}
		if (!empty($ressourcetitle) && !is_null($ressourcetitle) && ($ressourcetitle != ""))
		{
			$montitre = $ressourcetitle . " -> " . $montitre;
		}
		
		if ($idecranparent == 0)
		{
			$montitre = $title;
		}

	  	$_SESSION['user']->blogtitle = $montitre;
		
		//$_SESSION['user']->blogbody = $body;
      	if (!empty($body)) 
	 	{
		  	$_SESSION['user']->blogbody = $body;
	 	}
	 	else 
	 	{
		  	$_SESSION['user']->blogbody = $title;
		}
		
		$_SESSION['user']->blogtags = $ressourcetitle;
	
	// Convert string of tags into a preformatted array
		$tagarray = string_to_tag_array($ressourcetitle);

	// Make sure the title / description aren't blank
		if (empty($title) || empty($body)) 
		{
			register_error(elgg_echo("blog:blank"));
			forward($_SERVER['HTTP_REFERER']);
		}

	// Initialise a new ElggObject
		$blog = new ElggObject();
	// Tell the system it's a blog post
		$blog->subtype = "blog";
	// Set its owner to the current user
		$blog->owner_guid = get_loggedin_userid();
	// Set it's container
		$blog->container_guid = (int)get_input('container_guid', get_loggedin_userid());
	// For now, set its access
		$blog->access_id = $access;
	// Set its title and description appropriately
		$blog->title = $montitre;
		$blog->description = $body;

	// Now let's add tags. We can pass an array directly to the object property! Easy.
		if (is_array($tagarray)) 
		{
			$blog->tags = $tagarray;
		}
	//whether the user wants to allow comments or not on the blog post
		$blog->comments_on = $comments_on;
		
	// Now save the object
		if (!$blog->save()) 
		{
			register_error(elgg_echo("blog:error"));
			forward($_SERVER['HTTP_REFERER']);
		}

	// Success message
		system_message(elgg_echo("blog:posted"));
	// add to river
		add_to_river('river/object/blog/create', 'create', get_loggedin_userid(), $blog->guid);
	// Remove the blog post cache
		//unset($_SESSION['blogtitle']); unset($_SESSION['blogbody']); unset($_SESSION['blogtags']);
		remove_metadata($_SESSION['user']->guid,'blogtitle');
		remove_metadata($_SESSION['user']->guid,'blogbody');
		remove_metadata($_SESSION['user']->guid,'blogtags');
	
    //insertion de l'identifiant du blog créé pour l'idecran
	$insertBlog=sprintf("INSERT INTO `CAPE_SUPCAST_BLOG` (`IDRESSOURCE`,`OWNER_GUID`, `IDECRAN`, `GUID_OE`, `PARENT`, `TITRE`) VALUES ('%d','%d', '%d', '%d', '%d', '%s');",
	                                                       $IDRESSOURCE,get_loggedin_userid(), $idecran,$blog->guid,$idecranparent,mysql_real_escape_string($montitre));
	mysql_query($insertBlog);	
	}
	else
    {
    //modfication de l'identifiant du blog créé pour l'idecran
	$updateBlog=sprintf("UPDATE `CAPE_SUPCAST_BLOG` set `GUID_OE`=%d Where `IDRESSOURCE` = %d AND `IDECRAN`=%d",$blog->guid,$IDRESSOURCE,$idecran);
	mysql_query($updateBlog);	
	}
	
	$html_fragment ="";
	$html_fragment = $html_fragment . "<?xml version='1.0' encoding='UTF-8'?>";
	$html_fragment = $html_fragment . "<!DOCTYPE plist PUBLIC '-//Apple//DTD PLIST 1.0//EN' 'http://www.apple.com/DTDs/PropertyList-1.0.dtd'>";
	$html_fragment = $html_fragment . "<plist version='1.0'>";
	$html_fragment = $html_fragment . "<array>";
    $html_fragment = $html_fragment . "<dict>";
	$html_fragment = $html_fragment . "<key>status</key>";
	$html_fragment = $html_fragment . "<string>ok</string>";
	$html_fragment = $html_fragment . "<key>blogid</key>";
	$html_fragment = $html_fragment . "<string>";			
	$html_fragment = $html_fragment . $blog->guid;
	$html_fragment = $html_fragment . "</string>";

	$html_fragment = $html_fragment . "</dict>";
	$html_fragment = $html_fragment . "</array>";
	$html_fragment = $html_fragment . "</plist>";
    print ($html_fragment);
	}
	

?>
