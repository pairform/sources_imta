<?php
//error_log('//********************************************//');
//error_log('//*********** recupererMessages.php **********//');
//error_log('//********************************************//');
//Recuperation des valeurs des messages
include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
$os      = $_POST['os'];
$version = $_POST['version'];
//$id = $_POST['id_message'];
//error_log(print_r($_SESSION,true));
switch ($os)
{
    case 'web':
        $array_grains = array();
        $granularite  = ''; //globale, ressource ou page
        //En fonction de ce que l'on envoie au webservice, on va faire une sélection plus fine des messages
        if (isset($_POST['id_ressource']) && isset($_POST['nom_page']))
        {
            $id_ressource    = $_POST['id_ressource'];
            $nom_page        = $_POST['nom_page'];
            $granularite     = 'page';
            $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC,
          `cape_messages`.`id_message` ASC ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        else if (isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
        {
            $id_ressource    = $_POST['id_ressource'];
            $granularite     = 'ressource';
            $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC,
          `cape_messages`.`id_message` ASC ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        else if (!isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
        {
            $granularite     = 'globale';
            $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC,
          `cape_messages`.`id_message` ASC ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        //Pour chaque message
        while ($row_message_sc = mysql_fetch_assoc($result_messages))
        {
            //On chope les variables réutilisées souvent, pour éviter les accès au tableau et pour la lisibilité
            $message_id_ressource  = $row_message_sc['id_ressource'];
            $message_nom_page      = $row_message_sc['nom_page'];
            $message_nom_tag       = $row_message_sc['nom_tag'];
            $message_num_occurence = $row_message_sc['num_occurence'];
            //Options passées 
            $options               = array(
                'guid' => $row_message_sc['id_message'],
                'type' => 'object'
            );
            //Impossible d'utiliser cette fonction sans être connecté
            if (!elgg_get_logged_in_user_guid())
            {
                //Creation d'une session php par Elgg
                $user   = get_user_by_username("sc_bot");
                $result = login($user, true);
            }
            $messages_elgg   = elgg_get_entities($options);
            $message_elgg    = $messages_elgg[0];
            //error_log(print_r($messages_elgg, true));
            /* Retour elgg_get_entities : 
            [attributes:protected] => Array
            (
            [guid] => 73
            [type] => object
            [subtype] => 4
            [owner_guid] => 68
            [site_guid] => 1
            [container_guid] => 68
            [access_id] => 1
            [time_created] => 1363094781
            [time_updated] => 1363094781
            [last_action] => 1363094781
            [enabled] => yes
            [title] => 7/module_droit/PAGE/0
            [description] => <p>Test de message sur la page d'introduction de droits d'auteur</p>
            [tables_split] => 2
            [tables_loaded] => 2
            )
            
            */
            //On store ces valeurs, parce que les crochets passent mal dans la formation de la requête SQL
            $id_message_temp = $message_elgg->guid;
            $id_owner        = $message_elgg->owner_guid;
            $user            = elgg_get_entities(array(
                "type" => "user",
                "guid" => $id_owner
            ));
            //error_log('IDOwner : '. $id_owner .' : '.print_r($user,true));
            $iconURL         = $user[0]->getIconURL("medium");
            // donne l'utilité
            $likes           = elgg_get_annotations(array(
                'guid' => $id_message_temp,
                'annotation_name' => 'likes'
            ));
            $dislikes        = elgg_get_annotations(array(
                'guid' => $id_message_temp,
                'annotation_name' => 'dislikes'
            ));
            $utile           = count($likes) - count($dislikes);
            //On check si l'utilisateur courant à déjà voté
            $current_user_id = elgg_get_logged_in_user_guid();
            //error_log('Current user id : '.$current_user_id);
            $user_a_vote     = 0;
            //Renvoie 1 pour vote positif
            foreach ($likes as $key => $like)
            {
                //error_log("Like guid : " . $like['owner_guid']);
                if ($like['owner_guid'] == $current_user_id)
                    $user_a_vote = 1;
            }
            //Renvoie -1 pour vote négatif
            foreach ($dislikes as $key => $dislike)
            {
                //error_log("dislike guid : " . $dislike['owner_guid']);
                if ($dislike['owner_guid'] == $current_user_id)
                    $user_a_vote = -1;
            }
            //Renvoie 0 pour aucun vote
            $result_messages_enrichis = mysql_query("SELECT `geo_lattitude`, `geo_longitude`, `supprime_par`, `cape_utilisateurs_categorie`.`id_categorie`, `cape_categories`.`nom`
  FROM `cape_messages`,  `cape_utilisateurs_categorie`, `cape_categories` 
  WHERE `cape_messages`.`id_message` = $id_message_temp 
  AND `cape_utilisateurs_categorie`.`id_ressource` = $id_ressource
  AND `cape_utilisateurs_categorie`.`id_utilisateur` = $id_owner
  AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie`");
            if ($result_messages_enrichis)
            {
                $row_message   = mysql_fetch_assoc($result_messages_enrichis);
                //On forme le message
                $array_message = array(
                    "id_message" => $message_elgg->guid,
                    "owner_guid" => $message_elgg->owner_guid,
                    "owner_username" => $user[0]->name,
                    "owner_avatar_medium" => $iconURL,
                    "owner_rank_id" => $row_message['id_categorie'],
                    "owner_rank_name" => $row_message['nom'],
                    "value" => $message_elgg->description,
                    "geo_lattitude" => $row_message['geo_lattitude'],
                    "geo_longitude" => $row_message['geo_longitude'],
                    "supprime_par" => $row_message['supprime_par'],
                    "utilite" => $utile,
                    "user_a_vote" => $user_a_vote,
                    "time_created" => $message_elgg->time_created
                );
            }
            else
                error_log(mysql_error());
            //En fonction de la granularité demandé, on génère un retour plus ou moins profond
            switch ($granularite)
            {
                case 'globale':
                    if (!isset($array_grains[$message_id_ressource]))
                        $array_grains[$message_id_ressource] = array();
                    if (!isset($array_grains[$message_id_ressource][$message_nom_page]))
                        $array_grains[$message_id_ressource][$message_nom_page] = array();
                    if (!isset($array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag]))
                        $array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag] = array();
                    if (!isset($array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence]))
                        $array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence] = array(
                            "id_ressource" => $message_id_ressource,
                            "nom_page" => $message_nom_page,
                            "nom_tag" => $message_nom_tag,
                            "num_occurence" => $message_num_occurence
                        );
                    $array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence]['messages'][$message_elgg->guid] = $array_message;
                    $array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence]['num_messages']                  = count($array_grains[$message_id_ressource][$message_nom_page][$message_nom_tag][$message_num_occurence]['messages']);
                    break;
                case 'ressource':
                    if (!isset($array_grains[$message_nom_page]))
                        $array_grains[$message_nom_page] = array();
                    if (!isset($array_grains[$message_nom_page][$message_nom_tag]))
                        $array_grains[$message_nom_page][$message_nom_tag] = array();
                    if (!isset($array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence]))
                        $array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence] = array(
                            "id_ressource" => $message_id_ressource,
                            "nom_page" => $message_nom_page,
                            "nom_tag" => $message_nom_tag,
                            "num_occurence" => $message_num_occurence
                        );
                    $array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence]['messages'][$message_elgg->guid] = $array_message;
                    $array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence]['num_messages']                  = count($array_grains[$message_nom_page][$message_nom_tag][$message_num_occurence]['messages']);
                    break;
                case 'page':
                    if (!isset($array_grains[$message_nom_tag]))
                        $array_grains[$message_nom_tag] = array();
                    if (!isset($array_grains[$message_nom_tag][$message_num_occurence]))
                        $array_grains[$message_nom_tag][$message_num_occurence] = array(
                            "id_ressource" => $message_id_ressource,
                            "nom_page" => $message_nom_page,
                            "nom_tag" => $message_nom_tag,
                            "num_occurence" => $message_num_occurence
                        );
                    //error_log(print_r($array_message,true));
                    $array_grains[$message_nom_tag][$message_num_occurence]['messages'][$message_elgg->guid] = $array_message;
                    $array_grains[$message_nom_tag][$message_num_occurence]['num_messages']                  = count($array_grains[$message_nom_tag][$message_num_occurence]['messages']);
                    break;
                default:
                    # La réponse D.
                    break;
            }
        }
        //error_log('Array_grains : ' . print_r($array_grains, true));
        $return = json_encode($array_grains);
        //error_log('Retour JSON : ' . $return);
        //Retour JSON
        print($return);
        break;
    case 'ios':
        // N'affiche pas les reponse au niveau 1
        function getMessage($row_message_sc, $affiche_reponse = false)
        {
            //On chope les variables réutilisées souvent, pour éviter les accès au tableau et pour la lisibilité
            $message_id_ressource  = $row_message_sc['id_ressource'];
            $message_nom_page      = $row_message_sc['nom_page'];
            $message_nom_tag       = $row_message_sc['nom_tag'];
            $message_num_occurence = $row_message_sc['num_occurence'];
            //Options passées 
            $options               = array(
                'guid' => $row_message_sc['id_message'],
                'type' => 'object'
            );
            $messages_elgg         = elgg_get_entities($options);
            $message_elgg          = $messages_elgg[0];
            // Recupere l'entity objet 
            $message_entity        = get_entity($row_message_sc['id_message']);
            // Si l'objet est inconnu on arrete.
            if ($messages_elgg == null || $message_entity == null)
                return null;
            // Ne pas afficher si nous n'avons pas l'acces.
            $user = elgg_get_logged_in_user_entity();
            if (!has_access_to_entity($message_entity, $user))
                return null;
            $user_guid = $user->getGUID();
            // si c'est une reponse et qu'on ne doit pas afficher les reponses , on arrete
            $reponses  = $message_entity->getAnnotations("estReponse");
            if (!empty($reponses))
            {
                $estReponse = true;
            }
            else
            {
                $estReponse = false;
            }
            //On store ces valeurs, parce que les crochets passent mal dans la formation de la requête SQL
            $id_message_temp = $message_elgg->guid;
            $id_owner        = $message_elgg->owner_guid;
            $date            = $message_elgg->time_created;
            $time_updated    = $message_elgg->time_updated;
            $user            = elgg_get_entities(array(
                "type" => "user",
                "guid" => $id_owner
            ));
            //error_log('IDOwner : '. $id_owner .' : '.print_r($user,true));
            $iconURL         = $user[0]->getIconURL("medium");
            // donne l'utilité
            $likes           = elgg_get_annotations(array(
                'guid' => $id_message_temp,
                'annotation_name' => 'likes'
            ));
            $dislikes        = elgg_get_annotations(array(
                'guid' => $id_message_temp,
                'annotation_name' => 'dislikes'
            ));
            $utile           = count($likes) - count($dislikes);

            $user_a_vote     = 0;
            //Renvoie 1 pour vote positif
            foreach ($likes as $key => $like)
            {
                //error_log("Like guid : " . $like['owner_guid']);
                if ($like['owner_guid'] == $user_guid)
                    $user_a_vote = 1;
            }
            //Renvoie -1 pour vote négatif
            foreach ($dislikes as $key => $dislike)
            {
                //error_log("dislike guid : " . $dislike['owner_guid']);
                if ($dislike['owner_guid'] == $user_guid)
                    $user_a_vote = -1;
            }
            
            //Tag
            $tags            = $message_entity->getTags();
            if (empty($tags))
                $tags = "";
            // Defi
            $estDefi = $message_entity->getAnnotations("estDefi");
            if ($estDefi)
                $estDefi = true;
            else
                $estDefi = false;
            // Medaille
            $medaille = $message_entity->getAnnotations("medaille");
            if (!$medaille)
                $medaille = null;
            //####################
            //####################
            //####################
            //####################
            //DEBUT MODIF
            // Affichage des reponses.
            $arrayReponses = null;
            $array         = $message_entity->getAnnotations("reponse");
            //print_r($array);
            if (!empty($array))
            {
                foreach ($array as &$value)
                {
                    // Pour toute les reponses on prend l'id.
                    $result = $value->__get("value");
                    if (!empty($result))
                    {
                        $arrayReponses[] = $result;
                    }
                }
            }
            
            $result_messages_enrichis = mysql_query("SELECT `geo_lattitude`, `geo_longitude`, `supprime_par`, `cape_utilisateurs_categorie`.`id_categorie`, `cape_categories`.`nom`
  FROM `cape_messages`,  `cape_utilisateurs_categorie`, `cape_categories` 
  WHERE `cape_messages`.`id_message` = $id_message_temp 
  AND `cape_utilisateurs_categorie`.`id_ressource` = $message_id_ressource
  AND `cape_utilisateurs_categorie`.`id_utilisateur` = $id_owner
  AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie`");
            if ($result_messages_enrichis)
            {

                $row_message_enrichis   = mysql_fetch_assoc($result_messages_enrichis);

                //On forme le message
                $array_message = array(
                    "id_message" => "$id_message_temp",
                    "owner_guid" => $id_owner,
                    "owner_username" => $user[0]->name,
                    "owner_avatar_medium" => $iconURL,
                    "owner_rank_id" => $row_message_enrichis['id_categorie'],
                    "owner_rank_name" => $row_message_enrichis['nom'],
                    "value" => $message_elgg->description,
                    "geo_lattitude" => $row_message_sc['geo_lattitude'],
                    "geo_longitude" => $row_message_sc['geo_longitude'],
                    "supprime_par" => $row_message_sc['supprime_par'],
                    "utilite" => "$utile",
                    "tags" => $message_entity->getTags(),
                    "reponses" => $arrayReponses,
                    "estDefi" => $estDefi,
                    "estReponse" => $estReponse,
                    "medaille" => $medaille,
                    "id_ressource" => $row_message_sc['id_ressource'],
                    "nom_page" => $row_message_sc['nom_page'],
                    "nom_tag" => $row_message_sc['nom_tag'],
                    "num_occurence" => $row_message_sc['num_occurence'],
                    "time_created" => $date,
                    "time_updated" => $time_updated,
                    "user_a_vote" => "$user_a_vote"
                );
            }
            else 
                error_log(msql_error());

            $arrayFinal    = $array_message;
            return $arrayFinal;
        }

        
        $granularite = ''; //globale, ressource ou page

        //En fonction de ce que l'on envoie au webservice, on va faire une sélection plus fine des messages
        if (isset($_POST['id_ressource']) && isset($_POST['nom_page']) && isset($_POST['nom_tag']) && isset($_POST['num_occurence']))
        {
            $id_ressource    = $_POST['id_ressource'];
            $nom_page        = $_POST['nom_page'];
            $granularite     = 'message';
            $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' AND `nom_tag` = '$message_nom_tag' AND `num_occurence` = $message_num_occurence
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        else if (isset($_POST['id_ressource']) && isset($_POST['nom_page']))
        {
            $id_ressource    = $_POST['id_ressource'];
            $nom_page        = $_POST['nom_page'];
            $granularite     = 'page';
            $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        else if (isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
        {
            $id_ressource    = $_POST['id_ressource'];
            $granularite     = 'ressource';
            $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        else if (!isset($_POST['id_ressource']) && !isset($_POST['nom_page']) && !isset($_POST['vue_trans']))
        {
            $granularite     = 'globale';
            $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        else
        {
            $granularite     = 'transversale';
            $result_messages = mysql_query("SELECT  `id_message` , `id_ressource` , `nom_page` , `nom_tag` , `num_occurence` , `geo_lattitude` , `geo_longitude` , `supprime_par`   
          FROM `cape_messages` , `sce_entities`
          WHERE  `cape_messages`.`id_message` = `sce_entities`.`guid`
          ORDER BY `sce_entities`.`time_created` DESC   ");
            if (!$result_messages)
                error_log(mysql_error());
        }
        error_log($granularite);
        //Pour chaque message
        while ($row_message_sc = mysql_fetch_assoc($result_messages))
        {
            $message = getMessage($row_message_sc);
            if ($message)
            {
                $arrayFinal[] = $message;
            }
        }

        //error_log('Array_grains : ' . print_r($array_grains, true));
        $return = json_encode(array('messages' => $arrayFinal));
        //error_log('Retour JSON : ' . $return);
        //Retour JSON
        print($return);
        break;
    case 'and':
        # code...
        break;
    default:
        print('La réponse D');
        break;
}
?>