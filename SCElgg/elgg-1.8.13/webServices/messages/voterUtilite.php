<?php

  //********************************************//
  //************* voterPertinence **************//
  //********************************************//
  /*
   * Paramètres : 
   * up : booléen. True pour vote positif, false pour vote négatif.
   * id_message : id du message à traiter
   *
   * Retour :
   * code_erreur : code d'erreur à afficher (localisation future), false si ça roule. (?)
  */
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

	  $os = $_POST['os'];
  	$version = $_POST['version'];
 switch ($os) {
    case 'web':

    //error_log("GUID = ".elgg_get_logged_in_user_guid());
    if(elgg_get_logged_in_user_guid() == 0)
    {
      print("Erreur d'enregistrement du message.");
      exit;
    }
  	// Valeurs d'entrees 
    if(isset($_POST['up']))
    {
      	$bUp = $_POST['up'] == "true" ? true : false;
        //error_log($bUp);
        //$bUp = true; // TEST 
        $entity_guid = (int)$_POST['id_message'];
        //$entity_guid =  80;//112;// TEST

        $entity = get_entity($entity_guid);

        function getTypes($up, $type = '')
        {
            //Si on est dans le cas d'un ajout,
            if($up)
            {
                $typeLike = 'likes';
                $typeLikeQuote = "likes";
            }
            else
            {
                $typeLike = 'dislikes';
                $typeLikeQuote = "dislikes";
            }

            return ($type == "quote" ? $typeLikeQuote : $typeLike);
        }

        //Fonctions de créations / suppression
        function createLike($up, $entity_guid)
        {
            $annotation = create_annotation($entity_guid,
                getTypes($up),
                getTypes($up, "quote"),
                "",
                elgg_get_logged_in_user_guid(),
                2);
            $entity = get_entity($entity_guid);
            $entity->set("time_updated",time());
            $entity->save();

            //error_log($entity_guid);
        }
        
        function deleteLike($up, $entity_guid)
        {
            $likes = elgg_get_annotations(array(
                'guid' => $entity_guid,
                'annotation_owner_guid' => elgg_get_logged_in_user_guid(),
                'annotation_name' => getTypes($up) ));

            $like = $likes[0];

            if ($like && $like->canEdit())
            {
                $like->delete();
            }
            $entity = get_entity($entity_guid);
            $entity->set("time_updated",time());
            $entity->save();
        }
        // check si l'utilisateur a déjà le même vote
        if (elgg_annotation_exists($entity_guid, getTypes($bUp), elgg_get_logged_in_user_guid()))
        {
            //error_log("Annulation du like");
            deleteLike($bUp, $entity_guid);
        }
        else
        {     //S'il a fait le vote inverse
            if(elgg_annotation_exists($entity_guid, getTypes(!$bUp), elgg_get_logged_in_user_guid()))
            {
                //error_log("Vote inverse");

                deleteLike(!$bUp, $entity_guid);
                createLike($bUp, $entity_guid);
            }
            else
            {
                //error_log("Pas de vote précédent");
                createLike($bUp, $entity_guid);
            }
        }
      }

                //error_log("entity_guid : $entity_guid ,typeLike : ".getTypes(!$bUp).", !bUp : !$bUp, user->guid : $user->guid, annotation : $annotation");
     
      break;
    case 'ios':

    error_log("Post: " . print_r($_POST,true));
        error_log("GUID = ".elgg_get_logged_in_user_guid());
    if(elgg_get_logged_in_user_guid() == 0)
    {
      print("Erreur d'enregistrement du message.");
      exit;
    }
    // Valeurs d'entrees 
    if(isset($_POST['up']))
    {
        // $bUp = ($_POST['up'] == "true") ? true : false;
        $bUp = filter_var($_POST['up'], FILTER_VALIDATE_BOOLEAN);
        error_log("POST[up] binaire :".$bUp);
        //$bUp = true; // TEST 
        $entity_guid = (int)$_POST['id_message'];
        error_log("entity_guid : $entity_guid");
        //$entity_guid =  80;//112;// TEST

        $entity = get_entity($entity_guid);

        function getTypes($up, $type = '')
        {
            //Si on est dans le cas d'un ajout,
            if($up)
            {
                $typeLike = 'likes';
                $typeLikeQuote = "likes";
            }
            else
            {
                $typeLike = 'dislikes';
                $typeLikeQuote = "dislikes";
            }

            return ($type == "quote" ? $typeLikeQuote : $typeLike);
        }

        //Fonctions de créations / suppression
        function createLike($up, $entity_guid)
        {
            $old_guid = elgg_get_logged_in_user_guid();
            $annotation = create_annotation($entity_guid,
                getTypes($up),
                getTypes($up, "quote"),
                "",
                $old_guid,
                2);

            logout();

            //Creation d'une session php par Elgg
            $user   = get_user_by_username("sc_bot");
            $result = login($user, true);

            $entity = get_entity($entity_guid);
            $entity->set("time_updated",time());
            $entity->save();
            
            logout();

            //Creation d'une session php par Elgg
            $user   = get_user($old_guid);
            $result = login($user, true);


            // $entity->set("time_updated_utilite",time());
            // $entity->save();

            // error_log("Entity guid : $entity_guid");
            // error_log("Entity : " . print_r($entity, true));
            // error_log("Entity metadata : " . print_r(elgg_get_metadata(array("metadata_owner_guid" => $entity->getGUID() ))),true);

        }
        
        function deleteLike($up, $entity_guid)
        {
            $old_guid = elgg_get_logged_in_user_guid();

            $likes = elgg_get_annotations(array(
                'guid' => $entity_guid,
                'annotation_owner_guid' => $old_guid,
                'annotation_name' => getTypes($up) ));

            $like = $likes[0];

            if ($like && $like->canEdit())
            {
                $like->delete();
            }

            logout();

            //Creation d'une session php par Elgg
            $user   = get_user_by_username("sc_bot");
            $result = login($user, true);

            $entity = get_entity($entity_guid);
            $entity->set("time_updated",time());
            $entity->save();
            
            logout();

            //Creation d'une session php par Elgg
            $user   = get_user($old_guid);
            $result = login($user, true);

            // $entity->set("time_updated_utilite",time());
            // $entity->save();

            // error_log("Entity guid : $entity_guid");
            // error_log("Entity : " . print_r($entity, true));
            // error_log("Entity metadata : " . print_r(elgg_get_metadata(array("metadata_owner_guid" => $entity->getGUID()))),true);
        }
        // check si l'utilisateur a déjà le même vote
        if (elgg_annotation_exists($entity_guid, getTypes($bUp), elgg_get_logged_in_user_guid()))
        {
            error_log("Annulation du like");
            deleteLike($bUp, $entity_guid);
        }
        else
        {     //S'il a fait le vote inverse
            if(elgg_annotation_exists($entity_guid, getTypes(!$bUp), elgg_get_logged_in_user_guid()))
            {
                error_log("Vote inverse");

                deleteLike(!$bUp, $entity_guid);
                createLike($bUp, $entity_guid);
            }
            else
            {
                error_log("Pas de vote précédent");
                createLike($bUp, $entity_guid);
            }
        }
      }

                error_log("entity_guid : $entity_guid ,typeLike : ".getTypes(!$bUp).", !bUp : !$bUp, user->guid : $user->guid, annotation : $annotation");
      
      break;
    case 'and':
      # code...
      break;
    
    default:
      //print('error : default ');
      break;
  }
  



?>