<?php

  error_log("//********************************************//");
  error_log("//******** EnregistrerMessage.php ************//");
  error_log("//********************************************//");

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $os = $_POST['os'];
  $version = $_POST['version'];

  $id_ressource = $_POST['id_ressource'];
  $nom_page = $_POST['nom_page'];

  $nom_tag  = isset($_POST['nom_tag']) ? $_POST['nom_tag'] : 'PAGE';
  $num_occurence  = isset($_POST['num_occurence']) ? (int)$_POST['num_occurence'] : 0;

  //Recuperation des variables envoyé pour le message
  $message = $_POST['message'];

  $path_message = $id_ressource.'/'.$nom_page.'/'.$nom_tag.'/'.$num_occurence;
  error_log(print_r($_POST,true));
  error_log(elgg_get_logged_in_user_guid());

  if(!elgg_get_logged_in_user_guid())
    die(false);

  //Login
  //Test si le blog existe déjà
  //Sinon, on enregistre le blog dans la base
  //On génère un ID sous forme /id_ressource/nom_de_la_page.html/type_de_tag/occurence_du_tag
  //Ensuite, on enregistre le couple de cet ID et l'id Elgg dans la base 

  switch ($os) {
    case 'web':
        if(isset($_POST['id_message']))
        {
          $id_message = mysql_real_escape_string($_POST['id_message']);
          $options = array('owner_guid' => elgg_get_logged_in_user_guid(), 'type' => 'object', 'guid' => $id_message);  
          $messages_elgg = elgg_get_entities($options);

          $blog = $messages_elgg[0];

          $blog->description = $message;
          // Heure de modification
          $blog->time_updated = date("dd/mm/yy");
          // Now save the object
          if (!$blog->save()) 
          {
            register_error(elgg_echo("blog:error"));
            //forward($_SERVER['HTTP_REFERER']);
          }
          else print(true);
        }
        else
        {
        // Initialise a new ElggObject
          $blog = new ElggObject();
        // Tell the system it's a blog post
          $blog->subtype = "blog";
        // Set its owner to the current user
          $blog->owner_guid = elgg_get_logged_in_user_guid();
          // $blog->owner_guid = "35";
        // For now, set its access
          $blog->access_id = 1;
        // Set its title and description appropriately
          $blog->title = $path_message;

          // $blog->container_guid = "35";

          // $blog->site_guid = 1;
        // Description
          $blog->description = $message;

        // Heure de création
          $blog->time_created = time();
  
        // Now let's add tags. We can pass an array directly to the object property! Easy.
          if (is_array($tagarray)) 
          {
            $blog->tags = $tagarray;
          }
        //whether the user wants to allow comments or not on the blog post
          $blog->comments_on = false;
          
  
          //error_log(print_r($blog, true));
        // Now save the object
          if (!$blog->save()) 
          {
            register_error(elgg_echo("blog:error"));
            //forward($_SERVER['HTTP_REFERER']);
          }
  
          //ID Elgg du blog
          $id_message = $blog->guid;
        // Success message
          system_message(elgg_echo("blog:posted"));
        // add to river
          add_to_river('river/object/blog/create', 'create', elgg_get_logged_in_user_guid(), $blog->guid);
        // Remove the blog post cache
          //unset($_SESSION['blogtitle']); unset($_SESSION['blogbody']); unset($_SESSION['blogtags']);
        
          //modfication de l'identifiant du blog créé pour l'idecran
          
          $result = mysql_query("INSERT INTO `cape_messages` (`id_message`, `id_ressource`,`nom_page`,`nom_tag`,`num_occurence`, `geo_lattitude`,`geo_longitude`,`supprime_par`) 
            VALUES ($id_message, $id_ressource,'$nom_page','$nom_tag',$num_occurence, '','', 0)");
          
          if($result)
            print(true);
          else error_log(mysql_error());
        }
      break;
    case 'ios':
        if(isset($_POST['id_message']))
        {
          $id_message = mysql_real_escape_string($_POST['id_message']);
          $options = array('owner_guid' => elgg_get_logged_in_user_guid(), 'type' => 'object', 'guid' => $id_message);  
          $messages_elgg = elgg_get_entities($options);

          $blog = $messages_elgg[0];

          $blog->description = $message;
          // Heure de modification
          $blog->time_updated = date("dd/mm/yy");
          // Now save the object
          if (!$blog->save()) 
          {
            register_error(elgg_echo("blog:error"));
            //forward($_SERVER['HTTP_REFERER']);
          }
          else print(true);
        }
        else
        {
        // Initialise a new ElggObject
          $blog = new ElggObject();
        // Tell the system it's a blog post
          $blog->subtype = "blog";
        // Set its owner to the current user
          $blog->owner_guid = elgg_get_logged_in_user_guid();
          // $blog->owner_guid = "35";
        // For now, set its access
          $blog->access_id = 1;
        // Set its title and description appropriately
          $blog->title = $path_message;

          // $blog->container_guid = "35";

          // $blog->site_guid = 1;
        // Description
          $blog->description = $message;

        // Heure de création
          $blog->time_created = time();
  
        // Now let's add tags. We can pass an array directly to the object property! Easy.
          if (is_array($tagarray)) 
          {
            $blog->tags = $tagarray;
          }
        //whether the user wants to allow comments or not on the blog post
          $blog->comments_on = false;
          
  
          error_log(print_r($blog, true));
        // Now save the object
          if (!$blog->save()) 
          {
            register_error(elgg_echo("blog:error"));
            //forward($_SERVER['HTTP_REFERER']);
          }
  
          //ID Elgg du blog
          $id_message = $blog->guid;
        // Success message
          system_message(elgg_echo("blog:posted"));
        // add to river
          add_to_river('river/object/blog/create', 'create', elgg_get_logged_in_user_guid(), $blog->guid);
        // Remove the blog post cache
          //unset($_SESSION['blogtitle']); unset($_SESSION['blogbody']); unset($_SESSION['blogtags']);
        
          //modfication de l'identifiant du blog créé pour l'idecran
          
          $result = mysql_query("INSERT INTO `cape_messages` (`id_message`, `id_ressource`,`nom_page`,`nom_tag`,`num_occurence`, `geo_lattitude`,`geo_longitude`,`supprime_par`) 
            VALUES ($id_message, $id_ressource,'$nom_page','$nom_tag',$num_occurence, '','', 0)");
          
          if($result)
            print(json_encode(array('result' => true)));
          else {
            error_log(mysql_error());
            print(json_encode(array('result' => false)));
          }
        }    //Login
      break;
    case 'and':
      # code...
      break;
    
    default:
      print('La réponse D');
      break;
  }

?>