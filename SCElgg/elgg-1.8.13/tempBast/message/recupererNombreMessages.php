<?php

  //********************************************//
  //*********** recupererNombreMessages.php ***********//
  //********************************************//
  
  /* Recuperation du nombre de messages a un niveau particulier.
   *
   * Paramètres :
   * nom_page (string) - nom de la page concernée
   * id_ressource (int) - id de la ressource concernée
   * vue_trans (void) - option pour afficher en vue transversales
   *
   * Utilisation :
   *
   * aucun argument : vue globale
   * id_ressource : vue au niveau de la ressource 
   * id_ressource et nom_page : vue au niveau de la page ( chaque object d'apprentissage )
   * 
   *
   *
   * Retour : 
   * {"nombreDeMessages":
   *  {
   *    "7":12
   *   }
   * }
   * 
   */
  
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");





  $os = $_GET['os'];
  $version = $_POST['version'];
  //Exemple de message lu
  $listeMessagesLus = array(155);
  //$id = $_POST['id_message'];

  




// N'affiche pas les reponse au niveau 1
function getNombreDeMessages($result_messages,$granularite,$listeMessagesLus){



      
      while($row_message_sc = mysql_fetch_assoc($result_messages))
      {
        
        $id_message = $row_message_sc['id_message'];

            // Recupere l'entity objet 
            $message_entity = get_entity($id_message);

        if (  $message_entity == null || in_array($id_message,$listeMessagesLus) ) continue;
        
        /*$user = elgg_get_logged_in_user_entity();
        if(!has_access_to_entity($message_entity,$user))  continue;
        */
        

        $message_id_ressource = $row_message_sc['id_ressource'];
        $message_nom_page = $row_message_sc['nom_page'];
        $message_nom_tag = $row_message_sc['nom_tag'];
        $message_num_occurence = $row_message_sc['num_occurence'];


          
          switch ($granularite) {
          case 'globale':

          if ( !isset($result[$message_id_ressource])) $result[$message_id_ressource] = 0;
          $result[$message_id_ressource]++;
            break;
          case 'ressource':

          $result[$message_nom_page]++;
           break;

          case 'page':
          $result[$message_nom_tag][$message_num_occurence]++;
            break;

          default:
            # La réponse D.
            break;
        }

      }

      return $result;



  }


     

      $granularite = ''; //globale, ressource ou page

      //En fonction de ce que l'on envoie au webservice, on va faire une sélection plus fine des messages
      if(isset($_GET['id_ressource']) && isset($_GET['nom_page']))
      {

        
        $id_ressource = $_GET['id_ressource'];
        $nom_page = $_GET['nom_page'];
        $granularite = 'page';

        $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }

      else if(isset($_GET['id_ressource']) && !isset($_GET['nom_page']))
      {
        $id_ressource = $_GET['id_ressource'];
        $granularite = 'ressource';
        $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_ressource` = $id_ressource 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }


      else if(!isset($_GET['id_ressource']) && !isset($_GET['nom_page']) && !isset($_GET['vue_trans']))
      {        
        $granularite = 'globale';

        $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }

      else 
      {      
        $granularite = 'globale';

          $result_messages = mysql_query("SELECT  `id_message` , `id_ressource` , `nom_page` , `nom_tag` , `num_occurence` , `geo_lattitude` , `geo_longitude` , `supprime_par`   
            FROM `cape_messages` , `sce_entities`
            WHERE  `cape_messages`.`id_message` = `sce_entities`.`guid`
            ORDER BY `sce_entities`.`time_created` DESC   ");

        if(!$result_messages)
          error_log(mysql_error());
      }


      
      $result = getNombreDeMessages($result_messages,$granularite,$listeMessagesLus);
      $return = json_encode(array('nombreDeMessages' => $result));
        
      
      

      //error_log('Array_grains : ' . print_r($array_grains, true));
      

      //error_log('Retour JSON : ' . $return);

      //Retour JSON
      print($return);
  

  //error_log(print_r($_SESSION,true));

  switch ($os) {
    case 'web':


      
      break;
    case 'ios':
      # code...
      break;
    case 'and':
      # code...
      break;
    
    default:
      print('La réponse D');
      break;
  }

?>