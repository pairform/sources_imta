<?php

  //********************************************//
  //************** loginCompte.php *************//
  //********************************************//

  /*
  *
  * Log l'user dans Elgg, créé une session PHP et renvoie un JSON : 
  *
  * "id" : #
  * "username" : $
  * "rank" :{                       //Uniquement pour le web
  *   id_res :{ 
  *     "id_categorie" : #
  *     "nom_categorie" : $
  *   }
  *}
  *
  */
  
  include_once(dirname(dirname(__FILE__)) . "/engine/start.php");

	$os = $_GET['os'];
  $version = $_GET['version'];
  
  $username = $_GET['username'];
  $password = $_GET['password'];
	
  switch ($os) {
    case 'web':

      //error_log($username . " ". $password);

      //Logout préalable
      
      if ( elgg_is_logged_in() ){
        $json = json_encode(array('connexion' => 'ok'));
        print($json);
        return ;
      } 

      logout();
      //Verif du mot de passe
      if (elgg_authenticate($username,$password) === true) 
      {
        //Recuperation du ElggUser
        //Creation d'une session php par Elgg
        $user = get_user_by_username($username);
        $result = login($user, true);
        $json = json_encode(array('connexion' => 'ok'));
      }else
      {
        $json = json_encode(array('connexion' => 'ko'));
      }
      
      print($json);
      //else print("Mot de passe incorrect.");
      //setcookie("guid", $_SESSION['guid']);
      
      //error_log(print_r($_SESSION,true));
      
      /* SESSION : 
      Array
      (
          [__elgg_session] => fd84ed2ece76316a79a04c2684cb6535
          [msg] => Array
          [user] => ElggUser Object
              (
                  [url_override:protected] => 
                  [icon_override:protected] => 
                  [temp_metadata:protected] => Array
                  [temp_annotations:protected] => Array
                  [temp_private_settings:protected] => Array
                  [volatile:protected] => Array
                  [attributes:protected] => Array
                      (
                          [guid] => 68
                          [type] => user
                          [subtype] => 0
                          [owner_guid] => 0
                          [site_guid] => 1
                          [container_guid] => 0
                          [access_id] => 2
                          [time_created] => 1362665229
                          [time_updated] => 1362667385
                          [last_action] => 1363015202
                          [enabled] => yes
                          [tables_split] => 2
                          [tables_loaded] => 2
                          [name] => Azerty
                          [username] => azerty
                          [password] => 0044a5b64d50273027ca9dc31b5382d5
                          [salt] => 2c01ee59
                          [email] => mjuganaikloo@gmail.com
                          [language] => en
                          [code] => e7f633b6cb8ca703abe8cf0c369cb891
                          [banned] => no
                          [admin] => no
                          [prev_last_action] => 1363015152
                          [last_login] => 1362667385
                          [prev_last_login] => 0
                      )

                  [valid:protected] => 
              )
          [guid] => 68
          [id] => 68
          [username] => azerty
          [name] => Azerty
          [code] => 8a537a909c6850b1ca06802601c45ac4
          [last_forward_from] => http://imedia.emn.fr/SCElgg/elgg-1.8.13/admin/dashboard
      )

      */  
      
      session_start();

      $id_user = $_SESSION['guid'];
      $username = $_SESSION['name'];


      //*********************************************************//

      $result = mysql_query("SELECT cape_utilisateurs_categorie.id_ressource, cape_categories.nom, cape_categories.id_categorie 
        FROM cape_utilisateurs_categorie, cape_categories 
        WHERE id_utilisateur='$id_user'
        AND cape_utilisateurs_categorie.id_categorie=cape_categories.id_categorie
        ORDER BY cape_utilisateurs_categorie.id_ressource ASC");

      
      if($result)
      {
        $rank = array();
        while ($row = mysql_fetch_assoc($result)) {
          if($rank[$row['id_ressource']] == "")
            $rank[$row['id_ressource']] = array();

          $rank[$row['id_ressource']] = array('id_categorie' => $row['id_categorie'],'nom_categorie' => $row['nom']);
        }
      }
      else error_log(mysql_error());

      
      //$return = json_encode(array('id' => $id_user, 'username' => $username, 'rank' => $rank));
      
      //error_log('Rank : '.print_r($rank,true));

      
      break;
    case 'ios':
      
      break;
    case 'and':
      # code...
      break;
    
    default:
      print('La réponse D');
      break;
  }

?>