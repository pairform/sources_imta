<?php

//
//  Categories.php 
//  SupCast
//
//  créé par Didier PUTMAN
//  Copyright (c) 2012 EMN - CAPE. All rights reserved.
//
// Fonction : Liste des ressources disponibles dans SUPCAST
//

include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");

$id = $_POST['id'];
//Login
if ($id == 's1ecfz9fex2z1dzsdra78') {
    $html_fragment .= 
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
    <plist version=\"1.0\">
     <array>
      <dict>
       <key>domaines</key>
       <array>";
    
    // tout
    
    $queryCount       = sprintf("SELECT Count(1) FROM `CAPE_RESSOURCES` where  `ressourcevisible` = true");
    $resultQueryCount = mysql_query($queryCount);
    $rowCount         = mysql_fetch_array($resultQueryCount);
    $countTout        = $rowCount[0];
    mysql_free_result($resultQueryCount);
    
    //********************************************
    //Gestion des ressources en développement : récupération de l'ID user
    //********************************************
    
    $username = $_POST['pseudo'];
    $req      = "SELECT guid FROM CAPE_utilisateurs WHERE username = '$username'";
    $res      = mysql_query($req);
    
    $rowId  = mysql_fetch_array($res);
    $idUser = $rowId[0];
    //error_log('IdUser: '.$idUser);
    
    //On capte toutes les ressources en développement
    $queryIDs       = sprintf("SELECT OWNER_GUID FROM `CAPE_RESSOURCES` where  `ressourcevisible` = -1");
    $resultQueryIDs = mysql_query($queryIDs);
    
    //On check si l'user qui fait la requête a le droit d'accéder à chaque ressources grâce à OWNER_GUID
    while ($row = mysql_fetch_array($resultQueryIDs)) {
        $arrayIds = split(' ', $row[0]);
        //error_log('Ids associés à la ressource : '.print_r($arrayIds, true));
        
        foreach ($arrayIds as $value) {
            //Si il a le droit, on incrémente le compteur qui sera affiché sur la ligne dans iOS
            if ($value == $idUser)
                $countTout += 1;
            
            //error_log('$countTout : '.$countTout);
        }
    }
    
    $html_fragment .= "  <dict>
       <key>id</key>
       <string>-1</string>
       <key>domaine</key>
       <string>Tous les thèmes</string>
       <key>NomLong</key>
       <string>Tout</string>
       <key>count</key>
       <integer>" . $countTout . "</integer>
       <key>icone</key>
       <string>http://imedia.emn.fr/SupCast/logoDomaine/tout.png</string>
      </dict>";
    
    
    
    //error_log('––––––––––––––––––––––––––––––––');
    //liste des domaines
    $query       = sprintf("SELECT `id`,`NomCourt`,`NomLong`,`URLIcone` FROM `CAPE_DOMAINES` order by `NomCourt`");
    $resultQuery = mysql_query($query);
    while ($row = mysql_fetch_assoc($resultQuery)) {
        $id       = $row['id'];
        $NomCourt = $row['NomCourt'];
        $NomLong  = $row['NomLong'];
        $URLIcone = $row['URLIcone'];
        
        $queryCount = sprintf("SELECT Count(1) FROM `CAPE_RESSOURCES` where  `ressourcevisible` = true and `ressourcedomaine` = %d", $id);
        
        $resultQueryCount = mysql_query($queryCount);
        $rowCount         = mysql_fetch_array($resultQueryCount);
        $count            = $rowCount[0];
        
        //********************************************
        //On capte toutes les ressources en développement
        //********************************************
        
        $queryIDs       = sprintf("SELECT OWNER_GUID FROM `CAPE_RESSOURCES` where  `ressourcevisible` = -1  and `ressourcedomaine` = $id");
        $resultQueryIDs = mysql_query($queryIDs);
        //error_log('Ressource check');
        
        //On check si l'user qui fait la requête a le droit d'accéder à chaque ressources grâce à OWNER_GUID
        while ($row = mysql_fetch_array($resultQueryIDs)) {
            $arrayIds = split(' ', $row[0]);
            //error_log('Ids associés à la ressource : '.print_r($arrayIds, true));
            
            foreach ($arrayIds as $value) {
                //Si il a le droit, on incrémente le compteur qui sera affiché sur la ligne dans iOS
                if ($value == $idUser)
                    $count += 1;
                
                //error_log('$count : '.$count);
            }
        }
        
        // debut de boucle domaines
        
        $html_fragment .= "  <dict>
            <key>id</key>
            <string>" . $id . "</string>
            <key>domaine</key>
            <string>" . $NomCourt . "</string>
            <key>NomLong</key>
            <string>" . $NomLong . "</string>
            <key>count</key>
            <integer>" . $count . "</integer>
            <key>icone</key>
           <string>" . $URLIcone . "</string>
          </dict>";
        
        // fin de boucle domaines
        
    }
    
    mysql_free_result($resultQuery);
    
    
    $html_fragment .= "
     </array>
      </dict>";
    
    //****************************************************************************************
    //**********************************Etablissement*****************************************
    //****************************************************************************************
    
    $html_fragment .= "
      <dict>
       <key>etablissements</key>
       <array>";
    
    
    // tout
    
    $html_fragment .= "
      <dict>
       <key>id</key>
       <string>-1</string>
       <key>etablissement</key>
       <string>Tous les établissements</string>
       <key>NomLong</key>
       <string>Tout</string>
       <key>count</key>
       <integer>" . $countTout . "</integer>
       <key>icone</key>
       <string>http://imedia.emn.fr/SupCast/logoDomaine/tout.png</string>
      </dict>";
    
    
    
    //On détermine le contexte (écran) du message: requête au base de données
    $query       = sprintf("SELECT `id`,`NomCourt`,`NomLong`,`URLIcone` FROM `CAPE_ETABLISSEMENTS` order by `NomCourt`");
    $resultQuery = mysql_query($query);
    while ($row = mysql_fetch_assoc($resultQuery)) {
        $id       = $row['id'];
        $NomCourt = $row['NomCourt'];
        $NomLong  = $row['NomLong'];
        $URLIcone = $row['URLIcone'];
        
        $queryCount = sprintf("SELECT Count(1) FROM `CAPE_RESSOURCES` where `ressourcevisible` = true and `ressourceetablissement` = %d", $id);
        
        $resultQueryCount = mysql_query($queryCount);
        $rowCount         = mysql_fetch_array($resultQueryCount);
        $count            = $rowCount[0];
        mysql_free_result($resultQueryCount);
        
        //********************************************
        //On capte toutes les ressources en développement
        //********************************************
        
        $queryIDs       = sprintf("SELECT OWNER_GUID FROM `CAPE_RESSOURCES` where  `ressourcevisible` = -1  and `ressourceetablissement` = $id");
        $resultQueryIDs = mysql_query($queryIDs);
        //error_log('Ressource check');
        
        //On check si l'user qui fait la requête a le droit d'accéder à chaque ressources grâce à OWNER_GUID
        while ($row = mysql_fetch_array($resultQueryIDs)) {
            $arrayIds = split(' ', $row[0]);
            //error_log('Ids associés à la ressource : '.print_r($arrayIds, true));
            
            foreach ($arrayIds as $value) {
                //Si il a le droit, on incrémente le compteur qui sera affiché sur la ligne dans iOS
                if ($value == $idUser)
                    $count += 1;
                
                //error_log('$count : '.$count);
            }
        }
        
        // debut de boucle etablissements
        
        $html_fragment .= "
          <dict>
           <key>id</key>
           <string>" . $id . "</string>
           <key>etablissement</key>
           <string>" . $NomCourt . "</string>
           <key>NomLong</key>
           <string>" . $NomLong . "</string>
           <key>count</key>
           <integer>" . $count . "</integer>
           <key>icone</key>
           <string>" . $URLIcone . "</string>
          </dict>";
        
        // fin de boucle etablissements
        
    }
    
    mysql_free_result($resultQuery);
    
    $html_fragment .= "
      </array>
     </dict>
     </array>
    </plist>";
    
    print($html_fragment);
    
}


if ($id == 'asef1836tvsoegbvt9q1zdfrs') {
    $html_fragment .= 
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
    <plist version=\"1.0\">
    	<array>
    		<dict>
    			<key>domaines</key>
    				<array>";
    
    
    //liste de dommaines
    $query       = sprintf("SELECT `id`,`NomCourt`,`NomLong`,`URLIcone` FROM `CAPE_DOMAINES` order by `NomCourt`");
    $resultQuery = mysql_query($query);
    while ($row = mysql_fetch_assoc($resultQuery)) {
        $id       = $row['id'];
        $NomCourt = $row['NomCourt'];
        $NomLong  = $row['NomLong'];
        $URLIcone = $row['URLIcone'];
        
        
        // debut de boucle domaines
        
        $html_fragment .= "			<dict>";
        $html_fragment .= "				<key>id</key>";
        $html_fragment .= "				<string>" . $id . "</string>";
        $html_fragment .= "				<key>domaine</key>";
        $html_fragment .= "				<string>" . $NomCourt . "</string>";
        $html_fragment .= "				<key>NomLong</key>";
        $html_fragment .= "				<string>" . $NomLong . "</string>";
        $html_fragment .= "				<key>icone</key>";
        $html_fragment .= "				<string>" . $URLIcone . "</string>";
        $html_fragment .= "			</dict>";
        
        // fin de boucle domaines
        
    }
    
    mysql_free_result($resultQuery);
    
    
    $html_fragment .= "		</array>";
    $html_fragment .= "	</dict>";
    
    
    
    $html_fragment .= "	<dict>";
    $html_fragment .= "		<key>etablissements</key>";
    $html_fragment .= "		<array>";
    
    
    //On détermine le contexte (écran) du message: requête au base de données
    $query       = sprintf("SELECT `id`,`NomCourt`,`NomLong`,`URLIcone` FROM `CAPE_ETABLISSEMENTS` order by `NomCourt` ");
    $resultQuery = mysql_query($query);
    while ($row = mysql_fetch_assoc($resultQuery)) {
        $id       = $row['id'];
        $NomCourt = $row['NomCourt'];
        $NomLong  = $row['NomLong'];
        $URLIcone = $row['URLIcone'];
        
        
        
        // debut de boucle etablissements
        
        $html_fragment .= "			<dict>";
        $html_fragment .= "				<key>id</key>";
        $html_fragment .= "				<string>" . $id . "</string>";
        $html_fragment .= "				<key>etablissement</key>";
        $html_fragment .= "				<string>" . $NomCourt . "</string>";
        $html_fragment .= "				<key>NomLong</key>";
        $html_fragment .= "				<string>" . $NomLong . "</string>";
        $html_fragment .= "				<key>icone</key>";
        $html_fragment .= "				<string>" . $URLIcone . "</string>";
        $html_fragment .= "			</dict>";
        
        // fin de boucle etablissements
        
    }
    
    mysql_free_result($resultQuery);
    
    $html_fragment .= "		</array>";
    $html_fragment .= "	</dict>";
    $html_fragment .= "</array>";
    $html_fragment .= "</plist>";
    print($html_fragment);
    
}

?>
