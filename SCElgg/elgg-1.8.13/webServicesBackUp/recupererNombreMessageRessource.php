<?php

	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");

	//Récupérer l'id de l'user
	$pseudo = $_POST['pseudo'];
	$password = $_POST['password'];
	
	//On récupère la liste des ids
    $idsTransitional = $_POST['idsressources'];
    //error_log($idsTransitional);
    
	$idsRessources = explode(",",$idsTransitional);
	//error_log(print_r($idsRessources,true));
	
	array_pop($idsRessources);
	
	//Login
	if ($user = authenticate($pseudo,$password)) 
	{
         $result = login($user);
         
		//On récupère l'objet user
		$user = get_loggedin_user();
	}
	

	if ($result)
	{
	$html_fragment = "<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE plist PUBLIC '-//Apple//DTD PLIST 1.0//EN' 'http://www.apple.com/DTDs/PropertyList-1.0.dtd'>
<plist version='1.0'>
<array>";
		
		foreach($idsRessources as $idRes)
		{
			$nombreNouveauxMessages = 0;       // init compteur de nouveau message 
			$arrayBlogs = mysql_query("SELECT GUID_OE FROM CAPE_SUPCAST_BLOG WHERE IDRESSOURCE='$idRes'");
			
			// Boucle sur la liste des blogs
			while($blog = mysql_fetch_array($arrayBlogs))
			{
				$idBlog = $blog[0]; //identifiant du blog
		
				//Récupération du nombre de nouveaux messages.
				$arrayNombre = mysql_query("SELECT count(id) FROM {$CONFIG->dbprefix}annotations WHERE id > (IFNULL((SELECT ID_MSG FROM CAPE_USER_OE WHERE GUID_OE = '$idBlog' AND GUID_USER = '$user->guid'),0)) AND entity_guid = '$idBlog' AND owner_guid <> '$user->guid'");
				
				$row = mysql_fetch_array($arrayNombre);
				$nombreNouveauxMessages += (int) $row[0];
			}
				
   		$html_fragment .= "
  <dict>
    <key>". $idRes . "</key>
    <string>". $nombreNouveauxMessages . "</string>
  </dict>";
	
		}

		$html_fragment .= "
</array>
</plist>";
	
		//error_log("Retour php : ".$html_fragment);
		
		logout();
		print($html_fragment);
    }
?>