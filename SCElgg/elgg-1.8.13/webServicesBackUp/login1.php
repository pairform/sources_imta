<?php
	include_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");
	//Récupérer les données
	$pseudo = $_POST['pseudo'];
	$password = $_POST['password'];
	$html_fragment =  "<?xml version='1.0' encoding='UTF-8'?>
	<!DOCTYPE plist PUBLIC '-//Apple//DTD PLIST 1.0//EN' 'http://www.apple.com/DTDs/PropertyList-1.0.dtd'>
	<plist version='1.0'>
	<array>
		<dict>
			<key>nom</key>
			<string>" . $pseudo . "</string>";
	/*
	$html_fragment .=  "		<key>niveau</key>";
	
	
	if ($pseudo == "bono" || $pseudo == "admincape" || $pseudo == "adminCape")
	{
		$html_fragment .=  "		<string>gardien du temple</string>";
	}
	else
	{
		$html_fragment .=  "		<string>test</string>";
	}
	
	*/
	
	//Login
	if ($user = authenticate($pseudo,$password)) 
	{
		$result = login($user);
	}
	
	//Gestion des niveaux
	
	
	//Paramètres définis par défaut
	$scoreMessagePertinent = 1;
	$scoreMessageNonPertinent = -1;
	$scoreMessageSupprime = -2;
	$seuilNoviceApprenti = 8;
	$seuilApprentiMaitre = 30;
	$ancienNiveau = "";
	
	//On récupère le niveau de l'utilisateur lors de sa dernière connexion
	$pseudoMinuscules = strtolower($pseudo);
	$query = sprintf("SELECT * FROM CAPE_utilisateurs WHERE username='%s'", mysql_real_escape_string($pseudoMinuscules));
	$resultQuery = mysql_query($query);
	
	while ($row = mysql_fetch_assoc($resultQuery)) {
		$ancienNiveau = $row['categorie'];
		$guid = $row['guid'];
	}
	
	$html_fragment .= "
			<key>guid</key>
			<string>".$guid."</string>";
	
	mysql_free_result($resultQuery);
	
	//On récupère le nombre total de messages créés par l'utilisateur
	$GUID = $user->guid;
	
	$query = sprintf("SELECT COUNT(owner_guid) FROM {$CONFIG->dbprefix}annotations WHERE owner_guid='%d'", $GUID);
	$resultQuery = mysql_query($query);
	$nombreTotalMessagesCreesParUtilisateur = mysql_fetch_array($resultQuery);
	
	$scoreTotal = 0;
	
	//Pour chaque message créé par l'utilisateur, on calcule son score (vérifier s'il est pertinent ou pas / s'il a été supprimé)
	$annotations = get_annotations(0, "", "", "", "", $GUID, $nombreTotalMessagesCreesParUtilisateur[0], 0, "desc");
	
	$html_fragment .=  "
			<key>niveau</key>";
	for ($i=0 ; $i < $nombreTotalMessagesCreesParUtilisateur[0] ; $i++) {
		
		$annotation = $annotations[$i]; 
		$idMessage = $annotation->id;
		
		$difference = 0;
		$scorePertinents = 0;
			
		//Est-il pertinent : calcul de la différence OUI - NON dans la table "CAPE_votesPertinence"
		$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_votesPertinence WHERE idMessage=%d AND estPertinent='OUI'", $idMessage);
		$resultQuery = mysql_query($query);
		$votesPositifs = mysql_fetch_array($resultQuery);
		
		$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_votesPertinence WHERE idMessage=%d AND estPertinent='NON'", $idMessage);
		$resultQuery = mysql_query($query);
		$votesNegatifs = mysql_fetch_array($resultQuery);
		
		$difference = $votesPositifs[0] - $votesNegatifs[0];
		
		if($difference == 0) {
			$scorePertinents = 0;
		}
		
		if($difference > 0) {
			$scorePertinents = $scoreMessagePertinent;
		}
		
		if ($difference < 0) {
			$scorePertinents = $scoreMessageNonPertinent;
		}
		
		$scoreTotal = $scoreTotal + $scorePertinents;
	
		//Est-ce que ce message a été supprimé ?
		$query = sprintf("SELECT COUNT(idMessage) FROM CAPE_messagesSupprimes WHERE idMessage='%d'", $idMessage);
		$resultQuery = mysql_query($query);
		$messageSupprime = mysql_fetch_array($resultQuery);	
		
		$scoreTotal = $scoreTotal + ($scoreMessageSupprime * $messageSupprime[0]);
	
	}
	
	//$html_fragment = $html_fragment . $scoreTotal;
	
	if(($ancienNiveau != "sage") && ($ancienNiveau != "gardien du temple"))
	{
		
		//On détermine le nouveau niveau
		if ($scoreTotal <= ($seuilNoviceApprenti - 2)) {
		
			$nouveauNiveau = "novice";
		
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='novice' WHERE username='%s'", mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "1er IF");
			
		}
		
		if ((($seuilNoviceApprenti - 2) < $scoreTotal) && ($scoreTotal <= ($seuilNoviceApprenti + 2))) {
		
			if ($ancienNiveau == "maître") {
				$nouveauNiveau = "apprenti";
			}
			
			if ($ancienNiveau == "apprenti") {
				$nouveauNiveau = "apprenti";
			}
			
			if ($ancienNiveau == "novice") {
				$nouveauNiveau = "novice";
			}
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='%s' WHERE username='%s'", mysql_real_escape_string($nouveauNiveau), mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "2e IF");
			
		}			
		
		if ((($seuilNoviceApprenti + 2) < $scoreTotal) && ($scoreTotal <= ($seuilApprentiMaitre - 2))) {
			
			$nouveauNiveau = "apprenti";
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='apprenti' WHERE username='%s'", mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "3e IF");
						
		}
		
		if ((($seuilApprentiMaitre - 2) < $scoreTotal) && ($scoreTotal <= ($seuilApprentiMaitre + 2))) {
		
			if ($ancienNiveau == "maître") {
				$nouveauNiveau = "maître";
			}
			
			if ($ancienNiveau == "apprenti") {
				$nouveauNiveau = "apprenti";
			}
			
			if ($ancienNiveau == "novice") {
				$nouveauNiveau = "apprenti";
			}
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='%s' WHERE username='%s'", mysql_real_escape_string($nouveauNiveau), mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "4e IF");
			
		}		
		
		if (($seuilApprentiMaitre + 2) < $scoreTotal) {
			
			$nouveauNiveau = "maître";
			
			$query = sprintf("UPDATE CAPE_utilisateurs SET categorie='maître' WHERE username='%s'", mysql_real_escape_string($pseudo));
			$resultQuery = mysql_query($query);
			
			//$html_fragment = $html_fragment . "5e IF");
			
		}
		
		
		$html_fragment .=  "		<string>".$nouveauNiveau."</string>";
		
		
	
	}
	else
	{
		$html_fragment .=  "		<string>".$ancienNiveau."</string>";
	}
	/* Sortie
	if ($result) {
	
		if (($ancienNiveau == "sage") || ($ancienNiveau == "gardien du temple")) {
	
			$html_fragment = $html_fragment . '<message>OK - ' . $ancienNiveau . '</message>';
			logout();
		
		}
		
		else {
		
			$html_fragment = $html_fragment . '<message>' . $ancienNiveau . ' - ' . $nouveauNiveau . '</message>';
			logout();
		
		}
			
	}
		
	else {
	
		$html_fragment = $html_fragment . '<message>Erreur</message>';
		
	}
	*/
	if ($result)
	{
		$GUID = $user->guid;
		//On vérifie la presence de l'utilisateur dans la table CAPE_USER_SUPCAST
		$query = sprintf("SELECT `guid` FROM `elgg_iphone`.`CAPE_USER_SUPCAST` WHERE `guid`=%d",$GUID);
		$resultQuery = mysql_query($query);
		$row = mysql_fetch_assoc($resultQuery);
		mysql_free_result($resultQuery);
		if (!($row))
		{	
			//On insere l'utilisateur dans la table CAPE_USER_SUPCAST
			$queryInsert = sprintf("INSERT INTO `elgg_iphone`.`CAPE_USER_SUPCAST` (`guid`) VALUES ('%d');",$GUID);	
			$resultQueryInsert = mysql_query($queryInsert);
		}
		//Sortie
		$html_fragment .=  "		<key>message</key>";
		$html_fragment .=  "		<string>OK</string>";
		logout();		
	}
	else
	{
		$html_fragment .=  "		<key>message</key>";
		$html_fragment .=  "		<string>Erreur</string>";	
	}
	$html_fragment .=  "	</dict>";
	$html_fragment .=  "</array>";
	$html_fragment .=  "</plist>";
	error_log("************************************************************************************");
	error_log("Connection à SupCast de l'utilisateur : ".$pseudo);
	error_log("************************************************************************************");
    print ($html_fragment);
?>
