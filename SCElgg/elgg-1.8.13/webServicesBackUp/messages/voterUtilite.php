<?php

  //********************************************//
  //************* voterPertinence **************//
  //********************************************//
  /*
   * Paramètres : 
   * up : booléen. True pour vote positif, false pour vote négatif.
   * id_message : id du message à traiter
   *
   * Retour :
   * code_erreur : code d'erreur à afficher (localisation future), false si ça roule. (?)
  */
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  
	  $os = $_POST['os'];
  	$version = $_POST['version'];
 switch ($os) {
    case 'web':

    //error_log("GUID = ".elgg_get_logged_in_user_guid());
    if(elgg_get_logged_in_user_guid() == 0)
    {
      print("Erreur d'enregistrement du message.");
      exit;
    }
  	// Valeurs d'entrees 
    if(isset($_POST['up']))
    {
      	$bUp = $_POST['up'] == "true" ? true : false;
        //error_log($bUp);
        //$bUp = true; // TEST 
        $entity_guid = (int)$_POST['id_message'];
        //$entity_guid =  80;//112;// TEST

        $entity = get_entity($entity_guid);

        function getTypes($up, $type = '')
        {
            //Si on est dans le cas d'un ajout,
            if($up)
            {
                $typeLike = 'likes';
                $typeLikeQuote = "likes";
            }
            else
            {
                $typeLike = 'dislikes';
                $typeLikeQuote = "dislikes";
            }

            return ($type == "quote" ? $typeLikeQuote : $typeLike);
        }

        //Fonctions de créations / suppression
        function createLike($up, $entity_guid)
        {
            $annotation = create_annotation($entity_guid,
                getTypes($up),
                getTypes($up, "quote"),
                "",
                elgg_get_logged_in_user_guid(),
                2);

            //error_log($entity_guid);
        }
        
        function deleteLike($up, $entity_guid)
        {
            $likes = elgg_get_annotations(array(
                'guid' => $entity_guid,
                'annotation_owner_guid' => elgg_get_logged_in_user_guid(),
                'annotation_name' => getTypes($up) ));

            $like = $likes[0];

            if ($like && $like->canEdit())
            {
                $like->delete();
            }
        }
        // check si l'utilisateur a déjà le même vote
        if (elgg_annotation_exists($entity_guid, getTypes($bUp), elgg_get_logged_in_user_guid()))
        {
            //error_log("Annulation du like");
            deleteLike($bUp, $entity_guid);
        }
        else
        {     //S'il a fait le vote inverse
            if(elgg_annotation_exists($entity_guid, getTypes(!$bUp), elgg_get_logged_in_user_guid()))
            {
                //error_log("Vote inverse");

                deleteLike(!$bUp, $entity_guid);
                createLike($bUp, $entity_guid);
            }
            else
            {
                //error_log("Pas de vote précédent");
                createLike($bUp, $entity_guid);
            }
        }
      }

                //error_log("entity_guid : $entity_guid ,typeLike : ".getTypes(!$bUp).", !bUp : !$bUp, user->guid : $user->guid, annotation : $annotation");
     
      break;
    case 'ios':
      
      break;
    case 'and':
      # code...
      break;
    
    default:
      //print('error : default ');
      break;
  }
  



?>