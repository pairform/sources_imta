//
//  main.m
//  SupCastProducer
//
//  Created by CAPE - EMN on 23/11/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//


#import <Cocoa/Cocoa.h>
/*
int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **) argv);
}
 */

int main(int argc, char *argv[]) {
    int retVal;
    
    //    redirectNSLogToDocumentFolder();
    
    @try {
        retVal = NSApplicationMain(argc, (const char **) argv);
    }
    @catch (NSException *exception) {
        DLog(@"Exception interceptée %@: %@", 
             [exception name],
             [exception reason]);        
    }
    @finally {
    }
    
    return retVal;
}

