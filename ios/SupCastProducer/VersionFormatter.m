//
//  VersionFormatter.m
//  SupCastProducer
//
//  Created by Cape EMN on 04/01/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "VersionFormatter.h"

NSString * versionRegEx = @"(?:[0-9])+(?:[0-9.])+(?:[0-9.])+(?:[0-9.])+(?:[0-9.])+(?:[0-9.])+(?:[0-9.])";

@implementation VersionFormatter


-(id)init
{
    DLog(@"");
    self = [super init];
    if (self)
    {
    }
    
    return self;
}

-(NSString*)stringForObjectValue:(id)obj
{
    DLog(@"");
    if ([obj isKindOfClass:[NSString class]])
    {
        
        
        NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", versionRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:obj];
        if (myStringMatchesRegEx)
        {
            return obj;
        }
        
        
    }
    return nil;
}

-(BOOL)getObjectValue:(id *)obj forString:(NSString *)string errorDescription:(NSString **)error
{
    DLog(@"");
    DLog(@"string: %@",string);
    DLog(@"obj: %@",*obj);
    
    if (string)
    {   
        NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", versionRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
        if (myStringMatchesRegEx)
        {
            *obj = string;
            return YES;
        }
        
    }
    return NO;
}

-(void)dealloc
{
    DLog(@"");
    [super dealloc];
}
-(BOOL)isPartialStringValid:(NSString *)partialString 
           newEditingString:(NSString **)newString 
           errorDescription:(NSString **)error
{
    DLog(@"");
    BOOL myPartialStringMatchesRegEx = NO;
    BOOL myNewStringMatchesRegEx = NO;
  
    DLog(@"partialString: %@",partialString);
    DLog(@"newString: %@",*newString);

    
    if (partialString)
    {   
        NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", versionRegEx];
        myPartialStringMatchesRegEx = [regExPredicate evaluateWithObject:partialString];
        
    }

    if (*newString)
    {   
        NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", versionRegEx];
        myNewStringMatchesRegEx = [regExPredicate evaluateWithObject:*newString];
        
    }
    
    return myPartialStringMatchesRegEx && myNewStringMatchesRegEx;
    
}

-(BOOL)isPartialStringValid:(NSString **)partialStringPtr
      proposedSelectedRange:(NSRangePointer)proposedSelRangePtr 
             originalString:(NSString *)origString 
      originalSelectedRange:(NSRange)origSelRange
           errorDescription:(NSString **)error
{
    DLog(@"");
    
    
    BOOL myPartialStringMatchesRegEx = NO;
    BOOL myNewStringMatchesRegEx = NO;
    
    NSString * match;
    if ([*partialStringPtr length] == 0)
    {
        return YES;
    }
 /*   NSString * chaine = *partialStringPtr;
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", versionRegEx];
    myPartialStringMatchesRegEx = [regExPredicate evaluateWithObject:chaine];
        
    if (!myPartialStringMatchesRegEx)
    {
        return NO;
    }*/
        
    if (origSelRange.location == proposedSelRangePtr->location)
    {
        return YES;
    }
    
    return YES;
}

@end
