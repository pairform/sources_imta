//
//  MyDocument.m
//  SupCastProducer
//
//  Created by CAPE - EMN on 23/11/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "TraceNewMessage.h"
#import "ListeNewMessages.h"

#import "MyDocument.h"
#import "CreationFichierMedia.h"
#import "GestionFichier.h"
//#import "XMLToObjectParser.h"
#import "HTMLToObjectParser.h"

@implementation MyDocument
@synthesize textFieldFichierPubli;
@synthesize textFieldPathCible;
@synthesize textFieldURLdeDepot;
@synthesize textFieldDossierRessource;
@synthesize textFieldFichierIcone;
@synthesize textFieldTheme;
@synthesize textFieldEtablissement;
@synthesize textFieldNomRessource;
//@synthesize textViewDescription;
@synthesize textFieldTitreRessource;
@synthesize textFieldCatalogue;
@synthesize textFieldDossierWeb;
@synthesize textFieldVersion;
@synthesize textFieldVersion2;
@synthesize textFieldVersion3;
@synthesize textFieldId;
@synthesize path;
@synthesize dicoContenuFichier;
@synthesize listeThemes;
@synthesize listeEtablissements;
@synthesize imageIconeView;
@synthesize textViewDescription;
@synthesize receivedData;
@synthesize textViewCustomCSS;
@synthesize textFieldVisibiliteDev, comboVisibilite, checkBoxUpdate;


#pragma mark -
#pragma mark Initialisation

- (id)init
{
    DLog(@"");
    self = [super init];
    if (self) {
        
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
        
    }
    return self;
}
- (NSString *)windowNibName
{
    DLog(@"");
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"MyDocument";
}



-(void)awakeFromNib
{
    DLog(@"");
    
}

//Restitution du formulaire
- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    DLog(@"");
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeCombo:)
                                                 name:NSComboBoxSelectionDidChangeNotification
                                               object:nil];
    
    
    
    [self recupereListeCategorie];
    
    if (self.dicoContenuFichier)
    {   
        if(!self.dicoContenuFichier)
        { 
            DLog(@"Error: contenu fichier"); 
            
        }
        
        if (self.dicoContenuFichier)
        {   
            for (id key in self.dicoContenuFichier) 
            {
                if ([key isEqualToString:kDossierWeb])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldDossierWeb)
                            self.textFieldDossierWeb.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kFichierPubli])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldFichierPubli)
                            self.textFieldFichierPubli.stringValue = [self.dicoContenuFichier objectForKey:key];
                
                if ([key isEqualToString:kId])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if(self.textFieldId)
                            self.textFieldId.stringValue = [self.dicoContenuFichier objectForKey:key];
                
                
                if ([key isEqualToString:kVersion])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if(self.textFieldVersion)
                            self.textFieldVersion.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kVersion2])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if(self.textFieldVersion2)
                            self.textFieldVersion2.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kVersion3])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if(self.textFieldVersion3)
                            self.textFieldVersion3.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kPathCible])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if(self.textFieldPathCible)
                            self.textFieldPathCible.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kURLdeDepot])
                    if ([self.dicoContenuFichier objectForKey:key])        
                        if (self.textFieldURLdeDepot)        
                            self.textFieldURLdeDepot.stringValue = [self.dicoContenuFichier objectForKey:key];        
                if ([key isEqualToString:kDossierRessource])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldDossierRessource)
                            self.textFieldDossierRessource.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kFichierIcone])
                {   
                    if ([self.dicoContenuFichier objectForKey:key])
                    {   
                        if (self.textFieldFichierIcone)
                        {   
                            self.textFieldFichierIcone.stringValue = [self.dicoContenuFichier objectForKey:key];
                            if ([[NSFileManager defaultManager] fileExistsAtPath:self.textFieldFichierIcone.stringValue])
                            {
                                self.imageIconeView.image = [[NSImage alloc] initWithContentsOfFile:self.textFieldFichierIcone.stringValue];                
                            }
                            
                        }
                    }
                }
                if ([key isEqualToString:kTheme])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldTheme)
                            self.textFieldTheme.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kEtablissement])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldEtablissement)
                            self.textFieldEtablissement.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kNomRessource])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldNomRessource.stringValue)
                            self.textFieldNomRessource.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kDescription])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textViewDescription)
                            self.textViewDescription.string = [self.dicoContenuFichier objectForKey:key];         
                if ([key isEqualToString:kCustomCss])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textViewCustomCSS)
                            self.textViewCustomCSS.string = [self.dicoContenuFichier objectForKey:key];            
                if ([key isEqualToString:kTitreRessource])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldTitreRessource)
                            self.textFieldTitreRessource.stringValue = [self.dicoContenuFichier objectForKey:key];
                if ([key isEqualToString:kCatalogue])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldCatalogue)
                            self.textFieldCatalogue.stringValue = [self.dicoContenuFichier objectForKey:key];
                //DONE:Restitution des composants du form
                
                if ([key isEqualToString:kVisibilite])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.comboVisibilite)
                        {
                            [comboVisibilite selectItemWithObjectValue:[self.dicoContenuFichier objectForKey:key]];
                            [self comboBoxSelectionDidChange:nil];
                        }
                
                if ([key isEqualToString:kVisibiliteDev])
                    if ([self.dicoContenuFichier objectForKey:key])
                        if (self.textFieldVisibiliteDev)
                            self.textFieldVisibiliteDev.stringValue = [self.dicoContenuFichier objectForKey:key];
                 
            }
        }
        
        
    } // end readFromData
    
}

/*
-(void)recupereRessource
{
    NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kRessourcesURL];
    NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *paramDataString = [NSString stringWithFormat:@"id=%@",kKey];
    
    NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:paramData];
    
    connectionRessources = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(connectionRessources) {
        
        [indicator startAnimation:nil];
        [indicator setHidden:NO];
        
        NSMutableData *data = [[NSMutableData alloc] init];
        self.receivedData = data;
        [data release];
        
    }
    else {
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert release];
        
    }
    
    [urlConnexion release];
    [url release];
    [request release];
    
    
}*/

-(void)recupereListeCategorie
{
    DLog(@"");
    NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kCategoriesURL];
    NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *paramDataString = [NSString stringWithFormat:@"id=%@",kKey];
    
    NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:paramData];
    
    connectionListeCategorie = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(connectionListeCategorie) {
        
        [indicator startAnimation:nil];
        [indicator setHidden:NO];
        
        NSMutableData *data = [[NSMutableData alloc] init];
        self.receivedData = data;
        
    }
    else {
        
        
    }
    
    
    
}

#pragma mark - Connection delegate

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	if (connection == connectionListeCategorie)
    {
        [receivedData setLength:0];        
    }
    if(connection == connectionFinaleBDD)
    {
        [reponseAjoutRessource setLength:0];
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
    if (connection == connectionListeCategorie)
    {
        [receivedData appendData:data];
    }
    
    if(connection == connectionFinaleBDD)
    {
        [reponseAjoutRessource appendData:data];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DLog(@"");
	
	[indicator setHidden:YES];	
	[indicator stopAnimation:nil];	
	self.receivedData = nil;
	
	
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
    
    DLog(@"");
    if (connection == connectionRessources)
    {
    }
    else if (connection == connectionListeCategorie)
    {
        [indicator setHidden:YES];
        [indicator stopAnimation:nil];
        
        NSString *error; 
        NSPropertyListFormat format;
        
        // Initialisation des listes Themes et Etablissements
        self.listeThemes = nil;
        self.listeEtablissements = nil;
        NSArray * liste = [[NSMutableArray alloc] init];
        self.listeThemes = [[NSMutableDictionary alloc] init];
        self.listeEtablissements = [[NSMutableDictionary alloc] init];
        // serialisation du fichier Themes.plist en dictionnaire    
        liste = [NSPropertyListSerialization propertyListFromData:self.receivedData
                                                 mutabilityOption:NSPropertyListImmutable
                                                           format:&format
                                                 errorDescription:&error];
        
        
        // enumeration de la liste des Themes    
        for (NSDictionary * theme in [[liste objectAtIndex:0] objectForKey:kDomaines]) 
        {
            [listeThemes setValue:[theme objectForKey:@"id"] forKey:[theme objectForKey:@"domaine"]];
            [comboTheme addItemWithObjectValue:[theme objectForKey:@"domaine"]];
            //On ajoute l'ID en tant que tag pour l'item, pour éviter de revenir le chercher lors
            //de la requête SQL
            //[[comboTheme itemObjectValueAtIndex:([comboTheme numberOfItems] -1)] setTag:[[theme objectForKey:@"id"] intValue]];
        }
        // enumeration de la liste des Etablissements    
        for (NSDictionary * etablissement in [[liste objectAtIndex:1] objectForKey:kEtablissements]) 
        {
            [listeEtablissements setValue:[etablissement objectForKey:@"id"] forKey:[etablissement objectForKey:@"etablissement"]];
            [comboEtablissement addItemWithObjectValue:[etablissement objectForKey:@"etablissement"]];
            //On ajoute l'ID en tant que tag pour l'item, pour éviter de revenir le chercher lors
            //de la requête SQL
            //[[comboEtablissement itemObjectValueAtIndex:([comboEtablissement numberOfItems] -1)] setTag:[[etablissement objectForKey:@"id"] intValue]];
        }
        
        [comboTheme selectItemWithObjectValue:self.textFieldTheme.stringValue];
        [comboEtablissement selectItemWithObjectValue:self.textFieldEtablissement.stringValue];
        
        [comboTheme reloadData];
        [comboEtablissement reloadData];
        
    }
    
    if(connection == connectionFinaleBDD)
    {
        if(!reponseAjoutRessource)
        {
            NSAlert * alert =[[NSAlert alloc] init];
            [alert setMessageText:@"Erreur d'envoi de requete SQL (batard)"];
            [alert runModal];
            
        }
    }
    
}


#pragma mark -
#pragma mark Gestion des champs

- (BOOL)textShouldEndEditing:(NSText *)textObject
{
    DLog(@"");
    return YES;
}

- (void)controlTextDidBeginEditing:(NSNotification *)obj
{
    DLog(@"");
}

- (void)controlTextDidChange:(NSNotification *)obj
{
    DLog(@"");
    
}

- (void)controlTextDidEndEditing:(NSNotification *)obj
{
    DLog(@"");
    if ([obj object] == self.textFieldCatalogue)
    {
        [self controlCatalogue];
    }
    else if ([obj object] == self.textFieldDossierRessource)
    {
        [self controlDossierRessource];
    }
    else if ([obj object] == self.textFieldDossierWeb)
    {
        [self controlDossierWeb];
    }
    else if ([obj object] == self.textFieldEtablissement)
    {
        [self controlEtablissement];
    }
    else if ([obj object] == self.textFieldFichierIcone)
    {
        [self controlFichierIcone];
    }
    else if ([obj object] == self.textFieldFichierPubli)
    {
        [self controlFichierPubli];
    }
    else if ([obj object] == self.textFieldId)
    {
        [self controlId];
    }
    else if ([obj object] == self.textFieldNomRessource)
    {
        [self controlNomRessource];
    }
    else if ([obj object] == self.textFieldPathCible)
    {
        [self controlPathCible];
    }
    else if ([obj object] == self.textFieldTheme)
    {
        [self controlTheme];
    }
    else if ([obj object] == self.textFieldTitreRessource)
    {
        [self controlTitreRessource];
    }
    else if ([obj object] == self.textFieldURLdeDepot)
    {
        [self controlURLdeDepot];
    }
    else if ([obj object] == self.textFieldVersion)
    {
        [self controlVersion];
    }
    else if ([obj object] == self.textFieldVersion2)
    {
        [self controlVersion2];
    }
    else if ([obj object] == self.textFieldVersion3)
    {
        [self controlVersion3];
    }
    else if ([obj object] == self.textFieldVisibiliteDev)
    {
        [self controlVisibiliteDev];
    }
    else
    {
        DLog(@"else");
    }
    
}


-(BOOL)controlDossierWeb
{
    DLog(@"");
    NSFileManager * fileManager = [NSFileManager defaultManager];
    BOOL top = YES;
    BOOL isDir = NO;
    BOOL exists = NO;
    
    //************************************
    // verificer si le champ est renseigne
    if (([self.textFieldDossierWeb.stringValue isEqualToString:@""])|| (self.textFieldDossierWeb.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le dossier Web n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldDossierWeb becomeFirstResponder];
    }
    else
    {
        // verificer si la valeur saisie est valide
        exists = [fileManager fileExistsAtPath:self.textFieldDossierWeb.stringValue isDirectory:&isDir];
        if (exists)
        {
            if (!isDir)
            {
                // path est un fichier
                top = NO;
                NSAlert *alert = [[NSAlert alloc] init];
                [alert setMessageText:@"La valeur saisie ne correspond pas à dossier."];
                [alert setInformativeText:@"Veuillez saisir un nom de  dossier valide."];
                [alert runModal];
                [self.textFieldDossierWeb becomeFirstResponder];
            }    
        }
        else
        {
            // path n'existe pas
            top = NO;
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Le dossier n'existe pas."];
            [alert setInformativeText:@"Veuillez saisir un nom de dossier valide."];
            [alert runModal];
            [self.textFieldDossierWeb becomeFirstResponder];
        }            
    }
    
    return top;
}


-(BOOL)controlFichierPubli
{
    DLog(@"");
    NSFileManager * fileManager = [NSFileManager defaultManager];
    BOOL top = YES;
    BOOL isDir = NO;
    BOOL exists = NO;    //************************************
    // verificer si le champ est renseigne
    if (([self.textFieldFichierPubli.stringValue isEqualToString:@""])|| (self.textFieldFichierPubli.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le fichier publi n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldFichierPubli becomeFirstResponder];
    }
    else
    {
        // verificer si la valeur saisie est valide
        exists = [fileManager fileExistsAtPath:self.textFieldFichierPubli.stringValue isDirectory:&isDir];
        if (exists)
        {
            if (isDir)
            {
                // path est un dossier
                top = NO;
                NSAlert *alert = [[NSAlert alloc] init];
                [alert setMessageText:@"La valeur saisie ne correspond pas à fichier."];
                [alert setInformativeText:@"Veuillez saisir un nom de fichier valide."];
                [alert runModal];
                [self.textFieldFichierPubli becomeFirstResponder];
            }
        }
        else
        {
            // path n'existe pas
            top = NO;
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Le fichier n'existe pas."];
            [alert setInformativeText:@"Veuillez saisir un nom de fichier valide."];
            [alert runModal];
            [self.textFieldFichierPubli becomeFirstResponder];
        }            
    }
    
    
    return top;
}

-(BOOL)controlPathCible
{
    DLog(@"");
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    BOOL top = YES;
    BOOL isDir = NO;
    BOOL exists = NO;   
    //************************************
    
    //************************************
    // verificer si le champ est renseigne
    
    if (([self.textFieldPathCible.stringValue isEqualToString:@""])|| (self.textFieldPathCible.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le dossier Path cible n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldPathCible becomeFirstResponder];
    }
    else
    {
        // verificer si la valeur saisie est valide
        exists = [fileManager fileExistsAtPath:self.textFieldPathCible.stringValue isDirectory:&isDir];
        if (exists)
        {
            if (!isDir)
            {
                // path est un fichier
                top = NO;
                NSAlert *alert = [[NSAlert alloc] init];
                [alert setMessageText:@"La valeur saisie pour le Path cible ne correspond pas à dossier."];
                [alert setInformativeText:@"Veuillez saisir un dossier Path cible valide."];
                [alert runModal];
                [self.textFieldPathCible becomeFirstResponder];
            }    
        }
        else
        {
            // path n'existe pas
            top = NO;
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Le dossier Path cible n'existe pas."];
            [alert setInformativeText:@"Veuillez saisir un nom de dossier valide."];
            [alert runModal];
            [self.textFieldPathCible becomeFirstResponder];
        }            
    }
    
    return top;
}

-(BOOL)controlVersion
{
    
    DLog(@"");
    BOOL top = YES;
    //************************************
    // verificer si le champ est renseigne
    
    if (([self.textFieldVersion.stringValue isEqualToString:@""])|| (self.textFieldVersion.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le numéro de version 1 n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldVersion becomeFirstResponder];
    }
    
    return top;
}

-(BOOL)controlVersion2
{
    DLog(@"");
    
    BOOL top = YES;
    //************************************
    // verificer si le champ est renseigne
    
    if (([self.textFieldVersion2.stringValue isEqualToString:@""])|| (self.textFieldVersion2.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le numéro de version 2 n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldVersion2 becomeFirstResponder];
    }
    return top;
}

-(BOOL)controlVersion3
{
    
    DLog(@"");
    BOOL top = YES;
    //************************************
    // verificer si le champ est renseigne
    
    if (([self.textFieldVersion3.stringValue isEqualToString:@""])|| (self.textFieldVersion3.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le numéro de version 3 n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldVersion3 becomeFirstResponder];
    }
    
    return top;
}
-(BOOL)controlId
{
    DLog(@"");
    
    BOOL top = YES;
    //************************************
    // verificer si le champ est renseigne
    
    if (([self.textFieldId.stringValue isEqualToString:@""])|| (self.textFieldId.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"L'identifiant n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldId becomeFirstResponder];
    }
    return top;
}


-(BOOL)controlURLdeDepot
{
    DLog(@"");
    BOOL top = YES;
    
    if (([self.textFieldURLdeDepot.stringValue isEqualToString:@""])|| (self.textFieldURLdeDepot.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"L'URL de dépôt de la ressource n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldURLdeDepot becomeFirstResponder];
    }
    else
    {
        // verificer si la valeur saisie est valide
        NSURL * test = [[NSURL alloc] initWithString: self.textFieldURLdeDepot.stringValue];
        if (!test) 
        {
            top = NO;
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"La valeur de l'URL de dépôt n'est pas valide."];
            [alert setInformativeText:@"Veuillez saisir une valeur."];
            [alert runModal];
            [self.textFieldURLdeDepot becomeFirstResponder];
        }
    }
    return top;
    
}


-(BOOL)controlTitreRessource
{
    DLog(@"");
    BOOL top = YES;
    
    if (([self.textFieldTitreRessource.stringValue isEqualToString:@""])|| (self.textFieldTitreRessource.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le titre de la ressource n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldTitreRessource becomeFirstResponder];
    }
    return top;
}

-(BOOL)controlCatalogue
{
    DLog(@"");
    BOOL top = YES;
    if (([self.textFieldCatalogue.stringValue isEqualToString:@""])|| (self.textFieldCatalogue.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le catalogue n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldCatalogue becomeFirstResponder];
    }
    else
    {
        // verificer si la valeur saisie est valide
    }
    
    return top;
}

-(BOOL)controlDossierRessource
{
    DLog(@"");
    BOOL top = YES;
    
    if (([self.textFieldDossierRessource.stringValue isEqualToString:@""])|| (self.textFieldDossierRessource.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le fichier ressource n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldDossierRessource becomeFirstResponder];
    }
    else
    {
        // verificer si la valeur saisie est valide
    }
    return top;
}

-(BOOL)controlFichierIcone
{
    DLog(@"");
    NSFileManager * fileManager = [NSFileManager defaultManager];
    BOOL top = YES;
    BOOL isDir = NO;
    BOOL exists = NO;   
    
    if (([self.textFieldFichierIcone.stringValue isEqualToString:@""])|| (self.textFieldFichierIcone.stringValue == nil))
    {
        //[self.textFieldFichierIcone becomeFirstResponder];
        [[self windowForSheet] makeFirstResponder:self.textFieldFichierIcone]; 
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le fichier icone n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
    }
    else
    {
        // verificer si la valeur saisie est valide
        exists = [fileManager fileExistsAtPath:self.textFieldFichierIcone.stringValue isDirectory:&isDir];
        if (exists)
        {
            if (isDir)
            {
                // path est un dossier
                //[self.textFieldFichierIcone becomeFirstResponder];
                [[self windowForSheet] makeFirstResponder:self.textFieldFichierIcone]; 
                top = NO;
                NSAlert *alert = [[NSAlert alloc] init];
                [alert setMessageText:@"La valeur pour le fichier icone saisie ne correspond pas à fichier."];
                [alert setInformativeText:@"Veuillez saisir un nom de fichier icone valide."];
                [alert runModal];
            }
            else
            {
                
                self.imageIconeView.image = [[NSImage alloc] initWithContentsOfFile:self.textFieldFichierIcone.stringValue];                
                
            }
            
        }
        else
        {
            // path n'existe pas
            //[self.textFieldFichierIcone becomeFirstResponder];
            if (self.textFieldFichierIcone.window)
                [self.textFieldFichierIcone.window makeFirstResponder:self.textFieldFichierIcone]; 
            
            top = NO;
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Le fichier icone n'existe pas."];
            [alert setInformativeText:@"Veuillez saisir un fichier icone valide."];
            [alert runModal];
        }            
    }
    
    return top;
    
}

-(BOOL)controlTheme
{
    DLog(@"");
    BOOL top = YES;
    if (([self.textFieldTheme.stringValue isEqualToString:@""])|| (self.textFieldTheme.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le thème n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldTheme becomeFirstResponder];
    }
    else
    {
        // verificer si la valeur saisie est valide
    }
    return top;
}

-(BOOL)controlEtablissement
{
    DLog(@"");
    BOOL top = YES;
    
    if (([self.textFieldEtablissement.stringValue isEqualToString:@""])|| (self.textFieldEtablissement.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"L'établissement n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldEtablissement becomeFirstResponder];
    }
    return top;
}
-(BOOL)controlNomRessource
{
    DLog(@"");
    BOOL top = YES;
    if (([self.textFieldNomRessource.stringValue isEqualToString:@""])|| (self.textFieldNomRessource.stringValue == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Le nom de la ressource n'est pas renseigné."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textFieldNomRessource becomeFirstResponder];
    }
    return top;
}

-(BOOL)controlViewDescription
{
    DLog(@"");
    BOOL top = YES;
    if (([self.textViewDescription.string isEqualToString:@""])|| (self.textViewDescription == nil))
    {
        top = NO;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"La description n'est pas renseignée."];
        [alert setInformativeText:@"Veuillez saisir une valeur."];
        [alert runModal];
        [self.textViewDescription becomeFirstResponder];
    }
    return top;
}

-(void)comboBoxSelectionDidChange:(NSNotification *)notification
{
    //FIXED:La checkbox est correctement initialisée, mais on connaît pas la valeur de l'index
    //sélectionné. Du coup, ressourcevisibilite n'a pas de valeur et c'est la merde
    
    switch(self.comboVisibilite.indexOfSelectedItem)
    {
            case 0:
            ressourceVisibilite = kRessourceVisibleNon;
            [textFieldVisibiliteDev setEnabled:NO];
            break;
            case 1:
            ressourceVisibilite = kRessourceVisibleOui;
            [textFieldVisibiliteDev setEnabled:NO];
            break;
            case 2:
            ressourceVisibilite = kRessourceVisibleDev;
            [textFieldVisibiliteDev setEnabled:YES];
            break;
            default:
            break;
    }
}

-(BOOL)controlVisibiliteDev
{
    DLog(@"");
    BOOL top = YES;
    if(self.comboVisibilite.indexOfSelectedItem == 2)
        if (([textFieldVisibiliteDev.stringValue isEqualToString:@""])|| (textFieldVisibiliteDev == nil))
        {
            top = NO;
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Les ids des utilisateurs n'est pas renseignée."];
            [alert setInformativeText:@"Veuillez saisir une valeur."];
            [alert runModal];
            
            [self.textFieldVisibiliteDev becomeFirstResponder];
        }
    return top;
}

#pragma mark -
#pragma mark Combo delegation

- (void)changeCombo:(NSNotification *)notification
{
    
    //TODO:Virer ce textField qui se cache en dessous des combos, et faire ça propre.
    
    DLog(@"");
    if ([notification object] == comboTheme)
    {
        self.textFieldTheme.stringValue = [comboTheme objectValueOfSelectedItem];
    }
    if ([notification object] == comboEtablissement)
    {
        self.textFieldEtablissement.stringValue = [comboEtablissement objectValueOfSelectedItem];
    } 
}
#pragma mark -
#pragma mark Gestion des sauvegardes de formulaire

- (NSData *)dataOfType:(NSString *)pTypeName error:(NSError **)pOutError
{
    DLog(@"");
	NSDictionary * zDict;
    NSString *errorString;
    NSFileManager *fileManager;
    NSString *applicationSupportFolder = nil;
    fileManager = [NSFileManager defaultManager];
    applicationSupportFolder = [self applicationSupportFolder];
    
    if ([pTypeName compare:@"public.plain-text"] == NSOrderedSame ) {
		zDict = [NSDictionary dictionaryWithObjectsAndKeys:NSPlainTextDocumentType,NSDocumentTypeDocumentAttribute,nil];		
	} else {
		DLog(@"ERROR: dataOfType pTypeName=%@",pTypeName);
		*pOutError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:NULL];
		return NULL;
	} // end if
    
    //DONE:Enregistrer les valeurs du formulaire
    //DONE:Créer des constantes pour les deux champs “visibilité" et "visibilitéDev"
    
    //Enregistrement des valeurs du formulaire dans un pList
    NSData * data = [NSPropertyListSerialization dataFromPropertyList:
                     [[NSArray alloc] initWithObjects:
                      [[NSDictionary alloc] initWithObjectsAndKeys:
                       [self.textFieldFichierPubli stringValue],kFichierPubli,
                       [self.textFieldId stringValue],kId,
                       [self.textFieldVersion stringValue],kVersion,
                       [self.textFieldVersion2 stringValue],kVersion2,
                       [self.textFieldVersion3 stringValue],kVersion3,
                       [self.textFieldPathCible stringValue],kPathCible,
                       [self.textFieldTitreRessource stringValue],kTitreRessource,
                       [self.textFieldCatalogue stringValue],kCatalogue,
                       [self.textFieldURLdeDepot stringValue],kURLdeDepot,
                       [self.textFieldDossierRessource stringValue],kDossierRessource,
                       [self.textFieldFichierIcone stringValue],kFichierIcone,
                       [self.textFieldTheme stringValue],kTheme,
                       [self.textFieldEtablissement stringValue],kEtablissement,
                       [self.textFieldDossierWeb stringValue],kDossierWeb,
                       [self.textFieldNomRessource stringValue],kNomRessource,
                       self.textViewDescription.string,kDescription,
                       self.textViewCustomCSS.string,kCustomCss,
                       self.comboVisibilite.stringValue, kVisibilite,
                       self.textFieldVisibiliteDev.stringValue, kVisibiliteDev,
                       nil],nil] 
                                                               format:NSPropertyListXMLFormat_v1_0 
                                                     errorDescription:&errorString];
    
    return data;
}

- (BOOL)readFromData:(NSData *)pData ofType:(NSString *)pTypeName error:(NSError **)pOutError {
	
    
    DLog(@"");
    if ([pTypeName compare:@"public.plain-text"] != NSOrderedSame) {
		DLog(@"** ERROR ** readFromData pTypeName=%@",pTypeName);		
		*pOutError = [NSError errorWithDomain:NSOSStatusErrorDomain 
										 code:unimpErr 
									 userInfo:NULL];
		return NO;
	} // end if
    
    
	NSDictionary *zDict = [NSDictionary dictionaryWithObjectsAndKeys:NSPlainTextDocumentType,NSDocumentTypeDocumentAttribute, nil];	
	NSDictionary *zDictDocAttributes;
	NSError *zError = nil;
	NSAttributedString * zNSAttributedStringObj = 
	[[NSAttributedString alloc]initWithData:pData 
									options:zDict 
						 documentAttributes:&zDictDocAttributes 
									  error:&zError];
    if ( zError != NULL ) {
		DLog(@"Error initialising NSAttributedString: %@",[zError localizedDescription]);
		return NO;
	} // end if
	
	DLog(@"%@",[zNSAttributedStringObj string]);
    
    // traite self.receivedData
    NSString *error; 
    NSPropertyListFormat format;
    
    NSArray * arrayContenuFichier = [[NSArray alloc] initWithArray:[NSPropertyListSerialization propertyListFromData:pData                                                                                                    mutabilityOption:NSPropertyListImmutable                                                                                                              format:&format errorDescription:&error]];
    if (arrayContenuFichier)
    {    
        self.dicoContenuFichier = [[NSDictionary alloc] initWithDictionary:[arrayContenuFichier objectAtIndex:0]];
        
    }
    
    [[self undoManager] removeAllActions];
    
    return YES;
}

- (NSString *)applicationSupportFolder {
    DLog(@"");
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] :NSTemporaryDirectory();
    return [basePath stringByAppendingPathComponent:@"SupCastProducer"];
}

#pragma mark -
#pragma mark Actions utilisateurs

- (IBAction)connection:(id)sender {
    
    DLog(@"");
    
    //Envoi de la requête
    [self envoyerRequeteServeur:100];
    
    
    //Ouverture du tiroir
    //[self toggleTiroirLog];
    /*
     
     //On vérifie que tous les champs du formulaire soient remplis
     NSTextField *utilisateur = [[NSTextField alloc] initWithFrame:NSMakeRect(0,0,200,15)];
     NSTextFieldCell *utilisateurCell = [[NSTextFieldCell alloc] init];
     [utilisateurCell setPlaceholderString:@"login"];
     NSTextField *motdepasse = [[NSTextField alloc] initWithFrame:NSMakeRect(0,20,200,15)];
     NSTextFieldCell *motdepasseCell = [[NSTextFieldCell alloc] initWithFrame:NSMakeRect(0,0,200,15)];
     [motdepasseCell setPlaceholderString:@"mot de passe"];
     NSBox *  accessory = [[NSBox alloc]  initWithFrame:NSMakeRect(0,0,210,45)];
     [accessory addSubview:utilisateur];
     [accessory addSubview:motdepasse];
     
     NSAlert *alert = [[NSAlert alloc] init];
     [alert setMessageText:@"Veuillez saisir vos identifiants"];
     [alert setInformativeText:@""];
     [alert setAccessoryView:accessory];
     [alert runModal];
     [alert release];*/
    
}

//Lancement de la génération de la chaîne supcastisée
- (IBAction)lancer:(id)sender {
    DLog(@"");
       
    //Init du fileManager
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSString *applicationSupportFolder = nil;
    //Bool de contrôle de remplissage de champs
    BOOL top = YES;
    
    
    //Chemin absolu du dossier de l'application
    applicationSupportFolder = [self applicationSupportFolder];
    
    
    //Gros check des champs du form
    top = [self controlDossierWeb] && [self controlFichierPubli] && [self controlPathCible] && [self controlVersion] && [self controlVersion2] && [self controlVersion3] && [self controlId] && 
    [self controlURLdeDepot] && [self controlTitreRessource] && [self controlCatalogue] && [self controlDossierRessource] && [self controlFichierIcone] && [self controlTheme] && 
    [self controlEtablissement] && [self controlNomRessource] && [self controlViewDescription] && [self controlVisibiliteDev];
    
    
    //Si tous les champs sont remplis
    if (top)
    {
        
        //On initialise et affiche le tiroir du log
        [self initLog];
        [self ouvrirTiroirLog];
        
        [self addToLog:@"Debut de la génération..." :false];
        
        //S'il n'y a pas de dossier d'application (?)
        if (![fileManager fileExistsAtPath:applicationSupportFolder isDirectory:NULL]) 
        {
            [fileManager createDirectoryAtPath:applicationSupportFolder withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        [self addToLog:@"On enregistre le XML du form, qui contient toutes les données rentrées" :false];
        //On enregistre le XML du form, qui contient toutes les données rentrées
        [self saveDocument:(self)];
        
        //Useless, puisqu'on ne fait rien après la notif.
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(chargeHTML:)
                                                     name:kAppDataDownloadCompleted
                                                   object:nil];
                
        //Récupération de l'éventuel code CSS proposé
        CreationFichierMedia * fichierMedia = [[CreationFichierMedia alloc] initWithCss:[textViewCustomCSS string]];
        
        [self addToLog:@"On récupère tous les médias et on les cale tous dans WebPath." :false];
        //On récupère tous les médias et on les cale tous dans WebPath.

        NSDictionary * monDicoDeBase =[[NSDictionary alloc] initWithObjectsAndKeys:
                                       self.textFieldTitreRessource.stringValue,kTitreRessource,
                                       self.textFieldCatalogue.stringValue,kCatalogue,
                                       self.textFieldFichierPubli.stringValue,kFichierPubli,
                                       self.textFieldVersion.stringValue,kVersion,
                                       self.textFieldPathCible.stringValue,kPathCible,
                                       self.textFieldURLdeDepot.stringValue,kURLdeDepot,
                                       self.textFieldDossierRessource.stringValue,kDossierRessource,
                                       self.textFieldId.stringValue,kId,
                                       self.textFieldVersion.stringValue,kVersion,
                                       self.textFieldVersion2.stringValue,kVersion2,
                                       self.textFieldVersion3.stringValue,kVersion3,
                                       self.textFieldFichierIcone.stringValue,kFichierIcone,
                                       self.textFieldTheme.stringValue,kTheme,
                                       self.textFieldEtablissement.stringValue,kEtablissement,
                                       self.textFieldNomRessource.stringValue,kNomRessource,                                                   
                                       self.textFieldDossierWeb.stringValue,kDossierWeb,
                                       self.textViewDescription.string,kDescription,nil];
        [fichierMedia listeMediaWithDico:monDicoDeBase];
        
        
        //NSMutableArray * rootArray = [[NSMutableArray alloc] init]; 
        
        HTMLToObjectParser * objectHTML = [[HTMLToObjectParser alloc] initWithDico:monDicoDeBase];
        
        //[objectHTML eclate:[textFieldDossierWeb.stringValue  stringByAppendingPathComponent:@"index.html"]];
        
        //////////
        
        //Récupération de tous les liens externes de la page Index. En gros, tous les liens "menu" 
        NSArray * listeHTML = [objectHTML eclate:[textFieldDossierWeb.stringValue stringByAppendingPathComponent:@"index.html"]];
 
        //Dossier (absolu) dans lequel on va stocker tous ces liens
        NSString * pathCible =[[[self.textFieldPathCible stringValue] stringByAppendingPathComponent:[self.textFieldDossierRessource stringValue]] stringByAppendingPathComponent:@"menu"];
        
        //////////
        
        //Initialisation des compteurs de fichiers et d'erreurs
        arrayListe = [[NSMutableArray alloc] init];
        nombreDeFichiers = [listeHTML count];
        compteurDeFichiers = 0;
        compteurErreurs = 0;
        
        //On essaie d'ouvrir tous les liens du menu, pour checker leur validité.
        //Ça va triggerer du webView:didFinishLoadForFrame si success, du webView:didFailLooselyAndWeakly si ça échoue
        //dans lesquels on va faire le décompte des erreurs et succès
        [self addToLog:@"Transformation des fichiers :" :true];

        for (NSString * fichier in listeHTML) 
        {
            [self addToLog:fichier :false];
            WebView * aWebView =  [[WebView alloc] init];
            [aWebView setFrameLoadDelegate:self];
            NSMutableString * pathFichier =  [[NSMutableString alloc] initWithString:[pathCible stringByAppendingPathComponent:fichier]];
            [pathFichier replaceOccurrencesOfString:@" " withString:@"\%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0,[pathFichier length])];
            NSURL * monURL = [[NSURL alloc] initWithString:pathFichier];
            [[aWebView mainFrame] loadRequest:[NSURLRequest requestWithURL:monURL]];   
        }	
        
        
    }
}


- (IBAction)saveDocument:(id)sender
{
    DLog(@"");
    [super saveDocument:(self)];
}
#pragma mark - Affichage du log et gestion
-(void)ouvrirTiroirLog
{
    [logDrawer openOnEdge:NSMinYEdge];
}
//Ouvre le tiroir du log en bas de la fenêtre
-(void)toggleTiroirLog
{
    //NSMinYEdge : Constante du bas de la fenetre parente
    if([logDrawer state] == NSDrawerOpenState || [logDrawer state] == NSDrawerOpeningState)
        [logDrawer close];
    else [logDrawer open];
}

//TODO:Gestion du log
//Initialise le tableau de log : le vide 
-(void)initLog
{
    logArray = [[NSMutableArray alloc] initWithArray:[[NSArray alloc] init]]; 
    [logTableView reloadData];
    chrono = [NSDate date];   
}

//On ajoute task dans un dico + l'heure (mm:ss) et on le cale dans le log
//Booléen error sert à colorer la case en rouge dans le cas d'une erreur
-(void)addToLog:(NSString*)task:(BOOL)error
{
    NSTimeInterval timeSinceBegin = [[NSDate date] timeIntervalSince1970] - [chrono timeIntervalSince1970];
    NSDate * date = [[NSDate alloc] initWithTimeIntervalSince1970:timeSinceBegin];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterMediumStyle];
    [formatter setDateFormat:@"mm:ss"];
    
    NSString * time = [formatter stringFromDate:date];
    
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:task,@"task",time,@"time", [NSNumber numberWithBool:error], @"error", nil];
    
    //On ajoute le dictionnaire au tableau de log
    [logArray addObject:dict];    
    [self performSelectorInBackground:@selector(updateLog) withObject:nil];

     
}
//On récupère le dernier log et on l'update
-(void)updateLog
{
    [logTableView reloadData];    
    //Remise en focus du TV
    [logTableView resignFirstResponder];
}
#pragma mark - TableViewDataSource Protocol implementation
-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    NSInteger count = 0;
    if(logArray)
        count = [logArray count];
    return count;
}
-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    //On récupère l'identifier de la colonne et on l'envoie
    return [[logArray objectAtIndex:row] objectForKey:[tableColumn identifier]];
}
#pragma mark - TableView Delegate

-(void)tableView:(NSTableView *)tableView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    if([[[logArray objectAtIndex:row] objectForKey:@"error"] boolValue] == true)
        [cell setBackgroundColor:[NSColor colorWithSRGBRed:154 green:2 blue:28 alpha:0.6]];
    else
        [cell setBackgroundColor:[NSColor colorWithSRGBRed:0 green:0 blue:0 alpha:0.0]];
}
#pragma mark -
#pragma mark Recherche chemin fichier
-(void)recherche:(DOMNode *)noeud fichier:(NSString *)fichier
{
    
    //Si on est en présence d'un élément DOM de type UL, on incrémente la profondeur
    //du tableau (et vu la recursivité, a chaque passage ici, on est dans un nouveau UL)
    indice++;
    
    //Liste des nodes enfants au UL
    DOMNodeList * nodeListUL = [noeud childNodes];       
    
    //Pour chaque node enfant du UL
    for (int m = 0;m<[nodeListUL length];m++) 
    {
        DOMNode * nodeLI = [nodeListUL item:m];
        
        //Si on est dans la liste, ce qui est normalement le cas on itère dedans pour chercher un <a>, et récupérer le texte
        if ([nodeLI.nodeName isEqualToString:@"LI"])
        {
            //DLog(@"<li>");
            DOMNodeList * nodeListLI = [nodeLI childNodes];
            BOOL estLienDuMenuSelectionne = YES;
            NSMutableString * href = [[NSMutableString alloc] initWithString:@""];
            //Contenu du node (= texte de la ligne)
            NSMutableString * texteDuLien = [[NSMutableString alloc] initWithString:@""]; 
            
            //Iteration a travers les childnodes de <LI>
            do 
            {
                DOMNode * nodeChildLI = [nodeListLI item:0];
                
                //Si le il y a une balise <a>, ça veut dire que le lien du menu est cliquable, donc c'est pas le lien de menu actif
                if ([nodeChildLI.nodeName isEqualToString:@"A"])
                {
                    //DLog(@"<a>");
                    estLienDuMenuSelectionne = NO; 
                    [href setString:[[[nodeChildLI attributes] getNamedItem:@"HREF"] textContent]];
                }
                
                //On chope le nom dans le span. Si on aurait fait [node textContent], on aurait eu tout le texte présent dans <li>, dont le texte des sous menus
                if ([nodeChildLI.nodeName isEqualToString:@"SPAN"])
                {
                    //DLog(@"<span>");
                    if (nodeChildLI.textContent && ![nodeChildLI.textContent isEqualToString:@""])
                    {    
                        [texteDuLien setString:nodeChildLI.textContent];
                    }
                }       
                
                if ([nodeChildLI.nodeName isEqualToString:@"DIV"])
                {
                    //DLog(@"<div>");
                }
                
            }
            while ((nodeListLI = [[nodeListLI item:0] childNodes]));
            
            //Si on est tombé sur le lien de menu actif
            if (estLienDuMenuSelectionne)
            {
                //On ajoute au tableau le lien de menu actif (contenu dans fichier, avec le contenu de texte
                [array addObject:[[NSDictionary alloc] initWithObjectsAndKeys:fichier,@"referenceFichier",texteDuLien,@"sequenceName",[[NSNumber alloc] initWithInt:indice],@"indice",nil]];
            }
            else 
            {
                [array addObject:[[NSDictionary alloc] initWithObjectsAndKeys:href,@"referenceFichier",texteDuLien,@"sequenceName",[[NSNumber alloc] initWithInt:indice],@"indice",nil]];
            }
            
            DOMNode * nodeSousMenuUL = [[[[nodeLI childNodes] item:1] nodeName] isEqualToString:@"UL"] ? [[nodeLI childNodes] item:1] : nil;
            
            //Si on a affaire à un sous-menu, on utilise la super-récursivité et on creuse plus profond dans le DOM
            if (nodeSousMenuUL)
            {
                //DLog(@"<ul>");
                [self recherche:nodeSousMenuUL fichier:fichier];
            }    
        }
        else 
        {
            DLog(@"Balise inattendue");
        }
    }
    //Si on sort d'un élément DOM de type UL, on décrémente la profondeur du tableau
    indice--; 
    
}






- (IBAction)rechercheFichierIcone:(id)sender {
    
    DLog(@"");
    [textFieldFichierIcone becomeFirstResponder];
    int i; // Loop counter.
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];    
    NSURL * url =[[NSURL alloc] initWithString:[[textFieldFichierIcone stringValue] stringByDeletingLastPathComponent]];
    [openDlg setDirectoryURL:url];
    [openDlg setAllowedFileTypes:[NSArray arrayWithObjects:@"jpg", @"jpeg", @"gif", @"png", @"ico",  nil]];
    
    [openDlg setCanCreateDirectories:YES];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    [openDlg setAllowsMultipleSelection:NO];
    // Enable the selection of directories in the dialog.
    //[openDlg setCanChooseDirectories:YES];
    
    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if (  [openDlg runModal] == NSOKButton )
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg URLs];
        
        // Loop through all the files and process them.
        for( i = 0; i < [files count]; i++ )
        {
            NSURL* fileName = [files objectAtIndex:i];
            [textFieldFichierIcone setStringValue:[fileName path]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:self.textFieldFichierIcone.stringValue])
            {
                self.imageIconeView.image = [[NSImage alloc] initWithContentsOfFile:self.textFieldFichierIcone.stringValue];                
            }
            
            
            
            // Do something with the filename.
        }
    }
}

- (IBAction)rechercheFichierPubli:(id)sender{
    DLog(@"");
    [textFieldFichierPubli becomeFirstResponder];
    int i; // Loop counter.
    
    
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    NSURL *url = [[NSURL alloc] initWithString:textFieldFichierPubli.stringValue];
    [openDlg setCanCreateDirectories:YES];
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    [openDlg setAllowsMultipleSelection:NO];
    [openDlg setDirectoryURL:url];
    [openDlg setAllowedFileTypes:[NSArray arrayWithObjects:@"publi", nil]];
    [openDlg setAllowsOtherFileTypes:NO];
       
    if ( [openDlg runModal] == NSOKButton)
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg URLs];
        
        // Loop through all the files and process them.
        for( i = 0; i < [files count]; i++ )
        {
            NSURL* fileName = [files objectAtIndex:i];
            [textFieldFichierPubli setStringValue:[fileName path]];
            // Do something with the filename.
        }
    }
    
    
}
- (IBAction)rechercheDossierWeb:(id)sender{
    DLog(@"");
    [textFieldDossierWeb becomeFirstResponder];
    int i; // Loop counter.
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    NSURL *url = [[NSURL alloc] initWithString:textFieldDossierWeb.stringValue];
    
    [openDlg setCanCreateDirectories:YES];
    [openDlg setDirectoryURL:[[NSURL alloc] initWithString:[textFieldDossierWeb stringValue]]];
    // Enable the selection of files in the dialog.
    // [openDlg setCanChooseFiles:YES];
    [openDlg setAllowsMultipleSelection:NO];
    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:YES];
    [openDlg setDirectoryURL:url];

    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if ( [openDlg runModal] == NSOKButton )
    {    // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg URLs];
        
        // Loop through all the files and process them.
        for( i = 0; i < [files count]; i++ )
        {
            NSURL* fileName = [files objectAtIndex:i];
            [textFieldDossierWeb setStringValue:[fileName path]];
            // Do something with the filename.
        }
    }
    
    
}

- (IBAction)rechercheFichierPathCible:(id)sender {
    DLog(@"");
    [textFieldPathCible becomeFirstResponder];
    int i; // Loop counter.
    
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    NSURL *url = [[NSURL alloc] initWithString:textFieldPathCible.stringValue];
    [openDlg setCanCreateDirectories:YES];
    [openDlg setDirectoryURL:[[NSURL alloc] initWithString:[textFieldPathCible stringValue]]];
    // Enable the selection of files in the dialog.
    //[openDlg setCanChooseFiles:YES];
    [openDlg setAllowsMultipleSelection:NO];
    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:YES];
    [openDlg setDirectoryURL:url];

    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if ( [openDlg runModal] == NSOKButton )
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg URLs];
        
        // Loop through all the files and process them.
        for( i = 0; i < [files count]; i++ )
        {
            NSURL* fileName = [files objectAtIndex:i];
            [textFieldPathCible setStringValue:[fileName path]];
            // Do something with the filename.
        }
    }
    
}

#pragma mark -
#pragma mark WebView delegates
- (void)webView:(WebView *)sender didStartProvisionalLoadForFrame:(WebFrame *)frame
{
}

- (void)webView:(WebView *)sender willCloseFrame:(WebFrame *)frame
{
}

- (void)webView:(WebView *)sender willPerformClientRedirectToURL:(NSURL *)URL delay:(NSTimeInterval)seconds fireDate:(NSDate *)date forFrame:(WebFrame *)frame
{
}

- (void)webView:(WebView *)sender didCancelClientRedirectForFrame:(WebFrame *)frame
{
}
- (void)webView:(WebView *)sender didChangeLocationWithinPageForFrame:(WebFrame *)frame
{
}

- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame
{
    compteurErreurs++;
}

- (void)webView:(WebView *)sender didClearWindowObject:(WebScriptObject *)windowObject forFrame:(WebFrame *)frame
{
    
}
- (void)webView:(WebView *)sender didCommitLoadForFrame:(WebFrame *)frame
{
    
}


- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
    DLog(@"");
    
    compteurDeFichiers++;
    
    //Nom du fichier menu à traiter
    NSString * fichier = [[sender mainFrameURL] lastPathComponent]; 
    
    array = nil;
    array = [[NSMutableArray alloc] init];
    
    //Représentation de la page dans le DOM
    DOMDocument * document = [[sender mainFrame] DOMDocument];
    
    if (document != nil)
    {
        //Noeud de root du document (= Body)
        DOMNode * root =[[document documentElement] firstChild];
        // skip any text nodes before the first value node
        while (root.nodeName) 
        {
            //On vérifie bien qu'on est dans le body de la page
            if ([root.nodeName isEqualToString:@"BODY"])
            {
                DOMNode * node = [root firstChild];
                
                DOMNodeList * domNodeList = [node childNodes];
                
                for (int i = 0;i< [domNodeList length];i++) 
                {
                    DOMNode * noeud =  [domNodeList item:i];
                    indice = 0;
                    //On génère les états des menus
                    [self recherche:noeud fichier:fichier];
                }
                
                break; 
            }
            else
            {    
                //On ne va jamais là dedans ; wtf.
                root = [root nextSibling];
            }
        }
        
    }
    
    //On ajoute les liens menus à une liste globale
    [arrayListe addObject:[[NSDictionary alloc] initWithObjectsAndKeys:array,@"liste",fichier,@"fichier",nil]];
    
    //Une fois qu'on a récupéré tous les états de menus possibles,
    if (nombreDeFichiers == compteurDeFichiers+compteurErreurs)
    {    
        
        NSMutableArray * array1 = [[NSMutableArray alloc] init];
        
        for (NSDictionary * liensMenus in arrayListe)
        {
            if ([array1 containsObject:[liensMenus objectForKey:@"liste"]])
            {
            }
            else
            {
                [array1 addObject:[liensMenus objectForKey:@"liste"]];
            }
        }
        
        NSMutableArray * array2 = [[NSMutableArray alloc] init];
        for (NSArray * toto in array1)
        {
            NSUInteger index = 0;
            NSUInteger compteur = 0;
            NSUInteger index_last = 0;
            int indiceNoeud_last;
            for (NSDictionary * titi in toto) 
            {
                compteur++;
                NSNumber * indiceNoeud = [titi objectForKey:@"indice"];
                //NSString * fichierNoeud = [titi objectForKey:@"referenceFichier"];
                
                if ([array2 containsObject:titi])
                {
                    compteur = 0;
                    index = [array2 indexOfObject:titi];
                }
                else
                {
                    if (index == 0)
                    {   
                        [array2 addObject:titi];
                    }
                    else
                    {
                        [array2 insertObject:titi atIndex:index+compteur];
                    }
                }
                index_last = index;
                indiceNoeud_last = indiceNoeud.intValue;
                
            }            
        }
        
        //Parcours de l'arborescence du fichier traité
        NSMutableArray * liste1= [[NSMutableArray alloc] init];
        NSMutableArray * listeDesSequences= [[NSMutableArray alloc] init];
        NSMutableArray * listeDesSequencesNiveau1= [[NSMutableArray alloc] init];
        NSMutableArray * liste2= [[NSMutableArray alloc] init];
        //int indiceNoeud_last = 0;
        int compteur = 0;
        for (NSDictionary * titi in array2)
        {
            
            compteur++;
            NSNumber * indiceNoeud = [titi objectForKey:@"indice"];
            NSDictionary * noeudLast;
            NSDictionary * noeudtraite;
            NSString * fichierNoeud = [titi objectForKey:@"referenceFichier"];
            NSString * textNoeud = [titi objectForKey:@"sequenceName"];
            if (indiceNoeud.intValue == 1)
            {
                [listeDesSequencesNiveau1 addObject:textNoeud];
                BOOL top = NO;
                for (NSDictionary * toto in array2)
                {
                    if (titi == toto)
                    {
                        top = YES;
                        liste2= nil;
                        liste2= [[NSMutableArray alloc] init];
                    }   
                    else
                    {
                        if (top)
                        {    
                            NSNumber * indiceNoeud1 = [toto objectForKey:@"indice"];
                            //NSString * fichierNoeud1 = [toto objectForKey:@"referenceFichier"];
                            //NSString * textNoeud1 = [toto objectForKey:@"sequenceName"];
                            
                            
                            if (indiceNoeud.intValue + 1 == indiceNoeud1.intValue)                            
                            {
                                [liste2 addObject:toto];
                                noeudLast = toto;
                            }
                            else if (indiceNoeud.intValue >= indiceNoeud1.intValue)
                            {
                                top = NO;
                            }
                            else if (indiceNoeud.intValue < indiceNoeud1.intValue)                            
                            {
                                // appel 
                                if (!(noeudLast == noeudtraite))
                                {
                                    NSDictionary * result = [self rechercheNiveauInferieur:array2 noeud:noeudLast];
                                    [liste2 removeObject:noeudLast];
                                    [liste2 addObject:result];
                                    noeudtraite = noeudLast;
                                }
                            }
                        }
                    }
                    
                }
                
                listeDesSequences =nil;
                listeDesSequences = [[NSMutableArray alloc] init];
                
                for (NSDictionary * a in liste2)
                {
                    [listeDesSequences addObject:[a objectForKey:@"sequenceName"]];
                    
                }                
                [liste1 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[[NSArray alloc] initWithObjects:textNoeud,nil],@"listeDesItems",listeDesSequences,@"listeDesSequences",textNoeud,@"sequenceName",fichierNoeud,@"referenceFichier",liste2,@"sequenceContent",nil]];
                
            }
        }
        //TODO:Intégrer la recherche d'annexe + l'intégration dans le pList final.
        
        //Construction du pList
        //Dico de base ("Commencer...")
        NSDictionary * dicoBaseChaine = [[NSDictionary alloc] initWithObjectsAndKeys:
                               [[NSArray alloc] initWithObjects:self.textFieldNomRessource.stringValue,nil],@"listeDesItems",
                               listeDesSequencesNiveau1,@"listeDesSequences",
                               @"",@"referenceFichier",
                               self.textFieldNomRessource.stringValue,@"sequenceName",
                               liste1,@"sequenceContent",nil];
        
        
        arrayToAppendToFichierSequencesPlist = [[NSMutableArray alloc] init];
        [arrayToAppendToFichierSequencesPlist addObject:dicoBaseChaine];

        //S'il y a des annexes, on les met dans arrayToAppendToFichierSequencesPlist
        (void)[self rechercheAnnexes];
        
        NSArray * pList = [[NSArray alloc] initWithArray:arrayToAppendToFichierSequencesPlist];
        //Paths
        NSString * dir = [self.textFieldPathCible.stringValue stringByAppendingPathComponent:self.textFieldDossierRessource.stringValue];
        
        [[NSFileManager defaultManager] createDirectoryAtPath:dir withIntermediateDirectories:YES  attributes:nil  error:nil];
        
        NSString * fichierResultat = [dir stringByAppendingPathComponent:kFichierResultat];
        NSString * thePathOutPut = [dir stringByAppendingPathComponent:kFichierOutPut];
        NSString * thePathOutPut2 = [dir stringByAppendingPathComponent:kNomFichierParent];
       
        if (![pList writeToFile:fichierResultat atomically:YES])
        {
            
            DLog(@"erreur creation fichier %@ ",fichierResultat);
        }
        else
        {
            [self synchroPostElgg:fichierResultat thePathOutPut:thePathOutPut thePathOutPut2:thePathOutPut2];
        }
        
        
        [self ecritureFichierSupport];   
        
        
    }
}

#pragma mark - Génération de ressource : fin

-(NSDictionary *)rechercheNiveauInferieur:(NSArray *)array2 noeud:(NSDictionary *)noeud
{
    DLog(@"");
    NSDictionary * res;
    NSMutableArray * listeDesSequences = [[NSMutableArray alloc] init];
    NSMutableArray * liste2= [[NSMutableArray alloc] init];
    //int indiceNoeud_last = 0;
    int compteur = 0;
    
    compteur++;
    NSNumber * indiceNoeud = [noeud objectForKey:@"indice"];
    NSString * fichierNoeud = [noeud objectForKey:@"referenceFichier"];
    NSString * textNoeud = [noeud objectForKey:@"sequenceName"];
    NSDictionary * noeudLast;
    NSDictionary * noeudtraite;
    
    BOOL top = NO;
    for (NSDictionary * toto in array2)
    {
        if (noeud == toto)
        {
            top = YES;
            liste2= nil;
            liste2= [[NSMutableArray alloc] init];
            
        }   
        else
        {
            if (top)
            {    
                NSNumber * indiceNoeud1 = [toto objectForKey:@"indice"];
                //NSString * fichierNoeud1 = [toto objectForKey:@"referenceFichier"];
                //NSString * textNoeud1 = [toto objectForKey:@"sequenceName"];
                
                
                if (indiceNoeud.intValue + 1 == indiceNoeud1.intValue)                            
                {
                    [liste2 addObject:toto];
                    noeudLast = toto;
                }
                else if (indiceNoeud.intValue >= indiceNoeud1.intValue)
                {
                    top = NO;
                }
                else if (indiceNoeud.intValue < indiceNoeud1.intValue)                            
                {
                    
                    if (!(noeudLast == noeudtraite))
                    {
                        NSDictionary * result = [self rechercheNiveauInferieur:array2 noeud:noeudLast];
                        [liste2 removeObject:noeudLast];
                        [liste2 addObject:result];
                        noeudtraite = noeudLast;
                    }
                }
            }
        }
        
    }
    
    listeDesSequences =nil;
    listeDesSequences = [[NSMutableArray alloc] init];
    
    
    for (NSDictionary * a in liste2)
    {
        [listeDesSequences addObject:[a objectForKey:@"sequenceName"]];
        
    }
    
    res = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSArray alloc] initWithObjects:textNoeud,nil],@"listeDesItems",listeDesSequences,@"listeDesSequences",textNoeud,@"sequenceName",fichierNoeud,@"referenceFichier",liste2,@"sequenceContent",nil];
    return res;    
}

-(BOOL)rechercheAnnexes
{
    DLog(@"");
    //Dossier (absolu) dans lequel on va stocker tous ces liens
    NSString * pathCible =[[[self.textFieldPathCible stringValue] stringByAppendingPathComponent:[self.textFieldDossierRessource stringValue]] stringByAppendingPathComponent:@"WebPath/co"];
    
    //On reconstitue les noms des annexes (nom-fichier-publi_x.html)
    NSString * nomAnnexe = [[[textFieldFichierPubli stringValue] lastPathComponent] stringByDeletingPathExtension];
    
    NSMutableString * nomComplet = [[NSMutableString alloc] initWithString:[[pathCible stringByAppendingPathComponent:nomAnnexe] stringByAppendingString:@"_1.html"]];
    
    int i = 1;
    
    while ([[NSFileManager alloc] fileExistsAtPath:nomComplet])
    {
        NSString * titre = [self retrouverTitreAnnexe:nomComplet];
        titre = titre == @"" ? @"Sans-Titre" : titre;
        
        NSDictionary * temp = [[NSDictionary alloc] initWithObjectsAndKeys:titre,@"sequenceName",[nomComplet lastPathComponent],@"referenceFichier", nil];
        [arrayToAppendToFichierSequencesPlist addObject:temp];
        
        [nomComplet setString:[[nomComplet stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%d.html",nomAnnexe,++i]]];
    }
    
    return i == 1 ? false : true;
}

-(NSString*)retrouverTitreAnnexe:(NSString*)pathFichierAnnexe
{
    DLog(@"");
    NSError *error;
    NSString * contenuFichierAnnexe = [[NSString alloc] initWithContentsOfFile:pathFichierAnnexe encoding:NSUTF8StringEncoding error:&error];
    
    NSRange rangeOfTitleBegining = [contenuFichierAnnexe rangeOfString:@"class=\"mainContent_ti\"><span>"];
    
    if(rangeOfTitleBegining.location == NSNotFound)
        return @"";
    
    NSRange rangeOfTitleEnding = [contenuFichierAnnexe rangeOfString:@"</span>" options:NSCaseInsensitiveSearch range:NSMakeRange(rangeOfTitleBegining.location + rangeOfTitleBegining.length, contenuFichierAnnexe.length - (rangeOfTitleBegining.location + rangeOfTitleBegining.length))];
    
    if(rangeOfTitleEnding.location == NSNotFound)
        return @"";
    
    NSString * titleFinallyRetrieved = [contenuFichierAnnexe substringWithRange:NSMakeRange(rangeOfTitleBegining.location + rangeOfTitleBegining.length, rangeOfTitleEnding.location - (rangeOfTitleBegining.location + rangeOfTitleBegining.length))];
    
    return [self escapeAccents:titleFinallyRetrieved];
}

-(NSString*)escapeAccents:(NSString*)stringWithHTMLAccents
{
    NSMutableString * tempString = [[NSMutableString alloc] initWithString:stringWithHTMLAccents];
    
    [tempString replaceOccurrencesOfString:@"&eacute;" withString:@"é" options:NSLiteralSearch range:NSMakeRange(0, [tempString length])];
    
    
    [tempString replaceOccurrencesOfString:@"&egrave;" withString:@"è" options:NSLiteralSearch range:NSMakeRange(0, [tempString length])];
    
    
    [tempString replaceOccurrencesOfString:@"&Eacute;" withString:@"É" options:NSLiteralSearch range:NSMakeRange(0, [tempString length])];
    
    
    [tempString replaceOccurrencesOfString:@"&Egrave;" withString:@"È" options:NSLiteralSearch range:NSMakeRange(0, [tempString length])];
    [tempString replaceOccurrencesOfString:@"&#145;" withString:@"'" options:NSLiteralSearch range:NSMakeRange(0, [tempString length])];
    [tempString replaceOccurrencesOfString:@"&#146;" withString:@"'" options:NSLiteralSearch range:NSMakeRange(0, [tempString length])];
    return (NSString*)tempString;
}

-(void)ecritureFichierSupport
{
    DLog(@"");
    NSFileManager * fileManager =[NSFileManager defaultManager];
    NSString *file;
    
    NSString * pathcible = [NSString pathWithComponents:[NSArray arrayWithObjects:self.textFieldPathCible.stringValue,self.textFieldDossierRessource.stringValue,nil]]; 
    
    
    unsigned long long totalSize = 0;
    NSDirectoryEnumerator *enumerateur = [fileManager enumeratorAtPath:pathcible];
    while ((file = [enumerateur nextObject])) 
    {
        NSError * error;
        NSDictionary * attributesOfItemAtPath =  [fileManager attributesOfItemAtPath:[pathcible stringByAppendingPathComponent:file] error:&error];
        if (attributesOfItemAtPath)
        {
            NSNumber * sizeNumber = [attributesOfItemAtPath objectForKey:NSFileSize];
            if (sizeNumber) 
            {
                totalSize += [sizeNumber unsignedLongLongValue]; 
            }
        }
    }
    
    
    CGFloat tailleMega =   totalSize/1000000;
    
    NSDictionary * monDico = [[NSDictionary alloc] initWithObjectsAndKeys:									
                              self.textFieldURLdeDepot.stringValue          ,kRessourcebaseURL,
                              self.textFieldCatalogue.stringValue                ,kRessourceCatalogueStr,
                              self.textFieldTheme.stringValue                    ,kRessourceDomaineStr,
                              self.textFieldId.stringValue                       ,kRessourceElggidStr,
                              self.textFieldEtablissement.stringValue            ,kRessourceEtablissementStr,
                              kNomFichierParent                                  ,kRessourceFichierParent,
                              self.textFieldDossierRessource.stringValue         ,kRessourceFolderStr,
                              [self.textFieldFichierIcone.stringValue lastPathComponent],kRessourceIconeStr,
                              self.textFieldId.stringValue                       ,kRessourceIdStr,
                              self.textFieldNomRessource.stringValue             ,kRessourceNomStr,
                              [NSDate date]                                      ,kRessourceReleasedStr,
                              kFichierOutPut                                     ,kRessourceStructureStr,
                              self.textViewDescription.string                    ,kRessourceSummaryStr,
                              kRessourcesWebPlist                                ,kRessourceWebStr,
                              [[NSNumber alloc] initWithFloat:tailleMega]        ,kRessourceTailleStr,
                              self.textFieldTitreRessource.stringValue           ,kRessourceTitleStr,
                              [NSDate date]                                      ,kRessourceUpdatedStr,
                              @"zip"                                             ,@"status",
                              self.textFieldVersion.stringValue                  ,kRessourceVersion1Str,
                              self.textFieldVersion2.stringValue                 ,kRessourceVersion2Str,
                              self.textFieldVersion3.stringValue                 ,kRessourceVersion3Str,
                              //self.textFieldURLdeDepot.stringValue          ,kRessourceURL,
                              nil];
    
    NSString * fichierEntry = [pathcible stringByAppendingPathComponent:kSupportFile];
    if([[[NSArray alloc] initWithObjects:monDico,nil] writeToFile:fichierEntry atomically:YES] == NO)
        DLog(@"erreur ecriture fichier : %@",fichierEntry);
    
    //Mettre requête SQL ici pour envoyer vers elgg_iphone RESSOURCES 
    if(checkBoxUpdate.state == NSOnState)
        [self envoyerRequeteServeur:tailleMega];
    
    
    
    
}

-(void)envoyerRequeteServeur:(CGFloat) tailleMega
{
    //DONE: refaire un dico propre a envoyer au service web, puis aller boire des bières
    //NOT-TODO: Creer un champ de login dans le formulaire
    //DONE: Créer un champ d'OWNER_GUID
    //FIXED: Recuperer l'id des domaines et enseignements plutot que les strings
    //Ca fausse la requête sql, et ca plante supcast derriere. Fuck
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString * todayDate = [formatter stringFromDate:[NSDate date]];
    
    //FIXME:idTheme & idEtablissement renvoie nil, et ça foire le dico.
    NSString * idTheme = [listeThemes objectForKey:textFieldTheme.stringValue];
    NSString * idEtablissement = [listeEtablissements objectForKey:textFieldEtablissement.stringValue];

    
    
    NSDictionary * dicoBDD = [[NSDictionary alloc] initWithObjectsAndKeys:
    self.textFieldId.stringValue                       ,kRessourceIdStr,
    ressourceVisibilite                                 ,kRessourceVisible,
    self.textFieldNomRessource.stringValue             ,kRessourceNomStr,
    self.textFieldId.stringValue                       ,kRessourceElggidStr,
    kRessourcesWebPlist                                ,kRessourceWebStr,
    self.textFieldTitreRessource.stringValue           ,kRessourceTitleStr,
    self.textFieldURLdeDepot.stringValue               ,kRessourceURL,
    self.textFieldURLdeDepot.stringValue               ,kRessourcebaseURL,
    kNomFichierParent                                  ,kRessourceFichierParent,
    self.textFieldDossierRessource.stringValue         ,kRessourceFolderStr,
    [[NSNumber alloc] initWithFloat:tailleMega]        ,kRessourceTailleStr,
    kFichierOutPut                                     ,kRessourceStructureStr,
    self.textViewDescription.string                    ,kRessourceSummaryStr,
    [self.textFieldFichierIcone.stringValue lastPathComponent],kRessourceIconeStr,
    kMediaPlist                                        ,kRessourceMedia,
    idEtablissement                                    ,kRessourceEtablissementStr,
    idTheme                                            ,kRessourceDomaineStr,
    self.textFieldCatalogue.stringValue                ,kRessourceCatalogueStr,
    todayDate                                          ,kRessourceUpdatedStr,
    self.textFieldVersion.stringValue                  ,kRessourceVersion1Str,
    self.textFieldVersion2.stringValue                 ,kRessourceVersion2Str,
    self.textFieldVersion3.stringValue                 ,kRessourceVersion3Str,
    self.textFieldVisibiliteDev.stringValue            ,kRessourceOwnerGUID,
    nil];
    //Alert de débug
    NSAlert *alert = [[NSAlert alloc] init];
    
    NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kAjouterBDD];
    NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData * jsonDico = [NSJSONSerialization dataWithJSONObject:dicoBDD options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonDico length]] forHTTPHeaderField:@"Content-Length"];
        
    
    //[request setAllHTTPHeaderFields:dicoBDD];
    [request setHTTPBody:jsonDico];
    
    connectionFinaleBDD = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(connectionFinaleBDD) 
    {
        NSMutableData *data = [[NSMutableData alloc] init];
        reponseAjoutRessource = data;
        [alert setMessageText:@"Ressource générée avec succès!"];
        [alert runModal];
    }
    else 
    {    
        [alert setMessageText:@"Erreur de connection à iMedia (batard)."];
        [alert runModal];
    }
    
    
}

//Useless
- (void)chargeHTML:(NSNotification *)notification
{
    DLog(@"");
    // * urlSource = [notification object];   // incoming object is an NSArray of AppRecords
    
    
}


-(void)synchroPostElgg:(NSString*)thePath 
         thePathOutPut:(NSString*)thePathOutPut 
        thePathOutPut2:(NSString*)thePathOutPut2
{
    DLog(@"");
    /***************************************************/
    int indiceSynchro = 0;
    
    NSError *error;
    
    NSString *stringFromFileAtPath = [[NSString alloc]
                                      
                                      initWithContentsOfFile:thePath
                                      
                                      encoding:NSUTF8StringEncoding
                                      
                                      error:&error];
    
    if (stringFromFileAtPath == nil) {
        
        // an error occurred
        
        NSLog(@"Error reading file at %@\n%@",
              
              thePath, [error localizedFailureReason]);
        
        // implementation continues ...
        
    } 
    
    NSMutableString * buffer = [[NSMutableString alloc] initWithString:stringFromFileAtPath]; 
    
    NSRange MyRange =[buffer rangeOfString:@"<key>title</key>" options:NSCaseInsensitiveSearch|NSBackwardsSearch]; 
    
    BOOL ok = YES;
    NSUInteger position;
    if (MyRange.location != NSNotFound )
    {
        position = MyRange.location;
        // NSLog(@"position = %lu",position);
        ok = YES;
    }
    else
    {
        ok = NO;
        position = 0;
    }        
    
    while (ok)
    {       
        NSString * stringToInsert = [[NSString alloc] initWithFormat:@"%@%@%@%d%@",                                     
                                     @"<key>visible</key>",
                                     @"<true/>",
                                     @"<key>idecran</key><string>",++indiceSynchro,@"</string>\n"];
        [buffer insertString:stringToInsert atIndex:position];
        
        NSRange MyRange =[buffer rangeOfString:@"<key>title</key>" options:NSCaseInsensitiveSearch|NSBackwardsSearch range:NSMakeRange(0,position)]; 
        
        if (MyRange.location != NSNotFound )
        {
            position = MyRange.location;
            //  NSLog(@"position = %lu",position);
            ok = YES;
        }
        else
        {
            ok = NO;
            position = 0;
            
        }  
        
    }    
    
    /*******************************/
    
    MyRange =[buffer rangeOfString:@"<key>sequenceName</key>" options:NSCaseInsensitiveSearch|NSBackwardsSearch]; 
    
    ok = YES;
    if (MyRange.location != NSNotFound )
    {
        position = MyRange.location;
        //   NSLog(@"position = %lu",position);
        ok = YES;
    }
    else
    {
        ok = NO;
        position = 0;
    }        
    
    while (ok)
    {       
        NSString * stringToInsert = [[NSString alloc] initWithFormat:@"%@%@%@%d%@",                                     
                                     @"<key>visible</key>",
                                     @"<true/>",
                                     @"<key>idecran</key><string>",++indice,@"</string>\n"];
        [buffer insertString:stringToInsert atIndex:position];
        
        NSRange MyRange =[buffer rangeOfString:@"<key>sequenceName</key>" options:NSCaseInsensitiveSearch|NSBackwardsSearch range:NSMakeRange(0,position)]; 
        
        if (MyRange.location != NSNotFound )
        {
            position = MyRange.location;
            //   NSLog(@"position = %lu",position);
            ok = YES;
        }
        else
        {
            ok = NO;
            position = 0;
            
        }  
        
    }    
    
    /**************************/
    /*******************************/
    
    MyRange =[buffer rangeOfString:@"<key>partName</key>" options:NSCaseInsensitiveSearch|NSBackwardsSearch]; 
    
    ok = YES;
    if (MyRange.location != NSNotFound )
    {
        position = MyRange.location;
        //  NSLog(@"position = %lu",position);
        ok = YES;
    }
    else
    {
        ok = NO;
        position = 0;
    }        
    
    while (ok)
    {       
        NSString * stringToInsert = [[NSString alloc] initWithFormat:@"%@%@%@%d%@",                                     
                                     @"<key>visible</key>",
                                     @"<true/>",
                                     @"<key>idecran</key><string>",++indice,@"</string>\n"];
        [buffer insertString:stringToInsert atIndex:position];
        
        NSRange MyRange =[buffer rangeOfString:@"<key>partName</key>" options:NSCaseInsensitiveSearch|NSBackwardsSearch range:NSMakeRange(0,position)]; 
        
        if (MyRange.location != NSNotFound )
        {
            position = MyRange.location;
            //    NSLog(@"position = %lu",position);
            ok = YES;
        }
        else
        {
            ok = NO;
            position = 0;
            
        }  
        
    }    
    
    /**************************/
    
    [buffer writeToFile:thePathOutPut atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    
    NSArray * modulesList = [[NSArray alloc] initWithContentsOfFile:thePathOutPut];
    
    //-----------------------get the parents-children relations
    
    NSArray *listIdEcran=[[NSArray alloc] initWithArray:[TraceNewMessage findPCR:modulesList]];
    
    //NSLog(@"listIdEcran = %@  ",listIdEcran);
    
    NSMutableArray *pcrDic= [[NSMutableArray alloc] init];
    NSMutableArray *pcrDic1= [[NSMutableArray alloc] init];
    
    for (id element in listIdEcran)
    {        
        
        // NSLog(@"element->idecran = %@",[element objectForKey:@"idecran"]);
        NSDictionary *subIdData=[[NSDictionary alloc] initWithDictionary:[TraceNewMessage dataPCR:[element objectForKey:@"idecran"] forData:modulesList]];
        //NSLog(@"subIdData = %@",subIdData);
        
        NSArray *subId=[[NSArray alloc] initWithArray:[TraceNewMessage findPCR2:subIdData 
                                                                        element:[element objectForKey:@"idecran"]
                                                                       element1:[element objectForKey:@"idecran"] 
                                                                            cas:@"cas1"]];
        //NSLog(@"subId = %@",subId);
        //if ([subId containsObject:[element objectForKey:@"idecran"]])
        //    [subId removeObject:[element objectForKey:@"idecran"]];
        
        if(subId!=nil && [subId count] > 0)
            [pcrDic addObjectsFromArray:subId];
        
    }
    
    
    ok = YES;
    
    NSUInteger count_last = [pcrDic count];
    
    while (ok)
    {
        pcrDic1 = [[NSMutableArray alloc] initWithArray:[TraceNewMessage suppressionDesLiensdeParenteIndirect:pcrDic]]; 
        
        if (count_last > [pcrDic1 count])
        {
            ok =YES;
            count_last = [pcrDic1 count];
        }
        else
        {
            ok =NO;
            
        }      
        pcrDic = pcrDic1;
    }         
    NSLog(@"pcrDic1 count = %lu",[pcrDic1 count]);
    [TraceNewMessage affichage:pcrDic];
    //[pcrDic release];
    
    //-----------------------get the parents-children relations
    
    NSArray *listIdEcran1=[[NSArray alloc] initWithArray:[ListeNewMessages findPCR:modulesList]];
    
    NSMutableDictionary *pcrDic3 = [[NSMutableDictionary alloc] init];
    for (id element in listIdEcran1)
    {
        NSDictionary *subIdData=[[NSDictionary alloc] initWithDictionary:[ListeNewMessages dataPCR:element forData:modulesList]];
        NSArray *subId=[ListeNewMessages findPCR:subIdData];
        if(subId!=nil)
            [pcrDic3 setObject:subId forKey:element];
        
    }
    if ([pcrDic3 isKindOfClass:[NSDictionary class]]) NSLog(@"NSDictionary");
    
    /**************************/
    
    ([[NSFileManager defaultManager] fileExistsAtPath:thePathOutPut2])?NSLog(@"le fichier '%@' existe",thePathOutPut2):NSLog(@"le fichier '%@' n existe pas",thePathOutPut2);
    
    NSArray *arrayRes = [[NSMutableArray alloc] initWithObjects:pcrDic3,nil];
    
    ([arrayRes writeToFile:thePathOutPut2 atomically:YES ])?NSLog(@"le fichier '%@' a été écrit",thePathOutPut2):NSLog(@"le fichier '%@' n a pas été écrit",thePathOutPut2);
    
    /**************************/       
    
    // [pcrDic release];
    //-----------------------
    
    //[data release]; 
    //[modulesList release]; 
}


@end
