//
//  XMLToObjectParser.m
//  SupCastProducer
//
//  Created by CAPE - EMN on 02/03/11.
//  Copyright 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionFichier.h"
#import "XMLToObjectParser.h"

@implementation XMLToObjectParser

#pragma mark -
#pragma mark initialisation

@synthesize situation;
@synthesize level;
@synthesize levelLast;
@synthesize path;
@synthesize currentNodeContent;
@synthesize monDicoItemAudio;
@synthesize monArrayDicoItemAudio;
@synthesize monDicoArrayDicoItemAudio;
@synthesize monDicoRacine;
@synthesize monFichierDeBase;

@synthesize arrayFichier;
@synthesize dicoFichier;
@synthesize tableDictionnaireFichier;
@synthesize listeDesItems;
@synthesize listeDesSousSequences;
@synthesize listeDesSequences;
@synthesize monDicoContenuSequences;
@synthesize monArrayIntermediaire;
@synthesize monTableauDeCollecteIntermediare;
@synthesize arrayIndex;
@synthesize iCompeurFichier;
//@synthesize pathRacineProjet;

/*
 
 dicoFichier
 -->monDicoContenuSequences
 */


/*
 monArrayIntermediaire
 -->monDicoContenuSequences
 */

/*
 monDicoRacine
 -->monTableauDeCollecteIntermediare
 -->-->monDicoContenuSequences
 */

/*
 monDicoRacine
 
 -->tableDictionnaireFichier
 -->-->monDicoResult
 
 -->-->dicoFichier
 -->-->-->listeDesSousSequences
 -->-->-->arrayFichier
 -->-->-->currentNodeContent
 
 -->-->listeDesSequences
 -->-->-->currentNodeContent
 
 */



-(id)initWithSituation:(int)aSituation
          fichierPubli:(NSString *)aPathRacineProjet 
                 level:(int)aLevel
       iCompeurFichier:(int)aCompteur
{
	DLog(@"");
	if (self == [super init])
	{        
		self.situation = aSituation;
		self.path = aPathRacineProjet;
        self.iCompeurFichier = aCompteur;
	
    
    
        self.arrayIndex = [[NSMutableArray alloc] init]; 
        int h = 0;
        for (int i=0; i<1000; i++) {
            
            
            /*
             22 23 44
             32 33 73
             40 41 74
             
             
             
             */
            if (i == 22)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",23]];
                h = h + 1;
            }
            else if (i == 31)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",33]];
                h = h + 1;
            }
            else if (i == 38)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",41]];
                h = h + 1;
            }
            else if (i == 41)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",22]];
                h = h - 1;
            }
            else if (i == 69)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",73]];
                h = h + 1;
            }
            else if (i == 70)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",40]];
                h = h - 1;
            }
            else if (i == 71)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",74]];
                h = h + 1;
            }
            else if (i == 73)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",71]];
                h = h - 1;
            }
            else if (i == 74)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",72]];
                h = h - 1;
            }
            else if (i == 75)
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",32]];
                h = h - 1;
            }
            else
            {
                [arrayIndex addObject:[[NSString alloc] initWithFormat:@"%d",i+h]];
            }
            
        }        
        
        DLog(@"arrayIndex is %@",arrayIndex);

    
    }
    indiceFichier = 0;
	return self;
}

#pragma mark -
#pragma mark XMLToObjectParser Methods

- (id)parseXMLFromData:(NSData *)data 
			parseError:(NSError **)error
{
	DLog(@"");
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser parse];
	
	if([parser parserError] && error)
	{
		*error = [parser parserError];
	}
	[parser release];
	return self;
}

-(NSDictionary *)inspectionCollecte:(NSString *)nomFichier
{
    
	topBalisespex = NO;
	
	self.listeDesItems = nil;
	self.listeDesItems = [[NSMutableArray alloc] init];
	self.listeDesSousSequences = nil;
	self.listeDesSousSequences = [[NSMutableArray alloc] init];
	self.listeDesSequences = nil;
	self.listeDesSequences = [[NSMutableArray alloc] init];
	self.tableDictionnaireFichier = [[NSMutableArray alloc] init];
	self.monDicoRacine =[[NSMutableDictionary alloc] init];
	NSMutableArray * monArrayDictionnaireSequences = [[NSMutableArray alloc] init];
	//	NSMutableDictionary * rootDico = [[NSMutableDictionary alloc] init];
	
    //NSString * fichierEclate = [self eclate1:nomFichier];
    
	//===================================================
	// parse du fichier
	//===================================================
	
    //if (fichierEclate != nil)
    //{
    //    if ([[NSFileManager defaultManager] fileExistsAtPath:fichierEclate])
    //    {
    //        DLog(@"XMLeclate is %@",[fichierEclate lastPathComponent]);
    //        [self parseXMLFromData:[[NSFileManager defaultManager] contentsAtPath:fichierEclate]
    //                    parseError:nil];
    //    }
    //    else 
    //    {
    //        DLog(@"***************le fichie n'existe pas : %@",nomFichier);
    //    }	
   // }
   // else
   // {
    
    
     NSString* fichierXXXXXX = [[[nomFichier stringByDeletingLastPathComponent] 
     stringByAppendingPathComponent:
     [[NSString alloc] initWithFormat:@"%@_XXXXXX",[[nomFichier lastPathComponent] stringByDeletingPathExtension]]] 
     stringByAppendingPathExtension:[nomFichier pathExtension]];
     
     DLog(@"fichierXXXXXX is %@",fichierXXXXXX);
     if ([[NSFileManager defaultManager] fileExistsAtPath:fichierXXXXXX])
     {
         DLog(@"XML is %@",[nomFichier lastPathComponent]);
         [self parseXMLFromData:[[NSFileManager defaultManager] contentsAtPath:fichierXXXXXX]
                     parseError:nil];
     }
     else if ([[NSFileManager defaultManager] fileExistsAtPath:nomFichier])
     {
            DLog(@"XML is %@",[nomFichier lastPathComponent]);
            [self parseXMLFromData:[[NSFileManager defaultManager] contentsAtPath:nomFichier]
                        parseError:nil];
     }
     else 
     {
          DLog(@"***************le fichie n'existe pas : %@",nomFichier);
     }
        
    //}
	//===================================================
    //
	//===================================================
	[monArrayDictionnaireSequences release];
	monArrayDictionnaireSequences = nil;
	
	//===================================================
	
    [self.monDicoRacine setObject:nomFichier 
						   forKey:@"referenceFichier"];
	[self.monDicoRacine setObject:self.listeDesItems 
						   forKey:@"listeDesItems"];
	//===================================================
	//********************* Release *********************
	//===================================================
	[self.listeDesItems release];
	//[self.monDicoRacine release];
	return self.monDicoRacine;
}

- (NSRange)rechercheFin:(NSMutableString *)buffer 
     rangeOfStringDebut:(NSString *)rangeOfStringDebut 
       rangeOfStringFin:(NSString *)rangeOfStringFin
                  debut:(NSRange)rangeDebut 
{
    DLog(@"");
    BOOL ok = YES;
    NSRange MyRangeDebut = rangeDebut;
    NSRange MyRangeDebutLast = rangeDebut;
    NSRange MyRangeFin = NSMakeRange(0,0);
    NSRange MyRangeFinLast = [buffer rangeOfString:rangeOfStringFin 
                                           options:NSCaseInsensitiveSearch 
                                             range:NSMakeRange(MyRangeDebut.location+MyRangeDebut.length,[buffer length]-MyRangeDebut.location-MyRangeDebut.length)];
    
    if (MyRangeFinLast.location != NSNotFound) 
    {
        MyRangeFin = MyRangeFinLast;
    }
    else
    {
        ok = NO;
    }
    
    while (ok)
    {
        DLog(@"len is %lu",[buffer length]-MyRangeFin.location-MyRangeDebut.location-MyRangeDebut.length);        
        MyRangeDebutLast = [buffer rangeOfString:rangeOfStringDebut 
                                         options:NSCaseInsensitiveSearch 
                                           range:NSMakeRange(MyRangeDebut.location+MyRangeDebut.length,MyRangeFin.location-MyRangeDebut.location-MyRangeDebut.length)];
        if (MyRangeDebutLast.location == NSNotFound) 
        {
            return MyRangeFin;
        }
        else
        {
            MyRangeDebut =  MyRangeDebutLast;
            MyRangeFinLast = [buffer rangeOfString:rangeOfStringFin 
                                           options:NSCaseInsensitiveSearch 
                                             range:NSMakeRange(MyRangeFin.location+MyRangeFin.length,[buffer length]-MyRangeFin.location-MyRangeFin.length)];
            
            if (MyRangeFinLast.location != NSNotFound) 
            {
                MyRangeFin = MyRangeFinLast; 
            }
            else
            {
                ok = NO;
            }    
        }
    }
    
    return MyRangeFin;
}

-(NSArray *)eclate:(NSString *)nomFichier 
{
    DLog(@"XML is %@",[nomFichier lastPathComponent]);
	
	topBalisespex = NO;
    
    NSError *error;
    int position = 0;
    NSString *stringFromFileAtPath = [[NSString alloc]
                                      initWithContentsOfFile:nomFichier
                                      encoding:NSUTF8StringEncoding
                                      error:&error];
    
    if (stringFromFileAtPath == nil) {
        
        // an error occurred
        
        NSLog(@"Error reading file at %@\n%@",
              
              nomFichier, [error localizedFailureReason]);
        
        // implementation continues ...
        
    } 
    /////////////
    NSMutableString * buffer = [[NSMutableString alloc] initWithString:stringFromFileAtPath]; 
    NSString * myString1 = [[NSString alloc] initWithFormat:@"%@",@"<sp:div>"];
    NSRange MyRange1 =[buffer rangeOfString:myString1 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste1 = [[NSMutableArray alloc] init]; 
    
    BOOL ok = YES;
    int i = 0;
    //NSUInteger position;
    if (MyRange1.location != NSNotFound )
    {
        ok = YES;
        position = MyRange1.location + MyRange1.length;
        
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString1];
        [maBalise setLocationDebut:MyRange1.location];
        [maBalise setLongueurDebut:MyRange1.length];
        [maBalise setLocationFin:0];
        [maBalise setLongueurFin:0];
        [liste1 addObject:maBalise];
        [maBalise release];
        
    }
    else
    {
        ok = NO;
        //position = 0;
    }        
    
    while (ok)
    {       
        i++;        
        
        MyRange1 =[buffer rangeOfString:myString1 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange1.location+MyRange1.length,[buffer length]-MyRange1.location-MyRange1.length)];
        
        if (MyRange1.location != NSNotFound )
        {
            ok = YES;
            position = MyRange1.location + MyRange1.length;
            
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString1];
            [maBalise setLocationDebut:MyRange1.location];
            [maBalise setLongueurDebut:MyRange1.length];
            [maBalise setLocationFin:0];
            [maBalise setLongueurFin:0];
            [liste1 addObject:maBalise];
            [maBalise release];
            
            
            position = MyRange1.location + MyRange1.length;
        }
        else
        {
            ok = NO;
            
        }  
        
    }   
    
    ///////////////////
    NSString * myString2 = [[NSString alloc] initWithFormat:@"%@",@"</sp:div>"];
    NSRange MyRange2 =[buffer rangeOfString:myString2 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste2 = [[NSMutableArray alloc] init]; 
    if (MyRange2.location != NSNotFound )
    {
        ok = YES;
        position = MyRange2.location + MyRange2.length;
        
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString1];
        [maBalise setLocationFin:MyRange2.location];
        [maBalise setLongueurFin:MyRange2.length];
        [maBalise setLocationDebut:0];
        [maBalise setLongueurDebut:0];
        
        [liste2 addObject:maBalise];
        [maBalise release];
        
        
    }
    else
    {
        ok = NO;
    }        
    
    while (ok)
    {       
        i++;        
        
        MyRange2 =[buffer rangeOfString:myString2 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange2.location+MyRange2.length,[buffer length]-MyRange2.location-MyRange2.length)];
        
        if (MyRange2.location != NSNotFound )
        {
            ok = YES;
            position = MyRange2.location + MyRange2.length;
            
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString1];
            [maBalise setLocationFin:MyRange2.location];
            [maBalise setLongueurFin:MyRange2.length];
            [maBalise setLocationDebut:0];
            [maBalise setLongueurDebut:0];
            
            [liste2 addObject:maBalise];
            [maBalise release];
            
            
        }
        else
        {
            ok = NO;
            
        }  
        
    }   
    
    /////////////
    
    //////////////
    /////////////
    NSString * myString17 = [[NSString alloc] initWithFormat:@"%@",@"<sp:intro>"];
    NSRange MyRange17 =[buffer rangeOfString:myString17 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste17 = [[NSMutableArray alloc] init]; 
    
    ok = YES;
    i = 0;
    //NSUInteger position;
    if (MyRange17.location != NSNotFound )
    {
        ok = YES;
        position = MyRange17.location + MyRange17.length;
        
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString17];
        [maBalise setLocationDebut:MyRange17.location];
        [maBalise setLongueurDebut:MyRange17.length];
        [maBalise setLocationFin:0];
        [maBalise setLongueurFin:0];
        [liste17 addObject:maBalise];
        [maBalise release];
        
    }
    else
    {
        ok = NO;
        //position = 0;
    }        
    
    while (ok)
    {       
        i++;        
        
        MyRange17 =[buffer rangeOfString:myString17 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange17.location+MyRange17.length,[buffer length]-MyRange17.location-MyRange17.length)];
        
        if (MyRange17.location != NSNotFound )
        {
            ok = YES;
            position = MyRange17.location + MyRange17.length;
            
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString17];
            [maBalise setLocationDebut:MyRange17.location];
            [maBalise setLongueurDebut:MyRange17.length];
            [maBalise setLocationFin:0];
            [maBalise setLongueurFin:0];
            [liste17 addObject:maBalise];
            [maBalise release];
            
            
            position = MyRange17.location + MyRange17.length;
        }
        else
        {
            ok = NO;
            
        }  
        
    }   
    
    ///////////////////
    NSString * myString18 = [[NSString alloc] initWithFormat:@"%@",@"</sp:intro>"];
    NSRange MyRange18 =[buffer rangeOfString:myString18 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste18 = [[NSMutableArray alloc] init]; 
    if (MyRange18.location != NSNotFound )
    {
        ok = YES;
        position = MyRange18.location + MyRange18.length;
        
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString17];
        [maBalise setLocationFin:MyRange18.location];
        [maBalise setLongueurFin:MyRange18.length];
        [maBalise setLocationDebut:0];
        [maBalise setLongueurDebut:0];
        
        [liste18 addObject:maBalise];
        [maBalise release];
        
        
    }
    else
    {
        ok = NO;
    }        
    
    while (ok)
    {       
        i++;        
        
        MyRange18 =[buffer rangeOfString:myString18 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange18.location+MyRange18.length,[buffer length]-MyRange18.location-MyRange18.length)];
        
        if (MyRange18.location != NSNotFound )
        {
            ok = YES;
            position = MyRange18.location + MyRange18.length;
            
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString17];
            [maBalise setLocationFin:MyRange18.location];
            [maBalise setLongueurFin:MyRange18.length];
            [maBalise setLocationDebut:0];
            [maBalise setLongueurDebut:0];
            
            [liste18 addObject:maBalise];
            [maBalise release];
        }
        else
        {
            ok = NO;
        }  
    }   
    
    /////////////

    NSString * myString7 = [[NSString alloc] initWithFormat:@"%@",@"<sp:conclu>"];
    NSRange MyRange7 =[buffer rangeOfString:myString7 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste7 = [[NSMutableArray alloc] init]; 
    
    ok = YES;
    i = 0;
    //NSUInteger position;
    if (MyRange7.location != NSNotFound )
    {
        ok = YES;
        position = MyRange7.location + MyRange7.length;
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString7];
        [maBalise setLocationDebut:MyRange7.location];
        [maBalise setLongueurDebut:MyRange7.length];
        [maBalise setLocationFin:0];
        [maBalise setLongueurFin:0];
        [liste7 addObject:maBalise];
        [maBalise release];
    }
    else
    {
        ok = NO;
        //position = 0;
    }        
    
    while (ok)
    {       
        i++;        
        
        MyRange7 =[buffer rangeOfString:myString7 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange7.location+MyRange7.length,[buffer length]-MyRange7.location-MyRange7.length)];
        
        if (MyRange7.location != NSNotFound )
        {
            ok = YES;
            position = MyRange7.location + MyRange7.length;
            
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString7];
            [maBalise setLocationDebut:MyRange7.location];
            [maBalise setLongueurDebut:MyRange7.length];
            [maBalise setLocationFin:0];
            [maBalise setLongueurFin:0];
            [liste7 addObject:maBalise];
            [maBalise release];
            
            
            position = MyRange7.location + MyRange7.length;
        }
        else
        {
            ok = NO;
            
        }  
        
    }   
    
    ///////////////////
    NSString * myString8 = [[NSString alloc] initWithFormat:@"%@",@"</sp:conclu>"];
    NSRange MyRange8 =[buffer rangeOfString:myString8 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste8 = [[NSMutableArray alloc] init]; 
    if (MyRange8.location != NSNotFound )
    {
        ok = YES;
        position = MyRange8.location + MyRange8.length;
        
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString7];
        [maBalise setLocationFin:MyRange8.location];
        [maBalise setLongueurFin:MyRange8.length];
        [maBalise setLocationDebut:0];
        [maBalise setLongueurDebut:0];
        
        [liste8 addObject:maBalise];
        [maBalise release];
        
        
    }
    else
    {
        ok = NO;
    }        
    
    while (ok)
    {       
        i++;        
        
        MyRange8 =[buffer rangeOfString:myString8 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange8.location+MyRange8.length,[buffer length]-MyRange8.location-MyRange8.length)];
        
        if (MyRange8.location != NSNotFound )
        {
            ok = YES;
            position = MyRange8.location + MyRange8.length;
            
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString7];
            [maBalise setLocationFin:MyRange8.location];
            [maBalise setLongueurFin:MyRange8.length];
            [maBalise setLocationDebut:0];
            [maBalise setLongueurDebut:0];
            
            [liste8 addObject:maBalise];
            [maBalise release];
            
            
        }
        else
        {
            ok = NO;
            
        }  
        
    }   
    
    /////////////
    
    ///////////////////
    NSString * myString3 = [[NSString alloc] initWithFormat:@"%@",@"<sp:courseUc>"];
    NSRange MyRange3 =[buffer rangeOfString:myString3 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste3 = [[NSMutableArray alloc] init]; 
    if (MyRange3.location != NSNotFound )
    {
        ok = YES;
        position = MyRange3.location + MyRange3.length;
        
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString3];
        [maBalise setLocationDebut:MyRange3.location];
        [maBalise setLongueurDebut:MyRange3.length];
        [maBalise setLocationFin:0];
        [maBalise setLongueurFin:0];
        [liste3 addObject:maBalise];
        [maBalise release];
        
    }
    else
    {
        ok = NO;
    }        
    
    while (ok)
    {       
        i++;        
        
        MyRange3 =[buffer rangeOfString:myString3 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange3.location+MyRange3.length,[buffer length]-MyRange3.location-MyRange3.length)];
        
        if (MyRange3.location != NSNotFound )
        {
            ok = YES;
            position = MyRange3.location + MyRange3.length;
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString3];
            [maBalise setLocationDebut:MyRange3.location];
            [maBalise setLongueurDebut:MyRange3.length];
            [maBalise setLocationFin:0];
            [maBalise setLongueurFin:0];
            [liste3 addObject:maBalise];
            [maBalise release];
        }
        else
        {
            ok = NO;
        }  
        
    }   
    
    /////////////
    
    ///////////////////
    NSString * myString4 = [[NSString alloc] initWithFormat:@"%@",@"</sp:courseUc>"];
    NSRange MyRange4 =[buffer rangeOfString:myString4 options:NSCaseInsensitiveSearch];
    NSMutableArray * liste4 = [[NSMutableArray alloc] init]; 
    if (MyRange4.location != NSNotFound )
    {
        ok = YES;
        position = MyRange4.location + MyRange4.length;
        Balise * maBalise = [[Balise alloc] init];
        [maBalise setTexte:@""];
        [maBalise setType:myString3];
        [maBalise setLocationFin:MyRange4.location];
        [maBalise setLongueurFin:MyRange4.length];
        [maBalise setLocationDebut:0];
        [maBalise setLongueurDebut:0];
        [liste4 addObject:maBalise];
        [maBalise release];
    }
    else
    {
        ok = NO;
    }        
    
    while (ok)
    {       
        i++;        
        MyRange4 =[buffer rangeOfString:myString4 options:NSCaseInsensitiveSearch range:NSMakeRange(MyRange4.location+MyRange4.length,[buffer length]-MyRange4.location-MyRange4.length)];
        
        if (MyRange4.location != NSNotFound )
        {
            ok = YES;
            position = MyRange4.location + MyRange4.length;
            
            Balise * maBalise = [[Balise alloc] init];
            [maBalise setTexte:@""];
            [maBalise setType:myString3];
            [maBalise setLocationFin:MyRange4.location];
            [maBalise setLongueurFin:MyRange4.length];
            [maBalise setLocationDebut:0];
            [maBalise setLongueurDebut:0];
            [liste4 addObject:maBalise];
            [maBalise release];
            
        }
        else
        {
            ok = NO;
            
        }  
        
    }   
    
    
    
    
    
    NSMutableArray * liste5 = [[NSMutableArray alloc] init]; 
    NSMutableArray * liste6 = [[NSMutableArray alloc] init]; 
    
    int k = 0;
    int j = 0;
    ok = YES;
    
    Balise * a1 = [[Balise alloc] init];
    Balise * b1 = [[Balise alloc] init];
    Balise * c1 = [[Balise alloc] init];
    
    Balise * a3 = [[Balise alloc] init];
    Balise * b3 = [[Balise alloc] init];
    Balise * c3 = [[Balise alloc] init];
    Balise * d3 = [[Balise alloc] init];
    
    Balise * a2 = [[Balise alloc] init];
    Balise * c2 = [[Balise alloc] init];
    
    while (ok)
    {
        a1 = [liste1 objectAtIndex:k];
        b1 = [liste1 objectAtIndex:k+1];
        c1 = [liste2 objectAtIndex:j];
        /*cas1 
         <div>   a
         </div>  c
         <div>   b 
         */ 
        /*cas2 
         <div>   a
         <div>   b
         </div>  c
         */
        
        if( [c1 locationFin] < [b1 locationDebut])
        {  /* cas 1 */
            // on stocke a avec c
            
            Balise * x = [[Balise alloc] init];                           
            [x setTexte:[a1 texte]];
            [x setType:[a1 type]];
            [x setLocationDebut:[a1 locationDebut]];
            [x setLongueurDebut:[a1 longueurDebut]];
            [x setLocationFin:[c1 locationFin]];
            [x setLongueurFin:[c1 longueurFin]];
            [liste5 addObject:x];
            [x release];
            
            [liste1 removeObjectAtIndex:k];
            [liste2 removeObjectAtIndex:0];
            
            k = 0;
            j = 0;
        }
        else
        {
            k++;
            j=0;
        }
        
        if( ([liste1 count] <= k+1) || ([liste2 count] <= j+1) )
            
        {
            
            ok = NO; 
            
            if ([liste1 count] == 1)
            {
                // il reste une Balise
                
                a2 = [liste1 objectAtIndex:0];
                c2 = [liste2 objectAtIndex:0];
                
                
                Balise * x = [[Balise alloc] init];                           
                [x setTexte:[a2 texte]];
                [x setType:[a2 type]];
                [x setLocationDebut:[a2 locationDebut]];
                [x setLongueurDebut:[a2 longueurDebut]];
                [x setLocationFin:[c2 locationFin]];
                [x setLongueurFin:[c2 longueurFin]];
                [liste5 addObject:x];
                [x release];
                
                [liste1 removeObjectAtIndex:0];
                [liste2 removeObjectAtIndex:0];
            }
            else
            {
                
                
                a3 = [liste1 objectAtIndex:0];
                b3 = [liste1 objectAtIndex:1];
                c3 = [liste2 objectAtIndex:0];
                d3 = [liste2 objectAtIndex:1];
                
                if( [c3 locationFin] < [b3 locationDebut])
                {  /* cas 1 */
                    // on stocke a avec c
                    
                    
                    Balise * x = [[Balise alloc] init];                           
                    [x setTexte:[a3 texte]];
                    [x setType:[a3 type]];
                    [x setLocationDebut:[a3 locationDebut]];
                    [x setLongueurDebut:[a3 longueurDebut]];
                    [x setLocationFin:[c3 locationFin]];
                    [x setLongueurFin:[c3 longueurFin]];
                    [liste5 addObject:x];
                    [x release];
                    
                    Balise * y = [[Balise alloc] init];                           
                    [y setTexte:[b3 texte]];
                    [y setType:[b3 type]];
                    [y setLocationDebut:[b3 locationDebut]];
                    [y setLongueurDebut:[b3 longueurDebut]];
                    [y setLocationFin:[d3 locationFin]];
                    [y setLongueurFin:[d3 longueurFin]];
                    [liste5 addObject:y];
                    [y release];
                    
                }
                else
                {
                    
                    Balise * x = [[Balise alloc] init];                           
                    [x setTexte:[a3 texte]];
                    [x setType:[a3 type]];
                    [x setLocationDebut:[a3 locationDebut]];
                    [x setLongueurDebut:[a3 longueurDebut]];
                    [x setLocationFin:[d3 locationFin]];
                    [x setLongueurFin:[d3 longueurFin]];
                    [liste5 addObject:x];
                    [x release];
                    
                    Balise * y = [[Balise alloc] init];                           
                    [y setTexte:[b3 texte]];
                    [y setType:[b3 type]];
                    [y setLocationDebut:[b3 locationDebut]];
                    [y setLongueurDebut:[b3 longueurDebut]];
                    [y setLocationFin:[c3 locationFin]];
                    [y setLongueurFin:[c3 longueurFin]];
                    [liste5 addObject:y];
                    [y release];
                    
                    
                    
                    /*                    
                     [liste5 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                     [a objectForKey:@"location"],@"locationDebut",
                     [a objectForKey:@"longueur"],@"longueurDebut",
                     [d objectForKey:@"location"],@"locationFin",
                     [d objectForKey:@"longueur"],@"longueurFin",nil]];
                     
                     [liste5 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                     [b objectForKey:@"location"],@"locationDebut",
                     [b objectForKey:@"longueur"],@"longueurDebut",
                     [c objectForKey:@"location"],@"locationFin",
                     [c objectForKey:@"longueur"],@"longueurFin",nil]];
                     */                  
                }    
                
                [liste1 removeObjectAtIndex:0];
                [liste2 removeObjectAtIndex:0];
                [liste1 removeObjectAtIndex:0];
                [liste2 removeObjectAtIndex:0];
                
            }
            
            
        }
        
    }
    DLog(@"liste5 is %lu",[liste5 count]);
    
    
    k = 0;
    j = 0;
    ok = YES;
    
    while (ok)
    {
        
        
        Balise * a11 = [[Balise alloc] init];
        Balise * b11 = [[Balise alloc] init];
        Balise * c11 = [[Balise alloc] init];
        
        DLog(@"retainCount is %lu",[a11 retainCount]);
        
        a11 = [liste3 objectAtIndex:k];
        b11 = [liste3 objectAtIndex:k+1];
        c11 = [liste4 objectAtIndex:j];
        /*cas1 
         <div>   a
         </div>  c
         <div>   b 
         */ 
        /*cas2 
         <div>   a
         <div>   b
         </div>  c
         */
        
        if( [c11 locationFin]< [b11 locationDebut])
        {  /* cas 1 */
            // on stocke a avec c
            
            
            Balise * x = [[Balise alloc] init];                           
            [x setTexte:[a11 texte]];
            [x setType:[a11 type]];
            [x setLocationDebut:[a11 locationDebut]];
            [x setLongueurDebut:[a11 longueurDebut]];
            [x setLocationFin:[c11 locationFin]];
            [x setLongueurFin:[c11 longueurFin]];
            [liste6 addObject:x];
            [x release];
            
            /*
             [liste6 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
             [a objectForKey:@"location"],@"locationDebut",
             [a objectForKey:@"longueur"],@"longueurDebut",
             [c objectForKey:@"location"],@"locationFin",
             [c objectForKey:@"longueur"],@"longueurFin",nil]];
             */
            [liste3 removeObjectAtIndex:k];
            [liste4 removeObjectAtIndex:0];
            
            k = 0;
            j = 0;
        }
        else
        {
            k++;
            j=0;
        }
        
        //[a11 release];
        //[b11 release];
        //[c11 release];
        
        DLog(@"%lu, %d ",[liste3 count],k);
        DLog(@"%lu, %d ",[liste4 count],j);
        if( ([liste3 count] <= k+1) || ([liste4 count] <= j+1) )
            
        {
            ok = NO; 
            if ([liste3 count] == 1)
            {
                Balise * a21 = [[Balise alloc] init];
                Balise * c21 = [[Balise alloc] init];
                
                a21 = [liste3 objectAtIndex:0];
                c21 = [liste4 objectAtIndex:0];
                
                
                Balise * x = [[Balise alloc] init];                           
                [x setTexte:[a21 texte]];
                [x setType:[a21 type]];
                [x setLocationDebut:[a21 locationDebut]];
                [x setLongueurDebut:[a21 longueurDebut]];
                [x setLocationFin:[c21 locationFin]];
                [x setLongueurFin:[c21 longueurFin]];
                [liste6 addObject:x];
                [x release];
                
                [liste3 removeObjectAtIndex:0];
                [liste4 removeObjectAtIndex:0];
                
                //[a21 release];
                //[c21 release];
                
                /*
                 [liste6 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                 [a objectForKey:@"location"],@"locationDebut",
                 [a objectForKey:@"longueur"],@"longueurDebut",
                 [c objectForKey:@"location"],@"locationFin",
                 [c objectForKey:@"longueur"],@"longueurFin",nil]];
                 */                
            }
            else
            {
                
                Balise * a31 = [[Balise alloc] init];
                Balise * b31 = [[Balise alloc] init];
                Balise * c31 = [[Balise alloc] init];
                Balise * d31 = [[Balise alloc] init];
                
                a31 = [liste3 objectAtIndex:0];
                b31 = [liste3 objectAtIndex:1];
                c31 = [liste4 objectAtIndex:0];
                d31 = [liste4 objectAtIndex:1];
                
                
                if( [c31 locationFin] < [b31 locationDebut])
                {  /* cas 1 */
                    
                    // on stocke a avec c
                    
                    Balise * x = [[Balise alloc] init];                           
                    [x setTexte:[a31 texte]];
                    [x setType:[a31 type]];
                    [x setLocationDebut:[a31 locationDebut]];
                    [x setLongueurDebut:[a31 longueurDebut]];
                    [x setLocationFin:[c31 locationFin]];
                    [x setLongueurFin:[c31 longueurFin]];
                    [liste6 addObject:x];
                    [x release];
                    
                    
                    Balise * y = [[Balise alloc] init];                           
                    [y setTexte:[b31 texte]];
                    [y setType:[b31 type]];
                    [y setLocationDebut:[b31 locationDebut]];
                    [y setLongueurDebut:[b31 longueurDebut]];
                    [y setLocationFin:[d31 locationFin]];
                    [y setLongueurFin:[d31 longueurFin]];
                    [liste6 addObject:y];
                    [y release];
                    /*
                     [liste6 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                     [a objectForKey:@"location"],@"locationDebut",
                     [a objectForKey:@"longueur"],@"longueurDebut",
                     [c objectForKey:@"location"],@"locationFin",
                     [c objectForKey:@"longueur"],@"longueurFin",nil]];
                     
                     [liste6 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                     [b objectForKey:@"location"],@"locationDebut",
                     [b objectForKey:@"longueur"],@"longueurDebut",
                     [d objectForKey:@"location"],@"locationFin",
                     [d objectForKey:@"longueur"],@"longueurFin",nil]];
                     */
                    
                }
                else
                {
                    Balise * x = [[Balise alloc] init];                           
                    [x setTexte:[a31 texte]];
                    [x setType:[a31 type]];
                    [x setLocationDebut:[a31 locationDebut]];
                    [x setLongueurDebut:[a31 longueurDebut]];
                    [x setLocationFin:[d31 locationFin]];
                    [x setLongueurFin:[d31 longueurFin]];
                    [liste6 addObject:x];
                    [x release];
                    
                    
                    Balise * y = [[Balise alloc] init];                           
                    [y setTexte:[b31 texte]];
                    [y setType:[b31 type]];
                    [y setLocationDebut:[b31 locationDebut]];
                    [y setLongueurDebut:[b31 longueurDebut]];
                    [y setLocationFin:[c31 locationFin]];
                    [y setLongueurFin:[c31 longueurFin]];
                    [liste6 addObject:y];
                    [y release];
                    /*
                     [liste6 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                     [a objectForKey:@"location"],@"locationDebut",
                     [a objectForKey:@"longueur"],@"longueurDebut",
                     [d objectForKey:@"location"],@"locationFin",
                     [d objectForKey:@"longueur"],@"longueurFin",nil]];
                     
                     [liste6 addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                     [b objectForKey:@"location"],@"locationDebut",
                     [b objectForKey:@"longueur"],@"longueurDebut",
                     [c objectForKey:@"location"],@"locationFin",
                     [c objectForKey:@"longueur"],@"longueurFin",nil]];
                     */
                }
                
                [liste3 removeObjectAtIndex:0];
                [liste4 removeObjectAtIndex:0];
                [liste3 removeObjectAtIndex:0];
                [liste4 removeObjectAtIndex:0];
                
                //[a31 release];
                //[b31 release];
                //[c31 release];
                //[d31 release];
                
                
            }    
            
        }
    }
    
    Balise * myBalise1 = [[Balise alloc] init];                           
    Balise * myBalise2 = [[Balise alloc] init];                           
    
    if ([liste7 count]== 1)
    {
        myBalise1 = [liste7 objectAtIndex:0];
        myBalise2 = [liste8 objectAtIndex:0];
        Balise * monX = [[Balise alloc] init];                           
        [monX setTexte:[myBalise1 texte]];
        [monX setType:[myBalise1 type]];
        [monX setLocationDebut:[myBalise1 locationDebut]];
        [monX setLongueurDebut:[myBalise1 longueurDebut]];
        [monX setLocationFin:[myBalise2 locationFin]];
        [monX setLongueurFin:[myBalise2 longueurFin]];
        [liste7 replaceObjectAtIndex:0 withObject:monX];
        //   [maBalise1 release];
        //   [maBalise2 release];
        //   [x release];
        
    }
    
    [liste5 addObjectsFromArray:(NSArray *)liste6];
    [liste5 addObjectsFromArray:(NSArray *)liste7];
    
    NSSortDescriptor * debutDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"locationDebut"
                                                                      ascending:YES] autorelease];
    NSArray* sortDescriptors = [NSArray arrayWithObject:debutDescriptor];
    
    NSMutableArray * sortArray = [[NSMutableArray alloc] initWithArray:[liste5 sortedArrayUsingDescriptors:sortDescriptors]];
    
    for (int i = 0;i<[sortArray count];i++)
    {
        Balise * maBalis2  = [[Balise alloc] init];
        maBalis2 = [sortArray objectAtIndex:i];
        [maBalis2 setIndiceBalise:i];
        [maBalis2 setIndiceFichier:0];
        [sortArray replaceObjectAtIndex:i withObject:maBalis2];
    }
    
    DLog(@"count is %lu",[sortArray count]);
    
    
    //////////
    Balise * maBalise1 = [[Balise alloc] init];
    Balise * maBalise2 = [[Balise alloc] init];
    NSMutableArray * array = [[NSMutableArray alloc] init]; 
    ok = YES;
    int indice = 0;
    NSUInteger indiceFicier = 0;
    maBalise1 = [sortArray objectAtIndex:indice];
    [maBalise1 setIndiceFichier:indiceFicier];
    [array addObject:maBalise1];
    [sortArray removeObjectAtIndex:indice];
    int locationDebut1;
    int locationFin1;
    int locationDebut2;
    int locationFin2;
    while (ok)
    {
        
        maBalise1      = [array lastObject];
        locationDebut1 = [maBalise1 locationDebut];
        locationFin1 = [maBalise1 locationFin];
        
        maBalise2      = [sortArray objectAtIndex:indice];
        locationDebut2 = [maBalise2 locationDebut];
        locationFin2   = [maBalise2 locationFin];
        if (    (locationDebut1 < locationDebut2)
            && (locationFin1   < locationFin2  ) )
        {
            [maBalise2 setIndiceFichier:++indiceFicier];
            [array addObject:maBalise2];
            [sortArray removeObjectAtIndex:indice];
            indice = 0;
        }
        else
        {
            indice++;            
        }
        if ([sortArray count]-1 == indice) 
        {
            if ([sortArray count] != 0 ) {
                indice = 0;                
                maBalise1 = [sortArray objectAtIndex:indice];
                [maBalise1 setIndiceFichier:++indiceFicier];
                [array addObject:maBalise1];
                [sortArray removeObjectAtIndex:indice];
            }
        }        
        if ([sortArray count] == 0) 
        {
            ok = NO;
        }
    }     
    
    for (int i=0;i<[array count];i++)
    {
        DLog(@"%lu,%lu",[(Balise *)[array objectAtIndex:i] indiceFichier],[(Balise *)[array objectAtIndex:i] indiceBalise]);
    }
    //////////
    
    /////
    
    Balise * maBalise = [[Balise alloc] init];
    
    for (int i=0;i<[array count];i++)
    {
        maBalise = [array objectAtIndex:i];
        int loc = [maBalise locationDebut] + [maBalise longueurDebut];
        int len = [maBalise locationFin] - [maBalise locationDebut] - [maBalise longueurDebut];
        
        NSString * toto = [buffer substringWithRange:NSMakeRange(loc,len)];
        
        NSString * titi = [[NSString alloc] initWithFormat:
                           @"%@%@%@%@%@%@",
                           @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                           @"<sc:item xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
                           @"<op:ue xmlns:op=\"utc.fr:ics/opale3\" xmlns:sp=\"http://www.utc.fr/ics/scenari/v3/primitive\" xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
                           toto,                            
                           @"</op:ue>",
                           @"</sc:item>"];
        DLog(@"titi is %@",titi);
        
        [maBalise setTexte:titi];
        
        [array replaceObjectAtIndex:i  withObject:maBalise];
        
        NSData *parseData = [titi dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString * pathFichier = [[NSString alloc] initWithFormat:@"%@/%@_%d%@",[nomFichier stringByDeletingLastPathComponent],[[nomFichier lastPathComponent] stringByDeletingPathExtension],i+1,@".xml"]; 
        
        [parseData writeToFile:pathFichier atomically:YES];
        
    }
    
    /////
    
    
    return array;
    
}
-(NSArray *)eclate1:(NSString *)nomFichier fichierDeBase:(NSString *)fichierDeBase
{
    DLog(@"XML is %@",[nomFichier lastPathComponent]);
	
	self.monFichierDeBase =[[NSMutableString alloc] init];
	[self.monFichierDeBase setString:fichierDeBase];

    indiceFichier = 0;
    
    NSString *error1; 
    NSPropertyListFormat format;
    
    NSString * fichierData = [self.path stringByAppendingPathComponent:@"fichierData.plist"];                 
    if (![[NSFileManager defaultManager] fileExistsAtPath:fichierData])
    {
        [[[NSArray alloc] initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:indiceFichier],@"indiceFichier",nil],nil] writeToFile:fichierData atomically:YES];
    }
    
    NSArray * arrayContenuFichier = [[NSArray alloc] initWithArray:[NSPropertyListSerialization propertyListFromData:[[NSData alloc] initWithContentsOfFile:fichierData]                                                                                                    mutabilityOption:NSPropertyListImmutable                                                                                                              format:&format errorDescription:&error1]];
    if (arrayContenuFichier)
    {    
        NSDictionary * dicoContenuFichier = [[NSDictionary alloc] initWithDictionary:[arrayContenuFichier objectAtIndex:0]];
        NSNumber * lastIndice = [dicoContenuFichier valueForKey:@"indiceFichier"];
        indiceFichier = [lastIndice integerValue];
    }  
    else
        indiceFichier = 0;
    
    
	topBalisespex = NO;
    NSMutableArray * listeFichiers = [[NSMutableArray alloc] init];
    
    NSError *error;
    
    NSString *stringFromFileAtPath = [[NSString alloc]
                                      initWithContentsOfFile:nomFichier
                                      encoding:NSUTF8StringEncoding
                                      error:&error];
    
    if (stringFromFileAtPath == nil) 
    {
        
        // an error occurred
        
        NSLog(@"Error reading file at %@\n%@",
              
              nomFichier, [error localizedFailureReason]);
        
        // implementation continues ...
        
    } 
    /////////////
    
    
    NSMutableString * buffer = [[NSMutableString alloc] initWithString:stringFromFileAtPath]; 
    NSRange MyRangeDebut1 = [buffer rangeOfString:@"<sp:div>"       options:NSCaseInsensitiveSearch];
    NSRange MyRangeFin1   = NSMakeRange(0,0);
    NSRange MyRangeDebut2 = [buffer rangeOfString:@"<sp:courseUc>"  options:NSCaseInsensitiveSearch];
    NSRange MyRangeFin2   = NSMakeRange(0,0);
    NSRange MyRangeDebut3 = [buffer rangeOfString:@"<sp:conclu>"    options:NSCaseInsensitiveSearch];
    NSRange MyRangeFin3   = NSMakeRange(0,0);
    NSRange MyRangeDebut4 = [buffer rangeOfString:@"<sp:intro>"    options:NSCaseInsensitiveSearch];
    NSRange MyRangeFin4   = NSMakeRange(0,0);
    NSRange MyRangeDebut5 = [buffer rangeOfString:@"sp:courseUa>"    options:NSCaseInsensitiveSearch];
    NSRange MyRangeFin5   = NSMakeRange(0,0);
    
    BOOL ok = NO;
    NSUInteger debut = 0;
    int t= 0;
    
    if (MyRangeDebut1.location != NSNotFound )
    {
        debut = MyRangeDebut1.location;
        t = 1;
        ok = YES;
    }
    if (MyRangeDebut2.location != NSNotFound )
    {
        if ((MyRangeDebut2.location < debut) || (debut == 0))
        {    
            debut = MyRangeDebut1.location;
            t= 2; 
            ok = YES;
        }
    }
    if (MyRangeDebut3.location != NSNotFound )
    {
        if ((MyRangeDebut3.location < debut) || (debut == 0))
        {    
            debut = MyRangeDebut3.location;
            t= 3;
            ok = YES;
        }
    }
    if (MyRangeDebut4.location != NSNotFound )
    {
        if ((MyRangeDebut4.location < debut) || (debut == 0))
        {    
            debut = MyRangeDebut4.location;
            t= 4;
            ok = YES;
        }
    }
    
    if (MyRangeDebut5.location != NSNotFound )
    {
        if ((MyRangeDebut5.location < debut) || (debut == 0))
        {    
            debut = MyRangeDebut5.location;
            t= 5;
            ok = YES;
        }
    }
    
    NSUInteger loc;
    NSUInteger len;
    NSString * toto;
    NSString * toto1;
    NSString * toto2;
    
    BOOL top = NO;
    
    while (ok)
    {    
        top = YES;
        
        
        DLog(@"location is %lu,%lu,%lu,%lu,%lu,%lu,%d",MyRangeDebut1.location,MyRangeFin1.location,MyRangeDebut2.location,MyRangeFin2.location,MyRangeDebut3.location,MyRangeFin3.location,t    );
        
        NSString * titi; 
        switch (t) {
            case 1:
                loc = MyRangeDebut1.location+ MyRangeDebut1.length;
                MyRangeFin1 = [self rechercheFin:buffer rangeOfStringDebut:@"<sp:div>" rangeOfStringFin:@"</sp:div>" debut:MyRangeDebut1];
                
                len = MyRangeFin1.location - MyRangeDebut1.location - MyRangeDebut1.length;
                toto = [buffer substringWithRange:NSMakeRange(loc,len)];
                
                toto1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebut1.location)];
                toto2 = [buffer substringWithRange:NSMakeRange(MyRangeFin1.location+MyRangeFin1.length,[buffer length]-MyRangeFin1.location-MyRangeFin1.length )];
                
                titi = [[NSString alloc] initWithFormat:
                        @"%@%@%@%@%@%@",
                        @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                        @"<sc:item xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
                        @"<op:expUc xmlns:op=\"utc.fr:ics/opale3\" xmlns:sp=\"http://www.utc.fr/ics/scenari/v3/primitive\">",
                        toto, 
                        @"</op:expUc>",
                        @"</sc:item>"];
                break;
            case 2:
                loc = MyRangeDebut2.location+ MyRangeDebut2.length;
                
                MyRangeFin2 = [self rechercheFin:buffer rangeOfStringDebut:@"<sp:courseUc>" rangeOfStringFin:@"</sp:courseUc>"  debut:MyRangeDebut2];
                
                len = MyRangeFin2.location - MyRangeDebut2.location - MyRangeDebut2.length;
                toto = [buffer substringWithRange:NSMakeRange(loc,len)];
                
                toto1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebut2.location)];
                toto2 = [buffer substringWithRange:NSMakeRange(MyRangeFin2.location+MyRangeFin2.length,[buffer length]-MyRangeFin2.location-MyRangeFin2.length )];
                titi = [[NSString alloc] initWithFormat:
                        @"%@%@%@%@%@%@",
                        @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                        @"<sc:item xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
                        @"<op:expUc xmlns:op=\"utc.fr:ics/opale3\" xmlns:sp=\"http://www.utc.fr/ics/scenari/v3/primitive\">",
                        toto, 
                        @"</op:expUc>",
                        @"</sc:item>"];
                break;
            case 3:
                loc = MyRangeDebut3.location+MyRangeDebut3.length;
                
                MyRangeFin3 = [self rechercheFin:buffer rangeOfStringDebut:@"<sp:conclu>" rangeOfStringFin:@"</sp:conclu>"  debut:MyRangeDebut3];
                
                len = MyRangeFin3.location - MyRangeDebut3.length - MyRangeDebut3.location;
                toto = [buffer substringWithRange:NSMakeRange(loc,len)];
                
                toto1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebut3.location)];
                toto2 = [buffer substringWithRange:NSMakeRange(MyRangeFin3.location+MyRangeFin3.length,[buffer length]-MyRangeFin3.location-MyRangeFin3.length )];
                titi = [[NSString alloc] initWithFormat:
                        @"%@%@%@%@%@%@%@%@%@%@",
                        @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                        @"<sc:item xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
                        @"<op:expUc xmlns:op=\"utc.fr:ics/opale3\" xmlns:sp=\"http://www.utc.fr/ics/scenari/v3/primitive\">",                      
                        @"<op:ueDiv>",                      
                        @"<op:ueDivM>",                      
                        @"<sp:title>Conclusion</sp:title>",                      
                        @"</op:ueDivM>",                      
                        toto, 
                        @"</op:ueDiv>",
                        @"</op:expUc>",
                        @"</sc:item>"];
                break;
            case 4:
                loc = MyRangeDebut4.location+MyRangeDebut4.length;
                
                MyRangeFin4 = [self rechercheFin:buffer rangeOfStringDebut:@"<sp:intro>" rangeOfStringFin:@"</sp:intro>"  debut:MyRangeDebut4];
                
                len = MyRangeFin4.location - MyRangeDebut4.length - MyRangeDebut4.location;
                toto = [buffer substringWithRange:NSMakeRange(loc,len)];
                
                toto1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebut4.location)];
                toto2 = [buffer substringWithRange:NSMakeRange(MyRangeFin4.location+MyRangeFin4.length,[buffer length]-MyRangeFin4.location-MyRangeFin4.length )];
                titi = [[NSString alloc] initWithFormat:
                        @"%@%@%@%@%@%@%@%@%@%@",
                        @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                        @"<sc:item xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
                        @"<op:expUc xmlns:op=\"utc.fr:ics/opale3\" xmlns:sp=\"http://www.utc.fr/ics/scenari/v3/primitive\">",                      
                        @"<op:ueDiv>",                      
                        @"<op:ueDivM>",                      
                        @"<sp:title>Introduction</sp:title>",                      
                        @"</op:ueDivM>",                      
                        toto, 
                        @"</op:ueDiv>",
                        @"</op:expUc>",
                        @"</sc:item>"];
                break;
            case 5:
                loc = MyRangeDebut5.location+MyRangeDebut5.length;
                
                MyRangeFin5 = [self rechercheFin:buffer rangeOfStringDebut:@"<sp:courseUa>" rangeOfStringFin:@"</sp:courseUa>"  debut:MyRangeDebut5];
                
                len = MyRangeFin5.location - MyRangeDebut5.length - MyRangeDebut5.location;
                toto = [buffer substringWithRange:NSMakeRange(loc,len)];
                
                toto1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebut5.location)];
                toto2 = [buffer substringWithRange:NSMakeRange(MyRangeFin5.location+MyRangeFin5.length,[buffer length]-MyRangeFin5.location-MyRangeFin5.length )];
                titi = [[NSString alloc] initWithFormat:
                        @"%@%@%@%@%@%@",
                        @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                        @"<sc:item xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
                        @"<op:expUc xmlns:op=\"utc.fr:ics/opale3\" xmlns:sp=\"http://www.utc.fr/ics/scenari/v3/primitive\">",
                        toto, 
                        @"</op:expUc>",
                        @"</sc:item>"];
                break;
            default:
                break;
        }
        
        
        
        
        
        /*
         NSString * titi = [[NSString alloc] initWithFormat:
         @"%@%@%@%@",
         @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
         @"<sc:item xmlns:sc=\"http://www.utc.fr/ics/scenari/v3/core\">",
         toto, 
         @"</sc:item>"];
         
         */      
        
        
        
        
        NSData *parseData = [titi dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString * pathFichier = [[NSString alloc] initWithFormat:@"%@/%@_%@%@",[nomFichier stringByDeletingLastPathComponent],self.monFichierDeBase,[self.arrayIndex objectAtIndex:indiceFichier],@".xml"]; 
        
        [listeFichiers addObject:pathFichier]; 
        NSString * pathFichierPourXXXX  = [[NSString alloc] initWithFormat:@"/%@/%@_%@%@",[[nomFichier stringByDeletingLastPathComponent] lastPathComponent],self.monFichierDeBase,[self.arrayIndex objectAtIndex:indiceFichier],@".xml"]; 
        
        [parseData writeToFile:pathFichier atomically:YES];
        
        
        [buffer setString:[[NSString alloc] initWithFormat:
                           @"%@%@%@%@%@",
                           toto1,
                           @"<sp:courseUc sc:refUri=\"",pathFichierPourXXXX,@"\"/>",
                           toto2 ]];
        
        MyRangeDebut1.location = NSNotFound;
        MyRangeFin1.location   = NSNotFound;
        MyRangeDebut2.location = NSNotFound;
        MyRangeFin2.location   = NSNotFound;
        MyRangeDebut3.location = NSNotFound;
        MyRangeFin3.location   = NSNotFound;
        MyRangeDebut4.location = NSNotFound;
        MyRangeFin4.location   = NSNotFound;
        MyRangeDebut5.location = NSNotFound;
        MyRangeFin5.location   = NSNotFound;
        
        MyRangeDebut1 = [buffer rangeOfString:@"<sp:div>"        options:NSCaseInsensitiveSearch];
        MyRangeFin1   = [buffer rangeOfString:@"</sp:div>"       options:NSCaseInsensitiveSearch];
        MyRangeDebut2 = [buffer rangeOfString:@"<sp:courseUc>"   options:NSCaseInsensitiveSearch];
        MyRangeFin2   = [buffer rangeOfString:@"</sp:courseUc>"  options:NSCaseInsensitiveSearch];
        MyRangeDebut3 = [buffer rangeOfString:@"<sp:conclu>"     options:NSCaseInsensitiveSearch];
        MyRangeFin3   = [buffer rangeOfString:@"</sp:conclu>"    options:NSCaseInsensitiveSearch];
        MyRangeDebut4 = [buffer rangeOfString:@"<sp:intro>"      options:NSCaseInsensitiveSearch];
        MyRangeFin4   = [buffer rangeOfString:@"</sp:intro>"     options:NSCaseInsensitiveSearch];
        MyRangeDebut5 = [buffer rangeOfString:@"<sp:courseUa>"   options:NSCaseInsensitiveSearch];
        MyRangeFin5   = [buffer rangeOfString:@"</sp:courseUa>"  options:NSCaseInsensitiveSearch];
        
        ok = NO;
        debut = 0;
        t= 0;
        
        if (MyRangeDebut1.location != NSNotFound )
        {
            debut = MyRangeDebut1.location;
            t = 1;
            ok = YES;
        }
        if (MyRangeDebut2.location != NSNotFound )
        {
            if ((MyRangeDebut2.location < debut) || (debut == 0))
            {    
                debut = MyRangeDebut2.location;
                t= 2; 
                ok = YES;
            }
        }
        if (MyRangeDebut3.location != NSNotFound )
        {
            if ((MyRangeDebut3.location < debut) || (debut == 0))
            {    
                debut = MyRangeDebut3.location;
                t= 3;
                ok = YES;
            }
        }
        
        if (MyRangeDebut4.location != NSNotFound )
        {
            if ((MyRangeDebut4.location < debut) || (debut == 0))
            {    
                debut = MyRangeDebut4.location;
                t= 4;
                ok = YES;
            }
        }
        
        if (MyRangeDebut5.location != NSNotFound )
        {
            if ((MyRangeDebut5.location < debut) || (debut == 0))
            {    
                debut = MyRangeDebut5.location;
                t= 5;
                ok = YES;
            }
        }
        
        indiceFichier++;        
        [[[NSArray alloc] initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:indiceFichier],@"indiceFichier",nil],nil] writeToFile:fichierData atomically:YES];
        
    }   

 if (top)
    {
        
        NSData *parseData = [buffer dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString * pathFichier2 = [[NSString alloc] initWithFormat:@"%@/%@_%@%@",[nomFichier stringByDeletingLastPathComponent],[[nomFichier lastPathComponent] stringByDeletingPathExtension],@"XXXXXX",@".xml"]; 
        
        [parseData writeToFile:pathFichier2 atomically:YES];
        //[listeFichiers addObject:pathFichier2];
                
        [[[NSArray alloc] initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:indiceFichier],@"indiceFichier",nil],nil] writeToFile:fichierData atomically:YES];
        
        
//        return pathFichier2;
    }
    /*else
    {
        return nil;
    }*/

    //////////////
    /*for (NSString * fichier in listeFichiers) 
    {
        [self eclate1:fichier fichierDeBase:fichierDeBase];
    }*/
    //////////////

    return listeFichiers;
 
 }


#pragma mark -
#pragma mark Parser Methods


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    DLog(@"");
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    DLog(@"");
    
    topBaliseopexpUc = NO;
    topBaliseopueM = NO;
    topBaliseopuM = NO;
    topBaliseopueDiv = NO;
    topBalisespex = NO;	
    topBalisescitemizedList = NO;
    topBaliseoppb = NO;
    topBaliseopres = NO;
    topBaliseoptxt = NO;
    topBaliseopue = NO;
    topBaliseopueDivM = NO;
    topBalisescinlineImg = NO;
    topBalisescinlineStyle = NO;
    topBalisescitem = NO;
    topBalisesclistItem = NO;
    topBalisescpara = NO;
    topBalisescrefUri = NO;
    topBalisesctextLeaf = NO;
    topBalisescuLink = NO;
    topBalisespcourseUa = NO; 
    topBalisespcourseUc = NO;
    topBalisespdiv = NO;
    topBalisespform = NO;
    topBalisespimg = NO;
    topBalisespintro = NO;
    topBalisespconclu = NO;
    topBalisesppractUc = NO;
    topBalisespsTitle = NO;
    topBalisesptitle = NO;
    topBalisesptxt = NO;
    topBalisespvid = NO;
    topBalisesptab = NO;
    topBaliseopcourseUa = NO; 
    topBalisespinfo = NO; 
    topBalisespodInstruct = NO; 
    isStartBaliseopexpUc = NO;
    isStartBaliseoppb = NO;
    isStartBaliseopres = NO;
    isStartBaliseoptxt = NO;
    isStartBaliseopue = NO;
    isStartBaliseopueDiv = NO;
    isStartBaliseopueDivM = NO;
    isStartBaliseopueM = NO;
    isStartBaliseopuM = NO;
    isStartBalisescinlineImg = NO;
    isStartBalisescinlineStyle = NO;
    isStartBalisescitem = NO;
    isStartBalisescitemizedList = NO;
    isStartBalisesclistItem = NO;
    isStartBalisescpara = NO;
    isStartBalisescrefUri = NO;
    isStartBalisesctextLeaf = NO;
    isStartBalisescuLink = NO;
    isStartBalisespcourseUa = NO; 
    isStartBalisespcourseUc = NO;
    isStartBalisespdiv = NO;
    isStartBalisespex = NO;
    isStartBalisespform = NO;
    isStartBalisespimg = NO;
    isStartBalisespintro = NO;
    isStartBalisespconclu = NO;
    isStartBalisesppractUc = NO;
    isStartBalisespsTitle = NO;
    isStartBalisesptitle = NO;
    isStartBalisesptxt = NO;
    isStartBalisespvid = NO;
    isStartBalisesptab = NO;
    isStartBaliseopcourseUa = NO; 
    isStartBalisespinfo = NO; 
    isStartBalisespodInstruct = NO; 
    isEndBaliseopexpUc = NO;
    isEndBaliseoppb = NO;
    isEndBaliseopres = NO;
    isEndBaliseoptxt = NO;
    isEndBaliseopue = NO;
    isEndBaliseopueDiv = NO;
    isEndBaliseopueDivM = NO;
    isEndBaliseopueM = NO;
    isEndBaliseopuM = NO;
    isEndBalisescinlineImg = NO;
    isEndBalisescinlineStyle = NO;
    isEndBalisescitem = NO;
    isEndBalisescitemizedList = NO;
    isEndBalisesclistItem = NO;
    isEndBalisescpara = NO;
    isEndBalisescrefUri = NO;
    isEndBalisesctextLeaf = NO;
    isEndBalisescuLink = NO;
    isEndBalisespcourseUa = NO; 
    isEndBalisespcourseUc = NO;
    isEndBalisespdiv = NO;
    isEndBalisespex = NO;
    isEndBalisespform = NO;
    isEndBalisespimg = NO;
    isEndBalisespintro = NO;
    isEndBalisespconclu = NO;
    isEndBalisesppractUc = NO;
    isEndBalisespsTitle = NO;
    isEndBalisesptitle = NO;
    isEndBalisesptxt = NO;
    isEndBalisespvid = NO;
    isEndBalisesptab = NO;
    isEndBaliseopcourseUa = NO; 
    isEndBalisespinfo = NO; 
    isEndBalisespodInstruct = NO; 
    CompteurBaliseopueDiv = 0;
    CompteurContent = 0;
    topBalisescitemizedList = NO;
    
    self.currentNodeContent = nil;
    self.currentNodeContent = [[NSMutableString alloc] init];
    
    // alloc init des tableaux de collecte
    self.monDicoContenuSequences = nil;
    self.monDicoContenuSequences = [[NSMutableDictionary alloc] init];
    self.monArrayIntermediaire = nil;
    self.monArrayIntermediaire = [[NSMutableArray alloc] init];
    self.monTableauDeCollecteIntermediare = nil;
    self.monTableauDeCollecteIntermediare = [[NSMutableArray alloc] init];
    
}

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    //   DLog(@"");
    isStartBaliseoppb           = NO;
    isStartBaliseopres          = NO;
    isStartBaliseoptxt          = NO;
    isStartBaliseopue           = NO;
    isStartBaliseopueDiv        = NO;
    isStartBaliseopueDivM       = NO;
    isStartBaliseopueM          = NO;
    isStartBaliseopuM           = NO;
    isStartBalisescinlineImg    = NO;
    isStartBalisescinlineStyle  = NO;
    isStartBalisescitem         = NO;
    isStartBalisescitemizedList = NO;
    isStartBalisesclistItem     = NO;
    isStartBalisescpara         = NO;
    isStartBalisescrefUri       = NO;
    isStartBalisesctextLeaf     = NO;
    isStartBalisescuLink        = NO;
    isStartBalisespcourseUa     = NO; 
    isStartBalisespcourseUc     = NO;
    isStartBalisespdiv          = NO;
    isStartBalisespex           = NO;
    isStartBalisespform         = NO;
    isStartBalisespimg          = NO;
    isStartBalisespintro        = NO;
    isStartBalisespconclu       = NO;
    isStartBalisesppractUc      = NO;
    isStartBalisespsTitle       = NO;
    isStartBalisesptitle        = NO;
    isStartBalisesptxt          = NO;
    isStartBalisespvid          = NO;
    isStartBaliseopexpUc        = NO;
    
    
    /* balise sc:item en début de fichier  */ 
    if ([elementName isEqualToString:kBalisescitem])           
    {
        isStartBalisescitem = YES;
        topBalisescitem = YES;
        
    }
    
    /* balise op:expUc en début de fichier  */ 
    if ([elementName isEqualToString:kBaliseopexpUc]) 
    {
        isStartBaliseopexpUc = YES;
        topBaliseopexpUc = YES;
        self.dicoFichier = nil; 
        self.dicoFichier = [[NSMutableDictionary alloc] init]; 
        //CompteurBaliseopueDiv++;
        if (self.arrayFichier)
        {
            self.arrayFichier = nil;
            self.arrayFichier = [[NSMutableArray alloc] init]; 
        }
    }  
    
    /*
     if ([elementName isEqualToString:kBaliseopueDiv]) 
     {
     isStartBaliseopueDiv = YES;
     topBaliseopueDiv = YES;
     self.dicoFichier = nil; 
     self.dicoFichier = [[NSMutableDictionary alloc] init]; 
     CompteurBaliseopueDiv++;
     if (self.arrayFichier)
     {
     self.arrayFichier = nil;
     self.arrayFichier = [[NSMutableArray alloc] init]; 
     }
     }
     */       
    
    
    
    if ([elementName isEqualToString:kBaliseoppb])           
    {
        isStartBaliseoppb = YES;
        topBaliseoppb = YES;
        self.monArrayDicoItemAudio =[[NSMutableArray alloc] init];
        self.monDicoArrayDicoItemAudio =[[NSMutableDictionary alloc] init];
        
    }
    if ([elementName isEqualToString:kBaliseopres])          
    {
        isStartBaliseopres = YES;
        topBaliseopres = YES;
    }
    if ([elementName isEqualToString:kBaliseoptxt])           
    {
        isStartBaliseoptxt = YES;
        topBaliseoptxt = YES;
        
    }
    if ([elementName isEqualToString:kBaliseopue])           
    {
        isStartBaliseopue = YES;
        topBaliseopue = YES;
        
    }
    if ([elementName isEqualToString:kBaliseopueDivM])           
    {
        isStartBaliseopueDivM = YES;
        topBaliseopueDivM = YES;
        
    }
    if ([elementName isEqualToString:kBaliseopueM])           
    {
        isStartBaliseopueM = YES;
        topBaliseopueM = YES;
        
    }
    if ([elementName isEqualToString:kBaliseopuM])           
    {
        isStartBaliseopuM = YES;
        topBaliseopuM = YES;
        
    }
    if ([elementName isEqualToString:kBalisescinlineImg])           
    {
        isStartBalisescinlineImg = YES;
        topBalisescinlineImg = YES;
        
    }
    if ([elementName isEqualToString:kBalisescinlineStyle])           
    {
        isStartBalisescinlineStyle = YES;
        topBalisescinlineStyle = YES;
        
    }
    if ([elementName isEqualToString:kBalisescitemizedList])           
    {
        isStartBalisescitemizedList = YES;
        topBalisescitemizedList = YES;
        
    }
    if ([elementName isEqualToString:kBalisesclistItem])           
    {
        isStartBalisesclistItem = YES;
        topBalisesclistItem = YES;
        
    }
    if ([elementName isEqualToString:kBalisescpara])           
    {
        isStartBalisescpara = YES;
        self.currentNodeContent = nil;
        self.currentNodeContent = [[NSMutableString alloc] init];
        topBalisescpara = YES;
        
    }
    if ([elementName isEqualToString:kBalisescrefUri])           
    {
        isStartBalisescrefUri = YES;
        topBalisescrefUri = YES;
        
    }
    if ([elementName isEqualToString:kBalisesctextLeaf])           
    {
        isStartBalisesctextLeaf = YES;
        topBalisesctextLeaf = YES;
        
    }
    if ([elementName isEqualToString:kBalisescuLink])           
    {
        isStartBalisescuLink = YES;
        topBalisescuLink = YES;
        
    }
    if ([elementName isEqualToString:kBalisespcourseUa])           
    {
        isStartBalisespcourseUa = YES; 
        topBalisespcourseUa = YES;
        
    }
    if ([elementName isEqualToString:kBalisespcourseUc])           
    {
        isStartBalisespcourseUc     = YES;
        topBalisespcourseUc = YES;
        
    }
    if ([elementName isEqualToString:kBalisespdiv])           
    {
        isStartBalisespdiv          = YES;
        topBalisespdiv = YES;
        
    }
    if ([elementName isEqualToString:kBalisespex])           
    {
        isStartBalisespex = YES;
        topBalisespex = YES;
        
        
    }
    if ([elementName isEqualToString:kBalisespform])           
    {
        isStartBalisespform = YES;
        topBalisespform = YES;
        
    }
    if ([elementName isEqualToString:kBalisespimg])           
    {
        isStartBalisespimg = YES;
        topBalisespimg = YES;
        
    }
    if ([elementName isEqualToString:kBalisespintro])           
    {
        isStartBalisespintro = YES;
        topBalisespintro = YES;
        CompteurContent = 0;
        
    }    if ([elementName isEqualToString:kBalisespconclu])           
    {
        isStartBalisespconclu = YES;
        topBalisespconclu = YES;
        CompteurContent = 0;
        
    }
    if ([elementName isEqualToString:kBalisesppractUc])           
    {
        isStartBalisesppractUc      = YES;
        topBalisesppractUc = YES;
        
    }
    if ([elementName isEqualToString:kBalisespsTitle])           
    {
        isStartBalisespsTitle       = YES;
        self.currentNodeContent = nil;
        self.currentNodeContent = [[NSMutableString alloc] init];
        topBalisespsTitle = YES;
        
    }
    if ([elementName isEqualToString:kBalisesptitle])           
    {
        topBalisesptitle = YES;
        isStartBalisesptitle        = YES;
        self.currentNodeContent = nil;
        self.currentNodeContent = [[NSMutableString alloc] init];
        
    }
    if ([elementName isEqualToString:kBalisesptxt])           
    {
        isStartBalisesptxt = YES;
        topBalisesptxt = YES;
        
    }
    if ([elementName isEqualToString:kBalisespvid])           
    {
        isStartBalisespvid = YES;
        topBalisespvid = YES;
        
    }
    if ([elementName isEqualToString:kBalisespinfo])           
    {
        isStartBalisespinfo = YES;
        topBalisespinfo = YES;
        
    }
    
    if ([elementName isEqualToString:kBalisespodInstruct])           
    {
        isStartBalisespodInstruct = YES;
        topBalisespodInstruct = YES;
        
    }
    
    if ([elementName isEqualToString:kBaliseopcourseUa])
    {   
        isStartBaliseopcourseUa = YES;
        topBaliseopcourseUa = YES;
    }
    
    
    
    if ([elementName isEqualToString:kBalisesptab])
    {   
        isStartBalisesptab = YES;
        topBalisesptab = YES;
    }
    
    if ([elementName isEqualToString:kBaliseopexpUc])
    {   
        isStartBaliseopexpUc = YES;
        topBaliseopexpUc = YES;
    }
    
    // initialisation des tableaux et dictionnaires de contenu
    switch (situation) 
    {
        case isListeFichier: 
            DLog(@"start isListeFichier");
            // récupération de fichier XML en balise sp:courseUc
            if ((isStartBalisespcourseUc
                 || isStartBalisespcourseUa
                 || isStartBalisespdiv
                 || isStartBalisespintro
                 || isStartBalisespconclu
                 /**/
                 
                 )) 
            {
                //*****************************
                // Fichier à analyser 
                //*****************************
                //-----------------------------------------------------------------------
                // inspection du fichier XML du tableau qui renvoie un dictionnaire 
                //if (!topBaliseopueDiv)
                if (([attributeDict objectForKey:kBalisescrefUri]) !=nil)
                {                    
                    topElse = NO;                      
                    self.levelLast = self.level;
                    self.level++;
                    //[[NSNotificationCenter defaultCenter] postNotificationName:kInsertionNotification object:[[NSDictionary alloc] initWithObjectsAndKeys:pathFichierXML,@"path",[[NSString alloc] initWithFormat:@"%d",self.level],@"level",[[NSString alloc] initWithFormat:@"%d",self.levelLast],@"levelLast", nil]];                    
                    NSString * pathFichierXML = [[GestionFichier alloc] concateneDirectory:self.path fileName:[attributeDict objectForKey:kBalisescrefUri]];                 
                    XMLToObjectParser * objectXMLsousRefFichier;
                    if (!topBaliseopexpUc)
                    {                    
                        DLog(@"cas1 %@",pathFichierXML);
                        objectXMLsousRefFichier = [[XMLToObjectParser alloc] initWithSituation:isContent fichierPubli:path level:self.level iCompeurFichier:self.iCompeurFichier];
                    }
                    else
                    {
                        DLog(@"cas2 %@",pathFichierXML);
                        objectXMLsousRefFichier = [[XMLToObjectParser alloc] initWithSituation:isContent fichierPubli:path level:self.level iCompeurFichier:self.iCompeurFichier];
                    }
                    
                    
                    
                    
                    
                    NSDictionary * monDicoResult = [objectXMLsousRefFichier inspectionCollecte:pathFichierXML];
                    if (([monDicoResult objectForKey:@"listeDesItems"] != nil ) &&( [[monDicoResult objectForKey:@"listeDesItems"] count] != 0))
                        [self.listeDesSequences addObject:[[monDicoResult objectForKey:@"listeDesItems"] objectAtIndex:0]];
                    [self.tableDictionnaireFichier addObject:monDicoResult];
                    [monDicoResult release];
                    [objectXMLsousRefFichier release]; 
                    self.level--;
                }
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // récuperation dans un tableau du fichier dans la Balise <sp:img>
            if (isStartBalisespimg)
            {
                if ([attributeDict objectForKey:kBalisescrefUri] !=nil)
                {
                    if ([[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] != nil)
                    {
                        NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                        [imageHTML appendString:@"<div class=\"resInFlow_co\">"];
                        [imageHTML appendString:@"<img width=\""];
                        [imageHTML appendString:[NSString stringWithFormat:@"20"]];
                        [imageHTML appendString:@"\" height=\""];
                        [imageHTML appendString:[NSString stringWithFormat:@"20"]];
                        [imageHTML appendString:@"\" alt=\"\" src=\'"];
                        [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri] ];
                        [imageHTML appendString:@"\'></img></div>"];
                        [dicoFichier setObject:[NSString stringWithString:imageHTML]
                                        forKey:@"html"];                        
                        [dicoFichier setObject:[[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] 
                                        forKey:@"imageName"];
                    }
                }
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // récuperation dans un tableau du fichier dans la Balise <sp:vid>
            if (isStartBalisespvid)
            {
                if ([attributeDict objectForKey:kBalisescrefUri] !=nil)
                {
                    if ([[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] != nil)
                    {
                        NSString * nomFichier = [[attributeDict objectForKey:kBalisescrefUri] lastPathComponent];
                        if ([[nomFichier pathExtension] isEqualToString:@"mp3" ] )
                        {
                            NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo resInFlow\">"];
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo_co resInFlow_co\">"];
                            [imageHTML appendString:@"<object class=\" resVideo\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\""];
                            [imageHTML appendString:@" classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" "];
                            [imageHTML appendString:@"width=\"300\" height=\"20\">"];
                            [imageHTML appendString:@"<param name=\"src\" value=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"autoplay\">"];
                            [imageHTML appendString:@"<param value=\"true\" name=\"controller\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"loop\">"];
                            [imageHTML appendString:@"<param value=\"aspect\" name=\"scale\">"];
                            [imageHTML appendString:@"<embed scale=\"aspect\" loop=\"false\" "];
                            [imageHTML appendString:@"controller=\"true\" autoplay=\"false\" "];
                            [imageHTML appendString:@"class=\" resVideo\" "];
                            [imageHTML appendString:@"pluginspage=\"http://www.apple.com/quicktime/download/\" "];
                            [imageHTML appendString:@"src=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\" "];
                            [imageHTML appendString:@"type=\"video/quicktime\" width=\"300\" "];
                            [imageHTML appendString:@"height=\"20\">"];
                            [imageHTML appendString:@"</embed>"];
                            [imageHTML appendString:@"</object>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"<br/>"];
                            [self.dicoFichier setObject:imageHTML
                                                 forKey:@"html"];
                            [self.dicoFichier setObject:nomFichier
                                                 forKey:@"soundName"];
                        }
                        else
                        {
                            NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo resInFlow\">"];
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo_co resInFlow_co\">"];
                            [imageHTML appendString:@"<object class=\" resVideo\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\""];
                            [imageHTML appendString:@" classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" "];
                            [imageHTML appendString:@"width=\"300\" height=\"180\">"];
                            [imageHTML appendString:@"<param name=\"src\" value=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"autoplay\">"];
                            [imageHTML appendString:@"<param value=\"true\" name=\"controller\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"loop\">"];
                            [imageHTML appendString:@"<param value=\"aspect\" name=\"scale\">"];
                            [imageHTML appendString:@"<embed scale=\"aspect\" loop=\"false\" "];
                            [imageHTML appendString:@"controller=\"true\" autoplay=\"false\" "];
                            [imageHTML appendString:@"class=\" resVideo\" "];
                            [imageHTML appendString:@"pluginspage=\"http://www.apple.com/quicktime/download/\" "];
                            [imageHTML appendString:@"src=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\" "];
                            [imageHTML appendString:@"type=\"video/quicktime\" width=\"300\" "];
                            [imageHTML appendString:@"height=\"180\">"];
                            [imageHTML appendString:@"</embed>"];
                            [imageHTML appendString:@"</object>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"<br/>"];
                            [self.dicoFichier setObject:imageHTML
                                                 forKey:@"html"];
                            [self.dicoFichier setObject:nomFichier 
                                                 forKey:@"videoName"];
                        }
                    }
                }
            }
            
            
            if (isStartBalisesptab)
            {
                if ([attributeDict objectForKey:kBalisescrefUri] !=nil)
                {
                    if ([[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] != nil)
                    {
                        [self.dicoFichier setObject:[[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] 
                                             forKey:@"tabName"];
                    }	
                }	
            }	
            
            
            break;
        case isContent:
            
            // récupération de fichier XML en balise sp:courseUc
            if (isStartBalisespcourseUc
                || isStartBalisespcourseUa
                
                /**/
                || isStartBalisespdiv
                || isStartBalisespconclu
                || isStartBalisespintro
                /**/
                
                ) 
            {
                //*****************************
                // Fichier à analyser 
                //*****************************
                //Analyse du fichier XML
                //-----------------------------------------------------------------------
                // inspection du fichier XML du tableau qui renvoie un dictionnaire 
                
                //if  (!topBaliseopueDiv)
                if (([attributeDict objectForKey:kBalisescrefUri]) !=nil)
                {
                    topElse = NO;                      
                    self.levelLast = self.level;
                    self.level++;
                    NSString * pathFichierXML = [[GestionFichier alloc] concateneDirectory:self.path fileName:[attributeDict objectForKey:kBalisescrefUri]];                 
                    //[[NSNotificationCenter defaultCenter] postNotificationName:kInsertionNotification object:[[NSDictionary alloc] initWithObjectsAndKeys:pathFichierXML,@"path",[[NSString alloc] initWithFormat:@"%d",self.level],@"level",[[NSString alloc] initWithFormat:@"%d",self.levelLast],@"levelLast", nil]];                    
                    XMLToObjectParser * objectXMLsousRefFichier;
                    if  (!topBaliseopexpUc)
                    {
                        DLog(@"cas3 %@",pathFichierXML);
                        objectXMLsousRefFichier = [[XMLToObjectParser alloc] initWithSituation:isContent fichierPubli:path level:self.level iCompeurFichier:self.iCompeurFichier];
                    }
                    else
                    {                      
                        DLog(@"cas4 %@",pathFichierXML);
                        objectXMLsousRefFichier = [[XMLToObjectParser alloc] initWithSituation:isContent fichierPubli:path level:self.level iCompeurFichier:self.iCompeurFichier];
                    }
                    //
                    NSDictionary * monDicoResult = [objectXMLsousRefFichier inspectionCollecte:pathFichierXML];
                    DLog(@"monDicoResult3 is %@",monDicoResult);
                    if (([monDicoResult objectForKey:@"listeDesItems"] != nil ) &&( [[monDicoResult objectForKey:@"listeDesItems"] count] != 0))
                        [self.listeDesSequences addObject:[[monDicoResult objectForKey:@"listeDesItems"] objectAtIndex:0]];
                    [self.tableDictionnaireFichier addObject:monDicoResult];
                    [monDicoResult release];
                    [objectXMLsousRefFichier release];
                    self.level--;
                }
            }
            
            DLog(@"start isContent");
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du nom du fichier image dans la Balise <sp:img>
            if (isStartBalisespimg && ( topBalisespinfo || topBalisespex )) 
            { 
                //CompteurContent++;
                if ([attributeDict objectForKey:kBalisescrefUri] !=nil)
                {
                    if ([[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] != nil)
                    {
                        NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                        
                        [imageHTML appendString:@"<div class=\"resInFlow_co\">"];
                        [imageHTML appendString:@"<img width=\""];
                        [imageHTML appendString:[NSString stringWithFormat:@"20"]];
                        [imageHTML appendString:@"\" height=\""];
                        [imageHTML appendString:[NSString stringWithFormat:@"20"]];
                        [imageHTML appendString:@"\" alt=\"\" src=\'"];
                        [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri] ];
                        [imageHTML appendString:@"\'></img></div>"];
                        
                        [monDicoContenuSequences setObject:[NSString stringWithString:imageHTML]
                                                    forKey:@"html"];     
                        
                        
                        
                        
                        [monDicoContenuSequences setObject:[[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] 
                                                    forKey:@"imageName"];
                        [monTableauDeCollecteIntermediare addObject:monDicoContenuSequences];
                        //[monDicoContenuSequences release];
                        //monDicoContenuSequences = nil;
                        //monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                        
                        
                    }
                }
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du nom du video image dans la Balise <sp:vid>
            if (isStartBalisespvid && ( topBalisespinfo || topBalisespex )) 
            { 
                if ([attributeDict objectForKey:kBalisescrefUri] !=nil)
                {
                    if ([[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] != nil)
                    {
                        
                        NSString * nomFichier = [[attributeDict objectForKey:kBalisescrefUri] lastPathComponent];
                        if ([[nomFichier pathExtension] isEqualToString:@"mp3" ] )
                        {
                            
                            NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                            
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo resInFlow\">"];
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo_co resInFlow_co\">"];
                            [imageHTML appendString:@"<object class=\" resVideo\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\""];
                            [imageHTML appendString:@" classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" "];
                            [imageHTML appendString:@"width=\"300\" height=\"20\">"];
                            [imageHTML appendString:@"<param name=\"src\" value=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"autoplay\">"];
                            [imageHTML appendString:@"<param value=\"true\" name=\"controller\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"loop\">"];
                            [imageHTML appendString:@"<param value=\"aspect\" name=\"scale\">"];
                            [imageHTML appendString:@"<embed scale=\"aspect\" loop=\"false\" "];
                            [imageHTML appendString:@"controller=\"true\" autoplay=\"false\" "];
                            [imageHTML appendString:@"class=\" resVideo\" "];
                            [imageHTML appendString:@"pluginspage=\"http://www.apple.com/quicktime/download/\" "];
                            [imageHTML appendString:@"src=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\" "];
                            [imageHTML appendString:@"type=\"video/quicktime\" width=\"300\" "];
                            [imageHTML appendString:@"height=\"20\">"];
                            [imageHTML appendString:@"</embed>"];
                            [imageHTML appendString:@"</object>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"<br/>"];
                            
                            [monDicoContenuSequences setObject:imageHTML
                                                        forKey:@"html"];
                            
                            
                            [monDicoContenuSequences setObject:nomFichier
                                                        forKey:@"soundName"];
                        }
                        else {
                            
                            NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                            
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo resInFlow\">"];
                            [imageHTML appendString:@"<div class=\"sfile_mov_resVideo_co resInFlow_co\">"];
                            [imageHTML appendString:@"<object class=\" resVideo\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\""];
                            [imageHTML appendString:@" classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" "];
                            [imageHTML appendString:@"width=\"300\" height=\"180\">"];
                            [imageHTML appendString:@"<param name=\"src\" value=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"autoplay\">"];
                            [imageHTML appendString:@"<param value=\"true\" name=\"controller\">"];
                            [imageHTML appendString:@"<param value=\"false\" name=\"loop\">"];
                            [imageHTML appendString:@"<param value=\"aspect\" name=\"scale\">"];
                            [imageHTML appendString:@"<embed scale=\"aspect\" loop=\"false\" "];
                            [imageHTML appendString:@"controller=\"true\" autoplay=\"false\" "];
                            [imageHTML appendString:@"class=\" resVideo\" "];
                            [imageHTML appendString:@"pluginspage=\"http://www.apple.com/quicktime/download/\" "];
                            [imageHTML appendString:@"src=\""];
                            [imageHTML appendString:[attributeDict objectForKey:kBalisescrefUri]];
                            [imageHTML appendString:@"\" "];
                            [imageHTML appendString:@"type=\"video/quicktime\" width=\"300\" "];
                            [imageHTML appendString:@"height=\"180\">"];
                            [imageHTML appendString:@"</embed>"];
                            [imageHTML appendString:@"</object>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"</div>"];
                            [imageHTML appendString:@"<br/>"];
                            
                            [monDicoContenuSequences setObject:imageHTML
                                                        forKey:@"html"];
                            
                            [monDicoContenuSequences setObject:nomFichier 
                                                        forKey:@"videoName"];
                        }
                        
                        
                        [monTableauDeCollecteIntermediare addObject:monDicoContenuSequences];
                        //[monDicoContenuSequences release];
                        //monDicoContenuSequences = nil;
                        //monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                        
                        
                    }
                }
            }
            
            if (isStartBalisesptab && ( topBalisespinfo || topBalisespex ))
            {
                if ([attributeDict objectForKey:kBalisescrefUri] !=nil)
                {
                    if ([[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] != nil)
                    {
                        [monDicoContenuSequences setObject:[[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] forKey:@"tabFile"];
                        [monTableauDeCollecteIntermediare addObject:monDicoContenuSequences];
                        //[monDicoContenuSequences release];
                        //monDicoContenuSequences = nil;
                        //monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                        
                    }	
                }	
            }	
            
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation de l'image dans la Balise <sc:inlineImg>
            if (isStartBalisescinlineImg && 
                topBalisespvid         && 
                topBalisespodInstruct)
                
            {
                
                if ([attributeDict objectForKey:kBalisescrefUri] !=nil)
                {
                    if ([[attributeDict objectForKey:kBalisescrefUri] lastPathComponent] != nil)
                    {
                        
                        NSString * nomFichier = [[attributeDict objectForKey:kBalisescrefUri] lastPathComponent];
                        {
                            [monDicoContenuSequences setObject:nomFichier
                                                        forKey:@"imageicone"];
                        }
                    }
                }
            }
            
            break;
        default:
            DLog(@"start default");
            
            break;
    }
}

- (void)parser:(NSXMLParser *)parser 
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    // DLog(@"");
    
    isEndBaliseopexpUc         = NO;
    isEndBaliseoppb            = NO;
    isEndBaliseopres           = NO;
    isEndBaliseoptxt           = NO;
    isEndBaliseopue            = NO;
    isEndBaliseopueDiv         = NO;
    isEndBaliseopueDivM        = NO;
    isEndBaliseopueM           = NO;
    isEndBaliseopuM            = NO;
    isEndBalisescinlineImg     = NO;
    isEndBalisescinlineStyle   = NO;
    isEndBalisescitem          = NO;
    isEndBalisescitemizedList  = NO;
    isEndBalisesclistItem      = NO;
    isEndBalisescpara          = NO;
    isEndBalisescrefUri        = NO;
    isEndBalisesctextLeaf      = NO;
    isEndBalisescuLink         = NO;
    isEndBalisespcourseUa      = NO;
    isEndBalisespcourseUc      = NO;
    isEndBalisespdiv           = NO;
    isEndBalisespex            = NO;
    isEndBalisespform          = NO;
    isEndBalisespimg           = NO;
    isEndBalisespintro         = NO;
    isEndBalisespconclu        = NO;
    isEndBalisesppractUc       = NO;
    isEndBalisespsTitle        = NO;
    isEndBalisesptitle         = NO;
    isEndBalisesptxt           = NO;
    isEndBalisespvid           = NO;
    isEndBalisespinfo          = NO;
    isEndBalisespodInstruct    = NO;
    
    if ([elementName isEqualToString:kBaliseopexpUc])        isEndBaliseopexpUc        = YES;
    if ([elementName isEqualToString:kBaliseoppb])           isEndBaliseoppb           = YES;
    if ([elementName isEqualToString:kBaliseopres])          isEndBaliseopres          = YES;
    if ([elementName isEqualToString:kBaliseoptxt])          isEndBaliseoptxt          = YES;
    if ([elementName isEqualToString:kBaliseopue])           isEndBaliseopue           = YES;
    if ([elementName isEqualToString:kBaliseopueDiv])        isEndBaliseopueDiv        = YES;
    if ([elementName isEqualToString:kBaliseopueDivM])       isEndBaliseopueDivM       = YES;
    if ([elementName isEqualToString:kBaliseopueM])          isEndBaliseopueM          = YES;
    if ([elementName isEqualToString:kBaliseopuM])           isEndBaliseopuM           = YES;
    if ([elementName isEqualToString:kBalisescinlineImg])    isEndBalisescinlineImg    = YES;
    if ([elementName isEqualToString:kBalisescinlineStyle])  isEndBalisescinlineStyle  = YES;
    if ([elementName isEqualToString:kBalisescitem])   
        isEndBalisescitem         = YES;
    if ([elementName isEqualToString:kBalisescitemizedList]) isEndBalisescitemizedList = YES;
    if ([elementName isEqualToString:kBalisesclistItem])     isEndBalisesclistItem     = YES;
    if ([elementName isEqualToString:kBalisescpara])         isEndBalisescpara         = YES;
    if ([elementName isEqualToString:kBalisescrefUri])       isEndBalisescrefUri       = YES;
    if ([elementName isEqualToString:kBalisesctextLeaf])     isEndBalisesctextLeaf     = YES;
    if ([elementName isEqualToString:kBalisescuLink])        isEndBalisescuLink        = YES;
    if ([elementName isEqualToString:kBalisespcourseUa])     isEndBalisespcourseUa     = YES;
    if ([elementName isEqualToString:kBalisespcourseUc])     isEndBalisespcourseUc     = YES;
    if ([elementName isEqualToString:kBalisespdiv])          isEndBalisespdiv          = YES;
    if ([elementName isEqualToString:kBalisespex])           isEndBalisespex           = YES;
    if ([elementName isEqualToString:kBalisespform])         isEndBalisespform         = YES;
    if ([elementName isEqualToString:kBalisespimg])          isEndBalisespimg          = YES;
    if ([elementName isEqualToString:kBalisespintro])        isEndBalisespintro        = YES;
    if ([elementName isEqualToString:kBalisespconclu])       isEndBalisespconclu       = YES;
    if ([elementName isEqualToString:kBalisesppractUc])      isEndBalisesppractUc      = YES;
    if ([elementName isEqualToString:kBalisespsTitle])       isEndBalisespsTitle       = YES;
    if ([elementName isEqualToString:kBalisesptitle])        isEndBalisesptitle        = YES;
    if ([elementName isEqualToString:kBalisesptxt])          isEndBalisesptxt          = YES;
    if ([elementName isEqualToString:kBalisespvid])          isEndBalisespvid          = YES;
    if ([elementName isEqualToString:kBalisespinfo])         isEndBalisespinfo         = YES;
    if ([elementName isEqualToString:kBalisespodInstruct])   isEndBalisespodInstruct   = YES;
    
    if (isEndBaliseopueM) topBaliseopueM = NO;
    if (isEndBalisespex) topBalisespex = NO;
    
    if (isEndBaliseopueDiv)
    {	
        topBaliseopueDiv = NO;
    }
    
    //if (isEndBaliseopueDiv)
    if (isEndBaliseopexpUc) 
    {	
        topBaliseopexpUc = NO;
        DLog(@"self.dicoFichier : %@",self.dicoFichier);
        [self.tableDictionnaireFichier addObject:self.dicoFichier];
    }
    
    //if (isEndBalisespcourseUc && topElse)
    //    DLog(@"isEndBalisespcourseUc et topElse");
    switch (situation) 
    {
        case isContentAndItem:
            DLog(@"end isContentAndItem");
            break;
        case isItem:
            DLog(@"end isItem");
            
            break;
        case isListeFichier: 
            
            DLog(@"end isListeFichier");
            if (isEndBalisesptitle)
            {
                DLog(@"title is %@",self.currentNodeContent)
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du titre dans la Balise <sp:title> à l'interieur d'une balise <op:ueM>
            if (isEndBalisesptitle &&
                topBaliseopueM ) 
            {
                if (self.currentNodeContent)
                {
                    //Ajout du texte du titre dans la listeDesItems (liste des titres)
                    [self.listeDesItems addObject:[NSString stringWithString:self.currentNodeContent]];
                    //Ajout au dictionnaire de la clé Titre avec la valeur texte du titre
                    [self.monDicoRacine setObject:self.currentNodeContent 
                                           forKey:kKeySequenceName];
                }
                else
                {
                    DLog(@"currentNodeContent is nil");
                }
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du sous titre dans la Balise <sp:sTitle> à l'interieur d'une balise <op:ueM>
            if (isEndBalisespsTitle &&
                topBaliseopueM ) 
            {
                //Ajout au dictionnaire de la clé sous Titre avec la valeur texte du sous titre
                if (self.currentNodeContent)
                {
                    
                    [self.monDicoRacine setObject:self.currentNodeContent 
                                           forKey:kKeySequenceName];
                }
                else
                {
                    DLog(@"currentNodeContent is nil");
                }
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du texte dans la Balise <sc:para> à l'interieur d'une balise <op:ueDiv>
            if (isEndBalisescpara &&
                (topBaliseopueDiv  || topBaliseopexpUc ) ) 
            {
                if (self.currentNodeContent != nil) 
                {
                    //Ajout au dictionnaire de la clé sous Titre avec la valeur texte du sous titre
                    NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                    [imageHTML appendString:@"<div class=\"info\">"];
                    [imageHTML appendString:@"<p class=\"op_txt_p\">"];
                    [imageHTML appendString:self.currentNodeContent];
                    [imageHTML appendString:@"</p>"];
                    [imageHTML appendString:@"</div>"];
                    [imageHTML appendString:@"<br/>"];
                    [self.monDicoContenuSequences setObject:imageHTML
                                                     forKey:@"html"];
                    
                    [self.monDicoContenuSequences setObject:[NSString stringWithString:self.currentNodeContent]
                                                     forKey:@"texte"];
                    [monTableauDeCollecteIntermediare addObject:monDicoContenuSequences];
                    //[monDicoContenuSequences release];
                    //monDicoContenuSequences = nil;
                    //monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                    
                }
            }
            
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du tableau de récolte des Balises <sc:para> à l'interieur d'une balise <op:ueDiv>
            //if (isEndBaliseopueDiv) 
            if (isEndBaliseopexpUc) 
            {
                
                
                if (self.arrayFichier)
                {
                    [self.dicoFichier setObject:self.arrayFichier 
                                         forKey:@"sousRefFichier"];
                    [self.arrayFichier release];
                }
                
                
                if (monDicoContenuSequences != nil) 
                {
                    [self.dicoFichier setObject:monDicoContenuSequences 
                                         forKey:@"dicoContenuSequences"];
                }
                //[monDicoContenuSequences release];
                //monDicoContenuSequences = nil;
                //monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                
                if (self.listeDesSousSequences !=nil)
                {
                    
                    if ([self.listeDesSousSequences count]>0)
                    {
                        [self.dicoFichier setObject:self.listeDesSousSequences 
                                             forKey:@"listeDesSequences"];
                        
                        DLog(@"dicoFichier is %@",dicoFichier);
                        
                    }
                }
                [listeDesSousSequences release];
                listeDesSousSequences = nil;
                listeDesSousSequences = [[NSMutableArray alloc] init];
                
                
                
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du contenu de la Balise <sc:uLink> à l'interieur d'une balise <op:ueDiv>
            if (isEndBalisescuLink &&
                (topBaliseopueDiv  || topBaliseopexpUc ) ) 
            { 
                [self.dicoFichier setObject:self.currentNodeContent 
                                     forKey:kBalisescuLink];
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du contenu de la Balise <sc:inlineStyle> à l'interieur d'une balise <op:ueDiv>
            if (isEndBalisescinlineStyle &&
                (topBaliseopueDiv  || topBaliseopexpUc ) ) 
            { 
                [self.dicoFichier setObject:self.currentNodeContent 
                                     forKey:kBalisescinlineStyle];
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du titre dans la Balise <sp:title> à l'interieur d'une balise <op:ueDiv>
            if (isEndBalisesptitle &&
                (topBaliseopueDiv  || topBaliseopexpUc ) ) 
            { 
                if (self.currentNodeContent)
                {
                    [self.dicoFichier setObject:self.currentNodeContent 
                                         forKey:kKeySequenceName];
                    [self.listeDesSequences addObject:self.currentNodeContent];
                    
                }
                else
                {
                    DLog(@"currentNodeContent is nil");
                }
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du sous titre dans la Balise <sp:sTitle> à l'interieur d'une balise <op:ueDiv>
            if (isEndBalisespsTitle &&
                (topBaliseopueDiv  || topBaliseopexpUc ) ) 
            { 
                [self.dicoFichier setObject:self.currentNodeContent 
                                     forKey:kKeysTitle];
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du tableau de récolte des Balises ??? à la fin d'une balise <sc:item>
            if (isEndBalisescitem || isEndBaliseopexpUc) 
            {
                // DLog(@"monArrayIntermediaire is %@",monArrayIntermediaire)
                
                if (self.tableDictionnaireFichier !=nil)
                {
                    if ([self.tableDictionnaireFichier count]>0)
                    {
                        [self.monDicoRacine setObject:self.tableDictionnaireFichier 
                                               forKey:@"sequenceContent"];
                    }
                }
                if (self.listeDesSequences !=nil)
                {
                    if ([self.listeDesSequences count]>0)
                    {
                        [self.monDicoRacine setObject:self.listeDesSequences 
                                               forKey:@"listeDesSequences"];
                    }
                }
                
            }
            
            break;
        case isContent: 
            if (isEndBalisesptitle)
            {
                DLog(@"title is %@",self.currentNodeContent)
            }           
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation de l'image dans la Balise <sc:inlineImg>
            if (isEndBalisescinlineImg && 
                !topBalisespinfo       && 
                !topBalisespex)
            {
                [self.monDicoRacine setObject:self.currentNodeContent forKey:kBalisescinlineImg];
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation de l'image dans la Balise <sc:inlineStyle>
            if (isEndBalisescinlineStyle && 
                !topBalisespinfo         &&
                !topBalisespex)
            {
                [self.monDicoRacine setObject:self.currentNodeContent forKey:kBalisescinlineStyle];
                
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation de l'image dans la Balise <sc:textLeaf>
            if (isEndBalisesctextLeaf && 
                !topBalisespinfo      && 
                !topBalisespex)
            {
                [self.monDicoRacine setObject:self.currentNodeContent forKey:kBalisesctextLeaf];
                
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation de l'image dans la Balise <sc:uLink>
            if (isEndBalisescuLink)
            {
                [self.monDicoRacine setObject:self.currentNodeContent forKey:kBalisescuLink];
                
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du titre dans la Balise <sp:title> à l'interieur d'une balise <op:uM>
            if (isEndBalisesptitle && 
                topBaliseopuM ) 
            {
                if (self.currentNodeContent) 
                {
                    [self.listeDesItems addObject:[NSString stringWithString:self.currentNodeContent]];
                    [self.monDicoRacine setObject:self.currentNodeContent
                                           forKey:kKeySequenceName];
                }
                else
                {
                    DLog(@"currentNodeContent is nil");
                }
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du sous titre dans la Balise <sp:sTitle> à l'interieur d'une balise <op:uM>
            if (isEndBalisespsTitle	&&
                topBaliseopuM ) 
            {
                [self.monDicoRacine setObject:self.currentNodeContent
                                       forKey:kKeysTitle];
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du titre dans la Balise <sp:title> à l'interieur d'une balise <op:ueDiv>
            if (isEndBalisesptitle && 
                (topBaliseopueDiv  || topBaliseopexpUc )) 
            { 
                if (self.currentNodeContent) 
                {
                    //////////////////********************///////////
                    [self.listeDesItems addObject:[NSString stringWithString:self.currentNodeContent]];
                    //////////////////********************///////////
                    [self.dicoFichier setObject:self.currentNodeContent
                                         forKey:kKeySequenceName];
                    
                    
                }
                else
                {
                    DLog(@"currentNodeContent is nil");
                }
                
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du tableau de récolte des Balises ??? à la fin d'une balise <sc:item>
            if (isEndBalisescitem || isEndBaliseopexpUc) 
            {
                if (self.tableDictionnaireFichier != nil)
                {
                    if ([self.tableDictionnaireFichier count]>0)
                    {
                        [self.monDicoRacine setObject:self.tableDictionnaireFichier 
                                               forKey:@"sequenceContent"];
                    }	
                }	
                
            }	
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du texte dans la Balise <sc:para>
            if (isEndBalisescpara &&
                (topBalisespinfo || topBalisespex)) 
            { 
                if (self.currentNodeContent != nil) 
                {
                    
                    if ( ![[self.currentNodeContent stringByTrimmingCharactersInSet:[NSCharacterSet controlCharacterSet]] isEqualToString:@""])
                    {    
                        // Numérotation des clés 
                        // Ajout au dictionnaire de la clé Item? avec le texte collecté 
                        
                        NSMutableString *imageHTML = [[[NSMutableString alloc] init] autorelease];
                        [imageHTML appendString:@"<div class=\"info\">"];
                        [imageHTML appendString:@"<p class=\"op_txt_p\">"];
                        [imageHTML appendString:self.currentNodeContent];
                        [imageHTML appendString:@"</p>"];
                        [imageHTML appendString:@"</div>"];
                        [imageHTML appendString:@"<br/>"];
                        [self.monDicoContenuSequences setObject:imageHTML
                                                         forKey:@"html"];
                        
                        
                        [monDicoContenuSequences setObject:[NSString stringWithString:self.currentNodeContent] 
                                                    forKey:@"texte"];
                        //		forKey:[NSString stringWithFormat:@"Item %d",++CompteurContent]];
                    }   
                    [monTableauDeCollecteIntermediare addObject:monDicoContenuSequences];
                    //[monDicoContenuSequences release];
                    //monDicoContenuSequences = nil;
                    //monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                }
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation de l'image dans la Balise <sc:inlineStyle>
            if (isEndBalisescinlineStyle &&
                topBalisescpara &&
                (topBalisespinfo || topBalisespex))
            {
                if (self.currentNodeContent != nil) 
                {
                    // Ajout au dictionnaire de la clé scinlineStyle avec le texte collecté 
                    [monDicoContenuSequences setObject:self.currentNodeContent 
                                                forKey:@"scinlineStyle"];
                    
                }
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation de l'image dans la Balise <sc:textLeaf>
            if (isEndBalisesctextLeaf && 
                topBalisescpara && 
                (topBalisespinfo || topBalisespex))
            {
                // Ajout au dictionnaire de la clé sctextLeaf avec le texte collecté 
                [monDicoContenuSequences setObject:self.currentNodeContent
                                            forKey:@"sctextLeaf"];
                
            }
            
            
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // récuperation du tableau de récolte à la fin de la Balise <sp:img>
            if (isEndBalisespimg && 
                (topBalisespinfo || topBalisespex) ) 
            {
                // Ajout de contenu de la sequence à la liste intermédiaire 
                if (monDicoContenuSequences != nil) 
                    if ([monDicoContenuSequences count]>0) 
                        [monArrayIntermediaire addObject:monDicoContenuSequences];
                // release
                //[monDicoContenuSequences release];
                //monDicoContenuSequences = nil;
                //monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                CompteurContent=0;
                
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // récuperation du tableau de récolte à la fin de la Balise <sp:vid>
            if (isEndBalisespvid && 
                ( topBalisespinfo || topBalisespex )) 
            {
                // Ajout de contenu de la sequence à la liste intermédiaire 
                if (monDicoContenuSequences != nil) 
                    if ([monDicoContenuSequences count]>0) 
                        [monArrayIntermediaire addObject:monDicoContenuSequences];
                // release
                //[monDicoContenuSequences release];
                // monDicoContenuSequences = nil;
                // monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                CompteurContent=0;
                
            }
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du tableau de récolte à la fin de la Balise <sp:info>
            if (isEndBalisespinfo || isEndBalisespex) 
            {
                // Ajout de contenu de la sequence à la liste intermédiaire 
                if (monDicoContenuSequences != nil) 
                    if ([monDicoContenuSequences count]>0) 
                        [monArrayIntermediaire addObject:monDicoContenuSequences];
                // [monDicoContenuSequences release];
                // monDicoContenuSequences = nil;
                // monDicoContenuSequences = [[NSMutableDictionary alloc] init];
                CompteurContent=0;
                
            }
            
            //==============================================================================
            //==============================================================================
            //==============================================================================
            // recuperation du tableau de récolte à la fin de la Balise <sc:item>
            if (isEndBalisescitem || isEndBaliseopexpUc) 
            {
                
                DLog(@"monArrayIntermediaire is %@",monArrayIntermediaire)
                
                if (monTableauDeCollecteIntermediare != nil) 
                {
                    if ([monTableauDeCollecteIntermediare count]>0) 
                    {
                        [self.monDicoRacine setObject:monTableauDeCollecteIntermediare 
                                               forKey:@"sequenceContent"];
                    }
                }
                
                
                if (self.listeDesSequences !=nil)
                {
                    
                    if ([self.listeDesSequences count]>0)
                    {
                        [self.monDicoRacine setObject:self.listeDesSequences 
                                               forKey:@"listeDesSequences"];
                        
                    }
                }
                
                [monTableauDeCollecteIntermediare release];
                monTableauDeCollecteIntermediare = nil;
                monTableauDeCollecteIntermediare = [[NSMutableArray alloc] init];
                
            }
            
            break;
        default:
            DLog(@"end default");
            break;
    }
    
    if ((isEndBalisesptitle)  ||
        (isEndBalisespsTitle) ||
        (isEndBalisescpara)	)   
    {
        self.currentNodeContent = nil;
        self.currentNodeContent = [[NSMutableString alloc] init];
    }
    
    if (isEndBalisescitemizedList) topBalisescitemizedList = NO;
    if (isEndBaliseopueM)	  	   topBaliseopueM = NO;
    if (isEndBaliseopuM)		   topBaliseopuM = NO;
    if (isEndBaliseopueDiv)		   topBaliseopueDiv = NO;
    if (isEndBalisespex)		   topBalisespex = NO;
    if (isEndBaliseopexpUc)		   topBaliseopexpUc = NO;
    if (isEndBaliseoppb)		   topBaliseoppb = NO;
    if (isEndBaliseopres)		   topBaliseopres = NO;
    if (isEndBaliseoptxt)	       topBaliseoptxt = NO;
    if (isEndBaliseopue)		   topBaliseopue = NO;
    if (isEndBaliseopueDivM)	   topBaliseopueDivM = NO;
    if (isEndBalisescinlineImg)	   topBalisescinlineImg = NO;
    if (isEndBalisescinlineStyle)  topBalisescinlineStyle = NO;
    if (isEndBalisescitem)		   topBalisescitem = NO;
    if (isEndBalisesclistItem)	   topBalisesclistItem = NO;
    if (isEndBalisescpara)		   topBalisescpara = NO;
    if (isEndBalisescrefUri)	   topBalisescrefUri = NO;
    if (isEndBalisesctextLeaf)	   topBalisesctextLeaf = NO;
    if (isEndBalisescuLink)        topBalisescuLink = NO;
    if (isEndBalisespcourseUa)     topBalisespcourseUa = NO;
    if (isEndBalisespcourseUc)     topBalisespcourseUc = NO;
    if (isEndBalisespdiv)		   topBalisespdiv = NO;
    if (isEndBalisespform)		   topBalisespform = NO;
    if (isEndBalisespimg)		   topBalisespimg = NO;
    if (isEndBalisespintro)		   topBalisespintro = NO;
    if (isEndBalisespconclu)	   topBalisespconclu = NO;
    if (isEndBalisesppractUc)      topBalisesppractUc = NO;
    if (isEndBalisespsTitle)	   topBalisespsTitle = NO;
    if (isEndBalisesptitle)		   topBalisesptitle = NO;
    if (isEndBalisesptxt)		   topBalisesptxt = NO;
    if (isEndBalisespvid)		   topBalisespvid = NO;
    if (isEndBalisesptab)		   topBalisesptab = NO;
    if (isEndBaliseopcourseUa)	   topBaliseopcourseUa = NO;
    if (isEndBalisespinfo)		   topBalisespinfo = NO;
    if (isEndBalisespodInstruct)   topBalisespodInstruct = NO;
    
}

- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string
{   
    //string = [string stringByTrimmingCharactersInSet:[NSCharacterSet controlCharacterSet]];
    DLog(@"string : %@",string);
    [self.currentNodeContent appendString:string];
}

#pragma mark -
#pragma mark Memory Management Methods

- (void)dealloc
{
    DLog(@"");
    
    [currentNodeContent release];
    [monDicoItemAudio release];
    [monArrayDicoItemAudio release];
    [monDicoArrayDicoItemAudio release];	
    
    [dicoFichier release];
    [tableDictionnaireFichier release];
    [monDicoContenuSequences release];
    [monArrayIntermediaire release];
    [monDicoRacine release];
    
    [super dealloc];
}

@end