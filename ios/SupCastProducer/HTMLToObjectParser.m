//
//  HTMLToObjectParser.m
//  SupCastProducer
//
//  Created by Cape EMN on 14/02/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "HTMLToObjectParser.h"

@implementation HTMLToObjectParser
@synthesize dicoInfo;

-(id)initWithDico:(NSDictionary *)dico
{
    DLog(@"");
	if (self == [super init])
	{    
        self.dicoInfo = [[NSDictionary alloc] initWithDictionary:dico]; 
	}
	return self;
}

//Récupération des URLs des liens du menu à gauche
-(NSArray *)eclate:(NSString *)nomFichierIndex
{
    DLog(@"");
    
    NSString * fichierParametres = [[NSBundle mainBundle]  pathForResource:@"Parametres" ofType:@"plist"];
    NSDictionary * monDico =[[NSDictionary alloc] initWithContentsOfFile:fichierParametres];
    NSArray * arrayRecherche =[[NSArray alloc] initWithArray:[monDico objectForKey:@"arrayRecherche"]];
    
    NSMutableArray * listeDesFichiers = [[NSMutableArray alloc] init]; 
    //   NSMutableArray * listeFichiers = [[NSMutableArray alloc] init];
    
    NSError *error;
    
    NSString *stringFromFileAtPath = [[NSString alloc]
                                      initWithContentsOfFile:nomFichierIndex
                                      encoding:NSUTF8StringEncoding
                                      error:&error];
    
    if (stringFromFileAtPath == nil) 
    {
        
        // an error occurred
        
        NSLog(@"Error reading file at %@\n%@",
              
              nomFichierIndex, [error localizedFailureReason]);
        
        // implementation continues ...
        
    } 
    /////////////
    
    
    //On récupère le code HTML d'index.html dans un MutableString pour le modifier par la suite, et adapter le code en fonction de l'iPhone
    NSMutableString * buffer = [[NSMutableString alloc] initWithString:stringFromFileAtPath]; 
    
    //On récupère l'URL d'accueil, qui est contenue dans la balise meta Refresh d'index.html (fichier généré pointé par SCP)
    NSRange MyRangeDebut1 = [buffer rangeOfString:@"<meta http-equiv=\"Refresh\""  options:NSCaseInsensitiveSearch];
    NSRange MyRangeDebut2 = [buffer rangeOfString:@"URL=" options:NSCaseInsensitiveSearch range:NSMakeRange(MyRangeDebut1.location,[buffer length]- MyRangeDebut1.location)];
    NSRange MyRangeDebut3 = [buffer rangeOfString:@"\""  options:NSCaseInsensitiveSearch range:NSMakeRange(MyRangeDebut2.location,[buffer length]-MyRangeDebut2.location)];
    
    //On chope le lien de redirection de l'index ; c'est la page de présentation
    NSString * monURL = [buffer substringWithRange:NSMakeRange(MyRangeDebut2.location+MyRangeDebut2.length,MyRangeDebut3.location - MyRangeDebut2.location - MyRangeDebut2.length )];
    NSString * monURLpath = [[nomFichierIndex stringByDeletingLastPathComponent] stringByAppendingPathComponent:monURL];
    
    //On met le contenu de ce lien (basiquement, ****_web.html) dans le buffer
    [buffer setString:[[NSString alloc]
                       initWithContentsOfFile:monURLpath
                       encoding:NSUTF8StringEncoding
                       error:&error]];
    BOOL OK = YES; 
    NSMutableString * monURL1 = [[NSMutableString alloc] init];
    NSMutableString * monURLpath1 = [[NSMutableString alloc] init];
    //Chemin d'output des liens du menu
    NSString * pathCible =[[[dicoInfo objectForKey:kPathCible] stringByAppendingPathComponent:[dicoInfo objectForKey:kDossierRessource]] stringByAppendingPathComponent:@"menu"]; 
    
    NSError * erreur;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:pathCible  withIntermediateDirectories:YES  attributes:nil  error:&erreur])
    {
        DLog(@"erreur de creation du dossier %@ %@",pathCible,[erreur localizedDescription]);
    }
    while (OK)
    {
        //Reset du Range1 (?)
        MyRangeDebut1.location = NSNotFound; 
        
        
        //On teste une première fois si le buffer contient le premier paramètre <div id="mnuScroll">
        MyRangeDebut1 = [buffer rangeOfString:[arrayRecherche objectAtIndex:0] options:NSCaseInsensitiveSearch];

        //Si non :
        if ((MyRangeDebut1.location == NSNotFound) && ([buffer rangeOfString:[arrayRecherche objectAtIndex:1] options:NSCaseInsensitiveSearch].location == NSNotFound))
        {
            //On cherche la balise mnuScroll, nextH, et startBtn avec la typographie qui va bien

            for (NSUInteger i = 0;i<[arrayRecherche count];i++)
            {
                MyRangeDebut1 = [buffer rangeOfString:[arrayRecherche objectAtIndex:i]  options:NSCaseInsensitiveSearch];
                if (MyRangeDebut1.location == NSNotFound)
                {
                    OK = NO;
                }
                //Dès qu'on l'a trouvée, on se barre de la boucle
                else 
                {
                    OK = YES;
                    break;
                }

            }
            //On a donc trouvé la bonne typo
            if (OK)
            {
                //On récupère l'url de link des balises :
                MyRangeDebut2 = [buffer rangeOfString:@"href=\"" options:NSCaseInsensitiveSearch range:NSMakeRange(MyRangeDebut1.location,[buffer length]-MyRangeDebut1.location)];
                MyRangeDebut3 = [buffer rangeOfString:@"\""  options:NSCaseInsensitiveSearch range:NSMakeRange(MyRangeDebut2.location+MyRangeDebut2.length,[buffer length]-MyRangeDebut2.location-MyRangeDebut2.length)];            
                
                //Super useless
                //[buffer substringWithRange:NSMakeRange(MyRangeDebut2.location+MyRangeDebut2.length,MyRangeDebut3.location - MyRangeDebut2.location - MyRangeDebut2.length )];
                
                //On stocke l'URL dans monURL1
                [monURL1 setString:[buffer substringWithRange:NSMakeRange(MyRangeDebut2.location+MyRangeDebut2.length,MyRangeDebut3.location - MyRangeDebut2.location - MyRangeDebut2.length )]];
                
                
                //On retape le lien en enlevant les escapes Web (%20 entre autres)
                [monURL1 replaceOccurrencesOfString:@"\%20" withString:@" " options:NSCaseInsensitiveSearch range:NSMakeRange(0,[monURL1 length])];
                
                //On chope le chemin physique absolu du lien
                [monURLpath1 setString:[[monURLpath stringByDeletingLastPathComponent] stringByAppendingPathComponent:monURL1]];
                
                //On place dans buffer tout le contenu de cette page contenant le menu
                [buffer setString:[[NSString alloc]
                                   initWithContentsOfFile:monURLpath1
                                   encoding:NSUTF8StringEncoding
                                   error:&error]];
            }
        }
        //Si 
        else
        {
            if (OK) 
            {
                //Path + monURL1 dans le dossier Menu
                NSString * fichierCible = [pathCible stringByAppendingPathComponent:monURL1];
                NSString * resultat = [self traitementHTML:monURLpath1 l:fichierCible];
                
                if (resultat)
                {    
                    //?
                    [monURL1 setString:resultat];
                    
                    //On vire les probables espaces
                    [monURL1 replaceOccurrencesOfString:@"\%20" withString:@" " options:NSCaseInsensitiveSearch range:NSMakeRange(0,[monURL1 length])];
                    
                    //Ajout à la liste des liens
                    [listeDesFichiers addObject:[[NSString alloc] initWithString:monURL1]];
                    [monURLpath1 setString:[[monURLpath1 stringByDeletingLastPathComponent] stringByAppendingPathComponent:monURL1]];
                    if ([[NSFileManager defaultManager] fileExistsAtPath:monURLpath1])
                    {
                    }
                    [buffer setString:[[NSString alloc]
                     initWithContentsOfFile:monURLpath1
                     encoding:NSUTF8StringEncoding
                     error:&error]];
                    //[buffer setString:[[NSString alloc] initWithContentsOfFile:monURLpath1 encoding:NSUTF8StringEncoding error:&error]];
                    
                    
                    //}
                    
                }
                else
                {
                    OK = NO;
                    break;
                }
                
            }
        }
        
    }    
    
    
    
    return listeDesFichiers;
    
}

-(NSString *)traitementHTML:(NSString *)fichierSource l:(NSString *)fichierCible
{
    DLog(@"");
    
    //Check des variables bien remplies
    NSMutableString * monURL1 = [[NSMutableString alloc] init];
    bool status = false;
    if (!fichierSource) 
        return monURL1;
    if (!fichierCible) 
        return monURL1;
    
    //copie les fichier dans un dossier parametre
    
    
    NSError *error;
    
    NSString *stringFromFileAtPath = [[NSString alloc]
                                      initWithContentsOfFile:fichierSource
                                      encoding:NSUTF8StringEncoding
                                      error:&error];
    
    if (stringFromFileAtPath == nil) 
    {
        
        // an error occurred
        
        NSLog(@"Error reading file at %@\n%@",
              
              fichierSource, [error localizedFailureReason]);
        
        // implementation continues ...
        
    } 
    
    BOOL OK = YES;
    NSMutableString * myBuffer = [[NSMutableString alloc] initWithString:stringFromFileAtPath];
    NSRange RangeDebut2;
    NSRange RangeDebut3;
    NSRange RangeDebut1 = [myBuffer rangeOfString:@"<a class=\"nextBtn\"" options:NSCaseInsensitiveSearch];
    
    if (RangeDebut1.location == NSNotFound)
    {
        RangeDebut1 = [myBuffer rangeOfString:@"<a class=\" nextBtn\""  options:NSCaseInsensitiveSearch];
        if (RangeDebut1.location == NSNotFound)
        {
            RangeDebut1 = [myBuffer rangeOfString:@"class=\"nextBtn \""  options:NSCaseInsensitiveSearch];
            if (RangeDebut1.location == NSNotFound)
            {
                
                RangeDebut1 = [myBuffer rangeOfString:@"<a class=\"navBtn nextBtn"  options:NSCaseInsensitiveSearch];
                if (RangeDebut1.location == NSNotFound)
                {
                    RangeDebut1 = [myBuffer rangeOfString:@"class=\"nextBtn\""  options:NSCaseInsensitiveSearch];
                    if (RangeDebut1.location == NSNotFound)
                    {
                        OK = NO;
                        monURL1 = nil;
                    }
                }
            }
        }
    }
    
    if (OK)
    {
        RangeDebut2 = [myBuffer rangeOfString:@"href=\"" options:NSCaseInsensitiveSearch range:NSMakeRange(RangeDebut1.location,[myBuffer length]-RangeDebut1.location)];
        RangeDebut3 = [myBuffer rangeOfString:@"\""  options:NSCaseInsensitiveSearch range:NSMakeRange(RangeDebut2.location+RangeDebut2.length,[myBuffer length]-RangeDebut2.location-RangeDebut2.length)];            
        
        [myBuffer substringWithRange:NSMakeRange(RangeDebut2.location+RangeDebut2.length,RangeDebut3.location - RangeDebut2.location - RangeDebut2.length )];
        
        [monURL1 setString:[myBuffer substringWithRange:NSMakeRange(RangeDebut2.location+RangeDebut2.length,RangeDebut3.location - RangeDebut2.location - RangeDebut2.length )]];
        
        [monURL1 replaceOccurrencesOfString:@"\%20" withString:@" " options:NSCaseInsensitiveSearch range:NSMakeRange(0,[monURL1 length])];
    }
    
    
    /////////////
    
    BOOL ok = NO;
    NSUInteger debut = 0;
    
    NSMutableString * buffer = [[NSMutableString alloc] initWithString:stringFromFileAtPath]; 
    
    //On va chercher la Div Scroll, pour choper tout ce qu'il y a à l'intérieur et sortir un vieux xml
    NSRange MyRangeDebut1 = [buffer rangeOfString:@"<div id=\"mnuScroll\">" options:NSCaseInsensitiveSearch];
    NSRange MyRangeFin1   = NSMakeRange(0,0);
    NSRange MyRangeDebut2 = [buffer rangeOfString:@"<body>" options:NSCaseInsensitiveSearch];
    NSRange MyRangeFin2   = [buffer rangeOfString:@"</body>" options:NSCaseInsensitiveSearch];
    
    if (MyRangeDebut1.location == NSNotFound )
        MyRangeDebut1 = [buffer rangeOfString:@"<div class=\"mnuFra\">" options:NSCaseInsensitiveSearch];
    if (MyRangeDebut1.location != NSNotFound )
    {
        debut = MyRangeDebut1.location;
        ok = YES;
        
        //Merci le nom des variables
        NSUInteger loc;
        NSUInteger len;
        NSString * toto;
        NSString * toto1;
        NSString * toto2;
        
        NSString * titi; 
        loc = MyRangeDebut1.location  /* + MyRangeDebut1.length */ ;
        MyRangeFin1 = [self rechercheFin:buffer rangeOfStringDebut:@"<div" rangeOfStringFin:@"</div>" debut:MyRangeDebut1];
        
        if (MyRangeFin1.location != NSNotFound )
        {
            len = MyRangeFin1.location - MyRangeDebut1.location + MyRangeFin1.length /* - MyRangeDebut1.length */;
            toto = [buffer substringWithRange:NSMakeRange(loc,len)];
            toto1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebut2.location +MyRangeDebut2.length)];
            toto2 = [buffer substringWithRange:NSMakeRange(MyRangeFin2.location,[buffer length]-MyRangeFin2.location)];
            
            titi = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>%@",toto];
            
            NSData *parseData = [titi dataUsingEncoding:NSUTF8StringEncoding];
            
            status = [parseData writeToFile:fichierCible atomically:YES];
            
        }
        else
        {
        }
    }
    
    return monURL1;
}
- (NSRange)rechercheFin:(NSMutableString *)buff 
     rangeOfStringDebut:(NSString *)rangeOfStringDebut 
       rangeOfStringFin:(NSString *)rangeOfStringFin
                  debut:(NSRange)rangeDebut 
{
    DLog(@"");
    BOOL ok = YES;
    NSRange MyRangeDebut = rangeDebut;
    NSRange MyRangeDebutLast = rangeDebut;
    NSRange MyRangeFin = NSMakeRange(0,0);
    NSRange MyRangeFinLast = [buff rangeOfString:rangeOfStringFin 
                                         options:NSCaseInsensitiveSearch 
                                           range:NSMakeRange(MyRangeDebut.location+MyRangeDebut.length,[buff length]-MyRangeDebut.location-MyRangeDebut.length)];
    
    if (MyRangeFinLast.location != NSNotFound) 
    {
        MyRangeFin = MyRangeFinLast;
    }
    else
    {
        ok = NO;
    }
    
    while (ok)
    {
        MyRangeDebutLast = [buff rangeOfString:rangeOfStringDebut 
                                       options:NSCaseInsensitiveSearch 
                                         range:NSMakeRange(MyRangeDebut.location+MyRangeDebut.length,MyRangeFin.location-MyRangeDebut.location-MyRangeDebut.length)];
        if (MyRangeDebutLast.location == NSNotFound) 
        {
            return MyRangeFin;
        }
        else
        {
            MyRangeDebut =  MyRangeDebutLast;
            MyRangeFinLast = [buff rangeOfString:rangeOfStringFin 
                                         options:NSCaseInsensitiveSearch 
                                           range:NSMakeRange(MyRangeFin.location+MyRangeFin.length,[buff length]-MyRangeFin.location-MyRangeFin.length)];
            
            if (MyRangeFinLast.location != NSNotFound) 
            {
                MyRangeFin = MyRangeFinLast; 
            }
            else
            {
                ok = NO;
            }    
        }
        
        if ((MyRangeDebutLast.location == NSNotFound) || (MyRangeFinLast.location == NSNotFound)) 
        {
            return MyRangeFin;
        }
        
    }
    
    return MyRangeFin;
}

@end