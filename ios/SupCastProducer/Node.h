//
//  Node.h
//  AbstractTree
//
//  Created by Cape EMN on 01/12/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Node;

@interface Node : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * idecran;
@property (nonatomic, retain) NSSet *children;
@property (nonatomic, retain) Node *parent;
@end

@interface Node (CoreDataGeneratedAccessors)

- (void)addChildrenObject:(Node *)value;
- (void)removeChildrenObject:(Node *)value;
- (void)addChildren:(NSSet *)values;
- (void)removeChildren:(NSSet *)values;
@end
