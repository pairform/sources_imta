//
//  VersionFormatter.h
//  SupCastProducer
//
//  Created by Cape EMN on 04/01/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VersionFormatter : NSFormatter

@end
