//
//  Balise.m
//  SupCastProducer
//
//  Created by Cape EMN on 02/02/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "Balise.h"

@implementation Balise


@synthesize indiceBalise;
@synthesize indiceFichier;
@synthesize locationDebut;
@synthesize locationFin;
@synthesize longueurDebut;
@synthesize longueurFin;
@synthesize texte;
@synthesize type;

@end
