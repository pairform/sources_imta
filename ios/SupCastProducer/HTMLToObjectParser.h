//
//  HTMLToObjectParser.h
//  SupCastProducer
//
//  Created by Cape EMN on 14/02/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Balise.h"


@interface HTMLToObjectParser : NSObject  <NSXMLParserDelegate> {
NSDictionary * dicoInfo;

}

@property (nonatomic,strong) NSDictionary * dicoInfo;

-(id)initWithDico:(NSDictionary*)dico;
-(NSArray *)eclate:(NSString *)nomFichierIndex;
-(NSString *)traitementHTML:(NSString *)fichierSource l:(NSString *)fichierCible;

- (NSRange)rechercheFin:(NSMutableString *)buffer 
     rangeOfStringDebut:(NSString *)rangeOfStringDebut 
       rangeOfStringFin:(NSString *)rangeOfStringFin
                  debut:(NSRange)rangeDebut;

@end
