//
//  CreationFichierMedia.m
//  SupCastProducer
//
//  Created by CAPE - EMN on 02/03/11.
//  Copyright 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionFichier.h"
#import "CreationFichierMedia.h"
#import <WebKit/WebKit.h>

NSString *AppDataDownloadCompleted = @"AppDataDownloadCompleted";

@implementation CreationFichierMedia

@synthesize arrayReplace;
@synthesize arrayDIVID;
@synthesize arrayMainContent;
@synthesize fichierPubli ;
@synthesize tempPath;
@synthesize serveur;
@synthesize ressourcePath;
@synthesize fichierIcone;
@synthesize theme;
@synthesize etablissement;
@synthesize nomRessource;
@synthesize catalogue;
@synthesize titreRessource;
@synthesize description;
@synthesize pathcible;
@synthesize mediaspath;
@synthesize version1;
@synthesize version2;
@synthesize version3;
@synthesize idRessource;
@synthesize customCSS;

//Recuperation du CSS
-(id)initWithCss:(NSString *)stringCustomCSS
{
    [self setCustomCSS:stringCustomCSS];
    self = [super init];
    
    return self;
}

-(void) listeMediaWithDico:(NSDictionary * )infoRessource
{
    DLog(@"");
	//------------------------------------------------------------
    //Taleau de tous les médias de la ressource
	NSMutableArray * monArrayMedia = [[NSMutableArray alloc] init];
    //Default FM
    NSFileManager * fileManager =[NSFileManager defaultManager];
    
    //Variables de gestion d'erreur
    NSString * dossierWeb;
    NSError * erreur;
    
    //Verif de ressource non-nulle
    if (infoRessource)
    {   
        //Pour chaque 
        
        dossierWeb = [[NSString alloc] initWithString:[infoRessource objectForKey:kDossierWeb]];
        self.fichierPubli = [[NSString alloc] initWithString:[infoRessource objectForKey:kFichierPubli]];
        self.tempPath = [[NSString alloc] initWithString:[infoRessource objectForKey:kPathCible]];
        self.serveur = [[NSString alloc] initWithString:[infoRessource objectForKey:kURLdeDepot]];        
        self.ressourcePath = [[NSString alloc] initWithString:[infoRessource objectForKey:kDossierRessource]];
        self.fichierIcone = [[NSString alloc] initWithString:[infoRessource objectForKey:kFichierIcone]];
        self.theme = [[NSString alloc] initWithString:[infoRessource objectForKey:kTheme]];
        self.etablissement = [[NSString alloc] initWithString:[infoRessource objectForKey:kEtablissement]];
        self.nomRessource = [[NSString alloc] initWithString:[infoRessource objectForKey:kNomRessource]];
        self.description = [[NSString alloc] initWithString:[infoRessource objectForKey:kDescription]];
        self.titreRessource = [[NSString alloc] initWithString:[infoRessource objectForKey:kTitreRessource]];
        self.catalogue = [[NSString alloc] initWithString:[infoRessource objectForKey:kCatalogue]];
        self.version1 = [[NSString alloc] initWithString:[infoRessource objectForKey:kVersion]];
        self.version2 = [[NSString alloc] initWithString:[infoRessource objectForKey:kVersion2]];
        self.version3 = [[NSString alloc] initWithString:[infoRessource objectForKey:kVersion3]];
        self.idRessource = [[NSString alloc] initWithString:[infoRessource objectForKey:kId]];

    }
    
    //Liste des extensions prises en charges
    // Récupérer ces extensions à partir de la base SQL
    //Par l'intermédiaire du fichier Paramètres.pList
    NSString * fichierParametres = [[NSBundle mainBundle]  pathForResource:@"Parametres" ofType:@"plist"];
    NSDictionary * monDico =[[NSDictionary alloc] initWithContentsOfFile:fichierParametres];
    self.arrayDIVID =[[NSArray alloc] initWithArray:[monDico objectForKey:@"arrayDIVID"]];
    self.arrayMainContent =[[NSArray alloc] initWithArray:[monDico objectForKey:@"arrayMainContent"]];
    
    //Liste des formats 
    //Compatibles
    NSArray * arrayExt = [[NSArray alloc] initWithArray:[monDico objectForKey:@"arrayExtOK"]];
    //Incompatibles
    NSArray * arrayExtKO = [[NSArray alloc] initWithArray:[monDico objectForKey:@"arrayExtKO"]];
    //Dangereux
    NSArray * arrayExtWarning = [[NSArray alloc] initWithArray:[monDico objectForKey:@"arrayExtWarning"]];
    //HTML à remplacer.
    self.arrayReplace = [[NSArray alloc] initWithArray:[monDico objectForKey:@"arrayReplace"]];

	NSString *file;
	
    NSMutableArray * arrayFichier =[[NSMutableArray alloc] init];
	self.pathcible = [NSString pathWithComponents:[NSArray arrayWithObjects:self.tempPath,self.ressourcePath,nil]]; 
    //Creation du dossier cible
    
	if (![fileManager createDirectoryAtPath:self.pathcible withIntermediateDirectories:YES  attributes:nil  error:&erreur])
    {
        DLog(@"erreur de creation du dossier %@ %@",self.pathcible,[erreur localizedDescription]);
    }
    //Copie du fichier icône dans le dossier cible
    NSString * fichierIconeCible = [self.pathcible stringByAppendingPathComponent:[self.fichierIcone lastPathComponent]];
    if (![fileManager copyItemAtPath:self.fichierIcone toPath:fichierIconeCible error:&erreur])
    {
        DLog(@"erreur de copie de %@ vers %@ %@",
             self.fichierIcone,
             fichierIconeCible,
             [erreur localizedDescription]); 
    }
    
    //Création du dossier des fichiers medias  
	self.mediaspath = [self.pathcible stringByAppendingPathComponent:kWebPath]; 
    
	if (![fileManager createDirectoryAtPath:self.mediaspath withIntermediateDirectories:YES  attributes:nil  error:&erreur])
    {
        DLog(@"erreur de création du dossier %@ %@",
             self.mediaspath,
             [erreur localizedDescription]); 
    }
    
	BOOL isDir;
	NSString * fichierSource; 
	NSString * fichierCible; 
    BOOL topEcriture = NO;
    
    
    
    NSMutableString * fichierWarning = [[NSMutableString alloc] initWithString:@""];
    NSString * pathFichierWarning = [self.pathcible stringByAppendingPathComponent:@"fichierWarning.txt"];
    
    NSMutableString * fichierKO = [[NSMutableString alloc] initWithString:@""];
    NSString * pathFichierKO = [self.pathcible stringByAppendingPathComponent:@"fichierKO.txt"];
    
    NSMutableString * fichierInconnu = [[NSMutableString alloc]  initWithString:@""];
    NSString * pathFichierInconnu = [self.pathcible stringByAppendingPathComponent:@"fichierInconnu.txt"];
    
    
    
    
    //Enumération et copie des fichiers médias
	NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:dossierWeb];
	while ((file = [dirEnum nextObject])) 
	{
        NSString * filePathExtension = [[NSString alloc] initWithString:[file pathExtension]];
        NSString * fileLastPathComponent = [[NSString alloc] initWithString:[file lastPathComponent]];
        fichierSource = [NSString pathWithComponents:[NSArray arrayWithObjects:dossierWeb,file,nil]];
		fichierCible = [NSString pathWithComponents:[NSArray arrayWithObjects:self.mediaspath,file,nil]];
		
        // la path du fichier existe et ne correspond pas à un dossier
        if (([fileManager fileExistsAtPath:fichierSource isDirectory:&isDir]) && (!isDir))
		{
            // l'extension du fichier est dans le tableau des extensions
			if ([arrayExtWarning containsObject:filePathExtension])
			{
                [fichierWarning appendString:@"\n fichier Warning :"];
                [fichierWarning appendString:file];
                
                topEcriture = YES;
            }
            else if ([arrayExtKO containsObject:filePathExtension])
			{
                [fichierKO appendString:@"\n fichier KO :"];
                [fichierKO appendString:file];
                
                if ([filePathExtension isEqualToString:@"flv"])
                {
                   topEcriture = YES;                }
                else 
                {
                    
                    topEcriture = NO;
                }
            }
            else if ([arrayExt containsObject:filePathExtension])
            {                
                // le nom de fichier n'est utilisé pour une autre référence
                if (![arrayFichier containsObject:fileLastPathComponent])
                {
                    topEcriture = YES;
                }
                else
                {                    
                    // le nom de fichier n'est utilisé pour une autre référence
                    NSString * fichierHomonyme =  [[NSString alloc] initWithString:[[monArrayMedia objectAtIndex:[arrayFichier indexOfObject:fileLastPathComponent]] objectForKey:@"reference"]];
                    
                    // le contenu du fichier ne correspond pas au fichier référencé : les fichiers ne sont pas équivalents
                    if (![fileManager contentsEqualAtPath:fichierHomonyme andPath:fichierSource]) 
                    {       
                        topEcriture = YES;
                    }
                    
                    //***** Cas spécial : si on traite les includes d'Adobe Edge, pour l'HTML 5, on va passer
                    //outre cette anti-redondance, parce que les .html ont besoin de leurs includes dans leur dossier.
                    
                    if([file rangeOfString:@"edge_includes"].location != NSNotFound)
                        topEcriture = YES;
                }   
            }
            else
            {
                [fichierInconnu appendString:@"\n fichier inconnu :"];
                [fichierInconnu appendString:file];
                topEcriture = NO;
            }    
        }
        
        [[fichierWarning dataUsingEncoding:NSUTF8StringEncoding] writeToFile:pathFichierWarning atomically:YES];
        [[fichierKO dataUsingEncoding:NSUTF8StringEncoding] writeToFile:pathFichierKO atomically:YES];
        [[fichierInconnu dataUsingEncoding:NSUTF8StringEncoding] writeToFile:pathFichierInconnu atomically:YES];
        
        if (topEcriture)
        {
                        
            if ([filePathExtension isEqualToString:@"flv"])
            {

                //Ajout du nom de fichier dans le tableau liste des fichiers medias
                [arrayFichier addObject:[[fileLastPathComponent stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"]];
                
                //Ajout du dico d'informations du media dans le tableau liste des dicos medias
                [monArrayMedia addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                                          [[file stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"],kCheminRelatif,
                                          [[fichierSource stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"],kReference,
                                          [NSString pathWithComponents:[[NSArray alloc] initWithObjects:kWebPath,[[file stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"],nil]],kUrl,
                                          [[fileLastPathComponent stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"],kLastPathComponent,
                                          @"mp4",kExtension,nil]];
            }
            else
            {            
            //Ajout du nom de fichier dans le tableau liste des fichiers medias
            [arrayFichier addObject:fileLastPathComponent];
            
            //Ajout du dico d'informations du media dans le tableau liste des dicos medias
            [monArrayMedia addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                                      file,kCheminRelatif,
                                      fichierSource,kReference,
                                      [NSString pathWithComponents:[[NSArray alloc] initWithObjects:kWebPath,file,nil]],kUrl,
                                      fileLastPathComponent,kLastPathComponent,
                                      filePathExtension,kExtension,nil]];
            }
            //Redimensionnement des fichiers images jpg, png et jpeg
            
            //Traitement des fichiers html
            if (([filePathExtension isEqualToString:@"htm"]) ||
                ([filePathExtension isEqualToString:@"html"]) )
            {
                [self traitementHTML:fichierSource l:fichierCible l1:filePathExtension];    
            }
            //fichierSource,fichierCible,filePathExtension
            
            //DONE: Intégration du CSS custom dans transf.css, qui est le css présent dans n'importe quelle page
            
            if ([[fichierSource lastPathComponent] isEqualToString:@"transf.css"])
            {
                //Appel de ma fonction qui déchire des poneys
                BOOL reussi = [self customiserCSS:fichierSource pathToCible:fichierCible];
                
                if(!reussi)
                {
                    NSAlert * alert = [[NSAlert alloc ] init];
                    [alert setAlertStyle:NSWarningAlertStyle];
                    [alert setMessageText:@"Impossible de customiser le CSS"];
                    [alert runModal];
                }
            }
            
            //Copie du fichier media dans le dossier WebPath
            if (![fileManager fileExistsAtPath:fichierCible]) 
            { 
                NSString * dossierFichierCible =[fichierCible stringByDeletingLastPathComponent];
                BOOL exists = [fileManager fileExistsAtPath:dossierFichierCible isDirectory:&isDir];
                if (!exists)
                {
                    if(![fileManager createDirectoryAtPath:dossierFichierCible withIntermediateDirectories:YES attributes:nil error:&erreur])
                    {
                        DLog(@"erreur de creation du dossier %@ %@",dossierFichierCible,[erreur localizedDescription]);
                    }
                }
                else
                {
                    // existe
                    if (isDir)
                    {
                    }
                    else 
                    {
                    }    
                }
                
                if (![fileManager copyItemAtPath:fichierSource toPath:fichierCible error:&erreur])
                {
                    DLog(@"Erreur de copie de %@ vers %@ %@",fichierSource,fichierCible,[erreur localizedDescription]); 
                }
            }
        }
	}
    
    NSString * fichierRessourcesWebPlist = [self.pathcible stringByAppendingPathComponent:kRessourcesWebPlist];    
	if (![monArrayMedia writeToFile:fichierRessourcesWebPlist atomically:YES])
    {
        DLog(@"erreur creation fichier %@" ,fichierRessourcesWebPlist);
    }
    
    
    
    //NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc]
    //                                   initWithDateFormat:@"%Y-%m-%d" allowNaturalLanguage:NO] autorelease];
    //NSDate *myDate = [NSDate date];
    //NSString *stringRessourceUpdated = [dateFormatter stringFromDate:myDate];
	//NSString * stringRessourceReleased  = [dateFormatter stringFromDate:myDate];
    
	//creation de l'entry
    
    //////////
    
    
}
#pragma mark -
#pragma mark Customisation du fichier CSS

-(BOOL)customiserCSS:(NSString *)pathToTransfCss pathToCible:(NSString *)pathToTransfCssCible
{
    //Recup du fichier dans un string
    
    
    NSError * error;
    
    NSMutableString * transfCssTemp = [[NSMutableString alloc] initWithContentsOfFile:pathToTransfCss encoding:NSUTF8StringEncoding error:&error];
    
    if(error)
    {
        DLog(@"Warning : Erreur d'ouverture de transf.css");
        return NO;
    }
    
    //Append de mon CSS
    
    [transfCssTemp appendString:@"\n\n"];
    [transfCssTemp appendString:customCSS];
    
    //Enregistrement de l'ensemble dans transf.css
    
    
    NSData *parseData = [transfCssTemp dataUsingEncoding:NSUTF8StringEncoding];
    if ([parseData writeToFile:pathToTransfCssCible atomically:YES])
    {
        return YES;
    }
    else 
    {
        DLog(@"Warning : Erreur dans l'écriture du fichier transf.css");
        return NO;   
    }
    
}

#pragma mark -

CGSize reSize(NSString * w,NSString * h)
{
    DLog(@"");
    CGFloat width = 0;
    CGFloat height = 0;
    
    if (!w) 
        goto bail;
    if (!h) 
        goto bail;
    
    float maximum = 310.0f;
    width = [w floatValue];
    height = [h floatValue];
    
    if (width < maximum && height < maximum) 
    {
        goto bail;
    }
    else
    {    
        
        CGFloat ratio = width / height;
        
        if (width > maximum)
        {
            width = maximum;
            height = width / ratio;
        }
        
        if (height > maximum) 
        {
            height = maximum;
            width = height * ratio;
        }
    }    
    // Get image width, height. We'll use the entire image.
    
bail:
    return CGSizeMake(width,height);
    
}

CGSize getSize(NSString * fichier)
{
    DLog(@"");
    
    CGFloat width = 0;
    CGFloat height = 0;
    
    if (!fichier) 
        goto bail;
    
    //copie les fichier dans un dossier parametre
    CFMutableDataRef _t_data_ref = (__bridge_retained CFMutableDataRef)[[NSData alloc] initWithContentsOfFile:fichier];
    
    CGDataProviderRef imgDataProvider = CGDataProviderCreateWithCFData(_t_data_ref);
    CGImageRef sourceImage;    
    
    if ([[fichier pathExtension] isEqualToString:@"jpg"])
        sourceImage = CGImageCreateWithJPEGDataProvider(imgDataProvider,                                                                                                                                                                                             NULL,                                                                                                                                                                                             YES,kCGRenderingIntentDefault);
    else if ([[fichier pathExtension] isEqualToString:@"png"]) 
        sourceImage = CGImageCreateWithPNGDataProvider(imgDataProvider,                                                                                                                                                                                             NULL,                                                                                                                                                                                             YES,kCGRenderingIntentDefault);
    else if ([[fichier pathExtension] isEqualToString:@"jpeg"]) 
        sourceImage = CGImageCreateWithJPEGDataProvider(imgDataProvider,                                                                                                                                                                                             NULL,                                                                                                                                                                                             YES,kCGRenderingIntentDefault);
    
    
    // Get image width, height. We'll use the entire image.
    width = CGImageGetWidth(sourceImage);
    height = CGImageGetHeight(sourceImage);
    
bail:
    return CGSizeMake(width,height);
    
}
bool resizeImage(NSString * fichierSource,NSString * fichierCible,NSString * extension)
{
    DLog(@"");
    
    bool status = false;
    
    if (!fichierSource) 
        return status;
    if (!fichierCible) 
        return status;
    
    //copie les fichier dans un dossier parametre
    CFMutableDataRef _t_data_ref = (__bridge_retained CFMutableDataRef)[[NSData alloc] initWithContentsOfFile:fichierSource];
    
    CGDataProviderRef imgDataProvider = CGDataProviderCreateWithCFData(_t_data_ref);
    CGImageRef cibleImage;
    CGImageRef sourceImage;    
    
    
    if ([extension isEqualToString:@"jpg"])
        sourceImage = CGImageCreateWithJPEGDataProvider(imgDataProvider,                                                                                                                                                                                             NULL,                                                                                                                                                                                             YES,kCGRenderingIntentDefault);
    else if ([extension isEqualToString:@"png"]) 
        sourceImage = CGImageCreateWithPNGDataProvider(imgDataProvider,                                                                                                                                                                                             NULL,                                                                                                                                                                                             YES,kCGRenderingIntentDefault);
    else if ([extension isEqualToString:@"jpeg"]) 
        sourceImage = CGImageCreateWithJPEGDataProvider(imgDataProvider,                                                                                                                                                                                             NULL,                                                                                                                                                                                             YES,kCGRenderingIntentDefault);
    
    float maximum = 480.0f;
    /********************************/
    // Create the bitmap context
    CGContextRef    context = NULL;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    // Get image width, height. We'll use the entire image.
    CGFloat width = CGImageGetWidth(sourceImage);
    CGFloat height = CGImageGetHeight(sourceImage);
    
    
    if (width < maximum && height < maximum) 
    {
        cibleImage = sourceImage;
        return status;
    }
    else
    {    
        
        CGFloat ratio = width / height;
        
        if (width > maximum)
        {
            width = maximum;
            height = width / ratio;
        }
        
        if (height > maximum) 
        {
            height = maximum;
            width = height * ratio;
        }
        
        CGRect rect = CGRectMake(0,0,width, height);
        
        // Declare the number of bytes per row. Each pixel in the bitmap in this
        // example is represented by 4 bytes; 8 bits each of red, green, blue, and
        // alpha.
        bitmapBytesPerRow   = (width * 4);
        bitmapByteCount     = (bitmapBytesPerRow * height);
        
        // Allocate memory for image data. This is the destination in memory
        // where any drawing to the bitmap context will be rendered.
        bitmapData = malloc( bitmapByteCount );
        if (bitmapData == NULL)
        {
            cibleImage =  nil;
            return status;
        }
        else
        {
            // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
            // per component. Regardless of what the source image format is
            // (CMYK, Grayscale, and so on) it will be converted over to the format
            // specified here by CGBitmapContextCreate.
            CGColorSpaceRef colorspace = CGImageGetColorSpace(sourceImage);
            context = CGBitmapContextCreate (bitmapData,width,height,8,bitmapBytesPerRow,
                                             colorspace,kCGImageAlphaNoneSkipFirst);
            CGColorSpaceRelease(colorspace);
            
            if (context == NULL)
                // error creating context
            {
                cibleImage =  nil;
                return status;
            }
            else
            {
                
                // Draw the image to the bitmap context. Once we draw, the memory
                // allocated for the context for rendering will then contain the
                // raw image data in the specified color space.
                CGContextDrawImage(context, rect, sourceImage);
                
                cibleImage = CGBitmapContextCreateImage(context);
                CGContextRelease(context);
                free(bitmapData);
            }
        }
    }
    /********************************/
    NSURL * url=[NSURL fileURLWithPath:fichierCible];
    
    if ([extension isEqualToString:@"jpg"])
        status = CGWriteToURL( cibleImage,(__bridge CFURLRef) url , kUTTypeJPEG);
    else if ([extension isEqualToString:@"png"]) 
        status = CGWriteToURL( cibleImage,(__bridge CFURLRef) url , kUTTypePNG);              
    else if ([extension isEqualToString:@"jpeg"]) 
        status = CGWriteToURL( cibleImage,(__bridge CFURLRef) url , kUTTypeJPEG);              
    
    return status;
}

#pragma mark  - Balise object



-(bool)traitementHTML:(NSString *)fichierSource
                    l:(NSString *)fichierCible 
                   l1:(NSString *)extension
{
    DLog(@"");
    
    bool status = false;
    
    if (!fichierSource) 
        goto bail;
    if (!fichierCible) 
        goto bail;
    
    //Si on essaie de traiter un dossier Web, genre une iFrame HTML5, on fait rien.
    if([fichierCible rangeOfString:@".eWeb"].location != NSNotFound)
        return status; //= goto bail = exit de la fonction.
    //copie les fichier dans un dossier parametre
    if (([extension isEqualToString:@"html"]) || ([extension isEqualToString:@"htm"]))
    {
        
        
        NSError *error;
        
        NSString *stringFromFileAtPath = [[NSString alloc]
                                          initWithContentsOfFile:fichierSource
                                          encoding:NSUTF8StringEncoding
                                          error:&error];
        
        if (stringFromFileAtPath == nil) 
        {
            
            // an error occurred
            
            NSLog(@"Error reading file at %@\n%@",
                  
                  fichierSource, [error localizedFailureReason]);
            
            // implementation continues ...
            
        } 
        /////////////
        
        BOOL ok = NO;
        NSUInteger debut = 0;
        
        //On ajoute quelques métas et un autre truc aussi
        NSMutableString * buffer = [[NSMutableString alloc] initWithString:stringFromFileAtPath]; 
        for (NSArray * tableau in self.arrayReplace) 
        {
            [buffer replaceOccurrencesOfString:[tableau objectAtIndex:0] 
                                    withString:[tableau objectAtIndex:1]
                                       options:NSCaseInsensitiveSearch 
                                         range:NSMakeRange(0,[buffer length])];
        }
        
        //////////////////////
        // Extraction de la partie principale de la page mainContent
        //////////////////////
        
        
        NSRange debutMainContentRange = [buffer rangeOfString:[self.arrayMainContent objectAtIndex:0] options:NSCaseInsensitiveSearch];
        //Si on a une balise <div class="mainContent ">
        if (debutMainContentRange.location == NSNotFound)
        {   
            //Sinon, <div class=" mainContent">,<div class=" mainContent ">,<div class="mainContent">
            for (NSUInteger i = 1;i<[self.arrayMainContent count];i++)
            {
                debutMainContentRange = [buffer rangeOfString:[self.arrayMainContent objectAtIndex:i]  options:NSCaseInsensitiveSearch];
                if (debutMainContentRange.location != NSNotFound)
                {
                    break;
                }
                
            }
        }
        
        
        //Si ya vraiment pas de mainContent, on enregistre pas et on se barre
        if (debutMainContentRange.location == NSNotFound) goto bail;
        NSRange debutBodyRange = [buffer rangeOfString:@"<body>" options:NSCaseInsensitiveSearch];
        NSRange finBodyRange   = [buffer rangeOfString:@"</body>" options:NSCaseInsensitiveSearch];
        
        if (debutMainContentRange.location != NSNotFound )
        {
            //debut = debutMainContentRange.location;
            ok = YES;
            
            NSUInteger loc;
            NSUInteger len;
            NSString * mainContent;
            NSString * head;
            NSString * bodyHtmlfinal;
            
            loc = debutMainContentRange.location  /* + MyRangeDebut1.length */ ;
            NSRange finMainContentRange = [self rechercheFin:buffer rangeOfStringDebut:@"<div" rangeOfStringFin:@"</div>" debut:debutMainContentRange];
            
            if (finMainContentRange.location != NSNotFound)
            {
                len = finMainContentRange.location - debutMainContentRange.location /* - MyRangeDebut1.length */;
                //Contient tout le mainContent, sans les balises body
                mainContent = [buffer substringWithRange:NSMakeRange(loc,len)];
                head = [buffer substringWithRange:NSMakeRange(0,debutBodyRange.location +debutBodyRange.length)];
                bodyHtmlfinal = [buffer substringWithRange:NSMakeRange(finBodyRange.location,[buffer length]-finBodyRange.location)];
                
                buffer = [[NSMutableString alloc] initWithFormat:
                             @"%@%@%@",
                             head,
                             mainContent, 
                             bodyHtmlfinal];
                
            }
        }
        
        
        
        //////////////////////
        // Mise en forme d'une vidéo
        //////////////////////
        
        /*
        BOOL OK = YES;
        
        while (OK) 
        {
            NSRange debutDivVideo = [buffer rangeOfString:@"<object class=\"resVideo"];
            NSRange finDivVideo;
            NSString * avantDivVideo = [[NSString alloc] initWithString:@""];
            NSString * apresDivVideo = [[NSString alloc] initWithString:@""];
            NSString * entreDivVideo = [[NSString alloc] initWithString:@""];
            
            NSMutableString * lienVideo = [[NSMutableString alloc] initWithString:@""];
            
            //Si on a une vidéo dans la page
            if(debutDivVideo.location != NSNotFound)
            {
                //On récupère les ranges qui serviront à extraire l'url de la vidéo
                finDivVideo = [buffer rangeOfString:@"</object>" 
                                            options:NSCaseInsensitiveSearch 
                                              range:NSMakeRange(debutDivVideo.location + debutDivVideo.length,[buffer length]-debutDivVideo.location-debutDivVideo.length)];
                
                //On divise le texte de la page, pour reconstruire la page par la suite
                avantDivVideo = [buffer substringWithRange:NSMakeRange(0,debutDivVideo.location)];
                apresDivVideo = [buffer substringWithRange:NSMakeRange(finDivVideo.location+finDivVideo.length,[buffer length]-finDivVideo.location- finDivVideo.length)];
                entreDivVideo = [buffer substringWithRange:NSMakeRange(debutDivVideo.location,finDivVideo.location- debutDivVideo.location + finDivVideo.length)];
                
                NSRange avantLink = [buffer rangeOfString:@"<param name=\"FlashVars\" value=\"flv=" options:NSCaseInsensitiveSearch range:NSMakeRange(debutDivVideo.location, entreDivVideo.length)];
                NSRange apresLink = [buffer rangeOfString:@"&amp;" options:NSCaseInsensitiveSearch range:NSMakeRange(avantLink.location, 120)];
                
                [lienVideo setString:[buffer substringWithRange:NSMakeRange((avantLink.location + avantLink.length), (apresLink.location - (avantLink.location +avantLink.length)))]];
                
                //Si on a réussi à extraire le lien
                if(![lienVideo isEqualToString:@""])
                {
                    //On check que le chemin relatif est correct.
                    //Sachant que le répertoire actuel est WebPath/co/…, on veut un ../res/… pour atterrir dans WebPath/res.
                    //Quelques fois, le chemin de l'objet flv est ../../res/…, d'où le contrôle.
                    
                    if([lienVideo rangeOfString:@"../.."].location != NSNotFound)
                        [lienVideo replaceOccurrencesOfString:@"../.." withString:@".." options:NSCaseInsensitiveSearch range:NSMakeRange(0, 5)];
                    
                    [buffer setString:[[NSString alloc] initWithFormat: @"%@<video controls src=\"%@\"></video>%@",avantDivVideo,lienVideo,apresDivVideo]];
                }
                else OK = NO;
                
            }
            else OK = NO;
        }
        */
        
        //////////////////////
        // Balise Object
        //////////////////////
        BOOL top = YES;
        BOOL OK = YES;
        
        NSRange MyRangeDebutObject = [buffer rangeOfString:@"<object " options:NSCaseInsensitiveSearch];
        NSRange MyRangeFinObject;
        
        top = YES;
        OK = YES;
        
        while (OK)
        {
            NSString * object1;
            NSString * object2;
            NSString * object3;
            if (MyRangeDebutObject.location == NSNotFound )
            {
                OK = NO;
                break;
            }
            else
            {
                
                MyRangeFinObject = [buffer rangeOfString:@"</object>" 
                                                 options:NSCaseInsensitiveSearch 
                                                   range:NSMakeRange(MyRangeDebutObject.location+MyRangeDebutObject.length,[buffer length]-MyRangeDebutObject.location-MyRangeDebutObject.length)];
                
                
                if (MyRangeFinObject.location != NSNotFound) 
                {
                    object1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebutObject.location)];
                    object2 = [buffer substringWithRange:NSMakeRange(MyRangeFinObject.location+MyRangeFinObject.length,[buffer length]-MyRangeFinObject.location- MyRangeFinObject.length)];
                    object3 = [buffer substringWithRange:NSMakeRange(MyRangeDebutObject.location,MyRangeFinObject.location- MyRangeDebutObject.location + MyRangeFinObject.length)];
                    
                    
                    NSString * url;  
                    
                    // cas du mp3
                    NSRange MyRangeDebutMP3 = [object3  rangeOfString:@"value=\"mp3=" 
                                                              options:NSCaseInsensitiveSearch];
                    if (MyRangeDebutMP3.location != NSNotFound) 
                    {
                        NSRange MyRangeFinMP3 = [object3 rangeOfString:@"&amp;" 
                                                               options:NSCaseInsensitiveSearch
                                                                 range:NSMakeRange(MyRangeDebutMP3.location+MyRangeDebutMP3.length,[object3 length]-MyRangeDebutMP3.location-MyRangeDebutMP3.length)];
                        
                        if (MyRangeFinMP3.location != NSNotFound) 
                        {
                            url = [object3 substringWithRange:NSMakeRange(MyRangeDebutMP3.location+MyRangeDebutMP3.length,MyRangeFinMP3.location- MyRangeDebutMP3.location - MyRangeDebutMP3.length)];
                            
                            [buffer setString:[[NSString alloc] initWithFormat:
                                               @"%@<audio controls><source src=\"%@\" /></audio>%@",
                                               object1,
                                               url,
                                               object2]];
                            
                            
                            top = NO;
                        }
                    }
                    else 
                    {
                        
                        NSRange MyRangeFlash = [object3  rangeOfString:@"type=\"application/x-shockwave-flash\"" 
                                                               options:NSCaseInsensitiveSearch];
                        
                        
                        if (MyRangeFlash.location != NSNotFound) 
                        {
                            
                            NSRange MyRangeDebutSWF = [object3  rangeOfString:@"param name=\"FlashVars\"" 
                                                                      options:NSCaseInsensitiveSearch];
                            if (MyRangeDebutSWF.location != NSNotFound) 
                            {
                                
                                
                                NSRange MyRangeDebutSWF1 = [object3  rangeOfString:@"value=\"flv=" 
                                                                           options:NSCaseInsensitiveSearch];
                                if (MyRangeDebutSWF1.location != NSNotFound) 
                                {
                                    
                                    NSRange MyRangeFinSWF = [object3 rangeOfString:@"&amp;" 
                                                                           options:NSCaseInsensitiveSearch
                                                                             range:NSMakeRange(MyRangeDebutSWF1.location+MyRangeDebutSWF1.length,[object3 length]-MyRangeDebutSWF1.location-MyRangeDebutSWF1.length)];
                                    
                                    if (MyRangeFinSWF.location != NSNotFound) 
                                    {
                                        url = [object3 substringWithRange:NSMakeRange(MyRangeDebutSWF1.location+MyRangeDebutSWF1.length,MyRangeFinSWF.location- MyRangeDebutSWF1.location - MyRangeDebutSWF1.length)];
                                        if (([[url pathExtension] isEqualToString:@"swf"]) || 
                                            ([[url pathExtension] isEqualToString:@"flv"]))
                                        {
                                            /*
                                            [buffer setString:[[NSString alloc] initWithFormat:
                                                               @"%@<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\"><param name=\"width\" value=\"160\"><param name=\"height\" value=\"90\"><param name=\"src\" value=\"%@\"><param name=\"controller\" value=\"true\"><param name=\"autoplay\" value=\"false\"><embed cache=\"true\" controller=\"true\" autoplay=\"false\" src=\"%@\"></object>%@",
                                                               object1,                                                               
                                                               [@"../res/" stringByAppendingPathComponent:[[[url lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"]],
                                                               [@"../res/" stringByAppendingPathComponent:[[[url lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"]],
                                                               object2]];
                                             */
                                            [buffer setString:[[NSString alloc] initWithFormat:
                                                               @"%@<video controls style=\"width:100\%; margin:1em 0em;\" src=\"%@\"></video>%@",
                                                               object1,                                                               
                                                               [@"../res/" stringByAppendingPathComponent:[[[url lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"mp4"]],
                                                               object2]];
                                            
                                            
                                        }
                                        else
                                        {
                                            /*
                                            [buffer setString:[[NSString alloc] initWithFormat:
                                                               @"%@<object width=\"360\" height=\"340\" classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\"><param name=\"width\" value=\"160\"><param name=\"height\" value=\"90\"><param name=\"src\" value=\"%@\"><param name=\"controller\" value=\"true\"><param name=\"autoplay\" value=\"false\"><embed width=\"360\" height=\"340\" cache=\"true\" controller=\"true\" autoplay=\"false\" src=\"%@\"></object>%@",
                                                               object1,                                                               
                                                               url,
                                                               url,
                                                               object2]];
                                            */
                                            
                                            if([url rangeOfString:@"../.."].location != NSNotFound)
                                                url = [url stringByReplacingOccurrencesOfString:@"../.." withString:@".."];
                                            
                                            [buffer setString:[[NSString alloc] initWithFormat:
                                                               @"%@<video controls style=\"width:100\%; margin:1em 0em;\" src=\"%@\"></video>%@",
                                                               object1,                                                               
                                                               url,
                                                               object2]];
                                            
                                            
                                            
                                        }
                                        top = NO;
                                    }
                                    
                                }
                                
                            }
                        }    
                        
                        if (top)
                        {    
                            [buffer setString:[[NSString alloc] initWithFormat:
                                               @"%@%@",
                                               object1,
                                               object2]];
                        }
                    }
                    
                }
                
                else
                {
                    ok = NO;
                    break;
                }
            }
            if (([buffer length]) > (MyRangeDebutObject.location + [object3 length] + MyRangeDebutObject.length))
            {
                MyRangeDebutObject = [buffer rangeOfString:@"<object " options:NSCaseInsensitiveSearch range:NSMakeRange(MyRangeDebutObject.location + MyRangeDebutObject.length,[buffer length] - MyRangeDebutObject.location - [object3 length]- MyRangeDebutObject.length)];
            }
            else
            {
                ok = NO;
                break;
            }
        }
        
        
        
        
        //////////////////////
        // Mise en forme d'une Animation HTML5 
        //////////////////////
        
        OK = YES;
        
        NSRange debutDivAnim = [buffer rangeOfString:@"<div class=\"eWeb\" style=\"position:relative;"];
        NSRange finDivAnim;
        NSString * avantDivAnim = [[NSString alloc] initWithString:@""];
        NSString * apresDivAnim = [[NSString alloc] initWithString:@""];
        
        //Style CSS à greffer sur l'élement 
        //NSString * paramsEnPlus = [[NSString alloc] initWithString:@"margin:0px -30px; box-shadow: 0 0 5px black; background-color:white; border-radius:10px"];
        
        NSMutableString * paramStyleAnim = [[NSMutableString alloc] initWithString:@""];
        
        while (OK) 
        {
            //Si on a un HTML5 dans la page
            if(debutDivAnim.location != NSNotFound)
            {
                //On cherche la fin des paramètres css (1ere occurence depuis la fin de debutDivAnim jusqu'à la fin du document)
                finDivAnim = [buffer rangeOfString:@";\">" 
                                            options:NSCaseInsensitiveSearch 
                                              range:NSMakeRange(debutDivAnim.location + debutDivAnim.length,[buffer length]-debutDivAnim.location-debutDivAnim.length)];
                
                //On divise le texte de la page, pour reconstruire la page par la suite
                avantDivAnim = [buffer substringWithRange:NSMakeRange(0,debutDivAnim.location)];
                apresDivAnim = [buffer substringWithRange:NSMakeRange(finDivAnim.location+finDivAnim.length,[buffer length]-finDivAnim.location- finDivAnim.length)];
                
                //Typiquement, paramStyleAnim = @"height:***px;width:***px;";
                [paramStyleAnim setString:[buffer substringWithRange:NSMakeRange(debutDivAnim.location,finDivAnim.location- debutDivAnim.location + finDivAnim.length)]];
                
                //On va ajouter un flag juste pour avertir que la balise a déjà été traitée par cette boucle.
                [paramStyleAnim replaceCharactersInRange:NSMakeRange(17, 1) withString:@" title:\"Animation\" "];
                
                
                //Si on a réussi à extraire les paramètres
                if(![paramStyleAnim isEqualToString:@""] && ([paramStyleAnim rangeOfString:@"max-width:"].location == NSNotFound))
                {
                    //On va transformer paramStyleAnim 
                    [paramStyleAnim replaceCharactersInRange:[paramStyleAnim rangeOfString:@"width:"] withString:@"max-width:"];
                    
                    [buffer setString:[[NSString alloc] initWithFormat: @"%@%@%@",avantDivAnim,paramStyleAnim,apresDivAnim]];
                }
                else OK = NO;
            }
            else OK = NO;
        }
        
         
        //TODO:Enlever le onclick qui affiche l'image en hover.
        //TODO:Faire que toute image soit zoomable.
        
        //On va l'enlever dans chaque balise <a> qui porte la classe imgZoom, propre à Scenari
        
        //Si on a une image
        

        //On check si elle est entourée d'une balise <a>
        //Pour ça, on regarde juste après la balise img si ya un </a>
        //On part du principe que si yen a une, c'est forcément pour le zoom
        
        //Si oui,        //On recupère le src de l'image
        //On retrouve la balise en ré-analysant toute la page à la recherche d'un href=src
        //On va supprimer l'ensemble de la balise.
        
        
        //On recréé une balise <a target="_blank" href="src">
        //On nest notre balise image dedans
        //Et on refait la page propre.
        
        
        OK = YES;
        
        //DONE:Zapper les images liées au thèmes
        //FIXED:Eviter de traiter le logo scenari
        //FIXME:Mettre la balise <a> en place
        //Solution : traiter uniquement le contenu de la page, au lieu de se taper tout le reste.
        //Voir dernier bloc de code de la fonction
        
        
        while (OK) 
        {
            NSRange debutDivImg = [buffer rangeOfString:@"<img src"];
            NSRange finDivImg;
            
            NSRange debutDivA;
            NSRange finDivA;
            
            NSMutableString * divImg = [[NSMutableString alloc] initWithString:@""];
            NSString * avantDivImg = [[NSString alloc] initWithString:@""];
            NSString * apresDivImg = [[NSString alloc] initWithString:@""];
            
            //Variable pour faire les ranges proprement
            unsigned long debutRange;
            unsigned long longueurRange;
            NSRange tempRange;
        
            //Si on a une image
            if(debutDivImg.location != NSNotFound)
            {                
                debutRange = debutDivImg.location + debutDivImg.length;
                longueurRange = [buffer length]-debutDivImg.location-debutDivImg.length; 
                tempRange = NSMakeRange(debutRange, longueurRange);
                finDivImg = [buffer rangeOfString:@">" 
                                           options:NSCaseInsensitiveSearch 
                                             range:tempRange];
                
                //On stocke la balise image
                
                debutRange = debutDivImg.location;
                longueurRange = (finDivImg.location + finDivImg.length) - debutDivImg.location; 
                tempRange = NSMakeRange(debutRange, longueurRange);
                
                [divImg setString:[buffer substringWithRange:tempRange]];
                
                //On modifie la balise img avec un attribut bidon, pour alerter la boucle que l'image a été traitée.
                NSString * divImgFinal = [divImg stringByReplacingOccurrencesOfString:@"src" withString:@"tagged src"];
                
                //On recupère le src de l'image
                NSRange imgSrcDebut = [divImg rangeOfString:@"src=\""];
                
                debutRange = imgSrcDebut.location + imgSrcDebut.length;
                longueurRange = divImg.length - debutRange; 
                tempRange = NSMakeRange(debutRange, longueurRange);
                
                NSRange imgSrcFin = [divImg rangeOfString:@"\"" options:NSCaseInsensitiveSearch range:tempRange];
                
                debutRange = imgSrcDebut.location + imgSrcDebut.length;
                longueurRange = imgSrcFin.location - debutRange; 
                tempRange = NSMakeRange(debutRange, longueurRange);
                
                //ATTENTION - Si ya un zoom le imgSrc est de forme ../res/nom_image_1.xxx et ya deux images, une entière et une miniature
                NSString * imgSrc = [divImg substringWithRange:tempRange];
                
                //On check si elle est entourée d'une balise <a>
                //Pour ça, on regarde juste après la balise img si ya un </a>
                //On part du principe que si yen a une, c'est forcément pour le zoom
                //On va chercher dans les 15 caractères suivant la div img, au cas où il y ait une <span> qui traîne
                
                debutRange = finDivImg.location + finDivImg.length;
                longueurRange = 15; 
                tempRange = NSMakeRange(debutRange, longueurRange);
                
                
                finDivA = [buffer rangeOfString:@"</a>" 
                                          options:NSCaseInsensitiveSearch 
                                            range:tempRange];
                //Si oui,        
                //ATTENTION - Si ya un zoom le imgSrc est de forme ../res/nom_image_1.xxx et ya deux images, une entière et une miniature
                //Et le src de la balise sera sous forme ../res/nom_image.xxx
                if(finDivA.location != NSNotFound)
                {                
                    //On récupère l'url de l'image d'origine (sans le _1)
                    NSString * aSrc = [[NSString alloc] initWithFormat:@"%@%@",[imgSrc substringToIndex:[imgSrc length]-6], [imgSrc substringFromIndex:[imgSrc length]-4]];
                    
                    //On retrouve la balise en ré-analysant les 250 caractères derrière la balise <img à la recherche d'un <a target="_blank" class="imgZoom" href=src
                    debutDivA = [buffer rangeOfString:[[NSString alloc] initWithFormat:@"<a target=\"_blank\" class=\"imgZoom\" %@%@",@"href=\"",aSrc] options:NSCaseInsensitiveSearch range:NSMakeRange(debutDivImg.location - 250, 250)];
                    
                    //On divise le texte de la page, pour reconstruire la page par la suite autour de la balise a
                    avantDivImg = [buffer substringWithRange:NSMakeRange(0,debutDivA.location)];
                    apresDivImg = [buffer substringWithRange:NSMakeRange(finDivA.location+finDivA.length,[buffer length]-finDivA.location- finDivA.length)];
                    
                    //On recréé une balise <a target="_blank" href="src">
                    //On nest notre balise image dedans
                    
                    NSString * nouvelleBalise = [[NSString alloc] initWithFormat:@"%@%@%@%@%@",@"<a href='",aSrc,@"'>",divImgFinal,@"</a>"];
                    //Et on refait la page propre.
                    [buffer setString:[[NSString alloc] initWithFormat: @"%@%@%@",avantDivImg,nouvelleBalise,apresDivImg]];
                    
                }
                else
                //Si non,
                {
                    //On divise le texte de la page, pour reconstruire la page par la suite autour de la balise img
                    avantDivImg = [buffer substringWithRange:NSMakeRange(0,debutDivImg.location)];
                    apresDivImg = [buffer substringWithRange:NSMakeRange(finDivImg.location+finDivImg.length,[buffer length]-finDivImg.location- finDivImg.length)];
                    //On recréé une balise <a target="_blank" href="src">
                    //On nest notre balise image dedans
                    
                    NSString * nouvelleBalise = [[NSString alloc] initWithFormat:@"%@%@%@%@%@",@"<a href='",imgSrc,@"'>",divImgFinal,@"</a>"];
                    //Et on refait la page propre.
                    [buffer setString:[[NSString alloc] initWithFormat: @"%@%@%@",avantDivImg,nouvelleBalise,apresDivImg]];
                }
            }
            else OK = NO;
        }
        
        /*
        
        //////////////////////
        
        
        //////////////////////
        // Balise <img 
        
        top = YES;
        OK = YES;
        
        NSRange MyRangeDebutImg = [buffer rangeOfString:@"<img " options:NSCaseInsensitiveSearch];
        NSRange MyRangeFinImg;
        NSString * textImg1;
        NSString * textImg2;
        NSString * textImg3;
        
        while (OK)
        {
            if (MyRangeDebutImg.location == NSNotFound )
            {
                OK = NO;
                break;
            }
            else
            {
                
                MyRangeFinImg = [buffer rangeOfString:@">" 
                                              options:NSCaseInsensitiveSearch 
                                                range:NSMakeRange(MyRangeDebutImg.location+MyRangeDebutImg.length,[buffer length]-MyRangeDebutImg.location-MyRangeDebutImg.length)];
                
                
                if (MyRangeFinImg.location != NSNotFound) 
                {
                    textImg1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebutImg.location)];
                    textImg2 = [buffer substringWithRange:NSMakeRange(MyRangeFinImg.location+MyRangeFinImg.length,[buffer length]-MyRangeFinImg.location- MyRangeFinImg.length)];
                    textImg3 = [buffer substringWithRange:NSMakeRange(MyRangeDebutImg.location,MyRangeFinImg.location- MyRangeDebutImg.location + MyRangeFinImg.length)];
                    
                    NSString * urlImage;  
                    NSString * myWidth;  
                    NSString * myHeight;  
                    CGSize mySize = CGSizeMake(0,0);
                    
                    // recherche width
                    NSRange MyRangeDebutWidth = [textImg3  rangeOfString:@"width=\"" 
                                                                 options:NSCaseInsensitiveSearch];
                    if (MyRangeDebutWidth.location != NSNotFound) 
                    {
                        NSRange MyRangeFinWidth = [textImg3 rangeOfString:@"\"" 
                                                                  options:NSCaseInsensitiveSearch
                                                                    range:NSMakeRange(MyRangeDebutWidth.location+MyRangeDebutWidth.length,[textImg3 length]-MyRangeDebutWidth.location-MyRangeDebutWidth.length)];
                        
                        if (MyRangeFinWidth.location != NSNotFound) 
                        {
                            
                            myWidth = [textImg3 substringWithRange:NSMakeRange(MyRangeDebutWidth.location+MyRangeDebutWidth.length,MyRangeFinWidth.location- MyRangeDebutWidth.location - MyRangeDebutWidth.length)];
                            
                            // recherche height
                            NSRange MyRangeDebutHeight = [textImg3  rangeOfString:@"height=\"" 
                                                                          options:NSCaseInsensitiveSearch];
                            if (MyRangeDebutHeight.location != NSNotFound) 
                            {
                                NSRange MyRangeFinHeight = [textImg3 rangeOfString:@"\"" 
                                                                           options:NSCaseInsensitiveSearch
                                                                             range:NSMakeRange(MyRangeDebutHeight.location+MyRangeDebutHeight.length,[textImg3 length]-MyRangeDebutHeight.location-MyRangeDebutHeight.length)];
                                
                                if (MyRangeFinHeight.location != NSNotFound) 
                                {
                                    
                                    myHeight = [textImg3 substringWithRange:NSMakeRange(MyRangeDebutHeight.location+MyRangeDebutHeight.length,MyRangeFinHeight.location- MyRangeDebutHeight.location - MyRangeDebutHeight.length)];
                                    mySize = reSize(myWidth,myHeight);
                                }
                            }
                        }
                    }
                    
                    
                    // cas du img
                    NSRange MyRangeDebutFichierImage = [textImg3  rangeOfString:@"src=\"" 
                                                                        options:NSCaseInsensitiveSearch];
                    if (MyRangeDebutFichierImage.location != NSNotFound) 
                    {
                        NSRange MyRangeFinFichierImage = [textImg3 rangeOfString:@"\"" 
                                                                         options:NSCaseInsensitiveSearch
                                                                           range:NSMakeRange(MyRangeDebutFichierImage.location+MyRangeDebutFichierImage.length,[textImg3 length]-MyRangeDebutFichierImage.location-MyRangeDebutFichierImage.length)];
                        
                        if (MyRangeFinFichierImage.location != NSNotFound) 
                        {
                            urlImage = [textImg3 substringWithRange:NSMakeRange(MyRangeDebutFichierImage.location+MyRangeDebutFichierImage.length,MyRangeFinFichierImage.location- MyRangeDebutFichierImage.location - MyRangeDebutFichierImage.length)];
                            
                            
                            //CGSize mySize = getSize([self.pathcible stringByAppendingPathComponent:urlImage]);
                            
                            
                            if (mySize.width != 0)
                            {    
                                
                                [buffer setString:[[NSString alloc] initWithFormat:
                                                   @"%@<img src=\"%@\" width=\"%dpx\" height=\"%dpx\">%@",textImg1,urlImage,[[[NSNumber alloc] initWithFloat:mySize.width] intValue],[[[NSNumber alloc] initWithFloat:mySize.height] intValue],textImg2]];
                            }
                            
                        }
                    }
                    else 
                    {
                        
                    }
                    
                }
                
                else
                {
                    ok = NO;
                    break;
                }
            }
            if (([buffer length]) > (MyRangeDebutImg.location + [textImg3 length] + MyRangeDebutImg.length))
            {
                MyRangeDebutImg = [buffer rangeOfString:@"<img " options:NSCaseInsensitiveSearch range:NSMakeRange(MyRangeDebutImg.location + MyRangeDebutImg.length,[buffer length] - MyRangeDebutImg.location - [textImg3 length]- MyRangeDebutImg.length)];
            }
            else
            {
                ok = NO;
                break;
            }
        }
        */
        
        //////////////////////
        //////////////////////////////////////////////////
        // lien vers *_flv.html 
        //////////////////////////////////////////////////
        
        /*BOOL*/ top = YES;
        /*BOOL*/ OK = YES;
        
        
        NSRange MyRangeDebutBalise = [buffer rangeOfString:@"<a " options:NSCaseInsensitiveSearch];
        NSRange MyRangeFinBalise;
        while (OK)
        {
            
            NSString * flvhtml1;    //html avant le <a>
            NSString * flvhtml2;    //html entre le <a></a>
            NSString * flvhtml3;    //html après le </a>
            if (MyRangeDebutBalise.location == NSNotFound )
            {
                OK = NO;
                break;
            }
            else
            {
                
                MyRangeFinBalise = [buffer rangeOfString:@"</a>" 
                                                 options:NSCaseInsensitiveSearch 
                                                   range:NSMakeRange(MyRangeDebutBalise.location+MyRangeDebutBalise.length,[buffer length]-MyRangeDebutBalise.location-MyRangeDebutBalise.length)];
                
                
                if (MyRangeFinBalise.location != NSNotFound) 
                {
                    
                    flvhtml1 = [buffer substringWithRange:NSMakeRange(0,MyRangeDebutBalise.location)];
                    flvhtml2 = [buffer substringWithRange:NSMakeRange(MyRangeFinBalise.location+MyRangeFinBalise.length,[buffer length]-MyRangeFinBalise.location- MyRangeFinBalise.length)];
                    flvhtml3 = [buffer substringWithRange:NSMakeRange(MyRangeDebutBalise.location,MyRangeFinBalise.location- MyRangeDebutBalise.location + MyRangeFinBalise.length)];
                    
                }                
                
                //On cherche l'URL dans l'onClick
                NSRange MyRangeDebutLien = [flvhtml3 rangeOfString:@"onclick=\"window.open(&quot;" 
                                                           options:NSCaseInsensitiveSearch];
                
                if (MyRangeDebutLien.location == NSNotFound) 
                {
                    
                    MyRangeDebutLien = [flvhtml3 rangeOfString:@"onclick=\"window.open(\"" 
                                                       options:NSCaseInsensitiveSearch];
                }
                
                if (MyRangeDebutLien.location != NSNotFound) 
                {
                    
                    NSMutableString * stringURL = [[NSMutableString alloc] init];  
                    
                    // cas du mp3
                    NSRange MyRangeFinLien = [flvhtml3 rangeOfString:@"&quot;" 
                                                             options:NSCaseInsensitiveSearch
                                                               range:NSMakeRange(MyRangeDebutLien.location+MyRangeDebutLien.length,[flvhtml3 length]-MyRangeDebutLien.location-MyRangeDebutLien.length)];
                    if (MyRangeFinLien.location == NSNotFound) 
                    {
                        MyRangeFinLien = [flvhtml3 rangeOfString:@"\"" 
                                                         options:NSCaseInsensitiveSearch
                                                           range:NSMakeRange(MyRangeDebutLien.location+MyRangeDebutLien.length,[flvhtml3 length]-MyRangeDebutLien.location-MyRangeDebutLien.length)];
                    }
                    
                    if (MyRangeFinLien.location != NSNotFound) 
                    {
                        [stringURL setString:[flvhtml3 substringWithRange:NSMakeRange(MyRangeDebutLien.location+MyRangeDebutLien.length,MyRangeFinLien.location- MyRangeDebutLien.location - MyRangeDebutLien.length)]];
                        
                        [stringURL setString:[[NSString alloc] initWithFormat:@"../res/%@",stringURL]];
                        
                        if ([stringURL replaceOccurrencesOfString:@"_flv.html" withString:@".mp4" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [stringURL length])] > 0)
                        {
                            /*
                            [buffer setString:[[NSString alloc] initWithFormat:
                                               @"%@<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\"><param name=\"src\" value=\"%@\"><param name=\"width\" value=\"160\"><param name=\"height\" value=\"90\"><param name=\"controller\" value=\"true\"><param name=\"autoplay\" value=\"false\"><embed width=\"360\" height=\"340\" cache=\"true\" controller=\"true\" autoplay=\"false\" src=\"%@\"></object>%@",
                                               flvhtml1,                                                               
                                               stringURL,
                                               stringURL,
                                               flvhtml2]];
                            
                            */
                            
                            [buffer setString:[[NSString alloc] initWithFormat:
                                               @"%@<video controls style\"width:100\%; margin:1em 0em;\" src=\"%@\"></video>%@",
                                               flvhtml1,                                                               
                                               stringURL,
                                               flvhtml2]];

                            
                            
                        }
                        else 
                        {
                            //Si, malgré tout, on tombe sur un fichier flash, on écrit ça dans l'application pour repérer les flashs.
                            NSRange MyRangeFlash = [stringURL rangeOfString:@"_swf.html" options:NSCaseInsensitiveSearch];
                            if (MyRangeFlash.location != NSNotFound) 
                            {
                                
                                [buffer setString:[[NSString alloc] initWithFormat:
                                                   @"%@<div style=\"font-size:20px; display:block; background-color:red;\">Ressource Flash indisponible</div>%@",
                                                   flvhtml1,                                                               
                                                   flvhtml2]];
                            }
                            
                            NSRange MyRangeGIF = [stringURL rangeOfString:@"_gif.html" options:NSCaseInsensitiveSearch];
                            if (MyRangeGIF.location != NSNotFound) 
                            {
                                
                                [buffer setString:[[NSString alloc] initWithFormat:
                                                   @"%@%@",
                                                   flvhtml1,                                                               
                                                   flvhtml2]];
                            }
                        }
                        top = YES;
                    }
                }
                
                
            }
            
            if (([buffer length]) > (MyRangeDebutBalise.location + [flvhtml3 length] + MyRangeDebutBalise.length))
            {
                MyRangeDebutBalise = [buffer rangeOfString:@"<a " options:NSCaseInsensitiveSearch range:NSMakeRange(MyRangeDebutBalise.location + MyRangeDebutBalise.length,[buffer length] - MyRangeDebutBalise.location - [flvhtml3 length]- MyRangeDebutBalise.length)];
            }
            else
            {
                ok = NO;
                break;
            }
            
            
            
        }
        
        //TODO: Trouver une solution plus clean que ce vieux patch
        //Problème: les pages scenari non-moulinées ont normalement une balise script en fin de body,
        //qui appelle la fonction tplMgr.init() => ça saute à la moulinette.
        //Ensuite, même si on appelle cette fonction, certaines versions du script tplMgr.js intègrent une gestion
        //des périphèriques mobile (avec user-agent et taille d'écran, merci Opale) qu'il fait bien chier d'overider.
        //Du coup, au lieu d'appeller cet init(), qui de toute façon, n'appelle qu'un onclick() sur toutes les balises collapse_open,
        //je fais un petit script de patch perso qu'il conviendrait de changer (oui, toi qui me lit, c'est pour toi).
        
        
        //BRICOLAGE
        //Rajout d'un appel javascript pour initialiser les blocs déroulant sur l'état "fermé"
        //Si on est en présence d'une page avec des blocs déroulant (type "Indice" & "Solution")
        //Càd qu'on a au moins une div avec comme classe "collapse_open"
        //On rajoute un petit script de bricolage qui onclick() toutes les div "collapse_open"
        //Et on le cale en mode crade tout à la fin, après le HTML, parce que quand même, on est lundi matin.
        
        //Si on est en présence d'une page avec des blocs déroulant (type "Indice" & "Solution")
        //Càd qu'on a au moins une div avec comme classe "collapse_open"
        BOOL isThereAnyCollapsingStuff = [buffer rangeOfString:@"collapse_open"].location != NSNotFound;
        
        if(isThereAnyCollapsingStuff)
        {
            //On rajoute un petit script de bricolage qui onclick() toutes les div "collapse_open"
            NSString * craftScript = [[NSString alloc] initWithString:@"<script>var array = document.getElementsByClassName('collapse_open'); while(array.length !=0) array[0].onclick();</script>"];
            
            //Et on le cale en mode crade tout à la fin, après le HTML, parce que quand même, on est lundi matin.
            [buffer appendString:craftScript];
            
        }
        
        
        //////////////////////
        // Enregistrement de la page
        //////////////////////
        
     
        NSData *parseData = [buffer dataUsingEncoding:NSUTF8StringEncoding];
        
        if ([parseData writeToFile:fichierCible atomically:YES])
        {
        }
        else
        {
            
            DLog(@"Erreur d'écriture du fichier");
        }
    }

bail:
    return status;
      
}


- (NSRange)rechercheFin:(NSMutableString *)buff 
     rangeOfStringDebut:(NSString *)rangeOfStringDebut 
       rangeOfStringFin:(NSString *)rangeOfStringFin
                  debut:(NSRange)rangeDebut 
{
    DLog(@"");
    BOOL ok = YES;
    //Début du mainContent
    NSRange MyRangeDebut = rangeDebut;
    NSRange MyRangeDebutLast = rangeDebut;
    
    //Fin du mainContent
    NSRange MyRangeFin = NSMakeRange(0,0);
    
    //Range depuis le mainContent jusqu'au dernier 
    NSRange MyRangeFinLast = [buff rangeOfString:rangeOfStringFin 
                                         options:NSCaseInsensitiveSearch 
                                           range:NSMakeRange(MyRangeDebut.location+MyRangeDebut.length,[buff length]-MyRangeDebut.location-MyRangeDebut.length)];
    
    if (MyRangeFinLast.location != NSNotFound) 
    {
        MyRangeFin = MyRangeFinLast;
    }
    else
    {
        ok = NO;
    }
    
    while (ok)
    {
        //On cherche le <div depuis la balise mainContent jusqu'à la fin
        MyRangeDebutLast = [buff rangeOfString:rangeOfStringDebut 
                                       options:NSCaseInsensitiveSearch 
                                         range:NSMakeRange(MyRangeDebut.location+MyRangeDebut.length,MyRangeFin.location-MyRangeDebut.location-MyRangeDebut.length)];
        if (MyRangeDebutLast.location == NSNotFound) 
        {
            return MyRangeFin;
        }
        else
        {
            MyRangeDebut =  MyRangeDebutLast;
            MyRangeFinLast = [buff rangeOfString:rangeOfStringFin 
                                         options:NSCaseInsensitiveSearch 
                                           range:NSMakeRange(MyRangeFin.location+MyRangeFin.length,[buff length]-MyRangeFin.location-MyRangeFin.length)];
            
            if (MyRangeFinLast.location != NSNotFound) 
            {
                MyRangeFin = MyRangeFinLast; 
            }
            else
            {
                ok = NO;
            }    
        }
    }
    
    return MyRangeFin;
}

//*******************************************************************//
//*******************************************************************//
//*******************************************************************//
bool CGWriteToURL(CGImageRef imageSource,CFURLRef absURL, CFStringRef typeName)
{
    DLog(@"");
    bool status = false;
    
    // Make sure the image exists before continuing.
    if (imageSource==nil){
        goto bail;
    }
    CGImageRetain(imageSource);
    
    // making the options dictionary
    float compression = 1.0; // Lossless compression if available.
    int orientation = 4; // Origin is at bottom, left
    CFStringRef myKeys[3];
    CFTypeRef   myValues[3];
    CFDictionaryRef myOptions = NULL;
    myKeys[0] = kCGImagePropertyOrientation;
    myValues[0] = CFNumberCreate(NULL, kCFNumberIntType, &orientation);
    myKeys[1] = kCGImagePropertyHasAlpha;
    myValues[1] = kCFBooleanTrue;
    myKeys[2] = kCGImageDestinationLossyCompressionQuality;
    myValues[2] = CFNumberCreate(NULL, kCFNumberFloatType, &compression);
    myOptions = CFDictionaryCreate( NULL, (const void **)myKeys, (const void **)myValues, 3,&kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    // Release the CFNumber and CFDictionary objects when you no longer need them.   
    
    // Create a URL image destination for the given image type,
    // for one image, with no options.
    CGImageDestinationRef imageDest;
    //imageDest=CGImageDestinationCreateWithURL(absURL,typeName, 1, myOptions);
    imageDest=CGImageDestinationCreateWithURL(absURL,typeName, 1, NULL);
    
    // Make sure the image destination exists before continuing.
    if (imageDest==nil) {
        goto bail;
    } else {
        puts("imageDest created");
    }
    
    // Add the image to the destination using previously saved options.
    //CGImageDestinationAddImage(imageDest, imageSource, myOptions);
    CGImageDestinationAddImage(imageDest, imageSource, NULL);
    puts("image added");
    // Finalize the image destination.
    status = CGImageDestinationFinalize(imageDest);
    // Release the image as soon as you no longer need it.
    CGImageRelease(imageSource);
    
bail:
    return status;
}

-(void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
    DLog(@"");
}

-(void)webView:(WebView *)sender resource:(id)identifier didFinishLoadingFromDataSource:(WebDataSource *)dataSource
{
    
    DLog(@"");
    
    DOMDocument * document = [[sender mainFrame] DOMDocument];
    
    if (document != nil)
    {
        DOMNode * root =[[document documentElement] firstChild];
        // skip any text nodes before the first value node
        while (root.nodeName) 
        {
            if ([root.nodeName isEqualToString:@"BODY"])
            {
            }
            root = [root nextSibling];
            
        }
        
        DOMNodeList * domNodeList = [document getElementsByTagName:@"DIV"];
        
        
        NSMutableArray * domElemsToRemove = [[NSMutableArray alloc] init];
        if ( [domNodeList length] > 0)
        {
            for (int i = 0;i< [domNodeList length];i++) 
            {
                [domElemsToRemove addObject:[domNodeList item:i]];
            }
            for (DOMElement * domElement in domElemsToRemove)
            {
                
                if ( domElement.nodeType == DOM_ELEMENT_NODE ) 
                {
                    if (([domElement.nodeName isEqualToString:@"DIV"]) && domElement.hasAttributes)
                    {
                        DOMAttr * attr= [domElement getAttributeNode:@"id"];
                        NSString * text= [attr textContent];
                        if (
                            [self.arrayDIVID containsObject:text]
                            )
                        {
                            [[domElement parentNode] removeChild:domElement];
                        }
                    }
                }
                
            }
            
            
            NSString * file = [[sender mainFrameURL] lastPathComponent];
            
            NSString * fichierCible;
            fichierCible = [NSString pathWithComponents:[NSArray arrayWithObjects:self.mediaspath,file,nil]];
            
            
        }
    }	
    
    
}

@end

