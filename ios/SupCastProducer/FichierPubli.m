//
//  FichierPubli.m
//  SupCastProducer

//
//  Created by CAPE - EMN on 23/11/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "FichierPubli.h"

@implementation FichierPubli
@synthesize currentNodeContent;
@synthesize fichierXML;

- (id)parseXMLFromFile:(NSString *)file 
			parseError:(NSError **)error
{
    NSData * data = [[NSData alloc] initWithContentsOfFile:file];
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser parse];
	
	if([parser parserError] && error)
	{
		*error = [parser parserError];
	}
	[parser release];
	[data release];
	return self;
}

#pragma mark -
#pragma mark Parser Methods


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
}
- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	
}

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
	attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:kBalisespue])
    {
        if (([attributeDict objectForKey:kBalisescrefUri]) !=nil)
        {
        fichierXML = [attributeDict objectForKey:kBalisescrefUri];
        }
    }
}

- (void)parser:(NSXMLParser *)parser 
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
  //  DLog(@"");
    
    
}


- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string
{   
  //  DLog(@"");
	[self.currentNodeContent appendString:string];
}

@end
