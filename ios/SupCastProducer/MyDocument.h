//
//  MyDocument.h
//  SupCastProducer
//
//  Created by CAPE - EMN on 23/11/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>


@interface MyDocument : NSDocument <NSTextViewDelegate,NSTextFieldDelegate,NSControlTextEditingDelegate, NSComboBoxDelegate, NSTableViewDelegate, NSTableViewDataSource>
{

    NSString *path;
    int idecran;
    NSDictionary *dicoContenuFichier;
    NSMutableData *receivedData;
    NSMutableData *reponseAjoutRessource;
    NSMutableDictionary *listeThemes;
    NSMutableDictionary *listeEtablissements;
    IBOutlet NSProgressIndicator *indicator;
    int indice;
    int nombreDeFichiers;
    int compteurDeFichiers;
    int compteurErreurs;

    IBOutlet NSComboBox *comboEtablissement;
    IBOutlet NSComboBox *comboTheme;
    NSURLConnection *connectionFinaleBDD;
    NSURLConnection *connectionRessources;
    NSURLConnection *connectionListeCategorie;
    NSMutableArray * array;
    NSMutableArray * arrayListe;
    
    NSString * ressourceVisibilite;
    
    IBOutlet NSDrawer *logDrawer;
    IBOutlet NSTableView *logTableView;
    NSMutableArray * logArray;
    NSDate * chrono;
    NSMutableArray * arrayToAppendToFichierSequencesPlist;
}

- (IBAction)rechercheFichierIcone:(id)sender;
- (IBAction)rechercheFichierPubli:(id)sender;
- (IBAction)rechercheDossierWeb:(id)sender;
- (IBAction)rechercheFichierPathCible:(id)sender;
- (IBAction)connection:(id)sender;


@property (nonatomic,weak) IBOutlet NSImageView *imageIconeView;

@property (nonatomic,strong) IBOutlet NSTextView *textViewDescription;

@property (nonatomic,strong) NSMutableData *receivedData;
@property (nonatomic,strong) NSMutableDictionary *listeThemes;
@property (nonatomic,strong) NSMutableDictionary *listeEtablissements;

@property (nonatomic,strong) NSString *path;
@property (nonatomic,strong) NSDictionary *dicoContenuFichier;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldFichierPubli;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldPathCible;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldURLdeDepot;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldDossierRessource;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldFichierIcone;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldTheme;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldEtablissement;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldNomRessource;
//@property (assign) IBOutlet NSTextField *textFieldDescription;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldTitreRessource;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldCatalogue;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldDossierWeb;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldVersion;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldVersion2;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldVersion3;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldId;
@property (nonatomic,weak) IBOutlet NSComboBox *comboVisibilite;
@property (nonatomic,weak) IBOutlet NSTextField *textFieldVisibiliteDev;
@property (nonatomic,weak) IBOutlet NSButton * checkBoxUpdate;
@property (nonatomic,strong) IBOutlet NSTextView *textViewCustomCSS;

- (void)changeCombo:(NSNotification *)notification;
-(void)recupereListeCategorie;
- (IBAction)lancer:(id)sender;
- (NSString *)applicationSupportFolder;
-(void)synchroPostElgg:(NSString*)thePath 
         thePathOutPut:(NSString*)thePathOutPut 
        thePathOutPut2:(NSString*)thePathOutPut2;
- (IBAction)saveDocument:(id)sender;
-(void)recherche:(DOMNode *)noeud fichier:(NSString *)fichier;
-(NSDictionary *)rechercheNiveauInferieur:(NSArray *)array2 noeud:(NSDictionary *)noeud;
-(void)ecritureFichierSupport;
-(void)envoyerRequeteServeur:(CGFloat) tailleMega;
-(void)ouvrirTiroirLog;
-(void)toggleTiroirLog;

-(void)initLog;
-(void)addToLog:(NSString*)task:(BOOL)error;
-(void)updateLog;

-(BOOL)rechercheAnnexes;
-(NSString*)retrouverTitreAnnexe:(NSString*)pathFichierAnnexe;
-(NSString*)escapeAccents:(NSString*)stringWithHTMLAccents;

-(BOOL)controlDossierWeb;
-(BOOL)controlFichierPubli;
-(BOOL)controlPathCible;
-(BOOL)controlVersion;
-(BOOL)controlVersion2;
-(BOOL)controlVersion3;
-(BOOL)controlId;
-(BOOL)controlURLdeDepot;
-(BOOL)controlTitreRessource;
-(BOOL)controlCatalogue;
-(BOOL)controlDossierRessource;
-(BOOL)controlFichierIcone;
-(BOOL)controlTheme;
-(BOOL)controlEtablissement;
-(BOOL)controlNomRessource;
-(BOOL)controlViewDescription;

-(BOOL)controlVisibiliteDev;


@end