//
//  CreationFichierMedia.h
//  SupCastProducer
//
//  Created by CAPE - EMN on 02/03/11.
//  Copyright 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import <QuartzCore/CoreAnimation.h>   
#import <ApplicationServices/ApplicationServices.h>

@interface CreationFichierMedia : NSObject {


}

@property (nonatomic,strong) NSString * fichierPubli;
@property (nonatomic,strong) NSString * tempPath;
@property (nonatomic,strong) NSString * serveur;
@property (nonatomic,strong) NSString * ressourcePath;
@property (nonatomic,strong) NSString * fichierIcone;
@property (nonatomic,strong) NSString * theme;
@property (nonatomic,strong) NSString * etablissement;
@property (nonatomic,strong) NSString * nomRessource;
@property (nonatomic,strong) NSString * catalogue;
@property (nonatomic,strong) NSString * titreRessource;
@property (nonatomic,strong) NSString * description;
@property (nonatomic,strong) NSString * pathcible;
@property (nonatomic,strong) NSString * mediaspath;
@property (nonatomic,strong) NSString * version1;
@property (nonatomic,strong) NSString * version2;
@property (nonatomic,strong) NSString * version3;
@property (nonatomic,strong) NSString * idRessource;
@property (nonatomic,strong) NSArray * arrayMainContent;
@property (nonatomic,strong) NSArray * arrayDIVID;
@property (nonatomic,strong) NSArray * arrayReplace;
@property (nonatomic,strong) NSString * customCSS;
bool CGWriteToURL(CGImageRef image,CFURLRef absURL, CFStringRef typeName);
bool resizeImage();
bool resizeImage(NSString * fichierSource,NSString * fichierCible,NSString * extension);
CGSize getSize(NSString * fichier);
CGSize reSize(NSString * w,NSString * h);

-(id)initWithCss:(NSString *) stringCustomCSS;
-(void)listeMediaWithDico:(NSDictionary * )infoRessource;
-(bool)traitementHTML:(NSString *)fichierSource
                    l:(NSString *)fichierCible 
                   l1:(NSString *)extension;
-(NSRange)rechercheFin:(NSMutableString *)buffer 
     rangeOfStringDebut:(NSString *)rangeOfStringDebut 
       rangeOfStringFin:(NSString *)rangeOfStringFin
                  debut:(NSRange)rangeDebut; 
-(BOOL)customiserCSS:(NSString *)pathToTransfCss pathToCible:(NSString *)pathToTransfCssCible;
@end
