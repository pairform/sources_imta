//
//  ListeNewMessages.m
//  SynchroPostElgg
//
//  Created by Cape EMN on 04/11/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "ListeNewMessages.h"

@implementation ListeNewMessages




+(id)findPCR:(id) data
{


    NSMutableArray *result=[[NSMutableArray alloc] initWithCapacity:20];
    int back=1;
    if([data isKindOfClass:[NSDictionary class]])
    {
        
        for (id key in data) {
            id subDictionary = [data objectForKey:key];
            if ([subDictionary isKindOfClass:[NSString class]])
            { 
                if ([key isEqualToString:@"idecran"]) //||[key isEqualToString:@"title"]  )
                {   [result addObject:subDictionary];
                    //return  result;
                }
            }
            else if([subDictionary isKindOfClass:[NSArray class]])
            {back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:subDictionary];
                
                [result addObjectsFromArray:[self findPCR:temp]];
            }
            else if([subDictionary isKindOfClass:[NSDictionary class]])
            {back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:subDictionary];
                //[result addObjectsFromArray:[self findPCR:temp] ];
                
                [result addObjectsFromArray:[self findPCR:temp]];
            }            
            else if([subDictionary isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }            
            
                
            
            
        }
    }
    
    else
    {
        
        for (id keyObj in data) {
            
            
            
            if([keyObj isKindOfClass:[NSArray class]])
            {back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:keyObj];
                
                [result addObjectsFromArray:[self findPCR:temp]]; 
            }
            else if([keyObj isKindOfClass:[NSDictionary class]])
            {back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:keyObj];
                
                [result addObjectsFromArray:[self findPCR:temp]];
            }
            else if([keyObj isKindOfClass:[NSString class]])
            {
            }
            else if([keyObj isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }            
            
            
        }
        
    }
    
    
    if (back==1)
        return result;
    return result;
}

+(id)dataPCR:(NSString*)number forData:(id)data
{    
;

    int back=1;
    static id result=nil;
    if([data isKindOfClass:[NSDictionary class]])
    {
        
        for (id key in data) {
            id subDictionary = [data objectForKey:key];
            if ([subDictionary isKindOfClass:[NSString class]])
            { 
                if ([key isEqualToString:@"idecran"] &&[subDictionary isEqualToString:number]  )
                {   result=data;
                    return result;
                }
            }
            else if([subDictionary isKindOfClass:[NSArray class]])
            {back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:subDictionary];
                result=[self dataPCR:number forData:temp];
                
            }
            else if([subDictionary isKindOfClass:[NSDictionary class]])
            {back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:subDictionary];
                result=[self dataPCR:number forData:temp];
                
            }
            else if([subDictionary isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }            

            
        }
    }
    
    else
    {
        
        for (id keyObj in data) {
            
            
            
            if([keyObj isKindOfClass:[NSArray class]])
            {back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:keyObj];
                result= [self dataPCR:number forData:temp];
                
            }
            else if([keyObj isKindOfClass:[NSDictionary class]])
            {back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:keyObj];
                result=[self dataPCR:number forData:temp];
                
            }
            
            else if([keyObj isKindOfClass:[NSString class]])
            {
            }
            else if([keyObj isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }            
            
            
        }
        
    }
    
    
    if (back==1)
        return result;
    return result;
}


@end
