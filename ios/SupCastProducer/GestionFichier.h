//
//  GestionFichier.h
//  SupCastProducer
//
//  Created by CAPE - EMN on 02/03/11.
//  Copyright 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GestionFichier : NSObject {
	
}

-(NSString *)dataFilePath:(NSString *)fileName;
-(NSString *)dataFilePathAtDirectory:(NSString *)directory
							fileName:(NSString *)fileName;

-(NSString *)concateneDirectory:(NSString *)directory 
					   fileName:(NSString *)nomFichier;

@end
