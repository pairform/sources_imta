//
//  Balise.h
//  SupCastProducer
//
//  Created by Cape EMN on 02/02/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Balise : NSObject
{
    NSUInteger indiceFichier;
    NSUInteger indiceBalise;
    NSUInteger locationDebut;
    NSUInteger locationFin;
    NSUInteger longueurDebut;
    NSUInteger longueurFin;
    NSString * texte;
    NSString * type;
    
}

@property (nonatomic) NSUInteger indiceFichier;
@property (nonatomic) NSUInteger indiceBalise;
@property (nonatomic) NSUInteger locationDebut;
@property (nonatomic) NSUInteger locationFin;
@property (nonatomic) NSUInteger longueurDebut;
@property (nonatomic) NSUInteger longueurFin;
@property (nonatomic,retain) NSString * texte;
@property (nonatomic,retain) NSString * type;

@end
