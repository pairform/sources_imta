//
//  Node.m
//  AbstractTree
//
//  Created by Cape EMN on 01/12/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "Node.h"

@implementation Node

@dynamic name;
@dynamic idecran;
@dynamic children;
@dynamic parent;

@end
