//
//  TraceNewMessage.m
//  SupCastProducer
//
//  Created by Cape on 06/06/11.
//  Copyright 2011 Cape. All rights reserved.
//

#import "TraceNewMessage.h"

@implementation TraceNewMessage
NSMutableString * element_last;

+(NSArray *)findPCR:(id) data
{

    // tableau des resultats
    NSMutableArray *resultArray=[[NSMutableArray alloc] init];
    
    // dictionnaire idecran , title/partName/sequenceName
    NSMutableDictionary * resultDico;
    
    // indicateur back ?????
    int back=1;
    
    // Si l'objet est de type de dictionnaire on teste toutes les clés
    if([data isKindOfClass:[NSDictionary class]])
    {
        resultDico =nil;
        resultDico =[[NSMutableDictionary alloc] init];
        
        // Si l'objet est de type de dictionnaire on teste toutes les clés
        for (id key in data) {
            
            // Récuperation de la valeur pour la clé donnée
            id subDictionary = [data objectForKey:key];
            
            // Si l'objet est de type de String on ajoute la valeur au tableau
            if ([subDictionary isKindOfClass:[NSString class]])
            { 
                // Si la clé est idecran
                if ([key isEqualToString:kIdecran])
                { 
                    // Ajout de cle/valeur idecran dans le dictionnaire résultat
                    [resultDico setObject:subDictionary forKey:kIdecran];
                }
                
                // Si la clé est partName
                if ([key isEqualToString:kPartName])
                {
                    // Ajout de cle/valeur partName dans le dictionnaire résultat
                    [resultDico setObject:subDictionary forKey:kPartName];
                }
                
                // Si la clé est title
                if ([key isEqualToString:kTitle]) 
                {
                    // Ajout de cle/valeur partName dans le dictionnaire résultat
                    [resultDico setObject:subDictionary forKey:kTitle];
                }
                
                // Si la clé est sequenceName
                if ([key isEqualToString:kKeySequenceName])
                {
                    // Ajout de cle/valeur sequenceName dans le dictionnaire résultat
                    [resultDico setObject:subDictionary forKey:kKeySequenceName];
                }
                
            }
            // Si l'objet est de type de NSArray appel recursif de la methode findPCR 
            else if([subDictionary isKindOfClass:[NSArray class]])
            {   
                back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:subDictionary];
                
                NSArray * result = [[NSArray alloc] initWithArray:[self findPCR:temp]];
                if ((result != nil) && ([result count] > 0))
                {
                    [resultArray addObjectsFromArray:result];
                }
                
            }
            // Si l'objet est de type de NSDictionary appel recursif de la methode findPCR 
            else if([subDictionary isKindOfClass:[NSDictionary class]])
            {
                back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:subDictionary];
                NSArray * result = [[NSArray alloc] initWithArray:[self findPCR:temp]];
                if ((result != nil) && ([result count] > 0))
                {
                    [resultArray addObjectsFromArray:result];
                }
                
            }
            else if([subDictionary isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }
            
            
            
            
        } /* fin de for */
        
        if ((resultDico != nil) && ([resultDico count] > 0))
        {
            [resultArray addObject:resultDico];
            resultDico = nil;
        }
    }
    
    else
        // Si l'objet n'est pas de type de NSDictionary (sous entendu qu'il est de type NSArray)
    {        
        // balayage de tous les items
        for (id keyObj in data) {
            
            // Si l'item est de type NSArray
            if([keyObj isKindOfClass:[NSArray class]])
            {
                // Si l'item est de type NSArray appel recursif de la methode findPCR 
                back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:keyObj];
                NSArray * result = [[NSArray alloc] initWithArray:[self findPCR:temp]];
                if ((result != nil) && ([result count] > 0))
                {
                    [resultArray addObjectsFromArray:result];
                }
                
            }
            // Si l'item est de type NSDictionary
            else if([keyObj isKindOfClass:[NSDictionary class]])
            { 
                // Si l'item est de type NSArray appel recursif de la methode findPCR 
                back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:keyObj];
                
                NSArray * result = [[NSArray alloc] initWithArray:[self findPCR:temp]];
                if ((result != nil) && ([result count] > 0))
                {
                    [resultArray addObjectsFromArray:result];
                }
                
            }
            else if([keyObj isKindOfClass:[NSString class]])
            {
            }
            else if([keyObj isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }
            
            
            
        }
        
    }
    
    
    if (back==1)
        return resultArray;
    return resultArray;
}

+(id)dataPCR:(NSString*)number forData:(id)data
{

    int back=1;
    static id result=nil;
    if([data isKindOfClass:[NSDictionary class]])
    {
        for (id key in data) {
            id subDictionary = [data objectForKey:key];
            if ([subDictionary isKindOfClass:[NSString class]])
            { 
                if ([key isEqualToString:kIdecran] &&[subDictionary isEqualToString:number])
                {
                    result=data;
                    return result;
                }
            }
            else if([subDictionary isKindOfClass:[NSArray class]])
            {back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:subDictionary];
                result=[self dataPCR:number forData:temp];
                
            }
            else if([subDictionary isKindOfClass:[NSDictionary class]])
            {
                back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:subDictionary];
                result=[self dataPCR:number forData:temp];
            }
            else if([subDictionary isKindOfClass:[NSNumber class]])
            {
            }
            else
            {

            }

        
        }
    }
    else
    {
        for (id keyObj in data) {
            if([keyObj isKindOfClass:[NSArray class]])
            {back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:keyObj];
                result= [self dataPCR:number forData:temp];
            }
            else if([keyObj isKindOfClass:[NSDictionary class]])
            {back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:keyObj];
                result=[self dataPCR:number forData:temp];
            }
            else if([keyObj isKindOfClass:[NSString class]])
            {
            }
            else if([keyObj isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }            
            
        }
    }
    if (back==1)
        return result;
    return result;
}
+(NSArray *)findPCR2:(id) data 
      element:(NSString *)element 
 element1:(NSString *)element1 
          cas:(NSString *)cas
{
    NSMutableArray *result=[[NSMutableArray alloc] init];
    element_last = [[NSMutableString alloc] initWithString:element1];

    int back=1;
    if([data isKindOfClass:[NSDictionary class]])
    {
        for (id key in data) {
            id subDictionary = [data objectForKey:key];
            if ([subDictionary isKindOfClass:[NSString class]])
            { 
                if ([key isEqualToString:kIdecran]) 
                {
                    element_last = [NSString stringWithString:subDictionary];
                    if (![element_last isEqualToString:element])
                        [result addObject:[[NSDictionary alloc] initWithObjectsAndKeys:element,kParent,element_last,kFils,nil]];
                }
            }
            else if([subDictionary isKindOfClass:[NSArray class]])
            {back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:subDictionary];
                
                [result addObjectsFromArray:[self findPCR2:temp element:element element1:element_last cas:@"cas2"]];
            }
            else if([subDictionary isKindOfClass:[NSDictionary class]])
            {back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:subDictionary];
                
                [result addObjectsFromArray:[self findPCR2:temp element:element element1:element_last cas:@"cas3"]];
            }
            
            else if([subDictionary isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
                
            }            

            
            
            
            
        }
    }
    else
    {
        for (id keyObj in data) 
        {
            if([keyObj isKindOfClass:[NSArray class]])
            {            
                back=0;
                NSArray *temp=[[NSArray alloc] initWithArray:keyObj];
                
                [result addObjectsFromArray:[self findPCR2:temp element:element element1:element_last cas:@"cas4"]]; 
            }
            else if([keyObj isKindOfClass:[NSDictionary class]])
            {
                back=0;
                NSDictionary *temp=[[NSDictionary alloc] initWithDictionary:keyObj];
                
                [result addObjectsFromArray:[self findPCR2:temp element:element element1:element_last cas:@"cas5"]];
            }
            
            else if([keyObj isKindOfClass:[NSString class]])
            {
            }
            else if([keyObj isKindOfClass:[NSNumber class]])
            {
            }
            else
            {
            }            

            
            
            
        }
    }
    
    if (back==1)
        return result;
    return result;
    
}

+(NSArray * )suppressionDesLiensdeParenteIndirect:(NSArray * )pcrDic 
{
 
    BOOL top = NO;
    
    //pcrDic = nil;
    //pcrDic= [[NSMutableArray alloc] initWithArray:pcrDic1];
    NSMutableArray * pcrDic1 = nil;
    pcrDic1= [[NSMutableArray alloc] init];
    
    for(NSDictionary * element in pcrDic)
    {
        top = YES;
        for(NSDictionary * element1 in pcrDic)
        {
            if ( [[element objectForKey:kFils] isEqualToString:[element1 objectForKey:kFils]] 
                &&  ![[element objectForKey:kParent] isEqualToString:[element1 objectForKey:kParent]])
                
            {
                
                for(NSDictionary * element2 in pcrDic)
                {
                    if ( [[element2 objectForKey:kParent] isEqualToString:[element1 objectForKey:kParent]] 
                        && [[element2 objectForKey:kFils] isEqualToString:[element objectForKey:kParent]])
                    {
                        top = YES;
                        break;                      
                        
                    }
                    else if ( [[element2 objectForKey:kFils] isEqualToString:[element1 objectForKey:kParent]] 
                             && [[element2 objectForKey:kParent] isEqualToString:[element objectForKey:kParent]])
                    {
                        top = NO;
                        break;                      
                        
                    }
                    else
                    {
                        
                    }
                }
            }
            else 
            {            
                
            }
            
        }
        
        if (top) 
        {
            [pcrDic1 addObject:element];
        }
    }
    return pcrDic1;    
}


+(void)affichage:(NSArray * )pcrDic 
{
    DLog(@"");
       
 for(NSDictionary * element in pcrDic)
    {
    }    
}


@end
