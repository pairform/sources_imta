//
//  MyWebFrame.h
//  SupCastProducer
//
//  Created by Cape EMN on 02/01/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <WebKit/WebKit.h>

@protocol monProtocole
// -------------------------------------------------------------------------------
//	handleError:error
// -------------------------------------------------------------------------------
- (void)handleError:(NSError *)error;
// -------------------------------------------------------------------------------
//	connection:didReceiveResponse:response
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)myResponse;
// -------------------------------------------------------------------------------
//	connection:didReceiveData:data
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)myData;
// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)myError;
// -------------------------------------------------------------------------------
//	connectionDidFinishLoading:connection
// -------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
@end

@interface DownLoad : NSObject <monProtocole>{
    
}

-(void)downLoad;
//-(NSString *)dataFilePath:(NSString *)fileName;
- (id)initWithDico:(NSMutableDictionary*)aDico;
@end
