//
//  XMLToObjectParser.h
//  SupCastProducer
//
//  Created by CAPE - EMN on 02/03/11.
//  Copyright 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//


//
#import <Foundation/Foundation.h>
#import "Balise.h"


@interface XMLToObjectParser : NSObject  <NSXMLParserDelegate> {

	BOOL topBaliseopueM;
	BOOL topBaliseopuM;
	BOOL topBaliseopueDiv;
	BOOL topBalisespex;	
	BOOL topBalisescitemizedList;
	BOOL topBaliseoppb;
	BOOL topBaliseopres;
	BOOL topBaliseoptxt;
	BOOL topBaliseopue;
	BOOL topBaliseopueDivM;
	BOOL topBalisescinlineImg;
	BOOL topBalisescinlineStyle;
	BOOL topBalisescitem;
	BOOL topBalisesclistItem;
	BOOL topBalisescpara;
	BOOL topBalisescrefUri;
	BOOL topBalisesctextLeaf;
	BOOL topBalisescuLink;
	BOOL topBalisespcourseUa; 
	BOOL topBalisespcourseUc;
	BOOL topBalisespdiv;
	BOOL topBalisespform;
	BOOL topBalisespimg;
	BOOL topBalisespintro;
	BOOL topBalisespconclu;
	BOOL topBalisesppractUc;
	BOOL topBalisespsTitle;
	BOOL topBalisesptitle;
	BOOL topBalisesptxt;
	BOOL topBalisespvid;
	BOOL topBalisesptab;
	BOOL topBaliseopcourseUa; 
	BOOL topBalisespinfo; 
	BOOL topBalisespodInstruct; 
	BOOL topBaliseopexpUc; 
	
	BOOL isStartBaliseopexpUc; 
	BOOL isStartBaliseoppb;
	BOOL isStartBaliseopres;
	BOOL isStartBaliseoptxt;
	BOOL isStartBaliseopue;
	BOOL isStartBaliseopueDiv;
	BOOL isStartBaliseopueDivM;
	BOOL isStartBaliseopueM;
	BOOL isStartBaliseopuM;
	BOOL isStartBalisescinlineImg;
	BOOL isStartBalisescinlineStyle;
	BOOL isStartBalisescitem;
	BOOL isStartBalisescitemizedList;
	BOOL isStartBalisesclistItem;
	BOOL isStartBalisescpara;
	BOOL isStartBalisescrefUri;
	BOOL isStartBalisesctextLeaf;
	BOOL isStartBalisescuLink;
	BOOL isStartBalisespcourseUa; 
	BOOL isStartBalisespcourseUc;
	BOOL isStartBalisespdiv;
	BOOL isStartBalisespex;
	BOOL isStartBalisespform;
	BOOL isStartBalisespimg;
	BOOL isStartBalisespintro;
	BOOL isStartBalisespconclu;
	BOOL isStartBalisesppractUc;
	BOOL isStartBalisespsTitle;
	BOOL isStartBalisesptitle;
	BOOL isStartBalisesptxt;
	BOOL isStartBalisespvid;
	BOOL isStartBalisesptab;
	BOOL isStartBaliseopcourseUa; 
	BOOL isStartBalisespinfo; 
    BOOL isStartBalisespodInstruct; 

	BOOL isEndBaliseopexpUc; 
    BOOL isEndBaliseoppb;
	BOOL isEndBaliseopres;
	BOOL isEndBaliseoptxt;
	BOOL isEndBaliseopue;
	BOOL isEndBaliseopueDiv;
	BOOL isEndBaliseopueDivM;
	BOOL isEndBaliseopueM;
	BOOL isEndBaliseopuM;
	BOOL isEndBalisescinlineImg;
	BOOL isEndBalisescinlineStyle;
	BOOL isEndBalisescitem;
	BOOL isEndBalisescitemizedList;
	BOOL isEndBalisesclistItem;
	BOOL isEndBalisescpara;
	BOOL isEndBalisescrefUri;
	BOOL isEndBalisesctextLeaf;
	BOOL isEndBalisescuLink;
	BOOL isEndBalisespcourseUa; 
	BOOL isEndBalisespcourseUc;
	BOOL isEndBalisespdiv;
	BOOL isEndBalisespex;
	BOOL isEndBalisespform;
	BOOL isEndBalisespimg;
	BOOL isEndBalisespintro;
	BOOL isEndBalisespconclu;
	BOOL isEndBalisesppractUc;
	BOOL isEndBalisespsTitle;
	BOOL isEndBalisesptitle;
	BOOL isEndBalisesptxt;
	BOOL isEndBalisespvid;
	BOOL isEndBalisesptab;
	BOOL isEndBaliseopcourseUa; 
	BOOL isEndBalisespinfo; 
	BOOL isEndBalisespodInstruct; 
	
	int CompteurBaliseopueDiv;
	int CompteurContent;
	int situation;
	int level;
	int levelLast;

	NSMutableString * currentNodeContent;
	NSMutableDictionary * monDicoItemAudio;
	NSMutableArray * monArrayDicoItemAudio;
	NSMutableDictionary * monDicoArrayDicoItemAudio;

	//)))))))))))))))))))))))))))))))))))))))))))))))))))))))   
	NSMutableDictionary * dicoFichier;
	NSMutableArray * tableDictionnaireFichier;
	NSMutableArray * listeDesSequences;
	NSMutableArray * listeDesSousSequences;
	NSMutableArray * listeDesItems;
	NSMutableDictionary * monDicoRacine;
	NSMutableDictionary * monDicoContenuSequences;
//	NSMutableArray * monArrayContenuSequences;
	
	NSMutableArray * arrayFichier;
	NSMutableArray * monArrayIntermediaire;
	NSMutableArray * monTableauDeCollecteIntermediare;
	//NSString * pathRacineProjet;
	//))))))))))))))))))))))))))))))))))))))))))))))))))))))))	
    NSString *path;
    BOOL topElse; 
    int iCompeurFichier;
    int indiceFichier;
    NSMutableString * monFichierDeBase;
    NSMutableArray * arrayIndex;
}

@property (nonatomic) int iCompeurFichier;
@property (nonatomic) int situation;
@property (nonatomic) int level;
@property (nonatomic) int levelLast;
@property (retain,nonatomic) NSString *path;

@property (nonatomic,retain) NSMutableDictionary * dicoFichier;
@property (nonatomic,retain) NSMutableArray * arrayIndex;
@property (nonatomic,retain) NSMutableArray * arrayFichier;
@property (nonatomic,retain) NSMutableArray * tableDictionnaireFichier;
@property (nonatomic,retain) NSMutableArray * listeDesSousSequences;
@property (nonatomic,retain) NSMutableArray * listeDesSequences;
@property (nonatomic,retain) NSMutableArray * listeDesItems;
@property (nonatomic,retain) NSMutableDictionary * monDicoContenuSequences;
//@property (nonatomic,retain) NSMutableArray * monArrayContenuSequences;
@property (nonatomic,retain) NSMutableArray * monArrayIntermediaire;
@property (nonatomic,retain) NSMutableArray * monTableauDeCollecteIntermediare;

@property (nonatomic,retain) NSMutableString * currentNodeContent;
@property (nonatomic,retain) NSMutableDictionary * monDicoItemAudio;
@property (nonatomic,retain) NSMutableString * monFichierDeBase;
@property (nonatomic,retain) NSMutableDictionary * monDicoRacine;
@property (nonatomic,retain) NSMutableArray * monArrayDicoItemAudio;
@property (nonatomic,retain) NSMutableDictionary * monDicoArrayDicoItemAudio;
//@property (nonatomic,retain) NSString * pathRacineProjet;

- (id)parseXMLFromData:(NSData *)data 
			parseError:(NSError **)error;
- (NSRange)rechercheFin:(NSMutableString *)buffer rangeOfStringDebut:(NSString *)rangeOfStringDebut rangeOfStringFin:(NSString *)rangeOfStringFin debut:(NSRange)rangeDebut;

-(NSArray *)eclate:(NSString *)nomFichier; 
-(NSArray *)eclate1:(NSString *)nomFichier fichierDeBase:(NSString *)fichierDeBase; 
-(NSDictionary *)inspectionCollecte:(NSString *)nomFichier;
- (void)parserDidStartDocument:(NSXMLParser *)parser;
-(id)initWithSituation:(int)aSituation
          fichierPubli:(NSString *)aPathRacineProjet 
                 level:(int)aLevel
       iCompeurFichier:(int)aCompteur;

@end