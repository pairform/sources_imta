//
//  GestionFichier.m
//  SupCastProducer
//
//  Created by CAPE - EMN on 02/03/11.
//  Copyright 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionFichier.h"

@implementation GestionFichier

-(NSString *)dataFilePath:(NSString *)fileName {
    DLog(@"");
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}

-(NSString *)dataFilePathAtDirectory:(NSString *)directory 
							fileName:(NSString *)fileName {
    DLog(@"");
    
	BOOL isDir=NO;
	
    if (([[NSFileManager defaultManager] fileExistsAtPath:directory isDirectory:&isDir]) && (isDir))
	{
		//
	}
	else
	{
		[[NSFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:YES  attributes:nil  error:nil];
	}
	
	return [directory stringByAppendingPathComponent:fileName];
	
}

-(NSString *)concateneDirectory:(NSString *)directory 
                       fileName:(NSString *)nomFichier {
    DLog(@"");

	NSMutableArray *myPathComponents = (NSMutableArray *)[nomFichier pathComponents];
	
    NSString * myPath;
	if ([[myPathComponents objectAtIndex:0] isEqualToString:@"/"]) 
	{
		if ([[myPathComponents objectAtIndex:1] isEqualToString:[directory lastPathComponent]])
		{
			[myPathComponents removeObjectAtIndex:0];
			[myPathComponents removeObjectAtIndex:0];
			myPath = [directory stringByAppendingPathComponent:[NSString pathWithComponents:myPathComponents]];
		}
		else 
		{
			myPath = [directory stringByAppendingPathComponent:nomFichier];
		}
	}
	else 
	{
		if ([[myPathComponents objectAtIndex:0] isEqualToString:[directory lastPathComponent]])
		{
			[myPathComponents removeObjectAtIndex:0];
			myPath = [directory stringByAppendingPathComponent:[NSString pathWithComponents:myPathComponents]];
		}
		else 
		{
			myPath = [directory stringByAppendingPathComponent:nomFichier];
		}
	}
	if ([[NSFileManager defaultManager] fileExistsAtPath:myPath])
	{
		return myPath;
		
	}
	else 
	{
		return nil;
		
	}	
	
}

- (void)dealloc {
    DLog(@"");
}

@end
