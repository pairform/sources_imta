//
//  FichierPubli.h
//  SupCastProducer
//
//  Created by CAPE - EMN on 23/11/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FichierPubli : NSObject <NSXMLParserDelegate>
{
    NSMutableString * currentNodeContent;
    NSString * fichierXML;
}

@property (nonatomic,retain) NSString * fichierXML;
@property (nonatomic,retain) NSMutableString * currentNodeContent;
- (id)parseXMLFromFile:(NSString *)file 
			parseError:(NSError **)error;
@end
