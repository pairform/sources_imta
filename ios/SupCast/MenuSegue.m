//
//  MenuSegue.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuSegue.h"
#import "JASidePanelController.h"

@implementation MenuSegue

- (void)perform
{
    // Add your own animation code here.
    
    UINavigationController * globalNavigationController = (UINavigationController*)[(JASidePanelController *)[[self sourceViewController] parentViewController] centerPanel];
    
    [globalNavigationController pushViewController:self.destinationViewController animated:true];
}
@end
