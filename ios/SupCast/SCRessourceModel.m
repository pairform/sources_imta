//
//  SCRessourceModel.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/04/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "SCRessourceModel.h"

@implementation SCRessourceModel

@synthesize nom_long;
@synthesize nom_court;
@synthesize description;
@synthesize icone;
@synthesize etablissement;
@synthesize theme;
@synthesize visible;
@synthesize date_upload;
@synthesize date_released;
@synthesize taille;

@end
