//
//  SCRessourceModel.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/04/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCRessourceModel : NSObject{
    NSString * nom_long;
    NSString * nom_court;
    NSString * description;
    NSString * icone;
    NSString * etablissement;
    NSString * theme;
    NSString * visible;
    NSString * date_upload;
    NSString * date_released;
    NSString * taille;
}
@property(nonatomic,strong) NSString * nom_long;
@property(nonatomic,strong) NSString * nom_court;
@property(nonatomic,strong) NSString * description;
@property(nonatomic,strong) NSString * icone;
@property(nonatomic,strong) NSString * etablissement;
@property(nonatomic,strong) NSString * theme;
@property(nonatomic,strong) NSString * visible;
@property(nonatomic,strong) NSString * date_upload;
@property(nonatomic,strong) NSString * date_released;
@property(nonatomic,strong) NSString * taille;

@end
