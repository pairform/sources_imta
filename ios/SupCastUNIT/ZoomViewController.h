//
//  ZoomViewController.h
//  SupCast
//
//  Created by Cape EMN on 17/08/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZoomFSViewController.h"

@interface ZoomViewController : UIViewController <UIScrollViewDelegate, ZoomFSViewDelegate>

@property(weak,nonatomic) IBOutlet UIImageView* imageView;
@property(weak,nonatomic) IBOutlet UIScrollView* scrollZoomView;

@property(strong,nonatomic) NSString* imagePath;

-(id)initWithPicture:(NSString *)imagePath;
-(IBAction)enterFullScreen:(id)sender;

@end
