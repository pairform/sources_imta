
#import "URLLoader.h"

/*!
 * @abstract Private routines.
 */
@interface URLLoader(Private)
- (void)notifyDidBeginURL:(NSURL*)aURL;
- (void)notifyDidFinishURL:(NSURL*)aURL;
- (void)notifyDidFinishAll;
@end

@implementation URLLoader

- (id)initWithDelegate:(id)delegate {
	DLog(@"");
	self = [super init];
	if (self == nil) return nil;
	
	fFinished = [NSMutableDictionary new];
	fPending = [NSMutableDictionary new];
	fLock = [NSRecursiveLock new];
	
	fDelegate = delegate;
	return self;
}

- (void)dealloc {
	DLog(@"");
	[fLock lock];
	[fLock unlock];
	
}
	
- (void)load:(NSArray*)URLs {
	DLog(@"");
	[fLock lock];
	[fPending removeAllObjects];
	[fFinished removeAllObjects];
	
	unsigned i=0;
	for (; i<[URLs count]; ++i) {
		//create a URLDataReceiver instance for each URL.
		URLDataReceiver* receiver = [[URLDataReceiver alloc] 
										initWithURL:[URLs objectAtIndex:i]
										delegate:self];
										
		//put the newly created receiver into pending list.
		[fPending setObject:receiver forKey:[URLs objectAtIndex:i]];
		[receiver startLoading];
		
		//notify the delegate.
		[self notifyDidBeginURL:[URLs objectAtIndex:i]];
	}
	[fLock unlock];
}

- (void)cancel {
	DLog(@"");
	[fLock lock];
	[fPending removeAllObjects];
	[fFinished removeAllObjects];
	[fLock unlock];
}

- (void)reset {
	DLog(@"");
	[self cancel];
}

- (NSData*)dataForURL:(NSURL*)aURL {
	DLog(@"");
	[fLock lock];
	URLDataReceiver* receiver = [fFinished objectForKey:aURL];
	[fLock unlock];
	
	if (receiver == nil)
		return nil;
	else
		return [receiver receivedData];
}

- (NSEnumerator*)dataEnumerator {
	DLog(@"");
	return [fFinished objectEnumerator];
}

- (NSEnumerator*)urlEnumerator {
	DLog(@"");
	return [fFinished keyEnumerator];
}

/////////////////// URLDataReceiverDelegate Protocol //////////////////
- (void)URLDataReceiverDidFinish:(URLDataReceiver*)dataReceiver {
	DLog(@"");
	[fLock lock];
	NSURL* url = [dataReceiver url];
	
	//move receiver from pending list to finished list.
	[fPending removeObjectForKey:url];
	[fFinished setObject:dataReceiver forKey:url];
	
	//notify delegate that this url has finished.
	[self notifyDidFinishURL:url];
	
	//if pending list is empty, notify delegate that all URLs have finished.
	if ([fPending count] == 0)
		[self notifyDidFinishAll];
	[fLock unlock];
}

///////////////// Private /////////////////////////
- (void)notifyDidBeginURL:(NSURL*)aURL {
	DLog(@"");
	if (fDelegate && [fDelegate respondsToSelector:@selector(URLLoader:didBeginURL:)])
		[fDelegate performSelector:@selector(URLLoader:didBeginURL:) withObject:self withObject:aURL];
}

- (void)notifyDidFinishURL:(NSURL*)aURL {
	DLog(@"%s / %@",__PRETTY_FUNCTION__,aURL);
	if (fDelegate && [fDelegate respondsToSelector:@selector(URLLoader:didFinishURL:)])
		[fDelegate performSelector:@selector(URLLoader:didFinishURL:) withObject:self withObject:aURL];


}

- (void)notifyDidFinishAll {
	DLog(@"");
	if (fDelegate && [fDelegate respondsToSelector:@selector(URLLoaderDidFinishAll:)])
		[fDelegate performSelector:@selector(URLLoaderDidFinishAll:) withObject:self];
}
@end
