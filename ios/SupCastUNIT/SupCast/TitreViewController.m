//
//  TitreViewController.m
//  SupCast
//
//  Created by Phetsana on 05/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "TitreViewController.h"
#import "StyledPullableView.h"
#import "Categorie.h"

@implementation TitreViewController
@synthesize arrayRacine;
@synthesize sequenceNameListe;
@synthesize logoEtablissement;
@synthesize statusConnexionReseau;
@synthesize guid;
//TODO:afficher les annexes, info auteurs et bibliographie s'il y a.
- (void)viewDidLoad
{    
    DLog(@"");
    [super viewDidLoad];
    
    //[(NSMutableArray*)self.arrayRacine addObject:[(NSArray*)[(NSDictionary*)[(NSArray*)arrayRacine objectAtIndex:0] objectForKey:@"sequenceContent"] objectAtIndex:0]];
    /*
    NSDictionary * test = [[NSDictionary alloc] initWithObjectsAndKeys:@"2500",@"idecran",@"1",@"visible",@"Abreviations",@"sequenceName",@"droit_web_1.html",@"referenceFichier", nil];
    [(NSMutableArray*)self.arrayRacine addObject:test];
    */

    self.sequenceNameListe = [[NSMutableArray alloc] init]; 
    for (NSDictionary * dicoItem in self.arrayRacine)
    {
        [self.sequenceNameListe addObject:[dicoItem objectForKey:@"sequenceName"]];
    }
    /*
     idecran = 198;
     indice = 2;
     referenceFichier = "notion_oeuvres.html";
     sequenceName = "Notion \"\U0152uvres\"";
     visible = 1;
    
    [self.sequenceNameListe addObject:(NSDictionary*)[(NSArray*)[(NSDictionary*)[(NSArray*)arrayRacine objectAtIndex:0] objectForKey:@"sequenceContent"] objectAtIndex:0]];
    */    NSDictionary * dicoRacine = [[NSDictionary alloc] initWithDictionary:[self.arrayRacine objectAtIndex:0]];
    if (!self.estVisiteur)
        if ([dicoRacine objectForKey:@"idecran"]) 
            self.idEcran = [[NSString alloc] initWithString:[dicoRacine objectForKey:@"idecran"]];
    
	self.titre = self.title;
    //On cale le titre
    self.title = [self.sequenceNameListe objectAtIndex:0];
    
    [self recupererNiveau:[self.ressourceInfo objectForKey:@"ressourceid"]];
    
    [tableView reloadData];
    
    //TODONE:Récuperer l'image de l'établissement, comme dans Categorie.m
    //Trouver un workaround pour éviter de refaire un appel au webservice pour récuperer le logo serait COOL.
    //Enregistrer les images d'établissement à la première fois qu'on va ajouter des ressources?
    //OUAIS.
    
    //[logoEtablissement setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[categ.listeEtablissements objectAtIndex:(int)[self.ressourceInfo objectForKey:kRessourceEtablissementStr]] objectForKey:kIcone]]]]];
    NSString * idEtablissement = [self.ressourceInfo objectForKey:@"ressourceetablissement"];
    UIImage * imageLogoEtablissement = [[UIImage alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/Documents/%@.png",NSHomeDirectory(),idEtablissement]];
       
    if(imageLogoEtablissement != nil)
        [[self logoEtablissement] setImage:imageLogoEtablissement];
    
    
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        NSString * infos = [self.ressourceInfo objectForKey:@"ressourcesummary"];   
        StyledPullableView * pullUpView = [[StyledPullableView alloc] initWithFrameAndText:CGRectMake(0, 0, 320, 460):infos];
        [self.view addSubview:pullUpView];
    }
   
}

-(NetworkStatus) etatConnexionReseau {
    self.statusConnexionReseau = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    return self.statusConnexionReseau;
}


- (BOOL)connected 
{
    if (NotReachable== (BOOL)[self etatConnexionReseau])
        return NO;    
    else
        return YES;    
}
-(void)recupererNiveau:(NSString*)ressourceid
{
    DLog(@"");
    if ([self connected])
    {    
        //Communication avec le serveur : recuperer le niveau
        
        NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kRecupererNiveauRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        NSString *paramDataString = [[NSString alloc] initWithFormat:
                                     @"guid=%@&ressource=%@",self.guid,ressourceid];
        NSData *paramData = [[NSData alloc] initWithData:[paramDataString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:paramData];
        
        self.connectionNiveauRessource = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if(self.connectionNiveauRessource) 
        {
            self.receivedDataNiveauRessource = [[NSMutableData data] init];
            
        }
        else 
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }
}


- (void)didReceiveMemoryWarning {
    DLog(@"");
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}
#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    DLog(@"");
    return [arrayRacine count] == 1 ? 1 : 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 1 ? 25 : 0;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    DLog(@"");
    return section == 0 ? nil : @"Annexes";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DLog(@"");
    return section == 0 ? 1 : [self.sequenceNameListe count] -1;
}

- (void)configureCellMenu:(MainViewCell *)cell forIndexPath:(NSIndexPath *)indexPath 
{
    DLog(@"");
	//cell.titleLabel.text = [self.sequenceNameListe objectAtIndex:indexPath.row];
	cell.detailTextLabel.text = [self.sequenceNameListe objectAtIndex:indexPath.row];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLog(@"");
	
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	
    
    //Variable de triche. On s'en sert comme repère pour se balader dans les tableaux
    //En théorie j'aurai du faire deux tableaux, un par section, mais fuck.
    int rowGlobal = indexPath.row + indexPath.section;
    
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	cell.textLabel.textColor = [UIColor blackColor];
    cell.imageView.image = nil;
    cell.textLabel.text = indexPath.section == 0 ? @"Commencer..." : [self.sequenceNameListe objectAtIndex:rowGlobal];
    
    //On calcule le nombre de message à l'intérieur
    NSDictionary * dico1 = [[NSDictionary alloc] initWithDictionary:[self.arrayRacine objectAtIndex:rowGlobal]];
    NSString *idEcranCell   = [[NSString alloc] initWithString:[dico1 objectForKey:kIdecran]];
    
    //DLog(@"_______________________________________________________");
    //DLog(@"Id de la ligne : %@",idEcranCell);
    NSMutableString * nombreDeMessages =  [[NSMutableString alloc] init];
    
    NSArray *listIdEcran=[[NSArray alloc] initWithArray:[self.pcrDictionary objectForKey:idEcranCell]];
    
    //Affichage ou non de la flèche
    
    if ([listIdEcran count] != 1)
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else cell.accessoryType = UITableViewCellAccessoryNone;

    //Méthode pour récuperer les messages de la cellule et de ses enfants (comme dans PVC) 
    
    //Méthode pour récuperer les messages de la cellule et de ses enfants
    if([self.dicoNombreMessages count])
    {
        int showNumberMsg = [self trouverMessagesDansMenu:dico1 messagesToFind:self.dicoNombreMessages];
        

        if(showNumberMsg != 0)
        {
            nombreDeMessages = [NSString stringWithFormat:@"%d",showNumberMsg];
            
            DLog(@"Nombre de messages = %@, ID de la ligne = %@ , Index de la ligne = %d", nombreDeMessages, idEcranCell, rowGlobal);
            
            //On créé une bulle rouge
            //cell.textLabel.shadowColor = [UIColor lightGrayColor];
            
            UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage * image;
            
            image = [UIImage imageNamed:kBulleBarreRedMiroir];
            [aButton1 setTitle:nombreDeMessages forState:UIControlStateNormal];
            [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            aButton1.titleLabel.font=[UIFont systemFontOfSize:13];    
            
            CGRect frame = CGRectMake(20.0, 0.0, 32.0, 32.0);                    
            frame.origin.y = (cell.frame.size.height - frame.size.height) / 2;
            aButton1.frame=frame;
            [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
            
            
            //Et on l'attribue à la cellule.
            //cell.accessoryView = aButton1;
            [cell.contentView addSubview:aButton1];       
        }
    }
    

    return cell;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DLog(@"");
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	
    //Variable de triche. On s'en sert comme repère pour se balader dans les tableaux
    //En théorie j'aurai du faire deux tableaux, un par section, mais fuck.
    int rowGlobal = indexPath.row + indexPath.section;
    
    NSDictionary * dico1 = [[NSDictionary alloc] initWithDictionary:[self.arrayRacine objectAtIndex:rowGlobal]];
    NSArray *array1;	    
    if(self.estVisiteur) 
    {       
        if ([dico1 objectForKey:@"listeDesSequences"] != nil ? [[dico1 objectForKey:@"listeDesSequences"] count] : false)
        {
            array1 = [[NSArray alloc] initWithArray:[dico1 objectForKey:@"listeDesSequences"]];
            // Appel MainViewController
            MainViewController *mainViewController = [MainViewController alloc];
            // infos communes
            mainViewController.estVisiteur   = self.estVisiteur;
            mainViewController.titre         = [self.sequenceNameListe objectAtIndex:rowGlobal];
            mainViewController.username      = @"";

            mainViewController.titreParent    = self.title;
            mainViewController.idEcranParent  = self.idEcran;
            if ([dico1 objectForKey:@"idecran"]) 
                mainViewController.idEcran   = [[NSString alloc] initWithString:[dico1 objectForKey:@"idecran"]];
            
            mainViewController.idBlog        = @"";
            mainViewController.password      = @"";
            mainViewController.categorie     = @"";
            mainViewController.ressourceInfo = self.ressourceInfo;
            mainViewController.pcrDictionary = self.pcrDictionary;
            // titre
            mainViewController.title         = [self.sequenceNameListe objectAtIndex:rowGlobal];
            // données ressources
            mainViewController.listeDesItems = array1;
            mainViewController.dicoRacine    = dico1;
            
            // init
            (void)[mainViewController initWithNibName:@"MainView" bundle:nil];
            [self.navigationController pushViewController:mainViewController 
                                                 animated:YES];

        }
        else 
        {
            // Appel WebViewContentsViewController
            WebViewContentsViewController *webViewcontentsViewController = [WebViewContentsViewController alloc];
            // infos communes
            webViewcontentsViewController.estVisiteur = self.estVisiteur;
            webViewcontentsViewController.titre = [self.sequenceNameListe objectAtIndex:rowGlobal];
            webViewcontentsViewController.username = @""; 
            webViewcontentsViewController.titreParent = @""; 
            webViewcontentsViewController.idEcranParent = @""; 
            webViewcontentsViewController.idEcran = @""; 
            webViewcontentsViewController.idBlog = @""; 
            webViewcontentsViewController.password = @""; 
            webViewcontentsViewController.categorie = self.categorie; 
            webViewcontentsViewController.ressourceInfo = self.ressourceInfo; 
            // titre
            webViewcontentsViewController.title = [self.sequenceNameListe objectAtIndex:rowGlobal];
            // infos ressources
            if ([dico1 objectForKey:@"referenceFichier"])
                webViewcontentsViewController.pageHtml = [dico1 objectForKey:@"referenceFichier"];
            if ([dico1 objectForKey:@"sousRefFichier"])
                webViewcontentsViewController.pageHtml = [dico1 objectForKey:@"sousRefFichier"];
            //webViewcontentsViewController.contentList = [moduleContent objectForKey:@"sequenceContent"];
            // init
            (void)[webViewcontentsViewController initWithNibName:@"WebViewContentView" 
                                                          bundle:nil];
            // push
            [self.navigationController pushViewController:webViewcontentsViewController 
                                                 animated:YES];

        }
    }
    
    else
    {
       /* 
        NSDictionary * dico1 = [[NSDictionary alloc] initWithDictionary:[self.arrayRacine objectAtIndex:rowGlobal]];
        NSArray *array1;	*/
        if ([dico1 objectForKey:@"listeDesSequences"] != nil ? [[dico1 objectForKey:@"listeDesSequences"] count] : false)
        {
            array1 = [[NSArray alloc] initWithArray:[dico1 objectForKey:@"listeDesSequences"]];
        
        // Appel MainViewController
        MainViewController *mainViewController = [MainViewController alloc];
        
        // infos communes
        mainViewController.estVisiteur   = self.estVisiteur;
        mainViewController.titre         = [self.sequenceNameListe objectAtIndex:rowGlobal];
        mainViewController.username      = self.username;
        mainViewController.titreParent   = [self.sequenceNameListe objectAtIndex:rowGlobal];
        mainViewController.idEcranParent = self.idEcran;
        if ([dico1 objectForKey:@"idecran"]) 
            mainViewController.idEcran   = [[NSString alloc] initWithString:[dico1 objectForKey:@"idecran"]];
        mainViewController.idBlog = @"";
        mainViewController.password      = self.password;
        mainViewController.categorie     = self.categorie;
        mainViewController.ressourceInfo = self.ressourceInfo;
        mainViewController.pcrDictionary = self.pcrDictionary;
        // données ressources
        mainViewController.listeDesItems = array1;
        mainViewController.dicoRacine    = dico1;
        // titre
        mainViewController.title         = [self.sequenceNameListe objectAtIndex:rowGlobal];
        // init
        (void)[mainViewController initWithNibName:@"MainView" bundle:nil];
        [self.navigationController pushViewController:mainViewController animated:YES];
        }
        else {
            // Appel WebViewContentsViewController
            WebViewContentsViewController *webViewcontentsViewController = [WebViewContentsViewController alloc];
            // infos communes
            webViewcontentsViewController.estVisiteur = self.estVisiteur;
            webViewcontentsViewController.titre = [self.sequenceNameListe objectAtIndex:rowGlobal]; 
            webViewcontentsViewController.username = self.username;
            webViewcontentsViewController.titreParent = self.titre;
            webViewcontentsViewController.idEcranParent = self.idEcran;
            if ([dico1 objectForKey:@"idecran"])
                webViewcontentsViewController.idEcran = [dico1 objectForKey:@"idecran"];
            webViewcontentsViewController.idBlog = @"";
            webViewcontentsViewController.password = self.password;
            webViewcontentsViewController.categorie = self.categorie;
            webViewcontentsViewController.ressourceInfo = self.ressourceInfo;
            // titre
            webViewcontentsViewController.title = [self.sequenceNameListe objectAtIndex:rowGlobal];
            // infos ressources
            webViewcontentsViewController.pcrDictionary = self.pcrDictionary;
            if ([dico1 objectForKey:@"referenceFichier"])
                webViewcontentsViewController.pageHtml = [dico1 objectForKey:@"referenceFichier"];
            if ([dico1 objectForKey:@"sousRefFichier"])
                webViewcontentsViewController.pageHtml = [dico1 objectForKey:@"sousRefFichier"];
            //webViewcontentsViewController.contentList = [moduleContent objectForKey:@"sequenceContent"];
            // init            
            (void)[webViewcontentsViewController initWithNibName:@"WebViewContentView" bundle:nil];
            // push            
            [self.navigationController pushViewController:webViewcontentsViewController animated:YES];
        }
    }
    
    
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

- (void)viewDidUnload {
    [self setLogoEtablissement:nil];
    [super viewDidUnload];
}
@end

