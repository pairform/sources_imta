//
//  PlistDataBuilder.h
//  SupCast
//
//  Created by Phetsana on 15/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractDataBuilder.h"

/* Classe concrête du patron de conception builder - Construction 
	de la structure de données à partir d'une plist */

@interface PlistDataBuilder : AbstractDataBuilder {
	
}

@end
