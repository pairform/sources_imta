//
//  MessageCell.h
//  SupCast
//
//  Created by Maen Juganaikloo on 25/04/13.
//  Copyright (c) 2013 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell

@end
