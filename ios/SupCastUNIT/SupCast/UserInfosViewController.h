//
//  UserInfosViewController.h
//  SupCast
//
//  Created by wbao11 on 19/07/11.
//  Copyright (c) 2011 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface UserInfosViewController : UIViewController <UITabBarDelegate,UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UIImageView *imageView;
    BOOL statusPublicationGPS;
    BOOL statusPublicationTwitter;
    IBOutlet UITableViewCell *customCell;
    UITextField *myTextField0;
    UITextField *myTextField1;
    NSMutableString * motdepasseTwitter;
    NSMutableString * userTwitter;
    NSArray * listeValeur;
}

@property(nonatomic,strong) NSMutableString * motdepasseTwitter;
@property(nonatomic,strong) NSArray * listeValeur;
@property(nonatomic,strong) NSMutableString * userTwitter;

-(NSString *)dataFilePath:(NSString *)fileName; 
- (void)checkButtonTapped:(id)sender event:(id)event;
-(IBAction)backgroundTap:(id)sender;
-(IBAction)textFieldDone:(id)sender;
-(void)cellMiseEnForme:(UITableView *)aTableView withIndexPath:(NSIndexPath *)indexPath withStatus:(BOOL)status withIndex:(NSUInteger)index;
-(void)miseEnFormeWithStatus:(BOOL)status withIndex:(NSUInteger)index withIdentifier:(NSString *)identifier;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
