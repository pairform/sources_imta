//
//  SequenceViewController.h
//  SupCast
//
//  Created by Phetsana on 20/07/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GestionReseauSocialViewController.h"


@interface SequenceViewController : GestionReseauSocialViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UIImageView *fondEcranImageView;
	NSDictionary *sequenceContent;

	IBOutlet UILabel *label;
	NSString *labelText;
	
	NSString *ecranPrecedent;
	
	NSString *dirRes;
	NSDictionary *dicoRacine;
	
}
@property (nonatomic, retain) NSDictionary *sequenceContent;
@property (nonatomic, retain) NSString *labelText;


@property (nonatomic, retain) NSDictionary *dicoRacine;
@property (nonatomic, retain) NSString *dirRes;

@property (nonatomic, retain) IBOutlet UILabel *label;

@property (nonatomic, retain) NSString *ecranPrecedent;

@end
