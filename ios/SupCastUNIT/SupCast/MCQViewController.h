//
//  MCQViewController.h
//  SupCast
//
//  Created by Phetsana on 23/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractMCQExerciceViewController.h"

@interface MCQViewController : AbstractMCQExerciceViewController {
	IBOutlet UITableView *indicationTableView;
	CGFloat height1;
	CGFloat height2;
}

@end
