//
//  MainViewCell.h
//  SupCast
//
//  Created by Phetsana on 05/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MainViewCell : UITableViewCell {
    UIImageView * myMessageReminderView;
    UILabel * numberOfMsgLabel;
	NSMutableArray * itemsArray;
}

@property(nonatomic,strong) UIImageView * myMessageReminderView;
@property(nonatomic,strong) NSMutableArray * itemsArray;
@property(nonatomic,strong) UILabel * numberOfMsgLabel;

@end
