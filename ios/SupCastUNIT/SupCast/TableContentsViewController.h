//
//  TableContentsViewController.h
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GestionFichier.h"

@class Movie;

@interface TableContentsViewController : UIViewController <UIAlertViewDelegate> {
	IBOutlet UILabel *labelTitre;
	IBOutlet UITableView *tableView;
	NSArray *contentList;
	int indexItemContent; // indice de l'item courant
	NSString *indication;
	IBOutlet UIToolbar *toolBar;
	IBOutlet UIScrollView *mainScrollView;
	IBOutlet UIView *mainView;
	NSString *dirRes;
	NSString *titre;
	BOOL piecesOnTop;  // Keeps track of whether or not two or more pieces are on top of each other
	CGPoint startTouchPosition; 
	UIImageView *firstPieceView;
	Movie *movie;


	
}
@property (nonatomic, retain) IBOutlet UIImageView *firstPieceView;

@property (nonatomic, retain) NSString *titre;
@property (nonatomic, retain) NSString *dirRes;
@property (nonatomic, retain) NSArray *contentList;


- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil
		 contentList:(NSArray *)list
		   indication:(NSString *)indic;
- (UIImage *)resizeImage:(UIImage *)sourceImage max:(CGFloat)maximum;
- (UIImage *)resizeImage:(UIImage *)sourceImage maxHeight:(CGFloat)maxHeight maxWidth:(CGFloat)maxWidth;


-(void)animateFirstTouchAtPoint:(CGPoint)touchPoint forView:(UIImageView *)theView;
-(void)animateView:(UIView *)theView toPosition:(CGPoint) thePosition;
-(void)dispatchFirstTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event;
-(void)dispatchTouchEvent:(UIView *)theView toPosition:(CGPoint)position;
-(void)dispatchTouchEndEvent:(UIView *)theView toPosition:(CGPoint)position;


@end
