//
//  MCQViewController.m
//  SupCast
//
//  Created by Phetsana on 23/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "MCQViewController.h"


@implementation MCQViewController

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	indicationTableView.backgroundColor = [UIColor clearColor];
	indicationTableView.allowsSelection = NO;
	
	/* Calcul de position et de dimension */
	UIFont *captionFont = [UIFont fontWithName:kVerdana size:14];
	CGSize cs = [indication sizeWithFont:captionFont 
					   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
						   lineBreakMode:UILineBreakModeWordWrap];
	height1 = cs.height + 40; 
	
	captionFont = [UIFont fontWithName:kHelvetica size:12];
	cs = [[[questionList objectAtIndex:indexQuestion] objectForKey:kText] sizeWithFont:captionFont 
																	   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
																		   lineBreakMode:UILineBreakModeWordWrap];
	
	if ([[questionList objectAtIndex:indexQuestion] objectForKey:kText] != nil)
		height2 = cs.height + 20;
	else
		height2 = 0;
	
	indicationTableView.frame = CGRectMake(indicationTableView.frame.origin.x, 
											indicationTableView.frame.origin.y, 
											indicationTableView.frame.size.width, 
											height1 + height2 + 46);
	
	float tmpHeight = 0;
	int numberOfSequence = [self tableView:tableView numberOfRowsInSection:0]; 
	NSString *answersChoice;
	for (int i = 0; i < numberOfSequence; i++) {
		answersChoice = [[[questionList objectAtIndex:indexQuestion] objectForKey:kAnswersChoice] objectAtIndex:i];
		UIFont *captionFont = [UIFont boldSystemFontOfSize:20];
		CGSize cs = [answersChoice sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
		tmpHeight += (cs.height + 20);
	}
	
	tableView.frame = CGRectMake(tableView.frame.origin.x, 
								 indicationTableView.frame.origin.y + indicationTableView.frame.size.height + 10, 
								 tableView.frame.size.width, 
								 tmpHeight + 22);
	
	mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width,
											tableView.frame.origin.y + tableView.frame.size.height);
}

- (void)update {
	[super update];
	[indicationTableView reloadData];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	if (aTableView == indicationTableView) {
		if ([[questionList objectAtIndex:indexQuestion] objectForKey:kText] != nil)
			return 2;
		else
			return 1;
	}
	return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	NSInteger result;
	
	if (aTableView == indicationTableView)
		result = 1;
	else
		result = [super tableView:aTableView numberOfRowsInSection:section];

	
	return result;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [super tableView:aTableView cellForRowAtIndexPath:indexPath];
	
	if (aTableView ==  indicationTableView) {
		if (indexPath.section == 0) {
			cell.textLabel.font = [UIFont fontWithName:kVerdana size:14];
			cell.textLabel.text = indication;
		} else {
			cell.textLabel.font = [UIFont fontWithName:kHelvetica	size:12];
			cell.textLabel.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kText];
		}
	}
	
	return cell;        	
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	
	if (aTableView == tableView) {
		[super tableView:aTableView didSelectRowAtIndexPath:indexPath];
	} 
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat result = [super tableView:aTableView heightForRowAtIndexPath:indexPath];
	UIFont *captionFont;
	CGSize cs;
	
	if (aTableView == indicationTableView)
		if (indexPath.section == 0) {
			if (height1 == 0) {
				captionFont =[UIFont fontWithName:kVerdana size:14];
				cs = [indication sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
				result = cs.height + 40; // Add a space between each row
			} else 
				result = height1;
		} else {
			if (height2 == 0) {
				captionFont =[UIFont fontWithName:kHelvetica size:12];
				cs = [[[questionList objectAtIndex:indexQuestion] objectForKey:kText] sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
				result = cs.height + 20; // Add a space between each row			
			} else
				result = height2;
		}

	return result;
}


- (void)dealloc {
	[indicationTableView release];
    [super dealloc];
}


@end
