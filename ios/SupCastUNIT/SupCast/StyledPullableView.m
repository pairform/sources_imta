
#import "StyledPullableView.h"

/**
 @author Fabio Rodella fabio@crocodella.com.br
 */

@implementation StyledPullableView

-(id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) 
    {
        [self createBackground:frame];
        
        CGFloat xOffset = 0, yOffset = [UIScreen mainScreen].bounds.size.height - 64.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            xOffset = 224;
        }
        openedCenter = CGPointMake(160 + xOffset,yOffset);
        closedCenter = CGPointMake(160 + xOffset,yOffset + 200);
        self.center = closedCenter;
    }
    return self;
}

-(id)initWithFrameAndText:(CGRect)frame :(NSString *)text
{
    if ([self initWithFrame:frame]) 
    {
        [self createDescription:text];
    }
    return self;
}

- (id)initWithFrameAndTextArray:(CGRect)frame :(NSArray *)textArray
{
    if ([self initWithFrame:frame]) 
    {
        [self createScroll:textArray];                    
    }
    return self;
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context 
{
    if (finished) 
    {
        // Restores interaction after the animation is over
        dragRecognizer.enabled = YES;
        tapRecognizer.enabled = toggleOnTap;
        
        if ([delegate respondsToSelector:@selector(pullableView:didChangeState:)]) {
            [delegate pullableView:self didChangeState:opened];
        }
        
        //Animation de la flèche en haut du puller
        UIImageView * fleche = (UIImageView*)[self viewWithTag:12];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        float angle = opened ? 3.14f : 0;
        
        CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformMakeTranslation(0, 0), angle);
        fleche.transform = transform;
        
        [UIView commitAnimations];
        
    }
}       
- (void)updateTextScroll:(NSArray*)textArray
{
    [[self viewWithTag:11] removeFromSuperview];
    [self createScroll:textArray];
}
-(void)createScroll:(NSArray *)textArray
{
    //TODO:Créer un scroll, une textview et page control pour l'affichage dynamique        
    UIScrollView * scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(40, 40, 240, 180)];
    [scroll setBackgroundColor:[UIColor clearColor]];
    [scroll setPagingEnabled:true];
    
    //Création du page control
    _helpPageControl = [[UIPageControl alloc] init];
    _helpPageControl.center = CGPointMake(160, 227);
    _helpPageControl.numberOfPages = [textArray count];
    [_helpPageControl setUserInteractionEnabled:NO];
    //[_helpPageControl setCurrentPageIndicatorTintColor:[UIColor blackColor]];
    //[_helpPageControl setPageIndicatorTintColor:[UIColor grayColor]];
    
    //Pour chaque info dans le tableau
    for(int i=0; i < [textArray count]; i++)
    {
        //On ajoute une textView au scroll
        UITextView * infoTextView = [[UITextView alloc]initWithFrame:CGRectMake(i*scroll.frame.size.width, 0, scroll.frame.size.width, scroll.frame.size.height)];
        
        //On case le texte
        [infoTextView setText:[textArray objectAtIndex:i]];
        [infoTextView setFont:[UIFont fontWithName:@"Helvetica" size:14]];
        [infoTextView setBackgroundColor:[UIColor clearColor]];
        [infoTextView setTextAlignment:UITextAlignmentCenter];
        [infoTextView setEditable:false];
        [scroll addSubview:infoTextView];
        
    }
    [scroll setContentSize:CGSizeMake(scroll.frame.size.width * [textArray count], scroll.frame.size.height)];
    [scroll setTag:11];
    
    [scroll setDelegate:self];
    [self addSubview:scroll];
    [self addSubview:_helpPageControl];
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float pageWidth = scrollView.frame.size.width;
    float rapportOffset = scrollView.contentOffset.x / pageWidth;
    
    NSInteger page = lround(rapportOffset);
    
    //[[[helpPageControl subviews] objectAtIndex:0] setTintColor:[UIColor blackColor]];
    _helpPageControl.currentPage = page;
}
-(void)createDescription:(NSString *)text
{
    //TODO:Créer une textview et page control pour l'affichage d'un seul item        
    //FIXME:Redraw du texe, il s'affiche mal quand ça dépasse les 27 lignes (???) ±1733 caractères dans le cas de Droit D'Auteur.
    UITextView * infoTextView = [[UITextView alloc] initWithFrame:CGRectMake(40, 40, 240, 180)];
    [infoTextView setText:text];
    [infoTextView setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [infoTextView setBackgroundColor:[UIColor clearColor]];
    [infoTextView setTextAlignment:UITextAlignmentCenter];
    [infoTextView setEditable:false];
    [self addSubview:infoTextView];
}
-(void)createBackground:(CGRect)frame
{
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_background.png"]];
    background.frame = CGRectMake(0, 0, 320, 240);
    [self addSubview:background];
    
    UIImageView *fleche = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fleche_puller.png"]];
    fleche.frame = CGRectMake(0, 0, 32, 24);
    fleche.center = CGPointMake(160, 18);
    fleche.opaque = NO;
    //Tag pour recupérer facilement la subview lors de l'animation.
    fleche.tag = 12;
    
    [self addSubview:fleche];
}
@end
