//
//  MCQSoundFlipsideViewController.m
//  SupCast
//
//  Created by Phetsana on 29/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
#import "GestionFichier.h"
#import "MCQSoundFlipsideViewController.h"
#import "AVFoundation/AVAudioPlayer.h"

@implementation MCQSoundFlipsideViewController

@synthesize player,dirRes,dicoRacine;
/*
-(NSString *)dataFilePath:(NSString *)fileName 
{    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}
*/

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil
				text1:(NSString *)t1
				text2:(NSString *)t2
				image:(UIImage *)img
		   soundsList:(NSArray *)list {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
		text1 = t1;
		text2 = t2;
		image = img;
		soundsList = list;
    }
    return self;
}




// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	[fondEcranImageView setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																						   fileName:[self.dicoRacine objectForKey:kImageFondEcran]]]];
	
	indicationTableView1.backgroundColor = [UIColor clearColor];
	tableView.backgroundColor = [UIColor clearColor];
	
	textView1.text = text1;
	textView2.text = text2;
	imageView.image = image;
	
	[mainScrollView setCanCancelContentTouches:NO];
	mainScrollView.clipsToBounds = YES;
	mainScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	[mainScrollView addSubview:mainView];
	[mainScrollView setContentSize:CGSizeMake(mainView.frame.size.width, mainView.frame.size.height)];
	[mainScrollView setScrollEnabled:YES];
	[mainScrollView release]; // Libération mémoire car plus besoin
	
	/* Calcul de position et de dimension */
	CGFloat height;
	UIFont *captionFont = [UIFont fontWithName:kHelvetica size:16];
	CGSize cs1 = [text1 sizeWithFont:captionFont
				   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
					   lineBreakMode:UILineBreakModeWordWrap];
	height = cs1.height + 40;
	
	indicationTableView1.frame = CGRectMake(indicationTableView1.frame.origin.x, 
											indicationTableView1.frame.origin.y, 
											indicationTableView1.frame.size.width, 
											height + 22);
	
	CGFloat yTmp;
	
	if (image == nil) {
		yTmp = 0;
	} else {
		yTmp = imageView.frame.size.height;
	}
	
	
	float tmpHeight = 0;
	int numberOfSection = [soundsList count];
	NSString *answersChoice;
	for (int section = 0; section < numberOfSection; section++) {
		int numberOfSequence = [[[soundsList objectAtIndex:section] objectForKey:kItems] count]; 
		for (int row = 0; row < numberOfSequence; row++) {
			answersChoice = [[[[soundsList objectAtIndex:section] objectForKey:kItems] objectAtIndex:row] objectForKey:kText];
			UIFont *captionFont = [UIFont boldSystemFontOfSize:20];
			CGSize cs = [answersChoice sizeWithFont:captionFont 
								  constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
									  lineBreakMode:UILineBreakModeWordWrap];
			tmpHeight += (cs.height + 20);
		}
	}
	
	if (tmpHeight > 600)
		tmpHeight = 600;
	
	tableView.frame = CGRectMake(tableView.frame.origin.x, 
								 imageView.frame.origin.y + yTmp + 10, 
								 tableView.frame.size.width, 
								 tmpHeight + numberOfSection * 40);
	
	mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width,
											tableView.frame.origin.y + tableView.frame.size.height);
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)playSoundWithSection:(int)section row:(int)row {
/*	NSString *path = [[NSBundle mainBundle] pathForResource:[[[[soundsList objectAtIndex:section] objectForKey:kItems] objectAtIndex:row] objectForKey:kSoundName]
													 ofType:[[[[soundsList objectAtIndex:section] objectForKey:kItems] objectAtIndex:row] objectForKey:kSoundExtension]];*/
	
	
	//NSString * myPath =[[NSBundle mainBundle] resourcePath];
	//NSString *path = [myPath stringByAppendingPathComponent:[[[[[soundsList objectAtIndex:section] objectForKey:kItems] objectAtIndex:row] objectForKey:kSoundName]
	//														 stringByAppendingPathExtension:[[[[soundsList objectAtIndex:section] objectForKey:kItems] objectAtIndex:row] objectForKey:kSoundExtension]]];
	NSString * path = [[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[[[[[soundsList objectAtIndex:section] objectForKey:kItems] objectAtIndex:row] objectForKey:kSoundName] stringByAppendingPathExtension:[[[[soundsList objectAtIndex:section] objectForKey:kItems] objectAtIndex:row] objectForKey:kSoundExtension]]];
	
	NSURL *url = [NSURL fileURLWithPath:path];
	
	// Initialisation AVAudioPlayer
	AVAudioPlayer* tmpPlayer;
	tmpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
	self.player = tmpPlayer;
	[tmpPlayer release];
	
	[self.player play];
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	if (aTableView == tableView)
		return [soundsList count];
	return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	if (aTableView == indicationTableView1) {
		return 1;
	} else if (aTableView == tableView) {
		return [[[soundsList objectAtIndex:section] objectForKey:kItems] count];	
	}
	
	return 1;
}

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
	if (aTableView == tableView)
		return [[soundsList objectAtIndex:section] objectForKey:kSectionName];
	return nil;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier] autorelease];
    }
	
	
	cell.textLabel.numberOfLines = 0;
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	
	if (aTableView == tableView) {
		cell.textLabel.text = [[[[soundsList objectAtIndex:indexPath.section] objectForKey:kItems] objectAtIndex:indexPath.row] objectForKey:kText];
	} else if (aTableView == indicationTableView1) {
		cell.textLabel.font = [UIFont fontWithName:kHelvetica size:16];
		cell.textLabel.text = text1;
	} 
	
	return cell;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	
	if (aTableView == tableView)
		[self playSoundWithSection:indexPath.section row:indexPath.row];
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGSize cs;
	UIFont *captionFont = [UIFont fontWithName:kHelvetica size:16];
	
	if (aTableView == indicationTableView1) {
		cs = [text1 sizeWithFont:captionFont 
			   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
				   lineBreakMode:UILineBreakModeWordWrap];
		return cs.height + 40; 
	}
	
	return 40;
}

- (void)dealloc {
	[soundsList release];
	[textView1 release];
	[textView2 release];
	[imageView release];
	[tableView release];
	[text1 release];
	[text2 release];
	[image release];
    [super dealloc];
}

@end
