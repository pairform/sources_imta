//
//  AddCommentViewController.m
//  SupCast
//
//  Created by fgutie10 on 22/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "AddCommentViewController.h"
#import "UtilsCore.h"
#import "SHKActionSheet.h"
#import "SHK.h"
#import "SHKTwitter.h"
#import "SHKTwitterForm.h"
#import "SHKSharer.h"
#import "SHKCustomShareMenu.h"

@implementation AddCommentViewController

@synthesize statusPublicationGPS;
@synthesize statusPublicationTwitter;
@synthesize statusPublicationGPSLocal;
@synthesize statusPublicationTwitterLocal;
@synthesize motdepasseTwitter;
@synthesize userTwitter;

@synthesize username;
@synthesize password;
@synthesize titreParent;
@synthesize idEcranParent;
@synthesize idEcran;
@synthesize idBlog;
@synthesize categorie;
@synthesize estExercice;
@synthesize pseudoCategorieTitre;
@synthesize labelTitre;
@synthesize titre;
@synthesize textView;
@synthesize boutonTwitter;
@synthesize boutonLocalisation;
@synthesize boutonPublier;
@synthesize boutonProposerExercice;
@synthesize boutonCommenterExercice;
@synthesize locationManager;
@synthesize coordonneesGPS;
@synthesize indicator;
@synthesize receivedData;
@synthesize ressourceInfo;
@synthesize urlPage;
@synthesize idMessage;

#pragma mark -
#pragma mark View Load


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    DLog(@"");
    connectionStatus = 0;
	[indicator stopAnimating];
    
	self.title = self.titre;
    
    NSString * nom_categorie = [[[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"rank"] objectForKey:[self.ressourceInfo objectForKey:kRessourceElggidStr]] objectForKey:@"nom_categorie"];
    NSString * user_name = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
    
	NSString *pseudoCategorieTitreTexte = [[NSString alloc] initWithFormat:@"%@ - (%@)",user_name, nom_categorie];
	pseudoCategorieTitre.text = pseudoCategorieTitreTexte;
	
	if (estExercice)
    {	
        // boutons à afficher        
		boutonCommenterExercice.hidden = NO;
		boutonProposerExercice.hidden = NO;
        
        // boutons à ne pas afficher		
		boutonTwitter.hidden = YES;
		boutonLocalisation.hidden =YES;        
		boutonPublier.hidden = YES;
		
	}
	else
    {
        // boutons à afficher        
		boutonPublier.hidden = NO;
        boutonTwitter.hidden = NO;
		boutonLocalisation.hidden = NO;
        
        // boutons à ne pas afficher		
		boutonCommenterExercice.hidden = YES;
		boutonProposerExercice.hidden = YES;
        
        
        // Recherche des coordonnées GPS
        
		self.locationManager = [[CLLocationManager alloc] init];
		self.locationManager.delegate = self;
		[self.locationManager startUpdatingLocation];
        
	}
    
    
    // Récupération de regle de publication     
    self.statusPublicationGPS = NO;
    self.statusPublicationTwitter = NO;
    self.statusPublicationGPSLocal = NO;
    self.statusPublicationTwitterLocal = NO;
    
    self.motdepasseTwitter  = [[NSMutableString alloc] initWithString:@""];
    self.userTwitter = [[NSMutableString alloc] initWithString:@""];
        
    NSString *thePathFilenameRegle = [self dataFilePath:kFilenameRegle];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:thePathFilenameRegle])
    {
        self.statusPublicationGPS = NO;
        self.statusPublicationTwitter = NO;
        [self.userTwitter setString:@""];
        [self.motdepasseTwitter setString:@""];
        
    }
    
    NSArray *myArray = [[NSArray alloc] initWithContentsOfFile:thePathFilenameRegle];
    self.statusPublicationGPS =[[[myArray objectAtIndex:0] objectForKey:kGPS] boolValue];
    self.statusPublicationTwitter =[[[myArray objectAtIndex:1] objectForKey:kTWITTER] boolValue];
    
    if (self.statusPublicationGPS)
    {
        
        BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
        BOOL locationAvailable =  ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized);
        
        if (!locationAllowed) 
        {            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation est désactivé." 
                                                            message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            
            self.statusPublicationGPSLocal = NO;
        }
        else 
        {
            if (!locationAvailable)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation de l'Application est désactivé." 
                                                                message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
                [alert show];
                
                self.statusPublicationGPSLocal = NO;
            }
            else
            {
                self.statusPublicationGPSLocal = YES;
            }
        }
        
    }
    
    else        
    {
        self.statusPublicationGPSLocal = self.statusPublicationGPS;
    }
    
    
    self.statusPublicationTwitterLocal = self.statusPublicationTwitter;
    
    if ( [[myArray objectAtIndex:1] objectForKey:kUSER] == nil)
        [self.userTwitter setString:@""];
    else
        [self.userTwitter setString:[[myArray objectAtIndex:1] objectForKey:kUSER]];
    
    if ( [[myArray objectAtIndex:1] objectForKey:kMDP] == nil)
        [self.motdepasseTwitter setString:@""];
    else
        [self.motdepasseTwitter setString:[[myArray objectAtIndex:1] objectForKey:kMDP]];
    
    
    [self gestionBouton];
    
    
    [super viewDidLoad];
}


#pragma mark -
#pragma mark Actions 


-(IBAction)sendMessage {
    
    DLog(@"");
	NSString *message = [[NSString alloc] initWithString:textView.text];
    
	if ([message length] != 0) 
    {
        if (self.statusPublicationTwitterLocal)
        {
            //Publier sur Twitter
            [self tweet:message];
            
            if (self.statusPublicationGPSLocal)
            {
                //Publier sur elgg avec localisation
                [self publierMessageAvecLocalisation:message];
            }
            else
            {
                //Publier sur elgg
                [self publierMessage];
            }
            
            
        } 
        else
        {    
            if (self.statusPublicationGPSLocal)
            {
                //Publier sur elgg avec localisation
                [self publierMessageAvecLocalisation:message];
            }
            else
            {
                //Publier sur elgg
                [self publierMessage];
            }
        }
    }        
    
	else 
    {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez composer un message avant de l'envoyer"
													   delegate:self
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
	}
    
}

-(void)gestionBouton
{
    DLog(@"");
    
    UIImage *image = (self.statusPublicationGPSLocal) ? [UIImage imageNamed:kImageLocalisationActivee] : [UIImage imageNamed:kImageLocalisationDesactivee];    
    //CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    //boutonLocalisation.frame = frame;	// match the button's size with the image size
    [boutonLocalisation setTitle:@"" forState:UIControlStateNormal];        
    [boutonLocalisation setImage:image forState:UIControlStateNormal];
    boutonLocalisation.hidden = NO;
    // [image release];  
    
    UIImage *newImage = (self.statusPublicationTwitterLocal) ? [UIImage imageNamed:kImageTwitterActivee]:[UIImage imageNamed:kImageTwitterDesactivee];
    //CGRect newFrame = CGRectMake(0.0, 0.0, newImage.size.width, newImage.size.height);
    //boutonTwitter.frame = newFrame;	// match the button's size with the image size
    [boutonTwitter setTitle:@"" forState:UIControlStateNormal];        
    [boutonTwitter setImage:newImage forState:UIControlStateNormal];
    boutonTwitter.hidden = NO;
    //   [newImage release];  
    
    
    
    if (self.statusPublicationTwitterLocal)
    {
        if (self.statusPublicationGPSLocal)
        {
            [boutonPublier setTitle:kPublierTwitterEtLocalisation forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        }
        else
        {
            [boutonPublier setTitle:kPublierTwitter forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
    } 
    else
    {
        if (self.statusPublicationGPSLocal)
        {
            [boutonPublier setTitle:kPublierLocalisation forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
        else
        {
            [boutonPublier setTitle:kPublier forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
    }
    
}

- (IBAction)activeLocalisation:(id)sender
{
    DLog(@"");
    
    self.statusPublicationGPSLocal = (self.statusPublicationGPSLocal) ? NO : YES;
    
    
    if (self.statusPublicationGPSLocal)
    {
        
        
        BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
        BOOL locationAvailable =  ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized);
        
        if (!locationAllowed) 
        {            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation est désactivé." 
                                                            message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            
            self.statusPublicationGPSLocal = NO;
        }
        else 
        {
            if (!locationAvailable)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation de l'Application est désactivé." 
                                                                message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
                
                [alert show];
                self.statusPublicationGPSLocal = NO;
            }
            else
            {
                self.statusPublicationGPSLocal = YES;
            }
        }
        
    }
    
    [self gestionBouton];
}
- (IBAction)activeTwitter:(id)sender
{
    DLog(@"");
    
    self.statusPublicationTwitterLocal = (self.statusPublicationTwitterLocal) ? NO : YES;
    [self gestionBouton];
}

- (IBAction)backgroundTap:(id)sender {
    
	[textView resignFirstResponder];
	boutonValider.enabled = YES;
    
}

- (IBAction)gestionClavier {
    DLog(@"");
    
	
	if([textView isFirstResponder])
    {		
		[textView resignFirstResponder];
		boutonValider.enabled = YES;
	}
	else
    {
		boutonValider.enabled = NO;
		[textView becomeFirstResponder];
	}
}



#pragma mark -
#pragma mark dealloc


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {

    [super viewDidUnload];
    
    [self setMotdepasseTwitter:nil];
    [self setUserTwitter:nil];
    [self setUsername:nil];
    [self setPassword:nil];
    [self setTitreParent:nil];
    [self setIdEcranParent:nil];
    [self setIdEcran:nil];
    [self setIdBlog:nil];
    [self setCategorie:nil];
    [self setPseudoCategorieTitre:nil];
    [self setLabelTitre:nil];
    [self setTitre:nil];
    [self setTextView:nil];
    [self setBoutonTwitter:nil];
    [self setBoutonLocalisation:nil];
    [self setBoutonPublier:nil];
    [self setBoutonProposerExercice:nil];
    [self setBoutonCommenterExercice:nil];
    [self setLocationManager:nil];
    [self setCoordonneesGPS:nil];
    [self setIndicator:nil];
    [self setReceivedData:nil];
    [self setRessourceInfo:nil];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



#pragma mark -
#pragma mark Publication

-(void)publierMessageAvecLocalisation:(NSString *)aMessage {
	
    DLog(@"");
    
	NSMutableString *messageAvant = [[NSMutableString alloc] initWithString:textView.text];
	
	if ([messageAvant length] != 0)
    {		
		if (self.coordonneesGPS == nil)            
        {			
            
            switch ([CLLocationManager authorizationStatus]) {
                case kCLAuthorizationStatusNotDetermined:
                    DLog(@"kCLAuthorizationStatusNotDetermined");
                    break;
                case kCLAuthorizationStatusRestricted:
                    DLog(@"kCLAuthorizationStatusRestricted");
                    break;
                case kCLAuthorizationStatusDenied:
                    DLog(@"kCLAuthorizationStatusDenied");
                    break;
                case kCLAuthorizationStatusAuthorized:
                    DLog(@"kCLAuthorizationStatusAuthorized");
                    break;
                    
                default:
                    break;
            } 
            
            switch ([CLLocationManager locationServicesEnabled]) {
                case YES:
                    DLog(@"locationServicesEnabled");
                    break;
                case NO:
                    DLog(@"locationServicesDisabled");
                    break;
                    
                default:
                    break;
            } 
            
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
															message:@"a) Si vous avez accepté l'envoi des données de localisation, veuillez attendre le chargement des coordonnées GPS.\nb) Sinon, pour utiliser cette fonctionnalité, autorisez l'envoi des données de localisation."
														   delegate:self
												  cancelButtonTitle:@"Accepter"
												  otherButtonTitles:nil];
			
			[alert show];
		}		
		else
        {			
            
			NSString *message = [[NSString alloc] initWithFormat:@"%@</string><key>longitude</key><string>%3.5f</string><key>latitude</key><string>%3.5f",messageAvant,self.coordonneesGPS.coordinate.longitude,self.coordonneesGPS.coordinate.latitude];
			DLog(@"message is %@", message);
			//[self publierMessage:message];
		}        
	}
	else
    {		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez composer un message avant de l'envoyer"
													   delegate:self
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];		
		[alert show];
	}
}


-(void)tweet:(NSString *)aMessage
{
    DLog(@"");
    
    [SHK logoutOfAll];
    NSString * message =[[NSString alloc] initWithFormat:@"#SupCast %@",aMessage];
    SHKItem *item = [[SHKItem alloc] init];
    item.shareType = SHKShareTypeText;
    if ([message length] > 140)
        item.text = [message substringToIndex:139];
    else
        item.text = message;
    
    [SHKTwitter canShare];
    
}

-(void)publierMessage {
    
	[TestFlight passCheckpoint:@"Publication de Message"];
    
    NSMutableDictionary * params;
    NSString * message = [[NSMutableString alloc] initWithString:textView.text];
    
    if([self idMessage])
        params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[self.ressourceInfo objectForKey:kRessourceElggidStr], @"id_ressource", [self urlPage], @"nom_page", [self idMessage], @"id_message", message, @"message", nil];
    else
       params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[self.ressourceInfo objectForKey:kRessourceElggidStr], @"id_ressource", [self urlPage], @"nom_page",  message, @"message", nil];
    
    
    [self post:[NSString stringWithFormat:@"%@/webServices/messages/enregistrerMessage.php",sc_server]
            param: params
            callback: ^(NSDictionary *wsReturn) {
                if([wsReturn objectForKey:@"result"])
                {
                    [UtilsCore rafraichirMessages];
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                                    message:@"Erreur de connexion"
                                                                   delegate:self
                                                          cancelButtonTitle:@"Accepter"
                                                          otherButtonTitles:nil];
                    
                    [alert show];
                }
            }
            errorMessage:@"Vous devez être connecté à Internet pour envoyer des messages."];
}
#pragma mark -
#pragma mark Core Location Delegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DLog(@"");
}
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    DLog(@"");
}
- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    DLog(@"");
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    DLog(@"");
}
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    DLog(@"");
}
- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    DLog(@"");
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    DLog(@"");    
	//self.coordonneesGPS = [NSString stringWithFormat:@"\n\nGPS : (%3.5f , %3.5f)",newLocation.coordinate.latitude,newLocation.coordinate.longitude];
	self.coordonneesGPS = newLocation;
	
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    DLog(@"");    
    
}

@end