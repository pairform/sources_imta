//
//  CreateUserViewController.h
//  SupCast
//
//  Created by fgutie10 on 28/06/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CreateUserViewController : UIViewController <UIActionSheetDelegate> {
	
	NSString *nomUtilisateur;
	NSString *prenomUtilisateur;
	NSString *eMailUtilisateur;
	NSString *pseudoUtilisateur;
	NSString *motDePasseUtilisateur;
	
	int connectionStatus;
	
	NSMutableData *receivedData;
	
	NSMutableString *messageString;
	NSMutableDictionary *currentMessageDict;
	NSString *currentElementName;
	NSMutableString *currentText;
	
	NSSet *interestingTags;
	
	BOOL doneCreateUser;
	
}

@property(nonatomic,weak) IBOutlet UITextField *nomTextField;
@property(nonatomic,weak) IBOutlet UITextField *prenomTextField;
@property(nonatomic,weak) IBOutlet UITextField *eMailTextField;
@property(nonatomic,weak) IBOutlet UITextField *pseudoTextField;
@property(nonatomic,weak) IBOutlet UITextField *motDePasseTextField;

@property(nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;

@property(nonatomic,strong) NSMutableData *receivedData;

@property(nonatomic,strong) NSString *nomUtilisateur;
@property(nonatomic,strong) NSString *prenomUtilisateur;
@property(nonatomic,strong) NSString *eMailUtilisateur;
@property(nonatomic,strong) NSString *pseudoUtilisateur;
@property(nonatomic,strong) NSString *motDePasseUtilisateur;
@property(nonatomic,strong) NSMutableString *messageString;

-(IBAction)enregistrer:(id)sender;

-(IBAction)textFieldDone:(id)sender;
-(IBAction)backgroundTap:(id)sender;
-(void) inscription;
-(NSString *)dataFilePath:(NSString *)fileName; 
//-(void)applicationWillTerminate:(NSNotification *)notification;

@end