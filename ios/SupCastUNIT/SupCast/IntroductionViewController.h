//
//  IntroductionViewController.h
//  SupCast
//
//  Created by Phetsana on 11/09/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface IntroductionViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UIImageView *fondEcranImageView;
	IBOutlet UIImageView *imageView;
	IBOutlet UITableView *tableView;
	NSArray *array;
	UIImage *image;
	NSString *idecran;
	NSString *dirRes;
	NSDictionary *dicoRacine;
}

@property (nonatomic, retain) NSDictionary *dicoRacine;
@property (nonatomic, retain) NSString *idecran;
@property (nonatomic, retain) NSArray *array;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) NSString *dirRes;

- (void)initImage;

@end
