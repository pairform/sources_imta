//
//  CreateUserViewController.m
//  SupCast
//
//  Created by fgutie10 on 28/06/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "CreateUserViewController.h"

#define INTERESTING_TAG_NAMES @"nom", @"message", nil


@implementation CreateUserViewController

@synthesize nomTextField;
@synthesize prenomTextField;
@synthesize eMailTextField;
@synthesize pseudoTextField;
@synthesize motDePasseTextField;
@synthesize nomUtilisateur;
@synthesize prenomUtilisateur;
@synthesize eMailUtilisateur;
@synthesize pseudoUtilisateur;
@synthesize motDePasseUtilisateur;
@synthesize messageString;
@synthesize indicator;
@synthesize receivedData;

#pragma mark -
#pragma mark Username Persistence Methods

-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}

#pragma mark -

-(void)viewDidLoad {
    DLog(@"");
	
	doneCreateUser = NO;
	
	/*
     UIApplication *app = [UIApplication sharedApplication];
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(applicationWillTerminate:)
     name:UIApplicationWillTerminateNotification
     object:app];
	 */
	
	UIBarButtonItem *boutonEnregistrer = [[UIBarButtonItem alloc] initWithTitle:@"Valider"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(enregistrer:)];
    
	self.navigationItem.rightBarButtonItem = boutonEnregistrer;
	self.title = @"Compte";
	[indicator stopAnimating];
	interestingTags = [[NSSet alloc] initWithObjects:INTERESTING_TAG_NAMES];
	[super viewDidLoad];
	
}

#pragma mark -

-(IBAction)enregistrer:(id)sender {
    DLog(@"");
	
	//On vérifie que tous les champs du formulaire sont remplis
	self.nomUtilisateur = nomTextField.text;
	self.prenomUtilisateur = prenomTextField.text;
	self.eMailUtilisateur = eMailTextField.text;
	self.pseudoUtilisateur = pseudoTextField.text;
	self.motDePasseUtilisateur = motDePasseTextField.text;
	
	BOOL alerted = NO;
	
	NSString *vide = @"";
	
	if ([self.nomUtilisateur isEqualToString: vide] || [self.prenomUtilisateur isEqualToString: vide] || [self.eMailUtilisateur isEqualToString: vide] ||
		[self.pseudoUtilisateur isEqualToString: vide] || [self.motDePasseUtilisateur isEqualToString: vide]) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez remplir toutes les données du formulaire"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Vérification compte mail
	NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
	NSString *emailUtilisateurMinuscules = [self.eMailUtilisateur lowercaseString];
	BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:emailUtilisateurMinuscules];
	
	if (!myStringMatchesRegEx && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez saisir une adresse mail valide"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Le pseudo doit comporter au moins 4 caractères (restriction imposée par Elgg)
	if((self.pseudoUtilisateur.length < 4) && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Le nom d'utilisateur doit comporter au moins 4 caractères"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Le mot de passe doit comporter au moins 6 caractères (restriction imposée par Elgg)
	if((self.motDePasseUtilisateur.length < 6) && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Le mot de passe doit comporter au moins 6 caractères"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Vérification nom utilisateur: caractères alphanumériques
	NSString *loginRegEx = @"^[0-9a-zA-Z]*$";
	NSPredicate *loginRegExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", loginRegEx];
	BOOL stringMatchesRegEx = [loginRegExPredicate evaluateWithObject:self.pseudoUtilisateur];
	
	if (!stringMatchesRegEx && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Votre nom d'utilisateur doit comporter uniquement des caractères alphanumériques [a-z 0-9]"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}	
	
	//Si tout est OK
	if (![self.nomUtilisateur isEqualToString: vide] && ![self.prenomUtilisateur isEqualToString: vide] && ![self.eMailUtilisateur isEqualToString: vide] &&
		![self.pseudoUtilisateur isEqualToString: vide] && ![self.motDePasseUtilisateur isEqualToString: vide] && myStringMatchesRegEx &&
		(self.pseudoUtilisateur.length >= 4) && (self.motDePasseUtilisateur.length >= 6) && stringMatchesRegEx) {
		
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Etes-vous d'accord avec les conditions d'utilisation de cette application ?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Je refuse"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"J'accepte",nil];
        
		[actionSheet showInView:self.view];
        
	}
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if(buttonIndex == 0) {
		
		[nomTextField resignFirstResponder];
		[prenomTextField resignFirstResponder];
		[eMailTextField resignFirstResponder];
		[pseudoTextField resignFirstResponder];
		[motDePasseTextField resignFirstResponder];
		
        [self inscription];
        
		//[self launchConnection];
		
	}
	else
		[self.navigationController popViewControllerAnimated:YES];
	
}

-(void)inscription
{
    [TestFlight passCheckpoint:@"Création d'un compte"];
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             self.nomUtilisateur,@"name",
                             self.prenomUtilisateur,@"surname",
                             self.eMailUtilisateur,@"email",
                             self.pseudoUtilisateur,@"username",
                             self.motDePasseUtilisateur, @"password", nil];
    
    
    [self post:[NSString stringWithFormat:@"%@/webServices/compte/registerCompte.php",sc_server]
         param:params
      callback:^(NSDictionary *wsReturn) {
          NSString * code = [wsReturn objectForKey:@"code"];
          
          if ([code isEqualToString: @"ok"]) {
              
              NSMutableArray *array = [[NSMutableArray alloc] init];
              [array addObject:pseudoTextField.text];
              [array addObject:motDePasseTextField.text];
              
              [array writeToFile:[self dataFilePath:kFilenameData] atomically:YES];
              
              
              UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Confirmation d'adresse électronique" message:@"Veuillez cliquer sur le lien que vous allez recevoir dans votre messagerie pour valider l'enregistrement de votre compte." delegate:nil cancelButtonTitle:@"Accepter" otherButtonTitles:nil];
              
              
              [alert show];
              
              [self.navigationController popViewControllerAnimated:YES];
              
          }
          
          if ([code isEqualToString: @"username"]) {
              
              
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                              message:@"Votre nom d'utilisateur est déjà utilisé.\nVeuillez en choisir un autre."
                                                             delegate:self
                                                    cancelButtonTitle:@"Accepter"
                                                    otherButtonTitles:nil];
              
              [alert show];
              
          }
          
          if ([code isEqualToString: @"email"]) {
              
              
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                              message:@"Votre adresse mail est déjà utilisée.\nVeuillez en saisir une autre."
                                                             delegate:self
                                                    cancelButtonTitle:@"Accepter"
                                                    otherButtonTitles:nil];
              
              [alert show];
              
          }
          if (([code isEqualToString:@"error"]) || (code == NULL))
          {
              
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                              message:@"Erreur d'enregistrement : veuillez contacter le CAPE de l'Ecole des Mines de Nantes."
                                                             delegate:self
                                                    cancelButtonTitle:@"Accepter"
                                                    otherButtonTitles:nil];
              
              [alert show];
          }
      }
     errorMessage:@"Vous devez être connecté à Internet pour créer un compte."];
}

-(IBAction)textFieldDone:(id)sender {
    DLog(@"");
    [sender resignFirstResponder];
}

- (IBAction)backgroundTap:(id)sender {
    DLog(@"");
	[nomTextField resignFirstResponder];
	[prenomTextField resignFirstResponder];
	[eMailTextField resignFirstResponder];
	[pseudoTextField resignFirstResponder];
	[motDePasseTextField resignFirstResponder];
}

//
//- (void)viewDidUnload {
//    DLog(@"");
//    [super viewDidUnload];
//	self.messageString = nil;
//	self.nomTextField = nil;
//	self.prenomTextField = nil;
//	self.eMailTextField = nil;
//	self.pseudoTextField = nil;
//	self.motDePasseTextField = nil;
//	self.indicator = nil;
//}

@end