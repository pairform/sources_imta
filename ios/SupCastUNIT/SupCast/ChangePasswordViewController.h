//
//  ChangePasswordViewController.h
//  SupCast
//
//  Created by wbao11 on 19/07/11.
//  Copyright (c) 2011 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserAccountViewController.h"


@interface ChangePasswordViewController : UIViewController <UITabBarDelegate,UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource,UIAlertViewDelegate> {
        
    IBOutlet UITableView *tableView;
    IBOutlet UITextField *myTextField0;
    IBOutlet UITextField *myTextField1;
    IBOutlet UITextField *myTextField2;
    IBOutlet UITableViewCell *customCell;              
}
@property(nonatomic,weak) IBOutlet   UIButton *confirmer;
@property(nonatomic,strong) NSString *currentPassword;
@property(nonatomic,strong) NSString *passwordNewA;
@property(nonatomic,strong) NSString *passwordNewB;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSMutableData *receivedData;
@property(nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
-(IBAction)confirmChangePassword:(id)sender;
-(IBAction)textFieldDone:(id)sender;
-(IBAction)backgroundTap:(id)sender;
-(void)changePassword;
@end
