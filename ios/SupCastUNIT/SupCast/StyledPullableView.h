
#import "PullableView.h"

/**
 Subclass of PullableView that uses a background image
 @author Fabio Rodella fabio@crocodella.com.br
 */
@interface StyledPullableView : PullableView <UIScrollViewDelegate>

@property (nonatomic,strong) UIPageControl * helpPageControl;
- (id)initWithFrameAndTextArray:(CGRect)frame:(NSArray*)textArray;
- (id)initWithFrameAndText:(CGRect)frame:(NSString*)text;
- (void)updateTextScroll:(NSArray*)textArray;
- (void)createScroll:(NSArray*)textArray;
- (void)createDescription:(NSString*)text;
- (void)createBackground:(CGRect)frame;
@end
