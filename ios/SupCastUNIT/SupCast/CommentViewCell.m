//
//  CommentViewCell.m
//  SupCast
//
//  Created by fgutie10 on 29/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "CommentViewCell.h"

#define kFontSize 14

@implementation CommentViewCell

@synthesize labelCommentaire;
@synthesize labelUsername, labelReputation;
@synthesize estSupprime;

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    DLog(@"");
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    
		labelUsername = [[UILabel alloc]init];
		labelUsername.numberOfLines = 1;
		labelUsername.font = [UIFont boldSystemFontOfSize:kFontSize];
		labelUsername.backgroundColor = [UIColor clearColor];
		labelUsername.textColor = [UIColor blackColor];
		labelUsername.text = @"xxxxxxxxxx";

        
		labelCommentaire = [[UILabel alloc]init];
		labelCommentaire.font = [UIFont systemFontOfSize:kFontSize];
		labelCommentaire.textColor = [UIColor blackColor];
		labelCommentaire.lineBreakMode = UILineBreakModeWordWrap;
		labelCommentaire.numberOfLines = 0;
		labelCommentaire.backgroundColor = [UIColor clearColor];

		labelReputation = [[UILabel alloc]init];
		labelReputation.numberOfLines = 1;
		labelReputation.font = [UIFont systemFontOfSize:kFontSize];
		labelReputation.backgroundColor = [UIColor clearColor];
		labelReputation.textColor = [UIColor grayColor];
		labelReputation.text = @"xxxxxxxxxx";
        
		[self.contentView addSubview:labelUsername];
		[self.contentView addSubview:labelCommentaire];
		[self.contentView addSubview:labelReputation];
    }

    return self;
	
}

+ (CGFloat)cellHeightForCaption:(NSString *)caption width:(CGFloat)width {
    DLog(@"");
	
    UIFont *captionFont = [UIFont systemFontOfSize:kFontSize];
    CGSize cs = [caption sizeWithFont:captionFont 
					constrainedToSize:CGSizeMake(width - 15, FLT_MAX) 
						lineBreakMode:UILineBreakModeWordWrap];
	
	return cs.height;
	
}

- (void)layoutSubviews {
    DLog(@"");
	
	[super layoutSubviews];
	
    //Si la hauteur est insuffisante pour l'affichage du contenu
    if(self.contentView.bounds.size.height <= 80)
    {
        [[self contentView] setBounds:CGRectMake(0,0, self.contentView.bounds.size.width, 80)];
    }
    
	CGFloat cellHeight = self.contentView.bounds.size.height;
	CGFloat cellWidth = self.contentView.bounds.size.width - 15;
	
	CGRect frame;	
	
	frame = CGRectMake(6, 6, cellWidth, 16);
	labelUsername.frame = frame;	
    
	frame = CGRectMake(6, 28, cellWidth, cellHeight-56);
	labelCommentaire.frame = frame;
	
	frame = CGRectMake(6, cellHeight-22, cellWidth, 16);
	labelReputation.frame = frame;	
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    DLog(@"");
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

- (void)dealloc {
    DLog(@"");

}

@end
