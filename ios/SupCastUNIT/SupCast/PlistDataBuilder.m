//
//  PlistDataBuilder.m
//  SupCast
//
//  Created by Phetsana on 15/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "PlistDataBuilder.h"


@implementation PlistDataBuilder

- (NSArray *)buildData:(NSString *)fileName {

	NSArray *array = [[NSArray alloc] initWithContentsOfFile:fileName];

	return [array autorelease];
}

@end
