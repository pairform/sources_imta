//
//  TitreViewController.h
//  SupCast
//
//  Created by Phetsana on 20/07/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GestionReseauSocialViewController.h"
#import "MainViewController.h"
#import "MainViewCell.h"
#import "ModuleViewController.h"
#import "Reachability.h"

@interface TitreViewController : GestionReseauSocialViewController <UITableViewDelegate, UITableViewDataSource> {
    
	IBOutlet UIImageView *fondEcranImageView;
	NSArray *arrayRacine; // Tableau qui va stocker la structure de données
	NSMutableArray * sequenceNameListe;
    BOOL connected;
}
@property(nonatomic,strong) NSArray *arrayRacine;
@property(nonatomic,strong) NSMutableArray * sequenceNameListe;
@property(nonatomic,strong) NSString * guid;
@property (weak, nonatomic) IBOutlet UIImageView *logoEtablissement;

@property NetworkStatus   statusConnexionReseau;

-(void)recupererNiveau:(NSString *)ressourceid;

-(NetworkStatus) etatConnexionReseau;
- (BOOL)connected;
@end
