//
//  DialogCell.m
//  SupCast
//
//  Created by Phetsana on 15/07/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "DialogCell.h"

#define kFontSize 20
#define kStdButtonWidth 106.0
#define kStdButtonHeight 40.0


@implementation DialogCell

@synthesize dialogLabel, dialogAnswerLabel;

- (void)action:(id)sender {
}

- (id)initWithFrame:(CGRect)frame 
	reuseIdentifier:(NSString *)reuseIdentifier 
			 string:(NSString *)aString 
	  numberOfField:(int)n {
//    if ((self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier])) {
        if ((self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier])) {
        // Initialization code
            [super setFrame:frame];
            
            s = aString;
		
		dialogLabel = [[UILabel alloc]init];
		dialogLabel.font = [UIFont systemFontOfSize:kFontSize];
		dialogLabel.lineBreakMode = UILineBreakModeWordWrap;
		dialogLabel.numberOfLines = 0;
		
		dialogLabel.text = s;
		
		dialogAnswerLabel = [[UILabel alloc]init];
		dialogAnswerLabel.font = [UIFont systemFontOfSize:kFontSize];
		dialogAnswerLabel.lineBreakMode = UILineBreakModeWordWrap;
		dialogAnswerLabel.numberOfLines = 0;
		
		textFieldList = [[NSMutableArray alloc] init];
		for (int i = 0; i < n; i++) {
			UIButton *roundedButtonType = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
			roundedButtonType.frame = CGRectMake(182.0, 5.0, kStdButtonWidth, kStdButtonHeight);
			[roundedButtonType setTitle:@"Rounded" 
							   forState:UIControlStateNormal];
			roundedButtonType.backgroundColor = [UIColor clearColor];
			[roundedButtonType addTarget:self 
								  action:@selector(action:) 
						forControlEvents:UIControlEventTouchUpInside];
			[textFieldList addObject:roundedButtonType];
			[roundedButtonType release];
		}
		
		for (int i = 0; i < n + 1; i++) {
			UILabel *label = [[UILabel alloc] init];
			label.font = [UIFont systemFontOfSize:kFontSize];
			label.lineBreakMode = UILineBreakModeWordWrap;
			label.numberOfLines = 0;
			[self.contentView addSubview:label];
		}
		
		[self.contentView addSubview:dialogLabel];
		[self.contentView addSubview:dialogAnswerLabel];
    }
	
    return self;
}

+ (CGFloat)cellHeightForCaption:(NSString *)caption 
						  width:(CGFloat)width {
    UIFont *captionFont = [UIFont boldSystemFontOfSize:kFontSize];
    CGSize cs = [caption sizeWithFont:captionFont 
					constrainedToSize:CGSizeMake(width - 50, FLT_MAX) 
						lineBreakMode:UILineBreakModeWordWrap];
	
	return cs.height + 20;
}

- (void)layoutSubviews {
	[super layoutSubviews];
	
	CGFloat cellHeight = self.contentView.bounds.size.height;
	CGFloat cellWidth = self.contentView.bounds.size.width - 20;
	
	CGRect frame;	
	frame = CGRectMake(6, 0, cellWidth, cellHeight);
	dialogLabel.frame = frame;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [super dealloc];
}


@end
