//
//  InfoViewController.h
//  SupCast
//
//  Created by Phetsana on 15/09/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface InfoViewController : UIViewController <UIWebViewDelegate> {

	IBOutlet UIWebView *webView;
}

@property(nonatomic,strong) NSMutableURLRequest * requestURL;
@property NetworkStatus statusConnexionReseau;

@end
