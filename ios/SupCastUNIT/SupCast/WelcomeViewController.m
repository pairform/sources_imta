//
//  WelcomeViewController.m
//  SupCast
//
//  Created by fgutie10 on 28/06/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//
#import "SupCastAppDelegate.h"
#import "WelcomeViewController.h"

@implementation WelcomeViewController
@synthesize statusConnexionReseau;

@synthesize loginVisiteur;
@synthesize loginUtilisateur;
@synthesize gestionCompte;
@synthesize pseudoTextField;
@synthesize motDePasseTextField;
@synthesize pseudoUtilisateur;
@synthesize motDePasseUtilisateur;
@synthesize receivedData, receivedDataInfos;
@synthesize connectionStatus;
@synthesize HUD;
@synthesize welcomeConnection, infosConnection;


// Status de la connexion internet  
-(NetworkStatus) etatConnexionReseau {
    self.statusConnexionReseau = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    return self.statusConnexionReseau;
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    /*
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }*/
    return NO;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if (!(HUD.hidden))
    {
        CGAffineTransform myT;
        if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            myT = CGAffineTransformMakeRotation(M_PI*1.5);
        } 
        else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) 
        {
            myT =  CGAffineTransformMakeRotation(M_PI/2);
        } 
        else if (toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            myT =  CGAffineTransformMakeRotation(-M_PI);
        }
        else
        {
            myT =  CGAffineTransformIdentity;
        }
        
        self.HUD.transform = myT; 
        
    }
}

#pragma mark -
#pragma mark Username Persistence Methods

-(NSString *)dataFilePath:(NSString *)fileName {
    DLog(@"");  
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}

/*
 -(void)applicationWillTerminate:(NSNotification *)notification {
 
 NSMutableArray *array = [[NSMutableArray alloc] init];
 [array addObject:pseudoTextField.text];
 [array addObject:motDePasseTextField.text];
 
 [array writeToFile:[self dataFilePath] atomically:YES];
 [array release];
 
 }
 */

#pragma mark -

-(void)changeScreen:(NSArray *)plist done:(BOOL)done
{
    
    DLog(@"");    
    NSString *nom;
    NSString *status;
    NSString *niveau = @"Fuck";
    NSString *guid = @"35";
    if (done)
    {    
        if ([plist count] != 0 )
        {
            NSDictionary *message = [[NSDictionary alloc] initWithDictionary:(NSDictionary *)[plist objectAtIndex:0]];
            nom = [[NSString alloc] initWithString:[message objectForKey:@"nom"]];
            status = [[NSString alloc] initWithString:[message objectForKey:@"message"]];
            niveau = [[NSString alloc] initWithString:[message objectForKey:@"niveau"]];
            guid = [[NSString alloc] initWithString:[message objectForKey:@"guid"]];
            
        }
    
        if (connectionStatus == 4)
        {
            [self gestionDeCompte];
        }
        else
        {
            
            if(userAccount == 0)
            {
                NSMutableArray *array = [[NSMutableArray alloc] init];
                [array addObject:pseudoTextField.text];
                [array addObject:motDePasseTextField.text];
                
                [array writeToFile:[self dataFilePath:kFilenameData] atomically:YES];
                
                //-----------------------
                //appel vers root 
                RootViewController * rootViewController = [RootViewController alloc];
                
                rootViewController.password = motDePasseUtilisateur;                                            
                rootViewController.username = pseudoUtilisateur;     
                rootViewController.nouveauNiveau = niveau;     
                rootViewController.estVisiteur = NO;     
                rootViewController.guid = guid;
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    (void)[rootViewController initWithNibName:@"RootViewController-iPad" bundle:nil];
                else 
                    (void)[rootViewController initWithNibName:@"RootViewController" bundle:nil];
                
                [self.navigationController pushViewController:rootViewController 
                                                     animated:YES];
                //-----------------------
                
                
                
            }
            
        }
        
    }
}
- (void)showLoadingProgress {
	if (HUD==nil) {
        
        DLog(@"showLoadingProgress");
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        CGRect windowFrame = mainWindow.frame;
        DLog(@"window=%@", [mainWindow description]);
        CGRect progressHUDrect = CGRectMake(windowFrame.origin.x,
                                            windowFrame.origin.y, 
                                            windowFrame.size.width,
                                            windowFrame.size.height);
        progressHUDrect.size.height = 120.0;
        progressHUDrect.origin.y = (windowFrame.size.height / 2.0) - (progressHUDrect.size.height / 2);
        progressHUDrect.origin.x = 80.0;
        progressHUDrect.size.width = windowFrame.size.width - (progressHUDrect.origin.x * 2);
        
        UIInterfaceOrientation orient = [self interfaceOrientation];
        CGAffineTransform myT;
        if (orient == UIInterfaceOrientationLandscapeLeft)
        {
            myT = CGAffineTransformMakeRotation(M_PI*1.5);
        } 
        else if (orient == UIInterfaceOrientationLandscapeRight) 
        {
            myT =  CGAffineTransformMakeRotation(M_PI/2);
        } 
        else if (orient == UIInterfaceOrientationPortraitUpsideDown)
        {
            myT =  CGAffineTransformMakeRotation(-M_PI);
        }
        else
        {
            myT =  CGAffineTransformIdentity;
        }
        
        
        self.HUD = [[WBProgressHUD alloc] initWithFrame:progressHUDrect];
        self.HUD.transform = myT; 
        
        HUD.colorBackground = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.30];
        DLog(@"alloc OK : HUD=%@", [HUD description]);
        [HUD setText:@"Chargement …"];
        [HUD showInView:mainWindow];
    }
}

- (void)hideLoadingProgress {
	DLog(@"hideLoadingProgress");
	if (HUD) {
		HUD.hidden = YES;
		[HUD removeFromSuperview];
		self.HUD = nil;
	}
}



#pragma mark -
#pragma mark NSURLConnection Callbacks
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	if(connection == infosConnection)
        [self.receivedDataInfos setLength:0];
    else 
    [self.receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if(connection == infosConnection)
        [self.receivedDataInfos appendData:data];
    else 
    [self.receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DLog(@"");  
    
    [self hideLoadingProgress];
	self.receivedData = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:nil
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
    DLog(@"");
    BOOL done = NO;
    [self hideLoadingProgress];
    
    if ((connectionStatus == 1)  || (connectionStatus == 4)) 
    {
        //Serialisation du fichier reçu        
        NSString *error; 
        NSPropertyListFormat format;
        NSArray * plist = [NSPropertyListSerialization propertyListFromData:self.receivedData
                                                           mutabilityOption:NSPropertyListImmutable
                                                                     format:&format
                                                           errorDescription:&error]; 
        if(!plist)
        { 
            DLog(@"Error: %@",error); 
        }
        else
        {
            if (!done)
            {
                done = YES;
            }
        }
        
        [self changeScreen:plist done:done];
        
    }
    else if(connection == infosConnection)
    {
        NSString * error; 
        NSPropertyListFormat format; 
        NSArray * arrayInfosDynamique = [NSPropertyListSerialization propertyListFromData:self.receivedDataInfos
                                                                         mutabilityOption:NSPropertyListImmutable
                                                                                   format:&format
                                                                         errorDescription:&error]; 
        
        //Si le webservice a renvoyé un pList,on update les infos
        if(arrayInfosDynamique)
        {
            //On met le timestamp d'aujourd'hui comme date de MAJ dans les userdefaults
            NSInteger newUpdateInfo = [[NSDate date] timeIntervalSince1970];
            [[NSUserDefaults standardUserDefaults] setInteger:newUpdateInfo forKey:@"updateInfo"];
            [[NSUserDefaults standardUserDefaults] setObject:arrayInfosDynamique forKey:@"arrayInfo"];
            if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
            {
                NSArray * infos = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"arrayInfo"] objectAtIndex:0];

                (void)[(StyledPullableView*)[[self view] viewWithTag:13] updateTextScroll:infos];
            }
            
            
        }
    }
    else receivedData = nil;
}


#pragma mark -
#pragma mark Info Application

- (void)showInfo {
	InfoViewController *infoViewController = [[InfoViewController alloc] initWithNibName:@"InfoView"
																				  bundle:nil];
	[self.navigationController pushViewController:infoViewController 
										 animated:YES];
}

#pragma mark -
-(void)viewWillDisappear:(BOOL)animated
{
    
    [self hideLoadingProgress];
    [self cancelConnection];
    [super viewWillDisappear:animated];
}

- (void)cancelConnection {
	DLog(@"cancelConnection");
	if (self.welcomeConnection) {
		[self.welcomeConnection cancel];
		self.welcomeConnection = nil;
	}
}

- (void)viewDidLoad {
    
    [TestFlight passCheckpoint:@"Entrée dans l'application"];
    
    DLog(@"");
    [self hideLoadingProgress];
    
    
    NSString *filePath = [self dataFilePath:kFilenameData];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) 
    {        
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        
        int nombreElements = [array count];
        if (nombreElements == 2)
        {
            pseudoTextField.text = [array objectAtIndex:0];
            motDePasseTextField.text = [array objectAtIndex:1];
        }        
    } 
    else
    {
        pseudoTextField.text = @"";
        motDePasseTextField.text = @"";
    }
    
    self.pseudoUtilisateur = pseudoTextField.text;
    self.motDePasseUtilisateur = motDePasseTextField.text;
    
    
    
    // create a UIButton (UIButtonTypeInfoLight)
	UIButton *infoDarkButtonType = [[UIButton alloc] initWithFrame:CGRectMake(250.0, 8.0, 25.0, 25.0)];
	infoDarkButtonType = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[infoDarkButtonType setTitle:@"Detail Disclosure" 
						forState:UIControlStateNormal];
	infoDarkButtonType.backgroundColor = [UIColor clearColor];
	[infoDarkButtonType addTarget:self 
						   action:@selector(showInfo) 
				 forControlEvents:UIControlEventTouchUpInside];
	
	// On ajoute le segment a une vue
	UIView *infoButtonView = [[UIView alloc] init];
	infoButtonView.backgroundColor = [UIColor clearColor];
	infoButtonView.frame = infoDarkButtonType.frame;
	infoButtonView.backgroundColor = [UIColor clearColor];
	[infoButtonView addSubview:infoDarkButtonType];
	
	// On ajoute la vue précédemment crée à la barre de navigation
	UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:infoButtonView];
	self.navigationItem.leftBarButtonItem = buttonItem;
    
	/*On affiche PAS les boutons
	UIImage *bouton = [UIImage imageNamed:@"whiteButton.png"];
	UIImage *stretchableBouton = [bouton stretchableImageWithLeftCapWidth:12 topCapHeight:0];
	[self.loginVisiteur setBackgroundImage:stretchableBouton forState:UIControlStateNormal];
	[self.loginUtilisateur setBackgroundImage:stretchableBouton forState:UIControlStateNormal];
	[self.gestionCompte setBackgroundImage:stretchableBouton forState:UIControlStateNormal];*/
    
	//Login
	UIBarButtonItem *boutonLogin = [[UIBarButtonItem alloc] initWithTitle:@"Créer un Compte"
																	style:UIBarButtonItemStylePlain
																   target:self
																   action:@selector(lancerFormulaireCreationUtilisateur)];
	self.navigationItem.rightBarButtonItem = boutonLogin;
	
    
    //Rajout de la pullView
    NSArray * infos = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"arrayInfo"] objectAtIndex:0];
    
    if(infos)
    {
        if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            StyledPullableView * pullUpView = [[StyledPullableView alloc] initWithFrameAndTextArray:CGRectMake(0, 0, 320, 460):infos];

            pullUpView.tag = 13;
        
            [self.view addSubview:pullUpView];
        }
    }
	
}


#pragma mark -
#pragma mark Formulaires et Lancement

-(IBAction)lancerFormulaireCreationUtilisateur{
    DLog(@"");
    
	CreateUserViewController *createUserViewController = [[CreateUserViewController alloc] initWithNibName:@"CreateUserView"
																									bundle:nil];
	[self.navigationController pushViewController:createUserViewController 
										 animated:YES];
}

//Entrer sans se connecter
-(IBAction)lancerApplication{
    
	DLog("");
    
    [TestFlight passCheckpoint:@"Entrée sans connexion"];
    
    //-----------------------
    //appel vers root 
    RootViewController * rootViewController = [RootViewController alloc];
    
    rootViewController.password = @"";                                            
    rootViewController.username = @"";     
    rootViewController.ancienNiveau = @"";     
    rootViewController.nouveauNiveau = @"";     
    rootViewController.estVisiteur = YES;     
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        (void)[rootViewController initWithNibName:@"RootViewController-iPad" bundle:nil];
    else 
        (void)[rootViewController initWithNibName:@"RootViewController" bundle:nil];
    /*    
    [UIView animateWithDuration:0.75 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [self.navigationController pushViewController:rootViewController animated:NO];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
        
    }];*/
    [self.navigationController pushViewController:rootViewController animated:YES];
    //-----------------------
    
    
}

#pragma mark -
#pragma mark Gestion Compte


-(IBAction) compteUtilisateur:(id)sender

{
    [TestFlight passCheckpoint:@"Gestion de compte"];
    userAccount=1;
    
    //On vérifie que tous les champs du formulaire soient remplis
    self.pseudoUtilisateur = pseudoTextField.text;
    self.motDePasseUtilisateur = motDePasseTextField.text;
    
    
    UIAlertView *actionAlertView = [[UIAlertView alloc] initWithTitle:@"Veuillez saisir vos identifiants." message:nil delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Se connecter", nil];
    [actionAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    UITextField * user = [actionAlertView textFieldAtIndex:0];
    UITextField * pwd  = [actionAlertView textFieldAtIndex:1];
    
    pwd.text = motDePasseUtilisateur;
    user.text = pseudoUtilisateur;
    
    pseudoTextField.text = self.pseudoUtilisateur;
    motDePasseTextField.text = self.motDePasseUtilisateur;
    
    [actionAlertView show];    
    
}

-(void)gestionDeCompte
{
    DLog(@"");
    UserAccountViewController *userAccountViewController=[UserAccountViewController alloc];
    userAccountViewController.username = self.pseudoUtilisateur;
    userAccountViewController.password = self.motDePasseUtilisateur;
    userAccountViewController.tabBar.hidden = NO;    
    [self.navigationController pushViewController:userAccountViewController animated:YES];
    
}
#pragma mark -
#pragma mark Login

-(IBAction) login:(id)sender
{
    DLog("");
    
    
    userAccount=0;
    
    //On vérifie que tous les champs du formulaire soient remplis
    self.pseudoUtilisateur = pseudoTextField.text;
    self.motDePasseUtilisateur = motDePasseTextField.text;
    
    
    UIAlertView *actionAlertView = [[UIAlertView alloc] initWithTitle:@"Veuillez saisir vos identifiants." message:nil delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Connexion", nil];
    [actionAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    UITextField * user = [actionAlertView textFieldAtIndex:0];
    UITextField * pwd  = [actionAlertView textFieldAtIndex:1];
    
    pwd.text = motDePasseUtilisateur;
    user.text = pseudoUtilisateur;
    
    pseudoTextField.text = self.pseudoUtilisateur;
    motDePasseTextField.text = self.motDePasseUtilisateur;
    
    [actionAlertView show];
    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex == 1) 
    {
        UITextField * user = [alertView textFieldAtIndex:0];
        UITextField * pwd  = [alertView textFieldAtIndex:1];
        
        self.motDePasseTextField.text = pwd.text;
        self.pseudoTextField.text = user.text;
        
        self.pseudoUtilisateur = pseudoTextField.text;
        self.motDePasseUtilisateur = motDePasseTextField.text;
        
        
        if(([pseudoUtilisateur isEqualToString: @""] || [motDePasseUtilisateur isEqualToString: @""])) {
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Veuillez remplir toutes les données du formulaire"
                                                           delegate:nil 
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
        }
        
        //Si tout est OK
        if (![pseudoUtilisateur isEqualToString: @""] && ![motDePasseUtilisateur isEqualToString: @""])
        {
            
            //[self.pseudoTextField resignFirstResponder];
            //[self.motDePasseTextField resignFirstResponder];
            
            if (userAccount == 0)
            {
                //Vers RootViewController
                connectionStatus = 1;
            }
            else if (userAccount == 1)
                
            {
                //Vers GestionCompte
                connectionStatus = 4;
                
            }
            [self launchConnection];
            
        }
    }
    
    
}

-(IBAction)launchConnection
{
	DLog("");
    
    [TestFlight passCheckpoint:@"Connexion utilisateur"];
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:self.pseudoUtilisateur,@"username", self.motDePasseUtilisateur, @"password", nil];
    

    
    [self post:[NSString stringWithFormat:@"%@/webServices/compte/loginCompte.php",sc_server]
              param: query
              callback: ^(NSDictionary *wsReturn) {
        
        //Inscription des infos de l'user dans les préférences de l'app
        NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
        for (NSString * key in wsReturn)
        {
            [preferences setObject:[wsReturn objectForKey:key] forKey: key];
        }
        
        
        if([wsReturn count] == 0)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Identifiants incorrects"
                                                            message:@"Vérifiez à nouveau votre login / mot de passe."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Réessayer"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [self changeScreen:[[NSArray alloc] init] done:YES];
        }
    }
            errorMessage:@"Vous devez être connecté au Web pour vous connecter"];
    
}
-(IBAction)textFieldDone:(id)sender {
    DLog("");  
    [self login:sender];
}

- (IBAction)backgroundTap:(id)sender {
    DLog("");  
    //[pseudoTextField resignFirstResponder];
    //[motDePasseTextField resignFirstResponder];
}
#pragma mark -
#pragma mark Memory Management Methods

- (void)didReceiveMemoryWarning {
    DLog("");  
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)viewDidUnload {
    DLog("");  
    logoImageView = nil;
    [super viewDidUnload];
    self.gestionCompte=nil;
    self.loginVisiteur = nil;
    self.loginUtilisateur = nil;
    self.pseudoTextField = nil;
    self.motDePasseTextField = nil;
  
    self.welcomeConnection= nil;

    self.HUD = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
@end