//
//  ModerationMessagesViewController.h
//  SupCast
//
//  Created by fgutie10 on 02/08/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ModerationMessagesViewController : UIViewController <NSXMLParserDelegate> {

	NSString *username;
	NSString *password;
	NSString *categorie;
	NSString *idMessage;
	
	NSString *message;
    NSString *postPseudo;
    NSString *postCategorie;
    
	int connectionStatus;
    
	NSString *vote;

	
	NSMutableData *receivedData;
	
	NSMutableString *ligneMessage;
	NSMutableDictionary *dictionnaire;
	NSString *currentElementName;
	NSMutableString *currentText;
	
	NSSet *interestingTags;
    NSString *elggid;

	
}

@property(nonatomic,strong) NSString *elggid;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *categorie;
@property(nonatomic,strong) NSString *idMessage;

@property (nonatomic, weak) IBOutlet UIButton *boutonOUI;
@property (nonatomic, weak) IBOutlet UIButton *boutonNON;
@property (nonatomic, weak) IBOutlet UIButton *boutonSupprimer;

@property (nonatomic, weak) IBOutlet UILabel *pseudoNiveau;
@property (nonatomic, weak) IBOutlet UITextView *champMessage;
@property (nonatomic, weak) IBOutlet UILabel *messageSortie;
@property (nonatomic, weak) IBOutlet UILabel *messageSupprimeLigne1;
@property(nonatomic,strong) NSString *postPseudo;
@property(nonatomic,strong) NSString *postCategorie;
@property(nonatomic,strong) NSString *message;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property(nonatomic,strong) NSMutableData *receivedData;

-(IBAction)voterMessage:(id)sender;
-(IBAction)supprimerMessage;

@end
