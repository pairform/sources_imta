//
//  FileMediaLoadOperationProtocol.h
//  SupCast
//
//  Created by CAPE on 24/05/11.
//  Copyright (c) 2011. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol FileMediaLoadOperationProtocol <NSObject>

-(void)finFileMediaLoadOperation:(NSDictionary *)dictResult;

@end
