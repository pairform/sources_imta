//
//  QPinyinVideoViewController.h
//  SupCast
//
//  Created by Phetsana on 23/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractPinyinViewController.h"


@interface QPinyinVideoViewController : AbstractPinyinViewController {
	IBOutlet UIImageView *fondEcranImageView;
	IBOutlet UILabel *prefix;
	IBOutlet UILabel *suffix;
	NSDictionary *dicoRacine;
}

@property (nonatomic, retain) NSDictionary *dicoRacine;

//-(NSString *)dataFilePath:(NSString *)fileName;


@end
