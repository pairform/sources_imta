//
//  UserInfosViewController.m
//  SupCast
//
//  Created by wbao11 on 19/07/11.
//  Copyright (c) 2011 EMN - CAPE. All rights reserved.
//

#import "UserInfosViewController.h"

@implementation UserInfosViewController
@synthesize tableView;
@synthesize motdepasseTwitter;
@synthesize userTwitter;
@synthesize listeValeur;


-(IBAction)backgroundTap:(id)sender
{
    DLog(@"");
    
    [myTextField0 resignFirstResponder];
    [myTextField1 resignFirstResponder];
}

-(void)ecriture
{
    DLog(@"");
    BOOL ressourceinitiale = YES;

    NSString *thePathFilenameRegle = [self dataFilePath:kFilenameRegle];
    if ([[NSFileManager defaultManager] fileExistsAtPath:thePathFilenameRegle])
    {
        NSArray *myArray = [[NSArray alloc] initWithContentsOfFile:thePathFilenameRegle];
        ressourceinitiale =[[[myArray objectAtIndex:1] objectForKey:kRessourceInitiale] boolValue];
    }
    else
    {
        ressourceinitiale = YES;
    }

    NSArray * array = [[NSArray alloc] initWithObjects:
                       [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:statusPublicationGPS],kGPS,
                        nil],
                       [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:statusPublicationTwitter],kTWITTER,
                        self.userTwitter,kUSER,
                        self.motdepasseTwitter,kMDP,
                        [NSNumber numberWithBool:ressourceinitiale],kRessourceInitiale,
                        nil],
                       nil];
    [array writeToFile:thePathFilenameRegle atomically:YES];
}

-(IBAction)textFieldDone:(id)sender
{
    DLog(@"");
    [self ecriture];    
}

#pragma -mark
#pragma mark TabBar Delegate Methods

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    DLog(@"");
}
-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    DLog(@"");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    DLog(@"");
    [super viewDidLoad];
    
    self.listeValeur = [[NSArray alloc] initWithObjects:
                        [[NSDictionary alloc] initWithObjectsAndKeys:
                        kTexteLocalisationActivee,kTexteChecked,
                        kTexteLocalisationDesactivee,kTexteUnChecked,
                        kImageLocalisationActivee,kImageChecked,
                        kImageLocalisationDesactivee,kImageUnChecked,nil],
                        [[NSDictionary alloc] initWithObjectsAndKeys:
                        kTexteTwitterActivee,kTexteChecked,
                        kTexteTwitterDesactivee,kTexteUnChecked,
                        kImageTwitterActivee,kImageChecked,
                        kImageTwitterDesactivee,kImageUnChecked,nil],nil];
    
    // Do any additional setup after loading the view from its nib.
    tableView.backgroundColor = [UIColor clearColor];
    tableView.backgroundView = nil;
    //tableView.rowHeight = kCustomRowHeight;

    statusPublicationGPS = NO;
    statusPublicationTwitter = NO;
    
    self.motdepasseTwitter  = [[NSMutableString alloc] initWithString:@""];
    self.userTwitter = [[NSMutableString alloc] initWithString:@""];
    
    NSString *thePathFilenameRegle = [self dataFilePath:kFilenameRegle];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:thePathFilenameRegle])
    {
        statusPublicationGPS = NO;
        statusPublicationTwitter = NO;
        [self.userTwitter setString:@""];
        [self.motdepasseTwitter setString:@""];
        
        [self ecriture];    
        
    }
    
    NSArray *myArray = [[NSArray alloc] initWithContentsOfFile:thePathFilenameRegle];
    statusPublicationGPS =[[[myArray objectAtIndex:0] objectForKey:kGPS] boolValue];
    statusPublicationTwitter =[[[myArray objectAtIndex:1] objectForKey:kTWITTER] boolValue];
    if([[myArray objectAtIndex:1] objectForKey:kUSER] == nil)
        [self.userTwitter setString:@""];
    else
        [self.userTwitter setString:[[myArray objectAtIndex:1] objectForKey:kUSER]];

    if([[myArray objectAtIndex:1] objectForKey:kMDP] == nil)
        [self.motdepasseTwitter setString:@""];
    else
        [self.motdepasseTwitter setString:[[myArray objectAtIndex:1] objectForKey:kMDP]];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    DLog(@"");
    
    

    
    
}    
- (void)viewWillDisappear:(BOOL)animated
{
    DLog(@"");
    
    [self ecriture];
}
- (void)viewDidUnload
{
    DLog(@"");
    imageView = nil;
    tableView = nil;
    customCell = nil;
    // [monSwitch release];
    //  monSwitch = nil;
    myTextField0 = nil;
    myTextField1 = nil;
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");

    return YES;
}

#pragma mark -
#pragma mark UITextFiedDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    DLog(@"");
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {  
    DLog(@"");
    
	[textField becomeFirstResponder];
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    DLog(@"");
    return NO;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    DLog(@"");
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{  
    DLog(@"");
    [self.userTwitter setString:myTextField0.text];
    [self.motdepasseTwitter setString:myTextField1.text];
    
	[textField resignFirstResponder];
    [self ecriture];
}


#pragma mark -
#pragma mark UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    DLog(@"");
    return 2;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    DLog(@"");
    
    NSInteger number = 0;
    
    if (section == kGPSSection)
    {
        number = 1;
    }
    
    if (section == kTwitterSection)
    {
        number = 3;
        
        // if (statusPublicationTwitter) 
        // {    
        // }
        // else
        // {
        //     number = 1;
        // }
    }
    
    return number;
    
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
    DLog(@"");
    
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tableView];
	NSIndexPath *indexPath = [tableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
		//[self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
		[self tableView:tableView didSelectRowAtIndexPath:indexPath];
	}
    
}

-(void)tableView:(UITableView *)aTableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");
    
    if (indexPath.section == kGPSSection)        
    { 
        if (indexPath.row == 0)
        { 
            // inversion du status
            statusPublicationGPS = (statusPublicationGPS) ? NO : YES;
            // mise en forme de la cellule
            [self cellMiseEnForme:aTableView withIndexPath:indexPath withStatus:statusPublicationGPS withIndex:indexPath.section];
        }
    }
    
    if (indexPath.section == kTwitterSection)
    { 
        if (indexPath.row == 0)
        { 
            // inversion du status
            statusPublicationTwitter = (statusPublicationTwitter) ? NO : YES;
            // mise en forme de la cellule
            [self cellMiseEnForme:aTableView withIndexPath:indexPath withStatus:statusPublicationTwitter withIndex:indexPath.section];
        }
    }
    
    
    [self ecriture];        
    
    [aTableView reloadData];
    
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    DLog(@"");
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {} else {tableView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, 740,tableView.frame.size.height);}

    
    static NSString *kGPSCellIdentifier           = @"GPSSWITCH";
	static NSString *kTWITTERSWITCHCellIdentifier = @"TWITTERSWITCH";
	static NSString *kTWITTERUSERCellIdentifier   = @"TWITTERUSER";
	static NSString *kTWITTERMDPCellIdentifier    = @"TWITTERMDP";
    
	UITableViewCell * cell = nil;
    
    
	switch (indexPath.section)
	{
		case kGPSSection:
            //monSwitch = nil;
            customCell = nil;
            
			customCell = [tableView dequeueReusableCellWithIdentifier:kGPSCellIdentifier];
			if (customCell == nil)
			{                
                [self miseEnFormeWithStatus:statusPublicationGPS withIndex:indexPath.section withIdentifier:kGPSCellIdentifier];
			}
            
            cell = customCell;
			
			break;
            
        case kTwitterSection:
            
            switch (indexPath.row) {
                case kTwitterSwitch:
                    //monSwitch = nil;
                    customCell = nil;
                    
                    customCell = [tableView dequeueReusableCellWithIdentifier:kTWITTERSWITCHCellIdentifier];
                    if (customCell == nil)
                    {
                        [self miseEnFormeWithStatus:statusPublicationTwitter withIndex:indexPath.section withIdentifier:kTWITTERSWITCHCellIdentifier];                        
                    }
                    
                    cell = customCell;
                    
                    break;
                case kTwitterMdp:
                    //  monSwitch = nil;
                    customCell = nil;
                    
                    customCell = [tableView dequeueReusableCellWithIdentifier:kTWITTERMDPCellIdentifier];
                    if (customCell == nil)
                    {
                        customCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kTWITTERMDPCellIdentifier];
                        customCell.textLabel.text = @"Mot de Passe : ";
                        customCell.textLabel.font = [UIFont systemFontOfSize:12];
                        
                        myTextField1 = [[UITextField alloc] initWithFrame:CGRectMake(100, 8, 190, 27)];
                        [myTextField1 setDelegate:self];
                        [myTextField1 setSecureTextEntry:YES];
                        [myTextField1 setAutocorrectionType:UITextAutocorrectionTypeNo];
                        [myTextField1 setKeyboardType:UIKeyboardTypeDefault];
                        [myTextField1 setAutocapitalizationType:UITextAutocapitalizationTypeNone];
                        [myTextField1 setBorderStyle:UITextBorderStyleBezel];
                        [myTextField1 setReturnKeyType:UIReturnKeyDone];
                        [myTextField1 setText:self.motdepasseTwitter];                      
                        [myTextField1 setPlaceholder:@"Mot de passe"];                        
                        [myTextField1 setTag:indexPath.row];
                        [myTextField1 setBackgroundColor:[UIColor clearColor]];
                        [myTextField1 addTarget:self 
                                         action:@selector(textFieldDone:) 
                               forControlEvents:UIControlEventEditingDidEnd];
                        
                        [customCell.contentView addSubview:myTextField1];
                    }
                    
                    cell = customCell;
                    
                    break;
                case kTwitterUser:
                    //  monSwitch = nil;
                    customCell = nil;
                    
                    customCell = [tableView dequeueReusableCellWithIdentifier:kTWITTERUSERCellIdentifier];
                    if (customCell == nil)
                    {
                        customCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kTWITTERUSERCellIdentifier];
                        customCell.textLabel.text = @"Compte Twitter : ";
                        customCell.textLabel.font = [UIFont systemFontOfSize:12];
                        
                        myTextField0 = [[UITextField alloc] initWithFrame:CGRectMake(100, 8, 190, 27)];
                        [myTextField0 setDelegate:self];
                        [myTextField0 setSecureTextEntry:NO];
                        [myTextField0 setAutocorrectionType:UITextAutocorrectionTypeNo];
                        [myTextField0 setKeyboardType:UIKeyboardTypeDefault];
                        [myTextField0 setAutocapitalizationType:UITextAutocapitalizationTypeNone];
                        [myTextField0 setBorderStyle:UITextBorderStyleBezel];
                        [myTextField0 setReturnKeyType:UIReturnKeyDone];
                        [myTextField0 setText:self.userTwitter];                      
                        [myTextField0 setPlaceholder:@"Compte Twitter"];                        
                        [myTextField0 setTag:indexPath.row];
                        [myTextField0 setBackgroundColor:[UIColor clearColor]];
                        [myTextField0 addTarget:self 
                                         action:@selector(textFieldDone:) 
                               forControlEvents:UIControlEventEditingDidEnd];
                        
                        [customCell.contentView addSubview:myTextField0];
                    }
                    
                    cell = customCell;
                    
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        default:
            break;
			
	}
    
	// Set attributes common to all cell types.
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
	return cell;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DLog(@"");
    
    if (indexPath.section == kGPSSection)        
        if (indexPath.row == 0)
        { 
            // inversion du status
            statusPublicationGPS = (statusPublicationGPS) ? NO : YES;
            // mise en forme de la cellule
            [self cellMiseEnForme:aTableView withIndexPath:indexPath withStatus:statusPublicationGPS withIndex:indexPath.section];
        }
    if (indexPath.section == kTwitterSection)
        if (indexPath.row == 0)
        { 
            // inversion du status
            statusPublicationTwitter = (statusPublicationTwitter) ? NO : YES;
            // mise en forme de la cellule
            [self cellMiseEnForme:aTableView withIndexPath:indexPath withStatus:statusPublicationTwitter withIndex:indexPath.section];
        }
    
    
    [self ecriture];    
    [aTableView reloadData];
    [aTableView deselectRowAtIndexPath:indexPath animated:NO];
    
}


-(void)cellMiseEnForme:(UITableView *)aTableView withIndexPath:(NSIndexPath *)indexPath withStatus:(BOOL)status withIndex:(NSUInteger)index
{
    DLog(@"");
    
    customCell = [aTableView cellForRowAtIndexPath:indexPath];
    if (customCell == nil)
    {
        
    }
    else
    {
        customCell.textLabel.font = (status) ?[UIFont systemFontOfSize:14]:[UIFont italicSystemFontOfSize:14];
        customCell.textLabel.text = (status) ? [[self.listeValeur objectAtIndex:index] objectForKey:kTexteChecked] : [[self.listeValeur objectAtIndex:index] objectForKey:kTexteUnChecked];
        UIButton *button = (UIButton *)customCell.accessoryView;    
        UIImage *newImage = (status) ? [UIImage imageNamed:[[self.listeValeur objectAtIndex:index] objectForKey:kImageChecked]] : [UIImage imageNamed:[[self.listeValeur objectAtIndex:index] objectForKey:kImageUnChecked]];
        
        CGRect frame = customCell.frame;
        frame.size.height += 71;
        customCell.frame = frame;
        
        [button setBackgroundImage:newImage forState:UIControlStateNormal];
    }
}

-(void)miseEnFormeWithStatus:(BOOL)status withIndex:(NSUInteger)index withIdentifier:(NSString *)identifier
{
    DLog(@"");
    customCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    customCell.textLabel.text = (status) ? [[self.listeValeur objectAtIndex:index] objectForKey:kTexteChecked] : [[self.listeValeur objectAtIndex:index] objectForKey:kTexteUnChecked];
    customCell.textLabel.font = (status) ? [UIFont systemFontOfSize:14]:[UIFont italicSystemFontOfSize:14];                
    UIImage *image = (status) ? [UIImage imageNamed:[[self.listeValeur objectAtIndex:index] objectForKey:kImageChecked]] : [UIImage imageNamed:[[self.listeValeur objectAtIndex:index] objectForKey:kImageUnChecked]];                
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect frame = CGRectMake(0.0, 0.0, 40, 40);
    button.frame = frame;	// match the button's size with the image size   
    
    [button setBackgroundImage:image forState:UIControlStateNormal];                
    [button addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    customCell.accessoryView = button;                
}



@end
