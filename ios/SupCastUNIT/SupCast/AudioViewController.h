//
//  AudioViewController.h
//  SupCast
//
//  Created by Phetsana on 16/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AudioViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	NSArray *itemsList;
	
	/*  
		La liste de sons se compose de plusieurs sections mais on ne 
		maintient qu'un unique tableau pour le système d'écriture.
		Ce Tableau permet d'avoir les indices des différentes sections
	 */
	NSMutableArray *positionsList;

	IBOutlet UIImageView *fondEcranImageView;
	
	IBOutlet UITableView *tableView; 
	IBOutlet UISegmentedControl *segmentedControl;

	// Tableaux contenant les textes dans les 3 systèmes d'écritures
	NSMutableArray *pinyinElements;
	NSMutableArray *ideogramElements;
	NSMutableArray *alphabetElements;
	
	// Tableaux contenant la liste des sons
	NSMutableArray *audioNameVersion1;
	NSMutableArray *audioNameVersion2;
	NSMutableArray *audioVersion1Extension;
	NSMutableArray *audioVersion2Extension;
	
	IBOutlet UILabel *label;
	NSString *textLabel;
	
	NSString *dirRes;
	
	NSDictionary *dicoRacine;
}

@property (nonatomic, retain) NSDictionary *dicoRacine;

@property (nonatomic, retain) NSString *dirRes;
@property (nonatomic, retain) NSString *textLabel;
@property (nonatomic, retain) NSArray *itemsList;

@property (nonatomic, retain) IBOutlet UILabel *label;

@end
