//
//  MCQImageVideoViewController.h
//  SupCast
//
//  Created by Phetsana on 17/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractMCQExerciceViewController.h"


@interface MCQImageVideoViewController : AbstractMCQExerciceViewController {
	IBOutlet UIImageView *imageView;
	IBOutlet UITableView *indicationTableView;
	CGFloat height;
}

//-(NSString *)dataFilePath:(NSString *)fileName;
@end
