//
//  MapViewController.m
//  SupCast
//
//  Created by CAPE on 11/07/11.
//  Copyright (c) 2011 ecole des mines de nantes. All rights reserved.
//

#import "MapViewController.h"
#import "Annotation.h"

@implementation MapViewController
@synthesize MapView;
@synthesize latitude;
@synthesize longitude;
@synthesize titre;
@synthesize soustitre;

-(IBAction)changeMap:(id)sender
{
    
    switch ([(UISegmentedControl *) sender selectedSegmentIndex]) {
        case 0:
            [MapView setMapType:MKMapTypeStandard];
            break;
        case 1:
            [MapView setMapType:MKMapTypeSatellite];
            break;
        case 2:
            [MapView setMapType:MKMapTypeHybrid];
            break;
        default:
            break;
    }
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [MapView setMapType:MKMapTypeHybrid];
    MKCoordinateRegion region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(latitude,longitude), MKCoordinateSpanMake(0.04, 0.04)); 
    [MapView setRegion:region animated:YES];
    [MapView setMapType:MKMapTypeStandard];
    
    Annotation *annotation = [[Annotation alloc] init];
    annotation.titre = titre;                           
    annotation.soustitre = soustitre;                           
    annotation.latitude = latitude;                           
    annotation.longitude = longitude;                           
    [MapView addAnnotation:annotation];
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    typeSelector = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
