//
//  WebViewContentViewController.m
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "WebViewContentViewController.h"
#import "GestionFichier.h"

#define kFontSize 20
#define FONT_SIZE_INDICATION 12


@implementation WebViewContentViewController

// The designated initializer. Override to perform setup that is required before the view is loaded.


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	DLog(@"xxxxxxxxxxxxxxxxxxx");
	
	
	labelTitre.text = titre;
	labelTitre.textColor = [UIColor blackColor];

    [super viewDidLoad];	
	
}

- (void)viewWillDisappear:(BOOL)animated {
	DLog(@"xxxxxxxxxxxxxxxxxxx");
	
	[super viewWillDisappear:animated];
}


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	DLog(@"xxxxxxxxxxxxxxxxxxx");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	DLog(@"xxxxxxxxxxxxxxxxxxx");
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if ((UIInterfaceOrientationLandscapeLeft == orientation) ||
        (UIInterfaceOrientationLandscapeRight == orientation))
    {
		[webView setFrame:CGRectMake(0, 0, 480, 320)];
		
    } 
    else 
    {
		
	}
}

- (void)dealloc {
	DLog(@"xxxxxxxxxxxxxxxxxxx");
    [webView release];
	[labelTitre release];
	[super dealloc];
}

@end
