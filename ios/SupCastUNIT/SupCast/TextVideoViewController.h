//
//  TextVideoViewController.h
//  SupCast
//
//  Created by Phetsana on 15/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface TextVideoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UIImageView *fondEcranImageView;
	NSString *text;
	NSString *videoName;
	NSString *videoExtension;
	Movie *movie;
	IBOutlet UITableView *tableView;
	
	NSString *textLabel;
	IBOutlet UILabel *label;
	NSString *dirRes;
	NSDictionary *dicoRacine;
}

@property (nonatomic, retain) NSDictionary *dicoRacine;

@property (nonatomic, retain) NSString *dirRes;

@property (nonatomic, retain) NSString *textLabel;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *videoName;
@property (nonatomic, retain) NSString *videoExtension;

@property (nonatomic, retain) IBOutlet UILabel *label;

- (IBAction)playMovie:(id)sender;

@end
