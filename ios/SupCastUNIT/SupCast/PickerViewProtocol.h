//
//  PickerViewProtocol.h
//  SupCast
//
//  Created by admin on 31/05/11.
//  Copyright 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PickerViewProtocol <NSObject>

- (void)dismissPickerView;
- (void)associate:(NSMutableArray *)resultList;
- (void)removeUserAnswer;

@end
