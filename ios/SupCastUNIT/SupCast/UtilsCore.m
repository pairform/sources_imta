//
//  Utils.m
//  testcoredate
//
//  Created by admin on 22/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "UtilsCore.h"
#import "SupCastAppDelegate.h"
#import "Image.h"
#import "Message.h"



@implementation UtilsCore

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext = _managedObjectContext;

static NSManagedObjectContext * volatile context = nil;

+ (void) initContext{
    DLog(@"");
    if ( context == nil ){
        context = [(SupCastAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
}

+ (void) sauvegardeImage:(NSNumber *)id_utilisateur:(NSString *)url{
    DLog(@"");
    [self initContext];
    UIImage * image = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:url]]];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Image" inManagedObjectContext:context];
    Image *imageEntity = [[Image alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    // Compression dans un nsdata
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:image];
    imageEntity.image = data;
    imageEntity.id_utilisateur = id_utilisateur;
    
    NSError *error;
	[context save:&error];
    NSLog(@"%@",error);
    
}

+ (UIImage *) getImage:(NSNumber *)id_utilisateur{
    DLog(@"");
    [self initContext];
    NSError *error;
    NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"Image" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:imageEntity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_utilisateur == %@)", id_utilisateur];
    [fetch setPredicate:predicate];
    
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    Image *image = [array objectAtIndex:0];
    UIImage *uiImage = [NSKeyedUnarchiver unarchiveObjectWithData:image.image];
    return uiImage;
}

+ (void) rafraichirMessagesWithParams:(NSDictionary *)params{
    DLog(@"");
    //on peut faire un cast pour éviter un warning du style 'managedObjectContext not find in protocol'
	[self initContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    //Pas de message d'erreur si pas de connexion?
    
    [self post:[NSString stringWithFormat:@"%@/webServices/messages/recupererMessages.php",sc_server] param:params
              callback:^(NSDictionary *wsReturn) {
                  NSDictionary * messages = [wsReturn objectForKey:@"messages"];
                  for (NSDictionary *item in messages)
                  {
                      NSLog(@"%@",[item objectForKey:@"id_message"]);

                      Message *message = [UtilsCore getMessage:[f numberFromString:[item objectForKey:@"id_message"]]];
                      // si le message n'existe pas , ou qu'il n'est pas a jour , on l'enregistre.
                      if ( (message == nil) || (message.time_updated.intValue < [f numberFromString:[item objectForKey:@"time_updated"]].intValue) ){
                          if ( message == nil){
                              message = [[Message alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
                          }

                          message.id_message = [f numberFromString:[item objectForKey:@"id_message"]];
                          message.value= [item objectForKey:@"value"];
                          message.owner_guid = [f numberFromString:[item objectForKey:@"owner_guid"]];
                          message.owner_username = [item objectForKey:@"owner_username"];
                          message.utilite= [f numberFromString:[item objectForKey:@"utilite"]];
                          message.supprime_par = [f numberFromString:[item objectForKey:@"supprime_par"]];
                          
                          if ( [item objectForKey:@"reponses"] != [NSNull null] ){
                              NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:[item objectForKey:@"reponses"]];
                              message.reponses = arrayData;
                          }else{
                              message.reponses = nil;
                          }
                          
                          message.value= [item objectForKey:@"value"];
                          message.id_ressource= [f numberFromString:[item objectForKey:@"id_ressource"]];
                          message.nom_page= [item objectForKey:@"nom_page"];
                          message.nom_tag= [item objectForKey:@"nom_tag"];
                          message.num_occurence= [f numberFromString:[item objectForKey:@"num_occurence"]];
                          message.time_created= [f numberFromString:[item objectForKey:@"time_created"]];
                          message.time_updated= [f numberFromString:[item objectForKey:@"time_updated"]];
                      
                          if ( [item objectForKey:@"tags"] != [NSNull null] ){
                              NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:[item objectForKey:@"tags"]];
                              message.tags = arrayData;
                          }else{
                              message.tags = nil;
                          }
                          
                          if([item objectForKey: @"estDefi"]== [NSNumber numberWithBool:YES]){
                              message.estDefi = [NSNumber numberWithBool:YES];
                          }else{
                              message.estDefi = [NSNumber numberWithBool:NO];
                          }
                          
                          if([item objectForKey: @"estReponse"] == [NSNumber numberWithBool:YES]){
                              message.estReponse = [NSNumber numberWithBool:YES];
                          }else{
                              message.estReponse = [NSNumber numberWithBool:NO];
                          }
                          
                          if ( [item objectForKey:@"medaille"] == [NSNull null]){
                              message.medaille= @"";
                          }else{
                              message.medaille= [item objectForKey:@"medaille"];
                          }
                          
                          
                          if ( [item objectForKey:@"owner_rank_name"] == [NSNull null]){
                              message.owner_rank_name= @"";
                          }else{
                              message.owner_rank_name= [item objectForKey:@"owner_rank_name"];
                          }
                          
                          if ( [item objectForKey:@"owner_rank_id"] == [NSNull null]){
                              message.owner_rank_id = @"";
                          }else{
                              message.owner_rank_id= [item objectForKey:@"owner_rank_id"];
                          }
                          message.user_a_vote= [f numberFromString:[item objectForKey:@"user_a_vote"]];
                          
                          [UtilsCore sauvegardeImage:message.owner_guid :[item objectForKey:@"owner_avatar_medium"]];
                          
                          NSLog(@"message fin");
                      }
                      
                  }
                  
                  NSError *error;
                  [context save:&error];
                  NSLog(@"%@",error);
                  //Post d'une notification de fin d'actualisation
                  [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesActualises" object: nil];
                  

              }];
}

+ (void) rafraichirMessages{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                              nil];
    [self rafraichirMessagesWithParams:params];
}

+ (void)rafraichirMessagesFromRessource:(NSNumber *)id_ressource{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_ressource, @"id_message", nil];
    [self rafraichirMessagesWithParams:params];
}
     

+ (void) rafraichirMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_ressource, @"id_message",
                             nom_page,@"nom_page",nil];
    [self rafraichirMessagesWithParams:params];
    
}
     
+ (void) rafraichirMessagesFromUser:(NSNumber *)owner_guid{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             owner_guid, @"owner_guid", nil];
    [self rafraichirMessagesWithParams:params];
    
}

+ (Message *) getMessage:(NSNumber *)id_message{
    DLog(@"");
    [self initContext];
    NSError *error;
	NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_message == %@)", id_message];
    //[NSPredicate predicateWithFormat: @"(lastName LIKE[c] 'Worsley') AND (salary > %@)", minimumSalary];
    [fetch setPredicate:predicate];
    
    
    
    
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    [UtilsCore afficherMessage:array];
    Message *message = [array objectAtIndex:0];
    return message;
	
    
    
}

+ (NSArray * ) getReponsesFromMessage:(NSNumber *)id_message{
    DLog(@"");
    Message * message = [UtilsCore getMessage:id_message];
    if ( message.reponses == nil ) return nil;
    
    NSMutableArray *id_reponses = [NSKeyedUnarchiver unarchiveObjectWithData:message.reponses];
    NSMutableArray * array = [[NSMutableArray alloc] initWithCapacity:id_reponses.count];

    
    for (NSNumber * id_reponse in id_reponses) {
        [array addObject:[UtilsCore getMessage:id_reponse]];
    }
    [UtilsCore afficherMessage:array];
    return array;
}

+ (NSArray * ) getMessagesFromUser:(NSNumber *)owner_guid{
    DLog(@"");
    [self initContext];
    NSError *error;
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(owner_guid == %@)", owner_guid];
    //[NSPredicate predicateWithFormat: @"(lastName LIKE[c] 'Worsley') AND (salary > %@)", minimumSalary];
    [fetch setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"time_created"
                                        ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    
    
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    [UtilsCore afficherMessage:array];
    return array;
}

+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource{
    DLog(@"");
    [self initContext];
    NSError *error;
	
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@)",id_ressource];
    //[NSPredicate predicateWithFormat: @"(lastName LIKE[c] 'Worsley') AND (salary > %@)", minimumSalary];
    [fetch setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"time_created"
                                        ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    

	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    [UtilsCore afficherMessage:array];
    return array;
}


+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page{
    DLog(@"");
    [self initContext];
    NSError *error;
    
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND (nom_page == %@)",id_ressource, nom_page];
    //[NSPredicate predicateWithFormat: @"(lastName LIKE[c] 'Worsley') AND (salary > %@)", minimumSalary];
    [fetch setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"time_created"
                                        ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    [UtilsCore afficherMessage:array];
    return array;
}


+ (NSArray * ) getAllMessages{
    DLog(@"");
    [self initContext];
    NSLog(@"getAllMessages");
    NSError *error;
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"time_created"
                                        ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil ){
        NSLog(@"error");
        return nil;
    }
    [UtilsCore afficherMessage:array];
    
    return array;
}

+ (void) afficherMessage:(NSArray *)array{
    DLog(@"");
//    for (Message * message in array) {
//        NSLog(@"--------------");
//        NSLog(@"%@ ",message.id_message);
//        NSLog(@"%@ ",message.value);
//        NSLog(@"%@ ",message.owner_guid);
//        NSLog(@"%@ ",message.owner_username);
//        NSLog(@"%@ ",message.id_ressource);
//        NSLog(@"%@ ",message.nom_page);
//        NSLog(@"%@ ",message.nom_tag);
//        NSLog(@"%@ ",message.num_occurence);
//        NSLog(@"%@ ",message.reponses);
//        NSLog(@"%@ ",message.utilite);
//        NSLog(@"%@ ",message.date);
//        NSLog(@"--------------");
//    }
}



+ (NSFetchRequest *) getMessageEntityFetch {
    DLog(@"");
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:entity];
    return fetch;
}

@end
