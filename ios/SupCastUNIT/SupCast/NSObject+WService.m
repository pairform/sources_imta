//
//  NSDictionary+WService.m
//  SupCast
//
//  Created by Maen Juganaikloo on 23/04/13.
//  Copyright (c) 2013 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "NSObject+WService.h"
#import "Reachability.h"


// helper function: get the string form of any object
static NSString *toString(id object) {
    return [NSString stringWithFormat: @"%@", object];
}

// helper function: get the url encoded string form of any object
static NSString *urlEncode(id object) {
    NSString *string = toString(object);
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}



@implementation NSObject (WService)




-(NSString*) urlEncodedString:(NSDictionary*) dico {
    NSMutableArray *parts = [NSMutableArray array];
    
    if([dico count] == 0)
        return @"";
    
    for (id key in dico)
    {
        id value = [dico objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@=%@", urlEncode(key), urlEncode(value)];
        [parts addObject: part];
    }
    return [parts componentsJoinedByString: @"&"];
}

-(BOOL) post:(NSString *)url callback: (void (^)(NSDictionary * wsReturn)) callback
{
    DLog(@"");
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        return false;
    }

    NSString *bodyData = [NSString stringWithFormat:@"os=%@&version=%@", sc_os, sc_version];
    
    NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Set the request's content type to application/x-www-form-urlencoded
    [postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // Designate the request a POST request and specify its body data
    [postRequest setHTTPMethod:@"POST"];
    [postRequest setHTTPBody:[NSData dataWithBytes:[bodyData UTF8String] length:[bodyData length]]];
    
    if([NSURLConnection respondsToSelector:@selector(sendAsynchronousRequest:queue:completionHandler:)])
    {
        // we can use the iOS 5 path, so issue the asynchronous request
        // and then just do whatever we want to do
        
		//to show:
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [NSURLConnection sendAsynchronousRequest:postRequest
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             //to hide
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             
             NSError *jsonParsingError = nil;
             
             NSDictionary *wsReturn = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
             
             //DLog(@"Erreur JSON : %@", jsonParsingError);
             
             callback(wsReturn);
             
         }];
    }
    return true;

}

-(BOOL) post:(NSString *)url callback: (void (^)(NSDictionary * wsReturn)) callback errorMessage:(NSString*) errorMessage
{
    DLog(@"");
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion."
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        return false;
    }
    
    NSString *bodyData = [NSString stringWithFormat:@"os=%@&version=%@", sc_os, sc_version];
    
    NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Set the request's content type to application/x-www-form-urlencoded
    [postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // Designate the request a POST request and specify its body data
    [postRequest setHTTPMethod:@"POST"];
    [postRequest setHTTPBody:[NSData dataWithBytes:[bodyData UTF8String] length:[bodyData length]]];
    
    if([NSURLConnection respondsToSelector:@selector(sendAsynchronousRequest:queue:completionHandler:)])
    {
        // we can use the iOS 5 path, so issue the asynchronous request
        // and then just do whatever we want to do
        
		//to show:
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [NSURLConnection sendAsynchronousRequest:postRequest
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             //to hide
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             
             NSError *jsonParsingError = nil;
             
             NSDictionary *wsReturn = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
             
             //DLog(@"Erreur JSON : %@", jsonParsingError);
             
             callback(wsReturn);
             
         }];
    }
    return true;
}

-(BOOL) post:(NSString *)url param: (NSDictionary*)param callback: (void (^)(NSDictionary * wsReturn)) callback errorMessage:(NSString*) errorMessage
{
    DLog("");
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion."
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    
        return false;
    }
    
    NSString * queryString = [self urlEncodedString:param];
    
    NSString *bodyData = [NSString stringWithFormat:@"%@&os=%@&version=%@", queryString, sc_os, sc_version];
    
    NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Set the request's content type to application/x-www-form-urlencoded
    [postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // Designate the request a POST request and specify its body data
    [postRequest setHTTPMethod:@"POST"];
    [postRequest setHTTPBody:[NSData dataWithBytes:[bodyData UTF8String] length:[bodyData length]]];
    
    if([NSURLConnection respondsToSelector:@selector(sendAsynchronousRequest:queue:completionHandler:)])
    {
        // we can use the iOS 5 path, so issue the asynchronous request
        // and then just do whatever we want to do
        
		//to show:
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [NSURLConnection sendAsynchronousRequest:postRequest
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             //to hide
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             
             NSError *jsonParsingError = nil;
             
             NSDictionary *wsReturn = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
             
             //DLog(@"Erreur JSON : %@", jsonParsingError);
             
             callback(wsReturn);
             
         }];
    }
    return true;

    
}

-(BOOL) post:(NSString *)url param: (NSDictionary*)param callback: (void (^)(NSDictionary * wsReturn)) callback
{
	DLog("");
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        return false;
    }
    
    NSString * queryString = [self urlEncodedString:param];
    
    NSString *bodyData = [NSString stringWithFormat:@"%@&os=%@&version=%@", queryString, sc_os, sc_version];
    
    NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Set the request's content type to application/x-www-form-urlencoded
    [postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // Designate the request a POST request and specify its body data
    [postRequest setHTTPMethod:@"POST"];
    [postRequest setHTTPBody:[NSData dataWithBytes:[bodyData UTF8String] length:[bodyData length]]];
    
    if([NSURLConnection respondsToSelector:@selector(sendAsynchronousRequest:queue:completionHandler:)])
    {
        // we can use the iOS 5 path, so issue the asynchronous request
        // and then just do whatever we want to do
        
		//to show:
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [NSURLConnection sendAsynchronousRequest:postRequest
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             //to hide
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             
             NSError *jsonParsingError = nil;

             NSDictionary *wsReturn = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
             
             //DLog(@"Erreur JSON : %@", jsonParsingError);
             
             callback(wsReturn);
                          
         }];
    }
    return true;
}

@end