//
//  IntroductionViewController.m
//  SupCast
//
//  Created by Phetsana on 11/09/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "GestionFichier.h"
#import "IntroductionViewController.h"

#define kSectionHeaderHeight 40

@implementation IntroductionViewController
@synthesize dirRes,dicoRacine,idecran,image,array;

- (void)initImage 
{
	imageView.image = image;
}




// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[fondEcranImageView setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																													   fileName:[self.dicoRacine objectForKey:kImageFondEcran]]]];
	
	
	tableView.backgroundColor = [UIColor clearColor];
	tableView.allowsSelection = NO;
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [array count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[[array objectAtIndex:section] objectForKey:kItems] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return [[array objectAtIndex:section] objectForKey:kSectionName];
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ModuleCell";
	
	
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier] autorelease];
    }
	
	// Configuration de la cellule
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.font = [UIFont fontWithName:kHelvetica size:16];
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.text = [[[array objectAtIndex:indexPath.section] objectForKey:kItems] objectAtIndex:indexPath.row];
	
	return cell;        	
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat result;
	
	UIFont *captionFont = [UIFont fontWithName:kHelvetica size:16];
	CGSize cs = [[[[array objectAtIndex:indexPath.section] objectForKey:kItems] objectAtIndex:indexPath.row] sizeWithFont:captionFont 
																										constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
																											lineBreakMode:UILineBreakModeWordWrap];
	result = cs.height + 40; // Ajout d'un espace entre chaque cellule
	
	return result;
}

-(CGFloat)tableView:(UITableView *)tableViewHeader heightForHeaderInSection:(NSInteger)section {
	
	if([self tableView:tableViewHeader titleForHeaderInSection:section] != nil) {
		return kSectionHeaderHeight;
	}
	else {
		return 0;
	}
	
}


-(UIView *)tableView:(UITableView *)tableViewHeader viewForHeaderInSection:(NSInteger)section {
	
	NSString *sectionTitle = [self tableView:tableViewHeader titleForHeaderInSection:section];
	
	if(sectionTitle == nil)
		return nil;
	
	//Créer étiquette avec le titre
	UILabel *label = [[[UILabel alloc] init] autorelease];
	label.frame = CGRectMake(20,6,300,30);
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor whiteColor];
	label.shadowColor = [UIColor clearColor];
	label.shadowOffset = CGSizeMake(0.0, 1.0);
	label.font =[UIFont boldSystemFontOfSize:16];
	label.text = sectionTitle;
	
	//Créer en-tête
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,kSectionHeaderHeight)];
	[view autorelease];
	[view addSubview:label];
	
	return view;
	
}

#pragma mark -

- (void)dealloc {
	[imageView release];
	[tableView release];
	[array release];
    [super dealloc];
}

@end
