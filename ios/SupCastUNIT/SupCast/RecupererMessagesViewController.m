//
//  RecupererMessagesViewController.m
//  SupCast
//
//  Created by fgutie10 on 28/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "RecupererMessagesViewController.h"
#import "AddCommentViewController.h"
#import "MessageCell.h"
#import "UtilsCore.h"
#define INTERESTING_TAG_NAMES @"user", @"commentaire", @"id", @"supprime", @"votes", nil

@implementation RecupererMessagesViewController

@synthesize username;
@synthesize password;
@synthesize ressourceInfo;
@synthesize titreParent;
@synthesize idEcranParent;
@synthesize idEcran;
@synthesize idBlog;
@synthesize categorie;
@synthesize labelTitre;
@synthesize pseudoCategorieTitre;
@synthesize titre;
@synthesize indicator;
@synthesize receivedDataMessages;
@synthesize listeCoor;
@synthesize boutonCreerBlog;
@synthesize plist;
@synthesize listeMessages;
@synthesize connectionMessages;
@synthesize connectionCreerBlog;
@synthesize receivedDataCreerBlog;

@synthesize idRessource;
@synthesize urlPage;
@synthesize tableView;


@synthesize swipeLeft;
@synthesize swipeRight;

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    
    return YES;
}

#pragma mark -

-(void)viewWillAppear:(BOOL)animated
{
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
}

- (void)viewDidLoad {
	DLog(@"");
    
    [TestFlight passCheckpoint:@"Lecture des messages"];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    refreshControl.tintColor = [UIColor colorWithRed:0.447 green:0.49 blue:0.969 alpha:1]; /*#727df7*/
    [refreshControl addTarget:self action:@selector(askForRefresh) forControlEvents:UIControlEventValueChanged];
    refreshControl.tag = 100;
    [tableView addSubview:refreshControl];
    
    [self refreshMessages];
}

-(void)refreshMessages{
    
    NSMutableArray * messagesAvantTraitement = [[UtilsCore getMessagesFromRessource:[NSNumber numberWithInt:[self.idRessource intValue]] nom_page:self.urlPage] mutableCopy];
    
    int rank = [[[[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"rank"] objectForKey:self.idRessource] objectForKey:@"id_categorie"] intValue];
    int idUser = [[NSUserDefaults standardUserDefaults] integerForKey:@"id"];
    
    _messages = [[NSArray arrayWithArray:messagesAvantTraitement] mutableCopy];
    
    for (Message * message in messagesAvantTraitement) {
        if(([message.supprime_par intValue] != 0) && ([message.supprime_par intValue] != idUser) && (rank < 3))
        {
            [_messages removeObject:message];
        }
    }
    
    
    [self ajouterControls];
    [tableView reloadData];
    
}
-(void)askForRefresh{
    [UtilsCore rafraichirMessagesFromRessource:[NSNumber numberWithInt:[self.idRessource intValue]] nom_page:self.urlPage];
    [(UIRefreshControl*)[tableView viewWithTag:100] endRefreshing];
}
#pragma mark -
#pragma mark Table View Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    DLog(@"");
    return 1;
}

-(NSInteger)tableView:(UITableView *)aTableView
numberOfRowsInSection:(NSInteger)section {
    DLog(@"");
    return _messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLog(@"");
    
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    
    static NSString *CellIdentifier = @"MessageCell";
    
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil] lastObject];
    }
    
    //Check de l'affichage
    
    int rank = [[[[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"rank"] objectForKey:self.idRessource] objectForKey:@"id_categorie"] intValue];
    int idUser = [[NSUserDefaults standardUserDefaults] integerForKey:@"id"];
    
    if([message.supprime_par intValue] == 0)
    {
        
        cell.contentView.alpha = 1;
        
    }
    else
    {
        if(([message.supprime_par intValue] == idUser) || (rank >= 3))
        {
            cell.contentView.alpha = 0.5;
        }
        //Else géré par heightForRow (on set la hauteur à 0)
    }
    
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
    (void)[imageView  initWithImage:[UtilsCore getImage:message.owner_guid]];
    
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:2];
    [lblName setText:message.owner_username];
    [lblName sizeToFit];
    
    UILabel *lblRank = (UILabel *)[cell viewWithTag:3];
    [lblRank setText:message.owner_rank_name];
    [lblRank sizeToFit];
    
    UILabel *lblDate = (UILabel *)[cell viewWithTag:4];
    NSDate * date_created = [NSDate dateWithTimeIntervalSince1970:[message.time_created  intValue]];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy à H:mm"];
    //    [formatter setDateFormat:@"dd/MM/yy"];
    NSString *dateString=[formatter stringFromDate:date_created];
    [lblDate setText:dateString];
    //    [lblDate sizeToFit];
    
    UILabel *lblValue = (UILabel *)[cell viewWithTag:5];
    [lblValue setText:message.value];
    
    UILabel *lblUtilite = (UILabel *)[cell viewWithTag:7];
    [lblUtilite setText:[message.utilite stringValue]];
    
    
    UIImageView * IvUtiliteLeft = ( UIImageView *)[cell viewWithTag:6];
    if ( [message.user_a_vote isEqualToNumber:@1]){
        [IvUtiliteLeft setImage:[UIImage imageNamed:@"arrow_left_active.png" ]];
    }else{
        [IvUtiliteLeft setImage:[UIImage imageNamed:@"arrow_left.png" ]];
    }
    
    
    UIImageView * IvUtiliteRight = ( UIImageView *)[cell viewWithTag:8];
    if ( [message.user_a_vote isEqualToNumber:@-1]){
       [IvUtiliteRight setImage:[UIImage imageNamed:@"arrow_right_active.png" ]];
    }else{
        [IvUtiliteRight setImage:[UIImage imageNamed:@"arrow_right.png" ]];
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    NSString *label =  message.value;
    
    
    int messageWidth = 227;
    
    if([tableView viewWithTag:5])
        messageWidth = [[tableView viewWithTag:5] frame].size.width;
    
    CGSize stringSize = [label sizeWithFont:[UIFont systemFontOfSize:14]
                          constrainedToSize:CGSizeMake(messageWidth, 9999)
                              lineBreakMode:NSLineBreakByWordWrapping];
    if(stringSize.height > 43)
        return (stringSize.height - 21) + 88;
    else
        return 88;
}
//Indispensable pour l'affichage du menuController
-(BOOL)canBecomeFirstResponder{
    return YES;
}

-(void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");
    
    [self becomeFirstResponder];
    
    MessageCell *cell = (MessageCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    NSMutableArray * menuItems = [[NSMutableArray alloc] init];
    
    
    
    int rank = [[[[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"rank"] objectForKey:self.idRessource] objectForKey:@"id_categorie"] intValue];
    int idUser = [[NSUserDefaults standardUserDefaults] integerForKey:@"id"];
    
    if (idUser != [message.owner_guid intValue])
    {
        UIMenuItem *repondre = [[UIMenuItem alloc] initWithTitle:@"Repondre" action:@selector(repondre:)];
        [menuItems addObject:repondre];
    }
    
    if (idUser == [message.owner_guid intValue])
    {
        UIMenuItem *editer = [[UIMenuItem alloc] initWithTitle:@"Editer" action:@selector(editer:)];
        [menuItems addObject:editer];
    }
    
    if([message.supprime_par intValue] == 0)
    {
        //Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
        if(([message.owner_guid intValue] == idUser) || (rank >= 3))
        {
            UIMenuItem *supprimer = [[UIMenuItem alloc] initWithTitle:@"Supprimer" action:@selector(supprimer:)];
            [menuItems addObject:supprimer];
        }
    }
    else
    {
        if(([message.supprime_par intValue] == idUser) || (rank >= 3))
        {
            UIMenuItem *supprimer = [[UIMenuItem alloc] initWithTitle:@"Supprimer" action:@selector(supprimer:)];
            [menuItems addObject:supprimer];
        }
        
    }
    
    [self setSelectedMessage:[_messages objectAtIndex:indexPath.row]];
    
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setMenuItems:menuItems];
    [menu setTargetRect:cell.frame inView: cell.superview];
    [menu setMenuVisible:YES animated:YES];
    
}

-(void)ajouterControls{
    //On va créer un rectangle transparent, que l'on va mettre comme background pour la toolbar
    //Solution pour iOS 5+ dite "propre"
    
    CGRect rect = CGRectMake(0,0,50,44.01);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context,rect);
    UIImage *transparentImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 50, 44.01)];
    
    //On l'applique à la toolbar
    
    [tools setBackgroundImage:transparentImage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    /*
     tools.barStyle = UIBarStyleBlack;
     tools.translucent = YES;
     tools.opaque = NO;
     tools.backgroundColor = [UIColor clearColor];
     */
    
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    
    //Création du bouton stylo
    UIImage *buttonImage = [UIImage imageNamed:kStylo];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setImage:buttonImage forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    aButton.showsTouchWhenHighlighted = YES;
    
    // Set the Target and Action pour le bouton stylo
    [aButton addTarget:self action:@selector(ajouterMessage) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    
    [buttons addObject:ajouterMessage];
    
    [tools setItems:buttons animated:NO];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
    
    
}

-(void)ajouterMessage {
    DLog(@"");
    
    AddCommentViewController *addCommentViewController = [AddCommentViewController alloc];
    
    addCommentViewController.titreParent   = self.titreParent;
    addCommentViewController.idEcranParent   = self.idEcranParent;
    addCommentViewController.idEcran   = self.idEcran;
    addCommentViewController.idBlog    = self.idBlog;
    addCommentViewController.titre     = self.titre;
    addCommentViewController.username  = self.username;
    addCommentViewController.ressourceInfo = self.ressourceInfo;
    addCommentViewController.password  = self.password;
    addCommentViewController.categorie = self.categorie;
    addCommentViewController.urlPage = self.urlPage;
    
    (void)[addCommentViewController initWithNibName:@"AddCommentView"
                                             bundle:nil];
    [self.navigationController pushViewController:addCommentViewController
                                         animated:YES];
    
}
-(void)editerMessage:(double)idMessage contenu:(NSString*)messageText{
    DLog(@"");
    
    AddCommentViewController *addCommentViewController = [AddCommentViewController alloc];
    
    addCommentViewController.titreParent   = self.titreParent;
    addCommentViewController.idEcranParent   = self.idEcranParent;
    addCommentViewController.idEcran   = self.idEcran;
    addCommentViewController.idBlog    = self.idBlog;
    addCommentViewController.titre     = self.titre;
    addCommentViewController.username  = self.username;
    addCommentViewController.ressourceInfo = self.ressourceInfo;
    addCommentViewController.password  = self.password;
    addCommentViewController.categorie = self.categorie;
    addCommentViewController.urlPage = self.urlPage;
    addCommentViewController.idMessage = idMessage;
    
    (void)[addCommentViewController initWithNibName:@"AddCommentView"
                                             bundle:nil];
    [self.navigationController pushViewController:addCommentViewController
                                         animated:YES];
    
    [[addCommentViewController textView] setText:messageText];
}

-(void)repondre:(id)sender{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Version bêta"
                                                    message:@"Cette fonctionnalité n'est pas encore disponible dans cette version."
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    
    [alert show];
}

-(void)editer:(id)sender{
    
    [self editerMessage:[[self selectedMessage].id_message doubleValue] contenu:[self selectedMessage].value];
}

-(void)supprimer:(id)sender{
    
    NSString * rank = [[[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"rank"] objectForKey:self.idRessource] objectForKey:@"id_categorie"];
    NSString * idUser = [[NSUserDefaults standardUserDefaults] stringForKey:@"id"];
    
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [self selectedMessage].id_message, @"id_message",
                             [self selectedMessage].supprime_par, @"supprime_par",
                             [self selectedMessage].owner_guid, @"id_user_op",
                             [self selectedMessage].owner_rank_id, @"id_rank_user_op",
                             idUser, @"id_user_moderator",
                             rank, @"id_rank_user_moderator",
                             [self idRessource], @"id_ressource", nil];
    

    
    [self post:[NSString stringWithFormat:@"%@/webServices/messages/supprimerMessage.php",sc_server]
         param: params
      callback: ^(NSDictionary *wsReturn) {
          switch ([[wsReturn objectForKey:@"result"] intValue]) {
              case -1:
              {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                                  message:@"Erreur de suppression"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Accepter"
                                                        otherButtonTitles:nil];
                  
                  [alert show];
                  break;
              }
              case 0:
              {
                  [self refreshMessages];
                  break;
              }
              case 1:
              {
                  [self refreshMessages];
                  break;
              }
              default:
              {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                                  message:@"Erreur de connexion"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Accepter"
                                                        otherButtonTitles:nil];
                  
                  [alert show];
                  break;
                  
              }
          }
      }
     errorMessage:@"Vous devez être connecté à Internet pour supprimer un message."];
    
}

-(IBAction)mapAffiche:(id)sender event:(id)event
{
    DLog(@"");
    
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tableView];
	NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
	}
}

- (IBAction)swipeToLeft:(id)sender {
    
    [TestFlight passCheckpoint:@"Vote d'utilité : positive"];
    
    // Prend la cellule
    CGPoint location = [swipeLeft locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    //Prend la fleche dans la cellule
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView * image = ( UIImageView *)[cell viewWithTag:6];
    
    [self lanceAnimationUtilite:image];
    
    //Appel du webservice
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"up",message.id_message , @"id_message", nil];
    [self post:[NSString stringWithFormat:@"%@/webServices/messages/voterUtilite.php",sc_server]
             param: query
          callback: ^(NSDictionary *wsReturn) {
              [self refreshMessages];
          }];
    
}
- (IBAction)swipeToRight:(id)sender {
    
    [TestFlight passCheckpoint:@"Vote d'utilité : négative"];
    
    // Prend la cellule
    CGPoint location = [swipeRight locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    //Prend la fleche dans la cellule
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView * image = ( UIImageView *)[cell viewWithTag:8];
    
    [self lanceAnimationUtilite:image];
    
    //Appel du webservice
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:@"false",@"up",message.id_message , @"id_message", nil];
    [self post:[NSString stringWithFormat:@"%@/webServices/messages/voterUtilite.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
        // Enregistrement dans le icore , puis rechargement de la table
              [self refreshMessages];
      }];
    
    
}



- (void)lanceAnimationUtilite:(UIImageView *)image{
    
    [UIView animateWithDuration:0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         image.transform = CGAffineTransformMakeScale(1.6, 1.6);
                         image.transform = CGAffineTransformMakeTranslation(0.5, 0.5);
                     }
                     completion:^(BOOL finished){
                         // Wait one second and then fade in the view
                         [UIView animateWithDuration:0.8
                                               delay: 0.0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              image.transform = CGAffineTransformIdentity;
                                          }
                                          completion:nil];
                     }];
}


#pragma mark -

- (void)didReceiveMemoryWarning {
   	DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
  	DLog(@"");
    [super viewDidUnload];
    [self setSwipeLeft:nil];
    [self setSwipeRight:nil];
    self.boutonCreerBlog = nil;
    self.indicator = nil;
	self.labelTitre = nil;
	self.indicator = nil;
    self.plist = nil;
    self.listeMessages = nil;
}

- (void)dealloc 
{
   	DLog(@"");
    //  [boutonCreerBlog release];
    
    
}


@end
