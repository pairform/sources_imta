//
//  CommentViewCell.h
//  SupCast
//
//  Created by fgutie10 on 29/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CommentViewCell : UITableViewCell {
	
	UILabel *labelCommentaire;
	UILabel *labelUsername;
	
	BOOL estSupprime;

}


@property(nonatomic,strong) UILabel *labelCommentaire;
@property(nonatomic,strong) UILabel *labelUsername;
@property(nonatomic,strong) UILabel *labelReputation;
@property(nonatomic) BOOL estSupprime;

+ (CGFloat)cellHeightForCaption:(NSString *)text width:(CGFloat)width;

@end
