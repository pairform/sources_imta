//
//  Parlez_vous_chinoisAppDelegate.m
//  SupCast
//
//  Created by Phetsana on 30/07/09.
//  Copyright EMN - CAPE 2009. All rights reserved.
//

#import "Parlez_vous_chinoisAppDelegate.h"
#import "WelcomeViewController.h"

@implementation Parlez_vous_chinoisAppDelegate

@synthesize window = _window;
@synthesize navigationController = _navigationController;
@synthesize welcomeViewController = _welcomeViewController;

- (void)applicationDidFinishLaunching:(UIApplication *)application {
	DLog(@"");
	_navigationController = [[UINavigationController alloc] init];
	_navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
	
	WelcomeViewController *viewController = [[WelcomeViewController alloc] initWithNibName:@"WelcomeView" bundle:nil];
	_welcomeViewController = viewController;
	[_navigationController pushViewController:_welcomeViewController
                                    animated:NO];
	
	[viewController release];
	[_window addSubview:_navigationController.view];
	
    // Override point for customization after application launch
    [_window makeKeyAndVisible];
	
}


- (void)dealloc {
	DLog(@"");
    [_window release];
	[_navigationController release];
	[_welcomeViewController release];
    [super dealloc];
}

@end