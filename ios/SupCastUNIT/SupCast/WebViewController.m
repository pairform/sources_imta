//
//  WebViewController.m
//  SupCast
//
//  Created by admin on 22/11/10.
//  Copyright 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

@synthesize aWebView,myLink;


- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil 
				 link:(NSString *)link


{
	DLog(@"");
	
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		myLink = link;
    }
	
    return self;
	
}

- (void)dealloc
{
	DLog(@"");
	aWebView.delegate = nil;
	[aWebView release];
	
	[super dealloc];
}

- (void)viewDidLoad
{
	DLog(@"");
	
	[super viewDidLoad];
	
	//self.title = NSLocalizedString(@"WebTitle", @"");
	
	CGRect webFrame = [[UIScreen mainScreen] applicationFrame];
	webFrame.origin.y += kTopMargin + 5.0;	// leave from the URL input field and its label
	webFrame.size.height -= 40.0;
	self.aWebView = [[[UIWebView alloc] initWithFrame:webFrame] autorelease];
	self.aWebView.backgroundColor = [UIColor whiteColor];
	self.aWebView.scalesPageToFit = YES;
	self.aWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	self.aWebView.delegate = self;
	[self.view addSubview: self.aWebView];

    [self.aWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:myLink]]];
}

// called after the view controller's view is released and set to nil.
// For example, a memory warning which causes the view to be purged. Not invoked as a result of -dealloc.
// So release any properties that are loaded in viewDidLoad or can be recreated lazily.
//
- (void)viewDidUnload
{
	
	DLog(@"");
	
	[super viewDidUnload];
	
	// release and set to nil
	self.aWebView = nil;
}


#pragma mark -
#pragma mark UIViewController delegate methods

- (void)viewWillAppear:(BOOL)animated
{
	DLog(@"");
    self.aWebView.delegate = self;	// setup the delegate as the web view is shown
	//[self.aWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:myLink]]];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	DLog(@"");
	
    [self.aWebView stopLoading];	// in case the web view is still loading its content
	self.aWebView.delegate = nil;	// disconnect the delegate as the webview is hidden
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	DLog(@"");
	
	// starting the load, show the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	DLog(@"");
	// finished loading, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	
	// load error, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// report the error inside the webview 
	NSString* errorString = [[NSString alloc] initWithFormat:@"<html><center><font size=+5 color='red'>Erreur : <br>%@<br>%@</font></center></html>",myLink,error.localizedDescription];
	[self.aWebView loadHTMLString:errorString baseURL:nil];
}

@end


