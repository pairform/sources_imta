//
//  RootViewController.h
//  SupCast
//
//  Created by Didier PUTMAN on 27/05/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GestionFichier.h"
#import "Categorie.h"
#import "TitreViewController.h"
#import "ZipArchive.h"
#import "Telechargement.h"
#import "Reachability.h"
#import "WBProgressHUD.h"
#import "StyledPullableView.h"
#import "RNBlurModalView.h"
#import "DACircularProgressView.h"

@interface RootViewController : UIViewController <UIAlertViewDelegate>
{
    NetworkStatus   statusConnexionReseau;
	Categorie *aCategorie;
    int currentIndex; 
    
    IBOutlet UITableView *aTableView;
    IBOutlet UIButton *boutonAjouterRessouces;
    WBProgressHUD   *HUD;

    NSURLConnection *rootConnection;
    

}
@property(nonatomic,strong) WBProgressHUD   *HUD;
@property(nonatomic,strong) NSURLConnection *rootConnection;

@property NetworkStatus   statusConnexionReseau;
@property(nonatomic,strong) NSString * guid;
@property(nonatomic,strong) NSArray *plist;
@property(nonatomic,strong) NSMutableData *receivedData;
@property(nonatomic,strong) NSString *ancienNiveau;
@property(nonatomic,strong) NSString *nouveauNiveau;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *username;
@property(nonatomic) BOOL estVisiteur;
@property(nonatomic,strong) NSMutableData *receivedDataNewMessages;
@property(nonatomic,strong) NSURLConnection *connectionNewMessages;
@property(nonatomic,strong) NSDictionary *nbreMessParRessource;

-(void)telechargement:(NSDictionary *)dico;
- (NSDictionary *)verif:(NSDictionary *)monDico;
- (IBAction)ajouterRessources:(id)sender;
- (void)goApp; 
- (void)initFichier;
- (void)rechercheRessourcesZippees;
- (void)recuparationListeApplication;
-(void)unzip:(NSString *)pathFichierZip;
-(BOOL)connected;
-(NetworkStatus) etatConnexionReseau;
- (void)newVersion:(id)sender;
- (void)showLoadingProgress;
- (void)hideLoadingProgress;
- (void)cancelConnection;
-(NSString *)dataFilePathCaches:(NSString *)fileName;
-(NSString *)dataFilePath:(NSString *)fileName;
-(NSString *)dataFilePathAtDirectory:(NSString *)directory 
                            fileName:(NSString *)fileName; 
-(void)addSkipBackupAttributeToPath:(NSString*)path;
-(void)requeteNvxMessages;
- (void)updatePercentageNotification:(NSNotification*)notification;
@end