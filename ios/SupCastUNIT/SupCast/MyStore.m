//
//  MyStore.m
//  SupCast
//
//  Created by admin on 24/11/10.
//  Copyright 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "MyStore.h"

@implementation MyStore

@synthesize aTextView;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil
			   bundle:(NSBundle *)nibBundleOrNil
				 downloadLink:(NSString *)downloadLink {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization

    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	
	CGRect textFrame = [[UIScreen mainScreen] applicationFrame];
	textFrame.origin.y += kTopMargin + 5.0;	// leave from the URL input field and its label
	textFrame.size.height -= 40.0;
	self.aTextView = [[[UITextView alloc] initWithFrame:textFrame] autorelease];
	self.aTextView.backgroundColor = [UIColor whiteColor];
	self.aTextView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	self.aTextView.delegate = self;
	
/*
		
	NSURLDownload * urlD =[NSURLDownload alloc] initWithRequest:urlRequestDownload delegate:self];

	[urlD Release];*/
	
    [super viewDidLoad];
	
	// Download Support
	
}


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

	aTextView.delegate = nil;
	[aTextView release];

    [super dealloc];
}


@end
