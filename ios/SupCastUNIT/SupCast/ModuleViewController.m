//
//  ModuleViewController.m
//  SupCast
//
//  Created by Phetsana on 11/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//
#import "GestionFichier.h"
#import "ModuleViewController.h"
#import "WebViewContentsViewController.h"
#import "AddCommentViewController.h"
#import "RecupererMessagesViewController.h"

@implementation ModuleViewController
@synthesize moduleContent;
@synthesize dicoRacine;
@synthesize selectedRowIndexPath;

- (void)viewDidLoad 
{
    DLog(@"");	
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    DLog(@"");	
    [super viewWillAppear:animated];
	// On déselectionne les précédentes sélections
    //NSIndexPath *tableSelection = [tableView indexPathForSelectedRow];
	//[tableView deselectRowAtIndexPath:tableSelection animated:NO];
    
    [tableView reloadData];
    
    [[tableView cellForRowAtIndexPath:self.selectedRowIndexPath] setSelected:YES animated:YES];

}

- (void)didReceiveMemoryWarning {
    DLog(@"");	
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    DLog(@"");	
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DLog(@"");	
	return [[self.moduleContent objectForKey:@"listeDesSequences"] count];
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLog(@"");	
    static NSString *CellIdentifier = @"ModuleCell";
	
	
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier];
    }
	
	// Configuration de la cellule
	cell.textLabel.numberOfLines = 0;
	
	cell.textLabel.textAlignment   = UITextAlignmentCenter;
	cell.textLabel.text = [[self.moduleContent objectForKey:@"listeDesSequences"] objectAtIndex:indexPath.row];
    
    //On calcule le nombre de message à l'intérieur
    NSDictionary * dico1 = [[NSDictionary alloc] initWithDictionary:[[self.moduleContent objectForKey:@"sequenceContent"] objectAtIndex:indexPath.row]];
    NSString *idEcranCell   = [[NSString alloc] initWithString:[dico1 objectForKey:kIdecran]];
    
    DLog(@"_______________________________________________________");
    DLog(@"Id de la ligne : %@",idEcranCell);
    
    NSMutableString * nombreDeMessages =  [[NSMutableString alloc] init];
    NSArray *listIdEcran=[[NSArray alloc] initWithArray:[self.pcrDictionary objectForKey:idEcranCell]];
   
    //Affichage ou non de la flèche
    
    DLog(@"Nombre d'enfants : %d",[listIdEcran count]);

    if ([listIdEcran count] != 1)
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else cell.accessoryType = UITableViewCellAccessoryNone;
    
    //Méthode pour récuperer les messages de la cellule et de ses enfants (comme dans PVC) 
    
    
    //Méthode pour récuperer les messages de la cellule et de ses enfants
    if([self.dicoNombreMessages count])
    {
        int showNumberMsg = [self trouverMessagesDansMenu:dico1 messagesToFind:self.dicoNombreMessages];
        

        if(showNumberMsg != 0)
        {
            nombreDeMessages = [NSString stringWithFormat:@"%d",showNumberMsg];
            
            DLog(@"Nombre de messages = %@, ID de la ligne = %@ , Index de la ligne = %@", nombreDeMessages, idEcranCell, indexPath);
            
            //On créé une bulle rouge
            //cell.textLabel.shadowColor = [UIColor lightGrayColor];
            
            UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage * image;
            
            image = [UIImage imageNamed:kBulleBarreRedMiroir];
            [aButton1 setTitle:nombreDeMessages forState:UIControlStateNormal];
            [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            aButton1.titleLabel.font=[UIFont systemFontOfSize:13];    
            aButton1.alpha = 0.8;
            
            //Framing du bouton
            CGRect frame = CGRectMake(10.0, 0.0, 32.0, 32.0);   
            frame.origin.y =  (cell.frame.size.height - frame.size.height) / 2;
            aButton1.frame=frame;
            
            /*Reframing de la cellule
             CGRect frameTextCell = cell.textLabel.frame;
             frameTextCell.size.width -= 40;
             frameTextCell.origin.x += 40;
             cell.textLabel.frame = frameTextCell;
             */
            
            [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
            
            //On lui attribue un tag pour le supprimer lors de la réutilisation de la ligne
            aButton1.tag = tagBulles;
            
            //Et on le cale en subview de la celulle.
            [cell.contentView addSubview:aButton1];       
        }
    }
    
    /*
    if (self.nvxMessage == nil) 
    {
        DLog(@"Pas de message correspondant à la ligne.");   
    }
    else
    {    
        //calcul du nombre de nouveau pour afficher dans la bulle
        for (NSDictionary *NewMsg in self.nvxMessage)
        {   
            if([[NewMsg objectForKey:@"idecran"] isEqualToString:idEcranCell] && 
               [[NewMsg objectForKey:@"nombreNouveauMessage"] intValue]>0        )
            {
                nombreDeMessages = [NewMsg objectForKey:@"nombreNouveauMessage"];
                DLog(@"Nombre de messages = %@, ID de la ligne = %@ , Index de la ligne = %@", nombreDeMessages, idEcranCell, indexPath);
                cell.textLabel.textColor = [UIColor redColor];
                
                UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
                UIImage * image;
                
                //si nouveaux messages bulle rouge
                image = [UIImage imageNamed:kBulleBarreRed];
                [aButton1 setTitle:nombreDeMessages forState:UIControlStateNormal];
                [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                aButton1.titleLabel.font=[UIFont systemFontOfSize:13];    
                
                CGRect frame = CGRectMake(20.0, 0.0, 32.0, 32.0);            
                aButton1.frame=frame;
                [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
                
                aButton1.showsTouchWhenHighlighted = YES;                
                cell.accessoryView = aButton1;
                
                break;
            }
        }
    }*/
    
	return cell;        	
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DLog(@"");	

	self.selectedRowIndexPath =indexPath;
    
    //
    NSString * referenceFichier;        
    NSString * monIdecran;        
    NSArray * sousRefFichier;
    NSArray * sequenceContent;
    NSString * sequenceName;
    NSArray * listeDesSequences;
    //
    if ([self.moduleContent objectForKey:@"idecran"])
        monIdecran = [[NSString alloc] initWithString:[self.moduleContent objectForKey:@"idecran"]];
    if ([self.moduleContent objectForKey:@"referenceFichier"])
        referenceFichier = [[NSString alloc] initWithString:[self.moduleContent objectForKey:@"referenceFichier"]];
    if ([self.moduleContent objectForKey:@"sequenceName"])
        sequenceName =[[NSString alloc] initWithString:[self.moduleContent objectForKey:@"sequenceName"]];
    if ([self.moduleContent objectForKey:@"listeDesSequences"])
        listeDesSequences = [[NSArray alloc] initWithArray:[self.moduleContent objectForKey:@"listeDesSequences"]];
    //
    NSDictionary * dicoModuleContent;
    if ([self.moduleContent objectForKey:@"sousRefFichier"])
    {   
        sousRefFichier = [[NSArray alloc] initWithArray:[self.moduleContent objectForKey:@"sousRefFichier"]];
        if (([sousRefFichier count] >0) && (indexPath.row<= [sousRefFichier count]) )
        {
            dicoModuleContent =  [[NSDictionary alloc] initWithDictionary:[sousRefFichier objectAtIndex:indexPath.row]];
        }
    }    
    else
    {
        if ([self.moduleContent objectForKey:@"sequenceContent"])
        {            sequenceContent = [[NSArray alloc] initWithArray:[self.moduleContent objectForKey:@"sequenceContent"]];
            if (([sequenceContent count] >0) && (indexPath.row<= [sequenceContent count]) )
            {
                dicoModuleContent = [[NSDictionary alloc] initWithDictionary:[sequenceContent objectAtIndex:indexPath.row]];
            }
        }
    }
    //
    NSString * itemIdecran;        
    NSString * itemSequenceName;
    NSString * itemReferenceFichier;
    
    //NSArray * itemSequenceContent;
    NSArray * itemListeDesSequences;
    
    if ([dicoModuleContent objectForKey:@"idecran"])
        itemIdecran = [[NSString alloc] initWithString:[dicoModuleContent objectForKey:@"idecran"]];
    //if ([dicoModuleContent objectForKey:@"sequenceContent"])
    //    itemSequenceContent = [[NSArray alloc] initWithArray:[dicoModuleContent objectForKey:@"sequenceContent"]];
    if ([dicoModuleContent objectForKey:@"sequenceName"])
        itemSequenceName = [[NSString alloc] initWithString:[dicoModuleContent objectForKey:@"sequenceName"]];
    if ([dicoModuleContent objectForKey:@"referenceFichier"])
        itemReferenceFichier = [[NSString alloc] initWithString:[dicoModuleContent objectForKey:@"referenceFichier"]];
    
    if ([dicoModuleContent objectForKey:@"listeDesSequences"])
    {   
        itemListeDesSequences = [[NSArray alloc] initWithArray:[dicoModuleContent objectForKey:@"listeDesSequences"]];
        if ( ([itemListeDesSequences count]>0) ) 
        {        
            if(self.estVisiteur)
            {
                
                // Appel ModuleViewController
                ModuleViewController *moduleViewController = [ModuleViewController alloc];
                // infos communes
                moduleViewController.estVisiteur   = self.estVisiteur;
                moduleViewController.titre         = [listeDesSequences objectAtIndex:indexPath.row]; 
                moduleViewController.username      = @"";
                moduleViewController.titreParent   = self.titre;
                moduleViewController.idEcranParent = self.idEcran;
                moduleViewController.idEcran       = itemIdecran;
                moduleViewController.idBlog        = @"";
                moduleViewController.password      = @"";
                moduleViewController.categorie     = @"";
                moduleViewController.ressourceInfo = self.ressourceInfo;
                moduleViewController.pcrDictionary = self.pcrDictionary;
                // titre
                moduleViewController.title         = [listeDesSequences objectAtIndex:indexPath.row]; 
                // infos ressources
                moduleViewController.dicoRacine    = dicoModuleContent;
                moduleViewController.moduleContent = dicoModuleContent;
                // init
                (void)[moduleViewController initWithNibName:@"ModuleView" bundle:nil];
                // push
                [self.navigationController pushViewController:moduleViewController animated:YES];
            }
            else
            {
                // Appel ModuleViewController
                ModuleViewController *moduleViewController = [ModuleViewController alloc];
                // Infos communes
                moduleViewController.estVisiteur   = self.estVisiteur;
                moduleViewController.titre         = [listeDesSequences objectAtIndex:indexPath.row];
                moduleViewController.username      = self.username;
                moduleViewController.titreParent   = self.titre;
                moduleViewController.idEcranParent = itemIdecran;
                moduleViewController.idEcran       = itemIdecran;
                moduleViewController.idBlog        = @"";
                moduleViewController.password      = self.password;
                moduleViewController.categorie     = self.categorie;
                moduleViewController.ressourceInfo = self.ressourceInfo;
                moduleViewController.pcrDictionary = self.pcrDictionary;
                // Infos communes
                moduleViewController.dicoRacine    = self.dicoRacine;
                moduleViewController.moduleContent = dicoModuleContent;
                // Titre
                moduleViewController.title         = [listeDesSequences objectAtIndex:indexPath.row]; 
                // init
                (void)[moduleViewController initWithNibName:@"ModuleView" bundle:nil];
                // push
                [self.navigationController pushViewController:moduleViewController animated:YES];
            }
            
        }
    }
    else
    {
        if(self.estVisiteur)
        {
            
             // Appel WebViewContentsViewController
             WebViewContentsViewController *webViewcontentsViewController = [WebViewContentsViewController alloc];
             // infos communes
             webViewcontentsViewController.estVisiteur   = self.estVisiteur;
             webViewcontentsViewController.titre         = itemSequenceName; 
             webViewcontentsViewController.username       = @"";
             
             webViewcontentsViewController.titreParent = self.titre;
             webViewcontentsViewController.idEcranParent = self.idEcran;
             webViewcontentsViewController.idEcran = [moduleContent objectForKey:kIdecran];
             
             webViewcontentsViewController.idBlog        = @"";
             webViewcontentsViewController.password      = @"";
             webViewcontentsViewController.categorie     = @"";
             webViewcontentsViewController.ressourceInfo = self.ressourceInfo;
             // infos ressources
             if (itemReferenceFichier != nil)
             webViewcontentsViewController.pageHtml  = itemReferenceFichier;
             //webViewcontentsViewController.contentList   = itemSequenceContent;
             // titre
             webViewcontentsViewController.title         = itemSequenceName; 
             // init
             (void)[webViewcontentsViewController initWithNibName:@"WebViewContentView" bundle:nil];
            
            [self.navigationController pushViewController:webViewcontentsViewController animated:YES];
            /*
            NSMutableArray * viewsVoisines = [[NSMutableArray alloc] init];
            
            for(NSDictionary * test in sequenceContent)
            {
                
                // Appel WebViewContentsViewController
                WebViewContentsViewController *webViewcontentsViewController = [WebViewContentsViewController alloc];
                // infos communes
                webViewcontentsViewController.estVisiteur   = self.estVisiteur;
                webViewcontentsViewController.titre         = [test objectForKey:@"sequenceName"]; 
                webViewcontentsViewController.username       = @"";
                
                webViewcontentsViewController.titreParent = self.titre;
                webViewcontentsViewController.idEcranParent = self.idEcran;
                webViewcontentsViewController.idEcran = [test objectForKey:kIdecran];
                
                webViewcontentsViewController.idBlog        = @"";
                webViewcontentsViewController.password      = @"";
                webViewcontentsViewController.categorie     = @"";
                webViewcontentsViewController.ressourceInfo = self.ressourceInfo;
                // infos ressources
                if (itemReferenceFichier != nil)
                    webViewcontentsViewController.pageHtml  = [test objectForKey:@"referenceFichier"];
                //webViewcontentsViewController.contentList   = itemSequenceContent;
                // titre
                webViewcontentsViewController.title         = [test objectForKey:@"sequenceName"]; 
                // init
                (void)[webViewcontentsViewController initWithNibName:@"WebViewContentView" bundle:nil];
                
                [viewsVoisines addObject:webViewcontentsViewController];
            }
            
            // Maen - Paging
            
            NSDictionary * options = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:UIPageViewControllerSpineLocationMax ] forKey:UIPageViewControllerOptionSpineLocationKey];
            
            PagesViewController *pageViewController = [[PagesViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:options];
                        
            NSArray *viewsVoisines2 = [[NSArray alloc] initWithObjects:[viewsVoisines objectAtIndex:0], [viewsVoisines objectAtIndex:2], nil];
            
            [pageViewController setViewControllers:viewsVoisines2 direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
            
            //
            [self.navigationController pushViewController:pageViewController animated:YES];
             */
        }    
        else
        {	
            
            // Appel WebViewContentsViewController
            WebViewContentsViewController *webViewcontentsViewController = [WebViewContentsViewController alloc];
            // infos communes
            webViewcontentsViewController.estVisiteur   = self.estVisiteur;
            webViewcontentsViewController.titre         = itemSequenceName; 
            webViewcontentsViewController.username      = self.username;
            
            webViewcontentsViewController.titreParent = self.titre;
            webViewcontentsViewController.idEcranParent = self.idEcran;
            if (itemIdecran != nil)
                webViewcontentsViewController.idEcran   = itemIdecran;
            webViewcontentsViewController.idBlog        = @"";
            webViewcontentsViewController.password      = self.password;
            webViewcontentsViewController.categorie     = self.categorie;
            webViewcontentsViewController.ressourceInfo = self.ressourceInfo;
            // infos ressources
            webViewcontentsViewController.pcrDictionary = self.pcrDictionary;
            if (itemReferenceFichier != nil)
                webViewcontentsViewController.pageHtml  = itemReferenceFichier;
            //webViewcontentsViewController.contentList   = itemSequenceContent;
            // titre
            webViewcontentsViewController.title         = itemSequenceName; 
            // init
            (void)[webViewcontentsViewController initWithNibName:@"WebViewContentView" bundle:nil];
            // push
            [self.navigationController pushViewController:webViewcontentsViewController animated:YES];
            
        }
    }
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


#pragma mark -


@end
