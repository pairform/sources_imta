//
//  MainViewCell.m
//  SupCast
//
//  Created by Phetsana on 05/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "MainViewCell.h"

@implementation MainViewCell

@synthesize itemsArray;
@synthesize myMessageReminderView;
@synthesize numberOfMsgLabel;

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        
        
		self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		// Initialisation du titre et nb de messages
	  
        numberOfMsgLabel=[[UILabel alloc] init];
        numberOfMsgLabel.textAlignment=UITextAlignmentCenter;
        numberOfMsgLabel.backgroundColor=[UIColor clearColor];
        numberOfMsgLabel.opaque=NO;        
        numberOfMsgLabel.font=[UIFont systemFontOfSize:12];
        //numberOfMsgLabel.textColor=[UIColor redColor];
		
		// Initialisation des éléments du titre
		itemsArray = [[NSMutableArray alloc] init];
		for(int i = 0; i < 4; i++) {
			UILabel *tmpLabel = [[UILabel alloc] init];
			tmpLabel.text = UITextAlignmentLeft;
			tmpLabel.font = [UIFont systemFontOfSize:10];	
			[itemsArray addObject:tmpLabel];
		}
		
		myMessageReminderView = [[UIImageView alloc] init];
		myMessageReminderView.opaque=NO;
		// Ajout à la vue

		[self.contentView addSubview:myMessageReminderView];
		[self.contentView addSubview:numberOfMsgLabel];        
        
        
	}
	
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}



@end
