//
//  ContentsViewController.h
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentsViewController : UIViewController <UIAlertViewDelegate> {
	IBOutlet UITextView *textView;
	IBOutlet UIImageView *imageView;
    IBOutlet UILabel *label;
	IBOutlet UILabel *labelTitre;
	IBOutlet UITableView *tableView;
	NSArray *contentList;
	int indexItemContent; // indice de l'item courant
	NSString *indication;
	UIAlertView *alertRightAnswer;
	IBOutlet UIToolbar *toolBar;
	IBOutlet UIScrollView *mainScrollView;
	IBOutlet UIView *mainView;
	NSString *dirRes;
	NSString *titre;
	
}

@property (nonatomic, retain) NSString *titre;
@property (nonatomic, retain) NSString *dirRes;
@property (nonatomic, retain) NSArray *contentList;
@property (nonatomic, retain) NSString *indication;

+ (NSMutableArray *)sortArray:(NSMutableArray *)arrayToSort;

- (void)previousQuestion;
- (void)nextQuestion;
- (UIImage *)resizeImage:(UIImage *)sourceImage max:(CGFloat)maximum;
- (UIImage *)resizeImage:(UIImage *)sourceImage maxHeight:(CGFloat)maxHeight maxWidth:(CGFloat)maxWidth;

@end
