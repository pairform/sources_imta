//
//  TableContentViewController.m
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "TableContentViewController.h"
#import "GestionFichier.h"
#import "Movie.h"

#define kFontSize 20
#define FONT_SIZE_INDICATION 12


@implementation TableContentViewController

// The designated initializer. Override to perform setup that is required before the view is loaded.


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	DLog(@"");
	
	
	labelTitre.text = titre;
	labelTitre.textColor = [UIColor blackColor];
	
	[mainView setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin |
	 UIViewAutoresizingFlexibleRightMargin | 
	 UIViewAutoresizingFlexibleWidth | 
	 UIViewAutoresizingFlexibleHeight];
    [mainView setAutoresizesSubviews:YES];
	
    [super viewDidLoad];	
	
}

- (void)viewWillDisappear:(BOOL)animated {
	DLog(@"");
	
	[super viewWillDisappear:animated];
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	DLog(@"");
	return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
	DLog(@"");
	return [contentList count];
	
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	DLog(@"");
	
	[firstPieceView removeFromSuperview];
	firstPieceView = nil;
	[firstPieceView release];
	
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
	{
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.textAlignment = UITextAlignmentLeft;
	
	cell.textLabel.textColor = [UIColor blackColor];
	cell.textLabel.font = [UIFont fontWithName:kHelvetica size:12];
	
    
    cell.textLabel.text = @"";			
    cell.imageView.image = nil;		
	
    if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"texte"] != nil )
	{		
		cell.textLabel.text = [[contentList objectAtIndex:indexPath.row] objectForKey:@"texte"];
		//cell.imageView.image = nil;		
	}
	else if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"imageName"] != nil )
	{
		cell.textLabel.text = @"";
		
		UIImage * myImage = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																												  fileName:[[contentList objectAtIndex:indexPath.row] objectForKey:@"imageName"]]];
		
		UIImage *resizedImage = [self resizeImage:myImage maxHeight:189.0f maxWidth:320.0f];			
		cell.imageView.image = resizedImage;			
		cell.textLabel.textAlignment = UITextAlignmentLeft;
	}
	else if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"videoName"] != nil )
	{
		cell.textLabel.text = @"Voir la video";				
		cell.textLabel.textAlignment = UITextAlignmentCenter;
	}
	else if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"soundName"] != nil )
	{
		cell.textLabel.text = @"Ecouter la séquence audio";				
		cell.textLabel.textAlignment = UITextAlignmentCenter;
	}
	if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"imageicone"] != nil )
	{
        
        UIImage * myImage = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																												  fileName:[[contentList objectAtIndex:indexPath.row] objectForKey:@"imageicone"]]];
	    cell.imageView.image = myImage;		
	}
	
	return cell;        	
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	[firstPieceView removeFromSuperview];
	firstPieceView = nil;
	[firstPieceView release];
	
	DLog(@"");
	
	if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"imageName"] != nil )
	{
		UIImage * myImage = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																												  fileName:[[contentList objectAtIndex:indexPath.row] objectForKey:@"imageName"]]];
		
		UIImage *resizedImage = [self resizeImage:myImage max:480.0f];
		firstPieceView=[[UIImageView alloc] initWithImage:resizedImage];
	    firstPieceView.userInteractionEnabled = YES;
		[mainView addSubview:firstPieceView];
		
	}
	else if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"videoName"] != nil )
	{
		NSString * fileName = [[contentList objectAtIndex:indexPath.row] objectForKey:@"videoName"];
		
		
		movie = [Movie alloc];
		movie.dirRes = self.dirRes;
		[movie initMoviePlayerWithFullName:fileName];
		[movie playMovie];
		
		
	}
	
	else if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"soundName"] != nil )
	{
		NSString * fileName = [[contentList objectAtIndex:indexPath.row] objectForKey:@"soundName"];
		
		movie = [Movie alloc];
		movie.dirRes = self.dirRes;
		[movie initMoviePlayerWithFullName:fileName];
		[movie playMovie];
		
		
	}
	
	
	else
	{
		[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	}
	
	
}



#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	DLog(@"");
    CGSize cs;
	
	UIFont *captionFont = [UIFont boldSystemFontOfSize:12];
	
	
	
	if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"texte"] != nil )
	{		
		cs = [[[contentList objectAtIndex:indexPath.row] objectForKey:@"texte"] sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
	}
	else
	{		
		if ([[contentList objectAtIndex:indexPath.row] objectForKey:@"imageName"] != nil )
		{
			UIImage * myImage = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																													  fileName:[[contentList objectAtIndex:indexPath.row] objectForKey:@"imageName"]]];
			UIImage *resizedImage = [self resizeImage:myImage maxHeight:180.0f maxWidth:320.0f];
		    cs = CGSizeMake(resizedImage.size.width,resizedImage.size.height);
			
		}
		else
		{
		    cs = CGSizeMake(320,50);
		}
		
	}	
	
	if (cs.height > 50 )
		return cs.height;
	else 
		return 50;
	
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self updateNavBar];
}

- (void) updateNavBar {
	
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if ((UIInterfaceOrientationLandscapeLeft == orientation) ||
        (UIInterfaceOrientationLandscapeRight == orientation)) {
		//  [myBar setFrame:CGRectMake(0, 0, 480, 44)];
		//	[myTableView setFrame:CGRectMake(0, 44.01, 480, 220)];
		//[myTabBar setFrame:CGRectMake(0, 260, 480, 44)];
		
    } else {
		//  [myBar setFrame:CGRectMake(0, 0, 320, 44)];
		//[myTableView setFrame:CGRectMake(0, 44.01, 320, 360)];
		//[myTabBar setFrame:CGRectMake(0, 420, 320, 44)];
		
	}
}

- (void)dealloc {
	DLog(@"");
    [tableView release];
	[labelTitre release];
	[super dealloc];
}

@end
