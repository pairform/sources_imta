//
//  ChangePasswordViewController.m
//  SupCast
//
//  Created by wbao11 on 19/07/11.
//  Copyright (c) 2011 EMN - CAPE. All rights reserved.
//

#import "ChangePasswordViewController.h"

@implementation ChangePasswordViewController

@synthesize confirmer;
@synthesize currentPassword;
@synthesize passwordNewA;
@synthesize passwordNewB;
@synthesize username;
@synthesize password;
@synthesize receivedData;
@synthesize indicator;


#pragma -mark
#pragma mark TabBar Delegate Methods

-(void)changePassword
{
    
    DLog(@"");
    
    NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kchangePassword];
    
    NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *passwordNew = [NSString stringWithString:self.passwordNewA];
    NSString *paramDataString = [NSString stringWithFormat:
                                 @"id=%@&pseudo=%@&password=%@&passwordNew=%@",
                                 kKey,
                                 self.username,
                                 self.password,
                                 passwordNew];
    
    NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:paramData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];;
    
    if(connection) 
    {
        [indicator startAnimating];
		self.receivedData = [[NSMutableData alloc] init];
    }
    else 
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                        message:@"Erreur de connexion"
                                                       delegate:self
                                              cancelButtonTitle:@"Accepter"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
    
    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    DLog(@"");
    
    if([alertView.message isEqualToString:@"Votre mot de passe a été changé,veuillez vous reidentifier!"])
    {
        UserAccountViewController *parentViewController=(UserAccountViewController *)[self.view.superview nextResponder];
        
        [parentViewController.navigationController popToRootViewControllerAnimated:YES]; 
    }
    
}

-(IBAction)backgroundTap:(id)sender
{
    DLog(@"");
    
    [myTextField0 resignFirstResponder];
    [myTextField1 resignFirstResponder];
    [myTextField2 resignFirstResponder];
}


-(IBAction)confirmChangePassword:(id)sender

{   
    DLog(@"");
    
    [myTextField0 resignFirstResponder];
    [myTextField1 resignFirstResponder];
    [myTextField2 resignFirstResponder];
    
    self.currentPassword = myTextField0.text;
    self.passwordNewA = myTextField1.text;
    self.passwordNewB = myTextField2.text;
    
    //confirm that all the fields are filled
    if (sender == confirmer) 
    {
        if ([self.currentPassword isEqualToString:@""] || 
            [self.passwordNewA isEqualToString:@""]    || 
            [self.passwordNewB isEqualToString:@""]) 
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Veuillez remplir toutes les données du formulaire"
                                                           delegate:self 
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        else if (![currentPassword isEqualToString:password])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Veuillez corriger votre ancien mot de passe !"
                                                           delegate:self 
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        else if (![passwordNewA isEqualToString:passwordNewB])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Veuillez corriger le nouveau mot de passe !"
                                                           delegate:self 
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
        }
    }   
    
    //Si tout est OK    
    if (![self.currentPassword isEqualToString:@""] && 
        ![self.passwordNewA isEqualToString:@""]    && 
        ![self.passwordNewB isEqualToString:@""]    &&  
        [currentPassword isEqualToString:password]  &&
        [passwordNewA isEqualToString:passwordNewB] &&
        (sender == confirmer))    
    {
        [self changePassword];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    DLog(@"");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
        self.title = @"Modifcation du mot de passe";
    }
    return self;
}

- (void)dealloc
{
    DLog(@"");
    
}

- (void)didReceiveMemoryWarning
{
    DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillDisappear:(BOOL)animated
{
    DLog(@"");
    [myTextField0 resignFirstResponder];
    [myTextField1 resignFirstResponder];
    [myTextField2 resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    DLog(@"");
    
}  

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    tableView.backgroundColor = [UIColor clearColor];
	tableView.backgroundView = nil;
    
    /*coustomize the button 
    UIImage *bouton = [UIImage imageNamed:@"whiteButton.png"];
    UIImage *stretchableBouton = [bouton stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [confirmer setBackgroundImage:stretchableBouton forState:UIControlStateNormal];
    */
    // keep the text fields blank
    currentPassword=@"";
    passwordNewA=@"";
    passwordNewB=@"";
    
	[tableView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
  	[indicator stopAnimating];
    
}

- (void)viewDidUnload
{
    DLog(@"");
    [super viewDidUnload];
    indicator = nil;
    currentPassword = nil;
    passwordNewB = nil;
    passwordNewA = nil;
    confirmer = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}



#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    DLog(@"");
    return 3;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    DLog(@"");
    
    return 1;
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {} else {tableView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, 740,tableView.frame.size.height);}
    DLog(@"");
    static NSString *kMdpActuelCellIdentifier         = @"Ancien";
	static NSString *kMdpNouveauCellIdentifier        = @"Nouveau";
	static NSString *kMdpConfirmationCellIdentifier   = @"Confirmation";
    
	UITableViewCell * cell = nil;
    
    
	switch (indexPath.section)
	{
		case 0:
            myTextField0 = nil;
            customCell = nil;
            
            customCell = [tableView dequeueReusableCellWithIdentifier:kMdpActuelCellIdentifier];
            if (customCell == nil)
            {
                customCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMdpActuelCellIdentifier];
                customCell.textLabel.text = @"Ancien : ";
                customCell.textLabel.font = [UIFont systemFontOfSize:12];
                
                myTextField0 = [[UITextField alloc] initWithFrame:CGRectMake(100, 8, 190, 27)];
                myTextField0.backgroundColor = [UIColor clearColor];
                myTextField0.tag = indexPath.section;
                myTextField0.delegate = self;
                myTextField0.secureTextEntry = YES;
                myTextField0.autocorrectionType = UITextAutocorrectionTypeNo;
                myTextField0.keyboardType = UIKeyboardTypeDefault;
                myTextField0.autocapitalizationType= UITextAutocapitalizationTypeNone;
                myTextField0.borderStyle = UITextBorderStyleBezel;
                myTextField0.returnKeyType = UIReturnKeyDone;
                [myTextField0 addTarget:self action:@selector(textFieldDone:) 
                       forControlEvents:UIControlEventEditingDidEnd];
                [myTextField0 setText:@""];   
                myTextField0.placeholder =  @"Ancien mot de passe"; 
                
                [customCell.contentView addSubview:myTextField0];
            }
            
            cell = customCell;
            
			
			break;
            
        case 1:
            
            myTextField1 = nil;
            customCell = nil;
            
            customCell = [tableView dequeueReusableCellWithIdentifier:kMdpNouveauCellIdentifier];
            if (customCell == nil)
            {
                customCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMdpNouveauCellIdentifier];
                customCell.textLabel.text = @"Nouveau : ";
                customCell.textLabel.font = [UIFont systemFontOfSize:12];
                myTextField1 = [[UITextField alloc] initWithFrame:CGRectMake(100, 8, 190, 27)];
                myTextField1.backgroundColor = [UIColor clearColor];
                myTextField1.tag = indexPath.section;
                myTextField1.delegate = self;
                myTextField1.placeholder =  @"Nouveau mot de passe"; 
                myTextField1.tag = indexPath.section;
                myTextField1.secureTextEntry = YES;
                myTextField1.autocorrectionType = UITextAutocorrectionTypeNo;
                myTextField1.keyboardType = UIKeyboardTypeDefault;
                myTextField1.autocapitalizationType= UITextAutocapitalizationTypeNone;
                myTextField1.borderStyle = UITextBorderStyleBezel;
                myTextField1.returnKeyType = UIReturnKeyDone;
                [myTextField1 addTarget:self action:@selector(textFieldDone:) 
                       forControlEvents:UIControlEventEditingDidEnd];
                
                [myTextField1 setText:@""];   
                
                [customCell.contentView addSubview:myTextField1];
            }
            
            cell = customCell;
            
            
            
            break;
            
        case 2:
            
            myTextField2 = nil;
            customCell = nil;
            
            customCell = [tableView dequeueReusableCellWithIdentifier:kMdpConfirmationCellIdentifier];
            if (customCell == nil)
            {
                customCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMdpConfirmationCellIdentifier];
                customCell.textLabel.text = @"Confirmation : ";
                customCell.textLabel.font = [UIFont systemFontOfSize:12];
                
                myTextField2 = [[UITextField alloc] initWithFrame:CGRectMake(100, 8, 190, 27)];
                myTextField2.backgroundColor = [UIColor clearColor];
                myTextField2.delegate = self;
                myTextField2.tag = indexPath.section;
                myTextField2.placeholder =  @"Confirmation mot passe"; 
                myTextField2.tag = indexPath.section;
                myTextField2.secureTextEntry = YES;
                myTextField2.autocorrectionType = UITextAutocorrectionTypeNo;
                myTextField2.keyboardType = UIKeyboardTypeDefault;
                myTextField2.autocapitalizationType= UITextAutocapitalizationTypeNone;
                myTextField2.borderStyle = UITextBorderStyleBezel;
                myTextField2.returnKeyType = UIReturnKeyDone;
                [myTextField2 addTarget:self action:@selector(textFieldDone:) 
                       forControlEvents:UIControlEventEditingDidEnd];
                
                
                [myTextField2 setText:@""];   
                
                [customCell.contentView addSubview:myTextField2];
            }
            
            cell = customCell;
            
            
            
            
            break;
            
        default:
            break;
			
	}
    
	// Set attributes common to all cell types.
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
	return cell;
}


- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DLog(@"");
    [aTableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

#pragma mark -
#pragma mark UITextFiedDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    DLog(@"");
	[textField resignFirstResponder];
	return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{  
    DLog(@"");
	[textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {  
    DLog(@"");
	[textField becomeFirstResponder];
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    DLog(@"");
    return NO;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    DLog(@"");
    return YES;
}

-(IBAction)textFieldDone:(id)sender
{
    DLog(@"");
    [sender resignFirstResponder];
    //  [self confirmChangePassword:sender];
    
}


#pragma mark -
#pragma mark NSURLConnection Callbacks
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    DLog(@"");
	
 	[indicator stopAnimating];	
	self.receivedData = nil;
    
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
	
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    DLog(@"");
    
    [indicator stopAnimating];	
	self.receivedData = nil;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Votre mot de passe a été modifié !"
                                                    message:@"Veuillez vous réidentifier !"
                                                   delegate:self
                                          cancelButtonTitle:@"Accepter"
                                          otherButtonTitles:nil];
    
    [alert show];
    
    
    
}



@end
