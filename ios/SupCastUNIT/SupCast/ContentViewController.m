//
//  ContentViewController.m
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "ContentViewController.h"
#import "GestionFichier.h"

#define kFontSize 20
#define FONT_SIZE_INDICATION 12


@implementation ContentViewController

// The designated initializer. Override to perform setup that is required before the view is loaded.


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	DLog(@"");

	
	labelTitre.text=titre;
	textView.textColor = [UIColor blackColor];
	textView.font = [UIFont fontWithName:kHelvetica	size:12];

	//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	DLog(@"index :  %d",indexItemContent);
	textView.text = [[contentList objectAtIndex:indexItemContent] objectForKey:@"Item 1"];
	UIImage * myImage = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease]
																  dataFilePathAtDirectory:self.dirRes
																  fileName:[[contentList
																			 objectAtIndex:indexItemContent]
																			objectForKey:@"imageName"]]];
	UIImage *resizedImage = [self resizeImage:myImage maxHeight:189.0f maxWidth:320.0f];
	imageView.image = resizedImage;
	
	
	//cell.imageView.image = [UIImage imageNamed:@"pagode-mini.gif"];
	
	//imageView.image = [UIImage	imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
	//																											 fileName:[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:@"imageName"]]];
	
	
	
    [super viewDidLoad];	
	
}

- (void)viewWillDisappear:(BOOL)animated {
	DLog(@"");

	[super viewWillDisappear:animated];
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	return [[[contentList objectAtIndex:indexItemContent] objectForKey:kAnswersChoice] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	
	cell.textLabel.text = [[[contentList objectAtIndex:indexItemContent] objectForKey:kAnswersChoice] objectAtIndex:indexPath.row];
	
	return cell;        	
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	
		NSString *selectedAnswer = [[[contentList objectAtIndex:indexItemContent] objectForKey:kAnswersChoice] objectAtIndex:indexPath.row];
		NSString *answer = [[contentList objectAtIndex:indexItemContent] objectForKey:kAnswer];
		NSString *alertString;
		
		if ([selectedAnswer isEqualToString:answer]) {
			
			alertRightAnswer = [[UIAlertView alloc] initWithTitle:@"" message:alertString
														 delegate:self cancelButtonTitle:kAccepter otherButtonTitles: nil];
			[alertRightAnswer show];	
			[alertRightAnswer release];
			
		}
	
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGSize cs;
	
	UIFont *captionFont = [UIFont boldSystemFontOfSize:kFontSize];
	cs = [[[[contentList objectAtIndex:indexItemContent] objectForKey:kAnswersChoice] objectAtIndex:indexPath.row] sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
	
	
	return cs.height + 20; // Add a space between each row
}

- (void)dealloc {
	[tableView release];
	[labelTitre release];
	[super dealloc];
}

@end
