//
//  BWItemViewController.m
//  SupCast
//
//  Created by admin on 22/11/10.
//  Copyright 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "BWItemViewController.h"


@implementation BWItemViewController
@synthesize dirRes;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil 
               bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil 
                           bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	DLog(@"BWviewDidLoad"); 
    [super viewDidLoad];
    [titleField becomeFirstResponder];
	
    
}

- (void)viewWillDisappear:(BOOL)animated
{
	DLog(@"BWviewWillDisappear"); 
	
    [titleField resignFirstResponder];
	NSDictionary * monDico = [[NSDictionary alloc] initWithObjectsAndKeys:									
	                          supportField.text,
                              kRessourceNomStr,
                              pathField.text,
                              kPathName,
                              linkField.text,
                              kRessourcebaseURL,
                              titleField.text,
                              kRessourceTitleStr,
                              nil];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kBWTitleChangedNotification object:nil
	 userInfo:monDico];

}

- (void)viewWillAppear:(BOOL)animated
{
	DLog(@"BWviewWillAppear"); 
    [titleField resignFirstResponder];
}


- (void)setViewTitle:(NSString *)newTitle
{
	DLog(@"BWsetViewTitle"); 
	
    [titleField setText:newTitle];
    [[self navigationItem] setTitle:newTitle];
}

- (void)setViewPath:(NSString *)newPath
{
	DLog(@"pathField"); 
	[pathField setText:newPath];
}

- (void)setViewLink:(NSString *)newLink
{
	DLog(@"linkField"); 
	[linkField setText:newLink];
}

- (void)setViewSupport:(NSString *)newSupport
{
	DLog(@"supportField"); 
	[supportField setText:newSupport];
}

- (void)setViewDomaine:(NSString *)newDomaine
{
	DLog(@"domaineField"); 
	[domaineField setText:newDomaine];
}

- (void)setViewCatalogue:(NSString *)newCatalogue
{
	DLog(@"catalogueField"); 
	[catalogueField setText:newCatalogue];
}

- (void)setViewEtablissement:(NSString *)newEtablissement
{
	DLog(@"etablissementField");
	[etablissementField setText:newEtablissement];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	DLog(@"BWtextFieldShouldReturn"); 
	
	[titleField resignFirstResponder];
	return NO;
}


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


- (void)didReceiveMemoryWarning {
	
	DLog(@"BWdidReceiveMemoryWarning"); 
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	DLog(@"BWviewDidUnload"); 
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	DLog(@"BWdealloc"); 
    [super dealloc];
}


@end
