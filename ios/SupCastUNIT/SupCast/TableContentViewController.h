//
//  TableContentViewController.h
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableContentsViewController.h"


@interface TableContentViewController : TableContentsViewController <UITableViewDelegate,UITableViewDataSource> {
	
}

- (void) updateNavBar;

@end
