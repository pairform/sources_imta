
#import <UIKit/UIKit.h>

/*!
 * @abstract Load data from a URL.
 *
 * This class asynchronously load data from a URL using NSURLConnection.
 */
@interface URLDataReceiver : NSObject {
	int fStatus;
	NSURL* fURL;
	NSMutableData* fReceived; /**< Received data */
	NSError* fError; /**< Last error if an error occurs during loading.*/
	id fDelegate;
	NSRecursiveLock* fLock;
	NSURLConnection* fConnection;
}

/*!
 * delegate must conform to URLDataReceiverDelegate protocol.
 */
- (id)initWithURL:(NSURL*)aURL delegate:(id)delegate;

/*!
 * @abstract Start loading data.
 */
- (void)startLoading;

/*!
 * @abstract Cancel data loading.
 *
 * It does nothing, if it is not currently loading.
 * Cacelling will reset the data that has been received.
 */
- (void)cancel;

/*!
 * @abstract return received data.
 *
 * Return nil if the loading failed or encountered any errors.
 */
- (NSData*)receivedData;


- (NSURL*)url;

/*!
 * @abstract Return last error.
 *
 * @note The return value can be nil, which indicates a successful load.
 */
- (NSError*)lastError;
@end


/*!
 * @abstract URLDataReceiverDelegate protocol.
 */
@interface NSObject(URLDataReceiverDelegate)
/*!
 * @abstract Notify delegate that loading has finished.
 *
 * The delegate can use lastError to query if there is any error.
 */
- (void)URLDataReceiverDidFinish:(URLDataReceiver*)dataReceiver;
@end
