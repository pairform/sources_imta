//
//  QPinyinVideoViewController.m
//  SupCast
//
//  Created by Phetsana on 23/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "GestionFichier.h"
#import "Movie.h"

@implementation QPinyinVideoViewController
//@synthesize dirRes;
@synthesize dicoRacine;
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
	[fondEcranImageView setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[self.dicoRacine objectForKey:kImageFondEcran]]]];
    
	prefix.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kPrefix];
	suffix.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kSuffix];
	activeField = textField;
	
	// Calcul de position
	prefix.frame = CGRectMake(prefix.frame.origin.x, 
							  indicationTableView.frame.origin.y + indicationTableView.frame.size.height + 10, 
							  prefix.frame.size.width, 
							  prefix.frame.size.height);
	
	suffix.frame = CGRectMake(suffix.frame.origin.x, 
							  indicationTableView.frame.origin.y + indicationTableView.frame.size.height + 10, 
							  suffix.frame.size.width, 
							  suffix.frame.size.height);
	
	textField.frame = CGRectMake(textField.frame.origin.x, 
								 indicationTableView.frame.origin.y + indicationTableView.frame.size.height + 10, 
								 textField.frame.size.width, 
								 textField.frame.size.height);
	
	mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, 
											prefix.frame.origin.y + prefix.frame.size.height + 10);
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)update {
	[super update];
	prefix.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kPrefix];
	suffix.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kSuffix];
	movie = [Movie alloc];
	movie.dirRes = self.dirRes;
	[movie initMoviePlayerWithName:[[questionList objectAtIndex:indexQuestion] objectForKey:kVideoName]
						 extension:[[questionList objectAtIndex:indexQuestion] objectForKey:kVideoExtension]];
	
	activeField.text = @"";
}

- (void)dealloc {
	[movie release];	
	[prefix release];
	[suffix release];
    [super dealloc];
}


@end
