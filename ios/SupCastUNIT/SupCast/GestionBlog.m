//
//  GestionBlog.m
//  SupCast
//
//  Created by Didier PUTMAN on 09/09/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionBlog.h"
#import "CommentViewCell.h"


@implementation GestionBlog
@synthesize elggid;
@synthesize username;
@synthesize password;
@synthesize idEcran;
@synthesize idBlog;
@synthesize categorie;
@synthesize receivedData;
@synthesize indicator;
@synthesize plist;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    
    connectionStatus = 0;
    
    NSString *urlConnexionListeBlog = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,krecupererListeBlogRessource];
    NSURL *urlListeBlog = [[NSURL alloc] initWithString:urlConnexionListeBlog];
    NSMutableURLRequest *requestListeBlog = [[NSMutableURLRequest alloc] initWithURL:urlListeBlog];
    [requestListeBlog setHTTPMethod:@"POST"];
    
    NSString *paramDataStringListeBlog = [NSString stringWithFormat:
                                          @"id=%@&idressource=%@&pseudo=%@&password=%@",
                                          kKey,
                                          self.elggid,
                                          self.username,
                                          self.password];
    
    NSData *paramDataListeBlog = [paramDataStringListeBlog dataUsingEncoding:NSUTF8StringEncoding];
    [requestListeBlog setHTTPBody:paramDataListeBlog];
    
 	connectionStatus = 1;
    [indicator startAnimating];
    [indicator setHidden:NO];
    [indicator setHidesWhenStopped:YES];
    
    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:requestListeBlog delegate:self];
    
    if(connection) {
        
        self.receivedData = [[NSMutableData alloc] init];
        
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                        message:@"Erreur de connexion"
                                                       delegate:self
                                              cancelButtonTitle:@"Accepter"
                                              otherButtonTitles:nil];
        
        [alert show];
        
    }
    
}

- (void)viewDidUnload
{
    [self setElggid:nil];
    [self setUsername:nil];
    [self setPassword:nil];
    [self setIdEcran:nil];
    [self setIdBlog:nil];
    [self setCategorie:nil];
    [self setReceivedData:nil];
    [self setIndicator:nil];
    [self setPlist:nil];
   
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


#pragma mark -
#pragma mark NSURLConnection Callbacks
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connectionStatus == 1)
        [self.receivedData appendData:data];
    if (connectionStatus == 2)
        [self.receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	
    [indicator stopAnimating];
    [indicator setHidden:YES];
    
	self.receivedData = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    DLog(@"");
    
    
    [indicator stopAnimating];
    [indicator setHidden:YES];
    
    if (connectionStatus == 1)
    {
        // traite self.receivedData
        DLog(@"");
        NSString *error; 
        NSPropertyListFormat format;
        self.plist = [[NSMutableArray alloc] init];    
        self.plist = [NSPropertyListSerialization propertyListFromData:self.receivedData
                                                      mutabilityOption:NSPropertyListImmutable
                                                                format:&format
                                                      errorDescription:&error]; 
        self.receivedData = nil;
        if(!self.plist)
        { 
            DLog(@"Error: %@",error); 
        }
        
        [aTableView reloadData];
    }
    
    if (connectionStatus == 2)
    {
        // creation blog
        NSString *error; 
        NSPropertyListFormat format;
        NSMutableArray *propertyList = [[NSMutableArray alloc] init];    
        propertyList = [NSPropertyListSerialization propertyListFromData:self.receivedData
                                                        mutabilityOption:NSPropertyListImmutable
                                                                  format:&format
                                                        errorDescription:&error]; 
        self.receivedData = nil;
        if(!plist)
        { 
            DLog(@"Error: %@",error); 
        }
        else
        {
            self.idBlog = [[propertyList objectAtIndex:0] objectForKey:@"blogid"];
/*            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[propertyList objectAtIndex:0] objectForKey:@"status"]
                                                            message:[[NSString alloc] initWithFormat:@"blog is %@ and idecran is %@ \n blog is %@ and idecran is %@",
                                                                     [[propertyList objectAtIndex:0] objectForKey:@"blogid"],
                                                                     [[propertyList objectAtIndex:0] objectForKey:@"idecran"],self.idBlog,self.idEcran]
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];*/
            
        }    
        
    }
    
}


#pragma mark -
#pragma mark Parser Methods



#pragma mark -
#pragma mark Table View Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView 
numberOfRowsInSection:(NSInteger)section {
    DLog(@"");
	return [self.plist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLog(@"");
    
	static NSString *TableIdentifier = @"TableIdentifier";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:TableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:TableIdentifier];
    }
	
	// Configuration de la cellule
    cell.accessoryView = nil;
    cell.accessoryType = UITableViewCellAccessoryNone;    
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.font = [UIFont fontWithName:kHelvetica size:16];
	cell.textLabel.numberOfLines = 0;
	NSUInteger row = [indexPath row];

    if (!self.plist)
    {
        DLog(@"plist vide");
    }
    else 
    {
        
        NSDictionary *message = [[NSDictionary alloc] initWithDictionary:[self.plist objectAtIndex:row]];
        
        NSString *monIdecran = [message objectForKey:@"idecran"];
        NSString *monGuidoe = [message objectForKey:@"guidoe"];
        NSString *monParent = [message objectForKey:@"parent"];
        NSString *monTitre = [message objectForKey:@"titre"];
        NSString *monGuid = [message objectForKey:@"guid"];
        NSString *monTitle = [message objectForKey:@"title"];
        NSString *monDescription = [message objectForKey:@"description"];
       
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.text = [[NSString alloc] initWithFormat:@"idecran : %@-guidoe : %@-parent : %@-titre : %@-guid : %@-title : %@-description : %@",monIdecran,monGuidoe,monParent,monTitre,monGuid,monTitle,monDescription];
    }
    
    return cell;
	
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    DLog(@"");
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-    (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSDictionary *message = [self.plist objectAtIndex:[indexPath row]];
	NSString *title = [[NSString alloc] initWithString:[message objectForKey:@"title"]];
    
	int sautDeLigne = 80;
	
	return [CommentViewCell cellHeightForCaption:title width:self.view.frame.size.width] + sautDeLigne;
	
}



@end
