//
//  ContentViewController.h
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContentsViewController.h"

@interface ContentViewController : ContentsViewController <UITableViewDelegate, UITableViewDataSource> {
	
}

@end
