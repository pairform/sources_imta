//
//  SaisieMotDePasse.h
//  SupCast
//
//  Created by Cape EMN on 16/01/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface SaisieMotDePasse : UIViewController
{
    
    NetworkStatus   statusConnexionReseau;
    IBOutlet UITextField *pseudoTextField;
    IBOutlet UITextField *motDePasseTextField;
    NSString *pseudoUtilisateur;
    NSString *motDePasseUtilisateur;
    UIActivityIndicatorView IBOutlet *indicator;
}
@end
