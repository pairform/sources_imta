//
//  BWItemViewController.h
//  SupCast
//
//  Created by admin on 22/11/10.
//  Copyright 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BWItemViewController : UIViewController { 
	IBOutlet UITextField *titleField;
	IBOutlet UITextField *supportField;
	IBOutlet UITextField *pathField;
	IBOutlet UITextField *linkField;
	IBOutlet UITextField *domaineField;
	IBOutlet UITextField *catalogueField;
	IBOutlet UITextField *etablissementField;

	NSString *dirRes;
	
}

@property (nonatomic, retain) NSString *dirRes;

- (void)setViewTitle:(NSString *)newTitle;
- (void)setViewPath:(NSString *)newPath;
- (void)setViewLink:(NSString *)newLink;
- (void)setViewSupport:(NSString *)newSupport;
- (void)setViewCatalogue:(NSString *)newCatalogue;
- (void)setViewDomaine:(NSString *)newDomaine;
- (void)setViewEtablissement:(NSString *)newEtablissement;

@end
