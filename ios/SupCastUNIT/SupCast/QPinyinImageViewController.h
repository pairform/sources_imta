//
//  QPinyinImageViewController.h
//  SupCast
//
//  Created by Phetsana on 26/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractPinyinViewController.h"

@interface QPinyinImageViewController : AbstractPinyinViewController {
	IBOutlet UIImageView *fondEcranImageView;
	IBOutlet UILabel *label;
	IBOutlet UIImageView *imageView;
	CGFloat height;
	NSDictionary *dicoRacine;
}

@property (nonatomic, retain) NSDictionary *dicoRacine;

//-(NSString *)dataFilePath:(NSString *)fileName;

@end
