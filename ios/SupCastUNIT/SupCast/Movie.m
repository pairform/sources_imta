//
//  Movie.m
//  SupCast
//
//  Created by Phetsana on 19/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "GestionFichier.h"
#import "Movie.h"


@implementation Movie
@synthesize dirRes;
#pragma mark Movie Player Routines

/*
-(NSString *)dataFilePath:(NSString *)fileName 
{    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}
*/ 

- (id)initMoviePlayerWithName:(NSString *)videoName 
					extension:(NSString *)videoExtension {
    /*
	 
	 Now create a MPMoviePlayerController object using the movie file provided in our bundle.
	 
	 The MPMoviePlayerController class supports any movie or audio files that already play 
	 correctly on an iPod or iPhone. For movie files, this typically means files with the extensions 
	 .mov, .mp4, .mpv, and .3gp and using one of the following compression standards:
	 
	 - H.264 Baseline Profile Level 3.0 video, up to 640 x 480 at 30 fps. Note that B frames 
	 are not supported in the Baseline profile.
	 
	 - MPEG-4 Part 2 video (Simple Profile)
	 
	 If you use this class to play audio files, it displays a black screen while the audio plays. For 
	 audio files, this class class supports AAC-LC audio at up to 48 kHz.
	 
	 */
	
	if (videoName != nil) {
		mMoviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[self movieURLWithName:videoName extension:videoExtension]];
		mMoviePlayer.scalingMode = MPMovieScalingModeAspectFit;
	}
	
	
    /* Set movie player settings (scaling, controller type and background color) to the currently set values
	 as specified in the Settings application */
	
    /* 
	 Movie scaling mode can be one of: MPMovieScalingModeNone, MPMovieScalingModeAspectFit,
	 MPMovieScalingModeAspectFill, MPMovieScalingModeFill.
	 */
	
	
	return self;
}


- (id)initMoviePlayerWithFullName:(NSString *)videoName {
    /*
	 
	 Now create a MPMoviePlayerController object using the movie file provided in our bundle.
	 
	 The MPMoviePlayerController class supports any movie or audio files that already play 
	 correctly on an iPod or iPhone. For movie files, this typically means files with the extensions 
	 .mov, .mp4, .mpv, and .3gp and using one of the following compression standards:
	 
	 - H.264 Baseline Profile Level 3.0 video, up to 640 x 480 at 30 fps. Note that B frames 
	 are not supported in the Baseline profile.
	 
	 - MPEG-4 Part 2 video (Simple Profile)
	 
	 If you use this class to play audio files, it displays a black screen while the audio plays. For 
	 audio files, this class class supports AAC-LC audio at up to 48 kHz.
	 
	 */
	
	if (videoName != nil) {
		mMoviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[self movieURLWithFullName:videoName]];
		mMoviePlayer.scalingMode = MPMovieScalingModeAspectFit;
	}
	
	
    /* Set movie player settings (scaling, controller type and background color) to the currently set values
	 as specified in the Settings application */
	
    /* 
	 Movie scaling mode can be one of: MPMovieScalingModeNone, MPMovieScalingModeAspectFit,
	 MPMovieScalingModeAspectFill, MPMovieScalingModeFill.
	 */
	
	
	return self;
}


- (void)playMovie {
    /*
	 
     As soon as you call the play: method, the player initiates a transition that fades 
     the screen from your current window content to the designated background 
     color of the player. If the movie cannot begin playing immediately, the player 
     object continues displaying the background color and may also display a progress 
     indicator to let the user know the movie is loading. When playback finishes, the 
     player uses another fade effect to transition back to your window content.
     
     */
	
    [mMoviePlayer play];
}


- (NSURL *)movieURLWithName:(NSString *)name extension:(NSString *)ext {
	
	if (mMovieURL == nil) {
        NSBundle *bundle = [NSBundle mainBundle];
        if (bundle) {
            NSString *moviePath = [bundle pathForResource:name ofType:ext];
            if (moviePath) {
                mMovieURL = [NSURL fileURLWithPath:moviePath];
                [mMovieURL retain];
            }
			else {
				
				//NSString * myPath =[[NSBundle mainBundle] resourcePath];
				//NSString *path = [myPath stringByAppendingPathComponent:[name stringByAppendingPathExtension:ext]];
				NSString * path = [[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[name stringByAppendingPathExtension:ext]];
				mMovieURL = [NSURL fileURLWithPath:path];
                [mMovieURL retain];
				
				
			}
			
        }
    }
    
    return mMovieURL;
}

- (NSURL *)movieURLWithFullName:(NSString *)name {
	
	if (mMovieURL == nil) {
        NSBundle *bundle = [NSBundle mainBundle];
        if (bundle) {
            NSString *moviePath = [bundle pathForResource:name ofType:[name pathExtension]];
            if (moviePath) {
                mMovieURL = [NSURL fileURLWithPath:moviePath];
                [mMovieURL retain];
            }
			else {
				
				//NSString * myPath =[[NSBundle mainBundle] resourcePath];
				//NSString *path = [myPath stringByAppendingPathComponent:[name stringByAppendingPathExtension:ext]];
				NSString * path = [[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:name];
				mMovieURL = [NSURL fileURLWithPath:path];
                [mMovieURL retain];
				
				
			}
			
        }
    }
    
    return mMovieURL;
}

- (void)dealloc {
	[mMoviePlayer release];
	[mMovieURL release];
	[super dealloc];
}

@end
