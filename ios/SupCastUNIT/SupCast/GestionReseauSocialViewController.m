//
//  GestionReseauSocialViewController.m
//  SupCast
//
//  Created by Didier PUTMAN on 02/09/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionReseauSocialViewController.h"
#import "UtilsCore.h"

#define INTERESTING_TAG_NAMES @"ObjectEntity",@"idecran", @"LastMessage", @"nombreNouveauMessage", nil

@implementation GestionReseauSocialViewController

// infos communes

@synthesize connectionNiveauRessource, receivedDataNiveauRessource, plistNiveauRessource;
@synthesize estVisiteur;
@synthesize titre;
@synthesize username;
@synthesize titreParent;
@synthesize idEcranParent;
@synthesize idEcran;
@synthesize idBlog;
@synthesize password;
@synthesize categorie;
@synthesize ressourceInfo;
@synthesize pcrDictionary;
// connections
@synthesize connectionGetBlog;
@synthesize connectionNewMessages;
// données recues
@synthesize nvxMessage;
@synthesize receivedDataGetBlog;
@synthesize receivedDataNewMessages;

@synthesize dicoNombreMessages;

#pragma mark -
#pragma mark Gestion des nouveaux messages

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}
#pragma mark -
#pragma mark init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    DLog(@"");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    
    self.navigationController.navigationItem.hidesBackButton = NO;
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.rowHeight = kCustomRowHeight;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    refreshControl.tintColor = [UIColor colorWithRed:0.447 green:0.49 blue:0.969 alpha:1]; /*#727df7*/
    [refreshControl addTarget:self action:@selector(askForRefresh) forControlEvents:UIControlEventValueChanged];
    refreshControl.tag = 100;
    [tableView addSubview:refreshControl];

}

-(void)askForRefresh{
    [self recupNombreMessages];
    [tableView reloadData];

    [(UIRefreshControl*)[tableView viewWithTag:100] endRefreshing];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    //Recupération du nombre de nouveaux messages avec l'UtilsCore
    [self recupNombreMessages];
    [tableView reloadData];
}
#pragma mark -
#pragma mark Gestion des messages

-(void)afficherControlsMessage:(NSString *)urlPage {
    DLog(@"");
    
    
    [self setUrlPage:urlPage];
    
    //On va créer un rectangle transparent, que l'on va mettre comme background pour la toolbar
    //Solution pour iOS 5+ dite "propre"
    
    CGRect rect = CGRectMake(0,0,102,44.01);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context,rect);
    UIImage *transparentImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
    
    //On l'applique à la toolbar
    
    [tools setBackgroundImage:transparentImage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    /*
     tools.barStyle = UIBarStyleBlack;
     tools.translucent = YES;
     tools.opaque = NO;
     tools.backgroundColor = [UIColor clearColor];
     */
    
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    
    //Création du bouton stylo
    UIImage *buttonImage = [UIImage imageNamed:kStylo];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setImage:buttonImage forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    aButton.showsTouchWhenHighlighted = YES;
    
    // Set the Target and Action pour le bouton stylo
    [aButton addTarget:self action:@selector(ajouterMessage) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    
    
    UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage * image;
    
    NSString * nombreMessages = [[dicoNombreMessages objectForKey:urlPage] stringValue];
    
    //si pas de nouveau message bulle normale
    if ([nombreMessages isEqualToString:@"0"] || [nombreMessages isEqualToString:@""] || !(nombreMessages)  )
    {
        image = [UIImage imageNamed:kBulleBarre];
        [aButton1 setTitle:@"" forState:UIControlStateNormal];
    } 
    else    
    {
        //si nouveaux messages bulle rouge
        image = [UIImage imageNamed:kBulleBarreRed];
        [aButton1 setTitle:nombreMessages forState:UIControlStateNormal];
        [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        aButton1.titleLabel.font=[UIFont systemFontOfSize:13];
    }
    
    CGRect frame = CGRectMake(5.0, 0.0, 32.0, 32.0);            
    [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
    aButton1.frame=frame;
    aButton1.showsTouchWhenHighlighted = YES;
    [aButton1 addTarget:self action:@selector(lireMessages) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithCustomView:aButton1];
    //[ajouterMessage2 setStyle:UIBarStyleBlackOpaque];
    
    [buttons addObject:ajouterMessage];
    [buttons addObject:ajouterMessage2];
    
    [tools setItems:buttons animated:NO];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];

    
    [tableView reloadData];
}

-(void)recupNombreMessages{
    DLog(@"");
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:[self.ressourceInfo objectForKey:kRessourceElggidStr], @"id_ressource", nil];
    
    [self post:[NSString stringWithFormat:@"%@/webServices/messages/recupererNombreMessages.php",sc_server]
              param:params
              callback:^(NSDictionary *wsReturn) {
                  self.dicoNombreMessages = wsReturn;
                  [tableView reloadData];
              }];
}

-(void)recupNombreMessages:(void (^)(NSDictionary * wsReturn)) callback{
    DLog(@"");
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:[self.ressourceInfo objectForKey:kRessourceElggidStr], @"id_ressource", nil];
    
    [self post:[NSString stringWithFormat:@"%@/webServices/messages/recupererNombreMessages.php",sc_server]
              param: params
              callback: callback];
}

-(void)recupMessages{
}

-(IBAction)lireMessages{
	DLog(@"");
    
    [UtilsCore rafraichirMessages];
    
    RecupererMessagesViewController *recupererMessagesViewController = [[RecupererMessagesViewController alloc] initWithNibName:@"RecupererMessagesView" bundle:nil];
    
    recupererMessagesViewController.titreParent   = self.titreParent;
    recupererMessagesViewController.idEcranParent = self.idEcranParent;
    recupererMessagesViewController.idEcran       = self.idEcran;
    recupererMessagesViewController.idBlog        = self.idBlog;
    recupererMessagesViewController.titre         = self.titre;
    recupererMessagesViewController.username      = self.username;
    recupererMessagesViewController.ressourceInfo = self.ressourceInfo;
    recupererMessagesViewController.password      = self.password;
    recupererMessagesViewController.categorie     = self.categorie;
   
    [recupererMessagesViewController setIdRessource:[self.ressourceInfo objectForKey:kRessourceElggidStr]];
    [recupererMessagesViewController setUrlPage: [self urlPage]];
    
    [self.navigationController pushViewController:recupererMessagesViewController
                                         animated:YES];
    
	
}

-(IBAction)ajouterMessage {
    DLog(@"");
    AddCommentViewController *addCommentViewController = [AddCommentViewController alloc];
    
    addCommentViewController.titreParent   = self.titreParent;
    addCommentViewController.idEcranParent   = self.idEcranParent;
    addCommentViewController.idEcran   = self.idEcran;
    addCommentViewController.idBlog    = self.idBlog;
    addCommentViewController.titre     = self.titre;
    addCommentViewController.username  = self.username;
    addCommentViewController.ressourceInfo = self.ressourceInfo;
    addCommentViewController.password  = self.password;
    addCommentViewController.categorie = self.categorie;
    addCommentViewController.urlPage = self.urlPage;
    
    (void)[addCommentViewController initWithNibName:@"AddCommentView"
                                       bundle:nil];
    [self.navigationController pushViewController:addCommentViewController 
                                         animated:YES];
}
-(int)trouverMessagesDansMenu:(id)menuItem messagesToFind:(NSDictionary*)dico{
    NSMutableString* count = [[NSMutableString alloc] initWithString:@"0"];
    [self trouverMessagesDansMenu:menuItem count:count messagesToFind:dico depth:0 parent:nil];
    return [count intValue];
}

-(void)trouverMessagesDansMenu:(id)menuItem count:(NSMutableString*)count messagesToFind:(NSDictionary*)dico depth:(int)depth parent:(id)parent{
    
    if([menuItem isKindOfClass:[NSDictionary class]])
    {
        if([menuItem objectForKey:@"sequenceContent"])
        {
            NSArray* child = [menuItem objectForKey:@"sequenceContent"];
            [self trouverMessagesDansMenu:child count:count messagesToFind:dico depth:depth+1 parent:menuItem];
        }
        else{
            //L'objet n'a pas de fils
            NSString * sequenceURL = [[menuItem objectForKey:@"referenceFichier"] stringByDeletingPathExtension];
            if ([dico objectForKey:sequenceURL]) {
                NSNumber * countInt = [[NSNumber alloc] initWithInt:[[dico objectForKey:sequenceURL] intValue] + [count intValue]];
                [count setString:[countInt stringValue]];
            }
        }
    }
    else if([menuItem isKindOfClass:[NSArray class]])
    {
        for (NSDictionary* child in menuItem) {
            [self trouverMessagesDansMenu:child count:count messagesToFind:dico depth:depth+1 parent:menuItem];
        }
    }
}


@end
