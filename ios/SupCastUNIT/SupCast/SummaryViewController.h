//
//  SummaryViewController.h
//  SupCast
//
//  Created by admin on 24/11/10.
//  Copyright 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaViewController.h"


@interface SummaryViewController : UITableViewController {
	MediaViewController *aMediaViewController;
	NSArray *donneesFichier;
	NSString * mySupport;
	
}

@property (nonatomic, retain) NSString * mySupport;
@property (nonatomic, retain) NSArray *donneesFichier;

- (id)initWithNibName:(NSString *)nibNameOrNil
			   bundle:(NSBundle *)nibBundleOrNil
		   nomSupport:(NSString *)nomSupport;

@end