//
//  ContentsViewController.m
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "ContentsViewController.h"
#import "GestionFichier.h"

@implementation ContentsViewController
@synthesize dirRes;
@synthesize titre;
@synthesize contentList;
@synthesize indication;
+ (NSMutableArray *)sortArray:(NSMutableArray *)arrayToSort {
	NSMutableArray *array = [[NSMutableArray alloc] init];
	NSMutableArray *arrayCopy = [[NSMutableArray alloc] init];
	
	for (NSString *elem in arrayToSort) {
		[arrayCopy addObject:elem];
	}
	
	while ([array count] != [arrayToSort count]) {
		int rNumber = rand() % [arrayCopy count];
		[array addObject:[arrayCopy objectAtIndex:rNumber]];
		[arrayCopy removeObjectAtIndex:rNumber];
	}
	
	[arrayCopy release];
	
	return [array autorelease];
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil
{
   	DLog(@"");
	
	self = [super initWithNibName:nibNameOrNil 
                           bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
		self.title = @"";
    }
	
    return self;
}

- (void)segmentAction:(id)sender {
	
	DLog(@"");
	
	if ([sender selectedSegmentIndex] == 0)
		[self previousQuestion];
	else if ([sender selectedSegmentIndex] == 1)
		[self nextQuestion];
	[tableView reloadData];
	
	
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	DLog(@"");
	
    [super viewDidLoad];
	textView.text = [[contentList objectAtIndex:indexItemContent] objectForKey:@"Item 1"];
	// Configuration du scroll
	[mainScrollView setCanCancelContentTouches:NO];
	mainScrollView.clipsToBounds = YES;	
	mainScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	[mainScrollView addSubview:mainView];
	[mainScrollView setContentSize:CGSizeMake(mainView.frame.size.width, mainView.frame.size.height)];
	[mainScrollView setScrollEnabled:YES];
	[mainScrollView release];
	
	
	// On implémente un composant segment qui va servir pour le suivant et précédent
	NSArray *buttonNames = [NSArray arrayWithObjects:@"<", @">", nil]; 
	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] 
											initWithItems:buttonNames]; 
	segmentedControl.momentary= YES; 
	
	// Configuration du segment
	segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth; 
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar; 
	segmentedControl.tintColor = [UIColor blackColor];
	segmentedControl.frame = CGRectMake(0, 0, 60, 30); 
	[segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged]; 
	
	// On ajoute le segment a une vue
	UIView *segmentedControlView = [[UIView alloc] init];
	segmentedControlView.frame = segmentedControl.frame;
	segmentedControl.backgroundColor = [UIColor clearColor];
	[segmentedControlView addSubview:segmentedControl];
	
	// On ajoute la vue précédemment crée à la barre de navigation
	UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControlView];
	self.navigationItem.rightBarButtonItem = buttonItem;
	[buttonItem release];
	[segmentedControl release]; 
	[segmentedControlView release];
	
	
	indexItemContent = 0; // Initialisation de l'index correspondant à la question courante
	
}

- (void)viewWillDisappear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)previousQuestion {
	
	DLog(@"");
	if (indexItemContent == 0)
		indexItemContent = [contentList count] - 1;
	else 
		indexItemContent = (indexItemContent - 1) % [contentList count];
	DLog(@"ttttt : %d",[contentList count]);
	DLog(@"count : %d",indexItemContent);
	textView.text = [[contentList objectAtIndex:indexItemContent] objectForKey:@"Item 1"];
	UIImage * myImage = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease]
														  dataFilePathAtDirectory:self.dirRes
														  fileName:[[contentList
																	 objectAtIndex:indexItemContent]
																	objectForKey:@"imageName"]]];
	UIImage *resizedImage = [self resizeImage:myImage maxHeight:189.0f maxWidth:320.0f];
	imageView.image = resizedImage;
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.view cache:YES];
	[UIView commitAnimations];
	
}

- (void)nextQuestion {
	DLog(@"");
	
	indexItemContent = (indexItemContent + 1) % [contentList count];
	
	DLog(@"ttttt : %d",[contentList count]);
	DLog(@"count : %d",indexItemContent);
	textView.text = [[contentList objectAtIndex:indexItemContent] objectForKey:@"Item 1"];
	
	
	
	
	
	UIImage * myImage = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease]
														  dataFilePathAtDirectory:self.dirRes
														  fileName:[[contentList
																	 objectAtIndex:indexItemContent]
																	objectForKey:@"imageName"]]];
	
	
	UIImage *resizedImage = [self resizeImage:myImage maxHeight:189.0f maxWidth:320.0f];
	imageView.image = resizedImage;
	
	//[imageView.image drawInRect:imageView.frame blendMode:kCGBlendModeNormal alpha:1];
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view cache:YES];
	[UIView commitAnimations];
}

- (void)dealloc {
	DLog(@"");
	[contentList release];
	[indication release];
	[toolBar release];
	
    [dirRes release];
	[super dealloc];
	
}

- (UIImage *)resizeImage:(UIImage *)sourceImage maxHeight:(CGFloat)maxHeight maxWidth:(CGFloat)maxWidth {
	CGFloat width = sourceImage.size.width;
	CGFloat height = sourceImage.size.height;
	
	if (width < maxWidth && height < maxHeight) {
		return sourceImage;
	}
	
	CGFloat ratio = width / height;
	
	if (width > maxWidth) 
	{
		width = maxWidth;
		height = width / ratio;
	}
	
	if (height > maxHeight)  
	{
		height = maxHeight;
		width = height * ratio;
	}
	
	CGSize size = CGSizeMake(width, height);
	
	UIGraphicsBeginImageContext(size);
	[sourceImage drawInRect:CGRectMake(0.0f, 0.0f, width, height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return newImage;
}
- (UIImage *)resizeImage:(UIImage *)sourceImage max:(CGFloat)maximum {
	CGFloat width = sourceImage.size.width;
	CGFloat height = sourceImage.size.height;
	
	if (width < maximum && height < maximum) {
		return sourceImage;
	}
	
	CGFloat ratio = width / height;
	
	if (width > height) {
		width = maximum;
		height = width / ratio;
	} else {
		height = maximum;
		width = height * ratio;
	}
	
	CGSize size = CGSizeMake(width, height);
	
	UIGraphicsBeginImageContext(size);
	[sourceImage drawInRect:CGRectMake(0.0f, 0.0f, width, height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
	return newImage;
}

@end
