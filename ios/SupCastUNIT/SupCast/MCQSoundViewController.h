//
//  MCQSoundViewController.h
//  SupCast
//
//  Created by Phetsana on 29/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractMCQExerciceViewController.h"
#import "AVFoundation/AVAudioPlayer.h"

@class MCQSoundFlipsideViewController;

@interface MCQSoundViewController : AbstractMCQExerciceViewController {
	IBOutlet UIImageView *fondEcranImageView;
	IBOutlet UIBarButtonItem *barButtonItem;
	IBOutlet UIToolbar *aToolBar;
	IBOutlet UILabel *questionNumber;
	NSDictionary *examples;
	MCQSoundFlipsideViewController *flipsideViewController;
	IBOutlet UITableView *indicationTableView;
	CGFloat height1;
	CGFloat height2;
	AVAudioPlayer *player;
	NSDictionary *dicoRacine;
}

@property (nonatomic, retain) NSDictionary *dicoRacine;
@property (nonatomic, retain) NSDictionary *examples;

@property (nonatomic, retain) AVAudioPlayer *player;

//-(NSString *)dataFilePath:(NSString *)fileName;


- (IBAction)playSound;
- (IBAction)showExamples;

@end
