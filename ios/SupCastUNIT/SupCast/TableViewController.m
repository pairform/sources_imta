//
//  TableViewController.m
//  SupCast
//
//  Created by Phetsana on 19/08/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "GestionFichier.h"
#import "TableViewController.h"
#import "AddCommentViewController.h"
#import "RecupererMessagesViewController.h"

#define kSectionHeaderHeight 40

@implementation TableViewController

@synthesize labelTitle;
@synthesize array;
@synthesize idecran;
@synthesize ecranPrecedent;
@synthesize dirRes;
@synthesize dicoRacine;


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil
			   bundle:(NSBundle *)nibBundleOrNil
				title:(NSString *)titreItem
				array:(NSArray *)a {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
		self.titre = titreItem;
		array = a;
    }
    return self;
}

- (void)viewDidLoad {

    [super viewDidLoad];	  
	labelTitle.text = self.titre;
	tableView.backgroundColor = [UIColor clearColor];
	tableView.allowsSelection = NO;
	
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [array count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return [[array objectAtIndex:section] objectForKey:kSectionName];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[[array objectAtIndex:section] objectForKey:kItems] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TextVideoCell";
	
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier] autorelease];
    }
	
	cell.textLabel.font = [UIFont fontWithName:kHelvetica size:16];
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.text = [[[array objectAtIndex:indexPath.section] objectForKey:kItems] objectAtIndex:indexPath.row];

	
	return cell;        	
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat result;
	
	UIFont *captionFont = [UIFont fontWithName:kHelvetica size:16];
	CGSize cs = [[[[array objectAtIndex:indexPath.section] objectForKey:kItems] objectAtIndex:indexPath.row] sizeWithFont:captionFont 
																										  constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
																											  lineBreakMode:UILineBreakModeWordWrap];
	result = cs.height + 40; 
	
	return result;
}

-(CGFloat)tableView:(UITableView *)tableViewHeader heightForHeaderInSection:(NSInteger)section {
	
	if([self tableView:tableViewHeader titleForHeaderInSection:section] != nil) {
		return kSectionHeaderHeight;
	}
	else {
		return 0;
	}
		
}
		

-(UIView *)tableView:(UITableView *)tableViewHeader viewForHeaderInSection:(NSInteger)section {
	
	NSString *sectionTitle = [self tableView:tableViewHeader titleForHeaderInSection:section];
	
	if(sectionTitle == nil)
		return nil;
	
	//Créer étiquette avec le titre
	UILabel *label = [[[UILabel alloc] init] autorelease];
	label.frame = CGRectMake(20,6,300,30);
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor whiteColor];
	label.shadowColor = [UIColor clearColor];
	label.shadowOffset = CGSizeMake(0.0, 1.0);
	label.font =[UIFont boldSystemFontOfSize:16];
	label.text = sectionTitle;
	
	//Créer en-tête
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,kSectionHeaderHeight)];
	[view autorelease];
	[view addSubview:label];
	
	return view;

}
	
#pragma mark -

- (void)dealloc {
	[labelTitle release];
	[array release];
	[tableView release];
	[username release];
	[password release];
	[categorie release];
	[ecranPrecedent release];
    [super dealloc];
}


@end