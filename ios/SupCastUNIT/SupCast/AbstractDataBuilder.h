//
//  AbstractDataBuilder.h
//  SupCast
//
//  Created by Phetsana on 15/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <Foundation/Foundation.h>

/* Classe abstraite du patron de conception builder */

@interface AbstractDataBuilder : NSObject {

}

- (NSArray *)buildData;

@end
