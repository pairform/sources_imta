//
//  Utils.h
//  testcoredate
//
//  Created by admin on 22/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SupCastAppDelegate.h"
#import "Message.h"

@interface UtilsCore : NSObject {
    NSFetchedResultsController *fetchedResultsController;
    NSManagedObjectContext *managedObjectContext;
}

+ (void) sauvegardeImage:(NSNumber *)id_utilisateur:(NSString *)url;

+ (UIImage *) getImage:(NSNumber *)id_utilisateur;

+ (void) rafraichirMessages;

+ (void) rafraichirMessagesFromRessource:(NSNumber *)id_ressource;

+ (void) rafraichirMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page;

+ (void) rafraichirMessagesFromUser:(NSNumber *)owner_guid;

+ (Message *) getMessage:(NSNumber *)id_message;

+ (NSArray * ) getReponsesFromMessage:(NSNumber *)id_message;

+ (NSArray * ) getMessagesFromUser:(NSNumber *)owner_guid;

+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource;

+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page;

+ (NSArray * ) getAllMessages;

+ (NSFetchRequest *) getMessageEntityFetch;

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end
