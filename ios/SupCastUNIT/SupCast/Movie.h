//
//  Movie.h
//  SupCast
//
//  Created by Phetsana on 19/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface Movie : NSObject {
	MPMoviePlayerController *mMoviePlayer;
	NSURL *mMovieURL;
	NSString *dirRes;
}
@property (nonatomic, retain) NSString *dirRes;

- (NSURL *)movieURLWithName:(NSString *)name 
				  extension:(NSString *)ext;
- (id)initMoviePlayerWithName:(NSString *)videoName 
					extension:(NSString *)videoExtension;
- (id)initMoviePlayerWithFullName:(NSString *)videoName;
- (NSURL *)movieURLWithFullName:(NSString *)name;

- (void)playMovie;
//-(NSString *)dataFilePath:(NSString *)fileName;

@end
