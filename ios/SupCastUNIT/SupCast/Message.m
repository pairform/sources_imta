//
//  User.m
//  profil
//
//  Created by admin on 24/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic time_created;
@dynamic time_updated;
@dynamic id_message;
@dynamic id_ressource;
@dynamic nom_page;
@dynamic nom_tag;
@dynamic num_occurence;
@dynamic owner_guid;
@dynamic owner_username;
@dynamic reponses;
@dynamic utilite;
@dynamic value;
@dynamic tags;
@dynamic estDefi;
@dynamic estReponse;
@dynamic medaille;
@dynamic owner_rank_name;
@dynamic owner_rank_id;
@dynamic supprime_par;
@dynamic user_a_vote;
@end
