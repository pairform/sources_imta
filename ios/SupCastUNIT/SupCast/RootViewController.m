//
//  RootViewController.m
//  SupCast
//
//  Created by Didier PUTMAN on 27/05/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "RootViewController.h"
#import "SupCastAppDelegate.h"
#include <sys/xattr.h>

#define TAG_SPIN_CELL  14

@implementation RootViewController

@synthesize rootConnection;
@synthesize statusConnexionReseau;
@synthesize username;
@synthesize ancienNiveau;
@synthesize nouveauNiveau;
@synthesize password;
@synthesize estVisiteur;
@synthesize receivedData;
@synthesize plist;
@synthesize HUD;
@synthesize guid, receivedDataNewMessages, connectionNewMessages,nbreMessParRessource;


#pragma mark -
#pragma mark Reachability

-(NetworkStatus) etatConnexionReseau {
    self.statusConnexionReseau = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    return self.statusConnexionReseau;
}


- (BOOL)connected 
{
    if (NotReachable== (BOOL)[self etatConnexionReseau])
        return NO;    
    else
        return YES;    
}

#pragma mark -
#pragma mark View life cycle

- (void)viewDidLoad
{
    DLog(@"");
    [TestFlight passCheckpoint:@"Ecran d'accueil de la ressource"];
    
    self.navigationItem.title = @"SupCast";
    aTableView.backgroundColor = [UIColor clearColor];
	aTableView.backgroundView = nil;
    aTableView.rowHeight = kCustomRowHeight;
    
    currentIndex = 0;
    
    ////////////////
    UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
    tools.barStyle = UIBarStyleBlackOpaque;
    tools.tintColor = [UIColor colorWithRed:1.0 green:0.6 blue:0.0 alpha:0.6];
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(goApp)];
    UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
    ajouterMessage2.tintColor = [UIColor colorWithRed:1.0 green:0.6 blue:0.0 alpha:0.6];
    
    [buttons addObject:ajouterMessage2];
    [buttons addObject:ajouterMessage];
    [tools setItems:buttons animated:NO];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
    ////////////////
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(titleDownloadFinishNotification:)
												 name:kTitleDownloadFinishNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(listeSupportChangedNotification:)
												 name:kListeSupportChangedNotification
											   object:nil];
	
	
    
	//[appButtonItem release];
    [self recuparationListeApplication];
    
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        
        //TODONE:Vérifier qu'on récupère bien le tableau
        
        NSArray * infos = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"arrayInfo"] objectAtIndex:1];
        StyledPullableView * pullUpView = [[StyledPullableView alloc] initWithFrameAndTextArray:CGRectMake(0, 0, 320, 460):infos];
        [self.view addSubview:pullUpView];
    }
	[super viewDidLoad];
    
    
}

-(void)editAction
{
    DLog(@"");
    [aTableView setEditing:YES animated:YES];
    ////////////////
    UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
    tools.barStyle = UIBarStyleBlackOpaque;
    tools.tintColor = [UIColor colorWithRed:1.0 green:0.6 blue:0.0 alpha:0.6];
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    
    [buttons addObject:ajouterMessage2];
    [tools setItems:buttons animated:NO];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
    ////////////////
    
}
-(void)doneAction
{
    DLog(@"");
    [aTableView setEditing:NO animated:YES];
    ////////////////
    UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
    tools.barStyle = UIBarStyleBlackOpaque;
    tools.tintColor = [UIColor colorWithRed:1.0 green:0.6 blue:0.0 alpha:0.6];
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(goApp)];
    UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
    
    [buttons addObject:ajouterMessage2];
    [buttons addObject:ajouterMessage];
    [tools setItems:buttons animated:NO];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
    ////////////////
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [UtilsCore rafraichirMessages];
}
- (void)viewWillAppear:(BOOL)animated
{
    DLog(@"");
    
    //On récupère l'éventuelle notif de progression de téléchargement envoyée par Telechargement.m
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePercentageNotification:)
                                                 name:@"Progression"
                                               object:nil];
    
    //On va créer un rectangle transparent, que l'on va mettre comme background pour la toolbar
    //Solution pour iOS 5+ dite "propre"
    
    CGRect rect = CGRectMake(0,0,102,44.01);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context,rect);
    UIImage *transparentImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
    
    //On l'applique à la toolbar
    
    [tools setBackgroundImage:transparentImage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    /*
     tools.barStyle = UIBarStyleBlack;
     tools.translucent = YES;
     tools.opaque = NO;
     tools.backgroundColor = [UIColor clearColor];
     */

    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(goApp)];
    [ajouterMessage setTintColor:[UIColor blackColor]];
    UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
    
    ajouterMessage2.tintColor = [UIColor colorWithRed:1.0 green:0.6 blue:0.0 alpha:0.6];
    
    [buttons addObject:ajouterMessage2];
    [buttons addObject:ajouterMessage];
    [tools setItems:buttons animated:NO];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
    ////////////////
    [self recuparationListeApplication];
    
    [self rechercheRessourcesZippees];  
    

    [self requeteNvxMessages];
    
    [aTableView reloadData];

    
    [super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{    DLog(@"");
    [self hideLoadingProgress];
    [self cancelConnection];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Progression" object:nil];
	[super viewWillDisappear:animated];
}
- (void)cancelConnection {
	DLog(@"cancelConnection");
	if (self.rootConnection) {
		[self.rootConnection cancel];
		self.rootConnection = nil;
	}
}


- (void)viewDidDisappear:(BOOL)animated
{
    DLog(@"");
	[super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    DLog(@"");
    aTableView = nil;
    boutonAjouterRessouces = nil;
    self.HUD = nil;
    self.rootConnection = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kTitleDownloadFinishNotification
                                                  object:nil];
    
    
    [super viewDidUnload];
    
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

#pragma mark -
#pragma mark Table view methods

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    DLog(@"");
    
    return 1;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	
	DLog(@"");
    
	// ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    
    NSArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	// Vérification de l'existence du fichier 
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
    NSDictionary * monDico = [titles objectAtIndex:indexPath.row];
	
	int nodeCount = [titles count];
	if (nodeCount > 0)
	{
        
        [self telechargement:monDico];
        
	}
    
	
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	DLog(@"");
    
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
    
	int count = [titles count];
	
	// if there's no data yet, return enough rows to fill the screen
	if (count == 0)
    {
        boutonAjouterRessouces.hidden = NO;
    }
    else
    {
        boutonAjouterRessouces.hidden = YES;
    }       
    
	return count;
}

//Vérifie si la ressource a été updaté
- (NSDictionary *)verif:(NSDictionary *)monDico 
{
    
    // Si le tableau est vide initialisation avec la nouvelle reference
    // sinon recherche de la presence de la reference de le tableau
    
    if (self.plist == nil || ([self.plist count] == 0)) 
    {
        return nil;
    }
    else
    {
        // Pour chaque ressource dans la liste des ressources de la BDD
        // pList téléchargé depuis elgg_iphone/mod/blog/actions/ListeApplications
        for (NSDictionary *monDicoRef in  self.plist)
        {
            NSString * idEnBase = [monDicoRef objectForKey:kRessourceIdStr];
            if (!idEnBase) 
                return nil;
            NSString * idDansAppli = [monDico objectForKey:kRessourceIdStr];
            if (!idDansAppli) 
                return nil;
            if ( [idEnBase isEqualToString:idDansAppli]) 
            {
                //NSDate * mydate;
                //NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
                //[dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'"];
                
                NSDate * dateEnBase = [monDicoRef objectForKey:kRessourceUpdatedStr];
                if (!dateEnBase) 
                    return nil;
                
                NSDate * dateDansAppli = [monDico objectForKey:kRessourceDownloadedStr];
                if (!dateDansAppli) 
                    return nil;
                
                NSComparisonResult test = [dateEnBase compare:dateDansAppli];
                if ( test == NSOrderedAscending)
                {
                    DLog(@"La ressource est à jour");
                    return nil;
                }
                else if ( test == NSOrderedSame)
                {
                    //rien à faire 
                    return nil;
                }
                //dateEnBase > dateDansAppli
                else if ( test == NSOrderedDescending)
                {
                    // 
                    DLog(@"La ressource n'est pas à jour");
                    return monDicoRef;
                }
                
                return nil;
                break;
            }
        }
    }
    
    return nil;
    
}



- (void)recuparationListeApplication
{
    DLog(@"");
    if ([self connected])
    {    
        //Communication avec le serveur recuperer la liste des themes
        NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kListeApplication];;
        NSURL *url = [[NSURL alloc] initWithString:urlConnexion];;
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *paramDataString = [[NSString alloc] initWithFormat:
                                     @"id=%@&pseudo=%@&password=%@",kKey,self.username,self.password];
        NSData *paramData = [[NSData alloc] initWithData:[paramDataString dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:paramData];
        self.rootConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if(self.rootConnection) {
            //[indicator startAnimating];
            //[indicator setHidden:NO];
            [self showLoadingProgress];
            
            NSMutableData *data = [[NSMutableData alloc] init];
            self.receivedData = data;
            
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        
        // Uncomment the following line to preserve selection between presentations.
        // self.clearsSelectionOnViewWillAppear = NO;
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem;
        
    }
    else
    {
    	//[indicator setHidden:YES];	
        [self cancelConnection];
        [self hideLoadingProgress];
    }
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	DLog(@"");
	
	static NSString *CellIdentifier = @"MainViewCell";
    
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
    
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
 	NSDictionary * monDico = [titles objectAtIndex:indexPath.row];
    
	
	int nodeCount = [titles count];
	
	if (nodeCount == 0 && indexPath.row == 0)
	{
	}
	
    //MainViewCell *cell = (MainViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     if (cell == nil)
     {
     cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
     reuseIdentifier:CellIdentifier] autorelease];
     }
     */
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                    reuseIdentifier:CellIdentifier];
	
    
	// Configure the cell.
	
	
	if (nodeCount > 0)
	{
		// Set up the cell...
		// nom du support
        
        DLog(@"monDico is %@",monDico);
		cell.textLabel.text = [monDico objectForKey:kRessourceNomStr];
		cell.detailTextLabel.text = [monDico objectForKey:kRessourceTitleStr];
		cell.detailTextLabel.font = [UIFont fontWithName:kHelvetica size:11];
        cell.accessoryView = nil;
		// Icon		
        /*
        CGRect frameCell = CGRectMake(cell.contentView.frame.size.width - 50.0 , 10.0,40, 40);
        UIActivityIndicatorView *spinCell = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        spinCell.hidden = NO;    
        spinCell.frame = frameCell;
        spinCell.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin; 
        spinCell.tag = TAG_SPIN_CELL;
        [cell.contentView addSubview:spinCell];
        */
        if ([[monDico objectForKey:kStatus] isEqualToString:kEncours])
        {
            [cell setTag:[[monDico objectForKey:kRessourceIdStr] integerValue]];
            
            DACircularProgressView * progressView = [[DACircularProgressView alloc] initWithFrameAndText:CGRectMake(cell.contentView.frame.size.width - 60.0 , 18.0, 32.0f, 32.0f)];
            progressView.roundedCorners = YES;
            progressView.progressTintColor = [UIColor blackColor];
            progressView.trackTintColor = [UIColor lightGrayColor];
            progressView.thicknessRatio = 0.2f;
            [progressView setTag:[[monDico objectForKey:kRessourceIdStr] integerValue]];
            [cell.contentView addSubview:progressView];
            /*
            [spinCell setHidden:NO];
            [spinCell setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.30]];
            [spinCell startAnimating];
             */
            
        } 
        else
            
        {
            /*
            [spinCell setHidden:YES];
            [spinCell stopAnimating];
            */
            //Si ya une MAJ
            if ([self verif:monDico] && ![[monDico objectForKey:kStatus] isEqualToString:kEncours])
            {
                //On cherche dans la mémoire de l'app si le pop up a déjà été affiché, et l'utilisateur alerté
                NSString * key = [[NSString alloc] initWithFormat:@"pop_%@",[monDico objectForKey:kRessourceIdStr]];
                              
                //Si non,
                if([[NSUserDefaults standardUserDefaults] boolForKey:key] == NO)
                {
                    NSString * mess = [[NSString alloc] initWithFormat:@"Le module \"%@\" a une mise à jour disponible. Voulez-vous la télécharger?", [monDico objectForKey:kRessourceNomStr]];
                    
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Mise à jour" 
                                                                  message:mess 
                                                                 delegate:self 
                                                        cancelButtonTitle:@"Plus tard" 
                                                        otherButtonTitles:@"Ok", nil];
                    [alert setTag:indexPath.row];
                    [alert show];
                    
                    //On enregistre le fait que le pop-up a été affiché pour ne plus le montrer.
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:key];
                }
                
                //On affiche l'image de Maj 
                UIImage * imageSeal = [UIImage imageNamed:@"maj.png"];
                CGRect frameMAJ = CGRectMake(cell.contentView.frame.size.width - 50.0 , 10.0,40, 40);  
                // Ajout à la vue
                UIButton * newButtonReminder = [[UIButton alloc] init];
                [newButtonReminder setTag:indexPath.row];
                [newButtonReminder addTarget:self action:@selector(newVersion:) forControlEvents:UIControlEventTouchUpInside];
                newButtonReminder.frame = frameMAJ;
                
                newButtonReminder.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin; 
                
                [newButtonReminder setImage:imageSeal forState:UIControlStateNormal];
                newButtonReminder.alpha = 0;
                [cell.contentView addSubview:newButtonReminder];
                
                [UIView animateWithDuration:0.5 animations:^{newButtonReminder.alpha = 1.0;}];
                //[NSThread detachNewThreadSelector:@selector(apparitionBouton:) toTarget:self withObject:cell];
                
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        
        
        DLog(@"");
        // Set appIcon and clear temporary data/image
        UIImage *image = [UIImage imageWithContentsOfFile:
                          [self 
                           dataFilePathAtDirectory:
                           [monDico objectForKey:kRessourceFolderStr]
                           fileName:[monDico objectForKey:kRessourceIconeStr]]];
        
        UIImage * image1;
        if (image.size.width != kAppIconHeight && image.size.height != kAppIconHeight)
        {
            CGSize itemSize = CGSizeMake(kAppIconHeight, kAppIconHeight);
            UIGraphicsBeginImageContextWithOptions(itemSize, NO, [UIScreen mainScreen].scale);
            CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
            [image drawInRect:imageRect];
            image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        else
        {
            image1 = image;
        }
        
		cell.imageView.image = image1; 
        
        if(self.nbreMessParRessource != nil)
        {
            NSString* nbreMess = [[nbreMessParRessource objectForKey:[monDico objectForKey:@"ressourceid"]] stringValue];
            
            DLog(@"%@",nbreMess);
            
            if([nbreMess length] != 0)
            {                
                //On créé une bulle rouge
                //cell.textLabel.shadowColor = [UIColor lightGrayColor];
                
                UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
                UIImage * image;
                
                image = [UIImage imageNamed:kBulleBarreRed];
                [aButton1 setTitle:nbreMess forState:UIControlStateNormal];
                [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                aButton1.titleLabel.font=[UIFont systemFontOfSize:13];    
                
                CGRect frame = CGRectMake(20.0, 0.0, 32.0, 32.0);                    
                frame.origin.y = (cell.frame.size.height - frame.size.height) / 2;
                frame.origin.x = (cell.frame.size.width - frame.size.width * 2);
                aButton1.frame=frame;
                [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
                
                
                //Et on l'attribue à la cellule.
                cell.accessoryView = aButton1;
                //[cell.contentView addSubview:aButton1];       
            }
        }
        
    }
	
	return cell;
}

- (void)newVersion:(id)sender
{
    DLog(@"");
    
    NSArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	// Vérification de l'existence du fichier 
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
    // NSDictionary * monDico = [titles objectAtIndex:indexPath.row];
	NSDictionary * monDico = [titles objectAtIndex:[sender tag]];
	int nodeCount = [titles count];
	if (nodeCount > 0)
	{        
        [self telechargement:[self verif:monDico]];
    }
}

// Override to support row selection in the table view.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	DLog(@"");
	
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
 	NSDictionary * monDico = [titles objectAtIndex:indexPath.row];
    
    currentIndex = indexPath.row;
    
    NSString * resourceFolder = [monDico objectForKey:kRessourceFolderStr];
    NSString * monStatus = [[NSString alloc] initWithString:[monDico objectForKey:kStatus]];
    
    NSString * fichierDescription = [monDico objectForKey:kRessourceStructureStr];
    NSString * fichierParent = [monDico objectForKey:kRessourceFichierParent];
    
    NSString * pathFichierDescription = [self dataFilePathAtDirectory:resourceFolder fileName:fichierDescription];
    NSString * pathFichierParent = [self dataFilePathAtDirectory:resourceFolder fileName:fichierParent];
    
    if ([monStatus isEqualToString:kTermine])
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSArray * arrayRacine;
        DLog(@"pathFichierDescription is %@",pathFichierDescription);
        if ([[NSFileManager defaultManager] fileExistsAtPath:pathFichierDescription]) 
        {
            arrayRacine = [[NSArray alloc] initWithContentsOfFile:pathFichierDescription];
            
            if (arrayRacine)
            {    
                if (self.estVisiteur)
                {   // n'est pas connecté
                    // Appel TitreViewController
                    TitreViewController *titreViewController = [TitreViewController alloc];
                    // infos communes
                    titreViewController.estVisiteur    = self.estVisiteur;
                    titreViewController.titre          = self.title;
                    titreViewController.username       = @"";
                    titreViewController.titreParent    = self.title;
                    titreViewController.idEcranParent  = @"0";
                    titreViewController.idEcran        = [[arrayRacine objectAtIndex:0] objectForKey:@"idecran"];
                    titreViewController.idBlog         = @"";
                    titreViewController.password       = @"";
                    titreViewController.categorie      = @"";
                    titreViewController.ressourceInfo  = monDico;
                    // infos ressources
                    titreViewController.arrayRacine    = arrayRacine;
                    
                    
                    /*************************************************************************/
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:pathFichierParent])
                    {
                        DLog(@"le fichier existe %@",pathFichierParent);  
                        NSArray *myArray = [[NSArray alloc] initWithContentsOfFile:pathFichierParent];
                        titreViewController.pcrDictionary = [myArray objectAtIndex:0];
                    }
                    
                    /*************************************************************************/
                    
                    // titre
                    titreViewController.title          = self.title;
                    // init
                    (void)[titreViewController initWithNibName:@"TitreView" 
                                                  bundle:nil];
                    // push
                    [self.navigationController pushViewController:titreViewController 
                                                         animated:YES];
                    
                }
                else
                {  // est connecté
                    // Appel TitreViewController
                    TitreViewController *titreViewController = [TitreViewController alloc];
                    // infos communes
                    titreViewController.guid    = self.guid;
                    
                    titreViewController.estVisiteur    = self.estVisiteur;
                    titreViewController.titre          = self.title;
                    titreViewController.username       = self.username;
                    titreViewController.titreParent    = self.title;
                    titreViewController.idEcranParent  = @"0";
                    titreViewController.idEcran        = [[arrayRacine objectAtIndex:0] objectForKey:@"idecran"];
                    titreViewController.idBlog         = @"";
                    titreViewController.password       = self.password;
                    titreViewController.categorie      = self.nouveauNiveau;
                    titreViewController.ressourceInfo  = monDico;
                    // infos ressources
                    titreViewController.arrayRacine    = arrayRacine;
                    
                    /*************************************************************************/
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:pathFichierParent])
                    {
                        DLog(@"le fichier existe %@",pathFichierParent);  
                        NSArray *myArray = [[NSArray alloc] initWithContentsOfFile:pathFichierParent];
                        titreViewController.pcrDictionary = [myArray objectAtIndex:0];
                    }
                    
                    /*************************************************************************/
                    
                    // titre
                    titreViewController.title          = self.title;
                    // init
                    (void)[titreViewController initWithNibName:@"TitreView" 
                                                  bundle:nil];
                    // push
                    [self.navigationController pushViewController:titreViewController 
                                                         animated:YES];
                    //-----------------------
                }
            }
            
        }
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"La ressource est en cours de téléchargement ou est incomplète." 
                                                      message:@"Veuillez attendre la fin du téléchargement." 
                                                     delegate:nil 
                                            cancelButtonTitle:@"Accepter" 
                                            otherButtonTitles:nil];
        [alert show];
        
        
    }    
    
    
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	// Return NO if you do not want the specified item to be editable.
    DLog(@"");
    
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
    int count = [titles count];
    
    if ((count == 0) && (indexPath.row == 0))
        return NO;
    
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath 
{
	DLog(@"");
    
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
	if (editingStyle == UITableViewCellEditingStyleDelete) 
    {
        NSDictionary * monDico = [titles objectAtIndex:indexPath.row];
		// Suppression du fichier support
		//DLog(@"titles=%@",titles);
		
        // dossier document de l'application
        //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        // path du dossier de la ressource
        NSString *pathRessourceFolder = [documentsDirectory stringByAppendingPathComponent:[monDico objectForKey:kRessourceFolderStr]];
		// delete dossier Ressource
		if (pathRessourceFolder != nil)
        {    
			DLog(@"pathRessourceFolder:%@",pathRessourceFolder);
			
            NSError *error;
            if ([[NSFileManager defaultManager] removeItemAtPath:pathRessourceFolder error:&error])
            { 
                DLog(@"dossier ressource %@ supprimé",pathRessourceFolder);
            }
            else
            {
                DLog(@"dossier ressource %@ non supprimé erreur %@",pathRessourceFolder,[error localizedDescription]);
            }
            
        }		
	    // arret du telechargement
        //
        if ([[monDico objectForKey:kStatus] isEqualToString:kEncours])
        {   
            NSMutableDictionary * monDicoMutable = [[NSMutableDictionary alloc] initWithDictionary:monDico];
            [monDicoMutable setObject:kSupprimme forKey:kStatus];            
            [[NSNotificationCenter defaultCenter] postNotificationName:kDownloadStoppedNotification object:[[NSDictionary alloc] initWithDictionary:monDicoMutable]];
        }
		
		// Suppression de l'objet à l'index dans le tableau titles
        
		[titles removeObjectAtIndex:indexPath.row];
		
		
		/*  Ecriture du fichier à partir des données du tableau */ 
		BOOL isFile;
        NSString *filePathListe = [self dataFilePath:kSupportFilename];
		if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
			isFile = [titles writeToFile:filePathListe atomically:YES];
		
		else
			isFile = [[NSFileManager defaultManager] createFileAtPath:filePathListe contents:(NSData *)titles attributes:nil]; 
		
		DLog(@"%@ %@",(isFile ? @"liste =":@"liste non modifiee="),filePathListe);
        
        [self addSkipBackupAttributeToPath:filePathListe];

        
        // suppression de la ligne dans la datasource.
		//[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView reloadData];
        
        
	}   
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath 
{
	DLog(@"");
	// ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
	
    int idxToMove = fromIndexPath.row;
	int idxToBe = toIndexPath.row;
	//NSString *itemToMove = [[titles objectAtIndex:idxToMove] objectForKey:kRessourceTitleStr];
	NSMutableDictionary * monDico = [[NSMutableDictionary alloc] initWithDictionary:[titles objectAtIndex:idxToMove]];
	
	// suppression de l item
	[titles removeObjectAtIndex:idxToMove];
	
	// insertion de l item
	[titles insertObject:monDico 
				 atIndex:idxToBe];
	
	
	
	/*  Ecriture du fichier à partir des données du tableau */ 
	BOOL isFile;
	
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		isFile = [titles writeToFile:filePathListe atomically:YES];
	else
		isFile = [[NSFileManager defaultManager] createFileAtPath:filePathListe contents:(NSData *)titles attributes:nil]; 
	
	DLog(@"%@ %@",(isFile ? @"liste =":@"liste non modifiee="),filePathListe);
	
    [self addSkipBackupAttributeToPath:filePathListe];

}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	// Return NO if you do not want the item to be re-orderable.
	DLog(@"");
	return YES;
}


#pragma mark -
#pragma mark Recupération des messages
-(void)requeteNvxMessages
{
    DLog(@"");
    if (!self.estVisiteur) {
        
        //**********************************************************//
        //* Recupération du nombre de messages de chaque ressource *//
        //**********************************************************//
        
        
        NSArray * titles;
        
        NSString *filePathListe = [self dataFilePath:kSupportFilename];
        
        /* Vérification de l'existence du fichier */
        if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
        {
            titles = [[NSArray alloc] initWithContentsOfFile:filePathListe];
        }
        
        //Si ya des ressources téléchargées
        if((titles != nil) && ([titles count] != 0))
        {
    
            [self post:[NSString stringWithFormat:@"%@/webServices/messages/recupererNombreMessages.php",sc_server]
                      callback:^(NSDictionary *wsReturn) {
                          nbreMessParRessource = wsReturn;
                          [aTableView reloadData];
            }];
    
        }
    }
}

#pragma mark -
#pragma mark View processing

- (IBAction)ajouterRessources:(id)sender {
    [self goApp];
}

- (void)goApp
{
	DLog(@"");
    
    aCategorie = [Categorie alloc];
    aCategorie.username = self.username;
    aCategorie.password = self.password;
    aCategorie.niveau = self.nouveauNiveau;
    (void)[aCategorie initWithNibName:@"Categorie" 
                                             bundle:nil];	
    [aCategorie setTitle:@"Ressources"];
    [UIView animateWithDuration:0.75 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [self.navigationController pushViewController:aCategorie animated:NO];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
        
        }];
	//[self.navigationController pushViewController:aCategorie animated:YES];
    
}

-(void)telechargement:(NSDictionary *)dico
{
    if (dico)
    {    
        Telechargement * aTelechargement = [Telechargement alloc];
        NSMutableDictionary *aDico = [[NSMutableDictionary alloc] initWithDictionary:dico];
        [aDico setObject:[NSDate date] forKey:kRessourceDownloadedStr];
        [aDico setObject:kEncours forKey:kStatus];
        aTelechargement.myDico =  [[NSMutableDictionary alloc] initWithDictionary:aDico];
        
        (void)[aTelechargement initWithNibName:@"Telechargement" 
                                  bundle:nil];
        
        /*
        [UIView animateWithDuration:0.75 animations:^{
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [self.navigationController pushViewController:aTelechargement animated:NO];
            [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
            
        }];*/
        [aTelechargement setModalPresentationStyle:UIModalPresentationFullScreen];
        [aTelechargement setModalTransitionStyle:UIModalTransitionStylePartialCurl];
        [self.navigationController presentModalViewController:aTelechargement animated:YES];
        //[self.navigationController pushViewController:aTelechargement animated:YES];
    }
}

- (void)updatePercentageNotification:(NSNotification*)notification
{
    NSDictionary *userInfo = notification.userInfo;
    CGFloat percentage = [(NSNumber*)[userInfo objectForKey:@"prog"] floatValue];
    int idRessource = [[userInfo objectForKey:@"id"] integerValue];
    
    UITableViewCell * cellTableau = (UITableViewCell*)[aTableView viewWithTag:idRessource];
    
    DACircularProgressView * progressView = (DACircularProgressView *)[cellTableau.contentView viewWithTag:idRessource];
    
    [progressView setProgress:percentage animated:YES];
    
}

//Fonction appellée quand la ressource a été dézipée, et qu'une ressource a été téléchargée
- (void)titleDownloadFinishNotification:(NSNotification *)notification
{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSMutableDictionary * myDico = [[NSMutableDictionary alloc] initWithDictionary:[notification object]];
    
    if([[myDico objectForKey:kStatus] isEqualToString:kZip])
    {  
        
        //[myDico setObject:[NSDate date] forKey:kRessourceDownloadedStr];
        
        //Je remplace la date de download par la date de release, sinon, on a jamais de maj proposée alors qu'on devrait.
        [myDico setObject:[myDico objectForKey:kRessourceReleasedStr] forKey:kRessourceDownloadedStr];
        [myDico setObject:kTermine forKey:kStatus];
        NSDictionary * monDico =  [[NSDictionary alloc] initWithDictionary:myDico];
        
        // ========================================================================================  	
        // ajoute la reference du support telecharge dans le fichier liste des supports téléchargés  	
        // ========================================================================================  	
        // initialisation  	
        NSMutableArray *array;
        array = nil;
        BOOL isFileListe = NO;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        // ajout de la reference du support telecharge dans le dictionnaire	
        
        //  recuperation du path du fichier de liste de support  
        NSString *filePathListe = [self dataFilePath:kSupportFilename];
        //  verification de l'existence du fichier de liste de support 
        //  et recuperation du contenu dans le tableau 
        if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe])
            array = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
        else		
            array = nil;
        
        DLog(@"array is %@",array);
        // Si le tableau est vide initialisation avec la nouvelle reference
        // sinon recherche de la presence de la reference de le tableau
        if (array == nil || ([array count] == 0)) 
        {
            array = [[NSMutableArray alloc] initWithObjects:monDico,nil];
        }
        else
        {
            // recherche presence support
            NSString * newID = [monDico objectForKey:kRessourceIdStr];
            BOOL top = NO;
            NSUInteger numeroSupport = 0;
            for (NSDictionary *monDicoRef in array)
            {
                NSString * monID = [monDicoRef objectForKey:kRessourceIdStr];
                if ( [monID isEqualToString:newID]) 
                {
                    top = YES;
                    if ([[monDicoRef objectForKey:kStatus] isEqualToString:kEncours] )
                    {
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[[NSString alloc] initWithFormat:@"Le téléchargement de \"%@\" est terminé.",[monDico objectForKey:kRessourceTitleStr]] 
                                                                      message:@"" 
                                                                     delegate:nil 
                                                            cancelButtonTitle:@"Accepter" 
                                                            otherButtonTitles:nil,nil];
                        [alert show];
                    }
                    break;
                }
                numeroSupport++;
            }
            if (top) 
            {
                [array replaceObjectAtIndex:numeroSupport withObject:monDico];
            }		
            else 
            {
                [array addObject:monDico];
            }
            
        }
        
        //DLog(@"array=%@",array);
        
        //  Ecriture du fichier à partir des données du tableau 
        isFileListe = [[NSFileManager defaultManager] createFileAtPath:filePathListe contents:(NSData *)array attributes:nil]; 
        
        DLog(@"%@%@",(isFileListe ? @"liste = ":@"liste non modifiee = "),filePathListe);

        [self addSkipBackupAttributeToPath:filePathListe];
        
        
    }
    else if([[myDico objectForKey:kStatus] isEqualToString:kSupprimme])
    {        
    }
    else if([[myDico objectForKey:kStatus] isEqualToString:kTermine])
    {
    }
    else if([[myDico objectForKey:kStatus] isEqualToString:kEncours])
    {
        [myDico setObject:[NSDate date] forKey:kRessourceDownloadedStr];
        [myDico setObject:kTermine forKey:kStatus];
        NSDictionary * monDico =  [[NSDictionary alloc] initWithDictionary:myDico];
        
        // ========================================================================================  	
        // ajoute la reference du support telecharge dans le fichier liste des supports téléchargés  	
        // ========================================================================================  	
        // initialisation  	
        NSMutableArray *array;
        array = nil;
        BOOL isFileListe = NO;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        // ajout de la reference du support telecharge dans le dictionnaire	
        
        //  recuperation du path du fichier de liste de support  
        NSString *filePathListe = [self dataFilePath:kSupportFilename];
        //  verification de l'existence du fichier de liste de support 
        //  et recuperation du contenu dans le tableau 
        if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe])
            array = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
        else		
            array = nil;
        
        DLog(@"array is %@",array);
        // Si le tableau est vide initialisation avec la nouvelle reference
        // sinon recherche de la presence de la reference de le tableau
        if (array == nil || ([array count] == 0)) 
        {
            array = [[NSMutableArray alloc] initWithObjects:monDico,nil];
        }
        else
        {
            // recherche presence support
            NSString * newID = [monDico objectForKey:kRessourceIdStr];
            BOOL top = NO;
            NSUInteger numeroSupport = 0;
            for (NSDictionary *monDicoRef in array)
            {
                NSString * monID = [monDicoRef objectForKey:kRessourceIdStr];
                if ( [monID isEqualToString:newID]) 
                {
                    top = YES;
                    if ([[monDicoRef objectForKey:kStatus] isEqualToString:kEncours] )
                    {
                        
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[[NSString alloc] initWithFormat:@"Le téléchargement de \"%@\" est terminé.",[monDico objectForKey:kRessourceTitleStr]] 
                                                                      message:@"" 
                                                                     delegate:nil 
                                                            cancelButtonTitle:@"Accepter" 
                                                            otherButtonTitles:nil,nil];
                        [alert show];
                         
                        /*
                        RNBlurModalView *alert = [[RNBlurModalView alloc] initWithViewController:self title:@"Téléchargement terminé." message:[[NSString alloc] initWithFormat:@"Le téléchargement de \"%@\" est terminé.",[monDico objectForKey:kRessourceTitleStr]] ];
                
                        [alert show];
                         */
                    }
                    break;
                }
                numeroSupport++;
            }
            if (top) 
            {
                [array replaceObjectAtIndex:numeroSupport withObject:monDico];
            }		
            else 
            {
                [array addObject:monDico];
            }
            
        }
        
        //DLog(@"array=%@",array);
        
        //  Ecriture du fichier à partir des données du tableau 
        isFileListe = [[NSFileManager defaultManager] createFileAtPath:filePathListe contents:(NSData *)array attributes:nil]; 
        
        DLog(@"%@%@",(isFileListe ? @"liste = ":@"liste non modifiee = "),filePathListe);
        
        [self addSkipBackupAttributeToPath:filePathListe];

        
    }
    
    [aTableView reloadData];
}

- (void)listeSupportChangedNotification:(NSNotification *)notification
{
	DLog(@"");
	self.navigationItem.title = @"SupCast";
	
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
	
	currentIndex = 0;
	// ====================================  
	// Recupere la liste des supports dans le fichier plist des supports téléchargés
	
    ////////////////
    UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
    tools.barStyle = UIBarStyleBlackOpaque;
    tools.tintColor = [UIColor colorWithRed:1.0 green:0.6 blue:0.0 alpha:0.6];
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(goApp)];
    UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
    
    [buttons addObject:ajouterMessage2];
    [buttons addObject:ajouterMessage];
    [tools setItems:buttons animated:NO];
    self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
    ////////////////
    
	
	//	[appButtonItem release];
	
	[super viewDidLoad];
	
}

- (void)titleChangedNotification:(NSNotification *)notification
{
	DLog(@"");
	
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
    
	// Récupération des informations dans un dictionnaire
	NSMutableDictionary * monDico = [[NSMutableDictionary alloc] initWithDictionary:[notification userInfo]];
	
	// Ajout du dico au table des infos
	if (monDico != nil) {
		[titles removeObjectAtIndex:currentIndex];
		[titles insertObject:monDico atIndex:currentIndex];
	}
	
	
	//  Ecriture du fichier à partir des données du tableau 
	BOOL isFile;
    
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		isFile = [titles writeToFile:filePathListe atomically:YES];	
	else
		isFile = [[NSFileManager defaultManager] createFileAtPath:filePathListe contents:(NSData *)titles attributes:nil]; 
	
    DLog(@"%@ %@",(isFile ? @"liste =":@"liste non modifiee="),filePathListe);

    [self addSkipBackupAttributeToPath:filePathListe];

}

- (void)setEditing:(BOOL)flag animated:(BOOL)animated
{
    DLog(@"");
    [super setEditing:flag animated:animated];
	if (flag == YES) {
		self.navigationItem.rightBarButtonItem = nil;
	}
	else 
	{
        
		
        ////////////////
        UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
        tools.barStyle = UIBarStyleBlackOpaque;
        tools.tintColor = [UIColor colorWithRed:1.0 green:0.6 blue:0.0 alpha:0.6];
        NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
        UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(goApp)];
        UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
        
        [buttons addObject:ajouterMessage2];
        [buttons addObject:ajouterMessage];
        [tools setItems:buttons animated:NO];
        self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
        ////////////////
        
        
	}
	
	
}

- (void)dealloc {
	DLog(@"");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	//[appButtonItem release];

}


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
    
}



- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if (!(HUD.hidden))
    {
        CGAffineTransform myT;
        if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            myT = CGAffineTransformMakeRotation(M_PI*1.5);
        } 
        else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) 
        {
            myT =  CGAffineTransformMakeRotation(M_PI/2);
        } 
        else if (toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            myT =  CGAffineTransformMakeRotation(-M_PI);
        }
        else
        {
            myT =  CGAffineTransformIdentity;
        }
        
        self.HUD.transform = myT; 
        
    }
}




#pragma mark -
#pragma mark Manip fichier


// ====================================  
// Début 
// Initilisation du fichier de liste de support 
// A supprimmer pour la version prod 
// ====================================  
// recuperation du path du fichier 

-(void) initFichier
{
	DLog(@"");
	
	
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	NSMutableArray * array; 
	BOOL isFile;
	array = nil;
	
	isFile = [[NSFileManager defaultManager] createFileAtPath:filePathListe contents:nil attributes:nil]; 
	
	
	DLog(@"%@ %@",(isFile ? @"liste modifie=":@"liste non modifiee="),filePathListe);
    [self addSkipBackupAttributeToPath:filePathListe];

}

-(void)unzip:(NSString *)pathFichierZip
{
    
    // ====================================  
	// Acces au fichier de liste de support  
	// ====================================  
	// recuperation du path du fichier
    NSMutableArray * titles;
    
	NSString *filePathListe = [self dataFilePath:kSupportFilename];
	/* Vérification de l'existence du fichier */
	if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe]) 
		titles = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
	else
		titles = nil;
    
    
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   // NSString *documentsDirectory = [paths objectAtIndex:0];
	NSArray *cachePaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [cachePaths objectAtIndex:0];
    
    NSString *fichieradezipper = [[NSString alloc] initWithString:[pathFichierZip lastPathComponent]];
    
    ZipArchive* za = [[ZipArchive alloc] init];
    if( [za UnzipOpenFile:pathFichierZip] )
    {
        
        NSString * dossierEntry = [cachesDirectory stringByAppendingPathComponent:[fichieradezipper stringByDeletingPathExtension]];
        [[NSFileManager defaultManager] removeItemAtPath:dossierEntry error:nil];
        
        BOOL ret = [za UnzipFileTo:cachesDirectory overWrite:YES];
        if( NO==ret )
        {
            // error handler here
        }

        [self addSkipBackupAttributeToPath:dossierEntry];
        
        [za UnzipCloseFile];
        
        NSString * fichierEntry = [[cachesDirectory stringByAppendingPathComponent:[fichieradezipper stringByDeletingPathExtension]]stringByAppendingPathComponent:@"Support.plist"];
        if([[NSFileManager defaultManager] fileExistsAtPath:fichierEntry]) 
        {              
            titles = [[NSMutableArray alloc] initWithContentsOfFile:fichierEntry];
            NSDictionary * monDico = [titles objectAtIndex:0];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kTitleDownloadFinishNotification object:monDico];
        }
        
    }
    
    
    
    NSError *error;
    if ([[NSFileManager defaultManager] removeItemAtPath:pathFichierZip error:&error])
    { 
        DLog(@"fichier zip %@ supprimé",pathFichierZip);
    }
    else
    {
        DLog(@"fichier zip %@ supprimé erreur %@",pathFichierZip,[error localizedDescription]);
    }
    
}

-(NSString *)dataFilePathCaches:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];

	
}


-(void)addSkipBackupAttributeToPath:(NSString*)path {
    u_int8_t b = 1;
    setxattr([path fileSystemRepresentation], "com.apple.MobileBackup", &b, 1, 0, 0);
}

-(NSString *)dataFilePathAtDirectory:(NSString *)directory 
							fileName:(NSString *)fileName {
    
	DLog(@"");
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *subDirectory = [documentsDirectory stringByAppendingPathComponent:directory];
	BOOL isDir=NO;
	
    if ([[NSFileManager defaultManager] fileExistsAtPath:subDirectory isDirectory:&isDir] && isDir)
	{
		//
	}
	else
	{

		if( [[NSFileManager defaultManager] createDirectoryAtPath:subDirectory withIntermediateDirectories:YES attributes:nil error:nil])
            [self addSkipBackupAttributeToPath:subDirectory];
	}
	
	return [subDirectory stringByAppendingPathComponent:fileName];
	
}

-(NSString *)dataFilePath:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}

- (void)rechercheRessourcesZippees
{
	DLog(@"");
    [self showLoadingProgress];

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	//NSArray *cachesPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    //NSString *cachesDirectory = [cachesPaths objectAtIndex:0];

    //********************************************************///////////////
    //// acces fichier des regles
    
    NSString *thePathFilenameRegle = [self dataFilePath:kFilenameRegle];
    BOOL ressourceinitiale = YES;
    
    //Si le fichier règle n'existe pas
    if (![[NSFileManager defaultManager] fileExistsAtPath:thePathFilenameRegle])
    {
        //On va installer une première et seule fois la ressource initiale        
        //Et on va écrire dans le fichier en question qu'il n'y a pas de ressource initiale
        //En gros, que la ressource a déjà été installée.
        NSArray * array = [[NSArray alloc] initWithObjects:
                           [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:NO],kGPS,
                            nil],
                           [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:NO],kTWITTER,
                            @"",kUSER,
                            @"",kMDP,
                            [NSNumber numberWithBool:NO],kRessourceInitiale,
                            nil],
                           nil];

        [array writeToFile:thePathFilenameRegle atomically:YES];
        
    }
    //S'il existe
    else
    {
        //On récupère la valeur du flag
        NSArray *myArray = [[NSArray alloc] initWithContentsOfFile:thePathFilenameRegle];
        ressourceinitiale =[[[myArray objectAtIndex:1] objectForKey:kRessourceInitiale] boolValue];
        
        if (ressourceinitiale)
        {
            BOOL statusPublicationGPS =[[[myArray objectAtIndex:0] objectForKey:kGPS] boolValue];
            BOOL statusPublicationTwitter =[[[myArray objectAtIndex:1] objectForKey:kTWITTER] boolValue];
            
            NSMutableString * userTwitter = [[NSMutableString alloc] init];
            NSMutableString * motdepasseTwitter = [[NSMutableString alloc] init];
            
            if([[myArray objectAtIndex:1] objectForKey:kUSER] == nil)
                [userTwitter setString:@""];
            else
                [userTwitter setString:[[myArray objectAtIndex:1] objectForKey:kUSER]];
            
            if([[myArray objectAtIndex:1] objectForKey:kMDP] == nil)
                [motdepasseTwitter setString:@""];
            else
                [motdepasseTwitter setString:[[myArray objectAtIndex:1] objectForKey:kMDP]];
            
            
            NSArray * array = [[NSArray alloc] initWithObjects:
                               [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:statusPublicationGPS],kGPS,
                                nil],
                               [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:statusPublicationTwitter],kTWITTER,
                                userTwitter,kUSER,
                                motdepasseTwitter,kMDP,
                                [NSNumber numberWithBool:NO],kRessourceInitiale,
                                nil],
                               nil];
            [array writeToFile:thePathFilenameRegle atomically:YES];
            
            
        }
        
    }
    
    ////*******************************************************///////////////
    //// traitement des fichiers zip dans le bundle
    if (ressourceinitiale)
    {
        //Caler ici le nom du fichier pour inclure la ressource initiale
        NSString *pathFichierZip1 = [[NSBundle mainBundle] pathForResource:@"C2i-D2" 
                                                                    ofType:@"zip"];
        
        DLog(@"pathFichierZip1 is %@",pathFichierZip1);
        if ([[NSFileManager defaultManager] fileExistsAtPath:pathFichierZip1])
        {            
            [self unzip:pathFichierZip1];
        }
    }
    
    //********************************************************///////////////
    //// traitement des fichiers zip dans le dossier Documents
    
    NSArray *dirEnum =[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];
    //NSString *fichierzip;
    for (NSString *fichierzip in dirEnum) 
    {
        //if ([dirEnum level] > 1) break;
        DLog(@" fichierzip is %@",fichierzip);
        if ([[fichierzip pathExtension] isEqualToString:@"zip"])
        {            
            NSString * pathFichierZip = [documentsDirectory stringByAppendingPathComponent:fichierzip];
            if ([[NSFileManager defaultManager] fileExistsAtPath:pathFichierZip])
            {   
                [self unzip:pathFichierZip];
                
            }
            
        }
    }
    [self hideLoadingProgress];

}


#pragma mark "HUD" chargement en cours

- (void)showLoadingProgress {
	if (HUD==nil) {
        
        DLog(@"showLoadingProgress");
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        CGRect windowFrame = mainWindow.frame;
        DLog(@"window=%@", [mainWindow description]);
        CGRect progressHUDrect = CGRectMake(windowFrame.origin.x,
                                            windowFrame.origin.y, 
                                            windowFrame.size.width,
                                            windowFrame.size.height);
        progressHUDrect.size.height = 120.0;
        progressHUDrect.origin.y = (windowFrame.size.height / 2.0) - (progressHUDrect.size.height / 2);
        progressHUDrect.origin.x = 80.0;
        progressHUDrect.size.width = windowFrame.size.width - (progressHUDrect.origin.x * 2);
        
        UIInterfaceOrientation orient = [self interfaceOrientation];
        CGAffineTransform myT;
        if (orient == UIInterfaceOrientationLandscapeLeft)
        {
            myT = CGAffineTransformMakeRotation(M_PI*1.5);
        } 
        else if (orient == UIInterfaceOrientationLandscapeRight) 
        {
            myT =  CGAffineTransformMakeRotation(M_PI/2);
        } 
        else if (orient == UIInterfaceOrientationPortraitUpsideDown)
        {
            myT =  CGAffineTransformMakeRotation(-M_PI);
        }
        else
        {
            myT =  CGAffineTransformIdentity;
        }
        
        
        self.HUD = [[WBProgressHUD alloc] initWithFrame:progressHUDrect];
        self.HUD.transform = myT; 

        HUD.colorBackground = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.30];
        DLog(@"alloc OK : HUD=%@", [HUD description]);
        [HUD setText:@"Chargement …"];
        [HUD showInView:mainWindow];
    }
}

- (void)hideLoadingProgress {
	DLog(@"hideLoadingProgress");
	if (HUD) {
		HUD.hidden = YES;
		[HUD removeFromSuperview];
		self.HUD = nil;
	}
}
#pragma mark -
#pragma mark UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //On appelle la méthode déclenchée par un clic sur le bouton de mis à jour,
    //Et on envoie l'alertView pour faire passer le tag, qui représente l'index 
    //de la ressource dans la liste du fichier Supports.pList. 
    
    //Si l'utilisateur n'a pas cliqué sur "Plus tard"
    if(buttonIndex != 0)
        [self newVersion:alertView];
}

#pragma mark -
#pragma mark NSURLConnection Callbacks delegate
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if(connection == self.connectionNewMessages)
        [self.receivedDataNewMessages setLength:0];
    else if (connection == self.rootConnection)
        [receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if(connection == self.connectionNewMessages)
        [self.receivedDataNewMessages appendData:data];
    else if(connection == self.rootConnection)
        [receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
    //	[indicator setHidden:YES];	
    //	[indicator stopAnimating];	
    [self hideLoadingProgress];
    
    self.receivedDataNewMessages = nil;
	self.receivedData = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
	//[indicator setHidden:YES];
	//[indicator stopAnimating];
    [self hideLoadingProgress];
    if(self.connectionNewMessages == connection)
    {
        NSString * error; 
        NSPropertyListFormat format; 
        NSMutableArray * nvxMessTest;
        
        nvxMessTest = [NSPropertyListSerialization propertyListFromData:self.receivedDataNewMessages
                                                       mutabilityOption:NSPropertyListImmutable
                                                                 format:&format
                                                       errorDescription:&error]; 
        self.receivedDataNewMessages = nil;
        if(!nvxMessTest)
        { 
            
            DLog(@"Error: %@",error); 
            
        }
        else
        {   
            DLog(@"Retour de script : %@",nvxMessTest);            
            self.nbreMessParRessource = [nvxMessTest objectAtIndex:0];

        }
        self.connectionNewMessages = nil;
    }
    else if(self.rootConnection == connection)
    {
        //[connection release];
        
        NSString *error; 
        NSPropertyListFormat format;
        
        // Initialisation des listes Themes et Etablissements
        self.plist = [[NSArray alloc] init];
        // serialisation du fichier Themes.plist en dictionnaire    
        self.plist = [NSPropertyListSerialization propertyListFromData:self.receivedData
                                                      mutabilityOption:NSPropertyListImmutable
                                                                format:&format
                                                      errorDescription:&error];
        
        
        if(!self.plist)
        { 
            DLog(@"Error: %@",error); 
        }
        else
        {
            //DLog(@"self.plist is %@",self.plist);
        }
        
    }
    [aTableView reloadData];
}

@end