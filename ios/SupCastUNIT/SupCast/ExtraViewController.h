//
//  ExtraViewController.h
//  SupCast
//
//  Created by fgutie10 on 29/06/10.
//  Copyright 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ExtraViewController : UIViewController {

	IBOutlet UILabel *nomLabel;
	IBOutlet UILabel *prenomLabel;
	
}

@property (nonatomic, retain) IBOutlet UILabel *nomLabel;
@property (nonatomic, retain) IBOutlet UILabel *prenomLabel;

@end
