//
//  AudioViewController.m
//  SupCast
//
//  Created by Phetsana on 16/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "GestionFichier.h"
#import "AudioViewController.h"
#import "AudioCell.h"

#define kRowHeight 60
#define kSectionHeaderHeight 40

@implementation AudioViewController
@synthesize label,dirRes,dicoRacine,textLabel,itemsList;

// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil 
{
	DLog(@"");
    self = [super initWithNibName:nibNameOrNil 
                           bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
		
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	DLog(@"");
    [super viewDidLoad];
	[fondEcranImageView setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[self.dicoRacine objectForKey:kImageFondEcran]]]];
	
	self.label.text = textLabel;
	
	tableView.rowHeight = kRowHeight;
	tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	tableView.backgroundColor = [UIColor clearColor];
	
	segmentedControl.tintColor = [UIColor blackColor];
	
	[tableView reloadData];
	
	[segmentedControl addTarget:self 
						 action:@selector(segmentAction:) 
			   forControlEvents:UIControlEventValueChanged];
	
	positionsList = [[NSMutableArray alloc] init];
	
	pinyinElements = [[NSMutableArray alloc] init];
	ideogramElements = [[NSMutableArray alloc] init];
	alphabetElements = [[NSMutableArray alloc] init];
	audioNameVersion1 = [[NSMutableArray alloc] init];
	audioNameVersion2 = [[NSMutableArray alloc] init];
	audioVersion1Extension = [[NSMutableArray alloc] init];
	audioVersion2Extension = [[NSMutableArray alloc] init];
	
	int tmpPosition = 0;
	
	for (NSDictionary *elem in itemsList) {
		NSArray *audiosList = [[elem objectForKey:kAudios] retain];
		[positionsList addObject:[NSNumber numberWithInt:tmpPosition]];
		tmpPosition += [audiosList count];
		for (NSDictionary *eachElement in audiosList) {
			DLog(@"eachElement : %@",eachElement);
			if ([eachElement objectForKey:kPinyin]) [pinyinElements addObject:[eachElement objectForKey:kPinyin]];
			if ([eachElement objectForKey:kIdeogram]) [ideogramElements addObject:[eachElement objectForKey:kIdeogram]];
			if ([eachElement objectForKey:kAlphabet]) [alphabetElements addObject:[eachElement objectForKey:kAlphabet]];
			if ([eachElement objectForKey:kAudioNameVersion1]) [audioNameVersion1 addObject:[eachElement objectForKey:kAudioNameVersion1]];
			if ([eachElement objectForKey:kAudioNameVersion2]) [audioNameVersion2 addObject:[eachElement objectForKey:kAudioNameVersion2]];
			if ([eachElement objectForKey:kAudioVersion1Extension]) [audioVersion1Extension addObject:[eachElement objectForKey:kAudioVersion1Extension]];
			if ([eachElement objectForKey:kAudioVersion2Extension]) [audioVersion2Extension addObject:[eachElement objectForKey:kAudioVersion2Extension]];
		}
		[audiosList release];
	}
	
	[tableView reloadData];
}


- (void)didReceiveMemoryWarning {
	DLog(@"");
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	DLog(@"");
    return [itemsList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	DLog(@"");
    return [[[itemsList objectAtIndex:section] objectForKey:kAudios] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	DLog(@"");
	return [[itemsList objectAtIndex:section] objectForKey:kSectionName];
}

- (UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	DLog(@"");
    
    static NSString *CellIdentifier = @"Cell";
	
	AudioCell *cell = (AudioCell *)[atableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[AudioCell alloc] autorelease];
		//[cell initWithFrame:CGRectZero 
		//	reuseIdentifier:CellIdentifier];
        [cell initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setFrame:CGRectZero];
    }
    
	[cell setDirRes:self.dirRes];
	[cell.myButton1 setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																												   fileName:[self.dicoRacine objectForKey:kImageTrianglerg]]]
					forState:UIControlStateNormal];
	
	[cell.myButton2 setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																												   fileName:[self.dicoRacine objectForKey:kImageTrianglebc]]] 
					forState:UIControlStateNormal];
	// Set up the cell...
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	
	int index = [[positionsList objectAtIndex:indexPath.section] intValue] + indexPath.row;
	
	switch ([segmentedControl selectedSegmentIndex] ) {
		case 0:
			if ([pinyinElements count]> index)
			{
				cell.primaryLabel.text = [pinyinElements objectAtIndex:index];
			}
			else 
			{
				cell.primaryLabel.text = @"A";
			}
			break;
		case 1:
			if ([ideogramElements count]> index)
			{
				cell.primaryLabel.text = [ideogramElements objectAtIndex:index];
			}
			else 
			{
				cell.primaryLabel.text = @"B";
			}
			break;
		case 2:
			if ([alphabetElements count]> index)
			{
				cell.primaryLabel.text = [alphabetElements objectAtIndex:index];
			}
			else 
			{
				cell.primaryLabel.text = @"C";
			}
			break;
		default:
			break;
	}
	
	[cell setAudioName:[audioNameVersion1 objectAtIndex:index] 
		 extensionName:[audioVersion1Extension objectAtIndex:index] 
			   version:0];
	[cell setAudioName:[audioNameVersion2 objectAtIndex:index] 
		 extensionName:[audioVersion2Extension objectAtIndex:index] 
			   version:1];
    return cell;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	DLog(@"");
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(CGFloat)tableView:(UITableView *)tableViewHeader heightForHeaderInSection:(NSInteger)section {
	
	DLog(@"");
	if([self tableView:tableViewHeader titleForHeaderInSection:section] != nil) {
		return kSectionHeaderHeight;
	}
	else {
		return 0;
	}
	
}


-(UIView *)tableView:(UITableView *)tableViewHeader viewForHeaderInSection:(NSInteger)section {
	
	DLog(@"");
	NSString *sectionTitle = [self tableView:tableViewHeader titleForHeaderInSection:section];
	
	if(sectionTitle == nil)
		return nil;
	
	//Créer étiquette avec le titre
	UILabel *labelHeader = [[[UILabel alloc] init] autorelease];
	labelHeader.frame = CGRectMake(20,6,300,30);
	labelHeader.backgroundColor = [UIColor clearColor];
	labelHeader.textColor = [UIColor whiteColor];
	labelHeader.shadowColor = [UIColor clearColor];
	labelHeader.shadowOffset = CGSizeMake(0.0, 1.0);
	labelHeader.font =[UIFont boldSystemFontOfSize:16];
	labelHeader.text = sectionTitle;
	
	//Créer en-tête
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,kSectionHeaderHeight)];
	[view autorelease];
	[view addSubview:labelHeader];
	
	return view;
	
}

#pragma mark -
#pragma mark segmentedControlDelegate

- (void)segmentAction:(id)sender {
	DLog(@"");
	[tableView reloadData];
}

- (void)dealloc {
	DLog(@"");
	[self.label release];
	[positionsList release];
	[pinyinElements release];
	[ideogramElements release];
	[alphabetElements release];
	[audioNameVersion1 release];
	[audioNameVersion2 release];
	[audioVersion1Extension release];
	[audioVersion2Extension release];
	[super dealloc];
}

@end
