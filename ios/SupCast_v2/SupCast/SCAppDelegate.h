//
//  SCAppDelegate.h
//  SupCast
//
//  Created by Maen Juganaikloo on 15/04/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SCViewController;

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SCViewController *viewController;

@end
