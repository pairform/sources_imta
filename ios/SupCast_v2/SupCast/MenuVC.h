//
//  MenuVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UITableViewController

@property (nonatomic, strong) NSMutableArray * menuItems;

@end
