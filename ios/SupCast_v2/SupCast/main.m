//
//  main.m
//  SupCast
//
//  Created by Maen Juganaikloo on 15/04/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCAppDelegate class]));
    }
}
