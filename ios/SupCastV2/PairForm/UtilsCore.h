//
//  Utils.h
//  testcoredate
//
//  Created by admin on 22/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCAppDelegate.h"
#import "Message.h"
#import "Utilisateur.h"
#import "Ressource.h"

@interface UtilsCore : NSObject {
    NSFetchedResultsController *fetchedResultsController;
    NSManagedObjectContext *managedObjectContext;
}
+ (void) sauvegardeUtilisateur:(NSNumber *) id_utilisateur name:(NSString *)name url:(NSString *)url;

+ (NSArray *) getAllUtilisateurs;

+ (Utilisateur *) getUtilisateur:(NSNumber *)id_utilisateur;

+ (void) rafraichirMessages;

+ (void) rafraichirMessagesFromRessource:(NSNumber *)id_ressource;

+ (void) rafraichirMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page;

+ (void) rafraichirMessagesFromUser:(NSNumber *)owner_guid;
+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message;

+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message indexTableView:(NSNumber *)indexTableView;

+ (Message *) getMessage:(NSNumber *)id_message;

+ (void) setMessageLu:(NSNumber *)id_message;

+ (NSArray * ) getReponsesFromMessage:(NSNumber *)id_message;

+ (NSArray * ) getMessagesFromUser:(NSNumber *)owner_guid;

+ (NSArray * ) getMessagesFromUsers:(NSArray *)id_utilisateurs;

+ (NSArray * ) getMessagesFromRessourceTrans:(NSNumber *)id_ressource;

+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource;

+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page;

+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page tag:(NSString *)tag num_occurence:(NSString *)num_occurence;

+ (NSArray * ) getMessagesFromOAInPage:(NSString *)nom_page;

+ (NSArray * ) getAllMessages;

+ (NSFetchRequest *) getMessageEntityFetch;

+ (NSNumber *) getNombreMessageNonLuFromRessourceTotale:(NSNumber *)id_ressource;
+ (NSNumber *) getNombreMessageNonLuFromRessource:(NSNumber *)id_ressource;
+ (NSNumber * ) getNombreMessageNonLuFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page;
+ (NSNumber * ) getNombreMessageNonLuFromRessourcePageTotale:(NSNumber *)id_ressource nom_page:(NSString *)nom_page;
+ (void) enregistrerRessource:(Ressource * )ressource;

+(void)startPullingTimer;

+(void)stopPullingtimer;
+(NSArray *)getAllTag;

+(NSMutableArray *)getMessageFromTag:(NSString *)tag;


+(NSString* ) trimPageUrlForLocalDB:(NSString *)pageUrl;

+ (void) enregistrerRessourceFromDictionary:(NSDictionary * )ressourceDictionary;

+ (Ressource*) convertDictionaryToRessource:(NSDictionary * )ressourceDictionary temporary:(BOOL)temporary;

+ (void) updateRessources:(NSArray*)ressources;

+ (void) setIndex:(int)index toRessource:(NSNumber *)id_ressource;

+ (Ressource *) getRessource:(NSNumber *)id_ressource;

+ (NSArray *) getAllRessources;

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end
