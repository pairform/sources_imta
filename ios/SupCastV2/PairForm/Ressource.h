//
//  Ressource.h
//  SupCast
//
//  Created by Maen Juganaikloo on 03/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Ressource : NSManagedObject

@property (nonatomic, retain) NSString * url_mob;
@property (nonatomic, retain) NSString * url_web;
@property (nonatomic, retain) NSString * nom_court;
@property (nonatomic, retain) NSString * nom_long;
@property (nonatomic, retain) NSString * description_res;
@property (nonatomic, retain) NSData * icone;
@property (nonatomic, retain) NSData * icone_etablissement;
@property (nonatomic, retain) NSString * etablissement;
@property (nonatomic, retain) NSString * theme;
@property (nonatomic, retain) NSString * auteur;
@property (nonatomic, retain) NSString * licence;
@property (nonatomic, retain) NSNumber * visible;
@property (nonatomic, retain) NSDate * date_update;
@property (nonatomic, retain) NSDate * date_release;
@property (nonatomic, retain) NSNumber * taille;
@property (nonatomic, retain) NSNumber * id_ressource;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSDate * date_downloaded;
@property (nonatomic, retain) NSString * path;

-(UIImage*) getIcone;
-(UIImage*) getIconeEtablissement;
-(UIImage*) getPath;

@end
