//
//  Image.h
//  profil
//
//  Created by admin on 26/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Image : NSManagedObject

@property (nonatomic, retain) NSNumber * id_utilisateur;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSData * image;

@end
