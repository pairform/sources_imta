//
//  Ressource.m
//  SupCast
//
//  Created by Maen Juganaikloo on 03/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "Ressource.h"


@implementation Ressource

@dynamic url_mob;
@dynamic url_web;
@dynamic nom_court;
@dynamic nom_long;
@dynamic description_res;
@dynamic icone;
@dynamic icone_etablissement;
@dynamic etablissement;
@dynamic theme;
@dynamic auteur;
@dynamic licence;
@dynamic visible;
@dynamic date_update;
@dynamic date_release;
@dynamic taille;
@dynamic id_ressource;
@dynamic date_downloaded;
@dynamic path;
@dynamic index;

-(UIImage*) getIcone {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[self icone]];
}

-(UIImage*) getIconeEtablissement {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[self icone_etablissement]];
}

-(NSString*) getPath{
    NSArray * documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory = [documentPath objectAtIndex:0];
    
    return [NSString stringWithFormat:@"%@/%@",documentDirectory,[self nom_court]];
}
@end
