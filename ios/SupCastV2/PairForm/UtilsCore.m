//
//  Utils.m
//  testcoredate
//
//  Created by admin on 22/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "UtilsCore.h"
#import "SCAppDelegate.h"
#import "NSData+MKBase64.h"
#import "RootVC.h"
#import "NSObject+WService.h"

@implementation UtilsCore

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext = _managedObjectContext;

static NSManagedObjectContext * volatile context = nil;
static  NSTimer* timer = nil;

+ (void) initContext{
    //DLog(@"");
    if ( context == nil ){
        context = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
}

+ (void) sauvegardeUtilisateur:(NSNumber *) id_utilisateur name:(NSString *)name url:(NSString *)url{
    //DLog(@"");
    [self initContext];
    // si l'image existe deja
    Utilisateur * utilisateurEntity = [self getUtilisateur:id_utilisateur];
    if (utilisateurEntity != nil){
        
        // Si l'image actuelle est celle en bdd , on arrete
        if ([utilisateurEntity.url isEqualToString:url]){
            return;
        }
    }
    // Si l'image n'existe pas , il faut la creer.
    if (!utilisateurEntity){
         NSEntityDescription *entity = [NSEntityDescription entityForName:@"Utilisateur" inManagedObjectContext:context];
        utilisateurEntity = [[Utilisateur alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    }
    UIImage * image = [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString:url]]];
   
    // Compression dans un nsdata
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:image];
    utilisateurEntity.image = data;
    utilisateurEntity.id_utilisateur = id_utilisateur;
    utilisateurEntity.url = url;
    utilisateurEntity.owner_username = name;
    
    NSError *error;
	[context save:&error];
    //DLog(@"%@",error);
    
}
+ (NSArray *) getAllUtilisateurs{
    //DLog(@"");
    [self initContext];
    NSError *error;
    NSEntityDescription *utilisateurEntity = [NSEntityDescription entityForName:@"Utilisateur" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:utilisateurEntity];
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    return array;
}


+ (Utilisateur *) getUtilisateur:(NSNumber *)id_utilisateur{
    //DLog(@"");
    [self initContext];
    NSError *error;
    NSEntityDescription *utilisateurEntity = [NSEntityDescription entityForName:@"Utilisateur" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:utilisateurEntity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_utilisateur == %@)", id_utilisateur];
    [fetch setPredicate:predicate];
    
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    Utilisateur *utilisateur = [array objectAtIndex:0];
    return utilisateur;
}

+ (void) rafraichirMessagesWithParams:(NSDictionary *)params{
    //DLog(@"");
    //on peut faire un cast pour éviter un warning du style 'managedObjectContext not find in protocol'
	[self initContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    BOOL success = [self post:[NSString stringWithFormat:@"%@/messages/recupererMessages.php",sc_server]
                        param:params
                     callback:^(NSDictionary *wsReturn) {
                         
                  NSArray * messages = [wsReturn objectForKey:@"messages"];
                         
                         //Sécurité anti-null de la part du WS
                         if([messages isKindOfClass:NSClassFromString(@"NSNull")])
                         {
                             [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
                             return;
                         }
                  for (NSDictionary *item in messages)
                  {
                      bool estDejaDansBdd = YES;
                      //DLog(@"Id message : %@",[item objectForKey:@"id_message"]);

                      Message *message = [UtilsCore getMessage:[f numberFromString:[item objectForKey:@"id_message"]]];
                      
                      // Si le message existe deja , On regarde si l'avatar de l'utilisateur est à jour, toujours.
                      if ( message != nil ){
                          [UtilsCore sauvegardeUtilisateur:message.owner_guid name:[item objectForKey:@"owner_username"] url:[item objectForKey:@"owner_avatar_medium"]];
                      }
                      
                      
                      // si le message n'existe pas , ou qu'il n'est pas a jour , on l'enregistre.
                      if ( (message == nil) || (message.time_updated.intValue < [f numberFromString:[item objectForKey:@"time_updated"]].intValue) ){
                          if ( message == nil){
                              estDejaDansBdd = NO;
                              message = [[Message alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
                          }

                          message.id_message = [f numberFromString:[item objectForKey:@"id_message"]];
                          message.value= [item objectForKey:@"value"];
                          message.owner_guid = [f numberFromString:[item objectForKey:@"owner_guid"]];
                          message.utilite= [f numberFromString:[item objectForKey:@"utilite"]];
                          message.supprime_par = [f numberFromString:[item objectForKey:@"supprime_par"]];
                          

                          
                          message.value= [item objectForKey:@"value"];
                          message.id_ressource= [f numberFromString:[item objectForKey:@"id_ressource"]];
                          message.nom_page= [item objectForKey:@"nom_page"];
                          message.nom_tag= [item objectForKey:@"nom_tag"];
                          message.num_occurence= [f numberFromString:[item objectForKey:@"num_occurence"]];
                          message.time_created= [f numberFromString:[item objectForKey:@"time_created"]];
                          message.time_updated= [f numberFromString:[item objectForKey:@"time_updated"]];
                          
                          
                          NSArray * arrayTag = [item objectForKey:@"tags"];
                          if (  arrayTag ){
                              NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:arrayTag];
                              message.tags = arrayData;
                          }else{
                              message.tags = nil;
                          }
                          
                          NSArray * arrayTagAuteurs = [item objectForKey:@"tags_auteurs"];
                          if (  arrayTagAuteurs ){
                              NSData *arrayDataTagsAuteurs = [NSKeyedArchiver archivedDataWithRootObject:arrayTagAuteurs];
                              message.tags_auteurs = arrayDataTagsAuteurs;
                          }else{
                              message.tags_auteurs = nil;
                          }
                          
                          message.estDefi = [item objectForKey: @"est_defi"];
                          
                          message.defi_valide = [item objectForKey: @"defi_valide"];
                          
                          // Si le message n'etait pas dans la bdd ,c'est un nouveau message.
                          if ( !estDejaDansBdd)
                          message.estLu = [NSNumber numberWithBool:NO];
                          
                          message.parent= [f numberFromString:[item objectForKey:@"parent"]];
                          
                          if ( [item objectForKey:@"medaille"] == [NSNull null]){
                              message.medaille= @"";
                          }else{
                              message.medaille= [item objectForKey:@"medaille"];
                          }
                          
                          
                          if ( [item objectForKey:@"owner_rank_name"] == [NSNull null]){
                              message.owner_rank_name= @"";
                          }else{
                              message.owner_rank_name= [item objectForKey:@"owner_rank_name"];
                          }
                          
                          if ( [item objectForKey:@"owner_rank_id"] == [NSNull null]){
                              message.owner_rank_id = @"";
                          }else{
                              message.owner_rank_id= [item objectForKey:@"owner_rank_id"];
                          }
                          message.user_a_vote= [f numberFromString:[item objectForKey:@"user_a_vote"]];
                          
                          [UtilsCore sauvegardeUtilisateur:message.owner_guid name:[item objectForKey:@"owner_username"] url:[item objectForKey:@"owner_avatar_medium"]];
                          
                          //DLog(@"message fin");
                      }
                      
                  }
                  
                  NSError *error;
                  [context save:&error];
                  //DLog(@"%@",error);
                  
                  if ( messages.count == 1 && [params objectForKey:@"index_tableview"] != nil){
                      int index_tableview = [[params objectForKey:@"index_tableview"] integerValue];
                      NSDictionary * userInfo = @{ @"index_tableview" : @(index_tableview) };
                      [[NSNotificationCenter defaultCenter] postNotificationName:@"messageActualise" object:nil userInfo:userInfo];
                  }else{
                      //Post d'une notification de fin d'actualisation
                      [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesActualises" object: nil];
                  }
                 
                  
                 
//                  
//                  //Fin d'activity (au cas ou il y en a un)
//                  
//                  id rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
//                  
//                  if ([rootController isKindOfClass:NSClassFromString(@"RootVC")]) {
//                      [[[(RootVC*)rootController centerPanel] view ] hideToastActivity];
//                  }
                  
              } errorMessage:@"Veuillez vous connecter au réseau pour actualiser vos messages."
              activityMessage:nil];
    
    if (!success) {
        
        //Post d'une notification de fin d'actualisation
        [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
        
    }
}

+ (void) rafraichirMessages{
    NSMutableDictionary * params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                              nil];
    
    // Filtrage des ressources.
    NSMutableArray * arrayIdRessource  = [[NSMutableArray alloc] init];
    NSArray * ressources = [self getAllRessources];
    for (Ressource * res in ressources) {
        [arrayIdRessource addObject:res.id_ressource] ;
    }
    NSError * error = [[NSError alloc]init];
    NSData* jsonDataRes = [NSJSONSerialization dataWithJSONObject:arrayIdRessource
                                                          options:NSJSONWritingPrettyPrinted error:&error];
    NSString * jsonStringRes = [[NSString alloc] initWithData:jsonDataRes encoding:NSUTF8StringEncoding];
    [params setObject:jsonStringRes forKey:@"filtre_ressource"];
    // Fin de filtrage des ressources.
//    
//    id rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
//    if ([rootController isKindOfClass:NSClassFromString(@"RootVC")]) {
//        [[[(RootVC*)rootController centerPanel] view ]  makeToastActivity:@"center" andMessage:@"Actualisation des messages..."];
//    }    
    
    [self rafraichirMessagesWithParams:params];
}

+ (void)rafraichirMessagesFromRessource:(NSNumber *)id_ressource{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_ressource, @"id_ressource", nil];
    [self rafraichirMessagesWithParams:params];
}
     

+ (void) rafraichirMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_ressource, @"id_ressource",
                             nom_page,@"nom_page",nil];
    [self rafraichirMessagesWithParams:params];
    
}
     
+ (void) rafraichirMessagesFromUser:(NSNumber *)owner_guid{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             owner_guid, @"owner_guid", nil];
    [self rafraichirMessagesWithParams:params];
    
}

+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_message, @"id_message", nil];
    [self rafraichirMessagesWithParams:params];
    
}
+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message indexTableView:(NSNumber *)indexTableView{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_message, @"id_message",indexTableView, @"index_tableview", nil];
    [self rafraichirMessagesWithParams:params];
    
}

+ (NSArray *) getMessagesFromPredicate:(NSPredicate *)predicate{
    //DLog(@"");
    [self initContext];
    NSError *error;
	NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"time_created"
                                        ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    return array;
}

+ (NSArray *) getMessagesFromPredicate:(NSPredicate *)predicate sortDescriptor:(NSSortDescriptor*)sortDescriptor{
    //DLog(@"");
    [self initContext];
    NSError *error;
	NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    return array;
}

+ (Message *) getMessage:(NSNumber *)id_message{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_message == %@)", id_message];
    Message *message = [[self getMessagesFromPredicate:predicate] objectAtIndex:0];
    return message;
}



+ (void) setMessageLu:(NSNumber *)id_message{
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                                @"(id_message == %@) AND ( estLu == 0)",id_message];
    [UtilsCore messagesLus:predicateLu];
}

+ (NSArray * ) getReponsesFromMessage:(NSNumber *)id_message{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(parent == %@)", id_message];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"time_created"
                                        ascending:YES];
    return [self getMessagesFromPredicate:predicate sortDescriptor:sortDescriptor];
}

+ (NSArray * ) getMessagesFromUser:(NSNumber *)owner_guid{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(owner_guid == %@)", owner_guid];
    return [self getMessagesFromPredicate:predicate];
}
+ (NSArray * ) getMessagesFromUsers:(NSArray *)id_utilisateurs{
    // On forme une suite de conditions en OU. le predicate de depart de ne dois pas etre vide , donc on met une valeur impossible
    NSPredicate * finalPredicate = [NSPredicate predicateWithFormat:
                                    @"(id_ressource == nil)"];  
    
    for (NSNumber * id_utilisateur  in id_utilisateurs) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"(owner_guid == %@)", id_utilisateur];
        
       finalPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:[NSArray arrayWithObjects:finalPredicate, predicate, nil]];
    }
   
    
    return [self getMessagesFromPredicate:finalPredicate];
}

+ (NSArray * ) getMessagesFromRessourceTrans:(NSNumber *)id_ressource{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@)", id_ressource];
    return [self getMessagesFromPredicate:predicate];
}


+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource{
   
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND (parent == nil) AND (nom_page == '' )",id_ressource];
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                                @"(id_ressource == %@) AND ( estLu == 0) AND (nom_page == '' )",id_ressource];
    [UtilsCore messagesLus:predicateLu];
    return [self getMessagesFromPredicate:predicate];
}


+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND (nom_page == %@) AND (parent == nil) AND (nom_tag == 'PAGE' )",id_ressource, nom_page];
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND (nom_page == %@ ) AND ( estLu == 0) AND (nom_tag == 'PAGE' )",id_ressource];
    [UtilsCore messagesLus:predicateLu];
    return [self getMessagesFromPredicate:predicate];
}

+ (NSArray * ) getMessagesFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page tag:(NSString *)tag num_occurence:(NSString *)num_occurence{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND (nom_page == %@) AND (nom_tag == %@ ) AND ( num_occurence == %@ ) AND (parent == nil)",id_ressource, nom_page,tag,num_occurence];

    
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                                @"(id_ressource == %@) AND (nom_page == %@ ) AND (nom_tag == %@ ) AND ( num_occurence == %@ ) AND ( estLu == 0)",id_ressource,nom_page,tag,num_occurence];
    [UtilsCore messagesLus:predicateLu];
    return [self getMessagesFromPredicate:predicate];
}


#pragma mark - OA

+ (NSArray * ) getMessagesFromOAInPage:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @" (nom_page == %@) AND (nom_tag != 'PAGE' ) ", nom_page];
    return [self getMessagesFromPredicate:predicate];
}

#pragma mark - AllMessage

+ (NSArray * ) getAllMessages{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource != nil)"];
    return [self getMessagesFromPredicate:predicate];
}

+ (void) afficherMessage:(NSArray *)array{
    //DLog(@"");
//    for (Message * message in array) {
//        DLog(@"--------------");
//        DLog(@"%@ ",message.id_message);
//        DLog(@"%@ ",message.value);
//        DLog(@"%@ ",message.owner_guid);
//        DLog(@"%@ ",message.owner_username);
//        DLog(@"%@ ",message.id_ressource);
//        DLog(@"%@ ",message.nom_page);
//        DLog(@"%@ ",message.nom_tag);
//        DLog(@"%@ ",message.num_occurence);
//        DLog(@"%@ ",message.reponses);
//        DLog(@"%@ ",message.utilite);
//        DLog(@"%@ ",message.date);
//        DLog(@"--------------");
//    }
}



+ (NSFetchRequest *) getMessageEntityFetch {
//    //DLog(@"");
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:entity];
    return fetch;
}

#pragma mark - Non lus

+ (void) messagesLus:(NSPredicate *)predicate{
    NSError *error;
	
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return;
    }
    for (Message * message in array) {
        message.estLu = [NSNumber numberWithBool:YES];
    }
    [context save:&error];
}

+ (NSNumber *) getNombreMessageNonLuFromPredicate:(NSPredicate *)predicate{
//    //DLog(@"");
    [self initContext];
    NSError *error;
	
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil ){
        return nil;
    }
    //DLog(@"%@",error);
    return [NSNumber numberWithInt:[array count]];
}

+ (NSNumber *) getNombreMessageNonLuFromRessourceTotale:(NSNumber *)id_ressource{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND ( estLu == 0)",id_ressource];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}

+ (NSNumber *) getNombreMessageNonLuFromRessource:(NSNumber *)id_ressource{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND ( estLu == 0) AND (nom_page == '')",id_ressource];
    NSNumber * number = [self getNombreMessageNonLuFromPredicate:predicate];
    
    // Si pas de nouveau message
    // on regarde s'il y a des message lu ,si oui on renvois 0,  sinon on renvois -1.
    if ([ number isEqualToNumber:@0 ]){
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:
                                   @"(id_ressource == %@) AND (nom_page == '' )",id_ressource];
        if ( [[self getNombreMessageNonLuFromPredicate:predicate2] isEqualToNumber:@0]){
            number = @-1;
        }
    }
    
    return number;
}



+ (NSNumber * ) getNombreMessageNonLuFromRessource:(NSNumber *)id_ressource nom_page:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND (nom_page == %@) AND ( estLu == 0) AND (nom_tag == 'PAGE' )",id_ressource, nom_page];
    
    NSNumber * number = [self getNombreMessageNonLuFromPredicate:predicate];
    
    // Si pas de nouveau message
    // on regarde s'il y a des message lu ,si oui on renvois 0,  sinon on renvois -1.
    if ([ number isEqualToNumber:@0 ]){
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:
                                   @"(id_ressource == %@) AND (nom_page == %@) AND (nom_tag == 'PAGE' )",id_ressource, nom_page];
        if ( [[self getNombreMessageNonLuFromPredicate:predicate2] isEqualToNumber:@0]){
            number = @-1;
        }
    }
        
     return number;
}

+ (NSNumber * ) getNombreMessageNonLuFromRessourcePageTotale:(NSNumber *)id_ressource nom_page:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource == %@) AND (nom_page == %@) AND ( estLu == 0)",id_ressource, nom_page];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}



#pragma mark - pulling

+(void)startPullingTimer{
    if ( timer != nil ) return;
    timer = [NSTimer timerWithTimeInterval:60.0f target:[UtilsCore class] selector:@selector(rafraichirMessages) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}
+(void)stopPullingtimer{
    if ( timer == nil ) return;
    [timer invalidate];
    timer = nil;
}


#pragma mark - Tags

+(NSArray *)getAllTag{
//    NSFetchRequest *fetchRequest = [self getMessageEntityFetch];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_ressource != nil)"];
    
    
    NSArray * array = [self getMessagesFromPredicate:predicate];
    NSMutableSet * tags = [[NSMutableSet alloc]init];
    for (Message * mes in array) {
        [tags addObjectsFromArray:mes.getTagsNormalized];
    }


    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES];
    NSArray *sortedArray = [tags sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
    //DLog (@"tags: %@",sortedArray);
    //DLog (@"tags: %@",tags);
    
    return sortedArray;
}

+(NSMutableArray *)getMessageFromTag:(NSString *)tag{
    NSArray * array= [self getAllMessages];
    NSMutableArray * result = [[NSMutableArray alloc] init];
    
    for (Message * mes in array) {
        NSArray * tags = mes.getTagsNormalized;
        if ( [tags containsObject:tag]){
            [result addObject:mes];
        }
    }
    return result;
}

#pragma mark - aide pour pages

+(NSString* ) trimPageUrlForLocalDB:(NSString *)pageUrl{
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@".*/([^/]*).html$" options:0 error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:pageUrl options:0 range:NSMakeRange(0, [pageUrl length])];
    NSString * result  = [pageUrl substringWithRange:[match rangeAtIndex:1]];
    
    return result;
}



#pragma mark - Gestion des ressources

+ (void) enregistrerRessource:(Ressource * )ressource{
    [self initContext];
    NSError * error;
    
    //On vérifie que la ressource n'est pas encore présente
    Ressource * localRessource = [self getRessource:ressource.id_ressource];
    
    if (localRessource == ressource) {
        [context deleteObject:localRessource];
        [context save:&error];
        [self initContext];

    }
    [context insertObject:ressource];
    
    //Sauvegarde
    [context save:&error];
}

+ (void) enregistrerRessourceFromDictionary:(NSDictionary * )ressourceDictionary{

    [self initContext];
    
    [self convertDictionaryToRessource:ressourceDictionary temporary:NO];
    
    //Notif à destination de HomeVC pour qu'il update la table
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unzipFinished" object:self];
    
    //Sauvegarde
    NSError * error;
    [context save:&error];
}

+(Ressource*) convertDictionaryToRessource:(NSDictionary * )ressourceDictionary temporary:(BOOL)temporary{
    
    NSNumberFormatter * numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Ressource" inManagedObjectContext:context];
    Ressource * rs;
    
    if (temporary) {
        rs = [[Ressource alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }
    else{
        rs = [[Ressource alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    }
    
    rs.id_ressource = [numFormatter numberFromString: ressourceDictionary[@"id_ressource"]];
    rs.nom_court = ressourceDictionary[@"nom_court"];
    rs.nom_long = ressourceDictionary[@"nom_long"];
    
    //Enregistrement de l'image en format Data
//    NSURL * iconeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",ressourceDictionary[@"url_mob"], ressourceDictionary[@"icone"]]];
//    UIImage * image = [UIImage imageWithData: [NSData dataWithContentsOfURL: iconeUrl]];
    NSData * data = [NSData dataFromBase64String:ressourceDictionary[@"icone_blob"]];
    UIImage * image = [UIImage imageWithData: data];
    NSData * iconeData = [NSKeyedArchiver archivedDataWithRootObject:image];
    
    rs.icone = iconeData;

    NSData * dataEtablissement = [NSData dataFromBase64String:ressourceDictionary[@"icone_etablissement"]];
    UIImage * imageEtablissement = [UIImage imageWithData: dataEtablissement];
    NSData * iconeDataEtablissement = [NSKeyedArchiver archivedDataWithRootObject:imageEtablissement];
    
    rs.icone_etablissement = iconeDataEtablissement;
    
    NSArray * documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory = [documentPath objectAtIndex:0];
    
    //Déprécié. Utiliser getPath.
    //Retourne le chemin complet au moment du download, incluant le hash d'appli de l'appareil.
    //Fait crasher lors des mises à jour, car le hash d'appli a changé, et le chemin est faux du coup.
    rs.path = [NSString stringWithFormat:@"%@/%@",documentDirectory,ressourceDictionary[@"nom_court"]];
    
    rs.etablissement = ressourceDictionary[@"etablissement"];
    rs.theme = ressourceDictionary[@"theme"];
    
    rs.licence = ressourceDictionary[@"licence"];
    rs.auteur = ressourceDictionary[@"auteur"];
    
    rs.taille = [numFormatter numberFromString:ressourceDictionary[@"taille"]];
    
    rs.description_res = ressourceDictionary[@"description"];
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    //TODO: Meilleur formatage des dates, surtout de la date de download
    rs.date_release =  [dateFormatter dateFromString:ressourceDictionary[@"date_release"]];
    rs.date_update = [dateFormatter dateFromString:ressourceDictionary[@"date_update"]];
    rs.date_downloaded = [NSDate date];
    
    rs.index = [NSNumber numberWithInt:0];
    rs.url_mob = ressourceDictionary[@"url_mob"];
    rs.url_web = ressourceDictionary[@"url_web"];
    rs.visible = [numFormatter numberFromString: ressourceDictionary[@"visible"]];
    //    rs.visible = [NSNumber numberWithInt:[ressourceDictionary[@"visible"] intValue]];
    
    return rs;
}
+(void) updateRessources:(NSArray*)ressources
{
    //ressources = toutes les ressources encore présentes sur le dashboard au moment de la validation de la suppression
    //Cad non supprimée par l'utilisateur
    
    //Gestion de la suppression des ressources
    //On récupère toutes les ressources en base
    NSMutableArray * ressourceToDelete = [[self getAllRessources] mutableCopy];
    
    //On fait un diff avec celles gardées par l'utilisateur
    [ressourceToDelete removeObjectsInArray:ressources];

    //Et on les enlève.
    for (Ressource * currentRessource in ressourceToDelete) {
        [context deleteObject:currentRessource];
    }
    
    //On remet les index à jour
    for (Ressource * currentRessource in ressources) {
        [self setIndex:[ressources indexOfObject:currentRessource] toRessource:currentRessource.id_ressource];
    }
    
    
    //Maintenant, on supprime tous les messages liés à ces ressources supprimées
    for (Ressource * deletedRessource in ressourceToDelete) {
        //On récupère dans le Core tous les messages de la ressource
        NSArray * messagesToDelete = [self getMessagesFromRessourceTrans:deletedRessource.id_ressource];
        
        //On indique qu'on souhaite la suppression de cet objet
        for (Message * messageToDelete in messagesToDelete) {
            [context deleteObject:messageToDelete];
        }
        
    }

    //Sauvegarde des ressources
    //Et on applique la déletion
    NSError * error;
    [context save:&error];
}

+ (void) setIndex:(int)index toRessource:(NSNumber *)id_ressource{
    Ressource * currentRessource = [self getRessource:id_ressource];
    currentRessource.index = [NSNumber numberWithInt:index];
}

+ (Ressource *) getRessource:(NSNumber *)id_ressource{
    
    //TODO: Gérer si la ressource n'existe pas
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Ressource" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_ressource == %@)", id_ressource];
    
    [fetch setPredicate:predicate];
    
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération d'une ressource");
        return nil;
    }
    if([array count])
        return array[0];
    else
        return nil;
    
}

+ (NSArray *) getAllRessources{
    
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Ressource" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"index"
                                        ascending:YES];
    
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération de toutes les ressources");
        return nil;
    }
    
    return array;
}

@end
