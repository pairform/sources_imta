//
//  Message.m
//  SupCast
//
//  Created by admin on 28/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic defi_valide;
@dynamic estDefi;
@dynamic estLu;
@dynamic id_message;
@dynamic id_ressource;
@dynamic medaille;
@dynamic nom_page;
@dynamic nom_tag;
@dynamic num_occurence;
@dynamic owner_guid;
@dynamic owner_rank_id;
@dynamic owner_rank_name;
@dynamic parent;
@dynamic supprime_par;
@dynamic tags;
@dynamic tags_auteurs;
@dynamic time_created;
@dynamic time_updated;
@dynamic user_a_vote;
@dynamic utilite;
@dynamic value;

-(NSArray*) getTags {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[self tags]];
}
-(NSArray*) getTagsNormalized {
    NSMutableArray * array = [[NSMutableArray alloc]init] ;
    for (NSString * st in [NSKeyedUnarchiver unarchiveObjectWithData:[self tags]]) {
        [array addObject:[[st stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]]lowercaseString]];
    }
    return [NSArray arrayWithArray:array];
}

-(NSArray*) getTagsAuteurs {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[self tags_auteurs]];
}

@end
