//
//  ModelController.m
//  Test
//
//  Created by Maen Juganaikloo on 22/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "ModelController.h"

#import "WebViewVC.h"

/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */

@interface ModelController()
@end


@implementation ModelController

@synthesize pageVCDelegate;

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (WebViewVC *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard
{   
    // Return the data view controller for the given index.
    if (([self.pageData count] == 0) || (index >= [self.pageData count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    WebViewVC * webView = [storyboard instantiateViewControllerWithIdentifier:@"WebViewVC"];
    webView.pageVCDelegate = pageVCDelegate;
    webView.url = self.pageData[index];

    
    if(index == 0)
    {
        // Create a new view controller and pass suitable data.
        WebViewVC * preloadedNextWebview = [storyboard instantiateViewControllerWithIdentifier:@"WebViewVC"];
        if ( [self.pageData count] > 1)preloadedNextWebview.url = self.pageData[index+1];
    }
    
    if((index > 0) && (index < ([self.pageData count] -1)))
    {
        // Create a new view controller and pass suitable data.
        WebViewVC * preloadedPreviousWebview = [storyboard instantiateViewControllerWithIdentifier:@"WebViewVC"];
        preloadedPreviousWebview.url = self.pageData[index-1];
        
        // Create a new view controller and pass suitable data.
        WebViewVC * preloadedNextWebview = [storyboard instantiateViewControllerWithIdentifier:@"WebViewVC"];
        preloadedNextWebview.url = self.pageData[index+1]; 
    }
    
    if(index == ([self.pageData count] -1))
    {
        // Create a new view controller and pass suitable data.
        WebViewVC * preloadedPreviousWebview = [storyboard instantiateViewControllerWithIdentifier:@"WebViewVC"];
        if ( [self.pageData count] > 1) preloadedPreviousWebview.url = self.pageData[index-1];
    }

    
    return webView;
}

- (NSUInteger)indexOfViewController:(WebViewVC *)viewController
{   
     // Return the index of the given data view controller.
     // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [self.pageData indexOfObject:viewController.url];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(WebViewVC *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(WebViewVC *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageData count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

@end
