//
//  AddCommentViewController.m
//  SupCast
//
//  Created by fgutie10 on 22/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "NouveauMessageController.h"
#import "UtilsCore.h"

@interface NouveauMessageController()
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIButton *boutonPublier;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property(nonatomic,strong) IBOutlet UIButton * gearButton;
@property(nonatomic,strong) NSMutableArray * rightButtonsArray;
@property(nonatomic,strong) NSMutableArray * visibiliteIdArray;
@property(nonatomic,strong) NSMutableArray * visibiliteArray;
@property(nonatomic,strong) NSMutableArray * tagArray;
@property(nonatomic) BOOL  boolDefi;
@property (weak, nonatomic) IBOutlet UIImageView *ivUnderText;
@property CGRect initialFrameForText;
@property (weak, nonatomic) IBOutlet UISwitch *switchTwitter;

@end



@implementation NouveauMessageController


@synthesize textView;
@synthesize boutonPublier;
@synthesize urlPage;
@synthesize idMessage;
@synthesize currentRessource;
@synthesize uiNavigationItem;
@synthesize gearButton;
@synthesize rightButtonsArray;
@synthesize visibiliteIdArray;
@synthesize visibiliteArray;
@synthesize tagArray;
@synthesize boolDefi;
@synthesize idMessageOriginal;
@synthesize nom_tag;
@synthesize num_occurence;
@synthesize ivUnderText;
@synthesize initialFrameForText;
@synthesize switchTwitter;

#pragma mark -
#pragma mark View Load


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    DLog(@"");
    
    [self setTitle:@"Ecrire"];
    
    //Options
    if (tagArray == nil) tagArray = [[NSMutableArray alloc]init];
    if (visibiliteArray == nil ) visibiliteArray = [[NSMutableArray alloc]initWithArray:[NSMutableArray arrayWithObjects:@"Public",nil] ];
    if (visibiliteIdArray == nil ) visibiliteIdArray = [[NSMutableArray alloc]initWithArray:[NSMutableArray arrayWithObjects:@2,nil] ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Ecriture d'un message"];
    
    /*if ( [[self getPreference:@"publishTwitter"] integerValue] == 1){
        [_bCopublier setHidden:YES];
    }*/

    if ( [[self getPreference:@"publishTwitter" ]integerValue ] == 1) {
        [switchTwitter setOn:YES];
    }
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (IS_IPHONE) {
        // navigation button bar
        rightButtonsArray = [uiNavigationItem.rightBarButtonItems mutableCopy] ;
        if ( rightButtonsArray.count == 1){
            gearButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [gearButton setFrame:CGRectMake(5,6,32,32)];
            [gearButton addTarget:self action:@selector(afficherOptionMessage:) forControlEvents:UIControlEventTouchUpInside];
            [gearButton setImage:[UIImage imageNamed:@"gear_64"] forState:UIControlStateNormal];
            UIBarButtonItem *bOption = [[UIBarButtonItem alloc] initWithCustomView:gearButton];
            [rightButtonsArray addObject:bOption];
            uiNavigationItem.rightBarButtonItems = rightButtonsArray;
        }
    }

}

-(void)putDoneEditingButton{
    NSMutableArray     *items = [[NSMutableArray alloc] init];

    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(5,6,32,32)];
    [a1 addTarget:self action:@selector(gestionClavier) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"keyboard_64"] forState:UIControlStateNormal];
    UIBarButtonItem *bOption = [[UIBarButtonItem alloc] initWithCustomView:a1];
    [items addObject:bOption];
    [uiNavigationItem setRightBarButtonItems:items animated:YES];
}

- (IBAction)gestionClavier {
    DLog(@"");
    [uiNavigationItem setRightBarButtonItems:rightButtonsArray animated:YES];
	if([textView isFirstResponder])
    {
		[textView resignFirstResponder];
	}
}

-(IBAction)afficherOptionMessage:(id)sender{
    [self performSegueWithIdentifier: @"messageOptionSegue" sender: self];
}


- (void)keyboardWillShow:(NSNotification *)aNotification
{
    [self putDoneEditingButton];
    
    /*
     Reduce the size of the text view so that it's not obscured by the keyboard.
     Animate the resize so that it's in sync with the appearance of the keyboard.
     */
    
    NSDictionary *userInfo = [aNotification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's
    // coordinate system. The bottom of the text view's frame should align with the top
    // of the keyboard's final position.
    //
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = textView.frame;
    initialFrameForText =  textView.frame;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y-textView.frame.origin.y;
    
    // Get the duration of the animation.
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.textView.frame = newTextViewFrame;
    
    [UIView commitAnimations];
}


- (void)keyboardWillHide:(NSNotification *)aNotification {
    
    NSDictionary *userInfo = [aNotification userInfo];
    
    /*
     Restore the size of the text view (fill self's view).
     Animate the resize so that it's in sync with the disappearance of the keyboard.
     */
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.textView.frame = initialFrameForText;
    
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark Actions 


-(IBAction)sendMessage {
    
    DLog(@"");
	NSString *message = [[NSString alloc] initWithString:textView.text];
    
	if ([message length] != 0)
    {
        if ( switchTwitter.isOn ) [self copublier];
        else [self publierMessage];
    }        
    
	else 
    {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Attention !" andMessage:@"Veuillez composer un message avant de l'envoyer"];
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        [alertView show];
	}
    
}



- (void)copublier {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSString * initialText = [NSString stringWithFormat:@"#%@ #%@ ",@"PairForm",currentRessource.nom_court];
        /*if ( ![urlPage isEqualToString:@""] ){
            initialText = [NSString stringWithFormat:@"%@#%@  ",initialText,urlPage ];
        }*/
        if (textView.text.length + initialText.length > 140){
            //NSRange r = NSMakeRange(0, 140);
            //initialText = [initialText substringWithRange: r];
            [UIPasteboard generalPasteboard].string = textView.text ;
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Tweet" andMessage:@"le message est trop long, il a été inséré dans le presse-papier."];
            [alertView addButtonWithTitle:@"Ok"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            [alertView show];
        }else{
            initialText = [NSString stringWithFormat:@"%@ %@",initialText,textView.text  ];
        }
       
        
        [mySLComposerSheet setInitialText:initialText];
        
        
        //[mySLComposerSheet addURL:[NSURL URLWithString:@"http://...."]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    DLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    DLog(@"Post Sucessful");
                    [self publierMessage];
                    break;
                    
                default:
                    break;
               
            }
            [self dismissViewControllerAnimated:YES completion:nil];
            [self gestionClavier];
            
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
}





#pragma mark - 


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark -
#pragma mark Publication




-(void)publierMessage {
    
    NSMutableDictionary * params;
    NSString * message = [[NSMutableString alloc] initWithString:textView.text];
    
    //debut convertion object to json
    NSError* error = nil;
    NSString *jsonStringVisiblite = @"";
    NSString *jsonStringTag = @"";
    
    // Visibilite et tagarray
    if ( visibiliteIdArray  && tagArray){
        NSData* jsonDataVisiblite = [NSJSONSerialization dataWithJSONObject:visibiliteIdArray
                                                                    options:NSJSONWritingPrettyPrinted error:&error];
        jsonStringVisiblite = [[NSString alloc] initWithData:jsonDataVisiblite encoding:NSUTF8StringEncoding];
        NSData* jsonDataTag = [NSJSONSerialization dataWithJSONObject:tagArray
                                                              options:NSJSONWritingPrettyPrinted error:&error];
        jsonStringTag = [[NSString alloc] initWithData:jsonDataTag encoding:NSUTF8StringEncoding];
    }

    
    // fin conversion object to json
    
    
    if([self idMessage])
        params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:currentRessource.id_ressource, @"id_ressource", [self urlPage], @"nom_page", [self idMessage], @"id_message", message, @"message", jsonStringVisiblite,@"visibilite",jsonStringTag,@"tag",boolDefi,@"est_defi",nil];
    else
       params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:currentRessource.id_ressource, @"id_ressource", [self urlPage], @"nom_page",  message, @"message",jsonStringVisiblite,@"visibilite",jsonStringTag,@"tag",nil];
    if([self idMessage]){
        [params setObject:idMessage forKey:@"nom_page"];
    }
    
    // Si reponse
    if ( idMessageOriginal ) [params setObject:idMessageOriginal forKey:@"id_message_original"];
    // Si objet apprentisage
    if ( nom_tag && num_occurence) {
        [params setObject:nom_tag forKey:@"nom_tag"];
        [params setObject:num_occurence forKey:@"num_occurence"];
    }
    // si defi
    if ( boolDefi){
        [params setObject:@"" forKey:@"est_defi"];
    }

    
    [self post:[NSString stringWithFormat:@"%@/messages/enregistrerMessage.php",sc_server]
            param: params
            callback: ^(NSDictionary *wsReturn) {
                if(![self displayErrors:wsReturn])
                {
                    [UtilsCore rafraichirMessagesFromMessageId:idMessage];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
     
}
#pragma mark - PrepareForSegue et delegate


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"messageOptionSegue"])
    {
        MessageOptionController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        viewC.boolDefi = boolDefi;
        viewC.visibiliteArray = visibiliteArray;
        viewC.visibiliteIdArray = visibiliteIdArray;
        viewC.tagArray = tagArray;
        viewC.id_ressource = currentRessource.id_ressource;
        viewC.idMessageOriginal = idMessageOriginal;
        
        
    }
}

- (void)recupereOptions:(NSMutableArray *)visibilite visibiliteId:(NSMutableArray *)visibiliteId tag:(NSMutableArray *)tag defi: (BOOL) defi {
    boolDefi = defi;
    visibiliteIdArray = visibiliteId;
    visibiliteArray = visibilite;
    tagArray = tag;
    
}

@end