//
//  TelechargementVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "TelechargementVC.h"
#import "UtilsCore.h"
#import "Ressource.h"
#import "MKNetworkKit.h"
#import "FileManager.h"
#import "SIAlertView.h"
#import "GAI.h"

@interface TelechargementVC ()

@end

@implementation TelechargementVC

@synthesize currentRessource = _currentRessource;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Détails"];
    [self fillFields];
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth:640];
    }
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Téléchargement : %@",_currentRessource.nom_court]];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    //    self.actionButton.center = [[self.view viewWithTag:20] center];
    [self resizeSubViews];
}


-(void)viewDidAppear:(BOOL)animated
{
    [self resizeSubViews];

}
-(void) resizeSubViews{
//    [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
    // Do any additional s etup after loading the view.

    CGRect globalFrame = self.contentView.frame;
    globalFrame.size.height += (self.description_res.contentSize.height - self.description_res.frame.size.height);
    
    self.contentView.frame = globalFrame;
    
//    globalFrame.height += frame.size.height - self.description_res.frame.size.height;
//    globalFrame.width = self.view.frame.size.width;
    self.scrollView.contentSize = globalFrame.size;
    
//    CGRect frame = self.description_res.frame;
//    frame.size.height = self.description_res.contentSize.height;
//    self.description_res.frame = frame;
    
    
    NSLayoutConstraint * constraint = self.description_res.constraints[0];
    constraint.constant = self.description_res.contentSize.height;
    
//    [self.scrollView setNeedsLayout];
//
//    for (UIView * view in [self.scrollView subviews]) {
//        [view setNeedsLayout];
//        [view setNeedsUpdateConstraints];
//        [view setNeedsDisplay];
//    }
//    
    
}

-(void) fillFields{
    NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
    [dFormatter setDateFormat:@"dd-MM-yyyy"];
    
    [self.iconeRes setImage:[_currentRessource getIcone]];
    
    [self.nomCourt setText:_currentRessource.nom_court];
    [self.nomLong setText:_currentRessource.nom_long];

    [self.etablissement setText:[NSString stringWithFormat:@"Par %@",_currentRessource.etablissement]];
    [self.theme setText:_currentRessource.theme];
    
    [self.auteur setText:[NSString stringWithFormat:@"Auteur(s) : %@",_currentRessource.auteur]];
    [self.licence setText:[NSString stringWithFormat:@"Licence : %@",_currentRessource.licence]];
    
    [self.created setText:[NSString stringWithFormat:@"Crée le %@",[dFormatter stringFromDate:_currentRessource.date_release]]];
    [self.updated setText:[NSString stringWithFormat:@"Mis à jour le %@",[dFormatter stringFromDate:_currentRessource.date_update]]];
    [self.taille setText:[NSString stringWithFormat:@"%@ Mo",_currentRessource.taille]];
    
    //On place le texte
    [self.description_res setText:_currentRessource.description_res];
    
//	[self resizeSubViews];
    //On déclare que c'est une nouvelle ressource pour l'user
    [self setIsUpdate:NO];
    
    //On adapte le bouton si l'user a déjà cette ressource
    Ressource * localRessource = [UtilsCore getRessource:_currentRessource.id_ressource];
    
    //Si la ressource est déjà téléchargée
    if(localRessource)
    {
        if (localRessource == _currentRessource)
        {
            [self post:[NSString stringWithFormat:@"%@/ressources/majRessource",sc_server] param:@{@"id_ressource": localRessource.id_ressource} callback:^(NSDictionary *wsReturn) {
                
                NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
                [dFormatter setDateFormat:@"yyyy-MM-dd"];
        
                NSDate * distantDate = [dFormatter dateFromString:wsReturn[@"date_update"]];
                [self updateButtonComparingLocalDate:localRessource.date_update distantDate:distantDate];
            }];
        }
        else
            [self updateButtonComparingLocalDate:localRessource.date_update distantDate:_currentRessource.date_update];
    }
    //TODO: Ajustement du centre du bouton au centre de la toolBar
//    self.actionButton.center = [[self.view viewWithTag:20] center];
    
}

-(void)updateButtonComparingLocalDate:(NSDate*)localDate distantDate:(NSDate*)distantDate{
    NSComparisonResult comparaison = [localDate compare:distantDate];
    
    if ((comparaison == NSOrderedDescending) || (comparaison == NSOrderedSame))
    {
        DLog(@"La ressource est à jour");
        [self.actionButton setTitle:@"Ressource à jour" forState:UIControlStateNormal];
        [self.actionButton setImage:[UIImage imageNamed:@"checkmark_24"] forState:UIControlStateNormal];
        [self.actionButton setEnabled:false];
    }
    
    else if ( comparaison == NSOrderedAscending)
    {
        DLog(@"La ressource n'est pas à jour");
        
        [UIView animateWithDuration:0.4 animations:^{
            self.actionButton.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.actionButton setTitle:@"Mettre à jour" forState:UIControlStateNormal];
            [self.actionButton setImage:[UIImage imageNamed:@"update"] forState:UIControlStateNormal];
            
            
            [UIView animateWithDuration:0.6 animations:^{
                self.actionButton.alpha = 1.0;
                //Ajustement du layout du bouton
                [self.actionButton sizeToFit];
            } completion:nil];
        }];

        
        //Flag de maj de ressource passée en notification
        [self setIsUpdate:YES];
    }
    
    //TODO: Refaire des images de 32x32 (@1x) pour le bouton
    //Ajustement du layout du bouton
    [self.actionButton sizeToFit];
//    
//    CGRect frame = self.actionButton.frame;
//    frame.size.height = 40;
//    
//    self.actionButton.frame = frame;
}
-(IBAction)download:(id)sender
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"action"    
                                                          action:@"download"
                                                           label:[NSString stringWithFormat:@"%@ : téléchargé", _currentRessource.nom_court]          
                                                           value:nil] build]];    
    
    [self.actionButton setTitle:@"Téléchargement en cours..." forState:UIControlStateNormal];
    [self.actionButton setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
    [self.actionButton setEnabled:false];
    
    //TODO: Refaire des images de 32x32 (@1x) pour le bouton
    //Ajustement du layout du bouton
    [self.actionButton sizeToFit];
    
    CGRect frame = self.actionButton.frame;
    frame.size.height = 40;
    
    self.actionButton.frame = frame;
    
    //Notif de début de téléchargement, pour HomeVC
    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadStarted" object:_currentRessource userInfo:@{@"isUpdate" : [NSNumber numberWithBool:self.isUpdate]}];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = paths[0];
    
	NSString *downloadPath = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip", _currentRessource.nom_court]];
    
    NSString * url = [NSString stringWithFormat:@"%@.zip", _currentRessource.url_mob];
    
    MKNetworkEngine* engine = [[MKNetworkEngine alloc]
                               initWithHostName:@"SupCast" customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithURLString:url];
    
    [op addDownloadStream:[NSOutputStream outputStreamToFileAtPath:downloadPath append:YES]];
    
    [engine enqueueOperation:op];
    
    [op onDownloadProgressChanged:^(double progress) {
        //Notif de la progression pour changer l'indicateur dans HomeVC
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgressed" object:_currentRessource userInfo:@{@"progress": [NSNumber numberWithDouble:progress], @"isUpdate" : [NSNumber numberWithBool:self.isUpdate]}];
        
//        DLog(@"%.2f", progress*100.0);
    }];
    
    [op addCompletionHandler:^(MKNetworkOperation* completedRequest) {
        
        //Notif à destination du rootNavigation, pour afficher un toast de fin de DL
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadToast" object:_currentRessource];
        
        [FileManager unzipFileAtPath:downloadPath];
        [UtilsCore enregistrerRessource:_currentRessource];
        [UtilsCore rafraichirMessagesFromRessource:_currentRessource.id_ressource];
        
        DLog(@"%@", completedRequest);
        
        //Notif à destination de HomeVC pour qu'il update la table
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadFinished" object:_currentRessource userInfo:@{@"isUpdate" : [NSNumber numberWithBool:self.isUpdate]}];
        
        [self.actionButton setTitle:@"Ressource à jour" forState:UIControlStateNormal];
        [self.actionButton setImage:[UIImage imageNamed:@"checkmark_24"] forState:UIControlStateNormal];
        [self.actionButton setEnabled:false];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
            
        DLog(@"%@", error);
        //Notif à destination du HomeVC, pour enlever la ressource en cours de DL
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadError" object:_currentRessource userInfo:@{@"isUpdate" : [NSNumber numberWithBool:self.isUpdate]}];
        //Notif à destination du rootNavigation, pour afficher un toast d'erreur de DL
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadErrorToast" object:_currentRessource];
//        
//        if (error.code == -1001) {
//            SIAlertView * alert = [[SIAlertView alloc] initWithTitle:@"Erreur" andMessage:[error localizedRecoverySuggestion]];
//            [alert show];
//            
//        }
        
    }];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
