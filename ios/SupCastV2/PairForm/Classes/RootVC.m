//
//  RootVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "RootVC.h"

@interface RootVC ()

@end

@implementation RootVC

//-(NSUInteger)supportedInterfaceOrientations{
//
//    if([self.navigationController.presentedViewController isKindOfClass:NSClassFromString(@"InsideRessourceVC")])
//    {
//        return UIInterfaceOrientationPortraitUpsideDown;
//    }
//    else return UIInterfaceOrientationMaskAllButUpsideDown;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) awakeFromNib
{
    self.recognizesPanGesture = false;
    self.shouldResizeRightPanel = true;
//    [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"leftViewController"]];
    [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"centerVC"]];
    [self setRightPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"rightVC"]];

    [[[(UINavigationController*)[self centerPanel] topViewController] navigationItem] setRightBarButtonItem:[self customRightButtonForCenterPanel]];
    
    //Rajout d'un splashScreen pour faire une transition entre le default image et l'écran principal   

    NSString * imageFile;
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    imageFile = (screenHeight == 568.0f) ? @"Default-568h.png" : @"Default.png";
    
    
    
    
    
    UIImageView * splashScreen = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageFile]];
    
    //Ajout de la marge de la status bar
    CGRect frame = splashScreen.frame;
    frame.origin.y = 20;
//    frame.origin.y = [UIApplication sharedApplication].statusBarFrame.size.height;
    splashScreen.frame = frame;
    
    //Tag pour futur récupération
    [splashScreen setTag:1000];
    
    [[[self centerPanel] view] addSubview:splashScreen];
    [[[self centerPanel] view] bringSubviewToFront:splashScreen];
    [self performSelector:@selector(killSplashScreen) withObject:nil afterDelay:0];

}

- (void)killSplashScreen {
    //Alpha transition
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [[[self centerPanel] view] viewWithTag:1000].alpha = 0.0;
        
        [[[self centerPanel] view] viewWithTag:1000].transform = CGAffineTransformScale(CGAffineTransformIdentity, 3.f, 3.f);
    } completion:(void (^)(BOOL)) ^{
        [[[[self centerPanel] view] viewWithTag:1000] removeFromSuperview];
    }];
}
@end
