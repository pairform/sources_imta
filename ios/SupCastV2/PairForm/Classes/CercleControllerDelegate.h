//
//  CercleControllerDelegate.h
//  SupCast
//
//  Created by admin on 06/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CercleControllerDelegate
- (void)actionAPartirDeCercle:(NSNumber*) idCercle nomCercle:(NSString *) nomCercle;
@end
