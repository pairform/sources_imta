//
//  EnregistrementVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "EnregistrementVC.h"
#import "SIAlertView.h"
#import "RootVC.h"

#define INTERESTING_TAG_NAMES @"nom", @"message", nil

@interface EnregistrementVC ()
{
    NSMutableArray * textFieldsArray;
}
@end
@implementation EnregistrementVC

@synthesize nomTextField;
@synthesize prenomTextField;
@synthesize eMailTextField;
@synthesize pseudoTextField;
@synthesize motDePasseTextField;
@synthesize nomUtilisateur;
@synthesize prenomUtilisateur;
@synthesize eMailUtilisateur;
@synthesize pseudoUtilisateur;
@synthesize motDePasseUtilisateur;
@synthesize messageString;

#pragma mark - Init

-(void)viewDidLoad {
    DLog(@"");
	[super viewDidLoad];
//    [TestFlight passCheckpoint:@"Enregistrement d'un nouvel utilisateur"];
    [self setTitle:@"Nouveau Compte"];
    
    // Do any additional setup after loading the view.
    
    //Ecran de validation de création de compte à cacher dès le départ
    [[self.view viewWithTag:1000] setHidden:YES];
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [self.pageController.view setFrame:self.view.bounds];
    
    NSArray * pages = [[[self view] subviews] copy];
    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:[pages count]];
    textFieldsArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < ([pages count] -1); i++) {
        [tempArray addObject:[self viewControllerFromUIView:pages[i]]];
        [[[[self view] subviews] objectAtIndex:0] removeFromSuperview];
        
        [textFieldsArray addObject:[[pages[i] subviews] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            return [evaluatedObject isKindOfClass:NSClassFromString(@"UITextField")];
            }]]
         ];

    }
    
    _dataSource = [tempArray copy];
    
    [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:0]]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:nil];
    
    [[UIPageControl appearanceWhenContainedIn:self.class, nil] setPageIndicatorTintColor:[UIColor lightGrayColor]];
    [[UIPageControl appearanceWhenContainedIn:self.class, nil] setCurrentPageIndicatorTintColor:[UIColor grayColor]];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    self.pageController.doubleSided = NO;
    self.pageController.delegate = self;

    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    UIButton * boutonValider = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [boutonValider setFrame:CGRectMake(5,6,32,32)];
    [boutonValider setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [boutonValider addTarget:self action:@selector(enregistrer) forControlEvents:UIControlEventTouchUpInside];
    [boutonValider setBackgroundImage:[UIImage imageNamed:@"paper_plane_64"] forState:UIControlStateNormal];
    [boutonValider setShowsTouchWhenHighlighted:true];
    [boutonValider setReversesTitleShadowWhenHighlighted:true];
    
    UIBarButtonItem *itemValider = [[UIBarButtonItem alloc] initWithCustomView:boutonValider];
    
    [UIView animateWithDuration:0.25
                          delay:0
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
        self.navigationItem.rightBarButtonItem.customView.alpha = 0;
    }
                     completion:^(BOOL finished) {
                         [self.navigationItem setRightBarButtonItem:itemValider];
                         itemValider.customView.alpha = 0;
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options: UIViewAnimationCurveEaseOut
                                          animations:^{
                                              itemValider.customView.alpha = 1;
                                          }
                                          completion:nil];

    }];
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

#pragma mark -
#pragma mark PageView Delegates

-(UIViewController*)viewControllerFromUIView:(UIView*)view{
    
    UIViewController * newVC = [[UIViewController alloc]  init];
    CGRect frame = view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    
    view.frame = frame;
    [newVC setView:view];
    
    return newVC;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    // Return the data view controller for the given index.
    if (([_dataSource count] == 0) || (index >= [_dataSource count])) {
        return nil;
    }
    return [_dataSource objectAtIndex:index];
    
}

- (NSUInteger)indexOfViewController:(UIViewController *)viewController
{
    // Return the index of the given data view controller.
    // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [_dataSource indexOfObject:viewController];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index == [_dataSource count]){
        return nil;
    }
    return [self viewControllerAtIndex:index];
}
//
//-(void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
//{
//    if ([self indexOfViewController:pendingViewControllers[0]] == ([_dataSource count] -1)) {
//        
//        [self enregistrer];
//    }
//}
//
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
//    if ([self indexOfViewController:previousViewControllers[0]] == ([_dataSource count] -2)) {
//        UIButton * boutonValider = [UIButton buttonWithType:UIButtonTypeCustom];
//        
//        [boutonValider setFrame:CGRectMake(5,6,32,32)];
//        [boutonValider setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [boutonValider addTarget:self action:@selector(enregistrer) forControlEvents:UIControlEventTouchUpInside];
//        [boutonValider setBackgroundImage:[UIImage imageNamed:@"paper_plane_64"] forState:UIControlStateNormal];
//        [boutonValider setShowsTouchWhenHighlighted:true];
//        [boutonValider setReversesTitleShadowWhenHighlighted:true];
//        
//        UIBarButtonItem *itemValider = [[UIBarButtonItem alloc] initWithCustomView:boutonValider];
//        [self.navigationItem setRightBarButtonItem:itemValider];
//
//        
//    }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return [_dataSource count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]];
}

#pragma mark - Textfields Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    //Récupération des indexs
    int indexEcran = [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]];
    int indexTextfield = [textFieldsArray[indexEcran] indexOfObject:textField];
    
    //On check si le textfield est le dernier de son écran
    if (indexTextfield != ([textFieldsArray[indexEcran] count] -1)) {
        //Si non,
        UITextField * nextField = textFieldsArray[indexEcran][indexTextfield +1];
        [nextField becomeFirstResponder];
    }
    //Si oui, on switch à l'écran suivant
    else {
        //Incrémentation de l'index
        indexEcran++;
        
        
        __weak NSArray * tfArray = textFieldsArray;
        
        //Présentation de l'écran suivant
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:indexEcran]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
                
            //S'il y a un textfield sur l'écran
            if([tfArray[indexEcran] count])
            {
                //On lui met le focus
                UITextField * nextField = tfArray[indexEcran][0];
                [nextField becomeFirstResponder];
            }
            //Sinon, rien.
        }];
        
    }
    
    return NO;
}

#pragma mark - Enregistrement

-(void)enregistrer {
    DLog(@"");
	
    [self.view endEditing:YES];
    
	//On vérifie que tous les champs du formulaire sont remplis
	self.nomUtilisateur = nomTextField.text;
	self.prenomUtilisateur = prenomTextField.text;
	self.eMailUtilisateur = eMailTextField.text;
	self.pseudoUtilisateur = pseudoTextField.text;
	self.motDePasseUtilisateur = motDePasseTextField.text;
	
    [nomTextField resignFirstResponder];
    [prenomTextField resignFirstResponder];
    [eMailTextField resignFirstResponder];
    [pseudoTextField resignFirstResponder];
    [motDePasseTextField resignFirstResponder];
    
	BOOL alerted = NO;
	
	NSString *vide = @"";
	
	if ([self.nomUtilisateur isEqualToString: vide] || [self.prenomUtilisateur isEqualToString: vide] || [self.eMailUtilisateur isEqualToString: vide] ||
		[self.pseudoUtilisateur isEqualToString: vide] || [self.motDePasseUtilisateur isEqualToString: vide]) {
		
		
        [self.view makeToast:@"Veuillez remplir toutes les données du formulaire."
                    duration:3.0
                    position:@"bottom"
                       title:@"Attention"
                       image:[UIImage imageNamed:@"flag_48" ]];		
		alerted = YES;
		
	}
	
	//Vérification compte mail
	NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
	NSString *emailUtilisateurMinuscules = [self.eMailUtilisateur lowercaseString];
	BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:emailUtilisateurMinuscules];
	
	if (!myStringMatchesRegEx && !alerted) {
		
        [self.view makeToast:@"Veuillez saisir une adresse valide."
                    duration:3.0
                    position:@"bottom"
                       title:@"Attention"
                       image:[UIImage imageNamed:@"flag_48" ]];
		alerted = YES;
		
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
	}
	
	//Le pseudo doit comporter au moins 4 caractères (restriction imposée par Elgg)
	if((self.pseudoUtilisateur.length < 4) && !alerted) {
		
        [self.view makeToast:@"Le nom d'utilisateur doit comporter au moins 4 caractères."
                    duration:3.0
                    position:@"bottom"
                       title:@"Attention"
                       image:[UIImage imageNamed:@"flag_48" ]];
		alerted = YES;
        
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
	}
	
	//Le mot de passe doit comporter au moins 6 caractères (restriction imposée par Elgg)
	if((self.motDePasseUtilisateur.length < 6) && !alerted) {
		
        [self.view makeToast:@"Le mot de passe doit comporter au moins 6 caractères."
                    duration:3.0
                    position:@"bottom"
                       title:@"Attention"
                       image:[UIImage imageNamed:@"flag_48" ]];
        
		alerted = YES;
        
        int direction = [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]] >= 1 ? UIPageViewControllerNavigationDirectionReverse : UIPageViewControllerNavigationDirectionForward;
        
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:1]] direction:direction animated:YES completion:nil];
		
	}
	//Les deux mots de passes doivent être identiques
    if(![self.motDePasseTextField.text isEqualToString:self.motDePasseTextField2.text] && !alerted) {
		
        [self.view makeToast:@"Les deux mots de passes ne sont pas identiques."
                    duration:3.0
                    position:@"bottom"
                       title:@"Attention"
                       image:[UIImage imageNamed:@"flag_48"]];
        
		alerted = YES;
		
        int direction = [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]] >= 1 ? UIPageViewControllerNavigationDirectionReverse : UIPageViewControllerNavigationDirectionForward;
        
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:1]] direction:direction animated:YES completion:nil];
	}
	
	//Vérification nom utilisateur: caractères alphanumériques
	NSString *loginRegEx = @"^[0-9a-zA-Z]*$";
	NSPredicate *loginRegExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", loginRegEx];
	BOOL stringMatchesRegEx = [loginRegExPredicate evaluateWithObject:self.pseudoUtilisateur];
	
	if (!stringMatchesRegEx && !alerted) {
		
        [self.view makeToast:@"Votre nom d'utilisateur doit comporter uniquement des caractères alphanumériques [a-z 0-9]."
                    duration:3.0
                    position:@"bottom"
                       title:@"Attention"
                       image:[UIImage imageNamed:@"flag_48" ]];
        
        
		alerted = YES;
		
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
	}	
	
	//Si tout est OK
	if (![self.nomUtilisateur isEqualToString: vide] && ![self.prenomUtilisateur isEqualToString: vide] && ![self.eMailUtilisateur isEqualToString: vide] &&
		![self.pseudoUtilisateur isEqualToString: vide] && ![self.motDePasseUtilisateur isEqualToString: vide] && myStringMatchesRegEx &&
		(self.pseudoUtilisateur.length >= 4) && (self.motDePasseUtilisateur.length >= 6) && stringMatchesRegEx && [self.motDePasseTextField.text isEqualToString:self.motDePasseTextField2.text]) {
		
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"CGU" andMessage:@"Etes-vous d'accord avec les conditions d'utilisation de cette application ?"];
        [alertView addButtonWithTitle:@"Non"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Refuse les conditions");
                              }];
        [alertView addButtonWithTitle:@"Oui"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Accepte les  conditions");
                                  [self inscription];
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
//        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        
        [alertView show];
        
	}
    
}


-(void)inscription
{
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             self.nomUtilisateur,@"name",
                             self.prenomUtilisateur,@"surname",
                             self.eMailUtilisateur,@"email",
                             self.pseudoUtilisateur,@"username",
                             self.motDePasseUtilisateur, @"password",
                             self.motDePasseUtilisateur, @"password2",
                             self.etablissementTextField.text, @"etablissement", nil];
    
    [self post:[NSString stringWithFormat:@"%@/compte/registerCompte.php",sc_server]
         param:params
      callback:^(NSDictionary *wsReturn) {
          NSString * status = [wsReturn objectForKey:@"status"];
          
          if ([status isEqualToString: @"ok"]) {
              
              UIView * successView = [self.view viewWithTag:1000];
              CGRect frame = successView.frame;
              frame.origin.x = 0;
              successView.frame = frame;
              
              successView.alpha = 0;

              [successView setHidden:NO];
              [self.view bringSubviewToFront:successView];
              
              [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  successView.alpha = 1.0;
                  self.pageController.view.alpha = 0.0;
              } completion:(void (^)(BOOL)) ^{
                  [self.pageController.view setHidden:YES];
              }];
              self.navigationItem.rightBarButtonItem = [(RootVC*)[self sidePanelController] customRightButtonForCenterPanel];
          }
          else
          {
              NSString * message = [wsReturn objectForKey:@"message"];
              
              if ([message isEqualToString:@"empty"]) {
                  
                  
                  [self.view makeToast:@"Tous les champs obligatoires du formulaire n'ont pas été renseignés."
                              duration:3.0
                              position:@"bottom"
                                 title:@"Attention"
                                 image:[UIImage imageNamed:@"delete_48"]];
              }
              
              else if ([message isEqualToString:@"username"]) {
                  
                  
                  [self.view makeToast:@"Votre nom d'utilisateur est déjà utilisé.\nVeuillez en choisir un autre."
                              duration:3.0
                              position:@"bottom"
                                 title:@"Attention"
                                 image:[UIImage imageNamed:@"delete_48"]];
              }
              
              else if ([message isEqualToString: @"email"]) {
                  
                  [self.view makeToast:@"Votre adresse mail est déjà utilisée.\nVeuillez en saisir une autre."
                              duration:3.0
                              position:@"bottom"
                                 title:@"Attention"
                                 image:[UIImage imageNamed:@"delete_48"]];
              }
              else if ([message isEqualToString:@"registerbad"] || (status == NULL))
              {
                  
                  
                  [self.view makeToast:@"Erreur d'enregistrement : veuillez contacter le CAPE de l'Ecole des Mines de Nantes."
                              duration:3.0
                              position:@"bottom"
                                 title:@"Attention"
                                 image:[UIImage imageNamed:@"delete_48"]];
              }
              else
              {
                  [self.view makeToast:message
                              duration:3.0
                              position:@"bottom"
                                 title:@"Attention"
                                 image:[UIImage imageNamed:@"delete_48"]];
              }
          }
        }
     errorMessage:@"Vous devez être connecté à Internet pour créer un compte."
     activityMessage:nil];
}


- (IBAction)backgroundTap:(id)sender {
    DLog(@"");
	[nomTextField resignFirstResponder];
	[prenomTextField resignFirstResponder];
	[eMailTextField resignFirstResponder];
	[pseudoTextField resignFirstResponder];
	[motDePasseTextField resignFirstResponder];
}

#pragma mark -
#pragma mark Username Persistence Methods

-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}
//
//- (void)viewDidUnload {
//    DLog(@"");
//    [super viewDidUnload];
//	self.messageString = nil;
//	self.nomTextField = nil;
//	self.prenomTextField = nil;
//	self.eMailTextField = nil;
//	self.pseudoTextField = nil;
//	self.motDePasseTextField = nil;
//	self.indicator = nil;
//}

@end