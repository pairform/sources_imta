

#import <UIKit/UIKit.h>
#import "URLDataReceiver.h"

/*!
 * @abstract Container of URLDataReceiver instances.
 *
 * This class control the loading of multiple URLs in an asynchronous fashion.
 */
@interface URLLoader : NSObject {
	NSMutableDictionary* fFinished; /**< Receivers that have finished.*/
	NSMutableDictionary* fPending; /**< Receivers that have not started or currently are loading.*/
	id fDelegate;
	NSRecursiveLock* fLock;
}

/*!
 * Delegate must conform to URLLoaderDelegate protocol.
 */
- (id)initWithDelegate:(id)delegate;

/*!
 * @abstract Start loading data.
 */
- (void)load:(NSArray*)URLs;

/*!
 * @abstract Cancel loading.
 *
 * Both fFinished and fPending will be cleared.
 */
- (void)cancel;

/*!
 * @abstract Currently reset has the same effect as cancel.
 */
- (void)reset;

/*!
 * @abstract Return the data from URL specified by aURL.
 *
 * Once loading finished, data receiver is stored in fFinished and indexed by
 * corresponding URL.
 */
- (NSData*)dataForURL:(NSURL*)aURL;

/*!
 * @abstract Return [fFnished objectEnumerator]
 */
- (NSEnumerator*)dataEnumerator;

/*!
 * @abstract Return [fFinished keyEnumerator];
 */
- (NSEnumerator*)urlEnumerator;

@end

/*!
 * @abstract URLLoaderDelegate protocol.
 */
@interface NSObject(URLLoaderDelegate)

/*!
 * @abstract Notify delegate that aURL has started loading.
 */
- (void)URLLoader:(URLLoader*)theURLLoader didBeginURL:(NSURL*)aURL;

/*!
 * @abstract Notify delegate that aURL has finished loading.
 */
- (void)URLLoader:(URLLoader*)theURLLoader didFinishURL:(NSURL*)aURL;

/*!
 * @abstract Notify delegate that all URLs have finished loading.
 */
- (void)URLLoaderDidFinishAll:(URLLoader*)theURLLoader;
@end