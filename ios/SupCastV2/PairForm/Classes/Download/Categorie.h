//
//  Categorie.h
//  SupCast
//
//  Created by Cape EMN on 07/12/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppListViewController.h"
#import "Reachability.h"
#import "WBProgressHUD.h"

@interface Categorie : UIViewController <UITableViewDelegate, UITableViewDataSource>
{

    AppListViewController *aAppListViewController;
    IBOutlet UISegmentedControl *categorieSelector;
    NSMutableArray *listeThemes;
    NSMutableArray *listeEtablissements;
    NSMutableData *receivedData;
    IBOutlet UITableView *aTableView;
    IBOutlet UILabel *labelTitre;
    NetworkStatus   statusConnexionReseau;
    NSURLConnection *categorieConnection;
    NSString *password;
	NSString *niveau;
	NSString *username;
	BOOL estVisiteur;


    WBProgressHUD   *HUD;
}
@property(nonatomic,strong) WBProgressHUD   *HUD;
@property NetworkStatus   statusConnexionReseau;


@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *niveau;
@property(nonatomic,strong) NSString *username;
@property(nonatomic) BOOL estVisiteur;

@property(nonatomic,strong) NSMutableData *receivedData;
@property(nonatomic,strong) NSMutableArray *listeThemes;
@property(nonatomic,strong) NSMutableArray *listeEtablissements;
@property(nonatomic,strong) NSURLConnection *categorieConnection;

-(void)reloadDataWithAnimation:(UITableView*)tableView;
-(void)enregistrerImages;
- (IBAction)changeCategorie:(id)sender;
-(NetworkStatus) etatConnexionReseau;
- (void)showLoadingProgress;
- (void)hideLoadingProgress;
- (void)cancelConnection;
@end
