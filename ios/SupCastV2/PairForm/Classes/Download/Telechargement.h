//
//  Telechargement.h
//  SupCast
//
//  Created by Didier PUTMAN on 25/11/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileMediaLoadOperationProtocol.h"
#import "FileMediaLoadOperation.h"

@interface Telechargement : UIViewController <UIAlertViewDelegate,FileMediaLoadOperationProtocol>
{
    double averageBandwidth;
	long dataBandwidth;
    int nombredetelechargement;
    int nombredefichieratelecharger;
    IBOutlet UITextView *aTextView;
    IBOutlet UIButton *boutonChargement;
    NSMutableDictionary *myDico;    
    NSOperationQueue *maFileOperation;
    BOOL topTelechargement;
    double tailleEffectuee;
}
@property(nonatomic,strong) NSOperationQueue *maFileOperation;
@property(nonatomic,strong) NSMutableDictionary *myDico;
@property(nonatomic) BOOL topTelechargement;

-(IBAction)chargement:(id)sender;
-(void)lancerRecup1:(NSString *)motcle;
-(void)telechargementdesfichiersWeb:(NSString *)fichier;
-(NSString *)telechargementFichier:(NSString *)subDirectory 
                               key:(NSString *)key;
-(void)lancerLoadOperation:(NSDictionary *)anObject;
-(void)finFileMediaLoadOperation:(NSDictionary *)dictResult;
-(double)updateAverageBandwidthWithNewBandwidth:(CGFloat )bandwidthParam
									   dataSize:(NSUInteger)datasizeParam;
- (NSDictionary *)verif;
-(void)lancement;
-(void)addSkipBackupAttributeToPath:(NSString*)path;
-(void)removeNotifUpdate;

@end
