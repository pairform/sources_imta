//
//  AppRecord.m
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright CAPE - Ecole des Mines de Nantes 2010. All rights reserved.
//

#import "AppRecord.h"

@implementation AppRecord

///////////////////////////
@synthesize ressourceupdated;
@synthesize ressourcereleased;
@synthesize ressourceid;
@synthesize ressourcenom;
@synthesize ressourcetitle;
@synthesize ressourceurl;
@synthesize ressourcebaseurl;
@synthesize ressourcefolder;
@synthesize ressourcestructure;
@synthesize ressourcesummary;
@synthesize ressourceicone;
@synthesize ressourcemedias;
@synthesize ressourcemedia;
@synthesize ressourceetablissement;
@synthesize ressourcedomaine;
@synthesize ressourceversion1;
@synthesize ressourceversion2;
@synthesize ressourceversion3;
@synthesize ressourcecatalogue;
@synthesize ressourceelggid;
@synthesize ressourceRessourcesWeb;
@synthesize ressourceparent;
@synthesize entry;
@synthesize entries;
@synthesize ressourceTaille;
@synthesize appID;
@synthesize appTitle;
@synthesize ressourceFolder;
@synthesize appIcone;
@synthesize appListeMedia;
@synthesize appIcon;
@synthesize appLink;
@synthesize appIconData;
@synthesize artist;

- (void)dealloc
{
	DLog(@"");

    
}

@end

