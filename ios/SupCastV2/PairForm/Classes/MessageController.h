//
//  RecupererMessagesViewController.h
//  SupCast
//
//  Created by fgutie10 on 28/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "Message.h"
#import "Ressource.h"
#import "PageVC.h"
#import "InsideRessourceVC.h"
#import "GCTagList.h"
#import "MessageCell.h"
#import "Utilisateur.h"
#import "SIAlertView.h"

typedef enum {
    ModeNouveauMessageNormal,
    ModeNouveauMessageReponse,
    ModeNouveauMessageEdition
} ModeNouveauMessage;

typedef enum {
    ModeMessageNormal,
    ModeMessageTransversal
} ModeMessage;

typedef enum {
    TransversalRessource,
    TransversalUtilisateur,
    TransversalCercle,
    TransversalTag
} Transversal;

@interface MessageController : UITableViewController <UITableViewDelegate, UITableViewDataSource , UIActionSheetDelegate>


@property (strong, nonatomic) NSNumber * idRessource;
@property (strong, nonatomic) NSString * urlPage;
@property (strong, nonatomic) Message * selectedMessage;
@property (strong, nonatomic) NSIndexPath * selectedMessagePath;
@property(nonatomic,strong) Ressource * currentRessource;
@property(nonatomic,strong) NSString * tag;
@property(nonatomic,strong) NSNumber * num_occurence;
@property(nonatomic) ModeNouveauMessage  modeNouveauMessage;
@property(nonatomic) ModeMessage  modeMessage;
@property(nonatomic) Transversal transversal;
@property (nonatomic) NSNumber * id_utilisateur;
@property (nonatomic) NSMutableArray * id_utilisateurs;
@property (nonatomic) NSString * tag_message;

- (IBAction)selectionneProfil:(id)sender;
- (IBAction)selectionneLienVersRessource:(id)sender;

- (IBAction)swipeToLeft:(id)sender;
- (IBAction)swipeToRight:(id)sender;
- (IBAction)singleTapGesture:(id)sender;
- (IBAction)tapGesture:(id)sender;
@end
