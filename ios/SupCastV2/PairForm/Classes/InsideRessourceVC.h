//
//  InsideRessourceVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RXMLElement.h"
#import "Ressource.h"
#import "TDBadgedCell.h"

@interface InsideRessourceVC : UITableViewController


@property (nonatomic, retain) NSMutableArray *arrayForTable;
@property (nonatomic, strong) NSArray * rootArray;
@property (nonatomic, strong) NSMutableArray * subMenuStack;
@property (nonatomic, strong) Ressource * currentRessource;
@property (nonatomic, weak) IBOutlet UIButton * infoButton;
@property (nonatomic, weak) IBOutlet UIImageView * icone_etablissement;
@property (nonatomic, strong) NSString * lastPageUrlReaded;

-(void)checkForUpdate;

@end
