//
//  WebViewVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 22/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "WebViewVC.h"

@interface WebViewVC ()
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *singleTap;
@property  BOOL firstTime ;
@property NSArray * tagArray;

@end

@implementation WebViewVC
@synthesize webView, url,pageVCDelegate,firstTime;
@synthesize tagArray;
#pragma view


- (void)viewDidLoad {
	
    [super viewDidLoad];
//    NSString * urlWithoutExtension = [self.url stringByDeletingPathExtension];
//    NSString * urlRessource = [[NSBundle mainBundle] pathForResource:urlWithoutExtension ofType:@"html"];

//    NSURL * mediaUrl = [NSURL fileURLWithPath:urlRessource];

    NSURL * mediaUrl = [NSURL fileURLWithPath:self.url];
    NSURLRequest * mediaUrlRequest = [[NSURLRequest alloc] initWithURL:mediaUrl];
    
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                            diskCapacity:0
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    
//    self.view.userInteractionEnabled = YES;
//    self.webView.userInteractionEnabled = YES;
//    self.view.multipleTouchEnabled = YES;
//    self.webView.multipleTouchEnabled = YES;
    
    [self.webView setDelegate:self];
    [self.webView loadRequest:mediaUrlRequest];

    firstTime = YES;
//    
//    if (IS_IPAD)
//        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
//    if (IS_IPAD) {
        [self.webView setHidden:YES];
//    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
   
    if (!firstTime)
    {
        [self afficheBadge];
    }
    firstTime = NO;

}

- (void)didReceiveMemoryWarning {
	
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	// Release any cached data, images, etc that aren't in use.
}

#pragma webview delegate
//
//// La redéfinition de cette fonction va permettre d'ouvrir tous les liens de la page avec le navigateur safari
//- (BOOL)webView:(UIWebView *)webView
//shouldStartLoadWithRequest:(NSURLRequest *)request
// navigationType:(UIWebViewNavigationType)navigationType {
//	// On s'assure que le lien que l'on ouvre est local
//    // Sinon, on l'ouvre dans un intent de safari
//    
//    NSString * urlString = [[NSMutableString alloc] initWithString:[[request URL] absoluteString]];
//    NSString *test = [urlString substringToIndex:4];
//	
//	if (![test isEqualToString:@"file"])
//    {
//		[[UIApplication sharedApplication] openURL:[request URL]];
//	}
//    
//    
//    //On check si la requête est une ressource. Si oui, on la push dans le view controller.
//    BOOL isPopUpRes = [urlString rangeOfString:@"/res/"].location != NSNotFound ? YES : NO;
//	
//    BOOL isHTML5 = [urlString rangeOfString:@".eWeb/"].location != NSNotFound ? YES : NO;
//    
//    if(isPopUpRes && !isHTML5)
//    {
//        
//        
//        BOOL isImage = ([urlString rangeOfString:@".jpg"].location != NSNotFound)
//        || ([urlString rangeOfString:@".png"].location != NSNotFound)
//        || ([urlString rangeOfString:@".gif"].location != NSNotFound)
//        ? TRUE : FALSE;
//        
//        
//        if(isImage)
//        {
//            
////            ZoomViewController * uiVC = [[ZoomViewController alloc] initWithPicture:urlString];
////            [self.navigationController pushViewController:uiVC animated:YES];
//        }
//        else
//        {
//            
//            UIViewController * uiVC = [[UIViewController alloc] init];
//            
//            UIWebView * resScenari = [[UIWebView alloc] initWithFrame:self.view.frame];
//            [resScenari loadRequest:request];
//            [uiVC setView:resScenari];
//            [self.navigationController pushViewController:uiVC animated:YES];
//        }
//        //[uiVC shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortrait];
//        
//        
//        return NO;
//    }
//	return YES;
//	
//}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (IS_IPAD) {
        [self.webView stringByEvaluatingJavaScriptFromString:
         [NSString stringWithFormat:@"var viewport = null;"
          "var content = null;"
          "var document_head = document.getElementsByTagName( 'head' )[0];"
          "var child = document_head.firstChild;"
          "while ( child )"
          "{"
          " if ( null == viewport && child.nodeType == 1 && child.nodeName == 'META' && child.getAttribute( 'name' ) == 'viewport' )"
          " {"
          "     viewport = child;"
          "     viewport.setAttribute( 'content' , 'width=640;initial-scale=1.0' );"
          "     break;"
          " }"
          " child = child.nextSibling;"
          "}"
          "if (null == viewport)"
          "{"
          " var meta = document.createElement( 'meta' );"
          " meta.setAttribute( 'name' , 'viewport' );"
          " meta.setAttribute( 'content' , 'width=600;initial-scale=1.0' );"
          " document_head.appendChild( meta );"
          "}"
          ]
         ];
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"OA" ofType:@"css"];
    NSString* js = [NSString stringWithFormat:
                    @"var fileref = document.createElement('link');\n"
                    "fileref.type = \"text/css\";\n"
                    "fileref.rel = \"stylesheet\";\n"
                    "fileref.href = \"%@\";\n"
                    "document.getElementsByTagName('head')[0].appendChild(fileref);\n",path];
    
    
    [self.webView stringByEvaluatingJavaScriptFromString:js];
    [pageVCDelegate selectOATrans:self.webView ];
    // Disable user selection
    [self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none'; document.documentElement.style.webkitTouchCallout='none';"];
    
//    [self.webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none'; document.body.style.KhtmlUserSelect='none'"];
    
    [self afficheBadge];
    
    self.webView.alpha = 0;
    self.webView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self.webView.alpha = 1;
    }];
}

-(void) afficheBadge{
    
    // Badges
    
    NSString * pageUrl = [UtilsCore trimPageUrlForLocalDB:url];
    NSArray * Messages = [UtilsCore getMessagesFromOAInPage:pageUrl];
    NSMutableDictionary * OAinPage = [[NSMutableDictionary alloc]init];
    // Population du dico
    
    
    // ex :
    //  p:10 -> 1  ( 1 nouveau message )
    //  p:9  -> 0  ( 0 nouveau message mais des messages quand même )
    
    
    for (Message * message in Messages) {
        NSString * key = [NSString stringWithFormat:@"%@:%@",message.nom_tag,message.num_occurence];
        NSNumber * oldNumber = [OAinPage objectForKey:key];
        if ( !oldNumber){
            NSNumber * number = [[NSNumber alloc]initWithInt:0];
            [OAinPage setObject:number forKey:key];
        }
        // Si message lu , dico +1
        if ( [message.estLu isEqualToNumber:@0])
            [OAinPage setObject:[NSNumber numberWithInt:[oldNumber intValue] + 1] forKey:key];
        
        
    }
    
    
    
    // Insert avec Js
    
    // Load the JavaScript code from the Resources and inject it into the web page
    NSString *pathJs = [[NSBundle mainBundle] pathForResource:@"JSTools" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:pathJs encoding:NSUTF8StringEncoding error:nil];
    [self.webView  stringByEvaluatingJavaScriptFromString: jsCode];
    
    for (NSString* key in OAinPage) {
        
        NSArray *tagArrayOa = [key componentsSeparatedByString: @":"];
        
        NSString * tag = [tagArrayOa objectAtIndex:0];
        NSNumber * num_occurence = [tagArrayOa objectAtIndex:1];
        
        // call to putBadge
        [self.webView  stringByEvaluatingJavaScriptFromString:
         [NSString stringWithFormat:@"putBadge('%@',%@,%@);",tag,num_occurence,[OAinPage objectForKey:key]]];
        
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
    
}


- (IBAction)singleTap:(id)sender {
    

    


}

- (IBAction)quickLongTap:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan){
        DLog(@"UIGestureRecognizerStateBegan.");
        // Load the JavaScript code from the Resources and inject it into the web page
        NSString *path = [[NSBundle mainBundle] pathForResource:@"JSTools" ofType:@"js"];
        NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        [webView stringByEvaluatingJavaScriptFromString: jsCode];
        
        CGPoint pt = [sender locationInView:webView];
        
        int displayWidth = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.innerWidth"] intValue];
        CGFloat scale = self.webView.frame.size.width / displayWidth;
        
        pt.x /= scale;
        pt.y /= scale;
        
        
//        // convert point from view to HTML coordinate system
//        CGPoint offset  = [webView scrollView].contentOffset ;
//        CGSize viewSize = [webView frame].size;
//        CGFloat windowSizeWidth = [webView window].frame.size.width;
//        
//        CGFloat f = windowSizeWidth / viewSize.width;
//        pt.x = pt.x * f ;//+ offset.x;
//        pt.y = pt.y * f ;//+ offset.y;
//        
        
        // get the Tags at the touch location
        NSString *tagString = [webView stringByEvaluatingJavaScriptFromString:
                               [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%i,%i);",(NSInteger)pt.x,(NSInteger)pt.y]];
        tagArray = [tagString componentsSeparatedByString: @":"];
        DLog(@"%f : %f",pt.x,pt.y);
    }

}


- (IBAction)longTap:(UILongPressGestureRecognizer*)sender {

    if (sender.state == UIGestureRecognizerStateBegan){
        DLog(@"UIGestureRecognizerStateBegan.");
        if ( tagArray != nil && [tagArray count] == 2  ){
            NSString * tag = [tagArray objectAtIndex:0];
            NSNumber * num_occurence = [tagArray objectAtIndex:1];
            DLog(@"%@ %@",tag,num_occurence);
            
            [pageVCDelegate voirMessageDepuisOA:tag num_occurence:num_occurence];
        }
    }
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"messageSegue"]){
        MessageController * messageController = [segue destinationViewController];
    }
    

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}




@end
