//
//  SuccesControllerViewController.m
//  SupCast
//
//  Created by admin on 08/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "SuccesControllerViewController.h"

@interface SuccesControllerViewController ()

@property NSMutableArray * arraySucces;
@end

@implementation SuccesControllerViewController

@synthesize succes;
@synthesize succesNonGagnes;
@synthesize arraySucces;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    arraySucces = [[NSMutableArray alloc]init];
    [arraySucces addObjectsFromArray:succes];
    [arraySucces addObjectsFromArray:succesNonGagnes];
	// Do any additional setup after loading the view.
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arraySucces count];
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"myId";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    if ( [succes containsObject:[arraySucces objectAtIndex:indexPath.row]]){
        UIImage * image = [UIImage imageNamed:[[succes objectAtIndex:indexPath.row] objectForKey:@"image"]];
        if ( image )[imageView setImage:image];
        else [imageView setImage:[UIImage imageNamed:@"succes.png"]];
    }
    else{
       [imageView setImage:[UIImage imageNamed:@"succes_default"]]; 
    }
        
     UILabel *lblName = (UILabel *)[cell viewWithTag:200];
        [lblName setText:[[arraySucces objectAtIndex:indexPath.row] objectForKey:@"succes"]];
    
    UILabel *lblDesc = (UILabel *)[cell viewWithTag:300];
    [lblDesc setText:[[arraySucces objectAtIndex:indexPath.row] objectForKey:@"description"]];
    
    // Configure the cell...
    
    return cell;
}


@end
