//
//  GroupeController.m
//  profil
//
//  Created by admin on 03/05/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "CercleController.h"
#define kAlertViewOne 1
#define kAlertViewTwo 2


@interface CercleController ()
@property (weak, nonatomic) IBOutlet UITableView *cercles;
@property (strong, nonatomic) NSMutableArray *cerclesListe;
@property (strong, nonatomic) NSMutableArray *classesListe;
@property (strong, nonatomic) NSMutableArray *classesRejointesListe;
@property (weak, nonatomic) NSNumber * idCercleSelection;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property (strong, nonatomic) UIButton *bEdit;
@property ( nonatomic) BOOL boolEdit;
@property (strong , nonatomic) UITextField* alertViewText;
@property (weak, nonatomic) IBOutlet UIButton *bRejoindre;
@end



@implementation CercleController

@synthesize boolEdit;
@synthesize bEdit;
@synthesize cerclesListe;
@synthesize classesListe, classesRejointesListe;
@synthesize cercles;
@synthesize alertViewText;
@synthesize bRejoindre;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self connect];
    boolEdit = NO;
    
    if ( self.modeCercle == ModeCercleAjoutDepuisProfil){
        [bRejoindre setEnabled:NO];
    }
    if (self.modeCercle == ModeCercleSelectionVisibilite){
        [self.boutonsView setHidden:YES];
        [self.boutonsView setHeight:0];
    }
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ((self.modeCercle == ModeCercleAjoutDepuisProfil) || (self.modeCercle == ModeCercleSelectionVisibilite)){
        
    }else{
        // navigation button bar
        NSMutableArray     *items = [_uiNavigationItem.rightBarButtonItems mutableCopy] ;
        
        if (IS_IPAD)
        {
            if ( items.count == 0){
                items = [[NSMutableArray alloc] init];
                bEdit = [UIButton buttonWithType:UIButtonTypeCustom];
                [bEdit setFrame:CGRectMake(5,6,32,32)];
                [bEdit addTarget:self action:@selector(ouvrirModeEdition) forControlEvents:UIControlEventTouchUpInside];
                [bEdit setImage:[UIImage imageNamed:@"pencil_64.png"] forState:UIControlStateNormal];
                UIBarButtonItem * bBarButton = [[UIBarButtonItem alloc] initWithCustomView:bEdit];
                [items addObject:bBarButton];
                _uiNavigationItem.rightBarButtonItems = items;
            }
        }
        else
        {
            if ( items.count == 1){
                bEdit = [UIButton buttonWithType:UIButtonTypeCustom];
                [bEdit setFrame:CGRectMake(5,6,32,32)];
                [bEdit addTarget:self action:@selector(ouvrirModeEdition) forControlEvents:UIControlEventTouchUpInside];
                [bEdit setImage:[UIImage imageNamed:@"pencil_64.png"] forState:UIControlStateNormal];
                UIBarButtonItem * bBarButton = [[UIBarButtonItem alloc] initWithCustomView:bEdit];
                [items addObject:bBarButton];
                _uiNavigationItem.rightBarButtonItems = items;
            }
        }
        
    }
}



-(void)ouvrirModeEdition{
    [bEdit removeTarget:self action:@selector(ouvrirModeEdition) forControlEvents:UIControlEventTouchUpInside];
    [bEdit addTarget:self action:@selector(fermerModeEdition) forControlEvents:UIControlEventTouchUpInside];
    boolEdit = YES;
    [cercles reloadData];
    //[cercles setEditing:YES animated:YES];
}

-(void)fermerModeEdition{
    [bEdit removeTarget:self action:@selector(fermerModeEdition) forControlEvents:UIControlEventTouchUpInside];
    [bEdit addTarget:self action:@selector(ouvrirModeEdition) forControlEvents:UIControlEventTouchUpInside];
    boolEdit = NO;
    [cercles reloadData];
    //[cercles setEditing:NO animated:YES];
}

-(void)connect{
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/afficherListeCercles.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          cerclesListe = [wsReturn objectForKey:@"cercles"];
          classesListe = [wsReturn objectForKey:@"classes"];
          classesRejointesListe = [wsReturn objectForKey:@"classesRejointes"];
          [cercles reloadData];
      }];

}


-(void)connect: (void (^)(void)) callbackBis{
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/afficherListeCercles.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          cerclesListe = [wsReturn objectForKey:@"cercles"];
          classesListe = [wsReturn objectForKey:@"classes"];
          classesRejointesListe = [wsReturn objectForKey:@"classesRejointes"];
          callbackBis();
          //[cercles reloadData];
          
      }];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( (NSNull *)cerclesListe == [NSNull null] ) return 0;
    
    switch (section) {
        case 0:
            return cerclesListe.count;
            break;
            
        case 1:
            return classesListe.count;
            break;
            
        case 2:
            return classesRejointesListe.count;
            break;
            
        default:
            return 0;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 30)];
    [label setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    [label setTextColor:[UIColor darkGrayColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    
    NSString *string;

    switch (section) {
        case 0:
            string = @"Mes cercles";
            break;
            
        case 1:
            string =  @"Mes classes";
            break;
            
        case 2:
            string =  @"Classes rejointes";
            break;
            
        default:
            string =  @"...";
            break;
    }
    
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
//    [view setBackgroundColor:[UIColor clearColor]];
    [view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"textured_paper"]]];
    
    UIView *separateur = [[UIView alloc] initWithFrame:CGRectMake(5, 43, tableView.frame.size.width - 10, 1)];
    [separateur setBackgroundColor:[UIColor lightGrayColor]];
    [view addSubview:separateur];
    
    return view;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary * groupe ;
    static NSString *CellIdentifier;
    
    switch (indexPath.section) {
        case 0:
            groupe = [cerclesListe objectAtIndex:indexPath.row];
            CellIdentifier = @"cercle";
            break;
            
        case 1:
            groupe = [classesListe objectAtIndex:indexPath.row];
            CellIdentifier = @"classe";
            break;
            
        case 2:
            groupe = [classesRejointesListe objectAtIndex:indexPath.row];
            CellIdentifier = @"classe";
            break;
            
        default:
            break;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //Si on est dans les classes rejointes
    if(indexPath.section == 2){
        //On cache le bouton d'ajout
        [[cell viewWithTag:4] setHidden:true];
    }
    
    CercleCell *collection = (CercleCell *)[cell viewWithTag:2];
    collection.delegate = collection;
    collection.dataSource = collection;
    
    //On transmet le flag d'édition si on est pas dans le cas d'une classe rejointe
    if (indexPath.section != 2)
        collection.boolEdit = boolEdit;
    //Sinon, on met false d'office.
    else
        collection.boolEdit = false;
    
    collection.members = [groupe objectForKey:@"members"];
    collection.id_cercle = [groupe objectForKey:@"id"];
    collection.index = indexPath;
    [collection reloadData];
    
    UILabel *lNom = (UILabel *)[cell viewWithTag:1];
    [lNom setText:[groupe objectForKey:@"name"]];

    if ((self.modeCercle == ModeCercleAjoutDepuisProfil) || (self.modeCercle == ModeCercleSelectionVisibilite)){
        UIButton * bAdd = (UIButton *)[cell viewWithTag:4];
        [bAdd setHidden:YES];
    }else{
        UIButton * bSelectionner = (UIButton *)[cell viewWithTag:5];
        [bSelectionner setHidden:YES];
    }
    
    if ((indexPath.section == 1) || (indexPath.section == 2)){
        UILabel *lCle = (UILabel *)[cell viewWithTag:3];
        [lCle setText:[groupe objectForKey:@"cle"]];
    }
    
    UIButton *bSupprime = (UIButton *)[cell viewWithTag:6];
    
    //Si la ligne concernée n'est pas la dernière, cad la ligne des classes rejointes
    if (boolEdit && (indexPath.section != 2)){
        [bSupprime setHidden:NO];
    }else{
        [bSupprime setHidden:YES];
    }
    
    return cell;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
- (void)creerCercle:(NSString *)nom {
    
    // GererClasse
   
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:nom,@"nom", nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/creerCercle.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if (![self displayErrors:wsReturn] )
          {
              [self connect:^(void){
                  [cercles beginUpdates];
                  NSIndexPath *newIndexPath ;
                  for ( NSDictionary * cercle in cerclesListe){
                      if ( [[cercle objectForKey:@"name"] isEqualToString:nom] ){
                          int index = [cerclesListe indexOfObject:cercle];
                          newIndexPath = [NSIndexPath indexPathForRow:(index) inSection:0];
                          break;
                      }
                  }
                  [cercles insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                  [cercles endUpdates];
                  [cercles scrollToRowAtIndexPath:newIndexPath
                                 atScrollPosition:UITableViewScrollPositionTop  animated:YES];
              }];
              
          }
      }];
}

- (void)creerClasse:(NSString *)nom {
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:nom,@"nom", nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/creerClasse.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ( ![self displayErrors:wsReturn] )
          {
              [self connect:^(void){
                  [cercles beginUpdates];
                  NSIndexPath *newIndexPath ;
                  for ( NSDictionary * classe in classesListe){
                      if ( [[classe objectForKey:@"name"] isEqualToString:nom] ){
                          int index = [classesListe indexOfObject:classe];
                          newIndexPath = [NSIndexPath indexPathForRow:(index) inSection:1];
                          break;
                      }
                  }
                  [cercles insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                  [cercles endUpdates];
                  [cercles scrollToRowAtIndexPath:newIndexPath
                                 atScrollPosition:UITableViewScrollPositionTop  animated:YES];
                  
                  SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Classe créée"
                                            andMessage:[NSString stringWithFormat:@"Clé de la classe : %@",[wsReturn objectForKey:@"cle"]]];
                  [alertView addButtonWithTitle:@"Ok"
                                           type:SIAlertViewButtonTypeDefault
                                        handler:^(SIAlertView *alertView) {
                                        }];
                  alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
                  [alertView show];
              }];
              
          }
      }];
}

- (IBAction)supprimerCercle:(id)sender {
    NSIndexPath * indexPath = [cercles indexPathForCell:(UITableViewCell *)[[sender superview] superview]];

    NSNumber * id_collection;
    if ( indexPath.section == 0){
        id_collection = [[cerclesListe objectAtIndex:indexPath.row] objectForKey:@"id"];
    }else{
        id_collection = [[classesListe objectAtIndex:indexPath.row] objectForKey:@"id"];
    }
    
    
    //Webservice de suppression
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:id_collection,@"id_collection", nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/supprimerCercle.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          
          if ( ![self displayErrors:wsReturn] )
          {
              //Suppression graphique
              [self connect:^(void){
                  [cercles beginUpdates];
                  //NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
                  [cercles deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                  [cercles endUpdates];
              }];
          }
      }];
    

}



- (IBAction)ajouterMembre:(id)sender {
    //Enregistrement du cercle selectionne , pour ajouter le membre dans ce dernier.
    NSIndexPath * index = [cercles indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
    if ( index.section == 0){
        _idCercleSelection = [[cerclesListe objectAtIndex:index.row] objectForKey:@"id"];
    }else{
        _idCercleSelection = [[classesListe objectAtIndex:index.row] objectForKey:@"id"];
    }
    //le button dirige deja sur la vue RechercheProfil.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"segueAjoutCercle"])
    {
        RechercheProfilController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        viewC.mode = modeAjoutCercle ;
    }
}

- (void)ajouteMembreDansCercleDepuisRechercheProfil:(NSNumber *)data
{
    
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:data,@"id_utilisateur",_idCercleSelection,@"id_collection", nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/ajouterMembre.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          [self connect:^(void){
              [cercles reloadData];
          }];
          
      }];
   
    
}

- (IBAction)supprimerMembre:(id)sender {

    //Webservice de suppression
    
    CercleMembreCell * collectionCell = (CercleMembreCell *)[[sender superview] superview];
    NSNumber * id_collection = collectionCell.id_collection;
    NSNumber * id_utilisateur = collectionCell.id_utilisateur;
    
    
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:id_utilisateur,@"id_utilisateur",id_collection,@"id_collection", nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/supprimerMembre.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ( [[wsReturn objectForKey:@"status"] isEqualToString:@"ok"] )
          {
              //Suppression graphique
              CercleCell * collection = (CercleCell *)[collectionCell superview ];
              NSIndexPath * indexCollection = collection.index;
              NSIndexPath * indexCell = [collection indexPathForCell:collectionCell];
              [self connect:^(void){
                  NSDictionary * groupe = [[NSDictionary alloc]init];
                  if ( indexCollection.section == 0){
                      groupe = [cerclesListe objectAtIndex:collection.index.row];
                  }else{
                      groupe = [classesListe objectAtIndex:collection.index.row];
                
                  }
                  
                  collection.members = [groupe objectForKey:@"members"];
                  [collection deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexCell]];
              }];
              
          }
      }];
    
    

    
    
}

- (IBAction)bCreerCercle:(id)sender {
    
    UITextField * nomField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    nomField.placeholder = @"Nom...";
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Cercles" andTextFields:@[nomField]];
    if ([[self getSessionValueForKey:@"estExpert"] isEqual: @1]){
        [alertView addButtonWithTitle:@"Créer Classe"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  [self creerClasse:nomField.text];
                              }];
    
  
    }
    [alertView addButtonWithTitle:@"Créer Cercle"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              [self creerCercle:nomField.text];
                          }];

    
    [alertView addButtonWithTitle:@"Annuler"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                            
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    
    [alertView show];
}

- (IBAction)bRejoindreClasse:(id)sender {

    UITextField * cleField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    cleField.placeholder = @"clé";

    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Entrez la clé de la classe" andTextFields:@[cleField]];
    [alertView addButtonWithTitle:@"Annuler"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    [alertView addButtonWithTitle:@"Rejoindre"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              //Appel du webservice
                              NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:cleField.text,@"cle", nil];
                              [self post:[NSString stringWithFormat:@"%@/cercle/rejoindreClasse.php",sc_server]
                                   param: query
                                callback: ^(NSDictionary *wsReturn) {
                                    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Classe"
                                                                                     andMessage:[wsReturn objectForKey:@"message"]];
                                    [alertView addButtonWithTitle:@"Ok"
                                                             type:SIAlertViewButtonTypeDefault
                                                          handler:^(SIAlertView *alertView) {
                                                              
                                                          }];
                                    
                                    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
                                    [alertView show];
                                }];
                          }];

    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    
    [alertView show];
}






- (IBAction)selectionnerPourAjouterMembreDepuisSonProfil:(id)sender {
    if ((self.modeCercle == ModeCercleAjoutDepuisProfil) || (self.modeCercle == ModeCercleSelectionVisibilite)){
        NSIndexPath * indexPath = [cercles indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
        NSNumber * idCercle;
        NSString * nomCercle;
        switch (indexPath.section) {
            case 0:
                idCercle = [[cerclesListe objectAtIndex:indexPath.row] objectForKey:@"id"];
                nomCercle = [[cerclesListe objectAtIndex:indexPath.row] objectForKey:@"name"];
                break;
                
            case 1:
                idCercle = [[classesListe objectAtIndex:indexPath.row] objectForKey:@"id"];
                nomCercle = [[classesListe objectAtIndex:indexPath.row] objectForKey:@"name"];
                break;
                
            case 2:
                idCercle = [[classesRejointesListe objectAtIndex:indexPath.row] objectForKey:@"id"];
                nomCercle = [[classesRejointesListe objectAtIndex:indexPath.row] objectForKey:@"name"];
                break;
                
            default:
                break;
        }
        
        [_delegate actionAPartirDeCercle:idCercle nomCercle:nomCercle];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}



@end
