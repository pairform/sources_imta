//
//  CercleCell.m
//  PairForm
//
//  Created by admin on 12/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "CercleCell.h"

@implementation CercleCell
@synthesize members;
@synthesize boolEdit;
@synthesize id_cercle;
@synthesize index;





// PARTIE COLLECTION VIEW
//

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return members.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CercleMembreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellule" forIndexPath:indexPath];
    [UtilsCore sauvegardeUtilisateur:[[members objectAtIndex:indexPath.row] objectForKey:@"id_utilisateur"] name:[[members objectAtIndex:indexPath.row] objectForKey:@"name"] url:[[members objectAtIndex:indexPath.row] objectForKey:@"image"]];
    Utilisateur * utilisateur = [UtilsCore getUtilisateur:[[members objectAtIndex:indexPath.row] objectForKey:@"id_utilisateur"]];
    UILabel *lNom = (UILabel *)[cell viewWithTag:101];
    NSString * name = [NSString stringWithFormat:@"%@",utilisateur.owner_username];
    [lNom setText:name];
    
    UIImageView * imageView = (UIImageView *)[cell viewWithTag:102];
    imageView.image = [utilisateur getImage];
    cell.id_utilisateur = [utilisateur id_utilisateur];
    cell.id_collection = id_cercle;

    
    UIButton *bSupprime = (UIButton *)[cell viewWithTag:103];
    if ( boolEdit ){
        [bSupprime setHidden:NO];
    }else{
        [bSupprime setHidden:YES];
    }
    
    
    return cell;
}



@end