//
//  MessageCell.m
//  SupCast
//
//  Created by admin on 27/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MessageCell.h"



@implementation MessageCell
@synthesize message;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *myCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"cellTag"
                                    forIndexPath:indexPath];
    
    UILabel * tagLabel = (UILabel*)[myCell viewWithTag:101];
    if ( message.getTags.count != 0)
    [tagLabel setText:message.getTags[indexPath.row]];
    
    
    UIImageView * image = (UIImageView*)[myCell viewWithTag:102];
    [image setImage:[[UIImage imageNamed:@"tag.png"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0]];
    

    
    return myCell;
}


-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return  message.getTags.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size for orientation
    NSString * tagLabel = [[NSString alloc] initWithString:message.getTags[indexPath.row]];
    CGSize stringSize = [tagLabel sizeWithFont:[UIFont systemFontOfSize:15]
                          constrainedToSize:CGSizeMake(9999, 20)
                              lineBreakMode:NSLineBreakByWordWrapping];
    
    return CGSizeMake(stringSize.width+10, 20);
}

@end
