

#import <UIKit/UIKit.h>
#import "PTSSpringBoard.h"

@interface HomeVC : UIViewController <PTSSpringBoardDataSource, PTSSpringBoardDelegate>

@property (nonatomic, strong) PTSSpringBoard * springBoard;
@property (nonatomic, strong) NSMutableArray * ressourcesArray;
@property (nonatomic, strong) NSMutableArray * downloadingRessourcesArray;
//@property (nonatomic,strong) UICollectionView * collectionView;

-(IBAction)showActionSheet:(id)sender forEvent:(UIEvent*)event;

-(void)registerDownloadingRessource:(NSNotification*) notification;
-(void)updateProgressionForRessource:(NSNotification*) notification;
-(void)finishDownloadingRessource:(NSNotification*) notification;
-(void)refreshMessages;
@end
