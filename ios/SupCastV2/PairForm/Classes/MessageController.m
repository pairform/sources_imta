//
//  RecupererMessagesViewController.m
//  SupCast
//
//  Created by fgutie10 on 28/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//


#import "UtilsCore.h"
#import "MessageController.h"
#import "NouveauMessageController.h"
#import "ProfilController.h"
#import "UACellBackgroundView.h"
#import "UIMenuItem+CXAImageSupport.h"
#import "GAI.h"

#define INTERESTING_TAG_NAMES @"user", @"commentaire", @"id", @"supprime", @"votes", nil


@interface MessageController()

@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeLeft;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeRight;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *singleTapGesture;
@property (strong, nonatomic) NSMutableArray * arrayParentsDeReponsesOuvertes;
@property BOOL boolAfficheReponse;

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSMutableArray *messages_fixe;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int id_message ;
@property (nonatomic ) NSIndexPath *indexPathParent;

@property NSArray * cercleArrayTemp;

@end


@implementation MessageController



@synthesize idRessource;
@synthesize urlPage;
@synthesize tableView;
@synthesize currentRessource;
@synthesize modeNouveauMessage;
@synthesize modeMessage;

@synthesize swipeLeft;
@synthesize swipeRight;
@synthesize tapGesture;
@synthesize singleTapGesture;
@synthesize arrayParentsDeReponsesOuvertes;
@synthesize tag;
@synthesize num_occurence;
@synthesize boolAfficheReponse;
@synthesize id_utilisateur;
@synthesize id_utilisateurs;
@synthesize tag_message;


@synthesize transversal;
@synthesize cercleArrayTemp;

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    
    return YES;
}




#pragma mark -

-(void)viewWillAppear:(BOOL)animated
{
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshOneMessage:)
                                                 name:@"messageActualise"
                                               object:nil];
    
    // On intercepte la notification des messages non-actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelRefreshMessages)
                                                 name:@"messagesErreur"
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshOneMessage:)
                                                 name:@"messageActualise"
                                               object:nil];
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesErreur"
                                                  object:nil];
}

- (void)viewDidLoad {
	DLog(@"");
//    [TestFlight passCheckpoint:@"Lecture des messages"];    
    boolAfficheReponse = NO;
    
    idRessource = currentRessource.id_ressource;
    arrayParentsDeReponsesOuvertes = [[NSMutableArray alloc] init];
    // Recuperation du nom de la page.
    if ( self.urlPage != nil ){
        urlPage = [UtilsCore trimPageUrlForLocalDB:urlPage];
    }else{
        urlPage =@"";
    }

    self.title = @"Messages";
    
    //Pull to refresh pour l'actualisation des messages
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Actualisation des messages..."];
    refreshControl.tintColor = [UIColor colorWithRed:0.447 green:0.49 blue:0.969 alpha:1]; /*#727df7*/
    [refreshControl addTarget:self action:@selector(rafraichirMessages) forControlEvents:UIControlEventValueChanged];
    refreshControl.tag = 100;
    [tableView addSubview:refreshControl];
    
    
    modeNouveauMessage = ModeNouveauMessageNormal;
    [singleTapGesture requireGestureRecognizerToFail:tapGesture];
    [self refreshMessages];
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    
    if ( modeMessage != ModeMessageTransversal)
    {
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"action"
                                                              action:@"message_seen"
                                                               label:[NSString stringWithFormat:@"%@ - %@ : messages vus", currentRessource.nom_court, urlPage]
                                                               value:nil] build]];
        
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Messages : %@ - %@", currentRessource.nom_court, urlPage]];
    }
    else{
        
        [tracker set:kGAIScreenName value:@"Messages : transversal"];
    }
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    // navigation button bar
    NSMutableArray     *items = [_uiNavigationItem.rightBarButtonItems mutableCopy] ;
    if (IS_IPHONE) {
        if ( items.count == 1){
            if ( modeMessage != ModeMessageTransversal){
                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                [a1 setFrame:CGRectMake(5,6,32,32)];
                [a1 addTarget:self action:@selector(ecrireMessageDepuisTopBar) forControlEvents:UIControlEventTouchUpInside];
                [a1 setImage:[UIImage imageNamed:@"pencil_64"] forState:UIControlStateNormal];
                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
                [items addObject:bNouveau];
                _uiNavigationItem.rightBarButtonItems = items;
            }
            else{
                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                [a1 setFrame:CGRectMake(5,6,32,32)];
                [a1 addTarget:self action:@selector(afficherFiltres) forControlEvents:UIControlEventTouchUpInside];
                [a1 setImage:[UIImage imageNamed:@"magnifier_64"] forState:UIControlStateNormal];
                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
                [items addObject:bNouveau];
                _uiNavigationItem.rightBarButtonItems = items;
            }
        }
    }
    
    if (IS_IPAD) {
        if ( items.count == 0){
            items = [[NSMutableArray alloc] init];
            if ( modeMessage != ModeMessageTransversal){
                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                [a1 setFrame:CGRectMake(5,6,32,32)];
                [a1 addTarget:self action:@selector(ecrireMessageDepuisTopBar) forControlEvents:UIControlEventTouchUpInside];
                [a1 setImage:[UIImage imageNamed:@"pencil_64"] forState:UIControlStateNormal];
                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
                [items addObject:bNouveau];
                _uiNavigationItem.rightBarButtonItems = items;
            }
            else{
                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                [a1 setFrame:CGRectMake(5,6,32,32)];
                [a1 addTarget:self action:@selector(afficherFiltres) forControlEvents:UIControlEventTouchUpInside];
                [a1 setImage:[UIImage imageNamed:@"magnifier_64"] forState:UIControlStateNormal];
                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
                [items addObject:bNouveau];
                _uiNavigationItem.rightBarButtonItems = items;
            }
        }
    }
    
    [tableView reloadData];
    
}

-(void)rafraichirMessages{
    if (idRessource){
        [UtilsCore rafraichirMessagesFromRessource:idRessource];
    }else{
        [UtilsCore rafraichirMessages];
    }
}
-(void)ecrireMessageDepuisTopBar{
    _selectedMessage = nil;
    [self ecrireMessage];
}

-(void)ecrireMessage{
        if ([self checkForLoggedInUser])
            [self performSegueWithIdentifier: @"nouveauMessageSegue" sender: self];
}

#pragma mark - Refresh

-(void)cancelRefreshMessages{
    [(UIRefreshControl*)[tableView viewWithTag:100] endRefreshing];
}



-(void)refreshMessages{
    
    
    [(UIRefreshControl*)[tableView viewWithTag:100] endRefreshing];
    
    if ( boolAfficheReponse == YES ) {
        [tableView reloadData];
        return;
    }
    
    NSMutableArray * messagesAvantTraitement;
    // S'il on est sur une ressource et non sur une page
    switch (modeMessage) {
        case ModeMessageTransversal:
            if ( id_utilisateur ){
                messagesAvantTraitement = [[UtilsCore getMessagesFromUser:id_utilisateur] mutableCopy];
                break;
            }
            if ( id_utilisateurs){
                messagesAvantTraitement = [[UtilsCore getMessagesFromUsers:id_utilisateurs] mutableCopy];
                break;
            }
            if ( tag_message ){
                messagesAvantTraitement = [[UtilsCore getMessageFromTag:tag_message] mutableCopy];
                break;
            }
            if ( idRessource ){
                messagesAvantTraitement = [[UtilsCore getMessagesFromRessourceTrans:idRessource] mutableCopy];
                break;
            }
            messagesAvantTraitement = [[UtilsCore getAllMessages] mutableCopy];
            break;
            
        default:
            if ( [self.urlPage isEqualToString:@""] ){
                messagesAvantTraitement= [[UtilsCore getMessagesFromRessource:[NSNumber numberWithInt:[self.idRessource intValue]] ] mutableCopy];
            }
            else
            {
                if ( tag && num_occurence){
                    messagesAvantTraitement= [[UtilsCore getMessagesFromRessource:[NSNumber numberWithInt:[self.idRessource intValue]] nom_page:self.urlPage tag:tag num_occurence:[NSString stringWithFormat:@"%d",[num_occurence intValue], nil]] mutableCopy];
                }
                else{
                    messagesAvantTraitement= [[UtilsCore getMessagesFromRessource:[NSNumber numberWithInt:[self.idRessource intValue]] nom_page:self.urlPage] mutableCopy];
                }
            }
    }
   
     
    
    /*int rank = [[[[self getSessionValueForKey:@"rank"] objectForKey:[self.idRessource stringValue]] objectForKey:@"id_categorie"] intValue];
    int idUser = [[self getSessionValueForKey:@"id"] intValue];
    
    // messages supprimé
    for (Message * message in _messages) {
        if(([message.supprime_par intValue] != 0) && ([message.supprime_par intValue] != idUser) && (rank < 3))
        {
            [messagesAvantTraitement removeObject:message];
        }
    }*/
    _messages = [NSArray arrayWithArray:messagesAvantTraitement];
    
    if ( [_messages count] != 0 )
    tableView.tableHeaderView = nil;

    [tableView reloadData];
//    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    
}

-(void) refreshOneMessage:(NSNotification *)n {
    int row = [n.userInfo[@"index_tableview"] integerValue];
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row
 inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self enleverAnimationUtiliteAtIndexPath:rowToReload];
    
    [tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark Table View Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    DLog(@"");
    return 1;
}

-(NSInteger)tableView:(UITableView *)aTableView
numberOfRowsInSection:(NSInteger)section {
    DLog(@"");
    if ( _messages == nil ) return 0;
    return _messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLog(@"");
    
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    
    NSString *CellIdentifier = @"MessageCell";
    if ( message.parent != nil && modeMessage == ModeMessageNormal ){
        CellIdentifier = @"ReponseCell";
    }
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    //Check de l'affichage
    int rank = [[[[self getSessionValueForKey:@"rank"] objectForKey:[message.id_ressource stringValue]] objectForKey:@"id_categorie"] intValue];
    int idUser = [[self getSessionValueForKey:@"id"] intValue];
    
    if([message.supprime_par intValue] == 0)
    {
        
        cell.contentView.alpha = 1;
        
    }
    else
    {
        
        cell.contentView.alpha = 0.5;
        
        //Else géré par heightForRow (on set la hauteur à 0)
    }
    
    Utilisateur * utilisateur = [UtilsCore getUtilisateur:message.owner_guid];
    
    UIButton *bImage = (UIButton *)[cell viewWithTag:1];
    UIImage * imageModifiee = [utilisateur.getImage thumbnailImage:48 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh];
    
    [bImage  setImage:imageModifiee forState:UIControlStateNormal];
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:2];
    [lblName setText:utilisateur.owner_username];
//    [lblName sizeToFit];
    
    UILabel *lblRank = (UILabel *)[cell viewWithTag:3];
    if ( message.owner_rank_name ){
        [lblRank setText:message.owner_rank_name];
    }else{
        [lblRank setText:@""];
    }
//    [lblRank sizeToFit];
    
    UILabel *lblDate = (UILabel *)[cell viewWithTag:4];
    NSDate * date_created = [NSDate dateWithTimeIntervalSince1970:[message.time_created  intValue]];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
//    if (IS_IPHONE) {
//        [formatter setDateFormat:@"dd/MM/yy à H:mm"];
//    }
//    else{
        [formatter setDateFormat:@"dd/MM/yy"];
//    }
    //    [formatter setDateFormat:@"dd/MM/yy"];
    NSString *dateString=[formatter stringFromDate:date_created];
    [lblDate setText:dateString];
    //    [lblDate sizeToFit];
    
    UILabel *lblValue = (UILabel *)[cell viewWithTag:5];
   
    if( ([message.supprime_par intValue] != 0)  && (([message.supprime_par intValue] != idUser) && (rank < 3)))
    {
         [lblValue setText:@"Message supprimé."];
    }else{
         [lblValue setText:message.value];
    }
    //[lblValue sizeToFit];
    
    UILabel *lblUtilite = (UILabel *)[cell viewWithTag:7];
    [lblUtilite setText:[message.utilite stringValue]];
    
    
    UIImageView * IvUtiliteLeft = ( UIImageView *)[cell viewWithTag:6];
    if ( [message.user_a_vote isEqualToNumber:@-1]){
        [IvUtiliteLeft setImage:[UIImage imageNamed:@"arrow_left_active.png" ]];
    }else{
        [IvUtiliteLeft setImage:[UIImage imageNamed:@"arrow_left.png" ]];
    }
    
    
    UIImageView * IvUtiliteRight = ( UIImageView *)[cell viewWithTag:8];
    if ( [message.user_a_vote isEqualToNumber:@1]){
       [IvUtiliteRight setImage:[UIImage imageNamed:@"arrow_right_active.png" ]];
    }else{
        [IvUtiliteRight setImage:[UIImage imageNamed:@"arrow_right.png" ]];
    }
    if ([CellIdentifier isEqualToString:@"MessageCell" ]){
        UIImageView * messageBG = ( UIImageView *)[cell viewWithTag:10];
        //TODO Solution temporaire
        if (![UtilsCore getReponsesFromMessage:message.id_message] || modeMessage != ModeMessageNormal ){
            [messageBG setImage:[UIImage imageNamed:@"MessageBG"]];
        }else{
            [messageBG setImage:[UIImage imageNamed:@"ReponseBG"]];
        }
    }
    UIButton * bTrans = ( UIButton *)[cell viewWithTag:11];
    if ( modeMessage != ModeMessageNormal ){
        [bTrans setHidden:NO];
        [bTrans setImage:[[UtilsCore getRessource:message.id_ressource]getIcone ] forState:UIControlStateNormal];
    }else{
        // Pas de vue transversale
        [bTrans setHidden:YES];
    }
    // est defi &&  defi valider
    
//    
//    cell.backgroundView = [[UACellBackgroundView alloc] initWithFrame:CGRectZero];
//
//    [(UACellBackgroundView*) cell.backgroundView setColorsFrom:[UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1]
//                                                            to:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:1]];
//    
    if (![message.estDefi isEqualToNumber:@0] || [message.defi_valide isEqualToNumber:@1]){
        [[cell viewWithTag:14] setHidden:NO];
        if ([message.estDefi isEqualToNumber:@1]) {
            [(UIImageView*)[cell viewWithTag:14] setImage:[UIImage imageNamed:@"DefiActif"]];
        }
        else if ([message.estDefi isEqualToNumber:@2]) {
            [(UIImageView*)[cell viewWithTag:14] setImage:[UIImage imageNamed:@"DefiFini"]];
        }
        else if ([message.defi_valide isEqualToNumber:@1]) {
            [(UIImageView*)[cell viewWithTag:14] setImage:[UIImage imageNamed:@"DefiValid"]];
        }
    }
    else
    {
        [[cell viewWithTag:14] setHidden:YES];
    }
    
//    [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionMiddle];
    
    
    
    UIImageView * medailleView = (UIImageView*)[cell viewWithTag:13];
    [medailleView setHidden:NO];
//    [medailleView setClipsToBounds:NO];
    
    // Medailles
    if ( [message.medaille isEqualToString:@""])
        [medailleView setHidden:YES];
    else
        [medailleView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"medaille_%@", message.medaille]]];
    
    
    cell.message  = message;
    UICollectionView * collectionViewTag = ( UICollectionView *)[cell viewWithTag:12];
    //Tags
    if ( [message.getTags count] != 0 && [message.supprime_par intValue] == 0){
        collectionViewTag.dataSource = cell;
        collectionViewTag.delegate = cell;
//        [collectionViewTag reloadData];
        [collectionViewTag setHidden:NO];
//        [collectionViewTag setBackgroundColor:[UIColor redColor]];
//        [collectionViewTag invalidateIntrinsicContentSize];
        NSLayoutConstraint * constraint = collectionViewTag.constraints[0];
        constraint.constant = [self getHeightForTags:message];
//        [collectionViewTag.collectionViewLayout invalidateLayout];
        
    }else{
        [collectionViewTag setHidden:YES];
    }
    
    return cell;
}


-(int) getHeightForTags:(Message *)message{
    NSString * tags = [message.getTags componentsJoinedByString:@" "];
    
    CGSize stringSize = [tags sizeWithFont:[UIFont systemFontOfSize:15]
                         constrainedToSize:CGSizeMake(tableView.contentSize.width-100,9999 )
                             lineBreakMode:NSLineBreakByWordWrapping];
    // Interstice en chaque ligne
    return stringSize.height+12*((stringSize.height/20)-1);
//    return stringSize.height + 10;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    

        
//    if([tableView viewWithTag:5]){
//        messageWidth = [[tableView viewWithTag:5] frame].size.width-20;
//    }
    
    
    
    //Différents cas de figure :
    
    Message * message = [_messages objectAtIndex:(indexPath.row)];
   
    
    int hauteurMessage;
    //S'il y a une médaille ou un défi, ou on est dans une lecture transversale
    if ((self.modeMessage == ModeMessageTransversal) || !([message.estDefi isEqualToNumber:@0]) || ([message.defi_valide isEqualToNumber:@1]) || (![message.medaille isEqualToString:@""])) {
        //On met une taille par défaut de 113
        hauteurMessage = 113;
    }
    else{
        //Sinon, on a une taille de 88
        hauteurMessage = 88;
    }
    
    
    NSString *label =  message.value;
    
    //Si le message est supprimé, on calcule la hauteur du label sur une chaine vide.
    if ( ![message.supprime_par isEqual: @0])
        label = @"";
    
    //Defaut : portrait
    int messageWidth = 238, messagePadTop = 32, messagePadBottom = 11;
    
    //Constante de largeur de messages
    if (IS_IPHONE && UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])){
        messageWidth = 400;
    }
    
    CGSize stringSize = [label sizeWithFont:[UIFont fontWithName:@"Helvetica Neue" size:14]
                          constrainedToSize:CGSizeMake(messageWidth, 9999)];
    
    //Pour chaque retour à la ligne, on rajoute de la hauteur à la chaine
//    NSInteger numberOfNewLines = [[label componentsSeparatedByCharactersInSet:
//                         [NSCharacterSet characterSetWithCharactersInString:@"\n\n" ]] count];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\n\n" options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfNewLines = [regex numberOfMatchesInString:label options:0 range:NSMakeRange(0, [label length])];

    if (numberOfNewLines) {
        stringSize.height += 14 * numberOfNewLines;
    }
    //Si le message fait plus de deux lignes
    if (stringSize.height > 36) {
        //La différence entre la taille totale prévue et la somme de la taille du message et du padding
        int diff = hauteurMessage - (stringSize.height + messagePadTop + messagePadBottom);
        //Si le message va déborder
        if(diff < 0)
        {
            //On rajoute cette différence à la hauteur totale
            hauteurMessage += -diff;
        }
        //Cas spécial : si on est en transversal
        if (self.modeMessage == ModeMessageTransversal)
        {
            //On rajoute 30 de padding, à cause de l'icône de contexte
            hauteurMessage += 30;
        }
    }
    
    if (![message.parent isEqualToNumber:@0]) {
        hauteurMessage += 5;
    }
    
    //S'il y a des tags
    if ([message.getTags count] != 0){
        //Hauteur du container des tags
        int tailleTag = 0;
        //On récupère la taille estimée
        tailleTag = [self getHeightForTags:message];
        //On rajoute automatiquement la taille calculé des tags + une marge
        hauteurMessage += tailleTag;
    }
    
    

    return hauteurMessage;
    
//    if(stringSize.height > 36)
//        // 88 taille du reste du message ?
//        return (stringSize.height  - 10/*21*/) + 113 + tailleTag ;
//    else
//        return 113 + tailleTag;
}
//Indispensable pour l'affichage du menuController
-(BOOL)canBecomeFirstResponder{
    return YES;
}


#pragma mark -
#pragma mark Actions


-(void)repondre:(id)sender{
    
    modeNouveauMessage = ModeNouveauMessageReponse;
    [self ecrireMessage];
}

-(void)editer:(id)sender{
    modeNouveauMessage = ModeNouveauMessageEdition;
    [self ecrireMessage];
    /*
    [self editerMessage:[[self selectedMessage].id_message doubleValue] contenu:[self selectedMessage].value];*/
}

-(void)terminerDefi:(id)sender{
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message", nil];
    [self post:[NSString stringWithFormat:@"%@/messages/terminerDefi.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          
          if (![self displayErrors:wsReturn]){
              [self.view makeToast:@"Défi terminé" duration:2 position:@"bottom" ];
          }
          // Enregistrement dans le icore , puis rechargement de la table
         
          [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInt:_selectedMessagePath.row]];
          [tableView reloadData];
          
          
      }];
}

-(void)donnerMedaille:(id)sender{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Donner une médaille" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil  otherButtonTitles:@"Or", @"Argent",@"Bronze",@"Effacer médaille", nil];
	[popupQuery showInView:self.view];
    [popupQuery setTag:3];
}


-(void)validerDefi:(id)sender{
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message", nil];
    [self post:[NSString stringWithFormat:@"%@/messages/validerDefi.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          // Enregistrement dans le icore , puis rechargement de la table
          [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInt:_selectedMessagePath.row]];
          [tableView reloadData];
      }];
}



-(void)supprimer:(id)sender{
    
    NSString * rank = [[[self getSessionValueForKey:@"rank"] objectForKey:[_selectedMessage.id_ressource stringValue]] objectForKey:@"id_categorie"];
    NSString * idUser = [self getSessionValueForKey:@"id"];
    
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [self selectedMessage].id_message, @"id_message",
                             [self selectedMessage].supprime_par, @"supprime_par",
                             [self selectedMessage].owner_guid, @"id_user_op",
                             [self selectedMessage].owner_rank_id, @"id_rank_user_op",
                             idUser, @"id_user_moderator",
                             rank, @"id_rank_user_moderator",
                             [self idRessource], @"id_ressource", nil];
    
    [self post:[NSString stringWithFormat:@"%@/messages/supprimerMessage.php",sc_server]
         param: params
      callback: ^(NSDictionary *wsReturn) {
          if ( ![self displayErrors:wsReturn]){
              [UtilsCore rafraichirMessagesFromMessageId:[self selectedMessage].id_message indexTableView:[NSNumber numberWithInt:_selectedMessagePath.row]];
          }
      }];
    
}

- (void)ajouterTags:(id)sender {
    UITextField * tagsField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    tagsField.placeholder = @"Tag,Tag...";
    
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Ajouter un ou des tags." andTextFields:@[tagsField]];
    [alertView addButtonWithTitle:@"Annuler"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    [alertView addButtonWithTitle:@"Ajouter"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message",tagsField.text , @"tags", nil];
                              [self post:[NSString stringWithFormat:@"%@/messages/ajouterTags.php",sc_server]
                                   param: query
                                callback: ^(NSDictionary *wsReturn) {
                                    [self displayErrors:wsReturn];
                                    [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInt:_selectedMessagePath.row]];
                                    [tableView reloadData];
                                }];
                          }];
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;

    [alertView show];
    
}

- (void)afficherInfos:(id)sender {
    Message * message = _selectedMessage;
    NSMutableString * string = [[NSMutableString alloc] initWithString:@""];

    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy à H:mm"];
    
    NSString *time_created =[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[message.time_created  intValue]]];
    
    [string appendString:[NSString stringWithFormat:@"Message posté le %@", time_created]];
    
    if (![message.supprime_par isEqualToNumber:@0]) {
        //    [formatter setDateFormat:@"dd/MM/yy"];
        
        NSString *dateString=[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[message.time_updated  intValue]]];

        Utilisateur * supprimeur = [UtilsCore getUtilisateur:message.supprime_par];
        [string appendString:[NSString stringWithFormat:@"\r Supprimé par %@ le %@", supprimeur.owner_username, dateString]];
    }
    
    NSArray *tags = message.getTags;
    NSArray * tagsAuteurs = message.getTagsAuteurs;
    for (int i = 0; i < tags.count; i++) {
         NSString * auteur = @"inconnu";
        if ( tagsAuteurs[i] != [NSNull null]  ) auteur = tagsAuteurs[i];
        [string appendString:[NSString stringWithFormat:@"\r %@%@ : %@\n",string,tags[i],auteur]];
    }
    
//    if ([string isEqualToString:@""])
//        [string setString:@"Pas d'informations disponibles."];
//    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Infos" andMessage:string];
    [alertView addButtonWithTitle:@"Ok"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    [alertView show];
}


-(void) copier{
    [UIPasteboard generalPasteboard].string = _selectedMessage.value;
    NSValue * point = [NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, tableView.contentOffset.y + (self.view.frame.size.height  - 100))];
    
    [self.view makeToast:@"Message copié dans le presse-papier." duration:2 position:point image:[UIImage imageNamed:@"copy_64"]];
}

-(void) suivreLien{
    
    //On regarde s'il y a des URLS dans le message
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:self.selectedMessage.value options:0 range:NSMakeRange(0, [self.selectedMessage.value length])];
    //S'il y en a
    
    switch ([matches count]) {
        case 0:
            //Aucune url, on sort
            break;
        case 1:
        {
            //Une url, on la suit direct
            [[UIApplication sharedApplication] openURL:[matches[0] URL]];
        }
            break;
        default:
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Liens du message :"
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:nil];
            
            // ObjC Fast Enumeration
            for (NSTextCheckingResult *match in matches) {
                [actionSheet addButtonWithTitle:[[match URL] absoluteString]];
            }
            
            [actionSheet addButtonWithTitle:@"Annuler"];
            actionSheet.cancelButtonIndex = [matches count];
            //Tag pour différencier les actions au niveau des méthodes déléguées
            [actionSheet setTag:4];
            [actionSheet showInView:self.view];
//            
//            //Plusieurs urls, on pop ça
//            for (NSTextCheckingResult *match in matches) {
//                if ([match resultType] == NSTextCheckingTypeLink) {
//                    NSURL *url = [match URL];
//                    DLog(@"found URL: %@", url);
//                }
//            }

        }
        break;
    }
}

- (void)copublier:(id)sender  {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        Ressource * ressource = [UtilsCore getRessource:_selectedMessage.id_ressource];
        
        NSString * initialText = [NSString stringWithFormat:@"#%@ #%@ >%@ : ",@"PairForm",ressource.nom_court,[UtilsCore getUtilisateur:_selectedMessage.owner_guid].owner_username];
        
        if (_selectedMessage.value.length + initialText.length > 140){
            //NSRange r = NSMakeRange(0, 140);
            //initialText = [initialText substringWithRange: r];
            [UIPasteboard generalPasteboard].string = _selectedMessage.value;
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Tweet" andMessage:@"le message est trop long, il a été inséré dans le presse-papier."];
            [alertView addButtonWithTitle:@"Ok"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            [alertView show];
        }else{
            initialText = [NSString stringWithFormat:@"%@ %@",initialText,_selectedMessage.value ];
        }
        [mySLComposerSheet setInitialText:initialText];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    DLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    DLog(@"Post Sucessful");
                    break; 
                default:
                    break; 
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];

    }
}



-(IBAction)mapAffiche:(id)sender event:(id)event
{
    DLog(@"");
    
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tableView];
	NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
	}
}
#pragma mark -
#pragma mark Utilité

- (IBAction)swipeToLeft:(id)sender {
    // Prend la cellule
    CGPoint location = [swipeLeft locationInView:self.tableView];
    [self voter:@"false" location:location];
    
}
- (IBAction)swipeToRight:(id)sender {
    
    // Prend la cellule
    CGPoint location = [swipeRight locationInView:self.tableView];
    [self voter:@"true" location:location];
    
}

- (void)voter:(NSString *)valeur location:(CGPoint )location{

    if (! [self checkForLoggedInUser]) return;
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    //S'il n'y a aucun élément à l'endroit swipé
    if (!indexPath)
        return;
    
    //Prend la fleche dans la cellule
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView * image ;
    
    if ([cell viewWithTag:2000]) {
        [self enleverAnimationUtiliteAtIndexPath:indexPath];
    }
    if ( [valeur isEqualToString:@"true"]){
        image = ( UIImageView *)[cell viewWithTag:8];
    }else{
        image = ( UIImageView *)[cell viewWithTag:6];
    }
    
    
    [self lanceAnimationUtilite:image];
    
    //Appel du webservice
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    if([message.supprime_par intValue] != 0){
        [self enleverAnimationUtiliteAtIndexPath:indexPath];
        return;
    }
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:valeur,@"up",message.id_message , @"id_message", nil];
    [self post:[NSString stringWithFormat:@"%@/messages/voterUtilite.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          
          if ( [self displayErrors:wsReturn]){
              [self enleverAnimationUtiliteAtIndexPath:indexPath];
              return;
          }
          // Enregistrement dans le icore , puis rechargement de la table
          [UtilsCore rafraichirMessagesFromMessageId:message.id_message indexTableView:[NSNumber numberWithInt:indexPath.row]];
          //[tableView reloadData];
          
          
      }];
   
}

- (void)lanceAnimationUtilite:(UIImageView *)image{
    
//    image.transform = CGAffineTransformMakeScale(1.6, 1.6);
//    [UIView animateWithDuration:0.3
//                          delay: 0.0
//                        options: UIViewAnimationOptionCurveEaseIn
//                     animations:^{
//                         image.transform = CGAffineTransformIdentity;
//                     }
//                     completion:nil];
    CGFloat glowSpread = 30.0f;
    
    UIImage * glowImage = nil;
    
    CGSize imageSize = CGSizeMake(image.bounds.size.width + glowSpread, image.bounds.size.height + glowSpread);
    
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, [UIScreen mainScreen].scale);
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetAllowsAntialiasing(context, true);
        CGContextSetShouldAntialias(context, true);
        
        CGContextSaveGState(context);
        
        CGGradientRef gradient = [image tag] == 6 ? [self newGlowGradientWithColor:[UIColor redColor]] : [self newGlowGradientWithColor:[UIColor greenColor]];
        
        CGPoint gradCenter = CGPointMake(floorf(imageSize.width / 2.0f), floorf(imageSize.height / 2.0f));
        CGFloat gradRadius = MAX(imageSize.width, imageSize.height) / 2.0f;
        
        CGContextDrawRadialGradient(context, gradient, gradCenter, 0.0f, gradCenter, gradRadius, kCGGradientDrawsBeforeStartLocation);
        
        CGGradientRelease(gradient), gradient = NULL;
        CGContextRestoreGState(context);
        
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, glowSpread / 2.0f, glowSpread / 2.0f);
        
        [image.layer renderInContext:context];
        
        CGContextRestoreGState(context);
        
        glowImage = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    
    // Make the glowing view itself, and position it at the same
    // point as ourself. Overlay it over ourself.
    UIView *glowView = [[UIImageView alloc] initWithImage:glowImage];
    [glowView setTag:2000];
    
    glowView.center = image.center;
    [image.superview insertSubview:glowView aboveSubview:image];
    
    // We don't want to show the image, but rather a shadow created by
    // Core Animation. By setting the shadow to white and the shadow radius to
    // something large, we get a pleasing glow.
    glowView.alpha = 0.0f;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut 
                     animations:
     ^{
         glowView.layer.opacity = 1.0f;
     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5
                                               delay:0.0
                                             options: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                                          animations:
                          ^{
                              glowView.layer.opacity = 0.5f;
                          }
                                          completion:nil];
                     }];
}

-(void)enleverAnimationUtiliteAtIndexPath:(NSIndexPath*) index{

    UIView * glowView = [[self.tableView cellForRowAtIndexPath:index] viewWithTag:2000];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseIn
                     animations:
     ^{
         glowView.layer.opacity = 0.0;
     }
                     completion:^(BOOL finished) {
                         
                         [glowView removeFromSuperview];
                         
                     }];
}

-(CGGradientRef)newGlowGradientWithColor:(UIColor*)color
{
    CGColorRef cgColor = [color CGColor];
    
    const CGFloat *sourceColorComponents = CGColorGetComponents(cgColor);
    
    CGFloat sourceRed;
    CGFloat sourceGreen;
    CGFloat sourceBlue;
    CGFloat sourceAlpha;
    if (CGColorGetNumberOfComponents(cgColor) == 2)
    {
        sourceRed = sourceColorComponents[0];
        sourceGreen = sourceColorComponents[0];
        sourceBlue = sourceColorComponents[0];
        sourceAlpha = sourceColorComponents[1];
    }
    else
    {
        sourceRed = sourceColorComponents[0];
        sourceGreen = sourceColorComponents[1];
        sourceBlue = sourceColorComponents[2];
        sourceAlpha = sourceColorComponents[3];
    }
    
    size_t locationsCount = 20;
    CGFloat step = 1.0f / locationsCount;
    
    CGFloat colorComponents[4 * locationsCount];
    CGFloat locations[locationsCount];
    
    NSUInteger componentsIndex = 0;
    for (NSUInteger index = 0; index < locationsCount; index++)
    {
        CGFloat point = index * step;
        locations[index] = point;
        
        CGFloat alpha = sourceAlpha * (1 - 0.5 * (1 - cos(point * M_PI)));
        
        colorComponents[componentsIndex] = sourceRed;
        colorComponents[componentsIndex + 1] = sourceGreen;
        colorComponents[componentsIndex + 2] = sourceBlue;
        colorComponents[componentsIndex + 3] = alpha;
        componentsIndex += 4;
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colorComponents, locations, locationsCount);
    
    CGColorSpaceRelease(colorSpace), colorSpace = NULL;
    
    return gradient;
}

#pragma mark -
#pragma mark Menu

- (IBAction)singleTapGesture:(id)sender {
    
    
    DLog(@"");
    
    [self becomeFirstResponder];
    
    CGPoint location = [singleTapGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    if (!indexPath) {
        return;
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    NSMutableArray * menuItems = [[NSMutableArray alloc] init];
    int idUser = [[self getSessionValueForKey:@"id"] intValue];
    int rank = [[[[self getSessionValueForKey:@"rank"] objectForKey:[message.id_ressource stringValue]] objectForKey:@"id_categorie"] intValue];
    

    if([message.supprime_par intValue] == 0){

        
        //Repondre
        if ( modeMessage != ModeMessageTransversal){
            UIMenuItem *repondre = [[UIMenuItem alloc] cxa_initWithTitle:@"Répondre"
                                                                  action:@selector(repondre:)
                                                                   image:[[UIImage imageNamed:@"comment_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                  bounds:CGSizeMake(32, 32)
                                                                                                                    interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:repondre];
        }
        
        //Retweeter
        UIMenuItem *retweeter = [[UIMenuItem alloc] cxa_initWithTitle:@"Retweeter"
                                                               action:@selector(copublier:)
                                                                image:[[UIImage imageNamed:@"Twitter_c"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                              bounds:CGSizeMake(32, 32)
                                                                                                                interpolationQuality:kCGInterpolationHigh]];
        [menuItems addObject:retweeter];
        
        //Copier
        UIMenuItem *copier = [[UIMenuItem alloc] cxa_initWithTitle:@"Copier"
                                                               action:@selector(copier)
                                                                image:[[UIImage imageNamed:@"copy_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                              bounds:CGSizeMake(32, 32)
                                                                                                                interpolationQuality:kCGInterpolationHigh]];
        [menuItems addObject:copier];
        
        //On regarde s'il y a des URLS dans le message
        NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
        NSArray *matches = [linkDetector matchesInString:message.value options:0 range:NSMakeRange(0, [message.value length])];
        //S'il y en a
        
        if ([matches count]) {
            //Copier
            UIMenuItem *copier = [[UIMenuItem alloc] cxa_initWithTitle:@"Lien(s)"
                                                                action:@selector(suivreLien)
                                                                 image:[[UIImage imageNamed:@"link_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                             bounds:CGSizeMake(32, 32)
                                                                                                               interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:copier];
        }

        
        // Ses messages
        if (idUser == [message.owner_guid intValue])
        {
            //Edition
//            UIMenuItem *editer = [[UIMenuItem alloc] cxa_initWithTitle:@"Editer" action:@selector(editer:) image:[UIImage imageNamed:@"pencil_64"]];
            //[menuItems addObject:editer];
            
            // si c'est un defi en cours
            //Terminer defi
            if ( [message.estDefi isEqualToNumber:@1]){
                UIMenuItem *terminerDefi = [[UIMenuItem alloc] cxa_initWithTitle:@"Terminer Défi" action:@selector(terminerDefi:)
                                                                           image:[[UIImage imageNamed:@"clock_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                        bounds:CGSizeMake(32, 32)
                                                                                                                          interpolationQuality:kCGInterpolationHigh]];
                [menuItems addObject:terminerDefi];
            }
            // Les messages des autres.
        }else{
            // Si expert ou admin
            //Donner Medaille
            if(rank >= 4){
                UIMenuItem *donnerMedaille = [[UIMenuItem alloc] cxa_initWithTitle:@"Médaille"
                                                                            action:@selector(donnerMedaille:)
                                                                             image:[[UIImage imageNamed:@"medaille_or"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                             bounds:CGSizeMake(32, 32)
                                                                                                                               interpolationQuality:kCGInterpolationHigh]];
                [menuItems addObject:donnerMedaille];
            }
            // si reponse et reponse d'un defi. et expert ou admin ET pas encore validé
            //Valider defi
            if ( message.parent && ![((Message*)[UtilsCore getMessage:message.parent]).estDefi isEqualToNumber:@0] && rank >= 4 && [message.defi_valide isEqualToNumber:@0]){
                UIMenuItem *validerDefi = [[UIMenuItem alloc] cxa_initWithTitle:@"Valider"
                                                                         action:@selector(validerDefi:)
                                                                          image:[[UIImage imageNamed:@"checkmark_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                           bounds:CGSizeMake(32, 32)
                                                                                                                             interpolationQuality:kCGInterpolationHigh]];
                [menuItems addObject:validerDefi];
            }
        }
    
        //Ajout de tags
        if((rank >= 2))
        {
            UIMenuItem *ajouterTags = [[UIMenuItem alloc] cxa_initWithTitle:@"Ajouter tags"
                                                                     action:@selector(ajouterTags:)
                                                                      image:[[UIImage imageNamed:@"tag_add_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                     bounds:CGSizeMake(32, 32)
                                                                                                                       interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:ajouterTags];
        }
        //Affichage des infos
        if((rank >= 4) || message.getTags)
        {
            UIMenuItem *afficherInfos = [[UIMenuItem alloc] cxa_initWithTitle:@"Afficher infos"
                                                                       action:@selector(afficherInfos:)
                                                                        image:[[UIImage imageNamed:@"info_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                    bounds:CGSizeMake(32, 32)
                                                                                                                      interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:afficherInfos];
        }
    
    }
    

    
    
    // messages supprimés.
   
    //Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
    if(([message.owner_guid intValue] == idUser) || (rank >= 3))
    {
        
        UIMenuItem *supprimer;
        if([message.supprime_par intValue] == 0){
            supprimer = [[UIMenuItem alloc] cxa_initWithTitle:@"Supprimer"
                                                action:@selector(supprimer:)
                                                image:[[UIImage imageNamed:@"comment_delete_64"]
                                                resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                bounds:CGSizeMake(32, 32)
                                                interpolationQuality:kCGInterpolationHigh]];
             [menuItems addObject:supprimer];
        }
        // if ( rank >= [message.supprime_par intValue])
        else{
            supprimer = [[UIMenuItem alloc] cxa_initWithTitle:@"Activer" action:@selector(supprimer:)
                                                        image:[[UIImage imageNamed:@"comment_check_64"]
                                                        resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                        bounds:CGSizeMake(32, 32)
                                                        interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:supprimer];
            
            UIMenuItem *afficherInfos = [[UIMenuItem alloc] cxa_initWithTitle:@"Afficher infos"
                                                                       action:@selector(afficherInfos:)
                                                                        image:[[UIImage imageNamed:@"info_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                    bounds:CGSizeMake(32, 32)
                                                                                                                      interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:afficherInfos];
        }

    }
    

    
    
    
    [self setSelectedMessage:[_messages objectAtIndex:indexPath.row]];
    [self setSelectedMessagePath:indexPath];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setMenuItems:menuItems];
    [menu setTargetRect:cell.frame inView: cell.superview];
    [menu setMenuVisible:YES animated:YES];
    
}

#pragma mark -
#pragma mark Reponses

- (IBAction)tapGesture:(id)sender {
    if ( modeMessage != ModeMessageNormal ) return;
    
    // Si on est sur une reponse , on cache les messages
    CGPoint location = [tapGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    if (!indexPath)
        return;
    
     Message * mes = [_messages objectAtIndex:indexPath.row];
    
    // si reponse
    if ([ (Message *)mes parent ] != nil ) {
        [self cacherReponses:[mes parent]];
       
        
        for (Message *item in _messages){
            if ([item.id_message isEqualToNumber: mes.parent]  ){
                NSIndexPath * indexPathParent = [NSIndexPath indexPathForItem:[_messages indexOfObject:item]  inSection:0];
                [tableView scrollToRowAtIndexPath:indexPathParent atScrollPosition:UITableViewScrollPositionNone animated:YES];
                break;
            }
        }
        [arrayParentsDeReponsesOuvertes removeObject:mes.parent];
    }
    else{
        // si parent , et reponses deja ouvertes
        if ( [arrayParentsDeReponsesOuvertes containsObject:mes.id_message]){
            [self cacherReponses:mes.id_message];
            [arrayParentsDeReponsesOuvertes removeObject:mes.id_message];
        }else{
            [self AfficheReponses];
            [arrayParentsDeReponsesOuvertes addObject:mes.id_message];
        }
        
    }
}

-(void)AfficheReponses{
    
    CGPoint location = [tapGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    if (!indexPath)
        return;
    
    Message * mes = [_messages objectAtIndex:indexPath.row];
    // Si on est sur un message deja ouvert
    //if ( _id_message == [mes.id_message integerValue]) return;
    _id_message = [mes.id_message integerValue];
    _indexPathParent = indexPath;

    // Si on est deja sur une reponse , on ne fait rien
    if ([ (Message *)[_messages objectAtIndex:indexPath.row ] parent ] != nil ) return;
   

    NSMutableArray * temp = [_messages mutableCopy];
    
    // On affiche les reponses
    NSArray * reponses = [UtilsCore getReponsesFromMessage:[NSNumber numberWithInt:_id_message]];
    
    if ( reponses != nil)
        DLog(@"%d",_id_message);
    {
        int index = indexPath.row+1;
        for ( Message *reponse in reponses){
            [temp insertObject:reponse atIndex:index];
            _messages = temp;
            [tableView beginUpdates];
            NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView endUpdates];
            index++;
            
        }
    }
    boolAfficheReponse = YES;
}

- (void) cacherReponses:(NSNumber *) id_parent{
    //On supprime les messages ouvert en reponses
        NSMutableArray * temp = [_messages mutableCopy];
        for (Message *item in temp){
            if ( [item.parent isEqualToNumber:id_parent])
            {
                int index = [_messages indexOfObject:item];
                [_messages removeObject:item];
                [tableView beginUpdates];
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                [tableView endUpdates];
            }
            
        }
     boolAfficheReponse = NO;
    
    
}


#pragma mark - Lecture Transversale , filtres

-(void) afficherFiltres{
    // Filtres : Utilisateur? , ressources , groupes? , tags

    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Filtrer par" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil  otherButtonTitles:@"Ressources", @"Utilisateur",@"Tags",@"Cercles",@"Afficher tous les messages", nil];
    [popupQuery setTag:1];
	[popupQuery showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // 1ere partie actionSheet
    if (actionSheet.tag == 1){
        if ( buttonIndex == actionSheet.cancelButtonIndex) return;
        // On prepare le prochain UiActionSheet
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Sélectionner la valeur" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil  otherButtonTitles:nil];
        [popupQuery setTag:2];


        
        switch (buttonIndex) {
            case 0:{
                //Ressources
                NSArray * ressources = [[UtilsCore getAllRessources] mutableCopy];
                for (Ressource * res in ressources) {
                    [popupQuery addButtonWithTitle:res.nom_court];
                }
                transversal = TransversalRessource;
                popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:@"Annuler"];
                [popupQuery showInView:self.view];
            }
                
                break;
            case 1:{
                //Utilisateur
                NSArray * utilisateurs = [UtilsCore getAllUtilisateurs];
                for (Utilisateur * utilisateur in utilisateurs) {
                    [popupQuery addButtonWithTitle:utilisateur.owner_username];
                }
                transversal = TransversalUtilisateur;
                popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:@"Annuler"];
                [popupQuery showInView:self.view];
            }
                
                break;
            case 2:{
                // Tags
                NSArray * tags = [[UtilsCore getAllTag] mutableCopy];
                for (NSString * tagLocal in tags) {
                    [popupQuery addButtonWithTitle:tagLocal];
                }
                transversal = TransversalTag;
                popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:@"Annuler"];
                [popupQuery showInView:self.view];
            }
                
                break;
            case 3:{
                //cercles
                if ( ![self checkForLoggedInUser] ) return;
                NSMutableArray * cercles = [[NSMutableArray alloc]init];
                NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: nil];
                [self post:[NSString stringWithFormat:@"%@/cercle/afficherListeCercles.php",sc_server]
                     param: query
                  callback: ^(NSDictionary *wsReturn) {
                      if ( !wsReturn ) return;
                      [cercles addObjectsFromArray: [wsReturn objectForKey:@"cercles"]];
                      [cercles addObjectsFromArray: [wsReturn objectForKey:@"classes"]];
                      for (NSDictionary * cercleLocal in cercles) {
                          [popupQuery addButtonWithTitle:[cercleLocal objectForKey:@"name"]];
                      }
                      cercleArrayTemp = cercles;
                      transversal = TransversalCercle;
                      popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:@"Annuler"];
                      [popupQuery showInView:self.view];
                      
                  }];
                
                
            }
                
                break;
                
            default:
                transversal = nil;
                idRessource = nil ;
                id_utilisateur = nil;
                tag_message = nil;
                id_utilisateurs = nil;
                [self refreshMessages];
                break;
        }
        
    }
    // 2eme partie actonSheet
    if (actionSheet.tag == 2){
        // Reset Trans
        if ( buttonIndex == actionSheet.cancelButtonIndex) return;

        idRessource = nil ;
        id_utilisateur = nil;
        tag_message = nil;
        id_utilisateurs = nil;
     
        
        switch (transversal) {
            case TransversalRessource:
                idRessource = ((Ressource * )[[UtilsCore getAllRessources] objectAtIndex:buttonIndex]).id_ressource;
                [self refreshMessages];
                break;
            case TransversalUtilisateur:
                id_utilisateur = ((Utilisateur* )[[UtilsCore getAllUtilisateurs] objectAtIndex:buttonIndex]).id_utilisateur;
                [self refreshMessages];
                break;
            case TransversalTag:
                tag_message =  [actionSheet buttonTitleAtIndex:buttonIndex];
                [self refreshMessages];
                break;
            case TransversalCercle:{
                if (!id_utilisateurs) id_utilisateurs = [[NSMutableArray alloc]init];
                NSArray * membres = [((NSDictionary *)[cercleArrayTemp objectAtIndex:buttonIndex] ) objectForKey:@"members"];
                for (NSDictionary * membre in membres) {
                    [id_utilisateurs addObject:[membre objectForKey:@"guid"]];
                }
                [self refreshMessages];
                break;
            }
                
                
                
            default:
                break;
        }
    }
    if ( actionSheet.tag == 3){
        NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if([title isEqualToString:@"Or"] || [title isEqualToString:@"Argent"] || [title isEqualToString:@"Bronze"] || [title isEqualToString:@"Effacer médaille"]) {
            //Appel du webservice
            NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message", nil];
            if ( ![title isEqualToString:@"Effacer médaille"]){
                [query setObject:[title lowercaseString] forKey:@"type_medaille"];
            }
            
            
            [self post:[NSString stringWithFormat:@"%@/messages/donnerMedaille.php",sc_server]
                 param: query
              callback: ^(NSDictionary *wsReturn) {
                  [self displayErrors:wsReturn];
                  [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInt:_selectedMessagePath.row]];
                  [tableView reloadData];
              }];
        }
    }
    
    if (actionSheet.tag == 4) {
        NSURL * url = [NSURL URLWithString:[actionSheet buttonTitleAtIndex:buttonIndex]];
        [[UIApplication sharedApplication] openURL:url];
    }


}

#pragma mark -

- (void)didReceiveMemoryWarning {
   	DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



- (void)dealloc 
{
   	DLog(@"");
}


- (IBAction)selectionneProfil:(id)sender {
    NSIndexPath * index = [tableView indexPathForCell:(UITableViewCell *)[[[sender superview] superview]superview]];
    _selectedMessage = [_messages objectAtIndex:index.row] ;
    [self performSegueWithIdentifier: @"profilSegue" sender: self];
    
}
- (IBAction)selectionneLienVersRessource:(id)sender {
    NSIndexPath * index = [tableView indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
    _selectedMessage = [_messages objectAtIndex:index.row] ;
    [UtilsCore setMessageLu:_selectedMessage.id_message];
    if ( [_selectedMessage.nom_page isEqualToString:@"" ]){
        [self performSegueWithIdentifier: @"transResSegue" sender: self];
    }else{
        [self performSegueWithIdentifier: @"transPageSegue" sender: self];
    }
    
     
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    boolAfficheReponse = NO;
    if([[segue identifier] isEqualToString:@"nouveauMessageSegue"]){
        NouveauMessageController * nouveauMessageController = segue.destinationViewController;
        nouveauMessageController.currentRessource = currentRessource;
        nouveauMessageController.urlPage = urlPage;
        nouveauMessageController.nom_tag = tag;
        nouveauMessageController.num_occurence = num_occurence;
        if (modeNouveauMessage == ModeNouveauMessageReponse){
            
            // Si on est deja sur une reponse , on repond au parent
            if ( _selectedMessage.parent != nil ) {
                nouveauMessageController.idMessageOriginal = _selectedMessage.parent;
            }else{
            // Sinon c'est une reponse classique
                nouveauMessageController.idMessageOriginal = _selectedMessage.id_message;
            }
        
        }
    }
    if([[segue identifier] isEqualToString:@"profilSegue"]){
        ProfilController * profilController = segue.destinationViewController;
        profilController.compte =  _selectedMessage.owner_guid;
    }
    
    if([[segue identifier] isEqualToString:@"transResSegue"]){
        PageVC * view = segue.destinationViewController;
        view.currentRessource = [UtilsCore getRessource:_selectedMessage.id_ressource];
    }
    
    if([[segue identifier] isEqualToString:@"transPageSegue"]){
        Ressource * currentRes = [UtilsCore getRessource:_selectedMessage.id_ressource];
        NSMutableArray * pageData = [[NSMutableArray alloc] init];
        [pageData addObject:[NSString stringWithFormat:@"%@/co/%@.html",[currentRes getPath],_selectedMessage.nom_page]];
        PageVC * pageVC = segue.destinationViewController;
        
        pageVC.pageData = [pageData copy];
        pageVC.currentRessource = currentRes;
        pageVC.selectedIndex = 0;
        pageVC.tag_trans = _selectedMessage.nom_tag;
        pageVC.num_occurence_trans = _selectedMessage.num_occurence;
    }
}



@end
