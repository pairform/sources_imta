//
//  MessageCell.h
//  SupCast
//
//  Created by admin on 27/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTagList.h"
#import "Message.h"

@interface MessageCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate>
@property Message * message;
@end
