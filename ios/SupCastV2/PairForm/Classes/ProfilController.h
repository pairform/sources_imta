//
//  ViewController.h
//  profil
//
//  Created by admin on 13/03/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+WService.h"
#import "EditerCompteController.h"
#import "CercleController.h"
#import "MessageController.h"
#import "SuccesControllerViewController.h"


@interface ProfilController : UIViewController <CercleControllerDelegate,UITableViewDataSource , UIActionSheetDelegate>

@property (strong, nonatomic)NSNumber * compte;

@end
