//
//  ViewControllerListe.m
//  profil
//
//  Created by admin on 18/03/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "RechercheProfilController.h"
#import "GAI.h"


@interface RechercheProfilController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *names;
@property (strong, nonatomic) NSMutableArray *images;
@property (strong, nonatomic) NSMutableArray *id_utilisateurs;
@property NSNumber * id_ressource;


@end



@implementation RechercheProfilController
@synthesize id_ressource;


NSArray *searchResults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Recherche d'un profil"];    
    
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
	// Do any additional setup after loading the view.
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Recherche Profil"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self connect];
    // navigation button bar
    NSMutableArray     *items = [self.navigationItem.rightBarButtonItems mutableCopy] ;
    if (IS_IPHONE) {
        if ( items.count == 1){
            
            UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [a1 setFrame:CGRectMake(5,6,32,32)];
            [a1 addTarget:self action:@selector(afficherFiltres) forControlEvents:UIControlEventTouchUpInside];
            [a1 setImage:[UIImage imageNamed:@"magnifier_64"] forState:UIControlStateNormal];
            UIBarButtonItem *bFilter = [[UIBarButtonItem alloc] initWithCustomView:a1];
            [items addObject:bFilter];
            self.navigationItem.rightBarButtonItems = items;
        }
    }
    else
    {
        items = [[NSMutableArray alloc] init];
        UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [a1 setFrame:CGRectMake(5,6,32,32)];
        [a1 addTarget:self action:@selector(afficherFiltres) forControlEvents:UIControlEventTouchUpInside];
        [a1 setImage:[UIImage imageNamed:@"magnifier_64"] forState:UIControlStateNormal];
        UIBarButtonItem *bFilter = [[UIBarButtonItem alloc] initWithCustomView:a1];
        [items addObject:bFilter];
        self.navigationItem.rightBarButtonItems = items;
    }
}

-(void) afficherFiltres{
    // Filtres : Utilisateur? , ressources , groupes? , tags
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Filtrer selon une ressource" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil  otherButtonTitles:nil];
    NSArray * ressources = [[UtilsCore getAllRessources] mutableCopy];
    for (Ressource * res in ressources) {
        [popupQuery addButtonWithTitle:res.nom_court];
    }
    [popupQuery addButtonWithTitle:@"Toutes les ressources"];
    popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:@"Annuler"];
    [popupQuery showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ( buttonIndex == actionSheet.cancelButtonIndex) return;
    if ( buttonIndex == actionSheet.cancelButtonIndex -1 ) {
        id_ressource = nil;
        [self connect];
        return;
    }
    id_ressource= ((Ressource * )[[UtilsCore getAllRessources] objectAtIndex:buttonIndex]).id_ressource;
     [self connect];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
    }
    else{
        return _names.count;
    }
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"myId";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    // Si recherche , on affiche les resultats de la recherche
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        cell.textLabel.text = [searchResults objectAtIndex:indexPath.row];
    }
    else
    {
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
        
    [UtilsCore sauvegardeUtilisateur:[_id_utilisateurs objectAtIndex:indexPath.row] name:[_names objectAtIndex:indexPath.row] url:[_images objectAtIndex:indexPath.row]];
    Utilisateur * utilisateur = [UtilsCore getUtilisateur:[_id_utilisateurs objectAtIndex:indexPath.row]];
        
    UIImage * imageModifiee = [utilisateur.getImage thumbnailImage:56 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh];
        
    [imageView setImage:imageModifiee];
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:200];
    [lblName setText:utilisateur.owner_username];
    }
   
    
    // Configure the cell...
    
    return cell;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"profilSegue"])
    {
        ProfilController *viewC = [segue destinationViewController];
        // A partir de la vue recherche
        if ([self.searchDisplayController isActive]) {
            NSIndexPath *indexPath = nil;
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            int index = [_names indexOfObject:[searchResults objectAtIndex:indexPath.row]];
            viewC.compte = [NSString stringWithFormat:@"%@", [_id_utilisateurs objectAtIndex:index]];
        }
        // A partir de la vue normale
        else
        {
            NSInteger selectedIndex = [[self.tableView indexPathForSelectedRow] row];
            viewC.compte = [NSString stringWithFormat:@"%@", [_id_utilisateurs objectAtIndex:selectedIndex]];
        }
    }
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( self.mode == modeAjoutCercle){
        if ([self.searchDisplayController isActive]) {
            NSIndexPath *indexPath = nil;
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            int index = [_names indexOfObject:[searchResults objectAtIndex:indexPath.row]];
            [_delegate ajouteMembreDansCercleDepuisRechercheProfil:[NSString stringWithFormat:@"%@", [_id_utilisateurs objectAtIndex:index]]]; 
        }else{
            [_delegate ajouteMembreDansCercleDepuisRechercheProfil:[_id_utilisateurs objectAtIndex:indexPath.row]];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        [self performSegueWithIdentifier: @"profilSegue" sender: self];
    }
}





- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    searchResults = [_names filteredArrayUsingPredicate:resultPredicate];
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}




- (void)connect {
    
    //Appel du webservice
    
    NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys: nil];
    if ( id_ressource ){
        [query setObject:id_ressource forKey:@"id_ressource"];
    }
    [self post:[NSString stringWithFormat:@"%@/profil/afficherListeDeProfils.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          NSArray *entries = [wsReturn objectForKey:@"profils"];
          if ( [entries count] == 0) return;
          _names = [[NSMutableArray alloc] initWithCapacity:[entries count]];
          _images= [[NSMutableArray alloc] initWithCapacity:[entries count]];
          _id_utilisateurs = [[NSMutableArray alloc] initWithCapacity:[entries count]];
          for (NSDictionary *item in entries)
          {
              
              //Enleve son propre profil.
              //if ( [[item objectForKey:@"id_utilisateur"] integerValue ]== [[self getSessionValueForKey:@"id"] integerValue]) continue;
              
              [_names addObject:[item objectForKey:@"name"]];
              [_images addObject:[item objectForKey:@"image"]];
              [_id_utilisateurs addObject:[item objectForKey:@"id_utilisateur"]];
          }
          [_tableView reloadData];
      }];
    


}




@end
