//
//  PageWebVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 22/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "PageVC.h"
#import "ModelController.h"
#import "WebViewVC.h"
#import "MessageController.h"
#import "GAI.h"


@interface PageVC ()
@property (readonly, strong, nonatomic) ModelController *modelController;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property (strong, nonatomic) NSString *lien_message_tag;
@property (strong, nonatomic) NSNumber *lien_message_num_occurence;
@property (strong,nonatomic) IBOutlet UIButton * uiMessageTopButton;
@property (strong, nonatomic) NSArray *viewControllersSave;
@end

@implementation PageVC

@synthesize modelController = _modelController;
@synthesize pageData;
@synthesize selectedIndexPath;
@synthesize uiNavigationItem;
@synthesize currentRessource;
@synthesize tag_trans;
@synthesize num_occurence_trans;
@synthesize lien_message_tag;
@synthesize lien_message_num_occurence;
@synthesize uiMessageTopButton;
@synthesize insideResVC = _insideResVC;
@synthesize viewControllersSave;

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Lecture d'une ressource"];
    self.modelController.pageData = self.pageData;
//    self.modelController.allPages = self.allPages;
    
    self.modelController.pageVCDelegate = self;
    // Do any additional setup after loading the view, typically from a nib.
    // Configure the page view controller and add it as a child view controller.
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.delegate = self;

    
    WebViewVC *startingViewController = [self.modelController viewControllerAtIndex:self.selectedIndex storyboard:self.storyboard];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
    
    self.pageViewController.dataSource = self.modelController;
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.view.bounds;

    if (IS_IPAD) {
//        pageViewRect = CGRectInset(pageViewRect, 40.0, 40.0);
        pageViewRect.size.height -= 44;
        pageViewRect.origin.y += 44;
    }
    self.pageViewController.view.frame = pageViewRect;
    
    //Important : pas de double page. Jamais.
    self.pageViewController.doubleSided = NO;
    
    [self.pageViewController didMoveToParentViewController:self];
    
    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageViewController.gestureRecognizers;
    
    //Sauvegarde du viewcontroller en cours
    self.viewControllersSave = self.pageViewController.viewControllers;
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 640];
//        [self setPanEnabled:NO];
    }
    
    
    WebViewVC *currentViewController = self.pageViewController.viewControllers[0];
    NSString * page  = [pageData objectAtIndex:[self.modelController indexOfViewController:currentViewController]];
    
    page = [UtilsCore trimPageUrlForLocalDB:page];
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value: [NSString stringWithFormat:@"%@ : %@", self.currentRessource.nom_court, page] ];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    WebViewVC *currentViewController = self.pageViewController.viewControllers[0];
    self.insideResVC.lastPageUrlReaded = [[pageData objectAtIndex:[self.modelController indexOfViewController:currentViewController]] lastPathComponent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(IS_IPHONE)
    {
        // navigation button bar
        NSMutableArray     *items = [uiNavigationItem.rightBarButtonItems mutableCopy] ;
        // En cas de retour , n'ajoute pas de bouton en plus.
        if (items.count == 1){
            uiMessageTopButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [uiMessageTopButton setFrame:CGRectMake(5,6,32,32)];
            [uiMessageTopButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [uiMessageTopButton addTarget:self action:@selector(voirMessage:) forControlEvents:UIControlEventTouchUpInside];
            [uiMessageTopButton setBackgroundImage:[UIImage imageNamed:@"comment_64"] forState:UIControlStateNormal];
            [uiMessageTopButton setShowsTouchWhenHighlighted:true];
            [uiMessageTopButton setReversesTitleShadowWhenHighlighted:true];
            UIBarButtonItem *bEdit = [[UIBarButtonItem alloc] initWithCustomView:uiMessageTopButton];
            [items addObject:bEdit];
            uiNavigationItem.rightBarButtonItems = items;
            
        }

    }
    
    [self changeNombreDeMessageSurTopBar];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (ModelController *)modelController
{
    // Return the model controller object, creating it if necessary.
    // In more complex implementations, the model controller may be passed to the view controller.
    if (!_modelController) {
        _modelController = [[ModelController alloc] init];
    }
    return _modelController;
}

-(IBAction)voirMessage:(id)sender{
    lien_message_tag = nil;
    lien_message_num_occurence = nil;
    [self performSegueWithIdentifier: @"messageSegue" sender: self];
}
- (void)voirMessageDepuisOA:(NSString *)theTag num_occurence:(NSNumber* ) theNum_occurence{
    lien_message_tag = theTag;
    lien_message_num_occurence = theNum_occurence;
    [self performSegueWithIdentifier: @"messageSegue" sender: self];
}
-(void) selectOATrans:(UIWebView *)webView{
    if ( !tag_trans || !num_occurence_trans) return;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"JSTools" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [webView stringByEvaluatingJavaScriptFromString: jsCode];
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"scrollToElem('%@',%@);",tag_trans,num_occurence_trans]];
    // Remise a zero , une fois fini.
    tag_trans= nil;
    num_occurence_trans = nil;
    
};
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    WebViewVC *currentViewController = self.pageViewController.viewControllers[0];
    NSString * page  = [pageData objectAtIndex:[self.modelController indexOfViewController:currentViewController]];
    
    page = [UtilsCore trimPageUrlForLocalDB:page];
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value: [NSString stringWithFormat:@"%@ : %@", self.currentRessource.nom_court, page] ];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    if(IS_IPAD)
    {
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pageDidChange" object:nil userInfo:@{@"url": page}];
        
        [self.stackController popToViewController:self animated:YES];
    }
    [self changeNombreDeMessageSurTopBar];
    
}

- (void)changeNombreDeMessageSurTopBar{
    WebViewVC *currentViewController = self.pageViewController.viewControllers[0];
     NSString * page  = [pageData objectAtIndex:[self.modelController indexOfViewController:currentViewController]];
    page = [UtilsCore trimPageUrlForLocalDB:page];
    
    
    [uiMessageTopButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [uiMessageTopButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    NSNumber * nombreDeMessage = [UtilsCore getNombreMessageNonLuFromRessource:currentRessource.id_ressource nom_page:page];
    
    if ( [nombreDeMessage integerValue ] > 0){
        [uiMessageTopButton setBackgroundImage:[UIImage imageNamed:@"comment_unreaded_64"] forState:UIControlStateNormal];
        [uiMessageTopButton setTitle:[nombreDeMessage stringValue] forState:UIControlStateNormal];
        uiMessageTopButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
    }else{
        [uiMessageTopButton setTitle:@"" forState:UIControlStateNormal];
    }
    
    if ( [nombreDeMessage isEqualToNumber:@0]){
        [uiMessageTopButton setBackgroundImage:[UIImage imageNamed:@"comment_readed_64"] forState:UIControlStateNormal];
    }
    if ( [nombreDeMessage isEqualToNumber:@-1]){
        [uiMessageTopButton setBackgroundImage:[UIImage imageNamed:@"comment_64"] forState:UIControlStateNormal];
    }
    
}

#pragma mark - UIPageViewController delegate methods

/*
 - (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
 {
 
 }
 */
/*
- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (UIInterfaceOrientationIsPortrait(orientation) || ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) || ([self.modelController.pageData count] == 1)) {
        // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to YES, so set it to NO here.
        
        WebViewVC *currentViewController = self.pageViewController.viewControllers[0];
        NSArray *viewControllers = @[currentViewController];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
        
        self.pageViewController.doubleSided = NO;
        return UIPageViewControllerSpineLocationMin;
    }
    
    // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
    WebViewVC *currentViewController = self.pageViewController.viewControllers[0];
    NSArray *viewControllers = nil;
    
    NSUInteger indexOfCurrentViewController = [self.modelController indexOfViewController:currentViewController];
    if (indexOfCurrentViewController == 0 || indexOfCurrentViewController % 2 == 0 ) {
        UIViewController *nextViewController = [self.modelController pageViewController:self.pageViewController viewControllerAfterViewController:currentViewController];
        viewControllers = @[currentViewController, nextViewController];
    } else {
        UIViewController *previousViewController = [self.modelController pageViewController:self.pageViewController viewControllerBeforeViewController:currentViewController];
        viewControllers = @[previousViewController, currentViewController];
    }
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
    
    
    return UIPageViewControllerSpineLocationMid;
}
*/

//Règle le problème de reset de la lecture.
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //On sauvegarde l'état dès qu'on tourne le mobile
    self.viewControllersSave = self.pageViewController.viewControllers;
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    //Comme ça, on le récupère ici
    NSArray *viewControllers = @[self.viewControllersSave[0]];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
    
    //Si on se contente de récuperer le viewController actuel dans self.pageViewControllers.viewControllers[0], comme dans l'exemple d'Apple,
    //on constate qu'il se fait reset sans aucune raison valable avant qu'on puisse s'en servir.
    //http://stackoverflow.com/questions/14188592/uipageviewcontroller-first-rotation-resets-views-to-initial-page
    
   return UIPageViewControllerSpineLocationMin;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"messageSegue"]){
        MessageController * messageController = segue.destinationViewController;
        messageController.currentRessource = currentRessource;
        
        WebViewVC *currentViewController = self.pageViewController.viewControllers[0];
        messageController.urlPage = [pageData objectAtIndex:[self.modelController indexOfViewController:currentViewController]];
        messageController.tag = lien_message_tag;
        messageController.num_occurence = lien_message_num_occurence;
    }
}

@end
