//
//  CercleCell.h
//  PairForm
//
//  Created by admin on 12/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CercleMembreCell.h"

@interface CercleCell : UICollectionView  <UICollectionViewDataSource,UICollectionViewDelegate>
@property NSArray * members ;
@property BOOL boolEdit;
@property NSNumber * id_cercle;
@property NSIndexPath * index;

@end
