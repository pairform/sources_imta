//
//  EditerCompteController.h
//  profil
//
//  Created by admin on 29/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+WService.h"
@interface EditerCompteController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) NSString * email;
@property (strong, nonatomic) NSString * etablissement;
@property (strong, nonatomic) UIImage * avatar;

-(IBAction)modifierAvatar:(id)sender;

@end
