//
//  FileManager.h
//  SupCast
//
//  Created by Maen Juganaikloo on 03/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

+(BOOL)unzipFileAtPath:(NSString*)pathFichierZip;
@end
