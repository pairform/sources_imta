//
//  MessageOptionController.h
//  SupCast
//
//  Created by admin on 04/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTagList.h"
#import "CercleController.h"

@protocol MessageOptionControllerDelegate;

@interface MessageOptionController : UIViewController<GCTagListDelegate,GCTagListDataSource,UITextFieldDelegate,CercleControllerDelegate>

@property (strong, nonatomic) NSMutableArray *tagArray;
@property (strong, nonatomic) NSMutableArray *visibiliteArray;
@property (strong, nonatomic) NSMutableArray *visibiliteIdArray;
@property(nonatomic) BOOL  boolDefi;
@property(strong, nonatomic) NSNumber * id_ressource;
@property (strong,nonatomic ) NSNumber *  idMessageOriginal;

@property (weak, nonatomic) id<MessageOptionControllerDelegate> delegate;
-(IBAction)validerOptions:(id)sender;
@end

@protocol MessageOptionControllerDelegate
- (void)recupereOptions:(NSMutableArray *)visibilite visibiliteId:(NSMutableArray *)visibiliteId tag:(NSMutableArray *)tag defi: (BOOL) defi ;


@end

