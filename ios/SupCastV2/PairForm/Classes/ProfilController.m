//
//  ViewController.m
//  profil
//
//  Created by admin on 13/03/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "ProfilController.h"
#import "CMPopTipView.h"
#import "GAI.h"

@interface ProfilController ()
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *etablissement;

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *succes_label;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionSucces;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionRessources;
@property (weak, nonatomic) IBOutlet UIButton *bMessages;
@property (weak, nonatomic) IBOutlet UIButton *bGroupe;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItemCurrent;

@property (strong, nonatomic) NSDictionary *json;
@property (strong, nonatomic) NSMutableArray *succes;
@property (strong, nonatomic) NSMutableArray *succesDerniers;
@property (strong, nonatomic) NSMutableArray *succesNonGagnes;
@property (strong, nonatomic) NSMutableArray *arrayRes;





@end

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1
@implementation ProfilController


@synthesize collectionRessources;
@synthesize collectionSucces;
@synthesize compte ;
@synthesize json;
@synthesize arrayRes;
@synthesize succes;
@synthesize succesDerniers;
@synthesize succesNonGagnes;

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Visionnage d'un profil"];
    [self.collectionRessources setClipsToBounds:NO];
//    [self connect];
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Profil"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self connect];
    
}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    //    self.actionButton.center = [[self.view viewWithTag:20] center];
    [self resizeSubViews];
}



-(void) resizeSubViews{
    NSLayoutConstraint * constraint = collectionRessources.constraints[0];
    constraint.constant = collectionRessources.contentSize.height;
}



- (void)connect {
    

 
    //Appel du webservice
    
    NSDictionary * query ;
    if ( compte == nil ){
         query = [[NSDictionary alloc] initWithObjectsAndKeys:nil];
            
    }
    else{
        query = [[NSDictionary alloc] initWithObjectsAndKeys:compte,@"id_utilisateur", nil];
    }
        
    
    [self post:[NSString stringWithFormat:@"%@/profil/afficherProfil.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          json = wsReturn;
          NSString* imageUrl = [json objectForKey:@"image"]; //2
          
          DLog(@"image: %@", imageUrl); //3
          
          NSURL *uneImage = [NSURL URLWithString: imageUrl];
          
          _name.text = [json objectForKey:@"name"];
          UIImage * imageModifiee = [[UIImage imageWithData: [NSData dataWithContentsOfURL: uneImage]] thumbnailImage:88 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
          
          _avatar.image = imageModifiee;
          _email.text = [json objectForKey:@"email"];
          
          
          if ([json objectForKey:@"etablissement"] == [NSNull null]) {
              _etablissement.text = @"";
          }else{
              _etablissement.text =  [json objectForKey:@"etablissement"];
          }
          
          
          
          if ([json objectForKey:@"res"] == [NSNull null]) {
              arrayRes = [[NSMutableArray alloc] init];
          }else{
              arrayRes = [json objectForKey:@"res"];
          }
          
         
          if ([json objectForKey:@"succes"] == [NSNull null]) {
              succes = [[NSMutableArray alloc] init];
          }else{
              succes = [[json objectForKey:@"succes"] mutableCopy];
              if ( [json objectForKey:@"succesDerniers"] != [NSNull null] ) 
              succesDerniers =  [[json objectForKey:@"succesDerniers"]  mutableCopy];
              
          }
          
          succesNonGagnes = [json objectForKey:@"succesNonGagnes"];

          
          
          _succes_label.text = [NSString stringWithFormat:@"Derniers succès (%i/%i)",[succes count],[succes count]+[succesNonGagnes count]];
          
          
          
          [collectionSucces reloadData];
          [collectionRessources reloadData];
          
          NSString* string = [json objectForKey:@"isOwner"]; //2
          if ( [string isEqualToString:@"yes"]  ){
              [_bGroupe setTitle:@"Gérer cercles" forState:UIControlStateNormal];
              NSMutableArray     *items = [_navigationItemCurrent.rightBarButtonItems mutableCopy] ;
              
              if (IS_IPAD)
              {
                  if ( items.count == 0)
                  {
                      items = [[NSMutableArray alloc] init];
                      UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                      [a1 setFrame:CGRectMake(5,6,32,32)];
                      [a1 addTarget:self action:@selector(editerCompte) forControlEvents:UIControlEventTouchUpInside];
                      [a1 setImage:[UIImage imageNamed:@"gear_64"] forState:UIControlStateNormal];
                      UIBarButtonItem *bEdit = [[UIBarButtonItem alloc] initWithCustomView:a1];
                      [items addObject:bEdit];
                      _navigationItemCurrent.rightBarButtonItems = items;
                  }
              }
              else
              {
                  if ( items.count == 1){

                      UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                      [a1 setFrame:CGRectMake(5,6,32,32)];
                      [a1 addTarget:self action:@selector(editerCompte) forControlEvents:UIControlEventTouchUpInside];
                      [a1 setImage:[UIImage imageNamed:@"gear_64"] forState:UIControlStateNormal];
                      UIBarButtonItem *bEdit = [[UIBarButtonItem alloc] initWithCustomView:a1];
                      [items addObject:bEdit];
                      _navigationItemCurrent.rightBarButtonItems = items;
                  }
              }
          }
      }];
}

-(void)editerCompte{
    [self performSegueWithIdentifier: @"editerCompteSegue" sender: self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView == self.collectionSucces) {
        return  [succes count];
    }
    else{
        return [arrayRes count];
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.collectionSucces) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myId" forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        UIImage * image = [UIImage imageNamed:[[succesDerniers objectAtIndex:indexPath.row] objectForKey:@"image"]];
        if ( image )[imageView setImage:image];
        else [imageView setImage:[UIImage imageNamed:@"succes.png"]];
        return cell;

    }
    else{
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myIdRank" forIndexPath:indexPath];
        //[cell setBackgroundColor:[UIColor greenColor]];
        NSDictionary * currentRessourceDict =  [arrayRes objectAtIndex:indexPath.row];
        
        //Autoriser l'overflow de la cell
        [cell setClipsToBounds:NO];
        
        //Récup image ressource et affichage
        Ressource * currentRessource = [UtilsCore getRessource:currentRessourceDict[@"id_ressource"]];
        
        UIImageView * icone_ressource = (UIImageView*)[cell viewWithTag:50];
        if(currentRessource)
            [icone_ressource setImage: [currentRessource getIcone]];
        else
            [icone_ressource setImage:[UIImage imageNamed:@"noRessourcePic"]];
        
        UILabel *lblName = (UILabel *)[cell viewWithTag:100];
        [lblName setText: [[arrayRes objectAtIndex:indexPath.row] objectForKey:@"nom"]];
        
        UIProgressView *progress = (UIProgressView *)[cell viewWithTag:200];
        int score = [[[arrayRes objectAtIndex:indexPath.row] objectForKey:@"score"] integerValue];
        int maxScore = [[[arrayRes objectAtIndex:indexPath.row] objectForKey:@"max_score"] integerValue];
        int id_categorie = [[[arrayRes objectAtIndex:indexPath.row] objectForKey:@"id_categorie"] integerValue];
        // Expert ou admin
        if ( id_categorie == 4 || id_categorie == 5){
            [progress setProgress: 1];
            [progress setProgressTintColor:[UIColor blueColor]];
        }else{
            [progress setProgress: (float)score/maxScore];
        }
        
        UILabel *lblRank = (UILabel *)[cell viewWithTag:250];
        [lblRank setText: [[arrayRes objectAtIndex:indexPath.row] objectForKey:@"categorie"]];
        
        UILabel *lblPts = (UILabel *)[cell viewWithTag:300];
        [lblPts setText: [NSString stringWithFormat:@"%i Pts",score]];
        
        [self resizeSubViews];
        return cell;
        
    }
    
    
      
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionSucces) {
        return CGSizeMake(50.f, 50.f);
    }else{
//        return CGSizeMake(self.collectionRessources.frame.size.width-10.f, 70.f);
        return CGSizeMake(158, 70);
    }
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionSucces) {
        
         printf("Selected View index=%d",indexPath.row);
        
    }
    else{
        printf("Selected View index (rank)=%d",indexPath.row);
        NSDictionary * currentRessourceDict =  [arrayRes objectAtIndex:indexPath.row];
        int rank = [[[[self getSessionValueForKey:@"rank"] objectForKey:currentRessourceDict[@"id_ressource"]] objectForKey:@"id_categorie"] integerValue];
        if ( rank > 3){
            UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Veuillez choisir un rang temporaire" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil  otherButtonTitles:@"Rang Normal",@"Collaborateur",@"Animateur",@"Expert", nil];
            [popupQuery setTag:[currentRessourceDict[@"id_ressource"] integerValue]];
            [popupQuery showInView:self.view];
        }
     
        
    }
   
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Annuler"] ){
        return;
    }
    NSString * cat;
    if([title isEqualToString:@"Rang Normal"] ){
        cat = @"0";
    }
    if([title isEqualToString:@"Collaborateur"] ){
        cat = @"2";
    }
    if([title isEqualToString:@"Animateur"] ){
        cat = @"3";
    }
    if([title isEqualToString:@"Expert"] ){
        cat = @"4";
    }
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:compte,@"id_utilisateur",[NSString stringWithFormat:@"%ld",(long)[actionSheet tag ]],@"id_ressource",cat,@"id_categorie", nil];
    [self post:[NSString stringWithFormat:@"%@/profil/changerRole.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          [self connect];
          [self displayErrors:wsReturn];
      }];
    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    // Mode "Ajout a cercle" depuis le profil d'un membre
    NSString* string = [json objectForKey:@"isOwner"];
    if([identifier isEqualToString:@"cercleSegue"] && [string isEqualToString:@"no"]   )
    {
        if ( [self checkForLoggedInUser] )
        return YES;
        else return NO;
            
    }
    // by default perform the segue transition
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DLog(@"segue identififer = %@",segue.identifier);
    if([[segue identifier] isEqualToString:@"editerCompteSegue"])
    {
        EditerCompteController *viewC = [segue destinationViewController];
        viewC.email = _email.text;
        viewC.etablissement = _etablissement.text;
        viewC.avatar = _avatar.image;
        
    }
    
    // Mode "Ajout a cercle" depuis le profil d'un membre
    NSString* string = [json objectForKey:@"isOwner"];
    if([[segue identifier] isEqualToString:@"cercleSegue"] && [string isEqualToString:@"no"]  )
    {
        CercleController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        viewC.modeCercle = ModeCercleAjoutDepuisProfil;
        
    }
    if([[segue identifier] isEqualToString:@"messageSegue"]  )
    {
        MessageController *viewC = [segue destinationViewController];
        viewC.modeMessage = ModeMessageTransversal;
        viewC.id_utilisateur = compte;
        
    }
    if([[segue identifier] isEqualToString:@"succes"]  )
    {
        SuccesControllerViewController *viewC = [segue destinationViewController];
        viewC.succesNonGagnes = succesNonGagnes;
        viewC.succes = succes;
        
    }
   
}
- (void)actionAPartirDeCercle:(NSNumber*) idCercle nomCercle:(NSString *) nomCercle{
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:compte,@"id_utilisateur",idCercle,@"id_collection", nil];
    [self post:[NSString stringWithFormat:@"%@/cercle/ajouterMembre.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          // Lancer notif
          // TODO
          
      }];
    
}

@end
