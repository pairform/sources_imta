//
//  TelechargementVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ressource.h"

@interface TelechargementVC : UIViewController

@property (nonatomic, strong) Ressource * currentRessource;
@property (nonatomic) BOOL isUpdate;
@property (nonatomic, weak) IBOutlet UIImageView * iconeRes;
@property (nonatomic, weak) IBOutlet UILabel * nomCourt;
@property (nonatomic, weak) IBOutlet UILabel * nomLong;
@property (nonatomic, weak) IBOutlet UILabel * etablissement;
@property (nonatomic, weak) IBOutlet UILabel * theme;
@property (nonatomic, weak) IBOutlet UILabel * auteur;
@property (nonatomic, weak) IBOutlet UILabel * licence;
@property (nonatomic, weak) IBOutlet UILabel * created;
@property (nonatomic, weak) IBOutlet UILabel * updated;
@property (nonatomic, weak) IBOutlet UILabel * taille;
@property (nonatomic, weak) IBOutlet UITextView * description_res;
@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet UIView * contentView;


@property (nonatomic, weak) IBOutlet UIButton * actionButton;


-(IBAction)download:(id)sender;
-(void)updateButtonComparingLocalDate:(NSDate*)localDate distantDate:(NSDate*)distantDate;
@end
