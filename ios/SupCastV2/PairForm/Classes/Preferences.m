//
//  Preferences.m
//  SupCast
//
//  Created by Maen Juganaikloo on 26/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "Preferences.h"
#import "SIAlertView.h"

@interface Preferences ()

@property (nonatomic,weak) IBOutlet UISwitch * rememberMe;
@property (nonatomic,weak) IBOutlet UISwitch * accountTwitter;
@property (nonatomic,weak) IBOutlet UISwitch * publishTwitter;
@property (nonatomic,weak) IBOutlet UISwitch * geolocalisation;
@property (nonatomic,weak) IBOutlet UISwitch * automaticUpdate;
@property (nonatomic,weak) IBOutlet UISlider * updateFrequency;
@property (nonatomic,weak) IBOutlet UISwitch * tutorialMode;

@property (nonatomic, strong) NSString * usernameGPlus;
@property (nonatomic, strong) NSString * passwordGPlus;
@property (nonatomic) int updateFrequencyValue;

-(IBAction)accountTwitterSwitched:(id)sender;
-(IBAction)updateFrequencyChanged:(id)sender;
-(IBAction)automaticUpdateChanged:(id)sender;

@end

@implementation Preferences

@synthesize rememberMe, accountTwitter, publishTwitter, geolocalisation, automaticUpdate, updateFrequency, tutorialMode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateFieldsWithPrefs];
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
	// Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    NSNumber * ufNumber = self.automaticUpdate.on ? [NSNumber numberWithFloat:self.updateFrequency.value] : @0;

    NSDictionary * parametres = @{@"rememberMe" : [NSNumber numberWithBool:self.rememberMe.on],
                                  @"usernameGPlus" : self.usernameGPlus,
                                  @"passwordGPlus" : self.passwordGPlus,
                                  @"publishTwitter" : [NSNumber numberWithBool:self.publishTwitter.on],
                                  @"geolocalisation" : [NSNumber numberWithBool:self.geolocalisation.on],
                                  @"automaticUpdate" : [NSNumber numberWithBool:self.automaticUpdate.on],
                                  @"updateFrequency" : ufNumber,
                                  @"tutorialMode" : [NSNumber numberWithBool:self.tutorialMode.on]};
    [preferences setPersistentDomain:parametres forName:kPreferences];   
}

-(void) updateFieldsWithPrefs
{
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];

    NSDictionary * parametres = [preferences persistentDomainForName: kPreferences];
    
    [self.rememberMe setOn: [parametres[@"rememberMe"] boolValue]];
    
    self.usernameGPlus = parametres[@"usernameGPlus"];
    self.passwordGPlus = parametres[@"passwordGPlus"];
    
    //S'il y a un username pour twiiter d'enregistré, on met le switch sur on
//    if(![self.usernameGPlus isEqualToString:@""])
//    {
//        [self.accountTwitter setOn:YES];
//        [self.publishTwitter setEnabled:YES];
//    }
//    else
//    {
//        [self.accountTwitter setOn:NO];
//        [self.publishTwitter setEnabled:NO];
//    }
    
    [self.publishTwitter setOn: [parametres[@"publishTwitter"] boolValue]];
    [self.geolocalisation setOn: [parametres[@"geolocalisation"] boolValue]];
    [self.automaticUpdate setOn: [parametres[@"automaticUpdate"] boolValue]];
    [self.tutorialMode setOn: [parametres[@"tutorialMode"] boolValue]];
    
    if([parametres[@"updateFrequency"] floatValue] == 0)
    {
        [self.automaticUpdate setOn:NO];
        [self.updateFrequency setValue:1];
        [self.updateFrequency setEnabled:NO];
    }
    else
    {
        [self.automaticUpdate setOn:YES];
        [self.updateFrequency setValue:[parametres[@"updateFrequency"] floatValue]];
    }
    
}


-(IBAction)accountTwitterSwitched:(id)sender{
    if (self.accountTwitter.on) {
        UITextField * pseudoField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        pseudoField.placeholder = @"Pseudo...";
                
        UITextField * passField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        passField.placeholder = @"Pass...";
        passField.secureTextEntry = YES;
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Login Twitter" andTextFields:@[pseudoField,passField]];
        [alertView addButtonWithTitle:@"Annuler"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Annulation du log");
                                  [self.accountTwitter setOn:NO animated:YES];
                              }];
        [alertView addButtonWithTitle:@"Login"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Login Twitter");
                                  [self.view makeToast:@"Connexion à Twitter réussie!" duration:3.0 position:@"bottom" image:[UIImage imageNamed:@"checkmark_48"]];
                                  
                                  //Enregistrement des usernames dans des variables
                                  [self setUsernameGPlus:pseudoField.text];
                                  [self setPasswordGPlus:passField.text];
                                  [self.publishTwitter setEnabled:YES];
                              }];
        
        alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
        alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        
        [alertView show];
    }
    else
    {
        //Enregistrement des usernames dans des variables
        [self setUsernameGPlus:@""];
        [self setPasswordGPlus:@""];
        [self.publishTwitter setEnabled:NO];
    }
}

-(IBAction)automaticUpdateChanged:(id)sender{
    if (self.automaticUpdate.on)
    {
        [self.updateFrequency setEnabled:YES];
        [self.view makeToast:@"Déplacez le curseur pour adapter la fréquence de rafraichissement" duration:2 position:@"bottom" image:[UIImage imageNamed:@"alarm_48"]];
        if ( [[NSNumber numberWithFloat:self.updateFrequency.value] intValue] == kMessageUpdateEveryMinute) [UtilsCore startPullingTimer];
    }
    else
    {
        [self.updateFrequency setEnabled:NO];
        self.updateFrequencyValue = kMessageUpdateNever;
        [self.view makeToast:@"Mise à jour manuelle (pull-to-refresh)" duration:2 position:@"bottom" image:[UIImage imageNamed:@"alarm_48"]];
        [UtilsCore stopPullingtimer];
    }

}

-(IBAction)updateFrequencyChanged:(id)sender
{
    int val = [[NSNumber numberWithFloat:self.updateFrequency.value] intValue];
    
    //Si on a une nouvelle valeur
    if (val != self.updateFrequencyValue) {
        switch (val) {
            case kMessageUpdateWhenLaunchApp:
                [self.view makeToast:@"Mettre à jour lors du lancement de l'application" duration:1 position:@"bottom" image:[UIImage imageNamed:@"alarm_48"]];
                    [UtilsCore stopPullingtimer];
                break;
                
            case kMessageUpdateWhenEnterRes:
                [self.view makeToast:@"Mettre à jour lors de la consultation d'une ressource" duration:1 position:@"bottom" image:[UIImage imageNamed:@"alarm_48"]];
                    [UtilsCore stopPullingtimer];
                break;
                
            case kMessageUpdateEveryMinute:
                [self.view makeToast:@"Mettre à jour toutes les minutes" duration:1 position:@"bottom" image:[UIImage imageNamed:@"alarm_48"]];
                    [UtilsCore startPullingTimer];
                break;
            default:
                [self.view makeToast:@"Bug du slider."];
                break;
        }
    }
    
    self.updateFrequencyValue = val;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
