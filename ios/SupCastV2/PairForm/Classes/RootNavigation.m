//
//  RootNavigation.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "RootNavigation.h"
#import "JASidePanelController.h"
#import "Toast+UIView.h"
#import "Ressource.h"

@interface RootNavigation ()

@end

@implementation RootNavigation

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Lancement de l'application"];    
    self.delegate = self;
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]];
    
    [self.view setBackgroundColor:background];

//    Custom background image pour la navigationBar
//    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.navigationBar.frame];
//    imageView.contentMode = UIViewContentModeLeft;
//    imageView.image = [UIImage imageNamed:@"NavBar-iPhone.png"];
//    [self.navigationBar insertSubview:imageView atIndex:0];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastDownloadedRessource:)
                                                 name:@"downloadToast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastErrorRessource:)
                                                 name:@"downloadErrorToast" object:nil];
}

-(void)toastDownloadedRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    
    [self.view makeToast:@"Téléchargement terminé!"
                duration:3.0
                position:@"bottom"
                   title:downloadedRessource.nom_court
                   image:[downloadedRessource getIcone]];
    
    
}

-(void)toastErrorRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    
    [self.view makeToast:@"Une erreur est survenue pendant le téléchargement, veuillez réessayer."
                duration:4.0
                position:@"bottom"
                   title:downloadedRessource.nom_court
                   image:[downloadedRessource getIcone]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    viewController.navigationItem.rightBarButtonItem = [(JASidePanelController*)[self parentViewController] customRightButtonForCenterPanel];
    
//    UIImage *backButtonImage = [UIImage imageNamed:@"precedent.png"];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
//    [[UIBarButtonItem appearance] setBackButtonBackgroundImage: backButtonImage forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];

//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage *backBtnImage = [UIImage imageNamed:@"precedent.png"];
//    
//    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
//    
//    backBtn.frame = CGRectMake(0, 0, 30, 30);
//    
//    [backBtn addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
//    [backBtn setShowsTouchWhenHighlighted:true];
//    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    [backButton setTitle:@"."];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
//                                   initWithTitle:@"Retour" style:UIBarButtonItemStyleBordered
//                                   target:nil action:nil];
//    
//    viewController.navigationItem.backBarButtonItem = backButton;
//    viewController.navigationItem.rightBarButtonItems = @[backButton,[(JASidePanelController*)[self parentViewController] rightButtonForCenterPanel]];
    
    if([navigationController.viewControllers count ] > 1) {
        UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0,0,44,44)];
        UIButton *myBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [myBackButton setFrame:CGRectMake(5,6,32,32)];
        [myBackButton setImage:[UIImage imageNamed:@"arrow_left_64"] forState:UIControlStateNormal];
        [myBackButton setEnabled:YES];
        [myBackButton setReversesTitleShadowWhenHighlighted:true];
        [myBackButton setShowsTouchWhenHighlighted:true];
        [myBackButton addTarget:viewController.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
        [backButtonView addSubview:myBackButton];
        
        UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
        viewController.navigationItem.leftBarButtonItem = backButton;
    }
}

@end
