//
//  EnregistrementVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface EnregistrementVC : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) NSArray *dataSource;

-(UIViewController*)viewControllerFromUIView:(UIView*)view;

@property(nonatomic,weak) IBOutlet UITextField *nomTextField;
@property(nonatomic,weak) IBOutlet UITextField *prenomTextField;
@property(nonatomic,weak) IBOutlet UITextField *eMailTextField;
@property(nonatomic,weak) IBOutlet UITextField *pseudoTextField;
@property(nonatomic,weak) IBOutlet UITextField *motDePasseTextField;
@property(nonatomic,weak) IBOutlet UITextField *motDePasseTextField2;
@property(nonatomic,weak) IBOutlet UITextField *etablissementTextField;

@property(nonatomic,strong) NSString *nomUtilisateur;
@property(nonatomic,strong) NSString *prenomUtilisateur;
@property(nonatomic,strong) NSString *eMailUtilisateur;
@property(nonatomic,strong) NSString *pseudoUtilisateur;
@property(nonatomic,strong) NSString *motDePasseUtilisateur;
@property(nonatomic,strong) NSMutableString *messageString;

-(void)enregistrer;

-(IBAction)backgroundTap:(id)sender;
-(void) inscription;
-(NSString *)dataFilePath:(NSString *)fileName; 
//-(void)applicationWillTerminate:(NSNotification *)notification;

@end