

#import "HomeVC.h"
#import "Cell.h"
#import "InsideRessourceVC.h"
#import "TSPopoverController.h"
#import "TSActionSheet.h"
#import "UtilsCore.h"
#import "SIAlertView.h"
#import "MKNetworkEngine.h"
#import "GAI.h"

NSString *kCellID = @"cellRes";                          // UICollectionViewCell storyboard id

@implementation HomeVC
@synthesize ressourcesArray = _ressourcesArray;
@synthesize springBoard;
@synthesize downloadingRessourcesArray = _downloadingRessourcesArray;

-(void)viewDidLoad
{
//    [TestFlight passCheckpoint:@"Vision des ressources"];
//    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
//    [[NSFileManager defaultManager] fileExistsAtPath:pathFichierZip];
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(finishDownloadingRessource:)
//                                                 name:@"downloadFinished" object:nil];
//    
    _ressourcesArray = [[UtilsCore getAllRessources] mutableCopy];
    _downloadingRessourcesArray = [[NSMutableArray alloc] init];
    if(IS_IPAD)
    {
        UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]];
        [self.view setBackgroundColor:background];
        [self setStackWidth: 640];        
    }
    
    //Initialisation de la collection
    
    CGRect pageViewRect = self.view.bounds;
    
    if (IS_IPAD) {
        pageViewRect.size.height -= 44;
        pageViewRect.origin.y += 44;
    }
    
    springBoard = [[PTSSpringBoard alloc] initWithFrame:pageViewRect];
    
    springBoard.delegate = self;
    springBoard.dataSource = self;
    
    [self.view addSubview:springBoard];
    
//    [springBoard updateSpringboard];

    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Mes ressources"];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];

    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    //Vieux hack : sur iPhone, il y a deux layers en permanence
    //Sur iPad, il n'y en a qu'un à l'origine
    if([[[UIApplication sharedApplication] windows] count] == (IS_IPHONE+1))
    {
        if([self checkForDefaultUser])
        {
            [self checkForLoggedInUser];
        }
        else
        {
            DLog(@"Pas de defaultUser");
        }
        
        //Si l'utilisateur n'a pas de ressource installée
        if(([_ressourcesArray count] == 0) && [[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunchUnzip"])
        {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Aucune ressource" andMessage:@"Vous n'avez aucune ressource téléchargée : voulez-vous accéder au store pour naviguer parmi les ressources disponibles?"];
            [alertView addButtonWithTitle:@"Non"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alertView) {
                                      DLog(@"Ne veut pas accéder au store");
                                  }];
            [alertView addButtonWithTitle:@"Oui"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      DLog(@"Accède au store");
                                      [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"] animated:YES];
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            [alertView show];
            
        }
        
    }

    [springBoard updateSpringboard];
}

-(void)viewWillAppear:(BOOL)animated
{
    DLog(@"");
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerDownloadingRessource:)
                                                 name:@"downloadStarted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgressionForRessource:)
                                                 name:@"downloadProgressed" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishDownloadingRessource:)
                                                 name:@"downloadFinished" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorDownloadingRessource:)
                                                 name:@"downloadError" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"unzipFinished" object:nil];
    
}

-(void)refreshMessages{
    [springBoard updateSpringboard];
}
-(void)viewWillDisappear:(BOOL)animated
{
    DLog(@"");
    if([springBoard isEditing])
        [springBoard toggleEditingMode:NO];
    
    [super viewWillDisappear:animated];
    
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadStarted" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadProgressed" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadFinished" object:nil];
}

#pragma mark - Notifications Handlers 

-(void)registerDownloadingRessource:(NSNotification *)notification
{
    DLog(@"");
    if([_downloadingRessourcesArray indexOfObject:notification.object] == NSNotFound)
    {
        int index_ressource = -1;
        
        [_downloadingRessourcesArray addObject:notification.object];
        
        if ([notification.userInfo[@"isUpdate"] boolValue])
        {
            
            for (Ressource * ressource in _ressourcesArray) {
                if ([ressource.id_ressource isEqualToNumber:((Ressource*) notification.object).id_ressource]) {
                    index_ressource = [_ressourcesArray indexOfObject:ressource];
                    break;
                }
            }
            if (index_ressource != -1)
            {
                [_ressourcesArray removeObjectAtIndex:index_ressource];
                [_ressourcesArray insertObject:notification.object atIndex:index_ressource];
            }
        
        }
        //Si ce n'est pas une mise à jour de ressource
        else
        {
            [_ressourcesArray addObject:notification.object];
            index_ressource = [_ressourcesArray count] -1;
        }
        
        [springBoard updateItemAtIndex:index_ressource];
    }
}

-(void)updateProgressionForRessource:(NSNotification*) notification
{
    //Si HomeVC a été désinstencié depuis le début du download, on réenregistre la ressource en cours de téléchargement
    if([_downloadingRessourcesArray indexOfObject:notification.object] == NSNotFound)
        [self registerDownloadingRessource:notification];
    
    int index = [_ressourcesArray indexOfObject:notification.object];
//    Ressource * downloadingRessource = notification.object;
    //Quand le springBoard est restauré (et pas nouvellement instanciée), une ImageView vient se glisser dans les subviews, et fausse l'index.
    //On check donc que le nombre de subviews et de ressources dans l'array sont égaux
    NSArray * springBoardItems = [[[springBoard subviews] objectAtIndex:0] subviews];
    
    //Sinon, on ajoute 1 à l'index pour contourner le problème
    index += [springBoardItems count] == [_ressourcesArray count] ? 0 : 1;
    
    PTSSpringBoardItem * item = [springBoardItems objectAtIndex:index];
//    UIView * item = [springBoard viewWithTag:downloadingRessource.id_ressource];
    UIProgressView * progressBar = (UIProgressView*)[item viewWithTag:100];
    float convertedProgress = [(NSNumber*)notification.userInfo[@"progress"] floatValue];
    
    DLog(@"%.2f",convertedProgress*100);
//    progressBar.progress = convertedProgress;
    [progressBar setProgress:convertedProgress animated:YES];

}

-(void)finishDownloadingRessource:(NSNotification*) notification
{
    DLog(@"");
    
    //Si pas d'objet de notification, on doit recharger toute la vue
    if (notification.object == nil) {
        
        _ressourcesArray = [[UtilsCore getAllRessources] mutableCopy];
        [springBoard updateSpringboard];
        [springBoard setNeedsDisplay];
    }
    else{
        Ressource * downloadingRessource = notification.object;
        
        int index = [_ressourcesArray indexOfObject:notification.object];
        
        [_downloadingRessourcesArray removeObject:notification.object];
        [_ressourcesArray removeObject:notification.object];
        
        //Si l'index est égal à 0, on ajoute plutôt qu'insérer
        if (index)
            [_ressourcesArray insertObject:[UtilsCore getRessource:downloadingRessource.id_ressource] atIndex:index];
        else
            [_ressourcesArray addObject:[UtilsCore getRessource:downloadingRessource.id_ressource]];
        
        [springBoard updateItemAtIndex:index];
    }
    
}

-(void)errorDownloadingRessource:(NSNotification*) notification{
    DLog(@"");
    if([_downloadingRessourcesArray indexOfObject:notification.object] != NSNotFound)
    {
//        int index = [_ressourcesArray indexOfObject:notification.object];
        [_downloadingRessourcesArray removeObject:notification.object];
//        [springBoard updateItemAtIndex:index];
        [_ressourcesArray removeObject:notification.object];
        [springBoard updateSpringboard];
    }
    
}

#pragma mark - Gestion des ressources

-(IBAction) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Gestion des ressources"];
    
    [actionSheet addButtonWithTitle:@"Ajouter" block:^{
        if(IS_IPHONE)
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"] animated:YES];
        else
            [self.stackController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"] fromViewController:self animated:YES];
    }];
    [actionSheet addButtonWithTitle:@"Editer" block:^{
        [self.springBoard toggleEditingMode];
    }];
    [actionSheet cancelButtonWithTitle:@"Annuler" block:nil];
    actionSheet.cornerRadius = 5;
    actionSheet.popoverGradient = true;
    actionSheet.titleShadow = true;
    
    
    [actionSheet showWithTouch:event];
}

-(void) getRessourcesFromStorage{
    
    NSArray * documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory = [documentPath objectAtIndex:0];
    NSArray * documentDirectoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentDirectory error:nil];
    
    _ressourcesArray = [[NSMutableArray alloc] initWithCapacity:([documentDirectoryContent count] -1)];
    
    if([documentDirectoryContent count] != 0)
    {
        for(NSString * ressourceName in documentDirectoryContent)
        {
            //Patch rapide : __MACOSX se met dans le dossier à cause du dezippeur, a voir plus tard.
            if(![ressourceName isEqualToString:@"__MACOSX"] && ![[ressourceName pathExtension] isEqualToString:@"sqlite"])
            {
                NSString * ressourceFolderPath = [NSString stringWithFormat:@"%@/%@",documentDirectory,ressourceName];
                //                [[NSFileManager defaultManager] contentsOfDirectoryAtPath:ressourceFolderPath error:nil];
                
                NSString * ressourceImagePath = [NSString stringWithFormat:@"%@/%@.png",ressourceFolderPath, ressourceName];
                UIImage * ressourceImage = [UIImage imageWithContentsOfFile:ressourceImagePath];
                
                [_ressourcesArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:ressourceName,@"title",ressourceImage, @"image", ressourceFolderPath, @"path", nil]];
            }
        }
    }
    
}

#pragma mark - CollectionView Delegate
//
//- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
//{
//    return [_ressourcesArray count];
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
//{
//    // we're going to use a custom UICollectionViewCell, which will hold an image and its label
//    //
////    NSDictionary * currentRessource = [_ressourcesArray objectAtIndex:indexPath.row];
//    
//    Ressource * currentRessource = [_ressourcesArray objectAtIndex:indexPath.row];
//    
//    Cell *cell = [cv dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
//    
////    cell.label.text = [currentRessource objectForKey:@"title"];
//    cell.label.text = currentRessource.nom_court;
//    cell.label.textColor = [UIColor blackColor];
//    
//    // load the image for this cell
////    cell.image.image = [currentRessource objectForKey:@"image"];
//    cell.image.image = [currentRessource getIcone];
//    
//    return cell;
//}
//
//
// the user tapped a collection item, load and set the image on the detail view controller
//
//
//#pragma mark - Navigation
// 
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if([segue.identifier isEqualToString:@"resClick"])
//    {
//        int index = [[[self.collectionView indexPathsForSelectedItems] objectAtIndex:0] row];
//        
//        InsideRessourceVC * insideRessourceVC = segue.destinationViewController;
//        insideRessourceVC.currentRessource = _ressourcesArray[index];
//    }
//}

#pragma mark -
#pragma mark PTSSpringBoardDataSource protocol

-(NSInteger)numberOfItemsInSpringboard:(PTSSpringBoard *)springboard
{
    DLog(@"");
    return [_ressourcesArray count];
}

-(PTSSpringBoardItem*)springboard:(PTSSpringBoard *)springboard itemForIndex:(NSInteger)index
{
    DLog(@"");
    PTSSpringBoardItem * item = [[PTSSpringBoardItem alloc] init];
    Ressource * currentRessource = [_ressourcesArray objectAtIndex:index];
    
    [item setLabelFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
    [item setLabelColor:[UIColor darkGrayColor]];
    
    [item setLabel:currentRessource.nom_court];
    
    if([_downloadingRessourcesArray indexOfObject:currentRessource] != NSNotFound)
    {
        
        [item setIcon: [self convertToGrayscale:[currentRessource getIcone]]];

        //Barre de progression de téléchargement
        UIProgressView * progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        [progressBar setTag:100];
        [progressBar setFrame:CGRectMake(25, 65, 50, 12)];
        
        [item addSubview:progressBar];
        
        [item setItemIdentifier:@"dl"];
    }
    else
    {
        [item setIcon:[currentRessource getIcone]];
        [item setItemIdentifier:[currentRessource.id_ressource stringValue]];
        [item setBadgeValue:  [[UtilsCore getNombreMessageNonLuFromRessourceTotale:currentRessource.id_ressource] stringValue ]];
    }

    return item;
}

-(BOOL)springBoardShouldEnterEditingMode:(PTSSpringBoard *)springboard {
    return YES;
}

-(BOOL)springboard:(PTSSpringBoard *)springboard shouldAllowDeletingItem:(PTSSpringBoardItem *)item {
//    if ([[item itemIdentifier] isEqualToString:@"dl"]) {
//        return NO;
//    } else {
//        return YES;
//    }
    return YES;
}

-(BOOL)springboard:(PTSSpringBoard *)springboard shouldAllowMovingItem:(PTSSpringBoardItem *)item {
    if ([[item itemIdentifier] isEqualToString:@"dl"]) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark -
#pragma mark PTSSpringBoardelegate protocol

-(void)springboard:(PTSSpringBoard *)springboard selectedItem:(PTSSpringBoardItem *)item atIndex:(NSInteger)index {
    if (![springboard isEditing]) {
        if ([[item itemIdentifier] isEqualToString:@"dl"]) {
            [self.view makeToast:@"Veuillez attendre la fin du téléchargement." duration:2 position:@"bottom" image:[UIImage imageNamed:@"cancel_48"]];
        }
        else
        {
            InsideRessourceVC * insideRessourceVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"InsideRessourceVC"];
            insideRessourceVC.currentRessource = _ressourcesArray[index];
            if (IS_IPAD)
                [[self stackController] pushViewController:insideRessourceVC fromViewController:self animated:YES];
          
            else
                [[self navigationController] pushViewController:insideRessourceVC animated:YES];
        }
    }
}

-(void)springboardDidEnterEditingMode:(PTSSpringBoard *)springboard {
    [self.view makeToast:@"Secouez votre iPhone pour sortir du mode édition." duration:3 position:@"bottom" image:[UIImage imageNamed:@"bulb_off_48"]];
}

-(void)springboardDidLeaveEditingMode:(PTSSpringBoard *)springboard{
    //TODO:Enlever la reconnaissance de mouvement lors de l'édition de la collection
//    [self sidePanelController] _removePanGestureToView;
    [UtilsCore updateRessources:_ressourcesArray];
}

//TODO: Supprimer les ressources
-(void)springboard:(PTSSpringBoard *)springboard deletedItem:(PTSSpringBoardItem *)item atIndex:(NSInteger)index {
    if ([[item itemIdentifier] isEqualToString:@"dl"]) {
        [self.view makeToast:@"Téléchargement annulé." duration:2 position:@"bottom" image:[UIImage imageNamed:@"stop_48"]];
        [MKNetworkEngine cancelOperationsContainingURLString:item.labelText];
    }
    [_ressourcesArray removeObjectAtIndex:index];
    if ([_ressourcesArray count] == 0) {
        [springBoard toggleEditingMode];
    }
}

-(void)springboard:(PTSSpringBoard *)springboard movedItem:(PTSSpringBoardItem *)item atIndex:(NSInteger)index toIndex:(NSInteger)newIndex {
    [_ressourcesArray exchangeObjectAtIndex:index withObjectAtIndex:newIndex];
}

#pragma mark -
#pragma mark Handles Accelerometer-Shake

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    DLog(@"");
    if (event.subtype == UIEventSubtypeMotionShake && [springBoard isEditing]) {
        [springBoard toggleEditingMode:NO];
    }
}

-(BOOL)canBecomeFirstResponder{
    return YES;
}

#pragma mark - UIImage GrayScale
- (UIImage *)convertToGrayscale:(UIImage*)originImage {
    CGSize size = [originImage size];
    int width = size.width;
    int height = size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [originImage CGImage]);
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[1] + 0.59 * rgbaPixel[2] + 0.11 * rgbaPixel[3];
            
            // set the pixels to gray
            rgbaPixel[1] = gray;
            rgbaPixel[2] = gray;
            rgbaPixel[3] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return resultUIImage;
}
@end
