//
//  EditerCompteController.m
//  profil
//
//  Created by admin on 29/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "EditerCompteController.h"

@interface EditerCompteController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIButton *bAvatar;
@property (weak, nonatomic) IBOutlet UITextField *lMail;
@property (weak, nonatomic) IBOutlet UITextField *lEtablissement;
@property (weak, nonatomic) IBOutlet UITextField *lAncienMDP;
@property (weak, nonatomic) IBOutlet UITextField *lNouveauMDP;
@property (weak, nonatomic) IBOutlet UITextField *lNouveauMDPBis;
@property (weak, nonatomic) IBOutlet UITextField *activeField;
@property ( nonatomic) CGPoint scrollPoint;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationBar;
@property (nonatomic) CGFloat kbHeight;
@property (nonatomic) BOOL boolAvatarEstModifie;


@end

@implementation EditerCompteController

@synthesize activeField;
@synthesize scroll;
@synthesize scrollPoint;
@synthesize kbHeight;
@synthesize boolAvatarEstModifie;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Edition d'un compte"];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWasShown:)
     name:UIKeyboardWillShowNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillBeHidden:)
     name:UIKeyboardWillHideNotification
     object:nil];
    _lMail.text = _email ;
    _lEtablissement.text = _etablissement;
    [_bAvatar setBackgroundImage:_avatar forState:UIControlStateNormal];
    boolAvatarEstModifie = NO;
    
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

/*- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_scroll scrollRectToVisible:textField.frame animated:YES];
    return YES;
}*/

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [self ajusteScroll];
     DLog(@"active");
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    DLog(@"keyboard");
    [scroll setContentOffset:scrollPoint animated:YES];
}
- (void)keyboardWasShown:(NSNotification*)aNotification {


    
    NSDictionary* info = [aNotification userInfo];
    

    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        kbHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.width;
    }
    else {
        kbHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    }
    
    scrollPoint = scroll.contentOffset;
    [self ajusteScroll];
    
}

-(void)ajusteScroll{
    CGRect aRect = scroll.frame;
    aRect.size.height -= kbHeight;
    CGPoint origin = activeField.frame.origin;
    if (!CGRectContainsPoint(aRect, origin) ) {
        CGPoint scrollPoint2 = CGPointMake(0.0, activeField.frame.origin.y-(aRect.size.height)+activeField.frame.size.height);
        [scroll setContentOffset:scrollPoint2 animated:YES];
    }
   
}









- (IBAction)modifierAvatar:(id)sender {
    [self showImagePicker];
}

- (void)showImagePicker
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Get the selected image.
    UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
    CGSize newSize = CGSizeMake(100.0f, 100.0f);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* image_reduite = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage * imageModifiee = [image_reduite thumbnailImage:88 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
    
    
    
    [_bAvatar setBackgroundImage:imageModifiee forState:UIControlStateNormal];
    NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    DLog(@"selectedImage: %@", assetURL);
    boolAvatarEstModifie = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    DLog(@"%f",[imageModifiee size].height);
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}




- (IBAction)enregistrer:(id)sender {
    if ( boolAvatarEstModifie ){
        [self uploadImage];
    }else{
        [self enregisterInfo];
    }
}


-(void)uploadImage{
    
    NSData * imageData = UIImagePNGRepresentation([_bAvatar backgroundImageForState:UIControlStateNormal]);
    
    [self post:[NSString stringWithFormat:@"%@/compte/editerAvatar.php",sc_server]
          data:imageData callback:
     ^(NSDictionary *wsReturn) {
         if ( ![self displayErrors:wsReturn]){
             [self enregisterInfo];}
     } errorMessage:@"Problème d'upload de l'image" activityMessage:@"Enregistrement de l'avatar"];
    
}

-(void)enregisterInfo{
    //Appel du webservice
    
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: _lMail.text , @"email",_lEtablissement.text,@"etablissement",_lAncienMDP.text,@"current_password",_lNouveauMDP.text,@"password",_lNouveauMDPBis.text,@"password2",nil];
    [self post:[NSString stringWithFormat:@"%@/compte/editerCompte.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ( ![self displayErrors:wsReturn] )
          {
              [self.navigationController popViewControllerAnimated:YES];
          }
          if (IS_IPAD) {
              [self.stackController popViewControllerAnimated:YES];
          }
      }];
}

@end
