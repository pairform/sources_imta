//
//  RessourceStoreVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RessourceStoreVC : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic,strong) NSMutableArray * data;
@property (nonatomic,strong) NSMutableArray * etablissementData;
@property (nonatomic,strong) NSMutableArray * sectionEtablissementData;
@property (nonatomic,strong) NSMutableArray * themeData;
@property (nonatomic,strong) NSMutableArray * sectionThemeData;
@property (nonatomic,strong) IBOutlet UICollectionView * collectionView;
@property (nonatomic,strong) IBOutlet UISegmentedControl * displaySelector;

-(IBAction)segmentedControlChanged:(id)sender;
@end
