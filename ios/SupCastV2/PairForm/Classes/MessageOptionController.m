//
//  MessageOptionController.m
//  SupCast
//
//  Created by admin on 04/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MessageOptionController.h"

@interface MessageOptionController ()



@property (strong, nonatomic) GCTagList * visibilite;
@property (strong, nonatomic) GCTagList * tag;
@property (weak, nonatomic) IBOutlet UIScrollView *visibiliteView;
@property (weak, nonatomic) IBOutlet UIScrollView *tagView;
@property (weak, nonatomic) IBOutlet UISwitch *switchDefi;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *bTags;
@property (weak, nonatomic) IBOutlet UIButton *bVisibilite;

@end

@implementation MessageOptionController

@synthesize tag;
@synthesize tagView;
@synthesize tagArray;
@synthesize visibilite;
@synthesize visibiliteView;
@synthesize visibiliteArray;
@synthesize visibiliteIdArray;
@synthesize switchDefi;
@synthesize boolDefi;
@synthesize scrollView;
@synthesize id_ressource;
@synthesize bTags;
@synthesize bVisibilite;
@synthesize idMessageOriginal;

#define visibiliteReponse @"Visibilité du message initial"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Options";
    
    int rank = [[[[self getSessionValueForKey:@"rank"] objectForKey:[id_ressource stringValue]] objectForKey:@"id_categorie"] intValue];
    // Si en dessous de Collaborateur
    if ( rank < 2 ){
        [bTags setEnabled:NO];
    }
    // Si en dessous d'Expert
    if ( rank < 4 ){
        [switchDefi setEnabled:NO];
    }
    // Si c'est une réponse
    if ( idMessageOriginal != nil ){
        [bVisibilite setEnabled:NO];
        visibiliteArray = [[NSMutableArray alloc]initWithObjects:visibiliteReponse, nil];
    }
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    if (boolDefi == YES ) [switchDefi setOn:YES];
    
    if (!tag){
        tag = [[GCTagList alloc] initWithFrame:CGRectMake(0, 0, tagView.frame.size.width, 100)];
        //taglist.firstRowLeftMargin = 80.f;
        tag.dataSource = self;
        tag.delegate = self;
        [self.tagView addSubview:tag];
        
    }
    [tag reloadData];
    if (!visibilite) {
        visibilite = [[GCTagList alloc] initWithFrame:CGRectMake(0, 0,  visibiliteView.frame.size.width, 100)];
        //taglist.firstRowLeftMargin = 80.f;
        visibilite.dataSource = self;
        visibilite.delegate = self;
        [self.visibiliteView addSubview:visibilite];
    }
    [visibilite reloadData];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillDisappear:(BOOL)animated {
    [_delegate recupereOptions: visibiliteArray visibiliteId:visibiliteIdArray tag:tagArray defi:switchDefi.isOn];
    [super viewWillDisappear:animated];
}

- (NSInteger)numberOfTagLabelInTagList:(GCTagList *)tagList {
    if (tagList == tag) return tagArray.count;
    else return visibiliteArray.count;
}

- (GCTagLabel*)tagList:(GCTagList *)tagList tagLabelAtIndex:(NSInteger)index {
    
    static NSString* identifier = @"TagLabelIdentifier";
    
    GCTagLabel* gctag = [tagList dequeueReusableTagLabelWithIdentifier:identifier];
    if(!gctag) {
        gctag = [GCTagLabel tagLabelWithReuseIdentifier:identifier];
        //gctag.labelBackgroundColor = [UIColor colorWithRed:84/255.f green:164/255.f blue:222/255.f alpha:1.f];
    }
    if ( tagList == tag){
        [gctag setLabelText: tagArray[index] accessoryType:GCTagLabelAccessoryCrossSign];
        
    }else{
        if ( [visibiliteArray[index] isEqualToString:@"Public" ] || [visibiliteArray[index] isEqualToString:visibiliteReponse ] ){
            [gctag setLabelText:visibiliteArray[index] accessoryType:GCTagLabelAccessoryNone];
        }
        else{
            [gctag setLabelText:visibiliteArray[index] accessoryType:GCTagLabelAccessoryCrossSign];
        }
    }
    return gctag;
}
//Removing
- (void)tagList:(GCTagList *)tagList accessoryButtonTappedAtIndex:(NSInteger)index {
    if (tagList == tag) {
        [tagArray removeObjectsInRange:NSMakeRange(index, 1)];
        [tag deleteTagLabelWithRange:NSMakeRange(index, 1) withAnimation:YES];
        
    }else{
        [visibiliteArray removeObjectsInRange:NSMakeRange(index, 1)];
        [visibiliteIdArray removeObjectAtIndex:index];
        [visibilite deleteTagLabelWithRange:NSMakeRange(index, 1) withAnimation:YES];
        
        if ( visibiliteArray.count == 0){
            [visibiliteArray insertObject:@"Public" atIndex:0];
            [visibiliteIdArray insertObject:@1 atIndex:0];
            [visibilite insertTagLabelWithRagne:NSMakeRange(0, 1) withAnimation:YES];
        }
        
    }
    
    

}
- (void)tagList:(GCTagList *)taglist didChangedHeight:(CGFloat)newHeight {
    DLog(@"%s:%.1f", __func__, newHeight);
    if (taglist == tag) {
        [tagView setContentSize:CGSizeMake( tagView.frame.size.width, newHeight)];
        if([tagView contentSize].height > tagView.frame.size.height)
        {
            CGPoint bottomOffset = CGPointMake(0, [tagView contentSize].height - tagView.frame.size.height);
            [tagView setContentOffset:bottomOffset animated:YES];
        }
        
    }else{
        [visibiliteView setContentSize:CGSizeMake(  visibiliteView.frame.size.width, newHeight)];
        if([visibiliteView contentSize].height > visibiliteView.frame.size.height)
        {
            CGPoint bottomOffset = CGPointMake(0, [visibiliteView contentSize].height - visibiliteView.frame.size.height);
            [visibiliteView setContentOffset:bottomOffset animated:YES];
        }
    }
}


- (void)tagList:(GCTagList *)taglist didSelectedLabelAtIndex:(NSInteger)index {
    DLog(@"selectIndex:%d", index);
}

- (GCTagLabelAccessoryType)accessoryTypeForGroupTagLabel {
    return GCTagLabelAccessoryCrossSign;
}
/*- (NSInteger)maxNumberOfRowAtTagList:(GCTagList*)tagList{
    return 2;
}*/

/*- (NSString*)tagList:(GCTagList *)tagList labelTextForGroupTagLabel:(NSInteger)interruptIndex {
 return [NSString stringWithFormat:@"chingchng", tagArray.count - interruptIndex];
 }*/

- (IBAction)ajouterTag:(id)sender {

    UITextField * tagField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    tagField.placeholder = @"Tag";
    
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Ajouter un tag" andTextFields:@[tagField]];
    [alertView addButtonWithTitle:@"Annuler"
                             type:SIAlertViewButtonTypeCancel
                          handler:nil];
    [alertView addButtonWithTitle:@"Valider"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              if ( ![tagArray containsObject:tagField.text]){
                                  [tagArray insertObject:tagField.text atIndex:tagArray.count];
                                  [tag insertTagLabelWithRagne:NSMakeRange(tagArray.count-1, 1) withAnimation:YES];
                                  
                                  if([tagView contentSize].height > tagView.frame.size.height)
                                  {
                                      CGPoint bottomOffset = CGPointMake(0, [tagView contentSize].height - tagView.frame.size.height);
                                      [tagView setContentOffset:bottomOffset animated:YES];
                                  }
                              }
                              
                          }];

    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    
    [alertView show];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 15) ? NO : YES;
}



- (IBAction)ajouterCercleDansVisibilite:(id)sender {
    [self performSegueWithIdentifier: @"cercleSegue" sender: self];

}

- (void)actionAPartirDeCercle:(NSNumber*) idCercle nomCercle:(NSString *) nomCercle{

    if ( ![visibiliteArray containsObject:nomCercle]){
        //suppression de "Public"
        if ([[visibiliteArray objectAtIndex:0] isEqualToString:@"Public"]){
            [visibiliteIdArray removeObjectAtIndex:0];
            [visibiliteArray removeObjectAtIndex:0];
            [visibilite deleteTagLabelWithRange:NSMakeRange(0, 1) withAnimation:YES];
        }
        [visibiliteArray insertObject:nomCercle atIndex:visibiliteArray.count];
        [visibiliteIdArray insertObject:idCercle atIndex:visibiliteIdArray.count];
        [visibilite insertTagLabelWithRagne:NSMakeRange(visibiliteArray.count-1, 1) withAnimation:YES];
        
        if([visibiliteView contentSize].height > visibiliteView.frame.size.height)
        {
            CGPoint bottomOffset = CGPointMake(0, [visibiliteView contentSize].height - visibiliteView.frame.size.height);
            [visibiliteView setContentOffset:bottomOffset animated:YES];
        }
    }

    
}


// Override to allow orientations other than the default portrait orientation.
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [tag setFrame:CGRectMake(0, 0, tagView.frame.size.width, 100)];
    [visibilite setFrame:CGRectMake(0, 0,  visibiliteView.frame.size.width, 100)];
    //rowMaxWidth TODO
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    // Ajout d'un cercle dans visibilite
    if([[segue identifier] isEqualToString:@"cercleSegue"]   )
    {
        CercleController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        // Temporaire
        viewC.modeCercle = ModeCercleSelectionVisibilite;
        
    }
    
}

-(IBAction)validerOptions:(id)sender
{
    int currentIndex = [[self stackController] indexOfViewController:self];
    UIViewController * previousViewController = [[[self stackController] viewControllers] objectAtIndex:(currentIndex -1)];
    
    [[self stackController] popToViewController:previousViewController animated:YES];
}


@end
