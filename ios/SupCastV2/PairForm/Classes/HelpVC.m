//
//  HelpVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 17/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "HelpVC.h"

@interface HelpVC ()

@end

@implementation HelpVC

@synthesize pageController;

-(id)initWithIndex:(int)index{
    
    if([self init])
    {
        _currentIndex = index;
    }
    return self;
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    if (IS_IPHONE)
        return UIInterfaceOrientationPortrait;
    else
        return UIInterfaceOrientationLandscapeLeft;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    self.stackWidth = 320;
    self.view.width = self.stackWidth;
    CGRect frame = self.view.bounds;
    frame.origin.y += 70;
    frame.size.height -= 70;
    
    [[self.pageController view] setFrame:frame];

//    [[self.pageController view] setFrame:];
    
//    UIViewController *initialViewController = [self viewControllerAtIndex:0];

    
//    UIViewController * helpScreensHolder = [[UIViewController alloc] initWithNibName:@"Help" bundle:nil];
    UIViewController * helpScreensHolder;
    
    if([_screenStoryboardID isEqualToString:@""] || (_screenStoryboardID == nil))
    {
        helpScreensHolder = [[UIStoryboard storyboardWithName:@"HelpStoryboard" bundle:nil] instantiateInitialViewController];
    }
    else
    {
        helpScreensHolder = [[UIStoryboard storyboardWithName:@"HelpStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:_screenStoryboardID];
        
    }
    
    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:[[[helpScreensHolder view] subviews] count]];
                                  
    for (UIView * helpView in [[helpScreensHolder view] subviews]) {
        helpView.width = self.stackWidth;
        [tempArray addObject:[self viewControllerFromUIView:helpView]];
    }
    
    _dataSource = [tempArray copy];
     
    [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:_currentIndex]]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    self.pageController.doubleSided = NO;
    
    [self.view setClipsToBounds:YES];
//    [self.pageControl setNumberOfPages: [_dataSource count]];
//    [self.pageControl setCurrentPage:_currentIndex];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIViewController*)viewControllerFromUIView:(UIView*)view{
    
    UIViewController * newVC = [[UIViewController alloc]  init];
    CGRect frame = view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    
    view.frame = frame;
    [newVC setView:view];
    
    return newVC;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {

    // Return the data view controller for the given index.
    if (([_dataSource count] == 0) || (index >= [_dataSource count])) {
        return nil;
    }
    return [_dataSource objectAtIndex:index];
    
}

- (NSUInteger)indexOfViewController:(UIViewController *)viewController
{
    // Return the index of the given data view controller.
    // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [_dataSource indexOfObject:viewController];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
//    
//    
//    if (_currentIndex == 0) {
//        return nil;
//    }
//    
//    // Decrease the index by 1 to return
//    _currentIndex--;
//    
//    return [self viewControllerAtIndex:_currentIndex];
//
    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
//    
//    _currentIndex++;
//    
//    if (_currentIndex == [_dataSource count]) {
//        return nil;
//    }
//    
//    return [self viewControllerAtIndex:_currentIndex];

    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [_dataSource count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return [_dataSource count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator. 
    return _currentIndex;
}

@end
