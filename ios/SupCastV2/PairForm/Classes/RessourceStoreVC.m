//
//  RessourceStoreVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "RessourceStoreVC.h"
#import "TelechargementVC.h"
#import "Ressource.h"
#import "UtilsCore.h"
#import "GAI.h"

@interface RessourceStoreVC ()

@end

@implementation RessourceStoreVC

@synthesize data;
@synthesize etablissementData;
@synthesize themeData;
@synthesize sectionEtablissementData;
@synthesize sectionThemeData;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Store";
    
    [self getRessources];
    
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 640];
    }
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Ressource Store"];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
}

-(void)getRessources{
    
    //Appel du webservice
    data = [[NSMutableArray alloc] init];
    etablissementData = [[NSMutableArray alloc] init];
    sectionEtablissementData = [[NSMutableArray alloc] init];
    themeData = [[NSMutableArray alloc] init];
    sectionThemeData = [[NSMutableArray alloc] init];
    
    [self post:[NSString stringWithFormat:@"%@/ressources/listerRessources.php",sc_server]
      callback: ^(NSDictionary *wsReturn)
      {
          data = [wsReturn objectForKey:@"ressources"];
          
          for (NSDictionary * ressourceDictionary in self.data) {
              
              Ressource * ressource = [UtilsCore convertDictionaryToRessource:ressourceDictionary temporary:YES];
              
              NSString * theme = ressource.theme;
              NSString * etablissement = ressource.etablissement;
              
              if (![sectionThemeData containsObject:theme]) {
                  [sectionThemeData addObject:theme];
                  [themeData addObject:[NSMutableArray arrayWithObject:ressource]];
              }
              else{
                  int index = [self.sectionThemeData indexOfObject:theme];
                  [[themeData objectAtIndex:index] addObject:ressource];
              }
              
              if (![sectionEtablissementData containsObject:etablissement]) {
                  [sectionEtablissementData addObject:etablissement];
                  [etablissementData addObject:[NSMutableArray arrayWithObject:ressource]];
              }
              else{
                  int index = [self.sectionEtablissementData indexOfObject:etablissement];
                  [[etablissementData objectAtIndex:index] addObject:ressource];
              }
          }
          [self.collectionView reloadData];
          	
      }
  errorMessage:@"Vous devez être connecté à Internet pour accéder aux ressources en ligne!"
     activityMessage:@""];

}

-(IBAction)segmentedControlChanged:(id)sender{
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView delegates 

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    // _data is a class member variable that contains one array per section.
    if([self.displaySelector selectedSegmentIndex] == 0)
        return [sectionThemeData count];
    else
        return [sectionEtablissementData count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if([self.displaySelector selectedSegmentIndex] == 0)
        return [[themeData objectAtIndex:section] count];
    else
        return [[etablissementData objectAtIndex:section] count];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if(kind == UICollectionElementKindSectionHeader)
    {
        
        UICollectionReusableView * header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeader" forIndexPath:indexPath];
        
        
        //modify your header
        UILabel * title = (UILabel*)[header viewWithTag:1];
        if([self.displaySelector selectedSegmentIndex] == 0)
            [title setText: [sectionThemeData objectAtIndex:indexPath.section]];
        else
            [title setText: [sectionEtablissementData objectAtIndex:indexPath.section]];
        
        return header;
    }
    return nil;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    Ressource * currentRessource;
    
    if([self.displaySelector selectedSegmentIndex] == 0)
        currentRessource = [[themeData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    else
        currentRessource = [[etablissementData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

    UICollectionViewCell * cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"cellRes" forIndexPath:indexPath];
    
    UIImageView * cellImage = (UIImageView *)[cell viewWithTag:1];

    [cellImage setImage: [currentRessource getIcone]];
    
    UILabel * cellTitle = (UILabel *)[cell viewWithTag:2];
    [cellTitle setText: currentRessource.nom_court];
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"storeCellSegue"])
    {
        int row = [[[self.collectionView indexPathsForSelectedItems] objectAtIndex:0] row];
        int section = [[[self.collectionView indexPathsForSelectedItems] objectAtIndex:0] section];
        
        TelechargementVC * telechargementVC = segue.destinationViewController;
        
        if([self.displaySelector selectedSegmentIndex] == 0)
            [telechargementVC setCurrentRessource:[[themeData objectAtIndex:section] objectAtIndex:row]];
        else
            [telechargementVC setCurrentRessource:[[etablissementData objectAtIndex:section] objectAtIndex:row]];

    }
}
@end
