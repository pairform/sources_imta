//
//  InsideRessourceVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "InsideRessourceVC.h"
#import "PageVC.h"
#import "MessageController.h"
#import "UtilsCore.h"
#import "TelechargementVC.h"
#import "TDBadgedCell.h"
#import "UACellBackgroundView.h"
#import "GAI.h"

@interface InsideRessourceVC ()
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property (strong,nonatomic) UIButton *uiButtonMessageTopBar;
@property (strong,nonatomic) RXMLElement * rootXML;
-(void)updateMenu;

@end

@implementation InsideRessourceVC

@synthesize rootXML = _rootXML;
@synthesize uiNavigationItem, infoButton;
@synthesize rootArray = _rootArray;
@synthesize arrayForTable = _arrayForTable;
@synthesize subMenuStack =_subMenuStack;
@synthesize currentRessource = _currentRessource;
@synthesize uiButtonMessageTopBar;
@synthesize lastPageUrlReaded = _lastPageUrlReaded;

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Entrée dans une ressource"];
    
    self.title = _currentRessource.nom_court;
    [self.tableView setAllowsMultipleSelection:YES];
    
    [self.tableView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.tableView.layer setShadowOffset:CGSizeMake(0,8)];
    [self.tableView.layer setShadowRadius:5.0];
    [self.tableView.layer setShadowOpacity:0.3];
    self.tableView.clipsToBounds = NO;
    self.tableView.layer.masksToBounds = NO;
    
    [self setUIImageEtablissement];
    
    
    NSString * pathToOutline = [NSString stringWithFormat:@"%@/co/outline.xml", [_currentRessource getPath]];
    
    int update = [[self getPreference:@"updateFrequency" ] intValue];
    if ( update == kMessageUpdateEveryMinute || update == kMessageUpdateWhenEnterRes){
        [UtilsCore rafraichirMessagesFromRessource:_currentRessource.id_ressource];
    }
        
	// Do any additional setup after loading the view.
    _rootXML = [RXMLElement elementFromXMLFullPathToFile:pathToOutline];
    
    [self setLastPageUrlReaded:@""];
    
    if ([_rootXML isValid]) {
        //[UtilsCore getMessagesFromRessource:_currentRessource.id_ressource];
        [self setRootArray:[_rootXML allDirectChildren]];
        [self setSubMenuStack:[[NSMutableArray alloc] initWithObjects:_rootArray, nil]];
        
        [self checkForUpdate];
    }
    else
    {
        if (IS_IPAD) {
            [self.stackController popViewController:self animated:YES];
            [[self.stackController view] makeToast:@"La ressource est corrompue. Essayez de mettre à jour la ressource ultérieurement." duration:3 position:@"bottom" image:[UIImage imageNamed:@"delete_64"]];
        }
        else
        {
            [[self.navigationController view] makeToast:@"La ressource est corrompue. Essayez de mettre à jour la ressource ultérieurement." duration:3 position:@"bottom" image:[UIImage imageNamed:@"delete_64"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
    if (IS_IPAD)
    {
        [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
        [self setStackWidth: 320];
    }
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:_currentRessource.nom_court];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

-(void)setUIImageEtablissement{
    DLog(@"");
    
    UIImage * iconeModifiee = [[_currentRessource getIconeEtablissement] resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(236, 108) interpolationQuality:kCGInterpolationHigh];
    
    //Icone de l'établissement
    [self.icone_etablissement setImage: [iconeModifiee roundedCornerImage:4 borderSize:1]];
}
-(void)viewDidAppear:(BOOL)animated
{
    DLog(@"");
    [super viewDidAppear:animated];
    
    
    // navigation button bar
    NSMutableArray     *items = [uiNavigationItem.rightBarButtonItems mutableCopy] ;
    
    if (!items) {
        items = [[NSMutableArray alloc] init];
    }
    
    //0 si iPad, 1 si Iphone
    int count = IS_IPHONE;
    // En cas de retour , n'ajoute pas de bouton en plus.
    if ( items.count == count){
        uiButtonMessageTopBar = [UIButton buttonWithType:UIButtonTypeCustom];
        [uiButtonMessageTopBar.titleLabel setFont:[UIFont fontWithName:@"HelveticaBold" size:12]];
        [uiButtonMessageTopBar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_unreaded_64"] forState:UIControlStateNormal];
        
        
        [uiButtonMessageTopBar setFrame:CGRectMake(5,6,32,32)];
        [uiButtonMessageTopBar addTarget:self action:@selector(voirMessages) forControlEvents:UIControlEventTouchUpInside];
        [uiButtonMessageTopBar setShowsTouchWhenHighlighted:true];
        [uiButtonMessageTopBar setReversesTitleShadowWhenHighlighted:true];
        
        
        UIBarButtonItem *bEdit = [[UIBarButtonItem alloc] initWithCustomView:uiButtonMessageTopBar];
        [items addObject:bEdit];
       
        uiNavigationItem.rightBarButtonItems = items;
        
    }
//    [self.tableView reloadData];
    //[UtilsCore rafraichirMessagesFromRessource:_currentRessource.id_ressource];
    NSNumber * nombreDeMessage = [UtilsCore getNombreMessageNonLuFromRessource:_currentRessource.id_ressource];
    
    [uiButtonMessageTopBar.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    if ( [nombreDeMessage integerValue ] > 0){
        [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_unreaded_64"] forState:UIControlStateNormal];
        [uiButtonMessageTopBar setTitle:[nombreDeMessage stringValue] forState:UIControlStateNormal];
        uiButtonMessageTopBar.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
    }else{
        [uiButtonMessageTopBar setTitle:@"" forState:UIControlStateNormal];
    }
    
    if ( [nombreDeMessage isEqualToNumber:@0]){
        [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_readed_64"] forState:UIControlStateNormal];
    }
    if ( [nombreDeMessage isEqualToNumber:@-1]){
        [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_64"] forState:UIControlStateNormal];
    }
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    [self.tableView reloadData];
    [self updateMenu];
    
    if (IS_IPAD) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(notifiedByPageVC:)
                                                     name:@"pageDidChange"
                                                   object:nil];
    }
}

-(void) notifiedByPageVC:(NSNotification *)notification
{
    self.lastPageUrlReaded = [NSString stringWithFormat:@"%@.html", notification.userInfo[@"url"]];
    [self updateMenu];
}

-(void) refreshMessages{
    [self updateMenu];
}

-(void)viewWillAppear:(BOOL)animated{
    DLog(@"");
    //Boutons de gauche
    NSMutableArray * leftItems = [self.navigationItem.leftBarButtonItems mutableCopy];
    
    // En cas de retour , n'ajoute pas de bouton en plus.
    if ( leftItems.count == 1)
    {
        UIBarButtonItem * infoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.infoButton];
        [leftItems addObject:infoBarButtonItem];
        
        self.navigationItem.leftBarButtonItems = leftItems;
    }
    

}

-(void)viewWillDisappear:(BOOL)animated
{
    
    DLog(@"");
    
    [super viewWillDisappear:animated];
    
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"pageDidChange"
                                                  object:nil];

}

-(void)updateMenu{ 
    
    if(![self.lastPageUrlReaded isEqualToString:@""])
    {
//        [self.view makeToastActivity];
        
        //On chope la dernière page lue
        NSString * xPath = [NSString stringWithFormat:@"//l[@u=\"%@\"]", self.lastPageUrlReaded];
        
        RXMLElement * elem = [[_rootXML childrenWithRootXPath:xPath] objectAtIndex:0];
        
        //On check sa position dans le menu en comparant les urls
        for (RXMLElement * elemInMenu in _subMenuStack[[_subMenuStack count] -1]) {
            if ([[elemInMenu attribute:@"u"] isEqualToString:[elem attribute:@"u"]]) {
                int ind = [_subMenuStack[[_subMenuStack count] -1] indexOfObject:elemInMenu];
                //On sélectionne la ligne (UI uniquement)
                [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:ind inSection:0] animated:YES scrollPosition:UITableViewScrollPositionBottom];
                return;
            }
        }
                
        //Reset du tableView
        [self setSubMenuStack:[[NSMutableArray alloc] initWithObjects:_rootArray, nil]];
        [self.tableView reloadData];
                
        //Pour chaque parent de cette page (en commencant par celui de plus haut niveau)
        for (int i = elem.parents.count -1; i >= 0 ; i--) {
            int index = -1;
            //On check sa position dans le menu en comparant les urls
            for (RXMLElement * elemInMenu in _subMenuStack[[_subMenuStack count] -1]) {
                if ([[elemInMenu attribute:@"u"] isEqualToString:[elem.parents[i] attribute:@"u"]]) {
                    index = [_subMenuStack[[_subMenuStack count] -1] indexOfObject:elemInMenu];
                }
            }
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            
            //On sélectionne la ligne (UI uniquement)
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            //On sélectionne le sous menu (ça update _subMenuStack au passage)
            [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        }
        for (RXMLElement * elemInMenu in _subMenuStack[[_subMenuStack count] -1]) {
            if ([[elemInMenu attribute:@"u"] isEqualToString:[elem attribute:@"u"]]) {
                int index = [_subMenuStack[[_subMenuStack count] -1] indexOfObject:elemInMenu];
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionBottom];
            }
        }
        //        [self.view hideToastActivity];

    }
}
#pragma mark - Verification de mise à jour
-(void)checkForUpdate{
    DLog(@"");
    [self post:[NSString stringWithFormat:@"%@/ressources/majRessource",sc_server] param:@{@"id_ressource": _currentRessource.id_ressource} callback:^(NSDictionary *wsReturn) {
        
        NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
        [dFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * distantDate = [dFormatter dateFromString:wsReturn[@"date_update"]];
        NSComparisonResult comparaison = [_currentRessource.date_update compare:distantDate];
        
        if (comparaison == NSOrderedAscending)
        {
            [UIView animateWithDuration:0.3 animations:^{
                self.infoButton.imageView.alpha = 0.0;
            } completion:^(BOOL finished) {
                [self.infoButton.imageView setImage:[UIImage imageNamed:@"update"]];
                
                [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat animations:^{
                    self.infoButton.imageView.alpha = 1.0;
                } completion:nil];
            }];
        }
    }];
}

#pragma mark - TableView Protocols & Delegates
// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    DLog(@"");

	return [_subMenuStack count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DLog(@"");
    NSArray * currentSectionArray = [_subMenuStack objectAtIndex:[_subMenuStack count] - (section +1)];
    return [currentSectionArray count];
}
//
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    //Si on a plus d'une section
//    if (([_subMenuStack count] > 1) && (indexPath.section != 0)) {
//        cell.backgroundColor = [UIColor colorWithRed:0.733 green:0.733 blue:0.733 alpha:1]; /*#bbbbbb*/
//    }
//}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DLog(@"");
    
    RXMLElement * currentXMLElement = [[_subMenuStack objectAtIndex:[_subMenuStack count] - (indexPath.section +1)] objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier;
    
    if([currentXMLElement.tag isEqualToString:@"b"])
        CellIdentifier = @"resMenuCell";
    else
        CellIdentifier = @"resLinkCell";
    
    TDBadgedCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TDBadgedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.backgroundView = [[UACellBackgroundView alloc] initWithFrame:CGRectZero];
    
    [(UACellBackgroundView*) cell.backgroundView setColorsFrom:[UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1]
                                                            to:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:1]];
    
    if (indexPath.row == 0)
        [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionTop];
    else if (indexPath.row == ([[_subMenuStack objectAtIndex:[_subMenuStack count] - (indexPath.section +1)] count] -1))
        [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionBottom];
    else
        [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionMiddle];
    
	UILabel * cellTitle = (UILabel*)[cell viewWithTag:1];
    [cellTitle setText:[currentXMLElement attribute:@"t"]];
//    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
//    {
//        NSLayoutConstraint * constraint = [[cell constraints] objectAtIndex:2];
//        
//        [constraint setConstant:(32 + 15)];
////        
////        CGRect frame = cellTitle.frame;
////        frame.origin.x = 10;
////        frame.size.width -= 10;
////        cellTitle.frame = frame;
//    }
    
    // Nombre de message
    
    // Recuperation du nom de la page.
    
    //Recuperation des enfant d'un noeud s'il y en a.
    NSArray * children = [currentXMLElement childrenWithRecursion:@""];
    int nombreMessage = 0;
    
    
    // Il y a des enfants
    if ( [children count] != 0){
        for (RXMLElement * elem in children) {
            NSString * urlPage = [[elem attribute:@"u"] stringByReplacingOccurrencesOfString:@".html" withString:@""];
            if ( urlPage == nil ) continue;
            int nombreMessageDePage = [[UtilsCore getNombreMessageNonLuFromRessourcePageTotale:_currentRessource.id_ressource nom_page:urlPage]integerValue];
            if ( nombreMessageDePage > 0)     nombreMessage += nombreMessageDePage;
        }
        
        
    }else{
        
    // Il n'y a pas d'enfant
        NSString * urlPage = [[currentXMLElement attribute:@"u"] stringByReplacingOccurrencesOfString:@".html" withString:@""];
        urlPage = [urlPage stringByReplacingOccurrencesOfString:@".html" withString:@""];
        nombreMessage = [[UtilsCore getNombreMessageNonLuFromRessourcePageTotale:_currentRessource.id_ressource nom_page:urlPage]integerValue];
    }


    if ( nombreMessage > 0)   cell.badgeString = [NSString stringWithFormat:@"%i",nombreMessage];
    else cell.badgeString = nil;
   
    cell.badgeColor = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1.000];
    cell.badge.fontSize = 16;
    cell.badge.radius = 11;





    return cell;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DLog(@"");

    if([indexPath section] != 0)
    {
        
        //On créé un NSRange intermédiaire qui va servir a supprimer les menu stackés
        NSRange indexRange2 = NSRangeFromString([NSString stringWithFormat:@"%d,%d",[_subMenuStack count] - indexPath.section, indexPath.section]);
        NSIndexSet * sectionsToDelete2 = [NSIndexSet indexSetWithIndexesInRange:indexRange2];
        
        [_subMenuStack removeObjectsAtIndexes:sectionsToDelete2];
        
        //On créé un NSRange intermédiaire qui va servir a supprimer la / les section(s)
        NSRange indexRange = NSRangeFromString([NSString stringWithFormat:@"0,%d",indexPath.section]);
        
        
        NSIndexSet * sectionsToDelete = [[NSIndexSet alloc] initWithIndexesInRange:indexRange];
        
        
        
        [tableView deleteSections:sectionsToDelete withRowAnimation:UITableViewRowAnimationBottom];

    }
}

- (NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    DLog(@"");

    for ( NSIndexPath* selectedIndexPath in tableView.indexPathsForSelectedRows ) {
        if ( selectedIndexPath.section == indexPath.section )
            [tableView deselectRowAtIndexPath:selectedIndexPath animated:NO] ;
    }
    return indexPath ;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");

    //Animation de la ligne
//	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RXMLElement * currentXMLElement = [[_subMenuStack objectAtIndex:[_subMenuStack count] - (indexPath.section +1)] objectAtIndex:indexPath.row];
    
    if([currentXMLElement.tag isEqualToString:@"l"])
        return;
    
    //Récupération des sous-éléments du menu sélectionné
	NSArray * subMenus = [currentXMLElement allDirectChildren];
    
    [tableView beginUpdates];
    
    //Si l'utilisateur remonte dans la hiérarchie
    if(indexPath.section != 0 ){
        
        //On créé un NSRange intermédiaire qui va servir a supprimer les menu stackés
        NSRange indexRange2 = NSRangeFromString([NSString stringWithFormat:@"%d,%d",[_subMenuStack count] - indexPath.section, indexPath.section]);
        NSIndexSet * sectionsToDelete2 = [NSIndexSet indexSetWithIndexesInRange:indexRange2];
        
        [_subMenuStack removeObjectsAtIndexes:sectionsToDelete2];
        
        //On créé un NSRange intermédiaire qui va servir a supprimer la / les section(s)
        NSRange indexRange = NSRangeFromString([NSString stringWithFormat:@"0,%d",indexPath.section]);
        
        
        NSIndexSet * sectionsToDelete = [[NSIndexSet alloc] initWithIndexesInRange:indexRange];

        
        
        [tableView deleteSections:sectionsToDelete withRowAnimation:UITableViewRowAnimationBottom];

    }
    
    //On ajoute le submenu cliqué dans une stack pour mesurer l'avancée dans les sous-menus
    [_subMenuStack addObject:subMenus];
    
    NSUInteger count = 0;
    
    NSMutableArray *arCells = [NSMutableArray array];
    
    for(RXMLElement * subMenu in subMenus ){
        [arCells addObject:[NSIndexPath indexPathForRow:count++ inSection:0]];
    }
    
    [tableView insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:nil];
    [tableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationBottom];
    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:true];
    
    [tableView setNeedsLayout];

    [tableView endUpdates];
    
}

-(void)voirMessages{
    [self performSegueWithIdentifier: @"messageSegue" sender: self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"messageSegue"]){
        MessageController * messageController = segue.destinationViewController;
        messageController.currentRessource = _currentRessource;
    }
    else if ([[segue identifier] isEqualToString:@"webSegue"]){
        NSIndexPath * currentIndexPath = [self.tableView indexPathForSelectedRow];
        
        int sectionClicked = [currentIndexPath section];
        int indexInStack = [_subMenuStack count] - (sectionClicked +1);
        
        
        
        NSMutableArray * allPages = [[NSMutableArray alloc] init];
        
        [_rootXML iterateWithRootXPath:@"//l" usingBlock:^(RXMLElement * elem) {
            [allPages addObject:[NSString stringWithFormat:@"%@/co/%@",[_currentRessource getPath],[elem attribute:@"u"]]];
        }];
        
        RXMLElement * currentXML = [[_subMenuStack objectAtIndex:indexInStack] objectAtIndex:[currentIndexPath row]];
                int indexOfSelectedItem = [allPages indexOfObject:[NSString stringWithFormat:@"%@/co/%@",[_currentRessource getPath],[currentXML attribute:@"u"]]];
        
        PageVC * pageVC = segue.destinationViewController;
        
        pageVC.pageData = allPages;
        pageVC.currentRessource = _currentRessource;
        pageVC.selectedIndex = indexOfSelectedItem;
        pageVC.insideResVC = self;

    }
    else if([segue.identifier isEqualToString:@"infoSegue"])
    {
        TelechargementVC * telechargementVC = segue.destinationViewController;
        
        [telechargementVC setCurrentRessource:_currentRessource];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
