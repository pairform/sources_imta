//
//  CWDLeftAlignedCollectionViewFlowLayout.h
//  SupCast
//
//  Created by admin on 28/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CWDLeftAlignedCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
