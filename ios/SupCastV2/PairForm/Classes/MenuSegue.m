//
//  MenuSegue.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuSegue.h"
#import "JASidePanelController.h"

@implementation MenuSegue

- (void)perform
{
    //Récupération du navigationController
    UINavigationController * globalNavigationController = (UINavigationController*)[(JASidePanelController *)[[self sourceViewController] parentViewController] centerPanel];
    
    //Pour chaque controller stacké dans la navigation
    for (id Controller in [globalNavigationController viewControllers]) {
        //On le supprime proprement
        [Controller viewWillDisappear:YES];
        [Controller removeFromParentViewController];
    }
    
    //On ne peut pas remplacer le RootViewController du NavigationController une fois celui ci initialisé.
    
    //Feinte :
    //On cache le bouton back du VC à ajouter
    [[self.destinationViewController navigationItem] setHidesBackButton:YES];
    
    //On le push dans la navigation. Du coup, le RootVC est toujours en premier, mais on ne peut pas y accéder (a part par les boutons du menu).
    [globalNavigationController pushViewController:self.destinationViewController animated:true];
    
}
@end
