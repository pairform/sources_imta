//
//  SuccesControllerViewController.h
//  SupCast
//
//  Created by admin on 08/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuccesControllerViewController : UIViewController
@property NSArray * succes;
@property NSArray * succesNonGagnes;

@end
