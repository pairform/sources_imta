//
//  FileManager.m
//  SupCast
//
//  Created by Maen Juganaikloo on 03/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "FileManager.h"

#import "ZipArchive.h"

@implementation FileManager

+(BOOL)unzipFileAtPath:(NSString *) srcPath{
    DLog(@"pathFichierZip1 is %@",srcPath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:srcPath])
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
       ^{
           //Dossier document
        NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory = [documentPath objectAtIndex:0];
        
        
        //Nom du zip
        NSString *bundleZipFile = [[NSString alloc] initWithString:[srcPath lastPathComponent]];
//        NSString * documentZipFile = [documentDirectory stringByAppendingPathComponent:bundleZipFile];
        
//        NSError *error;
        
//        [[NSFileManager defaultManager] moveItemAtPath:srcPath toPath:documentZipFile error:&error];
        
        ZipArchive* zipArch = [[ZipArchive alloc] init];
        if([zipArch UnzipOpenFile:srcPath])
        {
            
            //Chemin de destination
            NSString * dstPath = [documentDirectory stringByAppendingPathComponent:[bundleZipFile stringByDeletingPathExtension]];
            
            //Si on est dans le cas d'une mise à jour, et que la ressource existe déjà dans le dossier Documents
            if([[NSFileManager defaultManager] fileExistsAtPath:dstPath])
                [[NSFileManager defaultManager] removeItemAtPath:dstPath error:nil];
            
            BOOL ret = [zipArch UnzipFileTo:dstPath overWrite:YES];
            if(ret)
            {
//                NSDictionary * preferences = [[NSUserDefaults standardUserDefaults] persistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
                [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"firstLaunchUnzip"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadFinished" object:nil userInfo:nil];
            }
                else{
                DLog(@"Probleme de désarchivage de la ressource %@",dstPath);
            }

            //
//                        u_int8_t b = 1;
//                        setxattr([documentZipFile fileSystemRepresentation], "com.apple.MobileBackup", &b, 1, 0, 0);
//            
            [zipArch UnzipCloseFile];
//            
//            if ([[NSFileManager defaultManager] removeItemAtPath:srcPath error:&error])
//            {
//                DLog(@"fichier zip %@ supprimé",srcPath);
//                return true;
//                
//            }
//            else
//            {
//                DLog(@"fichier zip %@ supprimé erreur %@",srcPath,[error localizedDescription]);
//            }
            
        }

       });
            }
    return false;
}
@end
