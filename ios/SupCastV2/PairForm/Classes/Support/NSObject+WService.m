//
//  NSDictionary+WService.m
//  SupCast
//
//  Created by Maen Juganaikloo on 23/04/13.
//  Copyright (c) 2013 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "NSObject+WService.h"
#import "Reachability.h"
#import "SIAlertView.h"
#import "RootVC.h"

// helper function: get the string form of any object
static NSString *toString(id object) {
    return [NSString stringWithFormat: @"%@", object];
}

// helper function: get the url encoded string form of any object
static NSString *urlEncode(id object) {
    NSString *string = toString(object);
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}



@implementation NSObject (WService)

#pragma mark - Methodes à part


-(NSString*) urlEncodedString:(NSDictionary*) dico {
    NSMutableArray *parts = [NSMutableArray array];
    
    if([dico count] == 0)
        return @"";
    
    for (id key in dico)
    {
        id value = [dico objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@=%@", urlEncode(key), urlEncode(value)];
        [parts addObject: part];
    }
    return [parts componentsJoinedByString: @"&"];
}

-(BOOL)displayErrors:(NSDictionary *)wsReturn{
    if ( [[wsReturn objectForKey:@"status"] isEqualToString:@"ko"]){
        NSString * errorMessage = [wsReturn objectForKey:@"message"];
        if (!errorMessage) errorMessage = @"Problème serveur";
        
        id rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        
        if ([rootController isKindOfClass:NSClassFromString(@"RootVC")]) {
            if (IS_IPHONE) {
                [[[(RootVC*)rootController centerPanel] view] makeToast:errorMessage duration:4.0 position:@"bottom" title:@"Erreur"];
            }
        }else{
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Problème"
                andMessage:errorMessage];
            [alertView addButtonWithTitle:@"Ok"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                  }];
            alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            [alertView show];
        }
        
        return YES;

    }
    return NO;
}

#pragma mark - Posts sans paramètres

-(BOOL)post:(NSString *)url callback:(void (^)(NSDictionary *))callback
{
    return [self post:url mixedParam:nil callback:callback errorMessage:nil activityMessage:@""];
}

-(BOOL)post:(NSString *)url callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [self post:url mixedParam:nil callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Posts avec data

-(BOOL)post:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback
{
    return [self post:url mixedParam:nil callback:callback errorMessage:nil activityMessage:@""];
}

-(BOOL)post:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [self post:url mixedParam:data callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Posts avec paramètres

-(BOOL)post:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback
{
    return [self post:url mixedParam:param callback:callback errorMessage:nil activityMessage:@""];
}

-(BOOL)post:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [self post:url mixedParam:param callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Fonction de post commune

/*
 url: Url du webService
 mixedParam: NSDictionary de paramètres, NSData de binaire, ou nil pour aucun
 callback: fonction de retour
 errorMessage: nil pour aucun, NSString pour prompt avec proposition de réessayer
 activityMessage: NSString pour afficher le chargement au milieu de l'écran avec message, @"" sans message, nil pour ne pas afficher de chargement
 */

-(BOOL)post:(NSString *)url mixedParam:(id)mixedParam callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    DLog(@"");
    
    //*********************************\\
    //Mise en place du toast d'activité\\
    //*********************************\\
    
    //Vue courante (nil s'il n'y a pas d'activityMessage
    UIView * currentView;   
    //Si c'est nil, on ne fait rien
    if (activityMessage) {
        
        //On récupère la view du navigation controller
        
        //Sécurité, au cas ou le rootController ne serait pas RootVC (quand SIAlert est affiché, c'est lui le root. Pourquoi, je sais pas.)
        //TODO A regarder plus tard, on ne peut pas remplacer le rootViewController normalement, comment il fait lui?
    
        id rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        
        if ([rootController isKindOfClass:NSClassFromString(@"RootVC")]) {
            currentView = [[(RootVC*)rootController centerPanel] view];
        }
    
        if ([activityMessage isEqualToString:@""]) {
            [currentView makeToastActivity:@"center"];
        }
        else{
            [currentView makeToastActivity:@"center" andMessage:activityMessage];
        }
    }
    

    //*********************************\\
    //******* Test de connexion *******\\
    //*********************************\\
    
    if([[Reachability reachabilityWithHostname:sc_hostname] currentReachabilityStatus] == NotReachable)
    {
        
        //*********************************\\
        //***** Alerte personnalisée ******\\
        //*********************************\\
        
        //Si c'est nil, on n'affiche rien
        if (errorMessage) {
            
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Erreur de connexion." andMessage:errorMessage];
            [alertView addButtonWithTitle:@"Annuler"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alertView) {
                                  }];
            [alertView addButtonWithTitle:@"Réessayer"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      
                                      [alertView dismissAnimated:NO];
                                      [self post:url mixedParam:mixedParam callback:callback errorMessage:errorMessage activityMessage:activityMessage];
                                      
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            
            [alertView show];
        }
        [currentView hideToastActivity];
        return false;
    }
    
    //*********************************\\
    //*** Construction de la requête **\\
    //*********************************\\
    
    NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Designate the request a POST request and specify its body data
    [postRequest setHTTPMethod:@"POST"];
    
    //Si il y a des paramètres à envoyer
    if (mixedParam) {
        //Si c'est un dico de paramètres
        if ([mixedParam isKindOfClass:NSClassFromString(@"NSDictionary")]) {
            
            NSString * queryString = [self urlEncodedString:mixedParam];
            
            NSString *bodyData = [NSString stringWithFormat:@"%@&os=%@&version=%@", queryString, sc_os, sc_version];
            
            //On met le content type application/x-www-form-urlencoded
            [postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            
            [postRequest setHTTPBody:[NSData dataWithBytes:[bodyData UTF8String] length:[bodyData length]]];
            
        }
        //Si c'est des datas
        else if ([mixedParam isKindOfClass:NSClassFromString(@"NSData")])
        {
            
            NSString *filename = @"fileNameTest";
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [postRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *postbody = [NSMutableData data];
            
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"avatar\"; filename=\"%@.jpg\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[NSData dataWithData:mixedParam]];
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [postRequest setHTTPBody:postbody];
        }
        //Si c'est aucun des deux, c'est louche.
        else
            return false;
    }
    //S'il n'y a pas de paramètres
    else
    {
        
        NSString *bodyData = [NSString stringWithFormat:@"os=%@&version=%@", sc_os, sc_version];
        
        [postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        [postRequest setHTTPBody:[NSData dataWithBytes:[bodyData UTF8String] length:[bodyData length]]];
        
    }
    

    //*********************************\\
    //****** Envoi de la requête ******\\
    //*********************************\\
    
    if([NSURLConnection respondsToSelector:@selector(sendAsynchronousRequest:queue:completionHandler:)])
    {
        // we can use the iOS 5 path, so issue the asynchronous request
        // and then just do whatever we want to do
        
		//to show:
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [NSURLConnection sendAsynchronousRequest:postRequest
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             //to hide
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             
             NSError *jsonParsingError = nil;
             
             //Si data n'est pas nil
             if(data)
             {
                 NSDictionary *wsReturn = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
                 
                 //DLog(@"Erreur JSON : %@", jsonParsingError);
                 
                 if (wsReturn) {
                     callback(wsReturn);
                 }
             }
             
             
             //*********************************\\
             //*Suppression du toast d'activité*\\
             //*********************************\\
             
             //S'il n'y a pas de message / que currentView est nil (double sécurité), on ne fait rien
             if (activityMessage && currentView)
             {
                 //Sinon, on cache le toast
                 [currentView hideToastActivity];
             }
             
         }];
        
        return true;
    }
    else
        return false;
}

-(BOOL) isReachable{
    if([[Reachability reachabilityWithHostname:sc_hostname] currentReachabilityStatus] == NotReachable)
    {
        
        //*********************************\\
        //***** Alerte personnalisée ******\\
        //*********************************\\
        
        //Si c'est nil, on n'affiche rien
       
            
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Erreur de connexion." andMessage:@"Connexion requise"];
            [alertView addButtonWithTitle:@"Ok"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alertView) {
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            
            [alertView show];
        return NO;
    }
    return YES;
}

@end