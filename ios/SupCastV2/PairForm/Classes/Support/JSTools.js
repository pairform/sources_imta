
var x, y = 0;

function MyAppGetHTMLElementsAtPoint(x,y)
{
    var initialE =document.elementFromPoint(x,y);
    var e = initialE;
    // recherche objet apprentisage
//    while ( !hasClass(e,"opsc_oa") && e.nodeName != "BODY" ){
//        e = e.parentNode;
//    }
    // si aucun objet trouvé
//    if ( !hasClass(e,"resInFlow")){
//        e = initialE;
//        while ( e.nodeName != "P" && e.nodeName != "BODY" ){
//            e = e.parentNode;
//        }
//    }
    //Tant que le noeud courant n'est ni un paragraphe ou n'a pas la classe resInFlow, et qu'on est pas remonté au body
    while (e.nodeName != "P" && !((e.nodeName == "DIV") && hasClass(e,"resInFlow")) && e.nodeName != "BODY" ){
       
        e = e.parentNode;
//        alert('While '+e.nodeName + '.' + e.className + ' hasClass resInFlow : '+hasClass(e,"resInFlow"));
    }
//    alert('After '+e.nodeName + '.' + e.className + ' hasClass resInFlow : '+hasClass(e,"resInFlow"));
    
    unSquaring();
    if (( e.nodeName != "BODY") && (((e.nodeName == "DIV") && hasClass(e,"resInFlow")) ||(e.nodeName == "P"))){
        applySquaringOnElement(e);
        var lis = getAllElements(e.nodeName);
        for (var i = 0, len = lis.length; i < len; i++) {
            if (e === lis[i]) {
                return  e.nodeName + ":" + i;
                break;
            }
        }
    }
    
    return null;
}

function applySquaringOnElement(e)
{
    e.className += ' selected';

}



function unSquaring(){
    var lis = getAllElements("P");
    for (var i = 0, len = lis.length; i < len; i++) {
        if ( hasClass(lis[i],'selected')){
            removeClass(lis[i],'selected');
        }
    }
    var lis = getAllElements("div");
    for (var i = 0, len = lis.length; i < len; i++) {
        if ( hasClass(lis[i],'selected')){
            removeClass(lis[i],'selected');
        }
    }
    
}

function applyUniqueIdToElement()
{

}

Element.prototype.documentOffsetTop = function () {
    return this.offsetTop + ( this.offsetParent ? this.offsetParent.documentOffsetTop() : 0 );
};

function getAllElements(tag)
{
    var elements;
    
    if (tag == 'DIV')
    {
        var mainContent = document.getElementsByClassName('mainContent_co')[0];
        elements = mainContent.getElementsByTagName(tag);
    }
    else
    {
        elements = document.getElementsByTagName(tag);
    }
    return elements;
}
function scrollToElem(tag,num_occurence){
    var elements = getAllElements(tag);
    
    //elements[num_occurence].scrollIntoView(true);
    var top = elements[num_occurence].documentOffsetTop() - (window.innerHeight / 2 );
    window.scrollTo( 0, top );
    
    applySquaringOnElement( elements[num_occurence]);
    return "";
    
}

function putBadge(tag,num_occurence,count){
    
    var elements = getAllElements(tag);
    
    if ( !hasClass(elements[num_occurence],'hasComment'))
    elements[num_occurence].className +=  ' hasComment';
    
    elements[num_occurence].setAttribute('data-count', count);
    if ( count < 1 )
        elements[num_occurence].className +=  ' readed';
    else{
        if ( hasClass(elements[num_occurence],'readed')){
            removeClass(elements[num_occurence],'readed');
        }
    };
}

//function hasClass(ele,cls) {
//    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
//}
function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}
function removeClass(ele,cls) {
    if (hasClass(ele,cls)) {
        var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
        ele.className=ele.className.replace(reg,' ');
    }
}