//
//  UIViewController+Login.h
//  SupCast
//
//  Created by Maen Juganaikloo on 29/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilsCore.h"

@interface UIViewController (Login)

-(void) setNoLogin:(BOOL)status;
-(BOOL) userDontWantLogin;
-(void) popUpFirstLogin;
-(void) pushRegister;
-(void) popUpLogin;
-(void) popUpWrongLogin;

-(BOOL) checkForDefaultUser;
-(BOOL) isConnected;
-(BOOL) checkForLoggedInUser;
-(BOOL) logUser:(NSString *)username withPassword:(NSString*)password;
-(id) getSessionValueForKey:(NSString*)key;
-(void) logOut;
-(void) clearLogInfos;
-(void) setDefaultsPreferences;
-(id) getPreference:(NSString*)key;
@end
