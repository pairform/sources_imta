//
//  NSDictionary+WService.h
//  SupCast
//
//  Created by Maen Juganaikloo on 23/04/13.
//  Copyright (c) 2013 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (WService)
-(BOOL) post:(NSString *)url callback: (void (^)(NSDictionary * wsReturn)) callback;
-(BOOL) post:(NSString *)url callback: (void (^)(NSDictionary * wsReturn)) callback errorMessage:(NSString*)errorMessage activityMessage:(NSString*)activityMessage;
-(BOOL) post:(NSString *)url data: (NSData*)data callback: (void (^)(NSDictionary * wsReturn)) callback;
-(BOOL) post:(NSString *)url data: (NSData*)data callback: (void (^)(NSDictionary * wsReturn)) callback errorMessage:(NSString*)errorMessage activityMessage:(NSString*)activityMessage;
-(BOOL) post:(NSString *)url param: (NSDictionary*)param callback: (void (^)(NSDictionary * wsReturn)) callback;
-(BOOL) post:(NSString *)url param: (NSDictionary*)param callback: (void (^)(NSDictionary * wsReturn)) callback errorMessage:(NSString*)errorMessage activityMessage:(NSString*)activityMessage;

-(BOOL) post:(NSString *)url mixedParam:(id)mixedParam callback: (void (^)(NSDictionary * wsReturn)) callback errorMessage:(NSString*)errorMessage activityMessage:(NSString*)activityMessage;

-(NSString*) urlEncodedString:(NSDictionary*) dico;
-(BOOL)displayErrors:(NSDictionary *)wsReturn;
-(BOOL) isReachable;
@end
