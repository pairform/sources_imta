//
//  UIViewController+Login.m
//  SupCast
//
//  Created by Maen Juganaikloo on 29/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "UIViewController+Login.h"
#import "SIAlertView.h"
#import "RootVC.h"

@implementation UIViewController (Login)

#pragma mark - Méthodes de vérification de login

-(BOOL) checkForDefaultUser{
    //Si il y a un utilisateur enregistré sur l'appareil
    if([self getPermamentValue:@"username"])
    {
      
        return true;
    }
    else
    {
        //Si l'user ne veut pas être connecté
        if (![self userDontWantLogin])
        {
            [self popUpFirstLogin];
        }
        return false;
    }
}

-(void) setNoLogin:(BOOL)status{
    [[NSUserDefaults standardUserDefaults] setBool:status forKey:@"no_login"];
}

-(BOOL) userDontWantLogin{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"no_login"];
}

-(BOOL) isConnected{
    //Si il y a un utilisateur connecté sur l'appareil, ya les données du webService
    if([[NSUserDefaults standardUserDefaults] volatileDomainForName:kLogin])
        return true;
    else
        return false;
}
-(BOOL) checkForLoggedInUser{
    
    //Si il y a un utilisateur enregistré sur l'appareil
    if([self getPermamentValue:@"username"] && [[self getPreference:@"rememberMe"] boolValue])
    {
         //et pas connecté
        if(![self isConnected]){
            //On le log sans prompt
            return [self logUser:[self getPermamentValue:@"username"] withPassword:[self getPermamentValue:@"password"]];
        }
        else
            return true;
    }
    else
    {
        [self popUpLogin];
        return false;
    }
}

#pragma mark - PopUps

-(void) popUpFirstLogin{
    //Préférences par défaut pour monsieur
    [self setDefaultsPreferences];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Bienvenue sur PairForm!" andMessage:@"Pour profiter au maximum des fonctionalités de l'application, veuillez vous enregistrer / connecter!"];
    [alertView addButtonWithTitle:@"Enregistrement"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Login");
                              [self pushRegister];
                              
                          }];
    [alertView addButtonWithTitle:@"Login"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Login");
                              [self popUpLogin];
                              
                          }];
    [alertView addButtonWithTitle:@"Continuer sans connexion"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Annulation du log");
                              [self setNoLogin:true];
                              [self updateMessages];
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
}

-(void) pushRegister{

    if (IS_IPAD) {
        UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"AccountStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"EnregistrementVC"]];
        [(PSStackedViewController*)[[[[UIApplication sharedApplication] delegate] window] rootViewController] pushViewController:navController animated:YES];
    }
    else
    {
        UINavigationController * nController = [self isKindOfClass:NSClassFromString(@"UINavigationController")] ? (UINavigationController*)self : [self navigationController];
        if (nController == nil) {
            nController = (UINavigationController*)[(RootVC *)[[[[UIApplication sharedApplication] delegate] window] rootViewController] centerPanel];
            [(JASidePanelController*)[self parentViewController] toggleRightPanel:nil];
        }
        [nController pushViewController:[[UIStoryboard storyboardWithName:@"AccountStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"EnregistrementVC"] animated:YES];
    }
}

-(void) popUpLogin{
    UITextField * pseudoField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 32)];
    pseudoField.placeholder = @"Pseudo...";
    [pseudoField setBorderStyle:UITextBorderStyleRoundedRect];
    [pseudoField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    [pseudoField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    if([[self getPreference:@"rememberMe"] boolValue])
    {
        pseudoField.text = [self getPreference:@"username"];
    }
    
    UITextField * passField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 32)];
    passField.placeholder = @"Pass...";
    passField.secureTextEntry = YES;
    [passField setBorderStyle:UITextBorderStyleRoundedRect];
    [passField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Login" andTextFields:@[pseudoField,passField]];
    [alertView addButtonWithTitle:@"Annuler"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Annulation du log");
                          }];
    [alertView addButtonWithTitle:@"Login"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Login");
                              [self resignFirstResponder];
                              [self logUser:pseudoField.text withPassword:passField.text];
                              
                          }];
    
    [alertView addButtonWithTitle:@"Créer un compte"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Création de compte");
                              [self pushRegister];
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    
    [alertView show];

}

-(void) popUpWrongLogin{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Identifiants incorrects" andMessage:@"Vérifiez à nouveau votre login / mot de passe."];
    [alertView addButtonWithTitle:@"Annuler"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Annulation");
                              [self updateMessages];
                          }];
    [alertView addButtonWithTitle:@"Réessayer"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Réessaie");
                              [self popUpLogin];
                          }];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
    
    [alertView show];
}

#pragma mark - Méthodes de log
-(BOOL) logUser:(NSString *)username withPassword:(NSString*)password{
    
    [self setNoLogin:false];
    
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:username,@"username", password, @"password", nil];
    
    
    return [self post:[NSString stringWithFormat:@"%@/compte/loginCompte.php",sc_server]
         param: query
      callback: ^(NSDictionary *wsReturn) {
          
          
          //En cas d'échec
          if([[wsReturn objectForKey:@"status"] isEqualToString:@"ko"])
          {
              [self popUpWrongLogin];
          }
          //En cas de réussite
          else
          {
              
              [self updateMessages];
              //Inscription des infos de l'user dans les préférences permanentes de l'app
              NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
              
              //On supprime les infos permanentes si yen a déjà
              if([preferences persistentDomainForName:kLogin])
                  [self clearLogInfos];
              
              [preferences setPersistentDomain:query forName:kLogin];

              //Inscription des infos de l'user dans les préférences volatiles de l'app
              [preferences setVolatileDomain:wsReturn forName:kLogin];
              
              [self.view hideToastActivity];
              [self.view makeToast:@"Connexion réussie!"
                          duration:3.0
                          position:@"bottom"
                             image:[UIImage imageNamed:@"check.png"]];
              
              [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:self userInfo:@{@"status": @"connexion"}];
          }
      }
  errorMessage:@"Vous devez être connecté au Web pour vous connecter"
activityMessage:@"Connexion..."];
}

-(id) getPermamentValue:(NSString*)key
{
    //Récup des infos permamentes (username et pass)
    return [[[NSUserDefaults standardUserDefaults] persistentDomainForName:kLogin] objectForKey:key];
    
}

-(id) getSessionValueForKey:(NSString*)key
{
    //Récup des infos de session (tout le reste, envoyé du serveur)
    return [[[NSUserDefaults standardUserDefaults] volatileDomainForName:kLogin] objectForKey:key];
    
}

-(void) logOut{
    
    [self.view makeToast:@"Deconnexion réussie!"
                duration:3.0
                position:@"bottom"
                   image:[UIImage imageNamed:@"check.png"]];
    
    //Récup des préfs
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:self userInfo:@{@"status":@"deconnexion"}];
    //On clear toutes les infos retournées par le WS
    [preferences removeVolatileDomainForName:kLogin];
    [self clearLogInfos];
}

-(void) clearLogInfos{
    //Récup des préfs
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    
    //On clear toutes les préférences
    [preferences removePersistentDomainForName:kLogin];
}

-(void) setDefaultsPreferences{
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    
    NSDictionary * parametres = @{@"rememberMe" : [NSNumber numberWithBool:YES],
                                  @"usernameGPlus" : @"",
                                  @"passwordGPlus" : @"",
                                  @"publishTwitter" : [NSNumber numberWithBool:NO],
                                  @"geolocalisation" : [NSNumber numberWithBool:NO],
                                  @"automaticUpdate" : [NSNumber numberWithBool:YES],
                                  @"updateFrequency" : [NSNumber numberWithFloat:kMessageUpdateWhenLaunchApp],
                                  @"tutorialMode" : [NSNumber numberWithBool:YES]};
    
    [preferences setPersistentDomain:parametres forName:kPreferences];
}

-(id) getPreference:(NSString*)key{
    
    return [[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:key];
}

-(void)updateMessages{
    // Rafraichissement des messages
    DLog(@"%d",[[self getPreference:@"updateFrequency"] intValue ]);
    if ( [[self getPreference:@"updateFrequency"] intValue ] != 0){
        [UtilsCore rafraichirMessages];
    }
    if ( [[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"]intValue ] == kMessageUpdateEveryMinute){
        [UtilsCore startPullingTimer];
    }
}

@end
