//
//  WebViewVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 22/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageController.h"
#import "PageVC.h"

@interface WebViewVC : UIViewController <UIWebViewDelegate>

@property(nonatomic,strong) NSString * url;
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic ,strong) PageVC * pageVCDelegate;
@end
