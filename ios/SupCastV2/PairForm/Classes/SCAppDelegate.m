//
//  SCAppDelegate.m
//  SupCast
//
//  Created by Maen Juganaikloo on 15/04/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "SCAppDelegate.h"
#import "SIAlertView.h"
//#import "TestFlight.h"
#import "GAI.h"
#import "UtilsCore.h"
#import "FileManager.h"
#import "MessageCell.h"
#import "iRate.h"

@implementation SCAppDelegate

@synthesize managedObjectContext ;
@synthesize managedObjectModel ;
@synthesize persistentStoreCoordinator ;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //    [[NSBundle mainBundle] pathForResource:@"textured_paper" ofType:@"png"];
//    [_window setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunchUnzip"]) {

        [self lookForZipRessource];
        
        
    }
    
    // Optional: set Logger to VERBOSE for debug information.
//    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    // Initialize tracker.
    (void)[[GAI sharedInstance] trackerWithTrackingId:@"UA-40687872-3"];
    
    // start of your application:didFinishLaunchingWithOptions // ...
//    [TestFlight takeOff:@"b4b9b1bf-6bbd-418a-9180-45be4dde5987"];

    //Apparence des popups
    [[SIAlertView appearance] setMessageFont:[UIFont fontWithName:@"Helvetica-neue" size:13]];
    [[SIAlertView appearance] setTitleColor:[UIColor colorWithRed:0.133 green:0.133 blue:0.133 alpha:1]];
    [[SIAlertView appearance] setMessageColor:[UIColor colorWithRed:0.267 green:0.267 blue:0.267 alpha:1]];
    [[SIAlertView appearance] setCvBackgroundColor:[UIColor colorWithWhite:0.95 alpha:0.9]];
//    [[SIAlertView appearance] setCvBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper"]]];
    [[SIAlertView appearance] setCornerRadius:12];
    [[SIAlertView appearance] setShadowRadius:20];
    
//    if(IS_IPAD)
//    {
//        [[UIView appearanceWhenContainedIn:NSClassFromString(@"PSStackViewController"), nil] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"textured_paper"]]];
//    }
        [[UITableView appearanceWhenContainedIn:NSClassFromString(@"RootNavigation"), nil] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"textured_paper"]]];
        [[UIWebView appearanceWhenContainedIn:NSClassFromString(@"RootNavigation"), nil] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"textured_paper"]]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                UITextAttributeTextColor: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                          UITextAttributeTextShadowColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
                                     UITextAttributeFont: [UIFont fontWithName:@"Helvetica" size:0.0]
     }];
    
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"NavigationBar"] resizableImageWithCapInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 60) resizingMode:UIImageResizingModeTile] forBarMetrics:UIBarMetricsDefault];

#if TARGET_IPHONE_SIMULATOR
//    [[DCIntrospect sharedIntrospector] start];
#endif
    
    //configure iRate
    [iRate sharedInstance].applicationBundleID = @"fr.emn.PairForm";
    [iRate sharedInstance].daysUntilPrompt = 5;
    [iRate sharedInstance].usesUntilPrompt = 10;
    [iRate sharedInstance].appStoreCountry = @"FR";
    
    [_window makeKeyAndVisible];
	
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

     DLog( @"%d",[[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"]intValue ]);
    if ( [[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"]intValue ] != kMessageUpdateNever){
        [UtilsCore rafraichirMessages];
    }
    if ( [[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"]intValue ] == kMessageUpdateEveryMinute){
        [UtilsCore startPullingTimer];
    }
}



- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(BOOL)lookForZipRessource
{
    //Caler ici le nom du fichier pour inclure la ressource initiale
//    NSString *pathFichierZip = [[NSBundle mainBundle] pathForResource:@"C2i-D2" ofType:@"zip"];
    
    
    NSArray * arrayOfRessource = [[NSBundle mainBundle] pathsForResourcesOfType:@"zip" inDirectory:@""];
    
    NSArray * arrayOfResDictionary = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DefaultRes" ofType:@"plist"]];
    
    //Si les zip sont présents dans le bundle
    if ([arrayOfRessource count]) {
        
        //On les décompresse dans le dossier documents
        for (NSString* pathFichierZip in arrayOfRessource) {
            [FileManager unzipFileAtPath:pathFichierZip];
        }
        
        //On enregistre les ressources par défaut dans la base de données
        NSNumberFormatter * nFormatter = [[NSNumberFormatter alloc] init];
        
        for (NSDictionary * dico in arrayOfResDictionary) {
            
            NSNumber * id_ressource = [nFormatter numberFromString:dico[@"id_ressource"]];

            if(![UtilsCore getRessource:id_ressource])
            {
                [UtilsCore enregistrerRessourceFromDictionary:dico];
            }
            
        }
        
        return true;
    }
    
    return false;
    
}

- (void) customizeAppearance {
    
    UIImage *i1 = [UIImage imageNamed:@"precedent.png"];
    
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:i1
                                                      forState:UIControlStateNormal
                                                    barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:i1
                                                      forState:UIControlStateHighlighted
                                                    barMetrics:UIBarMetricsDefault];
    
}


// CORE DATA

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"mom" ];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


// FIN CORE DATA

@end
