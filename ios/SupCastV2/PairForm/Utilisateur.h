//
//  Image.h
//  profil
//
//  Created by admin on 26/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Utilisateur : NSManagedObject

@property (nonatomic, retain) NSNumber * id_utilisateur;
@property (nonatomic, retain) NSString * owner_username;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSData * image;
-(UIImage*) getImage ;
@end
