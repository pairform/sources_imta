//
//  Message.h
//  SupCast
//
//  Created by admin on 28/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Message : NSManagedObject

@property (nonatomic, retain) NSNumber * defi_valide;
@property (nonatomic, retain) NSNumber * estDefi;
@property (nonatomic, retain) NSNumber * estLu;
@property (nonatomic, retain) NSNumber * id_message;
@property (nonatomic, retain) NSNumber * id_ressource;
@property (nonatomic, retain) NSString * medaille;
@property (nonatomic, retain) NSString * nom_page;
@property (nonatomic, retain) NSString * nom_tag;
@property (nonatomic, retain) NSNumber * num_occurence;
@property (nonatomic, retain) NSNumber * owner_guid;
@property (nonatomic, retain) NSString * owner_rank_id;
@property (nonatomic, retain) NSString * owner_rank_name;
@property (nonatomic, retain) NSNumber * parent;
@property (nonatomic, retain) NSNumber * supprime_par;
@property (nonatomic, retain) NSData * tags;
@property (nonatomic, retain) NSData * tags_auteurs;
@property (nonatomic, retain) NSNumber * time_created;
@property (nonatomic, retain) NSNumber * time_updated;
@property (nonatomic, retain) NSNumber * user_a_vote;
@property (nonatomic, retain) NSNumber * utilite;
@property (nonatomic, retain) NSString * value;

-(NSArray*) getTags ;
-(NSArray*) getTagsNormalized ;
-(NSArray*) getTagsAuteurs ;

@end
