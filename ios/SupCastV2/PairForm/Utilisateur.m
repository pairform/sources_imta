//
//  Utilisateur.m
//  profil
//
//  Created by admin on 26/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "Utilisateur.h"


@implementation Utilisateur

@dynamic id_utilisateur;
@dynamic owner_username;
@dynamic url;
@dynamic image;

-(UIImage*) getImage {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[self image]];
}
@end