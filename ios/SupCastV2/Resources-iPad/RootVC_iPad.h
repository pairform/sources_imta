//
//  RootVC.h
//  PairForm
//
//  Created by Maen Juganaikloo on 01/10/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "PSStackedViewController.h"

@interface RootVC_iPad : PSStackedViewController <UINavigationControllerDelegate>

@end
