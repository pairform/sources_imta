//
//  RootVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 01/10/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "RootVC_iPad.h"
#import "HomeVC.h"
#import "MenuVC.h"
#import "MessageController.h"

@interface RootVC_iPad ()
@property (nonatomic, strong) PSStackedViewController *stackController;

@end

@implementation RootVC_iPad

@synthesize stackController = _stackController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNumberOfTouches:2];
    UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:[NSBundle mainBundle]];
    if (!self.rootViewController) {
        UIViewController * root = [mainStoryboard instantiateViewControllerWithIdentifier:@"RootVC"];
        [self setRootViewController:root];
        
    }
    MessageController * messageController = [mainStoryboard instantiateViewControllerWithIdentifier:@"MessageController"];
    messageController.modeMessage = ModeMessageTransversal;
    HomeVC * home = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    
//    [[[self rootViewController]view]setWidth:320];
    [self pushViewController:home animated:YES];
    [self pushViewController:messageController animated:YES];
    
    [self setLeftInset:70];
    [self setLargeLeftInset:320];
    [self setStackWidth:320];
//    [self setCornerRadius:1];
//    [self setDefaultShadowWidth:20.0f];
//    [TestFlight passCheckpoint:@"Lancement de l'application"];

    
    //    Custom background image pour la navigationBar
    //    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.navigationBar.frame];
    //    imageView.contentMode = UIViewContentModeLeft;
    //    imageView.image = [UIImage imageNamed:@"NavBar-iPhone.png"];
    //    [self.navigationBar insertSubview:imageView atIndex:0];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastDownloadedRessource:)
                                                 name:@"downloadToast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastErrorRessource:)
                                                 name:@"downloadErrorToast" object:nil];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    UIImageView * splashScreen = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Landscape"]];
    
    //Ajout de la marge de la status bar
//    CGRect frame = splashScreen.frame;
//    frame.origin.y = 20;
//    //    frame.origin.y = [UIApplication sharedApplication].statusBarFrame.size.height;
//    splashScreen.frame = frame;
//    
    //Tag pour futur récupération
    [splashScreen setTag:1000];
    
    [[self  view] addSubview:splashScreen];
    [[self view] bringSubviewToFront:splashScreen];
    [self performSelector:@selector(killSplashScreen) withObject:nil afterDelay:0];
}
- (void)killSplashScreen {
    //Alpha transition
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [[self view] viewWithTag:1000].alpha = 0.0;
        
        [[self view] viewWithTag:1000].transform = CGAffineTransformScale(CGAffineTransformIdentity, 3.f, 3.f);
    } completion:(void (^)(BOOL)) ^{
        [[[self view] viewWithTag:1000] removeFromSuperview];
    }];
}


-(void)toastDownloadedRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    
    [self.view makeToast:@"Téléchargement terminé!"
                duration:3.0
                position:@"bottom"
                   title:downloadedRessource.nom_court
                   image:[downloadedRessource getIcone]];
    
    
}

-(void)toastErrorRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    
    [self.view makeToast:@"Une erreur est survenue pendant le téléchargement, veuillez réessayer."
                duration:4.0
                position:@"bottom"
                   title:downloadedRessource.nom_court
                   image:[downloadedRessource getIcone]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
