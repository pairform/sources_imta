//
//  MenuVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuVC_iPad.h"
#import "PSStackedViewController.h"
#import "ProfilController.h"
#import "MessageController.h"
#import "UtilsCore.h"
#import "iRate.h"
#import "HelpVC.h"
#import "RootVC.h"
#import "Reachability.h"

@interface MenuVC_iPad ()

@end

@implementation MenuVC_iPad

@synthesize menuItems;

- (id)initWithStyle:(UITableViewStyle)style
{
//    UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:[NSBundle mainBundle]];
//    self = [mainStoryboard instantiateViewControllerWithIdentifier:@"rightVC"];
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    [super viewDidLoad];
    
    
    //Ajout d'un pull to refresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Actualisation de tous les messages..."];
    
    refreshControl.tintColor = [UIColor colorWithRed:0.447 green:0.49 blue:0.969 alpha:1]; /*#727df7*/
    [refreshControl addTarget:[UtilsCore class] action:@selector(rafraichirMessages) forControlEvents:UIControlEventValueChanged];
    refreshControl.tag = 100;
    
    [self.tableView insertSubview:refreshControl atIndex:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateConnectButton:) name:@"userLoggedIn" object:nil];

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(updateConnectButton:)
//                                                 name:@"rightPanelOpen" object:nil];
//    
    
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"menu_bg.png"]]];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    [[self tableView] setSeparatorColor:[UIColor colorWithWhite:1 alpha:0.1]];
    
    [self setStackWidth: 320];
    
    
//    menuItems = [[NSMutableArray alloc] initWithCapacity:8];
//    
//    for (int i=0; i<8; i++) {
//        [menuItems addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
//                              [NSString stringWithFormat:@"Item %d", i], @"title",
//                              [UIImage imageNamed:@"Sealred.png"], @"image", nil]];
//    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self updateConnectButton:nil];
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesErreur"
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesErreur"
                                                  object:nil];
}

-(void)refreshMessages{
    
    
    [(UIRefreshControl*)[self.tableView viewWithTag:100] endRefreshing];
}

-(void) updateConnectButton:(NSNotification*)aNotification{
    
    //Si l'utilisateur vient de se connecter, on passe par une notification
    if ([aNotification isKindOfClass:NSClassFromString(@"NSNotification")]) {
        NSString * status = [[aNotification userInfo] objectForKey:@"status"];
        if([status isEqualToString:@"connexion"])
            [self.connexionButton setText:@"Déconnexion"];
        else if([status isEqualToString:@"deconnexion"])
            [self.connexionButton setText:@"Connexion"];
        
    }
    //Si c'est la première fois que le menu s'affiche, il faut vérifier la connexion
    else
    {
        if([self isConnected])
            [self.connexionButton setText:@"Déconnexion"];
        else
            [self.connexionButton setText:@"Connexion"];
    }
    
    [self.connexionButton setNeedsLayout];
    [self.connexionButton layoutIfNeeded];
    
}

-(IBAction)feedback:(id)sender{
//    [TestFlight openFeedbackView];
    [[iRate sharedInstance] promptIfNetworkAvailable];
}
-(IBAction)connexion:(id)sender{

    if([self isConnected])
    {
        [[[self.parentViewController.stackController fullyVisibleViewControllers] lastObject] logOut];
    }
    else
    {
        [[[self.parentViewController.stackController fullyVisibleViewControllers] lastObject] checkForLoggedInUser];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Segue override

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if([segue isKindOfClass:NSClassFromString(@"PSStackedPopViewSegue")])
//        ((UIViewController*)segue.destinationViewController).view.width = 640;
//    else
//        ((UIViewController*)segue.destinationViewController).view.width = 320;
    
    if([[segue identifier] isEqualToString:@"derniersMessagesSegue"]){
        MessageController * messageController = segue.destinationViewController;
        messageController.modeMessage = ModeMessageTransversal;
    }
    if([[segue identifier] isEqualToString:@"monProfilSegue"]){
        ProfilController * view = segue.destinationViewController;
        view.compte = [self getSessionValueForKey:@"id"];
        view.stackWidth = 704;
    }
    if([[segue identifier] isEqualToString:@"AideSegue"]){
        NSString * screenID = [[self.navigationController topViewController] restorationIdentifier];
        if(IS_IPAD)
        {
            id lastVC = [self.parentViewController.stackController.fullyVisibleViewControllers lastObject];

            if ([lastVC isKindOfClass:NSClassFromString(@"UINavigationController")]) {
                lastVC = [lastVC topViewController];
            }
            
            screenID = [lastVC restorationIdentifier];
        }
        
        
        HelpVC * helpVC = segue.destinationViewController;
        [helpVC setScreenStoryboardID:screenID];
        [helpVC setCurrentIndex:0];
        helpVC.view.width = 320;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"monProfilSegue"]){
        if ([self checkForLoggedInUser])
            return YES;
        else
            return NO;
    }
    if ( [identifier isEqualToString:@"RechercheProfilSegue"]){
        return [self isReachable];
    }
    return YES;
}

#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    // Return the number of sections.
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//
//    // Return the number of rows in the section.
//    return [menuItems count];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"MenuCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    if([tableView viewWithTag:11] == cell)
//    {
//        NSDictionary * currentMenuItem = [menuItems objectAtIndex:indexPath.row];
//        
//        UIImageView * icon = (UIImageView *)[cell viewWithTag:0];
//        [icon setImage:[currentMenuItem objectForKey:@"image"]];
//        
//        UILabel * title = (UILabel *)[cell viewWithTag:1];
//        if([self checkForLoggedInUser])
//            [title setText:@"Deconnexion"];
//        else
//            [title setText:@"Connexion"];
//
//    }
//    
//    return cell;
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


@end
