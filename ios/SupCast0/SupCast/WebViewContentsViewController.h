//
//  WebViewContentsViewController.h
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GestionReseauSocialViewController.h"
#import "GestionFichier.h"

@interface WebViewContentsViewController : GestionReseauSocialViewController <UIWebViewDelegate,MKOverlay, UIGestureRecognizerDelegate>
{
	int indexItemContent; // indice de l'item courant
}

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UILabel *labelTitre;
@property (nonatomic, weak) IBOutlet UIToolbar *toolBar;
@property(nonatomic,strong) NSString *pageHtml;

@end
