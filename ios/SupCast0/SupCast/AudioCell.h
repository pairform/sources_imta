//
//  AudioCell.h
//  SupCast
//
//  Created by Phetsana on 16/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVFoundation/AVAudioPlayer.h"

@interface AudioCell : UITableViewCell {
	UILabel *primaryLabel;
	
	UIButton *myButton1;
	UIButton *myButton2;
	
	NSString *audioNameVersion1;
	NSString *audioNameVersion2;
	NSString *audioVersion1Extension;
	NSString *audioVersion2Extension;
	
	AVAudioPlayer *player;
	NSString *dirRes;
}
@property (nonatomic, retain) NSString *dirRes;
@property (nonatomic, retain) UILabel *primaryLabel;
@property (nonatomic, retain) UIButton *myButton1;
@property (nonatomic, retain) UIButton *myButton2;
@property (nonatomic, retain) NSString *audioNameVersion1;
@property (nonatomic, retain) NSString *audioNameVersion2;
@property (nonatomic, retain) NSString *audioVersion1Extension;
@property (nonatomic, retain) NSString *audioVersion2Extension;
@property (nonatomic, retain) AVAudioPlayer *player;

// Méthode de classe de construction de bouton 
+ (UIButton *)buttonWithTitle:	(NSString *)title
					   target:(id)target
					 selector:(SEL)selector
						frame:(CGRect)frame
						image:(UIImage *)image
				 imagePressed:(UIImage *)imagePressed
				darkTextColor:(BOOL)darkTextColor;
//-(NSString *)dataFilePath:(NSString *)fileName; 
- (void)setAudioName:(NSString *)audioName
	   extensionName:(NSString *)extensionName 
			 version:(int)version;
- (void)playSoundVersion1;
- (void)playSoundVersion2;

@end
