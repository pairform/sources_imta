//
//  RecupererMessagesViewController.h
//  SupCast
//
//  Created by fgutie10 on 28/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GestionBlog.h"
#import "CommentViewCell.h"
#import "ModerationMessagesViewController.h"
#import "MapViewController.h"

@interface RecupererMessagesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {

	NSString *username;
	NSString *password;
	NSString *titreParent;
	NSString *idEcranParent;
	NSString *idEcran;
	NSString *ressourceid;
	NSString *elggid;
	NSString *idBlog;
	NSString *categorie;
	NSString *titre;
			
	//NSMutableData *receivedData;
	
	int connectionStatus;
	
	NSMutableDictionary *dictionnaire;
	NSString *currentElementName;
	NSMutableString *currentText;
	int iteration;
	int cumul;
		
	IBOutlet UITableView * tableView;
    IBOutlet UIButton *boutonListeBlogs;
	IBOutlet UIButton *boutonMoreComments;
	IBOutlet UILabel *pasDeMessages;
	IBOutlet UILabel *plusDeMessages;
    NSMutableArray * listeCoor;
    NSMutableArray *plist;
    NSMutableArray *listeMessages;
    NSMutableData *receivedDataCreerBlog;
    NSURLConnection * connectionCreerBlog;
    NSMutableData *receivedDataMessages;
    NSURLConnection * connectionMessages;
    NSDictionary *ressourceInfo;
    
}
@property(nonatomic,strong) NSDictionary *ressourceInfo;
@property(nonatomic,strong) NSURLConnection * connectionMessages;
@property(nonatomic,strong) NSURLConnection * connectionCreerBlog;
@property(nonatomic,strong) NSMutableData *receivedDataMessages;
@property(nonatomic,strong) NSMutableData *receivedDataCreerBlog;

@property ( nonatomic, weak) IBOutlet UIButton *boutonCreerBlog;
@property(nonatomic,strong) NSMutableArray *plist;
@property(nonatomic,strong) NSMutableArray *listeMessages;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *titreParent;
@property(nonatomic,strong) NSString *idEcranParent;
@property(nonatomic,strong) NSString *idEcran;
@property(nonatomic,strong) NSString *idBlog;
@property(nonatomic,strong) NSString *categorie;

@property(nonatomic,strong) NSString *titre;
@property (nonatomic, weak) IBOutlet UILabel *labelTitre;
@property (nonatomic, weak) IBOutlet UILabel *pseudoCategorieTitre;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;

@property(nonatomic,strong) NSMutableArray * listeCoor;

-(IBAction)moreComments;
-(void)creerBlog;

@end
