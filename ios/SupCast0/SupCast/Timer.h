//
//  Timer.h
//  SupCast
//
//  Created by Phetsana on 19/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Timer : NSObject {
	int min;
	int sec1;
	int sec2;
	NSString *timeLeft;
}

@property (nonatomic, retain) NSString *timeLeft;
@property (nonatomic) int min;

- (id)initTimerWithMin:(int)m sec:(int)s;
- (NSString *)update;
- (NSString *)timeLeft;

@end
