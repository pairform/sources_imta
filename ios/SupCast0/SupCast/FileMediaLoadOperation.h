//
//  FileMediaLoadOperation.h
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileMediaLoadOperationProtocol.h"

#define K_STATUS_OPERATION @"statusFileMediaLoadingOperation"
#define K_BANDWITH_OPERATION @"bandwidthFileMediaLoadingOperation"
#define K_DATASIZE_OPERATION @"dataSizeFileMediaLoadingOperation"

@interface FileMediaLoadOperation : NSOperation {
    NSDictionary * infoRessource;
    NSDictionary * anObject;
    UIViewController <FileMediaLoadOperationProtocol> * parentProcessViewController;
    BOOL statusInCacheOK;
    double bandwidth;
    NSUInteger  tailleData;
    NSString * ressourceFolder;
    NSString * pathURL;
    
}
@property(nonatomic,strong) NSString * pathURL;

@property(nonatomic,strong) NSString * ressourceFolder;
@property(nonatomic,strong) NSDictionary * infoRessource;
@property(nonatomic,strong) NSDictionary * anObject;
@property(nonatomic,strong) UIViewController < FileMediaLoadOperationProtocol> * parentProcessViewController;

- (double)computeBandwidthWithDateDebut:(NSDate *)debutParam
								dateFin:(NSDate *)finParam
							 tailleData:(NSUInteger) tailleDataParam;

-(void) main;
-(void)downloadImageFromDict; 
-(void)addSkipBackupAttributeToPath:(NSString*)path;

@end
