//
//  GestionFichier.m
//  SupCast
//
//  Created by Didier PUTMAN on 08/02/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionFichier.h"
#include <sys/xattr.h>

@implementation GestionFichier

-(NSString *)dataFilePath:(NSString *)fileName {
	DLog(@"");
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}
-(void)addSkipBackupAttributeToPath:(NSString*)path {
    u_int8_t b = 1;
    setxattr([path fileSystemRepresentation], "com.apple.MobileBackup", &b, 1, 0, 0);
}

-(NSString *)dataFilePathAtDirectory:(NSString *)directory 
							fileName:(NSString *)fileName {
    
	DLog(@"");
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *subDirectory = [documentsDirectory stringByAppendingPathComponent:directory];
	BOOL isDir=NO;
	
    if ([[NSFileManager defaultManager] fileExistsAtPath:subDirectory isDirectory:&isDir] && isDir)
	{
		//
	}
	else
	{
		if ([[NSFileManager defaultManager] createDirectoryAtPath:subDirectory withIntermediateDirectories:YES attributes:nil error:nil])
            [self addSkipBackupAttributeToPath:subDirectory];
    
    }
	
	return [subDirectory stringByAppendingPathComponent:fileName];
	
}


- (void)dealloc {
	DLog(@"");
}

@end
