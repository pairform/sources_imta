//
//  MCQSoundViewController.m
//  SupCast
//
//  Created by Phetsana on 29/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
#import "GestionFichier.h"
#import "MCQSoundViewController.h"
#import "MCQSoundFlipsideViewController.h"

@implementation MCQSoundViewController

@synthesize player;
//@synthesize dirRes;
@synthesize dicoRacine;
@synthesize examples;

/*
-(NSString *)dataFilePath:(NSString *)fileName 
{    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}
*/
 
- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil 
		{
	
    self = [super initWithNibName:nibNameOrNil 
                           bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
				
    }
	
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	
	[fondEcranImageView setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																						   fileName:[self.dicoRacine objectForKey:kImageFondEcran]]]];
	
	indicationTableView.backgroundColor = [UIColor clearColor];
	
	questionNumber.text = [[NSString alloc] initWithFormat:@"%d/%d", indexQuestion + 1, [questionList count]];
	
	// Resize the tableView to hide empty row
	UIFont *captionFont =[UIFont fontWithName:kVerdana size:14];
	CGSize cs = [indication sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
	height1 = cs.height + 40; // Add a space between each row
	
	captionFont =[UIFont fontWithName:kHelvetica size:12];
	cs = [[[questionList objectAtIndex:indexQuestion] objectForKey:kText] sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
	
	if ([[questionList objectAtIndex:indexQuestion] objectForKey:kText] != nil)
		height2 = cs.height + 26;
	else
		height2 = 0;
	
	indicationTableView.frame = CGRectMake(indicationTableView.frame.origin.x, 
										   indicationTableView.frame.origin.y, 
										   indicationTableView.frame.size.width, 
										   height1 + height2 + 40 + 62 + 20);
	
	// Resize the tableView to hide empty row
	float tmpHeight = 0;
	int numberOfSequence = [self tableView:tableView numberOfRowsInSection:0]; 
	NSString *answersChoice;
	for (int i = 0; i < numberOfSequence; i++) {
		answersChoice = [[[questionList objectAtIndex:indexQuestion] objectForKey:kAnswersChoice] objectAtIndex:i];
		UIFont *captionFont = [UIFont boldSystemFontOfSize:20];
		CGSize cs = [answersChoice sizeWithFont:captionFont constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
		tmpHeight += (cs.height + 20)	;
	}
	
	tableView.frame = CGRectMake(tableView.frame.origin.x, 
								 indicationTableView.frame.origin.y + indicationTableView.frame.size.height + 10, 
								 tableView.frame.size.width, 
								 tmpHeight + 22);
	
	mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width,
											tableView.frame.origin.y + tableView.frame.size.height);
	
	
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)update {
	[super update];
	questionNumber.text = [[NSString alloc] initWithFormat:@"%d/%d", indexQuestion + 1, [questionList count]];
	[indicationTableView reloadData];
}

- (IBAction)playSound {
	// Get the audio file path
	/*	NSString *path = [[NSBundle mainBundle] pathForResource:[[questionList objectAtIndex:indexQuestion] objectForKey:kSoundName] 
	 ofType:[[questionList objectAtIndex:indexQuestion] objectForKey:kSoundExtension]];
	 */
	//NSString * myPath =[[NSBundle mainBundle] resourcePath];
	//NSString *path = [myPath stringByAppendingPathComponent:[[[questionList objectAtIndex:indexQuestion] objectForKey:kSoundName]
	//														 stringByAppendingPathExtension:[[questionList objectAtIndex:indexQuestion] objectForKey:kSoundExtension]]];
	
	

	NSString * path = [[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[[[questionList objectAtIndex:indexQuestion] objectForKey:kSoundName]
										  stringByAppendingPathExtension:[[questionList objectAtIndex:indexQuestion] objectForKey:kSoundExtension]]];
	
	NSURL *url = [NSURL fileURLWithPath:path];
	
	// Initilize AVAudioPlayer
	AVAudioPlayer *tmpPlayer;
	tmpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
	
	self.player = tmpPlayer;
	[tmpPlayer release];
	[player play];
}


- (void)segmentAction:(id)sender {
	if ([sender selectedSegmentIndex] == 0)
		[self previousQuestion];
	else if ([sender selectedSegmentIndex] == 1)
		[self nextQuestion];
}

- (void)backToExercice {
	// Build the Segmented Control 
	NSArray *buttonNames = [NSArray arrayWithObjects:@"<", @">", nil]; 
	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] 
											initWithItems:buttonNames]; 
	segmentedControl.momentary= YES; 
	// Customize the Segmented Control 
	segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth; 
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar; 
	segmentedControl.tintColor = [UIColor blackColor];
	segmentedControl.frame = CGRectMake(0, 0, 60, 30); 
	[segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged]; 
	
	// Embedded into a view
	UIView *segmentedControlView = [[UIView alloc] init];
	segmentedControlView.frame = segmentedControl.frame;
	segmentedControl.backgroundColor = [UIColor clearColor];
	[segmentedControlView addSubview:segmentedControl];
	
	// Add the control to the navigation bar 
	UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControlView];
	self.navigationItem.rightBarButtonItem = buttonItem;
	[buttonItem release];
	[segmentedControl release]; 
	[segmentedControlView release];
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	
	//[self.tabBarController.navigationController.navigationBar popNavigationItemAnimated:YES];
	[flipsideViewController.view removeFromSuperview];
	
	[UIView commitAnimations];
}

- (void)loadFlipsideViewController {
	//	flipsideViewController = [[MCQSoundFlipsideViewController alloc] 
	//							  initWithNibName:@"MCQSoundFlipsideView" 
	//							  bundle:nil
	//							  text1:[[examples objectForKey:kText1] retain]
	//							  text2:[examples objectForKey:kText2]
	//							  image:[UIImage imageNamed:[[NSString alloc] initWithFormat:@"%@.%@", 
	//														 [examples objectForKey:kImageName],
	//														 [examples objectForKey:kImageExtension]]]
	//							  soundsList:[examples objectForKey:kSounds]];
	

	flipsideViewController = [[MCQSoundFlipsideViewController alloc] 
							  initWithNibName:@"MCQSoundFlipsideView" 
							  bundle:nil
							  text1:[[examples objectForKey:kText1] retain]
							  text2:[examples objectForKey:kText2]
							  image:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[[NSString alloc] initWithFormat:@"%@.%@", 
														 [examples objectForKey:kImageName],
														 [examples objectForKey:kImageExtension]]]]
							  soundsList:[examples objectForKey:kSounds]];
	
	UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(backToExercice)];
	UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"..."];
	self.navigationItem.rightBarButtonItem = buttonItem;
	[self.tabBarController.navigationController.navigationBar pushNavigationItem:navigationItem animated:NO];
	[navigationItem release];
	[buttonItem release];
}

- (IBAction)showExamples {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	
	UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(showExamples)];
	UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"..."];
	self.navigationItem.rightBarButtonItem = buttonItem;
	//[self.tabBarController.navigationController.navigationBar pushNavigationItem:navigationItem animated:NO];
	[navigationItem release];
	[buttonItem release];
	
	[self loadFlipsideViewController];
	[self.view addSubview:flipsideViewController.view];
	[UIView commitAnimations];
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	if (aTableView == indicationTableView) {
		if ([[questionList objectAtIndex:indexQuestion] objectForKey:kText] != nil)
			return 3;
		return 2;
	}
	return 1;	
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	NSInteger result;
	
	if (aTableView == indicationTableView)
		result = 1;
	else
		result = [super tableView:aTableView numberOfRowsInSection:section];
	
	return result;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [super tableView:aTableView cellForRowAtIndexPath:indexPath];
	
	if (aTableView ==  indicationTableView) {
		if (indexPath.section == 0) {
			cell.textLabel.font = [UIFont fontWithName:kVerdana size:14];
			cell.textLabel.text = indication;
		} else if (indexPath.section == 1) {
			cell.textLabel.text = @"Ecouter l'extrait sonore";
		} else if (indexPath.section == 2) {
			cell.textLabel.font = [UIFont fontWithName:kHelvetica	size:12];
			cell.textLabel.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kText];
		}
		
	}
	
	return cell;        	
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat result = [super tableView:aTableView heightForRowAtIndexPath:indexPath];
	CGSize cs;
	
	if (aTableView == indicationTableView)
		if (indexPath.section == 0) {
			UIFont *captionFont = [UIFont fontWithName:kVerdana size:14];
			cs = [indication sizeWithFont:captionFont 
						constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) 
							lineBreakMode:UILineBreakModeWordWrap];
			result = cs.height + 40; // Add a space between each row
		} else if (indexPath.section == 2) {
			UIFont *textFont =[ UIFont fontWithName:kHelvetica size:12];
			cs = [[[questionList objectAtIndex:indexQuestion] objectForKey:kText] sizeWithFont:textFont
																			   constrainedToSize:CGSizeMake(self.view.frame.size.width , FLT_MAX) 
																				   lineBreakMode:UILineBreakModeWordWrap];			
			result = cs.height + 20; // Add a space between each row
		} else {
			result = 40;
		}
	
	return result;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	
	if (aTableView == tableView) {
		[super tableView:aTableView didSelectRowAtIndexPath:indexPath];
	} else if (aTableView == indicationTableView && indexPath.section == 1) {
		[self playSound];
	}
}


- (void)dealloc {
	[indicationTableView release];
	[examples release];
	[questionNumber release];
	//[flipsideViewController release];	
    [super dealloc];
}


@end
