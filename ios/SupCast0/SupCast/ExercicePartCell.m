//
//  ExercicePartCell.m
//  SupCast
//
//  Created by Phetsana on 17/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "ExercicePartCell.h"

#define kFontSize 20

@implementation ExercicePartCell

@synthesize label,dirRes;

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		
		self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		label = [[UILabel alloc]init];
		label.textAlignment = UITextAlignmentCenter;
		label.font = [UIFont systemFontOfSize:kFontSize];
		label.lineBreakMode = UILineBreakModeWordWrap;
		label.numberOfLines = 0;
		
		[self.contentView addSubview:label];
    }
    return self;
}

+ (CGFloat)cellHeightForCaption:(NSString *)caption 
						  width:(CGFloat)width {
    UIFont *captionFont = [UIFont boldSystemFontOfSize:kFontSize];
    CGSize cs = [caption sizeWithFont:captionFont 
					constrainedToSize:CGSizeMake(width - 50, FLT_MAX) 
						lineBreakMode:UILineBreakModeWordWrap];
	
	return cs.height + 20;
}

- (void)layoutSubviews {
	
	[super layoutSubviews];
	
	CGFloat cellHeight = self.contentView.bounds.size.height;
	CGFloat cellWidth = self.contentView.bounds.size.width;
	
	CGRect frame;	
	frame = CGRectMake(6, 0, cellWidth, cellHeight);
	label.frame = frame;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}


- (void)dealloc {
	[label release];
    [super dealloc];
}


@end
