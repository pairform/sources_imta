//
//  FileMediaLoadOperation.m
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "FileMediaLoadOperation.h"
#import "FileMediaLoadOperationProtocol.h"
#include <sys/xattr.h>

@implementation FileMediaLoadOperation

@synthesize pathURL;
@synthesize ressourceFolder;
@synthesize infoRessource;
@synthesize anObject;
@synthesize parentProcessViewController;

#pragma mark -
#pragma mark init

-(id)init {
    
    //DLog(@"");
    self=[super init];
    if (self) {
        statusInCacheOK = NO;
    }
    return self;
}


#pragma mark -
#pragma mark view life cycle methods

- (void) main {
    //DLog(@"");
    
    // appel de la methode de download
    [self downloadImageFromDict];    
    
    NSDictionary *dictRetour = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithBool:statusInCacheOK], K_STATUS_OPERATION,
                                [NSNumber numberWithDouble:bandwidth], K_BANDWITH_OPERATION,
                                [NSNumber numberWithDouble:tailleData], K_DATASIZE_OPERATION,
                                nil];
    
    // appel de la methode de fin traitement sur le thread principal
    [self.parentProcessViewController performSelectorOnMainThread:@selector(finFileMediaLoadOperation:) withObject:dictRetour waitUntilDone:NO];

}

-(void)addSkipBackupAttributeToPath:(NSString*)path {
    u_int8_t b = 1;
    setxattr([path fileSystemRepresentation], "com.apple.MobileBackup", &b, 1, 0, 0);
}

-(void)downloadImageFromDict 
{
    //DLog(@"");
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *fileError;
    statusInCacheOK = NO;
    
    NSString *nomComplet;
    NSString *ressourceExtension;
    NSString *ressourceLastPathComponent;
    NSString *ressourceReference;
    NSString *urlRessource;

    
    NSString * stringRessourcebaseURL = [self.infoRessource objectForKey:kRessourcebaseURL];
    NSString * ressourcefolder = [self.infoRessource objectForKey:kRessourceFolderStr];

    for (id key in anObject)
    {   
        if([key isEqualToString:@"CheminRelatif"])
            if ([anObject objectForKey:key])
                nomComplet = [anObject objectForKey:key];            
        if([key isEqualToString:@"nom"])
            if ([anObject objectForKey:key])
                nomComplet = [anObject objectForKey:key];            
        if([key isEqualToString:@"extension"])
            if ([anObject objectForKey:key])
                ressourceExtension = [anObject objectForKey:key];
        if([key isEqualToString:@"lastPathComponent"])
            if ([anObject objectForKey:key])
                ressourceLastPathComponent = [anObject objectForKey:key];
        if([key isEqualToString:@"reference"])
            if ([anObject objectForKey:key])
                ressourceReference = [anObject objectForKey:key];
        if([key isEqualToString:@"url"])
            if ([anObject objectForKey:key])
                urlRessource = [anObject objectForKey:key];
        if([key isEqualToString:@"baseurl"])
            if ([anObject objectForKey:key])
                urlRessource = [anObject objectForKey:key];
    }
    
    // dossier document de l'application
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // path du fichier à t de la ressource
    NSString *filePathRessource = [documentsDirectory stringByAppendingPathComponent:[self.ressourceFolder stringByAppendingPathComponent:urlRessource]];
	
    // path du sous dossier de la ressource    
    NSString *subDirectory = [filePathRessource stringByDeletingLastPathComponent];    
    
    BOOL isDir=NO;
    
   	NSString *ressourcePath = [[stringRessourcebaseURL stringByAppendingPathComponent:ressourcefolder] stringByAppendingPathComponent:urlRessource];
    //DLog(@"resourcePath is %@",ressourcePath );
        
    // formatage de l'url (remplacement espace par %20)    
    CFStringRef originalURLString = (__bridge CFStringRef)ressourcePath;
    CFStringRef preprocessedString =
    CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, originalURLString, CFSTR(""), kCFStringEncodingUTF8);
    CFStringRef urlString =
    CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, preprocessedString, NULL, NULL, kCFStringEncodingUTF8);
    CFURLRef url = CFURLCreateWithString(kCFAllocatorDefault, urlString, NULL);
    
    // verification de l'existence du dossier    
    if (!([fileManager fileExistsAtPath:subDirectory isDirectory:&isDir] && isDir))
	{
        
        // creation du dossier avec arborescence    
        if(![fileManager createDirectoryAtPath:subDirectory withIntermediateDirectories:YES attributes:nil error:&fileError])
        {
            // problème dans la création du chemin, certainement chemin invalide
            DLog(@"erreur création répertoire pour le cache de l'image : %@",
                 [fileError localizedDescription]);
        }
        else {
            [self addSkipBackupAttributeToPath:subDirectory];

        }
	}
    
    // verification de l'existence du fichier    
    if (![fileManager fileExistsAtPath:filePathRessource])
    {   
       
        //DLog(@"le fichier n'existe pas %@",filePathRessource);
        // telechargement du fichier si il n'existe pas    
        NSData * imageData = nil;
        NSDate *debut = [NSDate date];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        imageData = [NSData dataWithContentsOfURL:(__bridge NSURL *)url];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSDate *fin = [NSDate date];
        tailleData = [imageData length];
        bandwidth = [self computeBandwidthWithDateDebut:debut
                                                dateFin:fin
                                             tailleData:[imageData length]];
        
        // chemin créé OK, on écrit l'image sur disque
        if ([imageData writeToFile:filePathRessource
                        atomically:YES]) 
        {
            statusInCacheOK = YES;
        }
        
    }
    else
    {  
        //DLog(@"le fichier existe %@",filePathRessource);
    }
}



#pragma mark -
#pragma mark calcul

- (double)computeBandwidthWithDateDebut:(NSDate *)debutParam
								dateFin:(NSDate *)finParam
							 tailleData:(NSUInteger) tailleDataParam {
    //DLog(@"");
	double bandwidthComputed = 0.0;
	if ([finParam timeIntervalSinceDate:debutParam]>0.0) {
		bandwidthComputed = tailleDataParam/[finParam timeIntervalSinceDate:debutParam];
		DLog(@"taille = %i, durée = %.4f, débit = %.3f",
             tailleDataParam,
             [finParam timeIntervalSinceDate:debutParam],
             bandwidthComputed);
	}
	return bandwidthComputed;
}



@end
