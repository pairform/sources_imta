//
//  AddCommentViewController.m
//  SupCast
//
//  Created by fgutie10 on 22/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "AddCommentViewController.h"

#import "SHKActionSheet.h"
#import "SHK.h"
#import "SHKTwitter.h"
#import "SHKTwitterForm.h"
#import "SHKSharer.h"
#import "SHKCustomShareMenu.h"

@implementation AddCommentViewController

@synthesize statusPublicationGPS;
@synthesize statusPublicationTwitter;
@synthesize statusPublicationGPSLocal;
@synthesize statusPublicationTwitterLocal;
@synthesize motdepasseTwitter;
@synthesize userTwitter;

@synthesize username;
@synthesize password;
@synthesize titreParent;
@synthesize idEcranParent;
@synthesize idEcran;
@synthesize idBlog;
@synthesize categorie;
@synthesize estExercice;
@synthesize pseudoCategorieTitre;
@synthesize labelTitre;
@synthesize titre;
@synthesize textView;
@synthesize boutonTwitter;
@synthesize boutonLocalisation;
@synthesize boutonPublier;
@synthesize boutonProposerExercice;
@synthesize boutonCommenterExercice;
@synthesize locationManager;
@synthesize coordonneesGPS;
@synthesize indicator;
@synthesize receivedData;
@synthesize ressourceInfo;


#pragma mark -
#pragma mark View Load 

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    DLog(@"");
    connectionStatus = 0;
	[indicator stopAnimating];
    
	labelTitre.text = self.titre;
    
	NSString *pseudoCategorieTitreTexte = [[NSString alloc] initWithFormat:@"%@ - (%@)",self.username, self.categorie];
	pseudoCategorieTitre.text = pseudoCategorieTitreTexte;
	
	if (estExercice)
    {	
        // boutons à afficher        
		boutonCommenterExercice.hidden = NO;
		boutonProposerExercice.hidden = NO;
        
        // boutons à ne pas afficher		
		boutonTwitter.hidden = YES;
		boutonLocalisation.hidden =YES;        
		boutonPublier.hidden = YES;
		
	}
	else
    {
        // boutons à afficher        
		boutonPublier.hidden = NO;
        boutonTwitter.hidden = NO;
		boutonLocalisation.hidden = NO;
        
        // boutons à ne pas afficher		
		boutonCommenterExercice.hidden = YES;
		boutonProposerExercice.hidden = YES;
        
        
        // Recherche des coordonnées GPS
        
		self.locationManager = [[CLLocationManager alloc] init];
		self.locationManager.delegate = self;
		[self.locationManager startUpdatingLocation];
        
	}
    
    
    // Récupération de regle de publication     
    self.statusPublicationGPS = NO;
    self.statusPublicationTwitter = NO;
    self.statusPublicationGPSLocal = NO;
    self.statusPublicationTwitterLocal = NO;
    
    self.motdepasseTwitter  = [[NSMutableString alloc] initWithString:@""];
    self.userTwitter = [[NSMutableString alloc] initWithString:@""];
        
    NSString *thePathFilenameRegle = [self dataFilePath:kFilenameRegle];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:thePathFilenameRegle])
    {
        self.statusPublicationGPS = NO;
        self.statusPublicationTwitter = NO;
        [self.userTwitter setString:@""];
        [self.motdepasseTwitter setString:@""];
        
    }
    
    NSArray *myArray = [[NSArray alloc] initWithContentsOfFile:thePathFilenameRegle];
    self.statusPublicationGPS =[[[myArray objectAtIndex:0] objectForKey:kGPS] boolValue];
    self.statusPublicationTwitter =[[[myArray objectAtIndex:1] objectForKey:kTWITTER] boolValue];
    
    if (self.statusPublicationGPS)
    {
        
        BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
        BOOL locationAvailable =  ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized);
        
        if (!locationAllowed) 
        {            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation est désactivé." 
                                                            message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            
            self.statusPublicationGPSLocal = NO;
        }
        else 
        {
            if (!locationAvailable)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation de l'Application est désactivé." 
                                                                message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
                [alert show];
                
                self.statusPublicationGPSLocal = NO;
            }
            else
            {
                self.statusPublicationGPSLocal = YES;
            }
        }
        
    }
    
    else        
    {
        self.statusPublicationGPSLocal = self.statusPublicationGPS;
    }
    
    
    self.statusPublicationTwitterLocal = self.statusPublicationTwitter;
    
    if ( [[myArray objectAtIndex:1] objectForKey:kUSER] == nil)
        [self.userTwitter setString:@""];
    else
        [self.userTwitter setString:[[myArray objectAtIndex:1] objectForKey:kUSER]];
    
    if ( [[myArray objectAtIndex:1] objectForKey:kMDP] == nil)
        [self.motdepasseTwitter setString:@""];
    else
        [self.motdepasseTwitter setString:[[myArray objectAtIndex:1] objectForKey:kMDP]];
    
    
    [self gestionBouton];
    
    if (([self.idBlog isEqualToString:@""]) || (self.idBlog == nil))
    {   
        [self creerBlog];
    }
    
    
    
    [super viewDidLoad];
}


#pragma mark -
#pragma mark Actions 


-(IBAction)sendMessage {
    
    DLog(@"");
	NSString *message = [[NSString alloc] initWithString:textView.text];
    
	if ([message length] != 0) 
    {
        if (self.statusPublicationTwitterLocal)
        {
            //Publier sur Twitter
            [self tweet:message];
            
            if (self.statusPublicationGPSLocal)
            {
                //Publier sur elgg avec localisation
                [self publierMessageAvecLocalisation:message];
            }
            else
            {
                //Publier sur elgg
                [self publierMessage:message];
            }
            
            
        } 
        else
        {    
            if (self.statusPublicationGPSLocal)
            {
                //Publier sur elgg avec localisation
                [self publierMessageAvecLocalisation:message];
            }
            else
            {
                //Publier sur elgg
                [self publierMessage:message];
            }
        }
    }        
    
	else 
    {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez composer un message avant de l'envoyer"
													   delegate:self
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
	}
    
}

-(void)gestionBouton
{
    DLog(@"");
    
    UIImage *image = (self.statusPublicationGPSLocal) ? [UIImage imageNamed:kImageLocalisationActivee] : [UIImage imageNamed:kImageLocalisationDesactivee];    
    //CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    //boutonLocalisation.frame = frame;	// match the button's size with the image size
    [boutonLocalisation setTitle:@"" forState:UIControlStateNormal];        
    [boutonLocalisation setImage:image forState:UIControlStateNormal];
    boutonLocalisation.hidden = NO;
    // [image release];  
    
    UIImage *newImage = (self.statusPublicationTwitterLocal) ? [UIImage imageNamed:kImageTwitterActivee]:[UIImage imageNamed:kImageTwitterDesactivee];
    //CGRect newFrame = CGRectMake(0.0, 0.0, newImage.size.width, newImage.size.height);
    //boutonTwitter.frame = newFrame;	// match the button's size with the image size
    [boutonTwitter setTitle:@"" forState:UIControlStateNormal];        
    [boutonTwitter setImage:newImage forState:UIControlStateNormal];
    boutonTwitter.hidden = NO;
    //   [newImage release];  
    
    
    
    if (self.statusPublicationTwitterLocal)
    {
        if (self.statusPublicationGPSLocal)
        {
            [boutonPublier setTitle:kPublierTwitterEtLocalisation forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        }
        else
        {
            [boutonPublier setTitle:kPublierTwitter forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
    } 
    else
    {
        if (self.statusPublicationGPSLocal)
        {
            [boutonPublier setTitle:kPublierLocalisation forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
        else
        {
            [boutonPublier setTitle:kPublier forState:UIControlStateNormal];
            boutonPublier.titleLabel.font  = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        }
    }
    
}

- (IBAction)activeLocalisation:(id)sender
{
    DLog(@"");
    
    self.statusPublicationGPSLocal = (self.statusPublicationGPSLocal) ? NO : YES;
    
    
    if (self.statusPublicationGPSLocal)
    {
        
        
        BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
        BOOL locationAvailable =  ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized);
        
        if (!locationAllowed) 
        {            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation est désactivé." 
                                                            message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            
            self.statusPublicationGPSLocal = NO;
        }
        else 
        {
            if (!locationAvailable)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Le Service de Localisation de l'Application est désactivé." 
                                                                message:@"Rendez-vous dans les Réglages pour l'activer." 
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
                
                [alert show];
                self.statusPublicationGPSLocal = NO;
            }
            else
            {
                self.statusPublicationGPSLocal = YES;
            }
        }
        
    }
    
    [self gestionBouton];
}
- (IBAction)activeTwitter:(id)sender
{
    DLog(@"");
    
    self.statusPublicationTwitterLocal = (self.statusPublicationTwitterLocal) ? NO : YES;
    [self gestionBouton];
}

- (IBAction)backgroundTap:(id)sender {
    
	[textView resignFirstResponder];
	boutonValider.enabled = YES;
    
}

- (IBAction)gestionClavier {
    DLog(@"");
    
	
	if([textView isFirstResponder])
    {		
		[textView resignFirstResponder];
		boutonValider.enabled = YES;
	}
	else
    {
		boutonValider.enabled = NO;
		[textView becomeFirstResponder];
	}
}



#pragma mark -
#pragma mark dealloc


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {

    [super viewDidUnload];
    
    [self setMotdepasseTwitter:nil];
    [self setUserTwitter:nil];
    [self setUsername:nil];
    [self setPassword:nil];
    [self setTitreParent:nil];
    [self setIdEcranParent:nil];
    [self setIdEcran:nil];
    [self setIdBlog:nil];
    [self setCategorie:nil];
    [self setPseudoCategorieTitre:nil];
    [self setLabelTitre:nil];
    [self setTitre:nil];
    [self setTextView:nil];
    [self setBoutonTwitter:nil];
    [self setBoutonLocalisation:nil];
    [self setBoutonPublier:nil];
    [self setBoutonProposerExercice:nil];
    [self setBoutonCommenterExercice:nil];
    [self setLocationManager:nil];
    [self setCoordonneesGPS:nil];
    [self setIndicator:nil];
    [self setReceivedData:nil];
    [self setRessourceInfo:nil];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



#pragma mark -
#pragma mark Publication

-(void)publierMessageAvecLocalisation:(NSString *)aMessage {
	
    DLog(@"");
    
	NSMutableString *messageAvant = [[NSMutableString alloc] initWithString:textView.text];
	
	if ([messageAvant length] != 0)
    {		
		if (self.coordonneesGPS == nil)            
        {			
            
            switch ([CLLocationManager authorizationStatus]) {
                case kCLAuthorizationStatusNotDetermined:
                    DLog(@"kCLAuthorizationStatusNotDetermined");
                    break;
                case kCLAuthorizationStatusRestricted:
                    DLog(@"kCLAuthorizationStatusRestricted");
                    break;
                case kCLAuthorizationStatusDenied:
                    DLog(@"kCLAuthorizationStatusDenied");
                    break;
                case kCLAuthorizationStatusAuthorized:
                    DLog(@"kCLAuthorizationStatusAuthorized");
                    break;
                    
                default:
                    break;
            } 
            
            switch ([CLLocationManager locationServicesEnabled]) {
                case YES:
                    DLog(@"locationServicesEnabled");
                    break;
                case NO:
                    DLog(@"locationServicesDisabled");
                    break;
                    
                default:
                    break;
            } 
            
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
															message:@"a) Si vous avez accepté l'envoi des données de localisation, veuillez attendre le chargement des coordonnées GPS.\nb) Sinon, pour utiliser cette fonctionnalité, autorisez l'envoi des données de localisation."
														   delegate:self
												  cancelButtonTitle:@"Accepter"
												  otherButtonTitles:nil];
			
			[alert show];
		}		
		else
        {			
            
			NSString *message = [[NSString alloc] initWithFormat:@"%@</string><key>longitude</key><string>%3.5f</string><key>latitude</key><string>%3.5f",messageAvant,self.coordonneesGPS.coordinate.longitude,self.coordonneesGPS.coordinate.latitude];
			DLog(@"message is %@", message);
			[self publierMessage:message];
		}        
	}
	else
    {		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez composer un message avant de l'envoyer"
													   delegate:self
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];		
		[alert show];
	}
}

/*
 -(void)tweetBis:(NSString *)aMessage
 {
 DLog(@"");
 [SHK logoutOfAll];
 NSString * message =[[NSString alloc] initWithFormat:@"#ParlezVousChinois %@",aMessage];
 SHKItem *item=[SHKItem text:message];
 SHKActionSheet *actionSheet=[SHKActionSheet actionSheetForItem:item];
 [actionSheet showInView:self.view];    
 }
 */

/*
 -(void)tweetSansUser:(NSString *)aMessage
 {
 DLog(@"");
 
 [SHK logoutOfAll];
 NSString * message =[[NSString alloc] initWithFormat:@"#ParlezVousChinois %@",aMessage];
 SHKItem *item = [[SHKItem alloc] init];
 item.shareType = SHKShareTypeText;
 item.text = message;
 SHKActionSheet *actionSheet = [[SHKActionSheet alloc] initWithTitle:SHKLocalizedString(@"Share")
 delegate:self
 cancelButtonTitle:nil
 destructiveButtonTitle:nil
 otherButtonTitles:nil];
 actionSheet.item = [[[SHKItem alloc] init] autorelease];
 actionSheet.item.shareType = item.shareType;
 actionSheet.sharers = [NSMutableArray arrayWithCapacity:0];
 
 
 SHKTwitter * twit = [[SHKTwitter alloc] init];
 [SHKTwitter canShare];
 [actionSheet addButtonWithTitle:[SHKTwitter sharerTitle]];
 [actionSheet.sharers addObject:twit];
 
 actionSheet.item = item;    
 SHKSharer * controller = [SHKTwitter shareItem:item];
 // share and/or show UI
 //	[controller share];
 
 
 }
 */


-(void)tweet:(NSString *)aMessage
{
    DLog(@"");
    
    [SHK logoutOfAll];
    NSString * message =[[NSString alloc] initWithFormat:@"#SupCast %@",aMessage];
    SHKItem *item = [[SHKItem alloc] init];
    item.shareType = SHKShareTypeText;
    if ([message length] > 140)
        item.text = [message substringToIndex:139];
    else
        item.text = message;
    
    [SHKTwitter canShare];
    
}

-(void)creerBlog
{
    DLog(@"");
    if (self.idEcran)
    {    
        connectionStatus = 3;
        NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kaddBlogRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        NSString * blogtitle       = [[NSString alloc] initWithString:self.titre];
        NSString * blogbody        = [[NSString alloc] initWithString:self.titre];
        NSString * blogtags        = @"tags";
        NSString * access_id       = @"2";
        NSString * comments_select = @"select";
        
        NSString *paramDataString = [NSString stringWithFormat:
                                     @"id=%@&idressource=%@&titleparent=%@&idecranparent=%@&idecran=%@&pseudo=%@&password=%@&blogtitle=%@&blogbody=%@&blogtags=%@&access_id=%@&comments_select=%@",
                                     kKey,
                                     [self.ressourceInfo objectForKey:kRessourceElggidStr],
                                     self.titreParent,
                                     self.idEcranParent,
                                     self.idEcran,
                                     self.username,
                                     self.password,
                                     blogtitle,
                                     blogbody,
                                     blogtags,
                                     access_id,
                                     comments_select];
        
        DLog(@"paramDataString is %@",paramDataString);
        NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:paramData];
        
        connectionCreerBlog = nil;
        connectionCreerBlog = [[NSURLConnection alloc] initWithRequest:request delegate:self];;
        
        if(connectionCreerBlog) {
            
            //[indicator startAnimating];
            
            receivedDataCreerBlog = nil;
            receivedDataCreerBlog = [[NSMutableData alloc] init];
            //[dataRECU release];
            
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        
    }
    
}
-(void)publierMessage:(NSString *)aMessage {
	
	//Communication avec le serveur (envoyer message)
	if (!([self.idBlog isEqualToString:@""]) && (self.idBlog != nil))
    {
        
        
        NSString *message = [[NSString alloc] initWithFormat:@"<key>message</key><dict><key>contenu</key><string>%@</string></dict>",aMessage];
        
        NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kpostparidBlogRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        NSString *paramDataString = [NSString stringWithFormat:
                                     @"id=%@&idressource=%@&idBlog=%@&pseudo=%@&password=%@&message=%@",kKey,
                                     [self.ressourceInfo objectForKey:kRessourceElggidStr],self.idBlog,self.username,self.password,message];
        
        NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:paramData];
        
        connectionPublier = [[NSURLConnection alloc] initWithRequest:request delegate:self];;
        
        if(connectionPublier) {
            
            [indicator startAnimating];
            
            NSMutableData *data = [[NSMutableData alloc] init];
            self.receivedData = data;
            
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        
    }
}

#pragma mark -
#pragma mark NSURLConnection Callbacks delegate
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
    if (connectionCreerBlog == connection)
        [receivedDataCreerBlog setLength:0];
    else if (connectionPublier == connection)
        [receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connectionCreerBlog == connection) 
        [receivedDataCreerBlog appendData:data];
	else if (connectionPublier == connection)
        [receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	[indicator stopAnimating];	
	if (connectionCreerBlog == connection)
        receivedDataCreerBlog = nil;
    else if (connectionPublier == connection)
        self.receivedData = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
	
	[indicator stopAnimating];
    if (connectionCreerBlog == connection)
    {
        // creation blog
        NSString *error; 
        NSPropertyListFormat format;
        NSMutableArray * plist = [[NSMutableArray alloc] init];    
        plist = [NSPropertyListSerialization propertyListFromData:receivedDataCreerBlog
                                                 mutabilityOption:NSPropertyListImmutable
                                                           format:&format
                                                 errorDescription:&error]; 
        receivedDataCreerBlog = nil;
        if(!plist)
        { 
            DLog(@"Error: %@",error); 
        }
        else
        {
            DLog(@"plist is %@",plist);
            self.idBlog = [[plist objectAtIndex:0] objectForKey:@"blogid"];
            [indicator stopAnimating];
            [[NSNotificationCenter defaultCenter] postNotificationName:kBlogCreationNotification object:self.idBlog];
        }    
    }
    else
    {
    }
	//Changement d'Ecran
	
    if (connectionPublier == connection)
    {
        // Interception du retour de script : si le blog n'existe que dans SupCast_Blog et pas dans elgg_object_entity,
        // les messages vont se perdre dans la nature ; invisible sur elgg front office ; invisible dans l'app. Uniquement 
        // visibles dans la base de données.
        // Ça arrive quand on supprime un blog sur le front office d'elgg.
        
        NSString *error; 
        NSPropertyListFormat format;
        NSMutableArray * plist = [[NSMutableArray alloc] init];    
        plist = [NSPropertyListSerialization propertyListFromData:receivedData
                                                 mutabilityOption:NSPropertyListImmutable
                                                           format:&format
                                                 errorDescription:&error];
        receivedData = nil;
        
        //Si ça roule
        if(!plist)
        {
            DLog(@"Error: %@",error); 
        }
        //Sinon
        else
        {
            if([plist objectAtIndex:0])
            {
                DLog(@"Erreur de connexion : %@",plist);
            
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Erreur : idBlog=%@",idBlog] 
                                                            message:@"Le blog correspondant a été supprimé par le back-office de Elgg. Il faut également le supprimer dans CAPE_SUPCAST_BLOG" 
                                                           delegate:nil 
                                                  cancelButtonTitle:@"Je vais le supprimer tout de suite." 
                                                  otherButtonTitles:nil];
                [alert show];
            }
        }    
        [indicator stopAnimating];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark -
#pragma mark Core Location Delegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DLog(@"");
}
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    DLog(@"");
}
- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    DLog(@"");
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    DLog(@"");
}
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    DLog(@"");
}
- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    DLog(@"");
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    DLog(@"");    
	//self.coordonneesGPS = [NSString stringWithFormat:@"\n\nGPS : (%3.5f , %3.5f)",newLocation.coordinate.latitude,newLocation.coordinate.longitude];
	self.coordonneesGPS = newLocation;
	
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    DLog(@"");    
    
}

@end