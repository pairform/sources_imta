//
//  AbstractDataBuilder.m
//  SupCast
//
//  Created by Phetsana on 15/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "AbstractDataBuilder.h"


@implementation AbstractDataBuilder

- (NSArray *)buildData {
	[self doesNotRecognizeSelector:_cmd];
	return nil;
}

@end
