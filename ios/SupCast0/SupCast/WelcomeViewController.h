//
//  WelcomeViewController.h
//  SupCast
//
//  Created by fgutie10 on 28/06/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "InfoViewController.h"
#import "RootViewController.h"
#import "CreateUserViewController.h"
#import "UserAccountViewController.h"
#import "WBProgressHUD.h"


@interface WelcomeViewController : UIViewController <NSXMLParserDelegate,UIAlertViewDelegate> {  
    NetworkStatus   statusConnexionReseau;
	NSString *pseudoUtilisateur;
	NSString *motDePasseUtilisateur;
	NSMutableData *receivedData;                                
	int connectionStatus;
    int userAccount;// distinguish the login and gestion de compte
    IBOutlet UIImageView *logoImageView;
    WBProgressHUD   *HUD;
    NSURLConnection *welcomeConnection;
    
}

@property(nonatomic,strong) WBProgressHUD   *HUD;
@property NetworkStatus statusConnexionReseau;
@property(nonatomic) int connectionStatus;
@property (nonatomic, weak) IBOutlet UIButton *loginVisiteur;
@property (nonatomic, weak) IBOutlet UIButton *loginUtilisateur;
@property (nonatomic, weak) IBOutlet UIButton *gestionCompte;
@property (nonatomic, weak) IBOutlet UITextField *pseudoTextField;
@property (nonatomic, weak) IBOutlet UITextField *motDePasseTextField;
@property(nonatomic,strong) NSMutableData *receivedData;
@property(nonatomic,strong) NSMutableData *receivedDataInfos;
@property(nonatomic,strong) NSString *pseudoUtilisateur;
@property(nonatomic,strong) NSString *motDePasseUtilisateur;
@property(nonatomic,strong) NSURLConnection *welcomeConnection;
@property(nonatomic,strong) NSURLConnection *infosConnection;


-(NetworkStatus) etatConnexionReseau;

-(IBAction)lancerFormulaireCreationUtilisateur;
-(IBAction)lancerApplication;
-(IBAction)launchConnection;
-(IBAction)login:(id)sender;
-(IBAction)compteUtilisateur:(id)sender;
-(IBAction)textFieldDone:(id)sender;
-(IBAction)backgroundTap:(id)sender;
-(void)gestionDeCompte;
-(NSString *)dataFilePath:(NSString *)fileName;
-(void)changeScreen:(NSArray *)plist done:(BOOL)done;
-(void)showLoadingProgress;
-(void)hideLoadingProgress;
-(void)cancelConnection;

@end
