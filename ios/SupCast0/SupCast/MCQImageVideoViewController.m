//
//  MCQImageVideoViewController.m
//  SupCast
//
//  Created by Phetsana on 17/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
#import "GestionFichier.h"
#import "MCQImageVideoViewController.h"

@implementation MCQImageVideoViewController
//@synthesize dirRes;
/*
-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}
*/
- (void)viewDidLoad {

	[super viewDidLoad];
	indicationTableView.backgroundColor = [UIColor clearColor];
	indicationTableView.allowsSelection = NO;
	
	/* Calcul de position et de dimension */
	UIFont *captionFont =[UIFont fontWithName:kVerdana size:14];
	CGSize cs = [indication sizeWithFont:captionFont 
					   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
						   lineBreakMode:UILineBreakModeWordWrap];
	height = cs.height + 20; 
	
	indicationTableView.frame = CGRectMake(indicationTableView.frame.origin.x, 
										   indicationTableView.frame.origin.y, 
										   indicationTableView.frame.size.width, 
										   height + 22);
	
	imageView.frame = CGRectMake(imageView.frame.origin.x, 
								 height + 32, 
								 imageView.frame.size.width , 
								 imageView.frame.size.height);
	
	// Calcul pour cacher les cellules vides de la table
	float tmpHeight = 0;
	int numberOfSequence = [self tableView:tableView numberOfRowsInSection:0]; 
	NSString *answersChoice;
	for (int i = 0; i < numberOfSequence; i++) {
			
		answersChoice = [[[questionList objectAtIndex:indexQuestion] objectForKey:kAnswersChoice] objectAtIndex:i];
		UIFont *captionFont = [UIFont boldSystemFontOfSize:20];
		CGSize cs = [answersChoice sizeWithFont:captionFont 
							  constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
								  lineBreakMode:UILineBreakModeWordWrap];
				
		tmpHeight += (cs.height + 20);
	}
	
	tableView.frame = CGRectMake(tableView.frame.origin.x, 
								 imageView.frame.origin.y + imageView.frame.size.height + 10, 
								 tableView.frame.size.width, 
								 tmpHeight + 22);
	
	mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width,
											tableView.frame.origin.y + tableView.frame.size.height);
}

- (void)update {
	[super update];
//	imageView.image = [UIImage imageNamed:[[NSString alloc] initWithFormat:@"%@.%@", 
//										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageName],
//										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageExtension]]];
	imageView.image = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[[NSString alloc] initWithFormat:@"%@.%@", 
										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageName],
										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageExtension]]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark UITableViewDelegate

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	NSInteger result;
	
	if (aTableView == indicationTableView)
		result = 1;
	else
		result = [super tableView:aTableView numberOfRowsInSection:section];
	
	return result;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [super tableView:aTableView cellForRowAtIndexPath:indexPath];
	
	if (aTableView ==  indicationTableView) {
		cell.textLabel.font = [UIFont fontWithName:kVerdana size:14];
		cell.textLabel.text = indication;
	}
	
	return cell;        	
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat result = [super tableView:aTableView heightForRowAtIndexPath:indexPath];
	
	if (aTableView == indicationTableView)
		result = height;
	
	return result;
}

- (void)dealloc {
	[imageView release];
	[indicationTableView release];
    [super dealloc];
}

@end
