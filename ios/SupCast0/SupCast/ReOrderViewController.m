//
//  ReOrderViewController.m
//  SupCast
//
//  Created by Phetsana on 08/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ReOrderViewController.h"
#import "CustomPicker.h"
#import "CustomView.h"

@implementation ReOrderViewController
//@synthesize dirRes;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	indicationTableView.backgroundColor = [UIColor clearColor];
	
	type = [questionDict objectForKey:kType];
	
	userAnswerString = [[NSMutableString alloc] initWithString:@""];
	
	userAnswerList = [[NSMutableArray alloc] init];
	indexOfObjectUsed = [[NSMutableArray alloc] init];
	
	if ([[[questionDict objectForKey:kQuestionsList] objectAtIndex:indexQuestion] objectForKey:kIndication] != nil) {
		indication = [[[questionDict objectForKey:kQuestionsList] objectAtIndex:indexQuestion] objectForKey:kIndication];
	}
}

- (CGRect)pickerFrameWithSize:(CGSize)size {
	CGRect pickerRect = CGRectMake(0.0,
								   156,
								   size.width,
								   size.height);
	return pickerRect;
}

- (void)update {
	[super update];
	[userAnswerString setString:@""];
	[indicationTableView reloadData];
	[indexOfObjectUsed removeAllObjects];
	[used removeAllObjects];
	[userAnswerList removeAllObjects];
	
	if ([[[questionDict objectForKey:kQuestionsList] objectAtIndex:indexQuestion] objectForKey:kIndication] != nil) {
		indication = [[[questionDict objectForKey:kQuestionsList] objectAtIndex:indexQuestion] objectForKey:kIndication];
	}
}

- (void)createCustomPicker {
	[super createCustomPicker];
	customPickerView = [[CustomPicker alloc] initWithFrame:CGRectZero 
													array1:array1
													array2:array2
										   array1Displayed:array1Displayed
										   array2Displayed:array2Displayed
										numberOfComponents:1];
	customPickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	
	// note we are using CGRectZero for the dimensions of our picker view,
	// this is because picker views have a built in optimum size,
	// you just need to set the correct origin in your view.
	//
	// position the picker at the bottom
	CGSize pickerSize = [pickerView sizeThatFits:CGSizeZero];
	customPickerView.frame = [self pickerFrameWithSize:pickerSize];
	
	customPickerView.showsSelectionIndicator = YES;
	
	// add this picker to our view controller, initially hidden
	customPickerView.hidden = YES;
	[self.view addSubview:customPickerView];
}

/* 
	- On concatène l'occurrence sélectionnée à la chaîne de caractère de réponse 
 et on met à jour l'affichage de la table
	- Une occurrence utilisée est grisée et n'est plus utilisable
 */
- (IBAction)associate {
	if (canAnswer) {
		NSString *element = [customPickerView.array2Displayed objectAtIndex:[customPickerView selectedRowInComponent:0]];
		
		if (((CustomView *)[customPickerView.pickerView2 objectAtIndex:[customPickerView selectedRowInComponent:0]]).used) {
			if (!isPromptUsed) {
				isPromptUsed = YES;
				[NSTimer scheduledTimerWithTimeInterval:(2.0)
												 target:self
											   selector:@selector(alert)
											   userInfo:nil
												repeats:NO];
				customPickerView.frame = CGRectMake(initialPosition.x,
													initialPosition.y - PROMPT_SIZE, 
													customPickerView.frame.size.width, 
													customPickerView.frame.size.height);
				self.navigationItem.prompt = @"Mot déjà associé !";
				return;
			}
		} else {
			
			self.navigationItem.prompt = nil;
			
			[used addObject:element];
			[indexOfObjectUsed addObject:[NSNumber numberWithInt:[customPickerView selectedRowInComponent:0]]];
			
			if ([type compare:@"letter"] == 0) {
				[userAnswerString appendString:[customPickerView.array2Displayed objectAtIndex:[customPickerView selectedRowInComponent:0]]];
				[userAnswerList addObject:[customPickerView.array2Displayed objectAtIndex:[customPickerView selectedRowInComponent:0]]];
			} else if ([type compare:@"word"] == 0) {
				[userAnswerList addObject:[customPickerView.array2Displayed objectAtIndex:[customPickerView selectedRowInComponent:0]]];
				[userAnswerString appendFormat:@"  %@", [customPickerView.array2Displayed objectAtIndex:[customPickerView selectedRowInComponent:0]]];
			}
			
			[indicationTableView reloadData];
			
			[customPickerView updateElement1:(int)nil
								   elements2:[customPickerView selectedRowInComponent:0] 
									withBOOL:YES];
		}
	} 
}

/*
	- On fait la comparaison entre le tableau des réponses utilisateur et celui des vraies réponses
	- On ne garde que les réponses justes jusqu'à la première erreur
	- On rajoute une occurrence supplémentaire (indice)
	- On met à jour la roulette
 */
- (IBAction)indice {
	if (canAnswer) {
		NSArray *answerParts = customPickerView.array2;
		
		int test = 0;
		for (int i = 0; i < [userAnswerList count]; i++) {
			if ([[userAnswerList objectAtIndex:i] isEqualToString:[answerParts objectAtIndex:i]]) {
				test++;
			} else {
				break;
			}
		}
		
		test = (test > [answerParts count])?([answerParts count] - 1):test;
		
		if (test < [answerParts count]) {
			for (id elem in indexOfObjectUsed) {
				[customPickerView updateElement1:(-1) elements2:[elem intValue] withBOOL:NO];
			}
			[indexOfObjectUsed removeAllObjects];
			
			[userAnswerList removeAllObjects];
			NSString *stringToKeep = @"";
			for (int i = 0; i < test + 1; i++) {
				if ([type isEqualToString:@"letter"]) {
					stringToKeep = [[NSString alloc] initWithFormat:@"%@%@", stringToKeep, [answerParts objectAtIndex:i]];
				}
				else if ([type isEqualToString:@"word"]) {
					stringToKeep = [[NSString alloc] initWithFormat:@"%@ %@", stringToKeep, [answerParts objectAtIndex:i]];
				}
				[used addObject:[answerParts objectAtIndex:i]];
				[userAnswerList addObject:[answerParts objectAtIndex:i]];
			}
			
			for (id elem in userAnswerList) {
				int indexOfObject = [customPickerView.array2Displayed indexOfObject:elem];
				
				if (![indexOfObjectUsed containsObject:[NSNumber numberWithInt:indexOfObject]]) {
					[indexOfObjectUsed addObject:[NSNumber numberWithInt:indexOfObject]];
				} else {
					NSRange range;
					range.location = indexOfObject + 1;
					range.length = [customPickerView.array2Displayed count] - (indexOfObject + 1);
					indexOfObject = [customPickerView.array2Displayed indexOfObject:elem inRange:range];
					if (indexOfObject < [customPickerView.array2Displayed count]) {
						[indexOfObjectUsed addObject:[NSNumber numberWithInt:indexOfObject]];
					}
				}
			}				
			
			for (id a in indexOfObjectUsed) {
				[customPickerView updateElement1:(-1) elements2:[a intValue] withBOOL:YES];
			}
			
			[userAnswerString setString:stringToKeep];
			[indicationTableView reloadData];
		}
	}	
}

/*
	- On fait une comparaison de tableaux
	- On met à jour la roulette
 */
- (IBAction)checkUserAnswer {
	if (canAnswer && [userAnswerList count] > 0) {
		BOOL test = TRUE;
		
		if ([userAnswerList count] < [customPickerView.array2 count]) {
			test = FALSE;
		} else {
			for (int i = 0; i < [userAnswerList count]; i++) {
				if (![[userAnswerList objectAtIndex:i] isEqualToString:[customPickerView.array2 objectAtIndex:i]]) {
					test = FALSE;
					break;
				}
			}
		}
		
		if (test) {
			alertRightAnswer = [[UIAlertView alloc] initWithTitle:@"" 
														  message:rightAnswer
														 delegate:self 
												cancelButtonTitle:kAccepter 
												otherButtonTitles: nil];
			[self removeQuestionFromList:indexQuestion];
			[alertRightAnswer show];	
			[alertRightAnswer release];
		} else {
			UIAlertView *alertWrongAnswer = [[UIAlertView alloc] initWithTitle:@"" 
																	   message:wrongAnswer
																	  delegate:self 
															 cancelButtonTitle:kAccepter 
															 otherButtonTitles: nil];
			[alertWrongAnswer show];	
			[alertWrongAnswer release];
			[self indice];
			[self deleteLastChar];
		}
	}
}

/*
	- On enlève la dernière occurrence ajoutée dans le tableau des réponses utilisateur
	- On met à jour la chaîne de caractère de réponse et la roulette
 */
- (IBAction)deleteLastChar {
	if (canAnswer && [indexOfObjectUsed count] > 0 && [userAnswerList count] > 0) {
		NSString *stringToKeep;
		
		NSString *lastObject = [userAnswerList objectAtIndex:([userAnswerList count] - 1)];
		
		[userAnswerList removeLastObject];
		
		stringToKeep = @"";
		for (int i = 0; i < [userAnswerList count]; i++) {
			if ([type isEqualToString:@"letter"]) {
				stringToKeep = [[NSString alloc] initWithFormat:@"%@%@", stringToKeep, [userAnswerList objectAtIndex:i]];
			} else if ([type isEqualToString:@"word"]) {
				stringToKeep = [[NSString alloc] initWithFormat:@"%@ %@", stringToKeep, [userAnswerList objectAtIndex:i]];
			}
		}
		
		int indexOfObject = [customPickerView.array2Displayed indexOfObject:lastObject];
		
		if (!((CustomView *)[customPickerView.pickerView2 objectAtIndex:indexOfObject]).used) {
			do {
				NSRange range;
				range.location = indexOfObject + 1;
				range.length = [customPickerView.array2Displayed count] - (indexOfObject + 1);
				indexOfObject = [customPickerView.array2Displayed indexOfObject:lastObject inRange:range];
				if (indexOfObject < [customPickerView.array2Displayed count])
					break;
			} while (!((CustomView *)[customPickerView.pickerView2 objectAtIndex:indexOfObject]).used);
		} 			
		
		if (indexOfObject < [customPickerView.array2 count]) {
			[indexOfObjectUsed removeObject:[NSNumber numberWithInt:indexOfObject]];
			[customPickerView updateElement1:(-1) elements2:indexOfObject withBOOL:NO];
		}
		
		[userAnswerString setString:stringToKeep];
		[indicationTableView reloadData];
	}
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Set up the cell...
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	
	if (indexPath.section == 0) {
		cell.textLabel.font = [UIFont fontWithName:kVerdana size:14];
		cell.textLabel.text = indication;
	} else {
		cell.textLabel.text = userAnswerString;
	}
	
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewCellEditingStyleNone;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat result;
	
	if (aTableView == indicationTableView && indexPath.section == 0) {
		UIFont *captionFont =[UIFont fontWithName:kVerdana size:14];
		CGSize cs = [indication sizeWithFont:captionFont 
						   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
							   lineBreakMode:UILineBreakModeWordWrap];
		result = cs.height + 40;
	} else {
		result = 60;
	}
	
	return result;
}


- (void)dealloc {
	[indexOfObjectUsed release];
	[userAnswerString release];
	[userAnswerList release];
	[indicationTableView release];
	[type release];
    [super dealloc];
}


@end
