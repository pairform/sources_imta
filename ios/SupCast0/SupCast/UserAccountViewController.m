//
//  UserAccountViewController.m
//  SupCast
//
//  Created by wbao11 on 12/07/11.
//  Copyright (c) 2011 EMN - CAPE. All rights reserved.
//

#import "UserAccountViewController.h"

@implementation UserAccountViewController

@synthesize tabBarController;
@synthesize userInfosViewController;
@synthesize changePasswordViewController;
@synthesize username;
@synthesize password;
@synthesize tabBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    DLog(@"");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewWillAppear:(BOOL)animated
{
    DLog(@"");
    userInfosViewController.tableView.frame = CGRectMake(userInfosViewController.tableView.frame.origin.x, userInfosViewController.tableView.frame.origin.y, 10.0, 10.0);
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    DLog(@"");
    [self resignFirstResponder];
}

- (void)viewDidLoad
{
    
    DLog(@"");
    [super viewDidLoad];

    changePasswordViewController.username = self.username;
    changePasswordViewController.password = self.password;
    
    [self.view addSubview:tabBarController.view];
    tabBarController.tabBar.hidden = NO;
    [tabBarController shouldAutorotateToInterfaceOrientation:YES];
       
}

- (void)viewDidUnload
{
    DLog(@"");
    compteBarTab = nil;
    motdepasseBarTab = nil;
    [self setTabBarController:nil];
    [self setUserInfosViewController:nil];
    [self setChangePasswordViewController:nil];
    [self setTabBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    DLog(@"");
}


-(void)didRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    DLog(@"");
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{

    DLog(@"");
    // Return YES for supported orientations
       
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {  
        
        CGRect myFrame = CGRectMake(self.view.frame.origin.x,
                                    self.view.frame.origin.y,
                                    self.view.frame.size.width,
                                    self.view.frame.size.height - self.navigationController.toolbar.frame.size.height - self.tabBar.frame.size.height);
        [userInfosViewController.view setFrame:myFrame];
        [userInfosViewController.view setFrame:myFrame];
        [changePasswordViewController.view setFrame:myFrame];
        tabBarController.tabBar.frame = CGRectMake(0, myFrame.size.height-4, self.view.frame.size.width, 49);
       
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {   
        if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            self.view.frame = CGRectMake(0, 20, 768, 1024);
            self.userInfosViewController.view.frame = CGRectMake(0, 20, 768, 960);
            self.changePasswordViewController.view.frame = CGRectMake(0, 20, 768, 960);
            tabBarController.tabBar.frame = CGRectMake(0, 975, 768, 49);
        }
        if (interfaceOrientation == UIInterfaceOrientationPortrait)
        {
            self.view.frame = CGRectMake(0, 20, 768, 1024);
            self.userInfosViewController.view.frame = CGRectMake(0, 20, 768, 960);
            self.changePasswordViewController.view.frame = CGRectMake(0, 20, 768, 960);
            tabBarController.tabBar.frame = CGRectMake(0, 975, 768, 49);
        }
        
        if ( (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            self.view.frame = CGRectMake(0, 20, 1024, 768);
            self.userInfosViewController.view.frame = CGRectMake(0, 20, 1024, 719);
            self.changePasswordViewController.view.frame = CGRectMake(0, 20, 1024, 719);
            tabBarController.tabBar.frame = CGRectMake(0, 719, 1024, 49);
        }

        
        
    }
    
    return YES;    

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


@end
