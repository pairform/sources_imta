//
//  GestionReseauSocialViewController.h
//  SupCast
//
//  Created by Didier PUTMAN on 02/09/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCommentViewController.h"
#import "RecupererMessagesViewController.h"

@interface GestionReseauSocialViewController : UIViewController {
    BOOL estVisiteur;
    NSString * titre;
    NSString * idBlog;
	NSString * titreParent;
	NSString * idEcranParent;
	NSString * idEcran;
	NSString * username;
	NSString * password;
    NSString * categorie;
    int connectionStatus;
    NSString * currentElementName;
	NSMutableString * currentText;
    NSURLConnection * connectionGetBlog;
    NSURLConnection * connectionNewMessages;
    NSMutableDictionary * dictionnaire;
    NSSet * interestingTags;
	IBOutlet UITableView * tableView;
    NSMutableData *receivedDataGetBlog;
    NSMutableData *receivedDataNewMessages;
	NSDictionary *ressourceInfo;
    NSDictionary *pcrDictionary;
}
@property(nonatomic,strong) NSDictionary *ressourceInfo;
@property(nonatomic,strong) NSDictionary *pcrDictionary;
@property(nonatomic) BOOL  estVisiteur;
@property(nonatomic,strong) NSString * titre;
@property(nonatomic,strong) NSString * idBlog;
@property(nonatomic,strong) NSString * titreParent;
@property(nonatomic,strong) NSString * idEcranParent;
@property(nonatomic,strong) NSString * idEcran;
@property(nonatomic,strong) NSString * username;
@property(nonatomic,strong) NSString * password;
@property(nonatomic,strong) NSString * categorie;
@property(nonatomic,strong) NSURLConnection * connectionGetBlog;
@property(nonatomic,strong) NSURLConnection * connectionNewMessages;
@property(nonatomic,strong) NSMutableData *receivedDataGetBlog;
@property(nonatomic,strong) NSMutableData *receivedDataNewMessages;
@property(nonatomic,strong) NSMutableArray *nvxMessage;
@property(nonatomic,strong) NSArray * plistNiveauRessource;
@property(nonatomic,strong) NSMutableData *receivedDataNiveauRessource;
@property(nonatomic,strong) NSURLConnection *connectionNiveauRessource;
-(void)lancerRecuperation;
-(void)recupNewMessages;
-(void)analyzeMessage:(NSArray *)plist;
-(void)recupIdBlog:(NSNotification *)notification;

@end
