//
//  TableContentsViewController.m
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "TableContentsViewController.h"
#import "GestionFichier.h"

@implementation TableContentsViewController
@synthesize dirRes;
@synthesize titre;
@synthesize contentList;
@synthesize firstPieceView;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil
		  contentList:(NSArray *)list
		   indication:(NSString *)indic
{
   	DLog(@"");
	
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
		self.title = @"";
		contentList = list;
		indication = indic;
		
		
    }
	
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	DLog(@"");
	
    [super viewDidLoad];
	// Configuration du scroll
	[mainScrollView setCanCancelContentTouches:NO];
	mainScrollView.clipsToBounds = YES;	
	mainScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	[mainScrollView addSubview:mainView];
	[mainScrollView setContentSize:CGSizeMake(mainView.frame.size.width, mainView.frame.size.height)];
	[mainScrollView setScrollEnabled:YES];
	[mainScrollView release];
	
}

- (void)viewWillDisappear:(BOOL)animated {
	DLog(@"");
}

- (void)didReceiveMemoryWarning {
	DLog(@"");
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	DLog(@"");
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
	DLog(@"");
	[contentList release];
	[indication release];
	[toolBar release];
	
    [dirRes release];
	[super dealloc];
	
}

- (UIImage *)resizeImage:(UIImage *)sourceImage maxHeight:(CGFloat)maxHeight maxWidth:(CGFloat)maxWidth {
	DLog(@"");
	CGFloat width = sourceImage.size.width;
	CGFloat height = sourceImage.size.height;
	
	if (width < maxWidth && height < maxHeight) {
		return sourceImage;
	}
	
	CGFloat ratio = width / height;
	
	if (width > maxWidth) 
	{
		width = maxWidth;
		height = width / ratio;
	}
	
	if (height > maxHeight)  
	{
		height = maxHeight;
		width = height * ratio;
	}
	
	CGSize size = CGSizeMake(width, height);
	
	UIGraphicsBeginImageContext(size);
	[sourceImage drawInRect:CGRectMake(0.0f, 0.0f, width, height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return newImage;
}
- (UIImage *)resizeImage:(UIImage *)sourceImage max:(CGFloat)maximum {
	DLog(@"");
	CGFloat width = sourceImage.size.width;
	CGFloat height = sourceImage.size.height;
	
	if (width < maximum && height < maximum) {
		return sourceImage;
	}
	
	CGFloat ratio = width / height;
	
	if (width > height) {
		width = maximum;
		height = width / ratio;
	} else {
		height = maximum;
		width = height * ratio;
	}
	
	CGSize size = CGSizeMake(width, height);
	
	UIGraphicsBeginImageContext(size);
	[sourceImage drawInRect:CGRectMake(0.0f, 0.0f, width, height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
	return newImage;
}

// Handles the start of a touch
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	DLog(@"");
	NSUInteger numTaps = [[touches anyObject] tapCount];
	if(numTaps >= 2) {
	} else {
	}
	// Enumerate through all the touch objects.
	NSUInteger touchCount = 0;
	for (UITouch *touch in touches) {
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		[self dispatchFirstTouchAtPoint:[touch locationInView:mainView] forEvent:nil];
		touchCount++;  
	}	
}

// Checks to see which view, or views, the point is in and then calls a method to perform the opening animation,
// which  makes the piece slightly larger, as if it is being picked up by the user.
-(void)dispatchFirstTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event
{
	DLog(@"");
	if (CGRectContainsPoint([firstPieceView frame], touchPoint)) {
		[self animateFirstTouchAtPoint:touchPoint forView:firstPieceView];
	}
	
}

// Handles the continuation of a touch.
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{  
	
	DLog(@"");
	NSUInteger touchCount = 0;
	// Enumerates through all touch objects
	for (UITouch *touch in touches) {
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		[self dispatchTouchEvent:[touch view] toPosition:[touch locationInView:mainView]];
		touchCount++;
	}
	
	// When multiple touches, report the number of touches. 
	if (touchCount > 1) {
	} else {
	}
}

// Checks to see which view, or views, the point is in and then sets the center of each moved view to the new postion.
// If views are directly on top of each other, they move together.
-(void)dispatchTouchEvent:(UIView *)theView toPosition:(CGPoint)position
{
	DLog(@"");
	// Check to see which view, or views,  the point is in and then move to that position.
	if (CGRectContainsPoint([firstPieceView frame], position)) {
		firstPieceView.center = position;
	} 
}

// Handles the end of a touch event.
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	DLog(@"");
    // Enumerates through all touch object
	for (UITouch *touch in touches) {
		// Sends to the dispatch method, which will make sure the appropriate subview is acted upon
		[self dispatchTouchEndEvent:[touch view] toPosition:[touch locationInView:mainView]];
	}
}

// Checks to see which view, or views,  the point is in and then calls a method to perform the closing animation,
// which is to return the piece to its original size, as if it is being put down by the user.
-(void)dispatchTouchEndEvent:(UIView *)theView toPosition:(CGPoint)position
{   
	DLog(@"");

	// Check to see which view, or views,  the point is in and then animate to that position.
	if (CGRectContainsPoint([firstPieceView frame], position)) {
		[self animateView:firstPieceView toPosition: position];
	} 
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	DLog(@"");

	// Enumerates through all touch object
	for (UITouch *touch in touches) {
		// Sends to the dispatch method, which will make sure the appropriate subview is acted upon
		[self dispatchTouchEndEvent:[touch view] toPosition:[touch locationInView:mainView]];
	}
}

#pragma mark -
#pragma mark === Animating subviews ===
#pragma mark

// Scales up a view slightly which makes the piece slightly larger, as if it is being picked up by the user.
-(void)animateFirstTouchAtPoint:(CGPoint)touchPoint forView:(UIImageView *)theView 
{
	DLog(@"");
	// Pulse the view by scaling up, then move the view to under the finger.
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:GROW_ANIMATION_DURATION_SECONDS];
	theView.transform = CGAffineTransformMakeScale(1.2, 1.2);
	[UIView commitAnimations];
}

// Scales down the view and moves it to the new position. 
-(void)animateView:(UIView *)theView toPosition:(CGPoint)thePosition
{
	DLog(@"");
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:SHRINK_ANIMATION_DURATION_SECONDS];
	// Set the center to the final postion
	theView.center = thePosition;
	// Set the transform back to the identity, thus undoing the previous scaling effect.
	theView.transform = CGAffineTransformIdentity;
	[UIView commitAnimations];	
}


@end
