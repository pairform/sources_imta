//
//  WebViewController.h
//  SupCast
//
//  Created by admin on 22/11/10.
//  Copyright 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface WebViewController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *aWebView;
	NSString * myLink;
	
	
}

@property (nonatomic, retain)  UIWebView * aWebView;
@property (nonatomic, retain)  NSString * myLink;


- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil 
				 link:(NSString *)link;
@end
