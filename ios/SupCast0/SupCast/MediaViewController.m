//
//  MediaViewController.m
//  LecteurSupCast
//
//  Created by Didier PUTMAN on 04/12/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//
#import "GestionFichier.h"
#import "MediaViewController.h"
#import "AVFoundation/AVAudioPlayer.h"
#import "MediaPlayer/MediaPlayer.h"

@implementation MediaViewController
@synthesize donneesFichier;
@synthesize nomFile;
@synthesize extensionFile;
@synthesize player;
@synthesize mMoviePlayer;
@synthesize dirRes;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil
			   bundle:(NSBundle *)nibBundleOrNil
				 data:(NSData *)data
				  nom:(NSString *)nom
			extension:(NSString *)extension 
{
    if ((self = [super initWithNibName:nibNameOrNil 
								bundle:nibBundleOrNil])) {
        // Custom initialization
		self.donneesFichier= data;
		self.nomFile= nom;
		self.title= nom;
		self.extensionFile= extension;
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	if ([extensionFile isEqualToString:@"gif"]) 
	{
		UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,320)];
		imageView.image = [UIImage imageWithData:self.donneesFichier]; 
		[self.view addSubview:imageView];
		[imageView release];
	}

	if ([extensionFile isEqualToString:@"mp4"]) 
	{
		// Initialisation de AVAudioPlayer
		MPMoviePlayerController* tmpMoviePlayer;
		NSString * fileName = [[NSString alloc] initWithFormat:@"%@.%@",nomFile,extensionFile];
		
		

		NSString * filePath = [[[GestionFichier alloc] autorelease] dataFilePath:fileName];
		
		[[NSFileManager defaultManager] createFileAtPath:filePath contents:donneesFichier attributes:nil]; 
		NSURL * url = [NSURL fileURLWithPath:filePath];
		
		tmpMoviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
		self.mMoviePlayer = tmpMoviePlayer;
		[tmpMoviePlayer release];
		[mMoviePlayer play];
		
	}

	if ([extensionFile isEqualToString:@"mp3"]) 
	{	
		// Initialisation de AVAudioPlayer
		AVAudioPlayer* tmpPlayer;
		tmpPlayer = [[AVAudioPlayer alloc] initWithData:donneesFichier error:nil];
		
		self.player = tmpPlayer;
		[tmpPlayer release];
		[player play];
	}
	
}

/*
-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}
*/
 
// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
