//
//  DialogCell.h
//  SupCast
//
//  Created by Phetsana on 15/07/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DialogCell : UITableViewCell {
	UILabel *dialogLabel;
	UILabel *dialogAnswerLabel;
	NSMutableArray *textFieldList;
	NSMutableArray *labelList;
	NSString *s;
}

@property (nonatomic, retain) UILabel *dialogLabel;
@property (nonatomic, retain) UILabel *dialogAnswerLabel;

- (id)initWithFrame:(CGRect)frame 
	reuseIdentifier:(NSString *)reuseIdentifier 
			 string:(NSString *)aString 
	  numberOfField:(int)n;

+ (CGFloat)cellHeightForCaption:(NSString *)text 
						  width:(CGFloat)width;

@end
