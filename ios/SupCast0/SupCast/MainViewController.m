//
//  MainViewController.m
//  SupCast
//
//  Created by Phetsana on 05/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "MainViewController.h"

@implementation MainViewController
@synthesize listeDesItems;
@synthesize dicoRacine;
@synthesize selectedRowIndexPath;

- (void)viewDidLoad 
{
    DLog(@"");	
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    DLog(@"");	
    [super viewWillAppear:animated];
	// On déselectionne les précédentes sélections
   // NSIndexPath *tableSelection = [tableView indexPathForSelectedRow];
	//[tableView deselectRowAtIndexPath:tableSelection animated:NO];
    [tableView reloadData];
    
    [[tableView cellForRowAtIndexPath:self.selectedRowIndexPath] setSelected:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    
    return [listeDesItems count];
}

- (void)configureCellMenu:(MainViewCell *)cell forIndexPath:(NSIndexPath *)indexPath 
{
	//cell.titleLabel.text = [listeDesItems objectAtIndex:indexPath.row] ;
    cell.detailTextLabel.text = [listeDesItems objectAtIndex:indexPath.row] ;    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	cell.textLabel.textColor = [UIColor blackColor];
	cell.textLabel.text = [listeDesItems objectAtIndex:indexPath.row];
	cell.imageView.image = nil;
    cell.accessoryView = nil;
    
    if([cell.contentView viewWithTag:tagBulles] != nil)
    {
        [[cell.contentView viewWithTag:tagBulles] removeFromSuperview];
    }
    //On calcule le nombre de message à l'intérieur
    NSDictionary * dico1 = [[NSDictionary alloc] initWithDictionary:[[self.dicoRacine objectForKey:@"sequenceContent"] objectAtIndex:indexPath.row]];
    NSString *idEcranCell   = [[NSString alloc] initWithString:[dico1 objectForKey:kIdecran]];
    
    DLog(@"_______________________________________________________");
    DLog(@"Id de la ligne : %@",idEcranCell);
    
    NSMutableString * nombreDeMessages =  [[NSMutableString alloc] init];
    
    NSArray *listIdEcran=[[NSArray alloc] initWithArray:[self.pcrDictionary objectForKey:idEcranCell]];
    
    //Affichage ou non de la flèche
    
    DLog(@"Nombre d'enfants : %d",[listIdEcran count]);
    
    if ([listIdEcran count] > 1)
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else cell.accessoryType = UITableViewCellAccessoryNone;
    
    //Méthode pour récuperer les messages de la cellule et de ses enfants (comme dans PVC) 
    
    int showNumberMsg=0;
    
    if (self.nvxMessage == nil) 
    {
        DLog(@"Pas de message correspondant à la ligne.");   
    }
    else
    {   
        for (id idecran in listIdEcran)
        {
            for (id element_news in self.nvxMessage)
            {
                if([[element_news objectForKey:@"idecran"] isEqualToString:idecran] &&
                   [[element_news objectForKey:@"nombreNouveauMessage"] intValue]>0)
                {
                    showNumberMsg += [[element_news objectForKey:@"nombreNouveauMessage"] intValue];
                }
            }
        }
        if(showNumberMsg != 0)
        {
            nombreDeMessages = [NSString stringWithFormat:@"%d",showNumberMsg];

            DLog(@"Nombre de messages = %@, ID de la ligne = %@ , Index de la ligne = %@", nombreDeMessages, idEcranCell, indexPath);
            
            //On créé une bulle rouge
            //cell.textLabel.shadowColor = [UIColor lightGrayColor];
            
            UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage * image;
            
            image = [UIImage imageNamed:kBulleBarreRedMiroir];
            
            [aButton1 setTitle:nombreDeMessages forState:UIControlStateNormal];
            [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            aButton1.titleLabel.font=[UIFont systemFontOfSize:13];    
            aButton1.alpha = 0.8;
            
            //Framing du bouton
            CGRect frame = CGRectMake(10.0, 0.0, 32.0, 32.0);   
            frame.origin.y =  (cell.frame.size.height - frame.size.height) / 2;
            aButton1.frame=frame;
            
            /*Reframing de la cellule
            CGRect frameTextCell = cell.textLabel.frame;
            frameTextCell.size.width -= 40;
            frameTextCell.origin.x += 40;
            cell.textLabel.frame = frameTextCell;
            */
            
            [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
            
            //On lui attribue un tag pour le supprimer lors de la réutilisation de la ligne
            aButton1.tag = tagBulles;
            
            //Et on le cale en subview de la celulle.
            [cell.contentView addSubview:aButton1];            
        }
    }
    
    
    //Méthode pour récuperer les messages de la cellule UNIQUEMENT (pas les messages des ses enfants).
    /*
    if (self.nvxMessage == nil) 
    {
        DLog(@"Pas de message correspondant à la ligne.");   
    }
    else
    {           
        //Calcul du nombre de nouveaux messages pour afficher dans la bulle
        //On parcours la liste des nouveaux messages
        for (NSDictionary *NewMsg in self.nvxMessage)
        {   
            //Si le message correspond à l'id d'écran de la cellule, ET que le nbre de nouveaux messages est strictement supérieur à 0
            if([[NewMsg objectForKey:@"idecran"] isEqualToString:idEcranCell] && 
               [[NewMsg objectForKey:@"nombreNouveauMessage"] intValue]>0        )
            {
                //On récupère le nombre de nvx messages
                nombreDeMessages = [NewMsg objectForKey:@"nombreNouveauMessage"];
                DLog(@"Nombre de messages = %@, ID de la ligne = %@ , Index de la ligne = %@", nombreDeMessages, idEcranCell, indexPath);
                
                //On créé une bulle rouge
                cell.textLabel.textColor = [UIColor redColor];
                
                UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
                UIImage * image;
                
                image = [UIImage imageNamed:kBulleBarreRed];
                [aButton1 setTitle:nombreDeMessages forState:UIControlStateNormal];
                [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                aButton1.titleLabel.font=[UIFont systemFontOfSize:13];    

                CGRect frame = CGRectMake(20.0, 0.0, 32.0, 32.0);            
                aButton1.frame=frame;
                [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
                
                aButton1.showsTouchWhenHighlighted = YES;                
                
                //Et on l'attribute à la cellule.
                cell.accessoryView = aButton1;
                
                return cell;
            }
        }
    }
     */
    return cell;
	
}

- (void)tableView:(UITableView *)aTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	//[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	self.selectedRowIndexPath = indexPath;

    NSDictionary *  moduleContent = [[NSDictionary alloc] initWithDictionary:[[self.dicoRacine objectForKey:@"sequenceContent"] objectAtIndex:indexPath.row]];
    
	if(self.estVisiteur) 
	{
        if ( moduleContent != nil ) 
        {
            if ( ( [moduleContent objectForKey:@"listeDesSequences"] != nil)  && ([[moduleContent objectForKey:@"listeDesSequences"] count] != 0)) 
            {
				// Appel ModuleViewController
				ModuleViewController *moduleViewController = [ModuleViewController alloc];
				// infos ressources
				moduleViewController.estVisiteur   = self.estVisiteur;
				moduleViewController.titre         = [listeDesItems objectAtIndex:indexPath.row]; 
				moduleViewController.username      = @"";
				moduleViewController.titreParent   = self.titreParent;
				moduleViewController.idEcranParent = self.idEcranParent;
				moduleViewController.idEcran       = self.idEcran;
				moduleViewController.idBlog        = @"";
				moduleViewController.password      = @"";
				moduleViewController.categorie     = self.categorie;
				moduleViewController.ressourceInfo = self.ressourceInfo;
                moduleViewController.pcrDictionary = self.pcrDictionary;
				// infos ressources
                moduleViewController.dicoRacine    = self.dicoRacine;
				moduleViewController.moduleContent = moduleContent;
				// titre
				moduleViewController.title         = [listeDesItems objectAtIndex:indexPath.row]; 
				// init
				(void)[moduleViewController initWithNibName:@"ModuleView" 
											   bundle:nil];
				
				// push
				[self.navigationController pushViewController:moduleViewController animated:YES];
            }		
            else 
            {
                // Appel WebViewContentsViewController
                WebViewContentsViewController *webViewcontentsViewController = [WebViewContentsViewController alloc];
                // infos communes
                webViewcontentsViewController.estVisiteur = self.estVisiteur;
                webViewcontentsViewController.titre = [listeDesItems objectAtIndex:indexPath.row]; 
                webViewcontentsViewController.username = @""; 
                webViewcontentsViewController.titreParent = @""; 
                webViewcontentsViewController.idEcranParent = @""; 
                webViewcontentsViewController.idEcran = @""; 
                webViewcontentsViewController.idBlog = @""; 
                webViewcontentsViewController.password = @""; 
                webViewcontentsViewController.categorie = self.categorie; 
                webViewcontentsViewController.ressourceInfo = self.ressourceInfo; 
                // titre
                webViewcontentsViewController.title = [listeDesItems objectAtIndex:indexPath.row]; 
                // infos ressources
                if ([moduleContent objectForKey:@"referenceFichier"])
                    webViewcontentsViewController.pageHtml = [moduleContent objectForKey:@"referenceFichier"];
                if ([moduleContent objectForKey:@"sousRefFichier"])
                    webViewcontentsViewController.pageHtml = [moduleContent objectForKey:@"sousRefFichier"];
                //webViewcontentsViewController.contentList = [moduleContent objectForKey:@"sequenceContent"];
                // init
                (void)[webViewcontentsViewController initWithNibName:@"WebViewContentView" 
                                                        bundle:nil];
                // push
                [self.navigationController pushViewController:webViewcontentsViewController 
                                                     animated:YES];
            }		
		}
		
	}
	else
	{
        
        
        if ( moduleContent != nil ) 
        {
            if ( ([moduleContent objectForKey:@"listeDesSequences"] != nil) && ([[moduleContent objectForKey:@"listeDesSequences"] count] != 0 ) )
            {
	            // Appel ModuleViewController
     			ModuleViewController *moduleViewController = [ModuleViewController alloc];
	            // infos communes
				moduleViewController.estVisiteur = self.estVisiteur;
				moduleViewController.titre = [listeDesItems objectAtIndex:indexPath.row];
				moduleViewController.username = self.username;
				moduleViewController.titreParent = self.titre;
				moduleViewController.idEcranParent = self.idEcran;
				moduleViewController.idEcran = [moduleContent objectForKey:kIdecran];
                moduleViewController.idBlog = @"";
				moduleViewController.password = self.password;
				moduleViewController.categorie = self.categorie;
   				moduleViewController.ressourceInfo = self.ressourceInfo;
                moduleViewController.pcrDictionary=self.pcrDictionary;
	            // infos ressources
				moduleViewController.moduleContent = moduleContent;
				moduleViewController.dicoRacine = self.dicoRacine;
                // titre
                moduleViewController.title = [listeDesItems objectAtIndex:indexPath.row]; 
	            // init
				(void)[moduleViewController initWithNibName:@"ModuleView" bundle:nil];
	            // push
				[self.navigationController pushViewController:moduleViewController animated:YES];
            }		
            else 
            {
                // Appel WebViewContentsViewController
                WebViewContentsViewController *webViewcontentsViewController = [WebViewContentsViewController alloc];
                // infos communes
                webViewcontentsViewController.estVisiteur = self.estVisiteur;
                webViewcontentsViewController.titre = [listeDesItems objectAtIndex:indexPath.row]; 
                webViewcontentsViewController.username = self.username;
                webViewcontentsViewController.titreParent = self.titre;
                webViewcontentsViewController.idEcranParent = self.idEcran;
                if ([moduleContent objectForKey:@"idecran"])
                    webViewcontentsViewController.idEcran = [moduleContent objectForKey:@"idecran"];
                webViewcontentsViewController.idBlog = @"";
                webViewcontentsViewController.password = self.password;
                webViewcontentsViewController.categorie = self.categorie;
                webViewcontentsViewController.ressourceInfo = self.ressourceInfo;
                // titre
                webViewcontentsViewController.title = [listeDesItems objectAtIndex:indexPath.row]; 
                // infos ressources
                webViewcontentsViewController.pcrDictionary = self.pcrDictionary;
                if ([moduleContent objectForKey:@"referenceFichier"])
                    webViewcontentsViewController.pageHtml = [moduleContent objectForKey:@"referenceFichier"];
                if ([moduleContent objectForKey:@"sousRefFichier"])
                    webViewcontentsViewController.pageHtml = [moduleContent objectForKey:@"sousRefFichier"];
                //webViewcontentsViewController.contentList = [moduleContent objectForKey:@"sequenceContent"];
                // init            
                (void)[webViewcontentsViewController initWithNibName:@"WebViewContentView" bundle:nil];
                // push            
                [self.navigationController pushViewController:webViewcontentsViewController animated:YES];
                
            }
        }
        
	}

}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

#pragma mark -


@end
