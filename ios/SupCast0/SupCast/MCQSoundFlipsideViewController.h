//
//  MCQSoundFlipsideViewController.h
//  SupCast
//
//  Created by Phetsana on 29/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVFoundation/AVAudioPlayer.h"

@interface MCQSoundFlipsideViewController : UIViewController {
	IBOutlet UIImageView *fondEcranImageView;
	NSArray *soundsList;
	IBOutlet UIScrollView *mainScrollView;
	IBOutlet UIView *mainView;
	IBOutlet UITextView *textView1;
	IBOutlet UITextView *textView2;
	IBOutlet UIImageView *imageView;
	IBOutlet UITableView *tableView;
	IBOutlet UITableView *indicationTableView1;
	NSString *text1;
	NSString *text2;
	UIImage *image;
	AVAudioPlayer *player;
	NSString *dirRes;
	NSDictionary *dicoRacine;
}

@property (nonatomic, retain) NSDictionary *dicoRacine;
@property (nonatomic, retain) NSString *dirRes;

@property (nonatomic, retain) AVAudioPlayer *player;

//-(NSString *)dataFilePath:(NSString *)fileName; 

- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil
				text1:(NSString *)t1
				text2:(NSString *)t2
				image:(UIImage *)img
		   soundsList:(NSArray *)list;

- (void)playSoundWithSection:(int)section row:(int)row;
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section;

@end
