//
//  ContentController.h
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright CAPE - Ecole des Mines de Nantes 2010. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppRecord.h"

@interface ContentController : NSObject
{
}

@property(nonatomic,strong) AppRecord *detailItem;
@property(nonatomic,weak) IBOutlet UINavigationController *navigationController;

- (UIView *)view;

@end

