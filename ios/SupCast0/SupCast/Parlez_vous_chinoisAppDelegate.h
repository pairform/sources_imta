//
//  Parlez_vous_chinoisAppDelegate.h
//  SupCast
//
//  Created by Phetsana on 30/07/09.
//  Copyright EMN - CAPE 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WelcomeViewController;

@interface Parlez_vous_chinoisAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	UINavigationController *navigationController;
	WelcomeViewController *welcomeViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;
@property (nonatomic, retain) WelcomeViewController *welcomeViewController;

@end
