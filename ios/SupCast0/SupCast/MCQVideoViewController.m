//
//  MCQVideoViewController.m
//  SupCast
//
//  Created by Phetsana on 23/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "MCQVideoViewController.h"
#import "Movie.h"

@implementation MCQVideoViewController
//@synthesize dirRes;
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	indicationVideoTableView.backgroundColor = [UIColor clearColor];
	
	/* Calcul de position et de dimension */
	UIFont *captionFont = [UIFont fontWithName:kVerdana size:14];
	CGSize cs1 = [indication sizeWithFont:captionFont
						constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
							lineBreakMode:UILineBreakModeWordWrap];
	height1 = cs1.height + 40; 
	
	UIFont *textFont = [UIFont fontWithName:kHelvetica size:12];
	CGSize cs2 = [[[questionList objectAtIndex:indexQuestion] objectForKey:kText] sizeWithFont:textFont
																			   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
																				   lineBreakMode:UILineBreakModeWordWrap];
	
	if ([[questionList objectAtIndex:indexQuestion] objectForKey:kText] != nil)
		height2 = cs2.height + 20 + 20;
	else
		height2 = 0;
	
	indicationVideoTableView.frame = CGRectMake(indicationVideoTableView.frame.origin.x, 
												indicationVideoTableView.frame.origin.y, 
												indicationVideoTableView.frame.size.width, 
												height1 + height2 + 40 + 46);
	
	
	float tmpHeight = 0;
	int numberOfSequence = [self tableView:tableView numberOfRowsInSection:0];
	NSString *answersChoice;
	for (int i = 0; i < numberOfSequence; i++) {
		answersChoice = [[[questionList objectAtIndex:indexQuestion] objectForKey:kAnswersChoice] objectAtIndex:i];
		UIFont *captionFont = [UIFont boldSystemFontOfSize:20];
		CGSize cs = [answersChoice sizeWithFont:captionFont 
							  constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
								  lineBreakMode:UILineBreakModeWordWrap];
		tmpHeight += (cs.height + 20)	;
	}
	
	tableView.frame = CGRectMake(tableView.frame.origin.x, 
								 indicationVideoTableView.frame.origin.y + indicationVideoTableView.frame.size.height + 10, 
								 tableView.frame.size.width, 
								 tmpHeight + 22);
	
	mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width,
											tableView.frame.origin.y + tableView.frame.size.height);
	
}

- (void)update {
	[super update];
	
	movie = [Movie alloc];
	movie.dirRes = self.dirRes;
	[movie initMoviePlayerWithName:[[questionList objectAtIndex:indexQuestion] objectForKey:kVideoName]
						 extension:[[questionList objectAtIndex:indexQuestion] objectForKey:kVideoExtension]];
	[indicationVideoTableView reloadData];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	if (aTableView == indicationVideoTableView) {
		if ([[questionList objectAtIndex:indexQuestion] objectForKey:kText] != nil)
			return 3;
		return 2;
	}
	return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	NSInteger result;
	
	if (aTableView == indicationVideoTableView)
		result = 1;
	else
		result = [super tableView:aTableView numberOfRowsInSection:section];
	
	return result;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [super tableView:aTableView cellForRowAtIndexPath:indexPath];
	
	if (aTableView ==  indicationVideoTableView) {
		if (indexPath.section == 0) {
			cell.textLabel.font = [UIFont fontWithName:kVerdana size:14];
			cell.textLabel.text = indication;
		} else if (indexPath.section == 1) {
			cell.textLabel.text = @"Voir l'extrait vidéo";
		} else if (indexPath.section == 2) {
			cell.textLabel.font = [UIFont fontWithName:kHelvetica	size:12];
			cell.textLabel.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kText];
		}
	}
	
	return cell;        	
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat result = [super tableView:aTableView heightForRowAtIndexPath:indexPath];
	CGSize cs;
	
	if (aTableView == indicationVideoTableView)
		if (indexPath.section == 0) {
			UIFont *captionFont = [UIFont fontWithName:kVerdana size:14];
			cs = [indication sizeWithFont:captionFont 
						constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
							lineBreakMode:UILineBreakModeWordWrap];
			result = cs.height + 40;
		} else if (indexPath.section == 2) {
			UIFont *textFont =[ UIFont fontWithName:kHelvetica size:12];
			cs = [[[questionList objectAtIndex:indexQuestion] objectForKey:kText] sizeWithFont:textFont
																			   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
																				   lineBreakMode:UILineBreakModeWordWrap];			
			result = cs.height + 20;
		} else {
			result = 40;
		}
	
	return result;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	
	if (aTableView == tableView) {
		[super tableView:aTableView didSelectRowAtIndexPath:indexPath];
	} else if (aTableView == indicationVideoTableView && indexPath.section == 1) {
		[self playMovie];
	}
}

- (void)dealloc {

	[movie release];
	[indicationVideoTableView release];
    [super dealloc];
}

@end
