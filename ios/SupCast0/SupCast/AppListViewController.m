//
//  AppListViewController.m
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//
#import "AppListViewController.h"


NSString *AppDataDownloadCompleted = @"AppDataDownloadCompleted";

#pragma mark -
@interface AppListViewController ()
- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath;
@end

@implementation AppListViewController

@synthesize critere;
@synthesize entries;
@synthesize imageDownloadsInProgress;
@synthesize contentController;
@synthesize queue;
@synthesize appRecords;
@synthesize appListData;
@synthesize appListFeedConnection;
@synthesize plist;
@synthesize HUD;


@synthesize password;
@synthesize niveau;
@synthesize username;
@synthesize estVisiteur;

#pragma mark -
#pragma mark UIViewController

- (void)viewDidLoad {
	
	
	DLog(@"");
    [super viewDidLoad];
    
    //[indicator setHidden:NO];	
	//[indicator startAnimating];	
    [self showLoadingProgress];
    aTableView.backgroundColor = [UIColor clearColor];
	aTableView.backgroundView = nil;
    aTableView.rowHeight = kCustomRowHeight;
    
    /**/
	// Initialize the array of app records and pass a reference to that list to our root view controller
    self.appRecords = [NSMutableArray array];
    
    NSString * critereValeur;
    NSString * critereType;
    for (id type in self.critere) 
    {
        if ([type isEqualToString:kEtablissement])
        {
            critereType = kEtablissement; 
            critereValeur = [[NSString alloc] initWithString:[self.critere objectForKey:kEtablissementId]]; 
        }
        if ([type isEqualToString:kDomaine])
        {
            critereType = kDomaine; 
            critereValeur = [[NSString alloc] initWithString:[self.critere objectForKey:kDomaineId]]; 
        }
        
    }
    
    NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kURLlisteApp];
    NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *paramDataString = [NSString stringWithFormat:
                                 @"id=%@&critereValeur=%@&pseudo=%@&password=%@&critereType=%@",kKey,critereValeur,self.username,self.password,critereType];
    
    NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:paramData];
    
    self.appListFeedConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    // Test the validity of the connection object. The most likely reason for the connection object
    // to be nil is a malformed URL, which is a programmatic error easily detected during development
    // If the URL is more dynamic, then you should implement a more flexible validation technique, and
    // be able to both recover from errors and communicate problems to the user in an unobtrusive manner.
    //
    if(self.appListFeedConnection) 
    {
        //[indicator startAnimating];
        //[indicator setHidden:NO];
        [self showLoadingProgress];

    
    }
    else
    {    
        [self cancelConnection];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                        message:@"Erreur de connexion"
                                                       delegate:self
                                              cancelButtonTitle:@"Accepter"
                                              otherButtonTitles:nil];
        
        [alert show];
    }
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

	/**/
    
    self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
    
    // start listening for download completion
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(downloadCompleted:)
                                                 name:AppDataDownloadCompleted
                                               object:nil];
	
    
    
}

- (void)viewDidUnload {
   // [indicator release];
   // indicator = nil;
    aTableView = nil;
	
    self.critere = nil;
    self.entries = nil;
    self.imageDownloadsInProgress = nil;
    self.contentController = nil;
    self.queue = nil;
    self.appRecords = nil;
    self.appListData = nil;
    self.appListFeedConnection = nil;
    self.plist = nil;
    self.HUD = nil;

    
    
    
    
    
	DLog(@"");
	
	// Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
	[[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AppDataDownloadCompleted
                                                  object:nil];
	
}


- (void)dealloc {
	
	DLog(@"");
	
    

    //[indicator release];
}

- (void)didReceiveMemoryWarning {
	DLog(@"");
	
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
    NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    [allDownloads performSelector:@selector(cancelDownload)];
	
}

- (void)viewWillDisappear:(BOOL)animated {
	DLog(@"");
    [self hideLoadingProgress];
    [self cancelConnection];

    [super viewWillDisappear:animated];
}

- (void)cancelConnection {
	DLog(@"cancelConnection");
	if (self.appListFeedConnection) {
		[self.appListFeedConnection cancel];
		self.appListFeedConnection = nil;
	}
}


- (void)viewWillAppear:(BOOL)animated {
	DLog(@"");
    [aTableView reloadData];
    [super viewWillAppear:animated];
}


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

#pragma mark -
#pragma mark Table view creation (UITableViewDataSource)


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DLog(@"");
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
	
    currentIndex = indexPath.row;
    
    if (entries != nil && entries.count > 0)
    {
        AppRecord *app = [entries objectAtIndex:currentIndex];
        contentController.detailItem = app;
				
		// Navigation logic may go here -- for example, create and push another view controller.
        
        NSMutableDictionary *dico = [[NSMutableDictionary alloc] initWithDictionary:[self.plist objectAtIndex:currentIndex]];
        [dico setObject:[NSDate date] forKey:kRessourceDownloadedStr];
        [dico setObject:kEncours forKey:kStatus];
		aTelechargement = [Telechargement alloc];
        aTelechargement.myDico =  [[NSMutableDictionary alloc] initWithDictionary:dico];
        
		(void)[aTelechargement initWithNibName:@"Telechargement" bundle:nil];
		
		[self.navigationController pushViewController:aTelechargement animated:YES];
        
    }
    
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath

{
    DLog(@"");
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
	
    currentIndex = indexPath.row;
    
    if (entries != nil && entries.count > 0)
    {
        AppRecord *app = [entries objectAtIndex:currentIndex];
        contentController.detailItem = app;
        
		// Navigation logic may go here -- for example, create and push another view controller.
        
        NSMutableDictionary *dico = [[NSMutableDictionary alloc] initWithDictionary:[self.plist objectAtIndex:currentIndex]];
        [dico setObject:[NSDate date] forKey:kRessourceDownloadedStr];
        [dico setObject:kEncours forKey:kStatus];
		aTelechargement = [Telechargement alloc];
        aTelechargement.myDico =  [[NSMutableDictionary alloc] initWithDictionary:dico];
        
		(void)[aTelechargement initWithNibName:@"Telechargement" 
							      bundle:nil];
		
		[self.navigationController pushViewController:aTelechargement animated:YES];
        
    }
}

// customize the number of rows in the table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	DLog(@"");
	
	int count = [entries count];
	
	// if there's no data yet, return enough rows to fill the screen
    if (count == 0)
	{
        //return kCustomRowCount;
        return 1;
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	DLog(@"");
	// customize the appearance of table view cells
	//
	static NSString *CellIdentifier = @"LazyTableCell";
    static NSString *PlaceholderCellIdentifier = @"PlaceholderCell";
    
    // add a placeholder cell while waiting on table data
    int nodeCount = [self.entries count];
	
	if (nodeCount == 0 && indexPath.row == 0)
	{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PlaceholderCellIdentifier];
        if (cell == nil)
		{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
										   reuseIdentifier:PlaceholderCellIdentifier];   
            cell.detailTextLabel.textAlignment = UITextAlignmentCenter;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
			
        }
		
		cell.detailTextLabel.text = @"Pas de ressource disponible.";
		
		return cell;
    }
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
									   reuseIdentifier:CellIdentifier];
    }
	
    // Leave cells empty if there's no data yet
    if (nodeCount > 0)
	{
        // Set up the cell...
        AppRecord *appRecord = [self.entries objectAtIndex:indexPath.row];
        
		cell.textLabel.text = appRecord.ressourcenom;
        cell.detailTextLabel.text = appRecord.ressourcetitle;
	
        // Only load cached images; defer new downloads until scrolling ends
        if (!appRecord.appIcon)
        {
            if (tableView.dragging == NO && tableView.decelerating == NO)
            {
                [self startIconDownload:appRecord forIndexPath:indexPath];
            }
            // if a download is deferred or in progress, return a placeholder image
            cell.imageView.image = [UIImage imageNamed:@"Placeholder.png"];                
        }
        else
        {
			cell.imageView.image = appRecord.appIcon;
			//[appIconDatas addObject:appRecord.appIconData];
			//[appIcons addObject:appRecord.appIcon];
        }
        
		//cell.accessoryType = UITableViewCellAccessoryNone;
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    
    return cell;
}

#pragma mark -
#pragma mark Table cell image support

- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath
{
	DLog(@"");
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    if (iconDownloader == nil) 
    {
        iconDownloader = [[IconDownloader alloc] init];
        iconDownloader.appRecord = appRecord;
        iconDownloader.indexPathInTableView = indexPath;
        iconDownloader.delegate = self;
        [imageDownloadsInProgress setObject:iconDownloader forKey:indexPath];
        [iconDownloader startDownload];
    }
}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows
{
	DLog(@"");
    if ([self.entries count] > 0)
    {
        NSArray *visiblePaths = [aTableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            AppRecord *appRecord = [self.entries objectAtIndex:indexPath.row];
            
            if (!appRecord.appIcon) // avoid the app icon download if the app already has an icon
            {
                [self startIconDownload:appRecord forIndexPath:indexPath];
            }
        }
    }
}

// called by our ImageDownloader when an icon is ready to be displayed
- (void)appImageDidLoad:(NSIndexPath *)indexPath
{
	DLog(@"");
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    if (iconDownloader != nil)
    {
        UITableViewCell *cell = [aTableView cellForRowAtIndexPath:iconDownloader.indexPathInTableView];
        
        // Display the newly loaded image
        cell.imageView.image = iconDownloader.appRecord.appIcon;
    }
	[aTableView reloadData];
	
}


#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)

- (void)downloadCompleted:(NSNotification *)notification
{
	DLog(@"");

  //  [indicator setHidden:YES];	
	//[indicator stopAnimating];	
    [self hideLoadingProgress];

    self.entries = [notification object];   // incoming object is an NSArray of AppRecords
    [aTableView reloadData];
}

// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	DLog(@"");
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	DLog(@"AppListscrollViewDidEndDecelerating");
    [self loadImagesForOnscreenRows];
}

// -------------------------------------------------------------------------------
//	handleLoadedApps:loadedApps
// -------------------------------------------------------------------------------
- (void)handleLoadedApps:(NSArray *)loadedApps
{
	DLog(@"");
    [self.appRecords addObjectsFromArray:loadedApps];
    
    // tell our interested view controller reload its data, now that parsing has completed
    [[NSNotificationCenter defaultCenter] postNotificationName:AppDataDownloadCompleted object:loadedApps];
}

// -------------------------------------------------------------------------------
//	didFinishParsing:appList
// -------------------------------------------------------------------------------
- (void)didFinishParsing:(NSArray *)appList
{
	DLog(@"");
    [self performSelectorOnMainThread:@selector(handleLoadedApps:) withObject:appList waitUntilDone:NO];
    
    self.queue = nil;   // we are finished with the queue and our ParseOperation
}

- (void)parseErrorOccurred:(NSError *)error
{
	DLog(@"");
    [self performSelectorOnMainThread:@selector(handleError:) withObject:error waitUntilDone:NO];
}

#pragma mark "HUD" chargement en cours

- (void)showLoadingProgress {
	if (HUD==nil) {
		DLog(@"showLoadingProgress");
		UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
		CGRect windowFrame = mainWindow.frame;
		DLog(@"window=%@", [mainWindow description]);
		CGRect progressHUDrect = CGRectMake(windowFrame.origin.x,
											windowFrame.origin.y, 
											windowFrame.size.width,
											windowFrame.size.height);
		progressHUDrect.size.height = 120.0;
		progressHUDrect.origin.y = (windowFrame.size.height / 2.0) - (progressHUDrect.size.height / 2);
		progressHUDrect.origin.x = 80.0;
		progressHUDrect.size.width = windowFrame.size.width - (progressHUDrect.origin.x * 2);
        UIInterfaceOrientation orient = [self interfaceOrientation];
        CGAffineTransform myT;
        if (orient == UIInterfaceOrientationLandscapeLeft)
        {
            myT = CGAffineTransformMakeRotation(M_PI*1.5);
        } 
        else if (orient == UIInterfaceOrientationLandscapeRight) 
        {
            myT =  CGAffineTransformMakeRotation(M_PI/2);
        } 
        else if (orient == UIInterfaceOrientationPortraitUpsideDown)
        {
            myT =  CGAffineTransformMakeRotation(-M_PI);
        }
        else
        {
            myT =  CGAffineTransformIdentity;
        }
        
        
        self.HUD = [[WBProgressHUD alloc] initWithFrame:progressHUDrect];
        self.HUD.transform = myT; 
		HUD.colorBackground = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.30];
		DLog(@"alloc OK : HUD=%@", [HUD description]);
		[HUD setText:@"Chargement …"];
		[HUD showInView:mainWindow];
	}
}

- (void)hideLoadingProgress {
	DLog(@"hideLoadingProgress");
	if (HUD) {
		HUD.hidden = YES;
		[HUD removeFromSuperview];
		self.HUD = nil;
	}
}



- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if (!(HUD.hidden))
    {
    CGAffineTransform myT;
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        myT = CGAffineTransformMakeRotation(M_PI*1.5);
    } 
    else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) 
    {
        myT =  CGAffineTransformMakeRotation(M_PI/2);
    } 
    else if (toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        myT =  CGAffineTransformMakeRotation(-M_PI);
    }
    else
    {
        myT =  CGAffineTransformIdentity;
    }
    
    self.HUD.transform = myT; 
    
    }
}



#pragma mark -
#pragma mark NSURLConnection delegate methods

// -------------------------------------------------------------------------------
//	handleError:error
// -------------------------------------------------------------------------------
- (void)handleError:(NSError *)error
{
	DLog(@"");
    //NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"La liste des applications SUPCAST n'est pas disponible. "
														message:@"Vérifier votre connexion internet."  //errorMessage
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [alertView show];
}

// The following are delegate methods for NSURLConnection. Similar to callback functions, this is how
// the connection object,  which is working in the background, can asynchronously communicate back to
// its delegate on the thread from which it was started - in this case, the main thread.
//

// -------------------------------------------------------------------------------
//	connection:didReceiveResponse:response
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	DLog(@"");
    self.appListData = [NSMutableData data];    // start off with new data
}

// -------------------------------------------------------------------------------
//	connection:didReceiveData:data
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	DLog(@"");
    [appListData appendData:data];  // append incoming data
}

// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

    
    DLog(@"");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
   // [indicator setHidden:YES];
   // [indicator stopAnimating];
    [self hideLoadingProgress];
    if ([error code] == kCFURLErrorNotConnectedToInternet)
	{
        // if we can identify the error, we can present a more precise message to the user.
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"No Connection Error"
															 forKey:NSLocalizedDescriptionKey];
        NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain
														 code:kCFURLErrorNotConnectedToInternet
													 userInfo:userInfo];
        [self handleError:noConnectionError];
    }
	else
	{
        // otherwise handle the error generically
        [self handleError:error];
    }
    
    self.appListFeedConnection = nil;   // release our connection
}

// -------------------------------------------------------------------------------
//	connectionDidFinishLoading:connection
// -------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	DLog(@"");
    self.appListFeedConnection = nil;   // release our connection
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;  
  //  [indicator setHidden:YES];
  //  [indicator stopAnimating];
    [self hideLoadingProgress];

    
    
    //Serialisation du fichier reçu        
    NSString *error; 
    NSPropertyListFormat format;
    self.plist = [NSPropertyListSerialization propertyListFromData:self.appListData
                                                    mutabilityOption:NSPropertyListImmutable
                                                              format:&format
                                                    errorDescription:&error]; 
    if(!self.plist)
    { 
        DLog(@"Attention Erreur : %@",error); 
    }
    else
    {
        DLog(@"ok plist is %@",self.plist); 
        
        NSMutableArray * workingArray = [[NSMutableArray alloc] init];
        for (NSDictionary *object in self.plist) 
        {
            AppRecord * workingEntry            = [[AppRecord alloc] init];
            workingEntry.appIcon                = [object objectForKey:kRessourceIconeImageStr];
            workingEntry.appIconData            = [object objectForKey:kRessourceIconeDataStr];
            workingEntry.appIcone               = [object objectForKey:kRessourceIconeStr];
            workingEntry.appID                  = [object objectForKey:kRessourceIdStr];
            workingEntry.appLink                = [object objectForKey:kRessourcebaseURL];
            //workingEntry.appListeMedia          = [object objectForKey:kRessourceMediasStr];
            workingEntry.ressourcenom           = [object objectForKey:kRessourceNomStr];
            workingEntry.appTitle               = [object objectForKey:kRessourceTitleStr];
            workingEntry.ressourcebaseurl       = [object objectForKey:kRessourcebaseURL];
            workingEntry.ressourcecatalogue     = [object objectForKey:kRessourceCatalogueStr];
            workingEntry.ressourcedomaine       = [object objectForKey:kRessourceDomaineStr];
            workingEntry.ressourceelggid        = [object objectForKey:kRessourceElggidStr];
            workingEntry.ressourceetablissement = [object objectForKey:kRessourceEtablissementStr];
            workingEntry.ressourceFolder        = [object objectForKey:kRessourceFolderStr];
            workingEntry.ressourceTaille        = [object objectForKey:kRessourceTailleStr];
            workingEntry.ressourcefolder        = [object objectForKey:kRessourceFolderStr];
            workingEntry.ressourceicone         = [object objectForKey:kRessourceIconeStr];
            workingEntry.ressourceid            = [object objectForKey:kRessourceIdStr];
           // workingEntry.ressourcemedia         = [object objectForKey:kRessourceMediasStr];
           // workingEntry.ressourcemedias        = [object objectForKey:kRessourceMediasStr];
            workingEntry.ressourceparent        = [object objectForKey:kRessourceFichierParent];
            workingEntry.ressourceRessourcesWeb = [object objectForKey:kRessourceWebStr];
            workingEntry.ressourcestructure     = [object objectForKey:kRessourceStructureStr];
            workingEntry.ressourcesummary       = [object objectForKey:kRessourceSummaryStr];
            workingEntry.ressourcetitle         = [object objectForKey:kRessourceTitleStr];
            workingEntry.ressourceurl           = [object objectForKey:kRessourceURL];
            workingEntry.ressourceupdated       = [object objectForKey:kRessourceUpdatedStr];
            workingEntry.ressourcereleased      = [object objectForKey:kRessourceReleasedStr];
            workingEntry.ressourceversion1      = [object objectForKey:kRessourceVersion1Str];
            workingEntry.ressourceversion2      = [object objectForKey:kRessourceVersion2Str];
            workingEntry.ressourceversion3      = [object objectForKey:kRessourceVersion3Str];
            
            [workingArray addObject:workingEntry];
        }  
        
        [self didFinishParsing:workingArray];
        
        [aTableView reloadData];
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     
     
     
     // create the queue to run our ParseOperation
     self.queue = [[NSOperationQueue alloc] init];
     
     // create an ParseOperation (NSOperation subclass) to parse the RSS feed data so that the UI is not blocked
     // "ownership of appListData has been transferred to the parse operation and should no longer be
     // referenced in this thread.
     //
     ParseOperation *parser = [[ParseOperation alloc] initWithData:appListData delegate:self];
     
     [queue addOperation:parser]; // this will start the "ParseOperation"
     
     [parser release];
     
     // ownership of appListData has been transferred to the parse operation
     // and should no longer be referenced in this thread
     
     */
    
    self.appListData = nil;
}

@end
