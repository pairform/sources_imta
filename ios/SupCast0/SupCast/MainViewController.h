//
//  MainViewController.h
//  SupCast
//
//  Created by Phetsana on 20/07/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GestionReseauSocialViewController.h"
#import "MainViewCell.h"
#import "ModuleViewController.h"
#import "WebViewContentsViewController.h"
#import "GestionFichier.h"

@interface MainViewController : GestionReseauSocialViewController <UITableViewDelegate, UITableViewDataSource> {

    NSArray * listeDesItems; 	
	NSArray *modulesList; // Tableau qui va stocker la structure de données
	NSDictionary *dicoRacine;
    NSIndexPath * selectedRowIndexPath;
}
@property(nonatomic,strong) NSIndexPath * selectedRowIndexPath;
@property(nonatomic,strong) NSDictionary *dicoRacine;
@property(nonatomic,strong) NSArray  * listeDesItems;

@end
