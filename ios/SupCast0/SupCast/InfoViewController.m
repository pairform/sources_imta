//
//  InfoViewController.m
//  SupCast
//
//  Created by Phetsana on 15/09/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "InfoViewController.h"

@implementation InfoViewController

@synthesize statusConnexionReseau;
@synthesize requestURL;

#pragma mark -
#pragma mark Reachability

-(NetworkStatus) etatConnexionReseau {
    self.statusConnexionReseau = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    return self.statusConnexionReseau;
}


- (BOOL)connected 
{
    if (NotReachable == (BOOL)[self etatConnexionReseau])
        return NO;    
    else
        return YES;    
}


#pragma mark -
#pragma mark View life cycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:@"Infos"];
    
    self.requestURL = [[NSMutableURLRequest alloc] init];
    
    if ([self connected])
    { 
        
        NSString * urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLSupCast,kInfos];
        NSURL * url = [[NSURL alloc] initWithString:urlConnexion];
        [self.requestURL setURL:url];
        [webView loadRequest:self.requestURL];
    }
    else 
    {
        NSString *pathFichierInfo = [[NSBundle mainBundle] pathForResource:@"info" 
                                                                    ofType:@"html"];
            
        if ([[NSFileManager defaultManager] fileExistsAtPath:pathFichierInfo])
        {  
            NSURL * url = [[NSURL alloc] initWithString:pathFichierInfo];
            [self.requestURL setURL:url];
            [webView loadRequest:self.requestURL];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    webView = nil;
    requestURL = nil;
}

#pragma mark -
#pragma mark UIWebViewDelegate

// La redéfinition de cette fonction va permettre d'ouvrir tous les liens de la page avec le navigateur safari
-            (BOOL)webView:(UIWebView *)webView 
shouldStartLoadWithRequest:(NSURLRequest *)request 
            navigationType:(UIWebViewNavigationType)navigationType {
	// On s'assure que le lien que l'on ouvre n'est pas local
    
	if (navigationType == UIWebViewNavigationTypeLinkClicked) 
    {
		[[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
	}
	
	return YES;
}

#pragma mark -



@end
