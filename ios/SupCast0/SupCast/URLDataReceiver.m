

#import "URLDataReceiver.h"

/*!
 * @abstract Status of URLDataReceiver.
 */
enum {
	DRInit = 1,
	DRLoading,
	DRFinished,
	DRCancelled
};

/*!
 * @abstract Private routines.
 */
@interface URLDataReceiver(Private)
- (void)notifyDidFinish;
- (void)setError:(NSError*)error;
@end

@implementation URLDataReceiver

- (id)initWithURL:(NSURL*)aURL delegate:(id)delegate {
	DLog(@"");
	self = [super init];
	
	if (self == nil) return nil;
	
	fStatus = DRLoading;
	fURL = [aURL copy];
	fReceived = [NSMutableData new];
	fError = nil;
	fDelegate = delegate;
	fLock = [NSRecursiveLock new];
	fConnection = nil;
	
	return self;
}

- (void) dealloc {
	DLog(@"");
	//cancel it if we are loading.
	[fLock lock];
	if (fStatus == DRLoading) [self cancel];
	[fLock unlock];
	
	
}
	
	

- (void)startLoading {
	DLog(@"");
	[fLock lock];
	
	fStatus = DRLoading;
	fConnection = [[NSURLConnection alloc] 
					initWithRequest:[NSURLRequest requestWithURL:fURL]
					delegate:self];
	[fLock unlock];
}

- (void)cancel {
	DLog(@"");
	[fLock lock];
	
	//return right away if we are not loading.
	if (fStatus != DRLoading) {
		[fLock unlock];
		return;
	}
	
	fStatus = DRCancelled;
	
	if (fConnection) [fConnection cancel];
	[fReceived setLength:0];
	
	[fLock unlock];
}

- (NSData*)receivedData {
	DLog(@"");
	return fReceived;
}

- (NSURL*)url {
	DLog(@"");
	return fURL;
}

- (NSError*)lastError {
	DLog(@"");
	return fError;
}

/////////// NSURLConnection delegate functions //////////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
//	DLog(@"");
	[fLock lock];
	
	NSAssert(fStatus == DRLoading, @"I should be loadiing.");
	[fReceived appendData:data];
	[fLock unlock];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	DLog(@"%s / %@",__PRETTY_FUNCTION__,connection);
	[fLock lock];
	NSAssert(fStatus != DRFinished, @"Why am I here if I already finished.");
	fStatus = DRFinished;
	[fReceived setLength:0];
	[self setError:error];
	[self notifyDidFinish];
	[fLock unlock];
}

- (void)connection:(NSURLConnection *)connection 
				didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	DLog(@"%s / %@",__PRETTY_FUNCTION__,connection);
	[fLock lock];
	NSAssert(fStatus != DRFinished, @"Why am I here if I already finished.");
	[fConnection cancel];
	fStatus = DRFinished;
	[fReceived setLength:0];
	[self setError:nil];
	[self notifyDidFinish];
	[fLock unlock];
}

- (NSURLRequest *)connection:(NSURLConnection *)connection 
				willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse {
	DLog(@"%s / %@ / %@ / %@",__PRETTY_FUNCTION__,connection,request,redirectResponse);
	return request;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	DLog(@"%s / %@",__PRETTY_FUNCTION__,connection);
	[fLock lock];
	NSAssert(fStatus != DRFinished, @"Why am I here if I already finished.");
	fStatus = DRFinished;
	[self setError:nil];
	[self notifyDidFinish];
	[fLock unlock];
}

/////////////// Private Routines //////////////////

- (void)notifyDidFinish {
	DLog(@"");
	//notify the delegate.
	if (fDelegate && [fDelegate respondsToSelector:@selector(URLDataReceiverDidFinish:)])
		[fDelegate performSelector:@selector(URLDataReceiverDidFinish:) withObject:self];
 }

//TODO
- (void)setError:(NSError*)error {
	DLog(@"");
	if (error) 
		fError = error;
	else
		fError = nil;
}

@end
