//
//  LectureSupport.h
//  SupCast
//
//  Created by Didier PUTMAN on 24/11/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaViewController.h"


@interface LectureSupport : UITableViewController {
	MediaViewController *aMediaViewController;
	NSArray *donneesFichier;
	NSString * mySupport;
	NSString * dirRes;
	
}

@property (nonatomic, retain) NSString * dirRes;
@property (nonatomic, retain) NSString * mySupport;
@property (nonatomic, retain) NSArray *donneesFichier;

- (id)initWithNibName:(NSString *)nibNameOrNil
			   bundle:(NSBundle *)nibBundleOrNil
		   nomSupport:(NSString *)nomSupport;

@end