//
//  MyStore.h
//  SupCast
//
//  Created by admin on 24/11/10.
//  Copyright 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyStore : UIViewController <UITextViewDelegate> {
	IBOutlet UITextView *aTextView;

}

@property (nonatomic, retain) UITextView *aTextView;

- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil 
				 downloadLink:(NSString *)downloadLink;
@end
