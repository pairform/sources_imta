//
//  Timer.m
//  SupCast
//
//  Created by Phetsana on 19/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "Timer.h"


@implementation Timer

@synthesize timeLeft, min;

- (id)initTimerWithMin:(int)m sec:(int)s {
	min = m;
	if (s > 59)
		DLog(@"Attention les données sur les secondes sont incorrectes");
	sec1 = s / 10;
	sec2 = s % 10;
	timeLeft = [[NSString alloc] initWithFormat:@"%d : %d%d", min, sec1, sec2];
	
	return self;
}

- (NSString *)update {
	if (!(min == 0 && sec1 == 0 && sec2 ==0))
		sec2--;
	
	if (sec2 < 0) {
		sec1--;
		sec2 = 9;
		if (sec1 < 0) {
			min--;
			sec1 = 5;
			if (min < 0) {
				return [[NSString alloc] initWithFormat:@"0 : 00"];
			}
		}
	}
	
	return [[NSString alloc] initWithFormat:@"%d : %d%d", min, sec1, sec2];
}

- (NSString *)timeLeft {
	timeLeft = [self update];
	
	return timeLeft;
}

- (void)dealloc {
	[super dealloc];
}

@end
