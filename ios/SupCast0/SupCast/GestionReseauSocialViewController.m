//
//  GestionReseauSocialViewController.m
//  SupCast
//
//  Created by Didier PUTMAN on 02/09/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionReseauSocialViewController.h"

#define INTERESTING_TAG_NAMES @"ObjectEntity",@"idecran", @"LastMessage", @"nombreNouveauMessage", nil

@implementation GestionReseauSocialViewController

// infos communes

@synthesize connectionNiveauRessource, receivedDataNiveauRessource, plistNiveauRessource;
@synthesize estVisiteur;
@synthesize titre;
@synthesize username;
@synthesize titreParent;
@synthesize idEcranParent;
@synthesize idEcran;
@synthesize idBlog;
@synthesize password;
@synthesize categorie;
@synthesize ressourceInfo;
@synthesize pcrDictionary;
// connections
@synthesize connectionGetBlog;
@synthesize connectionNewMessages;
// données recues
@synthesize nvxMessage;
@synthesize receivedDataGetBlog;
@synthesize receivedDataNewMessages;

#pragma mark -
#pragma mark Gestion des nouveaux messages

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

- (void)lancerRecuperation
{   
    DLog(@"");
    if (!self.estVisiteur) {
        /**/
        self.connectionGetBlog = nil;
        self.receivedDataGetBlog = nil;
        if (self.idEcran)    
        {    
            NSString *URLgetBlog = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kgetblogRessource];
            NSURL *URLgetBlogObject = [[NSURL alloc] initWithString:URLgetBlog];
            NSMutableURLRequest *requestGetBlog = [[NSMutableURLRequest alloc] initWithURL:URLgetBlogObject];
            [requestGetBlog setHTTPMethod:@"POST"];
            //DLog(@"self.idEcran is %@",self.idEcran);
            NSString *paramDataGetBlogString = [[NSString alloc] initWithFormat:
                                                @"id=%@&idressource=%@&idecran=%@&pseudo=%@&password=%@",
                                                kKey,
                                                [self.ressourceInfo objectForKey:kRessourceElggidStr],
                                                self.idEcran,
                                                self.username,
                                                self.password];
            
            
            NSData *paramDataGetBlog = [[NSData alloc] initWithData:[paramDataGetBlogString dataUsingEncoding:NSUTF8StringEncoding]];
            [requestGetBlog setHTTPBody:paramDataGetBlog];
            
            connectionStatus = 1;
            self.receivedDataGetBlog = [[NSMutableData alloc] init];
            
            self.connectionGetBlog = [[NSURLConnection alloc] initWithRequest:requestGetBlog
                                                                     delegate:self];
            
            [self.connectionGetBlog scheduleInRunLoop:[NSRunLoop currentRunLoop]
                                              forMode:NSRunLoopCommonModes];
            [self.connectionGetBlog start];
            
            
            if(self.connectionGetBlog) 
            {
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                                message:@"Erreur de connexion"
                                                               delegate:self
                                                      cancelButtonTitle:@"Accepter"
                                                      otherButtonTitles:nil];
                
                [alert show];
                
            }
        }
        
    }
    
}

-(void)recupNewMessages
{
    DLog(@"");
    if (!self.estVisiteur) {
       
        NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,knewMessagesRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        NSString *paramDataString = [[NSString alloc] initWithFormat:
                                     @"id=%@&idressource=%@&idBlog=%@&pseudo=%@&password=%@",
                                     kKey,
                                     [self.ressourceInfo objectForKey:kRessourceElggidStr],
                                     self.idBlog,
                                     self.username,
                                     self.password];
        
        DLog(@"paramDataString is %@",paramDataString);
        NSData *paramData = [[NSData alloc] initWithData:[paramDataString dataUsingEncoding:NSUTF8StringEncoding]];
       
        [request setHTTPBody:paramData];
        
        connectionStatus = 2;
        self.receivedDataNewMessages = nil;
        self.receivedDataNewMessages = [[NSMutableData alloc] init];
        
        self.connectionNewMessages = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        
        if(self.connectionNewMessages)
        {                
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    DLog(@"");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    DLog(@"");
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kBlogCreationNotification
                                                  object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


-(void)viewWillAppear:(BOOL)animated
{
    DLog(@"");
    if (!self.estVisiteur) {
        
        if (([self.idBlog isEqualToString:@""]) || (self.idBlog == nil))
        {
            DLog(@"idBlog est vide");
            [self analyzeMessage:nil];
            [tableView reloadData];
            
        }
        else
        {
            [self recupNewMessages];
            [tableView reloadData];
        }
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    
    self.navigationController.navigationItem.hidesBackButton = NO;
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.rowHeight = kCustomRowHeight;

    if (!self.estVisiteur)
    {
        [self lancerRecuperation];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(recupIdBlog:)
                                                     name:kBlogCreationNotification
                                                   object:nil];
    }
    
    // start listening for download completion
    
    
}


- (void)recupIdBlog:(NSNotification *)notification
{
	DLog(@"");
    if (!self.estVisiteur) {
        self.idBlog = [notification object];   // incoming object is an NSArray of AppRecords
        //[self lancerRecuperation];
    }
}


- (void)viewDidUnload
{
    DLog(@"");
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    titre = nil;
    username = nil;
    titreParent = nil;
    idEcranParent = nil;
    idEcran = nil;
    idBlog = nil;
    password = nil;
    categorie = nil;
    ressourceInfo = nil;
    pcrDictionary = nil;
    connectionGetBlog = nil;
    connectionNewMessages = nil;
    receivedDataGetBlog = nil;
    receivedDataNewMessages = nil;
    
}

#pragma mark -
#pragma mark NSURLConnection Callbacks
- (BOOL)connection:(NSURLConnection *)connection 
canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    DLog(@"");
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection 
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    DLog(@"");
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection 
didReceiveResponse:(NSURLResponse *)response {
    DLog(@"");
    
    if (self.connectionGetBlog == connection)
    {    
        [self.receivedDataGetBlog setLength:0];
        
    }
    if (self.connectionNewMessages == connection)
    {    
        [self.receivedDataNewMessages setLength:0];
        
    }
    
    if (self.connectionNiveauRessource == connection)
    {    
        [self.receivedDataNiveauRessource setLength:0];
    }  
}

-(void)connection:(NSURLConnection *)connection 
   didReceiveData:(NSData *)data {
    
    DLog(@"");
    if (self.connectionGetBlog == connection)
    {    
        [self.receivedDataGetBlog appendData:data];
        
    }
    if (self.connectionNewMessages == connection)
    {    
        [self.receivedDataNewMessages appendData:data];
        
    }
    
    if (self.connectionNiveauRessource == connection)
    {    
        [self.receivedDataNiveauRessource appendData:data];
    }  
	
}

-(void)connection:(NSURLConnection *)connection 
 didFailWithError:(NSError *)error {
	
    DLog(@"");
    DLog(@"%d",connectionStatus);
    
    
    if (self.connectionGetBlog == connection)
    {    
        self.receivedDataGetBlog = nil;
        
    }
    if (self.connectionNewMessages == connection)
    {    
        self.receivedDataNewMessages = nil;
    }  
    if (self.connectionNiveauRessource == connection)
    {    
        self.receivedDataNiveauRessource = nil;
    }  
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    DLog(@"");
 	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if (self.connectionGetBlog == connection)
    {    
        NSString *error; 
        NSPropertyListFormat format; 
        NSDictionary* plist = [NSPropertyListSerialization propertyListFromData:self.receivedDataGetBlog
                                                               mutabilityOption:NSPropertyListImmutable
                                                                         format:&format
                                                               errorDescription:&error]; 
        self.receivedDataGetBlog = nil;
        if(!plist)
        { 
            
            DLog(@"Error: %@",error); 
            //  [self lancerRecuperation];
            
        }
        else
        {   
            //DLog(@"plist is %@",plist);            
            self.idBlog = [[NSString alloc] initWithString:[plist objectForKey:@"blog"]]; 
            //idBlog == nil : pList ne contient pas d'objet blog. Impossible en gros.
            //idBlog == @"" : l'écran n'a pas encore d'idBlog.
            if (([self.idBlog isEqualToString:@""]) || (self.idBlog == nil))
            {
                DLog(@"idBlog est vide");   
                [self analyzeMessage:nil];
            }
            else 
            {
                [self recupNewMessages];
            }
        }
    }
    if (self.connectionNewMessages == connection)
    {    
        
        NSString * error; 
        NSPropertyListFormat format; 
        NSMutableArray * nvxMessTest;
        
        nvxMessTest = [NSPropertyListSerialization propertyListFromData:self.receivedDataNewMessages
                                                                mutabilityOption:NSPropertyListImmutable
                                                                          format:&format
                                                                errorDescription:&error]; 
        self.receivedDataNewMessages = nil;
        if(!nvxMessTest)
        { 
            
            DLog(@"Error: %@",error); 
            
        }
        else
        {   
            self.nvxMessage = [nvxMessTest copy];
            //DLog(@"newMessage is %@",newMessage);            
            [self analyzeMessage:self.nvxMessage];
            //self.nvxMessage = nil;
        }
    }
    
    if (self.connectionNiveauRessource == connection)
    {    
        
        NSString *error; 
        NSPropertyListFormat format;
        
        self.plistNiveauRessource = [[NSArray alloc] init];  
        self.plistNiveauRessource = [NSPropertyListSerialization propertyListFromData:self.receivedDataNiveauRessource
                                                      mutabilityOption:NSPropertyListImmutable
                                                                format:&format
                                                      errorDescription:&error];
        
        
        if(!self.plistNiveauRessource)
        { 
            DLog(@"Error: %@",error); 
        }
        else
        {
            DLog(@"self.plist is %@",self.plistNiveauRessource);
            self.categorie = [[self.plistNiveauRessource objectAtIndex:0] objectForKey:@"nouveauNiveau"];
        }
    }  

}

-(void)analyzeMessage:(NSArray *)newMessage {
    DLog(@"");
    
    // visisteur, on ne fait rien !!!
    if (!self.estVisiteur)
    {
        if (self.titre)
        {
            NSMutableString * didReceive =  [[NSMutableString alloc] init];
            // pas de nouveau message
            if ([newMessage count]==0)
            {
                DLog(@"empty!!");   
            }
            else
            {    
                //calcul du nombre de nouveau pour afficher dans la bulle
                for (NSDictionary *NewMsg in newMessage)
                {   
                    if([[NewMsg objectForKey:@"ObjectEntity"] isEqualToString:self.idBlog] && 
                       [[NewMsg objectForKey:@"nombreNouveauMessage"] intValue]>=0        )
                    {
                        didReceive = [NewMsg objectForKey:@"nombreNouveauMessage"];
                        break;
                    }
                }
            }
            
            [tableView reloadData];
            
            
            //On va créer un rectangle transparent, que l'on va mettre comme background pour la toolbar
            //Solution pour iOS 5+ dite "propre"
            
            CGRect rect = CGRectMake(0,0,102,44.01);
            UIGraphicsBeginImageContext(rect.size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
            CGContextFillRect(context,rect);
            UIImage *transparentImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UIToolbar *tools = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 102, 44.01)];
            
            //On l'applique à la toolbar
            
            [tools setBackgroundImage:transparentImage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
            
            /*
             tools.barStyle = UIBarStyleBlack;
             tools.translucent = YES;
             tools.opaque = NO;
             tools.backgroundColor = [UIColor clearColor];
             */
            
            NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
            
            //Création du bouton stylo
            UIImage *buttonImage = [UIImage imageNamed:kStylo];
            UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [aButton setImage:buttonImage forState:UIControlStateNormal];
            aButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
            aButton.showsTouchWhenHighlighted = YES;
            
            // Set the Target and Action pour le bouton stylo
            [aButton addTarget:self action:@selector(ajouterMessage) forControlEvents:UIControlEventTouchUpInside];
            
            
            UIBarButtonItem *ajouterMessage = [[UIBarButtonItem alloc] initWithCustomView:aButton];
            
            
            UIButton *aButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage * image;
            
            //si pas de nouveau message bulle normale
            if ([didReceive isEqualToString:@"0"] || [didReceive isEqualToString:@""] || !(didReceive)  )
            {
                image = [UIImage imageNamed:kBulleBarre];
                [aButton1 setTitle:@"" forState:UIControlStateNormal];
            } 
            else    
            {
                //si nouveaux messages bulle rouge
                image = [UIImage imageNamed:kBulleBarreRed];
                [aButton1 setTitle:didReceive forState:UIControlStateNormal];
                [aButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                aButton1.titleLabel.font=[UIFont systemFontOfSize:13];
            }
            
            CGRect frame = CGRectMake(5.0, 0.0, 32.0, 32.0);            
            [aButton1 setBackgroundImage:image forState:UIControlStateNormal];
            aButton1.frame=frame;
            aButton1.showsTouchWhenHighlighted = YES;
            [aButton1 addTarget:self action:@selector(recupererMessages) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *ajouterMessage2 = [[UIBarButtonItem alloc] initWithCustomView:aButton1];
            //[ajouterMessage2 setStyle:UIBarStyleBlackOpaque];
            
            [buttons addObject:ajouterMessage];
            [buttons addObject:ajouterMessage2];
            
            [tools setItems:buttons animated:NO];
            self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView:tools];
        }
        
        [tableView reloadData];
        
    }
}

#pragma mark -
#pragma mark Gestion des messages

-(IBAction)recupererMessages 
{
	DLog(@"");
    //Si on est connecté
    if (!self.estVisiteur) {
        
        //Si on a eu un problème de connexion => on a pas l'iDBlog de l'écran.
        //Attention, c'est pas qu'il n'en a pas ; le soucis est qu'on ne l'a pas récupéré.
        if (self.idBlog == nil)
        {
            [self lancerRecuperation];
        }
        else
        {    
            RecupererMessagesViewController *recupererMessagesViewController = [RecupererMessagesViewController alloc];
            
            recupererMessagesViewController.titreParent   = self.titreParent;
            recupererMessagesViewController.idEcranParent = self.idEcranParent;
            recupererMessagesViewController.idEcran       = self.idEcran;
            recupererMessagesViewController.idBlog        = self.idBlog;
            recupererMessagesViewController.titre         = self.titre;
            recupererMessagesViewController.username      = self.username;
            recupererMessagesViewController.ressourceInfo = self.ressourceInfo;
            recupererMessagesViewController.password      = self.password;
            recupererMessagesViewController.categorie     = self.categorie;
            
            (void)[recupererMessagesViewController initWithNibName:@"RecupererMessagesView" 
                                                      bundle:nil];
            [self.navigationController pushViewController:recupererMessagesViewController
                                                 animated:YES];
            
        }
    }
	
}

-(IBAction)ajouterMessage {
    DLog(@"");
    if (!self.estVisiteur) {
        
        if (self.idBlog == nil)
        {    
            [self lancerRecuperation];
        }
        else
        {    
            
            AddCommentViewController *addCommentViewController = [AddCommentViewController alloc];
            
            addCommentViewController.titreParent   = self.titreParent;
            addCommentViewController.idEcranParent   = self.idEcranParent;
            addCommentViewController.idEcran   = self.idEcran;
            addCommentViewController.idBlog    = self.idBlog;
            addCommentViewController.titre     = self.titre;
            addCommentViewController.username  = self.username;
            addCommentViewController.ressourceInfo = self.ressourceInfo;
            addCommentViewController.password  = self.password;
            addCommentViewController.categorie = self.categorie;
            (void)[addCommentViewController initWithNibName:@"AddCommentView"
                                               bundle:nil];
            [self.navigationController pushViewController:addCommentViewController 
                                                 animated:YES];
            
        } 
    }
}


@end
