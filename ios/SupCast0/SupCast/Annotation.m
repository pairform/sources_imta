//
//  Annotation.m
//  SupCast
//
//  Created by Didier PUTMAN on 17/10/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation


@synthesize titre;
@synthesize soustitre;
@synthesize latitude;
@synthesize longitude;


- (CLLocationCoordinate2D)coordinate;
{
    CLLocationCoordinate2D theCoordinate;
    theCoordinate.latitude = latitude;
    theCoordinate.longitude = longitude;
    return theCoordinate; 
}

// required if you set the MKPinAnnotationView's "canShowCallout" property to YES
- (NSString *)title
{
    return titre;
}

// optional
- (NSString *)subtitle
{
    return soustitre;
}



@end