//
//  CreateUserViewController.m
//  SupCast
//
//  Created by fgutie10 on 28/06/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "CreateUserViewController.h"

#define INTERESTING_TAG_NAMES @"nom", @"message", nil


@implementation CreateUserViewController

@synthesize nomTextField;
@synthesize prenomTextField;
@synthesize eMailTextField;
@synthesize pseudoTextField;
@synthesize motDePasseTextField;
@synthesize nomUtilisateur;
@synthesize prenomUtilisateur;
@synthesize eMailUtilisateur;
@synthesize pseudoUtilisateur;
@synthesize motDePasseUtilisateur;
@synthesize messageString;
@synthesize indicator;
@synthesize receivedData;

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


#pragma mark -
#pragma mark Username Persistence Methods

-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}

#pragma mark -

-(void)changeScreen {
	
	DLog(@"%@",self.messageString);
	
	if ([self.messageString isEqualToString: @"OK"]) {
        
		NSMutableArray *array = [[NSMutableArray alloc] init];
		[array addObject:pseudoTextField.text];
		[array addObject:motDePasseTextField.text];
		
		[array writeToFile:[self dataFilePath:kFilenameData] atomically:YES];
		
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Confirmation d'adresse électronique" message:@"Veuillez quitter cette application et cliquez sur le lien que vous allez recevoir dans votre messagerie pour valider l'enregistrement de votre compte." delegate:nil cancelButtonTitle:@"Accepter" otherButtonTitles:nil];
		/*UIAlertView *alert = [[UIAlertView alloc] initWithTitle @"Confirmation d'adresse électronique" message:@"Veuillez quitter cette application et cliquez sur le lien que vous allez recevoir dans votre messagerie pour valider l'enregistrement de votre compte." cancelButtonTitle:@"Accepter" otherButtonTitles:nil];
         */
		[alert show];
		
		
	}
	
	if ([self.messageString isEqualToString: @"ErreurPseudo"]) {
		
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                        message:@"Votre nom d'utilisateur est déjà utilisé.\nVeuillez en choisir un autre."
                                                       delegate:self
                                              cancelButtonTitle:@"Accepter"
                                              otherButtonTitles:nil];
		
		[alert show];
		
	}
	
	if ([self.messageString isEqualToString: @"ErreurEMail"]) {
		
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Votre adresse mail est déjà utilisée.\nVeuillez en saisir une autre."
													   delegate:self
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
	}
	
}

-(IBAction)launchConnection{
	
    DLog(@"");
	//connexion au serveur, méthode POST - création du fichier XML
	connectionStatus = 1;
	
	NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kcreerSansFichier];
	NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
	[request setHTTPMethod:@"POST"];
	
	NSString *paramDataString = [NSString stringWithFormat:
								 @"id=%@&nom=%@&prenom=%@&email=%@&pseudo=%@&password=%@",
								 kKey,
                                 self.nomUtilisateur,
                                 self.prenomUtilisateur,
                                 self.eMailUtilisateur,
								 self.pseudoUtilisateur,
                                 self.motDePasseUtilisateur];
	
	NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
	[request setHTTPBody:paramData];
	
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];;
	
	if(connection) {
		
		[indicator startAnimating];
		
		self.receivedData = [[NSMutableData alloc] init];
		
	}
	else {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Erreur de connexion"
													   delegate:self
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
	}
	
	doneCreateUser = NO;
	
	//[urlConnexion release];
	
}
/*
 -(void)startParsing
 {
 DLog(@"");
 
 DLog(@"self.receivedData %@",self.receivedData);
 NSXMLParser *messageParser = [[NSXMLParser alloc] initWithData:self.receivedData];
 messageParser.delegate = self;
 [messageParser parse];
 [messageParser release];
 }
 */
#pragma mark -
#pragma mark NSURLConnection Callbacks
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    DLog(@"");
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    DLog(@"");
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    DLog(@"");
    [receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    DLog(@"");
    [receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DLog(@"");
	
	[indicator stopAnimating];	
	self.receivedData = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
    DLog(@"");
    if  (connectionStatus == 1) 
    {
        
        [indicator setHidden:YES];
        [indicator stopAnimating];
        
        NSString *error; 
        NSPropertyListFormat format;
        
        // Initialisation des listes Themes et Etablissements
        // serialisation du fichier Themes.plist en dictionnaire    
        NSDictionary * dico = [NSPropertyListSerialization propertyListFromData:self.receivedData
                                                               mutabilityOption:NSPropertyListImmutable
                                                                         format:&format
                                                               errorDescription:&error];
        
        self.messageString = [[NSMutableString alloc] init];
        [self.messageString setString:@""];
        if(!dico)
        { 
            DLog(@"Error: %@",error); 
            [self.messageString setString:@""];
        }
        else
        {    
            // enumeration de la liste des Themes    
            [self.messageString setString:[dico objectForKey:@"status"]];
            
            if(!doneCreateUser) 
            {			
                doneCreateUser = YES;
                [self changeScreen];
            }
            
        }
        //[indicator stopAnimating];
        //[connection release];
        
        //Lancer appel au parseur
        //[self startParsing];
        
    }
}

#pragma mark -
#pragma mark Parser Methods
/*
 - (void)parserDidStartDocument:(NSXMLParser *)parser {
 
 DLog(@"");
 [messageString release];
 messageString = [[NSMutableString alloc] initWithCapacity: 200];
 currentElementName = nil;
 currentText = nil;
 
 }
 
 - (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
 
 DLog(@"");
 if ([elementName isEqualToString:@"status"]) {
 [currentMessageDict release];
 currentMessageDict = [[NSMutableDictionary alloc] initWithCapacity: [interestingTags count]];
 }
 
 else if ([interestingTags containsObject: elementName]) {
 currentElementName = elementName;
 currentText = [[NSMutableString alloc] init];
 }
 
 }
 
 - (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
 
 DLog(@"");
 if ([elementName isEqualToString:currentElementName]) {
 [currentMessageDict setValue: currentText forKey: currentElementName];
 }
 
 else if ([elementName isEqualToString:@"status"]) {
 
 [messageString appendFormat:@"%@", [currentMessageDict valueForKey:@"message"]];
 
 }
 
 [currentText release];
 currentText = nil;
 
 }
 
 - (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
 DLog(@"");
 [currentText appendString:string];
 }
 
 - (void)parserDidEndDocument:(NSXMLParser *)parser {
 DLog(@"");
 [indicator stopAnimating];
 self.receivedData = nil;
 
 if(!doneCreateUser) 
 {			
 doneCreateUser = YES;
 [self changeScreen];
 }
 }
 */
#pragma mark -

-(IBAction)enregistrer:(id)sender {
    DLog(@"");
	
	//On vérifie que tous les champs du formulaire sont remplis
	self.nomUtilisateur = nomTextField.text;
	self.prenomUtilisateur = prenomTextField.text;
	self.eMailUtilisateur = eMailTextField.text;
	self.pseudoUtilisateur = pseudoTextField.text;
	self.motDePasseUtilisateur = motDePasseTextField.text;
	
	BOOL alerted = NO;
	
	NSString *vide = @"";
	
	if ([self.nomUtilisateur isEqualToString: vide] || [self.prenomUtilisateur isEqualToString: vide] || [self.eMailUtilisateur isEqualToString: vide] ||
		[self.pseudoUtilisateur isEqualToString: vide] || [self.motDePasseUtilisateur isEqualToString: vide]) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez remplir toutes les données du formulaire"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Vérification compte mail
	NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
	NSString *emailUtilisateurMinuscules = [self.eMailUtilisateur lowercaseString];
	BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:emailUtilisateurMinuscules];
	
	if (!myStringMatchesRegEx && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Veuillez saisir une adresse mail valide"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Le pseudo doit comporter au moins 4 caractères (restriction imposée par Elgg)
	if((self.pseudoUtilisateur.length < 4) && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Le nom d'utilisateur doit comporter au moins 4 caractères"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Le mot de passe doit comporter au moins 6 caractères (restriction imposée par Elgg)
	if((self.motDePasseUtilisateur.length < 6) && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Le mot de passe doit comporter au moins 6 caractères"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}
	
	//Vérification nom utilisateur: caractères alphanumériques
	NSString *loginRegEx = @"^[0-9a-zA-Z]*$";
	NSPredicate *loginRegExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", loginRegEx];
	BOOL stringMatchesRegEx = [loginRegExPredicate evaluateWithObject:self.pseudoUtilisateur];
	
	if (!stringMatchesRegEx && !alerted) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Votre nom d'utilisateur doit comporter uniquement des caractères alphanumériques [a-z 0-9]"
													   delegate:self 
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
		alerted = YES;
		
	}	
	
	//Si tout est OK
	if (![self.nomUtilisateur isEqualToString: vide] && ![self.prenomUtilisateur isEqualToString: vide] && ![self.eMailUtilisateur isEqualToString: vide] &&
		![self.pseudoUtilisateur isEqualToString: vide] && ![self.motDePasseUtilisateur isEqualToString: vide] && myStringMatchesRegEx &&
		(self.pseudoUtilisateur.length >= 4) && (self.motDePasseUtilisateur.length >= 6) && stringMatchesRegEx) {
		
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Etes-vous d'accord avec les conditions d'utilisation de cette application ?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Je refuse"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"J'accepte",nil];
        
		[actionSheet showInView:self.view];
        
	}
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if(buttonIndex == 0) {
		
		[nomTextField resignFirstResponder];
		[prenomTextField resignFirstResponder];
		[eMailTextField resignFirstResponder];
		[pseudoTextField resignFirstResponder];
		[motDePasseTextField resignFirstResponder];
		
		[self launchConnection];
		
	}
	else
		[self.navigationController popViewControllerAnimated:YES];
	
}


-(IBAction)textFieldDone:(id)sender {
    DLog(@"");
    [sender resignFirstResponder];
}

- (IBAction)backgroundTap:(id)sender {
    DLog(@"");
	[nomTextField resignFirstResponder];
	[prenomTextField resignFirstResponder];
	[eMailTextField resignFirstResponder];
	[pseudoTextField resignFirstResponder];
	[motDePasseTextField resignFirstResponder];
}

#pragma mark -

-(void)viewDidLoad {
    DLog(@"");
	
	doneCreateUser = NO;
	
	/*
     UIApplication *app = [UIApplication sharedApplication];
     [[NSNotificationCenter defaultCenter] addObserver:self 
     selector:@selector(applicationWillTerminate:) 
     name:UIApplicationWillTerminateNotification
     object:app];
	 */
	
	UIBarButtonItem *boutonEnregistrer = [[UIBarButtonItem alloc] initWithTitle:@"Lancer"
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(enregistrer:)];
    
	self.navigationItem.rightBarButtonItem = boutonEnregistrer;
	
	[indicator stopAnimating];
	interestingTags = [[NSSet alloc] initWithObjects:INTERESTING_TAG_NAMES];
	[super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning {
    DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    DLog(@"");
    [super viewDidUnload];
	self.messageString = nil;
	self.nomTextField = nil;
	self.prenomTextField = nil;
	self.eMailTextField = nil;
	self.pseudoTextField = nil;
	self.motDePasseTextField = nil;
	self.indicator = nil;
}


- (void)dealloc {
    DLog(@"");
    
}

@end