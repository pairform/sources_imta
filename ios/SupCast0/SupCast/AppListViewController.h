//
//  AppListViewController.h
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IconDownloader.h"
#import "Telechargement.h"
#import "GestionFichier.h"
#import "AppRecord.h"
#import "WBProgressHUD.h"
#import "ContentController.h"
#import <CFNetwork/CFNetwork.h>

extern NSString *AppDataDownloadCompleted;

@class ContentController;
@class Telechargement;

@interface AppListViewController : UIViewController <UIScrollViewDelegate, IconDownloaderDelegate>

{

    NSDictionary *critere;
    NSArray *entries;   // the main data model for our UITableView
    NSMutableDictionary *imageDownloadsInProgress;  // the set of IconDownloader objects for each app
	NSMutableArray *appRecords;
    NSURLConnection *appListFeedConnection;
    ContentController *contentController;
	NSMutableData *appListData;
	NSArray *plist;
    // the queue to run our "ParseOperation"
    NSOperationQueue *queue;
    int currentIndex; 
	IBOutlet Telechargement *aTelechargement;
    IBOutlet UITableView *aTableView;
	
    WBProgressHUD   *HUD;
    NSString *password;
    NSString *niveau;
    NSString *username;
    BOOL estVisiteur;
    


}
@property(nonatomic,strong) WBProgressHUD   *HUD;


@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *niveau;
@property(nonatomic,strong) NSString *username;
@property(nonatomic) BOOL estVisiteur;


@property(nonatomic,strong) NSArray *entries;
@property(nonatomic,strong) NSMutableDictionary *imageDownloadsInProgress;
@property(nonatomic,strong) NSMutableArray *appRecords;
@property(nonatomic,strong) NSURLConnection *appListFeedConnection;
@property(nonatomic,strong) NSArray *plist;
@property(nonatomic,strong) NSMutableData *appListData;
@property(nonatomic,strong) NSOperationQueue *queue;
@property(nonatomic,strong) NSDictionary *critere;
@property(nonatomic,strong) ContentController *contentController;

- (void)appImageDidLoad:(NSIndexPath *)indexPath;
- (void)showLoadingProgress;
- (void)hideLoadingProgress;
- (void)cancelConnection;

@end
