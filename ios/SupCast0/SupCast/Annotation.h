//
//  Annotation.h
//  SupCast
//
//  Created by Didier PUTMAN on 17/10/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <MapKit/MapKit.h>
//#import <CoreGraphics/CoreGraphics.h>
//#import <CoreLocation/CoreLocation.h>
//#import <Foundation/Foundation.h>

@protocol MKAnnotation;

@interface Annotation : NSObject <MKAnnotation>
{
    
    NSString * titre;
    NSString * soustitre;
    CLLocationDegrees latitude;
    CLLocationDegrees longitude;
    
}
@property(nonatomic,strong) NSString * titre;
@property(nonatomic,strong) NSString * soustitre;
@property(nonatomic) CLLocationDegrees latitude;
@property(nonatomic) CLLocationDegrees longitude;

@end