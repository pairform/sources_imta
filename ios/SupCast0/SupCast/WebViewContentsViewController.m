//
//  WebViewContentsViewController.m
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import "WebViewContentsViewController.h"
#import "ZoomViewController.h"

@implementation WebViewContentsViewController
@synthesize coordinate;
@synthesize boundingMapRect;
@synthesize labelTitre;
@synthesize toolBar;
@synthesize pageHtml;
@synthesize webView;


#pragma webview delegate 

// La redéfinition de cette fonction va permettre d'ouvrir tous les liens de la page avec le navigateur safari
-            (BOOL)webView:(UIWebView *)webView 
shouldStartLoadWithRequest:(NSURLRequest *)request 
            navigationType:(UIWebViewNavigationType)navigationType {
	// On s'assure que le lien que l'on ouvre est local
    // Sinon, on l'ouvre dans un intent de safari
    
    NSString * urlString = [[NSMutableString alloc] initWithString:[[request URL] absoluteString]];
    NSString *test = [urlString substringToIndex:4];
	
	if (![test isEqualToString:@"file"]) 
    {
		[[UIApplication sharedApplication] openURL:[request URL]];
	}
    
    
    //On check si la requête est une ressource. Si oui, on la push dans le view controller.
    BOOL isPopUpRes = [urlString rangeOfString:@"/res/"].location != NSNotFound ? YES : NO;
	
    BOOL isHTML5 = [urlString rangeOfString:@".eWeb/"].location != NSNotFound ? YES : NO;
   
    if(isPopUpRes && !isHTML5)
    {
        
        
        BOOL isImage = ([urlString rangeOfString:@".jpg"].location != NSNotFound)
                        || ([urlString rangeOfString:@".png"].location != NSNotFound)
                        || ([urlString rangeOfString:@".gif"].location != NSNotFound)
                        ? TRUE : FALSE;        
        
        
        if(isImage)
        {
           
            ZoomViewController * uiVC = [[ZoomViewController alloc] initWithPicture:urlString];
            [self.navigationController pushViewController:uiVC animated:YES];
        }
        else 
        {
            
            UIViewController * uiVC = [[UIViewController alloc] init];
        
            UIWebView * resScenari = [[UIWebView alloc] initWithFrame:self.view.frame];
            [resScenari loadRequest:request];
            [uiVC setView:resScenari];
            [self.navigationController pushViewController:uiVC animated:YES];
        }
                //[uiVC shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortrait];
        

        return NO;
    }
	return YES;
	
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DLog(@"");
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DLog(@"");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DLog(@"");
    
}

#pragma view


- (void)viewDidLoad {
	DLog(@"");
    [super viewDidLoad];
    
   	// fichier HTML
    NSMutableString *imageHTML = [[NSMutableString alloc] init];
    [imageHTML appendString:@"/"]; 
    [imageHTML appendString:[self.ressourceInfo objectForKey:@"ressourcefolder"]]; 
    [imageHTML appendString:@"/WebPath/co/"]; 
    //[imageHTML appendString:@"/co/"]; 
    [imageHTML appendString:[self.pageHtml lastPathComponent]];
    [imageHTML stringByReplacingOccurrencesOfString:@".xml" withString:@".html"];
    NSString * newLink1 = [imageHTML stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    NSString * urlRessource = [[GestionFichier alloc] dataFilePath:newLink1];
    NSURL * mediaUrl = [NSURL fileURLWithPath:urlRessource];
    DLog(@"mediaUrl : %@ ", mediaUrl);
    NSURLRequest * mediaUrlRequest = [[NSURLRequest alloc] initWithURL:mediaUrl];
   // [[NSURLCache sharedURLCache] removeAllCachedResponses];
   // [[NSURLCache sharedURLCache] removeCachedResponseForRequest:mediaUrlRequest];
   
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                            diskCapacity:0
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    self.view.userInteractionEnabled = YES;
    self.webView.userInteractionEnabled = YES;
    self.view.multipleTouchEnabled = YES;
    self.webView.multipleTouchEnabled = YES;
    
    [self.webView setDelegate:self];
    [self.webView loadRequest:mediaUrlRequest];  
    
}

- (void)viewWillDisappear:(BOOL)animated {
	DLog(@"");
    

    
}

- (void)didReceiveMemoryWarning {
	DLog(@"");
    

	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	DLog(@"");
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    self.labelTitre = nil;
    self.toolBar = nil;
    self.pageHtml = nil;
    self.webView = nil;

    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation 
{
	DLog(@"");
	
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if ((UIInterfaceOrientationLandscapeLeft == orientation) ||
            (UIInterfaceOrientationLandscapeRight == orientation)) {
            //[webView setFrame:CGRectMake(0, 0, 480, 320)];
        } 
        else
        {
            //[webView setFrame:CGRectMake(0, 0, 320, 480)];
        }
    }
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
/*    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }*/
    return YES;    
}


- (void)dealloc {
	DLog(@"");
    
	//[contentList release];
    
	
}

@end