//
//  GestionFichier.h
//  SupCast
//
//  Created by Didier PUTMAN on 08/02/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GestionFichier : NSObject {

}

-(NSString *)dataFilePath:(NSString *)fileName;
-(NSString *)dataFilePathAtDirectory:(NSString *)directory
							fileName:(NSString *)fileName;

-(void)addSkipBackupAttributeToPath:(NSString*)path;

@end
