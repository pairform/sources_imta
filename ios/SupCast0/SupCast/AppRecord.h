//
//  AppRecord.h
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright CAPE - Ecole des Mines de Nantes 2010. All rights reserved.
//

@interface AppRecord : NSObject
{
    
    ///////////////////////////
    NSDate *ressourceupdated;
    NSDate *ressourcereleased;
    NSString * ressourceid;
    NSString * ressourcenom;
    NSString * ressourcetitle;
    NSString * ressourceurl;
    NSString * ressourcebaseurl;
    NSString * ressourcefolder;
    NSString * ressourcestructure;
    NSString * ressourceparent;
    NSString * ressourcesummary;
    NSString * ressourceicone;
    NSString * ressourcemedias;
    NSString * ressourceetablissement;
    NSString * ressourcedomaine;
    NSString * ressourcecatalogue;
    NSString * ressourceelggid;
    NSString * ressourceRessourcesWeb;
    NSString * entry;
    NSString * entries;
    ///////////////////////////
    
    // primary page
    NSString *appID;
    NSString *appLink;
	NSString *appTitle;
	NSString *ressourceFolder;
	NSString *appListeMedia;
    NSString *appIcone;
    NSString *artist;
    // Image
    UIImage *appIcon;
    NSData *appIconData;
    // Version
	NSString * ressourceversion1;
    NSString * ressourceversion2;
    NSString * ressourceversion3;
    NSNumber *ressourceTaille;

}

///////////////////////////
@property(nonatomic,strong) NSDate * ressourceupdated;
@property(nonatomic,strong) NSString * ressourceid;
@property(nonatomic,strong) NSString * ressourcenom;
@property(nonatomic,strong) NSString * ressourcetitle;
@property(nonatomic,strong) NSString * ressourceurl;
@property(nonatomic,strong) NSString * ressourcebaseurl;
@property(nonatomic,strong) NSString * ressourceparent;
@property(nonatomic,strong) NSString * ressourcefolder;
@property(nonatomic,strong) NSString * ressourcestructure;
@property(nonatomic,strong) NSString * ressourcesummary;
@property(nonatomic,strong) NSString * ressourceicone;
@property(nonatomic,strong) NSString * ressourcemedias;
@property(nonatomic,strong) NSString * ressourcemedia;
@property(nonatomic,strong) NSString * ressourceetablissement;
@property(nonatomic,strong) NSString * ressourcedomaine;
@property(nonatomic,strong) NSString * ressourceversion1;
@property(nonatomic,strong) NSString * ressourceversion2;
@property(nonatomic,strong) NSString * ressourceversion3;
@property(nonatomic,strong) NSString * ressourcecatalogue;
@property(nonatomic,strong) NSString * ressourceelggid;
@property(nonatomic,strong) NSString * ressourceRessourcesWeb;
@property(nonatomic,strong) NSString * entry;
@property(nonatomic,strong) NSString * entries;
///////////////////////////

// primary page
@property(nonatomic,strong) NSString *appID;
@property(nonatomic,strong) NSString *appLink;
@property(nonatomic,strong) NSString *appTitle;
@property(nonatomic,strong) NSString *ressourceFolder;
@property(nonatomic,strong) NSNumber *ressourceTaille;
@property(nonatomic,strong) NSDate   *ressourcereleased;
@property(nonatomic,strong) NSString *appListeMedia;
@property(nonatomic,strong) NSString *appIcone;
//@property (nonatomic, retain) NSString *imageURLString;
@property(nonatomic,strong) NSString *artist;
// Image
@property(nonatomic,strong) UIImage *appIcon;
@property(nonatomic,strong) NSData *appIconData;

@end