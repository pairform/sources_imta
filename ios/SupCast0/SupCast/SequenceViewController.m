//
//  SequenceViewController.m
//  SupCast
//
//  Created by Phetsana on 20/07/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "GestionFichier.h"
#import "SequenceViewController.h"
#import "TextVideoViewController.h"
#import "AudioViewController.h"
#import "TableViewController.h"
#import "AddCommentViewController.h"
#import "RecupererMessagesViewController.h"

#define kTableViewRowHeight 60

@implementation SequenceViewController

@synthesize label;
@synthesize sequenceContent;
@synthesize labelText;
@synthesize ecranPrecedent;
@synthesize dirRes;
@synthesize dicoRacine;

/*
 -(NSString *)dataFilePath:(NSString *)fileName {
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 return [documentsDirectory stringByAppendingPathComponent:fileName];
 
 }
 */

/*
 - (id)initWithNibName:(NSString *)nibNameOrNil 
 bundle:(NSBundle *)nibBundleOrNil 
 title:(NSString *)title 
 dict:(NSDictionary *)dict {
 DLog(@"");
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 //self.title = title;
 labelText = title;
 sequenceContent = dict;
 }
 
 return self;
 }
 
 */

- (void)viewDidLoad {
	DLog(@"");
	label.text = labelText;
	tableView.rowHeight = kTableViewRowHeight;
	tableView.backgroundColor = [UIColor clearColor];
    [super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	DLog(@"");
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	DLog(@"");
    DLog(@"sequenceContent : %@",sequenceContent);
    return [[sequenceContent objectForKey:kSequenceContent] count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	DLog(@"");
	static NSString *CellIdentifier = @"SequenceCell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier] autorelease];
    }
	
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.numberOfLines = 2;
	cell.textLabel.textColor = [UIColor blackColor];
	cell.textLabel.font = [UIFont fontWithName:kHelvetica	size:11];
	//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	DLog(@"sequenceContent : %@",sequenceContent);
	
	cell.textLabel.text = [[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:@"Item 1"];
	//cell.imageView.image = [UIImage imageNamed:@"pagode-mini.gif"];
	
	cell.imageView.image = [UIImage	imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:@"imageName"]]];
	
	return cell;   
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	DLog(@"");
	
	
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
	
	NSString *leType = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kType] retain];
	NSString *leTitre = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kTitle] retain];
	NSString *lIdEcran = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kIdecran] retain];
	
	
	//	NSString *type = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kType] retain];
	//	NSString *title = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kTitle] retain];
	
	if ([leType isEqualToString:@"text-video"]) {
		
		TextVideoViewController *textVideoViewController = [TextVideoViewController alloc]; 
        textVideoViewController.dirRes = self.dirRes;
        textVideoViewController.dicoRacine = self.dicoRacine;
        
        textVideoViewController.textLabel = self.title;
        textVideoViewController.text = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kText] retain];
        textVideoViewController.videoName = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kVideoName] retain];
        textVideoViewController.videoExtension = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kVideoExtension] retain];
        
        
		[textVideoViewController initWithNibName:@"TextVideoView"
										  bundle:nil];
		
        [self.navigationController pushViewController:textVideoViewController animated:YES];
		[textVideoViewController release];
		
	} else if ([leType isEqualToString:@"audio"]) {
		
		AudioViewController *audioViewController = [AudioViewController alloc]; 
		
        audioViewController.dirRes = self.dirRes;
		audioViewController.dicoRacine = self.dicoRacine;
		audioViewController.textLabel = leTitre;
		audioViewController.itemsList = [[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kItems] retain];
		
        audioViewController = [audioViewController
							   initWithNibName:@"AudioView" 
							   bundle:nil];		
		[self.navigationController pushViewController:audioViewController
                                             animated:YES];
		[audioViewController release];
		
	} else if ([leType isEqualToString:@"exercice"]) {
		
		
	} else if ([leType isEqualToString:@"table"]) {
		
		if(estVisiteur) {
			
			TableViewController *tableViewController = [TableViewController alloc];
			tableViewController.idEcran        = lIdEcran;
            tableViewController.username       = @"";
			tableViewController.password       = @"";
			tableViewController.categorie      = @"";
			tableViewController.ecranPrecedent = @"";
			tableViewController.estVisiteur    = self.estVisiteur;
            tableViewController.ressourceInfo  = self.ressourceInfo;
			tableViewController.titre          = leTitre;
			tableViewController.array          = [[NSArray alloc] initWithArray:[[[sequenceContent objectForKey:kSequenceContent] objectAtIndex:indexPath.row] objectForKey:kItems]];
			[tableViewController initWithNibName:@"TableView" 
										  bundle:nil];
			
			[self.navigationController pushViewController:tableViewController animated:YES];
			[tableViewController release];
			
		}
		
		else {
			
			TableViewController *tableViewController = [TableViewController alloc];
			tableViewController.idEcran        = lIdEcran;
			tableViewController.username       = self.username;
            tableViewController.ressourceInfo  = self.ressourceInfo;
			tableViewController.password       = self.password;
			tableViewController.categorie      = self.categorie;
			tableViewController.ecranPrecedent = [[NSString alloc] initWithFormat:@"%@ - %@",self.ecranPrecedent, labelText];
			tableViewController.estVisiteur    = self.estVisiteur;
			tableViewController.titre          = leTitre;
			tableViewController.array          = [[NSArray alloc] initWithArray:[[[sequenceContent objectForKey:@"sequenceContent"] 
                                                                                  objectAtIndex:indexPath.row] objectForKey:@"items"]];
			
			[tableViewController initWithNibName:@"TableView" 
										  bundle:nil];
			
			[self.navigationController pushViewController:tableViewController 
												 animated:YES];
			[tableViewController release];
			
		}
		
	}
}

#pragma mark -

- (void)dealloc {
	[label release];
	[sequenceContent release];
	[username release];
	[password release];
	[categorie release];
	[ecranPrecedent release];
	[super dealloc];
}


@end

