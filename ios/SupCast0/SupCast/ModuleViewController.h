//
//  ModuleViewController.h
//  SupCast
//
//  Created by Phetsana on 11/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GestionReseauSocialViewController.h"


@interface ModuleViewController : GestionReseauSocialViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UIImageView *fondEcranImageView;
	NSDictionary *moduleContent;
	NSDictionary *dicoRacine;
    NSIndexPath * selectedRowIndexPath;
}
@property(nonatomic,strong) NSIndexPath * selectedRowIndexPath;
@property(nonatomic,strong) NSDictionary *dicoRacine;
@property(nonatomic,strong) NSDictionary *moduleContent;


@end
