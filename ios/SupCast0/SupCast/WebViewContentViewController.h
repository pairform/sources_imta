//
//  WebViewContentViewController.h
//  SupCast
//
//  Created by Phetsana on 22/06/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebViewContentsViewController.h"


@interface WebViewContentViewController : WebViewContentsViewController <UIWebViewDelegate> {
	
}

@end
