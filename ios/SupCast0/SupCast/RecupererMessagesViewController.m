    //
//  RecupererMessagesViewController.m
//  SupCast
//
//  Created by fgutie10 on 28/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "RecupererMessagesViewController.h"

#define INTERESTING_TAG_NAMES @"user", @"commentaire", @"id", @"supprime", @"votes", nil

@implementation RecupererMessagesViewController

@synthesize username;
@synthesize password;
@synthesize ressourceInfo;
@synthesize titreParent;
@synthesize idEcranParent;
@synthesize idEcran;
@synthesize idBlog;
@synthesize categorie;
@synthesize labelTitre;
@synthesize pseudoCategorieTitre;
@synthesize titre;
@synthesize indicator;
@synthesize receivedDataMessages;
@synthesize listeCoor;
@synthesize boutonCreerBlog;
@synthesize plist;
@synthesize listeMessages;
@synthesize connectionMessages;
@synthesize connectionCreerBlog;
@synthesize receivedDataCreerBlog;

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}


-(IBAction)recupererMessages:(NSString *)orderBy:(int)dernierMessageRecupere {
	
    DLog(@"");
	
    boutonMoreComments.hidden = YES;
	
	//Communication avec le serveur (envoyer requête)
	connectionStatus = 1;	
	
    if ( ([self.idBlog isEqualToString:@""]) ||
        ([self.idBlog isEqualToString:@"0"])    )
    {   
        boutonListeBlogs.hidden = NO;
        boutonCreerBlog.hidden  = NO;
        
        [self creerBlog];
        
        
    }
    else    
    {
        
        if (self.idBlog)
        {    
            boutonListeBlogs.hidden = YES;
            boutonCreerBlog.hidden = YES;
            
            NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,krecupererparidBlogRessource];
            
            
            NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
            [request setHTTPMethod:@"POST"];
            
            NSString *paramDataString = [NSString stringWithFormat:
                                         @"id=%@&ressourceid=%@&elggid=%@&idBlog=%@&categorie=%@&pseudo=%@&password=%@&orderBy=%@&last=%d",
                                         kKey,
                                         [self.ressourceInfo objectForKey:kRessourceIdStr],
                                         [self.ressourceInfo objectForKey:kRessourceElggidStr],
                                         self.idBlog,
                                         self.categorie,
                                         self.username,
                                         self.password,
                                         orderBy,
                                         dernierMessageRecupere];
            
            
            DLog(@"paramDataString = %@",paramDataString);
            NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:paramData];
            
            self.connectionMessages = nil;
            self.connectionMessages = [[NSURLConnection alloc] initWithRequest:request delegate:self];;
            
            if(self.connectionMessages) {
                
                [indicator startAnimating];
                self.receivedDataMessages = nil;
                self.receivedDataMessages = [[NSMutableData alloc] init];
                
            }
            else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                                message:@"Erreur de connexion"
                                                               delegate:self
                                                      cancelButtonTitle:@"Accepter"
                                                      otherButtonTitles:nil];
                
                [alert show];
                
            }
            
        }
    }
    
}


#pragma mark -
#pragma mark Action


-(void)creerBlog
{
    DLog(@"");
    if (self.idEcran)
    {    
        connectionStatus = 3;
        NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kaddBlogRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
         
        NSString * blogtitle       = [[NSString alloc] initWithString:self.titre];
        NSString * blogbody        = [[NSString alloc] initWithString:self.titre];
        NSString * blogtags        = @"tags";
        NSString * access_id       = @"2";
        NSString * comments_select = @"select";
        
        NSString *paramDataString = [NSString stringWithFormat:
                                     @"id=%@&idressource=%@&titleparent=%@&idecranparent=%@&idecran=%@&pseudo=%@&password=%@&blogtitle=%@&blogbody=%@&blogtags=%@&access_id=%@&comments_select=%@",
                                     kKey,
                                     [self.ressourceInfo objectForKey:kRessourceElggidStr],
                                     self.titreParent,
                                     self.idEcranParent,
                                     self.idEcran,
                                     self.username,
                                     self.password,
                                     blogtitle,
                                     blogbody,
                                     blogtags,
                                     access_id,
                                     comments_select];
        DLog(@"paramDataString is %@",paramDataString);
        NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:paramData];
        
        self.connectionCreerBlog = nil;
        self.connectionCreerBlog = [[NSURLConnection alloc] initWithRequest:request delegate:self];;
        
        if(self.connectionCreerBlog) {
            
            //[indicator startAnimating];
            
            self.receivedDataCreerBlog = nil;
            self.receivedDataCreerBlog = [[NSMutableData alloc] init];
            //[dataRECU release];
            
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        
    }
    
}

-(IBAction)touchBoutonCreerBlog:(id)sender
{
}


- (IBAction)touchBoutonListeBlogs:(id)sender {
    
    DLog(@"");
    
    GestionBlog *gestionBlog = [GestionBlog alloc];
    
    gestionBlog.username   = self.username;
    gestionBlog.password   = self.password;
    gestionBlog.categorie  = self.categorie;
    gestionBlog.elggid     = [self.ressourceInfo objectForKey:kRessourceElggidStr];
    gestionBlog.idBlog     = self.idBlog;
    gestionBlog.idEcran    = self.idEcran;
    (void)[gestionBlog initWithNibName:@"GestionBlog"
                          bundle:nil];
    [self.navigationController pushViewController:gestionBlog
                                         animated:YES];
    
}



-(IBAction)moreComments
{
	DLog(@"");
	
	int dernierMessage = 20*iteration;
	[self recupererMessages:@"desc":dernierMessage];
	
}


#pragma mark -
#pragma mark NSURLConnection Callbacks
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	DLog(@"");
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    DLog(@"");
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
    DLog(@"");
    if (self.connectionMessages == connection) [receivedDataMessages setLength:0];
    if (self.connectionCreerBlog == connection) [receivedDataCreerBlog setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
    DLog(@"");
    if (self.connectionMessages == connection) [self.receivedDataMessages appendData:data];
    if (self.connectionCreerBlog == connection) [self.receivedDataCreerBlog appendData:data];
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	DLog(@"");
    if (self.connectionMessages == connection)
    {    
        [indicator stopAnimating];	
        self.receivedDataMessages = nil;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
                                                        message:[NSString stringWithFormat:@"Connexion refusée : %@",
                                                                 [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:@"Accepter"
                                              otherButtonTitles:nil];
        
        [alert show];
	}
    if (self.connectionCreerBlog == connection)
    {    
        [indicator stopAnimating];	
        self.receivedDataCreerBlog = nil;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
                                                        message:[NSString stringWithFormat:@"Connexion refusée : %@",
                                                                 [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:@"Accepter"
                                              otherButtonTitles:nil];
        
        [alert show];
	}
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    DLog(@"");
    
    [indicator stopAnimating];
    
    if (self.connectionMessages == connection)
    {
        [indicator stopAnimating];
        //Lancer appel au parseur
        
        NSString *error; 
        NSPropertyListFormat format;
        NSMutableArray * recuperationParseur = [[NSMutableArray alloc] init];    
        recuperationParseur = [NSPropertyListSerialization propertyListFromData:self.receivedDataMessages
                                                               mutabilityOption:NSPropertyListImmutable
                                                                         format:&format
                                                               errorDescription:&error]; 
        self.receivedDataMessages = nil;
        if(!recuperationParseur)
        { 
            DLog(@"Error: %@",error); 
        }
        else
        {
            
            iteration++;
            DLog(@"recuperationParseur is %@",recuperationParseur);
            for (NSDictionary *dico in recuperationParseur) {
                
                NSString *supprime = [dico valueForKey:@"supprime"];
                
                if ([self.categorie isEqualToString:@"sage"] || [self.categorie isEqualToString:@"gardien du temple"])
                {
                    [self.listeMessages addObject:dico];
                }
                else if ([supprime isEqualToString:@""])
                {
                    [self.listeMessages addObject:dico];
                }
                
            }
            
            
            //S'il n'y a pas de messages
            if([self.listeMessages count] == 0)
                pasDeMessages.hidden = NO;
            
            //S'il y a plus de messages à afficher, on montre le bouton
            if(!([recuperationParseur count] < 20) && !([self.listeMessages count] == 0))		
                boutonMoreComments.hidden = NO;
            
            //S'il ne reste aucun message à récupérer
            if(([recuperationParseur count] == 0) && !([self.listeMessages count] == 0)) {
                boutonMoreComments.hidden = NO;
                boutonMoreComments.enabled = NO;
                [boutonMoreComments setAlpha:0.5];
                [boutonMoreComments setTitle:@"Il n'y a plus de messages" forState:UIControlStateDisabled];
                [boutonMoreComments setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
            }
            
            //Ajouter les nouveaux commentaires (avec le format standard) à la table
            [tableView reloadData];
            
            [indicator stopAnimating];
            
            self.receivedDataMessages = nil;
            
        }    
        
        //  [recuperationParseur release];
        
    }
    if (self.connectionCreerBlog == connection)
    {
        // creation blog
        NSString *error; 
        NSPropertyListFormat format;
        self.plist = [[NSMutableArray alloc] init];    
        self.plist = [NSPropertyListSerialization propertyListFromData:self.receivedDataCreerBlog
                                                      mutabilityOption:NSPropertyListImmutable
                                                                format:&format
                                                      errorDescription:&error]; 
        self.receivedDataCreerBlog = nil;
        if(!self.plist)
        { 
            DLog(@"Error: %@",error); 
        }
        else
        {
            DLog(@"plist is %@",plist);

            self.idBlog = [[self.plist objectAtIndex:0] objectForKey:@"blogid"];
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[self.plist objectAtIndex:0] objectForKey:@"status"]
             message:[[self.plist objectAtIndex:0] objectForKey:@"blogid"]
             delegate:self
             cancelButtonTitle:@"Accepter"
             otherButtonTitles:nil];
             [alert show];
             [alert release];*/
            iteration = 0;
            [indicator stopAnimating];
            [[NSNotificationCenter defaultCenter] postNotificationName:kBlogCreationNotification object:self.idBlog];
            
            NSString *orderByDesc = @"desc";
            [self recupererMessages:orderByDesc:0];

        }    
    }
    
}

#pragma mark -

-(void)viewWillAppear:(BOOL)animated
{
    [tableView reloadData];
}

- (void)viewDidLoad {
	DLog(@"");
    boutonMoreComments.hidden = YES;
    
    tableView.backgroundColor = [UIColor clearColor];
    tableView.backgroundView = nil;
    tableView.rowHeight = kCustomRowHeight;
    
    cumul = 0;
    self.listeMessages = [[NSMutableArray alloc] init];    
    
    labelTitre.text = self.titre;
    
    
    NSString *pseudoCategorieTitreTexte = [[NSString alloc] initWithFormat:@"%@",self.username];
    pseudoCategorieTitre.text = pseudoCategorieTitreTexte;
    
    //Si l'écran n'a pas de blog, on va lui en créer un.
    if (([self.idBlog isEqualToString:@""]) || ([self.idBlog isEqualToString:@"0"]) )
    {   
        /*if ([self.username isEqualToString:@"bono"])
         {
         boutonGestionDesBlogs.hidden = NO;
         boutonListeBlogs.hidden = NO;
         }
         
         [pseudoCategorieTitre setHidden:YES];
         
         */
        
        boutonListeBlogs.hidden = YES;
        boutonCreerBlog.hidden  = YES;
        [indicator startAnimating];
        [self creerBlog];
        
        tableView.hidden = YES; 
        //[indicator stopAnimating];
        //[indicator setHidden:YES];
    }
    else    
    {
        //boutonGestionDesBlogs.hidden = YES;
        //boutonListeBlogs.hidden = YES;
        
        
        iteration = 0;
        [indicator stopAnimating];
        
        NSString *orderByDesc = @"desc";
        [self recupererMessages:orderByDesc:0];
    }	
	
}

#pragma mark -
#pragma mark Table View Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    DLog(@"");
    return 1;
}

-(NSInteger)tableView:(UITableView *)aTableView 
numberOfRowsInSection:(NSInteger)section {
    DLog(@"");
    return [self.listeMessages count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLog(@"");
	
	static NSString *TableIdentifier = @"TableIdentifier";
	
	CommentViewCell *cell = (CommentViewCell *)[tableView dequeueReusableCellWithIdentifier:TableIdentifier];
    if (cell == nil) {
		//cell = [[[CommentViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:TableIdentifier] autorelease];
		cell = [[CommentViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableIdentifier];
	}
    cell.accessoryView = nil;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;    
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.backgroundColor = [UIColor whiteColor];
	NSUInteger row = [indexPath row];
    NSDictionary * dico =[[NSDictionary alloc] initWithDictionary:[self.listeMessages objectAtIndex:row]];
    NSString *user = [dico valueForKey:@"user"];
//    NSString *maCategorie = [dico valueForKey:@"categorie"];
    // NSString *monId = [dico valueForKey:@"id"];
    NSDictionary *message = [dico objectForKey:@"message"];
    NSString *contenu = [message valueForKey:@"contenu"];
    NSString *longitude = [message valueForKey:@"longitude"];
    NSString *latitude = [message valueForKey:@"latitude"];
    NSString *votesPositifs = [dico valueForKey:@"votesPositifs"];
    NSString *votesNegatifs = [dico valueForKey:@"votesNegatifs"];
    NSString *supprime = [dico valueForKey:@"supprime"];
    
    CLLocationDegrees newLocationCoordinateLatitude = 0.0;
    CLLocationDegrees newLocationCoordinateLongitude = 0.0;
    
    if (longitude != nil && latitude != nil )
    {
        newLocationCoordinateLatitude = [latitude floatValue];
        newLocationCoordinateLongitude = [longitude floatValue];
        
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;    
        
        UIImage * image = [UIImage imageNamed:@"Localiser.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        button.frame = frame;	// match the button's size with the image size
		
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(mapAffiche:event:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryView = button;
        
    }
    
    [listeCoor addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%f",newLocationCoordinateLatitude],@"lat",[NSString stringWithFormat:@"%f",newLocationCoordinateLongitude],@"long",nil]];
    
	cell.labelUsername.text = [[NSString alloc] initWithFormat:@"%@",user];
    
    
    
    //if ([self.categorie isEqualToString:@"novice"] || [self.categorie isEqualToString:@"apprenti"] || [self.categorie isEqualToString:@"sage"] || [self.categorie isEqualToString:@"gardien du temple"])
    
    	cell.labelCommentaire.text = contenu;
        if (!([supprime isEqualToString:@""])) 
        {                        
            // modification du commentaire pour la suppression
            cell.labelReputation.text = [[NSString alloc] initWithFormat:@"[Supprimé par : %@]",supprime];
            
        }       
        
        //Sinon, on ajoute le nombre de votes positifs et négatifs
        else
        {          
            // modification du commentaire pour les votes            
            cell.labelReputation.text  = [[NSString alloc] initWithFormat:@"[Pertinent : %@ | Non pertinent : %@]",votesPositifs,votesNegatifs];
        }
    
    
	//if (![self.categorie isEqualToString:@"novice"])
	//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	return cell;
	
}

- (void)tableView:(UITableView *)aTableView 
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");
    MapViewController * mapViewController= [MapViewController alloc];
    
	NSUInteger row = [indexPath row];
    NSDictionary * dico =[[NSDictionary alloc] initWithDictionary:[self.listeMessages objectAtIndex:row]];
    NSDictionary *message = [dico objectForKey:@"message"];
    NSString *contenu = [message valueForKey:@"contenu"];
    NSString *longitude = [message valueForKey:@"longitude"];
    NSString *latitude = [message valueForKey:@"latitude"];
    
    NSString *user = [dico valueForKey:@"user"];
    
    if (longitude != nil  && latitude != nil)    
    {
        mapViewController.latitude = [latitude floatValue];
        mapViewController.longitude = [longitude floatValue];
        mapViewController.titre = user;
        mapViewController.soustitre = contenu;
    }   
    
    (void)[mapViewController initWithNibName:@"MapViewController" bundle:nil];
    
    [self.navigationController pushViewController:mapViewController 
										 animated:YES];
}

-(void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");    
    
	NSUInteger row = [indexPath row];
    NSDictionary * dico =[[NSDictionary alloc] initWithDictionary:[self.listeMessages objectAtIndex:row]];
    NSString *user = [dico valueForKey:@"user"];
    NSString *maCategorie = [dico valueForKey:@"categorie"];
    NSString *monId = [dico valueForKey:@"id"];
    NSDictionary *message = [dico objectForKey:@"message"];
    NSString *contenu = [message valueForKey:@"contenu"];
	
    /*
	if([[user lowercaseString] isEqualToString:[self.username lowercaseString]] && !([maCategorie isEqualToString:@"sage)"] || [maCategorie isEqualToString:@"gardien du temple)"]) && (![self.categorie isEqualToString:@"novice"])) 
	{
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
														message:@"Vous ne pouvez pas juger la pertinence de votre message"
													   delegate:self
											  cancelButtonTitle:@"Accepter"
											  otherButtonTitles:nil];
		
		[alert show];
		
	}
	
    
    
	else {
     */
		ModerationMessagesViewController *moderationMessagesViewController = [ModerationMessagesViewController alloc];
		
		moderationMessagesViewController.username      = self.username;
		moderationMessagesViewController.password      = self.password;
		moderationMessagesViewController.categorie     = self.categorie;
		moderationMessagesViewController.postPseudo    = user;
		moderationMessagesViewController.postCategorie = maCategorie;
		moderationMessagesViewController.message       = contenu;
		moderationMessagesViewController.idMessage     = monId;
        moderationMessagesViewController.elggid     = [self.ressourceInfo objectForKey:kRessourceIdStr];

		(void)[moderationMessagesViewController initWithNibName:@"ModerationMessagesView"
												   bundle:nil];
		[self.navigationController pushViewController:moderationMessagesViewController 
											 animated:YES];
	//}
	
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger row = [indexPath row];
    DLog(@"row is %d sur %d",row,[self.listeMessages count]);
    
    NSDictionary * dico =[[NSDictionary alloc] initWithDictionary:[self.listeMessages objectAtIndex:row]];
    // NSString *monId = [dico valueForKey:@"id"];
    NSDictionary *message = [dico objectForKey:@"message"];
    NSString *contenu = [message valueForKey:@"contenu"];
    NSString *votesPositifs = [dico valueForKey:@"votesPositifs"];
    NSString *votesNegatifs = [dico valueForKey:@"votesNegatifs"];
    NSString *supprime = [dico valueForKey:@"supprime"];
    NSString *longitude = [message valueForKey:@"longitude"];
    NSString *latitude = [message valueForKey:@"latitude"];
    
    NSString * monText;
    
    if ([self.categorie isEqualToString:@"sage"] || [self.categorie isEqualToString:@"gardien du temple"])
    {
        if (!([supprime isEqualToString:@""])) 
        {                        
            // modification du commentaire pour la suppression
            monText = [[NSString alloc] initWithFormat:@"[Supprimé par : %@]\n%@",supprime,contenu];
            
        }       
        
        //Sinon, on ajoute le nombre de votes positifs et négatifs
        else
        {          
            // modification du commentaire pour les votes            
            monText  = [[NSString alloc] initWithFormat:@"[Pertinent : %@ | Non pertinent : %@]\n%@",votesPositifs,votesNegatifs,contenu];
        }
    }
    else
    {
    	monText = contenu;
        
    }   
    
    
    
	int sautDeLigne = 66;
	
	//if ([self.categorie isEqualToString:@"sage"] || [self.categorie isEqualToString:@"gardien du temple"])
	//	sautDeLigne = 80;
	
    CGFloat largeur;
    if (longitude != nil && latitude != nil )
    {
        largeur = self.view.frame.size.width-50;
    }
    else
    {
        largeur = self.view.frame.size.width-20;
    }        
    
    return [CommentViewCell cellHeightForCaption:monText width:largeur] + sautDeLigne;
	
}


-(IBAction)mapAffiche:(id)sender event:(id)event
{
    DLog(@"");
    
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tableView];
	NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
	}
}

#pragma mark -

- (void)didReceiveMemoryWarning {
   	DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload 
{
  	DLog(@"");
    [super viewDidUnload];
    self.boutonCreerBlog = nil;
    self.indicator = nil;
	self.labelTitre = nil;
	self.indicator = nil;
    self.plist = nil;
    self.listeMessages = nil;
}

- (void)dealloc 
{
   	DLog(@"");
    //  [boutonCreerBlog release];
    
    
}


@end
