//
//  TableViewController.h
//  SupCast
//
//  Created by Phetsana on 19/08/09.
//  Copyright (c) 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GestionReseauSocialViewController.h"


@interface TableViewController : GestionReseauSocialViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UIImageView *fondEcranImageView;
	NSArray *array;
	
	IBOutlet UILabel *labelTitle;
	
	NSString *ecranPrecedent;
	NSString *dirRes;

	NSDictionary *dicoRacine;
	NSString *idecran;
	
}
@property (nonatomic, retain) NSString *idecran;

@property (nonatomic, retain) NSDictionary *dicoRacine;
@property (nonatomic, retain) NSArray *array;

@property (nonatomic, retain) NSString *dirRes;
@property (nonatomic, retain) IBOutlet UILabel *labelTitle;

@property (nonatomic, retain) NSString *ecranPrecedent;

- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil 
				title:(NSString *)titreItem
				array:(NSArray *)a;

@end
