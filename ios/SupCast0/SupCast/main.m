//
//  main.m
//  SupCast
//
//  Created by Didier PUTMAN on 27/05/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SupCastAppDelegate.h"

int main(int argc, char *argv[]) {
   
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, NSStringFromClass([SupCastAppDelegate class]));
        return retVal;
    }
}
