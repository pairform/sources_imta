//
//  IconDownloader.h
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright CAPE - Ecole des Mines de Nantes 2010. All rights reserved.
//

@class AppRecord;

@protocol IconDownloaderDelegate;

@interface IconDownloader : NSObject
{
    AppRecord *appRecord;
    NSIndexPath *indexPathInTableView;
    id <IconDownloaderDelegate> __unsafe_unretained delegate;
    
    NSMutableData *activeDownload;
    NSURLConnection *imageConnection;
}

@property(nonatomic,strong) AppRecord *appRecord;
@property(nonatomic,strong) NSIndexPath *indexPathInTableView;
@property (nonatomic, unsafe_unretained) id <IconDownloaderDelegate> delegate;

@property(nonatomic,strong) NSMutableData *activeDownload;
@property(nonatomic,strong) NSURLConnection *imageConnection;

- (void)startDownload;
- (void)cancelDownload;

@end

@protocol IconDownloaderDelegate 

- (void)appImageDidLoad:(NSIndexPath *)indexPath;

@end