//
//  SupCastAppDelegate.m
//  SupCast
//
//  Created by Didier PUTMAN on 27/05/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "SupCastAppDelegate.h"
#import "WelcomeViewController.h"

@implementation SupCastAppDelegate

@synthesize window=_window;
@synthesize welcomeViewController = _welcomeViewController;

@synthesize navigationController=_navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    // Add the navigation controller's view to the window and display.
    //   [self.window makeKeyAndVisible];
    //   return YES;
    
	_navigationController = [[UINavigationController alloc] init];
	_navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
	
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        _welcomeViewController = [[WelcomeViewController alloc] initWithNibName:@"WelcomeView-iPad" bundle:nil];
    else
        _welcomeViewController = [[WelcomeViewController alloc] initWithNibName:@"WelcomeView" bundle:nil];
        
	
    self.window.rootViewController = self.navigationController;
    
    
    [_navigationController pushViewController:_welcomeViewController 
                                     animated:NO];
    [_window addSubview:_navigationController.view];
    
	
    
    // Override point for customization after application launch
    [_window makeKeyAndVisible];
    
    //TODONE:Checker le bon fonctionnement de ce module
    //TODONE:Ajouter un shared preference de date de modif du plist
    //Recup des infos
    
    int updateInfo = [[NSUserDefaults standardUserDefaults] integerForKey:@"updateInfo"];
    
    NSString * urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLSupCast,kRecupInfo];
    
    NSURL *url =[[NSURL alloc] initWithString:urlConnexion];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString * paramString = [[NSString alloc] initWithFormat:@"timestamp=%d",updateInfo];
    NSData *paramData = [[NSData alloc]initWithData:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:paramData];
    
    [_welcomeViewController setReceivedDataInfos:[[NSMutableData alloc] init]];
    
    [_welcomeViewController setInfosConnection:[[NSURLConnection alloc] initWithRequest:request delegate:_welcomeViewController]]; 
    
    return YES;
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{  
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}/*

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if(connection == connectionInfos)
        [receivedDataInfos setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if(connection == connectionInfos)
        [receivedDataInfos appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
    if(connection == connectionInfos)
    {
        NSString * error; 
        NSPropertyListFormat format; 
        NSArray * arrayInfosDynamique = [NSPropertyListSerialization propertyListFromData:receivedDataInfos
                                                       mutabilityOption:NSPropertyListImmutable
                                                                 format:&format
                                                       errorDescription:&error]; 
        connectionInfos = nil;
        
        //Si le webservice a renvoyé un pList,on update les infos
        if(arrayInfosDynamique)
        {
            //On met le timestamp d'aujourd'hui comme date de MAJ dans les userdefaults
            NSInteger newUpdateInfo = [[NSDate date] timeIntervalSince1970];
            [[NSUserDefaults standardUserDefaults] setInteger:newUpdateInfo forKey:@"updateInfo"];
            [[NSUserDefaults standardUserDefaults] setObject:arrayInfosDynamique forKey:@"arrayInfo"];
            
        }
    }
}

*/
@end
