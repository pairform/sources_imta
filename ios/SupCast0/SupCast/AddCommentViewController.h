//
//  AddCommentViewController.h
//  SupCast
//
//  Created by fgutie10 on 22/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface AddCommentViewController : UIViewController <UIActionSheetDelegate,CLLocationManagerDelegate, UITextViewDelegate,MKMapViewDelegate> {
		
	IBOutlet UIButton *boutonValider;
	
    NSDictionary *ressourceInfo;
    int connectionStatus;
    NSURLConnection *connectionCreerBlog;
    NSMutableData *receivedDataCreerBlog;
    NSURLConnection *connectionPublier;
}
@property(nonatomic) BOOL  statusPublicationGPS;
@property(nonatomic) BOOL  statusPublicationTwitter;
@property(nonatomic) BOOL  statusPublicationGPSLocal;
@property(nonatomic) BOOL  statusPublicationTwitterLocal;
@property(nonatomic,strong) NSMutableString * motdepasseTwitter;
@property(nonatomic,strong) NSMutableString * userTwitter;

@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSDictionary *ressourceInfo;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *titreParent;
@property(nonatomic,strong) NSString *idEcranParent;
@property(nonatomic,strong) NSString *idEcran;
@property(nonatomic,strong) NSString *idBlog;
@property(nonatomic,strong) NSString *categorie;
@property(nonatomic) BOOL estExercice;

@property(nonatomic,strong) NSString *titre;
@property (nonatomic, weak) IBOutlet UILabel *labelTitre;
@property (nonatomic, weak) IBOutlet UILabel *pseudoCategorieTitre;

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIButton *boutonProposerExercice;
@property (nonatomic, weak) IBOutlet UIButton *boutonCommenterExercice;
@property (nonatomic, weak) IBOutlet UIButton *boutonPublier;
@property (nonatomic, weak) IBOutlet UIButton *boutonTwitter;
@property (nonatomic, weak) IBOutlet UIButton *boutonLocalisation;

@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic,strong) CLLocation *coordonneesGPS;

@property(nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;
@property(nonatomic,strong) NSMutableData *receivedData;

-(IBAction)activeLocalisation:(id)sender;
-(IBAction)activeTwitter:(id)sender;
-(IBAction)gestionClavier;
-(IBAction)backgroundTap:(id)sender;
-(IBAction)sendMessage;
-(void)creerBlog;

-(void)publierMessageAvecLocalisation:(NSString *)aMessage;
//-(void)tweet:(NSString *)aMessage;
-(void)gestionBouton;
-(void)publierMessage:(NSString *)aMessage;

@end
