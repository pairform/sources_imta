//
//  ZoomFSViewController.m
//  SupCast
//
//  Created by Cape EMN on 30/10/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "ZoomFSViewController.h"

@implementation ZoomFSViewController

@synthesize imageView, imagePath = _imagePath, scrollZoomView,delegate;

-(id)initWithPicture:(NSString *)imagePath
{
    DLog(@"");
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _imagePath = imagePath;
        self.title = @"Zoom";
    }
    return self;
}

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    
    // Initialisation de l'image
    _imagePath = [_imagePath stringByReplacingOccurrencesOfString:@"file:///" withString:@"/"];
    _imagePath = [_imagePath stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    UIImage * imageRessource = [[UIImage alloc] initWithContentsOfFile:_imagePath];
    
    [self.imageView setImage:imageRessource];
    //scrollZoomView.minimumZoomScale = scrollZoomView.frame.size.width / imageView.frame.size.width;
    
}

- (void)viewDidUnload
{
    DLog(@"");
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)clicBouton:(id)sender
{
    [self.delegate dismissZoomFS];
}
@end
