//
//  ExercicePartCell.h
//  SupCast
//
//  Created by Phetsana on 17/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ExercicePartCell : UITableViewCell {
	UILabel *label;
    NSString *dirRes;
}

@property (nonatomic, retain) NSString *dirRes;
@property (nonatomic, retain) UILabel *label;

+ (CGFloat)cellHeightForCaption:(NSString *)text 
						  width:(CGFloat)width;

@end
