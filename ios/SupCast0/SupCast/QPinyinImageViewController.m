//
//  QPinyinImageViewController.m
//  SupCast
//
//  Created by Phetsana on 26/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "GestionFichier.h"
#import "QPinyinImageViewController.h"

@implementation QPinyinImageViewController

//@synthesize dirRes;
@synthesize dicoRacine;
/*
-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}
*/ 

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	  
	[fondEcranImageView setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes 
																						   fileName:[self.dicoRacine objectForKey:kImageFondEcran]]]];
	
	/* Calcul de position et de dimension */
	UIFont *captionFont =[UIFont fontWithName:kVerdana size:14];
	CGSize cs = [indication sizeWithFont:captionFont 
					   constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
						   lineBreakMode:UILineBreakModeWordWrap];
	height = cs.height + 60; 
	
	indicationTableView.frame = CGRectMake(indicationTableView.frame.origin.x, 
										   indicationTableView.frame.origin.y, 
										   indicationTableView.frame.size.width, 
										   height + 22);
	
	label.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kText];
	//imageView.image = [UIImage imageNamed:[[NSString alloc] initWithFormat:@"%@.%@", 
	//									   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageName], 
	//									   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageExtension]]];

	imageView.image = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[[NSString alloc] initWithFormat:@"%@.%@", 
										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageName], 
										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageExtension]]]];
	
	activeField = textField;
	
	imageView.frame = CGRectMake(imageView.frame.origin.x, 
								 indicationTableView.frame.origin.y + indicationTableView.frame.size.height + 10, 
								 imageView.frame.size.width, 
								 imageView.frame.size.height);
	
	label.frame = CGRectMake(label.frame.origin.x, 
							 imageView.frame.origin.y + imageView.frame.size.height + 10, 
							 label.frame.size.width, 
							 label.frame.size.height);
	
	textField.frame = CGRectMake(textField.frame.origin.x, 
								 imageView.frame.origin.y + imageView.frame.size.height + 10, 
								 textField.frame.size.width, 
								 textField.frame.size.height);
	
	mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, 
											label.frame.origin.y + label.frame.size.height + 20);
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)update {
	[super update];
	label.text = [[questionList objectAtIndex:indexQuestion] objectForKey:kText];
	//	imageView.image = [UIImage imageNamed:[[NSString alloc] initWithFormat:@"%@.%@", 
	//										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageName], 
	//										   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageExtension]]];
	

	imageView.image = [UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[[NSString alloc] initWithFormat:@"%@.%@", 
																		   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageName], 
																		   [[questionList objectAtIndex:indexQuestion] objectForKey:kImageExtension]]]];
	
	textField.text = @"";
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	
	cell.textLabel.font = [UIFont fontWithName:kVerdana size:14];
	cell.textLabel.text = indication;
	
	return cell;        	
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSInteger result;
	UIFont *captionFont;
	CGSize cs;
	
	captionFont = [UIFont fontWithName:kVerdana size:14];
	cs = [indication sizeWithFont:captionFont 
				constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
					lineBreakMode:UILineBreakModeWordWrap];
	result = cs.height + 60;
	
	return result;
}


- (void)dealloc {
	[label release];
	[imageView release];
	[super dealloc];
}


@end
