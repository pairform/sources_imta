//
//  TextVideoViewController.m
//  SupCast
//
//  Created by Phetsana on 15/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "GestionFichier.h"
#import "TextVideoViewController.h"
#import "Movie.h"

#define kFontSize 20

@implementation TextVideoViewController
@synthesize label,dirRes,dicoRacine,textLabel,text,videoName,videoExtension;

- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil {
	
    self = [super initWithNibName:nibNameOrNil 
						   bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
	
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    [super viewDidLoad];    
    
	[fondEcranImageView setImage:[UIImage imageWithContentsOfFile:[[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[self.dicoRacine objectForKey:kImageFondEcran]]]];
    
	label.text = textLabel;	
	tableView.backgroundColor = [UIColor clearColor];
	
	movie = [Movie alloc];
	movie.dirRes = self.dirRes;
	[movie initMoviePlayerWithName:videoName 
						 extension:videoExtension];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (IBAction)playMovie:(id)sender {
    /*
	 
     As soon as you call the play: method, the player initiates a transition that fades 
     the screen from your current window content to the designated background 
     color of the player. If the movie cannot begin playing immediately, the player 
     object continues displaying the background color and may also display a progress 
     indicator to let the user know the movie is loading. When playback finishes, the 
     player uses another fade effect to transition back to your window content.
     
     */
	
    [movie playMovie];
}

#pragma mark -
#pragma mark TableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TextVideoCell";
	
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier] autorelease];
    }
	
	cell.textLabel.font = [UIFont fontWithName:kHelvetica size:16];
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.text = text;
	cell.textLabel.numberOfLines = 0;
	
	return cell;        	
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[aTableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	UIFont *captionFont = [UIFont boldSystemFontOfSize:kFontSize];
    CGSize cs = [text sizeWithFont:captionFont 
				 constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX) 
					 lineBreakMode:UILineBreakModeWordWrap];
	
	return cs.height;
}

#pragma mark -

- (void)dealloc {
	[label release];
	[movie release];
	[videoName release];
	[videoExtension release];
	[text release];
    [super dealloc];
}

@end
