//
//  MapViewController.h
//  SupCast
//
//  Created by CAPE on 11/07/11.
//  Copyright (c) 2011 ecole des mines de nantes. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>


@interface MapViewController : UIViewController
{
    CLLocationDegrees latitude;
    CLLocationDegrees longitude;
    IBOutlet UISegmentedControl *typeSelector;
    NSString * titre;
    NSString * soustitre;
  
}
@property(nonatomic,strong) NSString * titre;
@property(nonatomic,strong) NSString * soustitre;
@property(nonatomic) CLLocationDegrees latitude;
@property(nonatomic) CLLocationDegrees longitude;

@property (nonatomic, weak) IBOutlet MKMapView *MapView;

-(IBAction)changeMap:(id)sender;
@end
