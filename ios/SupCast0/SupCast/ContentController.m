//
//  ContentController.m
//  SupCast
//
//  Created by Didier PUTMAN on 22/11/10.
//  Copyright CAPE - Ecole des Mines de Nantes 2010. All rights reserved.
//

#import "ContentController.h"

@implementation ContentController

@synthesize detailItem, navigationController;

- (void)dealloc
{
	DLog(@"");

}

- (UIView *)view
{
	DLog(@"");
    return self.navigationController.view;
}

@end
