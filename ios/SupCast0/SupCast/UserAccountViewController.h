//
//  UserAccountViewController.h
//  SupCast
//
//  Created by wbao11 on 12/07/11.
//  Copyright (c) 2011 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangePasswordViewController.h"
#import "UserInfosViewController.h"

@class ChangePasswordViewController;
@class UserInfosViewController;
@interface UserAccountViewController : UIViewController <UITabBarDelegate> {
    UIButton *button;
    NSString *username;
	NSString *password;
    
    IBOutlet UITabBarItem *compteBarTab;
    IBOutlet UITabBarItem *motdepasseBarTab;
}
@property (nonatomic, strong) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, weak) IBOutlet UserInfosViewController *userInfosViewController;
@property (nonatomic, weak) IBOutlet ChangePasswordViewController *changePasswordViewController;

@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *password;
@property ( nonatomic, weak) IBOutlet UITabBar *tabBar;

@end
