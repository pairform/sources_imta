//
//  AudioCell.m
//  SupCast
//
//  Created by Phetsana on 16/06/09.
//  Copyright 2009 EMN - CAPE. All rights reserved.
//

#import "GestionFichier.h"
#import "AudioCell.h"

@implementation AudioCell
@synthesize primaryLabel;
@synthesize myButton1;
@synthesize myButton2;
@synthesize audioNameVersion1;
@synthesize audioNameVersion2;
@synthesize audioVersion1Extension;
@synthesize audioVersion2Extension;
@synthesize player;
@synthesize dirRes;

+ (UIButton *)buttonWithTitle:	(NSString *)title
					   target:(id)target
					 selector:(SEL)selector
						frame:(CGRect)frame
						image:(UIImage *)image
				 imagePressed:(UIImage *)imagePressed
				darkTextColor:(BOOL)darkTextColor {	
	
	DLog(@"");
	UIButton *button = [[UIButton alloc] initWithFrame:frame];
	
	button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
	
	[button setTitle:title forState:UIControlStateNormal];	
	if (darkTextColor) {
		[button setTitleColor:[UIColor blackColor] 
					 forState:UIControlStateNormal];
	} else {
		[button setTitleColor:[UIColor whiteColor] 
					 forState:UIControlStateNormal];
	}
	
	UIImage *newImage = [image stretchableImageWithLeftCapWidth:12.0 
												   topCapHeight:0.0];
	[button setBackgroundImage:newImage 
					  forState:UIControlStateNormal];
	
	UIImage *newPressedImage = [imagePressed stretchableImageWithLeftCapWidth:12.0 
																 topCapHeight:0.0];
	[button setBackgroundImage:newPressedImage 
					  forState:UIControlStateHighlighted];
	[button addTarget:target 
			   action:selector 
	 forControlEvents:UIControlEventTouchUpInside];
	
    // in case the parent view draws with a custom color or gradient, use a transparent color
	button.backgroundColor = [UIColor clearColor];
	
	return button;
}

/*
 -(NSString *)dataFilePath:(NSString *)fileName 
 {    
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 return [documentsDirectory stringByAppendingPathComponent:fileName];
 
 }
 */

- (void)playSoundVersion1 {
	DLog(@"");
	/*	NSString *path = [[NSBundle mainBundle] pathForResource:self.audioNameVersion1 
	 ofType:self.audioVersion1Extension];
	 */
	//NSString * myPath =[[NSBundle mainBundle] resourcePath];
	//NSString *path = [myPath stringByAppendingPathComponent:[self.audioNameVersion1 stringByAppendingPathExtension:self.audioVersion1Extension]];
	NSString * path = [[[GestionFichier alloc] autorelease] dataFilePathAtDirectory:self.dirRes fileName:[self.audioNameVersion1 stringByAppendingPathExtension:self.audioVersion1Extension]];
	NSURL *url = [NSURL fileURLWithPath:path];
	
	// Initialisation de AVAudioPlayer
	AVAudioPlayer* tmpPlayer;
	tmpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
	
	self.player = tmpPlayer;
	[tmpPlayer release];
	[self.player play];
}

- (void)playSoundVersion2 {
	DLog(@"");
	/*	NSString *path = [[NSBundle mainBundle] pathForResource:self.audioNameVersion2 
	 ofType:self.audioVersion2Extension];*/
	//NSString * myPath =[[NSBundle mainBundle] resourcePath];
	//NSString *path = [myPath stringByAppendingPathComponent:[self.audioNameVersion2 stringByAppendingPathExtension:self.audioVersion2Extension]];
	NSString * path = [[[GestionFichier alloc] autorelease]  dataFilePathAtDirectory:self.dirRes fileName:[self.audioNameVersion2 stringByAppendingPathExtension:self.audioVersion2Extension]];
	
	NSURL *url = [NSURL fileURLWithPath:path];
	
	// Initialisation de AVAudioPlayer
	AVAudioPlayer* tmpPlayer;
	tmpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
	
	self.player = tmpPlayer;
	[tmpPlayer release];
	[self.player play];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
	DLog(@"");
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {        
		self.primaryLabel = [[UILabel alloc] init];
		self.primaryLabel.textAlignment = UITextAlignmentLeft;
		self.primaryLabel.font = [UIFont systemFontOfSize:14];
		
		CGRect frame = CGRectMake(0.0, 0.0, 32, 32);
		
		self.myButton1 = [AudioCell buttonWithTitle:@""
											 target:self
										   selector:@selector(playSoundVersion1)
											  frame:frame
											  image:nil
									   imagePressed:nil
									  darkTextColor:YES];
		
		//[self.myButton1 setImage:[UIImage imageNamed:@"Triangle_rg.png"] forState:UIControlStateNormal];
		
		
		
		
		frame = CGRectMake(0.0, 0.0, 32, 32);
		
		self.myButton2 = [AudioCell buttonWithTitle:@""
											 target:self
										   selector:@selector(playSoundVersion2)
											  frame:frame
											  image:nil
									   imagePressed:nil
									  darkTextColor:YES];
		
		//[self.myButton2 setImage:[UIImage imageNamed:[self.dicoRacine objectForKey:kImageTrianglebc]] forState:UIControlStateNormal];
		
		
		[self.contentView addSubview:self.primaryLabel];
		[self.contentView addSubview:self.myButton1];
		[self.contentView addSubview:self.myButton2];
    }
    return self;
}

- (void)layoutSubviews {
	
	DLog(@"");
	[super layoutSubviews];
	
	CGRect contentRect = self.contentView.bounds;
	
	CGFloat boundsX = contentRect.origin.x;
	CGFloat cellHeight = self.contentView.bounds.size.height;
	
	CGRect frame;
	
	frame= CGRectMake(boundsX + 10, cellHeight / 2 - 25, 200, 50);
	
	self.primaryLabel.frame = frame;
	
	frame= CGRectMake(boundsX + 216, cellHeight / 2 - 16, 32, 32);
	
	self.myButton1.frame = frame;
	
	frame= CGRectMake(boundsX + 258, cellHeight / 2 - 16, 32, 32);
	
	self.myButton2.frame = frame;
}

- (void)setAudioName:(NSString *)audioName 
	   extensionName:(NSString *)extensionName 
			 version:(int)version {
	DLog(@"");
	if (version == 0) {
		self.audioNameVersion1 = audioName;
		self.audioVersion1Extension = extensionName;
	} else {
		if ([audioName isEqualToString:@""]) {
			self.myButton1.alpha = 0;
			self.audioNameVersion2 = self.audioNameVersion1;
			self.audioVersion2Extension = self.audioVersion1Extension;
		} else {
			self.myButton1.alpha = 1;
			self.audioNameVersion2 = audioName;
			self.audioVersion2Extension = extensionName;
		}
	}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	DLog(@"");
    [super setSelected:selected animated:animated];
}

- (void)dealloc {
	DLog(@"");
	[self.primaryLabel release];
	[self.myButton1 release];
	[self.myButton2 release];
	[self.audioNameVersion1 release];
	[self.audioNameVersion2 release];
	[self.audioVersion1Extension release];
	[self.audioVersion2Extension release];
	[self.player release];
    [super dealloc];
}

@end
