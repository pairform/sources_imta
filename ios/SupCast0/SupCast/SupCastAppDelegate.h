//
//  SupCastAppDelegate.h
//  SupCast
//
//  Created by Didier PUTMAN on 27/05/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WelcomeViewController;

@interface SupCastAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UINavigationController *navigationController;

@property ( nonatomic, strong) IBOutlet WelcomeViewController *welcomeViewController;
@end
