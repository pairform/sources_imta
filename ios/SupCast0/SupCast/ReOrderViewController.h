//
//  ReOrderViewController.h
//  SupCast
//
//  Created by Phetsana on 08/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractAssociationExerciceViewController.h"

@interface ReOrderViewController : AbstractAssociationExerciceViewController {
	NSMutableString *userAnswerString; // Chaîne de caractère contenant l'ordre des réponses de l'utilisateur
	NSMutableArray *indexOfObjectUsed; /* Tableau pour connaître la correspondance 
	 de la position dans la roulette et la position dans l'ordre des réponses */
	NSMutableArray *userAnswerList; // Liste des réponses utilisateur
	int indiceNumber;
	NSString *type;
	IBOutlet UITableView *indicationTableView;
}
- (IBAction)deleteLastChar;
- (IBAction)indice;

@end
