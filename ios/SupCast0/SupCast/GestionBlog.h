//
//  GestionBlog.h
//  SupCast
//
//  Created by Didier PUTMAN on 09/09/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GestionBlog : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    NSString *elggid;
    NSString *username;
	NSString *password;
	NSString *idEcran;
	NSString *idBlog;
	NSString *categorie;
	NSString *titre;
	NSMutableArray *recuperationParseur;
	NSMutableDictionary *dictionnaire;
	NSString *currentElementName;
	NSMutableString *currentText;
	int iteration;
    int connectionStatus;

	NSSet *interestingTags;

    NSMutableData *receivedData;
    NSMutableArray *plist;
	IBOutlet UITableView *aTableView;
}
@property(nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;
@property(nonatomic,strong) NSMutableArray *plist;
@property(nonatomic,strong) NSMutableData *receivedData;
@property(nonatomic,strong) NSString *elggid;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *idEcran;
@property(nonatomic,strong) NSString *idBlog;
@property(nonatomic,strong) NSString *categorie;
//-(void)startParsing;

@end
