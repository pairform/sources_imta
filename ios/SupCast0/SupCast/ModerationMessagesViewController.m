//
//  ModerationMessagesViewController.m
//  SupCast
//
//  Created by fgutie10 on 02/08/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "ModerationMessagesViewController.h"

#define INTERESTING_TAG_NAMES @"dejaVote", @"supprime", nil


@implementation ModerationMessagesViewController

@synthesize elggid,username, password, categorie, idMessage;
@synthesize boutonOUI, boutonNON, boutonSupprimer;
@synthesize pseudoNiveau, champMessage, message, messageSortie, messageSupprimeLigne1;
@synthesize receivedData, indicator;
@synthesize postPseudo;
@synthesize postCategorie;

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

-(IBAction)supprimerMessage {
	
	connectionStatus = 0;
	
    if (self.idMessage && self.elggid)
    {    
        
        
        NSString *urlSupprimer = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,ksupprimerRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlSupprimer];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        
        
        /*
         NSString *paramDataString = [NSString stringWithFormat:
                                     @"id=%@&idressource=%@&pseudo=%@&password=%@&id=%@",
                                     kKey,
                                     self.elggid,
                                     self.username,
                                     self.password,
                                     self.idMessage];
        */
        NSString *paramDataString = [NSString stringWithFormat:
                                     @"idressource=%@&pseudo=%@&password=%@&id=%@",
                                     self.elggid,
                                     self.username,
                                     self.password,
                                     self.idMessage];
        
        NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:paramData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if(connection) {
            
            [indicator startAnimating];
            
            NSMutableData *data = [[NSMutableData alloc] init];
            self.receivedData = data;
            
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        
    }
}

-(IBAction)voterMessage:(id)sender {
	
    if (self.idMessage && self.elggid)
    {    
        if (sender == boutonOUI)
            vote = @"OUI";
        if (sender == boutonNON)
            vote = @"NON";
        
        //Communication avec le serveur
        connectionStatus = 0;
        
        NSString *urlVoter = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kvoterRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlVoter];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        NSString *paramDataString = [NSString stringWithFormat:
                                     @"id=%@&idressource=%@&pseudo=%@&password=%@&id=%@&vote=%@", 
                                     kKey,
                                     self.elggid,
                                     self.username,
                                     self.password,
                                     self.idMessage,
                                     vote];
        
        NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:paramData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if(connection) {
            
            [indicator startAnimating];
            
            NSMutableData *data = [[NSMutableData alloc] init];
            self.receivedData = data;
            
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        
        
    }
}

-(void)initialiserEcran
{
	//Communication avec le serveur (envoyer requête)
	connectionStatus = 1;
	
    
    
    if (self.idMessage && self.elggid)
    {    
        NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kinitSansFichierRessource];
        NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        NSString *paramDataString = [NSString stringWithFormat: 
                                     @"id=%@&idressource=%@&pseudo=%@&id=%@", 
                                     kKey,
                                     self.elggid,
                                     self.username,
                                     self.idMessage];
        
        NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:paramData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if(connection) 
        {
            
            [indicator startAnimating];
            self.receivedData = [[NSMutableData alloc] init];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                            message:@"Erreur de connexion"
                                                           delegate:self
                                                  cancelButtonTitle:@"Accepter"
                                                  otherButtonTitles:nil];
            
            [alert show];
        }
        
        
    }
}

-(void)startParsing
{
    DLog(@"");
	NSXMLParser *messageParser = [[NSXMLParser alloc] initWithData:self.receivedData];
	messageParser.delegate = self;
	[messageParser parse];
}


#pragma mark -
#pragma mark NSURLConnection Callbacks
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	[indicator stopAnimating];
	self.receivedData = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	if(connectionStatus == 0)
    {
		[indicator stopAnimating];
		self.receivedData = nil;
		[self.navigationController popViewControllerAnimated:YES];
	}
	
	if(connectionStatus == 1)
    {
    	[indicator stopAnimating];
		//Lancer appel au parseur
		[self startParsing];
	}
    
}

#pragma mark -
#pragma mark Parser Methods

- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	ligneMessage = [[NSMutableString alloc] initWithCapacity:200];
	currentElementName = nil;
	currentText = nil;
	
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	if ([elementName isEqualToString:@"message"]) {
		dictionnaire = [[NSMutableDictionary alloc] initWithCapacity: [interestingTags count]];
	}
	
	else if ([interestingTags containsObject: elementName]) {
		currentElementName = elementName;
		currentText = [[NSMutableString alloc] init];
	}
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	[currentText appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	if ([elementName isEqualToString:currentElementName]) {
		[dictionnaire setValue: currentText forKey: currentElementName];
	}
	
	else if ([elementName isEqualToString:@"message"]) {
		
		[ligneMessage appendFormat:@"%@ - %@", [dictionnaire valueForKey:@"dejaVote"], [dictionnaire valueForKey:@"supprime"]];
        
	}
	
	currentText = nil;
	
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
	
	[indicator stopAnimating];
	NSArray *elementsLigne = [ligneMessage componentsSeparatedByString:@" - "];
	
	NSString *messageDejaVote = [elementsLigne objectAtIndex:0];
	NSString *messageSupprime = [elementsLigne objectAtIndex:1];
    
    NSLog(@"messageSupprime is %@",messageSupprime);
    NSLog(@"messageDejaVote is %@",messageDejaVote);
    
	if ([self.categorie isEqualToString:@"sage"] || [self.categorie isEqualToString:@"gardien du temple"])
		boutonSupprimer.hidden = NO;
	
	//Si le message a été déjà voté on désactive les boutons
	if ([messageDejaVote isEqualToString:@"OUI"]) {
		
		boutonOUI.hidden = NO;
		boutonNON.hidden = NO;
		
		boutonOUI.enabled = NO;
		boutonNON.enabled = NO;
		
		[boutonOUI setAlpha:0.5];
		[boutonNON setAlpha:0.5];
		
		[boutonOUI setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
		[boutonNON setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
		
		messageSortie.hidden = NO;
		messageSortie.text = @"Vous avez déjà jugé la pertinence";
		
	}
	
	//Si le message a été déjà supprimé
	if ([messageSupprime isEqualToString:@"OUI"]) {
		
		boutonOUI.hidden = YES;
		boutonNON.hidden = YES;
		
		messageSupprimeLigne1.hidden = NO;
		
		boutonSupprimer.enabled = NO;
		[boutonSupprimer setAlpha:0.5];
		
		[boutonSupprimer setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
		
		messageSortie.hidden = NO;
		messageSortie.text = @"Ce message a été supprimé";
		
	}
	
	if (![messageDejaVote isEqualToString:@"OUI"] && ![messageSupprime isEqualToString:@"OUI"]) {
		
		boutonOUI.hidden = NO;
		boutonNON.hidden = NO;
		messageSortie.hidden = NO;
		
	}
	
    [indicator stopAnimating];
    self.receivedData = nil;
    
}

#pragma mark -


- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	[indicator stopAnimating];
	interestingTags = [[NSSet alloc] initWithObjects:INTERESTING_TAG_NAMES];
	
	pseudoNiveau.text = [[NSString alloc] initWithFormat:@"%@ - (%@) :",postPseudo,postCategorie];
	champMessage.text = self.message;
    
	[self initialiserEcran];
	
	//Si l'utilisateur est auteur de ce message, on empêche l'option de vote
	if ([[self.postPseudo lowercaseString] isEqualToString:[self.username lowercaseString]]) {
		
		boutonOUI.enabled = NO;
		boutonNON.enabled = NO;
		
		[boutonOUI setAlpha:0.5];
		[boutonNON setAlpha:0.5];
		
		[boutonOUI setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
		[boutonNON setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
		
		messageSortie.text = @"Vous ne pouvez pas juger votre message";
		
	}
	
	/*
     //Si l'utilisateur est "novice", il ne peut pas juger sur la pertinence des messages
     if([self.categorie isEqualToString:@"novice"]) {
     
     boutonOUI.enabled = NO;
     boutonNON.enabled = NO;
     
     [boutonOUI setAlpha:0.5];
     [boutonNON setAlpha:0.5];
     
     [boutonOUI setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
     [boutonNON setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
     
     messageSortie.text = @"Vous ne pouvez pas juger les messages";
     
     }
	 */
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.indicator = nil;
	self.boutonOUI = nil;
	self.boutonNON = nil;
	self.boutonSupprimer = nil;
	self.pseudoNiveau = nil;
	self.champMessage = nil;
	self.messageSortie = nil;
	self.messageSupprimeLigne1 = nil;
}




@end
