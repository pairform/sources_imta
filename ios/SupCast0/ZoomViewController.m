//
//  ZoomViewController.m
//  SupCast
//
//  Created by Cape EMN on 17/08/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "ZoomViewController.h"

@implementation ZoomViewController
@synthesize imageView, imagePath = _imagePath, scrollZoomView;


-(id)initWithPicture:(NSString *)imagePath
{
    DLog(@"");
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _imagePath = imagePath;
        //self.title = @"Zoom";
    }
    return self;
}

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    
    // Initialisation de l'image
    _imagePath = [_imagePath stringByReplacingOccurrencesOfString:@"file:///" withString:@"/"];
    _imagePath = [_imagePath stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    UIImage * imageRessource = [[UIImage alloc] initWithContentsOfFile:_imagePath];
    
    [self.imageView setImage:imageRessource];
    //scrollZoomView.minimumZoomScale = scrollZoomView.frame.size.width / imageView.frame.size.width;
    
}

- (void)viewDidUnload
{
    DLog(@"");
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)enterFullScreen:(id)sender
{
    ZoomFSViewController *grosZoom = [[ZoomFSViewController alloc] initWithPicture:_imagePath];

    grosZoom.delegate = self;
        
    [grosZoom setModalPresentationStyle:UIModalPresentationFullScreen];
    [grosZoom setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentModalViewController:grosZoom animated:true];
}

-(void)dismissZoomFS
{
    [self dismissModalViewControllerAnimated:true]; 
}
@end
