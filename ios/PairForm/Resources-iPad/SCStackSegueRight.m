//
//  SCStackSegueRight.m
//  PairForm
//
//  Created by Maen Juganaikloo on 03/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "SCStackSegueRight.h"
#import "SCStackViewController.h"

@implementation SCStackSegueRight

- (void)perform
{
    SCStackViewController * stackViewController = [self.sourceViewController sc_stackViewController];
    [self.destinationViewController setStackPosition:SCStackViewControllerPositionRight];
    
    //Si le controller source est à droite, on sucre les controllers qui sont après lui dans la pile
    if ([self.sourceViewController stackPosition] == SCStackViewControllerPositionRight) {
        [stackViewController popViewControllersAfter:self.sourceViewController AtPosition:SCStackViewControllerPositionRight animated:YES completion:^{
            [stackViewController pushViewController:self.destinationViewController fromViewController:self.sourceViewController atPosition:SCStackViewControllerPositionRight unfold:YES animated:YES completion:nil];
        }];
    }
    //Sinon, on les sucre tous
    else{
        [stackViewController popToRootViewControllerFromPosition:SCStackViewControllerPositionRight animated:YES completion:^{
            [stackViewController pushViewController:self.destinationViewController fromViewController:self.sourceViewController atPosition:SCStackViewControllerPositionRight unfold:TRUE animated:TRUE completion:nil];
        }];
    }
}

@end
