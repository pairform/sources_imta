//
//  MenuVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC_iPad : UITableViewController

@property (nonatomic, strong) NSMutableArray * menuItems;
@property (nonatomic, strong) IBOutlet UILabel * connexionButton;
-(IBAction)feedback:(id)sender;
-(IBAction)connexion:(id)sender;
@end
