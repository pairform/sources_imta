//
//  UIViewController+UIViewController_Stack.m
//  PairForm
//
//  Created by Maen Juganaikloo on 15/04/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "UIViewController+SCStackItem.h"
#import "RootNavigation_iPad.h"

#import "UIView+Shadows.h"

@interface UIViewController ()

@property (nonatomic, assign) SCStackViewControllerPosition position;

@end

@implementation UIViewController (SCStackItem)

- (instancetype)initWithPosition:(SCStackViewControllerPosition)position
{
    if(self = [super init]) {
        self.position = position;
    }
    
    return self;
}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    [self.view setBackgroundColor:[UIColor randomColor]];
//    [self updateShadow];
//    
//    switch (self.position) {
//        case SCStackViewControllerPositionTop:
//            self.visiblePercentageLabel.center = CGPointMake(self.view.center.x, CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.visiblePercentageLabel.bounds));
//            self.controlsContainer.center = CGPointMake(self.view.center.x, CGRectGetHeight(self.controlsContainer.bounds));
//            break;
//        case SCStackViewControllerPositionBottom:
//            self.visiblePercentageLabel.center = CGPointMake(self.view.center.x, CGRectGetHeight(self.visiblePercentageLabel.bounds));
//            self.controlsContainer.center = CGPointMake(self.view.center.x, CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.controlsContainer.bounds));
//            break;
//        case SCStackViewControllerPositionLeft:
//            self.visiblePercentageLabel.transform = CGAffineTransformMakeRotation(-M_PI/2);
//            self.visiblePercentageLabel.center = CGPointMake(CGRectGetWidth(self.visiblePercentageLabel.bounds), 100.0f);
//            break;
//        case SCStackViewControllerPositionRight:
//            self.visiblePercentageLabel.transform = CGAffineTransformMakeRotation(M_PI/2);
//            self.visiblePercentageLabel.center = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(self.visiblePercentageLabel.bounds), 100.0f);
//            break;
//    }
//}
//
//- (void)updateShadow
//{
//    static NSDictionary *positionToShadowEdge;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        positionToShadowEdge = (@{
//                                  @(SCStackViewControllerPositionTop)    : @(SCShadowEdgeTop),
//                                  @(SCStackViewControllerPositionLeft)   : @(SCShadowEdgeLeft),
//                                  @(SCStackViewControllerPositionBottom) : @(SCShadowEdgeBottom),
//                                  @(SCStackViewControllerPositionRight)  : @(SCShadowEdgeRight)
//                                  });
//    });
//    
//    [self.view castShadowWithPosition:[positionToShadowEdge[@(self.position)] intValue]];
//}
//
//- (void)viewWillLayoutSubviews
//{
//    
//    [(RootNavigation_iPad*)self.navigationController setRightBarButtonItemWithImageName:@"paint_48" andSelectorName:@"" fromViewController:self];
//    [(RootNavigation_iPad*)self.navigationController setLeftBarButtonItemsWithOptions:@[@{@"imageName": @"paint_48",@"selector": @""}, @{@"imageName": @"paint_48",@"selector": @""}] fromViewController:self];
//    [super viewWillLayoutSubviews];
//    [self updateShadow];
//}
//

@end
