//
//  UIImagePickerController+NoRotate.m
//  PairForm
//
//  Created by Maen Juganaikloo on 23/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "UIImagePickerController+NoRotate.h"

@implementation UIImagePickerController (NoRotate)

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}
-(BOOL)shouldAutorotate{
    return NO;
}
@end
