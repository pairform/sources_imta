//
//  RootVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 01/10/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "RootVC_iPad.h"
#import "HomeVC.h"
#import "DashboardVC.h"
#import "MenuVC.h"
#import "ProfilController.h"
#import "MessageController.h"
#import "SCStackLayouter.h"
#import "SCParallaxStackLayouter.h"
#import "SCResizingStackLayouter.h"
#import "SCSlidingStackLayouter.h"
#import "SCGoogleMapsStackLayouter.h"
#import "SCStackNavigationStep.h"
#import "SCReversedStackLayouter.h"
#import "UIImage+ImageEffects.h"

#import "UIViewController+SCStackItem.h"
#import "UIView+Shadows.h"

@interface RootVC_iPad () <SCStackViewControllerDelegate, SCOverlayViewDelegate, SCStackItemDelegate>

@property (nonatomic, strong) DashboardVC *mainViewController;
@property (nonatomic, strong) SCOverlayView *overlayView;



@end

@implementation RootVC_iPad


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIStoryboard * mainStoryboard = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] getIPadStoryBoard];
    
    
//    HomeVC *leftViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    DashboardVC * dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
    
    self.mainViewController = dashboard;
//    [dashboard loadView];
//    [dashboard viewDidLoad];
//    self.mainViewController = leftViewController;
//    [self.mainViewController.view castShadowWithPosition:SCShadowEdgeRight];
    [self.mainViewController willMoveToParentViewController:self];
    
    [self addChildViewController:self.mainViewController];
    [self.mainViewController didMoveToParentViewController:self];

    //Initialisation de la vue principale
    //Surcouche sombre
    self.overlayView = [[SCOverlayView alloc] initWithFrame:self.mainViewController.view.frame];
    [self.overlayView setDelegate:self];
    [self.overlayView setAlpha:0.0f];
    [self.mainViewController.view addSubview:self.overlayView];
    [self.overlayView setFrame:CGRectMake(0, 0, 960, 704)];
    //Initialisation du stackController
    self.stackViewController = [[SCStackViewController alloc] initWithRootViewController:self.mainViewController];
    [self.stackViewController setDelegate:self];
    [self.stackViewController willMoveToParentViewController:self];
    [self.stackViewController.view setTag:10];
    [self.view addSubview:self.stackViewController.view];
    
    [self addChildViewController:self.stackViewController];
    [self.stackViewController didMoveToParentViewController:self];
    
    [self.stackViewController setMinimumNumberOfTouches:1];
    [self.stackViewController setMaximumNumberOfTouches:1];
    [self.stackViewController setContinuousNavigationEnabled:YES];
        // Optional properties
    [self.stackViewController setShowsScrollIndicators:YES];
    
    SCStackViewControllerPosition leftPosition = SCStackViewControllerPositionLeft;
    SCStackViewControllerPosition rightPosition = SCStackViewControllerPositionRight;
    
    id<SCStackLayouterProtocol> leftLayouter = [[SCReversedStackLayouter alloc] init];
    [leftLayouter setShouldStackControllersAboveRoot:YES];
    [self.stackViewController registerLayouter:leftLayouter forPosition:leftPosition];
    
    id<SCStackLayouterProtocol> rightLayouter = [[SCResizingStackLayouter alloc] init];
    [rightLayouter setShouldStackControllersAboveRoot:YES];
    [self.stackViewController registerLayouter:rightLayouter forPosition:rightPosition];
    
    [(RootNavigation_iPad*)self.navigationController setLeftBarButtonItemWithImageName:@"arrow_left_64" andSelectorName:@"goToLastLeftView" fromViewController:self];
    
    [(RootNavigation_iPad*)self.navigationController setLeftBarButtonItemWithImageName:@"arrow_right_64" andSelectorName:@"goToLastRightView" fromViewController:self];
    
#ifdef __IPHONE_8_0
    if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 && [self deviceSupportsBlur]) {
        UIVisualEffectView * visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
        
        visualEffectView.frame = self.overlayView.bounds;
        
        [self.overlayView addSubview:visualEffectView];
    }
#endif

}

-(void)goToLastRightView{
    [self.stackViewController navigateToViewController:[[self.stackViewController viewControllersForPosition:SCStackViewControllerPositionRight] lastObject] animated:YES completion:nil];
}

-(void)goToLastLeftView{
    [self.stackViewController navigateToViewController:[[self.stackViewController viewControllersForPosition:SCStackViewControllerPositionLeft] lastObject] animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.stackViewController.view setFrame:CGRectMake(64, 0, 960, 704)];
}
#pragma mark - Stack methods
#pragma mark - Stack Delegate

-(void)stackViewController:(SCStackViewController *)stackViewController didShowViewController:(UIViewController *)controller position:(SCStackViewControllerPosition)position{
    if(self.overlayView.image == nil)
        [self showOverlay];
//    if (position == SCStackViewControllerPositionLeft) {

//    }
}
//-(void)stackViewController:(SCStackViewController *)stackViewController didHideViewController:(UIViewController *)controller position:(SCStackViewControllerPosition)position{
//    if (position == SCStackViewControllerPositionLeft) {
//        [UIView animateWithDuration:0.5 animations:^{
//            [self.overlayView setAlpha:0];
//        }];
//    }
//}

-(void)presentUserProfile{
    ProfilController* destinationController = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfilController"];
    [destinationController setStackPosition:SCStackViewControllerPositionLeft];
    
    [self.stackViewController popToRootViewControllerFromPosition:SCStackViewControllerPositionLeft animated:YES completion:^{
        [self.stackViewController pushViewController:destinationController fromViewController:self.mainViewController atPosition:SCStackViewControllerPositionLeft unfold:YES animated:YES completion:nil];
    }];
}

#pragma mark - SCOverlayViewDelegate

- (void)overlayViewDidReceiveTap:(SCOverlayView *)overlayView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"overlayTapped" object:self];
    [self hideOverlay];
    [self.stackViewController navigateToViewController:self.stackViewController.rootViewController animated:YES completion:nil];
}
-(void)showOverlay{
    
//    [self.overlayView setFrame:self.mainViewController.view.bounds];
    if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 && [self deviceSupportsBlur]) {
        [self.overlayView setImage:[self snapshot]];
        [self.overlayView setHidden:NO];
        [self.overlayView setAlpha:1];
    }
    else{
        UIImage * blurredSnap = [self blurredSnapshot];
        [self.overlayView setImage:blurredSnap];
        [self.overlayView setAlpha:1];
        [UIView animateWithDuration:0.5 animations:^{
            [self.overlayView setAlpha:1];
        }];
    }

    
}
-(void)hideOverlay{
    
    if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 && [self deviceSupportsBlur]) {
        [self.overlayView setImage:nil];
        [self.overlayView setHidden:YES];
    }
    else{
        [UIView animateWithDuration:0.5 animations:^{
            [self.overlayView setAlpha:0];
        } completion:^(BOOL finished) {
            [self.overlayView setImage:nil];
        }];
    }
    
}

-(UIImage *)snapshot
{
    // Create the image context
    UIGraphicsBeginImageContextWithOptions(self.mainViewController.view.layer.bounds.size, NO, [[self.mainViewController.view.window screen] scale]);
    
    // There he is! The new API method
    [self.mainViewController.view drawViewHierarchyInRect:self.mainViewController.view.layer.bounds afterScreenUpdates:NO];
    
    // Get the snapshot
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Be nice and clean your mess up
    UIGraphicsEndImageContext();
    //    return snapshotImage;
    return snapshotImage;
}


-(UIImage *)blurredSnapshot
{
    UIImage *blurredSnapshotImage = [[self snapshot] applyDarkEffect];
    
//    return snapshotImage;
    return blurredSnapshotImage;
}

#pragma mark - Helpers
#import <sys/utsname.h>
NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}
-(BOOL)deviceSupportsBlur{
    NSSet *graphicsQuality = [NSSet setWithObjects:@"iPad",
                              @"iPad1,1",
                              @"iPhone1,1",
                              @"iPhone1,2",
                              @"iPhone2,1",
                              @"iPhone3,1",
                              @"iPhone3,2",
                              @"iPhone3,3",
                              @"iPod1,1",
                              @"iPod2,1",
                              @"iPod2,2",
                              @"iPod3,1",
                              @"iPod4,1",
                              @"iPad2,1",
                              @"iPad2,2",
                              @"iPad2,3",
                              @"iPad2,4",
                              @"iPad3,1",
                              @"iPad3,2",
                              @"iPad3,3",
                              nil];
    if ([graphicsQuality containsObject:deviceName()]) {
        return NO;
    } else {
        return YES;
    }
}

@end
