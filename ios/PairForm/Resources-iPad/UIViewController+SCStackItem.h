//
//  UIViewController+UIViewController_Stack.h
//  PairForm
//
//  Created by Maen Juganaikloo on 15/04/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCStackViewController.h"

@protocol SCStackItemDelegate;

@interface UIViewController (SCStackItem)

@property (nonatomic, weak) id<SCStackItemDelegate> delegate;
@property (nonatomic, readonly) SCStackViewControllerPosition position;

- (instancetype)initWithPosition:(SCStackViewControllerPosition)position;

- (void)setVisiblePercentage:(CGFloat)percentage;

@end

@protocol SCStackItemDelegate <NSObject>

@optional
- (void)menuViewControllerDidRequestPush:(UIViewController *)menuViewController;
- (void)menuViewControllerDidRequestPop:(UIViewController *)menuViewController;

@end