//
//  MenuVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuVC_iPad.h"
#import "ProfilController.h"
#import "MessageController.h"
#import "UtilsCore.h"
#import "iRate.h"
#import "HelpVC.h"
#import "RootVC.h"
#import "Reachability.h"

@interface MenuVC_iPad ()

@end

@implementation MenuVC_iPad

@synthesize menuItems;

- (id)initWithStyle:(UITableViewStyle)style
{
//    UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:[NSBundle mainBundle]];
//    self = [mainStoryboard instantiateViewControllerWithIdentifier:@"rightVC"];
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    [super viewDidLoad];
    
    [self updateSelectedCell];
    //Ajout d'un pull to refresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Actualisation de tous les messages..."];
    
    refreshControl.tintColor = [UIColor colorWithRed:0.447 green:0.49 blue:0.969 alpha:1]; /*#727df7*/
    [refreshControl addTarget:[UtilsCore class] action:@selector(rafraichirMessages) forControlEvents:UIControlEventValueChanged];
    refreshControl.tag = 100;
    
    [self.tableView insertSubview:refreshControl atIndex:0];
    

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(updateConnectButton:)
//                                                 name:@"rightPanelOpen" object:nil];
//    
    
    
//    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background_pattern"]]];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
//    [[self tableView] setSeparatorColor:[UIColor colorWithWhite:1 alpha:0.1]];
    
//    [self setStackWidth: 320];
    
    
//    menuItems = [[NSMutableArray alloc] initWithCapacity:8];
//    
//    for (int i=0; i<8; i++) {
//        [menuItems addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
//                              [NSString stringWithFormat:@"Item %d", i], @"title",
//                              [UIImage imageNamed:@"Sealred.png"], @"image", nil]];
//    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesErreur"
                                               object:nil];
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateSelectedCell)
                                                 name:@"overlayTapped"
                                               object:nil];
    

    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesErreur"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"overlayTapped"
                                                  object:nil];
}

-(void)updateSelectedCell{
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

-(void)refreshMessages{
    
    
    [(UIRefreshControl*)[self.tableView viewWithTag:100] endRefreshing];
}

-(IBAction)feedback:(id)sender{
//    [TestFlight openFeedbackView];
    [[iRate sharedInstance] promptIfNetworkAvailable];
}

#pragma mark - Segue override

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"dashboardSegue"]){
        [(RootVC_iPad*)[self parentViewController] hideOverlay];
    }
    if([[segue identifier] isEqualToString:@"derniersMessagesSegue"]){
        MessageController * messageController = segue.destinationViewController;
        messageController.modeMessage = ModeMessageTransversal;
    }
    if([[segue identifier] isEqualToString:@"monProfilSegue"]){
        ProfilController * view = segue.destinationViewController;
        view.id_utilisateur = [UIViewController getSessionValueForKey:@"id_utilisateur"];
        view.stackWidth = 704;
    }
    if([[segue identifier] isEqualToString:@"AideSegue"]){
        NSString * screenID = [[self.navigationController topViewController] restorationIdentifier];
        if(IS_IPAD)
        {
//            id lastVC = [self.parentViewController.stackController.fullyVisibleViewControllers lastObject];
//
//            if ([lastVC isKindOfClass:NSClassFromString(@"UINavigationController")]) {
//                lastVC = [lastVC topViewController];
//            }
//            
//            screenID = [lastVC restorationIdentifier];
        }
        
        
        HelpVC * helpVC = segue.destinationViewController;
        [helpVC setScreenStoryboardID:screenID];
        [helpVC setCurrentIndex:0];
    }
    
    if([[segue identifier] isEqualToString:@"aProposSegue"]){
        UITabBarController * tab_bar_controller = segue.destinationViewController;
        tab_bar_controller.stackWidth = 640;
    }
    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"monProfilSegue"]){
        if ([self checkForLoggedInUser])
            return YES;
        else
            return NO;
    }
    if ( [identifier isEqualToString:@"RechercheProfilSegue"]){
        return [WService isReachable];
    }
    return YES;
}

#pragma mark - Table view data source

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


@end
