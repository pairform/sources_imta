//
//  RootNavigation.m
//  SCStackViewController
//
//  Created by Maen Juganaikloo on 14/04/14.
//

#import "RootNavigation_iPad.h"
#import "UIImage+Resize.h"
#import "ReachabilityManager.h"

#import "SCStackViewController.h"

#import "SCStackLayouter.h"
#import "SCParallaxStackLayouter.h"
#import "SCSlidingStackLayouter.h"
#import "SCGoogleMapsStackLayouter.h"
#import "SCMerryGoRoundStackLayouter.h"
#import "SCReversedStackLayouter.h"
#import "SCResizingStackLayouter.h"
#import "LanguageManager.h"
#import "SCOverlayView.h"

#import "SCStackNavigationStep.h"

#import "NotificationTVC.h"

#import "HomeVC.h"

@interface RootNavigation_iPad ()

@property (nonatomic,strong) NSMutableArray * leftBarButtonsArray;
@property (nonatomic,strong) NSMutableArray * leftBarButtonNamesArray;
@property (nonatomic,strong) NSMutableArray * rightBarButtonsArray;
@property (nonatomic,strong) NSMutableArray * rightBarButtonNamesArray;
@end

@implementation RootNavigation_iPad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.view.frame = CGRectMake(0,44,self.view.bounds.size.width,self.view.bounds.size.height);
    UIImageView *titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PairformFullFinal-transparent"]];
//    [titleView setFrame:CGRectMake(0, 0, 166, 44)];
    [self.navigationBar.topItem setTitleView:titleView];
    
    [self.navigationBar setBackgroundImage:[[UIImage imageNamed:@"NavigationBar7~iPad"] resizableImageWithCapInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 60.0) resizingMode:UIImageResizingModeTile] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.topItem.title = @"PairForm";
    [self.navigationBar setAutoresizesSubviews:NO];

    [self initUserBarWithCustomView:nil];
    
    [self initializeRootViewControllerBarButtonItem];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastDownloadedRessource:)
                                                 name:@"unzipFinished" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastErrorRessource:)
                                                 name:@"downloadErrorToast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateFromNotification:)
                                                 name:@"new_user_notification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages:)
                                                 name:@"downloadFinished" object:nil];
    //Notification de connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserBar:) name:@"userLoggedIn" object:nil];
    
//    if([self checkForDefaultUser])
//    {
//        [self checkForLoggedInUser];
//    }
//    else
//    {
//        DLog(@"Pas de defaultUser");
//    }

    //Si l'utilisateur était connecté, mais n'a pas de réseau, et qu'il ne souhaite pas retenir sa session sur son mobile
    if ([UIViewController isConnected] && ![[ReachabilityManager sharedManager].reachability isReachable] && [[UIViewController getPreference:@"rememberMe"] isEqualToNumber:@1]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:self userInfo:@{@"status": @"connexion", @"username" : [UIViewController getPermamentValue:@"username"]}];
    }
}

#pragma mark Récuperation des messages lors du téléchargement d'une ressource

-(void)refreshMessages:(NSNotification*)notification{
//    Capsule * capsule = notification.object;
    NSNumber * id_capsule = notification.userInfo[@"id_capsule"];
    [UtilsCore rafraichirMessagesFromCapsule:id_capsule];
}
#pragma mark - NavigationBar methods

-(void)displayProfil{
    [[[UIAlertView alloc] initWithTitle:@"Touched!" message:@"Profil touched" delegate:self cancelButtonTitle:@"Alright" otherButtonTitles:nil, nil] show];
}

-(void)setRightBarButtonItemWithImageName:(NSString*)imageName andSelectorName:(NSString*)selectorName fromViewController:(UIViewController*)viewController{
    [self setRightBarButtonItemsWithOptions:@[@{@"imageName": imageName, @"selectorName" : selectorName}] fromViewController:viewController];
}
-(void)setRightBarButtonItemsWithOptions:(NSArray*)barButtonItemsOptions fromViewController:(UIViewController*)viewController{
    [self setBarButtonItemsWithOptions:barButtonItemsOptions fromViewController:viewController andSide:@"right"];
}

-(void)setLeftBarButtonItemWithImageName:(NSString*)imageName andSelectorName:(NSString*)selectorName fromViewController:(UIViewController*)viewController{
    [self setLeftBarButtonItemsWithOptions:@[@{@"imageName": imageName, @"selectorName" : selectorName}] fromViewController:viewController];
}
-(void)setLeftBarButtonItemsWithOptions:(NSArray*)barButtonItemsOptions fromViewController:(UIViewController*)viewController{
    [self setBarButtonItemsWithOptions:barButtonItemsOptions fromViewController:viewController andSide:@"left"];
}

/*
 [
  {
   imageName : nom de l'image de fond (NSString*),
   selectorName : selecteur lors du touch inside up (NSString*)
  }, {...}
 ]
 */

-(void)setBarButtonItemsWithOptions:(NSArray*)barButtonItemsOptions fromViewController:(UIViewController*)viewController andSide:(NSString*)side{
    //Tableau stockant les boutons
    
    for (NSDictionary *item in barButtonItemsOptions) {
        NSAssert([item[@"imageName"] isKindOfClass:NSClassFromString(@"NSString")], @"ImageName n'est pas un NSString dans l'item demandé.");
        UIImage * itemImage = [UIImage imageNamed:item[@"imageName"]];
        
        UIView *customItem = [[UIView alloc] initWithFrame:CGRectMake(2,2,40,40)];
        UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [customButton setFrame:CGRectMake(4,4,32,32)];
        [customButton setImage:itemImage forState:UIControlStateNormal];
        [customButton setEnabled:YES];
        [customButton setReversesTitleShadowWhenHighlighted:true];
        [customButton setShowsTouchWhenHighlighted:true];
        
        if (item[@"selectorName"] != nil) {
            if (![item[@"selectorName"] isEqualToString:@""]) {
                NSAssert([item[@"selectorName"] isKindOfClass:NSClassFromString(@"NSString")], @"selectorName n'est pas un NSString dans l'item demandé.");
                [customButton addTarget:viewController action:NSSelectorFromString(item[@"selectorName"]) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        [customItem addSubview:customButton];
        
        //Pour l'animer par la suite, on le cache
        [customItem setAlpha:0];
        
        [UIView animateWithDuration:1.0 animations:^{
            [customItem setAlpha:1];
        }];
        
        UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithCustomView:customItem];
        
        if ([side isEqualToString:@"left"] && ![_leftBarButtonNamesArray containsObject:item[@"imageName"]]){
            [_leftBarButtonsArray addObject:backButton];
            [_leftBarButtonNamesArray addObject:item[@"imageName"]];
        }
        else if ([side isEqualToString:@"right"] && ![_rightBarButtonNamesArray containsObject:item[@"imageName"]]){
            [_rightBarButtonsArray addObject:backButton];
            [_rightBarButtonNamesArray addObject:item[@"imageName"]];
        }
    }
    
    [self.navigationBar.topItem setLeftBarButtonItems:[_leftBarButtonsArray copy]];
    [self.navigationBar.topItem setRightBarButtonItems:[_rightBarButtonsArray copy]];
    [[self navigationBar] setNeedsLayout];
}

-(void)removeLeftBarButtonItemWithIdentifier:(NSString*)imageName{
    [self removeBarButtonItemWithIdentifier:imageName andSide:@"left"];
}
-(void)removeRightBarButtonItemWithIdentifier:(NSString*)imageName{
    [self removeBarButtonItemWithIdentifier:imageName andSide:@"right"];
}
-(void)removeBarButtonItemWithIdentifier:(NSString*)imageName andSide:(NSString*)side{
    
    if ([side isEqualToString:@"left"]){
        int index = [_leftBarButtonNamesArray indexOfObject:imageName];
        UIBarButtonItem * customItem = [_leftBarButtonsArray objectAtIndex:index];
        [UIView animateWithDuration:1.0 animations:^{
            [[customItem customView] setAlpha:0];
        } completion:^(BOOL finished) {
        
            [_leftBarButtonsArray removeObjectAtIndex:index];
            [_leftBarButtonNamesArray removeObjectAtIndex:index];
            [self.navigationBar.topItem setLeftBarButtonItems:[_leftBarButtonsArray copy]];
        }];
    }
    else if ([side isEqualToString:@"right"]){
        int index = [_rightBarButtonNamesArray indexOfObject:imageName];
        
        UIBarButtonItem * customItem = [_rightBarButtonsArray objectAtIndex:index+1];
        [UIView animateWithDuration:1.0 animations:^{
            [[customItem customView] setAlpha:0];
        } completion:^(BOOL finished) {
            
            [_rightBarButtonsArray removeObjectAtIndex:index+1];
            [_rightBarButtonNamesArray removeObjectAtIndex:index];//Prise en compte de la userBar dans l'index
            [self.navigationBar.topItem setRightBarButtonItems:[_rightBarButtonsArray copy]];
        }];
    }
    
    [self.navigationBar setNeedsLayout];
}

- (void)initializeRootViewControllerBarButtonItem{
    

    _leftBarButtonsArray = [[NSMutableArray alloc] init];
    _rightBarButtonsArray = [[NSMutableArray alloc] initWithObjects:_userBar, nil];
    _leftBarButtonNamesArray = [[NSMutableArray alloc] init];
    _rightBarButtonNamesArray = [[NSMutableArray alloc] init];

    
//    UIViewController * rootVC = (UIViewController*)self.viewControllers[0];
    [self.navigationBar.topItem setLeftBarButtonItems:[_leftBarButtonsArray copy]];
    [self.navigationBar.topItem setRightBarButtonItems:[_rightBarButtonsArray copy]];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    viewController.navigationItem.rightBarButtonItems = @[_userBar];
    
}
-(void)setContent{
    if (![UIViewController isConnected]) {
        [(UIButton*)[_userBar.customView viewWithTag:1] setTitle:[LanguageManager get:@"button_connexion"] forState:UIControlStateNormal];
    }
}
-(void)initUserBarWithCustomView:(UIView*)userBarView{
    
    if (![userBarView isKindOfClass:NSClassFromString(@"UIView")]) {
        userBarView = [[NSBundle mainBundle] loadNibNamed:@"UserBar" owner:self options:nil][1];
        UIButton * connectButton = (UIButton*)[userBarView viewWithTag:1];
        [connectButton setTitle:[LanguageManager get:connectButton.titleLabel.text] forState:UIControlStateNormal];
        [connectButton addTarget:self action:@selector(popUpLogin) forControlEvents:UIControlEventTouchUpInside];
    }
    if (!_userBar) {
        _userBar = [[UIBarButtonItem alloc] initWithCustomView:userBarView];
        [_userBar setStyle:UIBarButtonItemStyleDone];
        [_userBar.customView setBounds:[userBarView frame]];

    }
    else{
        [_userBar setCustomView:userBarView];
    }
}
-(void)updateUserBar:(NSNotification*)aNotification{

    
    //Initialisation de la barre d'utilisateur
    NSArray * userBarArray = [[NSBundle mainBundle] loadNibNamed:@"UserBar" owner:self options:nil];
    UIView * userBarView;
    
    //Si l'utilisateur vient de se connecter, on passe par une notification
    if ([aNotification isKindOfClass:NSClassFromString(@"NSNotification")]) {
        NSString * status = [[aNotification userInfo] objectForKey:@"status"];
        if([status isEqualToString:@"connexion"]){
            
            userBarView = userBarArray[0];
            Utilisateur * utilisateur = [UtilsCore getUtilisateur:[UIViewController getSessionValueForKey:@"id_utilisateur"]];
            
            UIImage * avatarImg;
            NSString * username_str;
            
            if (utilisateur) {
                avatarImg = [[utilisateur getImage] thumbnailImage:34 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
                username_str = [utilisateur owner_username];

            }
            else {
                NSURL * avatarURL = [NSURL URLWithString:[UIViewController getSessionValueForKey:@"avatar_url"]];
                avatarImg = [[UIImage imageWithData: [NSData dataWithContentsOfURL: avatarURL]] thumbnailImage:34 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
                
                username_str = [aNotification userInfo][@"username"];
            }
            
            UIImageView * avatar = (UIImageView*)[userBarView viewWithTag:1];
            [avatar setImage:avatarImg];
            UITextField * username = (UITextField*)[userBarView viewWithTag:2];
            [username setText:username_str];
            UITextField * points = (UITextField*)[userBarView viewWithTag:3];
            
            [points setHidden:YES];
            if ([self.viewControllers[0] respondsToSelector:@selector(presentUserProfile)]) {
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self.viewControllers[0] action:@selector(presentUserProfile)];
                [userBarView setGestureRecognizers:@[tapGesture]];
            }
            
            [self initNotificationsBarButtons];
        }
        else if([status isEqualToString:@"deconnexion"]){
            userBarArray = userBarArray[1];
            [self removeRightBarButtonItemWithIdentifier:@"chart_area_48"];
            [self removeRightBarButtonItemWithIdentifier:@"speaker_48"];
            
        }
    }
    
    //Fadeout
    [UIView animateWithDuration:0.3 animations:^{
        [[_userBar customView] setAlpha:0];
    } completion:^(BOOL finished) {
        [self initUserBarWithCustomView:userBarView];
        [_userBar.customView setAlpha:0];
        //FadeIn
        [UIView animateWithDuration:0.3 animations:^{
            [[_userBar customView] setAlpha:1];
        }];

    }];
    
    
}

#pragma mark - SplashScreen

UIImageView * splashScreen;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    splashScreen = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Landscape"]];
    
    //Ajout de la marge de la status bar
    //    CGRect frame = splashScreen.frame;
    //    frame.origin.y = 20;
    //    //    frame.origin.y = [UIApplication sharedApplication].statusBarFrame.size.height;
    //    splashScreen.frame = frame;
    //
    //Tag pour futur récupération
    CGSize screenSize = [[[UIScreen mainScreen] currentMode] size];
    [splashScreen setFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    [splashScreen setTag:1000];
    
    [[self view] addSubview:splashScreen];
    [[self view] bringSubviewToFront:splashScreen];
    [self killSplashScreen];
//    [self performSelector:@selector(killSplashScreen) withObject:nil afterDelay:0];
}
- (void)killSplashScreen {
    //Alpha transition
    [UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        [[self view] viewWithTag:1000].alpha = 0.0;
        splashScreen.transform = CGAffineTransformMakeTranslation(0, [splashScreen frame].size.height);
//        [[[self view] viewWithTag:1000] setCenter:CGPointMake(512, 1152)];
//        [[self view] viewWithTag:1000].transform = CGAffineTransformScale(CGAffineTransformIdentity, 3.f, 3.f);
    } completion:(void (^)(BOOL)) ^{
        [splashScreen removeFromSuperview];
    }];
}

#pragma mark - Toasts

-(void)toastDownloadedRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    NSString * capsule_nom_court = notification.userInfo[@"capsule_nom_court"];
    
    dispatch_block_t block = ^{
        UIImage * icon = [downloadedRessource getIcone];
        NSString * title = [capsule_nom_court stringByAppendingFormat:@" : %@", [LanguageManager get:@"ios_label_telechargement_terminé"]];
        NSString * name = downloadedRessource.nom_court;
        [self.view makeToast:title duration:3.0 position:@"bottom" title: name image: icon style:nil completion:nil];
    };
    
    if ([NSThread isMainThread])
        block(); // execute the block directly, as main is already the current active queue
    else
        dispatch_sync(dispatch_get_main_queue(), block); // ask the main queue, which is not the current queue, to execute the block, and wait for it to be executed before continuing
}

-(void)toastErrorRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    NSString * capsule_nom_court = notification.userInfo[@"capsule_nom_court"];
    
    dispatch_block_t block = ^{
        UIImage * icon = [downloadedRessource getIcone];
        NSString * title = [capsule_nom_court stringByAppendingFormat:@" : %@", [LanguageManager get:@"ios_label_erreur_telechargement"]];
        NSString * name = downloadedRessource.nom_court;
        [self.view makeToast:title duration:3.0 position:@"bottom" title: name image: icon style:nil completion:nil];
    };
    
    if ([NSThread isMainThread])
        block(); // execute the block directly, as main is already the current active queue
    else
        dispatch_sync(dispatch_get_main_queue(), block); // ask the main queue, which is not the current queue, to execute the block, and wait for it to be executed before continuing
    
}
#pragma mark - Popovers

- (void)displayNotifications:(UIButton*)sender
{
    NotificationTVC * content = [[NotificationTVC alloc] initWithNotificationType:@"un"];
    [content setParent_vc:self];
    UIPopoverController* aPopover = [[UIPopoverController alloc]
                                     initWithContentViewController:content];
//    aPopover.delegate = self;
    
    // Store the popover in a custom property for later use.
    self.user_notif_popover = aPopover;
    
    [self removeBarBadgeOfType:@"un"];
    [aPopover presentPopoverFromRect:[sender frame] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    [aPopover presentPopoverFromBarButtonItem:sender
//                                   permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)displayScoreChanges:(UIButton*)sender
{
    NotificationTVC * content = [[NotificationTVC alloc] initWithNotificationType:@"us"];
    [content setParent_vc:self];
    
    UIPopoverController* aPopover = [[UIPopoverController alloc]
                                     initWithContentViewController:content];
    //    aPopover.delegate = self;
    
    // Store the popover in a custom property for later use.
    self.user_score_popover = aPopover;
    
    [self removeBarBadgeOfType:@"us"];
    
    [aPopover presentPopoverFromRect:[sender frame] inView:(UIView*)sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    //    [aPopover presentPopoverFromBarButtonItem:sender
    //                                   permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - Barre de notification

-(void)updateFromNotification:(NSNotification*)theNotification
{
    if ([UIViewController isConnected]) {
        NSDictionary * notif_options = [theNotification userInfo];
        
        if ([notif_options[@"t"] isEqualToString:@"us"]) {
            
            //Référence des points
            NSDictionary * PTS_def = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PTS_def" ofType:@"plist"]];
            NSString * points = PTS_def[notif_options[@"st"]];
            
            if (!points) {
                points = notif_options[@"st"];
                if (!points)
                    points = @"";
            }
            
            //Récup du bouton
            UIBarButtonItem * user_score_button = _rightBarButtonsArray[1];
            
            if ([[user_score_button customView] viewWithTag:1000]) {
                UIView * badgeView = [[user_score_button customView] viewWithTag:1000];
                UILabel * repLabel = (UILabel*)[badgeView viewWithTag:1001];
                int old_points = [repLabel.text intValue];
                int new_points = [points intValue] + old_points;
                
                if(new_points < 0){
                    [repLabel setText:[NSString stringWithFormat:@"%d", new_points]];
                    [badgeView setBackgroundColor:[UIColor colorWithRed:0.992 green:0.847 blue:0 alpha:0.9]];
                }
                else{
                    [repLabel setText:[NSString stringWithFormat:@"+%d", new_points]];
                    [badgeView setBackgroundColor:[UIColor colorWithRed:0.275 green:0.71 blue:0.145 alpha:0.9]];
                }
            }
            else{
                [[user_score_button customView] addSubview: [self makeBarBadgeWithNumber:[points intValue] andType:@"us"]];
            }
            
            //Update du décompte de point en haut à droite
            NSNumber * concerned_id_capsule = [NSNumber numberWithInt:[notif_options[@"c"] intValue]];
            
            //On ajoute le score à l'user en cours
            [UIViewController addScore:[points intValue] toRankOfCapsule: concerned_id_capsule];
            
            //Et on le montre, si besoin il y a
            if ([self arePointsForRessourceVisibles]) {
                [self showNumberOfPointsForCapsule:concerned_id_capsule];
            }
        }
        else if ([notif_options[@"t"] isEqualToString:@"un"]) {
            UIBarButtonItem * user_notification_button = _rightBarButtonsArray[2];
            
            if ([[user_notification_button customView] viewWithTag:1000]) {
                UIView * badgeView = [[user_notification_button customView] viewWithTag:1000];
                UILabel * repLabel = (UILabel*)[badgeView viewWithTag:1001];
                int old_count = [repLabel.text intValue];
                int new_count = old_count +1;
                
                [repLabel setText:[NSString stringWithFormat:@"%d", new_count]];
            }
            else{
                [[user_notification_button customView] addSubview: [self makeBarBadgeWithNumber:1 andType:@"un"]];
            }
        }
        
        [self.navigationBar setNeedsLayout];

    }
}
-(BOOL) arePointsForRessourceVisibles{
    UIView* userBarView = [_userBar customView];
    UITextField * textField_points = (UITextField*)[userBarView viewWithTag:3];
    
    return ![textField_points isHidden];
}
-(void) showNumberOfPointsForCapsule:(NSNumber*)id_capsule{
    if ([UIViewController isConnected]) {
        UIView* userBarView = [_userBar customView];
        UITextField * textField_points = (UITextField*)[userBarView viewWithTag:3];
        
        NSString * points = [[UIViewController getRankForCapsule:id_capsule] objectForKey:@"score"];
        if (points) {
            [textField_points setHidden:NO];
            [textField_points setText:[NSString stringWithFormat:@"%@ pts", points]];
        }
        
    }
}

-(void) hideNumberOfPointsForCapsule:(NSNumber*)id_capsule{
    if ([UIViewController isConnected]) {
        UIView* userBarView = [_userBar customView];
        UITextField * textField_points = (UITextField*)[userBarView viewWithTag:3];
        [textField_points setHidden:YES];
        [textField_points setText:@"... pts"];
    }
}
-(void) showNumberOfPointsForRessource:(NSNumber*)id_ressource{
    if ([UIViewController isConnected]) {
        UIView* userBarView = [_userBar customView];
        UITextField * textField_points = (UITextField*)[userBarView viewWithTag:3];
        
        NSString * points = [[UIViewController getRankForRessource:id_ressource] objectForKey:@"score"];
        if (points) {
            [textField_points setHidden:NO];
            [textField_points setText:[NSString stringWithFormat:@"%@ pts", points]];
        }
        
    }
}

-(void) hideNumberOfPointsForRessource:(NSNumber*)id_ressource{
    if ([UIViewController isConnected]) {
        UIView* userBarView = [_userBar customView];
        UITextField * textField_points = (UITextField*)[userBarView viewWithTag:3];
        [textField_points setHidden:YES];
        [textField_points setText:@"... pts"];
    }
}

-(UIView*) makeBarBadgeWithNumber:(int)number andType:(NSString*)type{
    UIView * badgeView = [[UIView alloc] initWithFrame:CGRectMake(4, 10, 32, 20)];
    badgeView.layer.cornerRadius = 8;
    badgeView.layer.masksToBounds = YES;
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 32, 20)];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTag:1001];
    
    if ([type isEqualToString:@"us"]) {
        
        if(number < 0){
            [label setText:[NSString stringWithFormat:@"%d", number]];
            [badgeView setBackgroundColor:[UIColor colorWithRed:0.992 green:0.847 blue:0 alpha:0.9]];
        }
        else{
            [label setText:[NSString stringWithFormat:@"+%d", number]];
            [badgeView setBackgroundColor:[UIColor colorWithRed:0.275 green:0.71 blue:0.145 alpha:0.9]];
        }
        
        
    } else if ([type isEqualToString:@"un"]) {
        [label setText:[NSString stringWithFormat:@"%d", number]]; 
        [badgeView setBackgroundColor:[UIColor colorWithRed:0.286 green:0.694 blue:0.851 alpha:0.9]];
    }
    
    [badgeView addSubview:label];
    [badgeView setTag:1000];
    //Pour l'animer par la suite, on le cache
    [badgeView setAlpha:0];
    [UIView animateWithDuration:1.0 animations:^{
        [badgeView setAlpha:1];
    }];
    [badgeView setUserInteractionEnabled:NO];
    //On place la vue
    return badgeView;
}

-(void) removeBarBadgeOfType:(NSString*)type{
    UIBarButtonItem * user_bar_button;
    
    if ([type isEqualToString:@"us"])
        //Récup du bouton
        user_bar_button = _rightBarButtonsArray[1];
    else
        user_bar_button = _rightBarButtonsArray[2];
    
    //Reset des notifications
    [UtilsCore setNotificationsSeenForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:type];
    
    UIView * badgeView = [[user_bar_button customView] viewWithTag:1000];
    
    if (badgeView) {
        [badgeView removeFromSuperview];
    }

}

-(void) initNotificationsBarButtons{
    [self setRightBarButtonItemsWithOptions:@[@{@"imageName": @"chart_area_48", @"selectorName" : @"displayScoreChanges:"}, @{@"imageName": @"speaker_48", @"selectorName" : @"displayNotifications:"}] fromViewController:self];
    
    //Boutton de réputation
    UIBarButtonItem * user_score_button = _rightBarButtonsArray[1];
    //Récup du total
    int totalRep = [UtilsCore getUnseenNotificationTotalForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:@"us"];
    //On place la réputation totale
    if (totalRep != 0) {
        [[user_score_button customView] addSubview: [self makeBarBadgeWithNumber:totalRep andType:@"us"]];
    }
    
    //Boutton de réputation
    UIBarButtonItem * user_notif_button = _rightBarButtonsArray[2];
    //Récup du total
    int totalNotif = [UtilsCore getUnseenNotificationTotalForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:@"un"];
    //On place la réputation totale
    if (totalNotif != 0) {
        [[user_notif_button customView] addSubview: [self makeBarBadgeWithNumber:totalNotif andType:@"un"]];
    }

    [self.navigationBar setNeedsLayout];
}
@end
