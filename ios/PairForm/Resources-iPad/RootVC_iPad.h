//
//  RootVC.h
//  PairForm
//
//  Created by Maen Juganaikloo on 01/10/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "SCStackViewController.h"
#import "MenuVC_iPad.h"

#import "SCOverlayView.h"
@interface RootVC_iPad : UIViewController

@property (nonatomic, strong) SCStackViewController *stackViewController;
@property (nonatomic, strong) MenuVC_iPad * menuController;

-(void)hideOverlay;
- (void)overlayViewDidReceiveTap:(SCOverlayView *)overlayView;

@end
