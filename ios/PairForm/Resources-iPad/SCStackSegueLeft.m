//
//  SCStackSegueLeft.m
//  PairForm
//
//  Created by Maen Juganaikloo on 03/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "SCStackSegueLeft.h"
#import "SCStackViewController.h"


@implementation SCStackSegueLeft

- (void)perform
{

    SCStackViewController * stackViewController = [self.sourceViewController sc_stackViewController];
    [self.destinationViewController setStackPosition:SCStackViewControllerPositionLeft];
    
    //Si le controller source est à droite, on sucre les controllers qui sont après lui dans la pile
    if ([self.sourceViewController stackPosition] == SCStackViewControllerPositionLeft) {
        [stackViewController popViewControllersAfter:self.sourceViewController AtPosition:SCStackViewControllerPositionLeft animated:YES completion:^{
            [stackViewController pushViewController:self.destinationViewController fromViewController:self.sourceViewController atPosition:SCStackViewControllerPositionLeft unfold:TRUE animated:TRUE completion:nil];
        }];
    }
    //Sinon, on les sucre tous
    else{
        [stackViewController popToRootViewControllerFromPosition:SCStackViewControllerPositionLeft animated:YES completion:^{
            [stackViewController pushViewController:self.destinationViewController fromViewController:self.sourceViewController atPosition:SCStackViewControllerPositionLeft unfold:TRUE animated:TRUE completion:nil];
        }];
    }
}

@end
