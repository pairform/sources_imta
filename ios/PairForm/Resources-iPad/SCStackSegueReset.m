//
//  SCStackSegue.m
//  PairForm
//
//  Created by Maen Juganaikloo on 17/04/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "SCStackSegueReset.h"
#import "SCStackViewController.h"

@implementation SCStackSegueReset

- (void)perform
{
    SCStackViewController * stackViewController = [self.sourceViewController sc_stackViewController];
    [self.destinationViewController setStackPosition:[self.sourceViewController stackPosition]];
    [stackViewController popToRootViewControllerFromPosition:[self.sourceViewController stackPosition] animated:YES completion:^{
         [stackViewController pushViewController:self.destinationViewController fromViewController:self.sourceViewController atPosition:[self.sourceViewController stackPosition] unfold:TRUE animated:TRUE completion:nil];
    }];
}

@end
