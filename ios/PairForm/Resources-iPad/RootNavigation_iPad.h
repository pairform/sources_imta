//
//  RootNavigation.h
//  SCStackViewController
//
//  Created by Maen Juganaikloo on 14/04/14.
//  Copyright (c) 2014 Stefan Ceriu. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RootNavigation_iPad : UINavigationController 

@property (nonatomic, strong) UIPopoverController * user_score_popover;
@property (nonatomic, strong) UIPopoverController * user_notif_popover;
@property (nonatomic,strong) UIBarButtonItem * userBar;
-(void)setRightBarButtonItemWithImageName:(NSString*)imageName andSelectorName:(NSString*)selectorName fromViewController:(UIViewController*)viewController;
-(void)setRightBarButtonItemsWithOptions:(NSArray*)barButtonItemsOptions fromViewController:(UIViewController*)viewController;
-(void)setLeftBarButtonItemWithImageName:(NSString*)imageName andSelectorName:(NSString*)selectorName fromViewController:(UIViewController*)viewController;
-(void)setLeftBarButtonItemsWithOptions:(NSArray*)barButtonItemsOptions fromViewController:(UIViewController*)viewController;
-(void)removeLeftBarButtonItemWithIdentifier:(NSString*)imageName;
-(void)removeRightBarButtonItemWithIdentifier:(NSString*)imageName;



-(void) showNumberOfPointsForRessource:(NSNumber*)id_ressource;
-(void) hideNumberOfPointsForRessource:(NSNumber*)id_ressource;
-(void) showNumberOfPointsForCapsule:(NSNumber*)id_capsule;
-(void) hideNumberOfPointsForCapsule:(NSNumber*)id_capsule;

//-(void)displayNotifications:(id)sender;
@end
