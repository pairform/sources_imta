//
//  SCStackSegue.h
//  PairForm
//
//  Created by Maen Juganaikloo on 17/04/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCStackSegueReset : UIStoryboardSegue

@end
