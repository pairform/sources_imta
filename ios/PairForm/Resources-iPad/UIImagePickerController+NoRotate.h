//
//  UIImagePickerController+NoRotate.h
//  PairForm
//
//  Created by Maen Juganaikloo on 23/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImagePickerController (NoRotate)

@end
