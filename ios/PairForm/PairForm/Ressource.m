//
//  Ressource.m
//  PairForm
//
//  Created by Maen Juganaikloo on 18/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "Ressource.h"
#import "Capsule.h"


@implementation Ressource

@dynamic cree_par;
@dynamic date_creation;
@dynamic date_edition;
@dynamic description_pf;
@dynamic espace_nom_court;
@dynamic icone;
@dynamic icone_etablissement;
@dynamic id_langue;
@dynamic id_ressource;
@dynamic index;
@dynamic licence;
@dynamic nom_court;
@dynamic nom_long;
@dynamic theme;
@dynamic url_logo;
@dynamic capsules;

//iVar pour contenir les capsules


-(UIImage*) getIcone {
    if ([self icone]) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:[self icone]];
    }
    else{
        return  [UIImage imageNamed:@"noRessourcePic"];
//        //On récupère l'image distante
//        UIImage * icone = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[sc_server_static stringByAppendingString:self.url_logo]]]];
//        //S'il n'y a pas de connexion internet
//        if (!icone) {
//            return  [UIImage imageNamed:@"noRessourcePic"];
//        }
//        //Sinon, on stocke l'icone dans la variable
//        [self setIcone:[NSKeyedArchiver archivedDataWithRootObject:icone]];
//        return icone;
    }
    
}

-(UIImage*) getIconeEtablissement {
    if ([self icone_etablissement]) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:[self icone_etablissement]];
    }
    else{
        return  [UIImage imageNamed:@"logo_espace_pf"];
    }
}
@end
