//
//  PJMessage+CoreDataProperties.m
//  PairForm
//
//  Created by Maen Juganaikloo on 14/01/2016.
//  Copyright © 2016 Ecole des Mines de Nantes. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PJMessage+CoreDataProperties.h"

@implementation PJMessage (CoreDataProperties)

@dynamic data;
@dynamic extension;
@dynamic id_message;
@dynamic nom_original;
@dynamic nom_serveur;
@dynamic taille;
@dynamic thumbnail;
@dynamic message;

@end
