//
//  UIViewController+Internationalisation.m
//  PairForm
//
//  Created by Maen Juganaikloo on 27/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "UIViewController+Internationalisation.h"

@implementation UIViewController (Internationalisation)

-(void)viewDidLoad{
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    if ([self respondsToSelector:@selector(setContent)]) {
        [self performSelector:@selector(setContent)];
    }

}

@end
