//
//  CGPlayGround.m
//  CGPlayground
//
//  Created by Maen Juganaikloo on 10/03/14.
//  Copyright (c) 2014 Maen Juganaikloo. All rights reserved.
//

#import "SpringboardItem.h"

@interface SpringboardItem()

@property (nonatomic, strong) NSTimer* chilling_timer;
@property (nonatomic, strong) NSTimer* merging_timer;

@end
@implementation SpringboardItem

int download_timer_counter;
bool is_downloading;
float download_progression;

bool is_chilling;
float chilling_timer_counter;
float chilling_time = 1000;
float chilling_alpha;


bool is_merging;
float merging_timer_counter;
float merging_time = 300;
float merging_progression;
float merging_timer_max;
int cool_framerate = 60;

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ([super initWithCoder:aDecoder]) {
        
        [self setFolderColor:[self randomColor]];
        
        [self setIconRect:CGRectMake(0, 0, 80, 80)];
    }
    return self;
}
#pragma mark - Drawing methods
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
   
//    [self drawIconsInsideCircle:context withRect:CGRectMake(100, 200, _iconRect.size.width, _iconRect.size.height) andThickness:2 andImages:@[@"PI_uTOP",@"SQL",@"SQL"]];

    
    if (is_merging && (merging_progression != 0)){
        
        [self drawIconMergingInsideCircle:context withRect:_iconRect andThickness:2 andImage:_itemImage forProgression:merging_progression];
    }
    else if (is_downloading && (download_progression != 0)) {
        [self drawIconWithMask:context withRect:_iconRect andImage:_itemImage forProgression:1-download_progression/100];
    }
    else{
        [self drawIconInsideZone:context withRect:_iconRect andImage:_itemImage];
    }
    
    //Dessin du nom
//    [self drawItemName:context withRect:_titleRect];
    
}

-(void)drawItemName:(CGContextRef)context withRect:(CGRect)rect{
    
}
-(void)drawIconWithMask:(CGContextRef)context withRect:(CGRect)rect andImage:(UIImage*)itemImage forProgression:(float)progression{
    CGContextSaveGState(context);
    
    [[UIColor whiteColor] setFill];
    [self drawIconInsideZone:context withRect:rect andImage:itemImage];
    
    CGContextSetAlpha(context, 0.95);
    CGContextSetBlendMode(context, kCGBlendModeLighten);
    CGRect mask_rect = rect;
    mask_rect.size.height = mask_rect.size.height * progression;
    CGContextSetShadowWithColor(context, CGSizeMake(0, 5), 10.0, [[UIColor whiteColor] CGColor]);
    CGContextFillRect(context, mask_rect);
    CGContextRestoreGState(context);
}

-(void)drawIconWithFade:(CGContextRef)context withRect:(CGRect)rect andImage:(UIImage*)itemImage forProgression:(float)progression{
    CGContextSaveGState(context);
    
    [[UIColor whiteColor] setFill];
    CGContextSetAlpha(context, 0.3);
    [self drawIconInsideZone:context withRect:rect andImage:itemImage];
    CGContextSetAlpha(context, progression);
    CGContextSetBlendMode(context, kCGBlendModeLighten);
    CGContextFillRect(context, rect);
    CGContextRestoreGState(context);

}

-(void)drawRadialGradient:(CGContextRef)context {
    CGContextSaveGState(context);
    
    CGContextRestoreGState(context);
}
-(void)drawCircle:(CGContextRef)context withRect:(CGRect)size andBorderThickness:(int)thickness{
    CGContextSaveGState(context);
    [[self darkerColorForColor:_folderColor] setFill];
    [_folderColor setStroke];
    
    CGRect borderRect = CGRectInset(size, thickness, thickness);
    
    CGContextSetLineWidth(context, 4);
    CGContextSetShadowWithColor(context, CGSizeMake(0, 2), 5.0, [[UIColor grayColor] CGColor]);
    CGContextAddEllipseInRect(context, borderRect);
    
    
    CGContextSetAlpha(context, 0.4);
    CGContextFillPath(context);

    CGContextAddEllipseInRect(context, size);
    
    CGContextSetAlpha(context, 1);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}
-(void)drawIconInsideZone:(CGContextRef)context withRect:(CGRect)rect andImage:(UIImage*)itemImage{
    CGContextSaveGState(context);
    if (is_chilling)
        CGContextSetAlpha(context, chilling_alpha);
    else
        CGContextSetAlpha(context, 1);
    
    UIImage * originalImage = itemImage;
    
    float sf = originalImage.size.width / rect.size.width;

    UIImage *scaledImage =
    [UIImage imageWithCGImage:[originalImage CGImage]
                        scale:sf
                  orientation:(originalImage.imageOrientation)];

    CGContextTranslateCTM(context, 0, scaledImage.size.height + rect.origin.y*2);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGPathRef clippath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:14.0].CGPath;
    CGContextAddPath(context, clippath);
    CGContextClip(context);
    CGContextDrawImage(context, rect, [scaledImage CGImage]);
//    [scaledImage drawInRect:rect];
    
    CGContextRestoreGState(context);
}

-(void)drawIconsInsideZone:(CGContextRef)context withRect:(CGRect)rect andImages:(NSArray*)itemImagesArray{
//    
//    CGRect slice, remainder;
//    CGRectDivide(rect, &slice, &remainder, rect.size.width /2, CGRectMinXEdge);
    CGContextSaveGState(context);
    
    int numberOfItems =[itemImagesArray count] <3 ? [itemImagesArray count] : 3;
   
    for (int i= numberOfItems; i > 0 ; i--) {
//        CGRect divided_rect = i % 2 ? slice : rect;
        CGFloat sf, x, y, width, height;
        CGRect imgRect;
        CGSize imgSize;
        
        switch (i) {
            case 1:
                //Calcul de la future taille
                sf = 0.5;
                imgSize = CGSizeMake(rect.size.width * sf, rect.size.height * sf);
                
                x = rect.origin.x + (rect.size.width - imgSize.width)/2;
                y = rect.origin.y + (rect.size.height - imgSize.height)/2;
                width = imgSize.width;
                height = imgSize.height;
                break;
            case 2:
                
                sf = 0.4;
                
                imgSize = CGSizeMake(rect.size.width * sf, rect.size.height * sf);
                
                x = rect.origin.x + (rect.size.width /20);
                y = rect.origin.y + (rect.size.height - imgSize.height)/2;
                width = imgSize.width;
                height = imgSize.height;
                break;
            case 3:
                sf = 0.4;
                
                imgSize = CGSizeMake(rect.size.width * sf, rect.size.height * sf);
                
                x = rect.origin.x + (rect.size.width - imgSize.width - rect.size.width /20);
                y = rect.origin.y + (rect.size.height - imgSize.height)/2;
                width = imgSize.width;
                height = imgSize.height;
                break;
        }
        
        imgRect = CGRectMake(x,y,width,height);
//        CGRect imgRect = CGRectApplyAffineTransform(rect, CGAffineTransformMakeScale(sf, sf));

        
        if (is_chilling)
            CGContextSetAlpha(context, chilling_alpha);
        else
            CGContextSetAlpha(context, 1);
        
        UIImage * originalImage = itemImagesArray[i-1];
        UIImage *scaledImage =
        [UIImage imageWithCGImage:[originalImage CGImage]
                            scale:sf
                      orientation:(originalImage.imageOrientation)];
        
        CGContextSaveGState(context);
        
        CGContextTranslateCTM(context, 0, rect.size.height + rect.origin.y*2);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        CGContextDrawImage(context, imgRect, [scaledImage CGImage]);
        
        CGContextRestoreGState(context);
//        [scaledImage drawInRect:imgRect];

    }
    
    CGContextRestoreGState(context);
}

-(void)drawIconsInsideCircle:(CGContextRef)context withRect:(CGRect)rect andThickness:(int)thickness andImages:(NSArray*)itemImagesArray{
    
    [self drawCircle:context withRect:rect andBorderThickness:thickness];
    [self drawIconsInsideZone:context withRect:rect andImages:itemImagesArray];
}


-(void)drawIconMergingInsideCircle:(CGContextRef)context withRect:(CGRect)rect andThickness:(int)thickness andImage:(UIImage*)itemImage forProgression:(float)progression{
    
    CGRect circleRect = rect;
    circleRect.size.width *= progression;
    circleRect.size.height *= progression;
    circleRect.origin.x += (rect.size.width - circleRect.size.width) /2;
    circleRect.origin.y += (rect.size.height - circleRect.size.height) /2;
    
    [self drawCircle:context withRect:circleRect andBorderThickness:thickness];
    
    CGRect iconsRect = rect;
    iconsRect.size.width *= 1 - 0.5 * progression;
    iconsRect.size.height *= 1 - 0.5 * progression;
    iconsRect.origin.x += (rect.size.width - iconsRect.size.width) /2;
    iconsRect.origin.y += (rect.size.height - iconsRect.size.height) /2;
    
    [self drawIconInsideZone:context withRect:iconsRect andImage:itemImage];
}
#pragma mark - Downloading effect

-(void)displayDownloadingWithTimer:(NSTimer *)timer{
    if (++download_timer_counter <= 300) {
        [self displayDownloadingWithPercentage:(download_timer_counter/300.f) * 100];
    }
    else{
        download_timer_counter =0;
        is_downloading = false;
        [timer invalidate];
    }
}

-(void)displayDownloadingWithPercentage:(float)percentage{
    is_downloading = true;
    download_progression = percentage;
    [self setNeedsDisplay];
}

#pragma mark - Chilling effects

-(void)startChillingEffect{
    is_chilling = YES;
    
    _chilling_timer = [NSTimer scheduledTimerWithTimeInterval:(float)(chilling_time/1000)/cool_framerate
                                     target:self
                                   selector:@selector(increaseChillingTimer:)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)increaseChillingTimer:(NSTimer*)timer{
    
    int chilling_timer_max = (chilling_time/1000) * cool_framerate;
    
    if (++chilling_timer_counter <= chilling_timer_max) {
        
        float x = ((float)chilling_timer_counter / chilling_timer_max) *2 -1;
        chilling_alpha = 0.5*(x*x) + 0.6;
        [self setNeedsDisplay];
        
    }
    else{
        chilling_timer_counter = 0;
        chilling_alpha = 1;
        [self setNeedsDisplay];
    }
}

-(void)stopChillingEffect{
    [_chilling_timer invalidate];
    is_chilling = NO;
    chilling_timer_counter = 0;
    chilling_time = 1;
    [self setNeedsDisplay];
}

#pragma mark - Merging effects

-(void)startMergingEffect{
    
    merging_timer_max = (merging_time/1000) * cool_framerate;
    is_merging = true;
    _merging_timer = [NSTimer scheduledTimerWithTimeInterval:(float)(merging_time/1000)/cool_framerate
                                                       target:self
                                                     selector:@selector(increaseMergingTimer:)
                                                     userInfo:nil
                                                      repeats:YES];
}

-(void)stopMergingEffect{
    
    merging_timer_max = (merging_time/1000) * cool_framerate;
    //Au cas ou
    is_merging = true;
    //On invalide le timer précédent
    [_merging_timer invalidate];
    //On le reprogramme
    _merging_timer = [NSTimer scheduledTimerWithTimeInterval:(float)(merging_time/1000)/cool_framerate
                                                      target:self
                                                    selector:@selector(decreaseMergingTimer:)
                                                    userInfo:nil
                                                     repeats:YES];

}

-(void)mergeIconInFolder{
    
}

-(void)increaseMergingTimer:(NSTimer*)timer{
    
    
    if (++merging_timer_counter <= merging_timer_max) {
        
        merging_progression = (float)merging_timer_counter/ merging_timer_max;
        [self setNeedsDisplay];
        
    }
    else{
        is_merging = true;
        [_merging_timer invalidate];
        [self setNeedsDisplay];
    }
    
}


-(void)decreaseMergingTimer:(NSTimer*)timer{
    
    
    if (--merging_timer_counter >= 0) {
        
        merging_progression = (float)merging_timer_counter/ merging_timer_max;
        
        [self setNeedsDisplay];
        
    }
    else{
        is_merging = false;
        [_merging_timer invalidate];
        [self setNeedsDisplay];
    }
    
}

#pragma mark - Helper function

-(UIColor *)randomColor
{
    CGFloat red = arc4random_uniform(255) / 255.0;
    CGFloat green = arc4random_uniform(255) / 255.0;
    CGFloat blue = arc4random_uniform(255) / 255.0;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    return color;
}
- (UIColor *)lighterColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MIN(r + 0.2, 1.0)
                               green:MIN(g + 0.2, 1.0)
                                blue:MIN(b + 0.2, 1.0)
                               alpha:a];
    return nil;
}

- (UIColor *)darkerColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MAX(r * 0.5, 0.0)
                               green:MAX(g * 0.5, 0.0)
                                blue:MAX(b * 0.5, 0.0)
                               alpha:a];
    return nil;
}
@end
