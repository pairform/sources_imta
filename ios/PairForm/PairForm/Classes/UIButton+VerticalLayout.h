//
//  UIButton+VerticalLayout.h
//  PairForm
//
//  Created by Maen Juganaikloo on 07/10/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (VerticalLayout)

- (void)centerVerticallyWithImageWidth:(float)width andVerticalPadding:(float)padding;
- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;

@end

