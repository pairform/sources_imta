//
//  VideoVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 13/01/2016.
//  Copyright © 2016 Ecole des Mines de Nantes. All rights reserved.
//

#import "VideoVC.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoVC ()
@property (nonatomic, strong) NSString * temp_file;
@property (nonatomic, strong) MPMoviePlayerController * player;
@end

@implementation VideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _temp_file = [NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.mp4"];
    [self.video writeToFile:_temp_file atomically:YES];
    
    _player = [[MPMoviePlayerController alloc] initWithContentURL: [NSURL URLWithString:_temp_file]];
    [_player setMovieSourceType:MPMovieSourceTypeFile];
    [_player setFullscreen:NO];
    [_player prepareToPlay];
    [_player setControlStyle:MPMovieControlStyleEmbedded];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [_player.view setFrame: self.view.bounds];  // player's frame must match parent's
    [self.view addSubview: _player.view];
    // ...
    [_player play];
}
-(void)viewWillDisappear:(BOOL)animated{
    NSError * error;
    [[NSFileManager defaultManager] removeItemAtPath:_temp_file error:&error];
    DLog(@"%@",error);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
