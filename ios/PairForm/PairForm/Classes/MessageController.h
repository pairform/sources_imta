//
//  RecupererMessagesViewController.h
//  SupCast
//
//  Created by fgutie10 on 28/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "Message.h"
#import "Capsule.h"
#import "PageVC.h"
#import "SommaireVC.h"
//#import "GCTagList.h"
#import "MessageCell.h"
#import "Utilisateur.h"
#import "SIAlertView.h"
#import "RMPickerViewController.h"

#import "MessageOptionController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

typedef enum {
    ModeNouveauMessageNormal,
    ModeNouveauMessageReponse,
    ModeNouveauMessageEdition,
    ModeNouveauMessageNone
} ModeNouveauMessage;

typedef enum {
    ModeMessageNormal,
    ModeMessageTransversal,
    ModeMessageLatest
} ModeMessage;

typedef enum {
    ModeTriVote,
    ModeTriDate,
    ModeTriMedaille
} ModeTri;

typedef enum {
    TransversalNo,
    TransversalRessource,
    TransversalUtilisateur,
    TransversalCercle,
    TransversalTag
} Transversal;


@interface MessageController : UITableViewController <MessageOptionControllerDelegate, UIActionSheetDelegate, UITextViewDelegate, UISearchBarDelegate, UITableViewDelegate, RMPickerViewControllerDelegate, UIGestureRecognizerDelegate>



@property (strong, nonatomic) NSNumber * idCapsule;
@property (strong, nonatomic) NSString * urlPage;
@property (strong, nonatomic) Message * selectedMessage;
@property (strong, nonatomic) NSIndexPath * selectedMessagePath;
@property(nonatomic,strong) Capsule * currentCapsule;
@property(nonatomic,strong) NSString * tag;
@property(nonatomic,strong) NSNumber * num_occurence;
@property(nonatomic) ModeNouveauMessage  modeNouveauMessage;
@property(nonatomic) ModeMessage  modeMessage;
@property(nonatomic) ModeTri  modeTri;
@property(nonatomic) Transversal transversal;
@property (nonatomic) NSNumber * id_utilisateur;
@property (nonatomic) NSMutableArray * id_utilisateurs;
@property (nonatomic) NSString * tag_message;

@property (nonatomic, strong) UITextView * messageTextView;
@property (nonatomic, strong) UIButton * boutonLangue;
@property (nonatomic, strong) UIButton * boutonFiltre;

//@property(nonatomic,strong) NSString *urlPage;
@property(nonatomic) NSNumber  * id_message_trans;
@property(nonatomic) NSNumber  * idMessage;
@property(nonatomic,strong) NSNumber *  idMessageOriginal;
@property(nonatomic,strong) NSString * nouveau_message_nom_tag;
@property(nonatomic,strong) NSNumber * nouveau_message_num_occurence;
@property(nonatomic,strong) NSMutableArray * visibiliteIdArray;
@property(nonatomic,strong) NSMutableArray * visibiliteArray;
@property(nonatomic,strong) NSMutableArray * tagArray;
@property(nonatomic) BOOL  boolDefi;
@property(nonatomic) BOOL publiTwitter;

-(void)refreshMessages;
- (IBAction)selectionneProfil:(id)sender;
- (IBAction)selectionneLienVersCapsule:(id)sender;
- (IBAction)selectionneLienVersChoixLangue:(id)sender;

- (IBAction)swipeToLeft:(id)sender;
- (IBAction)swipeToRight:(id)sender;
- (IBAction)singleTapGesture:(id)sender;
- (IBAction)tapGesture:(id)sender;
- (IBAction)afficherFiltres:(id)sender;
-(IBAction)voterPositif:(id)sender;
-(IBAction)voterNegatif:(id)sender;

@end
