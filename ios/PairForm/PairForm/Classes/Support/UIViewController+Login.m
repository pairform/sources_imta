//
//  UIViewController+Login.m
//  SupCast
//
//  Created by Maen Juganaikloo on 29/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "UIViewController+Login.h"
#import "SIAlertView.h"
#import "RootVC.h"
#import "EnregistrementVC.h"
#import "LanguageManager.h"

#import <Crashlytics/Crashlytics.h>

@implementation UIViewController (Login)

#pragma mark - Méthodes de vérification de login

-(BOOL) checkForDefaultUser{
    
    if([SCAppDelegate loginEnCours])
        return false;
    
    //Si il y a un utilisateur enregistré sur l'appareil
    if([UIViewController getPermamentValue:@"username"] && [[UIViewController getPreference:@"rememberMe"] boolValue])
    {
        return true;
    }
    else
    {
        //Si l'user ne veut pas être connecté
        if (![self userDontWantLogin])
        {
            [self popUpFirstLogin];
        }
        return false;
    }
}

-(void) setNoLogin:(BOOL)status{
    [[NSUserDefaults standardUserDefaults] setBool:status forKey:@"no_login"];
}

-(BOOL) userDontWantLogin{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"no_login"];
}

+(BOOL) isConnected{
    //Si il y a un utilisateur connecté sur l'appareil, ya les données du webService
    if((BOOL)[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kLogin] count])
        return true;
    else
        return false;
}

+(BOOL) estLieAuServeur{
    NSDictionary * login_user_info = [[NSUserDefaults standardUserDefaults] volatileDomainForName:kLoginUserInfo];
    
    DLog(@"================= estLieAuServeur : %@ ================", login_user_info[@"liaison_status"]);
    if(login_user_info && [login_user_info[@"liaison_status"] isEqualToNumber:@1])
        return true;
    
    return false;
}

+(void) setLieAuServeur:(BOOL)etatLiaison{
    DLog(@"================= setLieAuServeur :%d ================", etatLiaison);
    id value;
    //
    if(etatLiaison)
        value = @1; // Lié
    else
        value = @0; // Pas lié
    
    [[NSUserDefaults standardUserDefaults] setVolatileDomain:[[NSDictionary alloc] initWithObjectsAndKeys:value,@"liaison_status", nil] forName:kLoginUserInfo];
}

-(BOOL) checkForLoggedInUser{
    
    DLog(@"=================Check for user ================");
    //C'est nul ce fonctionnement : si le flag est la, on ne peut jamais s'authentifier
    if([SCAppDelegate loginEnCours])
        return false;
    
    if ([UIViewController isConnected]) {
        return true;
    }
    
    //Si il y a un utilisateur enregistré sur l'appareil
    if([UIViewController getPermamentValue:@"username"] && [[UIViewController getPreference:@"rememberMe"] boolValue])
    {
        //Mais pas connecté, avec une liaison au serveur
        if(![UIViewController isConnected] && [UIViewController estLieAuServeur]){
            //On le log sans prompt
            return [self logUser:[UIViewController getPermamentValue:@"username"] withPassword:[UIViewController getPermamentValue:@"password"]];
        }
        else
            return true;
    }
    else
    {
        [self popUpLogin];
        return false;
    }
}

-(BOOL) connecterUtilisateurLocalEnSilence{
    //Si il y a un utilisateur enregistré sur l'appareil
    if([UIViewController getPermamentValue:@"username"] && [[UIViewController getPreference:@"rememberMe"] boolValue])
    {
        //Avec une liaison au serveur
//        if([UIViewController estLieAuServeur]){
            //On le log sans prompt
            return [self logUser:[UIViewController getPermamentValue:@"username"] withPassword:[UIViewController getPermamentValue:@"password"]];
//        }
//        else
//            return true;
    }
    return false;
}

#pragma mark - PopUps

-(void) popUpFirstLogin{
    [SCAppDelegate loginEnCours:true];
    
    DLog(@"=================popUpFirstLogin ================");

    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
        //Préférences par défaut pour monsieur
        [UIViewController setDefaultsPreferences];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
    }
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_bienvenue"] andMessage:[LanguageManager get:@"ios_label_profiter_fonction"]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_creer_un_compte"]
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Login");
                              [self pushRegister];
                              
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_connexion"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Login");
                              [self popUpLogin];
                              
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"ios_button_sans_connexion"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Annulation du log");
                              [self setNoLogin:true];
                              [self updateMessages];
                              [SCAppDelegate loginEnCours:false];
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
}

-(void) pushRegister{

    if (IS_IPAD) {
        EnregistrementVC * accountVC = [[UIStoryboard storyboardWithName:@"AccountStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"EnregistrementVC"];
        [accountVC setStackWidth:320];
        if ([self isKindOfClass:NSClassFromString(@"RootNavigation_iPad")]) {
            [[(RootVC_iPad*)[(RootNavigation_iPad*)self viewControllers][0] stackViewController] pushViewController:accountVC atPosition:SCStackViewControllerPositionLeft unfold:YES animated:YES completion:nil];
        }
        else
            [[self sc_stackViewController] pushViewController:accountVC atPosition:SCStackViewControllerPositionLeft unfold:YES animated:YES completion:nil];
    }
    else
    {
        UINavigationController * nController = [self isKindOfClass:NSClassFromString(@"UINavigationController")] ? (UINavigationController*)self : [self navigationController];
        if (nController == nil) {
            nController = (UINavigationController*)[(RootVC *)[[[[UIApplication sharedApplication] delegate] window] rootViewController] centerPanel];
            [(JASidePanelController*)[self parentViewController] toggleRightPanel:nil];
        }
        [nController pushViewController:[[UIStoryboard storyboardWithName:@"AccountStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"EnregistrementVC"] animated:YES];
    }
}

-(void) popUpLogin{
    [SCAppDelegate loginEnCours:true];
    UITextField * pseudoField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 32)];
    pseudoField.placeholder = [LanguageManager get:@"label_pseudo"];
//    [pseudoField setBorderStyle:UITextBorderStyleRoundedRect];
//    [pseudoField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    [pseudoField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    if([[UIViewController getPreference:@"rememberMe"] boolValue])
    {
        pseudoField.text = [UIViewController getPreference:@"username"];
    }
    
    UITextField * passField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 32)];
    passField.placeholder = [LanguageManager get:@"label_mot_de_passe"];
    passField.secureTextEntry = YES;
//    [passField setBorderStyle:UITextBorderStyleRoundedRect];
//    [passField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_connexion"] andTextFields:@[pseudoField,passField]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Annulation du log");
                              [SCAppDelegate loginEnCours:false];
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_connexion"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Login");
                              [self resignFirstResponder];
                              
                              NSString * password = passField.text;
                              NSData * password_data = [password dataUsingEncoding:NSUTF8StringEncoding];
                              password = [password_data base64EncodedStringWithOptions:0];

                              [self logUser:pseudoField.text withPassword:password];
                              
                          }];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"button_creer_un_compte"]
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Création de compte");
                              [self pushRegister];
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];

}

-(void) popUpWrongLogin{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_erreur_identifiants"] andMessage:[LanguageManager get:@"ios_label_veuillez_reesayer"]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Annulation");
                              [self updateMessages];
                              
                              [SCAppDelegate loginEnCours:false];
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"ios_button_reesayer"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Réessaie");
                              [self popUpLogin];
                          }];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
    
    [alertView show];
}

#pragma mark - Méthodes de log

-(BOOL) logUser:(NSString *)username withPassword:(NSString*)password{
    
    [self setNoLogin:false];
    
    //Récuperation des ressources possédées pour mettre à jour les niveaux si ya besoin
    
    NSMutableArray * capsules_utilisateur_mobile  = [[NSMutableArray alloc] init];
    NSArray * all_ressources = [UtilsCore getAllRessources];
    
    for (Ressource * res in all_ressources) {
        __block NSMutableArray * array_id_capsule = [[NSMutableArray alloc] initWithCapacity:[res.capsules count]];
        
        [res.capsules enumerateObjectsUsingBlock:^(Capsule * capsule, BOOL *stop) {
            if (capsule.date_downloaded) {
                [array_id_capsule addObject:capsule.id_capsule];
            }
        }];
        if ([array_id_capsule count]) {
            [capsules_utilisateur_mobile addObject:@{@"id_ressource" :res.id_ressource, @"capsules" : array_id_capsule}];
        }
        
    }
    
    //Transformation en JSON
    NSError * error = [[NSError alloc] init];
    
    NSData* data_json_temp = [NSJSONSerialization dataWithJSONObject:capsules_utilisateur_mobile
                                                          options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString * capsules_utilisateur_mobile_json = [[NSString alloc] initWithData:data_json_temp encoding:NSUTF8StringEncoding];
    
    //Token pour les notifications
    NSString * token_mobile = [[NSUserDefaults standardUserDefaults] stringForKey:@"token_mobile"];
   
    //Construction de la requête
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:username,@"username", password, @"password", capsules_utilisateur_mobile_json ,@"ressources_utilisateur_mobile", token_mobile, @"token_mobile",nil];
    
    return [WService post:@"utilisateur/login"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          
          [SCAppDelegate loginEnCours:false];
          //En cas d'échec
          if([[wsReturn objectForKey:@"status"] isEqualToString:@"ko"])
          {
              [self popUpWrongLogin];
          }
          //En cas de réussite
          else
          {
              
              [self updateMessages];
              //Inscription des infos de l'user dans les préférences permanentes de l'app
              NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
              
              //On supprime les infos permanentes si yen a déjà
              if([preferences persistentDomainForName:kLogin] || [UIViewController getAllSessionValue])
                  [self clearLogInfos];
              
              [preferences setPersistentDomain:query forName:kLogin];
              [UIViewController setLieAuServeur:true];
              [UIViewController setSessionValue:[wsReturn copy]];

              // On renseigne crashlytics sur l'utilisateur actuel, pour une remontée plus poussée des logs d'erreurs
              [CrashlyticsKit setUserIdentifier:wsReturn[@"id_utilisateur"]];
              [CrashlyticsKit setUserEmail:wsReturn[@"email"]];
              [CrashlyticsKit setUserName:wsReturn[@"username"]];
              
              //Sauvegarde de soi-même (minimum syndical)
              NSNumber * id_utilisateur = [wsReturn[@"id_utilisateur"] isKindOfClass:[NSNumber class]] ? wsReturn[@"id_utilisateur"] : [NSNumber numberWithInt:wsReturn[@"id_utilisateur"]];
              [UtilsCore sauvegardeUtilisateur:id_utilisateur name:username url:wsReturn[@"avatar_url"]];
              
              
              
              [self.view hideToastActivity];
              [self.view makeToast:[LanguageManager get:@"label_connexion_reussi"] duration:3.0 position:@"bottom" title:nil image:nil style:nil completion:nil];
              
              [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:self userInfo:@{@"status": @"connexion", @"username" : username}];
              [UtilsCore getNotificationsFromServer];
              
              // int => Flag :
              //      -1 un des messages demande un id non existant ou qui aurait du etre envoyé au serveur ou que le serveur n'a pas reussie à ajouter
              //          == l'id voulue appartient à un autre message qu'on n'a pas pu synchroniser
              //      -2 le server renvoie une erreur
              //      -3 On as fini de parcourir tous le tableau
              //      -4 Aucun message dans la file. Aucune action de réalisée
             [UtilsCore synchroniserMessagesClientVersServeurWithCallBack:^(bool reussite, int flag) {
                  DLog(@"----------------------------------");
                  DLog(@"----------------------------------");
                  DLog(@"[Au login synchroniserMessagesClientVersServeurWithCallBack] reussite:%d  flag:%d", reussite, flag);
                  DLog(@"----------------------------------");
                  DLog(@"----------------------------------");
              }];
          }
      }
  errorMessage:[LanguageManager get:@"label_erreur_reseau_indisponible"]
activityMessage:nil];
}

+(id) getPermamentValue:(NSString*)key
{
    //Récup des infos permamentes (username et pass)
    return [[[NSUserDefaults standardUserDefaults] persistentDomainForName:kLogin] objectForKey:key];
}
+(id) getSessionValueForKey:(NSString*)key
{
    //Récup des infos de session (tout le reste, envoyé du serveur)
    return [[[NSUserDefaults standardUserDefaults] persistentDomainForName:kLoginUserInfo] objectForKey:key];
}

+(id) getAllSessionValue
{
    return [[NSUserDefaults standardUserDefaults] persistentDomainForName:kLoginUserInfo];
}

+(void) setSessionValue:(NSMutableDictionary*)dict
{
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    [preferences setPersistentDomain:dict forName:kLoginUserInfo];
}

+(NSDictionary*) getRankForRessource:(NSNumber*)id_ressource{
    NSDictionary * rank = [UIViewController getSessionValueForKey:@"rank"];
    __block NSDictionary * rank_of_capsule;
    
    // On cherche une capsule qui appartient à la ressource demandée
    [rank enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([obj[@"id_ressource"] isEqual:id_ressource]) {
            rank_of_capsule = obj;
            stop = YES;
        }
    }];
    
    // On retourne le rang de la ressource
    if (rank_of_capsule && [rank_of_capsule count]) {
        return rank_of_capsule;
    }
    //Sinon, on ne peut pas enregistrer de rang par défaut parce qu'on a pas de capsule
    //Donc on renvoie un dictionnaire par défaut, sans l'enregistrer
    else{
        return @{
                 @"id_ressource" : id_ressource,
                 @"id_categorie" : @0,
                 @"nom_categorie": @"Lecteur",
                 @"score": @0
                 };
    }
}
+(NSDictionary*) getRankForCapsule:(NSNumber*)id_capsule{
    //On recupère le rang de la capsule demandée
    NSDictionary * rank = [UIViewController getSessionValueForKey:@"rank"];
    //S'il y a bien un rang
    if ([[rank allKeys] indexOfObject:[id_capsule stringValue]] != NSNotFound) {
        return rank[[id_capsule stringValue]];
    }
    //Sinon, on créé un rang par défaut
    else{
        //Si ca marche
        if([self addDefaultRankToUserDataForCapsule:id_capsule])
            return [self getRankForCapsule:id_capsule];
        //Sinon
        return @{
                    @"id_categorie" : @0,
                    @"nom_categorie": @"Lecteur",
                    @"score": @0
                  };

    }
}
+(BOOL)addDefaultRankToUserDataForCapsule:(NSNumber*)id_capsule{
    Capsule * capsule = [UtilsCore getCapsule:id_capsule];
    if(capsule)
        return [self addRawRankToUserData:@{
                                        [id_capsule stringValue] : @{
                                                @"id_ressource" : capsule.id_ressource,
                                                @"id_categorie" : @0,
                                                @"nom_categorie": @"Lecteur",
                                                @"score": @0
                                        }
                              }];
    else
        return false;
}

+(BOOL)addRawRankToUserData:(NSDictionary*)rank_data{
    NSString * id_capsule = [rank_data allKeys][0];
    return [self addRankToUserData:rank_data[id_capsule] forID:id_capsule];
}

+(BOOL)addRankToUserData:(NSDictionary*)rank_data forID:(NSString*)capsule_id{
    if ([UIViewController isConnected]) {
        //Récup des infos de session (tout le reste, envoyé du serveur)
        //Copies mutables de tous ces dictionnaires
        NSMutableDictionary * volatile_session = [[UIViewController getAllSessionValue] mutableCopy];
        //Si pas de rang, car pas connecté, on met quand même un rang pour des questions de cohérence
        NSMutableDictionary * rank = volatile_session[@"rank"] ? [volatile_session[@"rank"] mutableCopy] : [[NSMutableDictionary alloc] init];
        [rank setObject:rank_data forKey:capsule_id];
        [volatile_session setObject:rank forKey:@"rank"];
        
        //On remet les informations de session modifiées dans le domaine volatile
        [UIViewController setSessionValue:volatile_session];
        
        return YES;
    }
    else
        return NO;
}

+(NSNumber*)addScore:(int)score_to_add toRankOfCapsule:(NSNumber*)id_capsule
{
    if ([UIViewController isConnected]) {
        //Récup des infos de session (tout le reste, envoyé du serveur)
        //Copies mutables de tous ces dictionnaires
        NSMutableDictionary * volatile_session = [[UIViewController getAllSessionValue] mutableCopy];
        NSMutableDictionary * rank = [volatile_session[@"rank"] mutableCopy];
        NSMutableDictionary * ressource_rank = [rank[[id_capsule stringValue]] mutableCopy];
        
        int old_points = [[ressource_rank objectForKey:@"score"] intValue];
        NSNumber *  new_points = [NSNumber numberWithInt:(old_points + score_to_add)];
 
        //On rengistre progressivement toutes les modifications
//        [ressource_rank setObject:new_points forKey:@"score"];
        
        //On chope la ressource correspondant à cette capsule
        NSString * id_ressource = ressource_rank[@"id_ressource"];
        
        NSMutableDictionary * rank_copy = [rank copy];
        //On affecte toutes les capsules appartenant à la même ressource
        for (NSString* other_id_capsule in rank_copy) {
            NSMutableDictionary * other_ressource_rank = [rank_copy[other_id_capsule] mutableCopy];
            //Si c'est de la même ressource
            if([other_ressource_rank[@"id_ressource"] isEqual:id_ressource]){
                //On remplace également la dedans
                [other_ressource_rank setObject:[new_points stringValue] forKey:@"score"];
                [rank setObject:other_ressource_rank forKey:other_id_capsule];
            }
        }
        
        [volatile_session setObject:rank forKey:@"rank"];
        
        //On remet les informations de session modifiées dans le domaine volatile
        [UIViewController setSessionValue:volatile_session];
        
        return new_points;
    }
    else
        return 0;
    
}

-(void) logOut{

    [WService post:@"utilisateur/logout"
    callback: ^(NSDictionary *wsReturn) {
        [UIViewController setLieAuServeur:NO];
        
        [self.view makeToast:[LanguageManager get:@"label_deconnexion_reussi"] duration:3.0 position:@"bottom" title:nil image:nil style:nil completion:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:self userInfo:@{@"status":@"deconnexion"}];
        
        //On clear toutes les infos retournées par le WS
        [self clearLogInfos];
    }];
}

-(void) clearLogInfos{
    //Récup des préfs
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    
    //On clear toutes les préférences
    [preferences removePersistentDomainForName:kLogin];
    [preferences removePersistentDomainForName:kLoginUserInfo];
    [preferences removeVolatileDomainForName:kLoginUserInfo];
}

+(void) setDefaultsPreferences{
    // Recuperation de la langue de l'application
    NSString *currentCodeLanguageApp = [LanguageManager getCurrentCodeLanguageApp];
    
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    
    NSDictionary * parametres = @{@"rememberMe" : [NSNumber numberWithBool:YES],
                                  @"usernameGPlus" : @"",
                                  @"passwordGPlus" : @"",
                                  @"publishTwitter" : [NSNumber numberWithBool:NO],
                                  @"geolocalisation" : [NSNumber numberWithBool:NO],
                                  @"automaticUpdate" : [NSNumber numberWithBool:YES],
                                  @"updateFrequency" : [NSNumber numberWithFloat:kMessageUpdateEveryMinute],
                                  @"tutorialMode" : [NSNumber numberWithBool:YES],
                                  @"idLangueApp" : [NSNumber numberWithInt:[LanguageManager idLangueWithCode:currentCodeLanguageApp]]};
    
    [preferences setPersistentDomain:parametres forName:kPreferences];
}

+(id) getPreference:(NSString*)key{
    
    return [[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:key];
}

-(void)updateMessages{
    // Rafraichissement des messages
    DLog(@"%d",[[UIViewController getPreference:@"updateFrequency"] intValue ]);
    if ( [[UIViewController getPreference:@"updateFrequency"] intValue ] != 0){
        [UtilsCore rafraichirMessages];
    }
    if ( [[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"]intValue ] == kMessageUpdateEveryMinute){
        [UtilsCore startPullingTimer];
    }
}

@end
