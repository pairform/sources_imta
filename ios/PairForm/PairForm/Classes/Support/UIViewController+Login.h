//
//  UIViewController+Login.h
//  SupCast
//
//  Created by Maen Juganaikloo on 29/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilsCore.h"

@interface UIViewController (Login)

-(void) setNoLogin:(BOOL)status;
-(BOOL) userDontWantLogin;
-(void) popUpFirstLogin;
-(void) pushRegister;
-(void) popUpLogin;
-(void) popUpWrongLogin;

-(BOOL) checkForDefaultUser;
+(BOOL) isConnected;

+(BOOL) estLieAuServeur;
+(void) setLieAuServeur:(BOOL)etatLiaison;

/** Connecte l'utilisateur local sans prompt
 @return flag de reussite (bool);
 */
-(BOOL) connecterUtilisateurLocalEnSilence;

/** Vérifie la connexion en local de l'utilisateur. 
    Peut connecter l'utilisateur local sans prompt
 @return flag de reussite (bool);
 */
-(BOOL) checkForLoggedInUser;

-(BOOL) logUser:(NSString *)username withPassword:(NSString*)password;

/** Retourne les variables de session de l'user loggé. Renvoie nil en cas d'echec
 @return password (string);
 @return username (string);
*/
+(id) getPermamentValue:(NSString*)key;

/** Retourne les variables de session de l'user loggé. Renvoie nil en cas d'echec
 @return "avatar_url"
 @return  elggperm
 @return  email
 @return  estExpert (bool)
 @return  etablissement
 @return  id (int)
 @return  name
 @return  rank =     {
 @return    id_res (int) =         {
 @return    "afficher_noms_reels" (int ± bool)
 @return    "id_categorie" (int)
 @return    "id_ressource" (int)
 @return    "nom_categorie"
 @return    "notification_mail" (int ± bool)
 @return    score (int)
 @return    }
 @return  };
 @return  username
 @return  }
 */
+(id) getSessionValueForKey:(NSString*)key;

+(id) getAllSessionValue;
+(void) setSessionValue:(NSMutableDictionary*)dict;

/** Recupère l'objet de rang correspondant à une ressource, et renvoie un rang par défaut s'il n'y en a pas (pas de création)
  @param id_ressource (NSNumber*) id de la ressource
 */
+(NSDictionary*) getRankForRessource:(NSNumber*)id_ressource;

/** Recupère l'objet de rang correspondant à une capsule, et créé par défaut un rang s'il n'y en a pas encore.
 Normalement, le serveur aura créé ce rôle en parallèle dans tous les cas.
 @param id_capsule (NSNumber*) id de la capsule
 */
+(NSDictionary*) getRankForCapsule:(NSNumber*)id_capsule;

/** Ajoute des points à l'utilisateur courant, sur une ressource donnée
 @param score_to_add (UInt) positif
 @param id_ressource (NSNumber*) id de la ressource à modifier
 @return Score total
 */
+(NSNumber*)addScore:(int)score_to_add toRankOfCapsule:(NSNumber*)id_capsule;

/** Ajoute les informations relatives au rang / score de l'utilisateur courant, sur une ressource donnée
 @param rank_data Réponse en vrac du webservice
 @return Flag de réussite  false si non connecté
 */
+(BOOL)addRawRankToUserData:(NSDictionary*)rank_data;

/** Ajoute les informations relatives au rang / score de l'utilisateur courant, sur une ressource donnée
 @param rank_data  Données concernant la ressource
 @param id_ressource  ID de la capsule à ajouter
 @return Flag de réussite  false si non connecté
 */
+(BOOL)addRankToUserData:(NSDictionary*)rank_data forID:(NSString*)capsule_id;
-(void) logOut;
-(void) clearLogInfos;
+(void) setDefaultsPreferences;
+(id) getPreference:(NSString*)key;
@end
