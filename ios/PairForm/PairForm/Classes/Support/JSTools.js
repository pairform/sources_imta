
//var selecteurs = [".mainContent_co p", ".mainContent_co .resInFlow", ".mainContent_ti"];

function getElementAtCoordinates(x,y, selecteurs)
{
    selecteurs = selecteurs.split(",");
    
    var initial_element = document.elementFromPoint(x,y);
    var element = initial_element;
    
    removeSquaringFromEveryElement(selecteurs);
    
    //Recherche d'un élément matchant les sélecteurs passés
    while(element.nodeName !== "BODY" && !matchedSelector(element, selecteurs)){
        element = element.parentNode;
    }
    
    //Pas trouvé d'élément compatible
    if (element.nodeName === "BODY")
        return element.nodeName;
    
    var matched_selector = matchedSelector(element, selecteurs);
    
    applySquaringOnElement(element);
    
    var elements_with_same_selector = document.querySelectorAll(matched_selector);
    for (var index = 0; index < elements_with_same_selector.length; index++) {
        if (element === elements_with_same_selector[index]) {
            return matched_selector + ";" + index;
            break;
        }
    }
    
    return element.nodeName;
    
}

function matchedSelector (element, selecteurs) {
    for (var i = 0; i < selecteurs.length; i++) {
        if (element.matches(selecteurs[i]))
            return selecteurs[i];
    }
    return null;
}
function applySquaringOnElement(e)
{
    e.className += ' selected';
    
}



function removeSquaringFromEveryElement(selecteurs){
    
    for (var i = 0; i < selecteurs.length; i++) {
        var elements_with_same_selector = document.querySelectorAll(selecteurs[i]);
        for (var index = 0; index < elements_with_same_selector.length; index++) {
            if ( hasClass(elements_with_same_selector[index],'selected')){
                removeClass(elements_with_same_selector[index],'selected');
            }
        }
    }
}

Element.prototype.documentOffsetTop = function () {
    return this.offsetTop + ( this.offsetParent ? this.offsetParent.documentOffsetTop() : 0 );
};

function scrollToElem(tag,num_occurence){
    var elements = document.querySelectorAll(tag);
    
    //elements[num_occurence].scrollIntoView(true);
    var top = elements[num_occurence].documentOffsetTop() - (window.innerHeight / 2 );
    window.scrollTo( 0, top );
    
    applySquaringOnElement( elements[num_occurence]);
    return "";
    
}

function putBadge(tag,num_occurence,count, est_lu){
    
    var elements = document.querySelectorAll(tag);
    
    if ( !hasClass(elements[num_occurence],'hasComment'))
        elements[num_occurence].className +=  ' hasComment';
    
    elements[num_occurence].setAttribute('data-count', count);
    if ( est_lu )
        elements[num_occurence].className +=  ' readed';
    else{
        if ( hasClass(elements[num_occurence],'readed')){
            removeClass(elements[num_occurence],'readed');
        }
    };
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}
function removeClass(ele,cls) {
    if (hasClass(ele,cls)) {
        var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
        ele.className=ele.className.replace(reg,' ');
    }
}