//
//  NSDictionary+WService.h
//  SupCast
//
//  Created by Maen Juganaikloo on 23/04/13.
//  Copyright (c) 2013 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WService : NSObject

+(BOOL)post:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)post:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)post:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)post:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)post:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)post:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)get:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)get:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)get:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)get:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)get:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)get:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)put:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)put:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)put:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)put:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)put:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)put:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)delete:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)delete:(NSString *)url callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)delete:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)delete:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;
+(BOOL)delete:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback;
+(BOOL)delete:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary * wsReturn))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;

+(BOOL)url:(NSString *)url method:(NSString*)httpMethod mixedParam:(id)mixedParam callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage;

+(NSString*) urlEncodedString:(NSDictionary*) dico;
+(BOOL)displayErrors:(NSDictionary *)wsReturn;
+(BOOL) isReachable;
@end
