//
//  LanguageManager.m
//  PairForm
//
//  Created by VESSEREAU on 05/05/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "LanguageManager.h"

@implementation LanguageManager

static NSBundle *bundle = nil;

// Fonctions gérant la récupération des termes externalisés

+ (void) initialize {
    [self setLanguage:[self getCurrentCodeLanguageApp]];
}

/*
 example calls:
 [Language setLanguage:@"it"];
 [Language setLanguage:@"de"];
 */
+ (void) setLanguage:(NSString *)language {
    NSString *path = [[ NSBundle mainBundle ] pathForResource:language ofType:@"lproj" ];
    bundle = [NSBundle bundleWithPath:path];
    
}

+ (NSString *) get:(NSString *)key {
    return [bundle localizedStringForKey:key value:key table:nil];
}



// Fonctions de conversion des formats de langue

+ (NSString *) codeLangueWithid:(int) idL {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesCode = [dictionary objectForKey:@"langue_values_code"];
    
    int arrayIndex = 0;
    
    if (idL > 0)
        arrayIndex = [self arrayIndexWithId: idL];
    
    return [langueValuesCode objectAtIndex:arrayIndex];
}

+ (NSString *) codeLangueWithName:(NSString *) name {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesCode = [dictionary objectForKey:@"langue_values_code"];
    
    int arrayIndex = [self arrayIndexWithName: name];
    
    return [langueValuesCode objectAtIndex:arrayIndex];
}

+ (NSString *) nameLangueWithCode:(NSString *) code {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[bundle pathForResource:@"Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesName = [dictionary objectForKey:@"langue_values_string"];
    
    int arrayIndex = [self arrayIndexWithCode: code];
    
    return [langueValuesName objectAtIndex:arrayIndex];
}

+ (NSString *) nameTrueLangueWithCode:(NSString *) code {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesName = [dictionary objectForKey:@"langue_values_string"];
    
    int arrayIndex = [self arrayIndexWithCode: code];
    
    return [langueValuesName objectAtIndex:arrayIndex];
}

+ (NSString *) nameLangueWithId:(int) idL {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[bundle pathForResource:@"Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesName = [dictionary objectForKey:@"langue_values_string"];
    
    int arrayIndex = 0;
    
    if (idL > 0)
        arrayIndex = [self arrayIndexWithId: idL];
    
    return [langueValuesName objectAtIndex:arrayIndex];
}

+ (NSString *) nameTrueLangueWithId:(int) idL {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesName = [dictionary objectForKey:@"langue_values_string"];
    
    int arrayIndex = 0;
    
    if (idL > 0)
        arrayIndex = [self arrayIndexWithId: idL];
    
    return [langueValuesName objectAtIndex:arrayIndex];
}

+ (int) idLangueWithCode:(NSString *) code {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesId = [dictionary objectForKey:@"langue_values_id"];
    
    int arrayIndex = [self arrayIndexWithCode: code];
    
    return [[langueValuesId objectAtIndex:arrayIndex] integerValue];
}

+ (int) idLangueWithName:(NSString *) name {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesId = [dictionary objectForKey:@"langue_values_id"];
    
    int arrayIndex = [self arrayIndexWithName: name];
    
    return [[langueValuesId objectAtIndex:arrayIndex] integerValue];
}




+ (int) arrayIndexWithCode:(NSString *) code {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesCode = [dictionary objectForKey:@"langue_values_code"];
    
    return [langueValuesCode indexOfObject:code];
}

+ (int) arrayIndexWithName:(NSString *) name {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[bundle pathForResource:@"Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesName = [dictionary objectForKey:@"langue_values_string"];
    
    return [langueValuesName indexOfObject:name];
}

+ (int) arrayIndexWithId:(int) idL {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesId = [dictionary objectForKey:@"langue_values_id"];
    
    NSString * idLangue = [[NSNumber numberWithInt:idL] stringValue];
    
    return [langueValuesId indexOfObject:idLangue];
}




+ (NSString *) codeLangueWithArrayIndex:(int) index {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesCode = [dictionary objectForKey:@"langue_values_code"];
    
    return [langueValuesCode objectAtIndex:index];
}

+ (NSString *) nameLangueWithArrayIndex:(int) index {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[bundle pathForResource:@"Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesName = [dictionary objectForKey:@"langue_values_string"];
    
    return [langueValuesName objectAtIndex:index];
}

+ (NSString *) nameTrueLangueWithArrayIndex:(int) index {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesName = [dictionary objectForKey:@"langue_values_string"];
    
    return [langueValuesName objectAtIndex:index];
}

+ (int) idLangueWithArrayIndex:(int) index {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    NSArray * langueValuesId = [dictionary objectForKey:@"langue_values_id"];
    
    return [[langueValuesId objectAtIndex:index] integerValue];
}



// Fonctions gérant la récupération des listes de langue

+ (NSArray *) getArrayIdLanguage {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    return [dictionary objectForKey:@"langue_values_id"];
}

+ (NSArray *) getArrayCodeLanguage {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    return [dictionary objectForKey:@"langue_values_code"];
}

+ (NSArray *) getArrayNameLanguage {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[bundle pathForResource:@"Arrays" ofType:@"plist"]];
    
    return [dictionary objectForKey:@"langue_values_string"];
}

+ (NSArray *) getArrayTrueNameLanguage {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PairForm-Arrays" ofType:@"plist"]];
    
    return [dictionary objectForKey:@"langue_values_string"];
}



// Fonctions gérant la langue de l'application

+ (NSString *) getCurrentCodeLanguageApp {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString* languageID = [languages firstObject];

    
    if([[self getArrayCodeLanguage] containsObject:languageID]) {
        return languageID;
    } else {
        NSString* localeID = [[NSLocale currentLocale] localeIdentifier];
        NSDictionary* components = [NSLocale componentsFromLocaleIdentifier:localeID];
        languageID = components[NSLocaleLanguageCode];
        
        if([[self getArrayCodeLanguage] containsObject:languageID]) {
            [self setCurrentLanguageAppWithCode:languageID];
            return languageID;
        } else {
            [self setCurrentLanguageAppWithCode:@"en"];
        }
    }
    
    return @"en";
}

+ (void) setCurrentLanguageAppWithCode: (NSString *) code {
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:code, nil]
                                              forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self setLanguage:code];
}



// Fonctions gérant les langues de l'utilisateur

+ (NSString *) getCodeMainLanguage {
    NSString * codeLanguePrincipale = [UIViewController getSessionValueForKey:@"langue_principale"];
    if (codeLanguePrincipale != nil) { //Connecté
        return codeLanguePrincipale;
    } else { // Non connecté
        return [self getCurrentCodeLanguageApp];
    }
}
+ (void) setMainLanguageWitCode: (NSString *) code {
    NSDictionary * dicoLang = [UIViewController getAllSessionValue];
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    for (NSString* key in dicoLang) {
        if(![key isEqualToString:@"langue_principale"]) {
            id value = [dicoLang objectForKey:key];
            [newDict setObject:value forKey:key];
        }
    }
    [newDict setObject:code forKey:@"langue_principale"];
    [UIViewController setSessionValue:newDict];
}

+ (BOOL) getPrefOtherLanguagesWithCodeLangue:(NSString *)code {
    NSArray * autresLangues = [UIViewController getSessionValueForKey:@"autres_langues"];
    
    for ( id autreLangue in autresLangues ) {
        if([[autreLangue objectForKey:@"code_langue"] isEqualToString:code])
            return true;
    }
    
    return false;
}

+ (void) addOtherLanguageWithCodeLangue:(NSString *) code {
    NSMutableArray * autresLangues = [[NSMutableArray alloc] initWithArray:[UIViewController getSessionValueForKey:@"autres_langues"]];
    NSDictionary * codeLangue = [[NSMutableDictionary alloc] initWithObjectsAndKeys:code, @"code_langue", nil];
    [autresLangues addObject:codeLangue];
    
    NSMutableDictionary * dicoPreferences = [[NSMutableDictionary alloc] initWithDictionary:[UIViewController getAllSessionValue]];
    [dicoPreferences setObject:autresLangues forKey:@"autres_langues"];
    [UIViewController setSessionValue:dicoPreferences];
}

+ (void) removeOtherLanguages {
    NSMutableDictionary * dicoPreferences = [[NSMutableDictionary alloc] initWithDictionary:[UIViewController getAllSessionValue]];
    [dicoPreferences setObject:[[NSArray alloc] init] forKey:@"autres_langues"];
    [UIViewController setSessionValue:dicoPreferences];
}



// Fonctions de récupération des listes de langue pour les requêtes en BDD

+ (NSString *) displayLanguagesStringForQuery {
    NSMutableString *chaineDisplayLangue = [NSMutableString stringWithString:@""];
    NSArray *tabCodeLangue = [self getArrayCodeLanguage];
    NSArray *tabIdLangue = [self getArrayIdLanguage];
    
    if ([UIViewController isConnected]) {
        [chaineDisplayLangue appendString:[NSString stringWithFormat:@"%d", [self idLangueWithCode:[self getCodeMainLanguage]]]];
        for ( id codeLangue in tabCodeLangue )
        {
            if([self getPrefOtherLanguagesWithCodeLangue:codeLangue]) {
                [chaineDisplayLangue appendString:[NSString stringWithFormat:@",%d", [self idLangueWithCode:codeLangue]]];
            }
        }
    } else {
        [chaineDisplayLangue appendString:[NSString stringWithFormat:@"%d", [[tabIdLangue objectAtIndex:0] integerValue]]];
        int i;
        for(i=1; i<[tabIdLangue count]; i++) {
            [chaineDisplayLangue appendString:[NSString stringWithFormat:@",%d", [[tabIdLangue objectAtIndex:i] integerValue]]];
        }
    }
    
    return chaineDisplayLangue;
}

+ (NSArray *) displayLanguagesArrayForQuery {
    // Recuperation des langues des messages à afficher
    NSArray *tabCodeLangue = [self getArrayCodeLanguage];
    NSArray *tabIdLangue = [self getArrayIdLanguage];
    NSMutableArray *tabIdLangueMessage = [[NSMutableArray alloc] init];
    
    if([UIViewController isConnected]) {
        [tabIdLangueMessage addObject:[NSNumber numberWithInt:[self idLangueWithCode:[self getCodeMainLanguage]]]];
        for ( id codeLangue in tabCodeLangue )
        {
            if([self getPrefOtherLanguagesWithCodeLangue:codeLangue]) {
                [tabIdLangueMessage addObject:[NSNumber numberWithInt:[self idLangueWithCode:codeLangue]]];
            }
        }
    } else {
        int i;
        for(i=0; i<[tabIdLangue count]; i++) {
            [tabIdLangueMessage addObject:[NSNumber numberWithInt:[[tabIdLangue objectAtIndex:i] integerValue]]];
        }
    }
    
    return tabIdLangueMessage;
}


// Autres

+ (BOOL) isConnected {
    //Si il y a un utilisateur connecté sur l'appareil, ya les données du webService
    if([UIViewController getAllSessionValue])
        return true;
    else
        return false;
}

@end
