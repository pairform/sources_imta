//
//  LanguageManager.h
//  PairForm
//
//  Created by VESSEREAU on 05/05/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageManager : NSObject

// Fonctions gérant la récupération des termes externalisés
+ (void) initialize;
+ (void) setLanguage:(NSString *)language;
+ (NSString *) get:(NSString *)key;

// Fonctions de conversion des formats de langue
+ (NSString *) codeLangueWithid:(int) idL;
+ (NSString *) codeLangueWithName:(NSString *) name;
+ (NSString *) nameLangueWithCode:(NSString *) code;
+ (NSString *) nameTrueLangueWithCode:(NSString *) code;
+ (NSString *) nameLangueWithId:(int) idL;
+ (NSString *) nameTrueLangueWithId:(int) idL;
+ (int) idLangueWithCode:(NSString *) code;
+ (int) idLangueWithName:(NSString *) name;

+ (int) arrayIndexWithCode:(NSString *) code;
+ (int) arrayIndexWithName:(NSString *) name;
+ (int) arrayIndexWithId:(int) idL;

+ (NSString *) codeLangueWithArrayIndex:(int) index;
+ (NSString *) nameLangueWithArrayIndex:(int) index;
+ (NSString *) nameTrueLangueWithArrayIndex:(int) index;
+ (int) idLangueWithArrayIndex:(int) index;

// Fonctions gérant la récupération des listes de langue
+ (NSArray *) getArrayIdLanguage;
+ (NSArray *) getArrayCodeLanguage;
+ (NSArray *) getArrayNameLanguage;
+ (NSArray *) getArrayTrueNameLanguage;

// Fonctions gérant la langue de l'application
+ (NSString *) getCurrentCodeLanguageApp;
+ (void) setCurrentLanguageAppWithCode: (NSString *) code;

// Fonctions gérant les langues de l'utilisateur
+ (NSString *) getCodeMainLanguage;
+ (void) setMainLanguageWitCode: (NSString *) code;
+ (BOOL) getPrefOtherLanguagesWithCodeLangue:(NSString *)code;
+ (void) addOtherLanguageWithCodeLangue:(NSString *) code;
+ (void) removeOtherLanguages;

// Fonctions de récupération des listes de langue pour les requêtes en BDD
+ (NSString *) displayLanguagesStringForQuery;
+ (NSArray *) displayLanguagesArrayForQuery;

// Autres
+ (BOOL) isConnected;

@end