//
//  NSDictionary+WService.m
//  SupCast
//
//  Created by Maen Juganaikloo on 23/04/13.
//  Copyright (c) 2013 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "WService.h"
#import "Reachability.h"
#import "SIAlertView.h"
#import "RootVC.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "LanguageManager.h"
#import "PJ.h"
#import "ReachabilityManager.h"

// helper function: get the string form of any object
static NSString *toString(id object) {
    return [NSString stringWithFormat: @"%@", object];
}

// helper function: get the url encoded string form of any object
static NSString *urlEncode(id object) {
    NSString *string = toString(object);
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}



static NSOperationQueue *_sharedNetworkQueue;

@implementation WService

// Network Queue is a shared singleton object.
// no matter how many instances of MKNetworkEngine is created, there is one and only one network queue
// In theory an app should contain as many network engines as the number of domains it talks to

#pragma mark -
#pragma mark Initialization

+(void) initialize {
    
    if(!_sharedNetworkQueue) {
        static dispatch_once_t oncePredicate;
        dispatch_once(&oncePredicate, ^{
            _sharedNetworkQueue = [[NSOperationQueue alloc] init];
            //            [_sharedNetworkQueue addObserver:[self self] forKeyPath:@"operationCount" options:0 context:NULL];
            [_sharedNetworkQueue setMaxConcurrentOperationCount:-1];
            
        });
    }
}

#pragma mark - Methodes à part


+(NSString*) urlEncodedString:(NSDictionary*) dico {
    NSMutableArray *parts = [NSMutableArray array];
    
    if([dico count] == 0)
        return @"";
    
    for (id key in dico)
    {
        id value = [dico objectForKey: key];

        if ([value isKindOfClass:[NSDictionary class]]){
            for (NSString *subKey in value)
                [parts addObject:[NSString stringWithFormat:@"%@[%@]=%@", urlEncode(key), urlEncode(subKey), urlEncode([value objectForKey:subKey])]];
        }
        else if ([value isKindOfClass:[NSArray class]]){
            for (NSString *subValue in value)
                [parts addObject:[NSString stringWithFormat:@"%@[]=%@", urlEncode(key), urlEncode(subValue)]];
        }
        else{
            [parts addObject: [NSString stringWithFormat: @"%@=%@", urlEncode(key), urlEncode(value)]];
        }
    }
    return [parts componentsJoinedByString: @"&"];
}



+(NSData *)multipartStringAvecParametres:(NSDictionary *)parametres //Paramètres
{
    NSMutableData *postbody = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    // add params
    [parametres enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, id parameterValue, BOOL *stop) {
        
        if([parameterValue isKindOfClass:NSClassFromString(@"NSData")]){
            NSString *mimetype  = [PJ mimeTypePourExtension:[parameterKey pathExtension]];
            
            [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", parameterKey, parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:parameterValue];
            [postbody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

        }else{
            
            if ([parameterValue isKindOfClass:[NSDictionary class]]){
                for (NSString *subKey in parameterValue){
                    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@[%@]\"\r\n\r\n", parameterKey, subKey] dataUsingEncoding:NSUTF8StringEncoding]];
                    [postbody appendData:[[NSString stringWithFormat:@"%@\r\n", [parameterValue objectForKey:subKey]] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                }
            }
            else if ([parameterValue isKindOfClass:[NSArray class]]){
                for (NSString *subValue in parameterValue){
                    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@[]\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
                    [postbody appendData:[[NSString stringWithFormat:@"%@\r\n", subValue] dataUsingEncoding:NSUTF8StringEncoding]];
                }
            }
            else{
                
                [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
                [postbody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
    }];
    
    [postbody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return postbody;
}

SIAlertView *alertView;

+(BOOL)isObsoleteVersion:(NSDictionary *)wsReturn{
    if ( [[wsReturn objectForKey:@"status"] isEqualToString:@"obsolete"]){
        NSString * message = [wsReturn objectForKey:@"message"];
        NSString * errorMessage;
        
        if (!message)
            errorMessage = @"Votre version d'application est obsolète : veuillez mettre à jour votre application pour continuer à utiliser PairForm !";
        else {
            errorMessage = [LanguageManager get:message];

            if(!errorMessage)
                errorMessage = message;
        }
        
        if (!alertView) {
            alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_label_probleme"]
                                                andMessage:errorMessage];
            
            
            [alertView addButtonWithTitle:@"Mode hors-ligne"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alertView) {
                                      [alertView dismissAnimated:YES];
                                  }];
            [alertView addButtonWithTitle:@"Mettre à jour"
                                     type:SIAlertViewButtonTypeDestructive
                                  handler:^(SIAlertView *alertView) {
                                      //                                  NSString * store_url = [@"itms-apps://itunes.apple.com/app/" stringByAppendingString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]];
                                      NSString * store_url = @"itms-apps://itunes.apple.com/app/id673935516";
                                      
                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString: store_url]];
                                  }];
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        }
        if (![alertView isVisible]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [alertView show];
            });

        }
    
        return YES;
        
    }
    return NO;
}

+(BOOL)displayErrors:(NSDictionary *)wsReturn{
    if ( [[wsReturn objectForKey:@"status"] isEqualToString:@"ko"]){
        NSString * errorMessage = [LanguageManager get:[wsReturn objectForKey:@"message"]];
        
        if (!errorMessage){
            errorMessage = [wsReturn objectForKey:@"message"];

            if (!errorMessage)
                errorMessage = @"Problème serveur";
        }
        id rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        
        if ([rootController isKindOfClass:NSClassFromString(@"RootVC")]) {
            if (IS_IPHONE) {
                [[[(RootVC*)rootController centerPanel] view] makeToast:errorMessage duration:4.0 position:@"bottom" title:[LanguageManager get:@"ios_title_erreur"] image:nil style:nil completion:nil];
            }
        }else{
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_label_probleme"]
                                                             andMessage:errorMessage];
            [alertView addButtonWithTitle:@"Ok"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                  }];
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            [alertView show];
        }
        
        return YES;
        
    }
    return NO;
}

#pragma mark - Posts sans paramètres

+(BOOL)post:(NSString *)url callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"post" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)post:(NSString *)url callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"post" mixedParam:nil callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Posts avec data

+(BOOL)post:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"post" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)post:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"post" mixedParam:data callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Posts avec paramètres

+(BOOL)post:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"post" mixedParam:param callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)post:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"post" mixedParam:param callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Gets sans paramètres

+(BOOL)get:(NSString *)url callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"get" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)get:(NSString *)url callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"get" mixedParam:nil callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Gets avec data

+(BOOL)get:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"get" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)get:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"get" mixedParam:data callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Gets avec paramètres

+(BOOL)get:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"get" mixedParam:param callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)get:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"get" mixedParam:param callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Puts sans paramètres

+(BOOL)put:(NSString *)url callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"put" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)put:(NSString *)url callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"put" mixedParam:nil callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Puts avec data

+(BOOL)put:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"put" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)put:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"put" mixedParam:data callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Puts avec paramètres

+(BOOL)put:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"put" mixedParam:param callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)put:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"put" mixedParam:param callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}


#pragma mark - Deletes sans paramètres

+(BOOL)delete:(NSString *)url callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"delete" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)delete:(NSString *)url callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"delete" mixedParam:nil callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Deletes avec data

+(BOOL)delete:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"delete" mixedParam:nil callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)delete:(NSString *)url data:(NSData *)data callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"delete" mixedParam:data callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Deletes avec paramètres

+(BOOL)delete:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback
{
    return [WService url:url method:@"delete" mixedParam:param callback:callback errorMessage:nil activityMessage:nil];
}

+(BOOL)delete:(NSString *)url param:(NSDictionary *)param callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    return [WService url:url method:@"delete" mixedParam:param callback:callback errorMessage:errorMessage activityMessage:activityMessage];
}

#pragma mark - Fonction de requete commune

+(BOOL)contientDonnees:(NSDictionary*)mixedParam{
    
    for (id key in mixedParam) {
        if([mixedParam[key] isKindOfClass:NSClassFromString(@"NSData")]){
            return true;
        }
    }
    
    return false;
}

/*
 url: Url du webService
 mixedParam: NSDictionary de paramètres, NSData de binaire, ou nil pour aucun
 callback: fonction de retour
 errorMessage: nil pour aucun, NSString pour prompt avec proposition de réessayer
 activityMessage: NSString pour afficher le chargement au milieu de l'écran avec message, @"" sans message, nil pour ne pas afficher de chargement
 */

+(BOOL)url:(NSString *)url method:(NSString*)httpMethod mixedParam:(id)mixedParam callback:(void (^)(NSDictionary *))callback errorMessage:(NSString *)errorMessage activityMessage:(NSString *)activityMessage
{
    
    NSArray * allowed_methods = @[@"POST", @"PUT", @"GET", @"DELETE"];
    NSAssert([allowed_methods indexOfObject:[httpMethod uppercaseString]] != NSNotFound, @"La méthode HTTP passée en parametre n'existe pas.");
    
    NSMutableString * final_url = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@/%@",sc_server, url]];
    DLog(@"Appel %@", final_url);
    //*********************************\\
    //Mise en place du toast d'activité\\
    //*********************************\\
    
    //Vue courante (nil s'il n'y a pas d'activityMessage
    UIView * currentView;
    
    //On récupère la view du navigation controller
    
    //Sécurité, au cas ou le rootController ne serait pas RootVC (quand SIAlert est affiché, c'est lui le root. Pourquoi, je sais pas.)
    //TODO A regarder plus tard, on ne peut pas remplacer le rootViewController normalement, comment il fait lui?
    
    id rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    if ([rootController isKindOfClass:NSClassFromString(@"RootVC")]) {
        currentView = [[(RootVC*)rootController centerPanel] view];
    }
    //Cas de l'iPad
    else if([rootController isKindOfClass:NSClassFromString(@"RootNavigation_iPad")]) {
        currentView = [(RootNavigation_iPad *)rootController  view];
    }
    
    //Si c'est nil, on ne fait rien
    if (activityMessage) {
        
        if ([activityMessage isEqualToString:@""]) {
            [currentView makeToastActivity:@"bottom"];
        }
        else{
//            [currentView makeToastActivity:@"bottom" andMessage:activityMessage];

            [currentView makeToastActivity:@"bottom"];
            [currentView makeToast:activityMessage];
        }
    }
    
    //*********************************\\
    //******* Test de connexion *******\\
    //*********************************\\
    
    struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(sc_port);
    address.sin_addr.s_addr = inet_addr([sc_hostname UTF8String]);
    
    if( ![[[ReachabilityManager sharedManager] reachability] isReachable] )
    {
        //Si on ne peut pas joindre le serveur alors annule le bolléen de liaison avec le serveur
        [UIViewController setLieAuServeur:NO];
        
        //*********************************\\
        //***** Alerte personnalisée ******\\
        //*********************************\\
        
        //Si c'est nil, on n'affiche rien
        if (errorMessage) {
            
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_erreur_connexion"] andMessage:errorMessage];
            [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alertView) {
                                  }];
            [alertView addButtonWithTitle:[LanguageManager get:@"ios_button_reesayer"]
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      
                                      [alertView dismissAnimated:NO];
                                      [WService url:url method:httpMethod mixedParam:mixedParam callback:callback errorMessage:errorMessage activityMessage:activityMessage];
                                      
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            
            [alertView show];
        }
        [currentView hideToastActivity];
        return false;
    }
    
    //*********************************\\
    //*** Construction de la requête **\\
    //*********************************\\
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:final_url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3000];
    
    // Set de la méthode
    [request setHTTPMethod:[httpMethod uppercaseString]];
    
    // On détermine si le contenu doit être dans le corps de la requête (POST && PUT), ou dans l'URL en querystring (GET && DELETE)
    BOOL params_on_body = false;
    
    if ([[httpMethod uppercaseString] isEqualToString:@"POST"] || [[httpMethod uppercaseString] isEqualToString:@"PUT"])
        params_on_body = true;
    
    //Si il y a des paramètres à envoyer
    if (mixedParam) {
        
        if ([mixedParam isKindOfClass:NSClassFromString(@"NSDictionary")]) { //Si c'est un dico de paramètres
            
            //Si il y a des données dans ce dico (exemple pieces jointes pour les messages)
            if([self contientDonnees:mixedParam]){
                
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", @"---------------------------14737809831466499882746641449"];
                
                [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                NSData* data = [self multipartStringAvecParametres:mixedParam];
                
                
                [request setHTTPBody:data];
                
                NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[data length]];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                
            }else{
                
                NSString * queryString = [self urlEncodedString:mixedParam];
                
                NSString *final_query_string = [NSString stringWithFormat:@"%@&os=%@&version=%@", queryString, sc_os, sc_version];
                
                if (params_on_body)
                {
                    //On met le content type application/x-www-form-urlencoded
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    
                    [request setHTTPBody:[NSData dataWithBytes:[final_query_string UTF8String] length:[final_query_string length]]];
                }
                else{
                    [final_url appendString:[NSString stringWithFormat:@"?%@",final_query_string]];
                    //On change le contenu de l'url de la requete pour inclure les paramètres
                    [request setURL:[NSURL URLWithString:final_url]];
                }
            }
        }
        //Si c'est des datas (uniquement le cas pour les avatars)
        else if ([mixedParam isKindOfClass:NSClassFromString(@"NSData")])
        {
            
            NSString *filename = @"fileNameTest";
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *postbody = [NSMutableData data];
            
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"avatar\"; filename=\"%@.jpg\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[NSData dataWithData:mixedParam]];
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:postbody];
        }
        //Si c'est aucun des deux, c'est louche.
        else
            return false;
    }
    //S'il n'y a pas de paramètres
    else
    {
        
        
        NSString *final_query_string = [NSString stringWithFormat:@"os=%@&version=%@", sc_os, sc_version];
        
        if (params_on_body)
        {
            //On met le content type application/x-www-form-urlencoded
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            
            [request setHTTPBody:[NSData dataWithBytes:[final_query_string UTF8String] length:[final_query_string length]]];
        }
        else{
            [final_url appendString:[NSString stringWithFormat:@"?%@",final_query_string]];
            //On change le contenu de l'url de la requete pour inclure les paramètres
            [request setURL:[NSURL URLWithString:final_url]];
        }
    }
    
    //*********************************\\
    //****** Envoi de la requête ******\\
    //*********************************\\
    
    if([NSURLConnection respondsToSelector:@selector(sendAsynchronousRequest:queue:completionHandler:)])
    {
        //On récupère notre propre queue d'opération pour nos appels web
        //Et on va fixer le nombre d'opération concurrente à 6
        [WService initialize];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [(NSOperationQueue*)[NSOperationQueue mainQueue] setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:_sharedNetworkQueue
                               completionHandler:
         ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             DLog(@"Réponse du serveur : %@", final_url);
             //to hide
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             
             NSError *jsonParsingError = nil;
             BOOL estUnTelechargement;
             
             if([response respondsToSelector:@selector(allHeaderFields)]){
                 NSString* contentDisposition = [(NSHTTPURLResponse*)response allHeaderFields][@"Content-Disposition"];
                 estUnTelechargement = (contentDisposition.length >= 10 && [[contentDisposition substringToIndex:10] isEqualToString:@"attachment"]);
             }
             
             //Si data n'est pas nil, et qu'il n'y a pas d'erreur fatale client / serveur
             if(data && ([(NSHTTPURLResponse*)response statusCode] <= 400))
             {
                 NSDictionary *wsReturn = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
                 
                 //DLog(@"Erreur JSON : %@", jsonParsingError);
                 
                 //Retour serveur & pas d'erreur json
                 if (wsReturn && !jsonParsingError && callback) {
                     // Vérification que la version ne soit pas obsolète
                     if (![self isObsoleteVersion:wsReturn]) {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             //On change le booléen correspondant à l'état de connection
                             [UIViewController setLieAuServeur:YES];
                             callback(wsReturn);
                         });
                     }
                 }else if (estUnTelechargement){
                     //Dans le cas d'une piece jointe = "Content-Disposition" = "attachment
                     dispatch_async(dispatch_get_main_queue(), ^{
                         //On change le booléen correspondant à l'état de connection
                         [UIViewController setLieAuServeur:YES];
                         NSMutableDictionary * dic = [NSMutableDictionary new];
                         [dic setObject:data forKey:@"data"];
                         callback(dic);
                     });
                 }
                 else{
                     [UIViewController setLieAuServeur:NO];
#ifdef DEBUG_DEVELOPMENT
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [currentView makeToast:[[jsonParsingError userInfo] objectForKey:@"NSDebugDescription"] duration:3.0 position:@"bottom" title:[LanguageManager get:@"ios_title_erreur"] image:[UIImage imageNamed:@"delete_48"] style:nil completion:nil];
                     });

#else
                     id tracker = [[GAI sharedInstance] defaultTracker];
                     
                     [tracker send:[[GAIDictionaryBuilder createExceptionWithDescription:
                                     [NSString stringWithFormat:@"Code %d - WS : %@ ; param : %@",
                                      [(NSHTTPURLResponse*)response statusCode],
                                      url,
                                      [[jsonParsingError userInfo] objectForKey:@"NSDebugDescription"]] withFatal:NO] build]];
#endif
                     
                 }
             }
             else{
                 [UIViewController setLieAuServeur:NO];
#ifdef DEBUG_DEVELOPMENT
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [currentView makeToast:[[error userInfo] objectForKey:@"NSErrorFailingURLStringKey"] duration:3.0 position:@"bottom" title:[LanguageManager get:@"ios_title_erreur"] image:[UIImage imageNamed:@"delete_48"] style:nil completion:nil];
                 });
#else
                 id tracker = [[GAI sharedInstance] defaultTracker];
                 
                 [tracker send:[[GAIDictionaryBuilder createExceptionWithDescription:
                                 [NSString stringWithFormat:@"Code %d - WS : %@ ; param : %@",
                                  [(NSHTTPURLResponse*)response statusCode],
                                  url,
                                  [mixedParam description]] withFatal:NO] build]];
#endif
                 DLog(@"%@",[error description]);
                 
                 // [currentView makeToast:[[error userInfo] objectForKey:@"NSErrorFailingURLStringKey"]
                 //                 [currentView makeToast:[LanguageManager get:@"ios_label_rapport_bug"]
                 
                 
             }
             
             
             //*********************************\\
             //*Suppression du toast d'activité*\\
             //*********************************\\
             
             //S'il n'y a pas de message / que currentView est nil (double sécurité), on ne fait rien
             if (currentView)
             {
                 //Sinon, on cache le toast
                 [currentView hideToastActivity];
             }
             
         }];
        
        return true;
    }
    else
        return false;
}

+(BOOL) isReachable{
    if([[Reachability reachabilityWithHostname:sc_hostname] currentReachabilityStatus] == NotReachable)
    {
        
        //*********************************\\
        //***** Alerte personnalisée ******\\
        //*********************************\\
        
        //Si c'est nil, on n'affiche rien
        
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_erreur_connexion"] andMessage:[LanguageManager get:@"label_erreur_reseau_indisponible"]];
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        
        
        [alertView show];
        return NO;
    }
    return YES;
}

@end