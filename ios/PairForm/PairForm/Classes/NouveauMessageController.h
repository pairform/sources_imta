//
//  AddCommentViewController.h
//  SupCast
//
//  Created by fgutie10 on 22/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Ressource.h"
//#import "MessageOptionController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface NouveauMessageController : UIViewController <UIActionSheetDelegate, UITextViewDelegate,MKMapViewDelegate>


@property(nonatomic,strong) NSString *urlPage;
@property(nonatomic,strong) Ressource * currentRessource;
@property(nonatomic) NSNumber  * idMessage;
@property(nonatomic,strong) NSNumber *  idMessageOriginal;
@property(nonatomic,strong) NSString * nom_tag;
@property(nonatomic,strong) NSNumber * num_occurence;


-(IBAction)afficherOptionMessage:(id)sender;
- (IBAction)modifierLangueMessage:(id)sender;
@end
