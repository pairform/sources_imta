#import <UIKit/UIKit.h>
#import "SpringboardItem.h"

@interface RessourceCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet SpringboardItem *itemIcon;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIView *badgeView;
@property (weak, nonatomic) IBOutlet UILabel *badgeCount;
@property (strong, nonatomic) UIColor * labelColor;
-(void)setRessourceName:(NSString*)ressourceName;
-(void)setRessourceName:(NSString*)ressourceName withColor:(UIColor*)color;
-(void)setRessourceImage:(UIImage*)image;
-(void)setRessourceImage:(UIImage*)image grayscale:(BOOL)isGrayScale;
-(void)setBadgeColor:(UIColor*)color;
-(void)setBadge:(id)text;

@end
