//
//  RessourceStoreVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ModeRessourceBrowse,
    ModeRessourceLatest
} ModeRessource;

@interface RessourceStoreVC : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>


@property (nonatomic,strong) NSMutableArray * data;
@property (nonatomic,strong) NSMutableArray * etablissementData;
@property (nonatomic,strong) NSMutableArray * latestData;
@property (nonatomic,strong) NSMutableArray * themeData;
@property (nonatomic,weak) IBOutlet UICollectionView * collectionView;
@property (nonatomic,weak) IBOutlet UIToolbar * toolbar;
@property (nonatomic,weak) IBOutlet UISegmentedControl * displaySelector;
@property (nonatomic) ModeRessource modeRessource;
/** Ressource à mettre en avant dans le kiosque. Ouvre automatiquement la ressource correspondante.
 */
@property (nonatomic, copy) NSNumber * idRessourceToHighlight;

-(IBAction)segmentedControlChanged:(id)sender;
@end
