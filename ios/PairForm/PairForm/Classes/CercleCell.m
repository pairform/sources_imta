//
//  CercleCell.m
//  PairForm
//
//  Created by admin on 12/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "CercleCell.h"

@implementation CercleCell
@synthesize members;
@synthesize boolEdit;
@synthesize id_cercle;
@synthesize index;





// PARTIE COLLECTION VIEW
//

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return members.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CercleMembreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellule" forIndexPath:indexPath];
    
    NSDictionary * member = [members objectAtIndex:indexPath.row];
    
    if (![UtilsCore getUtilisateur:[member objectForKey:@"id_utilisateur"]]) {
        [UtilsCore sauvegardeUtilisateur:[member objectForKey:@"id_utilisateur"] name:[member objectForKey:@"username"] url:[member objectForKey:@"image"]];

    }
    
    Utilisateur * utilisateur = [UtilsCore getUtilisateur:[member objectForKey:@"id_utilisateur"]];
    
    UILabel *lNom = (UILabel *)[cell viewWithTag:101];
    NSString * name = [NSString stringWithFormat:@"%@",utilisateur.owner_username];
    [lNom setText:name];
    
    UIImageView * imageView = (UIImageView *)[cell viewWithTag:102];
    
    imageView.image = [[utilisateur getImage]  thumbnailImage:50 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh];
    cell.id_utilisateur = [utilisateur id_utilisateur];
    cell.id_collection = id_cercle;

    
    UIButton *bSupprime = (UIButton *)[cell viewWithTag:103];
    if ( boolEdit ){
        [bSupprime setHidden:NO];
    }else{
        [bSupprime setHidden:YES];
    }
    
    
    return cell;
}



@end