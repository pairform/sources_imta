//
//  MenuCell.m
//  PairForm
//
//  Created by Maen Juganaikloo on 13/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuCell.h"
#import "LanguageManager.h"
@interface MenuCell()

@property (nonatomic, strong) NSString * cell_label_code;
@end
@implementation MenuCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
    
}

- (void)awakeFromNib
{
    _cell_label_code = self.cell_label.text;
    [self.cell_label setText:[LanguageManager get:_cell_label_code]];

    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self.cell_label setText:[LanguageManager get:_cell_label_code]];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];
    
    self.cell_image.image = [self.cell_image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    if (selected) {
        [self.cell_image setTintColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.9]];
        [self.cell_label setTextColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.9]];
    }
    else{
        [self.cell_image setTintColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.3]];
        [self.cell_label setTextColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.3]];
    }

}


@end
