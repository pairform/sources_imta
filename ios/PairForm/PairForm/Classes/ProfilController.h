//
//  ViewController.h
//  profil
//
//  Created by admin on 13/03/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditerCompteController.h"
#import "CercleController.h"
#import "SuccesControllerViewController.h"
#import "LanguageManager.h"


@interface ProfilController : UIViewController <CercleControllerDelegate, UIActionSheetDelegate, SuccesControllerDelegate>

@property (strong, nonatomic) NSNumber * id_utilisateur;

@end
