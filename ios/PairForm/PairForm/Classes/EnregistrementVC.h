//
//  EnregistrementVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "RMPickerViewController.h"


@interface EnregistrementVC : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, RMPickerViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) NSArray *dataSource;
@property (nonatomic, strong) UITableView * tableViewOtherLanguages;
@property (nonatomic, strong) NSMutableArray * choixOtherLanguages;
@property (strong, nonatomic) NSMutableArray * langueValuesString;
@property (strong, nonatomic) NSMutableArray * langueValuesId;
@property (nonatomic) int indexLangueAppSelect;

-(UIViewController*)viewControllerFromUIView:(UIView*)view;

@property(nonatomic,weak) IBOutlet UITextField *nomTextField;
@property(nonatomic,weak) IBOutlet UITextField *prenomTextField;
@property(nonatomic,weak) IBOutlet UITextField *eMailTextField;
@property(nonatomic,weak) IBOutlet UITextField *pseudoTextField;
@property(nonatomic,weak) IBOutlet UITextField *motDePasseTextField;
@property(nonatomic,weak) IBOutlet UITextField *motDePasseTextField2;
@property(nonatomic,weak) IBOutlet UITextField *etablissementTextField;
@property (weak, nonatomic) IBOutlet UIButton *langueButton;
@property (weak, nonatomic) IBOutlet UIButton *autresLanguesButton;
@property (weak, nonatomic) IBOutlet UILabel *labelChoisirPseudoMail;
@property (weak, nonatomic) IBOutlet UILabel *labelChoisirMdp;
@property (weak, nonatomic) IBOutlet UILabel *labelInfosComplement;
@property (weak, nonatomic) IBOutlet UILabel *labelLangue;
@property (weak, nonatomic) IBOutlet UILabel *labelCGU;
@property (weak, nonatomic) IBOutlet UITextView *textCGU;
@property (weak, nonatomic) IBOutlet UIWebView *webviewCGU;
@property (weak, nonatomic) IBOutlet UILabel *labelFinInscription;
@property (weak, nonatomic) IBOutlet UITextView *textFinInscription;

@property(nonatomic,strong) NSString *nomUtilisateur;
@property(nonatomic,strong) NSString *prenomUtilisateur;
@property(nonatomic,strong) NSString *eMailUtilisateur;
@property(nonatomic,strong) NSString *pseudoUtilisateur;
@property(nonatomic,strong) NSString *motDePasseUtilisateur;
@property(nonatomic,strong) NSMutableString *messageString;

-(void)enregistrer;

-(IBAction)backgroundTap:(id)sender;
- (IBAction)touchDownLanguageButton:(id)sender;
- (IBAction)touchDownOtherLanguageButton:(id)sender;
-(void) inscription;
-(NSString *)dataFilePath:(NSString *)fileName; 
//-(void)applicationWillTerminate:(NSNotification *)notification;

@end