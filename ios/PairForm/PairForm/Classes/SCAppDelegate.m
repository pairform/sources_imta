//
//  SCAppDelegate.m
//  SupCast
//
//  Created by Maen Juganaikloo on 15/04/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "SCAppDelegate.h"
#import "SIAlertView.h"
#import "RootNavigation_iPad.h"
//#import "SCRootViewController.h"
//#import "TestFlight.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "UtilsCore.h"
#import "FileManager.h"
#import "MessageCell.h"
#import "iRate.h"
#import "SommaireVC.h"
#import "PageVC.h"
#import "RootVC.h"
#import "RootNavigation.h"
#import "ProfilController.h"
#import <Crashlytics/Crashlytics.h>
#import <GoogleMaps/GoogleMaps.h>

#import <Fabric/Fabric.h>
#import "ReachabilityManager.h"

@implementation SCAppDelegate

@synthesize managedObjectContext ;
@synthesize managedObjectModel ;
@synthesize persistentStoreCoordinator ;

static bool estEnTrainDeSeConnecter = false;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //    [[NSBundle mainBundle] pathForResource:@"textured_paper" ofType:@"png"];
//    [_window setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunchUnzip"]) {

        [UtilsCore recupereDonneePieceJointe];
        [self lookForZipRessource];
        
        
    }
    //-- Set Notification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [application registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
#pragma clang diagnostic pop
    }
    
    
    
    // Optional: set Logger to VERBOSE for debug information.
//    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // start of your application:didFinishLaunchingWithOptions // ...
//    [TestFlight takeOff:@"b4b9b1bf-6bbd-418a-9180-45be4dde5987"];
    [[UIActionSheet appearance] setBackgroundColor:[UIColor darkGrayColor]];
        [[UIActionSheet appearance] setTintColor:[UIColor whiteColor]];
        [[UIActionSheet appearance] setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [_window setTintColor:[UIColor darkGrayColor]];
    
    [[UIToolbar appearance] setBackgroundColor:[UIColor darkGrayColor]];
    [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
    [[CSToastManager sharedStyle] setImageSize:CGSizeMake(64, 64)];
    //Apparence des popups
    [[SIAlertView appearance] setMessageFont:[UIFont fontWithName:@"Helvetica-neue" size:13]];
    [[SIAlertView appearance] setTitleColor:[UIColor colorWithRed:0.133 green:0.133 blue:0.133 alpha:1]];
    [[SIAlertView appearance] setMessageColor:[UIColor colorWithRed:0.267 green:0.267 blue:0.267 alpha:1]];
//    [[SIAlertView appearance] setCvBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1]];

//    [[SIAlertView appearance] setCvBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper"]]];
    [[SIAlertView appearance] setCornerRadius:12];
//    [[SIAlertView appearance] setShadowRadius:20];
//
//    [[SIAlertView appearance] setTitleColor:[UIColor colorWithWhite:0.6 alpha:1]];
//    [[SIAlertView appearance] setMessageColor:[UIColor colorWithWhite:0.6 alpha:1]];
    
    [[SIAlertView appearance] setBackgroundStyle:SIAlertViewBackgroundStyleSolid];
//    [[SIAlertView appearance] setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];

//    [[SIAlertView appearance] setCvBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.9]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    
    [[UITableView appearanceWhenContainedIn:NSClassFromString(@"RootNavigation"), nil] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"crossword"]]];
    [[UIWebView appearanceWhenContainedIn:NSClassFromString(@"RootNavigation"), nil] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"crossword"]]];

//    [[UINavigationBar appearance] setTitleTextAttributes:@{
//                                UITextAttributeTextColor: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
//                          UITextAttributeTextShadowColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
//                                     UITextAttributeFont: [UIFont fontWithName:@"Helvetica" size:0.0]
//     }];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:251/255.0f green:126/255.0f blue:4/255.0f alpha:1]];
    
    if (IS_IPHONE) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"NavigationBar"] resizableImageWithCapInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 60) resizingMode:UIImageResizingModeTile] forBarMetrics:UIBarMetricsDefault];
        else{
            
            [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"NavigationBar7"] resizableImageWithCapInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 60) resizingMode:UIImageResizingModeTile] forBarMetrics:UIBarMetricsDefault];
            //        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.94 green:0.32 blue:0 alpha:0.8]];
        }
    }
    else{
//        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.196f green:0.196f blue:0.196f alpha:1]];
        
    }
    
//    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackTranslucent];

#if TARGET_IPHONE_SIMULATOR
    
#else
    
    [[GAI sharedInstance] setTrackUncaughtExceptions:YES];
    // Initialize tracker.
    (void)[[GAI sharedInstance] trackerWithTrackingId:@"UA-40687872-3"];
    [Fabric with:@[[Crashlytics class]]];
    [Crashlytics startWithAPIKey:@"34e308708407c394e5e027465f6fb8d77cb0ccf1"];
    
#endif
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    //configure iRate
    [iRate sharedInstance].applicationBundleID = @"fr.emn.PairForm";
    [iRate sharedInstance].daysUntilPrompt = 5;
    [iRate sharedInstance].usesUntilPrompt = 10;
    [iRate sharedInstance].appStoreCountry = @"FR";
    
    if (IS_IPAD) {
        
//        RootNavigation_iPad * rootNavigation = (RootNavigation_iPad*)self.window.rootViewController;
//        rootNavigation.viewControllers = @[[[SCRootViewController alloc] initWithNibName:NSStringFromClass([SCRootViewController class]) bundle:nil]];
    }
    
    
//    [_window.rootViewController.view setTintColor:[UIColor orangeColor]];
    
	if (launchOptions != nil)
	{
		NSDictionary *dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
		if (dictionary != nil)
		{
            NSLog(@"Launched from push notification: %@", dictionary);
            [self handleRemoteNotification:application userInfo:dictionary];
		}
	}
    
    [ReachabilityManager sharedManager];
    
    /*struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(sc_port);
    address.sin_addr.s_addr = inet_addr([sc_hostname UTF8String]);
    _reachability = [Reachability reachabilityWithAddress:&address];*/
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkReachableAndGetMessages) name:kReachabilityChangedNotification object:nil];

    [self checkReachableAndGetMessages];
    
    [UtilsCore setUnseenNotificationOnAppBadge];
        
    [GMSServices provideAPIKey:@"AIzaSyBbV0k6G0iGjI8GakupzflZU2Lsha3qZYo"];
    
    [_window makeKeyAndVisible];
    
    //Tentative de connection de l'utilisateur en scred
    if ([[[ReachabilityManager sharedManager] reachability] isReachable]){
        //On prompt pour créer un compte & on met les préférences par défaut
        if([[[[UIApplication sharedApplication] keyWindow] rootViewController] checkForDefaultUser])
            //Si l'utilisateur rentre pour la première fois
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] connecterUtilisateurLocalEnSilence];
    }
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    [self handleRemoteNotification:application userInfo:userInfo];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	DLog(@"My token is: %@", deviceToken);
    
    NSString *newToken = [deviceToken description];
	newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"token_mobile"];
    
	DLog(@"My new_token is: %@", newToken);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	DLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[ReachabilityManager sharedManager].reachability stopNotifier];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //[_reachability stopNotifier];
    [[ReachabilityManager sharedManager].reachability stopNotifier];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [UtilsCore setUnseenNotificationOnAppBadge];
    //[_reachability startNotifier];
    
    [[ReachabilityManager sharedManager].reachability startNotifier];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self checkReachableAndGetMessages];
    
    if ([[[ReachabilityManager sharedManager] reachability] isReachable]){
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] connecterUtilisateurLocalEnSilence];
    }
}

-(void)checkReachableAndGetMessages{
    
    
    if ([[[ReachabilityManager sharedManager] reachability] isReachable]){
//        if(IS_IPAD){
//            [[[[UIApplication sharedApplication] keyWindow] rootViewController] checkForLoggedInUser];
//        }else{
//            if([[[[UIApplication sharedApplication] keyWindow] rootViewController] checkForDefaultUser])
//            {
//                [[[[UIApplication sharedApplication] keyWindow] rootViewController] checkForLoggedInUser];
//            }}
        [self getThoseMessagesAccordinglyToPreferences];
    }
}

-(void)getThoseMessagesAccordinglyToPreferences{
    DLog( @"%d",[[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"] intValue ]);
    if ( [[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"]intValue ] != kMessageUpdateNever){
        [UtilsCore rafraichirMessages];
    }
    if ( [[[[NSUserDefaults standardUserDefaults] persistentDomainForName:kPreferences] objectForKey:@"updateFrequency"]intValue ] == kMessageUpdateEveryMinute){
        [UtilsCore startPullingTimer];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(BOOL)lookForZipRessource
{
    //Caler ici le nom du fichier pour inclure la ressource initiale

    
    
    NSArray * arrayOfRessource = [[NSBundle mainBundle] pathsForResourcesOfType:@"zip" inDirectory:@""];
    
    NSArray * arrayOfResDictionary = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DefaultRes" ofType:@"plist"]];
    
    //Si les zip sont présents dans le bundle
    if ([arrayOfRessource count]) {
        
        
        //On enregistre les ressources par défaut dans la base de données
//        NSNumberFormatter * nFormatter = [[NSNumberFormatter alloc] init];
        
        for (NSDictionary * dico_ressource in arrayOfResDictionary) {
            
            NSNumber * id_ressource = dico_ressource[@"id_ressource"];

            if(![UtilsCore getRessource:id_ressource])
            {
                Ressource * current_ressource = [UtilsCore enregistrerRessourceFromDictionary:dico_ressource];
//                NSString * path_icone = [[NSBundle mainBundle] pathForResource:[current_ressource.id_ressource stringValue] ofType:@"png"];
//                UIImage * image = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:path_icone]]];
                //Enregistrement manuel de l'icone (datas dans le pList)
//                [current_ressource setIcone:[NSKeyedArchiver archivedDataWithRootObject:[UIImage imageNamed:[current_ressource.id_ressource stringValue]]]];
                
                for (NSDictionary * dico_capsule in dico_ressource[@"capsules"]) {
                    
                    NSNumber * id_capsule = dico_capsule[@"id_capsule"];
                    
                    if(![UtilsCore getCapsule:id_capsule])
                    {
                        NSMutableDictionary * m_capsule = [dico_capsule mutableCopy];
                        
                        [m_capsule setObject:id_ressource forKey:@"id_ressource"];
                        Capsule * current_capsule = [UtilsCore enregistrerCapsuleFromDictionary:m_capsule];
                        //Ajout de la référence de la capsule sur la ressource
                        [current_ressource addCapsulesObject:current_capsule];
                        
                        NSString *pathFichierZip = [[NSBundle mainBundle] pathForResource:[id_capsule stringValue] ofType:@"zip"];
                        //On les décompresse dans le dossier Application Support
                        NSString *ressourceDirectory = [UtilsCore getRessourcesPath];
                        NSString * final_path = [ressourceDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", current_ressource.id_ressource, current_capsule.id_capsule]];
                        [FileManager unzipFileAtPath:pathFichierZip toPath:final_path withRessource:current_ressource andCapsule:current_capsule isUpdate:NO];
                    }
                    
                }
                NSError * error;
                [[current_ressource managedObjectContext] save:&error];
                NSAssert(!error, [error description]);

            }
            
        }
        
        return true;
    }
    
    return false;
    
}

- (void) customizeAppearance {
    
    UIImage *i1 = [UIImage imageNamed:@"precedent.png"];
    
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:i1
                                                      forState:UIControlStateNormal
                                                    barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:i1
                                                      forState:UIControlStateHighlighted
                                                    barMetrics:UIBarMetricsDefault];
    
}


// CORE DATA

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext_local = self.managedObjectContext;
    if (managedObjectContext_local != nil) {
        if ([managedObjectContext_local hasChanges] && ![managedObjectContext_local save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            // property ID.
            id tracker = [[GAI sharedInstance] defaultTracker];
            
            //Envoi du crash
            [tracker send:[[GAIDictionaryBuilder createExceptionWithDescription:[NSString stringWithFormat:@"Context save - %@ : %@", error, [error userInfo]] withFatal:@NO] build]];

//            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}

- (NSManagedObjectContext *)childManagedObjectContext
{
//    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
//    if (coordinator != nil) {
        NSManagedObjectContext * childManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//        [childManagedObjectContext setPersistentStoreCoordinator:coordinator];
//        [managedObjectContext set]
        [childManagedObjectContext setParentContext:managedObjectContext];
        return childManagedObjectContext;
//    }
//    return nil;
}
// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"mom"];
    
    //Si on est pas sur l'ancien modèle de BDD
    if (modelURL == nil) {
        modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    }
    
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    
    // Détection d'une éventuelle migration
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                              URL:storeURL
                                                                                            error:&error];
    BOOL pscCompatibile = [[self managedObjectModel] isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata];
    
    
    BOOL __block besoinInsertionSelecteurs = NO;
    
    NSLog(@"Migration needed? %d", !pscCompatibile);
    if (sourceMetadata && !pscCompatibile) {
        
        NSSet *sourceEntities = [NSSet setWithArray:[(NSDictionary *)[sourceMetadata objectForKey:@"NSStoreModelVersionHashes"] allKeys]];
        NSSet *destinationEntities = [NSSet setWithArray:[(NSDictionary *)[[self managedObjectModel] entitiesByName] allKeys]];
        
        // Recherche des entités rajoutées
        NSMutableSet *addedEntities = [NSMutableSet setWithSet:destinationEntities];
        [addedEntities minusSet:sourceEntities];
        [addedEntities enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([obj isEqualToString:@"Selecteur"]) {
                stop = YES;
                besoinInsertionSelecteurs = YES;
            }
        }];
    
    
//        //Recherche des différences entre entitées
//        
//        NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil
//                                                                        forStoreMetadata:sourceMetadata];
//        NSManagedObjectModel * destinationModel = [self managedObjectModel];
//        
//        
//        NSMigrationManager *migrationManager =
//        [[NSMigrationManager alloc]
//         initWithSourceModel:sourceModel
//         destinationModel:destinationModel];
////
////        NSEntityDescription * capsuleEntity = [NSEntityDescription entityForName:@"Capsule" inManagedObjectContext:[self managedObjectContext]];
////    
////        NSString * old_version = [sourceMetadata objectForKey:@"NSStoreModelVersionIdentifiers"][0];
//////        NSString * new_version = [[[self managedObjectModel]] objectForKey:@"NSStoreModelVersionIdentifiers"][0];
////        [capsuleEntity attributesByName];
//        
//        NSError *error = nil;
//        
//        NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil
//                                                                forSourceModel:sourceModel
//                                                              destinationModel:destinationModel];
//        
//        if (mappingModel == nil) {
//            // deal with the error
//        }
//        
//        [migrationManager migrateStoreFromURL:storeURL
//                                                   type:NSSQLiteStoreType
//                                                options:nil
//                                       withMappingModel:mappingModel
//                                       toDestinationURL:storeURL
//                                        destinationType:NSSQLiteStoreType
//                                     destinationOptions:nil
//                                                  error:&error];
    }
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

    
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {

        if (error) {
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSString *ressourcesDirectory = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            
            NSArray *files = [fileManager contentsOfDirectoryAtPath:ressourcesDirectory error:nil];
            for (NSString *file in files) {
                NSString *fullPath = [ressourcesDirectory stringByAppendingPathComponent:file];
                if ([fileManager contentsOfDirectoryAtPath:fullPath error:nil])
                    [fileManager removeItemAtPath:fullPath error:nil];
            }
            
            [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
            
            //On recréé le store mainenant.
            [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
            
            [self lookForZipRessource];
            
            [[[[self window] rootViewController] view] makeToast:@"Suite à des changements majeurs dans l'application, vos ressources téléchargées n'ont pas pu être restaurées. Veuillez nous excuser pour cet inconvénient." duration:10 position:@"center" title:@"Mise à jour" image:nil style:nil completion:nil];
        }
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
        // property ID.
        id tracker = [[GAI sharedInstance] defaultTracker];
        
        //Envoi du crash
        [tracker send:[[GAIDictionaryBuilder createExceptionWithDescription:[NSString stringWithFormat:@"Migration coredata - %@ : %@", error, [error userInfo]] withFatal:@NO] build]];
    }
    //S'il y a besoin d'insérer les sélecteurs après coup
    if (besoinInsertionSelecteurs) {
        [self migrateCapsules];
    }
    
    //S'il y a besoin de migrer les formats de capsule (ou que du moins, ça n'a pas déjà été testé)
    if(![[NSUserDefaults standardUserDefaults] boolForKey:kMigrationFormats])
        [self migrateCapsulesFormat];
    
    return persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)migrateCapsules {
    NSArray * all_capsules = [UtilsCore getAllCapsules];
    
    for (Capsule * capsule in all_capsules) {
        [UtilsCore setSelecteurs:nil onCapsule:capsule];
    }
    NSError * error;
    
    [[self managedObjectContext] save:&error];
    if (error) {
        
        DLog(@"Migration selecteurs %@, %@", error, [error userInfo]);
        
        // property ID.
        id tracker = [[GAI sharedInstance] defaultTracker];
        
        //Envoi du crash
        [tracker send:[[GAIDictionaryBuilder createExceptionWithDescription:[NSString stringWithFormat:@"Migration selecteurs - %@ : %@", error, [error userInfo]] withFatal:@NO] build]];

    }
}

-(void)migrateCapsulesFormat {
    NSArray * all_capsules = [UtilsCore getAllCapsules];
    
    for (Capsule * capsule in all_capsules) {
        if(capsule.id_format)
            continue;
        else
            capsule.id_format = @1;
    }
    NSError * error;
    
    [[self managedObjectContext] save:&error];
    if (error) {
        
        DLog(@"Migration selecteurs %@, %@", error, [error userInfo]);
        
        // property ID.
        id tracker = [[GAI sharedInstance] defaultTracker];
        
        //Envoi du crash
        [tracker send:[[GAIDictionaryBuilder createExceptionWithDescription:[NSString stringWithFormat:@"Migration format capsules - %@ : %@", error, [error userInfo]] withFatal:@NO] build]];
        
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kMigrationFormats];
}

-(void)executeMigrationOfCapsulesIntoLibraryFolder{
    NSArray * all_capsules = [UtilsCore getAllCapsules];
    BOOL isBusy = NO;
    
    NSString * documents_directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString * application_support_directory = [UtilsCore getRessourcesPath];
    NSError * create_error;
    
    //On créé le dossier Application support, par défaut, il n'existe pas
    if (![[NSFileManager defaultManager] createDirectoryAtPath:application_support_directory withIntermediateDirectories:YES attributes:nil error:&create_error])
    {
        NSLog(@"%@", create_error);
    }

    for (Ressource * capsule in all_capsules) {
        
        //Déplacement de la ressource
        
        NSString * original_path = [NSMutableString stringWithFormat:@"%@/%@", documents_directory, [capsule nom_court]];
        NSString * wanted_path = [NSMutableString stringWithFormat:@"%@/%@", application_support_directory, [capsule nom_court]];
        
        //Si le fichier n'existe pas à cet endroit
        if ([[NSFileManager defaultManager] fileExistsAtPath:original_path] && ![[NSFileManager defaultManager] fileExistsAtPath:wanted_path]) {
            NSLog(@"Original : %@", original_path);
            NSLog(@"Wanted : %@", wanted_path);
            
            if (!isBusy) {
//                [_window.rootViewController.view makeToastActivity:@"center" andMessage:@"Veuilez patienter pendant la migration de vos données..."];
                [_window.rootViewController.view makeToastActivity:@"center"];
                isBusy = YES;
            }
            
            [FileManager migrateFromDocumentsDirectoryPath:original_path toApplicationSupportDirectory:wanted_path];
        }
        
        //Purge des vieux zip qui traîne éventuellement
        NSString * zip_path = [NSMutableString stringWithFormat:@"%@/%@.zip", documents_directory, [capsule nom_court]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:zip_path]) {
            [FileManager removeCapsuleZipAtPath:zip_path];
        }
    }
    
    if (isBusy) {
        [_window.rootViewController.view hideToastActivity];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunchMigration_V1.0.300"];
    
}


// FIN CORE DATA

-(void)handleRemoteNotification:(UIApplication*)application userInfo:(NSDictionary*)userInfo{
    [UtilsCore recordNotificationFromDictionary:userInfo];
    [UtilsCore setUnseenNotificationOnAppBadge];
    
    DLog(@"Application state : %d",[application applicationState]);
    
    if ([application applicationState] == UIApplicationStateBackground || [application applicationState] == UIApplicationStateInactive) {
        
        
        if ([userInfo[@"t"] isEqualToString:@"ui"]) {
            if (IS_IPAD) {
                SCStackViewController * stackViewController = [(RootVC_iPad*)[(RootNavigation_iPad*)[_window rootViewController] viewControllers][0] stackViewController];
                
                [stackViewController popToRootViewControllerFromPosition:SCStackViewControllerPositionRight animated:YES completion:nil];
            }
            else {
                SIAlertView * alert_info = [[SIAlertView alloc] initWithTitle:userInfo[@"aps"][@"alert"] andMessage:userInfo[@"c"]];
                alert_info.transitionStyle = SIAlertViewTransitionStyleSlideFromTop;
                alert_info.backgroundStyle = SIAlertViewBackgroundStyleGradient;
                [alert_info addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alertView) {
                    [alertView dismissAnimated:YES];
                }];
                [alert_info show];
            }
        }
        else{
            Message * selected_message = [UtilsCore getMessage:userInfo[@"im"]];
            
            if (selected_message) {
                [self showNotificationContext:userInfo withMessage:selected_message];
            }
            else{
                //Temporisation, le temps de télécharger le message
                __block NSObject * observer = [[NSNotificationCenter defaultCenter] addObserverForName:@"messagesActualises" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
                    NSLog(@"");
                    Message * selected_message = [UtilsCore getMessage:userInfo[@"im"]];
                    if (selected_message) {
                        [[NSNotificationCenter defaultCenter] removeObserver:observer];
                        [self showNotificationContext:userInfo withMessage:selected_message];
                    }
                    
                }];
                
            }
        }
    }
    else {
        if ([userInfo[@"t"] isEqualToString:@"ui"]) {
            SIAlertView * alert_info = [[SIAlertView alloc] initWithTitle:userInfo[@"aps"][@"alert"] andMessage:userInfo[@"c"]];
            alert_info.transitionStyle = SIAlertViewTransitionStyleSlideFromTop;
            alert_info.backgroundStyle = SIAlertViewBackgroundStyleGradient;
            [alert_info addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alertView) {
                [alertView dismissAnimated:YES];
            }];
            [alert_info show];
        }
        
    }

}
-(void)showNotificationContext:(NSDictionary*)userInfo withMessage:(Message*)selected_message{
    
    Capsule * selected_capsule = [UtilsCore getCapsule:selected_message.id_capsule];
    
    [UtilsCore setMessageLu:selected_message.id_message];
    
    UIStoryboard * mainStoryboard = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] getIPadStoryBoard];
    
    id pushed_vc;
    //Si on est dans le cas d'un succes, il n'y a pas de message attaché
    if (!userInfo[@"im"]) {
        ProfilController * profilVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProfilController"];
        
        [profilVC setStackPosition:SCStackViewControllerPositionLeft];
        //Si on est dans le cas d'un ajout à un cercle, ya un id d'utilisateur dans le contenu
        int id_utilisateur_distant = [userInfo[@"c"] intValue];
        
        //S'il est égal à 0, c'est que ce n'est pas un int (le cast renvoie 0 dans le cas d'une conversion ratée)
        if (id_utilisateur_distant != 0) {
            //On vérifie que l'utilisateur correspondant existe
            NSNumber * id_utilisateur_distant_number = [NSNumber numberWithInt: id_utilisateur_distant];
            if ([UtilsCore getUtilisateur: id_utilisateur_distant_number]) {
                //Si c'est cool, on va ouvrir la page profil de cette personne
                [profilVC setId_utilisateur:id_utilisateur_distant_number];
            }
        }
        pushed_vc = profilVC;
    }
    //Notifications sur un message attaché à une ressource
    else if ( [selected_message.nom_page isEqualToString:@"" ]){
        
        SommaireVC * sommaireVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SommaireVC"];
        sommaireVC.currentCapsule = selected_capsule;
        
        [sommaireVC showMessageTrans:selected_message.id_message];
        
        if (IS_IPAD) {
            [sommaireVC setStackPosition:SCStackViewControllerPositionRight];
        }
        pushed_vc = sommaireVC;
    }
    //Notifications sur un message attaché à une page / OA
    else{
        NSMutableArray * pageData = [[NSMutableArray alloc] init];
        [pageData addObject:[NSString stringWithFormat:@"%@/%@",[selected_capsule getPath],selected_message.nom_page]];
        PageVC * pageVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageVC"];
        
        if (IS_IPAD) {
            [pageVC setStackPosition:SCStackViewControllerPositionRight];
        }
        pageVC.pageData = [pageData copy];
        pageVC.currentCapsule = selected_capsule;
        pageVC.selectedIndex = 0;
        pageVC.id_message_trans = selected_message.id_message;
        pageVC.tag_trans = selected_message.nom_tag;
        pageVC.num_occurence_trans = selected_message.num_occurence;
        pushed_vc = pageVC;
    }
    
    if (IS_IPAD) {
        SCStackViewController * stackViewController = [(RootVC_iPad*)[(RootNavigation_iPad*)[_window rootViewController] viewControllers][0] stackViewController];
        
        [stackViewController popToRootViewControllerFromPosition:SCStackViewControllerPositionRight animated:YES completion:^{
            [stackViewController pushViewController:pushed_vc fromViewController:[_window rootViewController] atPosition:SCStackViewControllerPositionRight unfold:TRUE animated:TRUE completion:nil];
        }];
    }
    else{
        [(RootNavigation*)[(RootVC*)[_window rootViewController] centerPanel] pushViewController:pushed_vc animated:YES];
    }
}

-(UIStoryboard*)getIphoneStoryBoard{
    if(self.iphoneStoryBoard == nil)
        self.iphoneStoryBoard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                          bundle:[NSBundle mainBundle]];
    return self.iphoneStoryBoard;
}

-(UIStoryboard*)getIPadStoryBoard{
    if(self.iPadStoryBoard == nil)
        self.iPadStoryBoard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:[NSBundle mainBundle]];
    return self.iPadStoryBoard;
}

+(BOOL)loginEnCours{
    return estEnTrainDeSeConnecter;
}

+(void)loginEnCours:(BOOL)valeur{
    estEnTrainDeSeConnecter = valeur;
}

@end
