//
//  DashboardVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 07/05/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "DashboardVC.h"
#import "MessageController.h"
#import "RessourceStoreVC.h"
#import "Notification.h"

@interface DashboardVC ()

@property (nonatomic,strong) NSMutableArray * newsArray;

@property (nonatomic, weak) IBOutlet UILabel * label_derniers_messages;
@property (nonatomic, weak) IBOutlet UILabel * label_dernieres_infos;
@property (nonatomic, weak) IBOutlet UILabel * label_dernieres_ressources_ajoutees;
@end

@implementation DashboardVC


-(void)viewDidLoad{
    [super viewDidLoad];
    [self setContent];
    
    [self initNotificationDatasource];
    
       [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"new_user_notification" object:nil];
    
    if (IS_IPAD && ![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunchMigration_V1.0.300"]) {
        [(SCAppDelegate*)[[UIApplication sharedApplication] delegate] executeMigrationOfCapsulesIntoLibraryFolder];
    }

    //_newsArray = [[NSMutableArray alloc] initWithObjects:@{@"titre" : @"Nouvelle version disponible", @"description" : @"Une nouvelle version iPad va être mise en ligne. Elle améliorera la navigation..."},@{@"titre" : @"Présentation de PairForm",@"description" : @"A l'ecole de commerce de Grenoble, le 20 juin prochain. Réservement sur http://www.pairform.fr"},@{@"titre" : @"Utilisation de PairForm à l'étranger",@"description" : @"A l'institut Mines-Télécom, l'utilisation de PairForm dans les classes renouvelle et révolutionne la pédagogie!"}, nil];
}

-(void)initNotificationDatasource{
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Notification" inManagedObjectContext:[(SCAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]];
    Notification * first_info = [[Notification alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    
    first_info.titre = @"Bienvenue !";
    first_info.contenu = @"Téléchargez votre première ressource en cliquant sur \"Mes ressources\", puis sur le \"+\" dans la barre de navigation!";
    first_info.date_creation = [NSDate date];
    
    _newsArray = [[NSMutableArray alloc] init];
    //User 0 car les notifications ne sont pas adr
    [_newsArray addObjectsFromArray:[UtilsCore getAllNotificationsForUser:@0 ofType:@"ui"]];
    [_newsArray addObject:first_info];
}
-(void)updateNotificationDatasource{
    [self initNotificationDatasource];
    [self.tableView reloadData];
    [self tintInfo];
}

-(void)reloadTable:(NSNotification*)aNotification{
    if ([aNotification.userInfo[@"t"] isEqualToString:@"ui"]) {
        [self updateNotificationDatasource];
    }
}
-(void)setContent{
    [_label_derniers_messages setText:[LanguageManager get:@"label_derniers_messages"]];
    [_label_dernieres_infos setText:[LanguageManager get:@"label_dernieres_infos"]];
    [_label_dernieres_ressources_ajoutees setText:[LanguageManager get:@"label_dernieres_ressources_ajoutees"]];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    if ([segue.identifier isEqualToString:@"messageContainerSegue"]) {
        
        [(MessageController*)segue.destinationViewController setModeMessage: ModeMessageLatest];
    }
    else if ([segue.identifier isEqualToString:@"ressourceStoreContainerSegue"]) {
        
        [(RessourceStoreVC *)segue.destinationViewController setModeRessource: ModeRessourceLatest];
    }
    // Pass the selected object to the new view controller.
}

#pragma mark - Tableview delegate & datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_newsArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    Notification * current_news = [_newsArray objectAtIndex:indexPath.row];
    
    UILabel * cell_titre = (UILabel*)[cell viewWithTag:1002];
    [cell_titre setText:current_news.titre];
    
    UILabel * cell_description = (UILabel*)[cell viewWithTag:1003];
    [cell_description setText:current_news.contenu];
    NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
    [dFormatter setDateFormat:@"dd/MM/yy"];

    UILabel * cell_date = (UILabel*)[cell viewWithTag:1004];
    [cell_date setText: [dFormatter stringFromDate:current_news.date_creation]];

    
    return cell;
}

-(void)tintInfo {
    //Récuperation de la dernière info ajoutée
    UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    CALayer *tintLayer = [CALayer layer];
    
    // Center the layer on the view
    [tintLayer setBounds:[cell.contentView frame]];
    [tintLayer setPosition:CGPointMake([cell.contentView frame].size.width/2.0,
                                       [cell.contentView frame].size.height/2.0)];
    
    // Fill the color
    [tintLayer setBackgroundColor:
     //         [[UIColor colorWithRed:0.5 green:0.5 blue:0.0 alpha:1.0] CGColor]];
     [[UIColor orangeColor] CGColor]];
    
    [tintLayer setOpacity:0.5];
    [[cell.contentView layer] addSublayer:tintLayer];
    [cell setClipsToBounds:YES];
    
    // Add the animation
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:0.5]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    //        [animation setFillMode:kCAFillModeRemoved];
    // Animate back to the starting value automatically
    //        [animation setAutoreverses:YES];
    // Animate over the course of 5 seconds.
    [animation setDuration:4.0];
    [animation setRemovedOnCompletion:NO];
    [tintLayer addAnimation:animation forKey:@"opacity"];
    
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 3.5);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void){
        
        [tintLayer removeFromSuperlayer];
    });

}
@end
