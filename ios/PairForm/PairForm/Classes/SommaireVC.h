//
//  SommaireVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RXMLElement.h"
#import "Capsule.h"
#import "TDBadgedCell.h"

@interface SommaireVC : UITableViewController


@property (nonatomic, retain) NSMutableArray *arrayForTable;
@property (nonatomic, strong) NSArray * rootArray;
@property (nonatomic, strong) NSMutableArray * subMenuStack;
@property (nonatomic, strong) Capsule * currentCapsule;
@property (nonatomic, weak) IBOutlet UIButton * infoButton;
@property (nonatomic, weak) IBOutlet UIButton * messagesButton;
@property (nonatomic, weak) IBOutlet UIImageView * icone_etablissement;
@property (nonatomic, strong) NSString * lastPageUrlReaded;
@property (nonatomic, strong) NSIndexPath * selectedIndexPath;


-(void)checkForUpdate;

-(void) showMessageTrans:(NSNumber *)id_message_trans;
-(void) updateFromPageVC:(NSString*)url;
@end
