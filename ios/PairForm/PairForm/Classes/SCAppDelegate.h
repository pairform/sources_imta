//
//  SCAppDelegate.h
//  SupCast
//
//  Created by Maen Juganaikloo on 15/04/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@class SCViewController;

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) Reachability * reachability;
@property (strong, nonatomic) SCViewController *viewController;
@property (strong) UIStoryboard * iphoneStoryBoard;
@property (strong) UIStoryboard * iPadStoryBoard;

-(BOOL)lookForZipRessource;
-(void)customizeAppearance;
- (NSManagedObjectContext *)childManagedObjectContext;
/** Vérifie si l'utilisateur a des capsules au mauvais emplacement (Documents directory). S'il en a, on affiche un HUD et on déplace toute les ressources physiquement, et on update la base.
 */
-(void)executeMigrationOfCapsulesIntoLibraryFolder;
-(UIStoryboard*)getIphoneStoryBoard;
-(UIStoryboard*)getIPadStoryBoard;
+(void)loginEnCours:(BOOL)valeur;
+(BOOL)loginEnCours;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
