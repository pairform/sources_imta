//
//  CarteHeaderCell.h
//  PairForm
//
//  Created by Maen Juganaikloo on 03/07/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartePanel.h"

@interface CarteHeaderCell : UITableViewCell

@property (nonatomic, strong) IBOutlet CartePanel *cartePanel;
@end
