//
//  NewMessagePanel.m
//  PairForm
//
//  Created by vwilme15 on 21/04/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "NewMessagePanel.h"
#import "LanguageManager.h"

@implementation NewMessagePanel

- (void)initView{
    DLog(@"Init du message panel");
    
    [_messageTextView setText:[LanguageManager get:@"ios_label_message_placeholder"]];

}


@end
