//
//  GestionPJPanel.m
//  PairForm
//
//  Created by vincent on 09/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "GestionPJPanel.h"
#import "LanguageManager.h"
#import "SIAlertView.h"

@interface GestionPJPanel()


@property (nonatomic) int frame_x, frame_y, margin;
//coefCompression représente le coef de compression entre 0 et 1 avec 1 = pas compréssé
@property (nonatomic) int scroolViewHeight, tailleImage, coefCompression;
@property (nonatomic) long tailleTotalPJMenu;

/*@property (nonatomic) BOOL enregistrement;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic) int secondesRestante;*/

@property (nonatomic) UIImagePickerController *imagePicker;
@property (nonatomic) SIAlertView *alertView;

@end

@implementation GestionPJPanel

//Permet d'initialiser la vue des pièces jointes
- (void)initView{
    //Tableau des pj du message
    self.pjTableau = [[NSMutableArray alloc] init];
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.coefCompression = 0.7;
    
    [self.pjCollectionView setDelegate:self];
    [self.pjCollectionView setDataSource:self];
    
    self.imagePicker.delegate = self;
    self.imagePicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.margin = 5;
    self.frame_x = self.margin;
    self.frame_y = self.margin;
    //self.scroolViewHeight = self.pjScrollView.frame.size.height;
    self.tailleImage = 74;
}

#pragma -
#pragma Evenements

- (IBAction)surAjoutPhotoDemande:(id)sender{
    
    [self prendrePhoto];

    
    /*__weak typeof(self) weakSelf = self;
    self.alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_alertview_pj_library_photo_titre"]
                                             andMessage:[LanguageManager get:@"ios_alertview_pj_library_photo_contenu"]];
    
    [self.alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    
    [self.alertView addButtonWithTitle:[LanguageManager get:@"ios_alertview_pj_library_photo_titre_button"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              [weakSelf ouvrePhototheque];
                          }];
    
    [self.alertView addButtonWithTitle:[LanguageManager get:@"ios_alertview_pj_library_photo_prendre"]
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [weakSelf prendrePhoto];
                          }];
    
    self.alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    self.alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [self.alertView show];*/
}

- (IBAction)surAjoutVideoDemande:(id)sender{
    
   /* __weak typeof(self) weakSelf = self;
    self.alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_alertview_pj_library_video_titre"]
                                             andMessage:[LanguageManager get:@"ios_alertview_pj_library_video_contenu"]];
    
    [self.alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    
    [self.alertView addButtonWithTitle:[LanguageManager get:@"ios_alertview_pj_library_video_titre_button"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              [weakSelf ouvreVideotheque];
                          }];
    
    [self.alertView addButtonWithTitle:[LanguageManager get:@"ios_alertview_pj_library_video_prendre"]
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [weakSelf prendreVideo];
                          }];
    
    self.alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    self.alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [self.alertView show];*/
    [self ouvrePhototheque];
}

/*- (IBAction)surAjoutSonDemande:(id)sender{
    
    //[LanguageManager get:@"ios_alertview_pj_supprimer_titre"]
    
    self.secondesRestante = 0;
    
    NSString *docsDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* nomFichier = @"son_message.caf";
    NSURL *soundFileURL = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:nomFichier]];
    NSDictionary *recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    //[NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
                    
    if (!error){
        [_audioRecorder prepareToRecord];
    }

    
    
    self.enregistrement = false;
    __weak typeof(self) weakSelf = self;
    
    self.alertView = [[SIAlertView alloc] initWithTitle:@"Son" andMessage:@"Enregistrer un son audio"];
    
    [self.alertView addButtonWithTitle:@"Annuler"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              
                              if (weakSelf.audioPlayer.playing) {
                                  [weakSelf.audioPlayer stop];
                              }
                              
                              if (weakSelf.audioRecorder.recording)
                                  [weakSelf.audioRecorder stop];

                              
                              [weakSelf.audioRecorder deleteRecording];
                              
                              [alertView dismissAnimated:YES];
                          }];
    
    [self.alertView addButtonWithTitle:@"Valider"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              if (weakSelf.audioPlayer.playing) {
                                  [weakSelf.audioPlayer stop];
                              }
                              
                              NSData* data = [[NSData alloc] initWithContentsOfFile:soundFileURL];
                              
                              [weakSelf creerPJSonAvecDonnees:data];
                              
                              //On supprime le fichier temporaire qui contenait le son
                              [weakSelf.audioRecorder deleteRecording];
                              
                              [alertView dismissAnimated:YES];
                          }];
    
    [self.alertView addButtonWithTitle:@"Enregistrer"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              
                              //On est en train d'enregistrer
                              //On va arrêter l'enregistrement
                              if (weakSelf.audioRecorder.recording)
                              {
                                  [weakSelf.audioRecorder stop];
                                  weakSelf.enregistrement = true;
                                  //On a terminer d'enregistrer donc on autorise la validation
                                  [(UIButton*)[alertView recupererBouttons][1] setEnabled:YES];
                                  [alertView setMessage:[NSString stringWithFormat:@"Enregistrement terminé : %ds", weakSelf.secondesRestante]];
                                  
                                  [weakSelf stopTimer];
                                  
                                  //On propose de l'écouter
                                  UIButton* b = [alertView recupererBouttons][2];
                                  [b setTitle:@"Ecouter" forState:UIControlStateNormal];
                                  [b setImage:[UIImage imageNamed:@"video_play_24.png"] forState:UIControlStateNormal];
                                  [b setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 10)];
                                  
                                  //Si on ecoute le son
                                  //On arrête l'écoute
                              } else if (weakSelf.audioPlayer.playing) {
                                  
                                  UIButton* b = [alertView recupererBouttons][2];
                                  [b setTitle:@"Ecouter" forState:UIControlStateNormal];
                                  [b setImage:[UIImage imageNamed:@"video_play_24.png"] forState:UIControlStateNormal];
                                  [b setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 10)];
                                  [weakSelf.audioPlayer stop];
                                  
                              } else{
                                  //Cas restant:
                                  //Si on a enregistrer
                                  //Donc on veut écouter
                                  if(weakSelf.enregistrement){
                                      NSError *error;
                                      
                                      _audioPlayer = [[AVAudioPlayer alloc]
                                                      initWithContentsOfURL:_audioRecorder.url
                                                      error:&error];
                                      
                                      weakSelf.audioPlayer.delegate = weakSelf;
                                      if(!error)
                                          [weakSelf.audioPlayer play];
                                      
                                  }else{
                                      
                                      //[recorder recordForDuration:(NSTimeInterval) 10];
                                      //Sinon c'est qu'on veut enregistrer
                                      [weakSelf.audioRecorder record];
                                      [alertView setMessage:@"Enregistrement en cours ..."];
                                      [weakSelf initEtLanceTimer];
                                  }
                                  
                                  //Cas hors condition car commun :
                                  //Dans les deux cas : on lance enregistrement ou musique on veut l'arrêter ensuite
                                  UIButton* b = [alertView recupererBouttons][2];
                                  [b setTitle:@"Arrêter" forState:UIControlStateNormal];
                                  [b setImage:[UIImage imageNamed:@"video_stop_24.png"] forState:UIControlStateNormal];
                                  [b setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 10)];
                              }
                          }];
    
    self.alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    
    self.alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    self.alertView.empecherFermeture = YES;
    [self.alertView show];
    //Par default on desactive le boutton d'ajout du son tant qu'on la pas enregistrer
    [(UIButton*)[self.alertView recupererBouttons][1] setEnabled:NO];
    //On ajoute une jolie image au bouton principale de gestion de l'enregistremeznt pour guider l'utilisateur dans ces 4 états : enregistre, stop, ecoute son, stop
    UIButton* enregistrerBoutton = [self.alertView recupererBouttons][2];
    [enregistrerBoutton setImage:[UIImage imageNamed:@"video_rec_24.png"] forState:UIControlStateNormal];
    [enregistrerBoutton setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 10)];
    
    
}*/

#pragma -
#pragma Utilitaires

//vieux code du au changement en collection
-(void)refreshScrollView{
    
    /*self.frame_x = self.margin;
    
    for(UIView *subview in [self.pjScrollView subviews]) {
        [subview removeFromSuperview];
    }
    
    for(PJMenu* pj in self.pjTableau){
        [self ajoutePJDansScrollView:pj];
    }*/
    [self.pjCollectionView reloadData];
}

//Permet de supprimer toutes les pjs dans la collection
- (void)effaceToutesPJ{
    
    [self.pjTableau removeAllObjects];
    [self refreshScrollView];
}

//On ajoute la pj dans la vue
//vieux code du au changement en collection
-(void)ajoutePJDansScrollView:(PJMenu*)pj{
    
    /*self.tailleTotalPJMenu += [pj taille];
    
    pj.view.frame = CGRectMake(self.frame_x, self.frame_y, self.tailleImage, self.tailleImage);
    self.frame_x += self.tailleImage + self.margin;
    [self.pjScrollView addSubview:pj.view];
    self.pjScrollView.contentSize = CGSizeMake(self.frame_x, self.scroolViewHeight);

    CGPoint bottomOffset = CGPointMake(0, self.pjScrollView.contentSize.height - self.pjScrollView.bounds.size.height);
    [self.pjScrollView setContentOffset:bottomOffset animated:YES];*/
}

//Vérifie que l'appareil de l'utilisateur possède bien une caméra
-(BOOL)appareilPossedeCamera{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_alertview_pj_ko_appareil_titre"]
                                                         andMessage:[LanguageManager get:@"ios_alertview_pj_ko_appareil_contenu"]];
        
        [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  
                              }];
        
        alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        
        [alertView show];

        return false;
    }
    
    return true;
}

//Lance la prise de photo
-(void)prendrePhoto{
    if([self appareilPossedeCamera]){
        [self prendrePhotoVideo:@[(NSString *) kUTTypeImage, (NSString *) kUTTypeMovie]];
    }
}
//Lance la prise de vidéo
-(void)prendreVideo{
    if([self appareilPossedeCamera]){
        [self prendrePhotoVideo:@[(NSString *) kUTTypeMovie]];
    }
}

//Méthode permettant d'ouvrir le picker de photo ou de vidéo selon le paramètre
-(void)prendrePhotoVideo:(NSArray*)params{
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePicker.mediaTypes = params;
    //self.imagePicker.allowsEditing = YES;
    
    //[imagePicker setVideoMaximumDuration:30];
    //[imagePicker setVideoQuality:UIImagePickerControllerQualityQualityTypeMedium];
    
    self.imagePicker.allowsEditing = YES;
    if (IS_IPHONE) {
        self.imagePicker.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self.viewController presentViewController:self.imagePicker
                                          animated:YES
                                        completion:nil];
    }
    else{
        UIPopoverController * imagePicker_popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
        [imagePicker_popover presentPopoverFromRect:[self.ajoutPhoto frame]
                                             inView:self
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
    }
}

//Permet d'ouvrir la galerie de photo
-(void)ouvrePhototheque{
    [self ouvrePhotoVideo:@[(NSString *) kUTTypeImage, (NSString *) kUTTypeMovie]];
}

//Permet d'ouvrir la galerie de vidéo
-(void)ouvreVideotheque{
    [self ouvrePhotoVideo:@[(NSString *) kUTTypeMovie]];
}

//Permet d'ouvrir la galerie de photo ou de vidéo selon le paramètre
-(void)ouvrePhotoVideo:(NSArray*)params{
    
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.mediaTypes = params;
    self.imagePicker.allowsEditing = YES;
    if (IS_IPHONE) {
        self.imagePicker.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self.viewController presentViewController:self.imagePicker
                                          animated:YES
                                        completion:nil];
    }
    else{
        UIPopoverController * imagePicker_popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
        [imagePicker_popover presentPopoverFromRect:[self.ajoutVideo frame]
                                             inView:self
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
    }
}

/*-(void)creerPJSonAvecDonnees:(NSData*)pj_data{
    
    PJ* pj = [[PJ alloc] init];
    pj.delegate = self;

    [pj initView:Son view:[PJ imageSonParDefault]];
    pj.data = pj_data;
    
    [self.pjTableau addObject:pj];
    [self ajoutePJDansScrollView:pj];
}*/

/*-(void)initEtLanceTimer{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(aChaqueSecondeTimer:) userInfo:nil repeats:YES];
}

-(void)aChaqueSecondeTimer:(NSTimer *) timer {
    self.secondesRestante++;
    
    [self.alertView setMessage:[NSString stringWithFormat:@"Enregistrement en cours : %ds", self.secondesRestante]];
 
}

-(void)stopTimer{
    if(self.timer)
        [self.timer invalidate];
}*/

//Méthode affichant à l'utilisateur une pop up le prévenant que le format choisi n'est pas valide
-(void)formatNonSupporte:(NSString*)format{
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_alertview_pj_format_ko_titre"] andMessage:[NSString stringWithFormat:[LanguageManager get:@"ios_alertview_pj_format_ko_contenu"], format]];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];
    
}

//Cette fonction vérifie si en ajoutant le nouvelle pj en paramètre on ne dépasse pas les limites : nombre et taille
//On renvoie un booléen selon en plus d'afficher une pop up à l'utilisateur
-(BOOL)validAjoutDeLaPieceJointePossibleAvecPopUP:(PJMenu*)pj{
    
    NSString* msgErreur = @"";

    long totalTaille = self.tailleTotalPJMenu + [pj taille];
    
    //On dépasse la limite de nombre
    if([PJ nombreTotalMaxPJ] < (self.pjTableau.count + 1)){
        msgErreur = [LanguageManager get:@"ios_alertview_pj_ko_nombre"];
    }else if([PJ tailleTotalMaxPJ] < totalTaille){
        //On dépasse la limite de poids
        msgErreur = [LanguageManager get:@"ios_alertview_pj_ko_taille"];
    }
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_alertview_pj_limitation_ko_titre"] andMessage:msgErreur];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    if(![msgErreur isEqualToString:@""]){
        [alertView show];
        return false;
    }
    
    return true;
}

#pragma -
#pragma Delegates

//Méthode appelé dès que l'utilisateur a prise/ choisi une photo/vidéo
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    //On créer une pj
    PJMenu* pj = [[PJMenu alloc] init];
    pj.data = nil;
    
    //Cas d'une image
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
        int compressionMax = 2000000; //Variable contenant la taille max avant de devoir compresser une image. Permet de ne pas compresser une image qui n'est pas de grande qualité.

        if(assetURL){ // SI la phoro est choisi
            pj.extension = [assetURL pathExtension];
            
            CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)pj.extension , NULL));
            
            //Le jpg
            if (UTTypeConformsTo(imageUTI, kUTTypeJPEG) && [PJ formatEstAuthorise:Photo format:pj.extension])
            {
                NSData *imgData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 1.0);
                
                if([imgData length] < compressionMax)
                    pj.data = imgData;
                else
                    pj.data = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], self.coefCompression);
                
            }
            else if (UTTypeConformsTo(imageUTI, kUTTypePNG) && [PJ formatEstAuthorise:Photo format:pj.extension]) //le png
            {
                pj.data = UIImagePNGRepresentation(info[UIImagePickerControllerEditedImage]);
            }
            else if([PJ formatEstAuthorise:Photo format:pj.extension]){
                pj.data = info[UIImagePickerControllerEditedImage];
            }else{
                [self formatNonSupporte:pj.extension];
                pj = nil;
            }
            
            CFRelease(imageUTI);

        }else{ //SI la photo est prise
            //On a pris la photo avec la caméra
            //On le force en jpg dans tous les cas
            pj.extension = @"jpg";
            NSData *imgData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 1.0);
            
            if([imgData length] < compressionMax)
                pj.data = imgData;
            else
                pj.data = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], self.coefCompression);
        }
        
        if(pj != nil)
            [pj initView:Photo view:[[UIImageView alloc] initWithImage:[UIImage imageWithData:pj.data]]];
        
        //Si la pièce jointe respecte les limitation du serveur
        if(pj != nil && [self validAjoutDeLaPieceJointePossibleAvecPopUP:pj]){
            
            [self.pjTableau addObject:pj];
            [self ajoutePJDansScrollView:pj];
            [self refreshScrollView];
            
        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        //Dans le cas d'un film
        // Media is a video
        NSURL * original_URL = [info objectForKey:@"UIImagePickerControllerMediaURL"];
//
        NSString * original_extension = [original_URL pathExtension];
        
        if(![PJ formatEstAuthorise:Video format:original_extension]){
            [self formatNonSupporte:original_extension];
            pj = nil;
        }
        else{
            [self makeToast:@"Compression de la vidéo..." duration:10 position:@"bottom"];
            //Conversion en mp4
            [PJ convertirVideoEnMp4AvecURL:original_URL handler:^(AVAssetExportSession * session) {
                DLog(@"%@",session.status);
                if(session.status == AVAssetExportSessionStatusCompleted){
                    
                    //Récupération de la thumbnail
                    [PJ imageAsynchroneAPartirDeVideoUrl:session.outputURL WithHandler:^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error) {
                        
                        if (result != AVAssetImageGeneratorSucceeded) {
                            NSLog(@"couldn't generate thumbnail, error:%@", error);
                        }
                        
                        UIImage* thumb = [UIImage imageWithCGImage:im];
                        
                        pj.data = UIImageJPEGRepresentation(thumb, self.coefCompression);
                        
                        UIImageView * thumb_50 = [[UIImageView alloc] initWithImage:[thumb resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(50, 50) interpolationQuality:kCGInterpolationNone]];
                        
                        [pj initView:Video view:thumb_50 andUrl:session.outputURL];
                        
                        [self.pjTableau addObject:pj];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self ajoutePJDansScrollView:pj];
                            [self refreshScrollView];
                        });
                        
                    }];

                }
            }];
            
        }
    }
    
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}



/*-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
    UIButton* b = [self.alertView recupererBouttons][2];
    [b setTitle:@"Ecouter" forState:UIControlStateNormal];
    [b setImage:[UIImage imageNamed:@"video_play_24.png"] forState:UIControlStateNormal];
    [b setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 10)];
}*/

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.pjTableau count];
}

// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PJMenu * pj = [self.pjTableau objectAtIndex:(indexPath.row)];
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"pjMenuCellTag" forIndexPath:indexPath];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    recipeImageView.image = [(UIImageView*)pj.view image];
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //Permet de supprimer une piece jointe une fois selectionner par l'utilisateur
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_alertview_pj_supprimer_titre"] andMessage:[LanguageManager get:@"ios_alertview_pj_supprimer_message"]];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView){}];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"button_supprimer"]
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              
                              PJMenu * pj = [self.pjTableau objectAtIndex:(indexPath.row)];
                              
                              [self.pjCollectionView performBatchUpdates:^{
                                  
                                  [self.pjTableau removeObject:pj];
                                  NSArray *selectedItemsIndexPaths = [self.pjCollectionView indexPathsForSelectedItems];
                                  // Now delete the items from the collection view.
                                  [self.pjCollectionView deleteItemsAtIndexPaths:selectedItemsIndexPaths];
                                  
                              } completion:nil];
                              
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];

    
}

@end
