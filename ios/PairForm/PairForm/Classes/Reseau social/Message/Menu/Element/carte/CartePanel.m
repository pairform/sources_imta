//
//  CartePanel.m
//  PairForm
//
//  Created by vwilme15 on 21/04/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "CartePanel.h"

@implementation CartePanel{
    //tableau de markeurs pour les garder en mémoire
    NSMutableArray * mapMarkers;
    //liaison vers le marker actuellement selectionné
    GMSMarker * markerSelectionne;
    UIImage * iconDeselectionne;
    //représente les coordonnées du premier message de la liste donc le plus récent
    GMSMarker* firstMarker;
}

-(void) surOuverturePanel {
    DLog(@"");
    
    [self afficheCarte];
    
}

-(void) surInitialisationPanel{
    DLog(@"");
    mapMarkers = [[NSMutableArray alloc] init];
    _messageVisibleTrierCarte = [[NSMutableArray alloc] init];
    firstMarker = [[GMSMarker alloc] init];
    firstMarker.userData = nil;
    //permet d'afficher les markeurs, et les fenetres d'infos
    self.mapView.accessibilityElementsHidden = NO;
    //permet de cacher les éléments comme : aeroports, magasins, ...
    self.mapView.indoorEnabled = NO;
    
    //Ceci centre la carte sur ma position
    //il est important après  de recentrer la carte pour voir tous les markeurs.
    self.mapView.myLocationEnabled = YES;
    
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.settings.indoorPicker = NO;
    
    self.mapView.delegate = self;
    
    //Code pour ajouter un bouton sur la carte afin de changer le bouton par défault pour la position.
    //Cela permettra en loccurence de mettre un zoom à la position voulue sur la demande de position de l'utilisateur.
    
    
    /*UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(self.mapView.bounds.size.width - 110, self.mapView.bounds.size.height - 30, 100, 20);
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    [button setTitle:@"Button" forState:UIControlStateNormal];
    [self.mapView addSubview:button];*/
}

//Méthode supprimant tous les marqueurs
-(void) reinitialiserCarte{
    DLog(@"");

    [mapMarkers removeAllObjects];
    
    //enlève TOUS de la carte
    [self.mapView clear];
}

-(void) centrerCarteSurMaPosition {
    //self.mapView.myLocation peut etre nil si l'utilisateur à refuser d'autoriser l'acces à sa position
    if(self.mapView.myLocation){
        [self.mapView animateToLocation:self.mapView.myLocation.coordinate];
    }
}

-(void) afficheCarte {
    DLog(@"");
    
    //Ajoute les marqueurs
    [self ajouterTousLesMessagesSurCarte];
    [self triMessagesSurCarte]; //Tri les messages selon leur longitude
    [self centrerCarteSurMarkeurs]; //Centre la carte
    
}

-(void) mettreAJourCarte {
    [self ajouterTousLesMessagesSurCarte];
    [self triMessagesSurCarte];
}

- (void)centrerCarteSurMarkeurs
{
    DLog(@"");
    
    //S'il y a bien un premier marqueur (cad qu'il y a au moins un message géolocalisé)
    if (firstMarker.userData != nil) {
        firstMarker.position = CLLocationCoordinate2DMake(
                                                          firstMarker.position.latitude+0.0000001,
                                                          firstMarker.position.longitude
                                                          );
        
        DLog(@"Carte centré sur : %f / %f", firstMarker.position.latitude, firstMarker.position.longitude);
        
        [self.mapView moveCamera:[GMSCameraUpdate setTarget:firstMarker.position]];
        
        if(self.mapView.camera.zoom  > 4)
            [self.mapView animateToZoom:4];
    }
    //Sinon, on centre sur la position actuelle
    else{
        [self.mapView setCamera:[GMSCameraPosition cameraWithLatitude:self.mapView.camera.target.latitude
                                                            longitude:self.mapView.camera.target.longitude
                                                                 zoom:kGMSMinZoomLevel
                                                              bearing:self.mapView.camera.bearing
                                                         viewingAngle:self.mapView.camera.viewingAngle]];
    }
    
}

/*
 * Méthode ajoutant les messages sous forme de marqueurs sur la carte
 */
-(void) ajouterTousLesMessagesSurCarte {
    DLog(@"");
    [self reinitialiserCarte];
    
    //[self testMethode];
    
    
    DLog(@"Nombre de message affichés %li", self.messages_geolocalise.count);
    
    //Pour tous les messages géolocalisés
    [self.messages_geolocalise enumerateObjectsUsingBlock:^(Message * message, NSUInteger idx, BOOL *stop) {
        
        DLog(@"Message pos %@/%@", message.geo_latitude, message.geo_longitude);
        
        GMSMarker *marker = [self ajouterMarkeurSurLaCarte:[[message geo_latitude] doubleValue] longitude:[[message geo_longitude] doubleValue]];
        
        //je filtre afin de récupèrer le dernier message posté
        
        if(firstMarker.userData == nil){
            firstMarker.userData = message.date_creation;
            firstMarker.position = CLLocationCoordinate2DMake(
                                                              marker.position.latitude,
                                                              marker.position.longitude
                                                              );
        }else{
            if([(NSDate*)firstMarker.userData compare:(NSDate*)message.date_creation] == NSOrderedAscending){
                
                firstMarker.userData = message.date_creation;
                
                firstMarker.position = CLLocationCoordinate2DMake(
                                                                  marker.position.latitude,
                                                                  marker.position.longitude
                                                                  );
            }
        }
        
        //Je récupére l'icone de base car je ne connait pas son code couleur
        if(iconDeselectionne == nil)
            iconDeselectionne = marker.icon;
        
        //Je rajoute au marqueur comme mét data l'id du message
        marker.userData = [message id_message];
        
        marker.map = self.mapView;
        
        [mapMarkers addObject:marker];
    }];
}

//Méthode ajoutant un marqueur
-(GMSMarker *) ajouterMarkeurSurLaCarte:(float)latitude longitude:(float)longitude{
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(latitude,longitude);
    marker.icon = [GMSMarker markerImageWithColor:[UIColor colorWithRed:19.0/255.0
                                                                  green:126.0/255.0
                                                                   blue:156.0/255.0
                                                                  alpha:1.0]];
    return marker;
}

/*
-(void)testMethode{
    DLog(@"");
    //creer 1500 messages
    
    int count = (int)self.messages_geolocalise.count;
    
    if(count > 0)
    for(int i=0; i<count; i++){
        for(int j=0; j<100; j++){
            [self.messages_geolocalise addObject:(Message *)self.messages_geolocalise[i]];
        }
    }
}*/

//Méthode lançant le tri des messages visible sur la carte
-(void) chargeMessageGeolocaliseEtRenvoieCeuxAffichable {
    DLog(@"");
    [self triMessagesSurCarte];
    [self rechercheLesMessageSelonCoordonnee];
}

#pragma mark - Gestion méthodes pour le tri des messages à afficher selon la caméra de la carte. Ceux ci sont trier par ordre longitudinale du plus petit au plus grand.

-(void)triMessagesSurCarte{
    DLog(@"");
    
    //[self testMethode];
    
    NSMutableArray* arrayTmp = [[NSMutableArray alloc] initWithArray:self.messages_geolocalise];
    
    [self topDownMergeSortSplit:self.messages_geolocalise debut:0 fin:(int)self.messages_geolocalise.count-1 arrayTmp:arrayTmp];
}

-(void) topDownMergeSortSplit: (NSMutableArray*)array debut:(int)gauche fin:(int)droite arrayTmp:(NSMutableArray*)arrayTmp {
    
    if(droite > gauche){
        int milieu = (droite+gauche)/2;
        
        [self topDownMergeSortSplit:array debut:gauche fin:milieu arrayTmp:arrayTmp];
        [self topDownMergeSortSplit:array debut:milieu+1 fin:droite arrayTmp:arrayTmp];
        
        [self topDownMergeSortMerge:array debut:gauche milieu:milieu+1 fin:droite arrayTmp:arrayTmp];
    }
    
}

-(void) topDownMergeSortMerge: (NSMutableArray*)array debut:(int)gauche milieu:(int)milieu fin:(int)droite arrayTmp:(NSMutableArray*)arrayTmp {
    
    int finGauche = milieu-1;
    int k = gauche;
    int num = droite - gauche + 1;
    
    while(gauche <= finGauche && milieu <= droite)
        
        if([[(Message*)array[gauche] geo_longitude] doubleValue] <= [[(Message*)array[milieu] geo_longitude] doubleValue])
            arrayTmp[k++] = array[gauche++];
        else
            arrayTmp[k++] = array[milieu++];
    
    while(gauche <= finGauche)    // Copy rest of first half
        arrayTmp[k++] = array[gauche++];
    
    while(milieu <= droite)  // Copy rest of right half
        arrayTmp[k++] = array[milieu++];
    
    // Copy tmp back
    for(int i = 0; i < num; i++, droite--){
        array[droite] = arrayTmp[droite];
    }
}

-(void) rechercheLesMessageSelonCoordonnee{
    DLog(@"");
    
    /*CGRect monEcran = self.frame;
    CGRect superviewEcran = [[self superview] frame];
    
    monEcran.size.width = superviewEcran.size.width;
    monEcran.size.height = superviewEcran.size.height;
    [self setFrame:monEcran];
    
    [self.mapView setFrame:monEcran];
    
    
    */
    GMSCoordinateBounds* boundingBox  = [[GMSCoordinateBounds alloc] initWithRegion:self.mapView.projection.visibleRegion];
    
    
    //recherche dichotomique selon la longitude
    //Récupération des coordonnées de la partie de la carte visible
    double longMin = boundingBox.southWest.longitude;
    double longMax = boundingBox.northEast.longitude;
    double latMin = boundingBox.southWest.latitude;
    double latMax = boundingBox.northEast.latitude;
    
    [_messageVisibleTrierCarte removeAllObjects];
    
    /////////////////////////////
    //Var générale
    int taille_tab = (int)self.messages_geolocalise.count;
    
    /////////////////////////////
    //Var min
    double min = longMin;
    bool trouveMin = false;
    
    int milieu = 0, fin = taille_tab, debut = 0;
    double valeur;
    /////////////////////////////
    
    /////////////////////////////
    //Var max
    double max = longMax;
    bool trouveMax = false;
    
    int milieuMax = 0, finMax = taille_tab, debutMax = 0;
    double valeurMax;
    /////////////////////////////
    
    //Si il n'y a aucun message géolocalisé
    if (taille_tab == 0){
        [self.delegate surModificationCamera:_messageVisibleTrierCarte];
        return;
    }
    
    //Gestion des cas spéciaux :
    //Si le minimum est supérieur au maximum du tableau alors on est hors de l'intervalle
    //Si le maximum est inférieur au minimum du tableau alors on est hors de l'intervalle
    //Dans ces deux cas on n'entre pas dans la boucle
    if( min < [[self.messages_geolocalise[taille_tab-1] geo_longitude] doubleValue] && max > [[self.messages_geolocalise[0] geo_longitude] doubleValue])
        for(int k = 0; k < taille_tab; k++){
            
            DLog(@"Milieu max : %d === milieu min : %d", milieuMax, milieu);
            
            //Gestion du max !!
            //on chercher l'indice de la valeur qui est égale ou juste inférieur au max
            if(milieuMax != -1 && !trouveMax){ //Si on n'a tjr pas trouvé le maximum
                
                milieuMax = (finMax + debutMax) / 2;
                valeurMax = [[self.messages_geolocalise[milieuMax] geo_longitude] doubleValue];
                
                if(valeurMax == max){
                    int milieu_tmp = milieuMax;
                    while(milieu_tmp < taille_tab-1 && valeurMax == max){
                        milieu_tmp += 1;
                        valeurMax = [[self.messages_geolocalise[milieu_tmp] geo_longitude] doubleValue];
                        
                        if(valeurMax == max)
                            milieuMax = milieu_tmp;
                    }
                    trouveMax = true;
                }
                else if(valeurMax > max){
                    if(milieuMax == 0){
                        trouveMax = false;
                        milieuMax = -1;
                    }
                    else if([[self.messages_geolocalise[milieuMax - 1] geo_longitude] doubleValue] < max){
                        trouveMax = true;
                        milieuMax = milieuMax-1;
                    }
                    else
                        finMax = milieuMax;
                }
                else if (valeurMax < max){
                    if(milieuMax == taille_tab-1){
                        trouveMax = true;
                    }
                    else if([[self.messages_geolocalise[milieuMax+1] geo_longitude] doubleValue] > max){
                        trouveMax = true;
                    }
                    else
                        debutMax = milieuMax;
                }
            }
            
            //Gestion du min !!
            //on chercher l'indice de la valeur qui est égale ou juste supérieur au minimum
            if(milieu != -1 && !trouveMin){ //Si on n'a tjr pas trouvé le minimum
                
                milieu = (fin + debut) / 2;
                valeur = [[self.messages_geolocalise[milieu] geo_longitude] doubleValue];
                
                if(valeur == min){
                    int milieu_tmp = milieu;
                    while(milieu_tmp > 0 && valeur == min ){
                        milieu_tmp -= 1;
                        valeur = [[self.messages_geolocalise[milieu_tmp] geo_longitude] doubleValue];
                        
                        if(valeur == min)
                            milieu = milieu_tmp;
                    }
                    trouveMin = true;
                }
                else if(valeur > min){
                    if(milieu == 0){
                        trouveMin = true;
                    }
                    else if([[self.messages_geolocalise[milieu-1] geo_longitude] doubleValue] < min){
                        trouveMin = true;
                    }
                    else
                        fin = milieu;
                }
                else if (valeur < min){
                    if(milieu == taille_tab-1){
                        trouveMin = false;
                        milieu = -1;
                    }
                    else if([[self.messages_geolocalise[milieu+1] geo_longitude] doubleValue] > min){
                        trouveMin = true;
                        milieu = milieu+1;
                    }
                    else
                        debut = milieu;
                }
            }
            
            if(trouveMin && trouveMax)
                break;
            if(trouveMin && milieuMax == -1)
                break;
            if(milieu == -1 && trouveMax)
                break;
            if(milieu == -1 && milieuMax == -1)
                break;
            
            if(k == taille_tab){
                milieu = -1;
                milieuMax = -1;
            }
        }
    else{
        milieu = -1;
        milieuMax = -1;
    }
    
    DLog(@"Indice === [ %d | %d ]    &&  trouvéMin == %d  && trouvéMax == %d", milieu, milieuMax, trouveMin,trouveMax);
    
    if(milieu != -1 && milieuMax != -1)
        for(int k = milieu; k <= milieuMax; k++){
            
            Message* msg = self.messages_geolocalise[k];
            
            if ([msg.geo_latitude doubleValue] >= latMin && [msg.geo_latitude doubleValue] <= latMax){
                [_messageVisibleTrierCarte addObject:msg];
                DLog(@"%%%%%% Show : Carte [ %f - %f ] = Min : %f ! Max : %f", latMin, latMax, [msg.geo_latitude doubleValue], [msg.geo_latitude doubleValue]);
            }
        }
    
    //J'envoi au délégate = messagecontroleur la liste des messages visible
    [self.delegate surModificationCamera:_messageVisibleTrierCarte];
    
}

#pragma mark - Gestion delegate

//Méthode asynchrone appelé dès que la vue de la carte change
- (void) mapView:(GMSMapView *) mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    DLog(@"");
    
    //Condition permettant de ne pas lancer la recherche lors de l'initialisation du tableView
    //Cette méthode est appelé lors de l'initialisation sinon même en mode écriture message
    //Cette appel n'est autorisé que en mode carte cf : visible
    if(!self.isHidden && self.frame.size.height > 0)
        [self rechercheLesMessageSelonCoordonnee];
}

//Méthode appelée dès que l'utilisateur click sur un marqueur
- (BOOL) mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *) marker  {
    DLog(@"Cliker sur le %ld markeur", [(NSNumber*)marker.userData integerValue]);
    
    //Si on a déjà un marker de selectionné
    if(markerSelectionne != nil){
        markerSelectionne.icon = iconDeselectionne;
    }
    
    marker.icon = [GMSMarker markerImageWithColor:[UIColor colorWithRed:251.0/255.0 green:140.0/255.0 blue:23.0/255.0 alpha:1]];
    markerSelectionne = marker;
    
    [self.delegate surChoixMarker:(NSNumber*)marker.userData];
    
    return true;
}

//Si l'utilisateur clique sur la carte
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    DLog(@"");
    //On désectionne le marqueur sélectionné
    markerSelectionne.icon = iconDeselectionne;
    markerSelectionne = nil;
}

@end
