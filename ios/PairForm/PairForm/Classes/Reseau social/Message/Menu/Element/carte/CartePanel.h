//
//  CartePanel.h
//  PairForm
//
//  Created by vwilme15 on 21/04/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@protocol CartePanelDelegate <NSObject>

@required
-(void) surModificationCamera:(NSMutableArray*)messages;
-(void) surChoixMarker:(NSNumber*)idMessage;

@end

@interface CartePanel : UIView <GMSMapViewDelegate>

//@property (strong, nonatomic) NSArray *messages_geolocalise;
@property (strong, nonatomic) NSMutableArray* messages_geolocalise;
@property (strong, nonatomic) NSMutableArray* messageVisibleTrierCarte;
//@property (strong, nonatomic) IBOutlet UIView *content_view;
@property (strong, nonatomic) IBOutlet  GMSMapView *mapView;

@property (nonatomic, strong) id<CartePanelDelegate> delegate;

-(void) centrerCarteSurMaPosition;
-(void) centrerCarteSurMarkeurs;
-(void) ajouterTousLesMessagesSurCarte;
-(GMSMarker *) ajouterMarkeurSurLaCarte:(float)latitude longitude:(float)longitude;
-(void) surOuverturePanel;
-(void) mettreAJourCarte;
-(void) surInitialisationPanel;
-(void) triMessagesSurCarte;
-(void) rechercheLesMessageSelonCoordonnee;

-(void) chargeMessageGeolocaliseEtRenvoieCeuxAffichable;
@end
