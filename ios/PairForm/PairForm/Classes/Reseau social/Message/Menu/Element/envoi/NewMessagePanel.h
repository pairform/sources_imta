//
//  NewMessagePanel.h
//  PairForm
//
//  Created by vwilme15 on 21/04/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMessagePanel : UIView

//Si jamais on a besoin d'avoir une référence vers le bouton
@property (weak, nonatomic) IBOutlet UIButton *langueBouton;
@property (weak, nonatomic) IBOutlet UIButton *optionBouton;
@property (weak, nonatomic) IBOutlet UIButton *envoieBouton;
@property (nonatomic, weak) IBOutlet UITextView *messageTextView;


- (void)initView;

@end