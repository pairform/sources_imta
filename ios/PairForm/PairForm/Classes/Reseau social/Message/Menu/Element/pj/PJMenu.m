//
//  PJ.m
//  PairForm
//
//  Created by vincent on 09/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "PJMenu.h"
#import "LanguageManager.h"
#import "SIAlertView.h"
#import <MobileCoreServices/MobileCoreServices.h>

@implementation PJMenu

/**
 * Méthode initialisant l'objet avec un type et une vue
 */
- (void)initView:(PJType)pj_type view:(UIView*)pj_view{
    self.type = pj_type;
    self.view = pj_view;
    self.url = nil;
}
- (void)initView:(PJType)pj_type view:(UIView*)pj_view andUrl:(NSURL*)url{
    self.type = pj_type;
    self.view = pj_view;
    self.url = url;
    self.extension = [url pathExtension];
}

//Retourne la taille de la pj en bytes
-(long) taille{
    
    NSNumber *tailleDonnee = @-1;
    
    if(self.url){
        NSError *fileSizeError = nil;
        
        [self.url getResourceValue:&tailleDonnee
                            forKey:NSURLFileSizeKey
                             error:&fileSizeError];
    }else if(self.data){
        tailleDonnee = [NSNumber numberWithLong:self.data.length];
    }
    
    return [tailleDonnee longValue];
}

/**
 * Renvoi les données associés à la pièces jointes et nil si impossible
 */
- (NSData*)recupereDonnee{
    if (self.url){
        return [NSData dataWithContentsOfURL:self.url];
    }else if(self.data)
        return self.data;
    else
        return nil;
}




@end
