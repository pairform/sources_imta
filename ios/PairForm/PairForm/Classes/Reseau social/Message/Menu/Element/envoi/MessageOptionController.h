//
//  MessageOptionController.h
//  SupCast
//
//  Created by admin on 04/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTagList.h"
#import "CercleController.h"
#import "LanguageManager.h"
#import "RMPickerViewController.h"

@protocol MessageOptionControllerDelegate;

@interface MessageOptionController : UIViewController<GCTagListDelegate,GCTagListDataSource,UITextFieldDelegate,CercleControllerDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) NSMutableArray *tagArray;
@property (strong, nonatomic) NSMutableArray *visibiliteArray;
@property (strong, nonatomic) NSMutableArray *visibiliteIdArray;
@property(nonatomic) BOOL  boolDefi;
@property(nonatomic) BOOL  boolGeolocalisation;
@property(nonatomic) BOOL  publiTwitter;
//@property(strong, nonatomic) NSString *langueMsg;
@property(strong, nonatomic) NSNumber *id_ressource;
@property (strong,nonatomic) NSNumber *idMessageOriginal;

@property (weak, nonatomic) IBOutlet UILabel *labelVisibilite;
@property (weak, nonatomic) IBOutlet UILabel *labelTags;
@property (weak, nonatomic) IBOutlet UILabel *labelDefi;
@property (weak, nonatomic) IBOutlet UILabel *labelMsgDefi;
@property (weak, nonatomic) IBOutlet UILabel *labelTwitter;
@property (weak, nonatomic) IBOutlet UILabel *labelGeolocalisation;

@property (weak, nonatomic) id<MessageOptionControllerDelegate> delegate;
-(IBAction)validerOptions:(id)sender;
@end

@protocol MessageOptionControllerDelegate

- (void)recupereOptions:(NSMutableArray *)visibilite visibiliteId:(NSMutableArray *)visibiliteId tagArray:(NSMutableArray *)tagArray defi: (BOOL)defi publiTwitter:(BOOL)publiTwitter geolocalise: (BOOL) switchGeolocate;

@end

