//
//  GestionPJPanel.h
//  PairForm
//
//  Created by vincent on 09/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJMenu.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

@interface GestionPJPanel : UIView <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> //UIActionSheetDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate

@property (strong, nonatomic) IBOutlet UICollectionView * pjCollectionView;
@property (strong, nonatomic) IBOutlet UIButton * ajoutPhoto;
@property (strong, nonatomic) IBOutlet UIButton * ajoutVideo;
//@property (weak, nonatomic) IBOutlet UIButton * ajoutSon;

@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) NSMutableArray *pjTableau;

- (void)initView;
- (void)effaceToutesPJ;
- (IBAction)surAjoutPhotoDemande:(id)sender;
//Cette méthode permet d'ajouter des photo depuis la gallery
//Elle garde ce nom car son vrai but est d'ajouter une vidéo mais pour cvause de format cette action est annulé
- (IBAction)surAjoutVideoDemande:(id)sender;
//- (IBAction)surAjoutSonDemande:(id)sender;


@end
