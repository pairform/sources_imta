//
//  PJ.h
//  PairForm
//
//  Created by vincent on 09/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJ.h"

@interface PJMenu : NSObject

//Contient la vue représentant cette objet
@property (strong, nonatomic) UIView * view;
//Le type de la pièce jointe ajouté
@property (nonatomic) PJType type;
//Contient les données pour une photo ou un son
@property (nonatomic) NSData* data;
//Contient l'url vers la vidéo (éviter de se balader avec des données trop grosses)
@property (nonatomic) NSURL *url;
//Contient l'extension de la pj
@property (nonatomic) NSString *extension;

- (void)initView:(PJType)pj_type view:(UIView*)pj_view;
- (void)initView:(PJType)pj_type view:(UIView*)pj_view andUrl:(NSURL*)url;
- (long)taille;
- (NSData*)recupereDonnee;


@end
