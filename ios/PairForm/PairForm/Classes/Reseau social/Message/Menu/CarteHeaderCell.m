//
//  CarteHeaderCell.m
//  PairForm
//
//  Created by Maen Juganaikloo on 03/07/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "CarteHeaderCell.h"

@implementation CarteHeaderCell

- (void)awakeFromNib {
    // Initialization code
    [_cartePanel surInitialisationPanel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
