//
//  MessageHeaderCell.h
//  PairForm
//
//  Created by vwilme15 on 21/04/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMessagePanel.h"
#import "GestionPJPanel.h"

/*
 Enumération des états possible du menu
 Les valuers sont importantes. Elle doivent correspondrent au numéro du segment correspondant du segmentedButton
 */
typedef enum {
    NewMessage = 0,     //Ecriture d'un message
    NewMessageWrite = 1,     //Ecriture d'un message
    _PJ = 2
} PFMenuEtat;

struct PFTMenuTaille
{
    float messageInit;
    float messageWrite;
    float pj;
};

static float const menuTaille = 27;

@interface MessageHeaderCell : UITableViewCell


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paper_background_height_constraint;
@property (nonatomic, assign) PFMenuEtat etat_menu;
@property (nonatomic, strong) UITableView * delegate;
@property (nonatomic, weak) IBOutlet UIImageView * paper_background;
@property (nonatomic, strong) IBOutlet NewMessagePanel *messagePanel;
@property (nonatomic, strong) IBOutlet GestionPJPanel *pjPanel;

@property (nonatomic) struct PFTMenuTaille headerTaille;

- (void)redimensionneHeaderAvecTaille;
- (float)recupererTailleActuelle;
@end