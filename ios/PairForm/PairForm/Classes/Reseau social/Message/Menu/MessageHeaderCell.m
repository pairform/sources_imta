//
//  MessageHeaderCell.m
//  PairForm
//
//  Created by vwilme15 on 21/04/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "MessageHeaderCell.h"
#import "NewMessagePanel.h"
#import "CartePanel.h"
#import "LanguageManager.h"

@interface MessageHeaderCell()

//contient le panel actuellement affiché
@property (nonatomic, weak) UIView * panel_courant;

@end

@implementation MessageHeaderCell

//Auto generated
- (void)awakeFromNib {
    DLog(@"Initialise Menu");
    // Initialization code
    
    
    [_messagePanel initView];
    [_pjPanel initView];
    
    self.etat_menu =  NewMessage;
    
    _headerTaille.messageInit = 44;
    _headerTaille.messageWrite = 120;
    _headerTaille.pj = _headerTaille.messageWrite + 84;
    
    [self redimensionneHeaderAvecTaille];
    [super awakeFromNib];
}




#pragma mark Gestion panel

- (void)changePanelActuelAvecPanel:(UIView*)panel{
    
    if(panel == self.panel_courant)
        return;
    
    UIView *cell = [self contentView];
    
    [self.delegate beginUpdates];
    [self.delegate endUpdates];
    
    [self.panel_courant setHidden:YES];
    [panel setHidden:NO];
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = cell.frame;
        frame.size.height = menuTaille;
        [cell setFrame:frame];
    } completion:^(BOOL finished) {
        [self.panel_courant setHidden:YES];
        self.panel_courant = panel;
        [self redimensionneHeaderAvecTaille];
    }];
}
-(IBAction)ouvrirPJ:(id)sender{
    
    [self.messagePanel.messageTextView resignFirstResponder];
    
    if(self.etat_menu == _PJ){
        self.etat_menu = NewMessageWrite;
    }
    else{
        self.etat_menu = _PJ;
    }
    [self redimensionneHeaderAvecTaille];
}

- (float)recupererTailleActuelle {
    
    float taille = menuTaille;
    
    switch (self.etat_menu) {
        case NewMessage:
            taille = _headerTaille.messageInit;
            break;
        case NewMessageWrite:
            taille = _headerTaille.messageWrite;
            break;
        case _PJ:
            taille = _headerTaille.pj;
            break;
        default:
            break;
    }
    
    return taille;
}

//redimensionne la taille du header arbitrairement
- (void)redimensionneHeaderAvecTaille{
    float taille = [self recupererTailleActuelle];

        
    DLog(@"redimensionne vue à : %f", taille);
    
    
    UIView *cell = [self contentView];
    CGRect frame = cell.frame;
    int paper_height = 114;
    
    //Animation du papier d'arrière plan
    //Transition depuis un touch sur le textview, agrandissement du background
    if(frame.size.height == _headerTaille.messageInit && taille == _headerTaille.messageWrite){
//        NSLayoutConstraint * constraint = self.paper_background.constraints[0];
        paper_height = 114;
        
    }
    //Transition depuis un touch autre part, pour remettre le background à sa taille initiale
    else if(taille == _headerTaille.messageInit){
//        NSLayoutConstraint * constraint = self.paper_background.constraints[0];
        paper_height = 40;
    }
    
    frame.size.height = taille;
    
    //Force le tableview a recalculer et animer l'offset des messages
    [self.delegate beginUpdates];
    [self.delegate endUpdates];

    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{

        self.paper_background_height_constraint.constant = paper_height;
        [cell setFrame:frame];
//        [self layoutIfNeeded];
//        [cell layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.delegate setNeedsLayout];
        [self.delegate layoutIfNeeded];
    }];
}

@end
