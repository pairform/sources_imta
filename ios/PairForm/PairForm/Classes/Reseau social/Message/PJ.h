//
//  PJ.h
//  PairForm
//
//  Created by vincent on 22/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

typedef enum {
    Photo,
    Video,
    Fichier,
    Son
} PJType;

@interface PJ : NSObject

+(UIImage*)imageSonParDefault;
+(UIImage*)imageVideoParDefault;
+(UIImage*)imagePhotoParDefault;
+(UIImage*)imageFichierParDefault;

+(long)tailleTotalMaxPJ;
+(int)nombreTotalMaxPJ;
+(NSArray*)formatAuthoriseVideo;
+(NSArray*)formatAuthorisePhoto;
+(NSArray*)formatAuthoriseFichier;
+(NSArray*)formatAuthoriseSon;
+(PJType)estDuFormat:(NSString*)extension;
+(BOOL)formatEstAuthorise:(PJType)typePJ format:(NSString*)format;
+ (NSString *)mimeTypePourExtension:(NSString *)_extension;

+(UIImage*)thumbnailSynchroneAPartirDeVideoUrl:(NSURL*)url;

+(void)imageAsynchroneAPartirDeVideoUrl:(NSURL*)url WithHandler:(void (^)(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error)) handler;

+ (NSData*)renvoieDataPourThumbnailDeLextension:(NSString*) extension EtDesData:(NSData*)data;
+ (AVAssetExportSession*)convertirVideoEnMp4AvecURL:(NSURL*)inputURL
                                                      handler:(void (^)(AVAssetExportSession*))callback;

@end
