//
//  RecupererMessagesViewController.m
//  SupCast
//
//  Created by fgutie10 on 28/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//


#import "UtilsCore.h"
#import "MessageController.h"
//#import "NouveauMessageController.h"
#import "ProfilController.h"
#import "UACellBackgroundView.h"
#import "UIMenuItem+CXAImageSupport.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LanguageManager.h"
#import "PJMessage.h"
#import "MessageCell.h"
#import "UIButton+VerticalLayout.h"

#define INTERESTING_TAG_NAMES @"user", @"commentaire", @"id", @"supprime", @"votes", nil
//Utilisation de timeIntervalSince1970 pour récupérer le nombre de secondes correspondant à la date.
//Problème : lors de modification de reseau la valeur peut être diminuer à cause de modif de l'heure
//Ce problème ne se pose pas pour nous car on l'utilise quand mode hors réseau !! OUF
#define TIMESTAMP [[NSDate date] timeIntervalSince1970]

@interface MessageController()

@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeLeft;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeRight;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *singleTapGesture;
@property (strong, nonatomic) NSMutableArray * arrayParentsDeReponsesOuvertes;
@property BOOL boolAfficheReponse;

@property (weak, nonatomic) IBOutlet UILabel *labelPasDeMsg;
@property (weak, nonatomic) IBOutlet UILabel *labelPasDeContenu;
@property (weak, nonatomic) IBOutlet UILabel *labelEcrireMsg;

//variables contenant les messages
//les messages affichés à l'écran potentiellement triés/filtrés
@property (strong, nonatomic) NSMutableArray *messages;
//les messages uniquement géolocalisés
@property (strong, nonatomic) NSMutableArray *messagesGeolocalises;
//les messages avant qu'ils ne soient géolocalisés (pour filtre, recherche...)
@property (strong, nonatomic) NSMutableArray *messagesAvantGeolocalisation;

//tous les messages
@property (strong, nonatomic) NSMutableArray *messages_fixe;

@property (strong, nonatomic) NSArray * filterResults;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *langueValuesString;
@property(nonatomic,strong) NSString * langueMsg;

@property (nonatomic) int id_message ;
@property (nonatomic ) NSIndexPath *indexPathParent;


//geolocalisation
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *positionCourante;
//variable permettant de savoir si la geolocalisation est demandée par l'envoie de message ou part la vue
//variable permmetant de savoir si une geolocalisation est pas deja en cours
@property (nonatomic) BOOL demandeEnvoie, geolocalisationEnCourt;

@property (strong, nonatomic) MenuMessageHeaderCell * menuMessageHeaderCell;

@property (strong, nonatomic) MessageHeaderCell * messageHeaderCell;

@property (strong, nonatomic) CarteHeaderCell * carteHeaderCell;

@property (strong, nonatomic) UITableViewCell * rechercheHeaderCell;

@property (nonatomic, strong) RMPickerViewController * pickerEditExisistingMessageLanguage;
@property (nonatomic, strong) RMPickerViewController * pickerNewMessageLanguage;

@property (nonatomic) int tmp;

@property NSArray * cercleArrayTemp;

@end

@implementation MessageController


//geolocalisation
@synthesize locationManager;
@synthesize positionCourante;
@synthesize demandeEnvoie, geolocalisationEnCourt;

@synthesize idCapsule;
@synthesize urlPage;
@synthesize tableView;
@synthesize currentCapsule;
@synthesize modeNouveauMessage;
@synthesize modeMessage;
@synthesize modeTri;

@synthesize swipeLeft;
@synthesize swipeRight;
@synthesize tapGesture;
@synthesize singleTapGesture;
@synthesize arrayParentsDeReponsesOuvertes;
@synthesize tag;
@synthesize num_occurence;
@synthesize boolAfficheReponse;
@synthesize id_utilisateur;
@synthesize id_utilisateurs;
@synthesize tag_message;


@synthesize filtre_actif;
@synthesize cercleArrayTemp;

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    
    return YES;
}




#pragma mark -

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    
    //    if (modeMessage != ModeMessageTransversal) {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshOneMessage:)
                                                 name:@"messageActualise"
                                               object:nil];
    //    }
    
    // On intercepte la notification des messages non-actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelRefreshMessages)
                                                 name:@"messagesErreur"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuDidHide)
                                                 name:@"UIMenuControllerWillHideMenuNotification"
                                               object:nil];
    
    if (self.modeHeader == ModeHeaderMessages && self.messageHeaderCell) {
        switch (self.messageHeaderCell.etat_menu) {
            case NewMessage:
//                [self.messageHeaderCell.messagePanel.messageTextView becomeFirstResponder];
                [self.messageHeaderCell redimensionneHeaderAvecTaille];
                break;
            case NewMessageWrite:
                [self.messageHeaderCell.messagePanel.messageTextView becomeFirstResponder];
                [self.messageHeaderCell redimensionneHeaderAvecTaille];
                break;
            case _PJ:
//                [self.messageHeaderCell.messagePanel.messageTextView becomeFirstResponder];
                break;
            default:
                break;
        }
        

    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    //    if (modeMessage != ModeMessageTransversal) {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshOneMessage:)
                                                 name:@"messageActualise"
                                               object:nil];
    //    }
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesErreur"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"UIMenuControllerWillHideMenuNotification"
                                                  object:nil];
}

- (void)viewDidLoad {
    DLog(@"");
    [super viewDidLoad];
    //    [TestFlight passCheckpoint:@"Lecture des messages"];
    boolAfficheReponse = NO;
    
    //Important : mettre le mode d'ecriture à aucun;
    [self setModeNouveauMessage:ModeNouveauMessageNone];
    
    idCapsule = currentCapsule.id_capsule;
    arrayParentsDeReponsesOuvertes = [[NSMutableArray alloc] init];
    filtre_actif = FiltreAucun;
    // Recuperation du nom de la page.
    if ( self.urlPage != nil ){
        urlPage = [UtilsCore trimPageUrlForLocalDB:urlPage];
    }else{
        urlPage =@"";
    }
    
    //Pull to refresh pour l'actualisation des messages
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[LanguageManager get:@"button_recuperer_nouveaux_messages"]];
    refreshControl.tintColor = [UIColor colorWithRed:0.447 green:0.49 blue:0.969 alpha:1]; /*#727df7*/
    [refreshControl addTarget:self action:@selector(rafraichirMessages) forControlEvents:UIControlEventValueChanged];
    refreshControl.tag = 150;
    [tableView addSubview:refreshControl];
    
    
    [self.view makeToastActivity:@"center"];
    
    //    modeNouveauMessage = ModeNouveauMessageNormal;
    [singleTapGesture requireGestureRecognizerToFail:tapGesture];
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];
    
    // Recuperation des langues dans le fichier PairForm-Arrays.plist
    //    self.langueValuesString = [LanguageManager getArrayTrueNameLanguage];
    self.langueValuesString = [LanguageManager displayLanguagesArrayForQuery];
    
    [self setContent];
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
    
    if (IS_IPAD)
    {
        [self setStackWidth: 320];
    }
    
    //Mode recherche par défaut pour mode transversal
    if (modeMessage == ModeMessageTransversal || modeMessage == ModeMessageLatest) {
        self.modeHeader = ModeHeaderRecherche;
    }
    
    
    //Initialisation des nouveaux messages
    if ((modeMessage != ModeMessageTransversal) && (modeMessage != ModeMessageLatest)) {
        self.modeTri = ModeTriVote;
        [self rafraichirMessages];
        [self initNouveauMessage];
        
        [self highlightMessageFromTrans];
    }
    else{
        self.modeTri = ModeTriDate;
    }
    
    //initialisation du booléen de geolocalisation
    _boolGeolocalisation = [[UIViewController getPreference:@"geolocalisation"] boolValue];
    positionCourante = nil;
    demandeEnvoie = NO;
    geolocalisationEnCourt = NO;
    _messagesGeolocalises = [NSMutableArray array];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    /*if(_boolGeolocalisation && positionCourante == nil)
     [self commencerGeolocalisation];
     */
    
    //Nouvelle méthode depuis iOS 8 pour le calcul de la hauteur des cells
    //Il faut donner un indice de hauteur, mais laisser iOS calculer la hauteur par rapport au contenu
    //http://stackoverflow.com/a/29763200/1437016
    [[self tableView] setSectionHeaderHeight:UITableViewAutomaticDimension];
    [[self tableView] setEstimatedSectionHeaderHeight:94];
}

-(void)highlightMessageFromTrans:(NSNumber*)id {
    
    if (id) {
        __block NSUInteger index_of_message_trans = -1;
        
        
        //        NSNumber * id_parent = [[UtilsCore getMessage:_id_message_trans] id_message_parent];
        //        if (![id_parent isEqualToNumber:@0]) {
        //            __block NSUInteger index_of_parent_message = -1;
        //
        //            //Sélection du message parent
        //            [_messages enumerateObjectsUsingBlock:^(Message * obj, NSUInteger idx, BOOL *stop) {
        //                if ([obj.id_message isEqualToNumber:id_parent]) {
        //                    index_of_parent_message = idx;
        //                    _idMessageOriginal = obj.id_message;
        //                    //Et on arrête l'itération
        //                    stop = YES;
        //                }
        //            }];
        //
        //            if (index_of_parent_message != -1 || index_of_message_trans != NSNotFound) {
        //                //On référence le parent comme étant ouvert
        //                [arrayParentsDeReponsesOuvertes addObject:id_parent];
        //                [self AfficheReponsesAtIndexPath:[NSIndexPath indexPathForRow:index_of_parent_message inSection:0]];
        //            }
        //
        //        }
        
        [_messages enumerateObjectsUsingBlock:^(Message * obj, NSUInteger idx, BOOL *stop) {
            if ([obj.id_message isEqualToNumber:id]) {
                //On récupère l'index
                index_of_message_trans = idx;
                //Et on arrête l'itération
                stop = YES;
            }
        }];
        
        if (index_of_message_trans != -1) {
            NSIndexPath * index_path = [NSIndexPath indexPathForRow:index_of_message_trans inSection:0];
            
            [self.tableView scrollToRowAtIndexPath: index_path atScrollPosition:UITableViewScrollPositionTop animated:YES];
            //        [self.tableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
            MessageCell * cell = (MessageCell*)[self.tableView cellForRowAtIndexPath:index_path];
            //        [UIView animateWithDuration:2.0 animations:^{
            //            [cell.contentView setTransform:CGAffineTransform];
            //        }];
            //        UIView * separator = [cell viewWithTag:20];
            //        [separator setBackgroundColor:[UIColor orangeColor]];
            
            CALayer *tintLayer = [CALayer layer];
            
            // Center the layer on the view
            [tintLayer setBounds:[cell.contentView frame]];
            [tintLayer setPosition:CGPointMake([cell.contentView frame].size.width/2.0,
                                               [cell.contentView frame].size.height/2.0)];
            
            // Fill the color
            [tintLayer setBackgroundColor:
             //         [[UIColor colorWithRed:0.5 green:0.5 blue:0.0 alpha:1.0] CGColor]];
             [[UIColor orangeColor] CGColor]];
            
            [tintLayer setOpacity:0.5];
            [[cell.contentView layer] addSublayer:tintLayer];
            [cell setClipsToBounds:YES];
            
            // Add the animation
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
            [animation setFromValue:[NSNumber numberWithFloat:0.5]];
            [animation setToValue:[NSNumber numberWithFloat:0.0]];
            //        [animation setFillMode:kCAFillModeRemoved];
            // Animate back to the starting value automatically
            //        [animation setAutoreverses:YES];
            // Animate over the course of 5 seconds.
            [animation setDuration:2.0];
            [animation setRemovedOnCompletion:NO];
            [tintLayer addAnimation:animation forKey:@"opacity"];
            
            dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 1.9);
            dispatch_after(delay, dispatch_get_main_queue(), ^(void){
                
                [tintLayer removeFromSuperlayer];
            });
            
        }
        
    }
    
    
}

-(void)highlightMessageFromTrans{
    [self highlightMessageFromTrans:_id_message_trans];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIBezierPath * clippingPath = [UIBezierPath bezierPathWithRect:self.tableView.frame];
    [self.sc_stackViewController setTouchRefusalArea:clippingPath];
    
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    
    if ( (modeMessage != ModeMessageTransversal) && (modeMessage != ModeMessageLatest))
    {
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"action"
                                                              action:@"message_seen"
                                                               label:[NSString stringWithFormat:@"%@ - %@ : messages vus", currentCapsule.nom_court, urlPage]
                                                               value:nil] build]];
        
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Messages : %@ - %@", currentCapsule.nom_court, urlPage]];
    }
    else{
        
        [tracker set:kGAIScreenName value:@"Messages : transversal"];
    }
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //    // navigation button bar
    //    NSMutableArray *items = [_uiNavigationItem.rightBarButtonItems mutableCopy];
    //    if (IS_IPHONE) {
    //        if ( items.count == 1){
    //            if ( modeMessage != ModeMessageTransversal){
    //                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    //                [a1 setFrame:CGRectMake(5,6,32,32)];
    //                [a1 addTarget:self action:@selector(ecrireMessageDepuisTopBar) forControlEvents:UIControlEventTouchUpInside];
    //                [a1 setImage:[UIImage imageNamed:@"pencil_64"] forState:UIControlStateNormal];
    //                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
    //                [items addObject:bNouveau];
    //                _uiNavigationItem.rightBarButtonItems = items;
    //            }
    //            else{
    //                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    //                [a1 setFrame:CGRectMake(5,6,32,32)];
    //                [a1 addTarget:self action:@selector(afficherFiltres) forControlEvents:UIControlEventTouchUpInside];
    //                [a1 setImage:[UIImage imageNamed:@"magnifier_64"] forState:UIControlStateNormal];
    //                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
    //                [items addObject:bNouveau];
    //                _uiNavigationItem.rightBarButtonItems = items;
    //            }
    //        }
    //    }
    //
    //    if (IS_IPAD) {
    //        if ( items.count == 0){
    //            items = [[NSMutableArray alloc] init];
    //            if ( modeMessage != ModeMessageTransversal){
    //                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    //                [a1 setFrame:CGRectMake(5,6,32,32)];
    //                [a1 addTarget:self action:@selector(ecrireMessageDepuisTopBar) forControlEvents:UIControlEventTouchUpInside];
    //                [a1 setImage:[UIImage imageNamed:@"pencil_64"] forState:UIControlStateNormal];
    //                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
    //                [items addObject:bNouveau];
    //                _uiNavigationItem.rightBarButtonItems = items;
    //            }
    //            else{
    //                UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    //                [a1 setFrame:CGRectMake(5,6,32,32)];
    //                [a1 addTarget:self action:@selector(afficherFiltres) forControlEvents:UIControlEventTouchUpInside];
    //                [a1 setImage:[UIImage imageNamed:@"magnifier_64"] forState:UIControlStateNormal];
    //                UIBarButtonItem *bNouveau = [[UIBarButtonItem alloc] initWithCustomView:a1];
    //                [items addObject:bNouveau];
    //                _uiNavigationItem.rightBarButtonItems = items;
    //            }
    //        }
    //    }
    
    [self refreshMessages];
}

-(void) setContent {
    self.title = [LanguageManager get:@"title_messages"];
    self.labelPasDeMsg.text = [LanguageManager get:@"label_liste_messages_vide"];
    self.labelPasDeContenu.text = [LanguageManager get:@"ios_label_element_pas_de_contenu"];
    self.labelEcrireMsg.text = [LanguageManager get:@"ios_label_ecrire_maintenant"];
    


}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

-(void)rafraichirMessages{
    DLog(@"");
    
    //Si on est sur un grain
    if (tag && num_occurence)
        [UtilsCore rafraichirMessagesFromCapsule:idCapsule nom_page:urlPage nom_tag:tag num_occurence:num_occurence];
    //Si on est sur une page
    else if (idCapsule && urlPage)
        [UtilsCore rafraichirMessagesFromCapsule:idCapsule nom_page:urlPage];
    //Si on est sur une ressource
    else if (idCapsule)
        [UtilsCore rafraichirMessagesFromCapsule:idCapsule];
    //Si on est sur aucun de ces cas
    else{
        [UtilsCore rafraichirMessages];
    }
}
-(void)ecrireMessageDepuisTopBar{
    _selectedMessage = nil;
    [self ecrireMessage];
}

-(void)ecrireMessage{
    if ([self checkForLoggedInUser])
        [self performSegueWithIdentifier: @"nouveauMessageSegue" sender: self];
}

#pragma mark - Refresh

-(void)cancelRefreshMessages{
    UIRefreshControl * refresh_control = (UIRefreshControl*)[tableView viewWithTag:150];
    if (refresh_control) {
        [refresh_control endRefreshing];
    }
    
}
NSInteger sortMessageParDate(Message* message1, Message* message2, void *context)
{
    return [message2.date_creation compare:message1.date_creation];
}


NSInteger sortMessageParVote(Message* message1, Message* message2, void *context)
{
    if (context)
        return [message1.somme_votes compare:message2.somme_votes];
    else
        return [message2.somme_votes compare:message1.somme_votes];
}

-(NSMutableArray*) sortMessagesOfArray:(NSArray*)array withType:(int)sortType{
    __block NSMutableArray * array_messages = [[NSMutableArray alloc] init];
    __block NSMutableArray * array_reponses = [[NSMutableArray alloc] init];
    __block NSMutableArray * array_final = [[NSMutableArray alloc] init];
    
    
    
    if (self.modeMessage == ModeMessageNormal && self.modeHeader != ModeHeaderCarte) {
        
        [array enumerateObjectsUsingBlock:^(Message* message, NSUInteger idx, BOOL *stop) {
            if (message.id_message_parent && ![message.id_message_parent isEqualToNumber:@0]) {
                [array_reponses addObject:message];
            }
            else{
                [array_messages addObject:message];
            }
        }];
        
        //Tri par date des deux tableaux
        [array_messages sortUsingFunction:sortMessageParDate context:NULL];
        [array_reponses sortUsingFunction:sortMessageParDate context:NULL];
        
        if(sortType == ModeTriVote){
            //Tri par vote des deux tableaux
            [array_messages sortUsingFunction:sortMessageParVote context:NULL];
            //Contexte renseigné pour inverser le sens de tri :
            //Les messages les plus votés doivent être pushés en dernier, puisqu'on ajoute
            //les réponses à l'index suivant le parent
            [array_reponses sortUsingFunction:sortMessageParVote context:YES];
        }
        
        array_final = [array_messages mutableCopy];
        
        __block NSMutableDictionary * objet_messages = [[NSMutableDictionary alloc] init];
        [array_messages enumerateObjectsUsingBlock:^(Message * message, NSUInteger idx, BOOL *stop) {
            [objet_messages setObject:message forKey:message.id_message];
        }];
        
        [array_reponses enumerateObjectsUsingBlock:^(Message * message, NSUInteger idx_reponse, BOOL *stop_reponse) {
            
            Message * message_parent = [objet_messages objectForKey:message.id_message_parent];
            NSUInteger index_of_parent = [array_final indexOfObject:message_parent];
            
            if (index_of_parent != NSNotFound) {
                [array_final insertObject:message atIndex:index_of_parent + 1];
            }
            //Sinon, on l'insère en bas par défaut...
            else{
                [array_final addObject:message];
            }
            
        }];
        
    }
    else{
        array_final = [array mutableCopy];
        //Tri par date des deux tableaux
        [array_final sortUsingFunction:sortMessageParDate context:NULL];
        if(sortType == ModeTriVote){
            //Tri par vote des deux tableaux
            [array_final sortUsingFunction:sortMessageParVote context:NULL];
        }
        
    }
    return [array_final mutableCopy];
}

-(void)refreshMessages{
    UIRefreshControl * refresh_control = (UIRefreshControl*)[tableView viewWithTag:150];
    if (refresh_control) {
        [refresh_control endRefreshing];
    }
    
    if ( boolAfficheReponse == YES ) {
        [tableView reloadData];
        boolAfficheReponse = NO;
        return;
    }
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // switch to a background thread and perform your expensive operation
     
        NSMutableArray * messagesAvantTraitement;
        // S'il on est sur une ressource et non sur une page
        switch (modeMessage) {
            case ModeMessageTransversal:
                if ( id_utilisateur ){
                    messagesAvantTraitement = [[UtilsCore getMessagesFromUser:id_utilisateur] mutableCopy];
                    break;
                }
                if ( id_utilisateurs ){
                    messagesAvantTraitement = [[UtilsCore getMessagesFromUsers:id_utilisateurs] mutableCopy];
                    break;
                }
                if ( tag_message ){
                    messagesAvantTraitement = [[UtilsCore getMessageFromTag:tag_message] mutableCopy];
                    break;
                }
                if ( idCapsule ){
                    messagesAvantTraitement = [[UtilsCore getMessagesFromCapsuleTrans:idCapsule] mutableCopy];
                    break;
                }
                messagesAvantTraitement = [[UtilsCore getAllMessages] mutableCopy];
                break;
                
            case ModeMessageLatest:
                if ( id_utilisateur ){
                    messagesAvantTraitement = [[UtilsCore getMessagesFromUser:id_utilisateur] mutableCopy];
                    break;
                }
                if ( id_utilisateurs ){
                    messagesAvantTraitement = [[UtilsCore getMessagesFromUsers:id_utilisateurs] mutableCopy];
                    break;
                }
                if ( tag_message ){
                    messagesAvantTraitement = [[UtilsCore getMessageFromTag:tag_message] mutableCopy];
                    break;
                }
                if ( idCapsule ){
                    messagesAvantTraitement = [[UtilsCore getMessagesFromCapsuleTrans:idCapsule] mutableCopy];
                    break;
                }
                messagesAvantTraitement = [[UtilsCore getAllMessages] mutableCopy];
                //            [messagesAvantTraitement removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSRangeFromString(@"")]];
                NSRange  messages_to_display = NSMakeRange(0, MIN(20, [messagesAvantTraitement count]));
                
                messagesAvantTraitement = [[messagesAvantTraitement objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:messages_to_display]] mutableCopy];
                break;
                
            default:{
                if ( [self.urlPage isEqualToString:@""] ){
                    messagesAvantTraitement = [[UtilsCore getMessagesFromCapsule:[NSNumber numberWithInt:[self.idCapsule intValue]] ] mutableCopy];
                }
                else
                {
                    //On teste si l'url est trimée ou pas
                    self.urlPage = [UtilsCore trimPageUrlForLocalDB:self.urlPage];
                    if ( tag && num_occurence){
                        messagesAvantTraitement = [[UtilsCore getMessagesFromCapsule:[NSNumber numberWithInt:[self.idCapsule intValue]] nom_page:self.urlPage tag:tag num_occurence:[num_occurence stringValue]] mutableCopy];
                    }
                    else{
                        messagesAvantTraitement = [[UtilsCore getMessagesFromCapsule:[NSNumber numberWithInt:[self.idCapsule intValue]] nom_page:self.urlPage] mutableCopy];
                    }
                }
                
            }
        }
        
        /*int rank = [[[[UIViewController getSessionValueForKey:@"rank"] objectForKey:[self.idCapsule stringValue]] objectForKey:@"id_categorie"] intValue];
         int idUser = [[UIViewController getSessionValueForKey:@"id_utilisateur"] intValue];
         
         // messages supprimé
         for (Message * message in _messages) {
         if(([message.supprime_par intValue] != 0) && ([message.supprime_par intValue] != idUser) && (rank < 3))
         {
         [messagesAvantTraitement removeObject:message];
         }
         }*/
        
        _messages_fixe = [self sortMessagesOfArray:messagesAvantTraitement withType:self.modeTri];
        
        _messages = [_messages_fixe mutableCopy];
        
        [self filtrerMessageGeolocalise];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //Si on est en mode carte alors on doit afficher uniquement les messages geolocalise
            if(self.modeHeader == ModeHeaderCarte){
                [self afficheQueLesMessagesGeolocalise];
                [self.carteHeaderCell.cartePanel mettreAJourCarte];
                [self.carteHeaderCell.cartePanel rechercheLesMessageSelonCoordonnee];
            }
            
            [self gereAffichageFooter];
            
            //Si on est en train d'écrire un message (focus sur le textview || texte dans textview), le rechargement du tableview est trop relou, et fait perdre le message.
            
            UITextView *message = self.messageHeaderCell.messagePanel.messageTextView;
            
            if (message && ([message isFirstResponder] || ![[message text] isEqualToString:[LanguageManager get:@"ios_label_message_placeholder"]])) {
                //TODO: trouver une solution stylée pour update le tableview sans ruiner le message
            }
            //Sinon, c'est cool
            else{
                NSString *sourceString = [[NSThread callStackSymbols] objectAtIndex:1];
                // Example: 1   UIKit                               0x00540c89 -[UIApplication _callInitializationDelegatesForURL:payload:suspended:] + 1163
                NSCharacterSet *separatorSet = [NSCharacterSet characterSetWithCharactersInString:@" -[]+?.,"];
                NSMutableArray *array = [NSMutableArray arrayWithArray:[sourceString  componentsSeparatedByCharactersInSet:separatorSet]];
                [array removeObject:@""];
                
                DLog(@"####### Appellé par %@", [array objectAtIndex:4]);
                
                if (IS_IPHONE)
                    [tableView reloadData];
                else
                    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
            }
            
            [self.view hideToastActivity];
        });
    });
    
        //    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    
}

-(void) refreshOneMessage:(NSNotification *)n {
    NSUInteger row = [n.userInfo[@"index_tableview"] integerValue];
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    
    //Sécurité : sur iPad, les derniers messages (qui sont toujours affichés)
    //réagissent aussi à cette notification : s'il y a plus de message affiché dans le message controller
    //du grain que dans celui des derniers messages (ex: 24 vs 20), on va essayer d'updater la row à un index
    //dépassant le nombre de messages dans ce dernier (ex: update row 23 alors qu'il n'y en a que 20)
    //On peut pas bloquer filtrer par rapport à qui a activé la notification, donc quick fix
    if (row >= [_messages count]) {
        return;
    }
    [self enleverAnimationUtiliteAtIndexPath:rowToReload];
    
    [tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark Table View Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    DLog(@"");
    return 1;
}

//header
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    DLog(@"");
    //Instanciation et retain du Menu header
    if(self.menuMessageHeaderCell == nil || ![self.menuMessageHeaderCell.reuseIdentifier isEqualToString:@"MenuMessageHeader"]){
        self.menuMessageHeaderCell = [self.tableView dequeueReusableCellWithIdentifier:@"MenuMessageHeader"];
    }

    UIView * menu_and_header = [[UIView alloc] init];
    UIView * header = [[UIView alloc] init];
    
    if ((modeMessage != ModeMessageTransversal) && (modeMessage != ModeMessageLatest) && (self.modeHeader == ModeHeaderMessages)) {
    
        //Pour conserver les paramètres actuels
        if(self.messageHeaderCell == nil || ![self.messageHeaderCell.reuseIdentifier isEqualToString:@"MessageHeader"]){
            self.messageHeaderCell = [self.tableView dequeueReusableCellWithIdentifier:@"MessageHeader"];
            //Permet de pouvoir ouvrir le picker pour les photos, ...
            self.messageHeaderCell.pjPanel.viewController = self;
            self.messageHeaderCell.etat_menu = NewMessage;
            self.messageHeaderCell.delegate = self.tableView;
        }
        
        //Init langueMsg
        _langueMsg = [LanguageManager getCodeMainLanguage];
        
        //Sécurité
        //S'il y a bien une langue définie pour la capsule
        if (self.currentCapsule.langue) {
            //On override la langue principale
            _langueMsg = [LanguageManager codeLangueWithid:[self.currentCapsule.langue intValue]];
        }
        // Modification du drapeau de la langue du
        // [self.messageHeaderCell.messagePanel.langueBouton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icone_drapeau_rond_%@.png", _langueMsg]] forState:UIControlStateNormal];
        
        // ATTENTION : instruction extrêmement importante
        
        // Ce longPressGestureRecognizer... Je ne sais pas d'où il sort, ni a quoi il sert, mais il n'était pas là avant je crois.
        // Peut-être qu'il sert à afficher un menu custom pour les TVC, comme on peut souvent voir.
        // Il est dans le contentView de la cellule, dont le target est la UITableViewCell
        // Hors, la Cell n'est retenu par aucun objet, puisqu'on ne retient que la contentview de cette dernière
        // Du coup, dès qu'on faisait un longPress sur la contentView, ce gesture recognizer était appellé.
        // Résultat, crash, EXC_BAD_ACCESS.
        
        // On vire donc tous les gestureRecognizers attachés au contentView
        [[self.messageHeaderCell contentView] setGestureRecognizers:@[]];
        // Cette ligne est bénie des dieux du code
        
        header = [self.messageHeaderCell contentView];
    }
    else if(self.modeHeader == ModeHeaderRecherche){
        
        //Pour conserver les paramètres actuels
        if(self.rechercheHeaderCell == nil || ![self.rechercheHeaderCell.reuseIdentifier isEqualToString:@"SearchHeader"]){
            self.rechercheHeaderCell = [self.tableView dequeueReusableCellWithIdentifier:@"SearchHeader"];
            _boutonFiltre = (UIButton*) [[self.rechercheHeaderCell contentView] viewWithTag:101];
        }
        
        //Si on est en mode transversal & filtré
        if (filtre_actif != FiltreAucun) {
            //On change la couleur du filtre en orange pour l'utilisateur
            [_boutonFiltre setImage:[UIImage imageNamed:@"Filtre_on"] forState:UIControlStateNormal];
            
            //Activation du bouton de recherche
            [self.menuMessageHeaderCell.bouton_menu_recherche setSelected:YES];
        }
        else{
            //On met la couleur blanche de filtre non activé
            [_boutonFiltre setImage:[UIImage imageNamed:@"Filtre_off"] forState:UIControlStateNormal];
            
            //On check également la searchbar, au cas où il y a une recherche en cours
            UISearchBar * searchBar = (UISearchBar*)[[self.rechercheHeaderCell contentView] viewWithTag:100];

            if (![[searchBar text] isEqualToString:@""]){
                [self.menuMessageHeaderCell.bouton_menu_recherche setSelected:YES];
            }
            else{
                [self.menuMessageHeaderCell.bouton_menu_recherche setSelected:NO];
            }
        }
        
        // Même problème qu'au dessus
        // On vire tous les gestureRecognizers attachés au contentView
        [[self.rechercheHeaderCell contentView] setGestureRecognizers:@[]];
        // Cette ligne est bénie des dieux du code
        
        header = [self.rechercheHeaderCell contentView];
    }
    else if(self.modeHeader == ModeHeaderCarte){
        
        if(self.carteHeaderCell == nil || ![self.carteHeaderCell.reuseIdentifier isEqualToString:@"CarteHeader"]){
            self.carteHeaderCell = [self.tableView dequeueReusableCellWithIdentifier:@"CarteHeader"];
            self.carteHeaderCell.cartePanel.delegate = self;
            [self afficheQueLesMessagesGeolocalise];
            [self.carteHeaderCell.cartePanel surOuverturePanel];
            
            // Même problème qu'au dessus
            // On vire tous les gestureRecognizers attachés au contentView
            [[self.messageHeaderCell contentView] setGestureRecognizers:@[]];

        }
        // Cette ligne est bénie des dieux du code
        
        header = [self.carteHeaderCell contentView];
    }
    //Défaut, on renvoie une cellule vide
    else{
        UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Defaut"];
        header = [cell contentView];
        
    }
    
    //Ajout du menu
    [menu_and_header addSubview:[self.menuMessageHeaderCell contentView]];
    // Modification de l'origin du header pour la stacker en dessous du menu
    CGRect frame = header.frame;
    frame.origin.y = 53;
    [header setFrame:frame];
    
    //Rajout du header
    [menu_and_header addSubview:header];
    
    //KVO pour garder en mémoire la hauteur de la cellule, pour éviter le recalcul
//    [menu_and_header setValue:<#(nullable id)#> forKey:<#(nonnull NSString *)#>]
//    [menu_and_header setClipsToBounds:YES];
    return menu_and_header;

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //    DLog(@"");
    
    const int taille_menu = 53;
    int taille_header = 0;
    
    if ((modeMessage != ModeMessageTransversal) && (modeMessage != ModeMessageLatest) && (self.modeHeader == ModeHeaderMessages)) {
        //gestion spécial du message qui peut s'agrandire si en mode écriture
        taille_header = self.messageHeaderCell ? [self.messageHeaderCell recupererTailleActuelle] : 44;
    }
    else if(self.modeHeader == ModeHeaderRecherche){
        taille_header =  44;
    }
    else if(self.modeHeader == ModeHeaderCarte){
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        float tailleMap = screenRect.size.height*60/100;
//        taille_header =  tailleMap;
        taille_header = 120;
    }
    
    return (taille_menu + taille_header);
}

//message
-(NSInteger)tableView:(UITableView *)aTableView
numberOfRowsInSection:(NSInteger)section {
    //    DLog(@"");
    if ( _messages == nil ) return 0;
    return _messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    DLog(@"");

    Message * message = [_messages objectAtIndex:(indexPath.row)];
    
    NSString *CellIdentifier = @"MessageCell";
    if ( message.id_message_parent != nil && ![message.id_message_parent isEqualToNumber:@0] && modeMessage == ModeMessageNormal ){
        CellIdentifier = @"ReponseCell";
    }
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell nettoyerCellule];
    
    [cell setTableView:self.tableView];
    cell.message = message;
    cell.viewController = self;
    cell.navigationController = self.navigationController;
    
    cell.pjTableau = [UtilsCore recuperePJDuMessage:message.id_message WithContext:nil];
    
    if(modeMessage == ModeMessageNormal)
        [cell initViewWithModeMessage: ModeMessageCellNormal];
    else
        [cell initViewWithModeMessage: ModeMessageCellTransversal];
    
    /*if([message.id_message integerValue] < 0){
        
        CALayer *tintLayer = [CALayer layer];
        
        // Center the layer on the view
        [tintLayer setBounds:[cell.contentView frame]];
        [tintLayer setPosition:CGPointMake([cell.contentView frame].size.width/2.0,
                                           [cell.contentView frame].size.height/2.0)];
        
        // Fill the color
        [tintLayer setBackgroundColor:
         //         [[UIColor colorWithRed:0.5 green:0.5 blue:0.0 alpha:1.0] CGColor]];
         [[UIColor orangeColor] CGColor]];
        
        [tintLayer setOpacity:0.2];
        [[cell.contentView layer] addSublayer:tintLayer];
        [cell setClipsToBounds:YES];
    }*/
    
//    cell.layer.shouldRasterize = YES;
//    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    DLog(@"");
    
    //Différents cas de figure :
    
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    
    
//    MessageCell *cell = (MessageCell*)[self tableView:aTableView cellForRowAtIndexPath:indexPath];
    
    
    NSNumber * hauteur_message_number = objc_getAssociatedObject(message, @"PFCellHeight");

    if(!hauteur_message_number){
        int hauteurMessage = 0;
        
        if([[UtilsCore recuperePJDuMessage:message.id_message WithContext:nil] count] > 0)
            self.tmp = 80;
        else
            self.tmp = 0;
       
        //S'il y a une médaille ou un défi, ou on est dans une lecture transversale
        if ((self.modeMessage == ModeMessageTransversal) || (modeMessage == ModeMessageLatest) || !([message.est_defi isEqualToNumber:@0]) || ([message.defi_valide isEqualToNumber:@1]) || (![message.medaille isEqualToString:@""])) {
            //On met une taille par défaut de 113
            hauteurMessage = 113;
        }
        else{
            //Sinon, on a une taille de 88
            //        hauteurMessage = 88;
            hauteurMessage = 88;
        }
        
        
        NSString *label =  message.contenu;
        if (!label) {
            //TODO:Vincent
            hauteurMessage += self.tmp;
            return hauteurMessage;
        }
        //Si le message est supprimé, on calcule la hauteur du label sur une chaine vide.
        if ( ![message.supprime_par isEqual: @0])
            label = @"";
        
        //Defaut : portrait
        int messageWidth = 238, messagePadTop = 32, messagePadBottom = 11;
        
        //Constante de largeur de messages
        if (IS_IPHONE && UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])){
            messageWidth = 400;
        }
        CGSize maximumLabelSize = CGSizeMake(messageWidth, CGFLOAT_MAX);
        //    CGSize stringSize = [label sizeWithFont:[UIFont fontWithName:@"Helvetica Neue" size:14] constrainedToSize:CGSizeMake(messageWidth, 9999)];
        CGRect stringRect = [label boundingRectWithSize:maximumLabelSize
                                                options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                             attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]}
                                                context:nil];
        
        //Pour chaque retour à la ligne, on rajoute de la hauteur à la chaine
        //    NSInteger numberOfNewLines = [[label componentsSeparatedByCharactersInSet:
        //                         [NSCharacterSet characterSetWithCharactersInString:@"\n\n" ]] count];
        //    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\n\n" options:NSRegularExpressionCaseInsensitive error:nil];
        //    NSUInteger numberOfNewLines = [regex numberOfMatchesInString:label options:0 range:NSMakeRange(0, [label length])];
        //
        //    if (numberOfNewLines) {
        //        stringRect.size.height += 20 * numberOfNewLines;
        //    }
        //Si le message fait plus de deux lignes
        if (stringRect.size.height > 30) {
            //La différence entre la taille totale prévue et la somme de la taille du message et du padding
            int diff = hauteurMessage - (stringRect.size.height + messagePadTop + messagePadBottom);
            //Si le message va déborder
            if(diff < 0)
            {
                //On rajoute cette différence à la hauteur totale
                hauteurMessage += -diff;
            }
            //Cas spécial : si on est en transversal
            if ((self.modeMessage == ModeMessageTransversal) || (modeMessage == ModeMessageLatest))
            {
                //On rajoute 30 de padding, à cause de l'icône de contexte
                hauteurMessage += 30;
            }
            else
                hauteurMessage += 20;
            
            //BLAME : Bricolage
            hauteurMessage *= 1.05;
        }
        
        if (![message.id_message_parent isEqualToNumber:@0]) {
            hauteurMessage += 5;
        }
        
        //S'il y a des tags
        if (message.getTags && ([message.getTags count] != 0)){
            //Hauteur du container des tags
            int tailleTag = 0;
            //On récupère la taille estimée
            
    //        tailleTag = [cell getHeightForTags:message];
            //Taille fixe car collectionviewtag est fixe maintenant
            tailleTag = 20;
            //On rajoute automatiquement la taille calculé des tags + une marge
            hauteurMessage += tailleTag;
        }
        
        //TODO:Vincent
        hauteurMessage += self.tmp;
        
        objc_setAssociatedObject(message, @"PFCellHeight", [NSNumber numberWithInt:hauteurMessage], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        return hauteurMessage;
    }
    else{
        return [hauteur_message_number intValue];
    }
    
    //    if(stringSize.height > 36)
    //        // 88 taille du reste du message ?
    //        return (stringSize.height  - 10/*21*/) + 113 + tailleTag ;
    //    else
    //        return 113 + tailleTag;
}

//Indispensable pour l'affichage du menuController
-(BOOL)canBecomeFirstResponder{
    return YES;
}

//Fonction cachant/Affichant le footer selon si des messages sont affichés
//De plus on change sa taille pour qu'il ne prennent pas de la scrollbar.
//Hauteur du footer : 380
-(void)gereAffichageFooter {
    
    //EN mode carte on n'affiche pas le footer
    if(self.modeHeader == ModeHeaderCarte)
        return;
    
    UIView *footer = tableView.tableFooterView;
    CGRect frame = footer.frame;
    
    if ( [_messages count] != 0 ){
        frame.size.height = 0;
        [tableView.tableFooterView setHidden:YES];
    }
    else{
        frame.size.height = 380; //573+
        [tableView.tableFooterView setHidden:NO];
    }
    
    
    [footer setFrame:frame];
    [tableView setTableFooterView:footer];
}

#pragma mark -
#pragma mark Actions


-(void)repondre:(id)sender{
    DLog(@"");
    
    modeNouveauMessage = ModeNouveauMessageReponse;
    Utilisateur * selected_message_user = [UtilsCore getUtilisateur:_selectedMessage.id_auteur];
    if (_selectedMessage.id_message) {
        _idMessageOriginal = _selectedMessage.id_message;
        
        UITextView *message = self.messageHeaderCell.messagePanel.messageTextView;
        [message setText:[NSString stringWithFormat:@"@%@ ", selected_message_user.owner_username ]];
        [message becomeFirstResponder];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_messages indexOfObject:_selectedMessage] inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    //    [self ecrireMessage];
}

-(void)editer:(id)sender{
    DLog(@"");
    modeNouveauMessage = ModeNouveauMessageEdition;
    [self ecrireMessage];
    /*
     [self editerMessage:[[self selectedMessage].id_message doubleValue] contenu:[self selectedMessage].value];*/
}

-(void)terminerDefi:(id)sender{
    DLog(@"");
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message", nil];
    [WService post:@"message/defi/terminer"
             param: query
          callback: ^(NSDictionary *wsReturn) {
              
              if (![WService displayErrors:wsReturn]){
                  [self.view makeToast:[LanguageManager get:@"ios_label_defi_termine"] duration:2 position:@"bottom" ];
              }
              // Enregistrement dans le icore , puis rechargement de la table
              
              [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInteger:_selectedMessagePath.row]];
              [tableView reloadData];
              
              
          }];
}

-(void)donnerMedaille:(id)sender{
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"title_attribuer_medaille"] delegate:self cancelButtonTitle:[LanguageManager get:@"button_annuler"] destructiveButtonTitle:nil  otherButtonTitles:[LanguageManager get:@"label_medaille_or"], [LanguageManager get:@"label_medaille_argent"],[LanguageManager get:@"label_medaille_bronze"],[LanguageManager get:@"label_supprimer_medaille"], nil];
    
    [popupQuery showInView:self.view];
    [popupQuery setTag:3];
}

- (IBAction)modifierLangueMessage:(id)sender {
    DLog(@"");
    [self.messageHeaderCell.messagePanel.messageTextView resignFirstResponder];
    
    [RMPickerViewController setLocalizedTitleForCancelButton:[LanguageManager get:@"button_annuler"]];
    [RMPickerViewController setLocalizedTitleForSelectButton:[LanguageManager get:@"ios_button_selectionner"]];
    RMPickerViewController *pickerVC = [RMPickerViewController pickerController];
    
    _pickerNewMessageLanguage = pickerVC;
    pickerVC.delegate = self;
    pickerVC.titleLabel.text = [LanguageManager get:@"label_langue_du_message"];
    
    if (IS_IPAD)
        [pickerVC showFromViewController:self];
    else
        [pickerVC show];
}

-(void)modifierLangueMessageExistant:(id)sender{
    [self.messageHeaderCell.messagePanel.messageTextView resignFirstResponder];
    [RMPickerViewController setLocalizedTitleForCancelButton:[LanguageManager get:@"button_annuler"]];
    [RMPickerViewController setLocalizedTitleForSelectButton:[LanguageManager get:@"ios_button_selectionner"]];
    
    RMPickerViewController *pickerVC = [RMPickerViewController pickerController];
    _pickerEditExisistingMessageLanguage = pickerVC;
    pickerVC.delegate = self;
    pickerVC.titleLabel.text = [LanguageManager get:@"label_langue_du_message"];
    
    if (IS_IPAD)
        [pickerVC showFromViewController:self];
    else
        [pickerVC show];
}


-(void)validerDefi:(id)sender{
    DLog(@"");
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message", nil];
    [WService post:@"message/defi/valider"
             param: query
          callback: ^(NSDictionary *wsReturn) {
              // Enregistrement dans le icore , puis rechargement de la table
              [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInteger:_selectedMessagePath.row]];
              [tableView reloadData];
          }];
}



-(void)supprimer:(id)sender{
    
    DLog(@"");
    NSString * rank = [[UIViewController getRankForCapsule:_selectedMessage.id_capsule] objectForKey:@"id_categorie"];
    NSString * idUser = [UIViewController getSessionValueForKey:@"id_utilisateur"];
    
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             self.selectedMessage.id_message, @"id_message",
                             self.selectedMessage.supprime_par, @"supprime_par",
                             self.selectedMessage.id_auteur, @"id_user_op",
                             self.selectedMessage.id_role_auteur, @"id_rank_user_op",
                             idUser, @"id_user_moderator",
                             rank, @"id_rank_user_moderator",
                             [self idCapsule], @"id_ressource", nil];
    
    [WService post:@"message/supprimer"
             param: params
          callback: ^(NSDictionary *wsReturn) {
              if ( ![WService displayErrors:wsReturn]){
                  [UtilsCore rafraichirMessagesFromMessageId:[self selectedMessage].id_message indexTableView:[NSNumber numberWithInteger:_selectedMessagePath.row]];
              }
          }];
    
}

- (void)ajouterTags:(id)sender {
    DLog(@"");
    UITextField * tagsField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    tagsField.placeholder = [LanguageManager get:@"ios_label_tags"];
    
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_ajouter_des_tags"] andTextFields:@[tagsField]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_ajouter"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message",[tagsField.text componentsSeparatedByString:@","] , @"tags", nil];
                              [WService post:@"message/tags"
                                       param: query
                                    callback: ^(NSDictionary *wsReturn) {
                                        if(![WService displayErrors:wsReturn]){
                                            //Invalidation de sa taille en cache
                                            objc_setAssociatedObject(_selectedMessage, @"PFCellHeight", nil, OBJC_ASSOCIATION_ASSIGN);
                                            [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInteger:_selectedMessagePath.row]];
//                                            [tableView reloadData];
                                        }
                                    }];
                          }];
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];
}

- (void)afficherInfos:(id)sender {
    DLog(@"");
    Message * message = _selectedMessage;
    NSMutableString * string = [[NSMutableString alloc] initWithString:@""];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:[LanguageManager get:@"ios_format_date_heure"]];
    
    NSString *time_created =[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[message.date_creation  intValue]]];
    
    [string appendString:[NSString stringWithFormat:@"%@ %@", [LanguageManager get:@"label_message_poste_le"], time_created]];
    
    if (![message.supprime_par isEqualToNumber:@0]) {
        //    [formatter setDateFormat:@"dd/MM/yy"];
        
        NSString *dateString=[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[message.date_modification  intValue]]];
        
        Utilisateur * supprimeur = [UtilsCore getUtilisateur:message.supprime_par];
        [string appendString:[NSString stringWithFormat:@"\r %@ %@ %@ %@", [LanguageManager get:@"ios_label_supprime_par"], supprimeur.owner_username, [LanguageManager get:@"ios_label_le"], dateString]];
    }
    
    // Infos sur la langue
    [string appendString:[NSString stringWithFormat:@"\r %@ : %@", [LanguageManager get:@"label_langue"], [LanguageManager nameLangueWithId:[message.id_langue intValue]]]];
    
    NSArray *tags = message.getTags;
    NSArray * tagsAuteurs = message.getTagsAuteurs;
    for (int i = 0; i < tags.count; i++) {
        NSString * auteur = [LanguageManager get:@"ios_label_inconnu"];
        if ( tagsAuteurs[i] != [NSNull null]  ) auteur = tagsAuteurs[i];
        [string appendString:[NSString stringWithFormat:@"\r %@%@ : %@\n",string,tags[i],auteur]];
    }
    
    //    if ([string isEqualToString:@""])
    //        [string setString:@"Pas d'informations disponibles."];
    //
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_informations"] andMessage:string];
    [alertView addButtonWithTitle:@"Ok"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    [alertView show];
}


-(void) copier{
    DLog(@"");
    [UIPasteboard generalPasteboard].string = _selectedMessage.contenu;
    NSValue * point = [NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, tableView.contentOffset.y + (self.view.frame.size.height  - 100))];
    
    [self.view makeToast:[LanguageManager get:@"ios_label_message_copie_presse_papier"] duration:2 position:point title:nil image:[UIImage imageNamed:@"copy_64"] style:nil completion:nil];
}

-(void) suivreLien{
    DLog(@"");
    
    //On regarde s'il y a des URLS dans le message
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:self.selectedMessage.contenu options:0 range:NSMakeRange(0, [self.selectedMessage.contenu length])];
    //S'il y en a
    
    switch ([matches count]) {
        case 0:
            //Aucune url, on sort
            break;
        case 1:
        {
            //Une url, on la suit direct
            [[UIApplication sharedApplication] openURL:[matches[0] URL]];
        }
            break;
        default:
        {
            //TODO: régler ce problème de nil object !!
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"ios_label_liens_message"]
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:nil];
            
            // ObjC Fast Enumeration
            for (NSTextCheckingResult *match in matches) {
                [actionSheet addButtonWithTitle:[[match URL] absoluteString]];
            }
            
            [actionSheet addButtonWithTitle:[LanguageManager get:@"button_annuler"]];
            actionSheet.cancelButtonIndex = [matches count];
            //Tag pour différencier les actions au niveau des méthodes déléguées
            [actionSheet setTag:4];
            [actionSheet showInView:self.view];
            //
            //            //Plusieurs urls, on pop ça
            //            for (NSTextCheckingResult *match in matches) {
            //                if ([match resultType] == NSTextCheckingTypeLink) {
            //                    NSURL *url = [match URL];
            //                    DLog(@"found URL: %@", url);
            //                }
            //            }
            
        }
            break;
    }
}

- (void)copublier:(id)sender  {
    
    DLog(@"");
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        Capsule * ressource = [UtilsCore getCapsule:_selectedMessage.id_capsule];
        
        NSString * initialText = [NSString stringWithFormat:@"#%@ #%@ >%@ : ",@"PairForm",ressource.nom_court,[UtilsCore getUtilisateur:_selectedMessage.id_auteur].owner_username];
        
        if (_selectedMessage.contenu.length + initialText.length > 140){
            //NSRange r = NSMakeRange(0, 140);
            //initialText = [initialText substringWithRange: r];
            [UIPasteboard generalPasteboard].string = _selectedMessage.contenu;
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Tweet" andMessage:[LanguageManager get:@"ios_label_message_trop_long"]];
            [alertView addButtonWithTitle:@"Ok"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            [alertView show];
        }else{
            initialText = [NSString stringWithFormat:@"%@ %@",initialText,_selectedMessage.contenu ];
        }
        [mySLComposerSheet setInitialText:initialText];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    DLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    DLog(@"Post Sucessful");
                    break;
                default:
                    break;
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
        
    }
}



-(IBAction)mapAffiche:(id)sender event:(id)event
{
    DLog(@"");
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:currentTouchPosition];
    if (indexPath != nil)
    {
        [self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
    }
}

#pragma mark -
#pragma mark Gesture recognizers

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    if (gestureRecognizer == swipeLeft || gestureRecognizer == swipeRight) {
        return YES;
    }
    else return NO;
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return NO;
}
#pragma mark -
#pragma mark Utilité
- (IBAction)swipeToLeft:(id)sender {
    // Prend la cellule
    CGPoint location = [swipeLeft locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    //S'il n'y a aucun élément à l'endroit swipé
    if (!indexPath)
        return;
    
    [self voter:@"false" messageAtIndexPath:indexPath];
    
}
- (IBAction)swipeToRight:(id)sender {
    
    // Prend la cellule
    CGPoint location = [swipeRight locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    //S'il n'y a aucun élément à l'endroit swipé
    if (!indexPath)
        return;
    
    [self voter:@"true" messageAtIndexPath:indexPath];
    
}

-(IBAction)voterPositif:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //Si le vote réussi (sans savoir le retour serveur)
    if([self voter:@"true" messageAtIndexPath:indexPath])
        //On indique le raccourci a l'utilisateur
        [[[UIApplication sharedApplication] keyWindow] makeToast:[LanguageManager get:@"label_raccourci_vote"]  duration:10 position:@"bottom" title:[LanguageManager get:@"label_astuce"] image:[UIImage imageNamed:@"bulb_off_48"] style:nil completion:nil];
}

-(IBAction)voterNegatif:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //Si le vote réussi (sans savoir le retour serveur)
    if([self voter:@"false" messageAtIndexPath:indexPath])
        //On indique le raccourci a l'utilisateur
        [[[UIApplication sharedApplication] keyWindow] makeToast:[LanguageManager get:@"label_raccourci_vote"]  duration:10 position:@"bottom" title:[LanguageManager get:@"label_astuce"] image:[UIImage imageNamed:@"bulb_off_48"] style:nil completion:nil];
}

- (BOOL)voter:(NSString *)valeur messageAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (![self checkForLoggedInUser])
    if (![UIViewController isConnected] || ![UIViewController estLieAuServeur])
        return false;
    
    //Prend la fleche dans la cellule
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UIImageView * image ;
    
    if ( [valeur isEqualToString:@"true"]){
        image = ( UIImageView *)[cell viewWithTag:8];
    }else{
        image = ( UIImageView *)[cell viewWithTag:6];
    }
    
    
    [self lanceAnimationUtilite:image];
    
    //Appel du webservice
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    if([message.supprime_par intValue] != 0){
        [self enleverAnimationUtiliteAtIndexPath:indexPath];
        return false;
    }
    //Vérification que l'utilisateur en train de voter n'est pas l'auteur du message
    if([message.id_auteur isEqualToNumber:[UIViewController getSessionValueForKey:@"id_utilisateur"]] || [message.id_auteur isEqualToNumber:@0]){
        [self enleverAnimationUtiliteAtIndexPath:indexPath];
        
        [[self view] makeToast:[LanguageManager get:@"label_pas_voter_votre_message" ] duration:4.0 position:@"bottom" title:[LanguageManager get:@"ios_title_erreur"] image:nil style:nil completion:nil];
        return false;
    }
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: valeur, @"vote", message.id_message , @"id_message", nil];
    [WService post:@"message/voter"
             param: query
          callback: ^(NSDictionary *wsReturn) {
              
              if ([cell viewWithTag:2000]) {
                  [self enleverAnimationUtiliteAtIndexPath:indexPath];
              }
              if ( [WService displayErrors:wsReturn]){
                  return;
              }
              // Enregistrement dans le icore , puis rechargement de la table
              [UtilsCore rafraichirMessagesFromMessageId:message.id_message indexTableView:[NSNumber numberWithInteger:indexPath.row]];
              //[tableView reloadData];
              
              NSString * content;
              
              if ((modeMessage == ModeMessageTransversal) || (modeMessage == ModeMessageLatest)) {
                  //              content = [UtilsCore getRessource:message.id_capsule].nom_court;
                  content = [message.id_capsule stringValue];
              }
              else{
                  //              content = currentCapsule.nom_court;
                  content = [currentCapsule.id_capsule stringValue];
              }
              
              int new_action = [valeur boolValue] ? 1 : -1;
              int old_action = [message.utilisateur_a_vote intValue];
              
              //S'il n'y a pas d'ancien vote, on donne 1 ; sinon, si on annule son action, on enlève 1.
              //Sinon, on donne rien
              int will_gain_points_int;
              
              if(!old_action)
                  will_gain_points_int = 1;
              else if(old_action == new_action)
                  will_gain_points_int = -1;
              else
                  will_gain_points_int = 0;
              
              //S'il y a quelque chose à notifier
              if (will_gain_points_int != 0) {
                  bool will_gain_points = will_gain_points_int < 0 ? false : true;
                  
                  [UtilsCore recordNotificationFromDictionary:@{@"iu" : [UIViewController getSessionValueForKey:@"id_utilisateur"],
                                                                @"t" : @"us",
                                                                @"aps" : @{@"alert":@"Vote sur un message"},
                                                                @"st": will_gain_points ? @"PTSEVALUERMESSAGE" : @"PTSDEVALUERMESSAGE",
                                                                @"c": content,
                                                                @"im": message.id_message}];
              }
              
          }];
    
    return true;
    
}



- (void)lanceAnimationUtilite:(UIImageView *)image{
    
    //    image.transform = CGAffineTransformMakeScale(1.6, 1.6);
    //    [UIView animateWithDuration:0.3
    //                          delay: 0.0
    //                        options: UIViewAnimationOptionCurveEaseIn
    //                     animations:^{
    //                         image.transform = CGAffineTransformIdentity;
    //                     }
    //                     completion:nil];
    CGFloat glowSpread = 30.0f;
    
    UIImage * glowImage = nil;
    
    CGSize imageSize = CGSizeMake(image.bounds.size.width + glowSpread, image.bounds.size.height + glowSpread);
    
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, [UIScreen mainScreen].scale);
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetAllowsAntialiasing(context, true);
        CGContextSetShouldAntialias(context, true);
        
        CGContextSaveGState(context);
        
        CGGradientRef gradient = [image tag] == 6 ? [self newGlowGradientWithColor:[UIColor redColor]] : [self newGlowGradientWithColor:[UIColor greenColor]];
        
        CGPoint gradCenter = CGPointMake(floorf(imageSize.width / 2.0f), floorf(imageSize.height / 2.0f));
        CGFloat gradRadius = MAX(imageSize.width, imageSize.height) / 2.0f;
        
        CGContextDrawRadialGradient(context, gradient, gradCenter, 0.0f, gradCenter, gradRadius, kCGGradientDrawsBeforeStartLocation);
        
        CGGradientRelease(gradient), gradient = NULL;
        CGContextRestoreGState(context);
        
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, glowSpread / 2.0f, glowSpread / 2.0f);
        
        [image.layer renderInContext:context];
        
        CGContextRestoreGState(context);
        
        glowImage = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    
    // Make the glowing view itself, and position it at the same
    // point as ourself. Overlay it over ourself.
    UIView *glowView = [[UIImageView alloc] initWithImage:glowImage];
    [glowView setTag:2000];
    
    glowView.center = image.center;
    [image.superview insertSubview:glowView aboveSubview:image];
    
    // We don't want to show the image, but rather a shadow created by
    // Core Animation. By setting the shadow to white and the shadow radius to
    // something large, we get a pleasing glow.
    glowView.alpha = 0.0f;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:
     ^{
         glowView.layer.opacity = 1.0f;
     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5
                                               delay:0.0
                                             options: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                                          animations:
                          ^{
                              glowView.layer.opacity = 0.5f;
                          }
                                          completion:nil];
                     }];
}

-(void)enleverAnimationUtiliteAtIndexPath:(NSIndexPath*) index{
    
    UIView * glowView = [[self.tableView cellForRowAtIndexPath:index] viewWithTag:2000];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseIn
                     animations:
     ^{
         glowView.layer.opacity = 0.0;
     }
                     completion:^(BOOL finished) {
                         
                         [glowView removeFromSuperview];
                         
                     }];
}

-(CGGradientRef)newGlowGradientWithColor:(UIColor*)color
{
    CGColorRef cgColor = [color CGColor];
    
    const CGFloat *sourceColorComponents = CGColorGetComponents(cgColor);
    
    CGFloat sourceRed;
    CGFloat sourceGreen;
    CGFloat sourceBlue;
    CGFloat sourceAlpha;
    if (CGColorGetNumberOfComponents(cgColor) == 2)
    {
        sourceRed = sourceColorComponents[0];
        sourceGreen = sourceColorComponents[0];
        sourceBlue = sourceColorComponents[0];
        sourceAlpha = sourceColorComponents[1];
    }
    else
    {
        sourceRed = sourceColorComponents[0];
        sourceGreen = sourceColorComponents[1];
        sourceBlue = sourceColorComponents[2];
        sourceAlpha = sourceColorComponents[3];
    }
    
    size_t locationsCount = 20;
    CGFloat step = 1.0f / locationsCount;
    
    CGFloat colorComponents[4 * locationsCount];
    CGFloat locations[locationsCount];
    
    NSUInteger componentsIndex = 0;
    for (NSUInteger index = 0; index < locationsCount; index++)
    {
        CGFloat point = index * step;
        locations[index] = point;
        
        CGFloat alpha = sourceAlpha * (1 - 0.5 * (1 - cos(point * M_PI)));
        
        colorComponents[componentsIndex] = sourceRed;
        colorComponents[componentsIndex + 1] = sourceGreen;
        colorComponents[componentsIndex + 2] = sourceBlue;
        colorComponents[componentsIndex + 3] = alpha;
        componentsIndex += 4;
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colorComponents, locations, locationsCount);
    
    CGColorSpaceRelease(colorSpace), colorSpace = NULL;
    
    return gradient;
}

#pragma mark -
#pragma mark Menu

- (IBAction)singleTapGesture:(id)sender {
    
    
    DLog(@"");
    
    CGPoint location = [singleTapGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    if (!indexPath) {
        return;
    }
    

    MessageCell *cell = (MessageCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    CGPoint pointInCell = [singleTapGesture locationInView:cell];
    CGRect collectionViewFrame = [cell.pjMessageCollectionView frame];
    
    //Annulation du touch si c'est dans la collection view des pieces jointes
    //Et si les pj sont affichées
    if (![cell.pjMessageCollectionView isHidden] && CGRectContainsPoint(collectionViewFrame, pointInCell)) {
        return;
    }
    
    [self becomeFirstResponder];
    
    //Si on clique pour quitter l'état d'écriture d'un message
    if (self.messageHeaderCell.etat_menu != NewMessage) {
        [self.messageHeaderCell setEtat_menu:NewMessage];
        [self.messageHeaderCell redimensionneHeaderAvecTaille];
        
        //On n'affiche pas le menu, parce qu'il voulait surement juste sortir de l'édition
        return;
    }

    /*NSInteger numberOfCells = [cell.pjMessageCollectionView numberOfItemsInSection:0];
    UICollectionViewLayout *layout = cell.pjMessageCollectionView.collectionViewLayout;
    for (NSInteger i = 0; i < numberOfCells; i++) {
        UICollectionViewLayoutAttributes *layoutAttributes = [layout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
        CGRect cellFrame = layoutAttributes.frame;
        
        if (CGRectContainsPoint(cellFrame, pointInCell)) {
            return;
        }
    }*/
    
    Message * message = [_messages objectAtIndex:(indexPath.row)];
    _selectedMessage = message;
    NSMutableArray * menuItems = [[NSMutableArray alloc] init];
    int idUser = [[UIViewController getSessionValueForKey:@"id_utilisateur"] intValue];
    int rank = [[[UIViewController getRankForCapsule:message.id_capsule] objectForKey:@"id_categorie"] intValue];
    
    
    if([message.supprime_par intValue] == 0){
        
        
        //Repondre
        if (( modeMessage != ModeMessageTransversal) && (modeMessage != ModeMessageLatest)){
            UIMenuItem *repondre = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_repondre"]
                                                                  action:@selector(repondre:)
                                                                   image:[[UIImage imageNamed:@"comment_readed_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                         bounds:CGSizeMake(32, 32)
                                                                                                                           interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:repondre];
        }
        
        //Retweeter
        UIMenuItem *retweeter = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_partager_message"]
                                                               action:@selector(copublier:)
                                                                image:[[UIImage imageNamed:@"Twitter_c"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                              bounds:CGSizeMake(32, 32)
                                                                                                                interpolationQuality:kCGInterpolationHigh]];
        //        UIMenuItem *retweeter = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_partager_message" ] action:@selector(copublier:)];
        
        [menuItems addObject:retweeter];
        
        //Copier
        UIMenuItem *copier = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"ios_button_copier"]
                                                            action:@selector(copier)
                                                             image:[[UIImage imageNamed:@"copy_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                         bounds:CGSizeMake(32, 32)
                                                                                                           interpolationQuality:kCGInterpolationHigh]];
        [menuItems addObject:copier];
        
        //On regarde s'il y a des URLS dans le message
        NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
        NSArray *matches = [linkDetector matchesInString:message.contenu options:0 range:NSMakeRange(0, [message.contenu length])];
        //S'il y en a
        
        if ([matches count]) {
            //Copier
            UIMenuItem *copier = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"ios_button_liens"]
                                                                action:@selector(suivreLien)
                                                                 image:[[UIImage imageNamed:@"link_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                             bounds:CGSizeMake(32, 32)
                                                                                                               interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:copier];
        }
        
        
        // Ses messages
        if (idUser == [message.id_auteur intValue])
        {
            //Edition
            //            UIMenuItem *editer = [[UIMenuItem alloc] cxa_initWithTitle:@"Editer" action:@selector(editer:) image:[UIImage imageNamed:@"pencil_64"]];
            //[menuItems addObject:editer];
            
            // si c'est un defi en cours
            //Terminer defi
            if ( [message.est_defi isEqualToNumber:@1]){
                UIMenuItem *terminerDefi = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_terminer_defi"] action:@selector(terminerDefi:)
                                                                           image:[[UIImage imageNamed:@"clock_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                        bounds:CGSizeMake(32, 32)
                                                                                                                          interpolationQuality:kCGInterpolationHigh]];
                [menuItems addObject:terminerDefi];
            }
            // Les messages des autres.
        }else{
            // Si expert ou admin
            //Donner Medaille
            if(rank >= 4){
                UIMenuItem *donnerMedaille = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_attribuer_medaille"]
                                                                            action:@selector(donnerMedaille:)
                                                                             image:[[UIImage imageNamed:@"medaille_or"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                             bounds:CGSizeMake(32, 32)
                                                                                                                               interpolationQuality:kCGInterpolationHigh]];
                [menuItems addObject:donnerMedaille];
            }
            // si reponse et reponse d'un defi. et expert ou admin ET pas encore validé
            //Valider defi
            if (![message.id_message_parent isEqualToNumber:@0] && ![((Message*)[UtilsCore getMessage:message.id_message_parent]).est_defi isEqualToNumber:@0] && rank >= 4 && [message.defi_valide isEqualToNumber:@0]){
                UIMenuItem *validerDefi = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_valider_reponse_defi"]
                                                                         action:@selector(validerDefi:)
                                                                          image:[[UIImage imageNamed:@"checkmark_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                           bounds:CGSizeMake(32, 32)
                                                                                                                             interpolationQuality:kCGInterpolationHigh]];
                [menuItems addObject:validerDefi];
            }
        }
        
        // Modification de la langue du message
        // Si emetteur du message ou si expert ou admin
        if (idUser == [message.id_auteur intValue] || rank >= 4) {
            UIMenuItem *modifierLangue = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_changer_langue_message"]
                                                                        action:@selector(modifierLangue:)
                                                                         image:[[UIImage imageNamed:@"icone_langue_blanc"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                                bounds:CGSizeMake(32, 32)
                                                                                                                                  interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:modifierLangue];
        }
        
        //Ajout de tags
        if((rank >= 2))
        {
            UIMenuItem *ajouterTags = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"ios_button_ajouter_des_tags"]
                                                                     action:@selector(ajouterTags:)
                                                                      image:[[UIImage imageNamed:@"tag_add_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                     bounds:CGSizeMake(32, 32)
                                                                                                                       interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:ajouterTags];
        }
        //Affichage des infos
        if((rank >= 4) || message.getTags)
        {
            UIMenuItem *afficherInfos = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_infos_message"]
                                                                       action:@selector(afficherInfos:)
                                                                        image:[[UIImage imageNamed:@"info_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                    bounds:CGSizeMake(32, 32)
                                                                                                                      interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:afficherInfos];
        }
        
    }
    
    
    
    
    // messages supprimés.
    
    //Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
    if(([message.id_auteur intValue] == idUser) || (rank >= 3))
    {
        
        UIMenuItem *supprimer;
        if([message.supprime_par intValue] == 0){
            supprimer = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_supprimer_message"]
                                                       action:@selector(supprimer:)
                                                        image:[[UIImage imageNamed:@"comment_delete_64"]
                                                               resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                               bounds:CGSizeMake(32, 32)
                                                               interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:supprimer];
        }
        // if ( rank >= [message.supprime_par intValue])
        else{
            supprimer = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_retablir_message"]
                                                       action:@selector(supprimer:)
                                                        image:[[UIImage imageNamed:@"comment_check_64"]
                                                               resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                               bounds:CGSizeMake(32, 32)
                                                               interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:supprimer];
            
            UIMenuItem *afficherInfos = [[UIMenuItem alloc] initWithTitle:[LanguageManager get:@"button_infos_message"]
                                                                       action:@selector(afficherInfos:)
                                                                        image:[[UIImage imageNamed:@"info_64"] resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                                                                                    bounds:CGSizeMake(32, 32)
                                                                                                                      interpolationQuality:kCGInterpolationHigh]];
            [menuItems addObject:afficherInfos];
        }
    }
    
    [self setSelectedMessage:[_messages objectAtIndex:indexPath.row]];
    [self setSelectedMessagePath:indexPath];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    
    [menu setMenuItems:menuItems];
    [menu setTargetRect:cell.frame inView: cell.superview];
    [menu setMenuVisible:YES animated:YES];
    
}

-(void)menuDidHide{
    //    [self setSelectedMessage:nil];
}
#pragma mark -
#pragma mark Reponses

- (IBAction)tapGesture:(id)sender {
    if ( modeMessage != ModeMessageNormal ) return;
    
    // Si on est sur une reponse , on cache les messages
    CGPoint location = [tapGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    if (!indexPath)
        return;
    
    Message * mes = [_messages objectAtIndex:indexPath.row];
    [self handleResponsesDisplay:mes];
}

-(void)handleResponsesDisplay:(Message*)selected_message{
    // si reponse
    //    if (![[(Message *) selected_message id_message_parent ] isEqualToNumber:@0] ) {
    //        [self cacherReponses:[selected_message id_message_parent]];
    //        for (Message *item in _messages){
    //            if ([item.id_message isEqualToNumber: selected_message.id_message_parent]  ){
    //                NSIndexPath * indexPathParent = [NSIndexPath indexPathForItem:[_messages indexOfObject:item]  inSection:0];
    //                [tableView scrollToRowAtIndexPath:indexPathParent atScrollPosition:UITableViewScrollPositionNone animated:YES];
    //                break;
    //            }
    //        }
    //        [arrayParentsDeReponsesOuvertes removeObject:selected_message.id_message_parent];
    //    }
    //    else{
    //        // si parent , et reponses deja ouvertes
    //        if ( [arrayParentsDeReponsesOuvertes containsObject:selected_message.id_message]){
    //            [arrayParentsDeReponsesOuvertes removeObject:selected_message.id_message];
    //            [self cacherReponses:selected_message.id_message];
    //        }else{
    //            [arrayParentsDeReponsesOuvertes addObject:selected_message.id_message];
    //            [self AfficheReponsesFromTapPoint];
    //        }
    //
    //    }
    
}

-(void)AfficheReponsesFromTapPoint{
    
    CGPoint location = [tapGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    if (!indexPath)
        return;
    //    [self AfficheReponsesAtIndexPath:indexPath];
    
}

-(NSUInteger)getAnswersLastIndexForParentMessage:(Message*) message_with_parent
{
    NSUInteger last_answer_index_path = 0;
    NSNumber* id_parent = message_with_parent.id_message;
    
    for (Message * message_to_find in _messages){
        if ([message_to_find.id_message_parent isEqualToNumber:id_parent]) {
            last_answer_index_path = [_messages indexOfObject:message_to_find];
        }
    }
    return last_answer_index_path;
    
}

-(NSUInteger)AfficheReponsesAtIndexPath:(NSIndexPath*)indexPath{
    
    
    Message * mes = [_messages objectAtIndex:indexPath.row];
    // Si on est sur un message deja ouvert
    //if ( _id_message == [mes.id_message integerValue]) return;
    _id_message = [mes.id_message intValue];
    _indexPathParent = indexPath;
    
    // Si on est deja sur une reponse , on ne fait rien
    if (![[ (Message *)[_messages objectAtIndex:indexPath.row ] id_message_parent ] isEqualToNumber:@0] ){
        return -1;
    }
    
    
    NSMutableArray * temp = [_messages mutableCopy];
    
    // On affiche les reponses
    NSArray * reponses = [UtilsCore getReponsesFromMessage:[NSNumber numberWithInt:_id_message]];
    
    if ( reponses != nil)
    {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        NSMutableArray * array_answers_index_path = [[NSMutableArray alloc] init];
        
        NSUInteger index = indexPath.row;
        for ( Message *reponse in reponses){
            ++index;
            
            [temp insertObject:reponse atIndex:index];
            
            [array_answers_index_path addObject:[NSIndexPath indexPathForRow:index inSection:0]];
            
            
        }
        
        _messages = temp;
        [tableView insertRowsAtIndexPaths:array_answers_index_path withRowAnimation:UITableViewRowAnimationTop];
        [self.tableView endUpdates];
        boolAfficheReponse = YES;
        
        //On renvoie l'index du dernier message
        return index;
    }
    
    return -1;
}

- (void) cacherReponses:(NSNumber *) id_parent{
    //On supprime les messages ouvert en reponses
    NSMutableArray * temp = [_messages mutableCopy];
    
    [tableView beginUpdates];
    
    for (Message *item in temp){
        if ([item.id_message isEqualToNumber:id_parent]) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:[_messages indexOfObject:item] inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        if ( [item.id_message_parent isEqualToNumber:id_parent])
        {
            NSUInteger index = [_messages indexOfObject:item];
            [_messages removeObject:item];
            
            NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [tableView deleteRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
            [tableView endUpdates];
        }
        
    }
    boolAfficheReponse = NO;
    
    
}

#pragma mark - Gestion des boutons de la HeaderView
-(IBAction)afficherCarte:(id)sender{

    self.modeHeader = ModeHeaderCarte;
    [self afficheQueLesMessagesGeolocalise];
    [self.carteHeaderCell.cartePanel surOuverturePanel];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewCellStyleDefault];
    [(UIButton*)sender setEnabled:YES];
    
    if (self.messageHeaderCell) {
        [self.messageHeaderCell setEtat_menu:NewMessage];
        [self.messageHeaderCell redimensionneHeaderAvecTaille];
    }
}
-(IBAction)afficherMessages:(id)sender{
    //Rétablissement des messages avant qu'ils ne soient géolocaliser
    if(_messagesAvantGeolocalisation){
        _messages = [_messagesAvantGeolocalisation mutableCopy];
        _messagesAvantGeolocalisation = nil;
    }
    
    if (self.modeMessage == ModeMessageNormal) {
        self.modeHeader = ModeHeaderMessages;
    }
    else{
        //Alerter l'utilisateur qu'il ne peut pas écrire en mode transversal
        [self.view makeToast:[LanguageManager get:@"message_mode_transversal"]];
    }
    
    //AHAHAH
//    [self gereAffichageFooter];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewCellStyleDefault];
    [(UIButton*)sender setEnabled:YES];
    
    if (self.messageHeaderCell) {
        [self.messageHeaderCell setEtat_menu:NewMessage];
        [self.messageHeaderCell redimensionneHeaderAvecTaille];
    }
}
-(IBAction)afficherRecherche:(id)sender{
    //Rétablissement des messages avant qu'ils ne soient géolocaliser
    if(_messagesAvantGeolocalisation){
        _messages = [_messagesAvantGeolocalisation mutableCopy];
        _messagesAvantGeolocalisation = nil;
    }
    
    //_messages = [_messages_fixe mutableCopy];
    self.modeHeader = ModeHeaderRecherche;
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewCellStyleDefault];
    [(UIButton*)sender setEnabled:YES];

    
    if (self.messageHeaderCell) {
        [self.messageHeaderCell setEtat_menu:NewMessage];
        [self.messageHeaderCell redimensionneHeaderAvecTaille];
    }
}

#pragma mark  Tri
-(IBAction)afficherTri:(id)sender{
    // Filtres : Utilisateur? , ressources , groupes? , tags
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"trier_message"]
                                                            delegate:self
                                                   cancelButtonTitle:[LanguageManager get:@"button_annuler"]
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:[LanguageManager get:@"trier_message_date"], [LanguageManager get:@"trier_message_vote"], nil];
    [popupQuery setTag:5];
    
    if (IS_IPHONE)
        [popupQuery showInView:self.view];
    else{
        UIButton * sender_button = (UIButton*)sender;
        [popupQuery showFromRect:CGRectMake(0, 0, sender_button.frame.size.width, sender_button.frame.size.height) inView:sender_button animated:YES];
        
    }
}

#pragma mark - Lecture Transversale , filtres
-(IBAction)afficherFiltres:(id)sender{
    // Filtres : Utilisateur? , ressources , groupes? , tags
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"title_filtrer_messages"] delegate:self cancelButtonTitle:[LanguageManager get:@"button_annuler"] destructiveButtonTitle:nil  otherButtonTitles:[LanguageManager get:@"button_ressource"], [LanguageManager get:@"button_utilisateur"],[LanguageManager get:@"button_tag"],[LanguageManager get:@"ios_button_cercle"],[LanguageManager get:@"ios_button_afficher_tous_messages"], nil];
    [popupQuery setTag:1];
    
    if (IS_IPHONE)
        [popupQuery showInView:self.view];
    else{
        UIButton * sender_button = (UIButton*)sender;
        [popupQuery showFromRect:CGRectMake(0, 0, sender_button.frame.size.width, sender_button.frame.size.height) inView:sender_button animated:YES];
        
    }
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    actionSheet.backgroundColor = [UIColor whiteColor];
    for (UIView *subview in actionSheet.subviews) {
        subview.backgroundColor = [UIColor whiteColor];
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        
    }
}
//- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
//    int count=0;
//    for (UIView *object in actionSheet.subviews) {
//        if ([[[object class] description] isEqualToString:@"UIView"]) {
//            count++;
//            if (count==2) {
//                object.backgroundColor = [UIColor whiteColor];
//                break;
//            }
//        }
//    }
//}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // 1ere partie actionSheet
    if (actionSheet.tag == 1){
        if ( buttonIndex == actionSheet.cancelButtonIndex) return;
        // On prepare le prochain UiActionSheet
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"title_filtrer_messages"] delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil  otherButtonTitles:nil];
        [popupQuery setTag:2];
        
        
        
        switch (buttonIndex) {
            case 0:{
                //Ressources
                NSArray * ressources = [UtilsCore getAllRessources];
                //Liaison du tableau de ressource à l'actionsheet pour récupération dans l'étape suivante, sans avoir besoin de tout récupérer
                objc_setAssociatedObject(popupQuery, @"PFArrayRessources", ressources, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                for (Ressource * res in ressources) {
                    [popupQuery addButtonWithTitle:res.nom_court];
                }
                filtre_actif = FiltreRessource;
                popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:[LanguageManager get:@"button_annuler"]];
                
                if (IS_IPHONE)
                    [popupQuery showInView:self.view];
                else{
                    dispatch_async(dispatch_get_main_queue(), ^ {
                        [popupQuery showFromRect:CGRectMake(0, 0, _boutonFiltre.frame.size.width, _boutonFiltre.frame.size.height) inView:_boutonFiltre animated:YES];
                    });
                }
                break;
                
            }
            case 1:{
                //Utilisateur
                NSArray * utilisateurs = [UtilsCore getAllUtilisateurs];
                for (Utilisateur * utilisateur in utilisateurs) {
                    [popupQuery addButtonWithTitle:utilisateur.owner_username];
                }
                
                filtre_actif = FiltreUtilisateur;
                popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:[LanguageManager get:@"button_annuler"]];
                
                if (IS_IPHONE)
                    [popupQuery showInView:self.view];
                else
                    dispatch_async(dispatch_get_main_queue(), ^ {
                        [popupQuery showFromRect:CGRectMake(0, 0, _boutonFiltre.frame.size.width, _boutonFiltre.frame.size.height) inView:_boutonFiltre animated:YES];
                    });
                break;
            }
                
            case 2:{
                // Tags
                NSArray * tags = [[UtilsCore getAllTag] mutableCopy];
                for (NSString * tagLocal in tags) {
                    [popupQuery addButtonWithTitle:tagLocal];
                }
                filtre_actif = FiltreTag;
                popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:[LanguageManager get:@"button_annuler"]];
                
                if (IS_IPHONE)
                    [popupQuery showInView:self.view];
                else
                    
                    dispatch_async(dispatch_get_main_queue(), ^ {
                        [popupQuery showFromRect:CGRectMake(0, 0, _boutonFiltre.frame.size.width, _boutonFiltre.frame.size.height) inView:_boutonFiltre animated:YES];
                    });
                break;
            }
                
            case 3:{
                //cercles
                if ( ![self checkForLoggedInUser] ) return;
                NSMutableArray * cercles = [[NSMutableArray alloc]init];
                NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: nil];
                [WService get:@"reseau/liste"
                        param: query
                     callback: ^(NSDictionary *wsReturn) {
                         if ( !wsReturn ) return;
                         [cercles addObjectsFromArray: [wsReturn objectForKey:@"cercles"]];
                         [cercles addObjectsFromArray: [wsReturn objectForKey:@"classes"]];
                         for (NSDictionary * cercleLocal in cercles) {
                             [popupQuery addButtonWithTitle:[cercleLocal objectForKey:@"nom"]];
                         }
                         cercleArrayTemp = cercles;
                         filtre_actif = FiltreCercle;
                         popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:[LanguageManager get:@"button_annuler"]];
                         
                         if (IS_IPHONE)
                             [popupQuery showInView:self.view];
                         else
                             
                             dispatch_async(dispatch_get_main_queue(), ^ {
                                 [popupQuery showFromRect:CGRectMake(0, 0, _boutonFiltre.frame.size.width, _boutonFiltre.frame.size.height) inView:_boutonFiltre animated:YES];
                             });
                         
                     }];
                
                
                break;
            }
                
                
            default:{
                filtre_actif = nil;
                idCapsule = nil ;
                id_utilisateur = nil;
                tag_message = nil;
                id_utilisateurs = nil;
                [self refreshMessages];
                break;
            }
        }
        
    }
    // 2eme partie actonSheet
    if (actionSheet.tag == 2){
        // Reset Trans
        if ( buttonIndex == actionSheet.cancelButtonIndex) return;
        
        idCapsule = nil ;
        id_utilisateur = nil;
        tag_message = nil;
        id_utilisateurs = nil;
        
        
        switch (filtre_actif) {
            case FiltreRessource:{
                // On prepare le prochain UiActionSheet
                UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"title_filtrer_messages"] delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil  otherButtonTitles:nil];
                [popupQuery setTag:3];
                
                
                //Récuperation des capsules de la ressource
                //                Ressource * ressource = [UtilsCore getRessource:((Ressource * )[[UtilsCore getAllRessources] objectAtIndex:buttonIndex]).id_ressource];
                //Récupération du tableau stocké
                NSArray * ressources = objc_getAssociatedObject(actionSheet, @"PFArrayRessources");
                Ressource * ressource = ressources[buttonIndex];
                NSArray * capsules = [ressource.capsules sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"nom_court" ascending:YES]]];
                //Reset de l'objet associé
                objc_setAssociatedObject(popupQuery, @"PFArrayRessources",nil , OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                //Liaison du tableau de ressource à l'actionsheet pour récupération dans l'étape suivante, sans avoir besoin de tout récupérer
                objc_setAssociatedObject(popupQuery, @"PFArrayCapsules",capsules , OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                for (Capsule * caps in capsules) {
                    [popupQuery addButtonWithTitle:caps.nom_court];
                }
                filtre_actif = FiltreRessource;
                popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:[LanguageManager get:@"button_annuler"]];
                
                if (IS_IPHONE)
                    [popupQuery showInView:self.view];
                else{
                    dispatch_async(dispatch_get_main_queue(), ^ {
                        [popupQuery showFromRect:CGRectMake(0, 0, _boutonFiltre.frame.size.width, _boutonFiltre.frame.size.height) inView:_boutonFiltre animated:YES];
                    });
                }
                break;
            }
            case FiltreUtilisateur:
                id_utilisateur = ((Utilisateur* )[[UtilsCore getAllUtilisateurs] objectAtIndex:buttonIndex]).id_utilisateur;
                [self refreshMessages];
                break;
            case FiltreTag:
                tag_message =  [actionSheet buttonTitleAtIndex:buttonIndex];
                [self refreshMessages];
                break;
            case FiltreCercle:{
                if (!id_utilisateurs) id_utilisateurs = [[NSMutableArray alloc]init];
                NSArray * membres = [((NSDictionary *)[cercleArrayTemp objectAtIndex:buttonIndex] ) objectForKey:@"members"];
                for (NSDictionary * membre in membres) {
                    //                    [id_utilisateurs addObject:[membre objectForKey:@"guid"]];
                    [id_utilisateurs addObject:[membre objectForKey:@"id_utilisateur"]];
                }
                [self refreshMessages];
                break;
            }
                
                
                
            default:
                break;
        }
    }
    
    
    if ( actionSheet.tag == 3){
        // Reset Trans
        if ( buttonIndex == actionSheet.cancelButtonIndex) return;
        switch (filtre_actif) {
            case FiltreRessource:{
                //Récupération du tableau stocké
                NSArray * capsules = objc_getAssociatedObject(actionSheet, @"PFArrayCapsules");
                //Liaison du tableau de ressource à l'actionsheet pour récupération dans l'étape suivante, sans avoir besoin de tout récupérer
                objc_setAssociatedObject(actionSheet, @"PFArrayCapsules", nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                //On sauvegarde l'id de capsule précédent 
                idCapsule = [(Capsule*)capsules[buttonIndex] id_capsule];
                [self refreshMessages];
                break;
            }
            default:{
                //Cas de l'attribution d'une médaille (?)
//                NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
                //TODO: mettre des identifiants un jour, plutot que des strings en BDD...
                NSArray * correspondance_types_medailles = @[@"or",
                                                             @"argent",
                                                             @"bronze",
                                                             @""];
                //Si on est pas en overflow
                if(buttonIndex < [correspondance_types_medailles count]) {
                    NSString * type_medaille = correspondance_types_medailles[buttonIndex];
                    
                    //Appel du webservice
                    NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                   _selectedMessage.id_message, @"id_message",
                                                   type_medaille, @"type_medaille", nil];
                    
                    
                    [WService post:@"message/donnerMedaille"
                             param: query
                          callback: ^(NSDictionary *wsReturn) {
                              [WService displayErrors:wsReturn];
                              //Invalidation de sa taille en cache
                              objc_setAssociatedObject(_selectedMessage, @"PFCellHeight", nil, OBJC_ASSOCIATION_ASSIGN);
                              
                              [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInt:_selectedMessagePath.row]];
                              
//                              [tableView reloadData];
                          }];
                }
                
            }
                
        }
    }
    
    if (actionSheet.tag == 4) {
        NSURL * url = [NSURL URLWithString:[actionSheet buttonTitleAtIndex:buttonIndex]];
        [[UIApplication sharedApplication] openURL:url];
    }
    
    //Tri des messages
    if (actionSheet.tag == 5) {
        
        if ( buttonIndex == actionSheet.cancelButtonIndex) return;
        
        switch (buttonIndex) {
            case 0:{
                self.modeTri = ModeTriDate;
                break;
            }
            case 1:{
                self.modeTri = ModeTriVote;
                break;
            }
            default:{
                self.modeTri = ModeTriVote;
                break;
            }
                
        }
        [self refreshMessages];
    }
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
   	DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    [MessageCell clearCache];
    
    // Release any cached data, images, etc that aren't in use.
}



- (void)dealloc
{
   	DLog(@"");
}


- (IBAction)selectionneProfil:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    _selectedMessage = [_messages objectAtIndex:indexPath.row] ;
    [self performSegueWithIdentifier: @"profilSegue" sender: self];
    
}

- (IBAction)selectionneLienVersCapsule:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    _selectedMessage = [_messages objectAtIndex:indexPath.row] ;
    [UtilsCore setMessageLu:_selectedMessage.id_message];
    [self setStackPosition:SCStackViewControllerPositionLeft];
    if ( [_selectedMessage.nom_page isEqualToString:@"" ]){
        [self performSegueWithIdentifier: @"transResSegue" sender: self];
    }else{
        [self performSegueWithIdentifier: @"transPageSegue" sender: self];
    }
}

- (IBAction)selectionneLienVersChoixLangue:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    _selectedMessage = [_messages objectAtIndex:indexPath.row];
    [self modifierLangueMessageExistant:sender];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    boolAfficheReponse = NO;
    //    if([[segue identifier] isEqualToString:@"nouveauMessageSegue"]){
    //        NouveauMessageController * nouveauMessageController = segue.destinationViewController;
    //        nouveauMessageController.currentCapsule = currentCapsule;
    //        nouveauMessageController.urlPage = urlPage;
    //        nouveauMessageController.nom_tag = tag;
    //        nouveauMessageController.num_occurence = num_occurence;
    //        if (modeNouveauMessage == ModeNouveauMessageReponse){
    //
    //            // Si on est deja sur une reponse , on repond au parent
    //            if ( _selectedMessage.id_message_parent != nil ) {
    //                nouveauMessageController.idMessageOriginal = _selectedMessage.id_message_parent;
    //            }else{
    //            // Sinon c'est une reponse classique
    //                nouveauMessageController.idMessageOriginal = _selectedMessage.id_message;
    //            }
    //
    //        }
    //    }
    if([[segue identifier] isEqualToString:@"profilSegue"]){
        ProfilController * profilController = segue.destinationViewController;
        profilController.id_utilisateur =  _selectedMessage.id_auteur;
    }
    
    if([[segue identifier] isEqualToString:@"transResSegue"]){
        SommaireVC * sommaireVC = segue.destinationViewController;
        sommaireVC.currentCapsule = [UtilsCore getCapsule:_selectedMessage.id_capsule];
        
        if (IS_IPAD) {
            [sommaireVC setStackPosition:SCStackViewControllerPositionRight];
        }
    }
    
    if([[segue identifier] isEqualToString:@"transPageSegue"]){
        Capsule * current_capsule = [UtilsCore getCapsule:_selectedMessage.id_capsule];
        NSMutableArray * pageData = [[NSMutableArray alloc] init];
        [pageData addObject:[NSString stringWithFormat:@"%@/%@",[current_capsule getPath],_selectedMessage.nom_page]];
        PageVC * pageVC = segue.destinationViewController;
        
        if (IS_IPAD) {
            [pageVC setStackPosition:SCStackViewControllerPositionRight];
        }
        pageVC.pageData = [pageData copy];
        pageVC.currentCapsule = current_capsule;
        pageVC.selectedIndex = 0;
        pageVC.tag_trans = _selectedMessage.nom_tag;
        pageVC.num_occurence_trans = _selectedMessage.num_occurence;
    }
    
    if([[segue identifier] isEqualToString:@"messageOptionSegue"])
    {
        MessageOptionController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        viewC.boolDefi = _boolDefi;
        viewC.publiTwitter = _publiTwitter;
        viewC.visibiliteArray = _visibiliteArray;
        viewC.visibiliteIdArray = _visibiliteIdArray;
        viewC.tagArray = _tagArray;
        viewC.id_ressource = currentCapsule.id_ressource;
        viewC.idMessageOriginal = _idMessageOriginal;
        viewC.boolGeolocalisation = _boolGeolocalisation;
    }
    
}


#pragma mark - RMPickerViewController Delegates
-(void)pickerViewControllerDidCancel:(RMPickerViewController *)vc{
    _pickerEditExisistingMessageLanguage = nil;
    _pickerNewMessageLanguage = nil;
}

- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray *)selectedRows {
    if (vc == _pickerNewMessageLanguage) {
        int index = [[self.langueValuesString objectAtIndex:[selectedRows[0] intValue]] intValue];
        //        int index = [selectedRows[0] intValue];
        //        _langueMsg = [LanguageManager codeLangueWithArrayIndex:index];
        _langueMsg = [LanguageManager codeLangueWithid:index];
        [self.messageHeaderCell.messagePanel.langueBouton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icone_drapeau_rond_%@.png", _langueMsg]] forState:UIControlStateNormal];
        [self.messageHeaderCell.messagePanel.messageTextView becomeFirstResponder];
    }
    else{
        //        NSString *idLangue = [NSString stringWithFormat:@"%d", [LanguageManager idLangueWithArrayIndex:[[selectedRows objectAtIndex:0] intValue]]];
        
        NSString * idLangue = [self.langueValuesString objectAtIndex:[selectedRows[0] intValue]];
        // Appel du webservice
        NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_selectedMessage.id_message , @"id_message", idLangue, @"langue", nil];
        
        [WService post:@"message/modifierLangue"
                 param: query
              callback: ^(NSDictionary *wsReturn) {
                  [WService displayErrors:wsReturn];
                  
                  // Mise à jour du message dans la base locale
                  [UtilsCore setMessageLangue:_selectedMessage.id_message langue:[NSNumber numberWithInteger:[idLangue integerValue]]];
                  
                  [UtilsCore rafraichirMessagesFromMessageId:_selectedMessage.id_message indexTableView:[NSNumber numberWithInteger:_selectedMessagePath.row]];
                  [tableView reloadData];
              }];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.langueValuesString count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    //Récupération des langues choisies par l'utilisateur UNIQUEMENT
    return [LanguageManager nameLangueWithId:[[self.langueValuesString objectAtIndex:row] intValue]];
}

#pragma mark - Gestion menu


#pragma mark - CartePanelDelegate
//déclencher dès que la carte entraine une modification d'affichage de markeur
//array contient les messages actuellement visible sur la carte
-(void) surModificationCamera:(NSMutableArray*)array {
    DLog(@"taille message : %lu", array.count);
    
    _messages = [self sortMessagesOfArray:array withType:self.modeTri];
    
    [self gereAffichageFooter];
    [self.tableView reloadData];
}

-(void) surChoixMarker:(NSNumber*)idMessage {
    DLog(@"ID du message séléctionné : %@", idMessage);
    
    [self highlightMessageFromTrans:idMessage];
}

#pragma mark - Gestion PJ

#pragma mark - Gestion Geolocalisation

- (void)commencerGeolocalisation {
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        [locationManager requestWhenInUseAuthorization];
    
    if(!geolocalisationEnCourt){
        [locationManager startUpdatingLocation];
        geolocalisationEnCourt = YES;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DLog(@"Error geolocating");
    [locationManager stopUpdatingLocation];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_alertview_geolocate_title"] andMessage:[LanguageManager get:@"ios_alertview_geolocate_content"]];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"ios_alertview_geolocate_send_anyway"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              positionCourante = nil;
                              _boolGeolocalisation = false;
                              [self envoyerMessageApresGestionGeolocalisation];
                          }];
    
    [alertView addButtonWithTitle:[LanguageManager get:@"ios_button_reesayer"]
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [locationManager startUpdatingLocation];
                          }];
    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    if(demandeEnvoie == YES)
        [alertView show];
    
    
    geolocalisationEnCourt = NO;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    positionCourante = [locations lastObject];
    geolocalisationEnCourt = NO;
    
    if (positionCourante != nil) {
        DLog(@"*****Récupération  Longitude : %@", [NSString stringWithFormat:@"%.8f", positionCourante.coordinate.longitude]);
        DLog(@"*****Récupération  Latitude : %@", [NSString stringWithFormat:@"%.8f", positionCourante.coordinate.latitude]);
    }
    
    //we got the position so we stop the position fetching which would last forever !
    [locationManager stopUpdatingLocation];
    
    if(demandeEnvoie)
        [self envoyerMessageApresGestionGeolocalisation];
}

//Permet de n'afficher que les messages geolocalisé et de fournir les messages à afficher à la carte
-(void)afficheQueLesMessagesGeolocalise {
    [self filtrerMessageGeolocalise];
    //Sauvegarde des messages pas géolocalisés, s'il ne l'étaient pas déjà
    //Evite qu'un double clic ecrase cette variable
    if (!_messagesAvantGeolocalisation) {
        _messagesAvantGeolocalisation = [_messages mutableCopy];
    }
    
    _messages = [_messagesGeolocalises mutableCopy];
    
    self.carteHeaderCell.cartePanel.messages_geolocalise = _messagesGeolocalises;
}

//Filtre dans les messages actuel affichés ceux qui sont geolocalisés
-(void) filtrerMessageGeolocalise{
    DLog(@"Filtering geolocationed messages ...");
    [_messagesGeolocalises removeAllObjects];
    [_messages enumerateObjectsUsingBlock:^(Message * obj, NSUInteger idx, BOOL *stop) {
        
        if (obj.geo_longitude != nil && [obj.geo_longitude doubleValue] != 0 && [obj.geo_latitude doubleValue]) {
            [_messagesGeolocalises addObject:obj];
        }
    }];
    DLog(@"==========Nombre de message geolocalisé : %li", [_messagesGeolocalises count]);
}

#pragma mark - Gestion nouveau message

-(void) initNouveauMessage{
    //Options
    if (_tagArray == nil) _tagArray = [[NSMutableArray alloc]init];
    if (_visibiliteArray == nil ) _visibiliteArray = [[NSMutableArray alloc]initWithArray:[NSMutableArray arrayWithObjects:@"Public",nil] ];
    if (_visibiliteIdArray == nil ) _visibiliteIdArray = [[NSMutableArray alloc]initWithArray:[NSMutableArray arrayWithObjects:@0,nil] ];
    
    if ( [[UIViewController getPreference:@"publishTwitter"] integerValue ] == 1)
        [self setPubliTwitter:YES];
    else
        [self setPubliTwitter:NO];
    
}

-(IBAction)afficherOptionMessage:(id)sender{
    [self performSegueWithIdentifier: @"messageOptionSegue" sender: self];
    
}

- (void)recupereOptions:(NSMutableArray *)visibilite visibiliteId:(NSMutableArray *)visibiliteId tagArray:(NSMutableArray *)tagArray defi: (BOOL)defi publiTwitter:(BOOL)publiTwitter geolocalise: (BOOL)geolocalise{
    _boolDefi = defi;
    _visibiliteIdArray = visibiliteId;
    _visibiliteArray = visibilite;
    _tagArray = tagArray;
    _publiTwitter = publiTwitter;
    _boolGeolocalisation = geolocalise;
    
    //Si on desactive la geolocalisation dans les option alors on "vide" la variable
    if(!geolocalise)
        positionCourante = nil;
    //commence la geolocalisation si elle est demandée
    else if(_boolGeolocalisation && positionCourante == nil)
        [self commencerGeolocalisation];
}

-(IBAction)sendMessage {
    
    demandeEnvoie = YES;
    //lancement de la geolocalisation
    //appel de la méthode :
    // - success : didUpdateLocations
    // - failure : didFailWithError
    
    if(_boolGeolocalisation && positionCourante == nil){
        [self commencerGeolocalisation];
    }else
        [self envoyerMessageApresGestionGeolocalisation];
}

- (void)envoyerMessageApresGestionGeolocalisation {
    DLog(@"envoyerMessageApresGestionGeolocalisation :");
    DLog(@"Demande de geoloc : %d", _boolGeolocalisation);
    
    demandeEnvoie = NO;
    
    NSString *message = [[NSString alloc] initWithString:self.messageHeaderCell.messagePanel.messageTextView.text];
    NSMutableString * trim_message = [message mutableCopy];
    
    //Trim
    [trim_message replaceOccurrencesOfString:@" " withString:@"" options:nil range:NSMakeRange(0, [message length])];
    
    if ([trim_message length] != 0 && ![message isEqualToString:[LanguageManager get:@"ios_label_message_placeholder"]]){
        if (_publiTwitter)
            [self copublier];
        else
            [self envoieMessageApresPreps];
        
        //On a envoyé le message donc on enlève la position de l'utilisateur
        positionCourante = nil;
    }
    else
    {
        [self.navigationController.view makeToast:[LanguageManager get:@"ios_message_vide"] duration:3.0 position:@"bottom" title:nil image:[UIImage imageNamed:@"warning_48"] style:nil completion:nil];
    }
}

//Appel la méthode d'envoie des messages après avoir gérés les différents paramètres necessaires
- (void)envoieMessageApresPreps{
    
    //Si l'utilisateur n'est pas connet-cté alors
    if(![UIViewController isConnected]){
        //On le previens qu'il ne peut envoyer de message dans ce mode et on stop l'envoie
        DLog(@"User not connected : not sending messages");
        
        [self.navigationController.view makeToast:[LanguageManager get:@"ios_alertview_offline_send_message_not_logged_content"] duration:3.0 position:@"top" title:[LanguageManager get:@"ios_alertview_offline_send_message_not_logged_title"] image:[UIImage imageNamed:@"warning_48"] style:nil completion:nil];
        
        return;
    }
    
    DLog(@"User connected : sending messages");
    
    
    //Contenu du message
    NSString *contenuMessage = self.messageHeaderCell.messagePanel.messageTextView.text;
    
    //Gestion de la visibilité
    NSError* error = nil;
    NSString *visibliteTabMessage = @"";
    NSString *tagTabMessage = @"";
    
    //transforme un tableau en json string
    if (_visibiliteIdArray){
        NSData* jsonDataVisiblite = [NSJSONSerialization dataWithJSONObject:_visibiliteIdArray
                                                                    options:NSJSONWritingPrettyPrinted error:&error];
        visibliteTabMessage = [[NSString alloc] initWithData:jsonDataVisiblite encoding:NSUTF8StringEncoding];
    }
    
    //Gestion des tags
    //transforme un tableau en json string
    if (_tagArray){
        
        NSData* jsonDataTag = [NSJSONSerialization dataWithJSONObject:_tagArray
                                                              options:NSJSONWritingPrettyPrinted error:&error];
        tagTabMessage = [[NSString alloc] initWithData:jsonDataTag encoding:NSUTF8StringEncoding];
    }
    
    //Gestion de la modification d'un message
    //Si nil alors on en modifie pas un message prés existant
    NSNumber *currentIDMessage= _idMessage;
    
    //Gestion de l'id de la capsule
    NSNumber *capsuleIDMessage = currentCapsule.id_capsule;
    
    //Gestion urlPage
    NSString *urlPageMessage = urlPage;
    
    BOOL estDefiMessage = _boolDefi;
    
    //Gestion tag
    NSString *tagMessage = self.tag;
    //Gestion num_ocurence
    NSNumber *num_occurenceMessage = self.num_occurence;
    
    //Gestion des positions de geolocalisation
    NSNumber *longitudeMessage = @0;
    NSNumber *latitudeMessage = @0;
    
    if ( _boolGeolocalisation){
        longitudeMessage = [NSNumber numberWithDouble: positionCourante.coordinate.longitude];
        latitudeMessage = [NSNumber numberWithDouble: positionCourante.coordinate.latitude];
    }
    
    //Gestion de la langue
    NSNumber *langueMessage = [NSNumber numberWithInt:[LanguageManager idLangueWithCode:_langueMsg]];
    
    //Gestion de l'utilisateur actuel
//    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    NSDictionary * rank_for_capsule = [UIViewController getRankForCapsule:currentCapsule.id_capsule];
    
    //rang
    NSNumber *rankUserIDMessage = rank_for_capsule[@"id_categorie"];
    NSString *rankUserNameMessage = rank_for_capsule[@"nom_categorie"];
    //info
    NSNumber *userIDMessage =   [UIViewController getSessionValueForKey:@"id_utilisateur"];
    NSString *avatarUrlMessage = [UIViewController getSessionValueForKey:@"avatar_url"];
    
    NSArray* pjs = self.messageHeaderCell.pjPanel.pjTableau;
    
    [self publierMessage:contenuMessage
    visibiliteTabMessage:visibliteTabMessage
           tagTabMessage:tagTabMessage
        currentIDMessage:currentIDMessage
        capsuleIDMessage:capsuleIDMessage
          urlPageMessage:urlPageMessage
          estDefiMessage:estDefiMessage
              tagMessage:tagMessage
    num_occurenceMessage:num_occurenceMessage
         latitudeMessage:latitudeMessage
        longitudeMessage:longitudeMessage
           langueMessage:langueMessage
       rankUserIDMessage:rankUserIDMessage
     rankUserNameMessage:rankUserNameMessage
           userIDMessage:userIDMessage
        avatarUrlMessage:avatarUrlMessage
                     pjs:pjs];
}

- (void)copublier {
    
    UITextView *message = self.messageHeaderCell.messagePanel.messageTextView;
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSString * initialText = [NSString stringWithFormat:@"#%@ #%@ ",@"PairForm",currentCapsule.nom_court];
        /*if ( ![urlPage isEqualToString:@""] ){
         initialText = [NSString stringWithFormat:@"%@#%@  ",initialText,urlPage ];
         }*/
        if (message.text.length + initialText.length > 140){
            //NSRange r = NSMakeRange(0, 140);
            //initialText = [initialText substringWithRange: r];
            [UIPasteboard generalPasteboard].string = message.text ;
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_message_copublier_troplong_titre"]
                                                             andMessage:[LanguageManager get:@"ios_message_copublier_troplong_contenu"]];
            [alertView addButtonWithTitle:[LanguageManager get:@"ios_message_copublier_troplong_button"]
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            [alertView show];
        }else{
            initialText = [NSString stringWithFormat:@"%@ %@",initialText,message.text  ];
        }
        
        [mySLComposerSheet setInitialText:initialText];
        
        //[mySLComposerSheet addURL:[NSURL URLWithString:@"http://...."]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    DLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    DLog(@"Post Sucessful");
                    [self envoieMessageApresPreps];
                    break;
                    
                default:
                    break;
                    
            }
            [self dismissViewControllerAnimated:YES completion:nil];
            [message resignFirstResponder];
            
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
}

//currentIDMessage peut être null
//tagMessage peut être null
//num_occurenceMessage peut être null

-(void)publierMessage: (NSString*)contenuMessage
 visibiliteTabMessage: (NSString*)visibiliteTabMessage
        tagTabMessage: (NSString*)tagTabMessage
     currentIDMessage: (NSNumber*)currentIDMessage
     capsuleIDMessage: (NSNumber*)capsuleIDMessage
       urlPageMessage: (NSString*)urlPageMessage
       estDefiMessage: (BOOL)estDefiMessage
           tagMessage: (NSString*)tagMessage
 num_occurenceMessage: (NSNumber*)num_occurenceMessage
      latitudeMessage: (NSNumber*)latitudeMessage
     longitudeMessage: (NSNumber*)longitudeMessage
        langueMessage: (NSNumber*)langueMessage
    rankUserIDMessage: (NSNumber*)rankUserIDMessage
  rankUserNameMessage: (NSString*)rankUserNameMessage
        userIDMessage: (NSNumber*)userIDMessage
     avatarUrlMessage: (NSString*)avatarUrlMessage
     pjs: (NSArray*)donneePJ
{
    if (![self checkForLoggedInUser]) {
        return;
    }
    
    //Dismiss du clavier
    [self.messageHeaderCell.messagePanel.messageTextView resignFirstResponder];
    
    DLog(@"");
    
    __block NSMutableDictionary * params = [NSMutableDictionary new];
    
    [params setObject: [NSNumber numberWithDouble:-1.0*TIMESTAMP] forKey:@"id_message"];
    
    [params setObject:capsuleIDMessage forKey:@"id_capsule"];
    [params setObject:urlPageMessage forKey:@"nom_page"];
    [params setObject:contenuMessage forKey:@"contenu"];
    [params setObject:visibiliteTabMessage forKey:@"visibilite"];
    [params setObject:tagTabMessage forKey:@"tags"];
    
    if(self.boolGeolocalisation){
        [params setObject:latitudeMessage forKey:@"geo_latitude"];
        [params setObject:longitudeMessage forKey:@"geo_longitude"];
    }
    
    [params setObject:langueMessage forKey:@"id_langue"];
    [params setObject:rankUserIDMessage forKey:@"id_categorie"];
    [params setObject:rankUserNameMessage forKey:@"nom_categorie"];
    [params setObject:userIDMessage forKey:@"id_utilisateur"];
    [params setObject:avatarUrlMessage forKey:@"avatar_url"];
    
    if(estDefiMessage)
        [params setObject:@1 forKey:@"est_defi"];
    else
        [params setObject:@0 forKey:@"est_defi"];
    
    if(currentIDMessage)
        [params setObject:currentIDMessage forKey:@"id_message"];
    
    if (tagMessage != nil  && num_occurenceMessage != nil) {
        [params setObject:tagMessage forKey:@"nom_tag"];
        [params setObject:num_occurenceMessage forKey:@"num_occurence"];
    }
    else if(self.urlPage){
        [params setObject:@"PAGE" forKey:@"nom_tag"];
    }
    
     if([donneePJ count] > 0)
        [params setObject:donneePJ forKey:@"pjs"];
    
    ////////////////
    //Ce paramètre : id_message_parent n'est pas touché et déplacer car utilisé après l'envoie
    ////////////////
    
    
    __block NSUInteger index_of_new_message = 0;
    __block NSUInteger index_of_parent_message = -1;
    // Si reponse
    if ( _idMessageOriginal ){
        //Si le message sélectionné n'est pas un parent
        //AAJGAJEGIOJA putain de faux nil NSNUMBER ta race
        //^^^^^^^^^^^^  -> Crise ancienne, relative au fait que php renvoie des nulls tous pourris, defois non décelables quand
        //ils sont enregistrés dans un managed object model (marche pas ni avec NSNull, ni !, et n'a pas de valeur, mais n'est pas nil. Hahaha.)
        NSNumber * id_parent = _selectedMessage.id_message_parent;
        if (![id_parent isEqualToNumber:@0]) {
            //Sélection du message parent
            [_messages enumerateObjectsUsingBlock:^(Message * obj, NSUInteger idx, BOOL *stop) {
                if ([obj.id_message isEqualToNumber:id_parent]) {
                    index_of_parent_message = idx;
                    _idMessageOriginal = obj.id_message;
                    //Et on arrête l'itération
                    stop = YES;
                }
            }];
        }
        else{
            index_of_parent_message = [_messages indexOfObject:_selectedMessage];
        }
        
        //Sécurité anti not_found :
        if (index_of_new_message == NSNotFound) {
            index_of_new_message = 0;
        }
        else{
            index_of_new_message = index_of_parent_message +1;
        }
        [params setObject:_idMessageOriginal forKey:@"id_message_parent"];
    }
    
    [params setObject:[NSNumber numberWithInt:index_of_new_message ] forKey:@"index_tableview"];
    
    [UtilsCore recordMessageAsync:params callbackSync:^(Message * message, BOOL message_envoye_au_reseau) {
        
        DLog(@"CallBackSync");
        
        //Message envoyé donc on enlève toutes les pièces jointes de la vue du menu
        [self.messageHeaderCell.pjPanel effaceToutesPJ];
        
        //On l'insère dans le datasource
        [_messages insertObject:message atIndex:index_of_new_message];
        [_messages_fixe insertObject:message atIndex:index_of_new_message];
        
        if(_boolGeolocalisation){
            [_messagesGeolocalises addObject:message];
        }
        
        //Message envoyé donc on reinitialise la geolocalisation
        positionCourante = nil;
        //On remet la demande de geolocalisation selon les paramètres globaux car les paramètres du précédent message sont obsolètes
        _boolGeolocalisation = [[UIViewController getPreference:@"geolocalisation"] boolValue];
        
        //On récupère l'index_message
        NSIndexPath * index_path = [NSIndexPath indexPathForRow:index_of_new_message inSection:0];
        
        //Et on update les lignes au bon endroit
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:@[index_path] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
        [self.tableView scrollToRowAtIndexPath:index_path atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        
        [self.messageHeaderCell.messagePanel.messageTextView setText:@""];
        [self.messageHeaderCell.messagePanel.messageTextView setText:[LanguageManager get:@"ios_label_message_placeholder"]];
        
        [self gereAffichageFooter];
        
        //Et reset des contextes
        _idMessageOriginal = nil;
        _selectedMessage = nil;
        
        
        //Si le message n'a pas été envoyé au serveur
        if (!message_envoye_au_reseau) {
            //On met un petit toast d'envoi pour dire que son message sera envoyé
            [self.view makeToast:[LanguageManager get:@"ios_message_offline_synchro"] duration:6 position:@"bottom" title:nil image:[UIImage imageNamed:@"info_48"] style:nil completion:nil];
        }
    } callbackAsync:^(NSString * status) {
        
        if([status isEqualToString:@"success"]){
            DLog(@"Success CallBackAsync");
            
            NSArray* pjs = [UtilsCore recuperePJDuMessage:[(Message*)[_messages objectAtIndex:index_of_new_message] id_message] WithContext:nil];
            
            for(PJMessage* pj in pjs){
                [MessageCell ajouteDataPieceJointeApresEnvoiePourEviterTelechargement:pj EtDonnees:params[pj.nom_original]];
            }
            
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:index_of_new_message inSection:0];
            NSArray* rowsToReload = @[rowToReload];
            
            [tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
            
        }
        else if ([status isEqualToString:@"error"]) {
            DLog(@"Failure CallBackAsync");
            
            [self.view makeToast:[LanguageManager get:@"ios_message_toast_echec_envoie_reesayer"] duration:3.0 position:@"bottom" title:nil image:[UIImage imageNamed:@"warning_48"] style:nil completion:nil];
            
            //Supprime des messages de la carte si besoin
            // A faire avant de supprimer l'objet dans _messages, hein vincent :)
            [_messagesGeolocalises removeObject:[_messages objectAtIndex:index_of_new_message]];
            
            [_messages removeObjectAtIndex:index_of_new_message];
            [_messages_fixe removeObjectAtIndex:index_of_new_message];
            
            
            [self gereAffichageFooter];
            
            //Et on update les lignes au bon endroit
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index_of_new_message inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
            [self.tableView endUpdates];
        }
    }];
    
    
    //    [self.view makeToast:@"Envoi du message en cours..." duration:2.0 position:@"bottom"];
    //
    //    [WService post:[NSString stringWithFormat:@"%@/messages/enregistrerMessage.php",sc_server]
    //         param: params
    //      callback: ^(NSDictionary *wsReturn) {
    //          if(![WService displayErrors:wsReturn])
    //          {
    //              [UtilsCore rafraichirMessagesFromMessageId:_idMessage];
    //              //Enregistrement des points
    //              [UtilsCore recordNotificationFromDictionary:@{@"id_user" : [UIViewController getSessionValueForKey:@"id_utilisateur"],
    //                                                            @"type" : @"user_score",
    //                                                            @"title" : @"Message posté",
    //                                                            @"score_type": @"PTSECRIREMESSAGE",
    //                                                            @"content": currentCapsule.nom_court,
    //                                                            @"id_message": wsReturn[@"id_message"]}];
    //              if (IS_IPHONE) {
    //                  [self.navigationController popViewControllerAnimated:YES];
    //              }
    //              else{
    //                  [self.sc_stackViewController popViewControllersAfter:self AtPosition:self.stackPosition animated:YES completion:nil];
    //                [self.view makeToast:@"Message envoyé!" duration:2.0 position:@"bottom" title:nil image:[UIImage imageNamed:@"paper_plane_64"] style:nil completion:nil];
    //              }
    //          }
    //      }];
    
}

#pragma mark - Textview delegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    DLog(@"");
    
    [self setModeNouveauMessage:ModeNouveauMessageNormal];
    [self.messageHeaderCell setEtat_menu:NewMessageWrite];
    //Textview réactif
    if ([textView.text isEqualToString:[LanguageManager get:@"ios_label_message_placeholder"]]) {
        [textView setText:@""];
    }
    [textView  invalidateIntrinsicContentSize];
    
    [self.messageHeaderCell redimensionneHeaderAvecTaille];
//    self.tableView.tableHeaderView = self.tableView.tableHeaderView;
    //commence la geolocalisation si elle est demandée
    if(_boolGeolocalisation && positionCourante == nil)
        [self commencerGeolocalisation];
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    DLog(@"");
    [self setModeNouveauMessage:ModeNouveauMessageNone];
    
    if ([textView.text isEqualToString:@""]) {
        //        [textView setAttributedText:[[NSAttributedString alloc] initWithString:@"Exprimez vous..." attributes:@{NSFontAttributeName: [UIFont italicSystemFontOfSize:12]}]];
        [textView setText:[LanguageManager get:@"ios_label_message_placeholder"]];
        
    }
    
    
    //    [tableCell updateConstraints];
    
    /* [self.tableView beginUpdates];
     //    [self.tableView setNeedsDisplayInRect:CGRectMake(0, 0, 320, 150)];
     
     [self.tableView endUpdates];*/
    /*UIView* tableCell = [[textView superview] superview];*/
    
    //On ne redimensionne que si on est en mode message
    //Obligatoire car cette méthode met un certain temps à ce lancer et le menu as déjà pris en compte le resizing du nouvelle item cliqué
//    if (self.messageHeaderCell.etat_menu == NewMessage) {
//    }
    
    /*[UIView animateWithDuration:0.5 animations:^{
     CGRect frame = tableCell.frame;
     frame.size.height = 88; //44
     [tableCell setFrame:frame];
     }];*/
    
}
#pragma mark - Filtre des messages
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(updateTableViewFrom:) withObject:@[searchBar, searchText] afterDelay:0.3];
    
}
-(void)updateTableViewFrom:(NSArray *)search_components{
    UISearchBar *searchBar = search_components[0];
    NSMutableString * searchText = [search_components[1] mutableCopy];
    //Trim de la chaine de caractères
    CFStringTransform((__bridge CFMutableStringRef)searchText, NULL, kCFStringTransformStripCombiningMarks, NO);
    //    if (IS_IPAD && modeMessage == ModeMessageTransversal) {
    if ([searchText isEqualToString:@""]) {
        return [self searchBarCancelButtonClicked:searchBar];
    }
    //Activation du bouton de recherche
    [self.menuMessageHeaderCell.bouton_menu_recherche setSelected:YES];
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"(contenu contains[cd] %@)",
                                    searchText];
    
    //        _filterResults = [_allUsers filteredArrayUsingPredicate:resultPredicate];
    
    NSArray * newResults = [_messages_fixe filteredArrayUsingPredicate:resultPredicate];
    
    
    
    //Si on restreint les résultats
    if ([newResults count] < [_messages count]) {
        //Indexes des users affichés ne matchant pas le critère
        NSIndexSet *indexes = [_messages indexesOfObjectsPassingTest:^BOOL(Message* obj, NSUInteger idx, BOOL *stop){
            NSMutableString * obj_value = [obj.contenu mutableCopy];
            CFStringTransform((__bridge CFMutableStringRef)obj_value, NULL, kCFStringTransformStripCombiningMarks, NO);
            NSRange range = [[obj_value lowercaseString] rangeOfString: [searchText lowercaseString]];
            return range.location == NSNotFound;
        }];
        
        //Transformation en tableau d'indexpaths
        NSMutableArray *indexPaths = [NSMutableArray array];
        [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
        }];
        
        //Affectation des nouveaux résultats
        _messages = newResults;
        
        @try {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
            //Suppression animée des éléments
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationRight];
            [self.tableView endUpdates];
        }
        @catch (NSException *exception) {
            [self.view makeToast:[LanguageManager get:@"ios_message_lol_user"] duration:2.0 position:@"top"];
        }
    }
    //Si on élargit les résultats
    else if([newResults count] > [_messages count]){
        //Indexes des users rajoutés, par rapport au résultats précédents
        NSIndexSet *indexes = [newResults indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
            //Retourne true si l'utilisateur n'était pas dans les résultats précédents
            return [_messages indexOfObject:obj] == NSNotFound;
        }];
        
        //Transformation en tableau d'indexpaths
        NSMutableArray *indexPaths = [NSMutableArray array];
        [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
        }];
        
        //Affectation des nouveaux résultats
        _messages = newResults;
        
        //Suppression animée des éléments
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
    //Si il n'y a ni plus ni moins d'user filtré, on fait rien
    //    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //Activation du bouton de recherche
    [self.menuMessageHeaderCell.bouton_menu_recherche setSelected:NO];
    _messages = _messages_fixe;
    [self.tableView reloadData];
}

@end
