//
//  MessageCell.h
//  SupCast
//
//  Created by admin on 27/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTagList.h"
#import "Message.h"
#import "PJMessage.h"
#import "ZoomFSViewController.h"

//Enum représentant les mode d'affichage possible
typedef enum {
    ModeMessageCellNormal,
    ModeMessageCellTransversal,
} ModeMessageCell;

@interface MessageCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate, ZoomFSViewDelegate, UIDocumentInteractionControllerDelegate>

//Le message représenté par cette cellule
@property Message * message;
//Référence vers le contrôleur actuel
@property (strong, nonatomic) UIViewController *viewController;
//Référence vers le navigation contrôleur actuel
@property (strong, nonatomic) UINavigationController *navigationController;
//Tableau des pj présent dans la collection
@property (strong, nonatomic) NSArray *pjTableau;
//Deux collections de tags et de pjs
@property (weak, nonatomic) UICollectionView * pjMessageCollectionView;
@property (weak, nonatomic) UICollectionView * collectionViewTag;

@property(nonatomic) ModeMessageCell modeMessage;
//Permet de vider les variables et le message lors de l'initialisation d'une cellule
-(void)nettoyerCellule;
//Permet d'initialiser la vue ainsi que ces composant
- (void)initViewWithModeMessage:(ModeMessageCell)mode;
//setter
- (void)setTableView:(UITableView*)table;
//méthode renvoyant la hauteur pour les tags
- (int) getHeightForTags:(Message *)__message;
//Méthode permettant d'ajouter dans le tableau des pièces jointes original téléchargés une image dans le but de ne pas la télécharger
+(void) ajouteDataPieceJointeApresEnvoiePourEviterTelechargement:(PJMessage*)pj EtDonnees:(NSData*)data;
//Méthode permettant de supprimer toutes les images télécharger pour gagnezr de la mémoire RAM
+(void)clearCache;
@end