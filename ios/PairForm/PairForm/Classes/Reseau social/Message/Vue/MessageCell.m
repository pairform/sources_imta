//
//  MessageCell.m
//  SupCast
//
//  Created by admin on 27/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MessageCell.h"
#import "LanguageManager.h"
#import "PJMenu.h"
#import "SIAlertView.h"
#import "VideoVC.h"
#import <MediaPlayer/MediaPlayer.h>
@interface MessageCell()

@property (nonatomic, weak) UITableView * table_view_parent;
@property (nonatomic, strong) MPMoviePlayerViewController * videoVC;
@property (nonatomic, strong) UIDocumentInteractionController *docController;
@end

static NSMutableDictionary* pjData;

@implementation MessageCell
@synthesize message;

-(void)nettoyerCellule{
    self.pjTableau = @[];
    message = nil;
}

//On ne change pas les tag par des références par IBOutlet car cela évite de tous doublé pour les réponse.
//Les tags permettent un seul moyen unique de lier les composants des réponses et des messages
-(void)initViewWithModeMessage:(ModeMessageCell) mode{
    
    if(pjData == nil)
        pjData = [NSMutableDictionary new];
    
    self.pjMessageCollectionView = (UICollectionView *)[self viewWithTag:109];
    
    //Si c'est une réponse on inverse l'affichage en droite -> gauche
    if(message.id_message_parent != nil && ![message.id_message_parent isEqualToNumber:@0] && mode == ModeMessageCellNormal){
        [self.pjMessageCollectionView setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    
    
    self.collectionViewTag = ( UICollectionView *)[self viewWithTag:12];
    
    
    if(self.pjTableau.count > 0){
        [self.pjMessageCollectionView setHidden:NO];
        
        [self.pjMessageCollectionView setDelegate:self];
        [self.pjMessageCollectionView setDataSource:self];
    }
    else{
        //Pas de PJ
        [self.pjMessageCollectionView setHidden:YES];
    }
    
    self.modeMessage = mode;
    
    //Check de l'affichage
    int rank = [[[UIViewController getRankForCapsule:message.id_capsule] objectForKey:@"id_categorie"] intValue];
    int idUser = [[UIViewController getSessionValueForKey:@"id_utilisateur"] intValue];
    
    if([message.supprime_par intValue] == 0)
    {
        self.contentView.alpha = 1;
    }
    else
    {
        self.contentView.alpha = 0.5;
        //Else géré par heightForRow (on set la hauteur à 0)
    }
    
    Utilisateur * utilisateur = [UtilsCore getUtilisateur:message.id_auteur];
    
    UIButton *bImage = (UIButton *)[self viewWithTag:1];
    UIImage * imageModifiee = [utilisateur.getImage thumbnailImage:102 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh];
    
    [bImage  setImage:imageModifiee forState:UIControlStateNormal];
    
    UILabel *lblName = (UILabel *)[self viewWithTag:2];
    
    [lblName setText:utilisateur.owner_username];
    //[lblName setText: [NSString stringWithFormat:@"%@", message.id_message]];
//    
//    if([message.id_message integerValue] < 0)
//        [lblName setTextColor:[UIColor orangeColor]];
//    else
//        [lblName setTextColor:[UIColor blackColor]];
    
    //    [lblName sizeToFit];
    
    UILabel *lblRank = (UILabel *)[self viewWithTag:3];
    if ( message.role_auteur ){
        [lblRank setText:[LanguageManager get:[NSString stringWithFormat:@"label_user_%@", [message.role_auteur lowercaseString]]]];
    }else{
        [lblRank setText:@""];
    }
    //    [lblRank sizeToFit];
    
    UILabel *lblDate = (UILabel *)[self viewWithTag:4];
    NSDate * date_created = [NSDate dateWithTimeIntervalSince1970:[message.date_creation  intValue]];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    //    if (IS_IPHONE) {
    //        [formatter setDateFormat:@"dd/MM/yy à H:mm"];
    //    }
    //    else{
    [formatter setDateFormat:[LanguageManager get:@"ios_format_date"]];
    //    }
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString *dateString=[formatter stringFromDate:date_created];
    [lblDate setText:dateString];
    //    [lblDate sizeToFit];
    
    UILabel *lblValue = (UILabel *)[self viewWithTag:5];
    
    if( ([message.supprime_par intValue] != 0)  && (([message.supprime_par intValue] != idUser) && (rank < 3)))
    {
        [lblValue setText:[LanguageManager get:@"label_message_supprime"]];
    }else{
        [lblValue setText:message.contenu];
    }
    //    [lblValue setNeedsDisplay];
    //[lblValue sizeToFit];
    
    UILabel *lblUtilite = (UILabel *)[self viewWithTag:7];
    [lblUtilite setText:[message.somme_votes stringValue]];
    
    
    UIButton * IvUtiliteLeft = ( UIButton *)[self viewWithTag:6];
    if ( [message.utilisateur_a_vote isEqualToNumber:@-1]){
        [IvUtiliteLeft setImage:[UIImage imageNamed:@"arrow_left_active.png" ] forState:UIControlStateNormal];
    }else{
        [IvUtiliteLeft setImage:[UIImage imageNamed:@"arrow_left.png" ] forState:UIControlStateNormal];
    }
    
    
    UIButton * IvUtiliteRight = ( UIButton *)[self viewWithTag:8];
    if ( [message.utilisateur_a_vote isEqualToNumber:@1]){
        [IvUtiliteRight setImage:[UIImage imageNamed:@"arrow_right_active.png" ] forState:UIControlStateNormal];
    }else{
        [IvUtiliteRight setImage:[UIImage imageNamed:@"arrow_right.png" ] forState:UIControlStateNormal];
    }
    //    if ([CellIdentifier isEqualToString:@"MessageCell" ]){
    //        UIImageView * messageBG = ( UIImageView *)[cell viewWithTag:10];
    //        //TODO Solution temporaire
    //        //Si le message a bien un id_message
    //        //S'il y a des réponses
    //        //Et qu'on est pas dans le mode transversal
    //        if (![message.id_message isEqualToNumber:@0]
    //            && [UtilsCore getReponsesFromMessage:message.id_message]
    //            && (modeMessage != ModeMessageTransversal)
    //            && (modeMessage != ModeMessageLatest)
    //            && ![arrayParentsDeReponsesOuvertes containsObject:message.id_message]){
    //            [messageBG setImage:[UIImage imageNamed:@"ReponseBG"]];
    //        }else{
    //            [messageBG setImage:[UIImage imageNamed:@"MessageBG"]];
    //        }
    //    }
    UIImageView * messageBG = ( UIImageView *)[self viewWithTag:10];
    [messageBG setImage:[UIImage imageNamed:@"MessageBG"]];
    
    UIButton * bTrans = ( UIButton *)[self viewWithTag:11];
    
    if ( self.modeMessage != ModeMessageCellNormal ){
        [bTrans setHidden:NO];
        [utilisateur.getImage thumbnailImage:48 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh];
        Ressource * ressource = [UtilsCore getRessource:[[UtilsCore getCapsule:message.id_capsule] id_ressource]];
        [bTrans setImage:[[ressource getIcone] thumbnailImage:37 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh] forState:UIControlStateNormal];
    }else{
        // Pas de vue transversale
        [bTrans setHidden:YES];
    }
    // est defi &&  defi valider
    
    // Modification de la langue du message
    UIButton * bLangue;
    if ( self.modeMessage != ModeMessageCellNormal || ![message.id_message_parent isEqualToNumber:@0]){
        bLangue = (UIButton *)[self viewWithTag:72];
    } else {
        bLangue = (UIButton *)[self viewWithTag:11];
        [bLangue removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
        [bLangue addTarget:self action:@selector(selectionneLienVersChoixLangue:) forControlEvents:UIControlEventTouchUpInside];
        
        //Désactivation de l'ancien bouton, qui malgré qu'il est caché, reste activé
        UIButton * bOldLangue = (UIButton *)[self viewWithTag:72];
        [bOldLangue setEnabled:NO];
    }
    
    // Si emetteur du message ou superieur à collaborateur
    if (idUser == [message.id_auteur intValue] || rank >= 2) {
        [bLangue setHidden:NO];
        [bLangue setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icone_drapeau_rond_%@", [LanguageManager codeLangueWithid:[message.id_langue intValue]]]] forState:UIControlStateNormal];
        [bLangue setImageEdgeInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    } else {
        [bLangue setImageEdgeInsets:UIEdgeInsetsZero];
        [bLangue setHidden:YES];
    }
    
    
    //
    //    cell.backgroundView = [[UACellBackgroundView alloc] initWithFrame:CGRectZero];
    //
    //    [(UACellBackgroundView*) cell.backgroundView setColorsFrom:[UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1]
    //                                                            to:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:1]];
    //
    if (![message.est_defi isEqualToNumber:@0] || [message.defi_valide isEqualToNumber:@1]){
        [[self viewWithTag:14] setHidden:NO];
        if ([message.est_defi isEqualToNumber:@1]) {
            [(UIImageView*)[self viewWithTag:14] setImage:[UIImage imageNamed:@"DefiActif"]];
        }
        else if ([message.est_defi isEqualToNumber:@2]) {
            [(UIImageView*)[self viewWithTag:14] setImage:[UIImage imageNamed:@"DefiFini"]];
        }
        else if ([message.defi_valide isEqualToNumber:@1]) {
            [(UIImageView*)[self viewWithTag:14] setImage:[UIImage imageNamed:@"DefiValid"]];
        }
    }
    else
    {
        [[self viewWithTag:14] setHidden:YES];
    }
    
    //    [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionMiddle];
    
    //[(UIImageView*)[cell viewWithTag:72] setImage:[UIImage imageNamed:@"DefiFini"]];
    
    UIImageView * medailleView = (UIImageView*)[self viewWithTag:13];
    [medailleView setHidden:NO];
    //    [medailleView setClipsToBounds:NO];
    
    // Medailles
    if ( [message.medaille isEqualToString:@""])
        [medailleView setHidden:YES];
    else
        [medailleView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"medaille_%@", message.medaille]]];
    
    
    self.message  = message;
    //Tags
    if ( [message.getTags count] != 0 && [message.supprime_par intValue] == 0){
        self.collectionViewTag.dataSource = self;
        self.collectionViewTag.delegate = self;
        //        [collectionViewTag reloadData];
        [self.collectionViewTag setHidden:NO];
        //        [collectionViewTag setBackgroundColor:[UIColor redColor]];
        //        [collectionViewTag invalidateIntrinsicContentSize];
//        NSLayoutConstraint * constraint = self.collectionViewTag.constraints[0];
//        constraint.constant = [self getHeightForTags:message];
        //        [collectionViewTag.collectionViewLayout invalidateLayout];
        
        //[self.collectionViewTag setDelegate:self];
        //[self.collectionViewTag setDataSource:self];
    }else{
        [self.collectionViewTag setHidden:YES];
    }
    
    
    //Indicateur de progression
    UIActivityIndicatorView * activity_indicator = (UIActivityIndicatorView*)[self viewWithTag:100];
    
    if ([message.id_message integerValue] <= 0) {
        //        [cell.contentView makeToastActivity:@"center"];
        //        UIActivityIndicatorView * activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(50, 50, 40, 40)];
        //        [activity_indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        //        [activity_indicator setTag:2000];
        //        [cell addSubview:activity_indicator];
        [activity_indicator.layer setCornerRadius:18.5f];
        [activity_indicator startAnimating];
    }
    else{
        //        [activity_indicator stopAnimating];
        //        UIActivityIndicatorView * activity_indicator = (UIActivityIndicatorView*)[cell viewWithTag:2000];
        //
        //        if (activity_indicator) {
        //            [activity_indicator removeFromSuperview];
        //        }
        //        [cell.contentView hideToastActivity];
    }
    
    [self.pjMessageCollectionView reloadData];
    [self.collectionViewTag reloadData];
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

-(int) getHeightForTags:(Message *)__message{
    DLog(@"Message id : %@", message.id_message);
    NSString * tags = [__message.getTags componentsJoinedByString:@" "];
    
    CGSize stringSize = [tags sizeWithFont:[UIFont systemFontOfSize:15]
                         constrainedToSize:CGSizeMake(self.table_view_parent.contentSize.width-100,9999 )
                             lineBreakMode:NSLineBreakByWordWrapping];
    // Interstice en chaque ligne
    //    return stringSize.height+12*((stringSize.height/20)-1);
    return stringSize.height + 4;
}

- (void)setTableView:(UITableView*)table {
    self.table_view_parent = table;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//Méthode appelé après l'envoie d'un message pour ajouter la pj sans la télécharger
+(void) ajouteDataPieceJointeApresEnvoiePourEviterTelechargement:(PJMessage*)pj EtDonnees:(NSData*)data{
    
    //Si il n'y a aucune pj de récupéré on créer le tableau
    if(pjData == nil)
        pjData = [NSMutableDictionary new];
    
    //SECURITE : Si on passe vraiment une donnée on l'ajoute
    if(data != nil)
       [pjData setObject:data forKey:pj.nom_serveur];
}

+(void)clearCache{
    [pjData removeAllObjects];
}

//Méthode permettant d'afficher une image choisi en pleine écran
-(void) imagePleinEcran:(UIImage*) image{
    
    //Creation d'une nouvelle instance, si utilisée ailleur alors interet de centraliser dans le delegate pour éviter multiple instance
    UIStoryboard *st;
    
    if(IS_IPAD){
        st = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] getIPadStoryBoard];
        
    }else
        st = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] getIphoneStoryBoard];
        
    ZoomFSViewController * zoomFSVC = [st instantiateViewControllerWithIdentifier:@"ZoomFSVC"];
    
    zoomFSVC.image = image;
    zoomFSVC.contentSize = self.window.frame.size;
    zoomFSVC.delegate = self;
    
    if (IS_IPAD){
        //[[self.viewController sc_stackViewController] pushViewController:zoomFSVC atPosition:self.viewController.stackPosition unfold:YES animated:YES completion:nil];
//        [zoomFSVC setModalInPopover:YES];
        [zoomFSVC setModalPresentationStyle:UIModalPresentationFormSheet];
        [zoomFSVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        [self.viewController presentViewController:zoomFSVC animated:YES completion:nil];
    }
    else
        //[[self.viewController navigationController] pushViewController:zoomFSVC animated:YES];
        [self.viewController presentViewController:zoomFSVC animated:YES completion:nil];
}


//Méthode permettant d'afficher une image choisi en pleine écran
-(void) videoPleinEcran:(NSData*) video{
    
    //Creation d'une nouvelle instance, si utilisée ailleur alors interet de centraliser dans le delegate pour éviter multiple instance
//    UIStoryboard *st;
//
//    if(IS_IPAD){
//        st = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] getIPadStoryBoard];
//        
//    }else
//        st = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] getIphoneStoryBoard];
//    
//    VideoVC * videoVC = [st instantiateViewControllerWithIdentifier:@"VideoVC"];
//    
//    videoVC.video = video;
//
    
    // Do any additional setup after loading the view.
    NSString * temp_file = [NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.mp4"];
    [video writeToFile:temp_file atomically:YES];
    
    _videoVC = [[MPMoviePlayerViewController alloc] initWithContentURL: [NSURL fileURLWithPath:temp_file]];
    [[_videoVC moviePlayer] prepareToPlay];
    //    [videoVC setMovieSourceType:MPMovieSourceTypeFile];
//    [videoVC setFullscreen:NO];
//    [videoVC prepareToPlay];
//    [videoVC setControlStyle:MPMovieControlStyleEmbedded];

        //[[self.viewController navigationController] pushViewController:zoomFSVC animated:YES];
        [self.viewController presentMoviePlayerViewControllerAnimated:_videoVC];
    [[_videoVC moviePlayer] play];
}
-(void)ouvrirPJAvecAutreApp:(PJMessage*)pj avecData:(NSData*)data{
    NSURL * temp_file_path = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:pj.nom_original]];
    
    [data writeToURL:temp_file_path atomically:YES];
    
    _docController = [UIDocumentInteractionController interactionControllerWithURL:temp_file_path];
    [_docController setDelegate:self];
    
    BOOL will_display_some_options = [_docController presentOptionsMenuFromRect:self.pjMessageCollectionView.frame inView:self animated:YES];
    
    if (!will_display_some_options) {
        SIAlertView * alert = [[SIAlertView alloc] initWithTitle:@"Erreur" andMessage:@"Pas d'options disponibles pour ce type de fichier"];
        [alert addButtonWithTitle:@"Ok" type:SIAlertViewButtonTypeCancel handler:^(SIAlertView *alertView) {
            NSLog(@"Duck.");
        }];
        [alert show];
    }
    
}
//Méthode permetant d'afficher une pièce jointe en plein écran
//Ne gère que les images pour le moment
-(void) affichePleinEcranPJ:(PJMessage*)pj avecData:(NSData*)data{
    if([PJ estDuFormat:pj.extension] == Photo)
        [self imagePleinEcran:[UIImage imageWithData:data]];
    else if([PJ estDuFormat:pj.extension] == Video)
        [self videoPleinEcran:data];
    else
        [self ouvrirPJAvecAutreApp:pj avecData:data];
}

#pragma - Delegates Collection

-(void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller{
    NSError * error;
    
    [[NSFileManager defaultManager] removeItemAtPath:[controller.URL absoluteString] error:&error];

    if (error) {
        DLog(@"%@", error);
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *myCell;
    
    //Dans le cas de la collection d'aperçu
    if(collectionView == self.pjMessageCollectionView){
        PJMessage * pj = [self.pjTableau objectAtIndex:(indexPath.row)];
        
        DLog(@"Pj : %@ pour message : %@", pj.nom_serveur, message.id_message);
        
        myCell = [self.pjMessageCollectionView dequeueReusableCellWithReuseIdentifier:@"pjMessageCellTag" forIndexPath:indexPath];
        
        //A partir d'iOS 10, des fois, le thumbnail est caché. On sait pas trop pourquoi.
        [myCell setHidden:NO];
        
        //Si c'est une réponse on inverse l'affichage en droite -> gauche
        if(message.id_message_parent != nil && ![message.id_message_parent isEqualToNumber:@0] && self.modeMessage == ModeMessageCellNormal){
            
            [myCell.contentView setTransform:CGAffineTransformMakeScale(-1, 1)];
        }
        
        UIImageView *pjImageView = (UIImageView *)[myCell viewWithTag:110];
        
        //Si message est photo alors
        if([PJ estDuFormat:pj.extension] == Photo){
            if(pj.thumbnail == nil)
                pjImageView.image = [PJ imagePhotoParDefault];
            else
                pjImageView.image = [UIImage imageWithData:pj.thumbnail];

        //sinon si video
        }
        else if([PJ estDuFormat:pj.extension] == Video){
         
            //La video n'a pas été envoyée au serveur ...
            //=> pj.data contient donc la video en entiere et pas sa preview
            if(pj.thumbnail == nil){
                //Par default j'affiche une icone presto
                pjImageView.image = [UIImage imageNamed:@"video_24.png"];
            }
            else
                pjImageView.image = [UIImage imageWithData:pj.thumbnail];
            
        }
        else{

            pjImageView.image = [UIImage imageWithData:pj.thumbnail];
        }
    }else{ //Cas des tags
        
        myCell = [self.collectionViewTag
                  dequeueReusableCellWithReuseIdentifier:@"cellTag"
                  forIndexPath:indexPath];
        
        UILabel * tagLabel = (UILabel*)[myCell viewWithTag:101];
        [tagLabel setText:[@"#" stringByAppendingString:message.getTags[indexPath.row]]];
        UIImageView * image = (UIImageView*)[myCell viewWithTag:102];
        [image setImage:[[UIImage imageNamed:@"tag.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(6.0f, 6.0f, 6.0f, 6.0f)]];
    }
    
    return myCell;
}

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == self.pjMessageCollectionView)
        return self.pjTableau.count;
    else
        return  message.getTags.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
//    CGSize stringSize = [[@"#" stringByAppendingString:message.getTags[indexPath.row]] sizeWithFont:[UIFont systemFontOfSize:15]
//                          constrainedToSize:CGSizeMake(9999, 20)
//                              lineBreakMode:NSLineBreakByWordWrapping];
//

    if(collectionView == self.pjMessageCollectionView)
        return CGSizeMake(45, 45);
    
    
    CGRect stringSize = [[@"#" stringByAppendingString:message.getTags[indexPath.row]] boundingRectWithSize:CGSizeMake(142, 20)
                                options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:15]}
                                                                                                    context:nil];
    return CGSizeMake(stringSize.size.width+10, 20);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5.0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //Lors de la selection d'un aperçu
    if(collectionView == self.pjMessageCollectionView){
        
        PJMessage * pj = [self.pjTableau objectAtIndex:(indexPath.row)];
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        
        //Si on a pas de connection, on affiche la thumbnail en plus gros
        if (![UIViewController estLieAuServeur] && pj.thumbnail) {
            [self affichePleinEcranPJ:pj avecData:pj.thumbnail];
            [[[UIApplication sharedApplication] keyWindow] makeToast:[LanguageManager get:@"label_pj_basse_qualite"] duration:4 position:@"bottom" title:[LanguageManager get:@"label_astuce"] image:[UIImage imageNamed:@"bulb_off_48"] style:nil completion:nil];
        }
        //Cas ou le message n'est pas synchro donc dans pj.data on a les données = pas besoin de télécharger
        else if([message.id_message integerValue] < 0){
            [self affichePleinEcranPJ:pj avecData:pj.thumbnail];
        }
        
        else if([pjData objectForKey:pj.nom_serveur] == nil){
            
            //On affiche un jolie spinner tournant pour dire qu'on mouline
            UIActivityIndicatorView *activityIndicator;
            activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            activityIndicator.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
            [activityIndicator setBackgroundColor:[UIColor blackColor]];
            [activityIndicator.layer setOpacity:0.6];
            [cell addSubview: activityIndicator];
            [activityIndicator startAnimating];
            
            //On télécharge la pièce jointe
            [UtilsCore telechargePJ:pj WithCallback:^(BOOL status, NSData* data) {
                
                //On stop le moulinage car on a l'image
                [activityIndicator stopAnimating];
                [activityIndicator removeFromSuperview];
                
                if(status){
                    //Reussite et recup des données
                    [pjData setObject:data forKey:pj.nom_serveur];
                    //pjImageView.image = [UIImage imageWithData:pj.data];
                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
                    
                    [self affichePleinEcranPJ:pj avecData:pjData[pj.nom_serveur]];
                }
                else { //Erreur
                    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Erreur" andMessage:@"Impossible de récupérer la pièce jointe du serveur"];
                    
                    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                                             type:SIAlertViewButtonTypeCancel
                                          handler:^(SIAlertView *alertView) {
                                              
                                          }];
                    
                    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
                    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
                    
                    [alertView show];

                }
            }];
        }else if([pjData objectForKey:pj.nom_serveur] != nil){ //Si on a déjà télécharger
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            [self affichePleinEcranPJ:pj avecData:pjData[pj.nom_serveur]];
        }
            
        
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

#pragma - delegate ZoomFSVC

//Permet de fermer la fênetre affichant l'image en plein écran
-(void)dismissZoomFS{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

@end
