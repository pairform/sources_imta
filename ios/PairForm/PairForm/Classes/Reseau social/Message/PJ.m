//
//  PJ.m
//  PairForm
//
//  Created by vincent on 22/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "PJ.h"

//Classe contenant des méthodes génériques pour gérer les pjs
@implementation PJ

//renvoi une image défaut pour un son
+(UIImage*)imageSonParDefault{
    return [UIImage imageNamed:@"sound_64.png"];
}

//renvoi une image défaut pour une image
+(UIImage*)imagePhotoParDefault{
    return [UIImage imageNamed:@"photo_64.png"];
}

//renvoi une image défaut pour une vidéo
+(UIImage*)imageVideoParDefault{
    return [UIImage imageNamed:@"video_64.png"];
}

//renvoi une image défaut pour un document
+(UIImage*)imageFichierParDefault{
    return [UIImage imageNamed:@"document_64.png"];
}

//Accesseur général sur les informations des PJ
//Taille maximum pour toutes les pjs d'un message
+(long)tailleTotalMaxPJ{
    //On recupère le rang de la capsule demandée
    NSArray * pj = [PJ getPJData];
    
    return [[(NSDictionary*)pj[0] objectForKey:@"max_taille"] longValue];
}

//Nombre total de pj pour un message
+(int)nombreTotalMaxPJ{
    NSArray * pj = [PJ getPJData];
    
    return [[(NSDictionary*)pj[0] objectForKey:@"nb_pj"] integerValue];
}

//Taille en pixel pour le carré du thumbnail
+(int)tailleThumbnail{
    NSArray * pj = [PJ getPJData];
    
    return [[(NSDictionary*)pj[0] objectForKey:@"thumbnail_taille"] integerValue];
}

+(NSArray*)getPJData {
    NSArray * pj_data = [UIViewController getSessionValueForKey:@"pj_data"];
    
    if (!pj_data) {
        pj_data = [NSJSONSerialization JSONObjectWithData:[kPJDataDefault dataUsingEncoding:NSUTF8StringEncoding]
                                                  options:nil error:nil];
    }
    
    
    return pj_data;
}
//Formats autorisés
+(NSArray*)formatAuthoriseVideo{
    return [PJ formatAuthoriseAvecType:@"Video"];
}

+(NSArray*)formatAuthorisePhoto{
    return [PJ formatAuthoriseAvecType:@"Photo"];
}

+(NSArray*)formatAuthoriseFichier{
    return [PJ formatAuthoriseAvecType:@"Fichier"];
}

+(NSArray*)formatAuthoriseSon{
    return [PJ formatAuthoriseAvecType:@"Son"];
}

//Méthode renvoyant un tableau des format valide selon un paramètre : Video, Photo, ...
+(NSArray*)formatAuthoriseAvecType:(NSString*)type{
    NSArray * donneePJ = [PJ getPJData];
    
    NSArray * formats = donneePJ[0][@"format"] ;
    __block NSArray* format;
    
    
    [formats enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj[@"type"] isEqual:type]) {
            
            format = obj[@"valeur"];
            
            stop = YES;
        }
    }];
    
    return format;
}

//Méthode renvoyant un booléen représentant l'autorisation d'un format pour un type de pj
+(BOOL)formatEstAuthorise:(PJType)typePJ format:(NSString*)format{
    __block BOOL contient = false;
    NSArray *resultat;
    
    switch (typePJ){
        case Video:
            resultat = [PJ formatAuthoriseVideo];
            break;
        case Photo:
            resultat = [PJ formatAuthorisePhoto];
            break;
        case Fichier:
            resultat = [PJ formatAuthoriseFichier];
            break;
        case Son:
            resultat = [PJ formatAuthoriseSon];
            break;
        default :
            resultat = @[];
    }
    
    [resultat enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL *stop) {
        if([[obj uppercaseString] isEqual:[format uppercaseString]]){
            contient = true;
            stop = YES;
        }
    }];
    
    return contient;
}

//Méthode renvoyant le type correspondant à une extension
+(PJType)estDuFormat:(NSString*)extension{
    
    if([[PJ formatAuthoriseVideo] containsObject:[extension lowercaseString]])
        return Video;
    if([[PJ formatAuthorisePhoto] containsObject:[extension lowercaseString]])
        return Photo;
    if([[PJ formatAuthoriseFichier] containsObject:[extension lowercaseString]])
        return Fichier;
    if([[PJ formatAuthoriseSon] containsObject:[extension lowercaseString]])
        return Son;
    
    //Par défaut c'est un fichier
    return Fichier;
}


//Renvoie le mimetype pour le header de requete serveur selon l'extension de la piece jointe
+ (NSString *)mimeTypePourExtension:(NSString *)_extension
{
    CFStringRef extension = (__bridge CFStringRef)_extension;
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

//Méthode renvoyant les données correspondant à un aperçu d'un type particulier
+ (NSData*)renvoieDataPourThumbnailDeLextension:(NSString*) extension EtDesData:(NSData*)data{
    
    NSData* resultat;
    
    switch ([PJ estDuFormat:extension]) {
        case Photo:
            return [self renvoieDataPourThumbnailPhotoAvecExtension:extension EtData:data];
            break;
        case Video:
            return [self renvoieDataPourThumbnailPhotoAvecExtension:extension EtData:data];

            
            break;
        case Fichier:
            
            break;
        default:
            break;
    }
    
    return resultat;
}

//Méthode renvoyant un aperçu pour une vidéo
+(UIImage*)thumbnailSynchroneAPartirDeVideoUrl:(NSURL*)url{
    int tailleThumbnail = [PJ tailleThumbnail];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(3.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    
    return [thumb resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(tailleThumbnail, tailleThumbnail) interpolationQuality:kCGInterpolationNone];
    
    //return [UIImage imageNamed:@"video_24.png"];
}

//Méthode asynchrone renvoyant un aperçu pour une vidéo
+(void)imageAsynchroneAPartirDeVideoUrl:(NSURL*)url WithHandler:(void (^)(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error)) handler{
    int tailleThumbnail = [PJ tailleThumbnail];
    AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform=TRUE;
    CMTime thumbTime = CMTimeMakeWithSeconds(3.0,30);
    
    CGSize maxSize = CGSizeMake(tailleThumbnail, tailleThumbnail);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
}

//Méthode renvoyant un aperçu pour une image
+(NSData*)renvoieDataPourThumbnailPhotoAvecExtension:(NSString*)extension EtData:(NSData*) data{
    int tailleThumbnail = [PJ tailleThumbnail];
    UIImage * thumbnail = [[UIImage imageWithData:data] resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(tailleThumbnail, tailleThumbnail) interpolationQuality:kCGInterpolationNone];
    
    if([[extension lowercaseString] isEqualToString:@"jpg"]){
        return UIImageJPEGRepresentation(thumbnail, 0.7);
    }else if([[extension lowercaseString] isEqualToString:@"png"]){
        return UIImagePNGRepresentation(thumbnail);
    }else//Par défault JPEG image
        return UIImageJPEGRepresentation(thumbnail, 0.7);
    
    return nil;
}

+ (AVAssetExportSession*)convertirVideoEnMp4AvecURL:(NSURL*)inputURL
                                     handler:(void (^)(AVAssetExportSession*))callback
{
    
    NSArray *documentsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [documentsDirectory objectAtIndex:0];
    
    NSString *videoName = [[[inputURL lastPathComponent] stringByDeletingPathExtension] stringByAppendingString:@".mp4"];
    NSString *videoPath = [docPath stringByAppendingPathComponent:videoName];
    NSURL * outputURL = [NSURL fileURLWithPath:videoPath];
    
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         callback(exportSession);
     }];
    return exportSession;
}
@end
