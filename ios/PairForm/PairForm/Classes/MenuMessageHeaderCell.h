//
//  MenuMessageHeaderCell.h
//  PairForm
//
//  Created by Maen Juganaikloo on 16/12/2016.
//  Copyright © 2016 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuMessageHeaderCell : UITableViewCell

//Boutons du menu
@property (weak, nonatomic) IBOutlet UIButton * bouton_menu_carte;
@property (weak, nonatomic) IBOutlet UIButton * bouton_menu_tri;
@property (weak, nonatomic) IBOutlet UIButton * bouton_menu_recherche;
@property (weak, nonatomic) IBOutlet UIButton * bouton_menu_ecrire;

@end
