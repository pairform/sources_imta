//
//  TelechargementVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ressource.h"
#import "CapsuleCell.h"

@interface TelechargementVC : UIViewController <UITableViewDataSource, UITableViewDelegate, CapsuleCellDelegate>

@property (nonatomic, strong) Ressource * currentRessource;
@property (nonatomic, strong) NSArray * capsules;

@property (nonatomic) BOOL isUpdate;

@property (nonatomic, weak) IBOutlet UIScrollView * panel_infos;

@property (nonatomic, weak) IBOutlet UITableView * panel_sections;

@property (nonatomic, weak) IBOutlet UITableView * panel_objectifs;

@property (nonatomic, weak) IBOutlet UIImageView * iconeRes;
@property (nonatomic, weak) IBOutlet UILabel * nomCourt;
@property (nonatomic, weak) IBOutlet UILabel * nomLong;
@property (nonatomic, weak) IBOutlet UILabel * etablissement;
@property (nonatomic, weak) IBOutlet UILabel * theme;
@property (nonatomic, weak) IBOutlet UITextView * description_res;
@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet UIView * contentView;
@property (weak, nonatomic) IBOutlet UIImageView *langue;


@property (nonatomic, weak) IBOutlet UIButton * actionButton;

@property (nonatomic, strong) NSIndexPath * expanded_cell_index;
@property (nonatomic) BOOL isLocalModeOnly;

-(IBAction)telechargerToutesLesCapsules:(id)sender;
-(void)updateButtonComparingLocalDate:(NSDate*)localDate distantDate:(NSDate*)distantDate;
@end
