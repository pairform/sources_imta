//
//  Categorie.m
//  SupCast
//
//  Created by Cape EMN on 07/12/11.
//  Copyright (c) 2011 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "Categorie.h"

@implementation Categorie
//@synthesize liste;
@synthesize listeThemes;
@synthesize listeEtablissements;
@synthesize receivedData;
@synthesize statusConnexionReseau;
@synthesize HUD;
@synthesize categorieConnection;
@synthesize password;
@synthesize niveau;
@synthesize username;
@synthesize estVisiteur;
-(NetworkStatus) etatConnexionReseau {
    self.statusConnexionReseau = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    return self.statusConnexionReseau;
}


- (BOOL)connected 
{
    if (NotReachable== (BOOL)[self etatConnexionReseau])
        return NO;    
    else
        return YES;    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    DLog(@"");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    aTableView.backgroundColor = [UIColor clearColor];
    aTableView.backgroundView = nil;
    aTableView.rowHeight = kCategorieRowHeight;
    //Communication avec le serveur recuperer la liste des themes
    
    NSString *urlConnexion = [[NSString alloc] initWithFormat:@"%@%@",kURLBase,kCategories];
    NSURL *url = [[NSURL alloc] initWithString:urlConnexion];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *paramDataString = [NSString stringWithFormat:
                                 @"id=%@&pseudo=%@&password=%@",kKey,self.username,self.password];
    
    NSData *paramData = [paramDataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:paramData];
    
    self.categorieConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(self.categorieConnection) {
        
        //[indicator startAnimating];
        //[indicator setHidden:NO];
        [self showLoadingProgress];

        NSMutableData *data = [[NSMutableData alloc] init];
        self.receivedData = data;
        
    }
    else 
    {
        [self hideLoadingProgress];
        [self cancelConnection];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention !"
                                                        message:@"Erreur de connexion"
                                                       delegate:self
                                              cancelButtonTitle:@"Accepter"
                                              otherButtonTitles:nil];
        
        [alert show];
        
    }
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    
    aTableView = nil;
    self.categorieConnection = nil;
    self.HUD = nil;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [labelTitre setShadowColor:[UIColor lightGrayColor]];
    [labelTitre setShadowOffset:CGSizeMake(-1, -1)];
    
    switch ([categorieSelector selectedSegmentIndex]) {
        case 0:                
            [labelTitre setText:@"Thèmes"];
            break;
        case 1:
            [labelTitre setText:@"Etablissements"];
            break;
        default:
            break;
    }
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self hideLoadingProgress];
    [self cancelConnection];
    [super viewWillDisappear:animated];
}

- (void)cancelConnection {
	DLog(@"cancelConnection");
	if (self.categorieConnection) {
		[self.categorieConnection cancel];
		self.categorieConnection = nil;
	}
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

//TODONE: enregistrerImages
-(void)enregistrerImages
{
    for (int i =0; i < [listeEtablissements count]; i++) 
    {
        NSString * imageName = [[self.listeEtablissements objectAtIndex:i] objectForKey:kIcone];
        NSString * imageId = [[self.listeEtablissements objectAtIndex:i] objectForKey:@"etablissement"];
        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageName]]];
        
        NSString *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.png",imageId]];
        
        [UIImagePNGRepresentation(image) writeToFile:imagePath atomically:YES];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    DLog(@"");
    switch ([categorieSelector selectedSegmentIndex])
    {
        case 0:                
            return [self.listeThemes count];
            break;
        case 1:
            return [self.listeEtablissements count];
            break;
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:10];
    int compteur;
    switch ([categorieSelector selectedSegmentIndex])
    {
        case 0:  
        {
            cell.textLabel.text = [[self.listeThemes objectAtIndex:indexPath.row] objectForKey:kDomaine];
            compteur = 0;
            compteur = [[[self.listeThemes objectAtIndex:indexPath.row] valueForKey:kCount] intValue];
            switch (compteur) {
                case 0:
                    cell.detailTextLabel.text = @"Pas de ressource disponible.";
                    break;
                case 1:
                    cell.detailTextLabel.text = [[NSString alloc] initWithFormat:@"%d ressource disponible.",compteur];
                    break;
                default:
                    cell.detailTextLabel.text = [[NSString alloc] initWithFormat:@"%d ressources disponibles.",compteur];
                    break;
            }
            
            
            UIImage *imageTheme = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[self.listeThemes objectAtIndex:indexPath.row] objectForKey:kIcone]]]];
            
            UIImage * image1;
            if (imageTheme.size.width != kAppIconHeight && imageTheme.size.height != kAppIconHeight)
            {
                CGSize itemSize = CGSizeMake(kAppIconHeight, kAppIconHeight);
                UIGraphicsBeginImageContextWithOptions(itemSize, NO, [UIScreen mainScreen].scale);
                CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
                [imageTheme drawInRect:imageRect];
                image1 = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }
            else
            {
                image1 = imageTheme;
            }
            
            cell.imageView.image = image1; 
            
            break;
        }
        case 1:
        {
            cell.textLabel.text = [[self.listeEtablissements objectAtIndex:indexPath.row] objectForKey:kEtablissement];
            compteur = 0;
            compteur = [[[self.listeEtablissements objectAtIndex:indexPath.row] valueForKey:kCount] intValue];
            switch (compteur) {
                case 0:
                    cell.detailTextLabel.text = @"Pas de ressource disponible.";
                    break;
                case 1:
                    cell.detailTextLabel.text = [[NSString alloc] initWithFormat:@"%d ressource disponible.",compteur];
                    break;
                default:
                    cell.detailTextLabel.text = [[NSString alloc] initWithFormat:@"%d ressources disponibles.",compteur];
                    break;
            }
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[self.listeEtablissements objectAtIndex:indexPath.row] objectForKey:kIcone]]]];
            
            UIImage * image2;
            
            if (image.size.width > image.size.height)
            {    
                if (image.size.width != kAppIconWidth && image.size.height != kAppIconHeight)
                {
                    CGSize itemSize = CGSizeMake(kAppIconWidth, kAppIconWidth * (image.size.height/image.size.width));
                    UIGraphicsBeginImageContextWithOptions(itemSize, NO, [UIScreen mainScreen].scale);
                    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width , itemSize.height);
                    [image drawInRect:imageRect];
                    image2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                }
                else
                {
                    image2 = image;
                }
            }
            else
            {
                if (image.size.width != kAppIconHeight && image.size.height != kAppIconHeight)
                {
                    CGSize itemSize = CGSizeMake(kAppIconHeight, kAppIconHeight);
                    UIGraphicsBeginImageContextWithOptions(itemSize, NO, [UIScreen mainScreen].scale);
                    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
                    [image drawInRect:imageRect];
                    image2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                }
                
                else
                {
                    image2 = image;
                }
            }    
            cell.imageView.image = image2; 
            
            
            
            break;
        }
        default:
        {
            break;
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	DLog(@"");
	aAppListViewController = [AppListViewController alloc];	
	aAppListViewController.username = self.username;
	aAppListViewController.password = self.password;
	aAppListViewController.niveau = self.niveau;
    
    (void)[aAppListViewController initWithNibName:@"AppListViewController" bundle:nil];	
    
    switch ([categorieSelector selectedSegmentIndex]) {
        case 0:                
            [aAppListViewController setTitle:[[self.listeThemes objectAtIndex:indexPath.row] objectForKey:kDomaine]];
            aAppListViewController.critere = [self.listeThemes objectAtIndex:indexPath.row];
            break;
        case 1:
            [aAppListViewController setTitle:[[self.listeEtablissements objectAtIndex:indexPath.row] objectForKey:kEtablissement]];
            aAppListViewController.critere = [self.listeEtablissements objectAtIndex:indexPath.row];
            break;
        default:
            break;
    }
    
    [self.navigationController pushViewController:aAppListViewController animated:YES];
    
}

- (IBAction)changeCategorie:(id)sender {
    
    DLog(@"");
    
    [categorieSelector selectedSegmentIndex];
    
    switch ([(UISegmentedControl *) sender selectedSegmentIndex]) {
        case 0:                
            [labelTitre setText:@"Thèmes"];
            /*
            [aTableView numberOfRowsInSection:[self.listeThemes count]];
            [aTableView reloadData];
            */
            [aTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
            break;
        case 1:
            [labelTitre setText:@"Etablissements"];
            /*
            [aTableView numberOfRowsInSection:[self.listeEtablissements count]];
            [aTableView reloadData];
             */
            [aTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            break;
        default:
            break;
    }
    
}

-(void)reloadDataWithAnimation:(UITableView*)tableView{
    [tableView reloadData];
    
    
}
#pragma mark "HUD" chargement en cours

- (void)showLoadingProgress {
	if (HUD==nil) {
		DLog(@"showLoadingProgress");
		UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
		CGRect windowFrame = mainWindow.frame;
		DLog(@"window=%@", [mainWindow description]);
		CGRect progressHUDrect = CGRectMake(windowFrame.origin.x,
											windowFrame.origin.y, 
											windowFrame.size.width,
											windowFrame.size.height);
		progressHUDrect.size.height = 120.0;
		progressHUDrect.origin.y = (windowFrame.size.height / 2.0) - (progressHUDrect.size.height / 2);
		progressHUDrect.origin.x = 80.0;
		progressHUDrect.size.width = windowFrame.size.width - (progressHUDrect.origin.x * 2);
        UIInterfaceOrientation orient = [self interfaceOrientation];
        CGAffineTransform myT;
        if (orient == UIInterfaceOrientationLandscapeLeft)
        {
            myT = CGAffineTransformMakeRotation(M_PI*1.5);
        } 
        else if (orient == UIInterfaceOrientationLandscapeRight) 
        {
            myT =  CGAffineTransformMakeRotation(M_PI/2);
        } 
        else if (orient == UIInterfaceOrientationPortraitUpsideDown)
        {
            myT =  CGAffineTransformMakeRotation(-M_PI);
        }
        else
        {
            myT =  CGAffineTransformIdentity;
        }
        
        
        self.HUD = [[WBProgressHUD alloc] initWithFrame:progressHUDrect];
        self.HUD.transform = myT; 
		HUD.colorBackground = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.30];
		DLog(@"alloc OK : HUD=%@", [HUD description]);
		[HUD setText:@"Chargement …"];
		[HUD showInView:mainWindow];
	}
}

- (void)hideLoadingProgress {
	DLog(@"hideLoadingProgress");
	if (HUD) {
		HUD.hidden = YES;
		[HUD removeFromSuperview];
		self.HUD = nil;
	}
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if (!(HUD.hidden))
    {
        CGAffineTransform myT;
        if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            myT = CGAffineTransformMakeRotation(M_PI*1.5);
        } 
        else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) 
        {
            myT =  CGAffineTransformMakeRotation(M_PI/2);
        } 
        else if (toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            myT =  CGAffineTransformMakeRotation(-M_PI);
        }
        else
        {
            myT =  CGAffineTransformIdentity;
        }
        
        self.HUD.transform = myT; 
        
    }
}



#pragma mark -
#pragma mark NSURLConnection Callbacks delegate
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	//[indicator setHidden:YES];	
	//[indicator stopAnimating];	
    [self hideLoadingProgress];

	self.receivedData = nil;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de Connexion !"
													message:[NSString stringWithFormat:@"Connexion refusée : %@",
															 [error localizedDescription]]
												   delegate:self
										  cancelButtonTitle:@"Accepter"
										  otherButtonTitles:nil];
	
	[alert show];
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
	//[indicator setHidden:YES];	
	//[indicator stopAnimating];	
    [self hideLoadingProgress];

	
	
    NSString *error; 
    NSPropertyListFormat format;
    
    // Initialisation des listes Themes et Etablissements
    self.listeThemes = nil;
    self.listeEtablissements = nil;
    NSArray * liste = [[NSMutableArray alloc] init];
    self.listeThemes = [[NSMutableArray alloc] init];
    self.listeEtablissements = [[NSMutableArray alloc] init];
    // serialisation du fichier Themes.plist en dictionnaire    
    liste = [NSPropertyListSerialization propertyListFromData:self.receivedData
                                             mutabilityOption:NSPropertyListImmutable
                                                       format:&format
                                             errorDescription:&error];
    
    
    if(!liste)
    { 
        DLog(@"Error: %@",error); 
    }
    
    DLog(@"%@",[liste objectAtIndex:0]);
    DLog(@"%@",[liste objectAtIndex:1]);
    
    // enumeration de la liste des Themes    
    for (NSDictionary * theme in [[liste objectAtIndex:0] objectForKey:kDomaines]) 
    {
        DLog(@"%@",theme);
        [self.listeThemes addObject:theme];
    }
    // enumeration de la liste des Etablissements    
    for (NSDictionary * etablissement in [[liste objectAtIndex:1] objectForKey:kEtablissements]) 
    {
        DLog(@"%@",etablissement);
        [self.listeEtablissements addObject:etablissement];
    }
    
    [self enregistrerImages];
    [aTableView reloadData];
}

@end
