//
//  Telechargement.m
//  SupCast
//
//  Created by Didier PUTMAN on 25/11/10.
//  Copyright (c) 2010 CAPE - Ecole des Mines de Nantes. All rights reserved.
//
#import "Telechargement.h"
#include <sys/xattr.h>

@implementation Telechargement

@synthesize myDico;
@synthesize maFileOperation;
@synthesize topTelechargement;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil 
			   bundle:(NSBundle *)nibBundleOrNil
{
    
    DLog(@"");
    
	self = [super initWithNibName:nibNameOrNil
                           bundle:nibBundleOrNil];
    
    if (self)
    {
        // Custom initialization
		self.title = [self.myDico objectForKey:kRessourceTitleStr];
    }
    
    return self;
}

#pragma mark - view life cycle methods

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    DLog(@"");
	[super viewDidLoad];	
    averageBandwidth = 0.0;
    dataBandwidth = 0;
    //-----------------------------------------------------------------------
	//creation de la queue
	//-----------------------------------------------------------------------
    self.maFileOperation = [[NSOperationQueue alloc] init];
    [self.maFileOperation setMaxConcurrentOperationCount:1];
    self.topTelechargement = YES;
	//-----------------------------------------------------------------------
	//affichage de la textview
	//-----------------------------------------------------------------------
	aTextView.text = [self.myDico objectForKey:kRessourceSummaryStr] ;
    /*UIImage *imageBouton = [UIImage imageNamed:@"whiteButton.png"];
	UIImage *stretchableBouton = [imageBouton stretchableImageWithLeftCapWidth:12 topCapHeight:0];
	[boutonChargement setBackgroundImage:stretchableBouton forState:UIControlStateNormal];
    */
    NSNumber * ressourcetaille = [myDico objectForKey:kRessourceTailleStr];
    [boutonChargement setTitle:[[NSString alloc] initWithFormat:@"Téléchargement (%@ Mo)",[ressourcetaille stringValue]] forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(downloadStoppedNotification:)
                                                 name:kDownloadStoppedNotification
                                               object:nil];
    
    
    
}

-(void)downloadStoppedNotification:(NSNotification *)notif
{
    
    NSDictionary * monDico = [notif object];
    if (([self.maFileOperation.name isEqualToString:[monDico objectForKey:kRessourceNomStr]])
        &&
        ([[self.myDico objectForKey:kRessourceIdStr] isEqualToString:[monDico objectForKey:kRessourceIdStr]]))
    {   
        DLog(@"myDico at stop is %@,%@,%@,%d",monDico,self.myDico,self.maFileOperation.name,[self.maFileOperation operationCount]);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;  
        
        self.myDico = [[NSMutableDictionary alloc] initWithDictionary:monDico];
        
        if (self.maFileOperation)
        {
            [self.maFileOperation cancelAllOperations];
            self.topTelechargement = NO;
            
        }
    }
}

- (IBAction)chargement:(id)sender
{   
    DLog(@"");
    nombredetelechargement = 0;
    nombredefichieratelecharger = 0;
    NSDictionary * monDico = [[NSDictionary alloc] initWithDictionary:[self verif]]; 
    if ([monDico count] == 0)
    {
        [self lancement];        
    }
    else
    {
        NSString * monStatus = [[NSString alloc] initWithString:[monDico objectForKey:kStatus]];
        if ([monStatus isEqualToString:kTermine])
        {   
            //On cherche dans la mémoire de l'app si la ressource a une mise à jour
            NSString * key = [[NSString alloc] initWithFormat:@"pop_%@",[monDico objectForKey:kRessourceIdStr]];
            
            //Si non,
            if([[NSUserDefaults standardUserDefaults] boolForKey:key] == NO)
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"La ressource est déjà présente." 
                                                          message:@"Voulez-vous la recharger à nouveau ?" 
                                                         delegate:self 
                                                cancelButtonTitle:@"Oui" 
                                                otherButtonTitles:@"Non",nil];
                [alert show];
            }
            else [self lancement];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"La ressource est en cours de téléchargement." 
                                                          message:@"Veuillez devez attendre la fin du téléchargement." 
                                                         delegate:nil
                                                cancelButtonTitle:@"Accepter"
                                                otherButtonTitles:nil];
            [alert show];
            
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    DLog(@"buttonIndex is %d",buttonIndex);
	
	if(buttonIndex == 0) 
    {
        [self lancement];        
		
	}
    
}

-(void)lancement
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Le téléchagement va se dérouler en arrière-plan." 
                                                  message:@"" 
                                                 delegate:nil 
                                        cancelButtonTitle:@"Accepter" 
                                        otherButtonTitles:nil,nil];
    [alert show];
    
    [boutonChargement setEnabled:NO];
    
    [self.maFileOperation setName:[self.myDico objectForKey:kRessourceNomStr]]; 
    
    
    // recuperation du dossier documents de l'application
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
	NSArray *cachePaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [cachePaths objectAtIndex:0];
    NSError * error;
    NSString *subDirectory = [cachesDirectory stringByAppendingPathComponent:[self.myDico objectForKey:kRessourceFolderStr]];
    if (!([[NSFileManager defaultManager] removeItemAtPath:subDirectory error:&error]))
    {
        DLog(@"dossier ressource %@ erreur a la suppression %@",subDirectory,[error localizedDescription]);
    }

    
    [self.maFileOperation addOperation:[[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(lancerRecup1:)
                                        object:nil]
     ];
    
}


- (NSDictionary *)verif
{
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDictionary *monDicoVide = [[NSDictionary alloc] initWithObjectsAndKeys:nil];
    // ======================================================================
    // verification de presence de du support telecharge dans le fichier liste des supports téléchargés  	
    // ======================================================================  
    // initialisation  	
    NSMutableArray *array = [[NSMutableArray alloc] init ];
    
    //========================================================  
    // récuperation du path du fichier de liste de support  
    NSString *filePathListe = [documentsDirectory stringByAppendingPathComponent:kSupportFilename];
    
    //  vérification de l'existence du fichier de liste de support 
    //  et recuperation du contenu dans le tableau 
    if([[NSFileManager defaultManager] fileExistsAtPath:filePathListe])
        array = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
    else		
        return monDicoVide;
    
    // Si le tableau est vide initialisation avec la nouvelle reference
    // sinon recherche de la presence de la reference de le tableau
    ////NSLog(@"array=%@",array);
    if (array == nil || ([array count] == 0)) 
    {
        return monDicoVide;
    }
    else
    {
        // recherche presence support
        NSString * newID = [self.myDico objectForKey:kRessourceIdStr];
        for (NSDictionary *monDicoRef in  array)
        {
            NSString * monID = [monDicoRef objectForKey:kRessourceIdStr];
            if ( [monID isEqualToString:newID]) 
            {
                return monDicoRef;
                break;
            }
        }
        
    }
    
    return monDicoVide;
    
}
-(void)addSkipBackupAttributeToPath:(NSString*)path {
    u_int8_t b = 1;
    setxattr([path fileSystemRepresentation], "com.apple.MobileBackup", &b, 1, 0, 0);
}

- (void)lancerRecup1:(NSString *)motcle
{   
    DLog(@"");
 	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    @autoreleasepool {
    
        NSFileManager * monManageur = [NSFileManager defaultManager];
        
        // recuperation du dossier documents de l'application
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
	NSArray *cachePaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *cachesDirectory = [cachePaths objectAtIndex:0];
        
        // path du dossier de la ressource
        NSString *subDirectory = [cachesDirectory stringByAppendingPathComponent:[self.myDico objectForKey:kRessourceFolderStr]];
        DLog(@"subDirectory is %@",subDirectory);
        // creation du dossier de la ressource
        BOOL isDir=NO;
        NSError * error;
        if (!([monManageur fileExistsAtPath:subDirectory isDirectory:&isDir] && isDir))
        {

            if (!([monManageur createDirectoryAtPath:subDirectory withIntermediateDirectories:YES attributes:nil error:&error]))
            {
                DLog(@"dossier ressource %@ erreur a la creation %@",subDirectory,[error localizedDescription]);
            }
            else {
                [self addSkipBackupAttributeToPath:subDirectory];

            }
            
        }
        else 
        {
            if (!([monManageur removeItemAtPath:subDirectory error:&error]))
            {
                DLog(@"dossier ressource %@ erreur a la suppression %@",subDirectory,[error localizedDescription]);
            }
          

            if (!([monManageur createDirectoryAtPath:subDirectory withIntermediateDirectories:YES attributes:nil error:&error]))
            {
                DLog(@"dossier ressource %@ erreur a la creation %@",subDirectory,[error localizedDescription]);
            }
            else {
                [self addSkipBackupAttributeToPath:subDirectory];
            }
            
        }
        
        //-------------------------------------------------------------------
	//-fichier kRessourceFichierParent
	//-------------------------------------------------------------------
        NSString * nomFichierParent = [self telechargementFichier:subDirectory key:kRessourceFichierParent];
        //-------------------------------------------------------------------
	//-fichier structure
	//-------------------------------------------------------------------
        NSString * nomFichierStructure = [self telechargementFichier:subDirectory key:kRessourceStructureStr];
        //-------------------------------------------------------------------
        //-fichier image icone
        //-------------------------------------------------------------------
        NSString * nomFichierIcon = [self telechargementFichier:subDirectory key:kRessourceIconeStr];    
        //-------------------------------------------------------------------
        //-fichier media Web
        //-------------------------------------------------------------------
        NSString * nomFichierWeb = [self telechargementFichier:subDirectory key:kRessourceWebStr];    
        if (    (nomFichierParent == nil)
            || (nomFichierStructure == nil)
            || (nomFichierIcon == nil)
            || (nomFichierWeb == nil))
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"La ressource est momentanément indisponible." 
                                                          message:@"Veuillez essayer ultérieurement." 
                                                         delegate:nil 
                                                cancelButtonTitle:@"Accepter" 
                                                otherButtonTitles:nil];
            [alert show];
            
            
            
        }
        else
        {    
            // ======================================================================
            // ajoute la reference du support telecharge dans le fichier liste des supports téléchargés  	
            // ======================================================================  
            // initialisation  	
            NSMutableArray *array;
            array = nil;
            
            // ajout de la reference du support telecharge dans le dictionnaire	
            
            //========================================================  
            // récuperation du path du fichier de liste de support  
            NSString *filePathListe = [documentsDirectory stringByAppendingPathComponent:kSupportFilename];
            
            //  vérification de l'existence du fichier de liste de support 
            //  et recuperation du contenu dans le tableau 
            if([monManageur fileExistsAtPath:filePathListe])
                array = [[NSMutableArray alloc] initWithContentsOfFile:filePathListe];
            else		
                array = nil;
            
            // Si le tableau est vide initialisation avec la nouvelle reference
            // sinon recherche de la presence de la reference de le tableau
            ////NSLog(@"array=%@",array);
            if (array == nil || ([array count] == 0)) 
            {
                array = [[NSMutableArray alloc] initWithObjects:[[NSDictionary alloc] initWithDictionary:self.myDico],nil];
            }
            else
            {
                // recherche presence support
                NSString * newID = [self.myDico objectForKey:kRessourceIdStr];
                BOOL top = NO;
                NSUInteger numeroSupport = 0;
                for (NSDictionary *monDicoRef in  array)
                {
                    NSString * monID = [monDicoRef objectForKey:kRessourceIdStr];
                    if ( [monID isEqualToString:newID]) 
                    {
                        top = YES;
                        break;
                    }
                    numeroSupport++;
                }
                if (top) 
                {
                    [array replaceObjectAtIndex:numeroSupport withObject:[[NSDictionary alloc] initWithDictionary:self.myDico]];
                }		
                else 
                {
                    [array addObject:[[NSDictionary alloc] initWithDictionary:self.myDico ]];
                }
            }
            
            //  Ecriture du fichier à partir des données du tableau 
            
            if ([array writeToFile:filePathListe atomically:YES])
            {
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:kListeSupportChangedNotification object:nil
                 userInfo:nil];
                DLog(@"%@%@",@"liste = ",filePathListe);
            }
            else
            {            
                DLog(@"%@%@",@"liste non modifiee = ",filePathListe);
            }
            
            [self addSkipBackupAttributeToPath:filePathListe];
            
            //-------------------------------------------------------------------
            //- Téléchargement des fichiers medias                              -
            //-------------------------------------------------------------------
            [self telechargementdesfichiersWeb:nomFichierWeb];
            
        }
    
    }    
    
}

-(NSString *)telechargementFichier:(NSString *)subDirectory
                               key:(NSString *)key
{
    DLog(@"");   
    
    NSString * stringRessourcebaseURL = [self.myDico objectForKey:kRessourcebaseURL];
    NSString * ressourcefolder = [self.myDico objectForKey:kRessourceFolderStr];
	//-------------------------------------------------------------------
	// info fichier
	//-------------------------------------------------------------------
 	NSString * downloadLink = [self.myDico objectForKey:key];
	NSString * nomFichier = [downloadLink lastPathComponent];
    
	//-------------------------------------------------------------------
	//-download fichier structure
	//-------------------------------------------------------------------
	// URL du fichier
	NSURL *urlFichier = [NSURL URLWithString:[[stringRessourcebaseURL stringByAppendingPathComponent:ressourcefolder]stringByAppendingPathComponent:downloadLink]];
	// recuperation des datas
	NSData * receivedData = nil; 
	// network indicator visible
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    receivedData = [NSData dataWithContentsOfURL:urlFichier];
	// network indicator caché
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;    
	// Enregistrement du fichier structure	
	NSString *filePath = [subDirectory stringByAppendingPathComponent:nomFichier];
    
    if ((receivedData == nil) || ([receivedData length] == 0 ))
    {
        return nil;
    }
    else
    {
        [receivedData writeToFile:filePath atomically:YES];
    }
    
    return nomFichier;
}

-(void)telechargementdesfichiersWeb:(NSString *)fichier
{
	
    DLog(@"");
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
	NSArray *cachePaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [cachePaths objectAtIndex:0];
    NSString *subDirectory = [cachesDirectory stringByAppendingPathComponent:[self.myDico objectForKey:kRessourceFolderStr]];
    
    //-------------------------------------------------------------------
    //- Téléchargement des fichiers medias                              -
    //-------------------------------------------------------------------
	// creation de la queue qui gere le telechargement des fichiers    
    [self.maFileOperation setMaxConcurrentOperationCount:4];
    
    NSArray * mediaList = [[NSArray alloc] initWithContentsOfFile:[subDirectory stringByAppendingPathComponent:fichier]];
    nombredefichieratelecharger = [mediaList count];
    NSEnumerator *enumerator = [mediaList objectEnumerator];
    id anObject;
    while ((anObject = [enumerator nextObject]))
    {
        if ([anObject isKindOfClass:[NSDictionary class]])
        {
            [self lancerLoadOperation:(NSDictionary *)anObject];
        }
    }
    
}

-(double)updateAverageBandwidthWithNewBandwidth:(CGFloat )bandwidthParam
									   dataSize:(NSUInteger)datasizeParam {

    DLog(@"Bande passante : %f, taille data : %d", bandwidthParam, datasizeParam);
    
    
    //Test de notification à RootViewController pour updater le pourcentage
    //Transformation de la taille en Octet
    double tailleRessourceEnOctet = [[myDico objectForKey:@"ressourcetaille"] integerValue] * 1048576;
    
    tailleEffectuee += datasizeParam;
    
    CGFloat progNonPercent = tailleEffectuee / tailleRessourceEnOctet;
    
    NSDictionary * userinfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:progNonPercent], @"prog", [myDico objectForKey:@"ressourceid"], @"id" ,nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Progression" object:nil userInfo:userinfo];
    
	if (dataBandwidth+datasizeParam>0) {
		averageBandwidth = ((averageBandwidth*dataBandwidth) + (bandwidthParam*datasizeParam)) / dataBandwidth+datasizeParam;
		if (averageBandwidth<10000) {
			[self.maFileOperation setMaxConcurrentOperationCount:2];
		} else {
			[self.maFileOperation setMaxConcurrentOperationCount:4];
		}
	}
	return averageBandwidth;
}


-(void)finFileMediaLoadOperation:(NSDictionary *)dictResult {
    
    NSUInteger count = [self.maFileOperation operationCount];
    
    nombredetelechargement++; 
    
    if ((count == 0) || (nombredetelechargement >= nombredefichieratelecharger))
    {
        DLog(@"dictResult is %@",dictResult);       
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kTitleDownloadFinishNotification object:[[NSDictionary alloc] initWithDictionary:self.myDico]];
    }    
    
    if ([[dictResult objectForKey:K_STATUS_OPERATION] boolValue]) {
        [self updateAverageBandwidthWithNewBandwidth:[[dictResult objectForKey:K_BANDWITH_OPERATION] doubleValue]
                                            dataSize:[[dictResult objectForKey:K_DATASIZE_OPERATION] intValue]];
    } 
    else
    {
        // problème dans le WS pour recupérere l'image OU pb d'enregistrement dans le cache de la sandbox
    }
}

-(void) lancerLoadOperation:(NSDictionary *)anObject
{
    //DLog(@"");
    if (self.topTelechargement)
    {
        FileMediaLoadOperation *operation = [[FileMediaLoadOperation alloc] init];
        operation.anObject = anObject;
        operation.ressourceFolder = [self.myDico objectForKey:kRessourceFolderStr];
        operation.pathURL = [self.myDico objectForKey:@"ressourcebaseurl"];
        operation.infoRessource = self.myDico;
        operation.parentProcessViewController = self;
        [self.maFileOperation setName:[self.myDico objectForKey:kRessourceNomStr]]; 
        [self.maFileOperation addOperation:operation];
    }
}

-(void)removeNotifUpdate
{
    //On reset dans la mémoire de l'app l'affichage du pop-up.
    NSString * key = [[NSString alloc] initWithFormat:@"pop_%@",[myDico objectForKey:kRessourceIdStr]];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:key];
}

#pragma mark - memory methods

- (void)didReceiveMemoryWarning {
    DLog(@"");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
}

- (void)viewDidUnload {
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDownloadStoppedNotification
                                                  object:nil];
    
    [self removeNotifUpdate];
    
    boutonChargement = nil;
    aTextView = nil;
    DLog(@"");
    // Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    
    [self setMyDico:nil];
    //
    [self setMaFileOperation:nil];
    //
}


#pragma mark - interface rotation 

// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

@end
