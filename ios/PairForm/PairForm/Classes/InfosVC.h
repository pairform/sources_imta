//
//  InfosVC.h
//  PairForm
//
//  Created by VESSEREAU on 16/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfosVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textInfos;

@end
