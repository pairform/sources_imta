

#import "HomeVC.h"
#import "SCStackSegueRight.h"
//#import "Cell.h"
#import "TelechargementVC.h"
//#import "TSPopoverController.h"
#import "TSActionSheet.h"
#import "UtilsCore.h"
#import "SIAlertView.h"
#import "MKNetworkEngine.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "RootNavigation_iPad.h"
#import "LanguageManager.h"
#import "FileManager.h"

NSString *kCellID = @"cellRes";                          // UICollectionViewCell storyboard id

@implementation HomeVC
@synthesize ressourcesArray = _ressourcesArray;
@synthesize springBoard;
@synthesize downloadingRessourcesArray = _downloadingRessourcesArray;

-(void)viewDidLoad
{
    [super viewDidLoad];
    //    [TestFlight passCheckpoint:@"Vision des ressources"];
    //    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"textured_paper.png"]]];
    //    [[NSFileManager defaultManager] fileExistsAtPath:pathFichierZip];
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(finishDownloadingRessource:)
    //                                                 name:@"downloadFinished" object:nil];
    //
                                                                                                                                             
    _ressourcesArray = [[UtilsCore getAllRessources] mutableCopy];
    _downloadingRessourcesArray = [[NSMutableArray alloc] init];
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword"]];
    [self.view setBackgroundColor:background];
    if(IS_IPAD)
    {

//        [self setStackWidth: 480];
        [self setStackWidth: 320];
        [self setStackPosition:SCStackViewControllerPositionLeft];
    }
    
    //Initialisation de la collection
    
    CGRect pageViewRect = self.view.bounds;
//    
//    if (IS_IPAD) {
//        pageViewRect.size.height -= 44;
//        pageViewRect.origin.y += 44;
//    }
    
    springBoard = [[PTSSpringBoard alloc] initWithFrame:pageViewRect];
    
    springBoard.delegate = self;
    springBoard.dataSource = self;
    
    [self.view addSubview:springBoard];
    
    //    [springBoard updateSpringboard];
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Mes ressources"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [LanguageManager initialize];
    [self setContent];
    
    if (IS_IPHONE && ![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunchMigration_V1.0.300"]) {
        [(SCAppDelegate*)[[UIApplication sharedApplication] delegate] executeMigrationOfCapsulesIntoLibraryFolder];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    //Vieux hack : sur iPhone, il y a deux layers en permanence
    //Sur iPad, il n'y en a qu'un à l'origine
//    if([[[UIApplication sharedApplication] windows] count] == (IS_IPHONE+1))
//    {
    if (IS_IPHONE) {
        /*if([self checkForDefaultUser])
        {
            [self checkForLoggedInUser];
        }
        else
        {
            DLog(@"Pas de defaultUser");
        }*/
    }
    
        //Si l'utilisateur n'a pas de ressource installée
        if(([_ressourcesArray count] == 0) && [[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunchUnzip"])
        {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_aucune_ressource"] andMessage:[LanguageManager get:@"ios_label_aucune_ressource_telechargee"]];
            [alertView addButtonWithTitle:[LanguageManager get:@"button_non"]
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alertView) {
                                      DLog(@"Ne veut pas accéder au store");
                                  }];
            [alertView addButtonWithTitle:[LanguageManager get:@"button_oui"]
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      DLog(@"Accède au store");
                                      if(IS_IPHONE)
                                          [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"] animated:YES];
                                      else{
                                          UIViewController * ressourceStore = [self.storyboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"];
                                          [ressourceStore setStackPosition:SCStackViewControllerPositionLeft];
                                          [[self sc_stackViewController] popViewControllersAfter:self AtPosition:self.stackPosition animated:YES completion:^{
                                              [[self sc_stackViewController] pushViewController:ressourceStore atPosition:SCStackViewControllerPositionLeft unfold:YES animated:YES completion:nil];
                                          }];
                                          
                                      }

                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            [alertView show];
            
        }
        
//    }
    
    [springBoard updateSpringboard];
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    DLog(@"");
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerDownloadingRessource:)
                                                 name:@"downloadStarted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgressionForRessource:)
                                                 name:@"downloadProgressed" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(finishDownloadingRessource:)
//                                                 name:@"downloadFinished" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorDownloadingRessource:)
                                                 name:@"downloadError" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishDownloadingRessource:)
                                                 name:@"unzipFinished" object:nil];
    if (IS_IPAD) {
        [(RootNavigation_iPad*)self.navigationController setLeftBarButtonItemWithImageName:@"plus_64" andSelectorName:@"showActionSheet:forEvent:" fromViewController:self];
    }
}

-(void) setContent {
    self.navigationMesRessources.title = [LanguageManager get:@"title_accueil"];
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

-(void) updateLayoutOfDownloadedCapsule{
    [springBoard updateItemAtIndex:[_ressourcesArray count] -1];
}
-(void) refreshMessages {
    [springBoard updateSpringboard];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    DLog(@"");
    if([springBoard isEditing]){
        [springBoard toggleEditingMode:NO];
        [springBoard updateSpringboard];
    }
    
    [super viewWillDisappear:animated];
    
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadStarted" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadProgressed" object:nil];
    if (IS_IPAD) {
        [(RootNavigation_iPad*)self.navigationController removeLeftBarButtonItemWithIdentifier:@"plus_64"];
    }

    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadFinished" object:nil];
}

#pragma mark - Notifications Handlers

-(void)registerDownloadingRessource:(NSNotification *)notification
{
    DLog(@"");
    Ressource * new_downloading_ressource = notification.object;
    __block BOOL ressource_already_downloading = NO;
    __block int index;
    
    [_ressourcesArray enumerateObjectsUsingBlock:^(Ressource* obj, NSUInteger idx, BOOL *stop) {
        if([obj.id_ressource isEqualToNumber:new_downloading_ressource.id_ressource]){
            ressource_already_downloading = YES;
            stop = YES;
            index = idx;
        }
    }];
    
    [_downloadingRessourcesArray addObject:new_downloading_ressource];
    
    if (!ressource_already_downloading) {
        [_ressourcesArray addObject:notification.object];
        [springBoard updateItemAtIndex:[_ressourcesArray count] -1];
    }
    else{
        //Sinon, il va y avoir plusieurs capsules en parallèles
        [springBoard updateItemAtIndex:index];
    }
//    if([_downloadingRessourcesArray indexOfObject:notification.object] == NSNotFound)
//    {
//        int index_ressource = -1;
//        
//        [_downloadingRessourcesArray addObject:notification.object];
//        
//        if ([notification.userInfo[@"isUpdate"] boolValue])
//        {
//            
//            for (Ressource * ressource in _ressourcesArray) {
//                if ([ressource.id_ressource isEqualToNumber:((Ressource*) notification.object).id_ressource]) {
//                    index_ressource = [_ressourcesArray indexOfObject:ressource];
//                    break;
//                }
//            }
//            if (index_ressource != -1)
//            {
//                [_ressourcesArray removeObjectAtIndex:index_ressource];
//                [_ressourcesArray insertObject:notification.object atIndex:index_ressource];
//            }
//            
//        }
//        //Si ce n'est pas une mise à jour de ressource
//        else
//        {
//            [_ressourcesArray addObject:notification.object];
//            index_ressource = [_ressourcesArray count] -1;
//        }
//        
//        [springBoard updateItemAtIndex:index_ressource];
//    }
}

-(void)updateProgressionForRessource:(NSNotification*) notification
{
    //Si HomeVC a été désinstencié depuis le début du download, on réenregistre la ressource en cours de téléchargement
    if([_downloadingRessourcesArray indexOfObject:notification.object] == NSNotFound)
        [self registerDownloadingRessource:notification];
    
    Ressource * new_downloading_ressource = notification.object;
    __block int index = 0;
    
    [_ressourcesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[(Ressource*)obj id_ressource] isEqualToNumber:new_downloading_ressource.id_ressource]){
            stop = YES;
            index = idx;
        }
    }];
    //    Ressource * downloadingRessource = notification.object;
    //Quand le springBoard est restauré (et pas nouvellement instanciée), une ImageView vient se glisser dans les subviews, et fausse l'index.
    //On check donc que le nombre de subviews et de ressources dans l'array sont égaux
    NSArray * springBoardItems = [[[springBoard subviews] objectAtIndex:0] subviews];
    
    //Sinon, on ajoute 1 à l'index pour contourner le problème
    index += [springBoardItems count] == [_ressourcesArray count] ? 0 : 1;
    
    PTSSpringBoardItem * item = [springBoardItems objectAtIndex:index];
    //    UIView * item = [springBoard viewWithTag:downloadingRessource.id_ressource];
    UIProgressView * progressBar = (UIProgressView*)[item viewWithTag:100];
    [progressBar setTintColor:[UIColor orangeColor]];
    float convertedProgress = [(NSNumber*)notification.userInfo[@"progress"] floatValue];
    
    DLog(@"%.2f",convertedProgress*100);
    //    progressBar.progress = convertedProgress;
    [progressBar setProgress:convertedProgress animated:YES];
    
    UIActivityIndicatorView * activity_indicator = (UIActivityIndicatorView*)[item viewWithTag:101];
 
    
    if (convertedProgress >= 0.98f) {
//        [progressBar setHidden:YES];
        [activity_indicator startAnimating];
    }
    else{
        [activity_indicator stopAnimating];
    }
    
}

-(void)finishDownloadingRessource:(NSNotification*) notification
{
    DLog(@"");
    dispatch_sync(dispatch_get_main_queue(), ^{
        //Si pas d'objet de notification, on doit recharger toute la vue
        if (notification.object == nil) {
            
            _ressourcesArray = [[UtilsCore getAllRessources] mutableCopy];
            [springBoard updateSpringboard];
            [springBoard setNeedsDisplay];
        }
        else{
            Ressource * downloadingRessource = notification.object;
            //On vire la ressource du tableau des ressources en téléchargement
            [_downloadingRessourcesArray removeObject:downloadingRessource];
            //Récuperation de l'index de la ressource en cours de traitement
            NSUInteger index = [_ressourcesArray indexOfObject:downloadingRessource];
            //Si on ne trouve pas l'index
            if (index == NSNotFound) {
                _ressourcesArray = [[UtilsCore getAllRessources] mutableCopy];
                //Garde fou ; on update tout le springboard
                return [springBoard updateSpringboard];
                //L'update du springboard provoque la récupération des ressources depuis la base de données directement, pas besoin de rajouter la ressource dans le tableau.
            }
            else{
                
                //Sinon, on ajoute la ressource dans le tableau
                [springBoard updateItemAtIndex:index];
            }
            
//            [_ressourcesArray removeObject:notification.object];
            
            //Si l'index est égal à 0, on ajoute plutôt qu'insérer
//            Ressource * ressource = [UtilsCore getRessource:downloadingRessource.id_ressource];
//            
//            if (ressource) {
//                if (index)
//                    [_ressourcesArray insertObject:ressource atIndex:index];
//                else
//                    [_ressourcesArray addObject:ressource];
//                [springBoard updateItemAtIndex:index];
//            }
            
            
        }
    });
    
}

-(void)errorDownloadingRessource:(NSNotification*) notification{
    DLog(@"");
    Ressource * ressource = notification.object;
    if([_downloadingRessourcesArray indexOfObject:ressource] != NSNotFound)
    {

        [_downloadingRessourcesArray removeObject:ressource];

        //S'il n'y a aucne capsule de téléchargée dans cette ressource
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"date_downloaded != nil"];
        NSSet * capsules_locales = [[ressource capsules] filteredSetUsingPredicate:predicate];

        if([capsules_locales count] == 0){
            //On la vire du springboard,
        
            [_ressourcesArray removeObject:notification.object];
        }
        //Sinon on la garde
        [springBoard updateSpringboard];
    }
    
}

#pragma mark - Gestion des ressources

-(IBAction) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:[LanguageManager get:@"ios_title_gestion_ressources"]];
    
    [actionSheet addButtonWithTitle:[LanguageManager get:@"button_ajouter"] block:^{
        if(IS_IPHONE)
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"] animated:YES];
        else{
            UIViewController * ressourceStore = [self.storyboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"];
            [ressourceStore setStackPosition:SCStackViewControllerPositionLeft];
            [[self sc_stackViewController] popViewControllersAfter:self AtPosition:self.stackPosition animated:YES completion:^{
                [[self sc_stackViewController] pushViewController:ressourceStore atPosition:SCStackViewControllerPositionLeft unfold:YES animated:YES completion:nil];
            }];

        }
    }];
    [actionSheet addButtonWithTitle:[LanguageManager get:@"ios_button_editer"] block:^{
        [self.springBoard toggleEditingMode];
    }];
    [actionSheet cancelButtonWithTitle:[LanguageManager get:@"button_annuler"] block:nil];
    actionSheet.cornerRadius = 5;
    actionSheet.popoverGradient = true;
    actionSheet.titleShadow = true;
    
    
    [actionSheet showWithTouch:event];
}

-(void) getRessourcesFromStorage{
    
    NSString *ressourceDirectory = [UtilsCore getRessourcesPath];
    NSArray * documentDirectoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:ressourceDirectory error:nil];
    
    _ressourcesArray = [[NSMutableArray alloc] initWithCapacity:([documentDirectoryContent count] -1)];
    
    if([documentDirectoryContent count] != 0)
    {
        for(NSString * ressourceName in documentDirectoryContent)
        {
            //Patch rapide : __MACOSX se met dans le dossier à cause du dezippeur, a voir plus tard.
            if(![ressourceName isEqualToString:@"__MACOSX"] && ![[ressourceName pathExtension] isEqualToString:@"sqlite"])
            {
                NSString * ressourceFolderPath = [NSString stringWithFormat:@"%@/%@",ressourceDirectory,ressourceName];
                //                [[NSFileManager defaultManager] contentsOfDirectoryAtPath:ressourceFolderPath error:nil];
                
                NSString * ressourceImagePath = [NSString stringWithFormat:@"%@/%@.png",ressourceFolderPath, ressourceName];
                UIImage * ressourceImage = [UIImage imageWithContentsOfFile:ressourceImagePath];
                
                UIImage * ressourceImageFlag = [UIImage imageNamed: @"icone_drapeau_rond_fr.png"];
                
                [_ressourcesArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:ressourceName,@"title",ressourceImage, @"image", ressourceFolderPath, @"path",ressourceImageFlag,@"flag", nil]];
            }
        }
    }
    
}

#pragma mark - CollectionView Delegate
//
//- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
//{
//    return [_ressourcesArray count];
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
//{
//    // we're going to use a custom UICollectionViewCell, which will hold an image and its label
//    //
////    NSDictionary * currentRessource = [_ressourcesArray objectAtIndex:indexPath.row];
//
//    Ressource * currentRessource = [_ressourcesArray objectAtIndex:indexPath.row];
//
//    Cell *cell = [cv dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
//
////    cell.label.text = [currentRessource objectForKey:@"title"];
//    cell.label.text = currentRessource.nom_court;
//    cell.label.textColor = [UIColor blackColor];
//
//    // load the image for this cell
////    cell.image.image = [currentRessource objectForKey:@"image"];
//    cell.image.image = [currentRessource getIcone];
//
//    return cell;
//}
//
//
// the user tapped a collection item, load and set the image on the detail view controller
//
//
//#pragma mark - Navigation
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if([segue.identifier isEqualToString:@"resClick"])
//    {
//        int index = [[[self.collectionView indexPathsForSelectedItems] objectAtIndex:0] row];
//
//        SommaireVC * sommaireVC = segue.destinationViewController;
//        sommaireVC.currentRessource = _ressourcesArray[index];
//    }
//}

#pragma mark -
#pragma mark PTSSpringBoardDataSource protocol

-(NSInteger)numberOfItemsInSpringboard:(PTSSpringBoard *)springboard
{
    DLog(@"");
    return [_ressourcesArray count];
}

-(PTSSpringBoardItem*)springboard:(PTSSpringBoard *)springboard itemForIndex:(NSInteger)index
{
    DLog(@"");
    PTSSpringBoardItem * item = [[PTSSpringBoardItem alloc] init];
    Ressource * currentRessource = [_ressourcesArray objectAtIndex:index];
    
    [item setLabelFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
    [item setLabelColor:[UIColor darkGrayColor]];
    
    [item setLabel:currentRessource.nom_court];
    
    if([_downloadingRessourcesArray indexOfObject:currentRessource] != NSNotFound)
    {
        
        [item setIcon: [self convertToGrayscale:[currentRessource getIcone]]];
        
        //Barre de progression de téléchargement
        UIProgressView * progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        [progressBar setTag:100];
        [progressBar setFrame:CGRectMake(25, 65, 50, 12)];

        [item addSubview:progressBar];
        
        UIActivityIndicatorView * activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(30, 30, 40, 40)];
        
        [activity_indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activity_indicator setTag:101];
        [activity_indicator setHidesWhenStopped:YES];
        [activity_indicator.layer setCornerRadius:20.0f];
        [activity_indicator setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
        [activity_indicator startAnimating];
        [item addSubview:activity_indicator];

        [item setItemIdentifier:@"dl"];
    }
    else
    {
        [item setIcon:[currentRessource getIcone]];
        [item setItemIdentifier:[currentRessource.id_ressource stringValue]];
        
        NSUInteger nombre_messages_non_lus_capsule = [[UtilsCore getNombreMessageNonLuFromRessourceTotale:currentRessource.id_ressource] integerValue];
        
        //S'il y a des messages non-lus
        if ( nombre_messages_non_lus_capsule > 0){
            [item setBadgeColor:[UIColor colorWithRed:0.286 green:0.694 blue:0.851 alpha:1]];
            [item setBadgeValue: [@(nombre_messages_non_lus_capsule) stringValue]];
        }
        else{
            NSUInteger nombre_messages_lus_de_capsule = [[UtilsCore getNombreMessageFromRessourceTotale:currentRessource.id_ressource] integerValue];
            
            [item setBadgeColor:[UIColor colorWithWhite:0.8 alpha:1]];
            [item setBadgeValue: [@(nombre_messages_lus_de_capsule) stringValue]];
        }

        
        // Ajout du drapeau de la langue de la ressource
        NSString *codeLangueRes = @"icone_drapeau_rond_";
        codeLangueRes = [codeLangueRes stringByAppendingString:[LanguageManager codeLangueWithid:[currentRessource.id_langue intValue]]];
        codeLangueRes = [codeLangueRes stringByAppendingString:@".png"];
        UIImage * fImage = [UIImage imageNamed: codeLangueRes];
        UIImageView * flagImage = [[UIImageView alloc] initWithImage:fImage];
        [flagImage setFrame:CGRectMake(62, 58, flagImage.frame.size.width, flagImage.frame.size.height)];
        [item addSubview:flagImage];
    }
    
    return item;
}

-(BOOL)springBoardShouldEnterEditingMode:(PTSSpringBoard *)springboard {
    return YES;
}

-(BOOL)springboard:(PTSSpringBoard *)springboard shouldAllowDeletingItem:(PTSSpringBoardItem *)item {
    //    if ([[item itemIdentifier] isEqualToString:@"dl"]) {
    //        return NO;
    //    } else {
    //        return YES;
    //    }
    return YES;
}

-(BOOL)springboard:(PTSSpringBoard *)springboard shouldAllowMovingItem:(PTSSpringBoardItem *)item {
    if ([[item itemIdentifier] isEqualToString:@"dl"]) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark -
#pragma mark PTSSpringBoardelegate protocol

int selectedIndex;

-(void)springboard:(PTSSpringBoard *)springboard selectedItem:(PTSSpringBoardItem *)item atIndex:(NSInteger)index {
    //Sécurité : parfois, quand il n'y a aucune ressource de téléchargée le springboard hallucine, et croit qu'il a des items avec des index not found.
    if (![springboard isEditing] && index != NSNotFound) {
//        if ([[item itemIdentifier] isEqualToString:@"dl"]) {
//            [self.view makeToast:[LanguageManager get:@"ios_label_attendre_fin_telechargement"] duration:2 position:@"bottom" title:nil image:[UIImage imageNamed:@"cancel_48"] style:nil completion:nil];
//        }
//        else
//        {
            if (IS_IPAD){
                selectedIndex = index;
                [self performSegueWithIdentifier:@"ressourceSegue" sender:self];
//                [self prepareForSegue:[[SCStackSegueRight alloc] initWithIdentifier:@"ressourceSegue" source:self destination:sommaireVC] sender:self];
//                [[self sc_stackViewController] pushViewController:sommaireVC fromViewController:self atPosition:SCStackViewControllerPositionRight unfold:YES animated:YES completion:nil];
                
            }
            
            else{
                TelechargementVC * telechargementVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"TelechargementVC"];
                telechargementVC.currentRessource = _ressourcesArray[index];
                [telechargementVC setIsLocalModeOnly:YES];
                [[self navigationController] pushViewController:telechargementVC animated:YES];
            }
//        }
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([_ressourcesArray count] && selectedIndex != NSNotFound) {
        [(TelechargementVC*)segue.destinationViewController setCurrentRessource:_ressourcesArray[selectedIndex]];
        [(TelechargementVC*)segue.destinationViewController setIsLocalModeOnly:YES];
    }
//    [(SommaireVC*)segue.destinationViewController setCurrentCapsule:_ressourcesArray[selectedIndex]];
}

-(void)springboardDidEnterEditingMode:(PTSSpringBoard *)springboard {
    [self.view makeToast:[LanguageManager get:@"ios_label_secouez_iphone_sortir_mode_edition"] duration:3 position:@"bottom" title:nil image:[UIImage imageNamed:@"bulb_off_48"] style:nil completion:nil];
}

-(void)springboardDidLeaveEditingMode:(PTSSpringBoard *)springboard{
    //TODO:Enlever la reconnaissance de mouvement lors de l'édition de la collection
    //    [self sidePanelController] _removePanGestureToView;
    [UtilsCore updateRessources:_ressourcesArray];
}

//TODO: Supprimer les ressources
-(void)springboard:(PTSSpringBoard *)springboard deletedItem:(PTSSpringBoardItem *)item atIndex:(NSInteger)index {
    SIAlertView * confirm = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"button_supprimer"] andMessage:[LanguageManager get:@"label_confirmation_suppression_ressource"]];
    [confirm addButtonWithTitle:@"Supprimer" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alertView) {
        if ([[item itemIdentifier] isEqualToString:@"dl"]) {
            [self.view makeToast:[LanguageManager get:@"ios_label_telechargement_annule"] duration:2 position:@"bottom" title:nil image:[UIImage imageNamed:@"stop_48"] style:nil completion:nil];
            [MKNetworkEngine cancelOperationsContainingURLString:item.labelText];
        }
        [_ressourcesArray removeObjectAtIndex:index];
        //    if ([_ressourcesArray count] == 0) {
        [springBoard toggleEditingMode];
        //    }
    }];
    [confirm addButtonWithTitle:@"Annuler" type:SIAlertViewButtonTypeCancel handler:^(SIAlertView *alertView) {
        [springboard updateSpringboard];
    }];
    
    [confirm show];
}

-(void)springboard:(PTSSpringBoard *)springboard movedItem:(PTSSpringBoardItem *)item atIndex:(NSInteger)index toIndex:(NSInteger)newIndex {
    [_ressourcesArray exchangeObjectAtIndex:index withObjectAtIndex:newIndex];
}

#pragma mark -
#pragma mark Handles Accelerometer-Shake

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    DLog(@"");
    if (event.subtype == UIEventSubtypeMotionShake && [springBoard isEditing]) {
        [springBoard toggleEditingMode:NO];
    }
}

-(BOOL)canBecomeFirstResponder{
    return YES;
}

#pragma mark - UIImage GrayScale
- (UIImage *)convertToGrayscale:(UIImage*)originImage {
    CGSize size = [originImage size];
    int width = size.width;
    int height = size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [originImage CGImage]);
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[1] + 0.59 * rgbaPixel[2] + 0.11 * rgbaPixel[3];
            
            // set the pixels to gray
            rgbaPixel[1] = gray;
            rgbaPixel[2] = gray;
            rgbaPixel[3] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return resultUIImage;
}
@end
