//
//  SommaireVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//
#import "SommaireVC.h"
#import "PageVC.h"
#import "MessageController.h"
#import "UtilsCore.h"
#import "TelechargementVC.h"
#import "TDBadgedCell.h"
#import "UACellBackgroundView.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LanguageManager.h"
#import "RootVC.h"
#import "MenuVC.h"
#import "UIScrollView+APParallaxHeader.h"

@interface SommaireVC ()
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property (strong,nonatomic) UIButton *uiButtonMessageTopBar;
@property (strong,nonatomic) RXMLElement * rootXML;
@property (strong, nonatomic) NSNumber *id_message_trans;
@property (assign, nonatomic) BOOL flag_capsule_corrompue;
@property (nonatomic, weak) PageVC * currentPageVC;
-(void)updateMenu;

@end

@implementation SommaireVC

@synthesize rootXML = _rootXML;
@synthesize uiNavigationItem, infoButton;
@synthesize rootArray = _rootArray;
@synthesize arrayForTable = _arrayForTable;
@synthesize subMenuStack =_subMenuStack;
@synthesize currentCapsule = _currentCapsule;
@synthesize uiButtonMessageTopBar;
@synthesize lastPageUrlReaded = _lastPageUrlReaded;

BOOL is_scenari = true;

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Entrée dans une capsule"];
    
    self.title = _currentCapsule.nom_court;
    [self.tableView setAllowsMultipleSelection:YES];
    
    [self.tableView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.tableView.layer setShadowOffset:CGSizeMake(0,8)];
    [self.tableView.layer setShadowRadius:5.0];
    [self.tableView.layer setShadowOpacity:0.3];
    self.tableView.clipsToBounds = NO;
    self.tableView.layer.masksToBounds = NO;
    
    
    
    NSString * pathToOutline;
    
    is_scenari = [_currentCapsule.id_format isEqualToNumber:kFormat_Scenari];
    
    if (is_scenari)
        pathToOutline = [NSString stringWithFormat:@"%@/co/outline.xml", [_currentCapsule getPath]];
    else
        pathToOutline = [NSString stringWithFormat:@"%@/outline.xml", [_currentCapsule getPath]];
    
    int update = [[UIViewController getPreference:@"updateFrequency" ] intValue];
    if ( update == kMessageUpdateEveryMinute || update == kMessageUpdateWhenEnterRes){
        [UtilsCore rafraichirMessagesFromCapsule:_currentCapsule.id_capsule];
    }
        
	// Do any additional setup after loading the view.
    _rootXML = [RXMLElement elementFromXMLFullPathToFile:pathToOutline];
    
    [self setLastPageUrlReaded:@""];
    _flag_capsule_corrompue = NO;
    if ([_rootXML isValid]) {
        //[UtilsCore getMessagesFromRessource:_currentCapsule.id_capsule];
        [self setRootArray:[_rootXML allDirectChildren]];
        [self setSubMenuStack:[[NSMutableArray alloc] initWithObjects:_rootArray, nil]];
        
//        [self checkForUpdate];
    }
    else
    {

        
        [[self.navigationController view] makeToast:[LanguageManager get:@"ios_label_ressource_corrompue"] duration:3 position:@"bottom" title:@"" image:[UIImage imageNamed:@"delete_64"] style:nil completion:nil];
        //On ne pop pas ici, parce que la vue n'a pas fini d'être animée... On flag pour poper dans le viewDidAppear
        _flag_capsule_corrompue = YES;
    }
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];

    if (IS_IPAD)
    {
        
        [(RootNavigation_iPad*)self.navigationController showNumberOfPointsForCapsule:self.currentCapsule.id_capsule];

        [self setStackWidth: 320];
    }
    else{
        [(MenuVC*)[(RootVC*)self.navigationController.parentViewController rightPanel] showNumberOfPointsForCapsule:self.currentCapsule.id_capsule];
    }
    
    [self setUIImageEtablissement];
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:_currentCapsule.nom_court];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)setUIImageEtablissement{
    DLog(@"");
    
//    UIImage * iconeModifiee = [[[UtilsCore getRessource:_currentCapsule.id_ressource] getIconeEtablissement] resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(236, 108) interpolationQuality:kCGInterpolationHigh];
//    
//    //Icone de l'établissement
//    if (iconeModifiee) {
//        [self.icone_etablissement setImage: [iconeModifiee roundedCornerImage:4 borderSize:1]];
//    }
//    [self.icone_etablissement setImage: [[UtilsCore getRessource:_currentCapsule.id_ressource] getIconeEtablissement]];
    
    [self.tableView addParallaxWithImage:[[UtilsCore getRessource:_currentCapsule.id_ressource] getIconeEtablissement] withWidth:self.stackWidth andHeight:200 andShadow:YES];
    [self.tableView addParallaxWithView:self.tableView.tableHeaderView withWidth:self.stackWidth andHeight:200 andShadow:YES];
    
    [self.tableView.tableHeaderView setFrame:CGRectZero];

}
-(void)viewDidAppear:(BOOL)animated
{
    DLog(@"");
    [super viewDidAppear:animated];
    
    if (_flag_capsule_corrompue == YES) {
        if (IS_IPAD) {
            [[self sc_stackViewController] popViewControllerAtPosition:SCStackViewControllerPositionRight animated:YES completion:nil];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
    // navigation button bar
//    NSMutableArray     *items = [uiNavigationItem.rightBarButtonItems mutableCopy] ;
//    
//    if (!items) {
//        items = [[NSMutableArray alloc] init];
//    }
    
    //0 si iPad, 1 si Iphone
//    int count = IS_IPHONE;
//    if (IS_IPHONE) {
//        // En cas de retour , n'ajoute pas de bouton en plus.
//        if ( items.count == 1){
//            uiButtonMessageTopBar = [UIButton buttonWithType:UIButtonTypeCustom];
//            
//            [uiButtonMessageTopBar.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
//            [uiButtonMessageTopBar setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//            [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_unreaded_64"] forState:UIControlStateNormal];
//            
//            
//            [uiButtonMessageTopBar setFrame:CGRectMake(5,6,32,32)];
//            [uiButtonMessageTopBar addTarget:self action:@selector(voirMessages) forControlEvents:UIControlEventTouchUpInside];
//            [uiButtonMessageTopBar setShowsTouchWhenHighlighted:true];
//            [uiButtonMessageTopBar setReversesTitleShadowWhenHighlighted:true];
//            
//            
//            UIBarButtonItem *bEdit = [[UIBarButtonItem alloc] initWithCustomView:uiButtonMessageTopBar];
//            [items addObject:bEdit];
//            
//            uiNavigationItem.rightBarButtonItems = items;
//            
//        }
//        //    [self.tableView reloadData];
//        //[UtilsCore rafraichirMessagesFromCapsule:_currentCapsule.id_capsule];
//        NSNumber * nombreDeMessage = [UtilsCore getNombreMessageNonLuFromRessource:_currentCapsule.id_capsule];
//        
//        if ( [nombreDeMessage integerValue ] > 0){
//            [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_unreaded_64"] forState:UIControlStateNormal];
//            [uiButtonMessageTopBar setTitle:[nombreDeMessage stringValue] forState:UIControlStateNormal];
//            uiButtonMessageTopBar.titleEdgeInsets = UIEdgeInsetsMake(0, 9, 6, 0);
//        }else{
//            [uiButtonMessageTopBar setTitle:@"" forState:UIControlStateNormal];
//        }
//        
//        if ( [nombreDeMessage isEqualToNumber:@0]){
//            [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_readed_64"] forState:UIControlStateNormal];
//        }
//        if ( [nombreDeMessage isEqualToNumber:@-1]){
//            [uiButtonMessageTopBar setBackgroundImage:[UIImage imageNamed:@"comment_64"] forState:UIControlStateNormal];
//        }
//    }
//    else{
    
    //Affichage du nombre de commentaires sur la capsule
    NSNumber * nombreDeMessage = [UtilsCore getNombreMessageNonLuFromCapsule:_currentCapsule.id_capsule];
    
    //S'il y a des messages non lus
    if ( [nombreDeMessage integerValue ] > 0){
        [self.messagesButton setBackgroundImage:[UIImage imageNamed:@"comment_unreaded_64"] forState:UIControlStateNormal];
        [self.messagesButton setTitle:[nombreDeMessage stringValue] forState:UIControlStateNormal];
        [self.messagesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.messagesButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 6, 0);
    }else{
        
        nombreDeMessage = [UtilsCore getNombreMessageFromCapsule:_currentCapsule.id_capsule];
        //S'il y a des messages lus
        if ( [nombreDeMessage integerValue] > 0){
            [self.messagesButton setBackgroundImage:[UIImage imageNamed:@"comment_readed_64"] forState:UIControlStateNormal];
            
            [self.messagesButton setTitle:[nombreDeMessage stringValue] forState:UIControlStateNormal];
            [self.messagesButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.messagesButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 6, 0);
        }
        else {
            [self.messagesButton setTitle:@"" forState:UIControlStateNormal];
            [self.messagesButton setBackgroundImage:[UIImage imageNamed:@"comment_64"] forState:UIControlStateNormal];
        }
    }
    
        //    }
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
//    [self.tableView reloadData];
    [self updateMenu];
    
//    if (IS_IPAD) {
    
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(notifiedByPageVC:)
                                                     name:@"pageDidChange"
                                                   object:nil];
//    }
    if (_id_message_trans){
        //Petit délai, pour laisser le temps à l'animation de PageVC de se faire
        //Et ensuite envoyer l'animation de message VC
        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.2);
        dispatch_after(delay, dispatch_get_main_queue(), ^(void){
            [self voirMessages];
        });
        
    }

}

-(void) updateFromPageVC:(NSString*)url{
    //On trime le "co/" devant pour aller chercher dans le sommaire
    self.lastPageUrlReaded = [url stringByReplacingOccurrencesOfString:@"co/" withString:@""];
    [self updateMenu];
}

-(void) notifiedByPageVC:(NSNotification *)notification
{
    //On trime le "co/" devant pour aller chercher dans le sommaire
    self.lastPageUrlReaded = [notification.userInfo[@"url"] stringByReplacingOccurrencesOfString:@"co/" withString:@""];
    [self updateMenu];
}

-(void) refreshMessages{
    [self updateMenu];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    DLog(@"");
    if (IS_IPHONE) {
        //Boutons de gauche
        NSMutableArray * leftItems = [self.navigationItem.leftBarButtonItems mutableCopy];
        
        // En cas de retour , n'ajoute pas de bouton en plus.
        if ( leftItems.count == 1)
        {
            UIBarButtonItem * infoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.infoButton];
            [leftItems addObject:infoBarButtonItem];
            
            self.navigationItem.leftBarButtonItems = leftItems;
        }
    }
    else{
        [(RootNavigation_iPad*)self.navigationController showNumberOfPointsForCapsule:self.currentCapsule.id_capsule];
//        [(RootNavigation_iPad*)self.navigationController setLeftBarButtonItemWithImageName:@"update" andSelectorName:@"" fromViewController:self];
    }
    

}

-(void)dealloc{

    DLog(@"Blah");
    
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"pageDidChange"
                                                  object:nil];
    if (IS_IPAD) {
        [(RootNavigation_iPad*)self.navigationController hideNumberOfPointsForCapsule:self.currentCapsule.id_capsule];
    }
    
    else{
        [(MenuVC*)[(RootVC*)self.navigationController.parentViewController rightPanel] hideNumberOfPointsForCapsule:self.currentCapsule.id_capsule];
    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    
    DLog(@"");
//    
//    if(IS_IPAD){
////        [(RootNavigation_iPad*)self.navigationController removeLeftBarButtonItemWithIdentifier:@"update"];
//    }
    [super viewWillDisappear:animated];
    
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    

}

-(void)updateMenu{ 
    
    if(![self.lastPageUrlReaded isEqualToString:@""])
    {
//        [self.view makeToastActivity];
        
        //On chope la dernière page lue
        NSString * xPath = [NSString stringWithFormat:@"//l[@u=\"%@\"]", self.lastPageUrlReaded];
        
        if ([[_rootXML childrenWithRootXPath:xPath] count] == 0) {
            return;
        }
        
        RXMLElement * elem = [[_rootXML childrenWithRootXPath:xPath] objectAtIndex:0];
        
        //S'il est dans la section 0, c-a-d dans la même section que la page selectionné précédement
        //On check sa position dans le menu en comparant les urls
        for (RXMLElement * elemInMenu in _subMenuStack[[_subMenuStack count] -1]) {
            if ([[elemInMenu attribute:@"u"] isEqualToString:[elem attribute:@"u"]]) {
                NSUInteger ind = [_subMenuStack[[_subMenuStack count] -1] indexOfObject:elemInMenu];
                //On sélectionne la ligne (UI uniquement)
                //Si on a changé de page = que la cell selectionné n'est pas la même que la dernière visitée
                NSIndexPath * indexPath = [NSIndexPath indexPathForRow:ind inSection:0];

                if ([self.selectedIndexPath isEqual:[NSIndexPath indexPathForRow:ind inSection:0]] == NO) {
                    [self.tableView deselectRowAtIndexPath:self.selectedIndexPath animated:YES];
                    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];
                    self.selectedIndexPath = indexPath;
                }
                return;
            }
        }
        //Reset du tableView
        [self setSubMenuStack:[[NSMutableArray alloc] initWithObjects:_rootArray, nil]];
        [self.tableView reloadData];
                
        //Pour chaque parent de cette page (en commencant par celui de plus haut niveau)
        for (int i = elem.parents.count -1; i >= 0 ; i--) {
            NSUInteger index = -1;
            //On check sa position dans le menu en comparant les urls
            for (RXMLElement * elemInMenu in _subMenuStack[[_subMenuStack count] -1]) {
                if ([[elemInMenu attribute:@"u"] isEqualToString:[elem.parents[i] attribute:@"u"]]) {
                    index = [_subMenuStack[[_subMenuStack count] -1] indexOfObject:elemInMenu];
                }
            }
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            
            //On sélectionne la ligne (UI uniquement)
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            //On sélectionne le sous menu (ça update _subMenuStack au passage)
            [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        }
        for (RXMLElement * elemInMenu in _subMenuStack[[_subMenuStack count] -1]) {
            if ([[elemInMenu attribute:@"u"] isEqualToString:[elem attribute:@"u"]]) {
                NSUInteger index = [_subMenuStack[[_subMenuStack count] -1] indexOfObject:elemInMenu];
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionBottom];
            }
        }
        //        [self.view hideToastActivity];

    }
}
#pragma mark - Verification de mise à jour
-(void)checkForUpdate{
    DLog(@"");
    [WService post:[NSString stringWithFormat:@"%@/ressources/majRessource",sc_server] param:@{@"id_ressource": _currentCapsule.id_ressource} callback:^(NSDictionary *wsReturn) {
        
        if (wsReturn[@"date_update"] != [NSNull null]) {
            NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
            [dFormatter setDateFormat:@"yyyy-MM-dd"];
            
            NSDate * distantDate = [dFormatter dateFromString:wsReturn[@"date_update"]];
            NSComparisonResult comparaison = [_currentCapsule.date_edition compare:distantDate];
            
            if (comparaison == NSOrderedAscending)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    self.infoButton.imageView.alpha = 0.0;
                } completion:^(BOOL finished) { 
                    [self.infoButton.imageView setImage:[UIImage imageNamed:@"update"]];
                    
                    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat animations:^{
                        self.infoButton.imageView.alpha = 1.0;
                    } completion:nil];
                }];
            }

        }
   }];
}

#pragma mark - TableView Protocols & Delegates
// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    DLog(@"");

	return [_subMenuStack count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DLog(@"");
    NSArray * currentSectionArray = [_subMenuStack objectAtIndex:[_subMenuStack count] - (section +1)];
    return [currentSectionArray count];
}
//
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    //Si on a plus d'une section
//    if (([_subMenuStack count] > 1) && (indexPath.section != 0)) {
//        cell.backgroundColor = [UIColor colorWithRed:0.733 green:0.733 blue:0.733 alpha:1]; /*#bbbbbb*/
//    }
//}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DLog(@"");
    
    RXMLElement * currentXMLElement = [[_subMenuStack objectAtIndex:[_subMenuStack count] - (indexPath.section +1)] objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier;
    
    if([currentXMLElement.tag isEqualToString:@"b"])
        CellIdentifier = @"resMenuCell";
    else
        CellIdentifier = @"resLinkCell";
    
    TDBadgedCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TDBadgedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
//    cell.backgroundView = [[UACellBackgroundView alloc] initWithFrame:CGRectZero];
//    
//    [(UACellBackgroundView*) cell.backgroundView setColorsFrom:[UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1]
//                                                            to:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:1]];
//    
//    if (indexPath.row == 0)
//        [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionTop];
//    else if (indexPath.row == ([[_subMenuStack objectAtIndex:[_subMenuStack count] - (indexPath.section +1)] count] -1))
//        [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionBottom];
//    else
//        [(UACellBackgroundView *)cell.backgroundView setPosition:UACellBackgroundViewPositionMiddle];
    
    
    
	UILabel * cellTitle = (UILabel*)[cell viewWithTag:1];
    [cellTitle setText:[currentXMLElement attribute:@"t"]];
//    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
//    {
//        NSLayoutConstraint * constraint = [[cell constraints] objectAtIndex:2];
//        
//        [constraint setConstant:(32 + 15)];
////        
////        CGRect frame = cellTitle.frame;
////        frame.origin.x = 10;
////        frame.size.width -= 10;
////        cellTitle.frame = frame;
//    }
    
    // Nombre de message
    
    // Recuperation du nom de la page.
    
    //Recuperation des enfant d'un noeud s'il y en a.
    NSArray * children = [currentXMLElement childrenWithRecursion:@""];
    NSUInteger nombre_messages_non_lus = 0;
    NSUInteger nombre_messages_lus = 0;
    
    UIView *bgColorView = [[UIView alloc] init];

    // Il y a des enfants
    if ( [children count] != 0){
        bgColorView.backgroundColor = [UIColor colorWithRed:(73.0/255.0) green:(177.0/255.0) blue:(217.0/255.0) alpha:0.66];
        for (RXMLElement * elem in children) {
            NSString * urlPage = [elem attribute:@"u"];
            if ( urlPage == nil ) continue;
            if (is_scenari) {
                urlPage = [@"co/" stringByAppendingString:urlPage];
            }
            NSUInteger nombre_messages_non_lus_de_page = [[UtilsCore getNombreMessageNonLuFromCapsulePageTotale:_currentCapsule.id_capsule nom_page:urlPage] integerValue];
            //S'il y a des messages non-lus
            if ( nombre_messages_non_lus_de_page > 0){
                nombre_messages_non_lus += nombre_messages_non_lus_de_page;
            }
            else{
                NSUInteger nombre_messages_lus_de_page = [[UtilsCore getNombreMessageFromCapsulePageTotale:_currentCapsule.id_capsule nom_page:urlPage] integerValue];
                nombre_messages_lus += nombre_messages_lus_de_page ? nombre_messages_lus_de_page : 0;
            }
        }
        
        
    }else{
        bgColorView.backgroundColor = [UIColor colorWithRed:(73.0/255.0) green:(177.0/255.0) blue:(217.0/255.0) alpha:1.0];
//        bgColorView.backgroundColor = [UIColor colorWithRed:(252.0/255.0) green:(132.0/255.0) blue:(18.0/255.0) alpha:0.6];
    // Il n'y a pas d'enfant
        
        NSString * urlPage = [currentXMLElement attribute:@"u"];
        if (is_scenari) {
            urlPage = [@"co/" stringByAppendingString:urlPage];
        }
        NSUInteger nombre_messages_non_lus_de_page = [[UtilsCore getNombreMessageNonLuFromCapsulePageTotale:_currentCapsule.id_capsule nom_page:urlPage] integerValue];
        //S'il y a des messages non-lus
        if ( nombre_messages_non_lus_de_page > 0){
            nombre_messages_non_lus = nombre_messages_non_lus_de_page;
        }
        else{
            NSUInteger nombre_messages_lus_de_page = [[UtilsCore getNombreMessageFromCapsulePageTotale:_currentCapsule.id_capsule nom_page:urlPage] integerValue];
            nombre_messages_lus = nombre_messages_lus_de_page ? nombre_messages_lus_de_page : 0;
        }
    }

    bgColorView.layer.masksToBounds = YES;
    cell.selectedBackgroundView = bgColorView;
    cell.textLabel.highlightedTextColor = [UIColor whiteColor];

    if (nombre_messages_non_lus > 0){
        [cell setBadgeColor:[UIColor colorWithRed:0.286 green:0.694 blue:0.851 alpha:1]];
        cell.badgeString = [NSString stringWithFormat:@"%i",nombre_messages_non_lus];
    }
    else if (nombre_messages_lus > 0){
        [cell setBadgeColor:[UIColor colorWithWhite:0.8 alpha:1]];
        cell.badgeString = [NSString stringWithFormat:@"%i",nombre_messages_lus];
    }
    else{
        cell.badgeString = nil;
    }
   
    //    cell.badgeColor = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1.000];
    cell.badge.fontSize = 16;
    cell.badge.radius = 11;





    return cell;
}

//-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    DLog(@"");
//

//}

- (NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    DLog(@"");

    for ( NSIndexPath* selectedIndexPath in tableView.indexPathsForSelectedRows ) {
        if ( selectedIndexPath.section == indexPath.section )
            [tableView deselectRowAtIndexPath:selectedIndexPath animated:NO] ;
    }
    return indexPath ;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"");

    //Animation de la ligne
//	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    @try {
        

        RXMLElement * currentXMLElement = [[_subMenuStack objectAtIndex:[_subMenuStack count] - (indexPath.section +1)] objectAtIndex:indexPath.row];
   

    
//    if([currentXMLElement.tag isEqualToString:@"l"])
//        return;
        
        [tableView beginUpdates];
        
        //Si l'utilisateur remonte dans la hiérarchie
        if(indexPath.section != 0 ){
            
            //On créé un NSRange intermédiaire qui va servir a supprimer les menu stackés
            NSRange indexRange2 = NSRangeFromString([NSString stringWithFormat:@"%d,%d",[_subMenuStack count] - indexPath.section, indexPath.section]);
            NSIndexSet * sectionsToDelete2 = [NSIndexSet indexSetWithIndexesInRange:indexRange2];
            
            [_subMenuStack removeObjectsAtIndexes:sectionsToDelete2];
            
            //On créé un NSRange intermédiaire qui va servir a supprimer la / les section(s)
            NSRange indexRange = NSRangeFromString([NSString stringWithFormat:@"0,%d",indexPath.section]);
            
            
            NSIndexSet * sectionsToDelete = [[NSIndexSet alloc] initWithIndexesInRange:indexRange];
            
            [tableView deleteSections:sectionsToDelete withRowAnimation:UITableViewRowAnimationBottom];
            
            indexPath = [NSIndexPath indexPathForRow:indexPath.row
                                           inSection:indexPath.section - indexRange2.length];
            
        }
        [tableView endUpdates];
        
        if([currentXMLElement.tag isEqualToString:@"l"]){
            if (IS_IPAD) {
                self.selectedIndexPath = [indexPath copy];
                
                
                if (IS_IPHONE || !_currentPageVC) {
                    [self performSegueWithIdentifier:@"webSegue" sender:self];
                }
                else{
                    NSUInteger sectionClicked = [indexPath section];
                    NSUInteger indexInStack = [_subMenuStack count] - (sectionClicked +1);
                    
                    NSMutableArray * allPages = [[NSMutableArray alloc] init];
                    
                    [_rootXML iterateWithRootXPath:@"//l" usingBlock:^(RXMLElement * elem) {
                        if (is_scenari)
                            [allPages addObject:[NSString stringWithFormat:@"%@/co/%@",[_currentCapsule getPath],[elem attribute:@"u"]]];
                        else
                            [allPages addObject:[NSString stringWithFormat:@"%@/%@",[_currentCapsule getPath],[elem attribute:@"u"]]];
                    }];
                    
                    RXMLElement * currentXML = [[_subMenuStack objectAtIndex:indexInStack] objectAtIndex:[indexPath row]];
                    NSUInteger indexOfSelectedItem;
                    
                    if (is_scenari)
                        indexOfSelectedItem = [allPages indexOfObject:[NSString stringWithFormat:@"%@/co/%@",[_currentCapsule getPath],[currentXML attribute:@"u"]]];
                    else
                        indexOfSelectedItem = [allPages indexOfObject:[NSString stringWithFormat:@"%@/%@",[_currentCapsule getPath],[currentXML attribute:@"u"]]];
                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedPage" object:self userInfo:@{@"pageData": allPages, @"currentCapsule" : _currentCapsule, @"selectedIndex" : [NSNumber numberWithInt:indexOfSelectedItem], @"insideResVC" : self}];
                    if (indexOfSelectedItem != NSNotFound) {
                        [_currentPageVC updatePage:@{@"pageData": allPages, @"currentCapsule" : _currentCapsule, @"selectedIndex" : [NSNumber numberWithInteger:indexOfSelectedItem], @"insideResVC" : self}];
                    }
                }
            }
            
            return;
        }
        else{
            //Récupération des sous-éléments du menu sélectionné
            NSArray * subMenus = [currentXMLElement allDirectChildren];
            
            [tableView beginUpdates];
            
            
            //On ajoute le submenu cliqué dans une stack pour mesurer l'avancée dans les sous-menus
            [_subMenuStack addObject:subMenus];
            
            NSUInteger count = 0;
            
            NSMutableArray *arCells = [NSMutableArray array];
            
            for(RXMLElement * subMenu in subMenus ){
                [arCells addObject:[NSIndexPath indexPathForRow:count++ inSection:0]];
            }
        
            [tableView insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:nil];
            [tableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationBottom];
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:true];
            
            [tableView setNeedsLayout];

            [tableView endUpdates];
        }
    }
    @catch (NSException *exception) {
        DLog(@"%@", exception.description);
    }
    @finally {
        
    }
}
-(IBAction)voirMessages:(id)sender{
    [self voirMessages];
}

-(void)voirMessages{
    [self performSegueWithIdentifier: @"messageSegueRight" sender: self];
}

-(void) showMessageTrans:(NSNumber *)id_message_trans{
    
   
        _id_message_trans = id_message_trans;
    
    
    // Remise a zero , une fois fini.
    //    tag_trans= nil;
    //    num_occurence_trans = nil;
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"messageSegueRight"]){
        _currentPageVC = nil;
        MessageController * messageController = segue.destinationViewController;
        
        messageController.currentCapsule = _currentCapsule;
        
        if (_id_message_trans)
            messageController.id_message_trans = _id_message_trans;

    }
    else if ([[segue identifier] isEqualToString:@"webSegue"]){
        
        //Sur iPhone, le segue est triggeré par la cellule directement = sender
        //Alors que sur iPad, on est déjà passé par le didSelectRow : le selectedIndexPath est déjà défini.
        //Le sender est donc SommaireVC : il ne faut pas essayer de récuperer l'index path correspondant au sender du coup
        if (IS_IPHONE) {
            NSIndexPath * currentIndexPath = [self.tableView indexPathForCell:sender];
            self.selectedIndexPath = [currentIndexPath copy];
        }
        
        NSUInteger sectionClicked = [self.selectedIndexPath section];
        NSUInteger indexInStack = [_subMenuStack count] - (sectionClicked +1);
        
        
        
        NSMutableArray * allPages = [[NSMutableArray alloc] init];
        
        [_rootXML iterateWithRootXPath:@"//l" usingBlock:^(RXMLElement * elem) {
            if (is_scenari)
                [allPages addObject:[NSString stringWithFormat:@"%@/co/%@",[_currentCapsule getPath],[elem attribute:@"u"]]];
            else
                [allPages addObject:[NSString stringWithFormat:@"%@/%@",[_currentCapsule getPath],[elem attribute:@"u"]]];
        }];
        
        RXMLElement * currentXML = [[_subMenuStack objectAtIndex:indexInStack] objectAtIndex:[self.selectedIndexPath row]];
        NSUInteger indexOfSelectedItem;
        
        if (is_scenari)
            indexOfSelectedItem = [allPages indexOfObject:[NSString stringWithFormat:@"%@/co/%@",[_currentCapsule getPath],[currentXML attribute:@"u"]]];
        else
            indexOfSelectedItem = [allPages indexOfObject:[NSString stringWithFormat:@"%@/%@",[_currentCapsule getPath],[currentXML attribute:@"u"]]];
        
        PageVC * pageVC = segue.destinationViewController;
        
        _currentPageVC = pageVC;
        
        pageVC.pageData = allPages;
        
        pageVC.currentCapsule = _currentCapsule;
        pageVC.selectedIndex = indexOfSelectedItem;
        pageVC.insideResVC = self;

    }
    else if([segue.identifier isEqualToString:@"infoSegue"])
    {
        TelechargementVC * telechargementVC = segue.destinationViewController;
        
        //TODO : currentRessource -> currentCapsule dans pageVC
        [telechargementVC setCurrentRessource: [UtilsCore getRessource:_currentCapsule.id_ressource]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
