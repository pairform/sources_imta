//
//  SectionTVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 16/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "CapsuleCell.h"
#import "LanguageManager.h"

@implementation CapsuleCell

- (void)awakeFromNib {
    // Initialization code
    UITapGestureRecognizer * double_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    [double_tap setNumberOfTapsRequired:2];
    [double_tap setCancelsTouchesInView:YES];
    [double_tap setDelaysTouchesBegan:YES];
    [self setGestureRecognizers:@[double_tap]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setBadgeNumber:(int)number{
    if (number == 0) {
        [self.badge setHidden:YES];
    }
    else{
        [self.badge setHidden:NO];
        [self.badge.layer setCornerRadius:12.0f];
        
        UILabel * badge_label = (UILabel*) self.badge.subviews[0];
        [badge_label setText:[NSString stringWithFormat:@"%d",number]];
        //Si les messages sont lus
        if (self.flagMessagesLus) {
            //On change la couleur
            [badge_label setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:1]];
        }
        else{
            [badge_label setBackgroundColor:[UIColor colorWithRed:0.286 green:0.694 blue:0.851 alpha:1]];
        }
        [badge_label.layer setCornerRadius:12.0f];
    }
}

-(void)doubleTap:(UITapGestureRecognizer*)tapGestureRecognizer{
    [self action_button:self];
}
-(IBAction)action_button:(id)sender{
    if (self.etat_capsule == PFCapsuleLocal || self.etat_capsule == PFCapsulePrivate) {
        if ([self.delegate respondsToSelector:@selector(accederCapsule:)]) {
            
            NSAssert(self.capsule, @"La référence de la capsule n'a pas été passée à la cellule");

            //Si c'est le bouton d'action (ouvrir) qui est initiateur de l'action
            if ([sender isKindOfClass:NSClassFromString(@"UIButton")]) {
                //On affiche un toast pour inciter l'utilisateur à double cliquer
                [[[UIApplication sharedApplication] keyWindow] makeToast:[LanguageManager get:@"label_raccourci_double_clic"] duration:4 position:@"bottom" title:[LanguageManager get:@"label_astuce"] image:[UIImage imageNamed:@"bulb_off_48"] style:nil completion:nil];
            }
            [self.delegate accederCapsule:self.capsule];
        }
    }
    else {
        if ([self.delegate respondsToSelector:@selector(telechargerCapsule:isUpdate:)]) {
            BOOL isUpdate = self.etat_capsule == PFCapsuleNeedsUpdate;
            NSAssert(self.capsule, @"La référence de la capsule n'a pas été passée à la cellule");
            [self.delegate telechargerCapsule:self.capsule isUpdate:isUpdate];
        }
    }
}
-(IBAction)action_delete:(id)sender{
    if (self.etat_capsule == PFCapsuleLocal) {
        if ([self.delegate respondsToSelector:@selector(supprimerCapsule:)]) {
            
            NSAssert(self.capsule, @"La référence de la capsule n'a pas été passée à la cellule");
            
            [self.delegate supprimerCapsule:self.capsule];
        }
    }
}
-(void)setProgression:(CGFloat)progress{
    [self.download_progress_view setProgress:progress];
}

-(void)setEtatCapsule:(PFCapsuleState)etat{
    self.etat_capsule = etat;
    if (etat == PFCapsuleLocal) {
        [self setIsLocal];
    }
    else if (etat == PFCapsuleDistant){
        [self setIsDistant];
    }
    else if (etat == PFCapsuleIsDownloading){
        [self setIsDownloading];
    }
    else if (etat == PFCapsuleNeedsUpdate){
        [self setNeedsUpdate];
    }
    else if (etat == PFCapsulePrivate){
        [self setIsPrivate];
    }
}
-(void)setIsDistant{
    [self.action_button setTitle:[LanguageManager get:@"button_telecharger"] forState:UIControlStateNormal];
    [self.action_button setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
    [self.action_button setEnabled:true];
    [self.delete_button setEnabled:NO];
    [self.download_progress_view setHidden:YES];
}
-(void)setIsDownloading{
    [self.action_button setTitle:[LanguageManager get:@"ios_label_telechargement_en_cours"] forState:UIControlStateNormal];
    [self.action_button setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
    [self.action_button setEnabled:false];
    [self.delete_button setEnabled:NO];
    [self.download_progress_view setHidden:NO];
}
-(void)setNeedsUpdate{
    [self.action_button setTitle:[LanguageManager get:@"title_mettre_a_jour_ressource"] forState:UIControlStateNormal];
    [self.action_button setImage:[UIImage imageNamed:@"update"] forState:UIControlStateNormal];
    [self.action_button setEnabled:true];
    [self.download_progress_view setHidden:YES];
    [self.delete_button setEnabled:NO];
    [self.nom_long setTextColor: [UIColor colorWithRed:251/255.0f green:126/255.0f blue:4/255.0f alpha:1.0f]];
}
-(void)setIsLocal{
//    [self.action_button setTitle:[LanguageManager get:@"ios_label_ressource_a_jour"] forState:UIControlStateNormal];
    [self.action_button setTitle:[LanguageManager get:@"ios_label_acceder_capsule"] forState:UIControlStateNormal];
    [self.action_button setImage:nil forState:UIControlStateNormal];
    [self.action_button setEnabled:true];
    [self.download_progress_view setHidden:YES];
    [self.delete_button setEnabled:YES];
    [self.nom_long setTextColor: [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
//    [self.nom_long setAttributedText:[[NSAttributedString alloc] initWithString:self.nom_long.text attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:12]}]];
    
    
}

-(void)setIsPrivate{
    [self.action_button setTitle:[LanguageManager get:@"button_telecharger_prive"] forState:UIControlStateNormal];
    [self.action_button setImage:[UIImage imageNamed:@"lock_24"] forState:UIControlStateNormal];
    [self.action_button setEnabled:true];
    [self.download_progress_view setHidden:YES];
    [self.delete_button setEnabled:NO];
}

//Override de setCapsule pour mettre les valeurs directement depuis la cellule
-(void)setCapsuleForCell:(Capsule *)capsule{
    [self setCapsule:capsule];
    
    //Par défaut, on pense que la ressource est distante
    [self setEtatCapsule:PFCapsuleDistant];
    
    NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
    [dFormatter setDateFormat:[LanguageManager get:@"ios_format_date"]];
    
    NSString * final_date = capsule.date_edition ? [dFormatter stringFromDate:capsule.date_edition] : [dFormatter stringFromDate:capsule.date_creation];

    //Couleur par défaut du texte : plus clair, pour montrer la différence avec les capsules qu'on a.
    [self.nom_long setTextColor: [UIColor colorWithRed:127/255.0f green:127/255.0f blue:127/255.0f alpha:1.0f]];
    [self.nom_long setText: capsule.nom_long];
    [self.auteurs setText: [@"Par " stringByAppendingString:capsule.auteurs]];
    [self.licence setText: [@"Licence : " stringByAppendingString:capsule.licence]];
    [self.updated setText: [@"Mis à jour le " stringByAppendingString:final_date]];
    [self.taille setText: [[capsule.taille stringValue] stringByAppendingString:@" Mo"]];
    [self.description_cap setText: capsule.description_pf];
    [self.download_progress_view setProgress:0];
    [self.download_progress_view setHidden:YES];

//    [self setBadgeNumber:capsule.badge];
    [self setBadgeNumber:0];

}
@end
