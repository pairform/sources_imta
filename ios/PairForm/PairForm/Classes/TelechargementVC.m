//
//  TelechargementVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "TelechargementVC.h"
#import "UtilsCore.h"
#import "MKNetworkKit.h"
#import "FileManager.h"
#import "SIAlertView.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LanguageManager.h"
#import "SommaireVC.h"

@interface TelechargementVC ()
@property (nonatomic, weak) UIView * current_panneau;
@property (nonatomic, strong) NSDictionary * nombre_messages_par_capsules;
@property (nonatomic, strong) NSString * url_logo_espace;
@property (nonatomic, strong) NSArray * sorted_capsules;
@end

@implementation TelechargementVC

@synthesize currentRessource = _currentRessource;
@synthesize sorted_capsules = _sorted_capsules;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];

    if (IS_IPAD)
    {
        [self setStackWidth:320];
    }
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Téléchargement : %@",_currentRessource.nom_court]];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self getRessource];
    [self setContentOfInfosPanel];
    [self getMessagesNumberForRessource];
    
    [self resizeSubViews];
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
    
    //Inscription au notification center pour le changement d'utilisateur (rafraichissement des capsules en fonction)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRessource) name:@"userLoggedIn" object:nil];
    
    _current_panneau = self.panel_sections;
    [_current_panneau setHidden:NO];
}

-(void)getRessource{
    //On essaie de récuperer la ressource locale, si jamais elle y est
    //C'est important, car lors du téléchargement, on va chercher à enregistrer la ressource dans le contexte.
    //Donc, si elle y est déjà, autant traiter directement avec celle ci.
    //Sinon, on peut avoir des problèmes de différence de contexte entre les capsules et la ressource
    Ressource * ressource_locale = [UtilsCore getRessource:[_currentRessource id_ressource]];
    if (ressource_locale) {
        _currentRessource = ressource_locale;
        [self sortCapsulesOfRessource];
    }

    //Si on est en mode hors-ligne
    //Ou qu'on est dans un état inconsistent, genre entité coredata déliée
    if ((self.isLocalModeOnly && ![UIViewController estLieAuServeur]) || ![_currentRessource id_ressource]) {
        //Ben on fait rien
        
//        if ([_currentRessource.capsules count] == 0) {
//            [_currentRessource addCapsules:[NSSet setWithArray:[UtilsCore getAllCapsulesOfRessource:[_currentRessource id_ressource]]]];
//        }
//        [self.actionButton setHidden:YES];
//        [self sortCapsulesOfRessource];
//        [self.panel_sections reloadData];
    }
    else{
        
        [self.view makeToastActivity:@"bottom" ];
        [WService get:@"ressource" param:@{@"id_ressource": [_currentRessource id_ressource]} callback:^(NSDictionary *wsReturn) {
            NSDictionary * ressourceDictionary = wsReturn[@"ressource"];
            
            if (ressourceDictionary) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//                    sleep(3);
                    _url_logo_espace = ressourceDictionary[@"url_logo_espace"];
                    
                    //Si on a pas encore importé les capsules dans la ressource (elle est donc distante), et que la ressource n'a pas de contexte
                    if ([_currentRessource.capsules count] == 0 && ![_currentRessource managedObjectContext]) {
                        //On rempli la ressource (qui n'a pas de contexte) de capsules sans contexte
                        for (NSDictionary * capsuleDictionary in ressourceDictionary[@"capsules"]) {
                           
                            //YES pour capsule sans contexte
                            Capsule * distant_capsule = [UtilsCore convertDictionaryToCapsule:capsuleDictionary temporary:YES];
                            
                            //Si le serveur renvoie un id de langue, on surcharge. Sinon, on envoie celui de la ressource
                            NSNumber * id_langue = capsuleDictionary[@"id_langue"] ? capsuleDictionary[@"id_langue"] : _currentRessource.id_langue;
                            [distant_capsule setLangue:id_langue];
                            [distant_capsule setId_ressource:[_currentRessource id_ressource]];
                            
                            [_currentRessource addCapsulesObject:distant_capsule];
                            
                        }
                    }
                    //On fait le diff avec les capsules distantes
                    else{
                        for (NSDictionary * capsuleDictionary in ressourceDictionary[@"capsules"]) {
                            //On essaie de la récuperer en local
                            __block Capsule * capsule;
                            
                            Capsule * temp_distant_capsule = [UtilsCore convertDictionaryToCapsule:capsuleDictionary temporary:YES];
                            
                            //BLAME : Fuck d'override.
                            //Les capsules n'ont pas valeur pour langue, parce que le nom de la key diffère du serveur. Mais là, j'ai mal au crâne.
                            [temp_distant_capsule setLangue:capsuleDictionary[@"id_langue"]];
                            [temp_distant_capsule setId_ressource:[_currentRessource id_ressource]];
                            
                            [[_currentRessource capsules] enumerateObjectsUsingBlock:^(Capsule* obj, BOOL *stop) {
                                if ([obj.id_capsule isEqualToNumber:temp_distant_capsule.id_capsule]) {
                                    capsule = obj;
                                    stop = true;
                                }
                            }];
                            
                            //Si elle n'est pas en local, elle est nouvelle
                            if(!capsule){
                                //TODO : peut-être qu'il faut l'enregistrer
                                [UtilsCore enregistrerCapsule:temp_distant_capsule withRessource:_currentRessource];
                                //Donc on ajoute
                                [_currentRessource addCapsulesObject:temp_distant_capsule];
                            }
                            
                            //Si la ressource est déjà téléchargée
                            else if(capsule && capsule.date_downloaded)
                            {
    //                            [self updateButtonComparingLocalDate:capsule.date_edition distantDate:distant_capsule.date_edition];
                                NSComparisonResult comparaison = [capsule.date_downloaded compare:temp_distant_capsule.date_edition];
                                
                                //Capsule pas à jour
                                if ( comparaison == NSOrderedAscending){
    //                                [UtilsCore enregistrerCapsule:temp_distant_capsule withRessource:_currentRessource];
    //                                NSMutableDictionary * capsule_dictionary_update = [[NSMutableDictionary alloc] initWithDictionary:capsuleDictionary];
                                    
                                    //Flag pour indiquer qu'il y a besoin d'une mise à jour
    //                                [capsule_dictionary_update setObject:@YES forKey:@"is_update"];
                                    
                                    [UtilsCore updateCapsule:capsule withValuesOfDictionary:capsuleDictionary];
    //                                capsule = temp_distant_capsule;
                                }
                            }
                            //Si la ressource est pas téléchargée, on met quand même à jour les méta (date d'edition par exemple, ou id_visibilite)
                            else {
                                
                                    NSComparisonResult comparaison = [capsule.date_edition compare:temp_distant_capsule.date_edition];
                                    
                                    //Capsule pas à jour
                                    if ( comparaison == NSOrderedAscending){
                                        [UtilsCore updateCapsule:capsule withValuesOfDictionary:capsuleDictionary];
                                    }
                                
                                    //Id de visibilité pas à jour, on update quand même
                                    else if (![capsule.id_visibilite isEqualToNumber:temp_distant_capsule.id_visibilite]){
                                        [UtilsCore updateCapsule:capsule withValuesOfDictionary:capsuleDictionary];
                                    }
                                }
                        }
                        //S'il y a du changement
                        if ([[[_currentRessource managedObjectContext] updatedObjects] count]) {
                            //On enregistre le contexte
                            NSError * error;
                            [[_currentRessource managedObjectContext] save:&error];
                            NSAssert(!error, [error description]);
                            
                        }
                    }
                    [self sortCapsulesOfRessource];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.panel_sections reloadData];
                        [self.view hideToastActivity];
                    });
                });
            }
            
        }];

    }
   
    
}
/** Tri les capsules de la ressource par ordre alphabétique.
 * Instancie _sorted_capsules
*/
-(void)sortCapsulesOfRessource{
    _sorted_capsules = [_currentRessource.capsules sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"nom_long" ascending:YES]]];
}
-(void)getMessagesNumberForRessource{
    //Si on est en mode hors-ligne
//    if (self.isLocalModeOnly) {
//        _nombre_messages_par_capsules = [UtilsCore getNombreMessageNonLuFromRessourceTotale:[_currentRessource id_ressource]];
//    }
//    else{
        NSString * langues_affichage = [LanguageManager displayLanguagesStringForQuery];
        [WService get:@"message/nombre/ressource" param:@{@"langues_affichage": langues_affichage, @"id_ressource" : [_currentRessource id_ressource]}
             callback: ^(NSDictionary *wsReturn)
         {
             _nombre_messages_par_capsules = wsReturn[@"nombre_messages"];
             [self.panel_sections reloadData];
         } errorMessage:nil activityMessage:nil];
        
//    }

}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    //    self.actionButton.center = [[self.view viewWithTag:20] center];
    [self resizeSubViews];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerDownloadingRessource:)
                                                 name:@"downloadStarted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgressionForRessource:)
                                                 name:@"downloadProgressed" object:nil];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(finishDownloadingRessource:)
    //                                                 name:@"downloadFinished" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorDownloadingRessource:)
                                                 name:@"downloadError" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishDownloadingRessource:)
                                                 name:@"unzipFinished" object:nil];
}
-(void) resizeSubViews{
//    [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
    // Do any additional s etup after loading the view.
    CGRect description_res_frame = [self.description_res.text boundingRectWithSize:CGSizeMake(self.description_res.frame.size.width, FLT_MAX)
                                      options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15.0]}
                                      context:nil];
    
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\n" options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfNewLines = [regex numberOfMatchesInString:self.description_res.text options:0 range:NSMakeRange(0, [self.description_res.text length])];
    
    if (numberOfNewLines) {
        description_res_frame.size.height += 15 * numberOfNewLines;
    }
    else{
        description_res_frame.size.height += 10;
    }
    
    CGRect globalFrame = self.contentView.frame;
    //44 pour la taille de la toolbar
    globalFrame.size.height += (description_res_frame.size.height - self.description_res.frame.size.height);
    
    NSLayoutConstraint * constraint = self.description_res.constraints[0];

    constraint.constant = constraint.constant < description_res_frame.size.height ? description_res_frame.size.height : constraint.constant;
    
    [self.panel_infos invalidateIntrinsicContentSize];
}


- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContentOfInfosPanel];
}

-(void) setContentOfInfosPanel{
    NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
    [dFormatter setDateFormat:[LanguageManager get:@"ios_format_date"]];
    
    [self.iconeRes setImage:[_currentRessource getIcone]];
    
    [self.nomCourt setText:_currentRessource.nom_court];
    [self.nomLong setText:_currentRessource.nom_long];

    [self.etablissement setText:_currentRessource.espace_nom_court];
    [self.theme setText:[NSString stringWithFormat:@"%@ : %@", [LanguageManager get:@"label_theme"], _currentRessource.theme]];
    [self.langue setImage: [UIImage imageNamed:[@"icone_drapeau_rond_" stringByAppendingString: [LanguageManager codeLangueWithid:[_currentRessource.id_langue intValue]]]]];

    //On place le texte
    [self.description_res setText:_currentRessource.description_pf];
    
//	[self resizeSubViews];
    //On déclare que c'est une nouvelle ressource pour l'user
    [self setIsUpdate:NO];
    
    //On adapte le bouton si l'user a déjà cette ressource
//    Ressource * localRessource = [UtilsCore getRessource:[_currentRessource id_ressource]];
//
//    //Si la ressource est déjà téléchargée
//    if(localRessource)
//    {
//        if (localRessource == _currentRessource)
//        {
//            [WService post:[NSString stringWithFormat:@"%@/ressources/majRessource",sc_server] param:@{@"id_ressource": localRessource.id_ressource} callback:^(NSDictionary *wsReturn) {
//                
//                NSDateFormatter * dFormatter = [[NSDateFormatter alloc] init];
//                [dFormatter setDateFormat:@"yyyy-MM-dd"];
//        
//                NSDate * distantDate = [dFormatter dateFromString:wsReturn[@"date_update"]];
//                [self updateButtonComparingLocalDate:localRessource.date_edition distantDate:distantDate];
//            }];
//        }
//        else
//            [self updateButtonComparingLocalDate:localRessource.date_edition distantDate:_currentRessource.date_edition];
//    }
//    else if ([_currentRessource.visible isEqualToNumber:@0]) {
//        [self.actionButton setTitle:[LanguageManager get:@"button_telecharger_prive" ] forState:UIControlStateNormal];
//        [self.actionButton setImage:[UIImage imageNamed:@"lock_24"] forState:UIControlStateNormal];
//        [self.actionButton setEnabled:false];
//    }

    //TODO: Ajustement du centre du bouton au centre de la toolBar
//    self.actionButton.center = [[self.view viewWithTag:20] center];
    
}

-(void)updateButtonComparingLocalDate:(NSDate*)localDate distantDate:(NSDate*)distantDate{
    NSComparisonResult comparaison = [localDate compare:distantDate];
    
    if ((comparaison == NSOrderedDescending) || (comparaison == NSOrderedSame))
    {
        DLog(@"La ressource est à jour");
        [self.actionButton setImage:[UIImage imageNamed:@"checkmark_24"] forState:UIControlStateNormal];
        [self.actionButton setEnabled:false];
    }
    
    else if ( comparaison == NSOrderedAscending)
    {
        DLog(@"La ressource n'est pas à jour");
        
        [UIView animateWithDuration:0.4 animations:^{
            self.actionButton.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.actionButton setImage:[UIImage imageNamed:@"update"] forState:UIControlStateNormal];
            
            
            [UIView animateWithDuration:0.6 animations:^{
                self.actionButton.alpha = 1.0;
                //Ajustement du layout du bouton
                [self.actionButton sizeToFit];
            } completion:nil];
        }];

        
        //Flag de maj de ressource passée en notification
        [self setIsUpdate:YES];
    }
    
    //TODO: Refaire des images de 32x32 (@1x) pour le bouton
    //Ajustement du layout du bouton
    [self.actionButton sizeToFit];
}

#pragma mark - Gestion du téléchargement

-(IBAction)telechargerToutesLesCapsules:(id)sender{
    for (Capsule * capsule_to_download in _currentRessource.capsules) {
        //Si la capsule a une date de téléchargement nulle (qu'elle n'est pas encore téléchargée) ou qu'il s'agit d'un update
        //Et qu'il s'agit d'une ressource accessible
        if((capsule_to_download.date_downloaded == nil || capsule_to_download.isUpdate) && [capsule_to_download.id_visibilite isEqualToNumber:@1])
            [self download:capsule_to_download isUpdate:capsule_to_download.isUpdate];
    }
}

-(void)download:(Capsule*)capsule isUpdate:(BOOL)isUpdate
{
    
    MKNetworkEngine* engine = [[MKNetworkEngine alloc] initWithHostName:sc_server customHeaderFields:nil];
    
    struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(sc_port);
    address.sin_addr.s_addr = inet_addr([sc_hostname UTF8String]);
    
    //    if([[Reachability reachabilityWithHostname:sc_hostname] currentReachabilityStatus] == NotReachable)
    if([[Reachability reachabilityWithAddress:&address] currentReachabilityStatus] == NotReachable){
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_erreur_connexion"] andMessage:[LanguageManager get:@"label_erreur_reseau_indisponible"]];
        
        [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                              }];
        [alertView addButtonWithTitle:[LanguageManager get:@"ios_button_reesayer"]
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  
                                  [alertView dismissAnimated:NO];
                                  [self download:capsule isUpdate:isUpdate];
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        
        
        [alertView show];

        return;
    }
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"action"    
                                                          action:@"download"
                                                           label:[NSString stringWithFormat:@"%@ - %@ : téléchargé", _currentRessource.nom_court, capsule.nom_court]
                                                           value:nil] build]];    
    
    
    //Notif de début de téléchargement, pour HomeVC et téléchargementVC
    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadStarted"
                                                        object:_currentRessource
                                                      userInfo:@{@"id_capsule" : capsule.id_capsule, @"isUpdate" : [NSNumber numberWithBool:isUpdate]}];

    
    NSString *ressourceDirectory = [UtilsCore getRessourcesPath];
    
	NSString * downloadPath = [ressourceDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.zip", [_currentRessource id_ressource], capsule.id_capsule]];
    NSString * final_path = [ressourceDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", [_currentRessource id_ressource], capsule.id_capsule]];
    NSString * url;
    
    if ([capsule.url_mobile hasSuffix:@".zip"]) {
        url = capsule.url_mobile;
    }
    else{
        url = [capsule.url_mobile stringByAppendingString:@".zip"];
    }
    
    //Sécurité en cas de double demande de téléchargement
    [MKNetworkEngine cancelOperationsContainingURLString:url];
    
    MKNetworkOperation *op = [engine operationWithURLString:url];
    
    
    [op addDownloadStream:[NSOutputStream outputStreamToFileAtPath:downloadPath append:YES]];
    
    [engine enqueueOperation:op];
    
    __block int last_progress = 0;
    int step = 2;
    
    [op onDownloadProgressChanged:^(double progress) {

        //On envoie pas des notifications à chaque fois, mais à chaque 2%
        if ((int)(progress * 100) - last_progress >= step) {
            last_progress = (int)(progress * 100);
            //Notif de la progression pour changer l'indicateur dans HomeVC
            [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgressed" object:_currentRessource userInfo:@{@"id_capsule" : capsule.id_capsule, @"progress": [NSNumber numberWithDouble:progress], @"isUpdate" : [NSNumber numberWithBool:isUpdate]}];
            DLog(@"%.2f", progress*100.0);
        }
        

    }];
    
    [op addCompletionHandler:^(MKNetworkOperation* completedRequest) {
        //Récuperation du niveau correspondant à la ressource
        [WService post:@"utilisateur/categorie/verifier" param:@{@"id_ressource": [_currentRessource id_ressource], @"id_capsule": capsule.id_capsule} callback:^(NSDictionary *wsReturn) {
            if ([wsReturn[@"status"] isEqualToString:@"up"] && [wsReturn[@"categorie_ressource"] isKindOfClass:[NSDictionary class]]) {
//                [self addRawRankToUserData:wsReturn[@"rank"]];
                [UIViewController addRankToUserData:wsReturn[@"categorie_ressource"] forID:[capsule.id_capsule stringValue]];
            }
        } errorMessage:nil activityMessage:nil];

        //S'il y a une url de logo d'espace
        if (_url_logo_espace) {
            //On récupère l'image distante
            UIImage * icone = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[sc_server_static stringByAppendingString:_url_logo_espace]]]];
            //S'il n'y a pas de connexion internet
            if (!icone) {
                icone = [UIImage imageNamed:@"noRessourcePic"];
            }
            //Sinon, on stocke l'icone dans la variable
            [_currentRessource setIcone_etablissement:[NSKeyedArchiver archivedDataWithRootObject:icone]];

        }
        
        //Mise à jour de la date de download
        [capsule setDate_downloaded:[NSDate date]];
        //On enlève le flag d'update
        [capsule setIsUpdate:NO];
        
        [UtilsCore enregistrerRessource:_currentRessource];
        
        //Mise à jour de l'identifiant de ressource
//        [capsule setId_ressource:[_currentRessource id_ressource]];
//        if (isUpdate)
//            [UtilsCore enregistrerCapsule:capsule];
//        else

        
//        //IMPORTANT
//        //Copie des entités pour ne pas mélanger les contextes ni les références
//        Ressource * downloadedRessource;
//        
//        Capsule * downloadedCapsule = [capsule copy];
//        
//        //Mise à jour de l'identifiant de ressource
//        [downloadedCapsule setId_ressource:downloadedRessource.id_ressource];
//        
//        //Enregistrement de la capsule
//        [UtilsCore enregistrerCapsule:downloadedCapsule];
//        
//        //Si on est dans le cas d'une mise à jour
//        if (isUpdate) {
//            downloadedRessource = [UtilsCore getRessource:[_currentRessource id_ressource]];
//            NSMutableSet * nouveauSetCapsules = [downloadedRessource.capsules mutableCopy];
//            [nouveauSetCapsules addObject:downloadedCapsule];
//            [downloadedRessource setCapsules:nouveauSetCapsules];
//            
//        }
//        //Sinon, on copie la ressource
//        else{
//            //On ne met dans la ressource que la capsule téléchargée
//            downloadedRessource = [_currentRessource copy];
//            [downloadedRessource setCapsules:[NSSet setWithObjects:downloadedCapsule, nil]];
//            
//        }
//        
//
//        [UtilsCore enregistrerRessource:downloadedRessource];
        
//        [downloadedCapsule setRessource:downloadedRessource];
        
        
        [FileManager unzipFileAtPath:downloadPath toPath:final_path withRessource:_currentRessource andCapsule:capsule isUpdate:isUpdate];
//        [UtilsCore rafraichirMessagesFromCapsule:[_currentRessource id_ressource]];
        
        DLog(@"%@", completedRequest);
        
        //Notif à destination de HomeVC pour qu'il update la table
        //Fix : déporté dans le file manager pour être sur que la ressource est dézipée avant de rentrer dedans
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadFinished" object:_currentRessource userInfo:@{@"isUpdate" : [NSNumber numberWithBool:self.isUpdate]}];
        
        //Notif à destination du rootNavigation, pour récupérer les messages de la capsule
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadFinished" object:_currentRessource userInfo:@{@"id_capsule" : capsule.id_capsule, @"capsule_nom_court" : capsule.nom_court, @"isUpdate" : [NSNumber numberWithBool:isUpdate]}];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
            
        DLog(@"%@", error);
        //Notif à destination du HomeVC, pour enlever la ressource en cours de DL
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadError" object:_currentRessource userInfo:@{@"id_capsule" : capsule.id_capsule, @"capsule_nom_court" : capsule.nom_court, @"isUpdate" : [NSNumber numberWithBool:isUpdate]}];
        //Notif à destination du rootNavigation, pour afficher un toast d'erreur de DL
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadErrorToast" object:_currentRessource userInfo:@{@"id_capsule" : capsule.id_capsule, @"capsule_nom_court" : capsule.nom_court, @"isUpdate" : [NSNumber numberWithBool:isUpdate]}];
//        
//        if (error.code == -1001) {
//            SIAlertView * alert = [[SIAlertView alloc] initWithTitle:@"Erreur" andMessage:[error localizedRecoverySuggestion]];
//            [alert show];
//            
//        }
        
    }];

}

#pragma mark - UITableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.panel_sections){
        return 1;
    }
    else if (tableView == self.panel_objectifs){
        return 2;
    }
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.panel_sections){
        return 0;
    }
    else if (tableView == self.panel_objectifs){
        return 44;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.panel_sections){
        return nil;
    }
    else if (tableView == self.panel_objectifs){
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"objectifsHeader"];
        UILabel * label = (UILabel*)[cell viewWithTag:1];
        if (section == 0)
            [label setText:@"Objectifs d'apprentissage"];
        else
            [label setText:@"Profil d'apprentissage"];
        return cell;

    }
    
    return nil;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.panel_sections)
        return [_sorted_capsules count];
    else if (tableView == self.panel_objectifs)
        return 0;
    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.panel_sections) {
        CapsuleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CapsuleCell" forIndexPath:indexPath];
        
        Capsule * current_capsule = [_sorted_capsules objectAtIndex:[indexPath row]];
        
        [cell setCapsuleForCell:current_capsule];
        [cell setDelegate:self];
        
        NSUInteger nombre_messages;
        //Si la ressource est téléchargée
        if (current_capsule.date_downloaded) {
            //On récupère le nombre de messages lus local

            NSUInteger nombre_messages_non_lus_capsule = [[UtilsCore getNombreMessageNonLuFromCapsuleTotale:current_capsule.id_capsule] integerValue];
            //S'il y a des messages non-lus
            if ( nombre_messages_non_lus_capsule > 0){
                nombre_messages = nombre_messages_non_lus_capsule;
                [cell setFlagMessagesLus:NO];
            }
            else{
                NSUInteger nombre_messages_lus_de_capsule = [[UtilsCore getNombreMessageFromCapsuleTotale:current_capsule.id_capsule] integerValue];
                nombre_messages = nombre_messages_lus_de_capsule ? nombre_messages_lus_de_capsule : 0;
                [cell setFlagMessagesLus:YES];
            }

        }
        else{
            //Sinon, on chope ça du serveur
            nombre_messages = [[_nombre_messages_par_capsules objectForKey:[current_capsule.id_capsule stringValue]] unsignedIntegerValue];
            [cell setFlagMessagesLus:NO];
        }

        
        if (nombre_messages) {
            [cell setBadgeNumber:nombre_messages];
        }
        
        //Gestion des états de la cellule
        //Si elle est déjà en local
        if (current_capsule.date_downloaded && !current_capsule.isUpdate) {
            //On vérifie la maj
            [cell setEtatCapsule:PFCapsuleLocal];
        }
        //Si elle n'a pas d'URL
        else if ([current_capsule.id_visibilite isEqualToNumber:@2]){
            //Ressource privée
            [cell setEtatCapsule:PFCapsulePrivate];
        }
        //Si elle a besoin d'une mise à jour
        else if (current_capsule.isUpdate){
            //Ressource privée
            [cell setEtatCapsule:PFCapsuleNeedsUpdate];
        }
        //Si la capsule est en téléchargement
        else if (current_capsule.download_progression) {
            //Le téléchargement est en cours
            if ([current_capsule.download_progression floatValue] < 0.98f){
                [cell setEtatCapsule:PFCapsuleIsDownloading];
                [cell setProgression:[current_capsule.download_progression floatValue]];
            }
            //Si le téléchargement est fini
            else{
                [cell setEtatCapsule:PFCapsuleLocal];
            }
        }
        //Si elle est juste distante
        else{
            [cell setEtatCapsule:PFCapsuleDistant];
        }
        
        return cell;
    }
    else if (tableView == self.panel_objectifs) {
        //Rien pour l'instant
    }
    
    return nil;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.panel_sections){
        if([indexPath isEqual:_expanded_cell_index])
            _expanded_cell_index = nil;
        else
            _expanded_cell_index = indexPath;
        
        [tableView beginUpdates];
        [tableView endUpdates];
    }
    else if (tableView == self.panel_objectifs){
        
    }
        

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.panel_sections){
        if([indexPath isEqual:_expanded_cell_index]){
            if(IS_IPAD)
                return 420;
            else
                return 300;
        }
        else
            return 55;
    }
    else if (tableView == self.panel_objectifs){
        return 44;
    }
    return 44;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.panel_sections){
        
        Capsule * current_capsule = [_sorted_capsules objectAtIndex:[indexPath row]];
        //Si elle est bien en local
        if(current_capsule.date_downloaded){
            UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Supprimer"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
                [UtilsCore deleteCapsuleById:[current_capsule id_capsule]];

                NSMutableSet * nouveauSetCapsules = [_currentRessource.capsules mutableCopy];
                [nouveauSetCapsules removeObject:current_capsule];
                [_currentRessource setCapsules:nouveauSetCapsules];
                [self sortCapsulesOfRessource];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }];
            
            return @[deleteAction];
        }
        else
            return @[];
            
    }
    else
        return @[];
}

// From Master/Detail Xcode template
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

    }
}
#pragma mark - CapsuleCell delegate
-(void)telechargerCapsule:(Capsule *)capsule isUpdate:(BOOL)isUpdate{
    [self download:capsule isUpdate:isUpdate];
}

-(void)accederCapsule:(Capsule *)capsule{
    //On vérifie que la capsule est bien accessible
    if ([capsule.id_visibilite isEqualToNumber:@1]) {
        [self performSegueWithIdentifier:@"sommaireSegue" sender:capsule];
    }
    else{
        SIAlertView * alert;
        if ([UIViewController isConnected]) {
            alert = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"label_acces_interdit"] andMessage:[LanguageManager get:@"label_acces_interdit_non_autorise"]];
            
            [alert addButtonWithTitle:[LanguageManager get:@"button_changer_utilisateur"] type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alertView) {
                [self logOut];
                [self popUpLogin];
            }];
        }
        else{
            alert = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"label_acces_interdit"] andMessage:[LanguageManager get:@"label_acces_interdit_connexion"]];
            
            [alert addButtonWithTitle:[LanguageManager get:@"button_connexion"] type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alertView) {
                [self popUpLogin];
            }];
        }
        
        [alert addButtonWithTitle:[LanguageManager get:@"button_annuler"] type:SIAlertViewButtonTypeCancel handler:^(SIAlertView *alertView) {
            [alertView dismissAnimated:YES];
        }];
        
        [alert show];
    }

}
-(void)supprimerCapsule:(Capsule*)current_capsule{
    SIAlertView * confirm = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"button_supprimer"] andMessage:[LanguageManager get:@"label_confirmation_suppression_ressource"]];
    [confirm addButtonWithTitle:@"Supprimer" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alertView) {
        [UtilsCore deleteCapsuleById:[current_capsule id_capsule]];
        
        NSMutableSet * nouveauSetCapsules = [_currentRessource.capsules mutableCopy];
        [nouveauSetCapsules removeObject:current_capsule];
        [_currentRessource setCapsules:nouveauSetCapsules];
        [self sortCapsulesOfRessource];
        [self.panel_sections reloadData];
//        [self.panel_sections deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

    }];
    [confirm addButtonWithTitle:@"Annuler" type:SIAlertViewButtonTypeCancel handler:^(SIAlertView *alertView) {
        
    }];
    
    [confirm show];

}
#pragma mark - Gestion des panneaux
- (IBAction)switchPanneau:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger indexPanneau = segmentedControl.selectedSegmentIndex;
    
    switch (indexPanneau) {
        case 0:
            [self showPanneau:self.panel_infos];
            break;
        case 1:
            [self showPanneau:self.panel_objectifs];
            break;
        case 2:
            [self showPanneau:self.panel_sections];
            break;
        default:
            break;
    }
    
}

-(void)showPanneau:(UIView*)panneau{
    
    panneau.layer.opacity = 0;
    [panneau setHidden:NO];
    
    [UIView animateWithDuration:0.2f animations:^{
        _current_panneau.layer.opacity = 0;
        panneau.layer.opacity = 1;
    } completion:^(BOOL finished) {
        [_current_panneau setHidden:YES];
        _current_panneau = panneau;
    }];
}

#pragma mark - Gestion des notifications handlers
-(void)registerDownloadingRessource:(NSNotification *)notification
{
    DLog(@"");
    Ressource * downloading_ressource = (Ressource*)notification.object;
    //Si on est dans une ressource correspondant à la notification
    if([[_currentRessource id_ressource] isEqualToNumber:downloading_ressource.id_ressource])
    {
        NSNumber * downloading_id_capsule = notification.userInfo[@"id_capsule"];
        //On chope la capsule correspondante
        
        [_sorted_capsules enumerateObjectsUsingBlock:^(Capsule* obj, NSUInteger idx, BOOL *stop) {
            if ([obj.id_capsule isEqualToNumber:downloading_id_capsule]) {
                obj.download_progression = [NSNumber numberWithFloat:0.0f];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.panel_sections reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                });
            }
        }];
           
        
    }
}

-(void)updateProgressionForRessource:(NSNotification*) notification
{
    DLog(@"");
    Ressource * downloading_ressource = (Ressource*)notification.object;
    //Si on est dans une ressource correspondant à la notification
    if([[_currentRessource id_ressource] isEqualToNumber:downloading_ressource.id_ressource])
    {
        NSNumber * downloading_id_capsule = notification.userInfo[@"id_capsule"];
        NSNumber * downloading_capsule_progress = notification.userInfo[@"progress"];
        //On chope la capsule correspondante
        [_sorted_capsules enumerateObjectsUsingBlock:^(Capsule* obj, NSUInteger idx, BOOL *stop) {
            if ([obj.id_capsule isEqualToNumber:downloading_id_capsule]) {
                obj.download_progression = downloading_capsule_progress;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.panel_sections reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                });

            }
        }];
    }
}

-(void)finishDownloadingRessource:(NSNotification*) notification
{
    DLog(@"");
    Ressource * downloading_ressource = (Ressource*)notification.object;
    //Si on est dans une ressource correspondant à la notification
    if([[_currentRessource id_ressource] isEqualToNumber:downloading_ressource.id_ressource])
    {
        NSNumber * downloading_id_capsule = notification.userInfo[@"id_capsule"];
        //On chope la capsule correspondante
        [_sorted_capsules enumerateObjectsUsingBlock:^(Capsule* obj, NSUInteger idx, BOOL *stop) {
            if ([obj.id_capsule isEqualToNumber:downloading_id_capsule]) {
                obj.download_progression = @0;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.panel_sections reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                });
            }
        }];

    }
}

-(void)errorDownloadingRessource:(NSNotification*) notification
{
    DLog(@"");
    Ressource * downloading_ressource = (Ressource*)notification.object;
    //Si on est dans une ressource correspondant à la notification
    if([[_currentRessource id_ressource] isEqualToNumber:downloading_ressource.id_ressource])
    {
        NSNumber * downloading_id_capsule = notification.userInfo[@"id_capsule"];
        //On chope la capsule correspondante
        [_sorted_capsules enumerateObjectsUsingBlock:^(Capsule* obj, NSUInteger idx, BOOL *stop) {
            if ([obj.id_capsule isEqualToNumber:downloading_id_capsule]) {
                obj.download_progression = @0;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.panel_sections reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                });
            }
        }];
    }
}

#pragma mark - Segue handler

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //Si on envoie la capsule cash comme sender, c-a-d qu'on a fait un double clic (ou ouvrir)
    if ([sender isKindOfClass:NSClassFromString(@"Capsule")])
        [(SommaireVC*) segue.destinationViewController setCurrentCapsule:sender];
    
    //Sinon, on prend la cellule qui est étendue (ne marche que si on clique sur le bouton ouvrir)
    else
        [(SommaireVC*) segue.destinationViewController setCurrentCapsule:_sorted_capsules[_expanded_cell_index.row]];
}


@end


