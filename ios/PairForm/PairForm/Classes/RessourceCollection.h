//
//  RessourceCollection.h
//  PairForm
//
//  Created by Maen Juganaikloo on 05/03/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RessourceCollection : UICollectionView 

@property (getter = isEditing) BOOL editing;

@end
