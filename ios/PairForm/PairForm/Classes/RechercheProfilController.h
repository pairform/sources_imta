//
//  ViewControllerListe.h
//  profil
//
//  Created by admin on 18/03/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "RechercheProfilControllerDelegate.h"




typedef enum {
    modeNormal,
    modeAjoutCercle
} Mode;



@interface RechercheProfilController : UIViewController <UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate, UIActionSheetDelegate>


@property Mode mode;
@property (weak, nonatomic) id<RechercheProfilControllerDelegate> delegate;

@end

