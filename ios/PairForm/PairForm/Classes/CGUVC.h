//
//  CGUVC.h
//  PairForm
//
//  Created by VESSEREAU on 16/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CGUVC : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textCGU;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
