//
//  PageWebVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 22/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Capsule.h"
#import "UtilsCore.h"
#import "SommaireVC.h"
#import "RXMLElement.h"

@interface PageVC : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray * pageData;
@property (nonatomic) int selectedIndex;
@property (strong, nonatomic) NSIndexPath * selectedIndexPath;
@property (strong, nonatomic) NSString *tag_trans;
@property (strong, nonatomic) NSNumber *num_occurence_trans;
@property (strong, nonatomic) NSNumber *id_message_trans;
@property(nonatomic,strong) Capsule * currentCapsule;
@property (strong, nonatomic) SommaireVC * insideResVC;


- (void)voirMessageDepuisOA:(NSString *)theTag num_occurence:(NSNumber* ) theNum_occurence;
-(void) selectOATrans:(UIWebView *)webView;
-(void)updatePage:(NSDictionary*)data;
@end
