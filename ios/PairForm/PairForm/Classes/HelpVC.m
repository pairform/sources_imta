//
//  HelpVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 17/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "HelpVC.h"
#import "LanguageManager.h"

@interface HelpVC ()

@end

@implementation HelpVC

@synthesize pageController;

-(id)initWithIndex:(int)index{
    
    if([self init])
    {
        _currentIndex = index;
    }
    return self;
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    if (IS_IPHONE)
        return UIInterfaceOrientationPortrait;
    else
        return UIInterfaceOrientationLandscapeLeft;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    self.stackWidth = 320;
//    self.view.width = self.stackWidth;
    CGRect frame = self.view.bounds;
    frame.origin.y += 70;
    frame.size.height -= 70;
    
    [[self.pageController view] setFrame:frame];

//    [[self.pageController view] setFrame:];
    
//    UIViewController *initialViewController = [self viewControllerAtIndex:0];

    
//    UIViewController * helpScreensHolder = [[UIViewController alloc] initWithNibName:@"Help" bundle:nil];
    
    if([_screenStoryboardID isEqualToString:@""] || (_screenStoryboardID == nil))
    {
        _helpScreensHolder = [[UIStoryboard storyboardWithName:@"HelpStoryboard" bundle:nil] instantiateInitialViewController];
    }
    else
    {
        _helpScreensHolder = [[UIStoryboard storyboardWithName:@"HelpStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:_screenStoryboardID];
    }
    
    [self setContent];
    
    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:[[[_helpScreensHolder view] subviews] count]];
                                  
    for (UIView * helpView in [[_helpScreensHolder view] subviews]) {
//        helpView.width = self.stackWidth;
        [tempArray addObject:[self viewControllerFromUIView:helpView]];
    }
    
    _dataSource = [tempArray copy];
     
    [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:_currentIndex]]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    self.pageController.doubleSided = NO;
    
    [self.view setClipsToBounds:YES];
//    [self.pageControl setNumberOfPages: [_dataSource count]];
//    [self.pageControl setCurrentPage:_currentIndex];
}

- (void) setContent {
    UIView * helpView;
    
    if([_screenStoryboardID isEqualToString:@""] || (_screenStoryboardID == nil)) {
        helpView = [[_helpScreensHolder view] viewWithTag:1000];
        ((UILabel *) [helpView viewWithTag:1001]).text = [LanguageManager get:@"ios_label_aide_contextuelle_inexistante"];
        ((UILabel *) [helpView viewWithTag:1002]).text = [LanguageManager get:@"ios_label_consulter_manuel"];
    } else if([_screenStoryboardID isEqualToString:@"MessageController"]){
        helpView = [[_helpScreensHolder view] viewWithTag:1000];
        ((UILabel *) [helpView viewWithTag:1001]).text = [LanguageManager get:@"ios_label_aide_double_clique_msg"];
        helpView = [[_helpScreensHolder view] viewWithTag:2000];
        ((UILabel *) [helpView viewWithTag:2001]).text = [LanguageManager get:@"ios_label_aide_balayer_msg"];
        helpView = [[_helpScreensHolder view] viewWithTag:3000];
        ((UILabel *) [helpView viewWithTag:3001]).text = [LanguageManager get:@"ios_label_aide_cliquer_msg"];
        helpView = [[_helpScreensHolder view] viewWithTag:4000];
        ((UILabel *) [helpView viewWithTag:4001]).text = [LanguageManager get:@"ios_label_aide_tirer_liste"];
        helpView = [[_helpScreensHolder view] viewWithTag:5000];
        ((UILabel *) [helpView viewWithTag:5001]).text = [LanguageManager get:@"ios_label_aide_liste_menu"];
    } else if([_screenStoryboardID isEqualToString:@"HomeVC"]) {
        helpView = [[_helpScreensHolder view] viewWithTag:1000];
        ((UILabel *) [helpView viewWithTag:1001]).text = [LanguageManager get:@"ios_label_aide_reorganiser_ressources"];
        helpView = [[_helpScreensHolder view] viewWithTag:2000];
        ((UILabel *) [helpView viewWithTag:2001]).text = [LanguageManager get:@"ios_label_aide_supprimer_ressouces"];
        helpView = [[_helpScreensHolder view] viewWithTag:3000];
        ((UILabel *) [helpView viewWithTag:3001]).text = [LanguageManager get:@"ios_label_aide_secouer_iphone"];
    } else if([_screenStoryboardID isEqualToString:@"ProfilController"]) {
        helpView = [[_helpScreensHolder view] viewWithTag:1000];
        ((UILabel *) [helpView viewWithTag:1001]).text = [LanguageManager get:@"ios_label_aide_succes"];
        helpView = [[_helpScreensHolder view] viewWithTag:2000];
        ((UILabel *) [helpView viewWithTag:2001]).text = [LanguageManager get:@"ios_label_aide_changer_role"];
        helpView = [[_helpScreensHolder view] viewWithTag:3000];
        ((UILabel *) [helpView viewWithTag:3001]).text = [LanguageManager get:@"ios_label_aide_attribuer_role"];
    } else if([_screenStoryboardID isEqualToString:@"RechercheProfilController"]) {
        helpView = [[_helpScreensHolder view] viewWithTag:1000];
        ((UILabel *) [helpView viewWithTag:1001]).text = [LanguageManager get:@"ios_label_aide_utiliser_filtres"];
        helpView = [[_helpScreensHolder view] viewWithTag:2000];
        ((UILabel *) [helpView viewWithTag:2001]).text = [LanguageManager get:@"ios_label_aide_select_ressource"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIViewController*)viewControllerFromUIView:(UIView*)view{
    
    UIViewController * newVC = [[UIViewController alloc]  init];
    CGRect frame = view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    
    view.frame = frame;
    [newVC setView:view];
    
    return newVC;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {

    // Return the data view controller for the given index.
    if (([_dataSource count] == 0) || (index >= [_dataSource count])) {
        return nil;
    }
    return [_dataSource objectAtIndex:index];
    
}

- (NSUInteger)indexOfViewController:(UIViewController *)viewController
{
    // Return the index of the given data view controller.
    // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [_dataSource indexOfObject:viewController];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
//    
//    
//    if (_currentIndex == 0) {
//        return nil;
//    }
//    
//    // Decrease the index by 1 to return
//    _currentIndex--;
//    
//    return [self viewControllerAtIndex:_currentIndex];
//
    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
//    
//    _currentIndex++;
//    
//    if (_currentIndex == [_dataSource count]) {
//        return nil;
//    }
//    
//    return [self viewControllerAtIndex:_currentIndex];

    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [_dataSource count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return [_dataSource count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator. 
    return _currentIndex;
}

@end
