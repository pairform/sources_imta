//
//  RootNavigation.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "RootNavigation.h"
#import "JASidePanelController.h"
#import "Ressource.h"
#import "LanguageManager.h"
#import "MessageController.h"

//@class MessageController;

@interface RootNavigation ()
@end

@implementation RootNavigation


- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Lancement de l'application"];    
    self.delegate = self;
//    [self setHidesBarsOnSwipe:YES];
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];


//    Custom background image pour la navigationBar
//    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.navigationBar.frame];
//    imageView.contentMode = UIViewContentModeLeft;
//    imageView.image = [UIImage imageNamed:@"NavBar-iPhone.png"];
//    [self.navigationBar insertSubview:imageView atIndex:0];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastDownloadedRessource:)
                                                 name:@"unzipFinished" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toastErrorRessource:)
                                                 name:@"downloadErrorToast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages:)
                                                 name:@"downloadFinished" object:nil];
}

-(void)toastDownloadedRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    NSString * capsule_nom_court = notification.userInfo[@"capsule_nom_court"];
    
    dispatch_block_t block = ^{
        UIImage * icon = [downloadedRessource getIcone];
        NSString * title = [capsule_nom_court stringByAppendingFormat:@" : %@", [LanguageManager get:@"ios_label_telechargement_terminé"]];
        NSString * name = downloadedRessource.nom_court;
        [self.view makeToast:title duration:3.0 position:@"bottom" title: name image: icon style:nil completion:nil];
    };
    
    if ([NSThread isMainThread])
        block(); // execute the block directly, as main is already the current active queue
    else
        dispatch_sync(dispatch_get_main_queue(), block); // ask the main queue, which is not the current queue, to execute the block, and wait for it to be executed before continuing
}

-(void)toastErrorRessource:(NSNotification*) notification
{
    Ressource * downloadedRessource = notification.object;
    NSString * capsule_nom_court = notification.userInfo[@"capsule_nom_court"];
    
    dispatch_block_t block = ^{
        UIImage * icon = [downloadedRessource getIcone];
        NSString * title = [capsule_nom_court stringByAppendingFormat:@" : %@", [LanguageManager get:@"ios_label_erreur_telechargement"]];
        NSString * name = downloadedRessource.nom_court;
        [self.view makeToast:title duration:3.0 position:@"bottom" title: name image: icon style:nil completion:nil];
    };
    
    if ([NSThread isMainThread])
        block(); // execute the block directly, as main is already the current active queue
    else
        dispatch_sync(dispatch_get_main_queue(), block); // ask the main queue, which is not the current queue, to execute the block, and wait for it to be executed before continuing

}

-(void)refreshMessages:(NSNotification*)notification{
//    Ressource * res = notification.object;
//    [UtilsCore rafraichirMessagesFromCapsule:res.id_ressource];
    
    NSNumber * id_capsule = notification.userInfo[@"id_capsule"];
    [UtilsCore rafraichirMessagesFromCapsule:id_capsule];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    //Depuis iOS 8, méthode native pour cacher la barre au swipe pour laisser de la place
    //Uniquement dans la vue de messages
    if ([viewController isKindOfClass:[MessageController class]]) {
        [self setHidesBarsOnSwipe:YES];
    }
    else {
        [self setHidesBarsOnSwipe:NO];
    }
    viewController.navigationItem.rightBarButtonItem = [(JASidePanelController*)[self parentViewController] customRightButtonForCenterPanel];
    
//    UIImage *backButtonImage = [UIImage imageNamed:@"precedent.png"];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
//    [[UIBarButtonItem appearance] setBackButtonBackgroundImage: backButtonImage forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];

//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage *backBtnImage = [UIImage imageNamed:@"precedent.png"];
//    
//    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
//    
//    backBtn.frame = CGRectMake(0, 0, 30, 30);
//    
//    [backBtn addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
//    [backBtn setShowsTouchWhenHighlighted:true];
//    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    [backButton setTitle:@"."];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
//                                   initWithTitle:@"Retour" style:UIBarButtonItemStyleBordered
//                                   target:nil action:nil];
//    
//    viewController.navigationItem.backBarButtonItem = backButton;
//    viewController.navigationItem.rightBarButtonItems = @[backButton,[(JASidePanelController*)[self parentViewController] rightButtonForCenterPanel]];
    
    if([navigationController.viewControllers count ] > 1) {
        UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0,0,44,44)];
        UIButton *myBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [myBackButton setFrame:CGRectMake(5,6,32,32)];
        [myBackButton setImage:[UIImage imageNamed:@"arrow_left_64"] forState:UIControlStateNormal];
        [myBackButton setEnabled:YES];
        [myBackButton setReversesTitleShadowWhenHighlighted:true];
        [myBackButton setShowsTouchWhenHighlighted:true];
        [myBackButton addTarget:viewController.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
        [backButtonView addSubview:myBackButton];
        
        UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
        viewController.navigationItem.leftBarButtonItem = backButton;
    }
}

@end
