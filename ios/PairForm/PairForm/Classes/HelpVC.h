//
//  HelpVC.h
//  PairForm
//
//  Created by Maen Juganaikloo on 17/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpVC : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) UIViewController *helpScreensHolder;
@property (strong, nonatomic) NSArray *dataSource;
//@property (strong, nonatomic) IBOutlet UIPageControl * pageControl;
@property (nonatomic, weak) NSString * screenStoryboardID;
@property (nonatomic) NSUInteger currentIndex;

-(id)initWithIndex:(int)index;
-(UIViewController*)viewControllerFromUIView:(UIView*)view;
@end
