//
//  NotificationTVC.m
//  PairForm
//
//  Created by Maen Juganaikloo on 30/04/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "NotificationTVC.h"
#import "Notification.h"
#import "SommaireVC.h"
#import "UtilsCore.h"
#import "LanguageManager.h"
#import "ProfilController.h"
#import "RessourceStoreVC.h"

@interface NotificationTVC ()

@end

@implementation NotificationTVC
- (id)initWithNotificationType:(NSString*)type
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        _notification_type = type;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([_notification_type isEqualToString:@"us"]) {
        
        [self.tableView registerNib:[UINib nibWithNibName:@"ScoreCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NotificationCell"];
        [self setTitle:[LanguageManager get:@"title_notif_points"]];
    }
    else{
        [self.tableView registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NotificationCell"];
        [self setTitle:[LanguageManager get:@"title_notif_notifications"]];
    }
    //Récuperation des notifications
    NSArray * notification_array = [[UtilsCore getAllNotificationsForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:_notification_type] mutableCopy];
    [self sortNotificationsByDate:notification_array ofType:_notification_type];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    if ( [notification_array count] != 0 ){
        [self.tableView.tableHeaderView setHidden:YES];
    }
    else{
        UIView * no_notifications = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        UILabel * no_notifications_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 320, 35)];

        if ([_notification_type isEqualToString:@"us"]) {
            [no_notifications_label setText:[LanguageManager get:@"label_no_notif_points"]];
        }
        else{
            [no_notifications_label setText:[LanguageManager get:@"label_no_notif_notifications"]];
        }

        [no_notifications_label setTextAlignment:NSTextAlignmentCenter];
        [no_notifications addSubview:no_notifications_label];
        [self.tableView setTableHeaderView:no_notifications];
        [self.tableView.tableHeaderView setHidden:NO];
    }
    [UtilsCore setUnseenNotificationOnAppBadge];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.preferredContentSize = CGSizeMake(self.tableView.frame.size.width,self.tableView.frame.size.height);
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.preferredContentSize = self.tableView.contentSize;
}

-(void) sortNotificationsByDate:(NSArray*)notification_array ofType:notification_type{
    
    NSMutableArray * today_notif = [[NSMutableArray alloc] init];
    NSMutableArray * yesterday_notif = [[NSMutableArray alloc] init];
    NSMutableArray * seven_days_notif = [[NSMutableArray alloc] init];
    NSMutableArray * thirty_days_notif = [[NSMutableArray alloc] init];
    NSMutableArray * latter_days_notif = [[NSMutableArray alloc] init];
    
    
    //TODO : Classer selon les dates
    
    for (Notification * notif in notification_array) {
        NSDate * notif_date = notif.date_creation ? notif.date_creation : notif.date_vue;

        int days = -1;
        if (notif_date) {
            days = [self daysBetween:[NSDate date] and:notif_date];
        }

        notif.total_count = notif.points;

        // Aujourd'hui,
        if (days == 0) {
            [self factorizeNotification:notif inArray:today_notif];
        }
        // Hier,
        else if (days == 1) {
            [self factorizeNotification:notif inArray:yesterday_notif];
        }
        // 7 derniers jours,
        else if (days <= 7) {
            [self factorizeNotification:notif inArray:seven_days_notif];
        }
        // 30 derniers jours,
        else if (days <= 30) {
            [self factorizeNotification:notif inArray:thirty_days_notif];
        }
        // Plus anciens
        else{
            [self factorizeNotification:notif inArray:latter_days_notif];
        }
    }
    NSArray * notifications_header_titles = @[@"Aujourd'hui", @"Hier", @"7 derniers jours", @"30 derniers jours", @"Précédemment"];
    _notifications_header_concerned = [[NSMutableArray alloc] initWithCapacity:5];
    
    _notifications = [[NSMutableArray alloc] init];
    
    //Ajouts dans le tableau de notifications
    if([today_notif count] != 0){
        [_notifications addObject:today_notif];
        [_notifications_header_concerned addObject:notifications_header_titles[0]];
    }
    if([yesterday_notif count] != 0){
        [_notifications addObject:yesterday_notif];
        [_notifications_header_concerned addObject:notifications_header_titles[1]];
    }
    if([seven_days_notif count] != 0){
        [_notifications addObject:seven_days_notif];
        [_notifications_header_concerned addObject:notifications_header_titles[2]];
    }
    if([thirty_days_notif count] != 0){
        [_notifications addObject:thirty_days_notif];
        [_notifications_header_concerned addObject:notifications_header_titles[3]];
    }
    if([latter_days_notif count] != 0){
        [_notifications addObject:latter_days_notif];
        [_notifications_header_concerned addObject:notifications_header_titles[4]];
    }
    

}

-(void)factorizeNotification:(Notification*) notif inArray:(NSMutableArray *)m_array {
//    bool flag_found = false;
    //Dans le cas des scores uniquement
//    if([notif.type isEqualToString:@"us"]){
//        for (Notification * n in m_array) {
//            if ([n.id_message isEqualToNumber:notif.id_message] && (n != notif)) {
//                n.total_count = [NSNumber numberWithInt:([notif.points intValue] + [n.total_count intValue])];
//                flag_found = true;
//                break;
//            }
//        }
//    }
//    
//    if (!flag_found) {
        [m_array addObject:notif];
//    }

}
- (int)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2 {
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return ABS([components day]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [_notifications count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 22;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_notifications[section] count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * header_view =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
    [header_view setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:1]];
//    [header_view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"MenuCell"]]];
    UILabel * header_text = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 120, 18)];
    [header_text setText:_notifications_header_concerned[section]];
    [header_text setFont:[UIFont fontWithName:@"Helvetica Neue" size:16]];
    [header_text setTextColor:[UIColor colorWithWhite:0.9 alpha:1]];
    [header_view addSubview:header_text];
    
    return header_view;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return _notifications_header_concerned[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotificationCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    Notification * curr_notif = [_notifications[indexPath.section] objectAtIndex:[indexPath row]];
    
    //Image ou label, ça dépend
    if ([_notification_type isEqualToString:@"us"]) {
        UILabel * label_score = (UILabel*)[cell viewWithTag:1001];
        
        //Référence des points
        if ([curr_notif.total_count intValue] > 0){
            [label_score setTextColor:[UIColor colorWithRed:0.0f green:89/255.0f blue:0.0f alpha:1]];
            [label_score setText:[NSString stringWithFormat:@"+%@",[curr_notif.total_count stringValue]]];
        }
        else{
            [label_score setTextColor:[UIColor orangeColor]];
            [label_score setText:[curr_notif.total_count stringValue]];
        }
    }
    else{
        UIImageView * image_notif = (UIImageView*) [cell viewWithTag:1001];
        //Cas spécial des médailles, il ne faut pas les redimensionner, sinon c'est moche
        if ([curr_notif.sous_type isEqualToString:@"gm"]){
//            [image_notif setImage:[NSKeyedUnarchiver unarchiveObjectWithData:curr_notif.illustration]];
            [image_notif setImage:[UIImage imageNamed:curr_notif.illustration]];
            [image_notif setContentMode:UIViewContentModeScaleAspectFit];
        }
        else{
//            [image_notif setImage:[[NSKeyedUnarchiver unarchiveObjectWithData:curr_notif.illustration] thumbnailImage:32 transparentBorder:1 cornerRadius:0 interpolationQuality:kCGInterpolationHigh]];
            [image_notif setImage:[[UIImage imageNamed:curr_notif.illustration] thumbnailImage:32 transparentBorder:1 cornerRadius:0 interpolationQuality:kCGInterpolationHigh]];
            [image_notif setContentMode:UIViewContentModeCenter];
        }
        [image_notif setClipsToBounds:NO];
    }
    
    //Nom de l'action
    UILabel * label_action = (UILabel*)[cell viewWithTag:1002];
    [label_action setText:[curr_notif titre]];
    
    //Nom de la ressource
    UILabel * label_content = (UILabel*)[cell viewWithTag:1003];
    
    if ([curr_notif.contenu intValue]){
        Capsule * capsule = [UtilsCore getCapsule:[NSNumber numberWithInt:[curr_notif.contenu intValue]]];
        NSString * sub_label;
        if (capsule) {
            sub_label = [NSString stringWithFormat:@"Sur %@",capsule.nom_court];
        }
        //Si on est sur une notification personnelle (type ajout dans un réseau par un autre utilisateur)
        else if([curr_notif.id_message isEqualToNumber:@0] && [curr_notif.contenu intValue] != 0){
            sub_label = @"Cliquez pour voir son profil";
        }
        else{
            sub_label = @"Sur une autre ressource";
        }
        
        [label_content setText: sub_label];
    }
    else
        [label_content setText: [curr_notif contenu]];
    
    
    
    //Heure ou date, en fonction de la section
    UILabel * label_date = (UILabel*)[cell viewWithTag:1004];
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    if ([_notifications_header_concerned[indexPath.section] isEqualToString:@"Aujourd'hui"] || [_notifications_header_concerned[indexPath.section] isEqualToString:@"Hier"]) {
        [df setDateFormat:@"H:mm"];
    }
    else{
        [df setDateFormat:@"dd/MM"];
    }
    [label_date setText: [df stringFromDate:[curr_notif date_creation]]];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Notification * touched_notification = _notifications[indexPath.section][indexPath.row];
    
    Message * selected_message = [UtilsCore getMessage:touched_notification.id_message];
    
    Capsule * selected_capsule = [UtilsCore getCapsule:selected_message.id_capsule];
    
    [UtilsCore setMessageLu:selected_message.id_message];
    
    UIStoryboard * mainStoryboard = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] getIPadStoryBoard];
    
    id pushed_vc;
    
    //Si on est dans le cas d'un succes, il n'y a pas de message attaché
    if ([touched_notification.id_message isEqualToNumber:@0]) {
        ProfilController * profilVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProfilController"];
        [profilVC setStackPosition:SCStackViewControllerPositionLeft];
            
        //Si on est dans le cas d'un ajout à un cercle, ya un id d'utilisateur dans le contenu
        int id_utilisateur_distant = [touched_notification.contenu intValue];
        
        //S'il est égal à 0, c'est que ce n'est pas un int (le cast renvoie 0 dans le cas d'une conversion ratée)
        if (id_utilisateur_distant != 0) {
            //On vérifie que l'utilisateur correspondant existe
            NSNumber * id_utilisateur_distant_number = [NSNumber numberWithInt: id_utilisateur_distant];
            if ([UtilsCore getUtilisateur: id_utilisateur_distant_number]) {
                //Si c'est cool, on va ouvrir la page profil de cette personne
                [profilVC setId_utilisateur:id_utilisateur_distant_number];
            }
        }

        pushed_vc = profilVC;
    }
    //Important : si on a pas la ressource, on renvoie vers le store
    else if(!selected_capsule){
        RessourceStoreVC * ressourceStoreVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RessourceStoreVC"];
        [ressourceStoreVC setStackPosition:SCStackViewControllerPositionLeft];
        //Si on a le message (ce qui ne devrait pas arrivé, je vois pas pourquoi j'ai codé ça. Dans le doute, je laisse)
        if (selected_message)
            [ressourceStoreVC setIdRessourceToHighlight:selected_message.id_capsule];
        else
            [ressourceStoreVC setIdRessourceToHighlight:[NSNumber numberWithInt:[touched_notification.contenu intValue]]];
        
        pushed_vc = ressourceStoreVC;
    }
    
    //Si on est sur une ressource entière
    else if ([selected_message.nom_page isEqualToString:@""]){
        
        SommaireVC * sommaireVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SommaireVC"];
        sommaireVC.currentCapsule = selected_capsule;
        [sommaireVC showMessageTrans:selected_message.id_message];
        
        if (IS_IPAD) {
            [sommaireVC setStackPosition:SCStackViewControllerPositionRight];
        }
        pushed_vc = sommaireVC;
    }
    //Sinon, on est sur un grain
    else{
        NSMutableArray * pageData = [[NSMutableArray alloc] init];
        [pageData addObject:[NSString stringWithFormat:@"%@/%@",[selected_capsule getPath],selected_message.nom_page]];
        PageVC * pageVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageVC"];
        
        if (IS_IPAD) {
            [pageVC setStackPosition:SCStackViewControllerPositionRight];
        }
        pageVC.pageData = [pageData copy];
        pageVC.currentCapsule = selected_capsule;
        pageVC.selectedIndex = 0;
        pageVC.tag_trans = selected_message.nom_tag;
        pageVC.num_occurence_trans = selected_message.num_occurence;
        pageVC.id_message_trans = selected_message.id_message;
        pushed_vc = pageVC;
    }
    if (IS_IPAD) {
        SCStackViewController * stackViewController = [(RootVC_iPad*)[[_parent_vc viewControllers] firstObject] stackViewController];
        [_parent_vc.user_notif_popover dismissPopoverAnimated:YES];
        [_parent_vc.user_score_popover dismissPopoverAnimated:YES];
        
        [stackViewController popToRootViewControllerFromPosition:[pushed_vc stackPosition] animated:YES completion:^{
            [stackViewController pushViewController:pushed_vc fromViewController:self atPosition:[pushed_vc stackPosition] unfold:TRUE animated:TRUE completion:nil];
        }];
    }
    else{
        [self.navigationController pushViewController:pushed_vc animated:YES];
        
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */
#pragma mark - Popover delegate

- (CGSize)contentSizeForViewInPopover {
    if ([_notifications count] != 0) {
        // Currently no way to obtain the width dynamically before viewWillAppear.
        CGFloat width = 320.0;
        CGRect rect = [self.tableView rectForSection:[self.tableView numberOfSections] - 1];
        CGFloat height = CGRectGetMaxY(rect);
        return (CGSize){width, height};
    }
    return (CGSize){320, 40};
}
@end
