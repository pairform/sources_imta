//
//  InfosVC.m
//  PairForm
//
//  Created by VESSEREAU on 16/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "InfosVC.h"
#import "LanguageManager.h"

@interface InfosVC ()

@end

@implementation InfosVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setContent];
    self.textInfos.editable = NO;
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
}

- (void) setContent {
    [self setTitle:[LanguageManager get:@"title_informations"]];
    
    // Mise à jour du texte des informations sur PairForm
    NSString *text = [NSString stringWithFormat:@"%@\n", [LanguageManager get:@"infos_pairform_dev"]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"\t%@ Christian Colin\n", [LanguageManager get:@"infos_chef_projet"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"\t%@ Maen Juganaikloo, Albert Cau, Didier Putman, Francisco Gutierrez, Bastien Aubry, Weiham Bao, Yannis Vessereau\n", [LanguageManager get:@"infos_participation_dev"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"\t%@ Evelyne Moreau, Jean-Michel Pichard, Eliane Vacheret\n\n", [LanguageManager get:@"infos_participation_projet"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n", [LanguageManager get:@"infos_partenaire_projet"]]];
    text = [text stringByAppendingString:@"\tEcole Centrale de Nantes\n"];
    text = [text stringByAppendingString:@"\tTélécom Bretagne\n"];
    text = [text stringByAppendingString:@"\tUniversité de Lorraine\n\n"];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n", [LanguageManager get:@"infos_financeurs"]]];
    text = [text stringByAppendingString:@"\tEcole des Mines de Nantes\n"];
    text = [text stringByAppendingString:@"\tInstitut Mines Télécom\n"];
    text = [text stringByAppendingString:@"\tUNIT\n"];
    text = [text stringByAppendingString:@"\tMission Numérique de l'Enseignement Supérieur\n\n"];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n", [LanguageManager get:@"infos_distinctions"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"\t%@\n\n", [LanguageManager get:@"infos_pre_version"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\tcontact@pairform.fr", [LanguageManager get:@"infos_contact"]]];
    
    self.textInfos.text = text;
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
