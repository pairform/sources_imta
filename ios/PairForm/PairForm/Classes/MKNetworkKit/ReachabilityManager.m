//
//  ReachabilityManager.m
//  PairForm
//
//  Created by vincent on 29/05/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "ReachabilityManager.h"

@implementation ReachabilityManager

+ (ReachabilityManager *)sharedManager {
    static ReachabilityManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)dealloc {
    // Stop Notifier
    if (_reachability) {
        [_reachability stopNotifier];
    }
}

- (id)init {
    self = [super init];
    
    if (self) {
        struct sockaddr_in address;
        address.sin_len = sizeof(address);
        address.sin_family = AF_INET;
        address.sin_port = htons(sc_port);
        address.sin_addr.s_addr = inet_addr([sc_hostname UTF8String]);
        // Initialize Reachability
        self.reachability = [Reachability reachabilityWithAddress:&address];
        //        self.reachability = [Reachability reachabilityWithHostname:sc_hostname];
        //        [self.reachability connectionRequired];
        // Start Monitoring
        [self.reachability startNotifier];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    }
    
    return self;
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        if(![UIViewController estLieAuServeur]){
            //On n'est plus connecté et on a récupérer le réseau
//            DLog(@"%%%%%%%%%%%%%%%%%%%%%%\% J'ouvre une alert view%%%%%%%%%%%%%%%%%%%");

            
            if([[[[UIApplication sharedApplication] keyWindow] rootViewController] checkForDefaultUser])
            {
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] checkForLoggedInUser];
            }

        }
    } else {
        DLog(@"00000000000!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Unreachable");
    }
}


@end
