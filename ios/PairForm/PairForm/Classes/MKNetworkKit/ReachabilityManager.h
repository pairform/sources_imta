//
//  ReachabilityManager.h
//  PairForm
//
//  Created by vincent on 29/05/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReachabilityManager : NSObject

@property (retain, nonatomic) Reachability *reachability;

+ (ReachabilityManager *)sharedManager;


@end
