

#import <UIKit/UIKit.h>
#import "PTSSpringBoard.h"
#import "RessourceCollection.h"

//@interface HomeVC : UIViewController <HMLauncherDataSource, HMLauncherViewDelegate>
@interface HomeVC : UIViewController

@property (nonatomic, strong) PTSSpringBoard * springBoard;
//@property (nonatomic, strong) IBOutlet RessourceCollection * springBoard;
@property (nonatomic, strong) NSMutableArray * ressourcesArray;
@property (nonatomic, strong) NSMutableArray * downloadingRessourcesArray;
//@property (nonatomic,strong) UICollectionView * collectionView;

@property (weak, nonatomic) IBOutlet UINavigationItem *navigationMesRessources;

-(IBAction)showActionSheet:(id)sender forEvent:(UIEvent*)event;

-(void)registerDownloadingRessource:(NSNotification*) notification;
-(void)updateProgressionForRessource:(NSNotification*) notification;
-(void)finishDownloadingRessource:(NSNotification*) notification;
-(void)refreshMessages;
@end
