//
//  UIButton+VerticalLayout.m
//  PairForm
//
//  Created by Maen Juganaikloo on 07/10/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "UIButton+VerticalLayout.h"

@implementation UIButton (VerticalLayout)


- (void)centerVerticallyWithImageWidth:(float)width andVerticalPadding:(float)padding
{
    CGSize imageSize = CGSizeMake(width, width);
    CGSize buttonSize = self.frame.size;


    [self setImage:[self.imageView.image resizedImage:imageSize interpolationQuality:kCGInterpolationHigh] forState:UIControlStateNormal];
    
    self.imageEdgeInsets = UIEdgeInsetsMake(padding,
                                            (buttonSize.width - imageSize.width) /2,
                                            buttonSize.height - (imageSize.height + padding),
                                            (buttonSize.width - imageSize.width) /2);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(padding + imageSize.height,
                                            - imageSize.width,
                                            0.0f,
                                            0.0f);
    
}

- (void)centerVerticallyWithPadding:(float)padding
{
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height),
                                            0.0f,
                                            0.0f,
                                            - titleSize.width);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f,
                                            - imageSize.width,
                                            - (totalHeight - titleSize.height),
                                            0.0f);
    
}


- (void)centerVertically
{
    const CGFloat kDefaultPadding = 6.0f;
    
    [self centerVerticallyWithPadding:kDefaultPadding];
}  


@end
