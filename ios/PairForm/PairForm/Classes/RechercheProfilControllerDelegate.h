//
//  RechercheProfilControllerDelegate.h
//  SupCast
//
//  Created by admin on 06/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RechercheProfilControllerDelegate <NSObject>
- (void)ajouteMembreDansCercleDepuisRechercheProfil:(NSDictionary *) utilisateur;
@end
