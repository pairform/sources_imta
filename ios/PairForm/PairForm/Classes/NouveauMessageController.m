//
//  AddCommentViewController.m
//  SupCast
//
//  Created by fgutie10 on 22/07/10.
//  Copyright (c) 2010 EMN - CAPE. All rights reserved.
//

#import "NouveauMessageController.h"
#import "UtilsCore.h"
#import "LanguageManager.h"

@interface NouveauMessageController()
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *boutonPublier;
@property (weak, nonatomic) IBOutlet UIButton *boutonLangue;
@property (weak, nonatomic) IBOutlet UILabel *labelLangue;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property(nonatomic,strong) IBOutlet UIButton * gearButton;
@property(nonatomic,strong) NSMutableArray * rightButtonsArray;
@property(nonatomic,strong) NSMutableArray * visibiliteIdArray;
@property(nonatomic,strong) NSMutableArray * visibiliteArray;
@property(nonatomic,strong) NSMutableArray * tagArray;
@property(nonatomic) BOOL  boolDefi;
@property(nonatomic,strong) NSString * langueMsg;
@property (nonatomic, strong) NSArray *langueValuesString;
@property (weak, nonatomic) IBOutlet UIImageView *ivUnderText;
@property CGRect initialFrameForText;
@property (weak, nonatomic) IBOutlet UISwitch *switchTwitter;

@end



@implementation NouveauMessageController


@synthesize textView;
@synthesize boutonPublier;
@synthesize urlPage;
@synthesize idMessage;
@synthesize currentRessource;
@synthesize uiNavigationItem;
@synthesize gearButton;
@synthesize rightButtonsArray;
@synthesize visibiliteIdArray;
@synthesize visibiliteArray;
@synthesize tagArray;
@synthesize boolDefi;
@synthesize langueMsg;
@synthesize idMessageOriginal;
@synthesize nom_tag;
@synthesize num_occurence;
@synthesize ivUnderText;
@synthesize initialFrameForText;
@synthesize switchTwitter;

#pragma mark -
#pragma mark View Load


// Rotation de l'iPhone  
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    DLog(@"");
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {        
        if ( (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)      ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
        {
            return NO;   
        }
    }
    return YES;    
}

-(NSString *)dataFilePath:(NSString *)fileName {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
	
}

//Copié
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    DLog(@"");
    
    [self setTitle:[LanguageManager get:@"ios_title_ecrire"]];
    
    //Options
    if (tagArray == nil) tagArray = [[NSMutableArray alloc]init];
    if (visibiliteArray == nil ) visibiliteArray = [[NSMutableArray alloc]initWithArray:[NSMutableArray arrayWithObjects:[LanguageManager get:@"ios_label_public"],nil] ];
    if (visibiliteIdArray == nil ) visibiliteIdArray = [[NSMutableArray alloc]initWithArray:[NSMutableArray arrayWithObjects:@2,nil] ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Ecriture d'un message"];
    
    /*if ( [[self getPreference:@"publishTwitter"] integerValue] == 1){
        [_bCopublier setHidden:YES];
    }*/

    if ( [[self getPreference:@"publishTwitter" ] integerValue] == 1) {
        [switchTwitter setOn:YES];
    }
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];
    
    //Init langueMsg
    langueMsg = [LanguageManager getCodeMainLanguage];
    langueMsg = [LanguageManager codeLangueWithid:[self.currentRessource.langue intValue]];
    
    // Modification du drapeau de la langue du message
    [self.boutonLangue setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icone_drapeau_rond_%@.png", langueMsg]] forState:UIControlStateNormal];
    
    // Recuperation des langues dans le fichier PairForm-Arrays.plist
    self.langueValuesString = [LanguageManager getArrayTrueNameLanguage];
    
    [self setDesign];
    [self setContent];

    if (IS_IPAD)
       [self setStackWidth: 320];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (IS_IPHONE) {
        // navigation button bar
        rightButtonsArray = [uiNavigationItem.rightBarButtonItems mutableCopy] ;
        if ( rightButtonsArray.count == 1){
            gearButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [gearButton setFrame:CGRectMake(5,6,32,32)];
            [gearButton addTarget:self action:@selector(afficherOptionMessage:) forControlEvents:UIControlEventTouchUpInside];
            [gearButton setImage:[UIImage imageNamed:@"gear_64"] forState:UIControlStateNormal];
            UIBarButtonItem *bOption = [[UIBarButtonItem alloc] initWithCustomView:gearButton];
            [rightButtonsArray addObject:bOption];
            uiNavigationItem.rightBarButtonItems = rightButtonsArray;
        }
    }

}

-(void) setContent {
    self.labelLangue.text = [NSString stringWithFormat:@"%@ :",  [LanguageManager get:@"label_langue_du_message"]];
    [self.boutonPublier setTitle:[LanguageManager get:@"button_publier_message"] forState:UIControlStateNormal];
}

- (void) setDesign {
    self.boutonLangue.layer.cornerRadius = 3;
    self.boutonLangue.layer.borderWidth = 1;
    self.boutonLangue.layer.borderColor = [UIColor orangeColor].CGColor;
}

-(void)putDoneEditingButton{
    NSMutableArray *items = [[NSMutableArray alloc] init];

    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(5,6,32,32)];
    [a1 addTarget:self action:@selector(gestionClavier) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:[UIImage imageNamed:@"keyboard_64"] forState:UIControlStateNormal];
    UIBarButtonItem *bOption = [[UIBarButtonItem alloc] initWithCustomView:a1];
    [items addObject:bOption];
    [uiNavigationItem setRightBarButtonItems:items animated:YES];
}

- (IBAction)gestionClavier {
    DLog(@"");
    [uiNavigationItem setRightBarButtonItems:rightButtonsArray animated:YES];
	if([textView isFirstResponder])
    {
		[textView resignFirstResponder];
	}
}

- (IBAction)modifierLangueMessage:(id)sender {
    [RMPickerViewController setLocalizedTitleForCancelButton:[LanguageManager get:@"button_annuler"]];
    [RMPickerViewController setLocalizedTitleForSelectButton:[LanguageManager get:@"ios_button_selectionner"]];
    RMPickerViewController *pickerVC = [RMPickerViewController pickerController];
    pickerVC.delegate = self;
    pickerVC.titleLabel.text = [LanguageManager get:@"label_langue_du_message"];
    
    [pickerVC show];
}

-(IBAction)afficherOptionMessage:(id)sender{
    [self performSegueWithIdentifier: @"messageOptionSegue" sender: self];
}


- (void)keyboardWillShow:(NSNotification *)aNotification
{
    [self putDoneEditingButton];
    
    /*
     Reduce the size of the text view so that it's not obscured by the keyboard.
     Animate the resize so that it's in sync with the appearance of the keyboard.
     */
    
    NSDictionary *userInfo = [aNotification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's
    // coordinate system. The bottom of the text view's frame should align with the top
    // of the keyboard's final position.
    //
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = textView.frame;
    initialFrameForText =  textView.frame;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y - textView.frame.origin.y;
    
    // Get the duration of the animation.
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.textView.frame = newTextViewFrame;
    
    [UIView commitAnimations];
}


- (void)keyboardWillHide:(NSNotification *)aNotification {
    
    NSDictionary *userInfo = [aNotification userInfo];
    
    /*
     Restore the size of the text view (fill self's view).
     Animate the resize so that it's in sync with the disappearance of the keyboard.
     */
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.textView.frame = initialFrameForText;
    
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark Actions 


-(IBAction)sendMessage {
    
    DLog(@"");
	NSString *message = [[NSString alloc] initWithString:textView.text];
    
	if ([message length] != 0)
    {
        if ( switchTwitter.isOn ) [self copublier];
        else [self publierMessage];
    }        
    
	else 
    {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_erreur_saisie"] andMessage:[LanguageManager get:@"label_erreur_saisie_message"]];
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        [alertView show];
	}
    
}



- (void)copublier {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSString * initialText = [NSString stringWithFormat:@"#%@ #%@ ",@"PairForm",currentRessource.nom_court];
        /*if ( ![urlPage isEqualToString:@""] ){
            initialText = [NSString stringWithFormat:@"%@#%@  ",initialText,urlPage ];
        }*/
        if (textView.text.length + initialText.length > 140){
            //NSRange r = NSMakeRange(0, 140);
            //initialText = [initialText substringWithRange: r];
            [UIPasteboard generalPasteboard].string = textView.text ;
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Tweet" andMessage:[LanguageManager get:@"ios_label_message_trop_long"]];
            [alertView addButtonWithTitle:@"Ok"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                      
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            //        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
            
            [alertView show];
        }else{
            initialText = [NSString stringWithFormat:@"%@ %@",initialText,textView.text  ];
        }
       
        
        [mySLComposerSheet setInitialText:initialText];
        
        
        //[mySLComposerSheet addURL:[NSURL URLWithString:@"http://...."]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    DLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    DLog(@"Post Sucessful");
                    [self publierMessage];
                    break;
                    
                default:
                    break;
               
            }
            [self dismissViewControllerAnimated:YES completion:nil];
            [self gestionClavier];
            
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
}





#pragma mark - 


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark -
#pragma mark Publication




-(void)publierMessage {
    
    NSMutableDictionary * params;
    NSString * message = [[NSMutableString alloc] initWithString:textView.text];
    
    //debut convertion object to json
    NSError* error = nil;
    NSString *jsonStringVisiblite = @"";
    NSString *jsonStringTag = @"";
    
    // Visibilite et tagarray
    if ( visibiliteIdArray  && tagArray){
        NSData* jsonDataVisiblite = [NSJSONSerialization dataWithJSONObject:visibiliteIdArray
                                                                    options:NSJSONWritingPrettyPrinted error:&error];
        jsonStringVisiblite = [[NSString alloc] initWithData:jsonDataVisiblite encoding:NSUTF8StringEncoding];
        NSData* jsonDataTag = [NSJSONSerialization dataWithJSONObject:tagArray
                                                              options:NSJSONWritingPrettyPrinted error:&error];
        jsonStringTag = [[NSString alloc] initWithData:jsonDataTag encoding:NSUTF8StringEncoding];
    }

    
    // fin conversion object to json
    
    
    if([self idMessage])
        params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:currentRessource.id_ressource, @"id_ressource", [self urlPage], @"nom_page", [self idMessage], @"id_message", message, @"message", jsonStringVisiblite,@"visibilite",jsonStringTag,@"tag",boolDefi,@"est_defi",nil];
    else
       params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:currentRessource.id_ressource, @"id_ressource", [self urlPage], @"nom_page",  message, @"message",jsonStringVisiblite,@"visibilite",jsonStringTag,@"tag",nil];
    if([self idMessage]){
        [params setObject:idMessage forKey:@"nom_page"];
    }
    
    // Langue du message
    langueMsg = [[NSNumber numberWithInt:[LanguageManager idLangueWithCode:langueMsg]] stringValue];
    [params setObject:langueMsg forKey:@"langue"];
    
    // Si reponse
    if ( idMessageOriginal ) [params setObject:idMessageOriginal forKey:@"id_message_original"];
    // Si objet apprentisage
    if ( nom_tag && num_occurence) {
        [params setObject:nom_tag forKey:@"nom_tag"];
        [params setObject:num_occurence forKey:@"num_occurence"];
    }
    // si defi
    if ( boolDefi){
        [params setObject:@"" forKey:@"est_defi"];
    }

    
    [WService post:[NSString stringWithFormat:@"%@/messages/enregistrerMessage.php",sc_server]
            param: params
            callback: ^(NSDictionary *wsReturn) {
                if(![WService displayErrors:wsReturn])
                {
                    [UtilsCore rafraichirMessagesFromMessageId:idMessage];
                    //Enregistrement des points
                    [UtilsCore recordNotificationFromDictionary:@{@"id_user" : [UIViewController getSessionValueForKey:@"id_utilisateur"],
                                                                  @"type" : @"us",
                                                                  @"title" : @"Message posté",
                                                                  @"score_type": @"PTSECRIREMESSAGE",
                                                                  @"content": currentRessource.nom_court,
                                                                  @"id_message": wsReturn[@"id_message"]}];
                    if (IS_IPHONE) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else{
                        [self.sc_stackViewController popViewControllerAtPosition:[self stackPosition]
                                                                                            animated:YES
                                                                                          completion:^(void){
                                                                                              
                                                                                          }];
                    }
                }
            }];
     
}
#pragma mark - PrepareForSegue et delegate


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"messageOptionSegue"])
    {
        MessageOptionController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        viewC.boolDefi = boolDefi;
        viewC.langueMsg = langueMsg;
        viewC.visibiliteArray = visibiliteArray;
        viewC.visibiliteIdArray = visibiliteIdArray;
        viewC.tagArray = tagArray;
        viewC.id_ressource = currentRessource.id_ressource;
        viewC.idMessageOriginal = idMessageOriginal;
    }
}

- (void)recupereOptions:(NSMutableArray *)visibilite visibiliteId:(NSMutableArray *)visibiliteId tag:(NSMutableArray *)tag defi: (BOOL) defi langueMsg: (NSString *) langue {
    boolDefi = defi;
    langueMsg = langue;
    visibiliteIdArray = visibiliteId;
    visibiliteArray = visibilite;
    tagArray = tag;
}


#pragma mark - RMPickerViewController Delegates
- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray *)selectedRows {
    langueMsg = [LanguageManager codeLangueWithArrayIndex:[[selectedRows objectAtIndex:0] intValue]];
    [self.boutonLangue setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icone_drapeau_rond_%@.png", langueMsg]] forState:UIControlStateNormal];
}
- (void)pickerViewControllerDidCancel:(RMPickerViewController *)vc {}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.langueValuesString count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.langueValuesString objectAtIndex:row];
}

@end