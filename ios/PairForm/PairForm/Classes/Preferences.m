//
//  Preferences.m
//  SupCast
//
//  Created by Maen Juganaikloo on 26/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "Preferences.h"
#import "SIAlertView.h"
#import "LanguageManager.h"

@interface Preferences ()

@property (nonatomic,weak) IBOutlet UISwitch * rememberMe;
@property (nonatomic,weak) IBOutlet UISwitch * accountTwitter;
@property (nonatomic,weak) IBOutlet UISwitch * publishTwitter;
@property (nonatomic,weak) IBOutlet UISwitch * geolocalisation;
@property (nonatomic,weak) IBOutlet UISwitch * automaticUpdate;
@property (nonatomic,weak) IBOutlet UISlider * updateFrequency;
@property (nonatomic,weak) IBOutlet UISwitch * tutorialMode;
@property (weak, nonatomic) IBOutlet UIButton * buttonAppLanguage;

@property (weak, nonatomic) IBOutlet UILabel *labelCompte;
@property (weak, nonatomic) IBOutlet UILabel *labelSeSouvenir;
@property (weak, nonatomic) IBOutlet UILabel *labelMessages;
@property (weak, nonatomic) IBOutlet UILabel *labelPublierTwitter;
@property (weak, nonatomic) IBOutlet UILabel *labelActiverGeolocalisation;
@property (weak, nonatomic) IBOutlet UILabel *labelMajAuto;
@property (weak, nonatomic) IBOutlet UILabel *labelLangueApplication;
@property (weak, nonatomic) IBOutlet UILabel *labelAutres;
@property (weak, nonatomic) IBOutlet UILabel *labelModeTuto;

@property (nonatomic, strong) NSString * usernameGPlus;
@property (nonatomic, strong) NSString * passwordGPlus;
@property (nonatomic) int updateFrequencyValue;
@property (nonatomic, strong) NSArray *langueValuesString;
@property (nonatomic) int indexLangueAppSelect;

-(IBAction)accountTwitterSwitched:(id)sender;
-(IBAction)updateFrequencyChanged:(id)sender;
-(IBAction)automaticUpdateChanged:(id)sender;
- (IBAction)touchDownButtonAppLanguage:(id)sender;

@end

@implementation Preferences

@synthesize rememberMe, accountTwitter, publishTwitter, geolocalisation, automaticUpdate, updateFrequency, tutorialMode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateFieldsWithPrefs];
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];

    if (IS_IPAD)
    {
        [self setStackWidth: 320];
    }
	// Do any additional setup after loading the view.
    
    // Recuperation des langues dans le fichier PairForm-Arrays.plist
    self.langueValuesString = [LanguageManager getArrayTrueNameLanguage];
    
    [self setDesign];
    [self setContent];
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    NSNumber * ufNumber = self.automaticUpdate.on ? [NSNumber numberWithFloat:self.updateFrequency.value] : @0;

    NSDictionary * parametres = [preferences persistentDomainForName: kPreferences];
    
    NSMutableDictionary *parametresMutable = parametres ? [parametres mutableCopy] : [[NSMutableDictionary alloc] init];
    
    [parametresMutable setObject:[NSNumber numberWithBool:self.rememberMe.on] forKey:@"rememberMe"];
//    [parametresMutable setObject:self.usernameGPlus forKey:@"usernameGPlus"];
//    [parametresMutable setObject:self.passwordGPlus forKey:@"passwordGPlus"];
    [parametresMutable setObject:[NSNumber numberWithBool:self.publishTwitter.on] forKey:@"publishTwitter"];
    [parametresMutable setObject:[NSNumber numberWithBool:self.geolocalisation.on] forKey:@"geolocalisation"];
    [parametresMutable setObject:[NSNumber numberWithBool:self.automaticUpdate.on] forKey:@"automaticUpdate"];
    [parametresMutable setObject:ufNumber forKey:@"updateFrequency"];
    [parametresMutable setObject:[NSNumber numberWithBool:self.tutorialMode.on] forKey:@"tutorialMode"];
    
    [preferences setPersistentDomain:parametresMutable forName:kPreferences];
}

- (void) setContent {
    // Traduction des termes suivant la langue de l'application
    self.title = [LanguageManager get:@"title_preferences"];
    self.labelCompte.text = [LanguageManager get:@"ios_title_compte"];
    self.labelSeSouvenir.text = [LanguageManager get:@"ios_label_se_souvenir"];
    self.labelMessages.text = [LanguageManager get:@"title_messages"];
    self.labelPublierTwitter.text = [LanguageManager get:@"ios_label_publier_auto_twitter"];
    self.labelActiverGeolocalisation.text = [LanguageManager get:@"ios_label_activer_geolocalisation"];
    self.labelMajAuto.text = [LanguageManager get:@"ios_label_maj_auto"];
    self.labelLangueApplication.text = [LanguageManager get:@"title_langue_application"];
    self.labelAutres.text = [LanguageManager get:@"title_autres"];
    self.labelModeTuto.text = [LanguageManager get:@"ios_label_mode_tuto"];
    [self.buttonAppLanguage setTitle:[LanguageManager nameLangueWithCode:[LanguageManager getCurrentCodeLanguageApp]] forState:UIControlStateNormal];
}


- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

- (void) setDesign {
    self.buttonAppLanguage.layer.cornerRadius = 3;
    self.buttonAppLanguage.layer.borderWidth = 1;
    self.buttonAppLanguage.layer.borderColor = [UIColor blueColor].CGColor;
    [self.buttonAppLanguage setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
}

-(void) updateFieldsWithPrefs
{
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];

    NSDictionary * parametres = [preferences persistentDomainForName: kPreferences];
    
    [self.rememberMe setOn: [parametres[@"rememberMe"] boolValue]];
    
    self.usernameGPlus = parametres[@"usernameGPlus"];
    self.passwordGPlus = parametres[@"passwordGPlus"];
    
    //S'il y a un username pour twiiter d'enregistré, on met le switch sur on
//    if(![self.usernameGPlus isEqualToString:@""])
//    {
//        [self.accountTwitter setOn:YES];
//        [self.publishTwitter setEnabled:YES];
//    }
//    else
//    {
//        [self.accountTwitter setOn:NO];
//        [self.publishTwitter setEnabled:NO];
//    }
    
    [self.publishTwitter setOn: [parametres[@"publishTwitter"] boolValue]];
    [self.geolocalisation setOn: [parametres[@"geolocalisation"] boolValue]];
    [self.automaticUpdate setOn: [parametres[@"automaticUpdate"] boolValue]];
    [self.tutorialMode setOn: [parametres[@"tutorialMode"] boolValue]];
    
    if([parametres[@"updateFrequency"] floatValue] == 0)
    {
        [self.automaticUpdate setOn:NO];
        [self.updateFrequency setValue:1];
        [self.updateFrequency setEnabled:NO];
    }
    else
    {
        [self.automaticUpdate setOn:YES];
        [self.updateFrequency setValue:[parametres[@"updateFrequency"] floatValue]];
    }
    
    // Recuperation de la langue de l'application
    int langueId = [parametres[@"idLangueApp"] intValue];
    NSString *langueString = [LanguageManager nameLangueWithId: langueId];
    self.indexLangueAppSelect = [LanguageManager arrayIndexWithId: langueId];
    
    // Initialisation du bouton de la langue de l'App
    [self.buttonAppLanguage setTitle:langueString forState:UIControlStateNormal];
}


-(IBAction)accountTwitterSwitched:(id)sender{
    if (self.accountTwitter.on) {
        UITextField * pseudoField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        pseudoField.placeholder = [LanguageManager get:@"label_pseudo"];
                
        UITextField * passField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        passField.placeholder = [LanguageManager get:@"label_mot_de_passe"];
        passField.secureTextEntry = YES;
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_connexion_twitter"] andTextFields:@[pseudoField,passField]];
        [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Annulation du log");
                                  [self.accountTwitter setOn:NO animated:YES];
                              }];
        [alertView addButtonWithTitle:[LanguageManager get:@"button_connexion"]
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Login Twitter");
                                  [self.view makeToast:[LanguageManager get:@"label_connexion_reussi"] duration:3.0 position:@"bottom" title:nil image:[UIImage imageNamed:@"checkmark_48"] style:nil completion:nil];
                                  
                                  //Enregistrement des usernames dans des variables
                                  [self setUsernameGPlus:pseudoField.text];
                                  [self setPasswordGPlus:passField.text];
                                  [self.publishTwitter setEnabled:YES];
                              }];
        
        alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        
        [alertView show];
    }
    else
    {
        //Enregistrement des usernames dans des variables
        [self setUsernameGPlus:@""];
        [self setPasswordGPlus:@""];
        [self.publishTwitter setEnabled:NO];
    }
}

-(IBAction)automaticUpdateChanged:(id)sender{
    if (self.automaticUpdate.on)
    {
        [self.updateFrequency setEnabled:YES];
        [self.view makeToast:[LanguageManager get:@"ios_label_deplacer_curseur"] duration:2 position:@"bottom" title:nil image:[UIImage imageNamed:@"alarm_48"] style:nil completion:nil];
        if ( [[NSNumber numberWithFloat:self.updateFrequency.value] intValue] == kMessageUpdateEveryMinute) [UtilsCore startPullingTimer];
    }
    else
    {
        [self.updateFrequency setEnabled:NO];
        self.updateFrequencyValue = kMessageUpdateNever;
        [self.view makeToast:[LanguageManager get:@"ios_label_maj_manuelle"] duration:2 position:@"bottom" title:nil image:[UIImage imageNamed:@"alarm_48"] style:nil completion:nil];
        [UtilsCore stopPullingtimer];
    }

}

- (IBAction)touchDownButtonAppLanguage:(id)sender {
    [RMPickerViewController setLocalizedTitleForCancelButton:[LanguageManager get:@"button_annuler"]];
    [RMPickerViewController setLocalizedTitleForSelectButton:[LanguageManager get:@"ios_button_selectionner"]];
    RMPickerViewController *pickerVC = [RMPickerViewController pickerController];
    pickerVC.delegate = self;
    pickerVC.titleLabel.text = [LanguageManager get:@"title_selectionner_langue"];
    
    if (IS_IPAD)
        [pickerVC showFromViewController:self];
    else
        [pickerVC show];
}

-(IBAction)updateFrequencyChanged:(id)sender
{
    int val = [[NSNumber numberWithFloat:self.updateFrequency.value] intValue];
    
    //Si on a une nouvelle valeur
    if (val != self.updateFrequencyValue) {
        switch (val) {
            case kMessageUpdateWhenLaunchApp:
                [self.view makeToast:[LanguageManager get:@"ios_label_maj_lancement_app"] duration:1 position:@"bottom" title:nil image:[UIImage imageNamed:@"alarm_48"] style:nil completion:nil];
                    [UtilsCore stopPullingtimer];
                break;
                
            case kMessageUpdateWhenEnterRes:
                [self.view makeToast:[LanguageManager get:@"ios_label_maj_consulte_ressource"] duration:1 position:@"bottom" title:nil image:[UIImage imageNamed:@"alarm_48"] style:nil completion:nil];
                    [UtilsCore stopPullingtimer];
                break;
                
            case kMessageUpdateEveryMinute:
                [self.view makeToast:[LanguageManager get:@"ios_label_maj_toutes_minutes"] duration:1 position:@"bottom" title:nil image:[UIImage imageNamed:@"alarm_48"] style:nil completion:nil];
                    [UtilsCore startPullingTimer];
                break;
            default:
                [self.view makeToast:[LanguageManager get:@"ios_label_bug_slider"]];
                break;
        }
    }
    
    self.updateFrequencyValue = val;
}

#pragma mark - RMPickerViewController Delegates
- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray *)selectedRows {
    self.indexLangueAppSelect = [[selectedRows objectAtIndex:0] intValue];
    
    [LanguageManager setCurrentLanguageAppWithCode:[LanguageManager codeLangueWithArrayIndex:self.indexLangueAppSelect]];
    
    //Enregistrement de la nouvelle langue de l'application
    NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
    NSDictionary * parametres = [preferences persistentDomainForName: kPreferences];
    NSMutableDictionary *parametresMutable = [parametres mutableCopy];
    [parametresMutable setObject:[NSNumber numberWithInt:[LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect]] forKey:@"idLangueApp"];
    [preferences setPersistentDomain:parametresMutable forName:kPreferences];
    
    // Modification des termes sur les autres écrans ouverts
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLangueApp" object:self];
}
- (void)pickerViewControllerDidCancel:(RMPickerViewController *)vc {}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.langueValuesString count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.langueValuesString objectAtIndex:row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
