//
//  SuccesControllerViewController.m
//  SupCast
//
//  Created by admin on 08/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "SuccesControllerViewController.h"
#import "LanguageManager.h"
#import "MKNetworkEngine.h"
#import <objc/runtime.h>

@interface SuccesControllerViewController ()

@property (nonatomic, strong) MKNetworkEngine * engine;

@end

@implementation SuccesControllerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];

    if (IS_IPAD)
    {
        [self setStackWidth: 320];
    }
    
    [self setContent];
    
    //Initialisation du date formatter
    _date_formatter = [[NSDateFormatter alloc] init];
    _date_formatter.timeStyle = NSDateFormatterNoStyle;
    _date_formatter.dateStyle = NSDateFormatterMediumStyle;
    [_date_formatter setDateFormat:@"dd/MM/yy"];
    
    _engine = [[MKNetworkEngine alloc] initWithHostName:sc_server_static];
    [_engine useCache];
}

- (void) setContent {
    if (_mode_affichage == ModeSucces)
        self.title = [LanguageManager get:@"ios_title_succes"];
    else if(_mode_affichage == ModeBadgeDisplay || _mode_affichage == ModeBadgeAttribute)
        self.title =[LanguageManager get:@"title_badges"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIImage*)lazyLoadImage:(NSString*)url_logo onObject:(id)object ofCollectionOrTableView:(id)collection_or_table_view atIndexPath:(NSIndexPath*)index_path{
    
    //Verification que l'objet n'a pas déjà d'image associée
    UIImage * image = objc_getAssociatedObject(object, kLazyLoadAssociatedObject);
    
    //S'il y en a une, on s'arrête là
    if (image) {
        return image;
    }
    
    [_engine imageAtURL:[NSURL URLWithString:url_logo]
      completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
          DLog(@"");
          //On cale l'image en runtime
          objc_setAssociatedObject(object, kLazyLoadAssociatedObject, fetchedImage, OBJC_ASSOCIATION_COPY);
          
          if ([collection_or_table_view isKindOfClass:[UICollectionView class]])
              [(UICollectionView*)collection_or_table_view reloadItemsAtIndexPaths:@[index_path]];
          else if ([collection_or_table_view isKindOfClass:[UITableView class]])
              [(UITableView*)collection_or_table_view reloadRowsAtIndexPaths:@[index_path] withRowAnimation:UITableViewRowAnimationNone];
          
      }
           errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
               DLog(@"");
           }];
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_array_data count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_mode_affichage == ModeSucces){
        return 80;
    }
    else{
        
        //En mode display, on agrandit pour voir le texte entier du badge
        NSDictionary * currentItem = [_array_data objectAtIndex:indexPath.row];

        int descriptionWidth = 223;
        
        //Constante de largeur de messages
        if (IS_IPHONE && UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])){
            descriptionWidth = 400;
        }
        
        CGSize maximumLabelSize = CGSizeMake(descriptionWidth, CGFLOAT_MAX);
        
        CGRect stringRect = [currentItem[@"description"] boundingRectWithSize:maximumLabelSize
                                                                      options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                                   attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:13]}
                                                                      context:nil];
            
        //64 = 100 - 36 -> Hauteur totale - hauteur par défaut de la description
        return MAX(100, stringRect.size.height + 64);
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = _mode_affichage == ModeSucces ? @"succes_cell" : @"badge_cell";
    NSDictionary * currentItem = [_array_data objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    
    if (_mode_affichage == ModeSucces) {
        if ([currentItem[@"succes_est_gagne"] isEqualToNumber:@1]){
            UIImage * image = [UIImage imageNamed:[currentItem objectForKey:@"nom_logo"]];
            
            if (image)
                [imageView setImage:image];
            else
                [imageView setImage:[UIImage imageNamed:@"succes"]];
        }
        else{
           [imageView setImage:[UIImage imageNamed:@"succes_default"]]; 
        }
    }
    else if (_mode_affichage == ModeBadgeDisplay || _mode_affichage == ModeBadgeAttribute){

        UIImage * image = [self lazyLoadImage:currentItem[@"url_logo"]
                                     onObject:currentItem
                                 ofCollectionOrTableView:tableView
                                  atIndexPath:indexPath];
        if (image)
            [imageView setImage:image];
        else
            [imageView setImage:[UIImage imageNamed:@"succes"]];
        
        UILabel *label_emetteur = (UILabel *)[cell viewWithTag:400];
        [label_emetteur setText:[[LanguageManager get:@"label_attribuer_par"] stringByAppendingString:[currentItem objectForKey:@"emetteur"]]];
        
        UILabel *label_date_gagne = (UILabel *)[cell viewWithTag:500];
        
        if (_mode_affichage == ModeBadgeDisplay) {
            NSDate * date_gagne = [NSDate dateWithTimeIntervalSince1970:[currentItem[@"date_gagne"] integerValue]];
            
            [label_date_gagne setHidden:false];
            [label_date_gagne setText:[_date_formatter stringFromDate:date_gagne]];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setTintColor:[UIColor orangeColor]];
        }
        else{
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [label_date_gagne setHidden:true];
        }
    }
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:200];
    [lblName setText:[currentItem objectForKey:@"nom"]];
    
    UILabel *lblDesc = (UILabel *)[cell viewWithTag:300];
    [lblDesc setText:[currentItem objectForKey:@"description"]];
    
    // Configure the cell...
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //Seulement en mode attribution, on fait quelque chose
    if (_mode_affichage == ModeBadgeAttribute) {
        NSDictionary * currentItem = [_array_data objectAtIndex:indexPath.row];
        [self.delegate ajouterBadge:currentItem[@"id_openbadge"] callback:^(NSDictionary * wsReturn, NSNumber * id_utilisateur) {
            DLog(@"");
            if ([wsReturn[@"status"] isEqualToString:@"ok"]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                if (wsReturn[@"message"]) {
                    [self.view makeToast:[LanguageManager get:wsReturn[@"message"]]];
                }
                else{
                    [self.view makeToast:[LanguageManager get:@"erreur_serveur"]];
                }
            }
        }];
    }
}

@end
