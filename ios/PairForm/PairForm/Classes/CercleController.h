//
//  GroupeController.h
//  profil
//
//  Created by admin on 03/05/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilsCore.h"
#import "CercleControllerDelegate.h"
#import "RechercheProfilController.h"
#import <objc/runtime.h>
#import "CercleCell.h"
#import "SIAlertView.h"
#import "LanguageManager.h"

typedef enum {
    ModeCercleNormal,
    ModeCercleAjoutDepuisProfil,
    ModeCercleSelectionVisibilite
} ModeCercle;


@interface CercleController : UIViewController <RechercheProfilControllerDelegate, UITableViewDataSource>

@property ModeCercle modeCercle;
@property (weak, nonatomic) id<CercleControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView * boutonsView;
@end


