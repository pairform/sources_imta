//
//  DashboardVC.h
//  PairForm
//
//  Created by Maen Juganaikloo on 07/05/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView * tableView;

@end
