//
//  FileManager.h
//  SupCast
//
//  Created by Maen Juganaikloo on 03/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
/** Dezip la ressource dans le dossier Library/Application Support/NOM_RESSOURCE.
 @param srcPath Chemin complet vers le zip de la ressource
 @return Booléen de réussite
 */

+(BOOL)unzipFileAtPath:(NSString *) srcPath;

/** Dezip la ressource dans le dossier Library/Application Support/NOM_RESSOURCE, puis envoie une notification de fin de zip si la ressource et le flag d'update sont renseignés.
 @param srcPath Chemin complet vers le zip de la ressource
 @param ressource Ressource à envoyer en notification (optionnel)
 @param isUpdate Flag pour indiquer si on est dans le cas d'une mise à jour (optionnel)
 @return Booléen de réussite
 */
+(BOOL)unzipFileAtPath:(NSString*)srcPath toPath:(NSString*)dstPath withRessource:(Ressource*)ressource andCapsule:(Capsule*)capsule isUpdate:(BOOL)isUpdate;
/** Déplace la ressource comme son nom l'indique :)
 */
+(BOOL)migrateFromDocumentsDirectoryPath:(NSString*) original_path toApplicationSupportDirectory:(NSString*)final_path;

/** Supprime le zip à l'emplacement donné.
 */
+(BOOL)removeCapsuleZipAtPath:(NSString*) zip_path;

/** Supprime physiquement la ressource et toutes ses capsules à partir d'un ID de ressource
 @param id_ressource Id de la ressource
 @return Booléen de réussite
 */
+(BOOL)removeRessourceWithId:(NSNumber*)id_ressource;

/** Supprime physiquement la capsule à partir d'un ID de ressource et de capsule
 @param id_ressource Id de la ressource
 @param id_capsule Id de la capsule
 @return Booléen de réussite
 */
+(BOOL)removeCapsuleWithId:(NSNumber*)id_capsule ofRessource:(NSNumber*)id_ressource;
@end
