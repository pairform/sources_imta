//
//  ModelController.h
//  Test
//
//  Created by Maen Juganaikloo on 22/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageVC.h"

@class WebViewVC;

@interface ModelController : NSObject <UIPageViewControllerDataSource>

- (WebViewVC *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(WebViewVC *)viewController;

@property (strong, nonatomic) NSArray *pageData;
@property ( weak , nonatomic ) PageVC * pageVCDelegate;

@end
