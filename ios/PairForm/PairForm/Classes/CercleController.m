//
//  GroupeController.m
//  profil
//
//  Created by admin on 03/05/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "CercleController.h"
#define kAlertViewOne 1
#define kAlertViewTwo 2


@interface CercleController ()
@property (weak, nonatomic) IBOutlet UITableView *cercles;
@property (strong, nonatomic) NSMutableArray *cerclesListe;
@property (strong, nonatomic) NSMutableArray *classesListe;
@property (strong, nonatomic) NSMutableArray *classesRejointesListe;
@property (strong, nonatomic) NSNumber * idCercleSelection;
@property (strong, nonatomic) NSIndexPath * indexPathCercleSelection;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationItem;
@property (strong, nonatomic) UIButton *bEdit;
@property ( nonatomic) BOOL boolEdit;
@property (strong , nonatomic) UITextField* alertViewText;
@property (weak, nonatomic) IBOutlet UIButton *bRejoindre;
@property (weak, nonatomic) IBOutlet UIButton *bCreer;
@end



@implementation CercleController

@synthesize boolEdit;
@synthesize bEdit;
@synthesize cerclesListe;
@synthesize classesListe, classesRejointesListe;
@synthesize cercles;
@synthesize alertViewText;
@synthesize bRejoindre;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self connect];
    if (self.modeCercle == modeNormal) {
        [self setBoolEdit:YES];
    }
    else{
        [self setBoolEdit:NO];
    }
    if ( self.modeCercle == ModeCercleAjoutDepuisProfil){
        [bRejoindre setEnabled:NO];
    }
    if (self.modeCercle == ModeCercleSelectionVisibilite){
        [self.boutonsView setHidden:YES];
//        [self.boutonsView setHeight:0];
    }
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword"]]];
    
    self.title = [LanguageManager get:@"title_cercles"];

    [self setContent];
    
    if (IS_IPAD)
    {
        [self setStackWidth: 320];
    }
}

-(void) setContent {
    [self.bCreer setTitle:[NSString stringWithFormat:@"%@...", [LanguageManager get:@"button_creer_un_cercle"]] forState:UIControlStateNormal];
    [self.bRejoindre setTitle:[LanguageManager get:@"button_rejoindre_classe"] forState:UIControlStateNormal];
}

-(void)ouvrirModeEdition{
    [bEdit removeTarget:self action:@selector(ouvrirModeEdition) forControlEvents:UIControlEventTouchUpInside];
    [bEdit addTarget:self action:@selector(fermerModeEdition) forControlEvents:UIControlEventTouchUpInside];
    [bEdit setTitle:[LanguageManager get:@"button_supprimer"] forState:UIControlStateNormal];
    boolEdit = YES;
    [cercles reloadData];
    //[cercles setEditing:YES animated:YES];
}

-(void)fermerModeEdition{
    [bEdit removeTarget:self action:@selector(fermerModeEdition) forControlEvents:UIControlEventTouchUpInside];
    [bEdit addTarget:self action:@selector(ouvrirModeEdition) forControlEvents:UIControlEventTouchUpInside];
    boolEdit = NO;
    [cercles reloadData];
    //[cercles setEditing:NO animated:YES];
}

-(void)connect{
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: nil];
    [WService get:@"reseau/liste"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          cerclesListe = [[wsReturn objectForKey:@"cercles"] mutableCopy];
          classesListe = [[wsReturn objectForKey:@"classes"] mutableCopy];
          classesRejointesListe = [[wsReturn objectForKey:@"classesRejointes"] mutableCopy];
          [cercles reloadData];
      }];

}


-(void)connect: (void (^)(void)) callbackBis{
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: nil];
    [WService get:@"reseau/liste"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          cerclesListe = [[wsReturn objectForKey:@"cercles"] mutableCopy];
          classesListe = [[wsReturn objectForKey:@"classes"] mutableCopy];
          classesRejointesListe = [[wsReturn objectForKey:@"classesRejointes"] mutableCopy];
          callbackBis();
          //[cercles reloadData];
          
      }];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( (NSNull *)cerclesListe == [NSNull null] ) return 0;
    
    switch (section) {
        case 0:
            return cerclesListe.count;
            break;
            
        case 1:
            return classesListe.count;
            break;
            
        case 2:
            return classesRejointesListe.count;
            break;
            
        default:
            return 0;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
//    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 30)];
//    [label setFont:[UIFont fontWithName:@"Helvetica" size:18]];
//    [label setTextColor:[UIColor darkGrayColor]];
//    [label setBackgroundColor:[UIColor clearColor]];
//    
    NSString *string;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"header"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"header"];
    }
    
    UIButton * cell_add = (UIButton*)[cell viewWithTag:6];
    if (self.modeCercle == modeNormal) {
        [cell_add setHidden:NO];
        [cell_add setEnabled:YES];
    }
    else{
        [cell_add setHidden:YES];
    }
    

    switch (section) {
        case 0:
            string = [LanguageManager get:@"title_mes_cercles"];
            [cell_add addTarget:self action:@selector(bCreerCercle:) forControlEvents:UIControlEventTouchUpInside];
            break;
            
        case 1:
            string =  [LanguageManager get:@"title_classes"];
            [cell_add addTarget:self action:@selector(bCreerCercle:) forControlEvents:UIControlEventTouchUpInside];
            if (![[UIViewController getSessionValueForKey:@"estExpert"] isEqual: @1])
                [cell_add setEnabled:NO];
            break;
            
        case 2:
            string =  [LanguageManager get:@"title_classes_rejointes"];
            [cell_add addTarget:self action:@selector(bRejoindreClasse:) forControlEvents:UIControlEventTouchUpInside];
            break;
            
        default:
            string =  @"...";
            break;
    }
    UILabel * cell_label = (UILabel*)[cell viewWithTag:1];
    [cell_label setText:string];
//    [view setBackgroundColor:[UIColor clearColor]];
//    [view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"crossword"]]];
    
    
    return cell;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary * groupe ;
    static NSString *CellIdentifier;
    
    switch (indexPath.section) {
        case 0:
            groupe = [cerclesListe objectAtIndex:indexPath.row];
            CellIdentifier = @"cercle";
            break;
            
        case 1:
            groupe = [classesListe objectAtIndex:indexPath.row];
            CellIdentifier = @"classe";
            break;
            
        case 2:
            groupe = [classesRejointesListe objectAtIndex:indexPath.row];
            CellIdentifier = @"classe";
            break;
            
        default:
            break;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //Si on est dans les classes rejointes
    if(indexPath.section == 2){
        //On cache le bouton d'ajout
        [[cell viewWithTag:4] setHidden:true];
    }
    
    CercleCell *collection = (CercleCell *)[cell viewWithTag:2];
    collection.delegate = collection;
    collection.dataSource = collection;
    
    //On transmet le flag d'édition si on est pas dans le cas d'une classe rejointe
    if (indexPath.section != 2)
        collection.boolEdit = boolEdit;
    //Sinon, on met false d'office.
    else
        collection.boolEdit = false;
    
    collection.members = [groupe objectForKey:@"profils"];
    collection.id_cercle = [groupe objectForKey:@"id_collection"];
    collection.index = indexPath;
    [collection reloadData];
    
    UILabel *lNom = (UILabel *)[cell viewWithTag:1];
    [lNom setText:[groupe objectForKey:@"nom"]];
    UIButton * bAdd = (UIButton *)[cell viewWithTag:4];
    UIButton * bSelectionner = (UIButton *)[cell viewWithTag:5];
    UIButton *bSupprime = (UIButton *)[cell viewWithTag:6];
    
    
    switch (self.modeCercle) {
        case ModeCercleAjoutDepuisProfil:{
            [bAdd setHidden:YES];
            indexPath.section != 2 ? [bSelectionner setHidden:NO] : [bSelectionner setHidden:YES];
            [bSupprime setHidden:YES];
            break;
        }
        case ModeCercleSelectionVisibilite:{
            [bAdd setHidden:YES];
            [bSelectionner setHidden:NO];
            [bSupprime setHidden:YES];
            break;
        }
        case ModeCercleNormal:{
            //Si la ligne concernée n'est pas la dernière, cad la ligne des classes rejointes
            if (boolEdit && (indexPath.section != 2)){
                [bSupprime setHidden:NO];
                [bAdd setHidden:NO];
            }
            else{
                [bSupprime setHidden:YES];
                [bAdd setHidden:YES];
                //            [bSupprime setHidden:YES];
            }
            
            [bSelectionner setHidden:YES];
            break;
        }
        default:{
            
            [bAdd setHidden:YES];
            [bSelectionner setHidden:YES];
            [bSupprime setHidden:YES];
            break;
        }
    }
    UILabel *lContentCle = (UILabel *)[cell viewWithTag:72];
    [lContentCle setText:[NSString stringWithFormat:@"%@ :",[LanguageManager get:@"ios_label_cle"]]];
    
    if ((indexPath.section == 1) || (indexPath.section == 2)){
        UILabel *lCle = (UILabel *)[cell viewWithTag:3];
        [lCle setText:[groupe objectForKey:@"cle"]];
    }
    
    
    return cell;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
- (void)creerCercle:(NSString *)nom {
    
    // GererClasse
   
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:nom,@"nom", nil];
    [WService put:@"reseau/cercle"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if (![WService displayErrors:wsReturn] )
          {
              [self connect:^(void){
                  [cercles beginUpdates];
                  NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                  for ( NSDictionary * cercle in cerclesListe){
                      if ( [[cercle objectForKey:@"nom"] isEqualToString:nom] ){
                          NSUInteger index = [cerclesListe indexOfObject:cercle];
                          newIndexPath = [NSIndexPath indexPathForRow:(index) inSection:0];
                          break;
                      }
                  }
                  [cercles insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
                  [cercles endUpdates];
//                  [cercles scrollToRowAtIndexPath:newIndexPath
//                                 atScrollPosition:UITableViewScrollPositionTop  animated:YES];
              }];
              
          }
      }];
}

- (void)creerClasse:(NSString *)nom {
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:nom,@"nom", nil];
    [WService put:@"reseau/classe"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ( ![WService displayErrors:wsReturn] )
          {
              [self connect:^(void){
                  [cercles beginUpdates];
                  NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                  for ( NSDictionary * classe in classesListe){
                      if ( [[classe objectForKey:@"nom"] isEqualToString:nom] ){
                          NSUInteger index = [classesListe indexOfObject:classe];
                          newIndexPath = [NSIndexPath indexPathForRow:(index) inSection:1];
                          break;
                      }
                  }
                  [cercles insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
                  [cercles endUpdates];
                  [cercles scrollToRowAtIndexPath:newIndexPath
                                 atScrollPosition:UITableViewScrollPositionTop  animated:YES];
                  
                  SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_classe_creee"]
                                                                   andMessage:[NSString stringWithFormat:@"%@ : %@",[LanguageManager get:@"label_clef_classe"],[wsReturn objectForKey:@"cle"]]];
                  [alertView addButtonWithTitle:@"Ok"
                                           type:SIAlertViewButtonTypeDefault
                                        handler:^(SIAlertView *alertView) {
                                        }];
                  alertView.transitionStyle = SIAlertViewTransitionStyleFade;
                  [alertView show];
              }];
              
          }
      }];
}

- (IBAction)supprimerCercle:(id)sender {
    NSIndexPath * indexPath = [cercles indexPathForCell:[self getCellOfSender:sender]];

    NSNumber * id_collection;
    if ( indexPath.section == 0){
        id_collection = [[cerclesListe objectAtIndex:indexPath.row] objectForKey:@"id_collection"];
//        [cerclesListe removeObjectAtIndex:indexPath.row];
    }else{
        id_collection = [[classesListe objectAtIndex:indexPath.row] objectForKey:@"id_collection"];
    }
    
    
    //Webservice de suppression
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:id_collection,@"id_collection", nil];
    [WService delete:@"reseau"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          
          if ( ![WService displayErrors:wsReturn] )
          {
              //Suppression graphique
              [self connect:^(void){
                  [cercles beginUpdates];
                  //NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
                  [cercles deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
                  [cercles endUpdates];
                  [cercles reloadData];
              }];
          }
      }];
    

}



- (IBAction)ajouterMembre:(id)sender {
    //Enregistrement du cercle selectionne , pour ajouter le membre dans ce dernier.
    NSIndexPath * indexPath = [cercles indexPathForCell:[self getCellOfSender:sender]];
    if ( indexPath.section == 0){
        _idCercleSelection = [[cerclesListe objectAtIndex:indexPath.row] objectForKey:@"id_collection"];
    }else{
        _idCercleSelection = [[classesListe objectAtIndex:indexPath.row] objectForKey:@"id_collection"];
    }
    _indexPathCercleSelection = [indexPath copy];
    //le button dirige deja sur la vue RechercheProfil.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"segueAjoutCercle"])
    {
        RechercheProfilController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        viewC.mode = modeAjoutCercle ;
    }
}

- (void)ajouteMembreDansCercleDepuisRechercheProfil:(NSDictionary *)utilisateur
{
    
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:
                            utilisateur[@"id_utilisateur"],@"id_utilisateur_concerne",
                            utilisateur[@"username"],@"pseudo_utilisateur",
                            _idCercleSelection,@"id_collection", nil];
    [WService put:@"reseau/utilisateur"
         param: query
          callback:^(NSDictionary * wsReturn) {
              if ([wsReturn[@"status"] isEqualToString:@"ok"]) {
                  //Si retour positif, on update la vue
                  
                  //On ne met que l'id d'utilisateur, puisque de toute façon, CercleCell n'a besoin que de ça pour retrouver l'image et le nom de l'utilisateur
                  //après son enregistrement avec l'id (voir delegate dans CercleCell)
//                  [self connect:^(void){
//                      //On récupère la collection concernée
//                      CercleCell * collection = (CercleCell *)[[cercles cellForRowAtIndexPath:_indexPathCercleSelection] viewWithTag:2];
//                      
//                      NSDictionary * groupe = [[NSDictionary alloc]init];
//                      if ( _indexPathCercleSelection.section == 0){
//                          groupe = [cerclesListe objectAtIndex:_indexPathCercleSelection.row];
//                      }else{
//                          groupe = [classesListe objectAtIndex:_indexPathCercleSelection.row];
//                          
//                      }
//                      
//                      collection.members = [groupe objectForKey:@"profils"];
//                      NSIndexPath * indexPath = [NSIndexPath indexPathForItem:[collection.members count] -1 inSection:0];
//                      [collection insertItemsAtIndexPaths:@[indexPath]];
//                      [collection scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
//                      
//                  }];
                  //On ne met que l'id d'utilisateur, puisque de toute façon, CercleCell n'a besoin que de ça pour retrouver l'image et le nom de l'utilisateur
                  //après son enregistrement avec l'id (voir delegate dans CercleCell)
                  NSDictionary * nouvel_utilisateur = [[NSDictionary alloc] initWithObjectsAndKeys:utilisateur[@"id_utilisateur"], @"id_utilisateur", nil];
                  NSMutableArray * concerned_list;
                  switch (_indexPathCercleSelection.section) {
                      case 0:
                          concerned_list = cerclesListe;
                          break;
                          
                      case 1:
                          concerned_list = classesListe;
                          break;
                          
                      case 2:
                          concerned_list = classesRejointesListe;
                          break;
                          
                      default:
                          //Oh noooo!
                          return;
                          break;
                  }
                  
                  //On réinjecte le tableau de membre contenant notre nouveau gus
                  NSMutableArray * members_copy = [(NSArray*)[[concerned_list objectAtIndex:_indexPathCercleSelection.row] objectForKey:@"profils"] mutableCopy];
                  [members_copy addObject:nouvel_utilisateur];
                  NSMutableDictionary * cercle_temp = [[NSMutableDictionary alloc] initWithDictionary:[concerned_list objectAtIndex:_indexPathCercleSelection.row]];
                  [cercle_temp setObject:members_copy forKey:@"profils"];
                  
                  [concerned_list replaceObjectAtIndex:_indexPathCercleSelection.row
                                            withObject:cercle_temp];
                  
                  //On récupère la collection concernée
                  CercleCell * collection = (CercleCell *)[[cercles cellForRowAtIndexPath:_indexPathCercleSelection] viewWithTag:2];
                  
                  //On copie le nouvel utilisateur dans le data source de cette dernière
                  collection.members = members_copy;
//                  [collection reloadData];
                  //Et on fait une insertion graphique au dernier index
                  NSIndexPath * indexPath = [NSIndexPath indexPathForItem:[collection.members count] -1 inSection:0];
                  [collection insertItemsAtIndexPaths:@[indexPath]];
                  [collection scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
              }
              else{
                  [self.view makeToast:wsReturn[@"message"] duration:2.0 position:@"bottom" title:@"Impossible d'ajouter l'utilisateur" image:[UIImage imageNamed:@"cancel_48"] style:nil completion:nil];
              }
          }];
   
    
}

- (IBAction)supprimerMembre:(id)sender {

    //Webservice de suppression
    
    CercleMembreCell * collectionCell = [self getSuperviewOf:sender ofKind:[CercleMembreCell class]];
    NSNumber * id_collection = collectionCell.id_collection;
    NSNumber * id_utilisateur = collectionCell.id_utilisateur;
    
    
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: id_utilisateur, @"id_utilisateur_concerne", id_collection, @"id_collection", nil];
    [WService delete:@"reseau/utilisateur"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ( [[wsReturn objectForKey:@"status"] isEqualToString:@"ok"] )
          {
              //Suppression graphique
              CercleCell * collection = [self getSuperviewOf:collectionCell ofKind:[CercleCell class]];
              NSIndexPath * indexCollection = collection.index;
              NSIndexPath * indexCell = [collection indexPathForCell:collectionCell];
              [self connect:^(void){
                  NSDictionary * groupe;
                  if ( indexCollection.section == 0){
                      groupe = [cerclesListe objectAtIndex:collection.index.row];
                  }else{
                      groupe = [classesListe objectAtIndex:collection.index.row];
                
                  }
                  
                  collection.members = [groupe objectForKey:@"profils"];
                  [collection deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexCell]];
              }];
              
          }
          else
              [self.view makeToast:wsReturn[@"message"] duration:4.0 position:@"bottom" title:nil image:[UIImage imageNamed:@"warning_64"] style:nil completion:nil];
      }];
    
    

    
    
}

- (IBAction)bCreerCercle:(id)sender {
    
    UITextField * nomField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    nomField.placeholder = [LanguageManager get:@"label_nom_cercle"];
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_creer_cercle"] andTextFields:@[nomField]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:nil];
    [alertView addButtonWithTitle:[LanguageManager get:@"ios_title_creer_cercle"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              [self creerCercle:nomField.text];
                          }];
    if ([[UIViewController getSessionValueForKey:@"estExpert"] isEqual: @1]){
        [alertView addButtonWithTitle:[LanguageManager get:@"ios_title_creer_classe"]
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  [self creerClasse:nomField.text];
                              }];
        
        
    }

    

    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];
}

- (IBAction)bRejoindreClasse:(id)sender {

    UITextField * cleField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    cleField.placeholder = [LanguageManager get:@"label_clef_classe"];

    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"ios_title_entrez_la_cle_classe"] andTextFields:@[cleField]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_annuler"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_rejoindre"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              //Appel du webservice
                              
                              NSString * cle = cleField.text;
                              NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:cle,@"cle", nil];
                              [WService put:@"reseau/classe/utilisateur"
                                   param: query
                                callback: ^(NSDictionary *wsReturn) {
                                    if (![WService displayErrors:wsReturn]) {
                                        [self connect:^(void){
                                            [cercles beginUpdates];
                                            NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
                                            for ( NSDictionary * classe in classesRejointesListe){
                                                if ( [[classe objectForKey:@"cle"] isEqualToString:cle] ){
                                                    NSUInteger index = [classesRejointesListe indexOfObject:classe];
                                                    newIndexPath = [NSIndexPath indexPathForRow:(index) inSection:2];
                                                    break;
                                                }
                                            }
                                            [cercles insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
                                            [cercles endUpdates];
                                            [cercles scrollToRowAtIndexPath:newIndexPath
                                                           atScrollPosition:UITableViewScrollPositionTop  animated:YES];
                                            
                                        }];

                                    }
                                    
                                }];
                          }];

    
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];
}


- (IBAction)selectionnerPourAjouterMembreDepuisSonProfil:(id)sender {
   
        NSIndexPath * indexPath = [cercles indexPathForCell:[self getCellOfSender:sender]];
        NSNumber * idCercle;
        NSString * nomCercle;
        switch (indexPath.section) {
            case 0:
                idCercle = [[cerclesListe objectAtIndex:indexPath.row] objectForKey:@"id_collection"];
                nomCercle = [[cerclesListe objectAtIndex:indexPath.row] objectForKey:@"nom"];
                break;
                
            case 1:
                idCercle = [[classesListe objectAtIndex:indexPath.row] objectForKey:@"id_collection"];
                nomCercle = [[classesListe objectAtIndex:indexPath.row] objectForKey:@"nom"];
                break;
                
            case 2:
                idCercle = [[classesRejointesListe objectAtIndex:indexPath.row] objectForKey:@"id_collection"];
                nomCercle = [[classesRejointesListe objectAtIndex:indexPath.row] objectForKey:@"nom"];
                break;
                
            default:
                break;
        }
    if (self.modeCercle == ModeCercleAjoutDepuisProfil){
        [_delegate actionAPartirDeCercle:idCercle nomCercle:nomCercle callback:^(NSDictionary * wsReturn, NSNumber * id_utilisateur) {
            if (![WService displayErrors:wsReturn]) {
                //Si retour positif, on update la vue
                
                //On ne met que l'id d'utilisateur, puisque de toute façon, CercleCell n'a besoin que de ça pour retrouver l'image et le nom de l'utilisateur
                //après son enregistrement avec l'id (voir delegate dans CercleCell)
                NSDictionary * nouvel_utilisateur = [[NSDictionary alloc] initWithObjectsAndKeys:id_utilisateur, @"id_utilisateur", nil];
                NSArray * concerned_list;
                switch (indexPath.section) {
                    case 0:
                        concerned_list = cerclesListe;
                        break;
                        
                    case 1:
                        concerned_list = classesListe;
                        break;
                        
                    case 2:
                        concerned_list = classesRejointesListe;
                        break;
                        
                    default:
                        //Oh noooo!
                        return;
                        break;
                }
                
                //On réinjecte le tableau de membre contenant notre nouveau gus
                NSMutableArray * members_copy = [(NSArray*)[[concerned_list objectAtIndex:indexPath.row] objectForKey:@"profils"] mutableCopy];
                [members_copy addObject:nouvel_utilisateur];
                
                //On récupère la collection concernée
                CercleCell * collection = (CercleCell *)[[cercles cellForRowAtIndexPath:indexPath] viewWithTag:2];
                
                //On copie le nouvel utilisateur dans le data source de cette dernière
                collection.members = members_copy;
                //Et on fait une insertion graphique au dernier index
                NSIndexPath * indexPath = [NSIndexPath indexPathForItem:[collection.members count] -1 inSection:0];
                [collection insertItemsAtIndexPaths:@[indexPath]];
                [collection scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
                
                //On ne met que l'id d'utilisateur, puisque de toute façon, CercleCell n'a besoin que de ça pour retrouver l'image et le nom de l'utilisateur
                //après son enregistrement avec l'id (voir delegate dans CercleCell)
                //                [self connect:^(void){
                //                    //On récupère la collection concernée
                //                    CercleCell * collection = (CercleCell *)[[cercles cellForRowAtIndexPath:_indexPathCercleSelection] viewWithTag:2];
                //
                //                    NSDictionary * groupe = [[NSDictionary alloc]init];
                //                    if ( _indexPathCercleSelection.section == 0){
                //                        groupe = [cerclesListe objectAtIndex:_indexPathCercleSelection.row];
                //                    }else{
                //                        groupe = [classesListe objectAtIndex:_indexPathCercleSelection.row];
                //
                //                    }
                //
                //                    collection.members = [groupe objectForKey:@"profils"];
                //                }];
            }
        }];

    }
    else if (self.modeCercle == ModeCercleSelectionVisibilite){
        [_delegate actionAPartirDeCercle:idCercle nomCercle:nomCercle];
        [self.navigationController popViewControllerAnimated:YES];
    }

    
    
}


#pragma mark Helpers


-(id)getSuperviewOf:(id)sender ofKind:(Class)class{
    if ([[sender superview] isKindOfClass:class]) {
        return (id)[sender superview];
    }
    else{
        //Si on est pas arrivé tout en haut de la hiérarchie de vue
        if ([sender superview])
            return [self getSuperviewOf:[sender superview] ofKind:class];
        //Sinon, on renvoi nil
        else
            return nil;
    }
}

-(UITableViewCell*)getCellOfSender:(id)sender{
    return [self getSuperviewOf:sender ofKind:[UITableViewCell class]];
}

@end
