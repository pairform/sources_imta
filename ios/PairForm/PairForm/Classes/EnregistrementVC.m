//
//  EnregistrementVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "EnregistrementVC.h"
#import "SIAlertView.h"
#import "RootVC.h"
#import "LanguageManager.h"

#define INTERESTING_TAG_NAMES @"nom", @"message", nil

@interface EnregistrementVC ()
{
    NSMutableArray * textFieldsArray;
}
@end
@implementation EnregistrementVC

@synthesize nomTextField;
@synthesize prenomTextField;
@synthesize eMailTextField;
@synthesize pseudoTextField;
@synthesize motDePasseTextField;
@synthesize nomUtilisateur;
@synthesize prenomUtilisateur;
@synthesize eMailUtilisateur;
@synthesize pseudoUtilisateur;
@synthesize motDePasseUtilisateur;
@synthesize messageString;

#pragma mark - Init

-(void)viewDidLoad {
    DLog(@"");
	[super viewDidLoad];
//    [TestFlight passCheckpoint:@"Enregistrement d'un nouvel utilisateur"];

    if (IS_IPHONE) {
        [self setTitle:@"Nouveau Compte"];
    }
    
    
    // Do any additional setup after loading the view.
    
    //Ecran de validation de création de compte à cacher dès le départ
    [[self.view viewWithTag:1000] setHidden:YES];
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [self.pageController.view setFrame:self.view.bounds];
    
    NSArray * pages = [[[self view] subviews] copy];
    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:[pages count]];
    textFieldsArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < ([pages count] -1); i++) {
        [tempArray addObject:[self viewControllerFromUIView:pages[i]]];
        [[[[self view] subviews] objectAtIndex:0] removeFromSuperview];
        
        [textFieldsArray addObject:[[pages[i] subviews] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            return [evaluatedObject isKindOfClass:NSClassFromString(@"UITextField")];
            }]]
         ];

    }
    
    _dataSource = [tempArray copy];
    
    [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:0]]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:nil];
    
    [[UIPageControl appearanceWhenContainedIn:self.class, nil] setPageIndicatorTintColor:[UIColor lightGrayColor]];
    [[UIPageControl appearanceWhenContainedIn:self.class, nil] setCurrentPageIndicatorTintColor:[UIColor grayColor]];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    self.pageController.doubleSided = NO;
    self.pageController.delegate = self;

    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];
    
    // Initialisation de la liste des langues des messages
    self.tableViewOtherLanguages = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 264, 134)];
    self.tableViewOtherLanguages.delegate = self;
    self.tableViewOtherLanguages.dataSource = self;
    
    // Modification du text du bouton de la langue avec la langue de l'appareil
    NSString * langueApp = [LanguageManager nameTrueLangueWithCode:[LanguageManager getCurrentCodeLanguageApp]];
    [self.langueButton setTitle:langueApp forState:UIControlStateNormal];
    self.indexLangueAppSelect = [LanguageManager arrayIndexWithCode:[LanguageManager getCurrentCodeLanguageApp]];
    
    // Recuperation des langues dans le fichier PairForm-Arrays.plist
    self.langueValuesString = [NSMutableArray arrayWithArray:[LanguageManager getArrayTrueNameLanguage]];
    self.langueValuesId = [NSMutableArray arrayWithArray:[LanguageManager getArrayIdLanguage]];
    [self.langueValuesId removeObjectAtIndex:[LanguageManager arrayIndexWithCode:[LanguageManager getCodeMainLanguage]]];
    
    // Initialisation du tableau du choix des autres langues
    self.choixOtherLanguages = [[NSMutableArray alloc] init];
    int i = 0;
    for ( id idLangue in self.langueValuesId ) {
        [self.choixOtherLanguages insertObject:[NSNumber numberWithBool:false] atIndex:i];
        i++;
    }
    
    [self setDesign];
    [self setContent];
    
    if (IS_IPAD)
    {
        [self setStackWidth: 320];
    }
    
    NSURL * mediaUrl = [NSURL URLWithString:[kURLCGU_rs stringByAppendingString:[LanguageManager getCodeMainLanguage]]];
    NSURLRequest * mediaUrlRequest = [[NSURLRequest alloc] initWithURL:mediaUrl];
    
    [NSURLCache setSharedURLCache:[NSURLCache sharedURLCache]];
    
    [self.webviewCGU loadRequest:mediaUrlRequest];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    UIButton * boutonValider = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [boutonValider addTarget:self action:@selector(enregistrer) forControlEvents:UIControlEventTouchUpInside];
    
    [boutonValider setShowsTouchWhenHighlighted:true];
    
    if (IS_IPHONE) {
        [boutonValider setFrame:CGRectMake(5,6,32,32)];
        [boutonValider setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [boutonValider setBackgroundImage:[UIImage imageNamed:@"paper_plane_64"] forState:UIControlStateNormal];
        [boutonValider setReversesTitleShadowWhenHighlighted:true];
        UIBarButtonItem *itemValider = [[UIBarButtonItem alloc] initWithCustomView:boutonValider];
        
        [UIView animateWithDuration:0.25
                              delay:0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             self.navigationItem.rightBarButtonItem.customView.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             [self.navigationItem setRightBarButtonItem:itemValider];
                             itemValider.customView.alpha = 0;
                             
                             [UIView animateWithDuration:0.5
                                                   delay:0
                                                 options: UIViewAnimationCurveEaseOut
                                              animations:^{
                                                  itemValider.customView.alpha = 1;
                                              }
                                              completion:nil];
                             
                         }];
    }
    else {
        [boutonValider setFrame:CGRectMake(220,667,92,32)];
        [boutonValider setTitle:[LanguageManager get:@"button_enregistrer"] forState:UIControlStateNormal];
        [boutonValider setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [boutonValider setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
//        [boutonValider setImage:[UIImage imageNamed:@"paper_plane_64_colored"] forState:UIControlStateNormal];
//        [boutonValider setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
//        [boutonValider setBackgroundImage:[UIImage imageNamed:@"MenuCell_cut"] forState:UIControlStateNormal];
        [self.view addSubview:boutonValider];
        [UIView animateWithDuration:0.5
                              delay:0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             boutonValider.alpha = 1;
                         }
                         completion:nil];
    }
}

-(void) setContent {
    [self setTitle:[LanguageManager get:@"title_nouveau_compte"]];
    self.labelChoisirPseudoMail.text = [LanguageManager get:@"ios_label_choisir_pseudo_mail"];
    self.pseudoTextField.placeholder = [NSString stringWithFormat:@"%@* (4+)", [LanguageManager get:@"label_pseudo"]];
    self.eMailTextField.placeholder = [LanguageManager get:@"label_email"];
    self.labelChoisirMdp.text = [LanguageManager get:@"ios_label_choisir_mdp"];
    self.motDePasseTextField.placeholder = [NSString stringWithFormat:@"%@* (6+)", [LanguageManager get:@"label_mot_de_passe"]];
    self.motDePasseTextField2.placeholder = [LanguageManager get:@"label_mot_de_passe_confirmation"];
    self.labelInfosComplement.text = [LanguageManager get:@"ios_label_infos_complement"];
    self.nomTextField.placeholder = [NSString stringWithFormat:@"%@ *",[LanguageManager get:@"label_nom"]];
    self.prenomTextField.placeholder = [NSString stringWithFormat:@"%@ *",[LanguageManager get:@"label_prenom"]];
    self.etablissementTextField.placeholder = [LanguageManager get:@"label_etablissement"];
    self.labelLangue.text = [NSString stringWithFormat:@"%@ :", [LanguageManager get:@"label_langue_principale_2"]];
    self.labelCGU.text = [LanguageManager get:@"ios_label_conditions_utilisation"];
    self.labelFinInscription.text = [LanguageManager get:@"ios_label_inscription_terminee"];
    self.textFinInscription.text = [LanguageManager get:@"label_creation_de_compte_reussi"];
    [self.autresLanguesButton setTitle:[NSString stringWithFormat:@"%@...",[LanguageManager get:@"button_autres_langues"]] forState:UIControlStateNormal];
}

- (void) setDesign {
    self.langueButton.layer.cornerRadius = self.autresLanguesButton.layer.cornerRadius = 3;
    self.langueButton.layer.borderWidth = self.autresLanguesButton.layer.borderWidth = 1;
    self.langueButton.layer.borderColor = self.autresLanguesButton.layer.borderColor = [UIColor blueColor].CGColor;
    [self.langueButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.autresLanguesButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

#pragma mark -
#pragma mark PageView Delegates

-(UIViewController*)viewControllerFromUIView:(UIView*)view{
    
    UIViewController * newVC = [[UIViewController alloc]  init];
    CGRect frame = view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    
    view.frame = frame;
    [newVC setView:view];
    
    return newVC;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    // Return the data view controller for the given index.
    if (([_dataSource count] == 0) || (index >= [_dataSource count])) {
        return nil;
    }
    return [_dataSource objectAtIndex:index];
    
}

- (NSUInteger)indexOfViewController:(UIViewController *)viewController
{
    // Return the index of the given data view controller.
    // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [_dataSource indexOfObject:viewController];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self indexOfViewController:(UIViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index == [_dataSource count]){
        return nil;
    }
    return [self viewControllerAtIndex:index];
}
//
//-(void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
//{
//    if ([self indexOfViewController:pendingViewControllers[0]] == ([_dataSource count] -1)) {
//        
//        [self enregistrer];
//    }
//}
//
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
//    if ([self indexOfViewController:previousViewControllers[0]] == ([_dataSource count] -2)) {
//        UIButton * boutonValider = [UIButton buttonWithType:UIButtonTypeCustom];
//        
//        [boutonValider setFrame:CGRectMake(5,6,32,32)];
//        [boutonValider setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [boutonValider addTarget:self action:@selector(enregistrer) forControlEvents:UIControlEventTouchUpInside];
//        [boutonValider setBackgroundImage:[UIImage imageNamed:@"paper_plane_64"] forState:UIControlStateNormal];
//        [boutonValider setShowsTouchWhenHighlighted:true];
//        [boutonValider setReversesTitleShadowWhenHighlighted:true];
//        
//        UIBarButtonItem *itemValider = [[UIBarButtonItem alloc] initWithCustomView:boutonValider];
//        [self.navigationItem setRightBarButtonItem:itemValider];
//
//        
//    }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return [_dataSource count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]];
}

#pragma mark - Textfields Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    //Récupération des indexs
    int indexEcran = [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]];
    int indexTextfield = [textFieldsArray[indexEcran] indexOfObject:textField];
    
    //On check si le textfield est le dernier de son écran
    if (indexTextfield != ([textFieldsArray[indexEcran] count] -1)) {
        //Si non,
        UITextField * nextField = textFieldsArray[indexEcran][indexTextfield +1];
        [nextField becomeFirstResponder];
    }
    //Si oui, on switch à l'écran suivant
    else {
        //Incrémentation de l'index
        indexEcran++;
        
        
        __weak NSArray * tfArray = textFieldsArray;
        
        //Présentation de l'écran suivant
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:indexEcran]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
                
            //S'il y a un textfield sur l'écran
            if([tfArray[indexEcran] count])
            {
                //On lui met le focus
                UITextField * nextField = tfArray[indexEcran][0];
                [nextField becomeFirstResponder];
            }
            //Sinon, rien.
        }];
        
    }
    
    return NO;
}

#pragma mark - Enregistrement

-(void)enregistrer {
    DLog(@"");
	
    [self.view endEditing:YES];
    
	//On vérifie que tous les champs du formulaire sont remplis
	self.nomUtilisateur = nomTextField.text;
	self.prenomUtilisateur = prenomTextField.text;
	self.eMailUtilisateur = eMailTextField.text;
	self.pseudoUtilisateur = pseudoTextField.text;
	self.motDePasseUtilisateur = motDePasseTextField.text;
	
    [nomTextField resignFirstResponder];
    [prenomTextField resignFirstResponder];
    [eMailTextField resignFirstResponder];
    [pseudoTextField resignFirstResponder];
    [motDePasseTextField resignFirstResponder];
    
	BOOL alerted = NO;
	
	NSString *vide = @"";
	
	if ([self.nomUtilisateur isEqualToString: vide] || [self.prenomUtilisateur isEqualToString: vide] || [self.eMailUtilisateur isEqualToString: vide] ||
		[self.pseudoUtilisateur isEqualToString: vide] || [self.motDePasseUtilisateur isEqualToString: vide]) {
		
		
        [self.view makeToast:[LanguageManager get:@"label_erreur_saisie_champ_vide"] duration:3.0 position:@"bottom" title:[LanguageManager get:@"title_erreur_saisie"] image:[UIImage imageNamed:@"flag_48" ] style:nil completion:nil];		
		alerted = YES;
		
	}
	
	//Vérification compte mail
	NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
	NSString *emailUtilisateurMinuscules = [self.eMailUtilisateur lowercaseString];
	BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:emailUtilisateurMinuscules];
	
	if (!myStringMatchesRegEx && !alerted) {
		
        [self.view makeToast:[LanguageManager get:@"ios_label_erreur_saisie_adresse"] duration:3.0 position:@"bottom" title:[LanguageManager get:@"title_erreur_saisie"] image:[UIImage imageNamed:@"flag_48" ] style:nil completion:nil];
		alerted = YES;
		
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
	}
	
	//Le pseudo doit comporter au moins 4 caractères (restriction imposée par Elgg)
	if((self.pseudoUtilisateur.length < 4) && !alerted) {
		
        [self.view makeToast:[LanguageManager get:@"ios_label_erreur_saisie_nom_utilisateur"]
                    duration:3.0
                    position:@"bottom"
                       title:[LanguageManager get:@"title_erreur_saisie"]
                       image:[UIImage imageNamed:@"flag_48" ] style:nil completion:nil];
		alerted = YES;
        
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
	}
	
	//Le mot de passe doit comporter au moins 6 caractères (restriction imposée par Elgg)
	if((self.motDePasseUtilisateur.length < 6) && !alerted) {
		
        [self.view makeToast:[LanguageManager get:@"ios_label_erreur_saisie_mdp"]
                    duration:3.0
                    position:@"bottom"
                       title:[LanguageManager get:@"title_erreur_saisie"]
                       image:[UIImage imageNamed:@"flag_48" ] style:nil completion:nil];
        
		alerted = YES;
        
        int direction = [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]] >= 1 ? UIPageViewControllerNavigationDirectionReverse : UIPageViewControllerNavigationDirectionForward;
        
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:1]] direction:direction animated:YES completion:nil];
		
	}
	//Les deux mots de passes doivent être identiques
    if(![self.motDePasseTextField.text isEqualToString:self.motDePasseTextField2.text] && !alerted) {
		
        [self.view makeToast:[LanguageManager get:@"ios_label_erreur_saisie_mdp_identique"]
                    duration:3.0
                    position:@"bottom"
                       title:[LanguageManager get:@"title_erreur_saisie"]
                       image:[UIImage imageNamed:@"flag_48"] style:nil completion:nil];
        
		alerted = YES;
		
        int direction = [self indexOfViewController:[self.pageController.viewControllers objectAtIndex:0]] >= 1 ? UIPageViewControllerNavigationDirectionReverse : UIPageViewControllerNavigationDirectionForward;
        
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:1]] direction:direction animated:YES completion:nil];
	}
	
	//Vérification nom utilisateur: caractères alphanumériques
	NSString *loginRegEx = @"^[0-9a-zA-Z]*$";
	NSPredicate *loginRegExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", loginRegEx];
	BOOL stringMatchesRegEx = [loginRegExPredicate evaluateWithObject:self.pseudoUtilisateur];
	
	if (!stringMatchesRegEx && !alerted) {
		
        [self.view makeToast:[LanguageManager get:@"ios_label_erreur_saisie_nom_utilisateur_alpha"]
                    duration:3.0
                    position:@"bottom"
                       title:[LanguageManager get:@"title_erreur_saisie"]
                       image:[UIImage imageNamed:@"flag_48" ] style:nil completion:nil];
        
        
		alerted = YES;
		
        [self.pageController setViewControllers:@[[self viewControllerAtIndex:0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
	}	
	
	//Si tout est OK
	if (![self.nomUtilisateur isEqualToString: vide] && ![self.prenomUtilisateur isEqualToString: vide] && ![self.eMailUtilisateur isEqualToString: vide] &&
		![self.pseudoUtilisateur isEqualToString: vide] && ![self.motDePasseUtilisateur isEqualToString: vide] && myStringMatchesRegEx &&
		(self.pseudoUtilisateur.length >= 4) && (self.motDePasseUtilisateur.length >= 6) && stringMatchesRegEx && [self.motDePasseTextField.text isEqualToString:self.motDePasseTextField2.text]) {
		
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_cgu"] andMessage:[LanguageManager get:@"ios_label_accepter_cgu"]];
        [alertView addButtonWithTitle:[LanguageManager get:@"button_non"]
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Refuse les conditions");
                              }];
        [alertView addButtonWithTitle:[LanguageManager get:@"button_oui"]
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  DLog(@"Accepte les  conditions");
                                  [self inscription];
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
//        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        
        [alertView show];
	}
    
}


-(void)inscription
{
    // Recuperation de la langue de l'utilisateur
    NSString * idLangue = [[NSNumber numberWithInt:[LanguageManager idLangueWithArrayIndex: self.indexLangueAppSelect]] stringValue];
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             self.nomUtilisateur,@"name",
                             self.prenomUtilisateur,@"surname",
                             self.eMailUtilisateur,@"email",
                             self.pseudoUtilisateur,@"username",
                             self.motDePasseUtilisateur, @"password",
                             self.motDePasseUtilisateur, @"password2",
                             self.etablissementTextField.text, @"etablissement",
                             idLangue, @"langue" , nil];
    
    [WService post:@"utilisateur/enregistrer"
         param:params
      callback:^(NSDictionary *wsReturn) {
          NSString * status = [wsReturn objectForKey:@"status"];
          
          if ([status isEqualToString: @"ok"]) {
              // Enregistrement des langues
              [self enregistrerLanguesAvecIDUtilisateur: [wsReturn objectForKey:@"id_utilisateur"]];
          }
          else
          {
              NSString * message = [wsReturn objectForKey:@"message"];
              
              if ([message isEqualToString:@"empty"]) {
                  
                  
                  [self.view makeToast:[LanguageManager get:@"ios_label_erreur_retour_champs"]
                              duration:3.0
                              position:@"bottom"
                                 title:[LanguageManager get:@"title_erreur_saisie"]
                                 image:[UIImage imageNamed:@"delete_48"] style:nil completion:nil];
              }
              
              else if ([message isEqualToString:@"username"]) {
                  
                  
                  [self.view makeToast:[LanguageManager get:@"ios_label_erreur_retour_nom_utilisateur"]
                              duration:3.0
                              position:@"bottom"
                                 title:[LanguageManager get:@"title_erreur_saisie"]
                                 image:[UIImage imageNamed:@"delete_48"] style:nil completion:nil];
              }
              
              else if ([message isEqualToString: @"email"]) {
                  
                  [self.view makeToast:[LanguageManager get:@"ios_label_erreur_retour_mail"]
                              duration:3.0
                              position:@"bottom"
                                 title:[LanguageManager get:@"title_erreur_saisie"]
                                 image:[UIImage imageNamed:@"delete_48"] style:nil completion:nil];
              }
              else if ([message isEqualToString:@"registerbad"] || (status == NULL))
              {
                  
                  
                  [self.view makeToast:[LanguageManager get:@"ios_label_erreur_enregistrement"]
                              duration:3.0
                              position:@"bottom"
                                 title:[LanguageManager get:@"title_erreur_saisie"]
                                 image:[UIImage imageNamed:@"delete_48"] style:nil completion:nil];
              }
              else
              {
                  NSString * toast = [LanguageManager get:message];
                  if (!toast)
                      toast = message;
                  
                  [self.view makeToast:toast
                              duration:3.0
                              position:@"bottom"
                                 title:[LanguageManager get:@"title_erreur_saisie"]
                                 image:[UIImage imageNamed:@"delete_48"] style:nil completion:nil];
              }
          }
        }
     errorMessage:[LanguageManager get:@"label_erreur_reseau_indisponible"]
     activityMessage:nil];
}

- (void) enregistrerLanguesAvecIDUtilisateur:(NSString *)idUtilisateur {
    // Recuperation des parametres necessaires pour modifier les langues de l'utilisateur
    NSString * idLanguePrincipale = [[NSNumber numberWithInt:[LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect]] stringValue];
    NSMutableArray * idsAutresLangues = [[NSMutableArray alloc] init];
    int i = 0;
    for ( id choix in self.choixOtherLanguages ) {
        if([choix boolValue]) {
            [idsAutresLangues addObject:[self.langueValuesId objectAtIndex:i]];
        }
        i++;
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:idsAutresLangues options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    // Appel du webservice pour modifier les langues
    NSDictionary *query = [[NSDictionary alloc] initWithObjectsAndKeys: idUtilisateur, @"id_utilisateur", idLanguePrincipale, @"langue_principale", jsonString, @"autres_langues", nil];
    [WService post:@"utilisateur/editerLangues"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ( ![WService displayErrors:wsReturn] )
          {
              UIView * successView = [self.view viewWithTag:1000];
              CGRect frame = successView.frame;
              frame.origin.x = 0;
              successView.frame = frame;
              
              successView.alpha = 0;
              
              [successView setHidden:NO];
              [self.view bringSubviewToFront:successView];
              
              [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  successView.alpha = 1.0;
                  self.pageController.view.alpha = 0.0;
              } completion:(void (^)(BOOL)) ^{
                  [self.pageController.view setHidden:YES];
              }];
              self.navigationItem.rightBarButtonItem = [(RootVC*)[self sidePanelController] customRightButtonForCenterPanel];
          }
    }];
}


- (IBAction)backgroundTap:(id)sender {
    DLog(@"");
	[nomTextField resignFirstResponder];
	[prenomTextField resignFirstResponder];
	[eMailTextField resignFirstResponder];
	[pseudoTextField resignFirstResponder];
	[motDePasseTextField resignFirstResponder];
}

// Gére l'action du clique sur le bouton langue de l'utilisateur
- (IBAction)touchDownLanguageButton:(id)sender {
    // Fermeture du clavier
    [self.view endEditing:YES];
    
    // Affichage du choix de la langue principale
    [RMPickerViewController setLocalizedTitleForCancelButton:[LanguageManager get:@"button_annuler"]];
    [RMPickerViewController setLocalizedTitleForSelectButton:[LanguageManager get:@"ios_button_selectionner"]];
    RMPickerViewController *pickerVC = [RMPickerViewController pickerController];
    pickerVC.delegate = self;
    pickerVC.titleLabel.text = [LanguageManager get:@"title_selectionner_langue"];
    
    if (IS_IPAD)
        [pickerVC showFromViewController:self];
    else
        [pickerVC show];
}

// Gère l'action du clique sur le bouton autres langues
- (IBAction)touchDownOtherLanguageButton:(id)sender {
    // Fermeture du clavier
    [self.view endEditing:YES];
    
    // Affichage du choix des autres langues
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:[LanguageManager get:@"title_autres_langues"] message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [self.tableViewOtherLanguages removeFromSuperview];
    [av setValue:self.tableViewOtherLanguages forKey:@"accessoryView"];
    [av show];
}

// Fonction pour afficher l'Alert Dialog pour changer la langue de l'application
- (void) displayDialogChangeAppLanguage {
    // Changement de la langue
    NSString * lastCodeLanguageApp = [LanguageManager getCurrentCodeLanguageApp];
    [LanguageManager setCurrentLanguageAppWithCode:[LanguageManager codeLangueWithArrayIndex:self.indexLangueAppSelect]];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_langue_application"] andMessage:[LanguageManager get:@"label_changer_langue_app"]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_non"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Refuse le changement");
                              // Retablissement de la langue
                              [LanguageManager setCurrentLanguageAppWithCode:lastCodeLanguageApp];
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_oui"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Accepte le changement");
                              //Enregistrement de la nouvelle langue de l'application
                              NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
                              NSDictionary * parametres = [preferences persistentDomainForName: kPreferences];
                              NSMutableDictionary *parametresMutable = [parametres mutableCopy];
                              [parametresMutable setObject:[NSNumber numberWithInt:[LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect]] forKey:@"idLangueApp"];
                              [preferences setPersistentDomain:parametresMutable forName:kPreferences];
                              
                              //Modification des termes
                              [self setContent];
                              
                              //Modification des termes des autres écrans ouverts
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLangueApp" object:self];
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    [alertView show];
}

#pragma mark - TableViewController Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.langueValuesId count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = [LanguageManager nameTrueLangueWithId:[[self.langueValuesId objectAtIndex:indexPath.row] intValue]];
    
    if([[self.choixOtherLanguages objectAtIndex:indexPath.row] boolValue]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}
- (void) tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.choixOtherLanguages replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:TRUE]];
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.choixOtherLanguages replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:FALSE]];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - RMPickerViewController Delegates
- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray *)selectedRows {
    NSString * lastIdLangueApp = [@([LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect]) stringValue];
    self.indexLangueAppSelect = [[selectedRows objectAtIndex:0] intValue];
    [self.langueButton setTitle:[LanguageManager nameTrueLangueWithArrayIndex:self.indexLangueAppSelect] forState:UIControlStateNormal];
    
    int idLangueP = [LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect];
    
    int i = 0;
    for ( id idLangue in self.langueValuesId ) {
        if([idLangue intValue] == idLangueP) {
            [self.langueValuesId replaceObjectAtIndex:i withObject:lastIdLangueApp];
            [self.choixOtherLanguages replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:FALSE]];
            break;
        }
        i++;
    }
    
    [self.tableViewOtherLanguages reloadData];
    [self displayDialogChangeAppLanguage];
}
- (void)pickerViewControllerDidCancel:(RMPickerViewController *)vc {}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.langueValuesString count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.langueValuesString objectAtIndex:row];
}

#pragma mark -
#pragma mark Username Persistence Methods

-(NSString *)dataFilePath:(NSString *)fileName {
    
    NSString *ressourceDirectory = [UtilsCore getRessourcesPath];
    return [ressourceDirectory stringByAppendingPathComponent:fileName];
}
//
//- (void)viewDidUnload {
//    DLog(@"");
//    [super viewDidUnload];
//	self.messageString = nil;
//	self.nomTextField = nil;
//	self.prenomTextField = nil;
//	self.eMailTextField = nil;
//	self.pseudoTextField = nil;
//	self.motDePasseTextField = nil;
//	self.indicator = nil;
//}

@end