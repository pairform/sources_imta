
#import "RessourceCell.h"
#import "CustomCellBackground.h"
#import <QuartzCore/QuartzCore.h>

@interface RessourceCell ()
{
    
}
@end

@implementation RessourceCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CustomCellBackground *backgroundView = [[CustomCellBackground alloc] initWithFrame:CGRectZero];
        self.selectedBackgroundView = backgroundView;
        UILongPressGestureRecognizer * pressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        pressRecognizer.minimumPressDuration = 1;
        pressRecognizer.delaysTouchesBegan = 1;
        [self addGestureRecognizer:pressRecognizer];
        

        [self setLabelColor:[UIColor darkGrayColor]];
        [self.badgeView setHidden:YES];
        
//        [self setLabelColor:[UIColor redColor]];
    }
    return self;
}

-(void)setRessourceName:(NSString*)ressourceName{
    [self setRessourceName:ressourceName withColor:self.labelColor];
}

-(void)setRessourceName:(NSString*)ressourceName withColor:(UIColor*)color{
    [self.label setText:ressourceName];
    [self.label setTextColor:color];
}

-(void)setRessourceImage:(UIImage*)image{
    [self setRessourceImage:image grayscale:NO];
}

-(void)setRessourceImage:(UIImage*)image grayscale:(BOOL)isGrayScale{
    if (isGrayScale) {
        image = [self convertToGrayscale:image];
    }
    
    [self.itemIcon setItemImage:image];
    [self.itemIcon setNeedsDisplay];
}

-(void)setBadgeColor:(UIColor *)color{
    [self.badgeView setBackgroundColor:color];
}

-(void)setBadge:(id)text{
    NSString * badgeText;
    
    if (![text isKindOfClass:NSClassFromString(@"NSString")])
        badgeText = [NSString stringWithFormat:@"%@", text];
    else
        badgeText = text;
    
    [self.badgeCount setText:badgeText];
    [self.badgeView setHidden:NO];
//    self.badgeView.layer.cornerRadius = 10;
}

#pragma mark - Gesture recognizer
-(void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer {
}

-(void)handleDeleteAction:(id)sender {
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.6;
        [self setTransform:CGAffineTransformMakeScale(1.2, 1.2)];
    }];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.superview];
    self.center = CGPointMake(location.x, location.y);
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
        [self setTransform:CGAffineTransformMakeScale(1, 1)];
    }];
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
}

#pragma mark - UIImage GrayScale
- (UIImage *)convertToGrayscale:(UIImage*)originImage {
    CGSize size = [originImage size];
    int width = size.width;
    int height = size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [originImage CGImage]);
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[1] + 0.59 * rgbaPixel[2] + 0.11 * rgbaPixel[3];
            
            // set the pixels to gray
            rgbaPixel[1] = gray;
            rgbaPixel[2] = gray;
            rgbaPixel[3] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return resultUIImage;
}

@end
