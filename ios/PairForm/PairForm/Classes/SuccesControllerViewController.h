//
//  SuccesControllerViewController.h
//  SupCast
//
//  Created by admin on 08/07/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ModeSucces,
    ModeBadgeDisplay,
    ModeBadgeAttribute
} ModeAffichage;

@protocol SuccesControllerDelegate <NSObject>

- (void)ajouterBadge:(NSNumber*)id_openbadge callback:(void (^)(NSDictionary *, NSNumber*))callback;

@end

@interface SuccesControllerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property NSArray * array_data;
@property ModeAffichage mode_affichage;
@property (nonatomic, strong) id<SuccesControllerDelegate> delegate;
@property(nonatomic, strong) NSDateFormatter * date_formatter;
@end

