//
//  ViewController.m
//  profil
//
//  Created by admin on 13/03/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "ProfilController.h"
//#import "CMPopTipView.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "MessageController.h"
#import "MKNetworkEngine.h"

@interface ProfilController ()
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *etablissement;
@property (nonatomic) int *idLangue;

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIImageView *flag;
@property (weak, nonatomic) IBOutlet UILabel *succes_label;
@property (weak, nonatomic) IBOutlet UILabel *labelNiveauxRessources;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionSucces;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionRessources;
@property (weak, nonatomic) IBOutlet UIButton *bMessages;
@property (weak, nonatomic) IBOutlet UIButton *bGroupe;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItemCurrent;

@property (weak, nonatomic) IBOutlet UILabel *label_badges;
@property (weak, nonatomic) IBOutlet UICollectionView *collection_badges;

@property (strong, nonatomic) NSDictionary *json;
@property (strong, nonatomic) NSMutableArray *array_succes;
@property (strong, nonatomic) NSArray * array_derniers_succes;
@property (strong, nonatomic) NSMutableArray *array_badges;
@property (strong, nonatomic) NSArray * array_derniers_badges;
@property (strong, nonatomic) NSMutableArray *array_ressources;

@property (strong, nonatomic) NSArray *array_badges_attribuables;
@property (assign, nonatomic) BOOL is_connected_user;
@property (nonatomic, strong) MKNetworkEngine * engine;


@end

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1
@implementation ProfilController


@synthesize collectionRessources;
@synthesize collectionSucces;
@synthesize collection_badges;
@synthesize json;
@synthesize array_ressources;
@synthesize array_succes;
@synthesize array_badges;

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Visionnage d'un profil"];
    [self.collectionRessources setClipsToBounds:NO];
//    [self connect];
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];

    if (IS_IPAD)
    {        [self setStackWidth: 320];
    }
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Profil"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    // Mise à jour des textes
    [self setContent];
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
    
    _engine = [[MKNetworkEngine alloc] initWithHostName:sc_server_static];
    [_engine useCache];
}

- (void) setContent {
    self.title = [LanguageManager get:@"title_profil"];
    self.name.text = [NSString stringWithFormat:@"%@...", [LanguageManager get:@"label_chargement"]];
    self.etablissement.text = [NSString stringWithFormat:@"%@...", [LanguageManager get:@"label_chargement"]];
    [self.bMessages setTitle:[LanguageManager get:@"button_ses_messages"] forState:UIControlStateNormal];
    [self.bGroupe setTitle:[NSString stringWithFormat:@"%@...", [LanguageManager get:@"button_ajouter"]] forState:UIControlStateNormal];
    self.labelNiveauxRessources.text = [LanguageManager get:@"title_ses_ressources"];
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self connect];
    
}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    //    self.actionButton.center = [[self.view viewWithTag:20] center];
    [self resizeSubViews];
}



-(void) resizeSubViews{
    NSLayoutConstraint * constraint = collectionRessources.constraints[0];
    constraint.constant = collectionRessources.contentSize.height;
    CGSize contentSize = self.scrollview.contentSize;
    contentSize.height = collectionRessources.frame.origin.y + collectionRessources.contentSize.height;
    [self.scrollview setContentSize:contentSize];
}

-(UIImage*)lazyLoadImage:(NSString*)url_logo onObject:(id)object ofCollectionOrTableView:(id)collection_or_table_view atIndexPath:(NSIndexPath*)index_path{
    
    //Verification que l'objet n'a pas déjà d'image associée
    UIImage * image = objc_getAssociatedObject(object, kLazyLoadAssociatedObject);
    
    //S'il y en a une, on s'arrête là
    if (image) {
        return image;
    }
    
    [_engine imageAtURL:[NSURL URLWithString:url_logo]
      completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
          DLog(@"");
          //On cale l'image en runtime
          objc_setAssociatedObject(object, kLazyLoadAssociatedObject, fetchedImage, OBJC_ASSOCIATION_COPY);
          
          if ([collection_or_table_view isKindOfClass:[UICollectionView class]])
              [(UICollectionView*)collection_or_table_view reloadItemsAtIndexPaths:@[index_path]];
          else if ([collection_or_table_view isKindOfClass:[UITableView class]])
              [(UITableView*)collection_or_table_view reloadRowsAtIndexPaths:@[index_path] withRowAnimation:UITableViewRowAnimationNone];
          
      }
           errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
               DLog(@"");
           }];
    
    return nil;
}
- (void)connect {
    

 
    //Appel du webservice
    
    NSDictionary * query ;
    _is_connected_user = false;
    if ( _id_utilisateur == nil ){
        _id_utilisateur = [UIViewController getSessionValueForKey:@"id_utilisateur"];
        _is_connected_user = true;
    }
    
    query = [[NSDictionary alloc] initWithObjectsAndKeys:_id_utilisateur,@"id_utilisateur",[NSNumber numberWithInt:[LanguageManager idLangueWithCode:[LanguageManager getCurrentCodeLanguageApp]]],@"id_langue", nil];
    
        
    
    [WService get:@"utilisateur/profil"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ([wsReturn[@"status"] isEqualToString:@"ok"]) {
              json = wsReturn[@"datas"];
              NSString* imageUrl = [json objectForKey:@"avatar_url"]; //2
              
              DLog(@"image: %@", imageUrl); //3
              
              NSURL *uneImage = [NSURL URLWithString:[imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
              
              _name.text = [json objectForKey:@"username"];
              UIImage * imageModifiee = [[UIImage imageWithData: [NSData dataWithContentsOfURL: uneImage]] thumbnailImage:88 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
              
              _avatar.image = imageModifiee;
              _email.text = [json objectForKey:@"email"];
              
              // Modification de l'image du drapeau
              if([json objectForKey:@"langue_principale"] != [NSNull null]){
                  self.idLangue = [[json objectForKey:@"langue_principale"] integerValue];
                  NSString *iconeDrapeau = [@"icone_drapeau_" stringByAppendingString:[LanguageManager codeLangueWithid:self.idLangue]];
                  iconeDrapeau = [iconeDrapeau stringByAppendingString:@".png"];
                  _flag.image = [UIImage imageNamed: iconeDrapeau];
                  [_flag.layer setCornerRadius:12.0];
                  [_flag.layer setMasksToBounds:YES];
              }
              
              
              
              if ([json objectForKey:@"etablissement"] == [NSNull null]) {
                  _etablissement.text = @"";
              }else{
                  _etablissement.text =  [json objectForKey:@"etablissement"];
              }
              
              
              
              if ([json objectForKey:@"ressources"] == [NSNull null]) {
                  array_ressources = [[NSMutableArray alloc] init];
              }else{
                  array_ressources = [json objectForKey:@"ressources"];
              }
              
              
              if ([json objectForKey:@"succes"] == [NSNull null]) {
                  array_succes = [[NSMutableArray alloc] init];
              }
              else{
                  array_succes = [[json objectForKey:@"succes"] mutableCopy];
                  
              }
              
              _array_derniers_succes = json[@"derniers_succes"];
              
              
              
              if ([json objectForKey:@"openbadges"] == [NSNull null]) {
                  array_badges = [[NSMutableArray alloc] init];
              }
              else{
                  array_badges = [[json objectForKey:@"openbadges"] mutableCopy];
                  _array_derniers_badges = [array_badges subarrayWithRange:NSMakeRange(0, MIN([array_badges count],5))];
              }
              
              _succes_label.text = [NSString stringWithFormat:@"%@ (%@/%@)",[LanguageManager get:@"title_derniers_succes"],[json objectForKey:@"nb_succes_gagnes"],[json objectForKey:@"nb_succes"]];
              
              _label_badges.text = [NSString stringWithFormat:@"%@ (%lu)",[LanguageManager get:@"title_derniers_badges"],(unsigned long)[array_badges count]];
              
              [collectionSucces reloadData];
              [collection_badges reloadData];
              [collectionRessources reloadData];
              
              //Si l'utilisateur connecté regarde son propre profil
              if (_is_connected_user){
                  [_bGroupe setTitle:[LanguageManager get:@"title_cercles"] forState:UIControlStateNormal];
                  NSMutableArray     *items = [_navigationItemCurrent.rightBarButtonItems mutableCopy] ;
                  
                  if (IS_IPAD)
                  {
                      UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                      [a1 setFrame:CGRectMake(280,8,32,32)];
                      [a1 addTarget:self action:@selector(editerCompte) forControlEvents:UIControlEventTouchUpInside];
                      [a1 setImage:[UIImage imageNamed:@"options_black"] forState:UIControlStateNormal];
                      [self.scrollview addSubview:a1];
                      
                      UIButton *logout_button = [UIButton buttonWithType:UIButtonTypeCustom];
                      [logout_button setFrame:CGRectMake(280,48,32,32)];
                      [logout_button addTarget:self action:@selector(deconnecterUser) forControlEvents:UIControlEventTouchUpInside];
                      [logout_button setImage:[UIImage imageNamed:@"Logout"] forState:UIControlStateNormal];
                      [self.scrollview addSubview:logout_button];
                      
                      [self.bMessages setTitle:[LanguageManager get:@"button_mes_messages"] forState:UIControlStateNormal];
                      [self.bGroupe setTitle:[LanguageManager get:@"button_cercles"] forState:UIControlStateNormal];
                      self.labelNiveauxRessources.text = [LanguageManager get:@"title_mes_ressources"];
                  }
                  else
                  {
//                      if ( items.count == 1){
                      
                          UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
                          [a1 setFrame:CGRectMake(5,6,32,32)];
                          [a1 addTarget:self action:@selector(editerCompte) forControlEvents:UIControlEventTouchUpInside];
                          [a1 setImage:[UIImage imageNamed:@"gear_64"] forState:UIControlStateNormal];
                          UIBarButtonItem *bEdit = [[UIBarButtonItem alloc] initWithCustomView:a1];
                          [items addObject:bEdit];
                          _navigationItemCurrent.rightBarButtonItems = items;
                          
                          [self.bMessages setTitle:[LanguageManager get:@"button_mes_messages"] forState:UIControlStateNormal];
                          [self.bGroupe setTitle:[LanguageManager get:@"button_cercles"] forState:UIControlStateNormal];
                          self.labelNiveauxRessources.text = [LanguageManager get:@"title_mes_ressources"];
//                      }
                  }
              }

          }
    } errorMessage:nil activityMessage:nil];
}
-(void)deconnecterUser{
    if (IS_IPAD) {
        [(RootVC_iPad*)self.sc_stackViewController.parentViewController overlayViewDidReceiveTap:nil];
        [self.sc_stackViewController logOut];

    }
    else{
//        [self.navigationController popToRootViewControllerAnimated:YES];0
        [self.navigationController logOut];
    }
}
-(void)editerCompte{
    [self performSegueWithIdentifier: @"editerCompteSegue" sender: self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];    
    // Dispose of any resources that can be recreated.
}


// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView == self.collectionSucces) {
        return  [_array_derniers_succes count];
    }
    else if (collectionView == self.collection_badges) {
        return  [_array_derniers_badges count];
    }
    else{
        return [array_ressources count];
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.collectionSucces) {
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myId" forIndexPath:indexPath];
        
        UIImage * image = [UIImage imageNamed:[[_array_derniers_succes objectAtIndex:indexPath.row] objectForKey:@"nom_logo"]];
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        
        if (image)
            [imageView setImage:image];
        else
            [imageView setImage:[UIImage imageNamed:@"succes"]];
        
        return cell;
    }
    else if (collectionView == self.collection_badges) {
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myId" forIndexPath:indexPath];

        UIImage * image = [self lazyLoadImage:_array_derniers_badges[indexPath.row][@"url_logo"]
                                     onObject:_array_derniers_badges[indexPath.row]
                                 ofCollectionOrTableView:collectionView
                                  atIndexPath:indexPath];
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        
        if (image)
            [imageView setImage:image];
        else
            [imageView setImage:[UIImage imageNamed:@"succes"]];
        
        return cell;
    }
    else{
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myIdRank" forIndexPath:indexPath];
        //[cell setBackgroundColor:[UIColor greenColor]];
        NSDictionary * currentRessourceDict =  [array_ressources objectAtIndex:indexPath.row];
        
        //Autoriser l'overflow de la cell
        [cell setClipsToBounds:NO];
        
        //Récup image ressource et affichage
        Ressource * currentRessource = [UtilsCore getRessource:currentRessourceDict[@"id_ressource"]];
        //Si elle n'est pas en local
        if(!currentRessource)
            currentRessource = [UtilsCore convertDictionaryToRessource:currentRessourceDict temporary:YES];
        
        UIImageView * icone_ressource = (UIImageView*)[cell viewWithTag:50];
        if(currentRessource)
            [icone_ressource setImage: [currentRessource getIcone]];
        else
            [icone_ressource setImage:[UIImage imageNamed:@"noRessourcePic"]];
        
        UILabel *lblName = (UILabel *)[cell viewWithTag:100];
        [lblName setText: currentRessourceDict[@"nom_court"]];
        
        UIProgressView *progress = (UIProgressView *)[cell viewWithTag:200];
        int score = [currentRessourceDict[@"score"] integerValue];
        int maxScore = [json[@"score_max"] integerValue];
        int id_categorie = [currentRessourceDict[@"id_categorie"] integerValue];
        // Expert ou admin
        if ( id_categorie == 4 || id_categorie == 5){
            [progress setProgress: 1];
            [progress setProgressTintColor:[UIColor colorWithRed:252/255.0 green:136/255.0 blue:30/255.0 alpha:1.0]];
        }else{
            [progress setProgress: (float)score/maxScore];
        }
        
        UILabel *lblRank = (UILabel *)[cell viewWithTag:250];
        NSString *key_role =  [NSString stringWithFormat:@"label_user_%@", [currentRessourceDict[@"nom_categorie"] lowercaseString]];
        [lblRank setText: [LanguageManager get:key_role]];
        
        UILabel *lblPts = (UILabel *)[cell viewWithTag:300];
        [lblPts setText: [NSString stringWithFormat:@"%i %@",score, [LanguageManager get:@"label_pts"]]];
        
        [self resizeSubViews];
        return cell;
        
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionSucces || collectionView == self.collection_badges) {
        return CGSizeMake(50.f, 50.f);
    }else{
//        return CGSizeMake(self.collectionRessources.frame.size.width-10.f, 70.f);
        return CGSizeMake(158, 70);
    }
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionRessources) {
        
        NSDictionary * currentRessourceDict =  [array_ressources objectAtIndex:indexPath.row];
        NSUInteger rank = [[[UIViewController getRankForRessource:[NSNumber numberWithInteger:[currentRessourceDict[@"id_ressource"] integerValue]]] objectForKey:@"id_categorie"] unsignedIntegerValue];
        
        if ( rank > 3){
            UIAlertController * menu_actions_ressources = [UIAlertController alertControllerWithTitle:[LanguageManager get:@"label_action_ressource"] message:currentRessourceDict[@"nom_long"] preferredStyle:UIAlertControllerStyleActionSheet];
        
            UIAlertAction * changer_role = [UIAlertAction actionWithTitle:[LanguageManager get:@"ios_label_attribuer_role_temp"]
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * _Nonnull action) {
                UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"ios_label_attribuer_role_temp"] delegate:self cancelButtonTitle:[LanguageManager get:@"button_annuler"] destructiveButtonTitle:nil  otherButtonTitles:[LanguageManager get:@"ios_label_rang_normal"],[LanguageManager get:@"ios_label_collaborateur"],[LanguageManager get:@"ios_label_animateur"],[LanguageManager get:@"ios_label_expert"], nil];
                [popupQuery setTag:[currentRessourceDict[@"id_ressource"] integerValue]];
                [popupQuery showInView:self.view];
            }];
            
            [menu_actions_ressources addAction:changer_role];
        
            if(rank > 4){
                UIAlertAction * ajouter_badge = [UIAlertAction actionWithTitle:[LanguageManager get:@"label_attribuer_openbadge"]
                                                                         style:UIAlertActionStyleDefault
                   handler:^(UIAlertAction * _Nonnull action) {
                       [self attribuerOpenBadge];
                }];
                [menu_actions_ressources addAction:ajouter_badge];
                
            }
            
            [menu_actions_ressources addAction:[UIAlertAction actionWithTitle:[LanguageManager get:@"button_annuler"]
                                                                        style:UIAlertActionStyleCancel                                                                   handler:^(UIAlertAction *action) {
                                                                            [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            if (IS_IPHONE) {
                [self presentViewController:menu_actions_ressources animated:YES completion:nil];
            }
            else{
                
                UIPopoverController* aPopover = [[UIPopoverController alloc]
                                                 initWithContentViewController:menu_actions_ressources];
                
                [aPopover presentPopoverFromRect:[[collectionView layoutAttributesForItemAtIndexPath:indexPath] frame]
                                          inView:collectionView
                        permittedArrowDirections:UIPopoverArrowDirectionAny
                                        animated:YES];

            }
            
         }
        
    }
   
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:[LanguageManager get:@"button_annuler"]] ){
        return;
    }
    NSString * cat;
    if([title isEqualToString:[LanguageManager get:@"ios_label_rang_normal"]] ){
        cat = @"0";
    }
    if([title isEqualToString:[LanguageManager get:@"ios_label_collaborateur"]] ){
        cat = @"2";
    }
    if([title isEqualToString:[LanguageManager get:@"ios_label_animateur"]] ){
        cat = @"3";
    }
    if([title isEqualToString:[LanguageManager get:@"ios_label_expert"]] ){
        cat = @"4";
    }
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:_id_utilisateur,@"id_utilisateur_concerne",[NSString stringWithFormat:@"%ld",(long)[actionSheet tag ]],@"id_ressource",cat,@"id_categorie", nil];
    [WService post:@"utilisateur/role"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          [self connect];
          [WService displayErrors:wsReturn];
      }];
    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    // Mode "Ajout a cercle" depuis le profil d'un membre

    if([identifier isEqualToString:@"cercleSegue"] && !_is_connected_user   )
    {
        if ( [self checkForLoggedInUser] )
            return YES;
        else
            return NO;
    }
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DLog(@"segue identififer = %@",segue.identifier);
    if([[segue identifier] isEqualToString:@"editerCompteSegue"])
    {
        EditerCompteController *viewC = [segue destinationViewController];
        viewC.email = _email.text;
        viewC.etablissement = _etablissement.text;
        viewC.avatar = _avatar.image;
        viewC.idLangue = _idLangue;
    }
    
    // Mode "Ajout a cercle" depuis le profil d'un membre
    if([[segue identifier] isEqualToString:@"cercleSegue"] && !_is_connected_user)
    {
        CercleController *viewC = [segue destinationViewController];
        viewC.delegate = self;
        viewC.modeCercle = ModeCercleAjoutDepuisProfil;
        
    }
    if([[segue identifier] isEqualToString:@"messageSegue"]  )
    {
        MessageController *viewC = [segue destinationViewController];
        viewC.modeMessage = ModeMessageTransversal;
        viewC.id_utilisateur = _id_utilisateur;
        
    }
    if([[segue identifier] isEqualToString:@"succes"]  )
    {
        SuccesControllerViewController *viewC = [segue destinationViewController];
        viewC.array_data = array_succes;
        [viewC setMode_affichage:ModeSucces];
    }
    if([[segue identifier] isEqualToString:@"badge"]  )
    {
        SuccesControllerViewController *viewC = [segue destinationViewController];
        viewC.array_data = array_badges;
        
        //Tricks : sender null = quand on attribue un badge depuis le menu
        if ([sender isKindOfClass:[UIButton class]]) {
            [viewC setMode_affichage:ModeBadgeDisplay];
        }
        else{
            viewC.array_data = _array_badges_attribuables;
            [viewC setMode_affichage:ModeBadgeAttribute];
            [viewC setDelegate:self];
        }
    }
   
}
- (void)actionAPartirDeCercle:(NSNumber*) idCercle nomCercle:(NSString *) nomCercle callback:(void (^)(NSDictionary *, NSNumber*))callback{
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:_id_utilisateur,@"id_utilisateur_concerne",idCercle,@"id_collection", nil];
    [WService put:@"reseau/utilisateur"
         param: query
      callback:^(NSDictionary *wsReturn) {
          //On execute le callback avec le retour du webservice ET l'id de l'utilisateur pour updater le cercle / classe
          callback(wsReturn, _id_utilisateur);
      }];
    
}

- (void) attribuerOpenBadge{
    if ([UIViewController isConnected]) {
        [WService get:@"utilisateur/openbadge/attribuables"
             callback:^(NSDictionary *wsReturn) {
                 if ([wsReturn[@"status"] isEqualToString:@"ok"]) {
                     _array_badges_attribuables = wsReturn[@"data"];
                     [self performSegueWithIdentifier:@"badge" sender:nil];
                 }
                 else{
                     if (wsReturn[@"message"]) {
                         [self.view makeToast:[LanguageManager get:wsReturn[@"message"]]];
                     }
                     else
                         [self.view makeToast:[LanguageManager get:@"erreur_serveur"]];
                     
                 }
             }];
    }
    else{
        [self.view makeToast:[LanguageManager get:@"label_erreur_reseau_indisponible"]];
    }

}
- (void)ajouterBadge:(NSNumber*)id_openbadge callback:(void (^)(NSDictionary *, NSNumber*))callback{
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys:_id_utilisateur,@"id_utilisateur_cible", id_openbadge, @"id_openbadge", nil];
    [WService put:@"utilisateur/openbadge"
            param: query
         callback:^(NSDictionary *wsReturn) {
             if (IS_IPAD) {
                 //Rafraichissement du profil
                 [self connect];
             }
             //On execute le callback avec le retour du webservice ET l'id de l'utilisateur pour updater le cercle / classe
             callback(wsReturn, _id_utilisateur);
         }];
    
}
@end
