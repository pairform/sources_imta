//
//  EditerCompteController.m
//  profil
//
//  Created by admin on 29/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "EditerCompteController.h"
#import "LanguageManager.h"
#import "SIAlertView.h"

@interface EditerCompteController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIButton *bAvatar;
@property (weak, nonatomic) IBOutlet UIButton *bLanguePrincipale;
@property (weak, nonatomic) IBOutlet UITextField *lMail;
@property (weak, nonatomic) IBOutlet UITextField *lEtablissement;
@property (weak, nonatomic) IBOutlet UITextField *lAncienMDP;
@property (weak, nonatomic) IBOutlet UITextField *lNouveauMDP;
@property (weak, nonatomic) IBOutlet UITextField *lNouveauMDPBis;
@property (weak, nonatomic) IBOutlet UITextField *activeField;
@property (weak, nonatomic) IBOutlet UINavigationItem *uiNavigationBar;
@property (weak, nonatomic) IBOutlet UILabel *labelEtablissement;
@property (weak, nonatomic) IBOutlet UILabel *labelMdp;
@property (weak, nonatomic) IBOutlet UILabel *labelLangues;
@property (weak, nonatomic) IBOutlet UILabel *labelPrincipale;
@property (weak, nonatomic) IBOutlet UILabel *labelAutres;
@property (weak, nonatomic) IBOutlet UIButton *bEnregistrer;
@property (weak, nonatomic) IBOutlet UIButton *bAutresLangues;

@property (nonatomic) CGPoint scrollPoint;
@property (nonatomic) CGFloat kbHeight;
@property (nonatomic) BOOL boolAvatarEstModifie;

@property (nonatomic, strong) UIAlertView * alertViewOtherLanguages;
@property (nonatomic, strong) UITableView * tableViewOtherLanguages;
@property (nonatomic, strong) NSMutableArray * choixOtherLanguages;
@property (strong, nonatomic) NSMutableArray * langueValuesString;
@property (strong, nonatomic) NSMutableArray * langueValuesId;
@property (nonatomic) int indexLangueAppSelect;

@end

@implementation EditerCompteController

@synthesize activeField;
@synthesize scroll;
@synthesize scrollPoint;
@synthesize kbHeight;
@synthesize boolAvatarEstModifie;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Edition d'un compte"];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWasShown:)
     name:UIKeyboardWillShowNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillBeHidden:)
     name:UIKeyboardWillHideNotification
     object:nil];
    _lMail.text = _email ;
    _lEtablissement.text = _etablissement;
    [_bAvatar setBackgroundImage:_avatar forState:UIControlStateNormal];
    boolAvatarEstModifie = NO;
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];
    
    // Initialisation de la liste des langues des messages
    self.tableViewOtherLanguages = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 264, 134)];
    self.tableViewOtherLanguages.delegate = self;
    self.tableViewOtherLanguages.dataSource = self;
    
    self.indexLangueAppSelect = [LanguageManager arrayIndexWithCode:[LanguageManager getCodeMainLanguage]];
    
    // Recuperation des langues dans le fichier PairForm-Arrays.plist
    self.langueValuesString = [NSMutableArray arrayWithArray:[LanguageManager getArrayTrueNameLanguage]];
    self.langueValuesId = [NSMutableArray arrayWithArray:[LanguageManager getArrayIdLanguage]];
    [self.langueValuesId removeObjectAtIndex:[LanguageManager arrayIndexWithCode:[LanguageManager getCodeMainLanguage]]];
    
    // Initialisation du tableau du choix des autres langues
    self.choixOtherLanguages = [[NSMutableArray alloc] init];
    int i = 0;
    for ( id idLangue in self.langueValuesId ) {
        bool isSelected = [LanguageManager getPrefOtherLanguagesWithCodeLangue:[LanguageManager codeLangueWithid:[idLangue intValue]]];
        [self.choixOtherLanguages insertObject:[NSNumber numberWithBool:isSelected] atIndex:i];
        i++;
    }
    
    // Modification du text du bouton de la langue principale
    NSString * langueApp = [LanguageManager nameTrueLangueWithId:self.idLangue];
    [self.bLanguePrincipale setTitle:langueApp forState:UIControlStateNormal];
    
    [self setDesign];
    [self setContent];

    if (IS_IPAD)
    {
        [self setStackWidth: 320];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setContent {
    self.title = [LanguageManager get:@"title_modification_profil"];
    self.labelEtablissement.text = [LanguageManager get:@"label_etablissement"];
    self.labelMdp.text = [LanguageManager get:@"label_mot_de_passe"];
    self.lAncienMDP.placeholder = [LanguageManager get:@"label_mot_de_passe_ancien"];
    self.lNouveauMDP.placeholder = [LanguageManager get:@"label_mot_de_passe_nouveau"];
    self.lNouveauMDPBis.placeholder = [LanguageManager get:@"label_mot_de_passe_confirmation"];
    self.labelLangues.text = [LanguageManager get:@"label_langues"];
    self.labelPrincipale.text = [NSString stringWithFormat:@"%@ :", [LanguageManager get:@"label_langue_principale"]];
    self.labelAutres.text = [NSString stringWithFormat:@"%@ :", [LanguageManager get:@"label_autres_langues"]];
    [self.bAutresLangues setTitle:[NSString stringWithFormat:@"%@...", [LanguageManager get:@"button_choisir"]] forState:UIControlStateNormal];
    [self.bEnregistrer setTitle:[LanguageManager get:@"button_enregistrer"] forState:UIControlStateNormal];
}

- (void) setDesign {
    self.bLanguePrincipale.layer.cornerRadius = 3;
    self.bLanguePrincipale.layer.borderWidth = 1;
    self.bLanguePrincipale.layer.borderColor = [UIColor blueColor].CGColor;
    [self.bLanguePrincipale setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    
    self.bAutresLangues.layer.cornerRadius = 3;
    self.bAutresLangues.layer.borderWidth = 1;
    self.bAutresLangues.layer.borderColor = [UIColor blueColor].CGColor;
    [self.bAutresLangues setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

/*- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_scroll scrollRectToVisible:textField.frame animated:YES];
    return YES;
}*/

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [self ajusteScroll];
     DLog(@"active");
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    DLog(@"keyboard");
    [scroll setContentOffset:scrollPoint animated:YES];
}
- (void)keyboardWasShown:(NSNotification*)aNotification {


    
    NSDictionary* info = [aNotification userInfo];
    

    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        kbHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.width;
    }
    else {
        kbHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    }
    
    scrollPoint = scroll.contentOffset;
    [self ajusteScroll];
    
}

-(void)ajusteScroll{
    CGRect aRect = scroll.frame;
    aRect.size.height -= kbHeight;
    CGPoint origin = activeField.frame.origin;
    if (!CGRectContainsPoint(aRect, origin) ) {
        CGPoint scrollPoint2 = CGPointMake(0.0, activeField.frame.origin.y-(aRect.size.height)+activeField.frame.size.height);
        [scroll setContentOffset:scrollPoint2 animated:YES];
    }
   
}

- (IBAction)modifierAvatar:(id)sender {
    [self showImagePicker];
}


-(BOOL)shouldAutorotate{
    return NO;
}

- (IBAction)modifierLanguePrincipale:(id)sender {
    [RMPickerViewController setLocalizedTitleForCancelButton:[LanguageManager get:@"button_annuler"]];
    [RMPickerViewController setLocalizedTitleForSelectButton:[LanguageManager get:@"ios_button_selectionner"]];
    RMPickerViewController *pickerVC = [RMPickerViewController pickerController];
    pickerVC.delegate = self;
    pickerVC.titleLabel.text = [LanguageManager get:@"title_selectionner_langue"];
    
    if (IS_IPAD)
        [pickerVC showFromViewController:self];
    else
        [pickerVC show];
    
}

- (IBAction)modifierAutresLangues:(id)sender {
    if (!self.alertViewOtherLanguages) {
        self.alertViewOtherLanguages = [[UIAlertView alloc] initWithTitle:[LanguageManager get:@"title_autres_langues"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [self.alertViewOtherLanguages setValue:self.tableViewOtherLanguages forKey:@"accessoryView"];
    }
    [self.alertViewOtherLanguages show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
//    [alertView setValue:nil forKey:@"accessoryView"];
//    [self.tableViewOtherLanguages removeFromSuperview];
    
}
- (void)showImagePicker
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    //Si l'orientation paysage n'est pas supportée, sur IPAD ca crash net
    if(IS_IPAD){
        
        UIPopoverController * imagePicker_popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        [imagePicker_popover presentPopoverFromRect:[self.bAvatar frame]
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
//        [self.view makeToast:@"Le changement d'avatar n'est pas supporté sur iPad." duration:3.0 position:@"bottom" title:nil image:[UIImage imageNamed:@"info_48"] style:nil completion:nil];
    } else{
        
        [imagePicker setModalInPopover:YES];
        [imagePicker setModalPresentationStyle:UIModalPresentationPageSheet];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Get the selected image.
    UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
    CGSize newSize = CGSizeMake(100.0f, 100.0f);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* image_reduite = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage * imageModifiee = [image_reduite thumbnailImage:88 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
    
    [_bAvatar setBackgroundImage:imageModifiee forState:UIControlStateNormal];
//    NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
//    DLog(@"selectedImage: %@", assetURL);
    boolAvatarEstModifie = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    DLog(@"%f",[imageModifiee size].height);
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}




- (IBAction)enregistrer:(id)sender {
    if ( boolAvatarEstModifie ){
        [self uploadImage];
    }else{
        [self enregistrerInfos];
    }
}


-(void)uploadImage{
    
    NSData * imageData = UIImagePNGRepresentation([_bAvatar backgroundImageForState:UIControlStateNormal]);
    
    NSString *encodedString = [imageData base64EncodedStringWithOptions:nil];
    
    [WService post:@"utilisateur/avatar"
             param:@{@"avatar" : encodedString} callback:
     ^(NSDictionary *wsReturn) {
         if ( ![WService displayErrors:wsReturn]){
             [self enregistrerInfos];}
     } errorMessage:[LanguageManager get:@"ios_label_probleme_upload"] activityMessage:[LanguageManager get:@"label_avatar_mis_a_jour"]];
    
}

-(void)enregistrerInfos{
    //Appel du webservice
    NSDictionary * query = [[NSDictionary alloc] initWithObjectsAndKeys: _lMail.text , @"email",_lEtablissement.text,@"etablissement",_lAncienMDP.text,@"current_password",_lNouveauMDP.text,@"password",_lNouveauMDPBis.text,@"password2", nil];
    [WService post:@"utilisateur/editer"
         param: query
      callback: ^(NSDictionary *wsReturn) {}
     errorMessage:nil activityMessage:nil
     ];
    
    // Recuperation des parametres necessaires pour modifier les langues de l'utilisateur
    NSString * idLanguePrincipale = [[NSNumber numberWithInt:[LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect]] stringValue];
    NSString * id_user = [UIViewController getSessionValueForKey:@"id_utilisateur"];
    NSMutableArray * idsAutresLangues = [[NSMutableArray alloc] init];
    int i = 0;
    for ( id choix in self.choixOtherLanguages ) {
        if([choix boolValue]) {
            [idsAutresLangues addObject:[self.langueValuesId objectAtIndex:i]];
        }
        i++;
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:idsAutresLangues options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    // Appel du webservice pour modifier les langues
    query = [[NSDictionary alloc] initWithObjectsAndKeys: id_user, @"id_utilisateur", idLanguePrincipale, @"langue_principale", jsonString, @"autres_langues", nil];
    [WService post:@"utilisateur/editerLangues"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          if ( ![WService displayErrors:wsReturn] )
          {
              [self.navigationController popViewControllerAnimated:YES];
              
              [LanguageManager setMainLanguageWitCode:[LanguageManager codeLangueWithid:[idLanguePrincipale intValue]]];
              [LanguageManager removeOtherLanguages];
              int i = 0;
              for ( id choix in self.choixOtherLanguages ) {
                  if([choix boolValue]) {
                      [LanguageManager addOtherLanguageWithCodeLangue:[LanguageManager codeLangueWithid:[[self.langueValuesId objectAtIndex:i] intValue]]];
                  }
                  i++;
              } 
          }
          if (IS_IPAD) {
              [[self sc_stackViewController] popViewControllerAtPosition:[self stackPosition] animated:YES completion:nil];
          }
      } errorMessage:nil activityMessage:nil];
}

// Fonction pour afficher l'Alert Dialog pour changer la langue de l'application
- (void) displayDialogChangeAppLanguage {
    // Changement de la langue
    NSString * lastCodeLanguageApp = [LanguageManager getCurrentCodeLanguageApp];
    [LanguageManager setCurrentLanguageAppWithCode:[LanguageManager codeLangueWithArrayIndex:self.indexLangueAppSelect]];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:[LanguageManager get:@"title_langue_application"] andMessage:[LanguageManager get:@"label_changer_langue_app"]];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_non"]
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Refuse le changement");
                              [LanguageManager setCurrentLanguageAppWithCode:lastCodeLanguageApp];
                          }];
    [alertView addButtonWithTitle:[LanguageManager get:@"button_oui"]
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              DLog(@"Accepte le changement");
                              //Modification de la langue de l'application
                              [LanguageManager setCurrentLanguageAppWithCode:[LanguageManager codeLangueWithArrayIndex:self.indexLangueAppSelect]];
                              NSUserDefaults * preferences = [NSUserDefaults standardUserDefaults];
                              NSDictionary * parametres = [preferences persistentDomainForName: kPreferences];
                              NSMutableDictionary *parametresMutable = [parametres mutableCopy];
                              [parametresMutable setObject:[NSNumber numberWithInt:[LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect]] forKey:@"idLangueApp"];
                              [preferences setPersistentDomain:parametresMutable forName:kPreferences];
                              
                              //Modification des termes
                              [self setContent];
                              
                              //Modification des termes des autres écrans ouverts
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLangueApp" object:self];
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    [alertView show];
}


#pragma mark - TableViewController Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.langueValuesId count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = [LanguageManager nameTrueLangueWithId:[[self.langueValuesId objectAtIndex:indexPath.row] intValue]];
    
    if([[self.choixOtherLanguages objectAtIndex:indexPath.row] boolValue]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}
- (void) tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.choixOtherLanguages replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:TRUE]];
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.choixOtherLanguages replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:FALSE]];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - RMPickerViewController Delegates
- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray *)selectedRows {
    NSString * lastIdLangueApp = [@([LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect]) stringValue];
    self.indexLangueAppSelect = [[selectedRows objectAtIndex:0] intValue];
    [self.bLanguePrincipale setTitle:[LanguageManager nameTrueLangueWithArrayIndex:self.indexLangueAppSelect] forState:UIControlStateNormal];
    
    int idLangueP = [LanguageManager idLangueWithArrayIndex:self.indexLangueAppSelect];
 
    int i = 0;
    for ( id idLangue in self.langueValuesId ) {
        if([idLangue intValue] == idLangueP) {
            [self.langueValuesId replaceObjectAtIndex:i withObject:lastIdLangueApp];
            [self.choixOtherLanguages replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:FALSE]];
            break;
        }
        i++;
    }
    
    [self.tableViewOtherLanguages reloadData];
    [self displayDialogChangeAppLanguage];
}
- (void)pickerViewControllerDidCancel:(RMPickerViewController *)vc {}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.langueValuesString count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.langueValuesString objectAtIndex:row];
}

@end
