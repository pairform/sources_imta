//
//  ZoomFSViewController.m
//  SupCast
//
//  Created by Cape EMN on 30/10/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import "ZoomFSViewController.h"

@implementation ZoomFSViewController

@synthesize imageView, scrollZoomView,delegate;

- (void)viewDidLoad
{
    DLog(@"");
    [super viewDidLoad];
    
    self.title = @"Zoom";
    //Mode image
    if (self.image) {
        
        [self.imageView setImage:self.image];
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;

        self.scrollZoomView.imgView = self.imageView;
        self.scrollZoomView.minimumZoomScale=1;
        
        self.scrollZoomView.maximumZoomScale=6.0;
        
        self.scrollZoomView.zoomScale = 1.1;
        self.scrollZoomView.zoomScale = 1.0;
        
        self.scrollZoomView.contentSize = self.contentSize;
        
        self.scrollZoomView.delegate=self;
        self.scrollZoomView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;

        //scrollZoomView.minimumZoomScale = scrollZoomView.frame.size.width / imageView.frame.size.width;
    }
}

- (void)viewDidUnload
{
    DLog(@"");
    [super viewDidUnload];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = MAX((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0);
    CGFloat offsetY = MAX((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0);
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

-(IBAction)clicBouton:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(dismissZoomFS)])
        [self.delegate dismissZoomFS];
}
@end
