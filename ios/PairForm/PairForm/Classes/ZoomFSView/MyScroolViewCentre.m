//
//  MyScroolViewCentre.m
//  PairForm
//
//  Created by vincent on 30/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "MyScroolViewCentre.h"

@implementation MyScroolViewCentre

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.imgView != nil) {
        // center the image as it becomes smaller than the size of the screen
        CGSize boundsSize = self.bounds.size;
        
        UIImage * img = self.imgView.image;
        CGRect  frameToCenter =  CGRectZero;
        
        if (img) {
            // Change orientation: Portrait -> Lanscape : this lets you see the whole image
            if (UIDeviceOrientationIsLandscape( [UIDevice currentDevice].orientation)) {
                frameToCenter = CGRectMake(0, 0, (self.frame.size.height*img.size.width)/img.size.height  * self.zoomScale, self.frame.size.height * self.zoomScale);
            } else {
                frameToCenter = CGRectMake(0, 0, self.frame.size.width * self.zoomScale, (self.frame.size.width*img.size.height)/img.size.width  * self.zoomScale);
            }
        } else {
            frameToCenter =  self.imgView.frame;
        }
        
        // center horizontally
        if (frameToCenter.size.width < boundsSize.width)
            frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
        else
            frameToCenter.origin.x = 0;
        
        // center vertically
        if (frameToCenter.size.height < boundsSize.height)
            frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
        else
            frameToCenter.origin.y = 0;
        
        self.imgView.frame = frameToCenter;
        self.contentSize = self.imgView.frame.size;
    }
}

@end
