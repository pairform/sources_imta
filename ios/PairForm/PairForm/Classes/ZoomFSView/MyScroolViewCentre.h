//
//  MyScroolViewCentre.h
//  PairForm
//
//  Created by vincent on 30/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyScroolViewCentre : UIScrollView

@property (nonatomic,retain) UIImageView * imgView;

@end
