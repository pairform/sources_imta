//
//  ZoomFSViewController.h
//  SupCast
//
//  Created by Cape EMN on 30/10/12.
//  Copyright (c) 2012 CAPE - Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyScroolViewCentre.h"

@protocol ZoomFSViewDelegate;
@interface ZoomFSViewController : UIViewController <UIScrollViewDelegate>

@property(weak,nonatomic) IBOutlet UIImageView* imageView;
@property(weak,nonatomic) IBOutlet MyScroolViewCentre* scrollZoomView;
@property CGSize contentSize;
@property(strong,nonatomic) id <ZoomFSViewDelegate> delegate;

@property(weak,nonatomic)  UIImage* image;

//-(id)initWithPicture:(NSString *)imagePath;

-(IBAction)clicBouton:(id)sender;

@end
@protocol ZoomFSViewDelegate <NSObject>

-(void)dismissZoomFS;

@end