//
//  RessourceStoreVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 31/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "RessourceStoreVC.h"
#import "TelechargementVC.h"
#import "Ressource.h"
#import "UtilsCore.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LanguageManager.h"
#import "UIImage+ImageEffects.h"
#import "MKNetworkKit.h"
#import <objc/runtime.h>

#import "ReachabilityManager.h"

@interface RessourceStoreVC ()
@property (nonatomic, strong) NSDictionary * messagesNumberForRessource;
@property (nonatomic, strong) UILabel * label_no_internet;
@end

@implementation RessourceStoreVC

@synthesize data;
@synthesize etablissementData;
@synthesize themeData;
@synthesize latestData;
@synthesize modeRessource;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.title = @"Store";
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword"]]];
    
    if (modeRessource == ModeRessourceLatest) {
        [self setStackPosition:SCStackViewControllerPositionLeft];
        [self.toolbar setHidden:YES];
        [self checkReachable:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkReachable:) name:kReachabilityChangedNotification object:nil];
        
        _label_no_internet = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 480, 80)];
        [_label_no_internet setText:[LanguageManager get:@"label_erreur_reseau_indisponible"]];
        [_label_no_internet setFont:[UIFont fontWithName:@"Helvetica Neue" size:16.0f]];
        [_label_no_internet setTextColor:[UIColor grayColor]];
        [_label_no_internet setNumberOfLines:2];
        [_label_no_internet setTextAlignment:NSTextAlignmentCenter];
        [_label_no_internet setHidden:YES];
        
        [self.view addSubview:_label_no_internet];
//        [self checkReachable];
    }
    else{
        [self getRessourcesStuff];
    }
    
    if (IS_IPAD)
    {
        //        [self setStackWidth: 640];
        [self setStackWidth:320];
    }
    
 
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Ressource Store"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self setContent];
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
    
    //Inscription au notification center pour le changement d'utilisateur (rafraichissement des capsules en fonction)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRessources) name:@"userLoggedIn" object:nil];
}

//Latch de la valeur précédente, pour éviter des updates à chaque "changement" de statut de la connection
BOOL previous_reachability_status = NO;

-(void) checkReachable:(id)sender{
    //Ce petit malin de reachability envoie tout le temps des notifications, même quand le réseau n'a pas changé de statut
    
    Reachability * _reachability = [ReachabilityManager sharedManager].reachability;
    //Reachability * _reachability = [(SCAppDelegate*)[[UIApplication sharedApplication] delegate] reachability];
    if ([_reachability isReachable] != previous_reachability_status){
        if ([_reachability isReachable]){
            [self getRessourcesStuff];
            [_label_no_internet setHidden:YES];
        }
        else{
            [_label_no_internet setHidden:NO];
            latestData = [[NSMutableArray alloc] init];
            [self.collectionView reloadData];
        }
        previous_reachability_status = [_reachability isReachable];
    }
    
}
-(void) setContent {
    self.title = [LanguageManager get:@"title_ajouter_ressource"];
    [self.displaySelector setTitle:[LanguageManager get:@"button_etablissements"] forSegmentAtIndex:0];
    [self.displaySelector setTitle:[LanguageManager get:@"button_themes"] forSegmentAtIndex:1];
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}
-(void)getRessourcesStuff{
    [self.view makeToastActivity:@"center"];
    
    [self getMessagesNumberForRessources];
    [self getRessources];
}
-(void)getRessources{
    
    //Appel du webservice
    data = [[NSMutableArray alloc] init];
    etablissementData = [[NSMutableArray alloc] init];
    themeData = [[NSMutableArray alloc] init];
    latestData = [[NSMutableArray alloc] init];
    
    [WService get:@"ressource/liste"
             param:@{@"langues_affichage": [LanguageManager displayLanguagesStringForQuery], @"langue_app": [NSNumber numberWithInt:[LanguageManager idLangueWithCode:[LanguageManager getCurrentCodeLanguageApp]]]} callback: ^(NSDictionary *wsReturn)
     {
         data = [wsReturn objectForKey:@"ressources"];
         //On clean ce tableau pour ne pas avoir de duplicates
         [latestData removeAllObjects];
         
         MKNetworkEngine * engine = [[MKNetworkEngine alloc] initWithHostName:sc_server_static];
         [engine useCache];
         for (NSDictionary * ressourceDictionary in self.data) {
             
             Ressource * ressource = [UtilsCore convertDictionaryToRessource:ressourceDictionary temporary:YES];
             //On récupère l'image distante
             [engine imageAtURL:[NSURL URLWithString:[sc_server_static stringByAppendingString:ressource.url_logo]] completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
                 //S'il n'y avait vraiment pas d'image (ne pas faire getIcone, car il renvoie une image par défaut)
                 if (!ressource.icone) {
                     [ressource setIcone:[NSKeyedArchiver archivedDataWithRootObject:fetchedImage]];
                     objc_setAssociatedObject(ressource, @"PFNeedsUpdate", [NSNumber numberWithBool:YES], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                     
                     [self.collectionView reloadData];
                 }
             } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
                 [ressource setIcone:[NSKeyedArchiver archivedDataWithRootObject:[UIImage imageNamed:@"noRessourcePic"]]];
             }];
//           UIImage * icone = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[sc_server_static stringByAppendingString:ressource.url_logo]]]];

             
             if (modeRessource == ModeRessourceBrowse) {
                 
                 NSString * theme = ressource.theme;
                 NSString * etablissement = ressource.espace_nom_court;

                 
                 //Tri par thème
                 NSUInteger index_theme = [themeData indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                     return stop = [obj[@"header"] isEqualToString:theme];
                 }];
                 
                 if (index_theme == NSNotFound) {
                     [themeData addObject:@{@"header" : theme, @"data" : [NSMutableArray arrayWithObject:ressource]}];
                 }
                 else{
                     [themeData[index_theme][@"data"] addObject:ressource];
                 }
                 
                 NSUInteger index_etablissement = [etablissementData indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                     return stop = [obj[@"header"] isEqualToString:etablissement];
                 }];
                 
                 if (index_etablissement == NSNotFound) {
                     [etablissementData addObject:@{@"header" : etablissement, @"data" : [NSMutableArray arrayWithObject:ressource]}];
                 }
                 else{
                     [etablissementData[index_etablissement][@"data"] addObject:ressource];
                 }
//                 
//                 if (![sectionThemeData containsObject:theme]) {
//                     [sectionThemeData addObject:theme];
//                     [themeData addObject:[NSMutableArray arrayWithObject:ressource]];
//                 }
//                 else{
//                     int index = [self.sectionThemeData indexOfObject:theme];
//                     [[themeData objectAtIndex:index] addObject:ressource];
//                 }
//
//                 if (![sectionEtablissementData containsObject:etablissement]) {
//                     [sectionEtablissementData addObject:etablissement];
//                     [etablissementData addObject:[NSMutableArray arrayWithObject:ressource]];
//                 }
//                 else{
//                     int index = [self.sectionEtablissementData indexOfObject:etablissement];
//                     [[etablissementData objectAtIndex:index] addObject:ressource];
//                 }

             }
             else if (modeRessource == ModeRessourceLatest){
                 [latestData addObject:ressource];
             }
         }
         //Tri des tableaux par ordre alphabétique
         [themeData sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"header" ascending:YES]]];
         [etablissementData sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"header" ascending:YES]]];
         
         //Récuperation d'une éventuelle ressource à highlighter
//         if (modeRessource == ModeRessourceBrowse && _idRessourceToHighlight && [ressource.id_ressource isEqualToNumber:_idRessourceToHighlight]) {
//             int index = [self.sectionEtablissementData indexOfObject:etablissement];
//             indexPath = [NSIndexPath indexPathForItem:[[etablissementData objectAtIndex:index] count] -1 inSection:index];
//         }
         if (modeRessource == ModeRessourceLatest) {
             [latestData sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                 NSDate * date1 = [(Ressource*)obj1 date_creation];
                 NSDate * date2 = [(Ressource*)obj2 date_creation];
                 return [date2 compare:date1];
             }];
            
             if ([latestData count] > 8) {
                 NSRange r;
                 r.location = 8;
                 r.length = [latestData count] - r.location;
                 
                 [latestData removeObjectsInRange:r];                 
             }
         }
         [self.view hideToastActivity];
         [self.collectionView reloadData];
         
         //Highlight de la ressource en cours, dans le cas échéant
         if (modeRessource == ModeRessourceBrowse && _idRessourceToHighlight) {
             __block NSUInteger index_row;
             __block NSUInteger index_section;
             
             //Récuperation des index de la ressource en itérant à travers les data de l'établissement fraichement triés
             [etablissementData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                 [obj[@"data"] enumerateObjectsUsingBlock:^(Ressource* obj, NSUInteger idx, BOOL *stop) {
                     index_row = (stop = [obj.id_ressource isEqualToNumber:_idRessourceToHighlight]) ? idx : NSNotFound;
                 }];
                 index_section = (stop = index_row != NSNotFound) ? idx : NSNotFound;
             }];
             
             if (index_row != NSNotFound && index_section != NSNotFound) {
                 
                 NSIndexPath * indexPath = [NSIndexPath indexPathForItem:index_row inSection:index_section];
//                 [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
                 [self.collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
                 [self performSegueWithIdentifier:@"storeCellSegue" sender:self];
             }
         }
     }
      errorMessage:(modeRessource == ModeRessourceLatest) ? nil : [LanguageManager get:@"label_erreur_reseau_indisponible"]
   activityMessage:nil];
    
}

-(void)getMessagesNumberForRessources{
    
    NSString * langues_affichage = [LanguageManager displayLanguagesStringForQuery];
    [WService get:@"message/nombre" param:@{@"langues_affichage": langues_affichage}
          callback: ^(NSDictionary *wsReturn)
     {
         _messagesNumberForRessource = wsReturn[@"nombre_messages"];
         [self.collectionView reloadData];
     } errorMessage:nil activityMessage:nil];
    
}

-(IBAction)segmentedControlChanged:(id)sender{
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView delegates


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    // _data is a class member variable that contains one array per section.
    if (modeRessource == ModeRessourceBrowse) {
        if([self.displaySelector selectedSegmentIndex] == 0)
            return [etablissementData count];
        else
            return [themeData count];
    }
    else if (modeRessource == ModeRessourceLatest){
        return 1;
    }
    else
        return 1;
    
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (modeRessource == ModeRessourceBrowse) {
        if([self.displaySelector selectedSegmentIndex] == 0)
            return [[etablissementData objectAtIndex:section][@"data"] count];
        else
            return [[themeData objectAtIndex:section][@"data"] count];
    }
    else if (modeRessource == ModeRessourceLatest){
        return [latestData count];
    }
    else
        return 0;
    
    
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView * header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeader" forIndexPath:indexPath];
    if((kind == UICollectionElementKindSectionHeader) && (modeRessource == ModeRessourceBrowse))
    {
        //modify your header
        UILabel * title = (UILabel*)[header viewWithTag:1];
        if([self.displaySelector selectedSegmentIndex] == 0)
            [title setText: [etablissementData objectAtIndex:indexPath.section][@"header"]];
        else
            [title setText: [themeData objectAtIndex:indexPath.section][@"header"]];
        
        return header;
    }
    [header setHidden:YES];
    return header;
    
    
}
-(void)dealloc{
    self.collectionView = nil;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Ressource * currentRessource;
    
    if (modeRessource == ModeRessourceBrowse) {
        if([self.displaySelector selectedSegmentIndex] == 0)
            currentRessource = [[etablissementData objectAtIndex:indexPath.section][@"data"] objectAtIndex:indexPath.row];
        else
            currentRessource = [[themeData objectAtIndex:indexPath.section][@"data"] objectAtIndex:indexPath.row];
    }
    else if (modeRessource == ModeRessourceLatest){
        currentRessource = [latestData objectAtIndex:indexPath.row];
    }
    
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellRes" forIndexPath:indexPath];
    
    UIImageView * cellImage = (UIImageView *)[cell viewWithTag:1];
    
    
    //Si ressource privée
//    if ([currentRessource.visible isEqualToNumber:@0]) {
//        [[cell viewWithTag:1000] removeFromSuperview];
//        [cellImage setImage:[self blurredImageFromImage:[currentRessource getIcone]]];
//        [cell setClipsToBounds:NO];
//        UIImageView * locked_image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock_48"]];
//        [locked_image setFrame:CGRectMake(26, 15, 48, 48)];
//        [locked_image setTag:1000];
//        [cell addSubview:locked_image];
//    }
//    else{
        [[cell viewWithTag:1000] removeFromSuperview];
    UIImage * icone_ressource = [currentRessource getIcone];
    
    
//    if ([self image:icone_ressource isEqualTo:cellImage.image]){
    //Cast en booléen
    if (!!objc_getAssociatedObject(currentRessource, @"PFNeedsUpdate")) {
        objc_setAssociatedObject(currentRessource, @"PFNeedsUpdate", nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [cellImage setAlpha:0.0];
        [cellImage setImage: icone_ressource];
        [UIView animateWithDuration:0.6 animations:^{
            [cellImage setAlpha:1.0];
        }];
    }
    else{
        [cellImage setImage: icone_ressource];
    }
    // Ajout du drapeau de la langue de la ressource
    NSString * linkImage = [@"icone_drapeau_rond_" stringByAppendingString: [LanguageManager codeLangueWithid:[currentRessource.id_langue intValue]]];
    linkImage = [linkImage stringByAppendingString:@".png"];
    UIImage * fImage = [UIImage imageNamed:linkImage];
    UIImageView * flagImage = [[UIImageView alloc] initWithImage:fImage];
    [flagImage setFrame:CGRectMake(42, 42, flagImage.frame.size.width, flagImage.frame.size.height)];
    [cellImage addSubview:flagImage];
    
    UILabel * cellTitle = (UILabel *)[cell viewWithTag:2];
    
    [cellTitle setText: currentRessource.nom_court];
    
    //Affichage du nombre de messages en badge
    UILabel * badge_number = (UILabel*) [cell viewWithTag:4];
    UIView * badge_bg = (UIView*) [cell viewWithTag:3];
    [badge_bg.layer setCornerRadius:12];
    NSNumber * nbre_messages = [_messagesNumberForRessource objectForKey:[currentRessource.id_ressource stringValue]];
    
    if (nbre_messages) {
        
        [badge_number setText:[nbre_messages stringValue]];
        
        //Si le texte est différent, on l'affiche avec une animation
        if(![[nbre_messages stringValue] isEqualToString:badge_number.text]){
            [badge_bg setAlpha:0.0];
            [badge_bg setHidden:NO];
            [UIView animateWithDuration:0.6 animations:^{
                [badge_bg setAlpha:1.0];
            }];
        }
        //Sinon, non
        else{
            [badge_bg setHidden:NO];
        }
        
    }
    else{
        [badge_bg setHidden:YES];
        //        [badge_number setHidden:YES];
    }
    

    return cell;
}

#pragma mark - Helpers
- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqual:data2];
}

-(UIImage *)blurredImageFromImage:(UIImage *) image
{
    
    // Now apply the blur effect using Apple's UIImageEffect category
    //    UIImage *blurredSnapshotImage = [snapshotImage applyTintEffectWithColor:[UIColor orangeColor]];
    UIImage *blurredSnapshotImage = [image applyLightEffect];
    
    // Or apply any other effects available in "UIImage+ImageEffects.h"
    // UIImage *blurredSnapshotImage = [snapshotImage applyDarkEffect];
    // UIImage *blurredSnapshotImage = [snapshotImage applyExtraLightEffect];
    
    // Be nice and clean your mess up
    UIGraphicsEndImageContext();
    
    return blurredSnapshotImage;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"storeCellSegue"])
    {
        int row = [[[self.collectionView indexPathsForSelectedItems] objectAtIndex:0] row];
        int section = [[[self.collectionView indexPathsForSelectedItems] objectAtIndex:0] section];
        
        TelechargementVC * telechargementVC = segue.destinationViewController;
        
        if(modeRessource == ModeRessourceBrowse){
            if([self.displaySelector selectedSegmentIndex] == 0)
                [telechargementVC setCurrentRessource:[[etablissementData objectAtIndex:section][@"data"]  objectAtIndex:row]];
            else
                [telechargementVC setCurrentRessource:[[themeData objectAtIndex:section][@"data"]  objectAtIndex:row]];
        }
        else{
            [telechargementVC setCurrentRessource:latestData[row]];
        }
        
    }
    
    
}
@end
