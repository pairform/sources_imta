//
//  EditerCompteController.h
//  profil
//
//  Created by admin on 29/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMPickerViewController.h"

@interface EditerCompteController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, RMPickerViewControllerDelegate>

@property (strong, nonatomic) NSString * email;
@property (strong, nonatomic) NSString * etablissement;
@property (nonatomic) int idLangue;
@property (strong, nonatomic) UIImage * avatar;

-(IBAction)modifierAvatar:(id)sender;
- (IBAction)modifierLanguePrincipale:(id)sender;
- (IBAction)modifierAutresLangues:(id)sender;

@end
