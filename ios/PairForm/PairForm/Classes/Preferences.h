//
//  Preferences.h
//  SupCast
//
//  Created by Maen Juganaikloo on 26/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMPickerViewController.h"

@interface Preferences : UIViewController <RMPickerViewControllerDelegate>

@end
