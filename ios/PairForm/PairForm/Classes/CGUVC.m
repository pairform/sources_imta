//
//  CGUVC.m
//  PairForm
//
//  Created by VESSEREAU on 16/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "CGUVC.h"
#import "LanguageManager.h"

@interface CGUVC ()

@end

@implementation CGUVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    NSURL * mediaUrl = [NSURL URLWithString:[kURLCGU_mobile stringByAppendingString:[LanguageManager getCodeMainLanguage]]];
    NSURLRequest * mediaUrlRequest = [[NSURLRequest alloc] initWithURL:mediaUrl];
    
    [NSURLCache setSharedURLCache:[NSURLCache sharedURLCache]];
    
    [self.webView loadRequest:mediaUrlRequest];
    
//    [self setContent];
//    self.textCGU.editable = NO;
//    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
}

- (void) setContent {
    [self setTitle:[LanguageManager get:@"ios_title_cgu"]];
    
    // Mise à jour du texte des informations sur PairForm
    NSString *text = [NSString stringWithFormat:@"%@\n\n\"PairForm\"\n\n", [LanguageManager get:@"cgu_titre"]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_creation_compte"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_reseau_social_apprentissage"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_fonctionnalites_services"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"1. %@\n\n", [LanguageManager get:@"cgu_titre_creation_profil"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_texte_creation_profil"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"2. %@\n\n", [LanguageManager get:@"cgu_titre_msg_ou_post"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_msg_echanger"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_msg_interrogations"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_propos_qualite"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"3. %@\n\n", [LanguageManager get:@"cgu_titre_roles"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_attribuer_role"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_role_implication"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_role_expert"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"4. %@\n\n", [LanguageManager get:@"cgu_titre_responsabilite_utilisateur"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_fondamental"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_texte_legal"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_complement"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_attention_responsabilite"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"5. %@\n\n", [LanguageManager get:@"cgu_titre_responsabilite_intellectuelle"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_fondamental_msg"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_citation"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_conseil_msg"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_attention_msg"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"6. %@\n\n", [LanguageManager get:@"cgu_titre_responsabilite_utilisateur_msg"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_esprit_critique"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_seul_responsable"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_responsabilite_gestionnaires"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"7. %@\n\n", [LanguageManager get:@"cgu_titre_droit_reponse"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_texte_droit_reponse"]]];
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"8. %@\n\n", [LanguageManager get:@"cgu_titre_resiliation"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"%@\n\n", [LanguageManager get:@"cgu_attention_suspendre_compte"]]];
    text = [text stringByAppendingString:[LanguageManager get:@"cgu_attention_suspendre_compte_2"]];
    
    self.textCGU.text = text;
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
