//
//  MenuVC.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuVC.h"
#import "JASidePanelController.h"
#import "MessageController.h"
#import "UtilsCore.h"
#import "iRate.h"
#import "HelpVC.h"
#import "RootVC.h"
#import "Reachability.h"
#import "NotificationTVC.h"
#import "ReachabilityManager.h"

@interface MenuVC ()

@property (nonatomic, weak) IBOutlet UIView * userBar;
@property (nonatomic, weak) IBOutlet UIImageView * logo;


@end

@implementation MenuVC

@synthesize menuItems;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    [super viewDidLoad];
    
    
    //Ajout d'un pull to refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[LanguageManager get:@"button_recuperer_nouveaux_messages"] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    self.refreshControl.tintColor = [UIColor colorWithRed:255/255.0f green:120/255.0f blue:29/255.0f alpha:1.0f];
    [self.refreshControl addTarget:[UtilsCore class] action:@selector(rafraichirMessages) forControlEvents:UIControlEventValueChanged];
    self.refreshControl.tag = 100;
    
    [self.tableView insertSubview:self.refreshControl atIndex:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateConnectButton:) name:@"userLoggedIn" object:nil];
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(updateConnectButton:)
//                                                 name:@"rightPanelOpen" object:nil];
//    
    
    [self setCenterPanel:[(JASidePanelController*)[self parentViewController] centerPanel]];
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"Background_pattern"]]];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    [[self tableView] setSeparatorColor:[UIColor colorWithWhite:0.3 alpha:1]];
    
//    menuItems = [[NSMutableArray alloc] initWithCapacity:8];
//    
//    for (int i=0; i<8; i++) {
//        [menuItems addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
//                              [NSString stringWithFormat:@"Item %d", i], @"title",
//                              [UIImage imageNamed:@"Sealred.png"], @"image", nil]];
//    }
    [self setContent];
    [self initUserBar];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateFromNotification:)
                                                 name:@"new_user_notification" object:nil];
    //Notification de connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserBar:) name:@"userLoggedIn" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserNotificationsCount:) name:@"notificationsUpdated" object:nil];
    
    //Si l'utilisateur était connecté, mais n'a pas de réseau, et qu'il ne souhaite pas retenir sa session sur son mobile
    if ([UIViewController isConnected] && ![[ReachabilityManager sharedManager].reachability isReachable] && [[UIViewController getPreference:@"rememberMe"] isEqualToNumber:@1]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:self userInfo:@{@"status": @"connexion", @"username" : [UIViewController getPermamentValue:@"username"]}];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateConnectButton:nil];
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesActualises"
                                               object:nil];
    
    // On intercepte la notification des messages actualisés
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMessages)
                                                 name:@"messagesErreur"
                                               object:nil];

    

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesActualises"
                                                  object:nil];
    //On arrête l'interception
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"messagesErreur"
                                                  object:nil];
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

- (void) setContent {
    // Traduction des termes suivant la langue de l'application
    self.labelRessources.text = [LanguageManager get:@"title_ressources"];
    self.labelMesRessources.text = [LanguageManager get:@"button_accueil"];
    self.labelRessourceStore.text = [LanguageManager get:@"button_ajouter_ressource"];
    self.labelUtilisateurs.text = [LanguageManager get:@"title_utilisateurs"];
    self.labelProfil.text = [LanguageManager get:@"button_profil"];
    self.labelRechercheUtilisateur.text = [LanguageManager get:@"button_chercher_utilisateur"];
    self.labelMessages.text = [LanguageManager get:@"title_messages"];
    self.labelTousMessages.text = [LanguageManager get:@"button_tout_les_messages"];
    self.labelAutres.text = [LanguageManager get:@"title_autres"];
    self.labelPreferences.text = [LanguageManager get:@"button_preferences"];
    self.labelInformations.text = [LanguageManager get:@"button_infos"];
    self.labelAide.text = [LanguageManager get:@"button_aide"];
    self.labelCGU.text = [LanguageManager get:@"button_cgu_2"];
    
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[LanguageManager get:@"button_recuperer_nouveaux_messages"] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    [self updateConnectButton:nil];
}

-(void)refreshMessages{
    [self setContent];
    [(UIRefreshControl*)[self.tableView viewWithTag:100] endRefreshing];
}

-(void) updateConnectButton:(NSNotification*)aNotification{
    
    //Si l'utilisateur vient de se connecter, on passe par une notification
    if ([aNotification isKindOfClass:NSClassFromString(@"NSNotification")]) {
        NSString * status = [[aNotification userInfo] objectForKey:@"status"];
        if([status isEqualToString:@"connexion"])
            [self.connexionButton setText:[LanguageManager get:@"button_deconnexion"]];
        else if([status isEqualToString:@"deconnexion"])
            [self.connexionButton setText:[LanguageManager get:@"button_connexion"]];
        
    }
    //Si c'est la première fois que le menu s'affiche, il faut vérifier la connexion
    else
    {
        if([UIViewController isConnected])
            [self.connexionButton setText:[LanguageManager get:@"button_deconnexion"]];
        else
            [self.connexionButton setText:[LanguageManager get:@"button_connexion"]];
    }
    
    [self.connexionButton setNeedsLayout];
    [self.connexionButton layoutIfNeeded];
    
}

-(IBAction)feedback:(id)sender{
//    [TestFlight openFeedbackView];
    [[iRate sharedInstance] promptIfNetworkAvailable];
}
-(IBAction)connexion:(id)sender{

    //On cache le menu lorsqu'on clique
    [(JASidePanelController*)[self parentViewController] toggleRightPanel:sender];

    if([UIViewController isConnected])
    {
        [self.centerPanel logOut];
    }
    else
    {
        [self.centerPanel checkForLoggedInUser];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Segue override

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    //On cache le menu lorsqu'on clique
    [(JASidePanelController*)[self parentViewController] toggleRightPanel:sender];
    if([[segue identifier] isEqualToString:@"derniersMessagesSegue"]){
        MessageController * messageController = segue.destinationViewController;
        messageController.modeMessage = ModeMessageTransversal;
    }
    if([[segue identifier] isEqualToString:@"monProfilSegue"]){
//        ProfilController * view = segue.destinationViewController;
//        view.id_utilisateur = [UIViewController getSessionValueForKey:@"id_utilisateur"];
    }
    if([[segue identifier] isEqualToString:@"AideSegue"]){
        NSString * screenID = [[(UINavigationController*)self.sidePanelController.centerPanel topViewController] restorationIdentifier];
        HelpVC * helpVC = segue.destinationViewController;
        
        [helpVC setScreenStoryboardID:screenID];
        [helpVC setCurrentIndex:0];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"monProfilSegue"]){
        if ([self checkForLoggedInUser])
            return YES;
        else
            return NO;
    }
    if ( [identifier isEqualToString:@"RechercheProfilSegue"]){
//        return [WService isReachable];
        return  YES;
    }
    return YES;
}

#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    // Return the number of sections.
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//
//    // Return the number of rows in the section.
//    return [menuItems count];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"MenuCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    if([tableView viewWithTag:11] == cell)
//    {
//        NSDictionary * currentMenuItem = [menuItems objectAtIndex:indexPath.row];
//        
//        UIImageView * icon = (UIImageView *)[cell viewWithTag:0];
//        [icon setImage:[currentMenuItem objectForKey:@"image"]];
//        
//        UILabel * title = (UILabel *)[cell viewWithTag:1];
//        if([self checkForLoggedInUser])
//            [title setText:@"Deconnexion"];
//        else
//            [title setText:@"Connexion"];
//
//    }
//    
//    return cell;
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
 
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     }
*/

#pragma mark -
#pragma mark Notifications

-(void)initUserBar{
    if([UIViewController isConnected]){
        [self updateUserBarWithStatus:@"connexion" andUsername:[UIViewController getPermamentValue:@"username"]];
    }
}
-(void)updateUserBar:(NSNotification*)aNotification{
    //Si l'utilisateur vient de se connecter, on passe par une notification
    if ([aNotification isKindOfClass:NSClassFromString(@"NSNotification")]) {
        [self updateUserBarWithStatus:[aNotification userInfo][@"status"] andUsername:[aNotification userInfo][@"username"]];
    }
}
-(void)updateUserNotificationsCount:(NSNotification*)aNotification{
    //Si l'utilisateur vient de se connecter, on passe par une notification
    if ([aNotification isKindOfClass:NSClassFromString(@"NSNotification")]) {
        [self initNotificationsBarButtons];
    }
}


-(void)updateUserBarWithStatus:(NSString*)status andUsername:(NSString*)user_name{
    //Initialisation de la barre d'utilisateur
    NSArray * userBarArray = [[NSBundle mainBundle] loadNibNamed:@"UserBar" owner:self options:nil];
    UIView * userBarView;
    
    if([status isEqualToString:@"connexion"]){
        userBarView = userBarArray[2];
        Utilisateur * utilisateur = [UtilsCore getUtilisateur:[UIViewController getSessionValueForKey:@"id_utilisateur"]];
        
        UIImage * avatarImg;
        NSString * username_str;
        
        if (utilisateur) {
            avatarImg = [[utilisateur getImage] thumbnailImage:34 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
            username_str = [utilisateur owner_username];
            
        }
        else {
            NSURL * avatarURL = [NSURL URLWithString:[UIViewController getSessionValueForKey:@"avatar_url"]];
            avatarImg = [[UIImage imageWithData: [NSData dataWithContentsOfURL: avatarURL]] thumbnailImage:34 transparentBorder:1 cornerRadius:6 interpolationQuality:kCGInterpolationHigh];
            
            username_str = username_str;
        }
        
        UIImageView * avatar = (UIImageView*)[userBarView viewWithTag:1];
        [avatar setImage:avatarImg];
        UITextField * username = (UITextField*)[userBarView viewWithTag:2];
        [username setText:username_str];
        UITextField * points = (UITextField*)[userBarView viewWithTag:3];
        [points setHidden:YES];
        
        UIButton * user_score_button = (UIButton*)[userBarView viewWithTag:4];
        [user_score_button addTarget:self action:@selector(displayScoreChanges:) forControlEvents:UIControlEventTouchUpInside];
        UIButton * user_notification_button = (UIButton*)[userBarView viewWithTag:5];
        [user_notification_button addTarget:self action:@selector(displayNotifications:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.userBar addSubview:userBarView];
        [self initNotificationsBarButtons];
        [self.logo setHidden:YES];
        [self.userBar setHidden:NO];
    }
    else if([status isEqualToString:@"deconnexion"]){
        [self.userBar setHidden:YES];
        [self.logo setHidden:NO];
        [self.userBar.subviews[0] removeFromSuperview];
    }

}
#pragma mark - Popovers

- (void)displayNotifications:(UIButton*)sender
{
    NotificationTVC * content = [[NotificationTVC alloc] initWithNotificationType:@"un"];
    //Récupération du navigationController
    UINavigationController * globalNavigationController = (UINavigationController*)[(JASidePanelController *)[self parentViewController] centerPanel];
    
    //On le push dans la navigation. Du coup, le RootVC est toujours en premier, mais on ne peut pas y accéder (a part par les boutons du menu).
    [globalNavigationController pushViewController:content animated:true];
    
    [self removeBarBadgeOfType:@"un"];
    [(JASidePanelController*)[self parentViewController] toggleRightPanel:nil];
    
}

- (void)displayScoreChanges:(UIButton*)sender
{
    NotificationTVC * content = [[NotificationTVC alloc] initWithNotificationType:@"us"];
    
    //Récupération du navigationController
    UINavigationController * globalNavigationController = (UINavigationController*)[(JASidePanelController *)[self parentViewController] centerPanel];
    
    //On le push dans la navigation. Du coup, le RootVC est toujours en premier, mais on ne peut pas y accéder (a part par les boutons du menu).
    [globalNavigationController pushViewController:content animated:true];

    [self removeBarBadgeOfType:@"us"];
    [(JASidePanelController*)[self parentViewController] toggleRightPanel:nil];
}


-(void)updateFromNotification:(NSNotification*)theNotification
{
    if ([UIViewController isConnected]) {
        NSDictionary * notif_options = [theNotification userInfo];
        
        if ([notif_options[@"t"] isEqualToString:@"us"]) {
            
            //Référence des points
            NSDictionary * PTS_def = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PTS_def" ofType:@"plist"]];
            NSString * points = PTS_def[notif_options[@"st"]];

            
            if (!points) {
                points = notif_options[@"st"];
                
                if (!points)
                    points = @"";
            }
            
            //Récup du bouton
            UIButton * user_score_button = (UIButton*)[self.userBar viewWithTag:4];
            
            if ([user_score_button viewWithTag:1000]) {
                UIView * badgeView = [user_score_button viewWithTag:1000];
                UILabel * repLabel = (UILabel*)[badgeView viewWithTag:1001];
                int old_points = [repLabel.text intValue];
                int new_points = [points intValue] + old_points;
                
                if(new_points < 0){
                    [repLabel setText:[NSString stringWithFormat:@"%d", new_points]];
                    [badgeView setBackgroundColor:[UIColor colorWithRed:0.992 green:0.847 blue:0 alpha:0.9]];
                }
                else{
                    [repLabel setText:[NSString stringWithFormat:@"+%d", new_points]];
                    [badgeView setBackgroundColor:[UIColor colorWithRed:0.275 green:0.71 blue:0.145 alpha:0.9]];
                }
            }
            else{
                [user_score_button  addSubview: [self makeBarBadgeWithNumber:[points intValue] andType:@"us"]];
            }
            
            //Update du décompte de point en haut à droite
            NSNumber * concerned_id_capsule = [NSNumber numberWithInt:[notif_options[@"content"] intValue]];
            
            //On ajoute le score à l'user en cours
            [UIViewController addScore:[points intValue] toRankOfCapsule: concerned_id_capsule];
            
            //Et on le montre, si besoin il y a
            if ([self arePointsForRessourceVisibles]) {
                [self showNumberOfPointsForCapsule:concerned_id_capsule];
            }
        }
        else if ([notif_options[@"t"] isEqualToString:@"un"]) {
            
            UIButton * user_notification_button = (UIButton*)[self.userBar viewWithTag:5];
            if ([user_notification_button  viewWithTag:1000]) {
                UIView * badgeView = [user_notification_button viewWithTag:1000];
                UILabel * repLabel = (UILabel*)[badgeView viewWithTag:1001];
                int old_count = [repLabel.text intValue];
                int new_count = old_count +1;
                
                [repLabel setText:[NSString stringWithFormat:@"%d", new_count]];
            }
            else{
                [user_notification_button addSubview: [self makeBarBadgeWithNumber:1 andType:@"un"]];
            }
        }
        
        
    }
}
-(BOOL) arePointsForRessourceVisibles{
    UITextField * textField_points = (UITextField*)[self.userBar viewWithTag:3];
    
    return ![textField_points isHidden];
}
-(void) showNumberOfPointsForCapsule:(NSNumber*)id_capsule{
    if ([UIViewController isConnected]) {
        UITextField * textField_points = (UITextField*)[self.userBar viewWithTag:3];
        
        NSString * points = [[UIViewController getRankForCapsule:id_capsule] objectForKey:@"score"];
        [textField_points setHidden:NO];
        [textField_points setText:[NSString stringWithFormat:@"%@ pts", points]];
    }
}

-(void) hideNumberOfPointsForCapsule:(NSNumber*)id_capsule{
    if ([UIViewController isConnected]) {
        UITextField * textField_points = (UITextField*)[self.userBar viewWithTag:3];
        [textField_points setHidden:YES];
        [textField_points setText:@"... pts"];
    }
}
-(void) showNumberOfPointsForRessource:(NSNumber*)id_ressource{
    if ([UIViewController isConnected]) {
        UITextField * textField_points = (UITextField*)[self.userBar viewWithTag:3];
        
        NSString * points = [[UIViewController getRankForRessource:id_ressource] objectForKey:@"score"];
        if (points) {
            [textField_points setHidden:NO];
            [textField_points setText:[NSString stringWithFormat:@"%@ pts", points]];
        }
        
    }
}

-(void) hideNumberOfPointsForRessource:(NSNumber*)id_ressource{
    if ([UIViewController isConnected]) {
        UITextField * textField_points = (UITextField*)[self.userBar viewWithTag:3];
        [textField_points setHidden:YES];
        [textField_points setText:@"... pts"];
    }
}
-(UIView*) makeBarBadgeWithNumber:(int)number andType:(NSString*)type{
    UIView * badgeView = [[UIView alloc] initWithFrame:CGRectMake(4, 10, 32, 20)];
    badgeView.layer.cornerRadius = 8;
    badgeView.layer.masksToBounds = YES;
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 32, 20)];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTag:1001];
    
    if ([type isEqualToString:@"us"]) {
        
        if(number < 0){
            [label setText:[NSString stringWithFormat:@"%d", number]];
            [badgeView setBackgroundColor:[UIColor colorWithRed:0.992 green:0.847 blue:0 alpha:0.9]];
        }
        else{
            [label setText:[NSString stringWithFormat:@"+%d", number]];
            [badgeView setBackgroundColor:[UIColor colorWithRed:0.275 green:0.71 blue:0.145 alpha:0.9]];
        }
        
        
    } else if ([type isEqualToString:@"un"]) {
        [label setText:[NSString stringWithFormat:@"%d", number]];
        [badgeView setBackgroundColor:[UIColor colorWithRed:0.286 green:0.694 blue:0.851 alpha:0.9]];
    }
    
    [badgeView addSubview:label];
    [badgeView setTag:1000];
    //Pour l'animer par la suite, on le cache
    [badgeView setAlpha:0];
    [UIView animateWithDuration:1.0 animations:^{
        [badgeView setAlpha:1];
    }];
    [badgeView setUserInteractionEnabled:NO];
    //On place la vue
    return badgeView;
}

-(void) removeBarBadgeOfType:(NSString*)type{
    UIButton * user_bar_button;
    
    if ([type isEqualToString:@"us"])
        //Récup du bouton
        user_bar_button = (UIButton*)[self.userBar viewWithTag:4];
    else
        user_bar_button = (UIButton*)[self.userBar viewWithTag:5];
    
    //Reset des notifications
    [UtilsCore setNotificationsSeenForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:type];
    
    UIView * badgeView = [user_bar_button  viewWithTag:1000];
    
    if (badgeView) {
        [badgeView removeFromSuperview];
    }
    
}

-(void) initNotificationsBarButtons{

    //Boutton de réputation
    UIButton * user_score_button = (UIButton*)[self.userBar viewWithTag:4];
    //Récup du total
    int totalRep = [UtilsCore getUnseenNotificationTotalForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:@"us"];
    //On place la réputation totale
    if (totalRep != 0) {
        [user_score_button  addSubview: [self makeBarBadgeWithNumber:totalRep andType:@"us"]];
    }
    
    //Boutton de réputation
    UIButton * user_notif_button = (UIButton*)[self.userBar viewWithTag:5];
    //Récup du total
    int totalNotif = [UtilsCore getUnseenNotificationTotalForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:@"un"];
    //On place la réputation totale
    if (totalNotif != 0) {
        [user_notif_button addSubview: [self makeBarBadgeWithNumber:totalNotif andType:@"un"]];
    }
    
}


@end
