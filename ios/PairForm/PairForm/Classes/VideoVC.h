//
//  VideoVC.h
//  PairForm
//
//  Created by Maen Juganaikloo on 13/01/2016.
//  Copyright © 2016 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoVC : UIViewController

@property (nonatomic, strong) NSData * video;
@end
