//
//  FileManager.m
//  SupCast
//
//  Created by Maen Juganaikloo on 03/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "FileManager.h"

#import "ZipArchive.h"

@implementation FileManager


+(BOOL)unzipFileAtPath:(NSString *) srcPath{
    return [self unzipFileAtPath:srcPath toPath:nil withRessource:nil andCapsule:nil isUpdate:nil];
}

+(BOOL)unzipFileAtPath:(NSString*)srcPath toPath:(NSString*)dstPath withRessource:(Ressource*)ressource andCapsule:(Capsule*)capsule isUpdate:(BOOL)isUpdate{
    DLog(@"pathFichierZip1 is %@",srcPath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:srcPath])
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
                       ^{
                           //Dossier document
                           
                           NSString *ressourceDirectory = [UtilsCore getRessourcesPath];
                           
                           
                           //        NSString * documentZipFile = [documentDirectory stringByAppendingPathComponent:bundleZipFile];
                           
                           //        NSError *error;
                           
                           //        [[NSFileManager defaultManager] moveItemAtPath:srcPath toPath:documentZipFile error:&error];
                           
                           ZipArchive* zipArch = [[ZipArchive alloc] init];
                           if([zipArch UnzipOpenFile:srcPath])
                           {
                               NSString* final_path;
                               //S'il n'y a pas de chemin spécifié
                               if (!dstPath) {
                                   //Nom du zip
                                   NSString *bundleZipFile = [[NSString alloc] initWithString:[srcPath lastPathComponent]];
                                   //Chemin de destination
                                   final_path = [ressourceDirectory stringByAppendingPathComponent:[bundleZipFile stringByDeletingPathExtension]];
                               }
                               else{
                                   final_path = dstPath;
                               }
                               
                               
                               //Si on est dans le cas d'une mise à jour, et que la ressource existe déjà dans le dossier Documents
                               if([[NSFileManager defaultManager] fileExistsAtPath:final_path])
                                   [[NSFileManager defaultManager] removeItemAtPath:final_path error:nil];
                               
                               BOOL ret = [zipArch UnzipFileTo:final_path overWrite:YES];
                               if(ret)
                               {
                                   [self removeCapsuleZipAtPath:srcPath];
                                   
                                   if (![[NSUserDefaults  standardUserDefaults] boolForKey:@"firstLaunchUnzip"]) {
                                       [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"firstLaunchUnzip"];
                                   }
                                   
                                   
                                   [UtilsCore addSkipBackupAttributeToItemAtPath:final_path];
                                   if (ressource) {
//                                       [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadToast" object:ressource];
                                       //Notif à destination de HomeVC pour qu'il update la table
                                       [[NSNotificationCenter defaultCenter] postNotificationName:@"unzipFinished" object:ressource userInfo:@{@"id_capsule" : capsule.id_capsule, @"capsule_nom_court" : capsule.nom_court, @"is_update" : [NSNumber numberWithBool:isUpdate]}];
                                           
                                   }
                                   
                                   
                               }
                               else{
                                   DLog(@"Probleme de désarchivage de la ressource %@",final_path);
                               }
                               
                               [zipArch UnzipCloseFile];
                               
                           }
                           
                       });
    }
    return false;
}

+(BOOL)migrateFromDocumentsDirectoryPath:(NSString*) original_path toApplicationSupportDirectory:(NSString*)final_path{
    NSError * error;
    
    //Double check, pour être sur
    if([[NSFileManager defaultManager] fileExistsAtPath: original_path] && ![[NSFileManager defaultManager] fileExistsAtPath: final_path])
    {
        if([[NSFileManager defaultManager] moveItemAtPath:original_path toPath:final_path error:&error])
        {
            return YES;
        }
        else
        {
            NSLog(@"%@",error);
            
            return NO;
        }
    }
    return NO;
}

+(BOOL)removeCapsuleZipAtPath:(NSString*) zip_path{
    NSError * error;
    
    if([[NSFileManager defaultManager] removeItemAtPath:zip_path error:&error])
    {
        return YES;
    }
    else
    {
        NSLog(@"%@",error);
        return NO;
    }
}
+(BOOL)removeRessourceWithId:(NSNumber*)id_ressource{
    NSString * path_of_ressource = [[UtilsCore getRessourcesPath] stringByAppendingFormat:@"/%@", [id_ressource stringValue]];
    NSFileManager * file_manager = [NSFileManager defaultManager];
    if([file_manager fileExistsAtPath:path_of_ressource]){
        NSError * error;
        
        if([file_manager removeItemAtPath:path_of_ressource error:&error]){
            return YES;
        }
        else{
            NSLog(@"%@",error);
            return NO;
        }
    }
    else{
        return NO;
    }
}

+(BOOL)removeCapsuleWithId:(NSNumber*)id_capsule ofRessource:(NSNumber*)id_ressource{
    NSString * path_of_capsule = [[UtilsCore getRessourcesPath] stringByAppendingFormat:@"/%@/%@", [id_ressource stringValue],[id_capsule stringValue]];
    NSFileManager * file_manager = [NSFileManager defaultManager];
    if([file_manager fileExistsAtPath:path_of_capsule]){
        NSError * error;
        
        if([file_manager removeItemAtPath:path_of_capsule error:&error]){
            return YES;
        }
        else{
            NSLog(@"%@",error);
            return NO;
        }
    }
    else{
        return NO;
    }
}
@end
