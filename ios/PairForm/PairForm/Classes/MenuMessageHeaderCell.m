//
//  MenuMessageHeaderCell.m
//  PairForm
//
//  Created by Maen Juganaikloo on 16/12/2016.
//  Copyright © 2016 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuMessageHeaderCell.h"
#import "LanguageManager.h"
#import "UIButton+VerticalLayout.h"

@implementation MenuMessageHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    //Boutons du menu
    
    [self.bouton_menu_carte setTitle:[LanguageManager get:@"bouton_menu_carte"] forState:UIControlStateNormal];
    [self.bouton_menu_carte centerVerticallyWithImageWidth:30.0f andVerticalPadding:4.0f];
    
    [self.bouton_menu_tri setTitle:[LanguageManager get:@"bouton_menu_tri"] forState:UIControlStateNormal];
    [self.bouton_menu_tri centerVerticallyWithImageWidth:30.0f andVerticalPadding:4.0f];
    
    [self.bouton_menu_recherche setTitle:[LanguageManager get:@"bouton_menu_recherche"] forState:UIControlStateNormal];
    [self.bouton_menu_recherche centerVerticallyWithImageWidth:30.0f andVerticalPadding:4.0f];
    
    [self.bouton_menu_ecrire setTitle:[LanguageManager get:@"bouton_menu_ecrire"] forState:UIControlStateNormal];
    [self.bouton_menu_ecrire centerVerticallyWithImageWidth:30.0f andVerticalPadding:4.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
