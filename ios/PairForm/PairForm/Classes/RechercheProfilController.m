//
//  ViewControllerListe.m
//  profil
//
//  Created by admin on 18/03/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "RechercheProfilController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "ProfilController.h"

@interface RechercheProfilController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *names;
@property (strong, nonatomic) NSMutableArray *images;
@property (strong, nonatomic) NSMutableArray *id_utilisateurs;

@property (strong, nonatomic) NSArray *allUsers;
@property (strong, nonatomic) NSArray *filterResults;
@property NSNumber * id_ressource;


@end



@implementation RechercheProfilController
@synthesize id_ressource;


NSArray *searchResults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Recherche d'un profil"];    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"crossword.png"]]];

    
    if (IS_IPAD)
    {
        [self setStackWidth: 320];
    }
	// Do any additional setup after loading the view.
    
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:@"Recherche Profil"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self setContent];
    
    //Inscription au notification center pour le changement de langue de l'application
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLangueWithNotification:) name:@"changeLangueApp" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self connect];
    // navigation button bar
//    NSMutableArray     *items = [self.navigationItem.rightBarButtonItems mutableCopy] ;
//    if (IS_IPHONE) {
//        if ( items.count == 1){
//            
//            UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
//            [a1 setFrame:CGRectMake(5,6,32,32)];
//            [a1 addTarget:self action:@selector(afficherFiltres) forControlEvents:UIControlEventTouchUpInside];
//            [a1 setImage:[UIImage imageNamed:@"magnifier_64"] forState:UIControlStateNormal];
//            UIBarButtonItem *bFilter = [[UIBarButtonItem alloc] initWithCustomView:a1];
//            [items addObject:bFilter];
//            self.navigationItem.rightBarButtonItems = items;
//        }
//    }
    
}
-(IBAction)afficherFiltres:(id)sender{
    [self afficherFiltres];
}

-(void) setContent {
    self.title = [LanguageManager get:@"ios_title_recherche_utilisateur"];
}

- (void) changeLangueWithNotification:(NSNotification *) notification {
    [self setContent];
}

-(void) afficherFiltres{
    // Filtres : Utilisateur? , ressources , groupes? , tags
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:[LanguageManager get:@"ios_title_filtrer_selon_ressource"] delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil  otherButtonTitles:nil];
    NSArray * ressources = [[UtilsCore getAllRessources] mutableCopy];
    for (Ressource * res in ressources) {
        [popupQuery addButtonWithTitle:res.nom_court];
    }
    [popupQuery addButtonWithTitle:[LanguageManager get:@"ios_label_toutes_les_ressources"]];
    popupQuery.cancelButtonIndex = [popupQuery addButtonWithTitle:[LanguageManager get:@"button_annuler"]];
    [popupQuery showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ( buttonIndex == actionSheet.cancelButtonIndex) return;
    if ( buttonIndex == actionSheet.cancelButtonIndex -1 ) {
        id_ressource = nil;
        [self connect];
        return;
    }
    id_ressource= ((Ressource * )[[UtilsCore getAllRessources] objectAtIndex:buttonIndex]).id_ressource;
     [self connect];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"StickyFilter"];
    UISearchBar * searchBar = (UISearchBar*)[cell viewWithTag:100];
    searchBar.placeholder = [LanguageManager get:@"button_chercher_utilisateur"];
    return [cell contentView];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    DLog(@"");
      return 44;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (IS_IPHONE) {
//        if (tableView == self.searchDisplayController.searchResultsTableView) {
//            return [searchResults count];
//        }
//        else{
//            return _names.count;
//        }
//    }
//    else{
        return [_filterResults count];
//    }
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"myId";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
//    if (IS_IPHONE) {
//        // Si recherche , on affiche les resultats de la recherche
//        if (tableView == self.searchDisplayController.searchResultsTableView) {
//            cell.textLabel.text = [searchResults objectAtIndex:indexPath.row];
//        }
//        else
//        {
//            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
//            
//            [UtilsCore sauvegardeUtilisateur:[_id_utilisateurs objectAtIndex:indexPath.row] name:[_names objectAtIndex:indexPath.row] url:[_images objectAtIndex:indexPath.row]];
//            Utilisateur * utilisateur = [UtilsCore getUtilisateur:[_id_utilisateurs objectAtIndex:indexPath.row]];
//            
//            UIImage * imageModifiee = [utilisateur.getImage thumbnailImage:56 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh];
//            
//            [imageView setImage:imageModifiee];
//            
//            UILabel *lblName = (UILabel *)[cell viewWithTag:200];
//            [lblName setText:utilisateur.owner_username];
//        }
//    }
//    else if(IS_IPAD){
        NSDictionary * user = _filterResults[indexPath.row];
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
        [UtilsCore sauvegardeUtilisateur:user[@"id_utilisateur"] name:user[@"pseudo"] url:user[@"avatar_url"]];
        Utilisateur * utilisateur = [UtilsCore getUtilisateur:user[@"id_utilisateur"]];
        
        UIImage * imageModifiee = [utilisateur.getImage thumbnailImage:56 transparentBorder:1 cornerRadius:4 interpolationQuality:kCGInterpolationHigh];
        
        [imageView setImage:imageModifiee];
        
        UILabel *lblName = (UILabel *)[cell viewWithTag:200];
        [lblName setText:utilisateur.owner_username];
        
//    }

    
    // Configure the cell...
    
    return cell;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"profilSegue"])
    {
        ProfilController *viewC = [segue destinationViewController];

        NSInteger selectedIndex = [[self.tableView indexPathForSelectedRow] row];
        viewC.id_utilisateur = _filterResults[selectedIndex][@"id_utilisateur"];
    }
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( self.mode == modeAjoutCercle){
        NSInteger selectedIndex = [indexPath row];
        [_delegate ajouteMembreDansCercleDepuisRechercheProfil:_filterResults[selectedIndex]];
        if (IS_IPHONE) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }else {
        [self performSegueWithIdentifier: @"profilSegue" sender: self];
    }
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(updateTableViewFrom:) withObject:@[searchBar, searchText] afterDelay:0.3];
    
}
-(void)updateTableViewFrom:(NSArray *)search_components{
    UISearchBar *searchBar = search_components[0];
    NSMutableString * searchText = [search_components[1] mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)searchText, NULL, kCFStringTransformStripCombiningMarks, NO);

//    if (IS_IPAD) {
        DLog(@"Searchbar text change");
        
        if ([searchText isEqualToString:@""]) {
            return [self searchBarCancelButtonClicked:searchBar];
        }
        
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"(pseudo contains[cd] %@)",
                                        searchText];
        
//        _filterResults = [_allUsers filteredArrayUsingPredicate:resultPredicate];
        
        NSArray * newResults = [_allUsers filteredArrayUsingPredicate:resultPredicate];
        
        
        
        //Si on restreint les résultats
        if ([newResults count] < [_filterResults count]) {
            //Indexes des users affichés ne matchant pas le critère
            NSIndexSet *indexes = [_filterResults indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
                NSMutableString * obj_name = [obj[@"pseudo"] mutableCopy];
                CFStringTransform((__bridge CFMutableStringRef)obj_name, NULL, kCFStringTransformStripCombiningMarks, NO);

                NSRange range = [[obj_name lowercaseString] rangeOfString: [searchText lowercaseString]];
                return range.location == NSNotFound;
            }];
            
            //Transformation en tableau d'indexpaths
            NSMutableArray *indexPaths = [NSMutableArray array];
            [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
            }];
            
            //Affectation des nouveaux résultats
            _filterResults = newResults;
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
            //Suppression animée des éléments
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationRight];
            [self.tableView endUpdates];
        }
        //Si on élargit les résultats
        else if([newResults count] > [_filterResults count]){
            //Indexes des users rajoutés, par rapport au résultats précédents
            NSIndexSet *indexes = [newResults indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
                //Retourne true si l'utilisateur n'était pas dans les résultats précédents
                return [_filterResults indexOfObject:obj] == NSNotFound;
            }];
            
            //Transformation en tableau d'indexpaths
            NSMutableArray *indexPaths = [NSMutableArray array];
            [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
            }];
            
            //Affectation des nouveaux résultats
            _filterResults = newResults;
            
            //Suppression animée des éléments
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationLeft];
            [self.tableView endUpdates];
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

        }
        //Si il n'y a ni plus ni moins d'user filtré, on fait rien
//    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _filterResults = _allUsers;
    [self.tableView reloadData];
}



- (void)connect {
    
    //Appel du webservice
    
    NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys: nil];
    if ( id_ressource ){
        [query setObject:id_ressource forKey:@"id_ressource"];
    }
    [WService get:@"utilisateur/liste"
         param: query
      callback: ^(NSDictionary *wsReturn) {
          NSArray *entries = [wsReturn objectForKey:@"profils"];
          if ( [entries count] == 0) return;
          _names = [[NSMutableArray alloc] initWithCapacity:[entries count]];
          _images= [[NSMutableArray alloc] initWithCapacity:[entries count]];
          _id_utilisateurs = [[NSMutableArray alloc] initWithCapacity:[entries count]];
          for (NSDictionary *item in entries)
          {
              
              //Enleve son propre profil.
              //if ( [[item objectForKey:@"id_utilisateur"] integerValue ]== [[UIViewController getSessionValueForKey:@"id_utilisateur"] integerValue]) continue;
              
              [_names addObject:[item objectForKey:@"pseudo"]];
              [_images addObject:[item objectForKey:@"avatar_url"]];
              [_id_utilisateurs addObject:[item objectForKey:@"id_utilisateur"]];
          }
          
          _allUsers = [[NSArray alloc] initWithArray:entries];
          _filterResults = [[NSArray alloc] initWithArray:entries];
          [_tableView reloadData];
      }];
    


}




@end
