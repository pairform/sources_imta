//
//  SectionTVC.h
//  PairForm
//
//  Created by Maen Juganaikloo on 16/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Capsule.h"

typedef enum {
    PFCapsuleLocal,
    PFCapsuleIsDownloading,
    PFCapsuleNeedsUpdate,
    PFCapsuleDistant,
    PFCapsulePrivate
} PFCapsuleState;

@protocol CapsuleCellDelegate <NSObject>

-(void)telechargerCapsule:(Capsule *)capsule isUpdate:(BOOL)isUpdate;
-(void)accederCapsule:(Capsule *)capsule;
-(void)supprimerCapsule:(Capsule*)current_capsule;

@end

@interface CapsuleCell : UITableViewCell

@property (nonatomic, strong) id<CapsuleCellDelegate> delegate;
@property (nonatomic, strong) Capsule * capsule;
@property (nonatomic, weak) IBOutlet UILabel * nom_long;
@property (nonatomic, weak) IBOutlet UILabel * auteurs;
@property (nonatomic, weak) IBOutlet UILabel * licence;
@property (nonatomic, weak) IBOutlet UILabel * created;
@property (nonatomic, weak) IBOutlet UILabel * updated;
@property (nonatomic, weak) IBOutlet UILabel * taille;
@property (nonatomic, weak) IBOutlet UITextView * description_cap;
@property (nonatomic, weak) IBOutlet UIView * badge;
@property (nonatomic, weak) IBOutlet UIButton * action_button;
@property (nonatomic, weak) IBOutlet UIButton * delete_button;
@property (nonatomic, weak) IBOutlet UIProgressView * download_progress_view;
@property (nonatomic, assign) BOOL flagMessagesLus;

@property (nonatomic, assign) PFCapsuleState etat_capsule;

-(IBAction)action_button:(id)sender;
-(void)setEtatCapsule:(PFCapsuleState)etat;
-(void)setCapsuleForCell:(Capsule *)capsule;
-(void)setBadgeNumber:(int)number;
-(void)setProgression:(CGFloat)progress;
@end

