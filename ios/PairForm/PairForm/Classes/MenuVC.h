//
//  MenuVC.h
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfilController.h"

@interface MenuVC : UITableViewController

@property (nonatomic, strong) NSMutableArray * menuItems;
@property (nonatomic, strong) UIViewController * centerPanel;
@property (nonatomic, strong) IBOutlet UILabel * connexionButton;
@property (nonatomic, strong) UIRefreshControl * refreshControl;

@property (weak, nonatomic) IBOutlet UILabel *labelRessources;
@property (weak, nonatomic) IBOutlet UILabel *labelMesRessources;
@property (weak, nonatomic) IBOutlet UILabel *labelRessourceStore;
@property (weak, nonatomic) IBOutlet UILabel *labelUtilisateurs;
@property (weak, nonatomic) IBOutlet UILabel *labelProfil;
@property (weak, nonatomic) IBOutlet UILabel *labelRechercheUtilisateur;
@property (weak, nonatomic) IBOutlet UILabel *labelMessages;
@property (weak, nonatomic) IBOutlet UILabel *labelTousMessages;
@property (weak, nonatomic) IBOutlet UILabel *labelAutres;
@property (weak, nonatomic) IBOutlet UILabel *labelPreferences;
@property (weak, nonatomic) IBOutlet UILabel *labelInformations;
@property (weak, nonatomic) IBOutlet UILabel *labelAide;
@property (weak, nonatomic) IBOutlet UILabel *labelCGU;

-(IBAction)feedback:(id)sender;
-(void) updateConnectButton:(NSNotification*)aNotification;
-(IBAction)connexion:(id)sender;


-(void) showNumberOfPointsForRessource:(NSNumber*)id_ressource;
-(void) hideNumberOfPointsForRessource:(NSNumber*)id_ressource;
-(void) showNumberOfPointsForCapsule:(NSNumber*)id_capsule;
-(void) hideNumberOfPointsForCapsule:(NSNumber*)id_capsule;

@end
