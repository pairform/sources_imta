//
//  CGPlayGround.h
//  CGPlayground
//
//  Created by Maen Juganaikloo on 10/03/14.
//  Copyright (c) 2014 Maen Juganaikloo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpringboardItem: UIView

//@property (nonatomic,assign) CGRect itemRect;
@property (nonatomic,assign) CGRect iconRect;
//@property (nonatomic,assign) CGRect titleRect;
@property (nonatomic, strong) NSString * imageName;
@property (nonatomic, strong) UIImage * itemImage;
@property (nonatomic, strong) NSString * itemName;
@property (nonatomic, strong) UIColor * folderColor;
-(void)displayDownloadingWithTimer:(NSTimer*)timer;
-(void)displayDownloadingWithPercentage:(float)percentage;
-(void)startChillingEffect;
-(void)stopChillingEffect;
-(void)startMergingEffect;
-(void)stopMergingEffect;
-(UIColor*)randomColor;
@end
