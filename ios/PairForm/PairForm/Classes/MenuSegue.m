//
//  MenuSegue.m
//  SupCast
//
//  Created by Maen Juganaikloo on 17/05/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import "MenuSegue.h"
#import "JASidePanelController.h"
#import "RootVC_iPad.h"
#import "SCStackViewController.h"
#import "SCParallaxStackLayouter.h"
#import "SCSlidingStackLayouter.h"
#import "SCStackNavigationStep.h"
#import "MessageController.h"

@implementation MenuSegue

- (void)perform
{
    if (IS_IPHONE) {
        //Récupération du navigationController
        UINavigationController * globalNavigationController = (UINavigationController*)[(JASidePanelController *)[[self sourceViewController] parentViewController] centerPanel];
        
        //Remplacement du tableau direct.
        [globalNavigationController setViewControllers:@[self.destinationViewController] animated:NO];
        
//        [globalNavigationController popToRootViewControllerAnimated:NO];
        //Pour chaque controller stacké dans la navigation
//        for (id Controller in [globalNavigationController viewControllers]) {
            //On le supprime proprement
//            [Controller viewWillDisappear:YES];
//            [Controller removeFromParentViewController];
//        }
        
        //On ne peut pas remplacer le RootViewController du NavigationController une fois celui ci initialisé.
        
        //Feinte :
        //On cache le bouton back du VC à ajouter
//        [[self.destinationViewController navigationItem] setHidesBackButton:YES];
        
        //On le push dans la navigation. Du coup, le RootVC est toujours en premier, mais on ne peut pas y accéder (a part par les boutons du menu).
//        [globalNavigationController pushViewController:self.destinationViewController animated:true];
    }
    else{
        SCStackViewController * stackViewController = [(RootVC_iPad*)[self.sourceViewController parentViewController] stackViewController];
        [self.destinationViewController setStackPosition:SCStackViewControllerPositionLeft];
        [stackViewController popToRootViewControllerFromPosition:SCStackViewControllerPositionLeft animated:YES completion:^{
            if ([self.identifier isEqualToString:@"allMessageSegue"]) {
                [(MessageController*)self.destinationViewController setModeMessage: ModeMessageTransversal];
            }
            if (![self.identifier isEqualToString:@"dashboardSegue"]) {
                [stackViewController pushViewController:self.destinationViewController atPosition:SCStackViewControllerPositionLeft unfold:TRUE animated:TRUE completion:nil];                
            }
            else{
                [stackViewController popToRootViewControllerFromPosition:SCStackViewControllerPositionRight animated:YES completion:nil];
            }


        }];
        
        
    }
    
}
@end
