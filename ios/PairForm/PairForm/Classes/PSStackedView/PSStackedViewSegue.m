//
//  PSStackedViewSegue.m
//  PSStackedView
//
//  Created by Marcel Ball on 12-01-19.
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSStackedViewSegue.h"
#import "PSStackedViewController.h"
#import "UIViewController+PSStackedView.h"

@implementation PSStackedViewSegue

- (void)perform {
    PSStackedViewController* stackController = [self.sourceViewController stackController];
    
    //Si on est dans le cas d'une vue embedded dans une autre par container
    if (!stackController) {
        //On remonte d'un cran pour récuperer le stackController
        stackController = [[self.sourceViewController parentViewController] stackController];
    }
    
    [stackController pushViewController:self.destinationViewController fromViewController:self.sourceViewController animated:YES];
}

@end
