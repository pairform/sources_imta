//
//  NotificationTVC.h
//  PairForm
//
//  Created by Maen Juganaikloo on 30/04/14.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PageVC.h"

@interface NotificationTVC : UITableViewController

@property (nonatomic,strong) NSString * notification_type;
@property (nonatomic,strong) NSMutableArray * notifications;
@property (nonatomic,strong) NSMutableArray * notifications_header_concerned;
@property (nonatomic, weak) RootNavigation_iPad * parent_vc;
-(id)initWithNotificationType:(NSString*)type; // "user_notification" || "user_score"
@end
