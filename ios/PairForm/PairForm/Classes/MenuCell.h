//
//  MenuCell.h
//  PairForm
//
//  Created by Maen Juganaikloo on 13/06/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView * cell_image;
@property (nonatomic, weak) IBOutlet UILabel * cell_label;
@end
