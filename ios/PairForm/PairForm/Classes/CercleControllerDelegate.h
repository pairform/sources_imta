//
//  CercleControllerDelegate.h
//  SupCast
//
//  Created by admin on 06/06/13.
//  Copyright (c) 2013 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CercleControllerDelegate

@optional
/** Action pour editer les utilisateurs dans les cercles.
 */
- (void)actionAPartirDeCercle:(NSNumber*) idCercle nomCercle:(NSString *) nomCercle callback:(void (^)(NSDictionary *, NSNumber*))callback;

@optional
/** Action pour utiliser les cercles lors de la gestion de la visibilité
 */
- (void)actionAPartirDeCercle:(NSNumber*) idCercle nomCercle:(NSString *) nomCercle;
@end
