//
//  Utilisateur.h
//  PairForm
//
//  Created by Maen Juganaikloo on 18/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Message;

@interface Utilisateur : NSManagedObject

@property (nonatomic, retain) NSNumber * id_utilisateur;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * owner_username;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSSet *messages;
-(UIImage*) getImage ;
@end

@interface Utilisateur (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)values;
- (void)removeMessages:(NSSet *)values;

@end
