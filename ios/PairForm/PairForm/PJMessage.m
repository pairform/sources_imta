//
//  PJMessage.m
//  PairForm
//
//  Created by vincent on 17/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "PJMessage.h"
#import "Message.h"


@implementation PJMessage

@dynamic extension;
@dynamic id_message;
@dynamic nom_original;
@dynamic nom_serveur;
@dynamic position;
@dynamic taille;
@dynamic data;
@dynamic thumbnail;
@dynamic message;

@end
