//
//  Selecteur+CoreDataProperties.m
//  PairForm
//
//  Created by Maen Juganaikloo on 19/11/2015.
//  Copyright © 2015 Ecole des Mines de Nantes. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Selecteur+CoreDataProperties.h"

@implementation Selecteur (CoreDataProperties)

@dynamic id_selecteur;
@dynamic nom_selecteur;
@dynamic capsule;

@end
