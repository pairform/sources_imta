//
//  Message.h
//  PairForm
//
//  Created by Maen Juganaikloo on 18/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Capsule, Utilisateur, PJMessage;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSString * contenu;
@property (nonatomic, retain) NSNumber * date_creation;
@property (nonatomic, retain) NSNumber * date_modification;
@property (nonatomic, retain) NSNumber * defi_valide;
@property (nonatomic, retain) NSNumber * est_defi;
@property (nonatomic, retain) NSNumber * est_lu;
@property (nonatomic, retain) NSNumber * id_auteur;
@property (nonatomic, retain) NSNumber * id_capsule;
@property (nonatomic, retain) NSNumber * id_langue;
@property (nonatomic, retain) NSNumber * id_message;
@property (nonatomic, retain) NSNumber * id_message_parent;
@property (nonatomic, retain) NSString * id_role_auteur;
@property (nonatomic, retain) NSNumber * geo_latitude;
@property (nonatomic, retain) NSNumber * geo_longitude;
@property (nonatomic, retain) NSString * medaille;
@property (nonatomic, retain) NSString * nom_page;
@property (nonatomic, retain) NSString * nom_tag;
@property (nonatomic, retain) NSData * noms_tags_auteurs;
@property (nonatomic, retain) NSNumber * num_occurence;
@property (nonatomic, retain) NSString * role_auteur;
@property (nonatomic, retain) NSNumber * somme_votes;
@property (nonatomic, retain) NSNumber * supprime_par;
@property (nonatomic, retain) NSData * tags;
@property (nonatomic, retain) NSNumber * utilisateur_a_vote;
@property (nonatomic, retain) Utilisateur *utilisateur;
@property (nonatomic, retain) Capsule *capsule;
@property (nonatomic, retain) NSSet *pieceJointes;

-(NSArray*) getTags ;
-(NSArray*) getTagsNormalized ;
-(NSArray*) getTagsAuteurs ;

@end

@interface Message (CoreDataGeneratedAccessors)

- (void)addPieceJointesObject:(PJMessage *)value;
- (void)removePieceJointesObject:(PJMessage *)value;
- (void)addPieceJointes:(NSSet *)values;
- (void)removePieceJointes:(NSSet *)values;

@end
