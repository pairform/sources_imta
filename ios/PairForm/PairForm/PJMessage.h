//
//  PJMessage.h
//  PairForm
//
//  Created by vincent on 17/06/2015.
//  Copyright (c) 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Message;

@interface PJMessage : NSManagedObject

@property (nonatomic, retain) NSString * extension;
@property (nonatomic, retain) NSNumber * id_message;
@property (nonatomic, retain) NSString * nom_original;
@property (nonatomic, retain) NSString * nom_serveur;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSNumber * taille;
@property (nonatomic, retain) NSData * data;
@property (nonatomic, retain) NSData *thumbnail;
@property (nonatomic, retain) Message *message;

@end
