//
//  Utilisateur.m
//  PairForm
//
//  Created by Maen Juganaikloo on 18/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "Utilisateur.h"
#import "Message.h"


@implementation Utilisateur

@dynamic id_utilisateur;
@dynamic image;
@dynamic owner_username;
@dynamic url;
@dynamic messages;

-(UIImage*) getImage {
    if (self.image) {
       return [NSKeyedUnarchiver unarchiveObjectWithData:[self image]];
    }
    return [UIImage imageNamed:@"man_288"];
}
@end
