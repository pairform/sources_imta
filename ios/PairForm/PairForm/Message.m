//
//  Message.m
//  PairForm
//
//  Created by Maen Juganaikloo on 18/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "Message.h"
#import "Capsule.h"
#import "Utilisateur.h"


@implementation Message

@dynamic contenu;
@dynamic date_creation;
@dynamic date_modification;
@dynamic defi_valide;
@dynamic est_defi;
@dynamic est_lu;
@dynamic geo_latitude;
@dynamic geo_longitude;
@dynamic id_auteur;
@dynamic id_capsule;
@dynamic id_langue;
@dynamic id_message;
@dynamic id_message_parent;
@dynamic id_role_auteur;
@dynamic medaille;
@dynamic nom_page;
@dynamic nom_tag;
@dynamic noms_tags_auteurs;
@dynamic num_occurence;
@dynamic role_auteur;
@dynamic somme_votes;
@dynamic supprime_par;
@dynamic tags;
@dynamic utilisateur_a_vote;
@dynamic utilisateur;
@dynamic capsule;
@dynamic pieceJointes;



-(NSArray*) getTags {
    if ([self tags]) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:[self tags]];
    }
    else{
        return @[];
    }
    
}
-(NSArray*) getTagsNormalized {
    NSMutableArray * array = [[NSMutableArray alloc]init] ;
    for (NSString * st in [NSKeyedUnarchiver unarchiveObjectWithData:[self tags]]) {
        [array addObject:[[st stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]] lowercaseString]];
    }
    return [NSArray arrayWithArray:array];
}

-(NSArray*) getTagsAuteurs {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[self noms_tags_auteurs]];
}
@end
