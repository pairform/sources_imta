//
//  Ressource.h
//  PairForm
//
//  Created by Maen Juganaikloo on 18/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Capsule;

@interface Ressource : NSManagedObject

@property (nonatomic, retain) NSString * cree_par;
@property (nonatomic, retain) NSDate * date_creation;
@property (nonatomic, retain) NSDate * date_edition;
@property (nonatomic, retain) NSString * description_pf;
@property (nonatomic, retain) NSString * espace_nom_court;
@property (nonatomic, retain) NSData * icone;
@property (nonatomic, retain) NSData * icone_etablissement;
@property (nonatomic, retain) NSNumber * id_langue;
@property (nonatomic, retain) NSNumber * id_ressource;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSString * licence;
@property (nonatomic, retain) NSString * nom_court;
@property (nonatomic, retain) NSString * nom_long;
@property (nonatomic, retain) NSString * theme;
@property (nonatomic, retain) NSString * url_logo;
@property (nonatomic, retain) NSSet *capsules;


//iVar pour contenir les capsules

-(UIImage*) getIcone;
-(UIImage*) getIconeEtablissement;


@end

@interface Ressource (CoreDataGeneratedAccessors)

- (void)addCapsulesObject:(Capsule *)value;
- (void)removeCapsulesObject:(Capsule *)value;
- (void)addCapsules:(NSSet *)values;
- (void)removeCapsules:(NSSet *)values;

@end
