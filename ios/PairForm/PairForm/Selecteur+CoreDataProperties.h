//
//  Selecteur+CoreDataProperties.h
//  PairForm
//
//  Created by Maen Juganaikloo on 19/11/2015.
//  Copyright © 2015 Ecole des Mines de Nantes. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Selecteur.h"

NS_ASSUME_NONNULL_BEGIN

@interface Selecteur (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id_selecteur;
@property (nullable, nonatomic, retain) NSString *nom_selecteur;
@property (nullable, nonatomic, retain) Capsule *capsule;

@end

NS_ASSUME_NONNULL_END
