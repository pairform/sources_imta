//
//  Notification.m
//  PairForm
//
//  Created by Maen Juganaikloo on 08/10/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "Notification.h"


@implementation Notification

@dynamic contenu;
@dynamic date_creation;
@dynamic id_message;
@dynamic id_notification;
@dynamic id_utilisateur;
@dynamic illustration;
@dynamic date_vue;
@dynamic type_points;
@dynamic points;
@dynamic sous_type;
@dynamic titre;
@dynamic total_count;
@dynamic type;

@end
