//
//  MigrationFromV2toV3Policy.m
//  PairForm
//
//  Created by Maen Juganaikloo on 19/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import "MigrationFromV2toV3Policy.h"

@implementation MigrationFromV2toV3Policy

- (BOOL)beginEntityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
    //[manager fetchRequestForSourceEntityNamed:@"Ressource" predicateString:@"TRUEPREDICATE"]
    return [super beginEntityMapping:mapping manager:manager error:error];
}

-(BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError *__autoreleasing *)error{

    return [super createDestinationInstancesForSourceInstance:sInstance entityMapping:mapping manager:manager error:error];
}
-(BOOL)createRelationshipsForDestinationInstance:(NSManagedObject *)dInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError *__autoreleasing *)error{
    
//    NSError * err;
//    NSArray * test = [manager.destinationContext executeFetchRequest:[[NSFetchRequest alloc] initWithEntityName:@"Ressource"] error:&err];
    return [super createRelationshipsForDestinationInstance:dInstance entityMapping:mapping manager:manager error:error];
}
@end
