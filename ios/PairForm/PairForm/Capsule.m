//
//  Capsule.m
//  PairForm
//
//  Created by Maen Juganaikloo on 19/11/2015.
//  Copyright © 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import "Capsule.h"
#import "Message.h"
#import "Ressource.h"
#import "Selecteur.h"

@implementation Capsule

// Insert code here to add functionality to your managed object subclass

@synthesize download_progression;
@synthesize isUpdate;


-(NSString*) getPath{
    
    NSString *ressourceDirectory = [UtilsCore getRessourcesPath];
    NSString * path = [NSMutableString stringWithFormat:@"%@/%@/%@",ressourceDirectory,[self id_ressource], [self id_capsule]];
    
    
    //Si la capsule n'a pas encore de format (importée d'un vieux schéma), ou que c'est du scenari
    if (!self.id_format || [self.id_format isEqualToNumber:kFormat_Scenari]) {
        //On essaie de voir si le dossier 'co' existe
        BOOL folderCoExists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/co",path]];
        
        //S'il n'existe pas
        if (!folderCoExists) {
            //On essaie d'aller chercher dans le dossier mob/co/outline.xml
            path = [NSString stringWithFormat:@"%@/mob",path];
            //Et normalement, c'est cool.
        }
    }
    //Sinon, si c'est du PairForm, on s'attend à ce que ce soit là
    else{
        BOOL outlineExists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/outline.xml",path]];
        
        //S'il n'existe pas
        if (!outlineExists) {
            //On essaie d'aller chercher dans le dossier mob/co/outline.xml
            path = [NSString stringWithFormat:@"%@/mob",path];
            //Et normalement, c'est cool.
        }
    }
    
    
    return path;
}

@end
