//
//  MigrationFromV2toV3Policy.h
//  PairForm
//
//  Created by Maen Juganaikloo on 19/12/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface MigrationFromV2toV3Policy : NSEntityMigrationPolicy

@end
