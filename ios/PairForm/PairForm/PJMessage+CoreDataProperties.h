//
//  PJMessage+CoreDataProperties.h
//  PairForm
//
//  Created by Maen Juganaikloo on 14/01/2016.
//  Copyright © 2016 Ecole des Mines de Nantes. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PJMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface PJMessage (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *data;
@property (nullable, nonatomic, retain) NSString *extension;
@property (nullable, nonatomic, retain) NSNumber *id_message;
@property (nullable, nonatomic, retain) NSString *nom_original;
@property (nullable, nonatomic, retain) NSString *nom_serveur;
@property (nullable, nonatomic, retain) NSNumber *taille;
@property (nullable, nonatomic, retain) NSData *thumbnail;
@property (nullable, nonatomic, retain) Message *message;

@end

NS_ASSUME_NONNULL_END
