//
//  Capsule+CoreDataProperties.m
//  PairForm
//
//  Created by Maen Juganaikloo on 19/11/2015.
//  Copyright © 2015 Ecole des Mines de Nantes. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Capsule+CoreDataProperties.h"

@implementation Capsule (CoreDataProperties)

@dynamic auteurs;
@dynamic date_creation;
@dynamic date_downloaded;
@dynamic date_edition;
@dynamic description_pf;
@dynamic id_capsule;
@dynamic id_format;
@dynamic id_ressource;
@dynamic id_visibilite;
@dynamic langue;
@dynamic licence;
@dynamic nom_court;
@dynamic nom_long;
@dynamic path;
@dynamic taille;
@dynamic url_mobile;
@dynamic messages;
@dynamic ressource;
@dynamic selecteurs;

@end
