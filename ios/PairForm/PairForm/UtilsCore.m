//
//  Utils.m
//  testcoredate
//
//  Created by admin on 22/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import "UtilsCore.h"
#import "Capsule.h"
#import "SCAppDelegate.h"
#import "NSData+MKBase64.h"
#import "RootVC.h"
#import "Notification.h"
#import "LanguageManager.h"
#import "FileManager.h"
#import "Message.h"
#import "PJMessage.h"
#import "PJMenu.h"

@implementation UtilsCore

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext = _managedObjectContext;

static NSManagedObjectContext * volatile context = nil;
//NSManagedObjectContext * volatile parent_context = nil;
static  NSTimer* timer = nil;

bool flag_is_downloading_all_messages = NO;

+ (void) initContext{
    //DLog(@"");
    if ( context == nil ){
        context = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
}

+ (NSManagedObjectContext *) getChildContext{
//    parent_context = context;
    NSManagedObjectContext * child_context = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] childManagedObjectContext];
    return child_context;
}
//+ (NSManagedObjectContext *) resetContext{
//    if ( context == parent_context ){
//        context = [(SCAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
//        return context;
//    }
//    return parent_context;
//}
+ (void) updateUtilisateurs:(NSMutableDictionary *) array_of_users withContext:(NSManagedObjectContext*)current_context {
    
    for (NSString * id_user in array_of_users) {
        NSDictionary * user = array_of_users[id_user];
        [self sauvegardeUtilisateur:[NSNumber numberWithInt:[user[@"id_utilisateur"] intValue]] name:user[@"name"] url:user[@"url"] withContext:current_context];
    }
}
+ (void) sauvegardeUtilisateur:(NSNumber *) id_utilisateur name:(NSString *)name url:(NSString *)url withContext:(NSManagedObjectContext*)current_context{
    //DLog(@"");
    
    Utilisateur *utilisateurEntity = [self getUtilisateur:id_utilisateur withContext:current_context];
    
    if (utilisateurEntity != nil){
        
        UIImage * image = [UIImage imageWithData: [NSData dataWithContentsOfURL:  [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
        
        // Compression dans un nsdata
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:image];

        // Si l'image actuelle est celle en bdd , on arrete
        if ([utilisateurEntity.image isEqualToData:data]){
            return;
        }
    }
    // Si l'utilisateur n'existe pas encore
    else {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Utilisateur" inManagedObjectContext:current_context];
        utilisateurEntity = [[Utilisateur alloc] initWithEntity:entity insertIntoManagedObjectContext:current_context];
    }
    UIImage * image = [UIImage imageWithData: [NSData dataWithContentsOfURL:  [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    
    // Compression dans un nsdata
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:image];
    utilisateurEntity.image = data;
    utilisateurEntity.id_utilisateur = id_utilisateur;
    utilisateurEntity.url = url;
    utilisateurEntity.owner_username = name;
    
    NSError *error;
	[current_context save:&error];
    //DLog(@"%@",error);

}
+ (void) sauvegardeUtilisateur:(NSNumber *) id_utilisateur name:(NSString *)name url:(NSString *)url{
    [self initContext];
    [self sauvegardeUtilisateur:id_utilisateur name:name url:url withContext:context];
}
+ (NSArray *) getAllUtilisateurs{
    //DLog(@"");
    [self initContext];
    NSError *error;
    NSEntityDescription *utilisateurEntity = [NSEntityDescription entityForName:@"Utilisateur" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:utilisateurEntity];
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    return array;
}


+ (Utilisateur *) getUtilisateur:(NSNumber *)id_utilisateur withContext:(NSManagedObjectContext*)current_context{
    //DLog(@"");
    NSError *error;
    NSEntityDescription *utilisateurEntity = [NSEntityDescription entityForName:@"Utilisateur" inManagedObjectContext:current_context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:utilisateurEntity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_utilisateur == %@)", id_utilisateur];
    [fetch setPredicate:predicate];
    
    NSArray *array = [current_context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    Utilisateur *utilisateur = [array objectAtIndex:0];
    return utilisateur;

}
+ (Utilisateur *) getUtilisateur:(NSNumber *)id_utilisateur{
    [self initContext];
    return [self getUtilisateur:id_utilisateur withContext:context];
}

#pragma mark - Gestion des messages

#pragma mark Enregistrement des messages différés

+ (void) recordMessageAsync:(NSMutableDictionary*)dic callbackSync:(void (^)(Message *, BOOL))callbackSync callbackAsync:(void (^)(NSString *))callbackAsync{
    
    //Initialisation du contexte CoreData
    [self initContext];
    
    //Création d'une entité message
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    
    //Formatter de nombre, pour conversion des strings en nombres
    //[Vincent] J'ai commenté ces deux lignes car pas utilisées
    //NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    //[f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    //Allocation de l'entité message
    Message * message = [[Message alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    
    NSMutableArray* pjTableau = [[NSMutableArray alloc] init];
    NSMutableDictionary* dataThumbnail = [NSMutableDictionary new];
    
    //Gestion PJ
    if(dic[@"pjs"]){
        NSArray* donneePJ = [(NSArray*)dic[@"pjs"] copy];
        NSEntityDescription *pjMessageEntity = [NSEntityDescription entityForName:@"PJMessage" inManagedObjectContext:context];

        //Pour chaque piece jointe
        [donneePJ enumerateObjectsUsingBlock:^(PJMenu * obj, NSUInteger idx, BOOL *stop) {
            
            //on créer une identité coredata correspondant
            PJMessage *pj = [[PJMessage alloc] initWithEntity:pjMessageEntity insertIntoManagedObjectContext:context];
            
            pj.id_message = dic[@"id_message"];
            pj.nom_original = [NSString stringWithFormat:@"pj-%lu.%@", idx, [obj.extension lowercaseString]];
            pj.taille = [NSNumber numberWithLong:[obj taille]];
            pj.extension = [obj extension];
            //on met les donnee de l'image complete au cas OU le client ne sois pas lié avec le serveur. Dans ce cas on garde l'image complète
            //On récupère le thumbnail qu'a la réussite de l'envoie :-)
            
            pj.thumbnail = obj.data;
            
            //Ajout de la position de la PJ
            pj.position = [NSNumber numberWithUnsignedInteger:idx];
            
            pj.data = [obj recupereDonnee];
            
            [pj setMessage:(Message *)[pj.managedObjectContext objectWithID:message.objectID]];
            
            //On garde en mémoire le thumbnail pour APRES l'envoie
            //Dans le cas d'une photo on calcul le thumbnail
            if(obj.type == Photo){
                [dataThumbnail setObject:[PJ renvoieDataPourThumbnailDeLextension:obj.extension EtDesData:[obj recupereDonnee]]
                               forKey:pj.nom_original];
            }else if (obj.type == Video){
                //Dans le cas d'une video on la deja calculer pour l'affichage
                [dataThumbnail setObject:obj.data
                                  forKey:pj.nom_original];
            }
            
            //On l'ajoute au dico pour le serveur : image original + thumbnail
            [dic setObject:pj.data forKey:pj.nom_original];
            [dic setObject:dataThumbnail[pj.nom_original] forKey:[NSString stringWithFormat:@"pj-%lu-thumbnail.%@", idx, [obj.extension lowercaseString]]];
            [dic setObject:pj.position forKey:[NSString stringWithFormat:@"position_pj-%lu.%@", idx, [obj.extension lowercaseString]]];
            
            [pjTableau addObject:pj];
            DLog(@"Ajout d'une pj de taille : %ld (mo ou octet) ? et d'extension: %@", [obj taille], [obj extension]);
            
        }];
        
        [dic removeObjectForKey:@"pjs"];
    }
    
    
    
    
    
    [self recordAllPropertiesOfDictionary:dic
                                 inMessage:message
                                 ofEntity:entity
                    withStringNullDefault:@""
                    withNumberNullDefault:@0];
    
    Capsule * capsule = [self getCapsule:message.id_capsule withContext:context];
    if (capsule) {
        [message setCapsule:(Capsule*)[message.managedObjectContext objectWithID:capsule.objectID]];
    }
    
    //Pas encore d'id de message - on le remplira plus tard
    //    message.id_message = [f numberFromString:[dic objectForKey:@"id_message"]];

//    message.somme_votes= [f numberFromString:[dic objectForKey:@"utilite"] ? dic[@"utilite"] : @"0"];
//    message.supprime_par = [f numberFromString:[dic objectForKey:@"supprime_par"] ? dic[@"supprime_par"] : @"0"];
//
//
//    message.contenu= [dic objectForKey:@"contenu"];
//    message.id_capsule= [dic objectForKey:@"id_capsule"];
//    message.nom_page= [dic objectForKey:@"nom_page"] ? dic[@"nom_page"] : @"";
//    message.nom_tag= [dic objectForKey:@"nom_tag"] ? dic[@"nom_tag"] : @"PAGE";
//    message.num_occurence= [dic objectForKey:@"num_occurence"] ? dic[@"num_occurence"] : @0;
//
    NSNumber * now_timestamp = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
    message.id_auteur = [dic objectForKey:@"id_utilisateur"];
    message.date_creation= now_timestamp;
    
    [dic setObject:message.date_creation forKey:@"date_creation"];
    
    //on force l'id du parent à 0 car utiliser dans les predicates pour récupérer les messages lors du rafraichissement
    if(message.id_message_parent == nil)
        message.id_message_parent = @0;

//    
//    NSArray * arrayTag = [dic objectForKey:@"tags"];
//    if (  arrayTag ){
//        NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:arrayTag];
//        message.tags = arrayData;
//    }else{
//        message.tags = nil;
//    }
//    
//    NSArray * arrayTagAuteurs = [dic objectForKey:@"tags_auteurs"];
//    if (  arrayTagAuteurs ){
//        NSData *arrayDataTagsAuteurs = [NSKeyedArchiver archivedDataWithRootObject:arrayTagAuteurs];
//        message.noms_tags_auteurs = arrayDataTagsAuteurs;
//    }else{
//        message.noms_tags_auteurs = nil;
//    }
//    
//    message.est_defi = [dic objectForKey: @"est_defi"] ? @1 : @0;
//    
//    message.defi_valide = [dic objectForKey: @"defi_valide"] ? dic[@"defi_valide"] : @0;
//    
//    message.est_lu = [NSNumber numberWithBool:NO];
//    
//    message.id_message_parent= dic[@"id_message_parent"] ? dic[@"id_message_parent"] : @0;
//    
//    message.id_langue = dic[@"id_langue"] ? [NSNumber numberWithInt:[dic[@"id_langue"] intValue]] : @0;
//    
//    if ( [dic objectForKey:@"medaille"] == [NSNull null] || [dic objectForKey:@"medaille"] == nil){
//        message.medaille= @"";
//    }else{
//        message.medaille= [dic objectForKey:@"medaille"];
//    }
//    
//    
    if ( [dic objectForKey:@"nom_categorie"] == [NSNull null] || [dic objectForKey:@"nom_categorie"] == nil){
        message.role_auteur= @"";
    }else{
        message.role_auteur= [dic objectForKey:@"nom_categorie"];
    }
    
    if ( [dic objectForKey:@"id_categorie"] == [NSNull null] || [dic objectForKey:@"id_categorie"] == nil){
        message.id_role_auteur = @"";
    }else{
        //Cast en string
        message.id_role_auteur = [NSString stringWithFormat:@"%d", [[dic objectForKey:@"id_categorie"] integerValue]];
    }
    
//    message.utilisateur_a_vote= [NSNumber numberWithBool:NO];
        
    NSError * error;
    [context save:&error];
    
    BOOL message_envoye_au_reseau = false;
    //Cette condition permet de ne pas envoyer au serveur un message qui répond à un autre mes sage qui n'est pas encore synchronisé
    //Ainsi celui ci sera envoyé par le méchanisme de synchronisation
    if([message.id_message_parent integerValue] >= 0){
        message_envoye_au_reseau = [WService put:@"message"
                param: dic
             callback: ^(NSDictionary *wsReturn) {
                 
                 if(![WService displayErrors:wsReturn])
                 {
                     message.id_message = wsReturn[@"id_message"];
                     
                     //Enregistrement des points
                     [UtilsCore recordNotificationFromDictionary:@{@"iu" : [UIViewController getSessionValueForKey:@"id_utilisateur"],
                                                                   @"t" : @"us",
                                                                   @"aps" : @{@"alert" : @"Message posté"},
                                                                   @"st": @"PTSECRIREMESSAGE",
                                                                   @"c": [NSString stringWithFormat:@"%@", message.id_capsule],
                                                                   @"im": [NSString stringWithFormat:@"%@", message.id_message]}];
                     
                     //              [UtilsCore sauvegardeUtilisateur:message.id_auteur name:[dic objectForKey:@"owner_username"] url:[dic objectForKey:@"owner_avatar_medium"]];
                     
                     //on met à jour les pjs
                     [self enregistrePJ:pjTableau avecDonneesServeur:wsReturn EtThumbNail:dataThumbnail];
                     
                     //On enregistre finalement le message
                     NSError * error;
                     if(![context save:&error])
                         DLog(@"Save should not fail\n%@", [error localizedDescription]);
                     //              [[NSNotificationCenter defaultCenter] postNotificationName: @"messageActualise" object: nil userInfo:@{@"index_tableview": dic[@"index_tableview"]}];
                     
                     if (callbackAsync) {
                         callbackAsync(@"success");
                     }
                 }
                 else{
                     
                     //              NSError * error;
    //                 [context deleteObject:message];
                     //              [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
                     
                     if (callbackAsync) {
                         callbackAsync(@"error");
                     }
                 }
             } errorMessage:nil
      activityMessage:nil];
    }
    
    //A mettre avant l'envoi, parfois l'envoi se fait tellement rapidement qu'on passe dans le callbackAsync AVANT le callbackSync...
    if (callbackSync) {
        //On fait passer l'état d'envoi du message en meme temps
        callbackSync(message, message_envoye_au_reseau);
    }
    
    
    //Post d'une notification de fin d'actualisation
    //        [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
    
}

+ (void) recupererThumbnailsPiecesJointesWithContext:(NSManagedObjectContext *)current_context withCallback:(void (^)(void))callback{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(thumbnail == nil)"];
    
    NSArray * array_pj_sans_thumbnail = [self getPJFromPredicate:predicate WithContext:current_context];
    
    if (!array_pj_sans_thumbnail) {
        return callback();
    }
    __block NSUInteger nombre_pj = [array_pj_sans_thumbnail count];
//    dispatch_token;
    
    for (PJMessage * pj_sans_thumbnail in array_pj_sans_thumbnail) {
        //Je get la preview
        [UtilsCore telechargePreviewPJ:pj_sans_thumbnail WithCallback:^(BOOL status) {
            nombre_pj--;
            
            if(status){
                DLog(@"Preview téléchargé !");
            }else {
                DLog(@"Preview non récupéré !");
            }
            
            if (nombre_pj == 0) {
                callback();
            }
        }];
    }
}
+ (void) rafraichirMessagesWithParams:(NSDictionary *)params{
    //DLog(@"");
    
    
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    // Ajout du filtrage des langues dans les parametres
    NSMutableDictionary * newParams = [[NSMutableDictionary alloc] initWithDictionary:params];
    [newParams setObject:[LanguageManager displayLanguagesStringForQuery] forKey:@"langues_affichage"];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    [WService get:@"message"
                            param:newParams
                         callback:^(NSDictionary *wsReturn) {
                             
                             
                                 //on peut faire un cast pour éviter un warning du style 'managedObjectContext not find in protocol'
                            NSManagedObjectContext * _child_context = [self getChildContext];
                             
                             [_child_context performBlock:^{
                                 NSArray * messages = [wsReturn objectForKey:@"messages"];
                                 
                                 //Sécurité anti-null de la part du WS
                                 if([messages isKindOfClass:NSClassFromString(@"NSNull")])
                                 {
                                     [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
                                     return;
                                 }
                                 
                                 NSDictionary * all_messages_ids_and_update_time = [self getAllMessagesIdsAndUpdateTimeWithContext:context];
                                 
                                 
                                 NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:_child_context];
                                 NSMutableDictionary * users_to_update = [[NSMutableDictionary alloc] init];
                                 
                                 for (NSDictionary *item in messages)
                                 {
                                     bool should_record_message = NO;
                                     //DLog(@"Id message : %@",[item objectForKey:@"id_message"]);
                                     
                                    //On construit une liste d'utilisateurs à mettre à jour
                                     if (![users_to_update objectForKey:item[@"id_auteur"]]) {
                                         NSDictionary * user_to_update = @{@"id_utilisateur": item[@"id_auteur"], @"name" : item[@"pseudo_auteur"], @"url" :item[@"url_avatar_auteur"]};
                                         [users_to_update setObject:user_to_update forKey:item[@"id_auteur"]];
                                     }
                                     Message *message;
                                     NSNumber * id_message = [f numberFromString:item[@"id_message"]];
                                     
                                     @synchronized(id_message){
                                         //Si le message n'existe pas
                                         if (!all_messages_ids_and_update_time[id_message]){
                                             should_record_message = YES;
                                             message = [[Message alloc] initWithEntity:entity insertIntoManagedObjectContext:_child_context];
                                             
                                             NSNumber * id_capsule = [item[@"id_capsule"] isKindOfClass:NSClassFromString(@"NSString")] ? [NSNumber numberWithInt:[item[@"id_capsule"] integerValue]] : item[@"id_capsule"];
                                             //IMPORTANT : etablir la relation entre le message et sa capsule
                                             Capsule * capsule = [self getCapsule:id_capsule withContext:_child_context];
                                             //Catch that shit : vérifier que la capsule existe bien.
                                             if (capsule) {
                                                 //Important : importer la capsule créée dans le MOC du main thread dans le childMOC
                                                 [message setCapsule:(Capsule*)[message.managedObjectContext objectWithID:capsule.objectID]];
                                             }
                                         }
                                         
                                         //Si on est dans le cas d'un update
                                         else if ([all_messages_ids_and_update_time[id_message] intValue] < [[f numberFromString:[item objectForKey:@"date_modification"]] intValue]) {
                                             should_record_message = YES;
                                             message = [self getMessage:id_message withContext:_child_context];
                                         }
                                     }
                                     if (should_record_message) {
                                         
                                         if(item[@"pieces_jointes"] != nil && ![item[@"pieces_jointes"] isEqualToString:@"[]"]){
                                             
                                             //Tu créé les PJs ici et tu les lies avec le message
                                             NSEntityDescription *pjMessageEntity = [NSEntityDescription entityForName:@"PJMessage" inManagedObjectContext:_child_context];
                                             NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                             f.numberStyle = NSNumberFormatterDecimalStyle;
                                             NSError* localError;
                                             
                                             id pjObjets = [NSJSONSerialization JSONObjectWithData:[item[@"pieces_jointes"] dataUsingEncoding:NSUTF8StringEncoding]
                                                                                                     options:0
                                                                                                       error:&localError];
                                             
                                             
                                             for(NSDictionary* pjParametres in pjObjets){
                                                 PJMessage *pj = nil;
                                                 //Sécurité anti-Doublon
                                                 NSArray* codedata_pj = [self recuperePJDuMessage:id_message EtDuNomServeur:pjParametres[@"nom_serveur_pj"] WithContext:nil];
                                                 
                                                 if(codedata_pj.count > 0)
                                                     pj = (PJMessage*)codedata_pj.firstObject;
                                                 
                                                 if(pj == nil){
                                                     pj = [[PJMessage alloc] initWithEntity:pjMessageEntity insertIntoManagedObjectContext:_child_context];

                                                     pj.id_message = id_message;
                                                     pj.nom_serveur = pjParametres[@"nom_serveur_pj"];
                                                     pj.nom_original = pjParametres[@"nom_original_pj"];
                                                     pj.taille = [f numberFromString:pjParametres[@"taille_pj"]];
                                                     pj.extension = pjParametres[@"extension_pj"];
                                                     pj.position = [f numberFromString:pjParametres[@"position_pj"]];
                                                     
                                                     [pj setMessage:(Message *)[pj.managedObjectContext objectWithID:message.objectID]];
                                                 }
                                             }
                                         }
                                         
                                         [self recordAllPropertiesOfDictionary:item inMessage:message ofEntity:entity withStringNullDefault:@"" withNumberNullDefault:@0];
                               
                                     }
                                     
                                 }
                                 
                                 [self updateUtilisateurs:users_to_update withContext:_child_context];
                                 
                                 [self recupererThumbnailsPiecesJointesWithContext:_child_context withCallback:^(void) {
                                     NSError *error;
                                     
                                     if(![_child_context save:&error]){
                                         DLog(@"%@",error);
                                         NSAssert(!error, [error description]);
                                         
                                         //Post d'une notification de fin d'actualisation
                                         [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
                                     }
                                     [context performBlock:^{
                                         NSError *error;
                                         if(![context save:&error]){
                                             
                                             
                                             // DLog(@"%@",error);
                                             NSAssert(!error, [error description]);
                                             
                                             //Post d'une notification de fin d'actualisation
                                             [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
                                         }
                                         else{
                                             
                                             //Synchronisation des messages lus depuis le serveur si l'utilisateur est connecté
                                             if ([UIViewController isConnected]) {
                                                 [self rafraichirMessagesLus];
                                             }
                                             
                                             if ( messages.count == 1 && [params objectForKey:@"index_tableview"] != nil){
                                                 int index_tableview = [[params objectForKey:@"index_tableview"] integerValue];
                                                 NSDictionary * userInfo = @{ @"index_tableview" : @(index_tableview) };
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"messageActualise" object:nil userInfo:userInfo];
                                             }else{
                                                 //Post d'une notification de fin d'actualisation
                                                 [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesActualises" object: nil];
                                             }
                                             flag_is_downloading_all_messages = NO;
                                         }
                                     }];
                                 }];
//                                 NSManagedObjectContext * mainContext = [self resetContext];
                                 
                             }];
                             
                                 
                                 //
                                 //                  //Fin d'activity (au cas ou il y en a un)
                                 //
                                 //                  id rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
                                 //
                                 //                  if ([rootController isKindOfClass:NSClassFromString(@"RootVC")]) {
                                 //                      [[[(RootVC*)rootController centerPanel] view ] hideToastActivity];
                                 //                  }
                             
                         } errorMessage:nil
                  activityMessage:nil];
        
//    });
//    if (!success) {
    
        
//    }
    
}

+ (void) rafraichirMessages{
    
    if(flag_is_downloading_all_messages){
        //Post d'une notification de fin d'actualisation
        [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesErreur" object: nil];
        return;
    }

    else
        flag_is_downloading_all_messages = true;
    
    if ([UIViewController isConnected]) {
        [self envoyerMessagesLus];
    }
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    
    // Filtrage des capsules.
    NSMutableArray * arrayIdCapsule  = [[NSMutableArray alloc] init];
    NSMutableDictionary * filtre_timestamp  = [[NSMutableDictionary alloc] init];
    
    NSArray * capsules = [self getAllCapsules];
    
    for (Capsule * res in capsules) {
        [arrayIdCapsule addObject:res.id_capsule] ;
        [filtre_timestamp setObject:[self getLastUpdatedTimestampForCapsule:res.id_capsule] forKey:[res.id_capsule stringValue]];
    }
    
    NSError * error = [[NSError alloc] init];
    
//    NSData* jsonDataRes = [NSJSONSerialization dataWithJSONObject:arrayIdCapsule
//                                                          options:nil error:&error];
//    
//    NSString * jsonStringRes = [[NSString alloc] initWithData:jsonDataRes encoding:NSUTF8StringEncoding];

    //Surtout pas de pretty print, les espaces et retours chariots niquent le parametre dans l'url lors de leur escape.
    NSData* filtre_timestamp_data = [NSJSONSerialization dataWithJSONObject:filtre_timestamp
                                                          options:nil error:&error];
    
    NSString * filtre_timestamp_json = [[NSString alloc] initWithData:filtre_timestamp_data encoding:NSUTF8StringEncoding];
    
//    [params setObject:jsonStringRes forKey:@"filtre_ressource"];
    [params setObject:filtre_timestamp_json forKey:@"filtre_timestamp"];
    
    [self rafraichirMessagesWithParams:params];
}

+ (void)rafraichirMessagesFromCapsule:(NSNumber *)id_capsule{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_capsule, @"id_capsule", [self getLastUpdatedTimestampForCapsule:id_capsule], @"timestamp", nil];
    [self rafraichirMessagesWithParams:params];
}


+ (void) rafraichirMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page nom_tag:(NSString*)nom_tag num_occurence:(NSNumber*)num_occurence{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_capsule, @"id_capsule",
                             nom_page,@"nom_page",
                             nom_tag, @"nom_tag",
                             num_occurence,@"num_occurence",nil];
    [self rafraichirMessagesWithParams:params];
    
}

+ (void) rafraichirMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_capsule, @"id_capsule",
                             nom_page,@"nom_page",nil];
    [self rafraichirMessagesWithParams:params];
    
}

+ (void) rafraichirMessagesFromUser:(NSNumber *)owner_guid{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             owner_guid, @"owner_guid", nil];
    [self rafraichirMessagesWithParams:params];
    
}

+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_message, @"id_message", nil];
    [self rafraichirMessagesWithParams:params];
    
}
+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message indexTableView:(NSNumber *)indexTableView{
    NSDictionary * params = [[NSDictionary alloc] initWithObjectsAndKeys:
                             id_message, @"id_message",indexTableView, @"index_tableview", nil];
    [self rafraichirMessagesWithParams:params];
    
}

+ (NSNumber *) getLastUpdatedTimestampForCapsule:(NSNumber*)id_capsule{
    NSFetchRequest *fetchRequest = [self getMessageEntityFetch];
    
    // Specify criteria for filtering which objects to fetch
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_capsule == %@)", id_capsule];
    [fetchRequest setPredicate:predicate];
    
    // Specify how the fetched objects should be sorted
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date_modification"
                                                                   ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setFetchLimit:1];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil || [fetchedObjects count] == 0) {
        return @0;
    }
    return (NSNumber*)fetchedObjects[0][@"date_modification"];
}
+ (NSArray *) getMessagesFromPredicate:(NSPredicate *)predicate withContext:(NSManagedObjectContext*)current_context{
    return [self getMessagesFromPredicate:predicate withContext:current_context andFetchOffset:nil andFetchLimit:nil];
}
+ (NSArray *) getMessagesFromPredicate:(NSPredicate *)predicate withContext:(NSManagedObjectContext*)current_context andFetchOffset:(int)offset andFetchLimit:(int)limit{
    // Ajout de la condition des langues des messages
    NSPredicate * newCondition = [NSPredicate predicateWithFormat:@"id_langue IN %@", [LanguageManager displayLanguagesArrayForQuery]];
    //TODO
    NSPredicate * merged_predicate;
    if ([UIViewController isConnected]) {
        NSPredicate * condition_or_is_user_message = [NSPredicate predicateWithFormat:@"id_auteur = %@", [UIViewController getSessionValueForKey:@"id_utilisateur"]];
        merged_predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[newCondition, condition_or_is_user_message]];
        
    }
    else{
        merged_predicate = newCondition;
    }
    
    NSPredicate * newPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate, merged_predicate]];
    
    //DLog(@"");
    NSError *error;
    NSFetchRequest *fetch;
    if (current_context) {
        fetch = [UtilsCore getMessageEntityFetchWithPrivateContext:current_context];
    }
    else{
        [self initContext];
        fetch = [UtilsCore getMessageEntityFetch];
    }
    [fetch setPredicate:newPredicate];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"date_creation"
                                        ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
//    if (offset)
//        [fetch setFetchOffset:offset];
//    if (limit)
//        [fetch setFetchLimit:limit];
    
    NSArray *array;
    
    @try {
        if (current_context) {
            array = [current_context executeFetchRequest:fetch error:&error];
        }
        else{
            array = [context executeFetchRequest:fetch error:&error];
        }
    }
    @catch (NSException *exception) {
        DLog(@"%@",exception);
    }
    @finally {
        if ( error != nil || array == nil || array.count == 0){
            return nil;
        }
        return array;
    }
    

}
+ (NSArray *) getMessagesFromPredicate:(NSPredicate *)predicate{
    return [self getMessagesFromPredicate:predicate withContext:nil];
}

+ (NSArray *) getMessagesFromPredicateWithoutLanguageFilter:(NSPredicate *)predicate{
    [self initContext];
    NSError *error;
	NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"date_creation"
                                        ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return nil;
    }
    return array;
}

+ (NSArray *) getMessagesFromPredicate:(NSPredicate *)predicate sortDescriptor:(NSSortDescriptor*)sortDescriptor{
    
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSError * first_fetch_error;
    NSArray *first_fetch_array = [context executeFetchRequest:fetch error:&first_fetch_error];
    
    if ( first_fetch_error == nil && first_fetch_array != nil && [first_fetch_array count] != 0){
        
        NSMutableArray * return_array = [[NSMutableArray alloc] init];
        //        NSNumber * id_language = [NSNumber numberWithInt:[[LanguageManager displayLanguagesStringForQuery] intValue]];
        NSArray * languages_array = [LanguageManager displayLanguagesArrayForQuery];
        [first_fetch_array enumerateObjectsUsingBlock:^(Message* obj, NSUInteger idx, BOOL *stop) {
            
            //            [obj.langue isEqualToNumber:id_language] ? [return_array addObject:obj] : nil;
            [languages_array containsObject:obj.id_langue] ? [return_array addObject:obj] : nil;
        }];
        return return_array;
        // Ajout de la condition des langues des messages
        //        NSPredicate * newCondition = [NSPredicate predicateWithFormat:@"ANY langue IN %@", [LanguageManager displayLanguagesArrayForQuery]];
        //        NSPredicate * newPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate, newCondition, nil]];
        
        
        //        NSArray * array = [first_fetch_array filteredArrayUsingPredicate:newCondition];
        //        //DLog(@"");
        //        [self initContext];
        //        NSError *error;
        //        [fetch setPredicate:newPredicate];
        //
        //
        //        NSArray *array = [context executeFetchRequest:fetch error:&error];
        //        if ( error != nil || array == nil || array.count == 0){
        //            DLog(@"%@",error);
        //            return nil;
        //        }
        //        return array;
        
    }
    DLog(@"%@",first_fetch_error);
    return nil;
    
    
}

+ (NSDictionary *) getAllMessagesIdsAndUpdateTimeWithContext:(NSManagedObjectContext *)current_context{
    
//    [self initContext];
    @synchronized(context){
      
        NSFetchRequest *fetch = [UtilsCore getMessageEntityFetchWithPrivateContext:current_context];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                            initWithKey:@"date_modification"
                                            ascending:NO];
        
        [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        [fetch setReturnsDistinctResults:YES];
        [fetch setPropertiesToFetch:[NSArray arrayWithObjects:@"id_message",@"date_modification",nil]];
        NSError * error;
        NSArray *array = [context executeFetchRequest:fetch error:&error];
        
        if ( error != nil ){
            DLog(@"Erreur de récupération de toutes les ids de message");
            return nil;
        }

        NSMutableDictionary * dic_id_update_message = [[NSMutableDictionary alloc] initWithCapacity:[array count]];
        for (Message * message in array) {
            //Sécurité contre les messages éventuellements corrompus par un téléchargement / enregistrement de messages interrompu
            if (message.id_message && [message.id_message intValue] > 0 && message.capsule != nil) {
                [dic_id_update_message setObject:message.date_modification forKey:message.id_message];
            }
        }
        return [dic_id_update_message copy];
    }
}

+ (Message *) getMessage:(NSNumber *)id_message withContext:(NSManagedObjectContext *)current_context{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_message == %@)", id_message];
    Message *message = [[self getMessagesFromPredicate:predicate withContext:current_context] objectAtIndex:0];
    return message;
}
+ (Message *) getMessage:(NSNumber *)id_message{
    @synchronized(id_message){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"(id_message == %@)", id_message];
        Message *message = [[self getMessagesFromPredicate:predicate] objectAtIndex:0];
        return message;
    }

}

+ (void) setMessageLangue:(NSNumber *)id_message langue:(NSNumber *) id_langue{
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(id_message == %@)",id_message];
    NSError *error;
    
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
    
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return;
    }
    for (Message * message in array) {
        message.id_langue = id_langue;
    }
    [context save:&error];
}

+ (void) setMessageLu:(NSNumber *)id_message{
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                                @"(id_message == %@) AND ( est_lu == 0)",id_message];
    [UtilsCore messagesLus:predicateLu];
}

+ (NSArray * ) getReponsesFromMessage:(NSNumber *)id_message{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_message_parent == %@)", id_message];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"date_creation"
                                        ascending:YES];
    return [self getMessagesFromPredicate:predicate sortDescriptor:sortDescriptor];
}
+ (NSArray * ) getMessagesFromUser:(NSNumber *)owner_guid{
    return [self getMessagesFromUser:owner_guid andFetchOffset:nil andFetchLimit:nil];
}

+ (NSArray * ) getMessagesFromUser:(NSNumber *)owner_guid andFetchOffset:(int)offset andFetchLimit:(int)limit{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_auteur == %@)", owner_guid];
    //return [self getMessagesFromPredicate:predicate];
    return [self getMessagesFromPredicateWithoutLanguageFilter:predicate];
}
+ (NSArray * ) getMessagesFromUsers:(NSArray *)id_utilisateurs{
    // On forme une suite de conditions en OU. le predicate de depart de ne dois pas etre vide , donc on met une valeur impossible
    NSPredicate * finalPredicate = [NSPredicate predicateWithFormat:
                                    @"(id_auteur IN %@)", id_utilisateurs];
    
//    for (NSNumber * id_utilisateur  in id_utilisateurs) {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:
//                                  @"(id_auteur == %@)", id_utilisateur];
//        
//        finalPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:[NSArray arrayWithObjects:finalPredicate, predicate, nil]];
//    }
    
    //return [self getMessagesFromPredicate:finalPredicate];
    return [self getMessagesFromPredicateWithoutLanguageFilter:finalPredicate];
}

+ (NSArray * ) getMessagesFromCapsuleTrans:(NSNumber *)id_capsule{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@)", id_capsule];
    return [self getMessagesFromPredicate:predicate];
}


+ (NSArray * ) getMessagesFromCapsule:(NSNumber *)id_capsule{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@)  AND (nom_page == '' )",id_capsule];
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                                @"(id_capsule == %@) AND ( est_lu == 0) AND (nom_page == '' )",id_capsule];
    [UtilsCore messagesLus:predicateLu];
    return [self getMessagesFromPredicate:predicate];
}


+ (NSArray * ) getMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND (nom_page == %@)  AND (nom_tag == 'PAGE' )",id_capsule, nom_page];
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                                @"(id_capsule == %@) AND (nom_page == %@ ) AND ( est_lu == 0) AND (nom_tag == 'PAGE' )",id_capsule, nom_page];
    [UtilsCore messagesLus:predicateLu];
    return [self getMessagesFromPredicate:predicate];
}

+ (NSArray * ) getMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page tag:(NSString *)tag num_occurence:(NSString *)num_occurence{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND (nom_page == %@) AND (nom_tag == %@ ) AND ( num_occurence == %@ ) ",id_capsule, nom_page,tag,num_occurence];
    
    
    NSPredicate *predicateLu = [NSPredicate predicateWithFormat:
                                @"(id_capsule == %@) AND (nom_page == %@ ) AND (nom_tag == %@ ) AND ( num_occurence == %@ ) AND ( est_lu == 0)",id_capsule,nom_page,tag,num_occurence];
    [UtilsCore messagesLus:predicateLu];
    return [self getMessagesFromPredicate:predicate];
}

#pragma mark - OA
+ (NSArray * ) getMessagesForCapsule:(NSNumber*)id_capsule andOAInPage:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND (nom_page == %@) AND (nom_tag != 'PAGE' ) ", id_capsule, nom_page];
    return [self getMessagesFromPredicate:predicate];
}

+ (NSArray * ) getMessagesFromOAInPage:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @" (nom_page == %@) AND (nom_tag != 'PAGE' ) ", nom_page];
    return [self getMessagesFromPredicate:predicate];
}

#pragma mark - AllMessage

+ (NSArray * ) getAllMessages{
    return [self getAllMessagesWithContext:nil];
}

+ (NSArray * ) getAllMessagesWithContext: (NSManagedObjectContext*)context{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule != nil)"];
    return [self getMessagesFromPredicate:predicate withContext:context];
}

+ (void) afficherMessage:(NSArray *)array{
    //DLog(@"");
    //    for (Message * message in array) {
    //        DLog(@"--------------");
    //        DLog(@"%@ ",message.id_message);
    //        DLog(@"%@ ",message.contenu);
    //        DLog(@"%@ ",message.id_auteur);
    //        DLog(@"%@ ",message.owner_username);
    //        DLog(@"%@ ",message.id_capsule);
    //        DLog(@"%@ ",message.nom_page);
    //        DLog(@"%@ ",message.nom_tag);
    //        DLog(@"%@ ",message.num_occurence);
    //        DLog(@"%@ ",message.reponses);
    //        DLog(@"%@ ",message.somme_votes);
    //        DLog(@"%@ ",message.date);
    //        DLog(@"--------------");
    //    }
}



+ (NSFetchRequest *) getMessageEntityFetchWithPrivateContext:(NSManagedObjectContext*)privateContext{
    NSEntityDescription *entity;
    
    if(privateContext)
         entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:privateContext];
    else
         entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:entity];
    return fetch;
}

+ (NSFetchRequest *) getMessageEntityFetch {
    //    //DLog(@"");
    return [self getMessageEntityFetchWithPrivateContext:nil];
}

#pragma mark - Gestion PJ

/**
 * Méthode renvoyant toutes les pjs d'un message particulier
 */
+ (NSArray * ) recuperePJDuMessage:(NSNumber*)id_message WithContext:(NSManagedObjectContext*)_context {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_message == %@)", id_message];
    
    return [self getPJFromPredicate:predicate WithContext:_context];
}

/**
 * Méthode renvoyant toutes les pjs d'un message particulier avec le nom serveur passé en paramètre
 */
+ (NSArray * ) recuperePJDuMessage:(NSNumber*)id_message EtDuNomServeur:(NSString*)name WithContext:(NSManagedObjectContext*)_context {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_message == %@) AND (nom_serveur == %@)", id_message, name];
    
    return [self getPJFromPredicate:predicate WithContext:_context];
}

/**
 * Méthode renvoyant toutes les pjs des messages envoyés
 */
+ (NSArray * ) recupereToutesLesPJWithContext:(NSManagedObjectContext*)_context{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_message > 0)"];
    
    return [self getPJFromPredicate:predicate WithContext:_context];
}

/**
 * Méthode privé permettant de récupérer des pièces jointes selon le predicat en paramètre
 */
+ (NSArray * ) getPJFromPredicate:(NSPredicate *)predicate WithContext:(NSManagedObjectContext*)_context {
    
    NSError *error;
    NSFetchRequest *fetch;
    
    if(_context == nil){
        
        [self initContext];
        _context = context;
    }
    
    fetch = [UtilsCore getPJMessageEntityFetch];
    
    [fetch setPredicate:predicate];
    
    NSArray *array;
    @try {
        array = [_context executeFetchRequest:fetch error:&error];
    }
    @catch (NSException *exception) {
        DLog(@"%@",exception);
    }
    @finally {
        if ( error != nil || array == nil || array.count == 0){
            return nil;
        }
        return array;
    }

}

+ (NSFetchRequest *) getPJMessageEntityFetch{
    NSEntityDescription *entity;
    entity = [NSEntityDescription entityForName:@"PJMessage" inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    
    //On trie par la clé position
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position"
                                                                   ascending:YES];
    [fetch setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    [fetch setEntity:entity];
    return fetch;
}

/**
 * Méthode permettant d'intégrer les données renvoyées par le serveur dans un tableau de pj
 */
+(void)enregistrePJ:(NSArray*)pjs avecDonneesServeur:(NSDictionary*)donnee EtThumbNail:(NSDictionary*)thumbnailFromOriginalName{
    
    if(pjs > 0 && [donnee[@"dataPJ"] isKindOfClass:NSClassFromString(@"NSDictionary")]){
        
        //On modify les pj avec les donnees toutes fraiches du serveur
        NSDictionary* pjDatas = donnee[@"dataPJ"][@"dataPJ"];
        
        [pjs enumerateObjectsUsingBlock:^(PJMessage * obj, NSUInteger idx, BOOL *stop) {
            //Si le serveur renvoie des infos pour cette piece jointe
            if([pjDatas objectForKey:obj.nom_original] != nil){
                
                NSDictionary* serverPJData = pjDatas[obj.nom_original];
                
                obj.id_message = donnee[@"id_message"];
                obj.nom_serveur = serverPJData[@"name"];
                obj.taille = serverPJData[@"size"];
                obj.position = [NSNumber numberWithInt:[serverPJData[@"position"] intValue]];
                //Utile au cas ou on fasse une conversion cote serveur
                obj.extension = serverPJData[@"extension"];
                //Ce champs contient la preview de la PJ
                obj.data = thumbnailFromOriginalName[obj.nom_original];
            }
        }];
    }

}

/**
 * Méthode permettant de télécharger une pièce jointe originale (pas aperçu)
 */
+ (void)telechargePJ:(PJMessage*)pj WithCallback:(void (^)(BOOL, NSData*))callbackAsync{
    
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    [dic setObject:pj.nom_serveur forKey:@"nom_serveur_pj"];
    
    [WService get:@"recupererPieceJointe"
            param: dic
         callback: ^(NSDictionary *wsReturn) {
             
             if(![WService displayErrors:wsReturn])
             {
                 callbackAsync(true, wsReturn[@"data"]);
             }
             else{
                 callbackAsync(false, nil);
             }
         } errorMessage:nil
  activityMessage:nil];
}

/**
 * Méthode permettant de télécharger l'aperçu d'une pièce jointe
 */
+ (void)telechargePreviewPJ:(PJMessage*)pj WithCallback:(void (^)(BOOL))callbackAsync{
    
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    [dic setObject:pj.nom_serveur forKey:@"nom_serveur_pj"];
    [dic setObject:@"thumbnail" forKey:@"type"];
    
    [WService get:@"recupererPieceJointe"
            param: dic
         callback: ^(NSDictionary *wsReturn) {
             //Cas ou l'utilisateur n'est pas loggé mais on télécharge une ressource = la premiere par exemple (bac a sable)
             //-> on eneleve la securité loggin du coup
             //Sécurité anti-null de la part du WS
             if(wsReturn[@"data"] != nil){
                 pj.thumbnail = wsReturn[@"data"];
                 callbackAsync(true);
             }
             else{
                 callbackAsync(false);
             }
         } errorMessage:nil
  activityMessage:nil];
}

//Méthode qui va récupérer les données concernant les pièces jointes (poids, nombre max, ...)
+(void) recupereDonneePieceJointe{
    
    [WService get:@"informationsGeneral"
            param: [NSMutableDictionary new]
         callback: ^(NSDictionary *wsReturn) {
             
             [UIViewController setSessionValue:[wsReturn copy]];
             
         } errorMessage:nil
  activityMessage:nil];
    
}

#pragma mark - Non envoyés

//Methode renvoyant les messages qui n'ont pas étaient envoyés au server
// CF : ceux qui ont un id négatif correspondant à la date de création en secondes * -1
+ (NSArray * ) getMessagesNonEnvoyes{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_message < 0)"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"date_creation"
                                        ascending:NO];
    
    return [self getMessagesFromPredicate:predicate sortDescriptor:sortDescriptor];
}

// Méthode permettant de lancer la synchronisations des messages présents sur le téléphone
// Callback : méthode lancée lors de l'opération
// bool => réussie ou erreur
// int => code erreur :
//      -1 un des messages demande un id non existant ou qui aurait du etre envoyé au server ou que le server n'a pas reussie à ajouter
//          == l'id voulue appartient à un autre message qu'on n'a pas pu synchroniser
//      -2 le serveur renvoie une erreur
//      -3 On a fini de parcourir tous le tableau
//      -4 Aucun message dans la file. Aucune action de réalisée
//      -5 Ce message n'a pas été envoyé au serveur car l'auteur n'est pas la même personne
+ (void) synchroniserMessagesClientVersServeurWithCallBack:(void (^)(bool, int))callback {
    DLog(@"");
    
    NSAssert(callback, @"La méthode doit contenir un callback");
    
    //Dictionnaire qui va renvoyer l'id actuelle sur le serveur d'un message qui vient d'être envoyés en fonction de son vieille id négatif
    NSMutableDictionary *idServeurConversion = [[NSMutableDictionary alloc] init];
    
    //Je trie les messages en ordre décroissant sur l'id : le chiffre le + petit => le premier envoyé (== suceptible d'avoir une réponse :p)
    NSMutableArray *messagesRestant = [NSMutableArray arrayWithArray:[[UtilsCore getMessagesNonEnvoyes] sortedArrayUsingComparator:^NSComparisonResult(Message* obj1, Message* obj2) {
        if ([obj1.id_message integerValue] > [obj2.id_message integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1.id_message integerValue] < [obj2.id_message integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }]];
    
    int cpt = (int)messagesRestant.count;
    
    NSManagedObjectContext * _context_fils = [self getChildContext];
    
    if(cpt > 0)
        [UtilsCore boucleTantQueTousLesMessagesPasEnvoyes:messagesRestant
                                        callBack:callback
                                         totalMessage:cpt
                                  tableauConversionId:idServeurConversion
                                          withContext:_context_fils];
    else
        callback(true, -4);
}

//Méthode récursive pour itérer sur un appel asychrone avec le serveur
+ (void) boucleTantQueTousLesMessagesPasEnvoyes:(NSMutableArray*)messages callBack:(void (^)(bool, int))callBack totalMessage:(int)cpt tableauConversionId:(NSMutableDictionary *)tableauConversionId withContext:(NSManagedObjectContext*)context_courant{
    
    Message* messageTmp = messages.lastObject;
    
    //S'il n'y a plus de message à traiter
    if (!messageTmp) {
        //On se casse avec un flag d'echec
        return callBack(false, -2);
    }
    //création de dic à partir de messageTmp
    Message *message = [self getMessage:messageTmp.id_message withContext:context_courant];
    
    //Si l'utilisateur actuelle n'est pas celui qui à envoyé le message
    int id_utilisateur = [[UIViewController getSessionValueForKey:@"id_utilisateur"] intValue];
    if(id_utilisateur != [message.id_auteur integerValue]){
        //On n'envoie pas le message et on préviens par la callback que le message n'est pas envoyé
        callBack(false, -5);
        [messages removeLastObject];
        [UtilsCore boucleTantQueTousLesMessagesPasEnvoyes:messages
                                                 callBack:callBack
                                             totalMessage:cpt
                                      tableauConversionId:tableauConversionId
                                              withContext:context_courant];
        return;
    }
    
    // Si il a un id_message_parent < 0 == un de mes messages non envoyés au serveur
    //On récupère le nouvelle id dans la hashTable
    if(message.id_message_parent && [message.id_message_parent integerValue] < 0){
        
        //On a besoin d'un message hors ligne qui vient d'etre envoyé
        if([tableauConversionId objectForKey:message.id_message_parent] == nil){
            //Si on a pas son id renvoyer par le serveur
            //Alors on ne peut envoyer ce message au serveur donc erreur
            callBack(false, -1);
            [messages removeLastObject];
            
            
            [UtilsCore boucleTantQueTousLesMessagesPasEnvoyes:messages
                                                     callBack:callBack
                                                 totalMessage:cpt
                                          tableauConversionId:tableauConversionId
                                                  withContext:context_courant];
            return;
        }else{//Sinon on remplace l'id négatif précédent par le vrai renvoyé par le serveur
            message.id_message_parent = tableauConversionId[message.id_message_parent];
        }
    }
    
    //Création du dictionnaire contenant les paramètres du message à envoyer au serveur
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    [dic setObject:messageTmp.id_capsule forKey:@"id_capsule"];
    [dic setObject:messageTmp.nom_page forKey:@"nom_page"];
    [dic setObject:messageTmp.contenu ? messageTmp.contenu : @"" forKey:@"contenu"];
    //TODO: Gerer la visibilite en hors ligne
    //Dépendence : cercles hors ligne
//    [dic setObject:messageTmp forKey:@"visibilite"];
    
    NSError* error;
     NSData* jsonDataTag = [NSJSONSerialization dataWithJSONObject:[messageTmp getTags]
     options:NSJSONWritingPrettyPrinted error:&error];
    NSString* tagTabMessage = [[NSString alloc] initWithData:jsonDataTag encoding:NSUTF8StringEncoding];
    
    [dic setObject:tagTabMessage forKey:@"tags"];
    [dic setObject:messageTmp.geo_latitude forKey:@"geo_latitude"];
    [dic setObject:messageTmp.geo_longitude forKey:@"geo_longitude"];
    [dic setObject:messageTmp.id_langue forKey:@"id_langue"];
    [dic setObject:messageTmp.id_role_auteur forKey:@"id_categorie"];
    [dic setObject:messageTmp.role_auteur forKey:@"nom_categorie"];
    [dic setObject:messageTmp.id_auteur forKey:@"id_utilisateur"];
    [dic setObject:messageTmp.nom_tag forKey:@"nom_tag"];
    [dic setObject:messageTmp.num_occurence forKey:@"num_occurence"];
    [dic setObject:messageTmp.est_defi forKey:@"est_defi"];
    if(messageTmp.id_message_parent)
        [dic setObject:message.id_message_parent forKey:@"id_message_parent"];
    
    //On rajoute les informations concernant les pj
    NSArray *pjTableau = [self recuperePJDuMessage:message.id_message WithContext:context_courant];
    NSMutableDictionary* dataThumbnail = [NSMutableDictionary new];
    
    [pjTableau enumerateObjectsUsingBlock:^(PJMessage * obj, NSUInteger idx, BOOL *stop) {
        
        [dataThumbnail setObject:obj.thumbnail ? obj.thumbnail : [PJ renvoieDataPourThumbnailDeLextension:obj.extension EtDesData:obj.data]
                          forKey:obj.nom_original];
        
        //On ajoute au dico pour le serveur : thumbnail + image fullsize
        [dic setObject:dataThumbnail[obj.nom_original] forKey:[NSString stringWithFormat:@"pj-%lu-thumbnail.png", idx]];
        [dic setObject:obj.data forKey:obj.nom_original];
    }];
    
    [WService put:@"message"
            param: dic
         callback: ^(NSDictionary *wsReturn) {
             //On supprime maintenant le message car si l'application est tué à ce moment : au moins le message sera récupéré du serveur et surtout ne sera pas renvoyé à la prochaine synchro
             [messages removeLastObject];
             
             if(![WService displayErrors:wsReturn])
             {
                 
                 
                 //on envoie au serveur
                 //on met l'id dans la hash sur la réponse
                 //on met à jour core data
                 [tableauConversionId setObject:wsReturn[@"id_message"] forKey:message.id_message];
                 message.id_message = wsReturn[@"id_message"];
                 
                 //Enregistrement des points
                 [UtilsCore recordNotificationFromDictionary:@{@"iu" : [UIViewController getSessionValueForKey:@"id_utilisateur"],
                                                               @"t" : @"us",
                                                               @"aps" : @{@"alert" : @"Message posté"},
                                                               @"st": @"PTSECRIREMESSAGE",
                                                               @"c": [NSString stringWithFormat:@"%@", message.id_capsule],
                                                               @"im": [NSString stringWithFormat:@"%@", message.id_message]}];
                 
                 [self enregistrePJ:pjTableau avecDonneesServeur:wsReturn EtThumbNail:dataThumbnail];
                 
                 //On enregistre finalement le message et les pjs
                 NSError * error;
                 
                 if(![context_courant save:&error]){
                     DLog(@"%@", error);
                 }
                 else{
                     [context performBlock:^{
                         NSError *error;
                         if(![context save:&error])
                             DLog(@"%@", error);
                         else
                             [[NSNotificationCenter defaultCenter] postNotificationName: @"notificationsUpdated" object: nil];
                     }];
                 }
                 
                 //Préviens de la réussite avec le nombre d'itération fait
                 //J'enlève le message qui a bien était envoyé au serveur et sauvé dans core data
                
                callBack(true, cpt-(int)messages.count);
             }
             else{
                 return callBack(false, -2);
             }
             
             //J'ai fait tout le tableau
             if(messages.count == 0){
                 
                 //On fait une vérification importante
                 //Si on a ecrit un autre message hors ligne pendant l'execution du script alors on le relance
                 //TODO: Vincent : rajouter condition si dans les messages restant il y a des id_parent et qu'il ne sont pas dans le tableauconversionid sinon ca tournera en boucle
                 /*if([[UtilsCore getMessagesNonEnvoyes] count] > 0){
                     [UtilsCore synchroniserMessagesClientVersServeurWithCallBack: callBack];
                     return;
                 }*/
                 callBack(true, -3);
                 
                 return;
             }else{
                 //Je me rappel.
                 //la boucle est basé sur le nombre d'élément dans messages
                 [UtilsCore boucleTantQueTousLesMessagesPasEnvoyes:messages
                                                          callBack:callBack
                                                      totalMessage:cpt
                                               tableauConversionId:tableauConversionId
                                                       withContext:context_courant];
                 return;
             }
             
         } errorMessage:nil
  activityMessage:nil];
}

#pragma mark - Non lus

+ (void) rafraichirMessagesLus{
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    
    // Filtrage des capsules.
    NSMutableArray * array_capsules  = [[NSMutableArray alloc] init];
    
    NSArray * capsules = [self getAllCapsules];
    
    for (Capsule * res in capsules) {
        [array_capsules addObject:res.id_capsule] ;
    }
    
    NSError * error = [[NSError alloc] init];
    
    //    NSData* jsonDataRes = [NSJSONSerialization dataWithJSONObject:arrayIdCapsule
    //                                                          options:nil error:&error];
    //
    //    NSString * jsonStringRes = [[NSString alloc] initWithData:jsonDataRes encoding:NSUTF8StringEncoding];
    
    //Surtout pas de pretty print, les espaces et retours chariots niquent le parametre dans l'url lors de leur escape.
    NSData* array_capsules_data = [NSJSONSerialization dataWithJSONObject:array_capsules
                                                                    options:nil error:&error];
    
    NSString * array_capsules_json = [[NSString alloc] initWithData:array_capsules_data encoding:NSUTF8StringEncoding];
    
    //    [params setObject:jsonStringRes forKey:@"filtre_ressource"];
    [params setObject:array_capsules forKey:@"capsules"];
    
    [self rafraichirMessagesLusWithParams:params];
}

+ (void) rafraichirMessagesLusDeCapsule:(NSNumber*)id_capsule{
    
    NSDictionary * params = @{@"capsules" : @[id_capsule]};
    
    [self rafraichirMessagesLusWithParams:params];
}

+ (void) rafraichirMessagesLusWithParams:(NSDictionary*)params {
    
    [WService get:@"message/lu"
            param:params
         callback:^(NSDictionary *wsReturn) {
             
             if([wsReturn[@"status"] isEqualToString:@"ok"]) {
                 
                 NSMutableArray * array_message_lus = [[NSMutableArray alloc] init];
                 [wsReturn[@"data"] enumerateObjectsUsingBlock:^(NSDictionary*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                     [array_message_lus addObject:obj[@"id_message"]];
                 }];
                 
                 NSPredicate * est_dans_liste_messages_lus = [NSPredicate predicateWithBlock:^BOOL(Message *  _Nonnull message, NSDictionary<NSString *,id> * _Nullable bindings) {
                     if([array_message_lus indexOfObject:message.id_message] != NSNotFound){
                         [message setEst_lu:@1];
                         return YES;
                     }
                     return NO;
                 }];
                 NSManagedObjectContext * _child_context = [self getChildContext];
                 
                 [_child_context performBlock:^{
                     NSArray * array_all_messages = [[self getAllMessagesWithContext:_child_context] filteredArrayUsingPredicate:est_dans_liste_messages_lus];
                     
//                     NSLog(@"%@", array_all_messages);
                     
                     NSError * error;
                     
                     if(![_child_context save:&error]){
                         DLog(@"%@",error);
                     }
                     else{
                         [context performBlock:^{
                             NSError *error;
                             if(![context save:&error])
                                 DLog(@"%@",error);
                             else
                                 [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesActualises" object: nil];
                         }];
                     }

                 }];
             }
             
    }];
     
}

+ (void) envoyerMessagesLus{
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    
    // Filtrage des capsules.
    NSMutableArray * array_messages  = [[NSMutableArray alloc] init];
    
    NSArray * messages = [self getMessagesFromPredicate:[NSPredicate predicateWithFormat:@"(est_lu == -1)"]];
    
    for (Message * message in messages) {
        [array_messages addObject:message.id_message] ;
    }
    
    [params setObject:array_messages forKey:@"messages"];
    
    [self envoyerMessagesLusWithParams:params];
}


+ (void) envoyerMessagesLusWithParams:(NSDictionary*)params {
    
    [WService post:@"message/lu"
            param:params
         callback:^(NSDictionary *wsReturn) {
             
             if([wsReturn[@"status"] isEqualToString:@"ok"]) {
                 
                 NSManagedObjectContext * _child_context = [self getChildContext];
                 
                 [_child_context performBlock:^{
                     NSArray * array_messages = [self getMessagesFromPredicate:[NSPredicate predicateWithFormat:@"(est_lu == -1)"] withContext:_child_context];
                     
                     NSLog(@"%@", array_messages);
                     
                     [array_messages enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
                        [message setEst_lu:@1];
                     }];
                     
                     NSError * error;
                     
                     if(![_child_context save:&error]){
                         DLog(@"%@",error);
                     }
                     else{
                         [context performBlock:^{
                             NSError *error;
                             if(![context save:&error])
                                 DLog(@"%@",error);
                             else
                                 [[NSNotificationCenter defaultCenter] postNotificationName: @"messagesActualises" object: nil];
                         }];
                     }
                     
                 }];
             }
             
         }];
    
}

+ (void) messagesLus:(NSPredicate *)predicate{
    NSError *error;
	
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:predicate];
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil || array.count == 0){
        return;
    }
    for (Message * message in array) {
        message.est_lu = [NSNumber numberWithDouble:-1];
    }
    [context save:&error];
}

+ (NSNumber *) getNombreMessageNonLuFromPredicate:(NSPredicate *)predicate{
    //    //DLog(@"");
    [self initContext];
    NSError *error;
	
    //Rajout de la condition : supprimé
    //Si on est connecté, et qu'on a le droit de le voir
    //Ou si on est pas connecté du tout
    
    // Ajout de la condition des langues des messages
    NSPredicate * newCondition = [NSPredicate predicateWithFormat:@"supprime_par = 0"];

    NSPredicate * merged_predicate;
    if ([UIViewController isConnected]) {
        NSPredicate * condition_or_is_user_message = [NSPredicate predicateWithFormat:@"id_auteur = %@", [UIViewController getSessionValueForKey:@"id_utilisateur"]];
        merged_predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[newCondition, condition_or_is_user_message]];
        
    }
    else{
        merged_predicate = newCondition;
    }
    
    NSPredicate * newPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate, merged_predicate]];
    

    
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:newPredicate];
	NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil ){
        return nil;
    }
    //DLog(@"%@",error);
    return [NSNumber numberWithInt:(int)[array count]];
}

+ (NSNumber *) getNombreMessageNonLuFromRessourceByCapsules:(NSNumber *)id_ressource{
    //    //DLog(@"");
    [self initContext];
    NSError *error;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(capsule.id_ressource == %@) AND ( est_lu == 0)",id_ressource];
    //Rajout de la condition : supprimé
    //Si on est connecté, et qu'on a le droit de le voir
    //Ou si on est pas connecté du tout
    
    // Ajout de la condition des langues des messages
    NSPredicate * newCondition = [NSPredicate predicateWithFormat:@"supprime_par = 0"];
    
    NSPredicate * merged_predicate;
    if ([UIViewController isConnected]) {
        NSPredicate * condition_or_is_user_message = [NSPredicate predicateWithFormat:@"id_auteur = %@", [UIViewController getSessionValueForKey:@"id_utilisateur"]];
        merged_predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[newCondition, condition_or_is_user_message]];
        
    }
    else{
        merged_predicate = newCondition;
    }
    
    NSPredicate * newPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate, merged_predicate]];
    
    
    
    NSFetchRequest *fetch = [UtilsCore getMessageEntityFetch];
    [fetch setPredicate:newPredicate];
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ( error != nil || array == nil ){
        return nil;
    }
    //DLog(@"%@",error);
    return [NSNumber numberWithInt:(int)[array count]];

}

+ (NSNumber *) getNombreMessageFromRessourceTotale:(NSNumber *)id_ressource{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(capsule.id_ressource == %@)",id_ressource];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}

+ (NSNumber *) getNombreMessageNonLuFromRessourceTotale:(NSNumber *)id_ressource{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(capsule.id_ressource == %@) AND ( est_lu == 0)",id_ressource];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}

+ (NSNumber *) getNombreMessageNonLuFromCapsuleTotale:(NSNumber *)id_capsule{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND ( est_lu == 0)",id_capsule];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}

+ (NSNumber *) getNombreMessageFromCapsuleTotale:(NSNumber *)id_capsule{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@)",id_capsule];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}

+ (NSNumber *) getNombreMessageFromCapsule:(NSNumber *)id_capsule{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND (nom_page == '')",id_capsule];
    NSNumber * number = [self getNombreMessageNonLuFromPredicate:predicate];
    
    return number;
}

+ (NSNumber *) getNombreMessageNonLuFromCapsule:(NSNumber *)id_capsule{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND ( est_lu == 0) AND (nom_page == '')",id_capsule];
    NSNumber * number = [self getNombreMessageNonLuFromPredicate:predicate];
    
    return number;
}



+ (NSNumber * ) getNombreMessageNonLuFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND (nom_page == %@) AND ( est_lu == 0) AND (nom_tag == 'PAGE' )",id_capsule, nom_page];
    
    NSNumber * number = [self getNombreMessageNonLuFromPredicate:predicate];
    
    // Si pas de nouveau message
    // on regarde s'il y a des message lu ,si oui on renvois 0,  sinon on renvois -1.
    if ([ number isEqualToNumber:@0 ]){
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:
                                   @"(id_capsule == %@) AND (nom_page == %@) AND (nom_tag == 'PAGE' )",id_capsule, nom_page];
        if ( [[self getNombreMessageNonLuFromPredicate:predicate2] isEqualToNumber:@0]){
            number = @-1;
        }
    }
    
    return number;
}

+ (NSNumber * ) getNombreMessageNonLuFromCapsulePageTotale:(NSNumber *)id_capsule nom_page:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND (nom_page == %@) AND ( est_lu == 0)",id_capsule, nom_page];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}


+ (NSNumber * ) getNombreMessageFromCapsulePageTotale:(NSNumber *)id_capsule nom_page:(NSString *)nom_page{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule == %@) AND (nom_page == %@) AND ( est_lu == 1)",id_capsule, nom_page];
    return [self getNombreMessageNonLuFromPredicate:predicate];
}


#pragma mark - pulling

+(void)startPullingTimer{
    if ( timer != nil ) return;
    timer = [NSTimer timerWithTimeInterval:60.0f target:[UtilsCore class] selector:@selector(rafraichirMessages) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}
+(void)stopPullingtimer{
    if ( timer == nil ) return;
    [timer invalidate];
    timer = nil;
}


#pragma mark - Tags

+(NSArray *)getAllTag{
    //    NSFetchRequest *fetchRequest = [self getMessageEntityFetch];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(id_capsule != nil)"];
    
    
    NSArray * array = [self getMessagesFromPredicate:predicate];
    NSMutableSet * tags = [[NSMutableSet alloc]init];
    for (Message * mes in array) {
        [tags addObjectsFromArray:mes.getTagsNormalized];
    }
    
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES];
    NSArray *sortedArray = [tags sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
    //DLog (@"tags: %@",sortedArray);
    //DLog (@"tags: %@",tags);
    
    return sortedArray;
}

+(NSMutableArray *)getMessageFromTag:(NSString *)tag{
    NSArray * array= [self getAllMessages];
    NSMutableArray * result = [[NSMutableArray alloc] init];
    
    for (Message * mes in array) {
        NSArray * tags = mes.getTagsNormalized;
        if ( [tags containsObject:tag]){
            [result addObject:mes];
        }
    }
    return result;
}

#pragma mark - aide pour pages

+(NSString* ) trimPageUrlForLocalDB:(NSString *)pageUrl{
    
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@".*/([^/]*).html$" options:0 error:NULL];
    //Application Support\/\d.*?\/\d.*?\/(.*)$
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"/Application Support/[0-9].*?/[0-9].*?/(.*)$" options:0 error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:pageUrl options:0 range:NSMakeRange(0, [pageUrl length])];
    if (match.range.length != 0) {
        NSString * result  = [pageUrl substringWithRange:[match rangeAtIndex:1]];
        
        return result;
    }
    else return pageUrl;
}



#pragma mark - Gestion des ressources

+ (void) enregistrerRessource:(Ressource * )ressource{
    [self initContext];
    NSError * error;
    
    //On vérifie que la ressource n'est pas encore présente
    Ressource * localRessource = [self getRessource:ressource.id_ressource];
    
    //Insertion des capsules de la ressources dans le contexte courant
    for (Capsule * caps in ressource.capsules) {
        //Si leur contexte est différent, ou que la capsule n'est pas dans le contexte actuel
        if ([caps managedObjectContext] != [ressource managedObjectContext] || [caps managedObjectContext] != context) {
            [self enregistrerCapsule:caps withRessource:ressource];
        }
    }
    //Fix : on ne compare pas directement les ressources, mais juste l'existence d'une ressource avec l'id en question
//    if (localRessource == ressource) {
    //Si la ressource est déjà en local
    if (localRessource) {
        //Mais que c'est pas vraiment la meme
        if (localRessource != ressource) {
            //On met à jour en supprimant
            [context deleteObject:localRessource];
            [context save:&error];
            [context insertObject:ressource];
            //Sauvegarde
            [context save:&error];
            NSAssert(!error, [error description]);
        }
        //Sinon, c'est la même, donc on sauvegarde les éventuelles modificaions apportées à ses capsules.
        
        [context save:&error];
        NSAssert(!error, [error description]);
    }
    //Sinon, on l'insère et on sauve
    else{
        [context insertObject:ressource];
        //Tour de passe-passe : il ne faut pas que toutes les capsules soient enregistrés dans le contexte, parce qu'elles sont temporaires (contexte nil)
        //Et du coup, CoreData cherche a fixer la relation avec les capsules qui n'existent pas dans son contexte
        //La feinte, c'est d'enlever les capsules avant l'enregistrement
        //Et de les remettre juste après, pour que le controller TelechargementVC continue à afficher les capsules temporaires…
//        NSSet * __capsules_temp = ressource.capsules;
//        ressource.capsules = nil;
        //Sauvegarde
        [context save:&error];
        //Insertion des capsules de la ressources dans le contexte courant
        for (Capsule * caps in ressource.capsules) {
            [caps setRessource:ressource];
        }
        NSAssert(!error, [error description]);

        //C'est quand même de la nique. Si, par hasard, tu lis ça et que tu as une idée de génie, n'hésite pas.
//        ressource.capsules = __capsules_temp;
    }
    
}

+ (Ressource *) enregistrerRessourceFromDictionary:(NSDictionary * )ressourceDictionary{
    
    [self initContext];
    
    Ressource * ressource = [self convertDictionaryToRessource:ressourceDictionary temporary:NO];
    
    
    //Sauvegarde
    NSError * error;
    [context save:&error];
    
    NSAssert(!error, [error description]);
    return ressource;
}

+(NSString*)getRessourcesPath{
    NSArray * documentPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory = [documentPath objectAtIndex:0];
    
    return documentDirectory;
}

+(Ressource*) convertDictionaryToRessource:(NSDictionary * )ressourceDictionary temporary:(BOOL)temporary{
    
    NSNumberFormatter * numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Ressource" inManagedObjectContext:context];
    Ressource * rs;
    
    if (temporary) {
        rs = [[Ressource alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }
    else{
        rs = [[Ressource alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    }
    
    [self recordAllPropertiesOfDictionary:ressourceDictionary inObject:rs ofEntity:entity withStringNullDefault:@"" withNumberNullDefault:@0];
    
//    
//    rs.id_ressource = [numFormatter numberFromString: ressourceDictionary[@"id_ressource"]];
//    rs.nom_court = ressourceDictionary[@"nom_court"];
//    rs.nom_long = ressourceDictionary[@"nom_long"];
//    
//    //Enregistrement de l'image en format Data
//    //    NSURL * iconeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",ressourceDictionary[@"url_mob"], ressourceDictionary[@"icone"]]];
//    //    UIImage * image = [UIImage imageWithData: [NSData dataWithContentsOfURL: iconeUrl]];
//    NSData * data = [NSData dataFromBase64String:ressourceDictionary[@"icone_blob"]];
//    UIImage * image = [UIImage imageWithData: data];
//    NSData * iconeData = [NSKeyedArchiver archivedDataWithRootObject:image];
//    
//    rs.icone = iconeData;
//    
//    NSData * dataEtablissement = [NSData dataFromBase64String:ressourceDictionary[@"icone_etablissement"]];
//    UIImage * imageEtablissement = [UIImage imageWithData: dataEtablissement];
//    NSData * iconeDataEtablissement = [NSKeyedArchiver archivedDataWithRootObject:imageEtablissement];
//    
//    rs.icone_etablissement = iconeDataEtablissement;
//    
//    
//    NSString *ressourceDirectory = [UtilsCore getRessourcesPath];
//
//    //Déprécié. Utiliser getPath.
//    //Retourne le chemin complet au moment du download, incluant le hash d'appli de l'appareil.
//    //Fait crasher lors des mises à jour, car le hash d'appli a changé, et le chemin est faux du coup.
//    rs.path = [NSString stringWithFormat:@"%@/%@",ressourceDirectory,ressourceDictionary[@"nom_court"]];
//    
//    rs.etablissement = ressourceDictionary[@"etablissement"];
//    rs.theme = ressourceDictionary[@"theme"];
//    
//    rs.licence = ressourceDictionary[@"licence"];
//    rs.auteur = ressourceDictionary[@"auteur"];
//    
//    rs.taille = [numFormatter numberFromString:ressourceDictionary[@"taille"]];
//    
//    rs.description_res = ressourceDictionary[@"description"];
//    
//    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    
//    //TODO: Meilleur formatage des dates, surtout de la date de download
//    rs.date_release =  [dateFormatter dateFromString:ressourceDictionary[@"date_release"]];
//    rs.date_update = [dateFormatter dateFromString:ressourceDictionary[@"date_update"]];
//    rs.date_downloaded = [NSDate date];
//    
//    rs.index = [NSNumber numberWithInt:0];
//    rs.url_mob = ressourceDictionary[@"url_mob"];
//    rs.url_web = ressourceDictionary[@"url_web"];
//    rs.visible = [numFormatter numberFromString: ressourceDictionary[@"visible"]];
//    //    rs.visible = [NSNumber numberWithInt:[ressourceDictionary[@"visible"] intValue]];
//    rs.langue = [numFormatter numberFromString: ressourceDictionary[@"langue"]];
//    
    return rs;
}

+(void) updateRessources:(NSArray*)ressources
{
    //ressources = toutes les ressources encore présentes sur le dashboard au moment de la validation de la suppression
    //Cad non supprimée par l'utilisateur
    
    //Gestion de la suppression des ressources
    //On récupère toutes les ressources en base
    NSMutableArray * ressourceToDelete = [[self getAllRessources] mutableCopy];
    
    //On fait un diff avec celles gardées par l'utilisateur
    [ressourceToDelete removeObjectsInArray:ressources];
    
    //Et on les enlève.
    for (Ressource * currentRessource in ressourceToDelete) {
        [context deleteObject:currentRessource];
    }
    
    //On remet les index à jour
    for (Ressource * currentRessource in ressources) {
        [self setIndex:[ressources indexOfObject:currentRessource] toRessource:currentRessource.id_ressource];
    }
    
    
    //Maintenant, on supprime tous les messages et capsules lié(e)s à ces ressources supprimées
    for (Ressource * deletedRessource in ressourceToDelete) {
        [self deleteAllCapsulesOfRessource:deletedRessource.id_ressource];
    }
    
    //Sauvegarde des ressources
    //Et on applique la déletion
    NSError * error;
    [context save:&error];
    NSAssert(!error, [error description]);
}

+ (void) setIndex:(int)index toRessource:(NSNumber *)id_ressource{
    Ressource * currentRessource = [self getRessource:id_ressource];
    currentRessource.index = [NSNumber numberWithInt:index];
}

+ (Ressource *) getRessource:(NSNumber *)id_ressource{
    
    //TODO: Gérer si la ressource n'existe pas
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Ressource" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_ressource == %@)", id_ressource];
    
    [fetch setPredicate:predicate];
    
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération d'une ressource");
        return nil;
    }
    if([array count])
        return array[0];
    else
        return nil;
}

+ (NSArray *) getAllRessources{
    
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Ressource" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"index"
                                        ascending:YES];
    
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération de toutes les ressources");
        return nil;
    }
    
    return array;
}

#pragma mark - Gestion des capsules


+ (void) updateCapsule:(Capsule * )capsule_merged withValuesOfDictionary:(NSDictionary *)capsule_dictionary{
//    [self initContext];
//    NSError * error;

    //On garde juste la date d'édition du document
//    NSDate * date_edition = [[capsule_merged date_edition] copy];
    
    [self recordAllPropertiesOfDictionary:capsule_dictionary
                                 inObject:capsule_merged
                                 ofEntity:[capsule_merged entity]
                    withStringNullDefault:@""
                    withNumberNullDefault:@0];
    
    //Flag pour dire que c'est une mise à jour
    [capsule_merged setIsUpdate:YES];
    [self setSelecteurs:capsule_dictionary[@"selecteurs"] onCapsule:capsule_merged];
    //Et on remet la date d'edition précedente, pour qu'il continue à signaler que c'est à jour
//    [capsule_merged setDate_edition:date_edition];
    //Sauvegarde
    
//    [context save:&error];
//    NSAssert(!error, [error description]);
}

+ (void) enregistrerCapsule:(Capsule * )capsule withRessource:(Ressource *)ressource{
    [self initContext];
    NSError * error;
    
    //On vérifie que la capsule n'est pas encore présente
    Capsule * localCapsule = [self getCapsule:capsule.id_capsule];

    //Fix : on ne compare pas directement les capsules, mais juste l'existence d'une capsule avec l'id en question
//    if (localCapsule == capsule) {
    if (localCapsule) {
        [context deleteObject:localCapsule];
        [context save:&error];
        [self initContext];
    }
    [context insertObject:capsule];
    
    NSArray * selecteurs = [[capsule selecteurs] allObjects];
    
    if ([selecteurs count]) {
        for (Selecteur * selecteur in selecteurs) {
            [context insertObject:selecteur];
        }
    }
//    [capsule setRessource:ressource];
//    [capsule setRessource:[self getRessource:capsule.id_ressource]];

    //Sauvegarde
    [context save:&error];
    NSAssert(!error, [error description]);
}

+ (Capsule *) enregistrerCapsuleFromDictionary:(NSDictionary * )capsuleDictionary{
    
    [self initContext];
    
    Capsule * capsule = [self convertDictionaryToCapsule:capsuleDictionary temporary:NO];
    capsule.date_downloaded = [NSDate date];
    
    [capsule setRessource:[self getRessource:capsule.id_ressource]];
    [context insertObject:capsule];
    
    //Sauvegarde
    NSError * error;
    [context save:&error];
    NSAssert(!error, [error description]);
    return capsule;
}

+(NSString*)getCapsulesPath{
    NSArray * documentPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory = [documentPath objectAtIndex:0];
    
    return documentDirectory;
}

+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)path
{
    return [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:path isDirectory:YES]];
}
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

+(Capsule*) convertDictionaryToCapsule:(NSDictionary * )capsuleDictionary temporary:(BOOL)temporary{
    
    NSNumberFormatter * numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Capsule" inManagedObjectContext:context];
    Capsule * capsule;
    
    if (temporary) {
        capsule = [[Capsule alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }
    else{
        capsule = [[Capsule alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    }
    
    [self recordAllPropertiesOfDictionary:capsuleDictionary inObject:capsule ofEntity:entity withStringNullDefault:@"" withNumberNullDefault:@0];
    [self setSelecteurs:capsuleDictionary[@"selecteurs"] onCapsule:capsule];
    
    return capsule;
}

+(Capsule*) setSelecteurs:(id)selecteurs onCapsule:(Capsule*)capsule{
    NSArray * array_selecteurs;
    //Conversion si string passé
    if ([selecteurs isKindOfClass:[NSString class]]) {
         array_selecteurs = [selecteurs componentsSeparatedByString:@","];
    }
    else{
        array_selecteurs = selecteurs;
    }
    //Si pas de selecteur passé
    if (!array_selecteurs || [array_selecteurs count] == 0) {
        //Selecteurs de Scenari par défaut
        array_selecteurs = @[@".mainContent_co p", @".mainContent_co .resInFlow", @".mainContent_ti"];
    }
    
    //Création des objets Selecteur
    
    NSEntityDescription *selecteur_entite  = [NSEntityDescription entityForName:@"Selecteur" inManagedObjectContext:context];
    
    for (NSString * selecteur_string in array_selecteurs) {
        Selecteur * selecteur = [[Selecteur alloc] initWithEntity:selecteur_entite insertIntoManagedObjectContext:[capsule managedObjectContext]];
        [selecteur setNom_selecteur:selecteur_string];
        [selecteur setCapsule:capsule];
        
        [capsule addSelecteursObject:selecteur];
        
//        NSLog(@"%@",selecteur);
    }
    //Enregistrement du contexte ?
    
    return capsule;
}

+(void) updateCapsules:(NSArray*)capsules
{
    //capsules = toutes les capsules encore présentes sur le dashboard au moment de la validation de la suppression
    //Cad non supprimée par l'utilisateur
    
    //Gestion de la suppression des capsules
    //On récupère toutes les capsules en base
    NSMutableArray * capsuleToDelete = [[self getAllCapsules] mutableCopy];
    
    //On fait un diff avec celles gardées par l'utilisateur
    [capsuleToDelete removeObjectsInArray:capsules];
    
    //Et on les enlève.
    for (Capsule * currentCapsule in capsuleToDelete) {
        [context deleteObject:currentCapsule];
    }
    
    //On remet les index à jour
    for (Capsule * currentCapsule in capsules) {
        [self setIndex:[capsules indexOfObject:currentCapsule] toCapsule:currentCapsule.id_capsule];
    }
    
    
    //Maintenant, on supprime tous les messages liés à ces capsules supprimées
    for (Capsule * deletedCapsule in capsuleToDelete) {
        //On récupère dans le Core tous les messages de la capsule
        NSArray * messagesToDelete = [self getMessagesFromCapsuleTrans:deletedCapsule.id_capsule];
        
        //On indique qu'on souhaite la suppression de cet objet
        for (Message * messageToDelete in messagesToDelete) {
            [context deleteObject:messageToDelete];
        }
        
    }
    
    //Sauvegarde des capsules
    //Et on applique la déletion
    NSError * error;
    [context save:&error];
    NSAssert(!error, [error description]);
}

//Déprécié.
+(void) deleteCapsule:(Capsule *)capsule
{
    [self initContext];
    //On les enlève.
    [context deleteObject:capsule];
    
    //Maintenant, on supprime tous les messages liés à ces capsules supprimées

    //On récupère dans le Core tous les messages de la capsule
    NSArray * messagesToDelete = [self getMessagesFromCapsuleTrans:capsule.id_capsule];
    
    //On indique qu'on souhaite la suppression de cet objet
    for (Message * messageToDelete in messagesToDelete) {
        
        NSArray* pjs = [self recuperePJDuMessage:messageToDelete.id_message WithContext:context];
        
        for(PJMessage* pj in pjs)
            [context deleteObject:pj];
        
        [context deleteObject:messageToDelete];
    }
    
    //Sauvegarde des capsules
    //Et on applique la déletion
    NSError * error;
    [context save:&error];
}

+(void) deleteCapsuleById:(NSNumber *)id_capsule
{
    [self initContext];

//    [context deleteObject:[self getCapsule:id_capsule]];
    //On enlève juste l'attribut downloaded.
    Capsule * capsule = [self getCapsule:id_capsule];
    if (!capsule) {
        return;
    }
    capsule.date_downloaded = nil;
    
    //Maintenant, on supprime tous les messages liés à ces capsules supprimées
    //On récupère dans le Core tous les messages de la capsule
    NSArray * messagesToDelete = [self getMessagesFromCapsuleTrans:id_capsule];
    
    //On indique qu'on souhaite la suppression de cet objet
    for (Message * messageToDelete in messagesToDelete) {
        NSArray* pjs = [self recuperePJDuMessage:messageToDelete.id_message WithContext:context];
        
        for(PJMessage* pj in pjs)
            [context deleteObject:pj];

        [context deleteObject:messageToDelete];
    }
    
    //Sauvegarde des capsules
    //Et on applique la déletion
    NSError * error;
    [context deleteObject:capsule];
    //Suppression physique de la capsule s'il n'y a pas d'erreur
    [FileManager removeCapsuleWithId:capsule.id_capsule ofRessource:capsule.id_ressource];
    [context save:&error];
    NSAssert(!error, [error description]);
    
    
}

+ (void) deleteAllCapsulesOfRessource:(NSNumber*) id_ressource{
    
    NSArray * capsules = [UtilsCore getAllCapsulesOfRessource:id_ressource];
    
    for (Capsule * capsule in capsules) {
        [UtilsCore deleteCapsuleById:capsule.id_capsule];
    }
    
}
+ (Capsule *) getCapsule:(NSNumber *)id_capsule withContext:(NSManagedObjectContext*)current_context{
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Capsule" inManagedObjectContext:current_context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_capsule == %@)", id_capsule];
    
    [fetch setPredicate:predicate];
    
    NSError * error;
    NSArray *array = [current_context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération d'une capsule");
        return nil;
    }
    if([array count])
        return array[0];
    else
        return nil;
}

+ (Capsule *) getCapsule:(NSNumber *)id_capsule{
    //TODO: Gérer si la capsule n'existe pas
    [self initContext];
    
    return [self getCapsule:id_capsule withContext:context];
}
+ (NSArray *) getAllCapsules{
    // AND (date_downloaded != nil)
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Capsule" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(date_downloaded != nil)"];
    
    [fetch setPredicate:predicate];
    

    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération de toutes les capsules");
        return nil;
    }
    
    return array;
}

+ (NSArray *) getAllCapsulesOfRessource:(NSNumber*) id_ressource{
    
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Capsule" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_ressource == %@)", id_ressource];
    
    [fetch setPredicate:predicate];
    
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération de toutes les capsules");
        return nil;
    }
    
    return array;
}

#pragma mark - Gestion des notifications
+ (void) getNotificationsFromServer{
    [WService get:@"notification"
         callback:^(NSDictionary *wsReturn) {
        
        if ([wsReturn[@"status"] isEqualToString:@"ok"]) {
            
            //On chope tous les ids présent en base, pour vérifier si on les a enregistrés
            NSArray * already_recorded_notifications = [self getAllNotificationsIds];
            //on peut faire un cast pour éviter un warning du style 'managedObjectContext not find in protocol'
            NSManagedObjectContext * _child_context = [self getChildContext];
            
            [_child_context performBlock:^{
                NSArray * user_notifications = wsReturn[@"un"];
                NSArray * user_score = wsReturn[@"us"];
                
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Notification" inManagedObjectContext:_child_context];
                
                for (NSDictionary *item in user_notifications)
                {
                    if ([already_recorded_notifications indexOfObject:item[@"id_notification"]] == NSNotFound) {
                        Notification * notification =  [[Notification alloc] initWithEntity:entity insertIntoManagedObjectContext:_child_context];
                        
                        notification.type = @"un";
                        [self recordAllPropertiesOfDictionary:item inObject:notification ofEntity:entity withStringNullDefault:@"" withNumberNullDefault:@0];
                        
                        if (item[@"image"]) {
                            NSString * image_name;
                            
                            if ([notification.sous_type isEqualToString:@"gm"]) {
                                NSString * medaille_type = [[[notification.contenu componentsSeparatedByString:@" - "] lastObject] lowercaseString];
                                image_name = [NSString stringWithFormat:@"medaille_%@",medaille_type];
                            }
                            else{
                                image_name = item[@"image"];
                            }
                            
                            notification.illustration = image_name;
                        }
                        notification.id_utilisateur = [UIViewController getSessionValueForKey:@"id_utilisateur"];
                    }
                }
                
                for (NSDictionary *item in user_score)
                {
                    if ([already_recorded_notifications indexOfObject:item[@"id_notification"]] == NSNotFound) {
                        Notification * notification = [[Notification alloc] initWithEntity:entity insertIntoManagedObjectContext:_child_context];
                        
                        notification.type = @"us";
                        [self recordAllPropertiesOfDictionary:item inObject:notification ofEntity:entity withStringNullDefault:@"" withNumberNullDefault:@0];
                        notification.id_utilisateur = [UIViewController getSessionValueForKey:@"id_utilisateur"];
                    }
                }
                
                NSError *error;

                if(![_child_context save:&error]){
                    DLog(@"%@",error);
                }
                else{
                    [context performBlock:^{
                        NSError *error;
                        if(![context save:&error])
                            DLog(@"%@",error);
                        else
                            [[NSNotificationCenter defaultCenter] postNotificationName: @"notificationsUpdated" object: nil];
                    }];
                }
            }];

        }
        
        
    } errorMessage:nil activityMessage:nil];
}
+ (void) recordNotificationFromDictionary:(NSDictionary * )notificationDictionary{
    
    [self initContext];
    
    
    NSNumberFormatter * numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    //Si la notification est déjà enregistrée
    if (notificationDictionary[@"in"] && [self getNotificationForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] withId:notificationDictionary[@"in"]]) {
        //On n'enregistre pas
        return;
    }
    
    
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Notification" inManagedObjectContext:context];
    Notification * notif;
    
    notif = [[Notification alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    
    //Enregistrement des champs
    
    //ID de la notification
    if (notificationDictionary[@"in"]) {
        if([notificationDictionary[@"in"] isKindOfClass:NSClassFromString(@"NSString")])
            notif.id_notification = [numFormatter numberFromString:notificationDictionary[@"in"]];
        else
            notif.id_notification = notificationDictionary[@"in"];
    }
    else
        //Defaut
        notif.id_notification = @0;
    
    //Utilisateur cible
    if (notificationDictionary[@"iu"]) {
        if([notificationDictionary[@"iu"] isKindOfClass:NSClassFromString(@"NSString")])
            notif.id_utilisateur = [numFormatter numberFromString:notificationDictionary[@"iu"]];
        else
            notif.id_utilisateur = notificationDictionary[@"iu"];
    }
    else
        //Defaut
        notif.id_utilisateur = @0;
    
    //ID du message cible
    if (notificationDictionary[@"im"]) {
        NSNumber * id_message = notificationDictionary[@"im"];
        
        if([notificationDictionary[@"im"] isKindOfClass:NSClassFromString(@"NSString")])
            id_message = [numFormatter numberFromString:notificationDictionary[@"im"]];
        
        notif.id_message = id_message;
        
        //On ne rafraichit pas ce message, parce qu'il est surement déjà en train de se faire choper
        //par le téléchargement automatique des messages lors de l'entrée en foreground de l'app
        //SAUF SI ya pas de téléchargement des messages automatique.
        if (id_message && ![self getMessage:id_message] && [[UIViewController getPreference:@"updateFrequency"] intValue] == kMessageUpdateNever) {
            
            [UtilsCore rafraichirMessagesFromMessageId:id_message];
        }
    }
    else
        //Defaut
        notif.id_message = @0;
    
    
    
    //Type de score a ajouter : se réferer à la table des scores (scores.plist)
//    notif.type_score = notificationDictionary[@"st"] ? notificationDictionary[@"st"] : @"PTSNONE"; //Defaut
    notif.type_points = notificationDictionary[@"st"] ? notificationDictionary[@"st"] : @"PTSNONE"; //Defaut
    
    NSDictionary * PTS_def = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PTS_def" ofType:@"plist"]];
    
    notif.points = notificationDictionary[@"pt"] ? notificationDictionary[@"pt"] : [NSNumber numberWithInt:(int)[PTS_def[notif.type_points] intValue]];
    
    //Date d'emission
    notif.date_creation = notificationDictionary[@"date_creation"] ? [dateFormatter dateFromString:notificationDictionary[@"date_creation"]] : [[[NSDate alloc] init] dateByAddingTimeInterval:3600*2];
    //Type de notif - 'user_score' || 'user_notification'
    notif.type = notificationDictionary[@"t"] ? notificationDictionary[@"t"] : @"un"; //Defaut
    //Titre
    notif.titre = notificationDictionary[@"aps"][@"alert"] ? notificationDictionary[@"aps"][@"alert"] : @"Notification";
    //Contenu
    notif.contenu = notificationDictionary[@"c"] ? ([notificationDictionary[@"c"] isKindOfClass:NSClassFromString(@"NSNumber")] ? [notificationDictionary[@"c"] stringValue] : notificationDictionary[@"c"]) : @"Pas de description disponible";
    //Flag de vue
    notif.date_vue = notificationDictionary[@"date_vue"] ? [dateFormatter dateFromString:notificationDictionary[@"date_vue"]] : nil;
    //Sous type de la notification, uniquement pour les "un"
    if ([notif.type isEqualToString:@"un"]) {
        notif.sous_type = notificationDictionary[@"su"] ? notificationDictionary[@"su"] : @"";
    }
    
    //Image d'illustration
   
    UIImage * image;
    NSString * image_name;
    
    if(notificationDictionary[@"il"]){
        NSData * imageData = [NSData dataFromBase64String:notificationDictionary[@"il"]];
        image = [UIImage imageWithData: imageData];
    }
    else if ([notif.type isEqualToString:@"un"]) {
        //Défi créé
        if ([notif.sous_type isEqualToString:@"dc"]) {
            image = [UIImage imageNamed:@"DefiActif"];
            image_name = @"DefiActif";
        }
        //Défi terminé
        else if ([notif.sous_type isEqualToString:@"dt"]) {
            image = [UIImage imageNamed:@"DefiFini"];
            image_name = @"DefiFini";
        }
        //Défi accepté
        else if ([notif.sous_type isEqualToString:@"dv"]) {
            image = [UIImage imageNamed:@"DefiValid"];
            image_name = @"DefiValid";
        }
        //Ajout réseau
        else if ([notif.sous_type isEqualToString:@"ar"]) {
            image = [UIImage imageNamed:@"reseau_notif"];
            image_name = @"reseau_notif";
        }
        //Classe rejointe
        else if ([notif.sous_type isEqualToString:@"cr"]) {
            image = [UIImage imageNamed:@"reseau_notif"];
            image_name = @"reseau_notif";
        }
        //Réponse
        else if ([notif.sous_type isEqualToString:@"ru"]) {
            image = [UIImage imageNamed:@"comment_unreaded_64"];
            image_name = @"comment_unreaded_64";
        }
        //Médaille
        else if ([notif.sous_type isEqualToString:@"gm"]) {
            NSString * medaille_type = [[[notif.contenu componentsSeparatedByString:@" - "] lastObject] lowercaseString];
            image = [UIImage imageNamed:[NSString stringWithFormat:@"medaille_%@",medaille_type]];
            image_name = [NSString stringWithFormat:@"medaille_%@",medaille_type];
        }
        //Défaut
        else{
            image = [UIImage imageNamed:@"mail_48"];
            image_name = @"mail_48";
        }
    }
    
//    NSData * iconeData = [NSKeyedArchiver archivedDataWithRootObject:image];
    
    notif.illustration = image_name;
    //Sauvegarde
    NSError * error;
    [context save:&error];
    
    //On poste la nouvelle notif si tout se passe bien
    if (!error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"new_user_notification" object:self userInfo:notificationDictionary];
    }
}
+ (NSArray *) getAllNotificationsIds{
    
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Notification" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    
    [fetch setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"id_notification"
                                        ascending:NO];
    
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [fetch setReturnsDistinctResults:YES];
    [fetch setPropertiesToFetch:[NSArray arrayWithObject:@"id_notification"]];
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération de toutes les notifications");
        return nil;
    }
    NSMutableArray * array_id_notif = [[NSMutableArray alloc] initWithCapacity:[array count]];
    for (Notification * notif in array) {
        [array_id_notif addObject:notif.id_notification];
    }
    return [array_id_notif copy];
}

+ (Notification *) getNotificationForUser:(NSNumber*)user_id withId:(int)id_notification{
    
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Notification" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    
    [fetch setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_utilisateur == %d)  AND (id_notification == %d)", [user_id intValue], id_notification];
    
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"date_creation"
                                        ascending:NO];
    
    [fetch setFetchLimit:1];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil || [array count] == 0){
        DLog(@"Aucune notification enregistrée.");
        return nil;
    }
    
    return array[0];
}

+ (NSArray *) getAllNotificationsForUser:(NSNumber*)user_id ofType:(NSString*)notification_type{
    
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Notification" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    
    [fetch setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((id_utilisateur == %@) OR (id_utilisateur == 0)) AND (type == %@)", user_id, notification_type];
    
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"date_creation"
                                        ascending:NO];
    
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération de toutes les notifications");
        return nil;
    }
    
    return array;
}


+ (BOOL) setNotificationsSeenForUser:(NSNumber*)user_id ofType:(NSString*)notification_type{
    
    [self initContext];
    NSArray *array = [self getUnseenNotificationForUser:user_id ofType:notification_type];
    NSMutableArray * post_array = [[NSMutableArray alloc] init];
    
    for (Notification* notif in array) {
        notif.date_vue = [NSDate date];
        [post_array addObject:notif.id_notification];
    }
    NSError * json_error;
    
    NSData * post_array_data = [NSJSONSerialization dataWithJSONObject:post_array
                                                         options:NSJSONWritingPrettyPrinted error:&json_error];
    
    if (json_error) {
        DLog(@"%@",json_error.description);
        return false;
    }
    
    NSString * post_array_json = [[NSString alloc] initWithData:post_array_data encoding:NSUTF8StringEncoding];

    NSDictionary * post = [[NSDictionary alloc] initWithObjectsAndKeys:post_array_json, @"id_notifications", nil];
    [WService post:@"notification/vue"
             param: post
          callback: ^(NSDictionary *wsReturn) {
              if ([wsReturn[@"status"] isEqualToString:@"ko"]) {
                  DLog(@"Erreur d'enregistrement des notifications");
              }
          } errorMessage:nil activityMessage:nil];
    
    NSError * error;
    [context save:&error];
    if (!error)
        return true;
    else
        return false;
}

+ (int) getUnseenNotificationTotalForUser:(NSNumber*)user_id ofType:(NSString*)notification_type{
    NSArray * unseen_notif = [self getUnseenNotificationForUser:user_id ofType:notification_type];
    
    //S'il y a des notifs non lues
    if ([unseen_notif count] != 0) {
        NSDictionary * PTS_def = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PTS_def" ofType:@"plist"]];
        
        int total = 0;
        
        if([notification_type isEqualToString:@"us"]){
            for (Notification * notif in unseen_notif) {
                if(![notif.type_points isEqualToString:@"PTSNONE"]){
                    total += [PTS_def[notif.type_points] intValue];
                }
            }
        }
        else
            total = [unseen_notif count];
        
        return total;
    }
    else
        //Sinon, on renvoie 0 direct
        return 0;
}

+ (NSArray *) getUnseenNotificationForUser:(NSNumber*)user_id ofType:(NSString*)notification_type{
    
    [self initContext];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Notification" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_utilisateur == %@) AND (type == %@) AND (date_vue == nil)", user_id, notification_type];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id_user == %@) AND (type == %@)", user_id, notification_type];
    
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"date_creation"
                                        ascending:NO];
    
    //Récuperer un seul résultat
    //    [fetch setFetchLimit:1];
    
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSError * error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    
    if ( error != nil ){
        DLog(@"Erreur de récupération des notifications");
        return nil;
    }
    
    return array;
}

+ (void) setUnseenNotificationOnAppBadge {
    
    int total_of_notifications = [UtilsCore getUnseenNotificationTotalForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:@"un"] + [UtilsCore getUnseenNotificationTotalForUser:[UIViewController getSessionValueForKey:@"id_utilisateur"] ofType:@"us"];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:total_of_notifications];
}


/**
 * @brief fonction qui enregistre un dictionnaire d'un type d'objet de core data dans l'entitÃ© passÃ© en paramÃ¨tre
 * @param dico dictionnaire d'objet Ã  enregistrer
 * @param inObject type de l'objet que contient le dictionnaire
 * @param entity
 */

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

+(void)recordAllObjectsOfArray:(NSArray*)array inObject:(id)core_data_object ofEntity:(NSEntityDescription*)entity withStringNullDefault:(id)string_null_default withNumberNullDefault:(id)number_null_default{
    for (NSDictionary * dico in array) {
        [self recordAllPropertiesOfDictionary:dico inObject:core_data_object ofEntity:entity withStringNullDefault:string_null_default withNumberNullDefault:number_null_default];
    }
}

+(void)recordAllPropertiesOfDictionary:(NSDictionary*)dico inMessage:(Message*)core_data_object ofEntity:(NSEntityDescription*)entity withStringNullDefault:(id)string_null_default withNumberNullDefault:(id)number_null_default{
    
    [self recordAllPropertiesOfDictionary:dico
                                 inObject:core_data_object
                                 ofEntity:entity
                    withStringNullDefault:string_null_default
                    withNumberNullDefault:number_null_default];
    
    if([[dico allKeys] containsObject:@"geo_longitude"]){
        if ([dico[@"geo_longitude"] isKindOfClass:[NSString class]]){
            if(dico[@"geo_longitude"] == nil || [dico[@"geo_longitude"] isEqualToString:@"0"]){
                core_data_object.geo_longitude = nil; // defaut
                core_data_object.geo_latitude = nil; // defaut
            }
        }else{
            if(dico[@"geo_longitude"] == nil || [dico[@"geo_longitude"] isEqualToNumber:@0]){
                core_data_object.geo_longitude = nil; // defaut
                core_data_object.geo_latitude = nil; // defaut
            }
        }
    }
}
    
+(void)recordAllPropertiesOfDictionary:(NSDictionary*)dico inObject:(id)core_data_object ofEntity:(NSEntityDescription*)entity withStringNullDefault:(id)string_null_default withNumberNullDefault:(id)number_null_default{
    
    if(!string_null_default)
        string_null_default = nil;
    
    if(!number_null_default)
        number_null_default = nil;
    
    NSDictionary * annot_all_props = [entity attributesByName];
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd H:mm:ss"];
    //GMT +2 renvoyé par le serveur, et par défaut sur dateformatter : dateformatter enleve donc 2h pour avoir le GMT. Mettre le timezone à GMT + 0 permet d'annuler ça
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    //VÃ©rifier que le type de l'objet dans dicoAnno est le mÃªme que celui de l'entitÃ©
    [dico enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString * final_key;
        //Cas spécial de la key "description" (obligé de suffixer, description est réservé par cocoa)
        if ([key isEqualToString:@"description"])
            final_key = [key stringByAppendingString:@"_pf"];
        //Sinon, on garde la clé telle quelle
        else
            final_key = key;

        //On chope le setter sous forme setProperty
        NSMutableString * key_capitalized = [final_key mutableCopy];
        
        
        [key_capitalized replaceCharactersInRange:NSRangeFromString(@"0, 1")
                                       withString:[[NSString stringWithFormat:@"%c",[final_key characterAtIndex:0]] capitalizedString]];
        
        
        SEL setter = NSSelectorFromString([NSString stringWithFormat:@"set%@:", key_capitalized]);
       
        //Si l'entitÃ© a bien cette propriÃ©tÃ©
        if([core_data_object respondsToSelector:setter]){
            
            //On chope le type de la propriÃ©tÃ©
            Class core_data_object_prop_type = NSClassFromString([annot_all_props[final_key] attributeValueClassName]);
            
            //Si l'objet renvoyé par le serveur est null, on s'en occupe tout de suite avant de passer dans les autres cas
            if(obj == [NSNull null]){
                if (core_data_object_prop_type == NSClassFromString(@"NSNumber"))
                    [core_data_object performSelector:setter withObject:number_null_default];
                
                else if (core_data_object_prop_type == NSClassFromString(@"NSString"))
                    [core_data_object performSelector:setter withObject:string_null_default];
                else
                    [core_data_object performSelector:setter withObject:nil];
            }
            
            //Premier cas ou les deux types sont compatibles
            else if (core_data_object_prop_type  == [obj classForKeyedArchiver]){
                [core_data_object performSelector:setter withObject:obj];
            }
            //Cast de string vers nombre
            else if (core_data_object_prop_type == NSClassFromString(@"NSNumber")
                     && [obj isKindOfClass:NSClassFromString(@"NSString")]){
                [core_data_object performSelector:setter withObject:[NSNumber numberWithDouble:[obj doubleValue]]];
            }
            //Cast de string vers date
            else if (core_data_object_prop_type == NSClassFromString(@"NSDate")
                     && [obj isKindOfClass:NSClassFromString(@"NSString")]){
                
                [core_data_object performSelector:setter withObject: [dateFormatter dateFromString:obj]];
            }
            //Cast de timestamp vers date
            else if (core_data_object_prop_type == NSClassFromString(@"NSDate")
                     && [obj isKindOfClass:NSClassFromString(@"NSNumber")]){
                
                [core_data_object performSelector:setter withObject: [NSDate dateWithTimeIntervalSince1970:[obj doubleValue]]];
            }
            //Data
            else if (core_data_object_prop_type == NSClassFromString(@"NSData")
                     && [obj isKindOfClass:NSClassFromString(@"NSString")]){
                NSData * data = [obj dataUsingEncoding:NSUTF8StringEncoding];
                NSError * error;
                id json_object = [NSJSONSerialization JSONObjectWithData:data options:nil error:&error];
                
                
                if (json_object) {
                    NSData * data_object = [NSKeyedArchiver archivedDataWithRootObject:json_object];
                    [core_data_object performSelector:setter withObject:data_object];
                }
                else
                    [core_data_object performSelector:setter withObject:nil];
                
            }
            else{
                DLog(@"######### Valeur null %@", key);
            }
            
            
        }
    }];
    
}
#pragma clang diagnostic pop

@end
