//
//  Capsule.h
//  PairForm
//
//  Created by Maen Juganaikloo on 19/11/2015.
//  Copyright © 2015 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Message, Ressource, Selecteur;

NS_ASSUME_NONNULL_BEGIN

@interface Capsule : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@property (nonatomic, strong) NSNumber * download_progression;
@property (nonatomic) BOOL isUpdate;
-(NSString*) getPath;

@end

NS_ASSUME_NONNULL_END

#import "Capsule+CoreDataProperties.h"
