//
//  Notification.h
//  PairForm
//
//  Created by Maen Juganaikloo on 08/10/2014.
//  Copyright (c) 2014 Ecole des Mines de Nantes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Notification : NSManagedObject

@property (nonatomic, retain) NSString * contenu;
@property (nonatomic, retain) NSDate * date_creation;
@property (nonatomic, retain) NSNumber * id_message;
@property (nonatomic, retain) NSNumber * id_notification;
@property (nonatomic, retain) NSNumber * id_utilisateur;
@property (nonatomic, retain) NSString * illustration;
@property (nonatomic, retain) NSDate * date_vue;
@property (nonatomic, retain) NSString * type_points;
@property (nonatomic, retain) NSNumber * points;
@property (nonatomic, retain) NSString * sous_type;
@property (nonatomic, retain) NSString * titre;
@property (nonatomic, retain) NSNumber * total_count;
@property (nonatomic, retain) NSString * type;

@end
