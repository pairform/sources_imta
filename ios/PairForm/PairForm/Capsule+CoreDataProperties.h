//
//  Capsule+CoreDataProperties.h
//  PairForm
//
//  Created by Maen Juganaikloo on 19/11/2015.
//  Copyright © 2015 Ecole des Mines de Nantes. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Capsule.h"

NS_ASSUME_NONNULL_BEGIN

@interface Capsule (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *auteurs;
@property (nullable, nonatomic, retain) NSDate *date_creation;
@property (nullable, nonatomic, retain) NSDate *date_downloaded;
@property (nullable, nonatomic, retain) NSDate *date_edition;
@property (nullable, nonatomic, retain) NSString *description_pf;
@property (nullable, nonatomic, retain) NSNumber *id_capsule;
@property (nullable, nonatomic, retain) NSNumber *id_format;
@property (nullable, nonatomic, retain) NSNumber *id_ressource;
@property (nullable, nonatomic, retain) NSNumber *id_visibilite;
@property (nullable, nonatomic, retain) NSNumber *langue;
@property (nullable, nonatomic, retain) NSString *licence;
@property (nullable, nonatomic, retain) NSString *nom_court;
@property (nullable, nonatomic, retain) NSString *nom_long;
@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSNumber *taille;
@property (nullable, nonatomic, retain) NSString *url_mobile;
@property (nullable, nonatomic, retain) NSSet<Message *> *messages;
@property (nullable, nonatomic, retain) Ressource *ressource;
@property (nullable, nonatomic, retain) NSSet<Selecteur *> *selecteurs;

@end

@interface Capsule (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet<Message *> *)values;
- (void)removeMessages:(NSSet<Message *> *)values;

- (void)addSelecteursObject:(Selecteur *)value;
- (void)removeSelecteursObject:(Selecteur *)value;
- (void)addSelecteurs:(NSSet<Selecteur *> *)values;
- (void)removeSelecteurs:(NSSet<Selecteur *> *)values;

@end

NS_ASSUME_NONNULL_END
