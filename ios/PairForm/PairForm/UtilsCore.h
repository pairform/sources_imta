//
//  Utils.h
//  testcoredate
//
//  Created by admin on 22/04/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCAppDelegate.h"
#import "Utilisateur.h"
#import "Ressource.h"
#import "Capsule.h"
#import "Notification.h"
#import "Selecteur.h"
#import "Message.h"

@interface UtilsCore : NSObject {
    NSFetchedResultsController *fetchedResultsController;
    NSManagedObjectContext *managedObjectContext;
}
+ (void) sauvegardeUtilisateur:(NSNumber *) id_utilisateur name:(NSString *)name url:(NSString *)url;

+ (NSArray *) getAllUtilisateurs;

+ (Utilisateur *) getUtilisateur:(NSNumber *)id_utilisateur;


/** Enregistre un message dans la base de donnée, et enregistre ce dernier dans la base de donnée distante en parallèle,
 puis update la BDD locale lors du retour serveur avec l'id_message.
 @param dic  Dictionnaire contenant toutes les informations sauf l'id_message et les timestamps
 @param callback Fonction de callback, envoie le message construit en parametre
 @return Notification : messagesErreur en cas d'erreur
 @return Notification : messageActualise en cas de succes + userInfo avec index_tableview
 */
+ (void) recordMessageAsync:(NSMutableDictionary*)dic callbackSync:(void (^)(Message *, BOOL))callbackSync callbackAsync:(void (^)(NSString *))callbackAsync;

+ (void) rafraichirMessages;

+ (void) rafraichirMessagesFromCapsule:(NSNumber *)id_capsule;

+ (void) rafraichirMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page;
+ (void) rafraichirMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page nom_tag:(NSString*)nom_tag num_occurence:(NSNumber*)num_occurence;
+ (void) rafraichirMessagesFromUser:(NSNumber *)owner_guid;
+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message;

+ (void) rafraichirMessagesFromMessageId:(NSNumber *)id_message indexTableView:(NSNumber *)indexTableView;

+ (Message *) getMessage:(NSNumber *)id_message;

+ (void) setMessageLangue:(NSNumber *)id_message langue:(NSNumber *) id_langue;

+ (void) setMessageLu:(NSNumber *)id_message;

+ (NSArray * ) getReponsesFromMessage:(NSNumber *)id_message;

+ (NSArray * ) getMessagesFromUser:(NSNumber *)owner_guid;

+ (NSArray * ) getMessagesFromUsers:(NSArray *)id_utilisateurs;

+ (NSArray * ) getMessagesFromCapsuleTrans:(NSNumber *)id_capsule;

+ (NSArray * ) getMessagesFromCapsule:(NSNumber *)id_capsule;

+ (NSArray * ) getMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page;

+ (NSArray * ) getMessagesFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page tag:(NSString *)tag num_occurence:(NSString *)num_occurence;

+ (NSArray * ) getMessagesForCapsule:(NSNumber*)id_capsule andOAInPage:(NSString *)nom_page;

+ (NSArray * ) getMessagesFromOAInPage:(NSString *)nom_page;

+ (NSArray * ) getAllMessages;

+ (NSFetchRequest *) getMessageEntityFetch;

/** Récupère les messages lus de toutes les capsules présentes sur le terminal, et définit les messages locaux comme lus
 */
+ (void) rafraichirMessagesLus;

/** Récupère les messages lus de la capsule demandée, et définit les messages locaux comme lus
 * @param id_capsule : id de la capsule
 */
+ (void) rafraichirMessagesLusDeCapsule:(NSNumber*)id_capsule;

+ (NSNumber *) getNombreMessageFromRessourceTotale:(NSNumber *)id_capsule;
+ (NSNumber *) getNombreMessageNonLuFromRessourceTotale:(NSNumber *)id_capsule;

+ (NSNumber *) getNombreMessageFromCapsuleTotale:(NSNumber *)id_capsule;
+ (NSNumber *) getNombreMessageNonLuFromCapsuleTotale:(NSNumber *)id_capsule;

+ (NSNumber *) getNombreMessageFromCapsule:(NSNumber *)id_capsule;
+ (NSNumber *) getNombreMessageNonLuFromCapsule:(NSNumber *)id_capsule;
+ (NSNumber * ) getNombreMessageNonLuFromCapsule:(NSNumber *)id_capsule nom_page:(NSString *)nom_page;
+ (NSNumber * ) getNombreMessageNonLuFromCapsulePageTotale:(NSNumber *)id_capsule nom_page:(NSString *)nom_page;
+ (NSNumber * ) getNombreMessageFromCapsulePageTotale:(NSNumber *)id_capsule nom_page:(NSString *)nom_page;
+ (void) enregistrerRessource:(Ressource * )ressource;

+(void)startPullingTimer;

+(void)stopPullingtimer;
+(NSArray *)getAllTag;

+(NSMutableArray *)getMessageFromTag:(NSString *)tag;


+(NSString* ) trimPageUrlForLocalDB:(NSString *)pageUrl;
+(NSString*)getRessourcesPath;
+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)path;
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
+ (Ressource *) enregistrerRessourceFromDictionary:(NSDictionary * )ressourceDictionary;

+ (Ressource*) convertDictionaryToRessource:(NSDictionary * )ressourceDictionary temporary:(BOOL)temporary;

+ (void) updateRessources:(NSArray*)ressources;

+ (void) setIndex:(int)index toRessource:(NSNumber *)id_ressource;

+ (Ressource *) getRessource:(NSNumber *)id_ressource;

+ (NSArray *) getAllRessources;

/** Mise à jour d'une capsule avec les valeurs d'un dictionaire.
 * Utile pour la synchro serveur - client.
 * @param capsule_merged - La capsule a mettre à jour
 * @param capsule_dictionary - Les données du serveur
 */
+ (void) updateCapsule:(Capsule * )capsule_merged withValuesOfDictionary:(NSDictionary *)capsule_dictionary;
+ (void) enregistrerCapsule:(Capsule * )capsule withRessource:(Ressource*)ressource;
+ (Capsule *) enregistrerCapsuleFromDictionary:(NSDictionary * )capsuleDictionary;
+ (NSString*)getCapsulesPath;

+ (Capsule*) convertDictionaryToCapsule:(NSDictionary * )capsuleDictionary temporary:(BOOL)temporary;
+ (Capsule*) setSelecteurs:(id)selecteurs onCapsule:(Capsule*)capsule;
+ (void) updateCapsules:(NSArray*)capsules;
+ (void) setIndex:(int)index toCapsule:(NSNumber *)id_capsule;
+ (void) deleteAllCapsulesOfRessource:(NSNumber*) id_ressource;
+ (NSArray *) getAllCapsulesOfRessource:(NSNumber*) id_ressource;
+ (void) deleteCapsule:(Capsule *)capsule;
+ (void) deleteCapsuleById:(NSNumber *)id_capsule;
+ (Capsule *) getCapsule:(NSNumber *)id_capsule;
+ (NSArray *) getAllCapsules;

+ (void) getNotificationsFromServer;
+ (void) recordNotificationFromDictionary:(NSDictionary * )notificationDictionary;
+ (NSArray *) getAllNotificationsForUser:(NSNumber*)user_id ofType:(NSString*)notification_type;
+ (BOOL) setNotificationsSeenForUser:(NSNumber*)user_id ofType:(NSString*)notification_type;
+ (NSArray *) getUnseenNotificationForUser:(NSNumber*)user_id ofType:(NSString*)notification_type;
+ (int) getUnseenNotificationTotalForUser:(NSNumber*)user_id ofType:(NSString*)notification_type;
/** Change le nombre de notifications sur le badge de l'app.
 */
+ (void) setUnseenNotificationOnAppBadge;


//Synchronisation serveur
+ (void) synchroniserMessagesClientVersServeurWithCallBack:(void (^)(bool, int))callback;

//Gestion PJ
+ (NSArray *) recuperePJDuMessage:(NSNumber*)id_message WithContext:(NSManagedObjectContext*)context;
+ (NSArray *) recuperePJDuMessage:(NSNumber*)id_message EtDuNomServeur:(NSString*)name WithContext:(NSManagedObjectContext*)_context;
+ (NSArray *) recupereToutesLesPJWithContext:(NSManagedObjectContext*)context;
+ (void)telechargePJ:(PJMessage*)pj WithCallback:(void (^)(BOOL, NSData*))callbackAsync;
+ (void)telechargePreviewPJ:(PJMessage*)pj WithCallback:(void (^)(BOOL))callbackAsync;

+(void) recupereDonneePieceJointe;

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end
