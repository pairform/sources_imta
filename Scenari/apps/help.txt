Version : 1.0.1

TODO :
	- DOC



- Execution d'un ANT
 		@path	: [optionnel]chemin du ant � executer. Le ANT est execut� par rapport au r�pertoire wspOutDir (basedir = wspOutDir)
 		ex : <ant path="res/sample.ant"/>
 		ex : <ant>
				<echo>Ok!! ${basedir}</echo>
			 </ant>
			 
- Execution d'une XSL : 
 		@path 		: chemin de la XSL (relatif au r�pertoire conf)
 		@includes	: fichiers � inclures (s�par�s par une virgule)
 		@excludes	: fichiers � inclures (s�par�s par une virgule)
 		ex : <xsl path="res/sample.xsl" includes="rep/**/*.model" excludes="rep/**/pb.model"/>








- D�claration de l'espace de travail
 		wspInDir  : permet de d�clarer le r�pertoire principal de l'atelier d'entr�e (parent de sources et test).
 			@path 			: relatif au r�pertoire conf
 			@includeTestDir	: [true|false] copie ou non du r�pertoire 'tests' en plus du r�pertoire 'sources'
 		wspOutDir : permet de d�clarer le r�pertoire de sortie dans lequel sera cr�� le nouveau wsp (contiendra sources et test).
 			@path 			: relatif au r�pertoire conf

 	
 	- Ajout de fichiers/r�pertoires au WSP d'entr�e.
 		@fromDir	: chemin du r�pertoire dont le contenu sera copi� dans @toDir (relatif au r�pertoire conf)
 		@toDir		: chemin du r�pertoire dans lequel sera copi� @dir (relatif � wspOutDir)
 		ex : <overwrite fromDir="res/addOn" toDir="sources"/>