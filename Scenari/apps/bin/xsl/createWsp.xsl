<?xml version="1.0" encoding="UTF-8"?>
<!--
 * LICENCE[[
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1/CeCILL 2.O 
 *
 * The contents of this file are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/ 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the 
 * License. 
 * 
 * The Original Code is kelis.fr code. 
 * 
 * The Initial Developer of the Original Code is 
 * antoine.pourchez@kelis.fr 
 * 
 * Portions created by the Initial Developer are Copyright (C) 2005 
 * the Initial Developer. All Rights Reserved. 
 * 
 * Contributor(s): 
 * 
 * 
 * Alternatively, the contents of this file may be used under the terms of 
 * either of the GNU General Public License Version 2 or later (the "GPL"), 
 * or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"), 
 * or the CeCILL Licence Version 2.0 (http://www.cecill.info/licences.en.html), 
 * in which case the provisions of the GPL, the LGPL or the CeCILL are applicable 
 * instead of those above. If you wish to allow use of your version of this file 
 * only under the terms of either the GPL, the LGPL or the CeCILL, and not to allow 
 * others to use your version of this file under the terms of the MPL, indicate 
 * your decision by deleting the provisions above and replace them with the notice 
 * and other provisions required by the GPL, the LGPL or the CeCILL. If you do not 
 * delete the provisions above, a recipient may use your version of this file under 
 * the terms of any one of the MPL, the GPL, the LGPL or the CeCILL.
 * ]]LICENCE
 -->
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xalan="http://xml.apache.org/xalan"
    			xmlns:sc-wspder="http://scenari-platform.org/svn/wspderiver"
    			extension-element-prefixes="sc-wspder">
	<xsl:output encoding="UTF-8" method="xml"/>
	<xsl:param name="pLogRep"/>
	<xsl:param name="pSilent"/>
		
	<xsl:key name="originalFolders" match="/*/originalFolders/folder" use="@target"/>
	
		<!--The component and its script are in the xalan namespace and define the 
    implementation of the extension.-->
  <xalan:component prefix="sc-wspder" functions="protegInAttApos">
  	<!-- Contournement insuffisance XPATH1.0 pour l'escaping des apos et quot -->
    <xalan:script lang="javascript"><![CDATA[
      function protegInAttApos(pString){
      	var vString = pString;
      	vString = vString.replace(/'/g, "',xxxQUOTwww'xxxQUOTwww,'");
      	return vString;
      }
    ]]></xalan:script>
  </xalan:component>
	
	<xsl:variable name="app.subname">createWsp</xsl:variable>
	
	<xsl:include href="xUtil.xsl"/>
	<xsl:include href="xCommonTarget.xsl"/>
	  
	<xsl:template match="/wspTransformer">
		<project name="doTransformWsp" default="Main" basedir=".."><!-- Basedir défini dans le build principal : bin -->
			<taskdef classname="com.scenari.ant.SplashSpecialTask" name="splashspecial"/>
			<taskdef classname="com.scenari.ant.XmlDoctype" name="xmldoctype"/>
			<taskdef resource="net/sf/antcontrib/antcontrib.properties"/>
			<taskdef name="xmltask" classname="com.oopsconsultancy.xmltask.ant.XmlTask"/>
			
			<property name="app.subname" value="{$app.subname}"/>
			<property name="app.out.path" location="${{app.conf.path}}/{wspOut/@dir}"/>
			<property name="app.out.tmp.path" location="${{app.tmp.path}}/wspOut"/>
			<property name="app.in.path" location="${{app.conf.path}}"/>
			<tstamp><format property="timestamp" pattern="yyMMdd-HHmm"/></tstamp>
			<property name="app.fileTrace.path" location="{$pLogRep}/${{app.subname}}_${{timestamp}}.log"/>
			
			<target name="Main">
				<record action="start" name="${{app.fileTrace.path}}" loglevel="info"/>
				<xsl:call-template name="xCopyFromOriginal"/>
				<xsl:apply-templates select="*[name()!='wspOut' and name()!='originalFolders']"/>
				<xsl:call-template name="xFinalCopy"/>
				<echo>---- La construction de l'atelier a été réalisée avec succés</echo>
				<record action="stop" name="${{app.fileTrace.path}}"/>
			</target>
			
			<xsl:apply-templates mode="target"/>
			
			<xsl:call-template name="xPrivateTargets"/>
			<xsl:call-template name="xCommonTarget"/>
			
		</project>
	</xsl:template>

<!-- ###
       # Actions de base
       ### -->
	
	<!-- # xCopyFromOriginal -->
	<xsl:template name="xCopyFromOriginal">
		<xsl:for-each select="originalFolders/folder">
			<xsl:comment># copie du répertoire : '<xsl:value-of select="@from"/>'</xsl:comment>
			<echo># xCopyFromOriginal : '<xsl:value-of select="@from"/>'</echo>
			<if>
				<available file="${{app.in.path}}/{@from}"/>
				<then>
					<!-- copie du répertoire -->
					<xsl:if test="$pSilent='no'">
						<splashspecial imageurl="${{app.res.path}}/logo.png" texte2="{/wspTransformer/@name}" texte="Création du répertoire '{@target}'"/>
					</xsl:if>
					<mkdir dir="${{app.out.tmp.path}}/{@target}"/>
					<copy todir="${{app.out.tmp.path}}/{@target}" preservelastmodified="true">
						<fileset dir="${{app.in.path}}/{@from}">
							<xsl:attribute name="includes"><xsl:choose><xsl:when test="string-length(normalize-space(@includes))&gt;0"><xsl:value-of select="@includes"/></xsl:when><xsl:otherwise>**/**</xsl:otherwise></xsl:choose></xsl:attribute>
							<xsl:if test="string-length(normalize-space(@excludes))&gt;0"><xsl:attribute name="excludes"><xsl:value-of select="@excludes"/></xsl:attribute></xsl:if>
						    <xsl:if test="string-length(normalize-space(@includes))&gt;0"><xsl:attribute name="includes"><xsl:value-of select="@includes"/></xsl:attribute></xsl:if>
						</fileset>
					</copy>
				</then>
				<else>
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">error</xsl:with-param>
						<xsl:with-param name="pMessage">resetFromOriginal : Le répertoire '${app.in.path}/<xsl:value-of select="@from"/>' est inexistant.</xsl:with-param>
					</xsl:call-template>
				</else>
			</if>
		</xsl:for-each>
	</xsl:template>
	
	<!-- # xFinalCopy -->
	<xsl:template name="xFinalCopy">
		<mkdir dir="${{app.out.path}}"/>
		<!-- cleanUp -->
		<fileset id="fs.file.deleted" dir="${{app.out.path}}">
			<xsl:if test="string-length(normalize-space(/*/overwrite/@excludes))&gt;0"><xsl:attribute name="excludes"><xsl:value-of select="/*/overwrite/@excludes"/></xsl:attribute></xsl:if>
			<xsl:if test="string-length(normalize-space(/*/overwrite/@includes))&gt;0"><xsl:attribute name="includes"><xsl:value-of select="/*/overwrite/@includes"/></xsl:attribute></xsl:if>
			<!--  
			<xsl:for-each select="/*/originalFolders/folder">
				<exclude name="{@target}/**"/>
			</xsl:for-each>
			-->
			<not><present targetdir="${{app.out.tmp.path}}"/></not>
		</fileset>
		<dirset id="fs.dir.deleted" dir="${{app.out.path}}">
			<xsl:if test="string-length(normalize-space(/*/overwrite/@excludes))&gt;0"><xsl:attribute name="excludes"><xsl:value-of select="/*/overwrite/@excludes"/></xsl:attribute></xsl:if>
			<xsl:if test="string-length(normalize-space(/*/overwrite/@includes))&gt;0"><xsl:attribute name="includes"><xsl:value-of select="/*/overwrite/@includes"/></xsl:attribute></xsl:if>
			<!--
			<xsl:for-each select="/*/originalFolders/folder">
				<exclude name="{@target}/**"/>
			</xsl:for-each>
			-->
			<not><present targetdir="${{app.out.tmp.path}}"/></not>
		</dirset>
		<zip destfile="{$pLogRep}/${{app.subname}}_${{timestamp}}.bkp.zip" update="true"><!-- backup -->
		 	<fileset refid="fs.file.deleted"/>
  		      	<dirset refid="fs.dir.deleted"/>
		 </zip>
		<xsl:if test="not(/*/resetFromOriginal)"><!-- Pour compatibilité ascendante -->
			<foreach target="xDeleteSvnFile" param="pFileToDelete"><!-- supp des fichiers -->
				<path>
					<fileset refid="fs.file.deleted"/>
				</path>
			</foreach>
			<foreach target="xDeleteSvnDir" param="pDirToDelete"><!-- supp des reps -->
				<path>
					<dirset refid="fs.dir.deleted"/>
				</path>
			</foreach>
		</xsl:if>
		<!-- copie finale -->
		<copy todir="${{app.out.path}}" overwrite="true">
			<fileset dir="${{app.out.tmp.path}}"/>
		</copy>
	</xsl:template>
		
	<!-- # overwrite -->
	<xsl:template match="overwrite">
		<xsl:comment># Copie des fichiers de '<xsl:value-of select="@from"/>'</xsl:comment>
		<echo># overwrite : de '<xsl:value-of select="@from"/>'</echo>
		<if>
			<available file="${{app.conf.path}}/{@from}"/>
			<then>
				<xsl:if test="$pSilent='no'">
					<splashspecial imageurl="${{app.res.path}}/logo.png" texte2="{/wspTransformer/@name}" texte="Copie des fichiers '{@from}'..."/>
				</xsl:if>
				<copy todir="${{app.out.tmp.path}}" failonerror="false" overwrite="true" preservelastmodified="true">
					<fileset dir="${{app.conf.path}}/{@from}">
						<xsl:attribute name="includes"><xsl:choose><xsl:when test="string-length(normalize-space(@includes))&gt;0"><xsl:value-of select="@includes"/></xsl:when><xsl:otherwise>**/**</xsl:otherwise></xsl:choose></xsl:attribute>
						<xsl:if test="string-length(normalize-space(@excludes))&gt;0"><xsl:attribute name="excludes"><xsl:value-of select="@excludes"/></xsl:attribute></xsl:if>
						<xsl:if test="string-length(normalize-space(@includes))&gt;0"><xsl:attribute name="includes"><xsl:value-of select="@includes"/></xsl:attribute></xsl:if>
					</fileset>
				</copy>
				<xsl:if test="$pSilent='no'">
					<splashspecial imageurl="${{app.res.path}}/logo.png" texte2="{/wspTransformer/@name}" texte="Traitement des différents fichiers..."/>
				</xsl:if>
				<!-- Execution éventuelle des .execXsl -->
				<foreach target="xExecXsl" param="pXslPath">
					<path>
						<fileset dir="${{app.out.tmp.path}}">
							<include name="**/*.{$virtualFile.xsl.extension}"/>
			    		</fileset>
					</path>
				</foreach>
				<!-- Execution des ant -->
				<foreach target="xExecAnt" param="pAntPath">
					<path>
						<fileset dir="${{app.out.tmp.path}}">
							<include name="**/*.{$virtualFile.ant.extension}"/>
			    		</fileset>
					</path>
				</foreach>
				<!-- Execution des remove -->
				<foreach target="xExecRemoveFile" param="pRemoveFilePath"><!-- supp des fichiers -->
					<path>
						<fileset dir="${{app.out.tmp.path}}">
							<include name="**/*.{$virtualFile.remove.extension}"/>
			    		</fileset>
					</path>
				</foreach>
				<foreach target="xExecRemoveDir" param="pRemoveDirPath">
					<path>
						<dirset dir="${{app.out.tmp.path}}">
							<include name="**/*.{$virtualFile.remove.extension}"/>
			    		</dirset>
					</path>
				</foreach>
			</then>
			<else>
				<xsl:call-template name="tLogMessage">
					<xsl:with-param name="pLevel">error</xsl:with-param>
					<xsl:with-param name="pMessage">overwrite : Le répertoire '${app.conf.path}/<xsl:value-of select="@from"/>' est inexistant.</xsl:with-param>
				</xsl:call-template>
			</else>
		</if>
	</xsl:template>
	
	<!-- # xsl -->
	<xsl:template match="xsl">
		<xsl:comment># Execution de la XSL '<xsl:value-of select="@path"/>' sur '<xsl:value-of select="@includes"/>'</xsl:comment>
		<echo># XSL : '<xsl:value-of select="@path"/>' sur '<xsl:value-of select="@includes"/>'</echo>
		<if>
			<and>
				<available file="${{app.conf.path}}/{@path}"/>
				<available file="${{app.out.tmp.path}}/{@dir}"/>
			</and>
			<then>
				<xsl:if test="$pSilent='no'">
					<splashspecial imageurl="${{app.res.path}}/logo.png" texte2="{/wspTransformer/@name}" texte="Execution de la XSL '{@path}' sur '{@includes}'"/>
				</xsl:if>
				<property name="tmp.xsl.dir" location="${{app.tmp.path}}/xslwork"/>
				<mkdir dir="${{tmp.xsl.dir}}"/>
				<!-- Copie des fichiers dans un rep temporaire -->
				<copy todir="${{tmp.xsl.dir}}" preservelastmodified="true">
					<fileset dir="${{app.out.tmp.path}}/{@dir}">
						<xsl:attribute name="includes"><xsl:choose><xsl:when test="string-length(normalize-space(@includes))&gt;0"><xsl:value-of select="@includes"/></xsl:when><xsl:otherwise>**/**</xsl:otherwise></xsl:choose></xsl:attribute>
						<xsl:if test="string-length(normalize-space(@excludes))&gt;0"><xsl:attribute name="excludes"><xsl:value-of select="@excludes"/></xsl:attribute></xsl:if>
						<xsl:if test="string-length(normalize-space(@includes))&gt;0"><xsl:attribute name="includes"><xsl:value-of select="@includes"/></xsl:attribute></xsl:if>
					</fileset>
				</copy>
				<!-- Execution de la XSL -->
				<xslt basedir="${{tmp.xsl.dir}}" destdir="${{app.out.tmp.path}}/{@dir}" style="${{app.conf.path}}/{@path}" includes="**/**" force="true">
					<outputproperty name="method" value="xml"/>
					<mapper type="identity"/><!-- on garde les memes noms de fichier -->
				</xslt>
				<!-- cleanUp -->
				<delete dir="${{tmp.xsl.dir}}" failonerror="false"/>
			</then>
			<else>
				<xsl:call-template name="tLogMessage">
					<xsl:with-param name="pLevel">error</xsl:with-param>
					<xsl:with-param name="pMessage">Une XSL n'a pu être executée. Le repertoire '${app.out.tmp.path}/<xsl:value-of select="@dir"/>' est inexistant ou la xsl '${app.conf.path}/<xsl:value-of select="@path"/>' est manquante.</xsl:with-param>
				</xsl:call-template>
			</else>
		</if>
	</xsl:template>
	
	<!-- # ant -->
	<xsl:template match="ant">
		<xsl:comment># Execution du script ANT '<xsl:value-of select="@path"/>'</xsl:comment>
		<echo># ant : script ANT '<xsl:value-of select="@path"/>'</echo>
		<xsl:if test="$pSilent='no'">
			<splashspecial imageurl="${{app.res.path}}/logo.png" texte2="{/wspTransformer/@name}" texte="Execution du script ANT '{@path}'"/>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@path">
				<ant antfile="${{app.conf.path}}/{@path}" inheritall="false">
					<property name="basedir" location="${{app.out.tmp.path}}/{@dir}"/>
				</ant>
			</xsl:when>
			<xsl:otherwise><!-- code ant copié directement -->
				<ant antfile="${{app.tmp.build.path}}" target="ant_{generate-id()}" inheritall="false">
					<property name="basedir" location="${{app.out.tmp.path}}"/>
				</ant>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="ant" mode="target">
		<xsl:if test="not(@path)">
			<target name="ant_{generate-id()}">
				<xsl:copy-of select="./*"/>
			</target>
		</xsl:if>
	</xsl:template>
	
<!-- ###
       # targets privées
       ### -->
    <xsl:template name="xPrivateTargets">
   		<!-- xExecXsl : execute une xsl sur le fichier de meme nom
   				@param : pXslPath path vers la XSL -->
		<target name="xExecXsl" description="Execution d'une XSL">
			<propertyregex property="tmp.fileToModifyPath"
		             input="${{pXslPath}}"
		             regexp="(.*).{$virtualFile.xsl.extension}"
		             select="\1"
		             casesensitive="true"/>
			<if>
				<available file="${{tmp.fileToModifyPath}}"/>
				<then>
					<!-- on renomme le fichier pour éviter pb de fichier locké -->
					<move file="${{tmp.fileToModifyPath}}" tofile="${{tmp.fileToModifyPath}}.srcXslTmp"/>
					<!-- # on éxécute la XSL -->
					<echo>IN : ${tmp.fileToModifyPath}.srcXslTmp</echo>
					<echo>XSL : ${pXslPath}</echo>
					<echo>OUT : ${tmp.fileToModifyPath}</echo>
					<xslt style="${{pXslPath}}" in="${{tmp.fileToModifyPath}}.srcXslTmp" out="${{tmp.fileToModifyPath}}">
						<outputproperty name="method" value="xml"/>
					</xslt>
					<!-- on supprime les fichiers temporaires -->
					<delete file="${{tmp.fileToModifyPath}}.srcXslTmp" failonerror="false"/>
				</then>
				<else>
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">error</xsl:with-param>
						<xsl:with-param name="pMessage">Une XSL n'a pu être executée sur le fichier '${tmp.fileToModifyPath}' inexistant.</xsl:with-param>
					</xsl:call-template>
				</else>
			</if>
			<delete file="${{pXslPath}}" failonerror="false"/>
		</target>
		
		<!-- xExecAnt : execute un ANT sur le fichier de meme nom
   				@param : pAntPath path vers le ANT -->
		<target name="xExecAnt" description="Execution d'un ANT">
			<propertyregex property="tmp.fileToModifyPath"
		             input="${{pAntPath}}"
		             regexp="(.*).{$virtualFile.ant.extension}"
		             select="\1"
		             casesensitive="true"/>
			<if>
				<available file="${{tmp.fileToModifyPath}}"/>
				<then>
					<echo message="Execution du ANT sur '${{tmp.fileToModifyPath}}'"/>
					<ant antfile="${{pAntPath}}">
						<property name="basedir" value="${{app.out.tmp.path}}"/>
						<property name="file.path" value="${{tmp.fileToModifyPath}}"/>
					</ant>
				</then>
				<else>
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">error</xsl:with-param>
						<xsl:with-param name="pMessage">Un ANT n'a pu être executé sur le fichier '${tmp.fileToModifyPath}' inexistant.</xsl:with-param>
					</xsl:call-template>
				</else>
			</if>
			<delete file="${{pAntPath}}" failonerror="false"/>
		</target>
		
		<!-- xExecRemoveFile : suppression d'un fichier
   				@param : pRemoveFilePath -->
		<target name="xExecRemoveFile" description="Suppression d'un fichier">
			<propertyregex property="tmp.fileToRemovePath"
	              input="${{pRemoveFilePath}}"
	              regexp="(.*).{$virtualFile.remove.extension}"
	              select="\1"
	              casesensitive="true"/>
			<if>
				<available file="${{tmp.fileToModifyPath}}"/>
				<then>
					<echo message="Suppression du fichier '${{tmp.fileToRemovePath}}'"/>
					<delete file="${{tmp.fileToRemovePath}}" failonerror="false"/>
				</then>
				<else>
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">error</xsl:with-param>
						<xsl:with-param name="pMessage">Un fichier n'a pu être supprimé : '${tmp.fileToModifyPath}'.</xsl:with-param>
					</xsl:call-template>
				</else>
			</if>
			<delete file="${{pRemoveFilePath}}" failonerror="false"/>
		</target>
		
		<!-- xExecAnt : execute un ANT sur le fichier de meme nom
   				@param : pRemoveDirPath -->
		<target name="xExecRemoveDir" description="Suppression d'un répertoire">
			<propertyregex property="tmp.fileToRemovePath"
				              input="${{pRemoveDirPath}}"
				              regexp="(.*).{$virtualFile.remove.extension}"
				              select="\1"
				              casesensitive="true"/>
			<if>
				<available file="${{tmp.fileToModifyPath}}"/>
				<then>
					<echo message="Suppression du répertoire '${{tmp.fileToRemovePath}}'"/>
					<delete dir="${{tmp.fileToRemovePath}}" failonerror="false"/>
				</then>
				<else>
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">error</xsl:with-param>
						<xsl:with-param name="pMessage">Un répertoire n'a pu être supprimé : '${tmp.fileToModifyPath}'.</xsl:with-param>
					</xsl:call-template>
				</else>
			</if>
			<delete dir="${{pRemoveDirPath}}" failonerror="false"/>
		</target>
	
   </xsl:template>
	
   <xsl:template match="*" mode="target"/>
	
</xsl:stylesheet>
