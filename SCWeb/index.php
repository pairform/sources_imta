<!DOCTYPE html>
<html>
<head>
	<title>SupCast Web</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	
	<link rel="stylesheet" type="text/css" href="res/css/reset.css">
	<link rel="stylesheet/less" type="text/css" href="res/css/general.css">
	<link rel="stylesheet/less" type="text/css" href="res/css/header.css">
	<script type="text/javascript" src="res/js/less.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script type="text/javascript" src="res/js/main.js"></script>

	<script src="//imedia.emn.fr//SCWeb/res/js/import.js" type="text/JavaScript"></script>
</head>
<body>
	<?php 
		require_once("fonctions.php");
		include_once("res/mod/navBar.php");
	?>
<section id="content">
	<div>
		<div id="paper-top">
			<h1>Social learning</h1>
			<a title="Téléchargez l'application" href="http://itunes.apple.com/fr/app/supcast/id507452786?mt=8"><img src="res/img/appstore" alt="SupCast sur l'AppStore"></a>
		</div>
		<div id="paper-middle">
			<div id="presentation">
				<p>
					<div id="teaser">
						<img title="SupCast" src="res/img/icone_supcast.png" >
						À l'heure des MOOC, SupCast est un outil de social learning, i.e. Un outil qui permet un apprentissage entre pairs, en réseau. Les MOOC sont une expérience très intéressante tant pour les participants que pour les organisateurs. Mais ils demandent, pour ces derniers, 6 mois de préparatifs intenses selon l'expérience des premiers MOOC francophones (iTypa, Gestion de projets). Tous les formateurs ne peuvent investir autant de temps. De plus, les MOOC comme les cours ou formations fonctionnent par session avec une date de début et une date de fin. En dehors de ces deux dates, il n'est pas facile de se former ou d'échanger avec des pairs. SupCast permet de mettre en œuvre simplement une expérience de social learning, disponible toute l'année pour les participants, à partir de supports de formation existants.
					</div>
				</p>
				<p>
					 
				</p>
				<h2>Finalités de SupCast</h2>
				<p>
					Apprendre en échangeant avec ses pairs sur ses apprentissages, en sollicitant de l'aide si besoin, en collaborant, en partageant des ressources externes, en aidant une personne en difficulté, en dialoguant avec des experts du domaine...
				
				</p>
				<ul>
					<li>Apprendre avec les appareils technologiques d'aujourd'hui, en situation de mobilité, avec un confort plus grand.</li>
					<li>Apprendre en conservant ses habitudes d'usages numériques :
						<ul>
							<li>réseaux sociaux en communicant au delà de ses voisins.</li>
							<li>jeux pour vivre un apprentissage plus fun.</li>
							<li>être connecté sur le monde.</li>
						</ul>
					</li>
				</ul>
			    <h2>Un réseau social dédié à l'apprentissage</h2>
			    <p>
			    	SupCast permet de télécharger des supports de formation médiatisés et de profiter d'un réseau social au cœur même de chaque ressource pédagogique afin d’échanger avec ses pairs en vue d'enrichir son apprentissage. Toutes les personnes qui ont un même support peuvent voir les messages que les autres ont attachés sur les objets d'apprentissage constituant le support de formation. Le réseau relie les usagers d'un support pour :
			    </p>
				<ul>
					<li>Poser des questions, obtenir/rechercher/apporter de l’aide.</li>
					<li>Fournir une explication à un objet d'apprentissage qui ne semble pas clair au départ.</li>
					<li>Echanger du contenu (en complément à celui du support). Le matériau pédagogique devient vivant et évolutif grâce à la communauté.</li>
					<li>Rapprocher les personnes en fonction de points communs et/ou de leur proximité géographique. Diminuer l’isolement des apprenants.</li>
					<li>Renforcer la motivation à apprendre.</li>
					<li>Pointer des ressources externes intéressantes et complémentaires</li>
					<li>Établir un lien entre les usagers et les auteurs par exemple pour améliorer ou enrichir le support pédagogique,</li>
					<li>…</li>
				</ul>			   
				<h2>Une nouvelle expérience d'apprentissage avec SupCast 2</h2>
			    <p>SupCast permet d'apprendre avec ses pairs à travers les messages de réseau social. Plus on participe au réseau, plus on progresse dans les rôles (lecteur, participant, collaborateur, animateur, expert), plus de nouvelles fonctionnalités sont débloquées. On se construit un réseau de contacts partageant les mêmes problématiques d'apprentissage. On reçoit l'avis de la communauté sur ses messages et la validation des experts du domaine. Des mécanismes de jeu permettent de gagner des points pour les diverses actions réalisées et de progresser dans les rôles. On peut participer à des défis imaginés par les experts. On peut partager son tableau de réalisations auprès de ses contacts. Prochainement, on pourra annoter les supports de formation comme on le fait sur une version papier, partager les annotations et bénéficier d'une fiche de synthèse créée automatiquement à partie de ses annotations.</p>
			    <h2>Où trouver SupCast ?</h2>
			    <p>iPhone, iPad : http://itunes.apple.com/fr/app/supcast/id507452786?mt=8</p>
				<p>Web : http://supcast.net</p>
				<p>Android : automne 2013.</p>
				<p>Version 2 : automne 2013</p>
				<h2>Ressources disponibles</h2>
				<p>Une dizaine de supports de formation sont d'ores et déjà disponibles : Droit d’auteur ; Présentation en anglais scientifique et technique ; Optique pour l’ingénieur ; e-diagnostic ; Initiation à la modélisation des systèmes automatisés ; Certificat informatique et internet 2i (5 supports). D'autres ressources seront progressivement mises en ligne.</p>
				<p>Actuellement, les supports de formation ont été initialement conçus avec Scenari Opale. Ils ont été transformés le plus automatiquement possible pour s'adapter aux spécificités des smartphones et tablettes (taille d'écran, formats spécifiques, ...), et pour incorporer des balises délimitant les objets d'apprentissage. Les auteurs intéressés pour mettre leur support de formation sur SupCast peuvent nous contacter (cape-contact@mines-nantes.fr). Nous leur fournirons les outils de conversion.</p>
				<h2>Partenaires</h2>
				<p>Institut Mines Télécom (Mines Nantes), Union des Professeurs de Sciences et Techniques Industrielles, Université de Valenciennes et du Haut-Cambresis, Université du Maine. Ce projet est financé par UNIT, l’Institut Mines Télécom et la Mission  Numérique de l'Enseignement Supérieur.</p>
				<h2>Récompenses</h2>
				<p>SupCast a reçu le Grand Prix des “Trophées des Technologies Educatives” Educatec-Educatice 2012 (catégorie enseignement supérieur).</p> 
			</div>
		</div>
		<div id="paper-bottom"></div>
	</div>

	<h1 class="Ressources">Ressources disponibles sur SupCast :</h1>
	<!--<div id="switch">Classer par etablissement</div>-->
	<?php
			include_once("res/mod/shelf.php");
		?>
</section>
<footer>
</footer>
</body>
</html>