jQuery(function(){

	if (jQuery.fn.jquery != "1.9.0") {
		$.noConflict(true);
	}
	$.ajaxSetup({ cache: true});
	//Constant d'id de ressource test
	ID_RES_TEST = "51";
	extendDomManip();
    
	//Initialisation du LanguageMananger
	LanguageM.init();

	includeBar(function () {
		//Update des boutons de la navbar
		updateNavBar(true);
		updateLanguageAppNavBar();
	});


	globals.os = "web";
	globals.modules = globals.dynRes + "mod/";

	//On défini la version à utiliser en fonction de la présence d'OA sur la page
	checkDocumentVersion();

	updatePostOs();
	addAnalytics();

	//Selecteur d'éléments

	if(isScenari())
	{
		globals.selector = ".mainContent_co p, .resInFlow";
		globals.selectorRes = "#btn-bar-messages-section";
		globals.selectorPage = ".mainContent_ti";
		globals.selectorContent = ".mainContent_co";
	}
	else if(isFromLatex()){
		globals.selector = "#globalWrapper span";
		globals.selectorRes = "#btn-bar-messages-section";
		globals.selectorPage = "#globalWrapper";
		globals.selectorContent = "body";
	}
	else if(isSpecialCase()){
		//On ne fait rien, les selecteurs sont déclarés dans la page
	}
	//Sinon, c'est qu'on est sur profeci
	else
	{
		globals.selector = "*[data-oauid]:not(body)";
		// globals.selector = ".feedback, .inner, div.object-properties, a.diagram_link, table.display, table.html-grid";
		globals.selectorRes = "#btn-bar-messages-section";
		globals.selectorPage = ".inner > h2";
		globals.selectorContent = "body";
	}
	globals.selectorAll = globals.selector + ", " + globals.selectorPage + ", " + globals.selectorRes;
	$(globals.selectorAll).addClass('pf-commentable');
	//Position de commentaire
	globals.position = {"top" : "", "left" : ""};

	globals.stateWS = 'disconnected';


	//Initialisation des messages lus
	if(typeof(localStorage.array_messages_lus) == "undefined")
		localStorage.array_messages_lus = JSON.stringify([]);
	
	//On check si l'utilisateur utilise un appareil nomade
	var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.iOS());
	    }
	}

	if(isMobile.any())
	{
		// $('#tplFra').removeClass('tplFra_small');
		displayAppStoreIcon();
	}


	if (typeof(localStorage.langueApp) == "undefined")
		localStorage.langueApp = LanguageM.langueNavigateur ;

	// Recuperation de la langue de l'application dans LanguageManager et mise a jour du tableau des noms des langues
	LanguageM.langueApp = localStorage.langueApp;
	LanguageM.setLangueValuesNameArray();
	J42R.setLang(LanguageM.langueApp).load();

	//Recupération des messages de la page
	recupererMessages(checkURLMessage);

	//Récupération des messages de toutes les pages de la ressource 
	recupererMessagesRessource();

	//On cherche un token d'authentification LTI
	checkForLTIToken();

	if(isFirstTime())
	{
		showAccroche();
		localStorage.newFirstTime = "false";
	}
	
	//Connection au WebSocket
	if(isConnected())
	{
		// bindPusher();
		// bindWS();
	  	//WS hors service
	  	$('#wsIndicator').removeClass().addClass('disconnected').attr('title',J42R.get('web_label_maj_temps_reel_desactivee'));
	  	// Recuperation des langues dans le localStorage
	  	LanguageM.langueUserPrincipale = localStorage.langueUserPrincipale;
	  	LanguageM.languesUserAutres = JSON.parse(localStorage["languesUserAutres"]);
	}

	// //Récup du script de menu contextuel
	// $.getScript(globals.dynRes + "js/jquery.contextMenu.js",function () {
	// 	$.contextMenu({
	// 		zIndex : 30,
	// 		// define which elements trigger this menu
	// 		selector: globals.selectorAll,
	// 		// define the elements of the menu
	// 		items: {
	// 			ecrire: {name: "Commenter", icon:"commenter", callback: function(key,opt){
	// 				displayComForm(key,opt);
	// 			}},
	// 			lire: {name: "Lire les commentaires", icon:"lireCommentaires", callback: function(key, opt){ 
	// 				addFocusedItem(opt.$trigger);
	// 				// globals.focusedItem = opt.$trigger;
	// 				// globals.focusedItem.addClass("selectedItem");
	// 				displayMessages(opt.$trigger); 
	// 			}}
	// 		},
	// 		// there's more, have a look at the demos and docs
	// 		events:{
	// 			show: function(opt){ 
	// 				// addFocusedItem($(this));
	// 				globals.focusedItem = $(this); 
	// 			},
	// 			hide: function(opt){ 
	// 				//this.removeClass('currently-showing-menu'); 
	// 			}
	// 		}
	// 	});
	// });
	
	$(document).ajaxStart(function ()
	{
	    $('body').addClass('pf-wait');

	}).ajaxComplete(function () {

	    $('body').removeClass('pf-wait');

	});

	// Test si c'est une actualisation pour une création de compte suite au changement de la langue de l'application
	if(localStorage.createAccount) {
		// Affichage du formulaire
		displayLoginForm( function () {
			$("#loginBox").fadeOut("fast",function () {
				$("#registerBox").fadeIn("fast");
			});
			// Recupération des données du formulaire
			var dataForm = JSON.parse(localStorage.dataForm);
			$('#register-username').val(dataForm[0]);
			$('#register-password').val(dataForm[1]);
			$('#register-password2').val(dataForm[2]);
			$('#register-email').val(dataForm[3]);
			$('#register-name').val(dataForm[4]);
			$('#register-etablissement').val(dataForm[5]);

			// Suppression des données du localStorage
			localStorage.removeItem('createAccount');
			localStorage.removeItem('dataForm');
		});
	}

	// Test si c'est une actualisation pour une modification de compte suite au changement de la langue de l'application
	if(localStorage.updateAccount) {
		// Affichage du formulaire de modification du profil
		displayProfilConfigPanel(function () {
			// Recupération des données du formulaire
			var dataForm = JSON.parse(localStorage.dataForm);
			$('#register-password-old').val(dataForm[0]);
			$('#register-password').val(dataForm[1]);
			$('#register-password2').val(dataForm[2]);
			$('#register-name').val(dataForm[3]);
			$('#register-etablissement').val(dataForm[4]);

			//Mise à jour des langues
			setLanguesProfilConfig(dataForm[5], dataForm[6]);

			// Suppression des données du localStorage
			localStorage.removeItem('updateAccount');
			localStorage.removeItem('dataForm');
		});
	}

	

	/*****************************************************************************/
	/******************************* WebSockets **********************************/
	/*****************************************************************************/
	function bindWS () {
		var WebsocketClass = function(host){ 
		   this.socket = new WebSocket(host); 
		   this.initWebsocket();
		}; 

		WebsocketClass.prototype = { 
			initWebsocket : function(){ 
                   var $this = this; 
                   this.socket.onopen = function(){ 
                           $this.onOpenEvent(this); 
                   }; 
                   this.socket.onmessage = function(e){ 
                           $this._onMessageEvent(e); 
                   }; 
                   this.socket.onclose = function(){ 
                           $this._onCloseEvent(); 
                   }; 
                   this.socket.onerror = function(error){ 
                           $this._onErrorEvent(error); 
                   };
                   console.log('websocket init ');
           }, 
           _onErrorEvent :function(err){ 
                   console.log('websocket error : '+err); 
           }, 
           onOpenEvent : function(socket){ 
                   console.log('socket opened Welcome - status ' + socket.readyState); 
           }, 
           _onMessageEvent : function(e){ 
				retour = JSON.parse(e.data); 
				console.log('Event : '+retour);

				if(retour.msg.length > 0)
					retour.msg = JSON.parse(retour.msg); 

				console.log('message event lanched '); 
           }, 
           _onCloseEvent : function(){ 
                   console.log('websocket closed - server not running'); 
           },
           bind : function (event_name, callback) {
           		
           }
       }; 
       var socket = new WebsocketClass('ws://imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/ws/serveur.php'); /* on instancie un objet WebsocketClass avec l'URL en paramètre */ 
	}

	function bindPusher () {
		//Init Pusher
		$.getScript("http://js.pusher.com/2.1/pusher.min.js", function () {
    		globals.messageSound = new Audio(globals.dynRes + 'snd/bell.mp3');
			var pusher = new Pusher('0b0142b03d240425e90f');
		    var channel = pusher.subscribe('pairform');
		    pusher.connection.bind('state_change', function(states) {
			  // states = {previous: 'oldState', current: 'newState'}
			  globals.stateWS = states['current'];

			  if(states['current'] == "connected")
			  	$('#wsIndicator').removeClass().addClass('connected').attr('title',J42R.get('web_label_maj_temps_reel_activee'));
			  else if(states['current'] == "disconnected")
			  	$('#wsIndicator').removeClass().addClass('disconnected').attr('title',J42R.get('web_label_maj_temps_reel_desactivee'));
			  else
			  	$('#wsIndicator').removeClass().addClass('unsure').attr('title',J42R.get('web_label_maj_temps_reel'));
			});
		    pusher.connection.bind( 'error', function( err ) { 
			  if( err.data.code === 4004 ) {
			    displayAlert(J42R.get('web_label_maj_temps_reel'),'info');

			    globals.stateWS = "disconnected";
			  	$('#wsIndicator').removeClass().addClass('disconnected');
			  }
			});
		    channel.bind('new_vote', function(data) {
		    	updateVoteCount(data.id_message, data.voteCount);
		    });
		    channel.bind('new_message', function(data) {
		    	//Si ce nouveau message concerne la ressource actuelle
		    	if(data.id_ressource == globals.id_res)
		    	{
		    		var urlComponents = window.location.pathname.split('/')
		    		var currentPageName = urlComponents[urlComponents.length - 1].slice(0,-5);

		    		//Si ce message concerne la page courante
		    		if (data.nom_page == currentPageName)
		    			recupererMessages();
		    		else
		    			recupererMessagesRessource();

		    		if (typeof(data.modified) == "undefined")
						globals.messageSound.play();
		    	}
		    });
			channel.bind('suppr_message', function(data) {
		    	//Si ce nouveau message concerne la ressource actuelle
		    	if(data.id_ressource == globals.id_res)
		    	{
	    			recupererMessages();
	    			recupererMessagesRessource();

		    		if (data.id_user_op == localStorage.id) {
		    			data.supprime_par ? displayAlert(J42R.get('web_label_message_supprime'),'info') : displayAlert(J42R.get('web_label_message_recupere'),'info');
		    		}
					// globals.messageSound.play();
		    	}
		    });

		    channel.bind('new_points', function(data) {
		    	if (data.id_utilisateur == localStorage.id) {
    				//TODO: inclure le nom de la catégorie
		    		updatePoints(data.id_ressource, data.id_categorie, data.score);
		    	}
		    });

		    channel.bind('new_success', function(data) {
		    	if (data.id_utilisateur == localStorage.id) {
    				//TODO: inclure le nom de la catégorie
		    		displaySuccess(data.nom, data.description, data.image_blob);
		    	}
		    });
		});
	}
    
	/*****************************************************************************/
	/******************************* Binds **********************************/
	/*****************************************************************************/

	//Enlever toutes les actions par défauts sur les éléments commentables, pour les activer au double clic :)
	// $('.pf-commentable').each(function clickReplace(){
	//     $this = $(this);
	//     var list_of_events = $._data(this, "events");
	//     if (typeof(list_of_events) != "undefined") {
	// 	    $.each(list_of_events, function(i, event) {
	// 	        if(i == "click"){
	// 	            $.each(event, function (index, event_object){
	// 	                if(event_object.handler.name !== "handlePFClick"){
	// 	                   $this.dblclick(event_object.handler); 
	// 	                }
	// 	            });
	// 	            $this.off('click');
	// 	        }
	// 	    });
	//     }
	// });

	
	//Stop de la propagation des clics dans les vidéos et les eWeb, pour éviter l'affichage du panneau commentaire lors d'un clic dans l'élément
	//TODO : solution provisoire
	$('video, .eWeb').click(function(e){
		e.stopPropagation();
	});
	$(document).on('click', globals.selectorAll, function handlePFClick(event) {
		addFocusedItem($(this));
		// globals.focusedItem = opt.$trigger;
		// globals.focusedItem.addClass("selectedItem");
		displayMessages($(this)); 
		event.stopPropagation();
	});
	//Profil bouton hover, display du menu
	$(document).on('mouseenter', "#btn-profil", function () {
		if (!$("#panel-Profil").is(':visible')){
			$("#panel-Profil").css('display','block').animate({'top':'34px', 'opacity':'0.98'}, 150, 'swing');
		}
		if (typeof(globals.timer_profil) != "undefined")
			clearTimeout(globals.timer_profil);
	}).on('mouseleave', '#btn-profil', function () {
		if ($("#panel-Profil").is(':visible')){
			if (typeof(globals.timer_profil) != "undefined")
				clearTimeout(globals.timer_profil);

			globals.timer_profil = setTimeout(function () {
				$("#panel-Profil").animate({'top':'44px', 'opacity':'0'}, 150, 'swing', function () {
					$(this).hide();
				});
			},400);
		}
	});

	//Même chose, mais pour le menu de la langue de l'application
	$(document).on('mouseenter', "#btn-bar-langue", function () {
		if (!$("#panel-Langue-App").is(':visible')){
			$("#panel-Langue-App").css('display','block').animate({'top':'34px', 'opacity':'0.98'}, 150, 'swing');
		}
		if (typeof(globals.timer_langue_app) != "undefined")
			clearTimeout(globals.timer_langue_app);
	}).on('mouseleave', '#btn-bar-langue', function () {
		if ($("#panel-Langue-App").is(':visible')){
			if (typeof(globals.timer_langue_app) != "undefined")
				clearTimeout(globals.timer_langue_app);

			globals.timer_langue_app = setTimeout(function () {
				$("#panel-Langue-App").animate({'top':'44px', 'opacity':'0'}, 150, 'swing', function () {
					$(this).hide();
				});
			},400);
		}
	});
	
	//Même chose, mais pour le menu PairForm
	$(document).on('mouseenter', "#btn-menu", function () {
		if (!$("#panel-menu").is(':visible')){
			$("#panel-menu").css('display','block').animate({'top':'34px', 'opacity':'0.98'}, 150, 'swing');
		}
		if (typeof(globals.timer_menu) != "undefined")
			clearTimeout(globals.timer_menu);
	}).on('mouseleave', '#btn-menu', function () {
		if ($("#panel-menu").is(':visible')){
			if (typeof(globals.timer_menu) != "undefined")
				clearTimeout(globals.timer_menu);

			globals.timer_menu = setTimeout(function () {
				$("#panel-menu").animate({'top':'44px', 'opacity':'0'}, 150, 'swing', function () {
					$(this).hide();
				});
			},400);
		}
	});
	$(document).on('click', "#btn-logout", function () {
		logOut();
	});
	//Yo
	$(document).on('click', "#btn-login", function () {
		displayLoginForm();
	});

	$(document).on('click', '#btn-reinitmesslus', function () {
		localStorage.array_messages_lus = JSON.stringify([]);
		displayAlert(J42R.get('web_label_messages_lus_reinitialises'), 'info');

		setTimeout(function () {
			location.reload();
		},3000);
	});
	$(document).on('click', '#btn-refreshPage', function () {
		recupererMessages();
		displayAlert(J42R.get('web_label_messages_actualises'),'valid');

	});

	$(document).on('click', '#btn-refreshAll', function () {
		recupererMessagesRessource();
		displayAlert(J42R.get('web_label_messages_actualises'),'valid');
	});

	$(document).on('click', "#btn-menu-messages", function () {
		displayMessages();
	});

	$(document).on('click', "#btn-menu-presentation", function () {
		showPresentation();
	});
	
	
	$(document).on('click', "#btn-mon-profil", function () {
		// displayAllMessages();
		displayProfil();
	});
	$(document).on('click', '#btn-menu-recherche', function () {
		displayRechercheProfil();
	});

	//Bind Dynamiques
	$(document).on("click","#btn-profil-fermer", function () {
		// displayAllMessages();
		hideProfil();
	});
	
	$(document).on('mouseenter', "#comBar", function () {
		if (globals.comBarTimer)
			clearTimeout(globals.comBarTimer);

		$(this).animate({opacity : 0.98}, 200);
	}).on('mouseleave', '#comBar', function () {
		if (globals.comBarTimer)
			clearTimeout(globals.comBarTimer);
		
		globals.comBarTimer = setTimeout(function () {
			$('#comBar').animate({opacity : 0.20}, 400);
		}, 1000);
	});

	$(document).on("click","#writeCom",function () {
		displayInlineComForm();
	});

	$(document).on("click",".comRepondre",function () {
		displayInlineComForm($(this).closest('.comBox'));
	});

	$(document).on("click","#comBar .imgProfil, #comBar .comUserName",function () {
		var id_auteur = $(this).closest('.comBox').data('id_auteur');
		displayProfil(id_auteur);	
	});
 	$(document).on("click","#alertMod, .comAjouterTags",function () {
		displayAlert(J42R.get('web_label_fonctionnalité_indisponible'), "info", this);
	});

 	$(document).on("click","#btn-profil-mon-reseau",function  () {
 		displayReseaux('gestion');
 	});
 	
 	$(document).on("click","#btn-profil-ajouter-reseau",function  () {
 		displayReseaux('ajout');
 	});
 	
 	$(document).on("click","#btn-profil-config",function  () {
 		displayProfilConfig();
 	});
 	
 	$(document).on("click","#btn-profil-config-valider",function  () {
 		var passwordFieldsFilled = $('#profil-config-form input[name*=password]').map(function () {
 			return $(this).val().trim() == "" ? null : this;
 		});

 		if ((passwordFieldsFilled.length > 0) && (passwordFieldsFilled.length < 3)){
 			displayAlert(J42R.get('web_label_renseigner_champ'), 'info',$('#profil-config-form input[name*=password]').not(passwordFieldsFilled));
 		}
 		else{
	 		var queryString = $('#profil-config-form input').map(function () {
	 			return $(this).val().trim() == "" ? null : this;
	 		}).serialize();
 			editProfilConfig(queryString);
 		}
 	});

 	$(document).on("click","#btn-profil-config-fermer",function  () {
 		hideProfilConfig();
 	});
 	
 	$(document).on('click',".reseaux-add-item", function () {
 		showNetworkInlineForm($(this));
 	});

 	$(document).on('click',".reseaux-cancel-item", function () {
 		hideNetworkInlineForm($(this));
 	});
 	
 	$(document).on('click',".reseaux-add-to-item", function () {
 		var reseaux_panel = $('#reseaux-panel-container');
 		var id_utilisateur = reseaux_panel.data('id_utilisateur');
		var username = reseaux_panel.data('username');
		var user_avatar_src = reseaux_panel.data('user_avatar_src');
		var selected_network = $(this).parents('.reseaux-item');
		addUserToNetwork(id_utilisateur, username, user_avatar_src, selected_network); 
 	});

 	$(document).on('click',".reseaux-select-item", function () {
 		var id_collection = $(this).parents('.reseaux-item').data('id_collection');
 		var collection_name = $(this).siblings('.reseaux-item-label').html();
 		addNetworkToMessageOptions(id_collection, collection_name);
 	});

	$(document).on('click',".reseaux-valid-item", function () {
 		createNewNetwork($(this));
 	});

	$(document).on('click',".reseaux-delete-item", function () {
 		deleteNetwork($(this).parents('.reseaux-item').data('id_collection'));
 	});

 	$(document).on('click',".reseaux-add-user", function () {
 		var collection = $(this).parents('.reseaux-item');
 		displayRechercheProfilForNetworkAdd(collection);
 	});
	
 	$(document).on('click',".reseaux-delete-user", function () {
 		removeUserFromNetwork($(this).parents('.reseaux-item').data('id_collection'),$(this).parent().data('id_utilisateur'));
 	});

 	$(document).on('click',".reseaux-user > img", function () {
 		displayProfil($(this).parent().data('id_utilisateur'));
 	});
	
	$(document).on('click',"#btn-reseaux-fermer", function () {
 		hideReseaux();
 	});

	$(document).on('click',".profil-search-add-user",function () {
		var profil_search_result_user = $(this).parents('.profil-search-result-user');
		var id_utilisateur = profil_search_result_user.data('id_user');
		var username = profil_search_result_user.find('.profil-search-username').html();
		var user_avatar_src = profil_search_result_user.find('.profil-search-img').attr('src');

		addUserToNetwork(id_utilisateur, username, user_avatar_src); 
	});
 	

	$(document).on("click",".comSuppr",function () {
		supprMessage($(".comBox").has(this));
	});
	$(document).on("click",".comSupprDef",function () {
		supprMessage($(".comBox").has(this), true);
	});
	$(document).on("click","#btn-menu-aide, #btn-bar-aide",function () {
		showTutorial();
	});
	
	$(document).on("click","#btn-menu-ressources",function () {
		displayRessources();
	});

	//Même chose, mais pour le menu PairForm
	$(document).on('click', ".btn-bar-notifications", function () {
		displayNotifications(this);
	});
	$(document).on("click","li.notification",function (evt) {
		var notif_data = $(this).data();
		handleNotificationClick(notif_data['type'],
								notif_data['id_utilisateur'],
								notif_data['id_message'],
								notif_data['contenu'],
								notif_data['sous_type']);
	});

	$(document).on('click',"#btn-ressources-fermer", function () {
 		hideRessources();
 	});

	$(document).on('click',"#btn-ressources-switch", function () {
 		switchRessourcesSort();
 	});

	$(document).on("click","#comCloser",function () {
		// $("#comBar").fadeOut();
		if ($('.message-options-container').length){
			$('.message-options-container').fadeOut(function () {
				$(this).remove();
			});
		}

		if($('.message-language-container').length) {
			$('.message-language-container').fadeOut(function () {
				$(this).remove();
			});
		}

		$('#comBar').animate({'bottom':'-700px', 'opacity':'0'}, function () {
			 		$('#comBar').remove();
			 	});
		removeFocusedItem();
	});
	
	$(document).on("click","#com-detach",function () {
		detachComBar();
	});

	$(document).on("click","#com-sort",function () {
		changeComSort();
	});

	$(document).on("click",".vote",function () {
		voteMessage($(this));
	});

	$(document).on("click",".comEdit",function () {
		editerMessage($(this).closest('.comBox'));
	});
	$(document).on("click",".comDonnerMedaille",function () {
		$(".comDonnerMedaille-container").remove();

		var medaille_selector = $('<div>', {class:'comDonnerMedaille-container ui-tooltip-bottom'}).append(
				$('<img>', {class:'comDonnerMedaille-option', src:globals.dynRes+'img/medaille_or.png', title: J42R.get('web_label_attribuer_medaille_or')})
				.data('option','or')
			).append(
				$('<img>', {class:'comDonnerMedaille-option', src:globals.dynRes+'img/medaille_argent.png', title: J42R.get('web_label_attribuer_medaille_argent')})
				.data('option','argent')
			).append(
				$('<img>', {class:'comDonnerMedaille-option', src:globals.dynRes+'img/medaille_bronze.png', title: J42R.get('web_label_attribuer_medaille_bronze')})
				.data('option','bronze')
			).append(
				$('<img>', {class:'comDonnerMedaille-option', src:globals.dynRes+'img/error.png', title: J42R.get('label_supprimer_medaille')})
				.data('option'
				,'enlever'));

		medaille_selector
		.appendTo($(this))
		.css({opacity:0, marginTop: "-65px", marginLeft: "-120px"})
		// .position({
		// 	my: "center",
		// 	at: "center",
		// 	of: $(this)
		// })
		.animate({opacity:1, marginTop: "-85px"}, 'fast')
		.mouseleave(function(){
			$(".comDonnerMedaille-container").animate({opacity:0}).remove();
		});
	});

	$(document).on("click",".comModifierLangue",function () {
		if($(this).find(".comModifierLangue-container").length == 0) {
			var message_language = $('<div>', {class:'comModifierLangue-container ui-tooltip-bottom'});
			var list_language = $('<select>', {id:'message-set-language-list'});

			for(var i=0; i<LanguageM.arrayLanguesValuesName.length; i++) {
				var tmp_option = $('<option>', {value:LanguageM.arrayLanguesValuesId[i]});
				tmp_option.text(LanguageM.arrayRealLanguesValuesName[i]);
				list_language.append(tmp_option);
			}

			message_language.append(list_language);

			message_language
			.appendTo($(this))
			.css({opacity:0, marginTop: "-15px", marginLeft: "-55px"})
			.animate({opacity:1, marginTop: "-35px"}, 'fast')
			.mouseleave(function(){
				$(".comModifierLangue-container").animate({opacity:0}).remove();
			});

			// Mise à jour de la langue selectionnée
			var currentIdLangueSelect = $(this).closest(".comBox").attr('data-id_langue');
			$(this).find('#message-set-language-list option[value="'+ currentIdLangueSelect + '"]').prop('selected', true);
		}
	});

	$(document).on("click",".message-options-cog",function () {
		displayMessageExtras($(this));
	});

	$(document).on("click","#message-langue",function () {
		displayMessageChoiceLanguage($(this));
	});

	$(document).on("click",".message-visibilite-add-item",function () {
		displayReseaux('select');
	});

	$(document).on("click",".message-visibilite-item",function () {
		if (!$(this).hasClass('message-visibilite-add-item') && !$(this).hasClass('message-visibilite-item-public'))
			removeNetworkOfMessageOptions($(this));
	});

	$(document).on("change","#message-language-list",function (e) {
		var codeLangueSelect = this.options[e.target.selectedIndex].value;
		// Mise à jour de l'icone du drapeau du message
		$('#message-langue-img').attr('src', globals.dynRes +'img/langue/icone_drapeau_rond_'+ codeLangueSelect +'.png');
		// Mise à jour de l'input hidden de la langue
		$('#formComBar input[name="langue"]').val(LanguageM.idLangueWithCode(codeLangueSelect));
		// Desactive le popup de choix de la langue
		$(".message-language-container").animate({opacity:0}).remove();
	});

	$(document).on("change","#message-set-language-list",function (e) {
		modifierLangueMessage($(this).closest('.comBox'), this.options[e.target.selectedIndex].value);
	});

	$(document).on("click",".onoffswitch-checkbox",function () {

		var inputDefi = $('input[name="est_defi"]');
		var newVal = inputDefi.val() == "-1" ? "1" : "-1";

		inputDefi.val(newVal);
		// displayAlert($('input[name=onoffswitch]').val(),'info');
	});

	$(document).on("click",".comDonnerMedaille-option",function () {
		donnerMedaille($(this).closest('.comBox'), $(this).data('option'));
	});
	$(document).on("click",".comValiderMessage",function () {
		validerMessage($(this).closest('.comBox'));
	});
	// $(document).on("click",".comAjouterTags",function () {
	// 	ajouterTags($(this).closest('.comBox'));
	// });
	$(document).on("click",".comTerminerDefi",function () {
		terminerDefi($(this).closest('.comBox'));
	});
	$(document).on("click",".comLinkToRes",function () {
		checkURLMessage();
	});

	$(document).on('click',"#profil-info-avatar-label",function () {
		changerAvatar();
	});

	$(document).on('click',".profil-ressource-upgrade",function () {
		displayRankUpgrade($(this));
	});

	$(document).on('click',"#btn-profil-messages",function () {
		var id_user = $('#profil-info').data('id_user');
		displayMessages(undefined,undefined,id_user);
	});
	
	
	$(document).on("search","#profil-search-field input",function () {
		displayRechercheProfilResults($(this).val());
	});

	$(document).on('click',".profil-search-result-user",function () {
		var id_user = $(this).data('id_user');
		displayProfil(id_user);	
	});
	

	$(document).on("click","#btn-profil-search-trigger", function () {
		displayRechercheProfilResults($('#profil-search-field input').val());
	});

	$(document).on("click","#btn-profil-search-fermer", function () {
		hideProfilRecherche();
	});

	$(document).on("click","#profil-succes-container", function () {
		var id_user = $('#profil-info').data('id_user');
		displaySuccessBar(id_user);
	});

	$(document).on("click","#btn-menu-succes", function () {
		displaySuccessBar();
	});

	$(document).on("click","#btn-succes-fermer", function () {
		hideSuccessBar();
	});

	$(document).on("click","#btn-pres-creer-compte, #btn-accr-creer-compte", function () {
		removeOverlay(displayLoginForm);
	});

	$(document).on("click","#btn-afficher-plus", function () {
		removeOverlay(showPresentation);
	});
		

	$(document).on("click",".btn-pres-fermer", function () {
		removeOverlay();
	});
	
	$(document).on("click",".btn-cross-fermer", function () {
		removeOverlay();
	});

	$(document).on("click","#btn-menu-notifications", function () {
		changerNotificationStatus();
	});
	
	$(document).on("click","#btn-menu-noms-reels", function () {
		changerNomsReelsStatus();
	});

	$(document).on("click",".btn-change-langueApp", function () {
		changerLangueApp($(this).attr('cl'));
	});

	
	$.fn.bindScrollHandler = function(){
		$(this).scroll(function  () {
			var scroll = $(window).scrollTop();
		    if (scroll > 0) {
		        $(this).addClass("active");
		    }
		    else {
		        $(this).removeClass("active");
		    }
		});
	}


	/*****************************************************************************/
	/********************************** User *************************************/
	/*****************************************************************************/
	function includeBar (callback) {
	    // Recuperation de la langue de l'application
	    LanguageM.init();
	    var tmpLangueApp = localStorage.langueApp;
	    if(tmpLangueApp == null)
	    	tmpLangueApp = LanguageM.langueNavigateur;

	    //Inclusion de la barre;
	    var navBar = $('<nav></nav>').css({opacity:0, top:-40}).load(globals.dyn + 'includes.php', { 'langueApp': tmpLangueApp }, function() {
	        // includeHead(globals.dynRes + "/js/functions.js");
	        $('body').prepend(this);
	        
	        var page_title;

	        if (isScenari())
	        	page_title = $('#titleRoot span').html();
	        else
	        	page_title = $('head title').html();
			$('#label-title h3').html(page_title);
	        if (typeof(callback) == "function")
	        	callback();

	        $(this).animate({opacity:1, top:0},300,'swing');
	    });

	}
	
	function displayLoginForm (callback) {

		var connectProfil = $('<section class="popinWrapper" id="formContainer"></section>').load( globals.modules + 'connectProfil.php', { 'langueApp': LanguageM.langueApp }, function () {

			var wrapper = addOverlay(connectProfil);
			
			$(".popin").position({
				my: "center center",
				at: "center center",
				of: wrapper
			});

			// Mise à jour des listes
			setLanguesRegister(LanguageM.langueApp, []);

			// Sauvegarde temporaire des langues dans le localStorage
			localStorage.tmpLangueP = LanguageM.langueApp;
			localStorage.tmpLangueA = JSON.stringify([]);

			// Lancement du callback s'il existe
			if (typeof(callback) == "function")
	        	callback();

			$('#register-langue').change(function(e) {
				var newLP = this.options[e.target.selectedIndex].value;
				localStorage.tmpLangueP = newLP;
				var newLA = JSON.parse(localStorage.tmpLangueA);
				if(newLA.indexOf(newLP) != -1)
					newLA.splice(newLA.indexOf(newLP), 1);
				localStorage.tmpLangueA = JSON.stringify(newLA);
				setLanguesRegister(newLP, newLA);

				// Affichage du popup pour changer la langue de l'application
				J42R.setLang(localStorage.tmpLangueP, false);
				J42R.load();
				$('#popin-change-langue-app-register').find('h1').text(J42R.get('title_langue_application'));
				$('#popin-change-langue-app-register').find('p').text(J42R.get('label_changer_langue_app'));
				$('#btn-no-change-langue-app-register').text(J42R.get('button_non'));
				$('#btn-yes-change-langue-app-register').text(J42R.get('button_oui'));
				if($("#popin-change-langue-app-register").css('display') == "none") {
					$("#popin-change-langue-app-register").css('display', 'block'); 
				}
			});

			$('#btn-no-change-langue-app-register').click(function () {
				$("#popin-change-langue-app-register").css('display', 'none');
				J42R.setLang(LanguageM.langueApp, false);
				J42R.load();
			});

			$('#btn-yes-change-langue-app-register').click(function () {
				$("#popin-change-langue-app-register").css('display', 'none');
				// Modification de la langue de l'application
				localStorage.langueApp = localStorage.tmpLangueP;

				// Enregistrement des données du formulaire
				var dataForm = new Array();
				dataForm.push($('#register-username').val());
				dataForm.push($('#register-password').val());
				dataForm.push($('#register-password2').val());
				dataForm.push($('#register-email').val());
				dataForm.push($('#register-name').val());
				dataForm.push($('#register-etablissement').val());

				// Actualisation de la page
				localStorage.createAccount = true;
				localStorage.dataForm = JSON.stringify(dataForm);

				window.location.reload();
			});

			$('#btn-register-autres-langues').click(function () {
				if($("#popin-choix-autres-langues-register").css('display') == "none") {
					$("#popin-choix-autres-langues-register").css('display', 'block'); 
				}
			});

			$('#btn-ok-choice-other-languages-register').click(function () {
				$("#popin-choix-autres-langues-register").css('display', 'none');
			});


			$('.logSwitch').click(function () {
				if($("#registerBox").css('display') == "none")
					$("#loginBox").fadeOut("fast",function () {
						$("#registerBox").fadeIn("fast");
					});
				else
					$("#registerBox").fadeOut("fast",function () {
						$("#loginBox").fadeIn("fast");
					});
			});

			$('#passwordForgotSwitch').click(function () {
				$("#loginBox").fadeOut("fast",function () {
					$("#passwordForgotBox").fadeIn("fast");
				});
			});

			wrapper.fadeIn();
			connectProfil.children().scaleIn();

			//Submit Login
			$("#formLog").submit(function () {
				logUserWithParams($("#formLog").serialize()+globals.post_OS);
				return false; // ne change pas de page
			});
			
			$('#formPasswordForgot').submit(function  () {
				var postData = $("#formPasswordForgot").serialize();
				$.post( globals.webServices + "compte/resetPassword.php", postData,function(data){
					var retour = $.parseJSON(data);
					if (retour['status'] == 'ko')
					{
						$('.popin').effect("shake");
						displayAlert(retour['message'],'error');
					}
					else{
				   		removeOverlay();
						displayAlert(J42R.get('label_creation_de_compte_reussi'), "valid");
					}
				});
				return false; // ne change pas de page
			});
			$("#formRegister").submit(function () {
				if (!$('#formRegister input[name=register-cgu]').is(':checked')){
					displayAlert(J42R.get('web_label_accepter_cgu'),'error', $('#formRegister input[name=register-cgu]'));
					return false;
				}
				var postData = $("#formRegister").serialize()+'&id_ressource='+globals.id_res+globals.post_OS;
				$.post( globals.webServices + "compte/registerCompte.php", postData,function(data){
				   var retour = $.parseJSON(data);
				   if (retour['status'] == 'ko')
				   {
						$('.popin').effect("shake");

						if (retour['message'] == "empty") 
							displayAlert(J42R.get('label_erreur_saisie_champ_vide'),'error');

						else if (retour['message'] == "username") 
							displayAlert(J42R.get('web_label_erreur_nom_utilise') + "\n" + J42R.get('web_label_erreur_choisir_autre'),'error');

						else if (retour['message'] == "email")
							displayAlert(J42R.get('web_label_erreur_mail_utilise') + "\n" + J42R.get('web_label_erreur_saisir_autre'),'error');
						else if (retour['message'] == "registerbad")
							displayAlert(J42R.get('web_label_erreur_enregistrement'),'error');
						else
							displayAlert(retour['message'],'error');
				   }
				   else
				   {
				   		// Enregistrement des langues de l'utilisateur
				   		var queryLanguageString =  "id_utilisateur="+ retour['id_utilisateur'] +"&langue_principale="+ LanguageM.idLangueWithCode(localStorage.tmpLangueP) +"&autres_langues="+ JSON.stringify(LanguageM.arrayLanguageIdWithArrayLanguageCode(JSON.parse(localStorage.tmpLangueA)));
		
						$.post(globals.webServices + 'compte/editerLangues', queryLanguageString+globals.post_OS, function (data) {
							var retour = JSON.parse(data);
							if (retour['status'] == 'ok'){
								displayAlert(J42R.get('web_label_inscription_terminee'), "valid");
						   		$('#registerBox').fadeOut(function () {
						   			$('#postRegisterBox').fadeIn();
						   		});
							} else {
								displayAlert(J42R.get('web_label_erreur_modif_langues'), 'error');
							}
						});

  						ga('send', 'event', 'user', 'register', $('#formRegister input[name=email]').val());

				   		/*	
				   		var user = $.parseJSON(dataJSON);
				   		//On cale l'ID elgg et le nom dans le stockage local
						if(localStorage)
						{
							localStorage.id = user.id;
							localStorage.username = user.username;
							localStorage.rank = JSON.stringify(user.rank);
						}

						updateNavBar(true);
						
						removeOverlay();*/
				   }
				});
				return false; // ne change pas de page
			});
		});
	}

	$(document).on( "change", "#list-other-languages-register input:checkbox", function() {
		var newLA = JSON.parse(localStorage.tmpLangueA);
	    if($(this).is(':checked')){
	        newLA.push($(this).attr('value'));
	    } else {
	        newLA.splice(newLA.indexOf($(this).attr('value')), 1);
	    }
	    localStorage.tmpLangueA = JSON.stringify(newLA);
	});

	function setLanguesRegister(languePrincipale, autresLangues) {
		// Mise à jour de la liste des langues
		$('#register-langue').html("");
		for(var i=0; i<LanguageM.arrayLanguesValuesName.length; i++) {
			$('#register-langue').append('<option value="'+ LanguageM.codeLangueWithName(LanguageM.arrayLanguesValuesName[i]) +'">'+ LanguageM.arrayRealLanguesValuesName[i] +'</option>');
		}
		$('#register-langue option[value="'+ languePrincipale +'"]').prop('selected', true);

		
		// Mise à jour de la liste des autres langues
		var arrayOtherLanguagesWithoutLP = LanguageM.arrayLanguesValuesCode.slice();
		arrayOtherLanguagesWithoutLP.splice(arrayOtherLanguagesWithoutLP.indexOf(languePrincipale), 1);

		$('#list-other-languages-register').html("");
		for(var i=0; i<arrayOtherLanguagesWithoutLP.length; i++) {
			$('#list-other-languages-register').append('<li><img src="'+ globals.dynRes + 'img/langue/icone_drapeau_rond_' + arrayOtherLanguagesWithoutLP[i] +'.png" alt=""/> <h3 class="name-langue">'+ LanguageM.nameRealLangueWithCode(arrayOtherLanguagesWithoutLP[i]) +'</h3> <input type="checkbox" name="autres-langues" value="'+ arrayOtherLanguagesWithoutLP[i] +'"></li>');
			var j = autresLangues.indexOf(arrayOtherLanguagesWithoutLP[i]);
			if(j != -1) {
				$('#list-other-languages-register input[value='+ autresLangues[j] +']').attr('checked', true);
			}	
		}
	}


	function logUserWithParams (params, should_not_remove_overlay) {
		params += "&capsules_utilisateur_mobile=["+globals.id_res+"]";
		$.post( globals.webServices + "compte/loginCompte.php",params,function(data){
		   //user.id = idElgg. If "" alors erreur
		   //user.username = pseudo rentré
		   var retour = $.parseJSON(data);
		   if (typeof(retour['status']) == 'undefined')
		   {
		   		var user = retour;
				//On cale l'ID elgg et le nom dans le stockage local
				if(localStorage)
				{
					localStorage.id = user.id;
					localStorage.username = user.username;
					localStorage.rank = JSON.stringify(user.rank);
					//Hack de lastcache pour empêcher le cache de l'avatar.
					//Supprimer ce parametre permet d'indiquer au webService d'Elgg qu'on ne l'a pas caché avant.
					// localStorage.avatar_url = user.avatar_url.replace('lastcache','foo');
					localStorage.avatar_url = user.avatar_url;
					//Enregistrement des langues dans le localstorage et dans LanguageManager
					localStorage.langueUserPrincipale = LanguageM.langueUserPrincipale = user.langue_principale;
					LanguageM.languesUserAutres = LanguageM.arrayFromBDDOtherLanguages(user.autres_langues);
					localStorage.languesUserAutres = JSON.stringify(LanguageM.languesUserAutres);
				}

				localStorage.elggperm = user.elggperm;

				displayAlert(J42R.get('label_connexion_reussi'),'valid');
				setCookie(24);
				updateNavBar(true);
				updatePostOs();
				// bindPusher();

				if(!should_not_remove_overlay)
					removeOverlay();

				//Mise à jour des messages de la page
				recupererMessages(checkURLMessage);
				recupererMessagesRessource();
				ga('send', 'event', 'user', 'login_success', $('#formLog input[type=username]').val());

		   }
		   else
		   {
				$('#formLog input[type=password]').val("");
				$('#popin-connexion').effect("shake");
				var message;
				
				if(retour['message'] == "utilisateur_invalide")
					message = J42R.get('title_erreur_identifiants');

				displayAlert(message, "error");

				ga('send', 'event', 'user', 'login_fail', $('#formLog input[type=username]').val());
		   }
		  
		});
	}

	function setCookie (minutes) {
		
		var date = new Date();
		date.setTime(date.getTime() + (minutes * 60 * 1000));

		$.cookie('elggperm', localStorage.elggperm, {expires : date, path : '/'});
	}
	function checkDocumentVersion () {
		//S'il y a des OA definis sur la page, et qu'on a un id de page
		if ((($('[data-oauid]:not([data-oauid=""])').length != 0) || ($('meta[name="uid_page"]').not('[content=""]').length != 0))
			|| !(isScenari() || isSpecialCase() || isFromLatex()))
		{
			//On utilisera la version de base d'attachement de messages
			globals.version = "1.5";
			globals.styleAttacheMessages = "OA";
			globals.uid_page = $('meta[name="uid_page"]').attr('content');
		}
		//S'il n'y a pas d'OA definis sur la page
		else
		{
			//On utilisera la version avance d'attachement des messages par identifiants uniques
			globals.version =  "1";
			globals.styleAttacheMessages = "AUTO";
		}
		if (globals.id_res == ID_RES_TEST)
			displayAlert("Attention, vous êtes dans un environnement de test ; les messages ont une durée d'expiration d'une heure.","info");
	}

	function updatePostOs () {

		//Valeurs clés
		globals.post_OS = "&os=" + globals.os + "&version=" + globals.version;
		if ($.cookie != undefined) {
			globals.post_OS += $.cookie().elggperm ? '&elggperm=' + $.cookie().elggperm : '';
		}
	}

	function logOut () {
		
		if (globals.loginOut == false || typeof(globals.loginOut) == "undefined"){
			
			globals.loginOut = $.post( globals.webServices + "compte/logOut.php",globals.post_OS,function (dataJSON)
			{			
				globals.loginOut = false;
				ga('send', 'event', 'user', 'logout_success', localStorage.username);
				//localStorage.clear();
				localStorage.id = "";
				localStorage.rank = "";
				localStorage.username = "";
				$.cookie().elggperm = '';

				updateNavBar(true);
				updatePostOs();

				//Mise à jour des messages de la page
				recupererMessages(checkURLMessage);
				recupererMessagesRessource();
			});		
		}


	}
	//Retourne l'id si connecté, sinon false
	function isConnected (){
		if(localStorage)
		{
			//Si l'user est volontairement déconnecté
			if((typeof(localStorage.id) == "undefined") 
				|| (typeof(localStorage.username) == "undefined")
				|| (typeof(localStorage.rank) == "undefined")
				|| (localStorage.id == "")
				|| (localStorage.username == "")
				|| (localStorage.rank == ""))
			{
				return false;
			}
			//Si sa session a expiré
			else if (localStorage.elggperm != $.cookie().elggperm) {
				displayAlert(J42R.get('web_label_session_expiree'),'error');
				logOut();
				return false;
			} 
			//S'il est connecté
			else return localStorage.id;
		}		
	}

	//Retourne l'id si connecté, sinon false
	function isFirstTime (){
		if(localStorage)
		{
			if(typeof(localStorage.newFirstTime) == "undefined") 
			{
				// displayAlert('Attention, votre session a expir&eacute;e. Veuillez vous reconnecter.','error');
				// logOut();
				return true;
			} 
			else 
				return false;
		}
		displayAlert( J42R.get('web_label_navigateur_incompatible') + '\n\n' + J42R.get('web_label_navigateur_incompatible_suite'), 'error');
		return true;
	}
	function isAdmin () {
		if(getRank() == 5) 
			return true;
		else return false;
	
	}
	//Param : type = "id" || "string" ; default "id"
	//Retourne soit l'id soit le nom du niveau utilisateur
	function getRank (type) {
		if(localStorage)
		{
			if(typeof(localStorage.rank) == "undefined"
				|| (localStorage.rank == ''))
				return 0;

			var rankArray =  JSON.parse(localStorage.rank);
			
			if(typeof(rankArray[globals.id_res]) == 'undefined')
			{
				rankArray[globals.id_res] = {'id_categorie':"0",'nom_categorie':'lecteur','score':"0", 'notification_mail' :'1'};
				localStorage.rank = JSON.stringify(rankArray);
			}
				

			if(typeof(type) == "undefined" || type == "id")
				return parseInt(rankArray[globals.id_res]["id_categorie"]);
			
			else if(type == "string")
				return rankArray[globals.id_res]["nom_categorie"];
		}
	}

	/*****************************************************************************/
	/********************************** Langue *************************************/
	/*****************************************************************************/
	
	function updateLanguageAppNavBar() {
		$("#img-langueApp").attr("src", globals.dynRes + "img/langue/icone_drapeau_rond_"+ LanguageM.langueApp +".png");
		
		for(var i=0; i<LanguageM.arrayRealLanguesValuesName.length; i++) {
			$("#panel-Langue-App ul").append('<li class="btn-change-langueApp" cl="'+ LanguageM.arrayLanguesValuesId[i] +'"><img src="'+ globals.dynRes + "img/langue/icone_drapeau_rond_" + LanguageM.arrayLanguesValuesCode[i] + '.png" alt=""/> '+ LanguageM.arrayRealLanguesValuesName[i] +'</li>');
		}
	}

	function changerLangueApp(idLangue) {
		var tmpCodeLangue = LanguageM.codeLangueWithId(idLangue);

		if(tmpCodeLangue != LanguageM.langueApp) {
			// Mise à jour du locale storage et rafraichissement dela page avec la nouvelle langue 
			localStorage.langueApp = tmpCodeLangue;
			$("#img-langueApp").attr("src", globals.dynRes + "img/langue/icone_drapeau_rond_"+ tmpCodeLangue +".png");
			window.location.reload();
		}
	}


	/*****************************************************************************/
	/********************************** LTI **************************************/
	/*****************************************************************************/

	function checkForLTIToken () {
		//Si on est déjà authentifié, toute façon ça sert à rien (comme dirait l'autre)
		// if (isConnected()) {
		// 	return false;
		// }

		//Récuperation des parametres passés en GET dans l'URL depuis la passerelle LTI
		var url_params = getSearchParameters();
		//On récupère le token == elggperm
		var token = url_params['token']
		//Si on est bien dans le cas d'une connexion LTI (et qu'on a le localStorage)
		if (typeof(token) != "undefined" && typeof(localStorage) != "undefined"){
			//On enregsitre l'elggperm dans le localStorage pour maintenir la session (a faire avant setCookie)
			localStorage.elggperm = token;
			//On reconstruit le cookie
			setCookie(24);
			//Important : prévenir le WS qu'on est dans le cas de LTI, en changeant la valeur de version
			var post_params = "os=" + globals.os + "&version=LTI&elggperm="+token;
			//Et log de l'utilisateur.
			logUserWithParams(post_params,true);
			return true;
		}
	}
	function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
	}

	function transformToAssocArray( prmstr ) {
	    var params = {};
	    var prmarr = prmstr.split("&");
	    for ( var i = 0; i < prmarr.length; i++) {
	        var tmparr = prmarr[i].split("=");
	        params[tmparr[0]] = tmparr[1];
	    }
	    return params;
	}

	/*****************************************************************************/
	//****************** Recupération des messages ******************//
		/*  JSON :
        {
          id_blog :
          {
            id_message:
            {
              id_message: $
              id_blog: $
              owner_guid: $
              owner_name: $
              owner_avatar_url: $
              id_ressource: $
              nom_page: $
              nom_tag: $
              num_occurence: $
              value: $
              geo_lattitude: $
              geo_longitude: $
              supprime_par: $
            }
            ...
          }
          ...
        }
        */    
	/*****************************************************************************/
	//Check si l'url entrée est un sous-menu
	function pageIsMenu (url) {
		if(typeof(url) == "undefined")
		{	
			//Defaut : page courante	
			var url = window.location.pathname.split('/')[window.location.pathname.split('/').length -1 ];
		}
		//On test s'il est de classe ueDiv.
		var result = $('.mnu li div[id="'+ url +'"].mnu_b');
		
		return result.length == 0 ? false : true;
	}
  
	function recupererMessages (callback) {

		var id_ressource = globals.id_res;
		if (globals.styleAttacheMessages == "OA") {
			var uid_page = globals.uid_page;
			var post = 'id_ressource='+id_ressource+'&uid_page=' + uid_page;	
		}
		else{
			var nom_page = document.URL.split("/")[document.URL.split("/").length - 1];
			nom_page = nom_page.split(".")[0];
			var post = 'id_ressource='+id_ressource+'&nom_page=' + nom_page;	
		}

		post += "&langues_affichage=" + LanguageM.stringLanguesAffichageForBDD(isConnected());
		
		$.post( globals.webServices + "messages/recupererMessagesV2.php",post+globals.post_OS,function (dataJSON)
		{			
			//localStorage ne supporte que les strings :)
			localStorage.array_messages = dataJSON;
			//JSON.parse to deencode

			var array_messages = $.parseJSON(dataJSON);

			if(array_messages['messages'] !== null)
			{
				attacherMessages(array_messages['messages']);
			}

			//Callback function
			if(typeof(callback) === "function")
			{
				callback();
			}
		});		

	}

	function recupererMessagesRessource (callback) {
		var id_ressource = globals.id_res;
		var post = 'id_ressource='+id_ressource+"&langues_affichage="+LanguageM.stringLanguesAffichageForBDD(isConnected());

		$.post( globals.webServices + "messages/recupererMessagesV2.php",post+globals.post_OS,function (dataJSON)
		{			
			//localStorage ne supporte que les strings :)
			// localStorage.array_messages_ressource = dataJSON;
			//JSON.parse to deencode

			var array_messages_ressource = $.parseJSON(dataJSON);

			var count = 0;

			//IMPORTANT : génère les balises a
			$('.mnu_sel_no .mnu_tgle_c').each(function () {
				// $(this).click().click();
			});

			//TODO: gérer les multiples messages sur un même grain
			// $.each(array_messages_ressource['messages'], function (index, value) //Pour chaque page
			// {
			// 	attacherMessagesRessources(index,value);
			// });
			
			attacherMessagesRessources(array_messages_ressource['messages']);

			//Callback function
			if(typeof(callback) === "function")
			{
				callback();
			}
		});		
	}

	// function recupererMessagesRessourceWithoutLanguageFilter() {
	// 	var id_ressource = globals.id_res;
	// 	var post = 'id_ressource='+id_ressource+"&langues_affichage="+LanguageM.stringLanguesAffichageForBDD(false);
	// 	$.ajaxSetup({ async: false });
	// 	$.post( globals.webServices + "messages/recupererMessages.php",post+globals.post_OS,function (dataJSON)
	// 	{			
	// 		//localStorage ne supporte que les strings :)
	// 		// localStorage.array_messages_ressource = dataJSON;
	// 		//JSON.parse to deencode

	// 		var array_messages_ressource = $.parseJSON(dataJSON);

	// 		var count = 0;

	// 		//IMPORTANT : génère les balises a
	// 		$('.mnu_sel_no .mnu_tgle_c').each(function () {
	// 			// $(this).click().click();
	// 		});

	// 		//TODO: gérer les multiples messages sur un même grain
	// 		// $.each(array_messages_ressource['messages'], function (index, value) //Pour chaque page
	// 		// {
	// 		// 	attacherMessagesRessources(index,value);
	// 		// });
			
	// 		attacherMessagesRessources(array_messages_ressource['messages']);
	// 	});
	// 	$.ajaxSetup({ async: true });
	// }

	/*****************************************************************************/
	/*************************** General display *********************************/
	/*****************************************************************************/
	function checkURLMessage () {
		//S'il y a des messages à remettre dans le contexte 
		if(window.location.hash)
		{
			//Tableau avec les deux paramètres
			var params = window.location.hash.replace("#", "").split('-');
			var emplacement = params[0];
			var id_message = params[1];

			//Si c'est sur la ressource
			if (emplacement == "res") {
				displayMessages($(globals.selectorRes), id_message);
				addFocusedItem($(globals.selectorRes));		
			}
			//Si c'est sur la page
			else if (emplacement == "page")
			{
				
				var array_messages = JSON.parse(localStorage.array_messages);
				//Flag 
				var elementFound = false;
				//Dans tous les messages de la page
				$.each(array_messages['messages'], function (index, message) {
					//Si le message existe
					if (message['id_message'] == id_message)
					{
						//On essaie de retrouver l'élément DOM concerné
						var elementWithMess;

						//En fonction de la facon d'attacher les messages
						//Si on est dans le cas d'objet d'apprentissage identifiés
						if (globals.styleAttacheMessages == "OA") {
							//S'il est attaché à la page
							if (message['uid_oa'] == "")
							{
								elementWithMess = $(globals.selectorPage); //Profeci
							}
							//Sinon, c'est qu'il est sur un grain
							else
							{
								elementWithMess = $('[data-oauid='+message['uid_oa']+']');
							}
						}
						//Si on est dans le cas d'attache automatique sur élément bas niveau
						else{
							if (isScenari() || isFromLatex() || isSpecialCase())
							{
								//S'il est attaché à la page
								if (message['nom_tag'] == "PAGE")
									elementWithMess = $(globals.selectorPage);
								//Sinon, c'est qu'il est sur un grain
								else
									elementWithMess = $(message['nom_tag'] ,globals.selectorContent).get(message['num_occurence']);						
							}
							else
								displayAlert(J42R.get('web_label_aucun_objet_apprentissage'), "info");
						}

						//Si on l'a trouvé
						if ($(elementWithMess).length)
						{
							//On l'affiche, et on le met en valeur (true en second parametre, idMessToFocus)
							displayMessages($(elementWithMess), id_message);
							addFocusedItem($(elementWithMess));

					        $('#tplCo').animate({ 
					            scrollTop: $(elementWithMess).position().top 
					        }, 600);
							elementFound = true;
						}
						
					}
				});

				if(!elementFound)
					displayAlert(J42R.get('web_label_erreur_msg_inexistant'),'error');
			}
			
		}
	}

	function addPointsAndUpdate (points_to_add) {
		var id_ressource = globals.id_res;
		var rank = JSON.parse(localStorage.rank);
		var score = parseInt(rank[id_ressource]['score']) + parseInt(points_to_add);

		rank[id_ressource]['score'] = score;
		localStorage.rank = JSON.stringify(rank);

		$('nav #profil-pts').fadeOut(function () {
			$(this).text(score).fadeIn();
		});

	}

	function updatePointsOnly (score) {
		if ($('nav #profil-pts').text() != score){
			$('nav #profil-pts').fadeOut(function () {
				$(this).text(score).fadeIn();
			});
		}

		var rank = JSON.parse(localStorage.rank);
		rank[globals.id_res]['score'] = score;
		localStorage.rank = JSON.stringify(rank);
	}
	function updatePoints (id_ressource, id_categorie, score) {
		
		$('nav #profil-pts').fadeOut(function () {
			$(this).text(score).fadeIn();
		});

		var rank = JSON.parse(localStorage.rank);
		rank[id_ressource]['id_categorie'] = id_categorie;
		rank[id_ressource]['score'] = score;
		localStorage.rank = JSON.stringify(rank);
	}
	function updateNavBar (animated) {
		
		if(isConnected())
		{
			if(typeof(animated) != "undefined")
			{
				$('#btn-login').fadeOut(function () {
					
					$('#btn-wrapper-right').fadeIn();

				});

			}
			else
			{
				$('#btn-login').hide();
				$('#btn-wrapper-right').show();
			}

			//Hack de lastcache pour empêcher le cache de l'avatar.
			//Supprimer ce parametre permet d'indiquer au webService d'Elgg qu'on ne l'a pas caché avant.
			//Plus, placer un Random dans l'URL permet d'indiquer au browser qu'il faut recharger l'image absolument (car l'URL n'est pas la même qu'avant)
			// $('img','#btn-profil').attr('src', localStorage.avatar_url.replace('lastcache',Math.random()));
			$('img','#btn-profil').attr('src', localStorage.avatar_url);
			$('#profil-name').html(localStorage.username);
			var currentRank = getRank('string');
			var rank = JSON.parse(localStorage.rank);
			var currentPts = rank[globals.id_res]['score'];

			$('#profil-pts').text(currentPts);
			$('#panel-Profil a').each(function () {
				$(this).attr('href', $(this).attr('href') + localStorage.username);
			});

			updateNotifications();
			setNotificationUpdateTimer();

			setCookie(24);
	   		//Update du menu + true en parametre pour afficher une alerte
	   		updateMenuNotification();
	   		updateMenuNomsReels();
		}
		else
		{
			removeNotificationUpdateTimer();
			if(typeof(animated) != "undefined")
			{
				$('#btn-wrapper-right').fadeOut(function () {

					$('#btn-login').fadeIn();
				});
				$('img','#btn-profil').attr('src', '');
			}

			else
			{
				$('#btn-login').show();
				$('#btn-wrapper-right').hide();
				$('img','#btn-profil').attr('src', '');
			}	
		}

	}
	function displaySuccess (nom,description,image) {
		if(typeof(globals.alert) == "object")
		{
			if(globals.alert.css("display") != "none")
			{
				globals.oldAlert = globals.alert;
				window.clearTimeout(globals.alertTimeoutId);
				globals.oldAlert.fadeOut('fast',function () {
					globals.oldAlert.remove();
				});
			}
			else globals.alert.remove();
		}

		globals.alert = $('<div></div>').addClass('message').addClass('success').append(
			$('<div></div>').addClass('message-close').html('×')
		).append(
			$('<div></div>').addClass('message-inner').append(
				$('<img>',{class:"success-image"}).attr('src', 'data:image/png;base64,' + image)
			).append(
				$('<div>', {class:"success-inner"}).append(
					$('<div>',{class:"success-title", html:nom})
				).append(
					$('<div>',{class:"success-description", html:description})
				)
			)
		).click(function(){
			$(this).fadeOut('fast',function () {
				window.clearTimeout(globals.alertTimeoutId);
				$(this).remove();
			});
		});

		globals.alert.appendTo($('body'));

		globals.alert.position({
			my: "right bottom",
			at: "right bottom",
			of: $("body")
		});

		globals.alert.fadeIn('fast');
	}

	/*****************************************************************************/
	//********************* Display des messages *********************//
	/*****************************************************************************/
	function attacherMessages (array_messages) {
		//On récupère les infos de log
		var current_id_user = parseInt(isConnected());
		var is_admin = isAdmin();
		var rank = getRank();

		//Reset des compteurs
		$('.hasComment',globals.selectorContent).each(function () {
			$(this).data('count',0).attr('data-count',0);
		});

		//Pour chaque message
		$.each(array_messages, function () {
					var divWithBlog;
					//En fonction de la facon d'attacher les messages
					//Si on est dans le cas d'objet d'apprentissage identifiés
					if (globals.styleAttacheMessages == "OA") {
						//Si c'est un com sur la page 
						if((this.uid_page != "") && (this.uid_oa == ""))
						{
							divWithBlog = $(globals.selectorPage);
						}
						//Si c'est un com sur un grain fin
						else
						{
							divWithBlog = $('[data-oauid='+this.uid_oa+']');
						}
						
					}
					//Si on est dans le cas d'attache automatique sur élément bas niveau
					else{
						if (isScenari() || isFromLatex() || isSpecialCase())
						{
							//Si c'est un com sur la page 
							if(this.nom_tag == "PAGE")
							{
								divWithBlog = $(globals.selectorPage);
							}
							//Si c'est un com sur un grain fin
							else
							{
								divWithBlog = $($(this.nom_tag,globals.selectorContent)[this.num_occurence]);
							}
						}
						else
							displayAlert(J42R.get('web_label_aucun_objet_apprentissage'), "info");
					}

					//Traitement des messages vus ; si l'id du message est enregistré dans le localStorage
					var isReaded = JSON.stringify(localStorage.array_messages_lus).indexOf(this.id_message) == -1 ? false : true;
					//S'il est lu, on ajoute 0. Sinon, on ajoute 1
					var toCount = !isReaded;

					//Traitement des messages supprimés
					messageIsVisible(this, function () {
						toCount &= 1;
					}, function () {
						toCount &= 0;
					});
					var count = typeof(divWithBlog.data('count')) == "undefined" ? 0 : divWithBlog.data('count');

					divWithBlog.data('count', count + toCount).attr('data-count', count + toCount).addClass('hasComment').removeClass('readed');
		});
		
		//On rajoute la classe readed à tous ceux qui sont lus
		$('*[data-count=0]').addClass('readed');

		updateComBar();
		
	}

	function messageIsVisible (message, callbackTrue, callbackFalse) {
		if(message.supprime_par == 0)
		{
			if (typeof(callbackTrue) == "function")
				callbackTrue();
		}
		else{
			//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
			if((message.id_user == isConnected()) || isAdmin() || (getRank() >= 3))
			{
				if (typeof(callbackTrue) == "function")
					callbackTrue();
			}
			else{
				if (typeof(callbackFalse) == "function")
					callbackFalse();
			}
		}
	}

	function attacherMessagesRessources (array_messages) {
		count = 0;				

		var array_messages_pages = {};
		var array_messages_ressource = [];

		//Pour chaque message récupéré
		$.each(array_messages, function (index, message){
			
			//Si on est dans le cas d'objet d'apprentissage identifiés
			if (globals.styleAttacheMessages == "OA") {
				//Si ce n'est pas un message sur OA
				//Si c'est un com sur la page 
				if(message.uid_page != "")
				{
					if(typeof(array_messages_pages[message['uid_page']]) == 'undefined')
						array_messages_pages[message['uid_page']] = [];

					array_messages_pages[message['uid_page']].push(message);
				}
				//Si c'est un com sur la ressource
				else
				{
					array_messages_ressource.push(message);
				}	
			}
			//Si on est dans le cas d'attache automatique sur élément bas niveau
			else{
				//En fonction de la localisation (message sur ressource ou sur page)
				// if (message['nom_tag'] == "RES") { Pourrait être cool dans le futur
				if (message['nom_page'] == "") {
					//Cas d'une ressource
					array_messages_ressource.push(message);
				}
				else
				{
					//Cas d'une page
					if(typeof(array_messages_pages[message['nom_page']]) == 'undefined')
						array_messages_pages[message['nom_page']] = [];

					array_messages_pages[message['nom_page']].push(message);
				}
			}
		});

		//Ensuite, pour tous les messages par page qu'on a récupéré
		$.each(array_messages_pages, function (nom_page, messages) {

			var numberOfUnreadMess = 0;
			$.each(messages, function () {
				//Traitement des messages vus ; si l'id du message est enregistré dans le localStorage
				var isReaded = JSON.stringify(localStorage.array_messages_lus).indexOf(this.id_message) == -1 ? false : true;
				//S'il est lu, on ajoute 0. Sinon, on ajoute 1
				var toCount = !isReaded;
				//Traitement des messages supprimés
				messageIsVisible(this, function () {
					toCount &= 1;
				}, function () {
					toCount &= 0;
				});
				numberOfUnreadMess += toCount;
			});
			
			//Dans le cas d'une accroche automatique, et sur Scenari
			if (isScenari() && (globals.styleAttacheMessages == "AUTO")) {
				var a_tag = $("#tplMnu li a[href^='" + nom_page + ".html']").closest('li');
				if(a_tag.length){
					a_tag.data('count', numberOfUnreadMess).attr('data-count', numberOfUnreadMess).addClass('hasComment').removeClass('readed');
				}
				//On ajoute la classe "lue" à l'icone
				if (numberOfUnreadMess == 0)
					a_tag.addClass('readed');
				//Sinon, on affiche le point rouge indiquant les nouveaux messages
				else
					$(a_tag).closest('.mnu_b').addClass('activeMenuToggle');
			}
			else if (globals.styleAttacheMessages == "OA"){
				if (nom_page != globals.uid_page) {
					var matched_links = $('[href*=' + nom_page + ']');
					if(matched_links.length){
						matched_links.data('count', numberOfUnreadMess).attr('data-count', numberOfUnreadMess).addClass('hasComment').removeClass('readed');
					}
					if(!matched_links.is(":visible")){
						//On browse dans tous les parents
						var array_parents = [];
						//Pour chaque page de la ressource
						function recursiveSearch (_array_parents, _table, _nom_page) {
							$.each(_table, function  (__nom_page, __page) {
								//Si la page a des enfants, et qu'elle contient la page concernée par le message en cours
								if(__page.subs.indexOf(_nom_page) >= 0){
									//On la push dans un tableau de réference
									_array_parents.push(__nom_page);
									//Et on creuse dans la récursion, en cherchant dans quelle page est contenue la page parent 
									recursiveSearch(_array_parents, _table, __nom_page);
									return;
								}
							});
						}
						recursiveSearch(array_parents, table, nom_page);
						
						$.each(array_parents, function (index, object) {
							var that = $('[href^=' + object + ']');
							// that.data('count', numberOfUnreadMess).attr('data-count', ">").addClass('hasComment').removeClass('readed');
							that.addClass('pf-sub-link').removeClass('readed');
							//On ajoute la classe "lue" à l'icone
							if (numberOfUnreadMess == 0)
								that.addClass('readed');
						});
						
					}
					//On ajoute la classe "lue" à l'icone
					if (numberOfUnreadMess == 0)
						matched_links.addClass('readed');
				}
			}

		});
		
		var numberOfUnreadMess = 0;
		$.each(array_messages_ressource, function () {
			//Traitement des messages vus ; si l'id du message est enregistré dans le localStorage
			var isReaded = JSON.stringify(localStorage.array_messages_lus).indexOf(this.id_message) == -1 ? false : true;
			//S'il est lu, on ajoute 0. Sinon, on ajoute 1
			var toCount = !isReaded;
			//Traitement des messages supprimés
			messageIsVisible(this, function () {
				toCount &= 1;
			}, function () {
				toCount &= 0;
			});
			numberOfUnreadMess += toCount;
		});

		//On ajoute le badge à l'icone ressource
		$(globals.selectorRes).addClass('hasComment').removeClass('readed').data('count', numberOfUnreadMess).attr('data-count', numberOfUnreadMess);
		
		//On ajoute la classe "lue" à l'icone
		if (numberOfUnreadMess == 0)
			$(globals.selectorRes).addClass('readed');

		localStorage.array_messages_pages = JSON.stringify({'messages' : array_messages_pages});
		localStorage.array_messages_ressource = JSON.stringify({'messages' : array_messages_ressource});
	}

	function updateComBar (divWithBlog) {
		if (globals.focusedItem && $('#comBar').is(':visible')) {
			displayMessages(globals.focusedItem);
		}
	}

	function displayMessages (focusedItem, idMessToFocus, idUser) {
		
		//Inutile
		//var post = JSON.stringify(array_messages[focusedItem.data("nom_tag")][focusedItem.data("num_occurence")]['messages']);
		var messages_array_before_sort = {};
		var nom_tag = "";
		var num_occurence = "";
		var uid_page = "";
		var uid_oa = "";
		var post_message = {};
		globals.display_type = "normal";

		//On vérifie que l'utilisateur ne commente pas sur un sous menu s'il ne visionne pas les messages de la ressource
		if(pageIsMenu() && focusedItem.attr('id') !== globals.selectorRes.split("#")[1])
		{
			displayAlert(J42R.get('web_label_erreur_commenter_sous_menu'), 'info');
			return false;
		}
		//Transversal
		if(typeof(focusedItem) == "undefined")
		{
			if (typeof(idUser) != "undefined")
			{
				globals.display_type = "user";
				var messages_of_user = [];
				// recupererMessagesRessourceWithoutLanguageFilter();
			}
			else{
				globals.display_type = "transversal";
			}

			//Tableau de tous les messages de la page
			// var messages_page =JSON.parse(localStorage.array_messages);
			//Tableau de tous les messages de toutes les autres pages
			var messages_pages =JSON.parse(localStorage.array_messages_pages);
			var messages_pages_transversal = [];

			$.each(messages_pages['messages'], function (page, messages_of_pages) {
				messages_pages_transversal = messages_pages_transversal.concat(messages_of_pages);
			});

			//Tableau de tous les messages de la ressource
			var messages_ressource =JSON.parse(localStorage.array_messages_ressource);

			messages_array_before_sort = {'messages' : messages_ressource['messages'].concat(messages_pages_transversal)};
			
			//Reset du champ parent pour tous les messages, pour avoir les messages à plat.
			$.each(messages_array_before_sort['messages'], function (index, message) {
				if (message['id_message_parent'] != "")
					message['id_message_parent'] = "";

				if (globals.display_type == "user")
					if(message['id_auteur'] == idUser)
						messages_of_user.push(message);

			});

			if(globals.display_type == "user")
				messages_array_before_sort['messages'] = messages_of_user;

			//En fonction de la facon d'attacher les messages
			//Si on est dans le cas d'objet d'apprentissage identifiés
			if (globals.styleAttacheMessages == "OA") {
				uid_oa = "*";
			}
			//Si on est dans le cas d'attache automatique sur élément bas niveau
			else{
				nom_tag = "*";
				num_occurence = "*";
			}
  			ga('send', 'event', 'messages_seen', 'transversal', localStorage.id ? localStorage.username : "anonymous");
		}
		//Page
		else if(focusedItem.is(globals.selectorPage))
		{
			//Tableau de tous les messages de la page
			if (typeof(localStorage.array_messages) != "undefined")
				messages_array_before_sort = JSON.parse(localStorage.array_messages);
			else
				return;

			//En fonction de la facon d'attacher les messages
			//Si on est dans le cas d'objet d'apprentissage identifiés
			if (globals.styleAttacheMessages == "OA") {
				uid_page = globals.uid_page;
			}
			//Si on est dans le cas d'attache automatique sur élément bas niveau
			else{
				nom_tag = "PAGE";
				num_occurence = "0";
			}
  			ga('send', 'event', 'messages_seen', 'page', localStorage.id ? localStorage.username : "anonymous");
		}
		//Ressources
		else if(focusedItem.is(globals.selectorRes))
		{
			if (typeof(localStorage.array_messages_ressource) != "undefined")
				messages_array_before_sort = JSON.parse(localStorage.array_messages_ressource);
			else
				return;
			//En fonction de la facon d'attacher les messages
			//Si on est dans le cas d'objet d'apprentissage identifiés
			if (globals.styleAttacheMessages == "OA") {
				uid_page = '';
			}
			//Si on est dans le cas d'attache automatique sur élément bas niveau
			else{
				nom_tag = "PAGE";
				num_occurence = "0";
			}
  			ga('send', 'event', 'messages_seen', 'section', localStorage.id ? localStorage.username : "anonymous");
		}
		//Grain
		else
		{
			if (typeof(localStorage.array_messages) != "undefined")
				messages_array_before_sort = JSON.parse(localStorage.array_messages);
			else
				return;
			//En fonction de la facon d'attacher les messages
			//Si on est dans le cas d'objet d'apprentissage identifiés
			if (globals.styleAttacheMessages == "OA") {
				uid_page = globals.uid_page;
				uid_oa = focusedItem.data('oauid');
			}
			//Si on est dans le cas d'attache automatique sur élément bas niveau
			else{
				nom_tag = focusedItem.get(0).nodeName;
				num_occurence = $(nom_tag,globals.selectorContent).index(focusedItem);
			}
			
  			ga('send', 'event', 'messages_seen', 'grain', localStorage.id ? localStorage.username : "anonymous");
		}
		var messages_array = [];
		var messages_array_reponse = [];
		//Tableau des posts réarrangés
		var messages_object = {};

		//Stockage des messages lus
		var array_messages_lus = [];
		//On ajoute les messages lus à ceux éventuellements déjà lus
		if (typeof(localStorage.array_messages_lus) != "undefined")
			array_messages_lus = JSON.parse(localStorage.array_messages_lus);
		

		//S'il n'y a pas de message sur la page, ça sert a rien de faire la suite
		if (messages_array_before_sort['messages']) {
			//Tableau des messages de l'OA
			$.each(messages_array_before_sort['messages'], function (index,message) {
				//En fonction de la facon d'attacher les messages
				//Si on est dans le cas d'objet d'apprentissage identifiés
				if (globals.styleAttacheMessages == "OA") {
					if((message['uid_oa'] == uid_oa) || (uid_oa == "*"))
					{
						if(message['id_message_parent'] == "")	
							messages_array.push(message);
						else
							messages_array_reponse.push(message);

						if (array_messages_lus.indexOf(message['id_message']) == -1)
							array_messages_lus.push(message['id_message']);
					}
				}
				//Si on est dans le cas d'attache automatique sur élément bas niveau
				else{
					if(((message['nom_tag'] == nom_tag) && (message['num_occurence'] == num_occurence)) || ((nom_tag == "*") && (num_occurence == "*")))
					{
						if(message['id_message_parent'] == "")	
							messages_array.push(message);
						else
							messages_array_reponse.push(message);

						if (array_messages_lus.indexOf(message['id_message']) == -1)
							array_messages_lus.push(message['id_message']);
					}
				}
			});

			//Si on est pas en mode transversal
			if(typeof(focusedItem) != "undefined")
			{
				focusedItem.data('count', 0).attr('data-count',0).addClass('readed');
				localStorage.array_messages_lus = JSON.stringify(array_messages_lus);
			}
			

			//Récupération du mode de tri préféré en fonction du mode d'affichage des messages
			var sort_type = getSavedSortForType(globals.display_type);
			//Si c'est temporel
			// if ((sort_type == 'date') || (sort_type == 'default')) {
				//Ordre temporel décroissant
				messages_array.sort(function (a,b) {
					//Si a est créé avant
					if (a.date_creation > b.date_creation)
						return -1;
					if (a.date_creation < b.date_creation)
						return 1;
					return 0;			

				});

				//Ordre temporel décroissant
				messages_array_reponse.sort(function (a,b) {
					//Si a est créé avant
					if (a.date_creation > b.date_creation)
						return -1;
					if (a.date_creation < b.date_creation)
						return 1;
					return 0;			
				});
			// }
			//Si c'est par votes
			if (sort_type == 'votes') {
				//Plus voté d'abord
				messages_array.sort(function (a,b) {
					//Si a est plus utile
					if (a.somme_votes > b.somme_votes)
						return -1;
					if (a.somme_votes < b.somme_votes)
						return 1;
					return 0;			

				});

				//Plus voté d'abord
				messages_array_reponse.sort(function (a,b) {
					//Attention, ordre inversé!
					if (a.somme_votes > b.somme_votes)
						return 1;
					if (a.somme_votes < b.somme_votes)
						return -1;
					return 0;			
				});
			}
			post_message['next_sort_type'] = getNextSortType(sort_type);

			messages_object = toObject(messages_array);

			$.each(messages_array_reponse, function (index,message) {
				//Si le message est une réponse
				if((message['id_message_parent'] != ""))	
				{
					var parent_message = messages_object[message['id_message_parent']];
					var new_index = messages_array.indexOf(parent_message);
					messages_array.splice(new_index+1, 0, this);
				}
					
			});		
		}
		

		post_message['messages_array'] = messages_array;

		//Transversal
		if((globals.display_type == 'transversal') || (globals.display_type == 'user'))
			post_message['transversal'] = true;
		else
			post_message['transversal'] = false;

		post_message['langueApp'] = LanguageM.langueApp;

		//S'il n'y a aucun élement dans le tableau, on charge quand même le tableau
		// if(messages_array.length == 0)
		// {
		// 	displayAlert("Il n'y a pas encore de message sur cet &eacute;l&eacute;ment.", "info");
		// 	removeFocusedItem();
		// 	return false;
		// }

		//Si la comBar à déjà été chargée
		if($('#comBar').length)
		{
			// $('#comBar').css('opacity','0').animate({'bottom':'0px', 'opacity':'1'});
			// $('#comBar').fadeIn();
			$('#comContainer').fadeOut('fast').load( globals.modules + 'comBar.php .comBox, #formComBar, .comBar-no-element', post_message, function () {
				displayMessagesOptions();						

				//Transversal
				if((globals.display_type == 'transversal') || (globals.display_type == 'user')){
					$('#writeCom, .comBar-no-element .comBar-label h3, .comBar-arrow-placeholder').fadeOut();
				}
				else{
					$('#writeCom, .comBar-no-element .comBar-label h3, .comBar-arrow-placeholder').fadeIn();
				}
				
			}).fadeIn();

			//Au cas ou la comBar était cachée, car la souris ne la survolait pas
			$('#comBar').animate({opacity : 0.98});
		}
		else
		{
			$('<div id="comBar"></div>').appendTo($('body')).hide().load( globals.modules + 'comBar.php', post_message, function () {
				displayMessagesOptions();

				//Transversal
				if((globals.display_type == 'transversal') || (globals.display_type == 'user')){
					$('#writeCom, .comBar-no-element .comBar-label h3, .comBar-arrow-placeholder').hide();
				}
				
				$('#comContainer').bindScrollHandler();
				if (typeof(idMessToFocus) != "undefined")
				{
					if (idMessToFocus.length)
					{
						var comBoxToFocus = $('.comBox[data-id_message='+ idMessToFocus+']' ,'#comContainer');
						$('#comBar').animate({ 
				            scrollTop: comBoxToFocus.position().top 
				        }, 600,"swing", function () {
				        	comBoxToFocus.addClass('highlighted');
				        	setTimeout(function () {
				        		comBoxToFocus.removeClass('highlighted');
				        	},1000);
				        });
						// comBoxToFocus.animate({'box-shadow':'0 0 4px blue'}, 'fast').animate({'box-shadow':'none'}, 'fast');
					}
				}

			}).css('opacity','0').show().animate({'bottom':'0px', 'opacity':'0.98'});
		}

		if (typeof(idUser) != "undefined") {
			// Mise à jour des messages avec filtre de la langue après l'affichage de tous les messages d'un utilisateur
			recupererMessagesRessource();
		}

		/*
		var comBar = $('#comBar').val() == '' ? $('#comBar') : $('<div id="comBar"></div>').appendTo($('body'));

		comBar.load( globals.modules + 'comBar.php #comContainer', array_messages[id_blog]['messages'], function(response, status, xhr) {});
		*/
		//TODO: traitement du display
	}

	function displayMessageExtras (triggerer) {
		if (isConnected())
		{
			if ($(".message-options-container").length)
			{
				$(".message-options-container").animate({opacity:0}).remove();
			}
			else
			{
				var message_options = $('<div>', {class:'message-options-container ui-tooltip-bottom'});

				var visibilite_container = $('<div>',{class:'message-visibilite-container'});
				var label = $('<span>',{class:'message-visibilite-label', html : J42R.get('label_visibilite')+' :'});
				var list = $('<ul>',{class:'message-visibilite-list'});
				var item = $('<li>',{class:'message-visibilite-item message-visibilite-item-public', html : J42R.get('web_label_public'), title: J42R.get('web_label_par_defaut')});
				var addButton = $('<li>',{class:'message-visibilite-item message-visibilite-add-item', html : '+', title: J42R.get('web_label_changer_visibilite')});
				
				message_options.append(visibilite_container.append(label).append(list.append(item).append(addButton)));
				
				if (getRank() >=4){
					var defi_container = $('<div>', {class:'message-defi-container'});
					defi_container.append($('<span>', {html: J42R.get('label_defi')+" : "})).append(
					$('<div class="onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch"><label class="onoffswitch-label" for="myonoffswitch"><div class="onoffswitch-inner"></div><div class="onoffswitch-switch"></div></label></div>')
					);
					message_options.append(defi_container);
				}
				// .css({opacity:0})
				// .position({
				// 	my: "center bottom",
				// 	at: "center top",
				// 	of: triggerer
				// })
				// .animate({opacity:1, marginTop: "-20px"}, 'fast');
				
				//En fonction du mode d'affichage : inline ou pas
				if (triggerer.closest('.ui-tooltip-top').length) {
					message_options
					.addClass('message-options-tooltip')
					.css({opacity:0})
					.appendTo($('#message-options'))
					.animate({opacity:1});
				}
				else{
					message_options
					.appendTo($('body'))
					.css({opacity:0})
					.position({
						my: "center bottom",
						at: "center top",
						of: triggerer
					})
					.animate({opacity:1, marginTop: "-20px"}, 'fast');
				}

				if( $('input[name="est_defi"]').val() == "1")
					$('#myonoffswitch').attr('checked', true);

				// .mouseleave(function(){
				// 	$(".message-options-container").animate({opacity:0}).remove();
				// });
			}
		}
		else
		{
			displayAlert(J42R.get('web_label_etre_rang_superieur'),"info",$(this));
		}
	}

	function displayMessageChoiceLanguage (triggerer) {
		if (isConnected())
		{
			if ($(".message-language-container").length)
			{
				$(".message-language-container").animate({opacity:0}).remove();
			}
			else
			{

				var message_language = $('<div>', {class:'message-language-container ui-tooltip-bottom'});
				var list_language = $('<select>', {id:'message-language-list'});

				for(var i=0; i<LanguageM.arrayLanguesValuesName.length; i++) {
					var tmp_option = $('<option>', {value:LanguageM.arrayLanguesValuesCode[i]});
					tmp_option.text(LanguageM.arrayRealLanguesValuesName[i]);
					list_language.append(tmp_option);
				}

				message_language.append(list_language);
				
				//En fonction du mode d'affichage : inline ou pas
				if (triggerer.closest('.ui-tooltip-top').length) {
					message_language
					.addClass('message-options-tooltip')
					.css({opacity:0})
					.appendTo($('#message-options'))
					.animate({opacity:1});
				}
				else{
					message_language
					.appendTo($('body'))
					.css({opacity:0})
					.position({
						my: "center bottom",
						at: "center top",
						of: triggerer
					})
					.animate({opacity:1, marginTop: "-20px"}, 'fast');
				}

				// Mise à jour de la langue selectionnée
				var currentCodeLangueSelect = LanguageM.codeLangueWithId($('#formComBar input[name="langue"]').val());
				$('#message-language-list option[value="'+ currentCodeLangueSelect +'"]').prop('selected', true);



				// .mouseleave(function(){
				// 	$(".message-options-container").animate({opacity:0}).remove();
				// });
			}
		}
		else
		{
			displayAlert(J42R.get('web_label_etre_rang_superieur'),"info",$(this));
		}
	}

	function changeComSort () {
		var sort_type = switchSavedSortForType(globals.display_type);
		$('#com-sort').html(J42R.get('web_label_trier_par') +' '+ sort_type);

		if (globals.focusedItem)
			displayMessages(globals.focusedItem);
		else
			displayMessages();
	}

	/*
	* LocalStorage sorts_per_displays :
	* {
	*   'transversal' : 'date' || 'votes',
	*   'user' : 'date' || 'votes',
	*   'normal' : 'date' || 'votes'
	* }
	*
	* 'default' quand erreur / rien de trouvé / stocké.
	*/
	function getLocalStorageSortType () {
		if (typeof(localStorage) != "undefined")
		{
			//S'il n'y a pas encore de type, on met les valeurs par défaut
			if (typeof(localStorage.sorts_per_displays) == 'undefined') {
				var sorts_per_displays = {'transversal' : 'date', 'user' : 'date', 'normal' : 'votes'};
				localStorage.sorts_per_displays = JSON.stringify(sorts_per_displays);
			}
			else{
				//Récupération de la valeur stockée
				var sorts_per_displays = JSON.parse(localStorage.sorts_per_displays);
			}
			return sorts_per_displays;

		}
		else 
			return false;
	}

	function getSavedSortForType (display_type) {
		//Récupération des valeurs actuelles
		var sorts_per_displays = getLocalStorageSortType();

		//On renvoie le mode correspondant
		return sorts_per_displays !== false ? sorts_per_displays[display_type] : 'default';
	}

	function setSavedSortForType (sort_type, display_type) {
		//Récupération des valeurs actuelles
		var sorts_per_displays = getLocalStorageSortType();

		//Si les valeurs sont bien récupérées
		if(sorts_per_displays !== false){
			//On enregistre le type
			sorts_per_displays[display_type] = sort_type;

			localStorage.sorts_per_displays = JSON.stringify(sorts_per_displays);

			return true;
		}
		else 
			return false;
	}
	function getNextSortType (current_sort_type) {
		var sorts_array = ['date', 'votes'];

		//On récupère l'index du prochain mode de tri, et si on sort de la limite du tableau, on revient à 0
		var next_index = (sorts_array.indexOf(current_sort_type) +1) >= sorts_array.length ? 0 : sorts_array.indexOf(current_sort_type) +1;

		return sorts_array[next_index];
	}
	function switchSavedSortForType (display_type) {
		//Récupération des valeurs actuelles
		var sorts_per_displays = getLocalStorageSortType();

		//On renvoie le mode correspondant
		if(sorts_per_displays !== false){
			//On enregistre le type
			var sort_type = sorts_per_displays[display_type];

			sort_type = getNextSortType(sort_type);

			//On enregistre dans le local_storage
			sorts_per_displays[display_type] = sort_type;

			localStorage.sorts_per_displays = JSON.stringify(sorts_per_displays);

			return getNextSortType(sort_type);
		}
		else 
			return 'default';
	}

    function displayMessagesOptions () {
		//TODO : optimisation $.each pour looper une fois seulement
		//S'il y a un bien un item focused
		//Sinon, on est en mode transversal
		if (typeof(globals.focusedItem) != "undefined"){
			if (globals.focusedItem.length){
				$('#comContainer').data(globals.focusedItem.data());
    			displayRepondreMessage();
			}
		}

    	displaySupprMessage();
    	// displayEditMessage();
    	displayMedailleMessage();
    	displayValiderMessage();
    	displayAjouterTags();
    	displayTerminerDefi();
    	displaySupprDefMessage();
    	displayMoveMessage();
    	displayLangueMessage();
    }
    function detachComBar () {
    	if ($('#comBar').hasClass('detached'))
    	{
    		$('#com-detach').html(J42R.get('web_label_detacher_panneau'));
    		$('#comBar').animate({opacity:0},200,function () {
	    		$(this).removeClass('detached')
		    	.css({bottom:0, left:0, top:'initial'})
		    	.animate({opacity:0.98},200)
		    	.draggable('disable');
	    	});
    	}
    	else{
    		$('#com-detach').html(J42R.get('web_label_attacher_panneau'));
	    	$('#comBar').animate({opacity:0},200,function () {
	    		$(this).addClass('detached')
	    		.css({"top":"200px", "left":"200px"})
		    	.animate({opacity:0.98},200)
		    	.draggable({
		    		disabled: false,
				    cursor: 'move',        // sets the cursor apperance
				    opacity: 0.85
				});
	    	});
    	}
    	
    }
    function displayRepondreMessage () {
		// if(isConnected()) // CONNECTION ANNULE
		// {
			var current_id_user = parseInt(isConnected());
			$('.comBox').each(function () {
				//Si c'est pas un message écrit par l'utilisateur courant - ANNULE
				// if($(this).data("id_user") != current_id_user)
				// {
					//On affiche la croix de suppression
					var repondre_box = $('<div class="comRepondre">'+ J42R.get('button_repondre') +'</div>');
					$('.actionSection',this).append(repondre_box);
				// }
			});
		// }
    }
	function displayEditMessage () {
		if(isConnected())
		{
			var current_id_user = parseInt(isConnected());
			$('.comBox').each(function () {
				if($(this).data("id_user") == current_id_user)
				{
					//On affiche la croix de suppression
					var edit_box = $('<div class="comEdit">'+ J42R.get('button_modifier') +'</div>');
					$('.actionSection',this).append(edit_box);
				}
			});
		}
	}

	function displaySupprDefMessage () {
		if(isConnected())
		{
			if (isAdmin()) {
				//Pour chaque message
				$('.comBox').each(function () {
					//On affiche la croix de suppression
					var suppress_box = $('<div class="comSupprDef">'+ J42R.get('web_label_sup_msg_definitivement') +'</div>');
					$('.actionSection',this).append(suppress_box);
				});
			}
		}
	}

	function displayMoveMessage () {
		if(isConnected())
		{
			if (isAdmin()) {
				//Pour chaque message
				$('.comBox').draggable({
				    cursor: 'move',        // sets the cursor apperance
				    opacity: 0.85,
				    containement: 'body',
				    revert:'invalid',
				    start: function (event, ui) {
				    	$('#comBar').css({'overflow':'visible'});

				    	var posX = ui.helper.offset().left,
            				posY = ui.helper.offset().top,
            				finalX = event.pageX - posX,
            				finalY = event.pageY - posY;

				    	var position = finalX + 'px ' + finalY + 'px' 

				    	ui.helper.css({'-webkit-transform-origin':position});

				    	//On met les zones commentables droppable
				    	$(globals.selector).droppable({
				    		accept: $('.comBox'),
				    		hoverClass:'comWillDrop',
				    		drop: function (event, ui) {
				    			//S'il y a bien une zone de largage
				    			if ($('.comWillDrop').length)
				    				//On trigger l'action
				    				moveMessageToOA(ui.draggable, $('.comWillDrop'));
				    		}
				    	});

				    	displayAlert(J42R.get('web_label_deplacez_com_sur_grain'), 'info');
				    },
				    stop: function (event, ui) {
				    	$('#comBar').css('overflow', 'auto');

				    	ui.helper.removeClass('dragging');
				    },
				    drag: function (event, ui) {
				    	ui.helper.addClass('dragging');
				    }
				});
			}
		}
	}

	function moveMessageToOA (message, OA) {
		//Si l'OA est la page
		//On se méfie, parce qu'il est triggeré dès qu'un de ses éléments enfants est sélectionné
		var id_message = message.data('id_message');
		var post = 'id_message='+id_message;
				
		//Mode attache AUTO
		if (globals.version == "1") {
			var nom_page = document.URL.split("/")[document.URL.split("/").length - 1];
			nom_page = nom_page.split(".")[0];
			var nom_tag;
			var num_occurence;
			
			//Sur la page
			if(OA.is(globals.selectorPage))
			{
				nom_tag = "PAGE";
				num_occurence = "0";
			}
			//Sur la ressource
			else if (OA.is(selectorRes))
			{
				nom_page = "";
				nom_tag = "PAGE";
				num_occurence = "0";
			}
			//Sur un grain plus fin
			else
			{
				nom_tag = OA.get(0).nodeName;
				num_occurence = $(nom_tag,globals.selectorContent).index(OA);
			}

			post += '&nom_page='+nom_page+'&nom_tag='+nom_tag+'&num_occurence='+num_occurence;
		}
		//Mode attache OA
		else if (globals.version == "1.5") {
			var uid_page = globals.uid_page;
			var uid_oa = OA.data('oauid');
			if ((typeof(uid_oa) == "undefined") || (uid_oa == ""))
			{
				displayAlert(J42R.get('web_label_grain_sans_identifiant'),'info', OA);
				return;
			}
			//Sur la ressource, on ne met donc rien de plus.
			post += '&uid_page='+uid_page+'&uid_oa='+uid_oa;
		}

		$.post(globals.webServices + 'messages/deplacerMessage.php', post + globals.post_OS, function (dataJSON) {
			var retour = JSON.parse(dataJSON);
			if (retour['status'] == "ok") {
				recupererMessages();
				recupererMessagesRessource();
				displayAlert(J42R.get('web_label_msg_deplace_avec_succes'),'valid');
			}
			else{
				displayAlert(retour['message'],'error');
			}
		})
	}

	function displaySupprMessage () {
		if(isConnected())
		{
			var current_id_user = parseInt(isConnected());
			var is_admin = isAdmin();
			var rank = getRank();

			//Pour chaque message
			$('.comBox').each(function () {
				if($(this).data("supprime_par") == 0)
				{
					//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
					if(($(this).data("id_user") == current_id_user) || is_admin || (rank >= 3))
					{
						//On affiche la croix de suppression
						var suppress_box = $('<div class="comSuppr">'+ J42R.get('button_supprimer') +'</div>');
						$('.actionSection',this).append(suppress_box);
					}
				}
				else
				{
					if(($(this).data("supprime_par") == current_id_user) || is_admin || (rank >= 3))
					{
						//On affiche la croix de suppression
						var suppress_box = $('<div class="comSuppr">'+ J42R.get('web_button_retablir') +'</div>');
						$('.actionSection',this).append(suppress_box);	
					}
					else
						$('.comText', this).text(J42R.get('web_label_msg_supprime'));
					
				}
			});
		}
		else
		{
			//Pour chaque message
			$('.comBox').each(function () {
				if($(this).data("supprime_par") != 0)
					$('.comText', this).text(J42R.get('web_label_msg_supprime'));
			});
		}
	}

	function displayMedailleMessage () {
		if(isConnected())
		{
			var current_id_user = parseInt(isConnected());
			var is_admin = isAdmin();
			var rank = getRank();

			//Pour chaque message
			$('.comBox').each(function () {
				//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
				if(($(this).data("id_user") != current_id_user) && (is_admin || (rank >= 4)))
				{
					//On affiche la croix de suppression
					var medaille_box = $('<div class="comDonnerMedaille">'+ J42R.get('label_medaille') +'</div>');
					$('.actionSection',this).append(medaille_box);
				}
			});
		}
	}

	function displayLangueMessage () {
		if(isConnected())
		{
			var current_id_user = parseInt(isConnected());
			var is_admin = isAdmin();
			var rank = getRank();

			//Pour chaque message
			$('.comBox').each(function () {
				//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
				if(($(this).data("id_user") == current_id_user) || is_admin || (rank >= 4))
				{
					var codeLangueMessage = LanguageM.codeLangueWithId($(this).attr('data-id_langue'));
					//On affiche le label pour modifier la langue du message
					var langue_box = $('<div class="comModifierLangue"><img src="'+globals.dynRes+'img/langue/icone_drapeau_rond_'+ codeLangueMessage +'.png" alt="langue" /></div>');
					$('.actionSection',this).append(langue_box);
				}
			});
		}
	}

	function displayValiderMessage () {
		if(isConnected())
		{
			var current_id_user = parseInt(isConnected());
			var is_admin = isAdmin();
			var rank = getRank();

			//Pour chaque message
			$('.comBox').each(function () {
				var is_answer_of_defi = $('.comBox[data-est_defi][data-id_message='+$(this).data('id_message_parent')+']').length;
				//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
				if(($(this).data("id_user") != current_id_user) && is_answer_of_defi && (is_admin || (rank >= 4)))
				{
					//S'il est validé, on écrit invalider, sinon valider
					var label = $(this).data('defi_valide') ? J42R.get('web_label_invalider') : J42R.get('web_label_valider');
					//On affiche la croix de suppression
					var valider_box = $('<div>', {class:"comValiderMessage", html:label});
					$('.actionSection',this).append(valider_box);
				}
			});
		}
	}

	function displayTerminerDefi () {
		if(isConnected())
		{
			var current_id_user = parseInt(isConnected());
			var is_admin = isAdmin();
			var rank = getRank();

			//Pour chaque message
			$('.comBox').each(function () {
				//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
				//Et que c'est un défi en cours
				if(($(this).data("id_user") == current_id_user) && ($(this).data("est_defi") == 1) && (is_admin || (rank >= 4)))
				{
					//On affiche la croix de suppression
					var terminer_box = $('<div>', {class:"comTerminerDefi", html:J42R.get('button_terminer_defi')});
					$('.actionSection',this).append(terminer_box);
				}
			});
		}
	}

	function displayAjouterTags () {
		if(isConnected())
		{
			var rank = getRank();

			$('.comBox').each(function () {
				//Si c'est pas un message écrit par l'utilisateur courant
				if(rank >= 2)
				{
					//On affiche la croix de suppression
					var tags_box = $('<div class="comAjouterTags">'+ J42R.get('label_tags') +'</div>');
					$('.actionSection',this).append(tags_box);
				}
			});
		}
    }
	//Fonction de suppression de message
	//Paramètre : datas du message (id_message, id_user, id_rank_user)
	//Post :
	//id_message
	//supprime_par
	//id_user_op
	//id_rank_user_op
	//id_user_moderator
	//id_rank_user_moderator
	//id_ressource

	/*****************************************************************************/
	/******************************** Commentaires *******************************/
	/*****************************************************************************/

	function displayComForm (key,opt) {
		if(!isConnected())
		{	
			displayLoginForm();
			return false;
		}

		if(pageIsMenu())
		{
			displayAlert(J42R.get('web_label_erreur_commenter_sous_menu'), 'info');
			return false;
		}
		if (globals.focusedItem == "")
			return;
		
		globals.focusedItem.addClass('focusedItem');

		//On ferme la comBar si elle est la
		if ($('#formComBar:visible').length) {
	
			if ($('.message-options-container').length){
				$('.message-options-container').fadeOut(function () {
					$(this).remove();
				});
			}

			$('#comBar').animate({'bottom':'-700px', 'opacity':'0'}, function () {
				 		$('#comBar').remove();
				 	});
			removeFocusedItem();

		}

		// globals.position['top'] = opt.$menu.css('top');
		// globals.position['left'] = opt.$menu.css('left');
		globals.position['top'] = globals.focusedItem.offset().top;
		globals.position['left'] = globals.focusedItem.offset().left;

		var writeCom = $('<section class="popinWrapper" id="formContainer"></section>').load( globals.modules + 'writeCom.php',  { 'langueApp': LanguageM.langueApp }, function () {

			var wrapper =addOverlay(writeCom, removeFocusedItem);

			if (globals.focusedItem.is(globals.selectorRes)) {
				$(".ui-tooltip-top").addClass('onHome');
				var middle_of_box = globals.position['left'] + 50;
				$(".ui-tooltip-top").css('top', globals.position['top'] - 140);
				$(".ui-tooltip-top").css('left', middle_of_box);
			}
			else if(globals.focusedItem.is(globals.selectorPage))
			{
				$(".ui-tooltip-top").css('top', globals.position['top'] + $('.mainContent_ti').height() +20);
				$(".ui-tooltip-top").css('left', $('.mainContent_ti').width()/2 + 50);
			}
			else{
				var middle_of_box = globals.position['left'] + 50;
				$(".ui-tooltip-top").css('top', globals.position['top'] + globals.focusedItem.height() +20);
				$(".ui-tooltip-top").css('left', middle_of_box);
			}
			$(".ui-tooltip-top").draggable({
			    cursor: 'move',        // sets the cursor apperance
			    opacity: 0.85
			});


			wrapper.fadeIn();

			//Submit fonctionnel :)
			$("#formComInline").submit(function (e) {
				e.preventDefault();

				envoyerMessage($(this), function(){
					// removeOverlay(removeFocusedItem);
					removeOverlay();
					return false;
				});	
			
			});
		});
	}
	function displayInlineComForm (comBox) {
		if(!isConnected())
		{	
			displayLoginForm();
			return false;
		}
		var formComBar = $('#formComBar');
		//Si le form est visible
		if (formComBar.is(':visible')) {
			formComBar.slideUp(200, function () {
				if (formComBar.hasClass('reponse')) {
					formComBar.prependTo('#comContainer').removeClass('reponse');
					$('input[name=id_message_original]', formComBar).remove();
				}
				$('textarea', formComBar).val('');
				if ($('.message-options-container').length){
					$('.message-options-container').fadeOut(function () {
						$(this).remove();
					});
				}
				//S'il y a le label par défaut				
				if ($('.comBar-no-element').length) {
					//On le montre à nouveau
					$('.comBar-no-element').fadeIn();
				}
			});


		}
		//S'il n'est pas encore affiché
		else
		{
			//S'il y a le label par défaut	
			if ($('.comBar-no-element').length) {
				//On le cache
				$('.comBar-no-element').fadeOut();
			}

			//Si c'est une réponse
			if (typeof(comBox) != "undefined") {
				var id_message_original ="";
				
				//Si on essaie de répondre à une réponse
				if (comBox.hasClass('reponse'))
					//On prend l'id du parent pour l'attacher
					id_message_original = comBox.data('id_message_parent');
				else
					id_message_original = comBox.data('id_message');
				//On rajoute le @user
				// formComBar.find('textarea').val("@"+comBox.find('.comUserName').html()+" ");
				formComBar
				.insertAfter(comBox)
				.append('<input type="hidden" name="id_message_original" value="'+id_message_original+'"/>')
				.addClass('reponse')
				.slideDown(200, function () {
					// Focus sur le textarea 
					formComBar.find('textarea')
						.focus()
						.val("@"+comBox.find('.comUserName').html()+" ");
						// .val(formComBar.find('textarea').val());
				})
				.submit(function () {
					envoyerMessage($(this), function () {
						formComBar.fadeOut('fast',function () {
							formComBar.prependTo('#comContainer').removeClass('reponse');
							$('#formComBar input[name=id_message_original]').remove();
						});
					});
					return false;
				});
			}
			else
			{
				formComBar.slideDown(200, function () {
					// Focus sur le textarea 
					formComBar.find('textarea').focus();
				}).submit(function () {
					envoyerMessage($(this), function () {
						formComBar.fadeOut();
					});
					return false;
				});
			}
			// Mise à jour de l'icone du drapeau du message
			$('#message-langue-img').attr('src', globals.dynRes +'img/langue/icone_drapeau_rond_'+ LanguageM.codeLangueWithId(globals.id_langue_res) +'.png');
			// Mise à jour de l'input hidden de la langue
			$('#formComBar input[name="langue"]').val(globals.id_langue_res);
		}
		
	}


	function envoyerMessage (jQform, callback){

		//Si l'user n'est plus / pas connecté
		if(!isConnected())
		{	
			//On le stop
			displayLoginForm();
			return false;
		}

		if ($('[name=message]',jQform).val().trim() == "")
		{
			displayAlert(J42R.get('label_erreur_saisie_message'),"error",jQform);
			return false;
		}
		//Variables d'identification du message
		var id_ressource = globals.id_res;	//BIGINT(20)
		var post;
		//En fonction de la facon d'attacher les messages
		//Si on est dans le cas d'objet d'apprentissage identifiés
		
		if (globals.styleAttacheMessages == "OA") {
			var uid_page = '';
			var uid_oa = '';

			//Sur la page
			if(globals.focusedItem.is(globals.selectorPage))
			{
				uid_page = globals.uid_page;
				ga('send', 'event', 'message_sent', 'page', $('[name="id_message_original"]', jQform).val());
			}

			else if (globals.focusedItem.is(globals.selectorRes))
			{
				//Rien à faire :)
				ga('send', 'event', 'message_sent', 'section', $('[name="id_message_original"]', jQform).val());
			}
			//Sur un grain plus fin
			else if (globals.focusedItem.has("[data-oauid]"))
			{
				uid_page = globals.uid_page;
				uid_oa = globals.focusedItem.data('oauid');
				if ((typeof(uid_oa) == "undefined") || (uid_oa == ""))
				{
					displayAlert(J42R.get('web_label_grain_sans_identifiant'),'info', globals.focusedItem);
					return;
				}
				ga('send', 'event', 'message_sent', 'grain', $('[name="id_message_original"]', jQform).val());
			}
			//Sur la ressource, on ne met donc rien de plus.
			post = '&id_ressource='+id_ressource+'&uid_page='+uid_page+'&uid_oa='+uid_oa;
		}
		//Si on est dans le cas d'attache automatique sur élément bas niveau
		else{
			if (isScenari() || isFromLatex() || isSpecialCase())
			{
				var nom_page = document.URL.split("/")[document.URL.split("/").length - 1];
				nom_page = nom_page.split(".")[0];
				var nom_tag;
				var num_occurence;
				
				//Sur la page
				if(globals.focusedItem.is(globals.selectorPage))
				{
					nom_tag = "PAGE";
					num_occurence = "0";
					ga('send', 'event', 'message_sent', 'page', $('[name="id_message_original"]', jQform).val());
				}
				//Sur la ressource
				else if (globals.focusedItem.is(globals.selectorRes))
				{
					nom_page = "";
					nom_tag = "PAGE";
					num_occurence = "0";
					ga('send', 'event', 'message_sent', 'section', $('[name="id_message_original"]', jQform).val());
				}
				//Sur un grain plus fin
				else
				{
					nom_tag = globals.focusedItem.get(0).nodeName;
					num_occurence = $(nom_tag,globals.selectorContent).index(globals.focusedItem);
					ga('send', 'event', 'message_sent', 'grain', $('[name="id_message_original"]', jQform).val());
				}
	
				post = '&id_ressource='+id_ressource+'&nom_page='+nom_page+'&nom_tag='+nom_tag+'&num_occurence='+num_occurence;
			}
			else
				displayAlert(J42R.get('web_label_aucun_objet_apprentissage'), "info");
		}

		if (typeof(globals.sendingMessage) == "undefined")
		{

			//Conversion JSON du champ visibilite
			$('input[name=visibilite]').val(JSON.stringify($('input[name=visibilite]').val().split(',')));
			globals.sendingMessage = $.post( globals.webServices + "messages/enregistrerMessage.php",jQform.serialize()+post+globals.post_OS,function(dataJSON){
				var retour = JSON.parse(dataJSON);
				if(retour['status'] == "ok")
				{		   
				   	if (globals.stateWS != "connected")
					{
						//On récupère les messages à nouveau
						recupererMessages(function(){
							if ($(".message-options-container").length)
								$(".message-options-container").animate({opacity:0}).remove();
							// displayMessages(globals.focusedItem);
							// removeFocusedItem();
						});
						displayAlert(J42R.get('web_label_commentaire_poste'),"valid");
					}

					//Callback function
					if(typeof(callback) === "function")
					{
						callback();
						// removeFocusedItem();
					}

				}
				else
				{
					if (typeof(retour['message']) == "undefined")
						displayAlert(J42R.get('web_label_erreur_enregistrement_msg'), "error");
					else
					{
						if (retour['message'] == "not_logged_in")
							displayAlert(J42R.get('web_label_erreur_session_expiree_msg'), "error");
						else
							displayAlert(J42R.get('web_label_erreur_enregistrement_msg'), "error");
					}
				}
				//Reset de la requête
				globals.sendingMessage = undefined;
			});
		}
		else
			displayAlert(J42R.get('web_label_com_deja_en_enregistrement'),"info",jQform);

		return false; // ne change pas de page
	}


	/*****************************************************************************/
	//********************* Actions sur les messages *********************//
	/*****************************************************************************/

	function supprMessage (comBox, definitif) {

		var data_message = comBox.data();
		var array_rank = JSON.parse(localStorage.rank);
		var params = {'id_message' : data_message.id_message.toString(),
							'supprime_par' : data_message.supprime_par.toString(),
							'id_user_op' : data_message.id_auteur.toString(),
							'id_rank_user_op' : data_message.id_role_auteur.toString(),
							'id_user_moderator' : localStorage.id,
							'id_rank_user_moderator' : array_rank[globals.id_res].id_categorie,
							'id_ressource' : globals.id_res};

		//Si la suppression est définitive
		if (typeof(definitif) != "undefined"){
			var suppr_confirm = window.confirm(J42R.get('web_label_question_sup_msg_definitivement') +"\n\n"+ J42R.get('web_label_restauration_impossible_msg'));
			//Sinon, on quitte direct
			if (!suppr_confirm)
				return;

			//Si oui, on cast definitif en int et on le rajoute aux paramètres
			params['supprime_def'] = +definitif;
		}

		var post_message = $.param(params);

		$.post( globals.webServices + "messages/supprimerMessage.php",post_message+globals.post_OS,function(data){
		   
		   var retour = $.parseJSON(data);

			if(retour['status'] == "ok")
			{
				//Si la suppression est définitive
				if (typeof(definitif) != "undefined"){
					displayAlert(J42R.get('web_label_msg_supprime_definitivement'), 'valid');	
					recupererMessages();
					recupererMessagesRessource();
				}
				else{
					comBox.toggleClass("supprime");
					if(comBox.hasClass("supprime"))
					{
						//On set le data et on ajoute la classe
						comBox.data("supprime_par", parseInt(localStorage.id));
						displayAlert(J42R.get('web_label_msg_supprimer_avec_succes'), 'valid', comBox);	
						$('.comSuppr',comBox).text(J42R.get('web_button_retablir'));
					}
					else
					{
						//On set le data et on ajoute la classe
						comBox.data("supprime_par", 0);
						displayAlert(J42R.get('web_label_msg_retabli_avec_succes'), 'valid', comBox);	
						$('.comSuppr',comBox).text(J42R.get('button_supprimer'));
					}
				}
			}
			else
			{
				displayAlert(retour['message'], 'error', comBox);
			}
		});
	}


	//Message : .comBox
	function editerMessage (message) {
	 	
		var id_message = message.data('id_message');
		var comText = message.find('.comText').fadeOut().promise().done(function () {

			$('<textarea></textarea>').insertAfter(this)
			.html($.trim(this.html()))
			.addClass('textEdit')
			.attr('name', 'message')
			.fadeIn();

			$('<input/>').insertAfter(this)
			.attr('name', 'id_message')
			.val(id_message)
			.hide();
		});


		$('.actionSection', message).children().fadeOut().promise().done(function () {
			$('<div></div>').appendTo($('.actionSection', message))
			.addClass('annulerEdit')
			.html(J42R.get('button_annuler'))
			.fadeIn()
			.click(function(){
				stopEditionMessage(message);
			});

			$('<div></div>').appendTo($('.actionSection', message))
			.addClass('validerEdit')
			.html(J42R.get('web_button_valider'))
			.fadeIn()
			.click(function () {
				envoyerMessage($('.textEdit, input[name="id_message"]'), function () {
					stopEditionMessage(message, true);
				});
			});
		});

	}

	function stopEditionMessage (message, update) {

		$('.annulerEdit, .validerEdit, .textEdit').fadeOut().promise().done(function () {
			update ? $('.comText', message).html($('.textEdit').val()) : false;
			$(this).remove();
			$('.actionSection', message).children().fadeIn();
			$('.comText', message).fadeIn();
		});

	}

	function donnerMedaille(message, option)
	{

		var post = 'id_message='+message.data('id_message');
		if (option != "enlever")
			post += '&type_medaille='+option;

		$.post(globals.webServices + 'messages/donnerMedaille.php',post + globals.post_OS, function (data) {
			var retour = JSON.parse(data);
			if (retour['status'] == "ok") {
				//On récupère les messages à nouveau
				alert('enter');
				recupererMessages(function(){
					displayMessages(globals.focusedItem, message['id_message']);
					displayAlert(J42R.get('web_label_medaille_attribuee'), 'valid');
				});
			}
			else
			{
				displayAlert(retour['message'], 'error', message);
			}
			return false;
		});
	}

	function modifierLangueMessage(message, idLangueSelect)
	{
		var post = 'id_message='+message.data('id_message')+'&langue='+idLangueSelect;

		$.post(globals.webServices + 'messages/modifierLangue.php',post + globals.post_OS, function (data) {
			var retour = JSON.parse(data);
			if (retour['status'] == "ok") {
				// Mise à jour du data langue du message
				//On récupère les messages à nouveau
				recupererMessagesRessource( function(){
					recupererMessages(function(){
						message.attr('data-id_langue', idLangueSelect);
						displayMessages(globals.focusedItem, message['id_message']);
						displayAlert(J42R.get('web_label_langue_modifiee'), 'valid', message);
					});
				});
			}
			else
			{
				displayAlert(J42R.get('web_label_erreur_modif'), 'error', message);
			}
			return false;
		});
	}

	function terminerDefi(message) 
	{
		var post = 'id_message='+message.data('id_message');

		$.post(globals.webServices + 'messages/terminerDefi.php',post + globals.post_OS, function (data) {
			var retour = JSON.parse(data);
			if (retour['status'] == "ok") {
				//On récupère les messages à nouveau
				recupererMessages(function(){
					displayMessages(globals.focusedItem, message['id_message']);
					displayAlert(J42R.get('web_label_defi_termine'), 'valid', message);
				});
			}
			else
			{
				displayAlert(retour['message'], 'error');
			}
			return false;
		});
	}
	function validerMessage(message)
	{		
		var post = 'id_message='+message.data('id_message');

		$.post(globals.webServices + 'messages/validerDefi.php',post + globals.post_OS, function (data) {
			var retour = JSON.parse(data);
			if (retour['status'] == "ok") {
				//On récupère les messages à nouveau
				recupererMessages(function(){
					displayMessages(globals.focusedItem, message['id_message']);
					displayAlert(J42R.get('web_label_reponse_validee'), 'valid');
				});
			}
			else
			{
				displayAlert(retour['message'], 'error', message);
			}
			return false;
		});
	}
	function ajouterTags(message)
	{
		var post = 'id_message='+message.data('id_message');

		$.post(globals.webServices + 'messages/ajouterTags.php',post + globals.post_OS, function (data) {
			var retour = JSON.parse(data);
			if (retour['status'] == "ok") {
				//On récupère les messages à nouveau
				recupererMessages(function(){
					displayMessages(globals.focusedItem, message['id_message']);
					displayAlert(J42R.get('web_label_defi_termine'), 'valid', message);
				});
			}
			else
			{
				displayAlert(retour['message'], 'error');
			}
			return false;
		});
	}
	/*****************************************************************************/
	//*********************** Vote des messages ***********************//
	/*****************************************************************************/

	function updateVoteCount (id_message, voteCount) {

		// var array_messages = $.parseJSON(localStorage.array_messages);
		// var array_messages_pages = $.parseJSON(localStorage.array_messages_pages);
		// var array_messages_ressource = $.parseJSON(localStorage.array_messages_ressource);
		
		//On essaie de récuperer la comBox concernée
		var comBox = $('.comBox[data-id_message='+id_message+']');
		
		//Si elle existe, et qu'elle est visible
		if (comBox.length && comBox.is(":visible")) {
			//On update l'affichage
			comBox.find('.voteCount').fadeOut(function () {
				$(this).text(voteCount).fadeIn();
			});
		}


		changeLocalStorageVoteCount(id_message,voteCount);
	}

	function changeLocalStorageVoteCount (id_message, voteCount) {
		var messOnPage = false;
		
		var array_messages = $.parseJSON(localStorage.array_messages);
		
		$.each(array_messages['messages'],function(){
			if(this.id_message == id_message)
			{
				var voteActif = $('.comBox[data-id_message='+id_message+']').find('.vote.active');
				//S'il y a un vote enregistré
				if(voteActif.length)
					this.user_a_vote = voteActif.hasClass('voteYes') ? 1 : -1;
				else
					this.user_a_vote = 0;

				this.utilite = voteCount;
				messOnPage = true;
				return false;
			}
		});
		if (messOnPage)
			localStorage.array_messages = JSON.stringify(array_messages);
		//Si c'est sur la ressource
		else
		{
			var array_messages_ressource = $.parseJSON(localStorage.array_messages_ressource);
		
			$.each(array_messages_ressource['messages'],function(){
				if(this.id_message == id_message)
				{
					var voteActif = $('.comBox[data-id_message='+id_message+']').find('.vote.active');
					//S'il y a un vote enregistré
					if(voteActif.length)
						this.user_a_vote = voteActif.hasClass('voteYes') ? 1 : -1;
					else
						this.user_a_vote = 0;

					this.utilite = voteCount;
					messOnPage = true;
					return false;
				}
			});
			
			localStorage.array_messages_ressource = JSON.stringify(array_messages_ressource);
		}
	}
	function voteMessage (vote_div) {


		if(!isConnected())
		{	
			displayLoginForm();
			return false;
		}

		//Récupération des commentaires
		var comBox = vote_div.closest('.comBox');

		//On check si l'utilisateur essaie de voter sur son message
		if(parseInt(isConnected()) == comBox.data('id_user'))
		{
			displayAlert(J42R.get('web_label_pas_voter_votre_message'),'info', vote_div);	
			return;
		}
		//Récupération du type de vote
		//True pour up, false pour down
		var vote = vote_div.hasClass('voteYes') ? true : false;

		//Récup de l'autre vote_div
		var other_vote_div = vote_div.siblings('.vote');
		//Récupération du vote fait précédemennt par l'utilisateur
		//True pour up, false pour down, 0 pour aucun.
		var vote_precedent = vote_div.hasClass('active') ? vote : (other_vote_div.hasClass('active') ? !vote : 0);

		//Variable de post de message
		var post_message = 'up=' + vote + "&id_message=" + comBox.data('id_message');

		//Pointeur sur le compte de vote
		var vote_count = vote_div.siblings('.voteCount');


		//Annulation du vote
		if(vote === vote_precedent)
		{
			//Update du décompte
			vote_count.html((parseInt(vote_count.html()) + (vote ? -1 : 1)) + "");
			vote_div.removeClass('active');
		}
		else
		{		
			//Nouveau vote	
			if(vote_precedent === 0)
			{
				//Update du décompte
				vote_count.html((parseInt(vote_count.html()) + (vote ? 1 : -1)) + "");
				vote_div.addClass('active');
			}
			//Changement de vote
			else
			{
				//Update du décompte
				vote_count.html((parseInt(vote_count.html()) + (vote ? 2 : -2)) + "");
				other_vote_div.removeClass('active');
				vote_div.addClass('active');
			}
		}

		changeLocalStorageVoteCount(comBox.data('id_message'), parseInt(vote_count.html()));

		ga('send', 'event', 'message_vote', vote ? "up" : "down", localStorage.id);
		$.post( globals.webServices + "messages/voterUtilite.php",post_message+globals.post_OS,function(data){
			var retour = JSON.parse(data);

			if(retour['status'] == 'ko')
				displayAlert(retour['message'], "error");

			// if (globals.stateWS != "connected")
			// 	recupererMessages();
		});
	}


	/*****************************************************************************/
	//********************* Action sur les profils *********************//
	/*****************************************************************************/
	function displayProfil (id_user) {

		if ($('#profil-container').is(':visible'))	 		
			hideProfil(function () {
				displayProfilPanel(id_user);
			});
		else
			displayProfilPanel(id_user);
	}

	function displayProfilPanel (id_user) {
		if (typeof(id_user) == "undefined")
			var id_user = localStorage.id;

		var is_connected_user = isConnected() == id_user ? true : false;

		ga('send', 'event', 'user', 'display_profile', id_user);
		$.post(globals.webServices + 'profil/afficherProfil.php', 'id_utilisateur=' + id_user + '&langue_app=' + LanguageM.idLangueWithCode(LanguageM.langueApp) + globals.post_OS, function (data) {
			var retour = JSON.parse(data);
			var post = {'id_utilisateur':id_user, 'profil': retour, 'is_connected_user': is_connected_user, 'langueApp': LanguageM.langueApp};

			var profil_panel = $('<div>',{id:'profil-container', class:"panel-container-right"}).load(globals.dynRes + 'mod/profBar.php', post, function () {
				$('#profil-container').show().animate({'right':'0px', 'opacity':'0.98'});
				$('#profil-info').data('id_user', id_user);
				displayProfilExtras(profil_panel);
			}).appendTo($('body'));	
		});
	}


	function hideProfil (callback) {
		$('#profil-container').animate({'right':'-380px', 'opacity':'0'}, function () {
			$('#profil-container').remove();
			if (typeof(callback) == 'function')
				callback();
		});
	}

	function displayProfilExtras (profil_panel) {
		//Si l'user est connecté
		//Si le profil n'est pas le sien
		var id_user_profil = profil_panel.find('#profil-info').data('id_user');
		if ((id_user_profil != isConnected()) && isConnected()) {
			//On récupère l'élément rank
			var user_rank = JSON.parse(localStorage.rank);
			//Pour chaque ressource du profil visité
			$('.profil-ressource', profil_panel).each(function () {
				//On teste si l'utilisateur actuel a le niveau expert minimum
				var user_rank_res = user_rank[$(this).data('id_ressource')];
				
				//S'il a un rang, il a la ressource
				if (user_rank_res) {
					$(this).removeClass('profil-ressource-locked');
					
					if (user_rank_res['id_categorie'] >= 4) {
						//Si oui, on ajoute une classe à l'élément DOM
						$(this).addClass('profil-ressource-upgrade');
					}
				}
			});
		}
	}

	function displayRankUpgrade (profil_ressource) {

		var wrapper = $('<div>',{class:'ui-tooltip-bottom rank-upgrade-wrapper','data-id_capsule':profil_ressource.data('id_ressource')});
		//Suppression de tous les autres présents
		if ($('.rank-upgrade-wrapper').length){
			
			//Par contre, si on essaie de faire apparaître la même image
			if ($('.rank-upgrade-wrapper').data('id_capsule') == wrapper.data('id_capsule')) {
				//On ne va pas plus loin dans la fonction
				$('.rank-upgrade-wrapper').fadeOut(function(){$(this).remove();});
				return;
			}
			$('.rank-upgrade-wrapper').fadeOut(function(){$(this).remove();});
		}

		var list = $('<ul>',{class:'rank-upgrade-list'});
		var list_item = [];
		var array_of_ranks = [J42R.get('web_label_revenir_role_normal'),J42R.get('label_user_participant'),J42R.get('label_user_collaborateur'),J42R.get('label_user_animateur'),J42R.get('label_user_expert')];

		var id_user_profil = profil_ressource.parents('#profil-container').find('#profil-info').data('id_user')

		for (var i = 0; i < 5; i++) {
		 list_item.push($('<li>',{class:'rank-upgrade-list-item btn-action', html:array_of_ranks[i], 'data-id_user_profil':id_user_profil,'data-id_rank':i, 'data-id_capsule':profil_ressource.data('id_ressource')}));
		}
		$(document).on('click','.rank-upgrade-list-item', function () {
			giveRankToUserOnCapsule($(this).data('id_rank'), $(this).data('id_user_profil'), $(this).data('id_capsule'));
		});

		wrapper.append(list.append(list_item));
		wrapper.css('opacity',0).appendTo($('body')).position({
			my:"center bottom",
			at:"center top-15",
			of:profil_ressource
		}).animate({'opacity' :'0.98'});
	}
	function giveRankToUserOnCapsule (rank, id_user_profil, capsule) {
		var post ='id_utilisateur='+id_user_profil +'&id_ressource='+capsule +'&id_categorie='+rank;

		$.post(globals.webServices+'profil/changerRole.php', post + globals.post_OS, function (data) {
			var retour = JSON.parse(data);

			if (retour['status'] == 'ok'){
				displayAlert(J42R.get('web_label_rang_utilisateur_maj'), 'valid');
				$('.rank-upgrade-wrapper').remove();
				displayProfil(id_user_profil);
			}
			else{
				displayAlert(J42R.get('web_label_erreur')+' : '+retour['message'], 'error', $('.rank-upgrade-wrapper'));
			}
		});
		// displayAlert('Cette fonctionnalité n\'est pas encore disponible.', 'info', profil_ressource);
	}

	
	function displayProfilConfig () {
		if (!isConnected())
			return displayLoginForm();

		if ($('#profil-config-panel-container').is(':visible'))	 		
			hideProfilConfig(function () {
				displayProfilConfigPanel();
			});
		else
			displayProfilConfigPanel();
	}

	function displayProfilConfigPanel (callback) {

		var class_to_add = 'panel-container-right';
		
		//S'il y a déjà le panneau profil
		if ($('#profil-container').length)
			//On empile
			class_to_add += ' panel-container-right-second-level';

		var container = $('<div>',{id:'profil-config-panel-container', class: class_to_add}).load(globals.dynRes + 'mod/profilConfig.php', { 'langueApp': LanguageM.langueApp }, function () {
			$('#profil-config-panel-container').show().animate({'right':'0px', 'opacity':'0.98'});

			var wrapper = addOverlay(container);

			$(".popin").position({
				my: "center center",
				at: "center center",
				of: wrapper
			});

			// Mise à jour des listes
			setLanguesProfilConfig(LanguageM.langueUserPrincipale, LanguageM.languesUserAutres);

			// Sauvegarde temporaire des langues dans le localStorage
			localStorage.tmpLangueP = LanguageM.langueUserPrincipale;
			localStorage.tmpLangueA = JSON.stringify(LanguageM.languesUserAutres);

			if (typeof(callback) == 'function')
				callback();

			$('#register-langue-principale').change(function(e) {
				var newLP = this.options[e.target.selectedIndex].value;
				localStorage.tmpLangueP = newLP;
				var newLA = JSON.parse(localStorage.tmpLangueA);
				if(newLA.indexOf(newLP) != -1)
					newLA.splice(newLA.indexOf(newLP), 1);
				localStorage.tmpLangueA = JSON.stringify(newLA);
				setLanguesProfilConfig(newLP, newLA);

				// Affichage du popup pour changer la langue de l'application
				J42R.setLang(localStorage.tmpLangueP, false);
				J42R.load();
				$('#popin-change-langue-app-profil').find('h1').text(J42R.get('title_langue_application'));
				$('#popin-change-langue-app-profil').find('p').text(J42R.get('label_changer_langue_app'));
				$('#btn-no-change-langue-app-profil').text(J42R.get('button_non'));
				$('#btn-yes-change-langue-app-profil').text(J42R.get('button_oui'));
				if($("#popin-change-langue-app-profil").css('display') == "none") {
					$("#popin-change-langue-app-profil").css('display', 'block'); 
				}
			});

			$('#btn-no-change-langue-app-profil').click(function () {
				$("#popin-change-langue-app-profil").css('display', 'none');
				J42R.setLang(LanguageM.langueApp, false);
				J42R.load();
			});

			$('#btn-yes-change-langue-app-profil').click(function () {
				$("#popin-change-langue-app-profil").css('display', 'none');
				// Modification de la langue de l'application
				localStorage.langueApp = localStorage.tmpLangueP;

				// Enregistrement des données du formulaire
				var dataForm = new Array();
				dataForm.push($('#register-password-old').val());
				dataForm.push($('#register-password').val());
				dataForm.push($('#register-password2').val());
				dataForm.push($('#register-name').val());
				dataForm.push($('#register-etablissement').val());
				dataForm.push(localStorage.tmpLangueP);
				dataForm.push(JSON.parse(localStorage.tmpLangueA));

				// Actualisation de la page
				localStorage.updateAccount = true;
				localStorage.dataForm = JSON.stringify(dataForm);

				window.location.reload();
			});

			$('#btn-profil-autres-langues').click(function () {
				if($("#popin-choix-autres-langues").css('display') == "none") {
					$("#popin-choix-autres-langues").css('display', 'block'); 
				}
			});

			$('#btn-ok-choice-other-languages').click(function () {
				$("#popin-choix-autres-langues").css('display', 'none');
			});

		}).appendTo($('body'));
			
	}

	$(document).on( "change", "#list-other-languages input:checkbox", function() {
		var newLA = JSON.parse(localStorage.tmpLangueA);
	    if($(this).is(':checked')){
	        newLA.push($(this).attr('value'));
	    } else {
	        newLA.splice(newLA.indexOf($(this).attr('value')), 1);
	    }
	    localStorage.tmpLangueA = JSON.stringify(newLA);
	});

	function setLanguesProfilConfig(languePrincipale, autresLangues) {
		// Mise à jour de la liste des langues
		$('#register-langue-principale').html("");
		for(var i=0; i<LanguageM.arrayLanguesValuesName.length; i++) {
			$('#register-langue-principale').append('<option value="'+ LanguageM.codeLangueWithName(LanguageM.arrayLanguesValuesName[i]) +'">'+ LanguageM.arrayRealLanguesValuesName[i] +'</option>');
		}
		$('#register-langue-principale option[value="'+ languePrincipale +'"]').prop('selected', true);
		
		// Mise à jour de la liste des autres langues
		var arrayOtherLanguagesWithoutLP = LanguageM.arrayLanguesValuesCode.slice();
		arrayOtherLanguagesWithoutLP.splice(arrayOtherLanguagesWithoutLP.indexOf(languePrincipale), 1);

		$('#list-other-languages').html("");
		for(var i=0; i<arrayOtherLanguagesWithoutLP.length; i++) {
			$('#list-other-languages').append('<li><img src="'+ globals.dynRes + 'img/langue/icone_drapeau_rond_' + arrayOtherLanguagesWithoutLP[i] +'.png" alt=""/> <span class="name-langue">'+ LanguageM.nameRealLangueWithCode(arrayOtherLanguagesWithoutLP[i]) +'</span> <input type="checkbox" name="autres-langues" value="'+ arrayOtherLanguagesWithoutLP[i] +'"></li>');
			var j = autresLangues.indexOf(arrayOtherLanguagesWithoutLP[i]);
			if(j != -1) {
				$('#list-other-languages input[value='+ autresLangues[j] +']').attr('checked', true);
			}	
		}
	}

	function hideProfilConfig (callback) {
		$('#profil-config-panel-container').animate({'right':'-400px', 'opacity':'0'}, function () {
			$('#profil-config-panel-container').remove();
			if (typeof(callback) == 'function')
				callback();
		});
	}

	function editProfilConfig (queryString) {
		// Modification des langues
		var queryLanguageString =  "id_utilisateur="+ localStorage.id +"&langue_principale="+ LanguageM.idLangueWithCode(localStorage.tmpLangueP) +"&autres_langues="+ JSON.stringify(LanguageM.arrayLanguageIdWithArrayLanguageCode(JSON.parse(localStorage.tmpLangueA)));
		
		$.post(globals.webServices + 'compte/editerLangues', queryLanguageString+globals.post_OS, function (data) {
			var retour = JSON.parse(data);

			if (retour['status'] == 'ok'){
				// Mise à jour des langues
	  			LanguageM.languesUserAutres = JSON.parse(localStorage["languesUserAutres"]);
				LanguageM.langueUserPrincipale = localStorage.langueUserPrincipale = localStorage.tmpLangueP;
				localStorage.languesUserAutres = localStorage.tmpLangueA;
				LanguageM.languesUserAutres = JSON.parse(localStorage.tmpLangueA);
				// Mise à jour des messages
				recupererMessages(checkURLMessage);
				recupererMessagesRessource();
				// Mise à jour du drapeau du profil
				$('#profil-icone-langue').attr('src', globals.dynRes +'img/langue/icone_drapeau_rond_'+ LanguageM.langueUserPrincipale +'.png');
			} else {
				displayAlert(J42R.get('web_label_erreur_modif_langues'), 'error');
			}
		});

		//Si le formulaire est complétement vide
		if (queryString == ''){
			//displayAlert('Vous n\'avez saisi aucune modification.', 'info', $('#btn-profil-config-valider'));
			//On se casse
			displayAlert(J42R.get('web_label_langue_modifiee_avec_succes'), 'valid');
			hideProfilConfig();
			return;
		}

		$.post(globals.webServices + 'compte/editerCompte', queryString+globals.post_OS, function (data) {
			var retour = JSON.parse(data);

			if (retour['status'] == 'ok'){
				displayAlert(J42R.get('web_label_info_modifiee_avec_succes'), 'valid');
				hideProfilConfig();
			}
			else{
				switch(retour['message']){
					case 'current_password':
					{
						displayAlert(J42R.get('label_erreur_saisie_mot_de_passe'), 'error', $("#profil-config-form input[name='current_password']"));
						break;
					}
					case 'password':
					{
						displayAlert(J42R.get('web_label_nouveau_mdp_invalide'), 'error', $("#profil-config-form input[name='password']"));
						break;
					}
					case 'password2':
					{
						displayAlert(J42R.get('label_erreur_saisie_mot_de_passe_confirmation'), 'error', $("#profil-config-form input[name='password2']"));
						break;
					}
					case 'erreur_enregistrement':
					{
						displayAlert(J42R.get('web_label_erreur_contacter_pairform'), 'error', $("#profil-config-form input[name='erreur_enregistrement']"));
						break;
					}
					case 'erreur_reseau':
					{
						displayAlert(J42R.get('web_label_erreur_reesayer'), 'error', $("#profil-config-form input[name='erreur_reseau']"));
						break;
					}
					case 'non_connect':
					{
						displayAlert(J42R.get('web_label_session_expiree'), 'error', $("#profil-config-form input[name='erreur_reseau']"));
						break;
					}
					default:
					{
						displayAlert(J42R.get('web_label_erreur_parametres_non_enregistres'), 'error', $('#profil-config-panel-container'));
						break;
					}
				}
			}
		});
		return false;
	}

	/*****************************************************************************/
	/*************************** Cercles & Classes *******************************/
	/*****************************************************************************/

	function displayReseaux (mode) {
		if (!isConnected())
			return displayLoginForm();

		if ($('#reseaux-panel-container').is(':visible'))	 		
			hideReseaux(function () {
				displayReseauxPanel(mode);
			});
		else
			displayReseauxPanel(mode);
	}

	function displayReseauxPanel (mode) {
		ga('send', 'event', 'user', 'display_network', localStorage.id);
		$.post(globals.webServices + 'cercle/afficherListeCercles.php',globals.post_OS, function (data) {
			var retour = JSON.parse(data);
			var post = {'liste_reseaux': retour, 'langueApp': LanguageM.langueApp };
			var class_to_add = 'panel-container-right reseaux-'+mode;
			
			//S'il y a déjà le panneau profil
			if ($('#profil-container').length)
				//On empile
				class_to_add += ' panel-container-right-second-level';

			var container = $('<div>',{id:'reseaux-panel-container', class: class_to_add}).load(globals.dynRes + 'mod/reseaux.php', post, function () {
				$('#reseaux-panel-container').show().animate({'right':'0px', 'opacity':'0.98'});
			}).appendTo($('body'));	
			
			if (mode == "ajout") {
				container.data({'id_utilisateur': $('#profil-info').data('id_user'), 'user_avatar_src' : $('#profil-info-avatar img').attr('src'), 'username': $('#profil-username').html()});
 				displayAlert(J42R.get('web_label_selection_reseau_ajout_utilisateur'), 'info');
			}

		});
	}

	function hideReseaux (callback) {
		$('#reseaux-panel-container').animate({'right':'-380px', 'opacity':'0'}, function () {
			$('#reseaux-panel-container').remove();
			if (typeof(callback) == 'function')
				callback();
		});
	}

	function displaySuccessBar (id_user) {
		if ($('#profil-recherche-container').is(':visible'))
		{	 		
			hideProfilRecherche(function () {
				displaySuccessBarPanel(id_user);
			});
		}
		else if ($('#success-container').is(':visible'))
		{	 		
			hideSuccessBar(function () {
				displaySuccessBarPanel(id_user);
			});
		}
		else
			displaySuccessBarPanel(id_user);
	}
	function showNetworkInlineForm (add_button) {
		//Récuperation des éléments voisins
		var inline_form = add_button.siblings('.reseaux-add-item-form');
		add_button.animate({opacity :0}).hide();
		inline_form.show().animate({opacity :1});
	}
	function hideNetworkInlineForm (cancel_button){		
		//Récuperation des éléments voisins
		var inline_form = cancel_button.parent('.reseaux-add-item-form');
		var add_button = cancel_button.parent('.reseaux-add-item-form').siblings('.reseaux-add-item');
		inline_form.animate({opacity :0}).hide();
		add_button.show().animate({opacity :1});
	}
	function createNewNetwork (element) {
		if (!isConnected())
			return;

		//Récupération du nom de la collection
		var name = element.siblings('input').val();

		//Si champ vide
		if ($.trim(name) == ''){
			displayAlert(J42R.get('web_label_erreur_renseigner_nom'), 'error', element);
			return;
		}

		//Récupération du type de réseau - cercle ou classe
		var type = element.data('type');
		
		var post = 'nom='+name;
		var message = J42R.get('web_label_votre')+' '+type+' '+J42R.get('web_label_creee_avec_succes');
		var ws = '';

		if (type == "classe"){
			ws = 'cercle/creerClasse.php';
			//Si on est pas au moins expert
			if (getRank() < 4){
				//On saute
				displayAlert(J42R.get('web_label_erreur_role_creer_classe'), 'info', element);
				return;
			}
		}
		else if (type == "cercle"){
			ws = 'cercle/creerCercle.php';
		}
		//Si on est dans le cas d'un élève qui rejoint une classe
		else if (type == "classeRejoindre"){
			//On change l'intitulé du paramètre et le WS
			post = 'cle='+name;
			message = J42R.get('web_label_classe_rejointe');
			ws = 'cercle/rejoindreClasse.php';
		}

		ga('send', 'event', 'user', 'create_network', localStorage.id);
		//Envoi de la requête d'ajout
		$.post(globals.webServices + ws, post+globals.post_OS,function (data) {
			var retour = JSON.parse(data);
			//Ajout validé
			if (retour['status'] == 'ok'){
				//Update de l'affichage & notification
				displayAlert(message, 'valid');
				hideReseaux();
				displayReseaux();
				// hideNetworkInlineForm(element);
				// insertNetwork(type);
			}
			//Ajout refusé
			else
			{
				//Display de l'erreur correspondante
				displayAlert(J42R.get('web_label_erreur')+' : ' + retour['message'], 'error');
			}
		});
	}
	function insertNetwork (type) {
		var newNetwork = $('<li></li>',{class:'reseaux-item', 'data-id_collection': retour['id_collection']});
		var spanLabel = $('<span></span>', {class: 'reseaux-item-label'}).html(retour['name']);
		var divAdd = $('<div></div>', {class: 'reseaux-cancel reseaux-delete-item', title: J42R.get('web_label_supprimer_ce')+' '+type}).html(retour['cle']);
		var spanCle = $('<span></span>', {class: 'reseaux-item-cle-label'}).html(retour['cle']);
		var ulUser = $('<ul></ul>', {class:"reseaux-liste-user"});
		var divAddUser = $('<div></div>', {class:"reseaux-add reseaux-add-user",title: J42R.get('web_label_ajouter_personne_a_ce')+" " + type});
		newNetwork.append(spanLabel).append(divAdd).append(spanCle).append(ulUser.append(divAddUser));
	}
	function deleteNetwork (id_collection) {
		if (confirm(J42R.get('web_label_question_sup_reseau_definitivement'))) {
			//Envoi de la requête d'ajout
			var post = 'id_collection='+id_collection;
			$.post(globals.webServices + 'cercle/supprimerCercle.php', post+globals.post_OS,function (data) {
				var retour = JSON.parse(data);
				
				if (retour['status'] == 'ok'){
					//Update de l'affichage & notification
					displayAlert(J42R.get('Réseau supprimé avec succès.'), 'valid');
					hideReseaux();
					displayReseaux();
					// hideNetworkInlineForm(element);
				}
				//Ajout refusé
				else
				{
					//Display de l'erreur correspondante
					displayAlert(retour['message'], 'error');
				}
			});
		}
	}
	function displayRechercheProfilForNetworkAdd (collection) {
		//S'il y a déjà un réseau focused, on le sucre
		if ($('.reseaux-liste-user.focused-collection').length)
			$('.reseaux-liste-user.focused-collection').removeClass('focused-collection');
		
		//On repère la collection en cours de traitement
		collection.find('.reseaux-liste-user').addClass('focused-collection');
		//On affiche la recherche de profil
		displayRechercheProfil();
		displayAlert(J42R.get('web_label_utilisateurs_ajout_reseau'),'info', collection);

		//On ajoute les boutons bleus d'ajout
		$(document).on('click.network-add', '#btn-profil-search-trigger', function () {
			$('#profil-recherche-container').addClass('network-add');
		});
		$(document).off('click',".profil-search-result-user");

		//Enlver les boutons d'ajout, et le cercle focused dès qu'on sort de l'édition.
		$(document).on('click.network-add', '#btn-profil-search-fermer, #btn-reseaux-fermer', function () {
			collection.find('.reseaux-liste-user').removeClass('focused-collection');
			$('#profil-recherche-container').removeClass('network-add');
			$(document).off('.network-add');

			$(document).on('click',".profil-search-result-user",function () {
				var id_user = $(this).data('id_user');
				displayProfil(id_user);	
			});
		});

	}
	function addUserToNetwork (id_utilisateur, username, user_avatar_src, network) {
		//Récupération de l'élément DOM de la collection concernée
		var id_collection;
		if (typeof(network) == 'undefined')
		{
			var network = $('.reseaux-liste-user.focused-collection').parents('.reseaux-item');
			id_collection = network.data('id_collection');
		}
		else{
			id_collection = network.data('id_collection');
		}

		if (network.length != 1){
			displayAlert(J42R.get('web_label_erreur')+' : '+ J42r.get('web_label_plusieurs_reseaux_selectionnes'), 'error');
			return;
		}
		
		//Récupération de l'id utilisateur
		// var id_utilisateur = profil_search_result_user.data('id_user');

		//Envoi de la requête d'ajout
		var post = 'id_collection='+id_collection+'&id_utilisateur='+id_utilisateur;

		ga('send', 'event', 'user', 'add_to_network', localStorage.id);
		$.post(globals.webServices + 'cercle/ajouterMembre.php', post+globals.post_OS,function (data) {
			var retour = JSON.parse(data);
			
			if (retour['status'] == 'ok'){
				//Update de l'affichage & notification
				displayAlert(J42R.get('web_label_utilisateur_ajouté_avec_succes'), 'valid');
				// var user_avatar_src = profil_search_result_user.find('.profil-search-img').attr('src');
				// var username = profil_search_result_user.find('.profil-search-username').html();

				var added_user = $('<li>',{class:'reseaux-user','data-id_utilisateur': id_utilisateur}).append(
					$('<div>',{class: 'reseaux-cancel reseaux-delete-user', title: J42R.get('web_button_supprimer_cet_utilisateur')}).html('&#10008;')
				).append(
					$('<img>',{src: user_avatar_src})
				).append(
					$('<figcaption>').html(username)
				);

				added_user.hide().appendTo(network.find('.reseaux-liste-user')).fadeIn();
			}
			//Ajout refusé
			else
			{
				//Display de l'erreur correspondante
				displayAlert(retour['message'], 'error');
			}
		});
	}
	function removeUserFromNetwork (id_collection, id_utilisateur) {
		var post = 'id_collection='+id_collection+'&id_utilisateur='+id_utilisateur;

		$.post(globals.webServices + 'cercle/supprimerMembre.php', post+globals.post_OS,function (data) {
			var retour = JSON.parse(data);
			
			if (retour['status'] == 'ok'){
				//Update de l'affichage & notification
				displayAlert(J42R.get('web_label_utilisateur_sup_reseau'), 'valid');
				$('li.reseaux-user[data-id_utilisateur='+id_utilisateur+']','.reseaux-item[data-id_collection='+id_collection+']').fadeOut(function(){
					$(this).remove();
				});
			}
			//Ajout refusé
			else
			{
				//Display de l'erreur correspondante
				displayAlert(retour['message'], 'error');
			}
		});
	}

	function addNetworkToMessageOptions (id_collection, collection_name) {
		//Récuperation de la liste de réseaux
		var message_visibilite_list = $('.message-visibilite-list');
		//Sécurité: si elle est bien affichée
		if (message_visibilite_list.length) {
			//Si le tag public est là
			var public_tag = message_visibilite_list.find('.message-visibilite-item-public')
			if (public_tag.length){
				//On le vire
				removeNetworkOfMessageOptions(public_tag);
			}
			//Décomposition des valeurs
			var arr_val = $('input[name=visibilite]').val().split(',');
			//S'il n'y est pas déjà
			//Cast de id_collection en string, car dans le tableau, les ids sont stockés sous forme de string.
			if (arr_val.indexOf(id_collection+'') == -1){
				//Si c'est le premier item qu'on ajoute, l'input est vide mais pas le tableau splité
				if (arr_val.indexOf('') == 0)
					arr_val.splice(0,1);

				//Et on modifie dans le input caché
				arr_val.push(id_collection);
				var final_val = arr_val.join();
				$('input[name=visibilite]').val(final_val);

				//On construit le tag
				var new_visibilite_item = $('<li>',{class:'message-visibilite-item ', html : collection_name, title: J42R.get('web_label_cliquez_pour_supprimer')})
				.data('id_collection', id_collection)
				.hide();
				//On l'ajoute à la liste
				new_visibilite_item.prependTo(message_visibilite_list).fadeIn();
			}
		}
	}

	function removeNetworkOfMessageOptions (network_tag){
		//Récup de l'id de collection
		var id_collection = network_tag.data('id_collection');
		//Le tag Public n'a pas d'id de collection. On le descrimine
		if (typeof(id_collection) != "undefined") {
			//On enlève la valeur du tableau
			var arr_val = $('input[name=visibilite]').val().split(',');
			arr_val.splice(arr_val.indexOf(id_collection), 1);
			var final_val = arr_val.join();
			$('input[name=visibilite]').val(final_val);

		}

		//On enlève le tag
		network_tag.fadeOut(function () {
			$(this).remove();
			//S'il n'y a plus de visiblité
			if ($('.message-visibilite-list').children('li').not('.message-visibilite-add-item').length == 0)
			{
				$('<li>',{class:'message-visibilite-item message-visibilite-item-public', html : J42R.get('web_label_public'), title: J42R.get('web_label_par_defaut')})
				.hide()
				.prependTo($('.message-visibilite-list'))
				.fadeIn();
			}
		});

	}

	/*****************************************************************************/
	/************************************** Succès *******************************/
	/*****************************************************************************/

	function displaySuccessBarPanel (id_user) {
		var post = {'langueApp': LanguageM.langueApp, 'idLangueApp': LanguageM.idLangueWithCode(LanguageM.langueApp)};

		if (typeof(id_user) != "undefined"){
			post = {'id_utilisateur':id_user, 'langueApp': LanguageM.langueApp, 'idLangueApp': LanguageM.idLangueWithCode(LanguageM.langueApp)};
			ga('send', 'event', 'succes', 'display_user', id_user);
		}
		else
			ga('send', 'event', 'succes', 'display_all');

		$('<div>',{id:'success-container', class:"panel-container-left"}).load(globals.dynRes + 'mod/listeSucces.php', post, function () {
					$('#success-container').show().animate({'left':'0px', 'opacity':'0.98'});
				}).appendTo($('body'));	
	}

	function hideSuccessBar (callback) {
		$('#success-container').animate({'left':'-380px', 'opacity':'0'}, function () {
			$('#success-container').remove();
			if (typeof(callback) == 'function')
				callback();
		});

	}
	function displayRechercheProfil () {

		if ($('#profil-recherche-container').is(':visible'))
		{	 		
			hideProfilRecherche(function () {
				displayRechercheProfilPanel();
			});
		}
		else if ($('#success-container').is(':visible'))
		{	 		
			hideSuccessBar(function () {
				displayRechercheProfilPanel();
			});
		}
		else
			displayRechercheProfilPanel();
	}
	function displayRechercheProfilPanel () {
		$('<div>',{id:'profil-recherche-container'}).load(globals.dynRes + 'mod/rechProf.php', { 'langueApp': LanguageM.langueApp }, function () {
			$('#profil-recherche-container').show().animate({'left':'0px', 'opacity':'0.98'});	
		}).appendTo($('body'));	
	}
	function displayRechercheProfilResults (pattern) {
		var post = 'pattern='+pattern;

		ga('send', 'event', 'user', 'display_search');
		$.post(globals.webServices + 'profil/afficherListeDeProfils.php', post+globals.post_OS,function (data) {
			var retour = JSON.parse(data);
			var profils = retour['profils'];
			var template = $('#profil-search-result-user-template');
			var noResultDiv = $('.profil-search-not-found');
			var resultList = $('#profil-search-list-results');

			//On cache et vide la liste
			resultList.fadeOut('fast', function () {
				$(this).empty();

				//S'il n'y a pas de résultat, on affiche le span
				if (profils.length == 0)
				{
					noResultDiv.fadeIn('fast');
				}
				else
				{
					noResultDiv.fadeOut('fast');
					resultList.fadeIn('fast');
				}
				$.each(profils, function (index, profil) {
					var profilElement = template.clone().attr('id', '');

					if (profil['offline'] == false)
						profilElement.addClass('online').attr('title', J42R.get('web_label_en_ligne'));

					profilElement.data('id_user', profil['id_utilisateur']);
					profilElement.find('.profil-search-img').attr('src',profil['image']);
					profilElement.find('.profil-search-username').html(profil['username']);
					profilElement.find('.profil-search-etablissement').html(profil['etablissement']);
					profilElement.css({opacity:0}).appendTo(resultList).animate({opacity:1});
				});
			});
		});
	}

	function hideProfilRecherche (callback) {
		$('#profil-recherche-container').animate({'left':'-380px', 'opacity':'0'}, function () {
			$('#profil-recherche-container').remove();
			if (typeof(callback) == 'function')
				callback();
		});

	}

	function setNotificationUpdateTimer () {
		if (!globals.timer_notifications)
			globals.timer_notifications = setInterval(updateNotifications, 30000);
	}

	function removeNotificationUpdateTimer () {
		if (globals.timer_notifications)
			clearInterval(globals.timer_notifications);
	}
	
	function updateNotifications () {
		var post = "id_capsule="+globals.id_res;

		$.post(globals.webServices + 'accomplissement/recupererNotifications.php', post + globals.post_OS, function (data){
				   
			var retour = $.parseJSON(data);
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				var $us = $('#btn-bar-notifications-score'),
					$us_ul = $us.find('ul'),
					us = retour['us'],
					us_count = 0;
				
				if (us.length)
					$us_ul.empty();

				$.each(us, function (index, notif) {
					var type_of_score = "positif";

					if(parseInt(notif.points) > 0){
						notif.points = "+" + notif.points;
					}
					else
						type_of_score = "negatif";

					var new_line = $('<li>', {class:"notification"})
									.data('id_notification', notif.id_notification)
									.data('type', "us")
									.data('id_utilisateur', notif.id_utilisateur)
									.data('id_message', notif.id_message)
									.data('contenu', notif.contenu)
									.data('sous_type', notif.sous_type)
									.data('non_vue', notif.date_vue ? 1 : 0)
									.attr('data-non_vue', notif.date_vue ? 1 : 0)
									.append($('<div>',{class:type_of_score + " illustration"}).html(notif.points))
									.append($('<div>',{class:"content-wrapper"})
										.append($('<span>').html(notif.titre))
										.append($('<span>').html(notif.label))
									);

					$us_ul.append(new_line);
					us_count += notif.date_vue ? 0 : parseInt(notif.points) ;
				});

				if(us_count > 0){
					us_count = "+" + us_count;
				}

				$us.attr('data-count', us_count).data('count', us_count);

				var $un = $('#btn-bar-notifications-user'),
					$un_ul = $un.find('ul'),
					un = retour['un'],
					un_count = 0;
				
				if (un.length)
					$un_ul.empty();

				$.each(un, function (index, notif) {
					var new_line = $('<li>', {class:"notification"})
									.data('id_notification', notif.id_notification)
									.data('type', "un")
									.data('id_utilisateur', notif.id_utilisateur)
									.data('id_message', notif.id_message)
									.data('contenu', notif.contenu)
									.data('sous_type', notif.sous_type)
									.data('non_vue', notif.date_vue ? 1 : 0)
									.attr('data-non_vue', notif.date_vue ? 1 : 0)
									.append($('<div>',{class:"illustration"}).css("background-image","url("+globals.dynRes+"img/"+notif.image+".png)"))
									.append($('<div>',{class:"content-wrapper"})
										.append($('<span>').html(notif.titre))
										.append($('<span>').html(notif.label))
									);
					$un_ul.append(new_line);
					un_count += notif.date_vue ? 0 : 1 ;
				});

				$un.attr('data-count', un_count).data('count', un_count);

				var nouveau_score = retour['score'];
				updatePointsOnly(nouveau_score);
			}
		});
	}
	function displayNotifications (triggerer) {

		if (!$(".btn-bar-notifications ul").is(':visible')){
			$("ul", triggerer).show().animate({'top':'34px', 'opacity':'1'}, 150, 'swing');

			var notifications_non_vues = $(triggerer).find('.notification[data-non_vue=0]');
		
			//Gestion de la synchro des vues des notifications
			if (notifications_non_vues.length > 0)
			{
				$(triggerer).attr('data-count', 0);

				var array_of_notifications = [];

				notifications_non_vues.each(function () {
					array_of_notifications.push($(this).data('id_notification'));
				});
				var post = "id_notifications=" + JSON.stringify(array_of_notifications);

				$.post(globals.webServices + 'accomplissement/changerVueNotification.php', post , function (data){
						   
					var retour = $.parseJSON(data);
					//Si ya pas de soucis
					if (retour['status'] == 'ko')
					{
						$(triggerer).attr('data-count', $(triggerer).data('data-count'));
					}
					else{
						$(triggerer).data('data-count', 0);
					}
				});
				
			}
		}
		else{
			$(".btn-bar-notifications ul").animate({'top':'44px', 'opacity':'0'}, 150, 'swing', function () {
				$(".btn-bar-notifications ul").hide();
			});
		}
		

	}
	function handleNotificationClick (type, id_utilisateur, id_message, contenu, sous_type) {
		//Score utilisateur
		if (type == "us"){
			//Si on est dans le cas d'un succes, il n'y a pas de message attaché
			if (!id_message){
				displayProfil();
			}
			else{
				redirigerVersMessage(id_message);
			}
		}
		//Notification utilisateur
		else{
			switch(sous_type){
				// dc : défi créé 
				case "dc" : {
					redirigerVersMessage(id_message);
					break;
				}
				// dt : défi terminé 
				case "dt" : {
					redirigerVersMessage(id_message);
					break;
				}
				// dv : défi validé 
				case "dv" : {
					redirigerVersMessage(id_message);
					break;
				}
				// ar : ajout réseau
				case "ar" : {
					displayProfil(contenu);
					break;
				}
				// cr : classe rejointe
				case "cr" : {
					displayProfil(contenu);
					break;
				}
				// ru : réponse d'utilisateur
				case "ru" : {
					redirigerVersMessage(id_message);
					break;
				}
				// gm : gagné médaille
				case "gm" : {
					redirigerVersMessage(id_message);
					break;
				}
			}
		}
	}

	function redirigerVersMessage (id_message){
		window.location.replace(globals.webServices + "messages/redirigerVersMessage.php?id_message=" + id_message);
	}
	function changerNotificationStatus () {
		//Récup de l'id utilisateur
		var id_utilisateur = isConnected();

		//S'il est connecté
		if (id_utilisateur)
		{
			//On récupère l'id de ressource
			var id_ressource = globals.id_res;
			var post = 'id_utilisateur=' + id_utilisateur + '&id_ressource=' + id_ressource;
			ga('send', 'event', 'user', 'change_notifications_level', post);
			//On appelle le webService
			$.post(globals.webServices + 'profil/changerNewsParMail.php', post + globals.post_OS, function (data){
				   
				   var retour = $.parseJSON(data);
				   //Si ya pas de soucis
				   if (retour['status'] == 'ok')
				   {
				   		//Update du menu + true en parametre pour afficher une alerte
				   		updateMenuNotification(true);
				   }
				   else
				   {
						//Message d'alerte
						displayAlert(J42R.get('web_label_beneficier_focntionnalite_avoir_1_point'), 'error', $('#btn-menu-notifications'));
				   }
			});
			
		}
		//Sinon
		else
		{
			//Message d'alerte
			displayAlert(J42R.get('web_label_connecte_pour_changer_parametre'), 'info', $('#btn-menu-notifications'));
		}
	}
	function updateMenuNotification (shouldChangeValue) {
		//On récupère le tableau des rangs par ressource
		var rankArray =  JSON.parse(localStorage.rank);
		
		//Check qu'il ne soit pas vide (sécurité)
		if(typeof(rankArray[globals.id_res]) != 'undefined')
		{
			//Récup de la valeur actuelle de notification
			var statutNotification = parseInt(rankArray[globals.id_res]['notification_mail']);
			//On met le label en conséquence
			var labelMenuNotification = !statutNotification ? J42R.get('web_label_activer') : J42R.get('web_label_desactiver');


			if ((typeof(shouldChangeValue) != "undefined") && (shouldChangeValue == true))
			{
				//Si on vient d'activer, on met le label en conséquence
				labelMenuNotification = statutNotification ? J42R.get('web_label_activer') : J42R.get('web_label_desactiver');
				//L'opérateur + convertit en entier le booléen statutNotification, et le + '' le reconvertit en string
				rankArray[globals.id_res]['notification_mail'] = +!statutNotification +'';
				localStorage.rank = JSON.stringify(rankArray);
				var labelAlertNotification = !statutNotification ? J42R.get('web_label_activees') : J42R.get('web_label_desactivees');
				displayAlert(J42R.get('web_label_notification_par_mail')+' '+labelAlertNotification+ ' '+ J42R.get('web_label_sur_cette_ressource'),'valid');
			}

			$('#btn-menu-notifications').html(labelMenuNotification + ' ' + J42R.get('web_label_les_notifications'));
		}
	}

	function changerNomsReelsStatus () {
		//Récup de l'id utilisateur
		var id_utilisateur = isConnected();

		//S'il est connecté
		if (id_utilisateur)
		{
			if (getRank() >= 4) {
				//On récupère l'id de ressource
				var id_ressource = globals.id_res;
				var post = 'id_utilisateur=' + id_utilisateur + '&id_ressource=' + id_ressource;
				//On appelle le webService
				$.post(globals.webServices + 'profil/changerNomsReels.php', post + globals.post_OS, function (data){
					   
					   var retour = $.parseJSON(data);
					   //Si ya pas de soucis
					   if (retour['status'] == 'ok')
					   {
					   		//Update du menu + true en parametre pour afficher une alerte
					   		updateMenuNomsReels(true);
					   		recupererMessages();
					   		recupererMessagesRessource();
					   }
					   else
					   {
							//Message d'alerte
							displayAlert(retour['message'], 'error', $('#btn-menu-noms-reels'));
					   }
				});
			}
		   else
		   {
				//Message d'alerte
				displayAlert(J42R.get('web_label_avoir_rang_eleve'), 'error', $('#btn-menu-noms-reels'));
		   }
			
		}
		//Sinon
		else
		{
			//Message d'alerte
			displayAlert(J42R.get('web_label_connecte_pour_changer_parametre'), 'info', $('#btn-menu-noms-reels'));
		}
	}
	function updateMenuNomsReels (shouldChangeValue) {
		if (getRank() >= 4) {
			//On affiche le bouton, au cas ou il était caché avant
			$('#btn-menu-noms-reels').show();
			//On récupère le tableau des rangs par ressource
			var rankArray =  JSON.parse(localStorage.rank);
			
			//Check qu'il ne soit pas vide (sécurité)
			if(typeof(rankArray[globals.id_res]) != 'undefined')
			{
				//Récup de la valeur actuelle de NomsReels
				var statutNomsReels = parseInt(rankArray[globals.id_res]['afficher_noms_reels']);
				//On met le label en conséquence
				var labelMenuNomsReels = !statutNomsReels ? J42R.get('web_label_activer') : J42R.get('web_label_desactiver');


				if ((typeof(shouldChangeValue) != "undefined") && (shouldChangeValue == true))
				{
					//Si on vient d'activer, on met le label en conséquence
					labelMenuNomsReels = statutNomsReels ? J42R.get('web_label_activer') : J42R.get('web_label_desactiver');
					//L'opérateur + convertit en entier le booléen statutNomsReels, et le + '' le reconvertit en string
					rankArray[globals.id_res]['afficher_noms_reels'] = +!statutNomsReels +'';
					localStorage.rank = JSON.stringify(rankArray);
					var labelAlertNomsReels = !statutNomsReels ? J42R.get('web_label_active') : J42R.get('web_label_desactive');
					displayAlert(J42R.get('web_label_affichage_noms_reels_a_ete')+' '+labelAlertNomsReels+ ' '+J42R.get('web_label_sur_cette_ressource')+' '+ J42R.get('web_label_messages_recharges'),'valid');
				}

				$('#btn-menu-noms-reels').html(labelMenuNomsReels + ' ' + J42R.get('web_label_affichage_des_noms_reels'));
			}
		}
		else{
			//On le cache pour ne pas attirer la curiosité
			$('#btn-menu-noms-reels').hide();
		}
	}
	function changerAvatar () {
		var avatarForm = $('<section class="popinWrapper" id="formContainer"></section>').load( globals.modules + 'avatarForm.php', { 'langueApp': LanguageM.langueApp }, function () {

			var wrapper = addOverlay(avatarForm);

			//TODO: Ajuster le placement
			$(".popin").position({
				my: "center center",
				at: "center center",
				of: wrapper
			});

			wrapper.fadeIn();
			avatarForm.fadeIn();

			$('#formAvatar').submit(function (event) {

				// if(e.originalEvent.dataTransfer){
				//    if(e.originalEvent.dataTransfer.files.length) {
				// 	   // Stop the propagation of the event
				// 	   e.preventDefault();
				// 	   e.stopPropagation();

				// 	   // Main function to upload
				// 	   upload(e.originalEvent.dataTransfer.files);
				//    }  
				// }
				var form = new FormData($(this)[0]); 
				form.append('elggperm', $.cookie().elggperm);
	            $.ajax({
	                url: globals.webServices + 'compte/editerAvatar.php',
	                type: 'POST',
	                async: true,
	                cache: false,
	                contentType: false,
	                processData: false,
	                data: form
	            }).done(function(data) { 
					var retour = $.parseJSON(data);
					if (retour['status'] == 'ok')
					{
				   		displayAlert(J42R.get('web_label_avatar_modifie_avec_succes'), 'valid');
						removeOverlay();
						displayProfil();
						localStorage.avatar_url = retour['avatar_url'];
						updateNavBar(true);
					}
					else
				   		displayAlert(retour['message'], 'error');

	            }).fail(function() {
	                alert("fail!");
	            });

	            event.preventDefault();
	            event.stopPropagation();

				// $.post(globals.webServices + 'compte/editerAvatar.php', $("#formAvatar").serialize()+globals.post_OS,function(data){
				//    //user.id = idElgg. If "" alors erreur
				//    //user.username = pseudo rentré
				// 	var retour = $.parseJSON(data);
				// 	if (retour['status'] == 'ok')
				// 	{
				// 		removeOverlay();
				// 		displayProfil();
				// 	}
				// 	else
				//    		displayAlert(retour['message'], 'error');
				// });

				return false;
			});
		});
	}

	/*****************************************************************************/
	//******************************* Ressources *******************************//
	/*****************************************************************************/
	function displayRessources () {
		if ($('#ressources-container').is(':visible'))
		{	 		
			hideRessources(function () {
				displayRessourcesPanel();
			});
		}
		else
			displayRessourcesPanel();
	}
	function displayRessourcesPanel () {
		ga('send', 'event', 'ressources', 'display_ressources', localStorage.id);
		var post = "langues_affichage=" + LanguageM.stringLanguesAffichageForBDD(isConnected()) + "&langue_app=" + LanguageM.idLangueWithCode(LanguageM.langueApp) + globals.post_OS;
		$.post(globals.webServices + "ressources/listerRessources.php",post,function(data){
				var retour = $.parseJSON(data);
				// S'il a des ressources
				if (typeof(retour['ressources']) != 'undefined')
				{
					var ressources = retour['ressources'];
					//On va chercher le nombre de messages / On enlève le & du début de post_OS

					$.post(globals.webServices + "messages/recupererNombreMessagesV2.php",post,function(data){
						var messages = $.parseJSON(data);
						if (typeof(messages) != "undefined")
						{
							var postLoad = {'messages' : messages, 'ressources' : ressources, 'langueApp': LanguageM.langueApp };
						   	
							$('<div>',{class:'panel-container-left',id:'ressources-container'}).load(globals.dynRes + 'mod/ressources.php', postLoad, function () {
								$('#ressources-container').show().animate({'left':'0px', 'opacity':'0.98'});	
							}).appendTo($('body'));	
						}
					});
				}
			});
	}
	function hideRessources (callback) {
		$('#ressources-container').animate({'left':'-380px', 'opacity':'0'}, function () {
			$('#ressources-container').remove();
			if (typeof(callback) == 'function')
				callback();
		});
	}

	function switchRessourcesSort () {
		var res_domaine = $('.ressources-domaine');
		var res_etablissement = $('.ressources-etablissement');
		
		//Si le tri est par domaine
		if (res_domaine.is(':visible'))
		{
			$('#btn-ressources-switch').html(J42R.get('web_label_trier_par_theme'));
			res_domaine.fadeOut('fast',function () {
				res_etablissement.fadeIn();
			});
		}
		//Sinon, le tri est par établissement
		else{
			$('#btn-ressources-switch').html(J42R.get('web_label_trier_par_etablissement'));
			res_etablissement.fadeOut('fast',function () {
				res_domaine.fadeIn();
			});
		}
	}
	/*****************************************************************************/
	//******************************* Miscellanous *******************************//
	/*****************************************************************************/
	//Drag n drop image
	$(document).on('dragenter', '#dropfile', function() {
        $(this).css('outline', '3px dashed red');
        return false;
	});
	 
	$(document).on('dragover', '#dropfile', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).css('outline', '3px dashed red');
        return false;
	});
	 
	$(document).on('dragleave', '#dropfile', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).css('outline', '3px dashed #BBBBBB');
        return false;
	});
	$(document).on('drop', '#dropfile', function(e) {
        if(e.originalEvent.dataTransfer){
                   if(e.originalEvent.dataTransfer.files.length) {
                               // Stop the propagation of the event
                               e.preventDefault();
                               e.stopPropagation();
                               $(this).css('outline', '3px dashed green');
                               // Main function to upload
                               upload(e.originalEvent.dataTransfer.files);
                   }  
        }
        else {
                   $(this).css('outline', '3px dashed #BBBBBB');
        }
        return false;
	});
	function upload(files) {
	    var f = files[0] ;

	    // Only process image files.
	    // if (!f.type.match('image/png')) {
     //    	$('#spanWrapper h4').html("Format invalide").css('color','red');
	    //            return false ;
	    // }

	    var image = new Image();
        image.onload = function() {
			var reader = new FileReader();
			// When the image is loaded,
			// run handleReaderLoad function
			reader.onload = handleReaderLoad;

			// Read in the image file as a data URL.
			reader.readAsDataURL(f); 
        };
        image.onerror = function() {
        	displayAlert(J42R.get('web_label_format_fichier_invalide'), 'error');
            
        }; 
        image.src = URL.createObjectURL(f);  

	}
	function handleReaderLoad(evt) {
        var pic = {};
        pic.file = evt.target.result.split(',')[1];

        $('#profil-info #profil-img').attr('src', pic.file);

	}

	function showAccroche () {
		// ga('send', 'event', 'misc', 'display_overlay_presentation');
		var accrocheWrapper = $('<section class="popinWrapper"></section>').load(globals.modules + 'accroche.php', { 'langueApp': localStorage.langueApp }, function () {
			var wrapper = addOverlay(accrocheWrapper);

			$(".popin").position({
				my: "center center",
				at: "center center",
				of: wrapper
			});

			wrapper.fadeIn();
			accrocheWrapper.children().scaleIn(800);
			
			$('.btn-cross-fermer', accrocheWrapper).click(function () {
				accrocheWrapper.children().addClass('willGoToNavBar');
			});
			$('#popinFocus').click(function () {
				accrocheWrapper.children().addClass('willGoToNavBar');
			});
		});
	}

	function showPresentation () {
		var presWrapper = $('<section class="popinWrapper" id="pres-wrapper"></section>').load(globals.modules + 'pres.php', { 'langueApp': LanguageM.langueApp }, function () {
			var wrapper = addOverlay(presWrapper);

			wrapper.fadeIn();
			presWrapper.fadeIn();
		});
	}
	function showTutorial () {
		var m = {}; // namespace
		m.help = {}; // namespace
		 
		/**
		 * Add the help overlay SVG panel to the DOM.
		 */
		m.help.addSVG = function () {
		    var svg = $("<div id='helpsvg' style='width:100%;height:100%;"+
		                "background-color:rgba(0,0,0,0.4);position:absolute;top:0;left:0;"+
		                "z-index:2000'><svg xmlns='http://www.w3.org/2000/svg'"+
		                "style='width:100%;height:100%'><defs><marker id='head' orient='auto' markerWidth='6' markerHeight='8' refX='1' refY='4'><path id='headpoly' d='M0,0 V8 L6,4 Z' fill='white'></path></marker></defs></svg></div");
		 
		    $(document.body).append(svg);
		    svg.scaleIn();
		}
		 
		/**
		 * Removes the help panel from the DOM completely.
		 */
		m.help.remove = function () {
		    var svg = $('#helpsvg');
		    if (!svg) {
		        return;
		    }
		    svg.remove();
		}
		 
		/**
		 * Add a help label to the specified position.
		 * @param {number} tox
		 * @param {number} toy
		 * @param {string} label
		 * @param {string} pos
		 * @param {number=} length
		 */
		m.help.addLabel = function (tox, toy, label, pos, length,inversed) {
		    var svg = $('#helpsvg svg');
		    if (!svg) {
		        return;
		    }
		 
		    var awayx = 50;
		    var awayy = 30;
		 
		    if (pos.indexOf("top") > -1) {
		        awayy *= -1;
		    }
		 
		    if (pos.indexOf("left") > -1) {
		        awayx *= -1;
		    }
		 
		    if (length) {
		        awayx *= length;
		        awayy *= length;
		    }
		 
		    var fromx = tox + awayx;
		    var fromy = toy + awayy;
		    var labelawayx = -80;
		    var labelawayy = awayy > 0 ? 15 : -15;
		    var color = 'white';
		 
		 	if (typeof(inversed) == "undefined")
		 	{
			    var midx = fromx;
			    var midy = toy;
		 	}
		 	else
		 	{
			    var midx = tox;
			    var midy = fromy;
		    	labelawayx -= 100;
		 	}
		    var ns = "http://www.w3.org/2000/svg";
		 
		    var path = document.createElementNS(ns, "path");
		    path.setAttribute('marker-end', 'url(#head)');
		    path.setAttribute('stroke-width', '2');
		    path.setAttribute('fill', 'none');
		    path.setAttribute('stroke', color);
		    path.setAttribute('d', 'M' + fromx + ',' + fromy + ' Q' + midx + ',' + midy + ' ' + tox + ',' + toy);
		 
		    var text = document.createElementNS(ns, "text");
		    text.setAttribute('x', fromx + labelawayx);
		    text.setAttribute('y', fromy + labelawayy);
		    text.setAttribute('fill', color);
		    text.setAttribute('font-size', 18);
		    
		    //  The following two variables should really be passed as parameters
		    var MAXIMUM_CHARS_PER_LINE = 35;
		    var LINE_HEIGHT = 18;

		    var words = label.split(" ");
		    var line = "";
		    if (inversed) {
		    	var x = fromx;
			    var y = fromy;
		    }
		    else{
			    var x = fromx + labelawayx;
			    var y = fromy + labelawayy;
		    }

		    for (var n = 0; n < words.length; n++) {
		        var testLine = line + words[n] + " ";
		        if (testLine.length > MAXIMUM_CHARS_PER_LINE)
		        {
		            //  Add a new <tspan> element
		            var svgTSpan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
		            svgTSpan.setAttributeNS(null, 'x', x);
		            svgTSpan.setAttributeNS(null, 'y', y);

		            var tSpanTextNode = document.createTextNode(line);
		            svgTSpan.appendChild(tSpanTextNode);
		            text.appendChild(svgTSpan);

		            line = words[n] + " ";
		            y += LINE_HEIGHT;
		        }
		        else {
		            line = testLine;
		        }
		    }

		    var svgTSpan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
		    svgTSpan.setAttributeNS(null, 'x', x);
		    svgTSpan.setAttributeNS(null, 'y', y);

		    var tSpanTextNode = document.createTextNode(line);
		    svgTSpan.appendChild(tSpanTextNode);

		    text.appendChild(svgTSpan);

		    // var data = document.createTextNode(label);
		    // text.appendChild(data);
		 
		    document.getElementById('headpoly').setAttribute('fill', color);
		 
		    svg.append(path);
		    svg.append(text);
		}
		 
		/**
		 * Add help label to an element on the screen.
		 * @param {jQuery} dom
		 * @param {string} label
		 * @param {string} pos
		 * @param {number=} length
		 */
		m.help.addLabelTo = function (dom, label, pos, length, anchor, inversed) {
		    if (!dom || (dom.length == 0)) {
		        return;
		    }
		    var offset = dom.offset();
		    var toX, toY;

		    if (typeof(anchor) != 'undefined'){
			    if (anchor.match('right')) {
			    	toX = offset.left + dom.width();
			    }
			    else if (anchor.match('left')){
			    	toX = offset.left;
			    }
			    else{
			    	toX = offset.left + dom.width() /2;	
			    }
			}
		    else{
		    	toX = offset.left + dom.width() /2;	
		    }

		    toX += 20;
	    	toY = offset.top + dom.height() /2;
		    
		    m.help.addLabel(toX, toY, label, pos, length, inversed);
		}
		 
		// Sample usage
		m.help.addSVG();
		if ($('#btn-login').is(':visible'))
			m.help.addLabelTo($('#btn-login'), J42R.get('web_label_connexion_ou_creer_compte'), "bottomleft", 4,'left');
		else
		{
			m.help.addLabelTo($('#profil-pts'), J42R.get('web_label_survoler_btn_afficher_profil'), "bottomleft", 2.5, 'left');
			// m.help.addLabelTo($('#profil-name'), "Votre pseudo et avatar", "bottomleft", 1, true);
			// m.help.addLabelTo($('#profil-pts'), "Vos points sur cette ressource", "bottomleft", 2.5, true);
			// m.help.addLabelTo($('#wsIndicator'), "L'état actuel du service de mise à jour en temps réel.", "bottomleft", 4, true);
		}
			
			
		m.help.addLabelTo($('#btn-menu'), J42R.get('web_label_survoler_btn_afficher_menu'), "bottomright", 1, 'right');
		// m.help.addLabelTo($('.mainContent'), "Vous pouvez poster des commentaires sur tous les éléments de la page.", "bottomright", 1, 'center');
		m.help.addLabelTo($('.mainContent_ti'), J42R.get('web_label_commenter_page_entiere'), "bottomleft", 3,'center');
		m.help.addLabelTo($($('.mainContent_co p')[2]), J42R.get('web_label_faire_clic_droit_element'), "bottomright", 1, 'center');
		m.help.addLabelTo($('.hasComment', '.mainContent_co').last(), J42R.get('web_label_messages_sur_element'), "bottomleft", 1, 'left');
		m.help.addLabelTo($('.hasComment', '.mnuFra').last(), J42R.get('web_label_messages_dans_sous_menu'), "bottomright",2, 'center', true);
		m.help.addLabelTo($(globals.selectorRes), J42R.get('web_label_commenter_sur_ressource'), "topright", 1, 'right');

		ga('send', 'event', 'misc', 'display_overlay_help');
		$(document).on('click','body',function removeHelp () {
			if($('#helpsvg').length)
			{
				$('#helpsvg').fadeOut(function () {
					this.remove();
				});

				displayAlert(J42R.get('web_label_afficher_aide'), 'valid');
				// localStorage.firstTime = "false";
				$(document).off('click','body',removeHelp);
			}
		});
		
	}
	function addFocusedItem (item) {
		removeFocusedItem();
		globals.focusedItem = item;
		globals.focusedItem.addClass('focusedItem');
	}

	function removeFocusedItem () {
		$('.focusedItem').removeClass('focusedItem');
		if (typeof(globals.focusedItem) != "undefined") {
			if(globals.focusedItem != '')
			{
				globals.focusedItem.removeClass('focusedItem');
				globals.focusedItem.removeClass('selectedItem');
				globals.focusedItem = '';
			}
		}
	}

	function addOverlay(div_to_add,callback){

		var wrapper = $('<div id="popinFocus"></div>');	

		$('body').append(wrapper);

		wrapper.click(function () { 
			if(typeof(callback) === "function")
			{
				removeOverlay(callback);
			}
			else removeOverlay();
		});
		div_to_add.appendTo($('body'));
		// div_to_add.css({opacity:0, marginTop:"-30px"}).appendTo($('body')).animate({opacity:1, marginTop:"0"}, 200);
		// $('body').append(div_to_add);
		// $('#tplFra').css('-webkit-filter', 'blur(1px)');

		return wrapper;
	}
	function removeOverlay (callback) {
		
		// $('#tplFra').css('-webkit-filter', '');
		// $('#formWrapper').fadeOut(function(){$(this).remove()});
		// $('#pres-wrapper').fadeOut(function(){$(this).remove()});
		// $("#formContainer").fadeOut(function(){$(this).remove()});
		$("#popinFocus").fadeOut(600,function(){$(this).remove()});
		$(".popinWrapper").fadeOut(600,function(){$(this).remove()});
		
		if(typeof(callback) === "function")
			callback();
	}
	//texte : Texte à afficher
	// class_to_add : valid, info ou error
	// div_to_position : Element sur lequel placer l'erreur
	function displayAlert (texte, class_to_add, div_to_position) {
		if(typeof(globals.alert) == "object")
		{
			if(globals.alert.css("display") != "none")
			{
				globals.oldAlert = globals.alert;
				window.clearTimeout(globals.alertTimeoutId);
				globals.oldAlert.fadeOut('fast',function () {
					globals.oldAlert.remove();
				});
			}
			else globals.alert.remove();
		}
		globals.alert = $('<div>',{class:'pf-message '+ class_to_add}).append
		(
			$('<div>',{class:'message-close'}).html('x')
		).append
		(
			$('<div>',{class:'message-inner'}).append
			(
				$('<div>',{class:'message-text'}).html(texte)
			)
		).click(function(){
					$(this).fadeOut('fast',function () {
						window.clearTimeout(globals.alertTimeoutId);
						$(this).remove();
					});
				});

		globals.alert.appendTo($('body'));

		if(div_to_position)
			globals.alert.position({
				my: "left top",
				at: "right bottom",
				of: div_to_position
			});
		else
			globals.alert.css({"position":"fixed", "bottom":"10px","right":"10px"});

		globals.alert.fadeIn('fast')
		globals.alertTimeoutId = window.setTimeout(function(){
			globals.alert.fadeOut('fast',function () {
				globals.alert.remove();
			})
		},3000);
		
	}
	function toObject(arr) {
	  var rv = {};
	  for (var i = 0; i < arr.length; ++i)
	  {
	  	var id_post = arr[i].id_message;
	    rv[id_post] = arr[i];
	  }	  	
	  return rv;
	}
	Array.prototype.move = function (old_index, new_index) {
	    if (new_index >= this.length) {
	        var k = new_index - this.length;
	        while ((k--) + 1) {
	            this.push(undefined);
	        }
	    }
	    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
	    return this; // for testing purposes
	}


	$.fn.scaleIn = function(duration, ease){
		var localDuration = typeof(duration) == 'number' ? duration : 600;
		var localEase = typeof(ease) == 'string' ? ease : 'swing';

		$(this).css({
					'-webkit-transform':'scale(1.5)',
					'-moz-transform':'scale(1.5)',
					'-o-transform':'scale(1.5)',
					'transform':'scale(1.5)', 'opacity': '0'}).animate({ opacity: 1 }, {
			    step: function(now,fx) {
			        $(this).css({
			        	'-webkit-transform':'scale('+ (1 + (1 - now)/2) +')',
			        	'-moz-transform':'scale('+ (1 + (1 - now)/2) +')',
			        	'-o-transform':'scale('+ (1 + (1 - now)/2) +')',
			        	'transform':'scale('+ (1 + (1 - now)/2) +')'
			        });
			    },
			    duration:localDuration
			}, localEase);
	}
	
	function displayAppStoreIcon () {

		var appStore;

		if(isMobile.iOS())
		{
			appStore = '<a href="https://itunes.apple.com/fr/app/pairform/id673935516?mt=8&amp;uo=4"  style="position: fixed; z-index: 1; bottom: 4px; right: 10px;" target="itunes_store"><img alt="'+ J42R.get('web_label_disponible_sur_appstore') +'" src="'+globals.dynRes+'img/appstore.png"></a>'; 
		}
		else
		{
			appStore = '<a href="https://play.google.com/store/apps/details?id=cape.pairform" style="position: fixed; z-index: 1; bottom: 4px; right: 10px;" > <img alt="'+ J42R.get('web_label_disponible_sur_playstore') +'" src="https://developer.android.com/images/brand/fr_app_rgb_wo_45.png"> </a>'; 
		}
		$('body').append(appStore);
	}

	function addAnalytics () {
		//Pas de log en dev
		if (globals.webServices.match(/imedia.emn.fr/))
		{
			ga = function () {};
		}
		else{
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-40687872-2', 'pairform.fr');
			ga('send', 'pageview');
		}
	}

	function extendDomManip () {
		
	    var oldAppendTo = $.fn.appendTo;
	    $.fn.appendTo = function(){
	        var ret = oldAppendTo.apply(this, arguments);
	        if (arguments[0].nodeName === undefined)
	        	arguments[0] = arguments[0][0];

	        if (arguments[0].nodeName === 'BODY')
	        	ret.addClass('pf');
	        return ret;
	    };

	    var oldPrependTo = $.fn.prependTo;
	    $.fn.prependTo = function(){
	        var ret = oldPrependTo.apply(this, arguments);
	        if (arguments[0].nodeName === undefined)
	        	arguments[0] = arguments[0][0];

	        if (arguments[0].nodeName === 'BODY')
	        	ret.addClass('pf');
	        return ret;
	    };
	    var oldPrepend = $.fn.prepend;
	    $.fn.prepend = function(){
	        var ret = oldPrepend.apply(this, arguments);
	        if (this[0].nodeName === undefined)
	        	this[0] = this[0][0];

	        if (this[0].nodeName === 'BODY')
	        	$(arguments).addClass('pf').addClass('pf');
	        return ret;
	    };
	    var oldAppend = $.fn.append;
	    $.fn.append = function(){
	        var ret = oldAppend.apply(this, arguments);
	        if (this[0].nodeName === undefined)
	        	this[0] = this[0][0];

	        if (this[0].nodeName === 'BODY')
	        	$(arguments).addClass('pf').addClass('pf');
	        return ret;
	    };
	}
});