
//Global vars
var globals = {};

globals.webServices = "//imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/";
globals.dyn = "//imedia.emn.fr/SCWeb/dyn/";

globals.dynRes = globals.dyn + "res/";

function includeHead(url, callback)
{
    var head = window.document.getElementsByTagName('head')[0];
    var type = url.slice(url.lastIndexOf('.'));
    switch (type)
    {
        case '.js':
        var addedHead = window.document.createElement('script');
        addedHead.setAttribute('src', url);

        if(callback)
        {
            var completed = false;
            addedHead.onload = addedHead.onreadystatechange = function () {
                if (!completed && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                    completed = true;
                    callback();
                    addedHead.onload = addedHead.onreadystatechange = null;
                    head.removeChild(addedHead);
                }
            };
        }
        break;
        case '.css':
        var addedHead = window.document.createElement('link');
        addedHead.setAttribute('href', url);
        addedHead.setAttribute('type', 'text/css');
        addedHead.setAttribute('rel', 'stylesheet');
        break;
        case '.less':
        var addedHead = window.document.createElement('link');
        addedHead.setAttribute('href', url);
        addedHead.setAttribute('type', 'text/css');
        addedHead.setAttribute('rel', 'stylesheet/less');
        break;
        default:
        var addedHead = '<!--[if lte IE 8]>    <script type="text/javascript"      src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>    <sc     // Le code conditionnel qui check si Google Chrome Frame est déjà installé     // Il ouvre une iFrame pour proposer le téléchargement.     window.attachEvent("onload", function() {       CFInstall.check({         mode: "overlay" // the default       });     });    </script>        //La balise indiquant à IE d`utiliser GCF si présent    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->'
        break;

    }
    addedHead.async =true;
    head.appendChild(addedHead);
}

//Récupération du titre de la ressource
function getTitle () {

    //<title>Titre - titre_page</title>
    /*
    var title_tag = $('title','head');

    var titre_ressource = title_tag.html().split(" - ")[0];
    */
    
    var titre_ressource = document.querySelector('meta[name="nom_court"]').content;
    if(titre_ressource != '')
        return titre_ressource;
    else 
        return false;
    
}

//Récupération du titre de la ressource
function getHash () {
   
    var hash_ressource = document.querySelector('meta[name="hash_ressource"]').content;
    if(hash_ressource != '')
        return hash_ressource;
    else 
        return false;
    
}

function getMetas(){

    globals.meta_tags = {};
    document.querySelector('meta[name]').each(function(){
        if($(this).attr('name') != "viewport")
        globals.meta_tags[$(this).attr('name')] = $(this).content;
        $(this).delete();
    });
}
//Le site est-il construit avec Scenari
function isScenari () {
    //"<meta name='generator' content='* scenari || SCENARI *'>"
    var meta_tag_content = document.querySelector('meta[name="generator"]','head').content;
    if(typeof(meta_tag_content) == "undefined")
        return false;

    if(meta_tag_content.match(/scenari/i))
        return true;
    else
        return false;
}
//Le site est-il dans un cas spécial, ou le sélécteur est défini directement dans la page
function isSpecialCase () {
    var meta_tag_content = document.querySelector('meta[name="generator"]','head').content;
    if(typeof(meta_tag_content) == "undefined")
        return false;

    if(meta_tag_content == "special")
        return true;
    else
        return false;
}


//Le site est-il généré depuis LaTeX
function isFromLatex () {
    //"<meta name='generator' content='* scenari || SCENARI *'>"
    var meta_tag_content = document.querySelector('meta[name="generator"]','head').content;
    if(typeof(meta_tag_content) == "undefined")
        return false;

    if(meta_tag_content == "http://www.nongnu.org/elyxer/")
        return true;
    else
        return false;
}


function isWebVersion () {
    var meta_tag_content = document.querySelector('meta[name="platform"]','head').content;
    if(typeof(meta_tag_content) == "undefined")
        return true;
    else if(meta_tag_content === "web")
        return true;
    else 
        return false;
}

function isMobVersion () {
    var meta_tag_content = document.querySelector('meta[name="platform"]','head').content;
    if(typeof(meta_tag_content) == "undefined")
        return false;
    else if(meta_tag_content === "mob")
        return true;
    else 
        return false;
}

//Le site est-il intégré dans SupCast?
function isPairForm (callback) {
    // if(isScenari())
    // {
        
        var hash_ressource = getHash();
        if(hash_ressource)
        {
            var http = new XMLHttpRequest();
            var url = globals.webServices + 'ressources/verifierRessource.php';
            var post = 'hash_ressource=' + hash_ressource;
            http.open("POST", url, true);

            //Send the proper header information along with the request
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            http.onreadystatechange = function() {//Call a function when the state changes.
                if(http.readyState == 4 && http.status == 200) {
                    var dataJSON = JSON.parse(http.responseText);
                    if(dataJSON.id_res)
                    {    
                        if(isWebVersion())
                        {
                            globals.id_res = dataJSON.id_res;
                            globals.id_langue_res = dataJSON.id_langue;
                            callback();
                        }
                        else if(isMobVersion())
                        {
                            console.log("Enregistrée.");
                            callback();
                        }
                    }
                    else
                    {
                        //TODO: Gérer l'importation d'un formulaire de validation et d'upload de la ressource.
                        if(isWebVersion())
                        {   
                            alert("Cette ressource n'est pas encore enregistrée sur les serveurs PairForm.");
                            console.log("Pas enregistrée.");
                        }
                        else if(isMobVersion())
                        {
                            alert("Cette ressource n'est pas encore enregistrée sur les serveurs PairForm.");
                            console.log("Pas enregistrée.");
                        }
                    }
                }
            }
            http.send(post);
        }
    // }
}
//window.onload = function () {

//var head = document.getElementsByTagName("head")[0];

includeHead(globals.dynRes + "js/require.js", function(){
    includeHead(globals.dynRes + "css/supcast.css");
    require.config({
        baseUrl: globals.dynRes + 'js/',
        paths: {
            jquery: "//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min",
            jqueryui: "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min",
            jquerycookie: "jquery.cookie",
            jqueryuiposition: "jquery.ui.position",
            J42R: "J42R",
            languageManager: "languageManager",
            main: "main"
        }
    });
    isPairForm(function () {
        if(isWebVersion())
        {
            require(["jquery"],function () {
                require(["jquerycookie", "J42R", "languageManager"], function () {
                     require(["main","jqueryui", "jqueryuiposition"]);
                });
            });
        }
    });
});
// includeHead("//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js", function(){
//     includeHead("//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js");
//     includeHead(globals.dynRes + "js/jquery.cookie.js"); 
//     includeHead(globals.dynRes + "js/jquery.ui.position.js");

//     isPairForm(function () {
//         if(isWebVersion())
//         {
//             // includeHead("http://js.pusher.com/2.1/pusher.min.js");
//             // includeHead(globals.dynRes + "js/jquery.contextMenu.js");
//             // includeHead(globals.dynRes + "js/jquery.contextMenu.css");
//             includeHead(globals.dynRes + "css/supcast.css");
//             includeHead(globals.dynRes + "js/J42R.js");
//             includeHead(globals.dynRes + "js/languageManager.js");
//             includeHead(globals.dynRes + "js/main.js");
//             // includeBar();
//         }
//     });

//     // if(isMobVersion())
//     // {
//     //     includeHead(globals.dynRes + "css/supcast.css");   
//     //     includeHead(globals.dynRes + "js/mob-viewer.js");
//     //     includeHead(globals.dynRes + "css/mob-viewer.css");
//     // }     
    
    
//     jQuery.support.cors = true;
    
// });