
//Global vars
var globals = {};

globals.webServices = "http://imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/";
globals.dyn = "http://imedia.emn.fr/SCWeb/dyn/";
globals.dynRes = globals.dyn + "res/";
globals.modules = globals.dynRes + "mod/";


$(document).ready(function () {
	
	globals.os = "web";
	globals.version = "1";
	updatePostOs();

	if (isConnected()){
		//Vérification des droits de l'utilisateur
		checkRights(function success() {
			displayRessources();
			navBar = $('<nav></nav>').css({opacity:0, top:-40}).load(globals.dynRes + 'mod/navBarBO.php', function() {
		        $('body').prepend(this);
				updateNavBar();
		        $(this).animate({opacity:1, top:0},300,'swing');

		        //Profil bouton hover, display du menu
				$("#btn-profil").hover(function () {
					if (!$("#panel-Profil").is(':visible'))
						$("#panel-Profil").css('display','block').animate({'top':'34px', 'opacity':'0.98'}, 150, 'swing');
				},function () {
					if ($("#panel-Profil").is(':visible'))
						$("#panel-Profil").animate({'top':'44px', 'opacity':'0'}, 150, 'swing', function () {
							$(this).hide();
						});
				});


				$("#btn-logout").click(function () {
					logOut();
				});
		    });
		    includeGUI();

		}, function pending () {

			displayLoginForm();

			if($("#loginBox").length)
				$("#loginBox").fadeOut("fast",function () {
					$("#postDeposantBox").fadeIn("fast");
				});
		},function error () {
			displayLoginForm();
		});
	}
	else{
		displayLoginForm();
	}

	$(document).on('click', '.btn-modifier', function () {
		var hash_capsule = $(this).closest('section').data('hash');
		displayForm(hash_capsule);
	});
	$(document).on('click', '#formResCancel', function () {
		removeOverlay();
	});

	$(document).on('click', '.btn-visibilite', function () {
		displayAlert("Cette fonctionnalité n'est pas encore disponible.", 'info', $(this));
	});

	$(document).on('click', '.btn-supprimer', function () {
		removeMessages($(this).closest('section'));
	});
	
	//Récupère une métadata particulière
	function getMetadataValue (name) {
		return $('head').find('meta[name='+name+']').attr('content');
	}

	//Récupère toutes les métadatas et les renvoient dans un tableau
	function getAllMetadata () {
		return $('head').children('meta').not('[http-equiv]');
	}

	function includeGUI () {
		$('#my-ressources-loading').animate({opacity:1});
		var uploadWrapper = $('<div/>').attr('id','uploadWrapper').appendTo($('body'));
		var uploadImg = $('<img/>').attr('src', globals.dynRes+'img/icone_upload.png');
		var uploadTitle = $('<h1/>').html('Enregistrez votre capsule');
		
		uploadWrapper.append(uploadImg).append(uploadTitle);
		uploadWrapper.click(function () {
			displayForm();
		});

		// .css({position:"aboslute", bottom:"20px", right:"20px", width:"80px", height:"80px", background:"red"})
	}


	function addOverlay(div_to_add,callback){

		var wrapper = $('<div id="popinFocus"></div>');	

		$('body').append(wrapper);

		wrapper.click(function () { 
			if(typeof(callback) === "function")
			{
				removeOverlay(callback);
			}
			else removeOverlay();
		});
		div_to_add.appendTo($('body'));
		// div_to_add.css({opacity:0, marginTop:"-30px"}).appendTo($('body')).animate({opacity:1, marginTop:"0"}, 200);
		// $('body').append(div_to_add);
		// $('#tplFra').css('-webkit-filter', 'blur(1px)');

		return wrapper;
	}
	function removeOverlay (callback) {
		
		// $('#tplFra').css('-webkit-filter', '');
		// $('#formWrapper').fadeOut(function(){$(this).remove()});
		// $('#pres-wrapper').fadeOut(function(){$(this).remove()});
		// $("#formContainer").fadeOut(function(){$(this).remove()});
		$("#popinFocus").fadeOut(600,function(){$(this).remove()});
		$(".popinWrapper").fadeOut(600,function(){$(this).remove()});
		
		if(typeof(callback) === "function")
			callback();
	}

	function displayForm (hash_capsule) {
		var formContainer = $('<div/>',{'class':'popinWrapper'});

		var addRes = formContainer.load(globals.dynRes+'mod/addRes.php', function () {

			var wrapper = addOverlay(addRes);

			$(".popin").position({
				my: "center center",
				at: "center center",
				of: wrapper
			});
			wrapper.fadeIn();
			insertHashAndUpdate(hash_capsule);
			addRes.children().scaleIn();
		});
	}

	function insertHashAndUpdate (hash_capsule) {
		// var hash_capsule = getMetadataValue('hash_capsule');
		if (typeof(hash_capsule) != "undefined") {
			$('#formRes input[name=hash_capsule]').val(hash_capsule);
			updateFields(hash_capsule);
		};

		// var inputs = $('#formContainer form').children('input').not('[type="submit"]');
		// var metaDatas = getAllMetadata();
		// $.each(inputs, function () {
		// 	$(this).val(getMetadataValue($(this).attr('name')));
		// 	// inputs[key].val(value);
		// });
	}
	function updateFields (hash_capsule) {
		var get_params = 1;
		var params = {'hash_capsule' : hash_capsule, 'get_params': get_params};
		
		if (hash_capsule.length == 6) {
			$.post(globals.webServices+"ressources/creerRessource.php",$.param(params),function(data){
				var retour = $.parseJSON(data);

				if (retour['status'] == 'ok') {
					var row = retour['row'];

					$.each(row, function (key, value) {
						$('#formRes *[name='+key+']').val(value);
						if (key == 'etablissement') {
							$('#formRes input[name=etablissement]').val($('#formRes #etablissement option[data-id='+value+']').val());
							$('#formRes input[name=etablissement]').data('id', value);
						}
						if (key == 'theme') {
							$('#formRes input[name=theme]').val($('#formRes #theme option[data-id='+value+']').val());
							$('#formRes input[name=theme]').data('id', value);
						}
						if (key == 'id_ressource') {
							$('#formRes input[name=ressource]').val($('#formRes #ressource option[data-id='+value+']').val());
							$('#formRes input[name=ressource]').data('id', value);
						}
						if (key == 'icone_blob') {
        					$('#dropfile').html('').css('background-image','url(data:image/png;base64,'+value+')');
        					$('#spanWrapper h4').html('Image correcte').css('color','green');
						}
					});
					$('#hash_label').html("");	
				}
				else
				{
					$('#hash_label').html("Ce hash n'existe pas.");
					resetForm();
				}
			});
		}
		else {
			$('#hash_label').html("Veuillez entrer un identifiant à 6 caractères.");
			// resetForm();
		}
	}

	
	function serialize (formID) {
		var serialized = "";

		$(formID + ' *[name]').each(function () {
			if ($.trim($(this).val()) != "")
			{
				if (($(this).attr('name') == 'etablissement') || ($(this).attr('name') == 'theme'))
					serialized += $(this).attr('name') + "=" + $(this).data('id') + "&";
				else
					serialized += $(this).attr('name') + "=" + $(this).val() + "&";
				
			}
				
		});

		return serialized.slice(0, -1);
	}

	// includeGUI();

	function checkFieldInDatalist (input) {
		var datalist = $('datalist#'+input.attr('list'));

		//Si l'input de l'user est un choix de la liste
		if(input.data('id') != '#')
		{
			//On remplace la valeur dans le champ
			input.val(input.data('id'));
		}
		else
		{
			//On remplace la valeur dans le champ
			input.val('#'+input.val());	
		}
	}
	function checkMandatoryFields () {
		var missingFlag = false;
		$('#formRes span:contains("*")').next().each(function(){
			if($(this).val().trim() == "") {
				displayAlert('Ce champ est obligatoire', 'error', $(this));
				missingFlag = true;
				return false;
			}
		});

		if (missingFlag)
			return false;
		else
			return true;
	}
	
	/**************************************************************************************
	**************************************** Binds ****************************************
	**************************************************************************************/

	$(document).on('click','#formResSubmit', function (e) {
		e.preventDefault();
		e.stopPropagation();

		if ($('input[name=url]').val().match(/.*?(.zip)/)) {
			displayAlert('Veuillez enlever l\'extension .zip à la fin de l\'url', 'error', $('input[name=url]'));
			return false;
		}
		//S'il ne manque aucun champ,
		if(checkMandatoryFields()){
			// var params = serialize('#formRes');
			checkFieldInDatalist($('input[name=etablissement]'));
			checkFieldInDatalist($('input[name=theme]'));
			checkFieldInDatalist($('input[name=ressource]'));
			
			//S'il n'y a pas d'image, on met une image par défaut
			if ($('input[name=icone_blob]').val().trim() == "")
				$('input[name=icone_blob]').val("iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAJ4xJREFUeNqcXVuwJVV5Xt17nzkzDDPg4MgwgKNIiYJyMQkKiFdAQMEXS6vMQx5SqUoqqcpDKqlU8qA+5SHJQ+KDSVF5MSoXSy1L1AHkouMFwUuQi1Aig+AwDDMwtwMzZ87Z3Vnf6vWt/tbfq/eZZKjNOWfv3t2r13///+//u3r88ced/mvbNvv5yiuvuMlk4lZWVsLf27ad6Y4cPuyOHTt2yUv7D1y7fGL12oV1686rqvqs6XRhI46tKhxZ+Z9VOg/eauW8PKbGLxU/rbJr46/G/95/p0ovfK+q6+K68Yqnd3i7aZrBcek8cg5+zuOr+H1de2vWE4875t/c53/uXlyY/OD000+7f3Fx8aF1i4vH8G39zlr/qrUOPHjwoPMnDwT5/ve/f8mb3vSmT6xbt+7GhYWFd2zevMlfc72bggh1FVYdFlnFrU0L7jawkWuFRfob78jgF+1ko+Ln3PBug6vuGly47BjPyg2K7BC2se1OkNbC73bHdgutA+OklXVrqrr7CX+FDcWPpruH7qRytu78bdO61dnMnThxwi+5fdK/7jvllFO+4V/3upP8tyZBXn75Zbdr164Lt23b9g+eEJ/YsmXL4qZNm9x0Ou02qqoGnKd/ZxeTTRk7thIiKifbf2PrzohS9VKq5ytJDI+x92GvpVJi18XjcH68wMSvvfZau7q6uvOMM874J793u0r3ctIEefrppxe8yvq75eXlvz377LNPO/300wMhSjdvVZ3dTHusvm83wxKrtLFjm1U677x/Y9+3x1hCl+7XfsbPjx8/7g4fPnzca5VbPGN/xmucg/9nguza9cOthw4d/K8dO3bcdOaZZwa1RZ1b4lp7HruRdtG6YaVjlBtLxOV3wIljxLYbOU+KKTnz1rsWE+pPSqG+9+qrr0JiHvRE+dPNmzc/cdIE2bNnz3mPPfb4HTt2vPEPXv/61ztP2cHiSsawxEkloo3d9BiXzZPCkqrD53Z9Y0wzdo55anHsnsYYSgnkCQJp2e01zqc9UR5ckyAPPvjgmUePHt153nnnXXraaacFYugNqpSUFr+WWikRz0qcVTdWBax1nKrA0rnHmGPe5q5lt/Qz7I9KNwlCokCFHTp06Pnt27ff5InyyChBYDOeeuqpO88///zrVDKsmiqpgjGuLYm6qhkuEtfo3c1qdMPnbei8zSrZqDGiz7MnYxJdUlElVcaXDxsgKb/yXuv13gvbm4jJX7wn4H7/+9//o6fadTDeIMaYzSh5S2uJLLnEci3OX9fcrGpAePGeix7UyaoUG8e0Ju7gy655rXtXptJ/iMfwwv3p7/x748aNeF28b9++f/bXmQ4k5Nlnn73ME+THnmLrcbAuvqSexlzKearAhAPFTR5sqEtufkEa2xQ/jLnULo8WChIV45U50lPynNZy4+cx6MzHKiAihMB7sa13iT/tNdJtOGa6e/du56lULS0tfe7cc89dT29qTLeX/ql3UlpU/9n8RasaY2CW4rcCU3THDXSJq8YkpbD+FI3PkbiSBJyMwzDmkeleeXVV7d+//++8vf6O10pH6je/+c1u7969f+gJcaP/MBMtKyXWGJdSGmMBVUkVqaogx+CVuKhtBsdQrXTqrho4GVblpOtL1J+tpx0S2aouXkNf6nbzeHL+2HrVKUI8h9eGDRuw55d5Sfl4uCdY/K1bt/6Jp9DER+JFb6q0kJJeLhlYtR96o6XvZrkquR71nRJSrwN7B0ZyhTWX1pXZNNeOZAaqopdk16/X6PJ4ZSax19XvQysdOXLkT/z766YvvfTSOT7EvwYuLggSbuwkYouSiJdEe94xzHvVVR22Bf9vGuaF8k1vRwJL3MyTTz7psH7PWIFL14oh1nbLXUp2zotd1rIZ82IudXKw754gl3vP69L6wIEDN3pf+C30quzFNMAqcZqK5sm4vQNV6KLaSBsxzM4Os6vdT4g7iHHLLbe4L33pS87fSyY9/VrLXmEWT2Ub3op9aYsqh6rdSobes0pU6Z54LPbe38smHzReVT/zzDMf9W9MedJ5ibWBuBd09cBAt82akXDr8k2fGDWgN0+mAFc99thj7s4773Q33HCD896h+9rXvua8xGd6v/sOs7+u6IZbCdR/JdtR2my76SXmKwXD/Bv34zXVlVN/0OVKjJNJNRTjgd7/NJ5Nl7ZuW3dSHttaSUG8v379eoc6Dojx3ve+11166aVIebsf/vCH7tvf/nYg0LZt25J0rxXpM0XfxScx1W/SQ/QSuzR8O3BO5uXc7DoYyevvUL2eDufX/n/bYO1LNqFEzTEKt6mI0/Q3mWKIOvPSSiyZ3VwpUIvHYeGUDBDjsssuCyJ/6qmnuquvvtrBa9y5c6d74YUXkj3JuHxSpw1PXGy8rq5W03lN8ProPc1m5eBRr2G1C76rr1JwTKL598+YUiWoHhwzfPNih14NuYFXxAJTHvC16eYHxlEkTj9bXL8hSAak4KqrrgrEwLoZt8Btv/LKK8Px99xzj/vIRz7i3vCGNySV51xWmAzXaEwgy6xB2/brG2SlleFGVHU74hWOpVbiPSxOx4z1WvahdOKS9LRSXatYznQ5gUrJvV7yumNhwB97DMS4MxHDFsnwD5KCz/EeJOW6664L6stJFbBiqqbqJbVtWpfqvlHzZvui+1BgmP9P5lttULznqrbUG8vl2MCOnGI9sWFuqMk8ncyzCqXduhh7pDU0nQF/9NFHvZr6VpAAqqlSwAkVAxvznve8x+3YscPdfffd7sW9L/ZcWqvtU1VWZTFY7331ZWIbbdtYpyQNY/GPZeAUP41lJk+qHCsbZ2+mf/WeTnYDMcpm/d0aUR4LyYCa+s53vhM4/13veleQDOhjG7ApcyAfB+IFotxzd7ApIFYza4yq6AkfzpW8piZJc+vcIMnJ9VtOH4vUrcaxtgf/cE91SS2NJRX7z4aucVrAYKOapItpsDVIDNfnjTVt9hldW9oMEmOeROtntCmUlH379mVS38UTdWIucmhIs1T14LyzWZNJrpYOrMSUXGWbguIe0fmArZuW0iJjgVhPhDllzrjBlTHgmvIe2I3CNaF2nnjiiSAZ8KYuueSSzIBrRqEE4eHfkLArrrgi/A2i0KaAsHQ5bS3DDZg0ZhBMAnSoRXJmHXOQWD/R+AXSG7K9jDqt+FnDPUguirdC6I4zWCnrANTWm0qImtwAkhhwbcHhIAY2UDebXDXmDKiPr4YeRLn22mvdWWedlRF1LDnaRgjQZNL/Tk+g6IG5apAmGksfqetNya9LRmfMhcvUgjX2JvvZi2MTOayJfvgsOwdUBmIDut5QMyQG4wwu1maJxxwPWwCjTQFRENF/73vfcy8fOJCl5bP7FpyYelxWXTOQLLq4c8yAzaiTcSAldclvVoqr0cmJ1BQNmj1Hp6PrgZHvviep95WVsPHwpjTOgB2xuaexSmaJoUo25dxzznHf9qrw8OHDHaxJA7XKZdG6JrqKSVY3LEsggz4ztlKDQw02+VnSImM18Xn14DFDqrmjUnGfgZlyJYm43ut6SIYacBCjZNPmlWaVwexNYyNgU3D+7du3u69//evu6NGj6TqdUzKshbBsOfA2hTmoUvfs2RMIvby8nLm0Y3uoZd1g12z2suRX2/pDyUaopGiKIbmn/NxLRRXtCa9HNUUDjtwU1kM7oevRzAKLPJp8tDYlk6q4VqgvpFmAN7vtttvcoUMHE7qmv4c81VGq99NtxnvHjx9zP/vZz4J7vbS0FAgNIAPvX/dY99pql5q6ixwUfHWTb7ELUe8gXNC1xbR55hImbqpSkEXXlmoK6gTEKJUC+GpGVIEtHhXXQQfG/zxt82b3oQ99KKRWbr31tgQqt/ZJpUv3hvHT4uK6oKLuuutuVF7DGkEIEESJoqqqz43NMgOfCFICJ4y9irisNk9HO0bjxTq15KYWF5Nk0GYop86rYawF0SlVMnVzV6L6+sAHPhAKW5AUqBqsqa4nnbNR0ByqeiGdhw8fcV/96lfD5uM8kBgQCChFvAdpwd+4din5OKi12A1WbiBHDHP/zUD/YSP5mvBGrM5nsi5mbZlCBzEgGWNBn3JVqYw6lsxr52Sqw734c0J9QVLgBt9+++0B7T+dTsLGWvunNhL3iU2/447bw7ovvvhi16F1XABZgwhUXXiRKBYYYkvmta2EKY5I68s0Yt2i6oEkxUJBz8XSOiCgnU4yvJr69a9/7Xbu/G7Q5SAG6/nWyJUy0SWjXsRBzSkbdPmr7vjNUX0hYLzjjjuCpFBSlUFns07lTCbTsNmQqte97nXugx/8oAOwELYQUsfvrqysBkIAPgri4fcSokXTQLVNN5SMN/T+pO5uMFX32iF+qo2AO3AICkYJRSJczeISbMYVV3RBH25AvSHVuWrPLFyzlFxUtYSfWAtewUEQxtENYULyfe97X9jYW2+9FTVut8G/R+nvIvtJkGxs7le+8hUHQCGJge+DIPhJGwgCUn0tQVKWjobfLaibHQVYx+RTn/rUZ2HYOt1ZF4EEqm5SutqVA0pmsJHFrY3O71Loj7nvfve7KR1ibYYNoMY4fCx4LeXgtG5eqYrAi15b9Pbe+MY3hp6YBx980L3l/PODGiIHLyxMPaGOBoJRMtArQ+/Pag1lrhLaxDKZP8drU+XKtZAl2OSBAU3tUj01qkxPxzhj/aJ75JFHQo0CaooR+FoIQFv7ntfSoFFva6SgWJMIdRAJZmPw+P73v9/dd999QX198pOfDIgWMODS0aXw3pYtW9yHP/zhQAzrTdp/iEdwDDSGLUgRlKj3WRdh/24IZCgVVYK4z/qos21KOFlceME98fgT7q677w5qgQbcBm3WcFNKcCxsTK86yhF7XeiAGlQ5TYyS7E5dp+9vOvXUYFPgNUEajnp78dpSp6ZAHEgGJIcqGSqRAD+sE5oAn+MnNQ9VI20K3GEQicRMsVURxCC5nJJHo/p7kPGsJBqPAeAjj/zKPfDAA+5qUVNjbWUlMLaVDAVlZN89iWaf1JeoWQR8LkUoHAlJAfM8cP/97qteKrAOSgacAJtwVZC1Bn28DjaftswyH64VHZh2WspfZTlLCepK0Xle9aqC4dONQtC3a9euoKbe+c53ZhH4WKubuoT030tJT3IduJA3qc1F4PrWptUzN7zNCFWZhp9gtL2k/PKXvwzXgmRDTdEmjHUB0C5ar46uLwgDCdHgmNed4hdQBydRqs5D/pXwR7QfraRIHv3Vo8E4ghgXXXhhIEYpQ2s9JystJU4n19Fdxk/cMHQ2fsdmz8j9Bk6qBB5DtZMxIA2op+Azrn9MspUwxO1aDYT1gaBYK5gIUscKKK43nXVtvOFAXjC1Jgvc07Vlbk7GCNzcNglNAsn46U9/GtTUhRdd5BYQ9NmW6EI5c6x/0aooEoM3g8/xN+6Dn1kmGqArBSTH+7WqtI7nzaBKXfN72nyVeGVY7CeJoowOYkBK4NGBIKoFpqpL1WvRDGUAItRVseWgAGUJxHjooYdCbuqtF1wQ3luNm5Y4EHYgflc5b557y+tjw6GmtGjF7+P9EAMhnW+AEJXpa7eIkqrgLSkhswyA6ZMpVRJ5fISKZg4MysrIDoAw6mBMbY15AAvy14Hryh4NFXddSCjQ+89AjIcffjgQ4+1vf3sWZyRPSHT8PPD2WBplYd1CskUWRMBUOCQFnLgOQVoEw7l4bU6IcGLEmxEYVKk3vSpIrf7NfSyVx0EMxH1nnHFGWKMKA041tai7lAbWdHbdJleY7l0Vy7VMrKhkIDd1obcZFjeVvmNamucNDtDvg5uSZMyazPhbJoHnshwLRQum/t5aWI6kf5qRVrawyaZaavslS/GRTe2AGHAMGJ/o9z2DtVNafYh4J245DD9LUdQ5eE0XggjcEsNGyeTwxK1xfAbhnJYrdVMsMfpNEltgMtKLXk0cj/7+QlxPU8BKZdF9tBtNHPthbUMbj1F7U5uywBj4ArV9pFbI1Mr4dBqmWivQ3ggaSmtTnCQbeUxHjJ96NXWVu+Ctb01G16qgSQfOymvYMeuqYm5Lr53NWB+IQXSGuuYkqALaAufF3Bk9G9iUWobTtCOoySZ6aEV0Zl9kz9Il0+iilzqm6I5jLcytqa3hC/c2HUA/UQGr85S8S30S3X4GbnEddZEoRKXsKk+Mt0MyYKCjoZ4L3I4bolWzEqS1l4y8guiYX4vMY3FjmoqHlwS/H9KyEMu1TobbuEI5mmtU7G8T2uzKuN4i6tH/xPpx/dhukCJ7zUbI9zuVpXaDCJC8nlS5PPvQiRck4yc//rG70qupC972tkRlJ9xXbFcT40opK6Ew1JtSkJoiPrBt2uOeMmwmrQQvZzm6m7b9gj0pg7QPuThu9iR2CWSxWFzPzGK7oppljSikSlZOpNye2kh197Pkorq61qg1EYGYWgK8Af/Rj34UvKkLomtra+CTrmhf6LOIc7KkM9bic8lZ2DwS2QLRNJAdq3SqBwkgBT0bhRZlm89RUliLoN25+VVkVpxvJq58thavYVZOdJLNLELnVU669r22LbrI/tVOGeozaefE4DJgCel0pEQCEhD1jCfcD3btCoDmt3nJYC5/0D0UPLQ63Bz97FK7sKowXOOE37RTNm4M31kJeNxZsS0uA29XbuD+2mwD7geSEtSXlxao1yYbeBaJB+NaVYOhZZlXRomIhh4/KUkryyeCaqTNIOE0U1Gq8QQbwkgdfyS1kurinY4OhcUIy0Sl76677gqScZGPwBXemScoxWhq5pjH2tS5bMieF14IU+v+8I/+qJOkkJPqPbs8qYeNawbwT5suCYY31MAPh9zUW73zgVxVwhrTMUgaIXeDs4wCVJTpAuC1gmRHA859zZgwboxtg0uZbS1T2vRFV7iZZPBO1DOQ20ENWYmhE+NU9eFyVd//0EN2JKFnXdENPoa4//773d4XX3Q33nhjr6+rUo0c3xWJM+4qbxpaAMRAOh2xwMUXvzPlnGqD5M8kXVIqqdAEiR2gHTvPEvukxNAIX4No/a6ovrZWt5ZqixsaYhSvC7Ft4CqUXS+//PIgGSHOkJDfFZo0LVqDNwMuWo75sxPicfD1hq1bQ5r7mWeeCYgUttxp/UTxUrVtrZNSLhsqX5UaOJKd69fnqYwVKTnThq1EdEpQO1DfzObCa4rGmtgwloGppiw4Qh2JtGfyOc9Tj3ZNRQpuOGWD+83Tv3H33ntvwk3hBlejbre17za2JJTqG5k0iCpjm4KKMOCeH/3oR90zu3cHRlDAw2QwTaEt4rOoplDHvtUTAyoKQGsQhTE71edEzq9MmcVLhVQKdT+MN1R6htZpxzuVbWNPXH9bjwK4YvVL1RSKS8zdU5qsC9lEF1CJlNxqRZKAI2LK32lmmd6Mfx/Z0I/ffLN77rnngqTw2o1Bg6ysrhTBckEyBJCAVoRQjk2uc50Z1NUYtEHdQHJn1P+xrsL4J2iO+BldW6RqEnMRXL46y0AfGodQem1LQq0xAK0aRBMU76A6O9273/1u9453vGOQ0NNaPLm8Ng0sFt0xiyiUSnzwxO3xfTX4kJSPfexj7nfPPuu+9a1vFbPApdks2CRAdb785S8HIkAFbohub9jwqDJ7VMpql5GmSmHerbZuai/1OEcoL0f0ItPqPG5s5MZEytAG+tTbkC446kQW/vpTTz0VeikgGaz0qThmsEoGatKJZLFUKXnJPFBhAIEzxjVwmX8hTX39DTe43UZ9Ufcq3pfFKhADkgH1BMmAhGjt2s6vAtaqjmkdzXlVGoOE9zsjjKgf1wbjLh9f9tK1GrIc7K5aXW0GNm9G3HO0ozT8WoGsE+eC21dnYXG//e1vQ9BHYtColmCV+fdXB4Sy9QU754OSpXamNL3u3HPPDZLyrCfKN7/5zeSdUA3wpkgMoBBR/LnmmmuysqvGKQrCG5sH5gqxD84FpoWagjMQtESVe4GcVMQMd6jKBsM9yfbSlqQzKOl0YRoAwz//+c+DJ3XeeecN6hIakWZoiYgMSa1iTc8JisxohPN75IrYMelDtInGs88+Oxj63/3udwGCypQJCYN/gIJCTaH0CjUVYg2TnrCjoUpzTNIxkdHocbEaCfWXbEHlMhVFVKQiKC1Q3eKGoWK9tLW1Fkjgpz/99NPBmMJXh46FUaTOtdxYagFOXBvnXVk4D+E2tenMzdQJHYbCkBdU2m666aZAFBh6cjhu6MjRI2EIDSQCIGpAcajTLdLcDpDRwE5VyWqMOZjrglsLYnBPQg5LBgy0UqLOO8nIdHmbBomPc2GvJz7w+ix0NDYL03RwY0Tj2cVPJI9T7pVoRlvKsnY36a0olmvJncbG8D3gprZ6hnn4oYfcfr9m1F+CmrrDq6nXbUk2ozSvS1WX7V6yzKOGF5u5sLAulWITcV0VB6314A7XutGmTzs/0uSzDk9Vn8EAQioY7jPStI39pamitg1ObYVNZ0w4bySqv8r45nWVtzJkqiZ+Din+mJcUSAkM/f79+93mTZsDiI3qxKbGNVrONjUGndYr5HqwJwgG0QvC/bEpEcgGsxroo2Qjj43KTcnW7lmbsr3KFXQJIV5E25GriMSjLSn1YNjMK/R56LtDkUhGlVvURyYpZg6JbmwTiXiOtymY/AOoEVCGSHbiWrZIpJs91g02hhkGYaGmYDdOnFiJkNAmZW/h5zODm6dX+sEDignTnniLvEyoE746uEv/YbADUfdiYfwy0d2lQWDcOHIRypb4bNHf1GteRypRBoNc4u+11Outt9bntbo1Bpf4+uvDmrD+sdEWpd4SRRtaD4v2BJ+xuIRYxelIwAgAmbWzrFW7yxjnsZhKhea4mNSNmeA2JRchEfyATZAVjJt8sR2pXWvFjvEKa8jYKBITbiKIciKWUxPAYgTvZVWhbrYm7MDBVA/Wrikh1UPUNROKowTXSh9LwOr9lbDPHVGa5PKSCXR0hh1UoPUfvKZqyHgzjDuC+vKLWY6VLnyRaAnijWzqhNLB2b+vxX4Ipk2QyQXYOPRCCKy0Fp/f2qFSaZSJynYOet4OhFEuHZtdT4aiN0UODkQQglmkS9fiDecnHw1oMxrMRnMtyuyhcMaTU+QT+mG6EH3oOujs5dlygm9aH16DRejZzZs3pafyVCIBrEpuPGWjW3p1KQNSsFfDpkCSKjC17KyOYlSCLQJZNWYJqMTB5mgNnKqKQxpCRF4ARvRS06b0it2fcGwsUmnfCNVjgAGV+tHJvQS5wWvAilYkCUlUCSGc/D6MKjtR+3M2cTzFJMKAqiBBcFWTL05XNBIJaq0qSIndfFVVFuin39WATGeL6L3jPJCKzoCfCK0W3XytupPGpi/d8ppMB1VJ0xDjXJKiXF1y7drt1eOyoh7TA5kngpoJngGSZxHIQGlRQ9898GUSeuu6zaniWI1c1cw8gRcmCyHpd8yrL0Jq5jUN6SOUStDTseH3pZp3ydNieRcvrmM6mSYfEJoiqJbwPt3bvjqqbrAFGlYjwDrbXh5Ulm29Uo8mpINDdnISdGo4eWwy4fdgD/ATMQzeW1p6LXXp6lRpIjuI0GiZKY3eF3JCEw5gMZxuXVjNEo/N+irZIOuxcQNZzwgYLkTfJh/XD8DMZ837sCEBNZDKn0wqHVKXAbhrwXKpqtLfQ7LUDouxxk5DfARG7IqCFHTR60JIs7D/IQ0SgMgCUhSbRRvWvNrOqM1oxCJsn4ZeS5pV7FVU+E0JKGBr51QDpWYfSzDGGQGkHWsgVqU0Jnjt8lZ99lrRM630Mtp11YWn+JgMR5v1h7BGrh6HJvCQovbrTuMzoGvRSozjtA+7Ii6K4mjmelDcVf/DpiCXwyqf7WtcMaXewWg8CVS15GtntOtG44V7WCSILdUy4sSJJq5ZkIxMwbch9pA6jHQih3teOREcANuApJN/mJzM5mWVpviod6LpEgaPrENgE0FIphMGUxQiPMeOIHLJ3VuJ55+EGjVbjlmFCwyCGmSTdyaVjLxVMZq1teUCEoRlVyYTQ8pj1sSgM7ZguBwq2ooBdwn1YvJlMasdXNw1xoKoPQ52m5tEd48qwX7ZCTeHuVYbTgm/My7RjGyXfm9TBdLCXWaxMtiLNdLu4MQueIT3RReZP/V3myfLMGSFqN6WdzkVSFPoafiYydFZnNcwsGuiepaHAQjjlOydbXYSZ6rNvCz1m7WAQo+JM21TA8tqM2wZgARwfmKV423xRohxJpNs1mFqM40cjgifHarsGSQxbDJTMwSlEega0eP7yyeWAzOFdgVE4KudzVOVN3gakJSomV4PtkZKzgHqhPubToLEM1i00+1s5lnr7aEkXGos0RP0f1dZc0WOUpGpazEQrCYSLCWu6DeWsBo1mApFwgazt3siwdRYakWZaWx0OZhu86mbEqSUQV9UAGIPqmKaaCINpW2UAsQq7FXpAshmMGNFyxI6fcmqrWBDNKBi7EHvozhuCVNyoqEllqikHzuj3YTpB9NImBl7EenVyfOgwmjvCoubhc1h/YWco882KcFES+1nphkm4aYIdNDYoWnKzZx9LFEH1VoZTHKG1/VEsvUV60jQXmgKn/bS72WvsrBI1dM6LGxshm8PRouGjYFdMuLOu5JtapaZRGRLNekMJkEB0tETYKEKz9FGTkpPaU5vCfPLPJsa8NXZqjySoo1qNurwWXk4MlUDmKVkC8b6QtQLtNXD3pFoMomZ6ixd+1DGplDbtgaVcM4wXypsfPd31yjqBg/tatt8moKCAjpIqGCU4nXZ8rwS5zKqobQDwHSCWyLG+i7oC63I3hVFW0BmoO28L9eXl+Hq097RnqUSLZtWvd0A2FdbDXQdtndEYyU6FXnqRIr3VEGlp3smgkgqO6sz1BKohSi2ShzRBVJuMBZDhxTbyDxxYJzJuCxEsSM4SslCYm0X1y32KXQ6Cq2LsJzVZKca6WtnZrZpVoMLzG5kV7lB7NPN2/Xfr+oswC71KZYQOZSY5PaWngKj9QNbhtSRqarWNJG3GjO0LNbUWa8J+7zb/knYbT9jxY7CiBFN1p6mEftYTyJT6DTgw/xXk3lndg5it2GTfr1VP7gf8VH2yHIzIl2ZxbrQFjzIV1DL/IWJQuozhfRohVCNpu1nV3HMkmgItmp5AHAa9DKco56pwthG5oLkTVKz0PFjfTnYSgk7r+DWbvSv4yZotUCErK8l7mtIKk7Z3Npp2Oxpct64t7Nht/LYRAiuc2ZgUaqu4l62g3YEix+yRsuCkEvdQPYxQV2CMRpxPqqicoO0ec/lXT8IAdg0xLQf6zesT9kBW9vHTXISzwlplskCsqZTkZ3LmzNYkJhpm2kJRuxNVG34b1Z3PSm2R7I0HkoDQL5UEPiev6dlNOy86v/YCIPHKT2q41INwxSZSkg/TSeHxZt6QIriA4FcDkhr5GFcipdhkjPWrYOnFh81x/yZngeEoGorDSweIGAAVKg6qQgjbE3qKPzeVIOGI56Le2MribZeo9PtsLY+Dpop1u0ICPIL/8bVfFMLMHxpu1tpkOPgiQOxl1wje835h2MbGP6mT81PFN03yzwfcGhXm4gwoMiZiOhZCOPEOqZDSg92LOr8AO3sJ1T3EXbV1f3bps/siidmmbL0uHA12q2RJiUSpWVpaWk3CHKP57SrcWOwGyUgmXpfNi5R11Pnnnc15iq1SJPQKt7gMcBXUazKxN0tZK44G/bT2mKNnjNEON6V7c8qGVmnbTXJpJ+JwPRIpirvMu42swkBod3wuvDkUO6BFtC4ZuIR1IYoEh/5O38fD8Go7/Qf/LU/4Ay6lKkwZQAMGpXT01F1pgtJXlBM1YdiTuWyJ6BpBth6KnquxFngOOmEpV7mqD3clDbL9M2nlT70VoY61ynz0AreWEfyWQBgUruF1I3FAdA+Q7WCUThJDhltTpSTeKTxf/9keuTIkV/418Oew65n7KG9g2rU1a3LHrTlhs/102i/K+jAFNLe1FmfRUjeRQytphjySl8VN22WjT8Knaux1KzqBFLXjTXPI3Y+RTSbRe/aZLC5vnoSR1LFLITaqRL81OLUbGVQpUIbpCRqf/To0aO/gsqaHTp06I7NmzdfD8qpm6aRJFSC1kNKkbuqiAw+Ez5fLRt4DuavXAaMK7V/pZ5CkzmlGig9ZFjT26UaOwmSA91QGJuMPtSrlJJX9UpkjjYG2Q4q/Rkl53ZPh+Xg9nrd9Q0vJZ/xJ9zBh+VaP7kfJjxLqBT7/MPSRLgBDKau0vAYZnxLE0/18dyskoaMc5CoOs1etw9ZsZmAvEGUxO1bDRozPp2BbukJnoPmI/O52l1KAzursOl4ceodc1mRYPs9Qb6ICdjTGNUe2r9//7947v+82gN1yZSiHHfBdLm2bg1mp5hoGoqrrrTvvNvK2cpq0vt6g7WMdHLO9FoMSrN1KhOoumWNn0+Fm0zj+QS+1DZNgqfqtB61B2QsBtEakWsrQ+pT9C8SgMMK6PLS1sXfv+CFYk+w4QwIDx48+J9+o/94y5Yt7+EEaq2xa28IjX56bLYbPvdViVOC3FgVMOx8jT3osa7NSQ0lYHcpULVxkgWIW3inxhKl577boHgMb0WbQMkgMVRt8ZpROp70kvGvaDQK5kIed73ipeQv/Cb/wC9gE/P0qoeJhM8G708mo09Mtk6BFfUSBLSbR8WHP/JBKp1e12ZKVOXmPeilVMfWVorSXJdSNdJORR0M/Deod7q2SgCNxk1j06o/7i8PHDhwhOeb6mb4D/9n7969f75t27b/xvrZnEJ3OAwCi0SghChRSmJs8VAavVqbw6ltM1MYs5jeLs1Vjz59uVRzL8FGrXNQwvtaKVTwhPZTqkpXu6vlcc0aRDf4b1588cX7YFt47anlUO9pfcUT5fVnnXXWv+mMEG4gkey2abKEyS0BpMeGM897HF2Jo0vN98Xn6o6VEAqYrtKUuTGEiy062QcJqLsrWigcF+OSz7300kv/jthJGWFaCve9Tvt3/8Vj27dv/7w/ySKR7HYeo3YplW5EuWnelJ7SlGibXh97FLcStGSbxjpsS4jGMeSjSppFd1onxLbNWRyA10KNZ/q/37dv3z/jCQxWbU/dyHQ0T7lbnn/++d1nnnnmf/iLvAXqix6VbqytMtrNmfdwRfuzDLNpiw6ABTW0Iw8LturGjaDlS3Zn3vNKLAiv9Dgo6357Yuz13tRfeQ30dVVTaxKEN+rF6nvPPffclV5i/mHr1q1/5oPHU9J4CwOuGxsmPIZaL7WSzXu4fal4Vpqybed2jcGGSoScN+XapnHGMMUWOxa9qRUvFV98+eWXP/fKK688X4K5JjgT+tHdyBD7+Pernpo7PVHu9ASa+Aue7U92ammQgBr20iwUCzMqPUPDVi5L6Qk7YagyD0kuPTy+VFUsoWrsk4bs0xiczJO0NR9lvigRr/jo+2veVvyFl4oveBV1xEqqZabq5ptvHnCU6kC7UX6B53j19SEvLR/xtuUiLzFn+/dOH5O2ds5Y1hKSpZ3znPK12g/Gjlvrn+03KfVNWq+r8DyTGeoZfu/2eIl40jPxXX7/7/UGfLcOh5v3D57X/wowAOA3JLOldTGXAAAAAElFTkSuQmCC");

			// $('input[name=etablissement]').val($('input[name=etablissement]').data('id'));
			// $('input[name=theme]').val($('input[name=theme]').data('id'));
			var params = $('#formRes').serialize();

			$.post(globals.webServices+"ressources/creerRessource.php",params + globals.post_OS,function(data){

				var retour = $.parseJSON(data);
		
				displayRessources();
				
				//Retour de MAJ
				if (retour['status'] == 'ok') {
					$('#formRes').fadeOut(function () {
						$('#success_div_hash').hide();
						$('#success').fadeIn();
						setTimeout(function () {
							removeOverlay();
						}, 3000);	
					});
				}
				else if (retour['status'] == 'code') {
					$('#formRes').fadeOut(function () {
						var hash = retour['message'];
						$('#success_hash').html(hash);
						$('#success_div_hash').show();
						$('#success').fadeIn();
						
					});
				}
				else
				{
					$('#hash_label').html(retour['message']);
				}
				return false;
			});
		}
	});

	$(document).on('blur', '#formRes input[name=hash_capsule]', function  () {
		var hash_capsule = $(this).val();
		
		updateFields(hash_capsule);
		return false;
	});
	
	$(document).on('blur','#formRes input[name=etablissement]', function () {
		//Recherche dans le nom long
		var selector = '#etablissement option[value="'+$(this).val()+'"]';
		//Recherche dans le nom court
		var selector2 = '#etablissement option[label="'+$(this).val()+'"]';
		
		//On check si la valeur est dans le datalist
		if($(selector).length)
			$(this).data('id', $(selector).data('id'));
		
		//On check si la valeur est dans le datalist
		else if($(selector2).length)
			$(this).data('id', $(selector2).data('id'));

		//Sinon, on met #
		else
			$(this).data('id', '#');

	});

	$(document).on('blur','#formRes input[name=theme]', function () {
		//Recherche dans le nom
		var selector = '#theme option[value="'+$(this).val()+'"]';

		//On check si la valeur est dans le datalist
		if($(selector).length)
			$(this).data('id', $(selector).data('id'));

		//Sinon, on met #
		else
			$(this).data('id', '#');

	});
 
	$(document).on('blur','#formRes input[name=ressource]', function () {
		//Recherche dans le nom
		var selector = '#ressource option[value="'+$(this).val()+'"]';

		//On check si la valeur est dans le datalist
		if($(selector).length)
			$(this).data('id', $(selector).data('id'));

		//Sinon, on met #
		else
			$(this).data('id', '#');

	});
 

	/**************************************************************************************
	********************************* Image Upload ****************************************
	**************************************************************************************/

	function resetForm () {
		$.each($('#formRes *[name]'),function () {
			$(this).val('');
		});

		$('#dropfile').html('100x100 .png').css('background-image','url()');
		$('#spanWrapper h4').html('Aucune image').css('color','');
	}
	//Drag n drop image
	$(document).on('dragenter', '#dropfile', function() {
        $(this).css('outline', '3px dashed red');
        return false;
	});
	 
	$(document).on('dragover', '#dropfile', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).css('outline', '3px dashed red');
        return false;
	});
	 
	$(document).on('dragleave', '#dropfile', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).css('outline', '3px dashed #BBBBBB');
        return false;
	});
	$(document).on('drop', '#dropfile', function(e) {
        if(e.originalEvent.dataTransfer){
                   if(e.originalEvent.dataTransfer.files.length) {
                               // Stop the propagation of the event
                               e.preventDefault();
                               e.stopPropagation();
                               $(this).css('outline', '3px dashed green');
                               // Main function to upload
                               upload(e.originalEvent.dataTransfer.files);
                   }  
        }
        else {
                   $(this).css('outline', '3px dashed #BBBBBB');
        }
        return false;
	});
	function upload(files) {
	    var f = files[0] ;

	    // Only process image files.
	    if (!f.type.match('image/png')) {
        	$('#spanWrapper h4').html("Format invalide").css('color','red');
	               return false ;
	    }

	    var image = new Image();
        image.onload = function() {
        	if((this.width == 100) && (this.height == 100))
        	{
        		var reader = new FileReader();
			    // When the image is loaded,
			    // run handleReaderLoad function
			    reader.onload = handleReaderLoad;

			    // Read in the image file as a data URL.
			    reader.readAsDataURL(f); 
        	}
		    else
		    {
               $('#dropfile').css('outline', '3px dashed red');
    			$('#spanWrapper h4').html("Dimensions invalides").css('color','red');
		    }
        };
        image.onerror = function() {
        	$('#spanWrapper h4').html("Format invalide").css('color','red');
            
        }; 
        image.src = URL.createObjectURL(f);  

	}
	function handleReaderLoad(evt) {
        var pic = {};
        pic.file = evt.target.result.split(',')[1];

        var str = jQuery.param(pic);

        $('input[name=icone_blob]').val(pic.file);
        $('#dropfile').html('').css('background-image','url('+evt.target.result+')');
        $('#spanWrapper h4').html('Image correcte').css('color','green');

	}
});
		/***********************************************************/
		/************************** Login **************************/
		/***********************************************************/

	function displayRessources () {
		var id_deposeur = isConnected();
		//Si les ressources sont déjà affichées
		if ($('#my-ressources').length) {
			//On vire tout pour actualiser
			$('#my-ressources').fadeOut();
		}
		if (id_deposeur)
		{
			var post = 'id_deposeur='+id_deposeur;

			$.post(globals.webServices + "ressources/listerRessources.php",post+globals.post_OS,function(data){

				var retour = $.parseJSON(data);
				// S'il a des ressources
				if (typeof(retour['ressources']) != 'undefined')
				{
					var ressources = retour['ressources'];
					//On va chercher le nombre de messages / On enlève le & du début de post_OS
					$.post(globals.webServices + "messages/recupererNombreMessages.php",globals.post_OS.slice(1,globals.post_OS.length),function(data){
						var messages = $.parseJSON(data);
						if (typeof(messages) != "undefined")
						{
							var postLoad = {'messages' : messages, 'ressources' : ressources};
						   	$('#mes-res-wrapper').load(globals.modules + 'mesRessources.php', postLoad, function () {
								$('#my-ressources').animate({opacity:1});
							});
						}
					});
				}
			});
		}
	}

	function removeMessages (ressource) {
		if (window.confirm("Vous allez supprimer définitivement TOUS les messages attachés à cette ressource. Il est impossible d'annuler cette action.\n \n Souhaitez-vous continuer?")) { 
			var id_deposeur = isConnected();
			if (id_deposeur)
			{

				var id_capsule = ressource.data('id');
				var hash_capsule = ressource.data('hash');

				var post = 'id_deposeur='+id_deposeur+'&id_capsule='+id_capsule+'&hash_capsule='+hash_capsule;

				displayAlert('Messages en cours de suppression...', 'info', ressource);

				$.post(globals.webServices + "messages/purgerMessages.php",post+globals.post_OS,function(data){
					//user.id = idElgg. If "" alors erreur
					//user.username = pseudo rentré
					var retour = $.parseJSON(data);
					// if ((typeof(retour['ressources']) != 'undefined') && retour['ressources'].length)
					if (retour['status'] == "ok")
					{
						displayAlert(retour['message'], 'valid', ressource);
					}
					else
					{
						if (typeof(retour['message']) == 'string')
							displayAlert(retour['message'], 'error', ressource);
						else
							displayAlert(retour['message'], 'error', ressource);
					}
				});
			}
		}
	}

	function displayLoginForm () {

		var connectProfil = $('<section class="popinWrapper" id="formContainer"></section>').load( globals.modules + 'connectProfil.php', function () {

			var wrapper = addOverlay(connectProfil);

			
			$(".popin").position({
				my: "center center",
				at: "center center",
				of: wrapper
			});

			$('.logSwitch').click(function () {
				if($("#registerBox").css('display') == "none")
					$("#loginBox").fadeOut("fast",function () {
						$("#registerBox").fadeIn("fast");
					});
				else
					$("#registerBox").fadeOut("fast",function () {
						$("#loginBox").fadeIn("fast");
					});
			})
			wrapper.fadeIn();
			connectProfil.children().scaleIn();

			//Submit Login
			$("#formLog").submit(function () {
				
				$.post( globals.webServices + "compte/loginCompte.php",$("#formLog").serialize()+globals.post_OS,function(data){
				   //user.id = idElgg. If "" alors erreur
				   //user.username = pseudo rentré
				   var retour = $.parseJSON(data);
				   if (typeof(retour['status']) == 'undefined')
				   {
				   		var user = retour;
						//On cale l'ID elgg et le nom dans le stockage local
						if(localStorage)
						{
							localStorage.id = user.id;
							localStorage.username = user.username;
							localStorage.rank = JSON.stringify(user.rank);
							//Hack de lastcache pour empêcher le cache de l'avatar.
							//Supprimer ce parametre permet d'indiquer au webService d'Elgg qu'on ne l'a pas caché avant.
							// localStorage.avatar_url = user.avatar_url.replace('lastcache','foo');
							localStorage.avatar_url = user.avatar_url;
						}
						$.cookie('elggperm', user.elggperm);

						localStorage.elggperm = user.elggperm;

						updateNavBar(true);
						updatePostOs();
						
						//Vérification des droits de l'utilisateur
						checkRights(function success() {
							// displayAlert('Authentifié avec succès.','valid');
								// bindPusher();

								removeOverlay();
								location.reload();
							}, function pending () {
								
								if($("#loginBox").length)
									$("#loginBox").fadeOut("fast",function () {
										$("#postDeposantBox").fadeIn("fast");
									});
						});
				   }
				   else
				   {
						$('#formLog input[type=password]').val("");
						$('.popin').effect("shake");
						var message;
						
						if(retour['message'] == "utilisateur_invalide")
							message = "Les identifiants sont erronés ; vérifiez à nouveau.";

						displayAlert(message, "error");
				   }
				});
				return false; // ne change pas de page
			});

			$("#formRegister").submit(function () {
				if (!$('#formRegister input[name=register-cgu]').is(':checked')){
					displayAlert("Vous devez accepter les CGU pour continuer.",'error', $('#formRegister input[name=register-cgu]'));
					return false;
				}
				$.post( globals.webServices + "compte/registerCompte.php",$("#formRegister").serialize()+globals.post_OS,function(data){
				   var retour = $.parseJSON(data);
				   if (retour['status'] == 'ko')
				   {
						$('.popin').effect("shake");

						if (retour['message'] == "empty") 
							displayAlert("Tous les champs obligatoires du formulaire n'ont pas été renseignés.",'error');

						else if (retour['message'] == "username") 
							displayAlert("Votre nom d'utilisateur est déjà utilisé.\nVeuillez en choisir un autre.",'error');

						else if (retour['message'] == "email")
							displayAlert("Votre adresse mail est déjà utilisée.\nVeuillez en saisir une autre.",'error');
						else if (retour['message'] == "registerbad")
							displayAlert("Erreur d'enregistrement : veuillez contacter le CAPE de l'Ecole des Mines de Nantes.",'error');
						else
							displayAlert(message,'error');
				   }
				   else
				   {
				   		displayAlert("Enregistrement réussi!", "valid");
				   		$('#registerBox').fadeOut(function () {
				   			$('#postRegisterBox').fadeIn();
				   		});

				   		/*	
				   		var user = $.parseJSON(dataJSON);
				   		//On cale l'ID elgg et le nom dans le stockage local
						if(localStorage)
						{
							localStorage.id = user.id;
							localStorage.username = user.username;
							localStorage.rank = JSON.stringify(user.rank);
						}

						updateNavBar(true);
						
						removeOverlay();*/
				   }
				});
				return false; // ne change pas de page
			});
		});
	}

	function checkRights (success, pending, error) {
		//Vérification des droits de l'utilisateur
		$.post(globals.webServices + "compte/verifierDepositeur.php", globals.post_OS,function(data){
			var retour = $.parseJSON(data);
			//Retour positifs
			if (retour['status'] == 'ok'){
				if(retour['message'] == "first_query"){
					typeof(pending) == "function" ? pending() : '';
				}
				else if(retour['message'] == "user_ok"){
					//Appel callback
					typeof(success) == "function" ? success() : '';

					// location.reload();
				}
			}
			else{
				if(retour['message'] == "not_logged_in"){
					displayAlert('Erreur d\'identification : veuillez vous reconnecter.','error');
					typeof(error) == "function" ? error() : '';
				}
				else if(retour['message'] == "already_query"){
					displayAlert('Votre demande d\'autorisation a déjà été envoyée, et est en cours de traitement. Veuillez patienter jusqu\'à réception de la réponse de nos services.','info');
					typeof(error) == "function" ? error() : '';
				}
				else if(retour['message'] == "server_error"){
					displayAlert('Indépendemment de vous, une erreur est survenue. Veuillez réessayer!','error');
					typeof(error) == "function" ? error() : '';
				}
			}
		});

	}
	function updatePostOs () {

		//Valeurs clés
		globals.post_OS = "&os=" + globals.os + "&version=" + globals.version;
		globals.post_OS += $.cookie('elggperm') ? '&elggperm=' + $.cookie('elggperm') : '';
	}

	function logOut () {
		//localStorage.clear();
		localStorage.id = "";
		localStorage.username = "";
		$.cookie('elggperm', '');

		updateNavBar(true);
		updatePostOs();
		location.reload();
	}
	//Retourne l'id si connecté, sinon false
	function isConnected (){
		if(localStorage)
		{
			if((typeof(localStorage.id) == "undefined") 
				|| (typeof(localStorage.username) == "undefined")
				|| (typeof(localStorage.rank) == "undefined")
				|| (localStorage.id == "")
				|| (localStorage.username == "")
				|| (localStorage.rank == "")
				|| (localStorage.elggperm != $.cookie('elggperm')))
			{
				// displayAlert('Attention, votre session a expirée. Veuillez vous reconnecter.','error');
				// logOut();
				return false;
			} 
			else return localStorage.id;
		}
	}

	function addOverlay(div_to_add,callback){

		var wrapper = $('<div id="popinFocus"></div>');	

		$('body').append(wrapper);

		wrapper.click(function () { 
			if(typeof(callback) === "function")
			{
				removeOverlay(callback);
			}
			else removeOverlay();
		});
		div_to_add.appendTo($('body'));
		// div_to_add.css({opacity:0, marginTop:"-30px"}).appendTo($('body')).animate({opacity:1, marginTop:"0"}, 200);
		// $('body').append(div_to_add);
		// $('#tplFra').css('-webkit-filter', 'blur(1px)');

		return wrapper;
	}
	function removeOverlay (callback) {
		
		// $('#tplFra').css('-webkit-filter', '');
		// $('#formWrapper').fadeOut(function(){$(this).remove()});
		// $('#pres-wrapper').fadeOut(function(){$(this).remove()});
		// $("#formContainer").fadeOut(function(){$(this).remove()});
		$("#popinFocus").fadeOut(600,function(){$(this).remove()});
		$(".popinWrapper").fadeOut(600,function(){$(this).remove()});
		
		if(typeof(callback) === "function")
			callback();
	}
	//texte : Texte à afficher
	// class_to_add : valid, info ou error
	// div_to_position : Element sur lequel placer l'erreur
	function displayAlert (texte, class_to_add, div_to_position) {
		if(typeof(globals.alert) == "object")
		{
			if(globals.alert.css("display") != "none")
			{
				globals.oldAlert = globals.alert;
				window.clearTimeout(globals.alertTimeoutId);
				globals.oldAlert.fadeOut('fast',function () {
					globals.oldAlert.remove();
				});
			}
			else globals.alert.remove();
		}
		globals.alert = $('<div>',{class:'pf-message '+ class_to_add}).append
		(
			$('<div>',{class:'message-close'}).html('x')
		).append
		(
			$('<div>',{class:'message-inner'}).append
			(
				$('<div>',{class:'message-text'}).html(texte)
			)
		).click(function(){
					$(this).fadeOut('fast',function () {
						window.clearTimeout(globals.alertTimeoutId);
						$(this).remove();
					});
				});

		globals.alert.appendTo($('body'));

		if(div_to_position)
			globals.alert.position({
				my: "left top",
				at: "right bottom",
				of: div_to_position
			});
		else
			globals.alert.css({"position":"fixed", "bottom":"10px","right":"10px"});

		globals.alert.fadeIn('fast')
		globals.alertTimeoutId = window.setTimeout(function(){
			globals.alert.fadeOut('fast',function () {
				globals.alert.remove();
			})
		},3000);
		
	}

	function updateNavBar (animated) {
		
		if(isConnected())
		{
			if(typeof(animated) != "undefined")
			{
				$('#btn-login').fadeOut();
				$('#btn-profil').fadeIn();

			}
			else
			{
				$('#btn-login').hide();
				$('#btn-profil').show();
			}

			//Hack de lastcache pour empêcher le cache de l'avatar.
			//Supprimer ce parametre permet d'indiquer au webService d'Elgg qu'on ne l'a pas caché avant.
			//Plus, placer un Random dans l'URL permet d'indiquer au browser qu'il faut recharger l'image absolument (car l'URL n'est pas la même qu'avant)
			$('img','#btn-profil').attr('src', localStorage.avatar_url);
			// $('img','#btn-profil').attr('src', localStorage.avatar_url.replace('lastcache',Math.random()));
			$('#profil-name').html(localStorage.username);
		}
		else
		{
			if(typeof(animated) != "undefined")
			{
				$('#btn-login').fadeIn();
				$('#btn-profil').fadeOut();
				$('img','#btn-profil').attr('src', '');
			}

			else
			{
				$('#btn-login').show();
				$('#btn-profil').hide();
				$('img','#btn-profil').attr('src', '');
			}	
		}

	}

	$.fn.scaleIn = function(duration, ease){
		var localDuration = typeof(duration) == 'number' ? duration : 600;
		var localEase = typeof(ease) == 'string' ? ease : 'swing';

		$(this).css({
					'-webkit-transform':'scale(1.5)',
					'-moz-transform':'scale(1.5)',
					'-o-transform':'scale(1.5)',
					'transform':'scale(1.5)', 'opacity': '0'}).animate({ opacity: 1 }, {
			    step: function(now,fx) {
			        $(this).css({
			        	'-webkit-transform':'scale('+ (1 + (1 - now)/2) +')',
			        	'-moz-transform':'scale('+ (1 + (1 - now)/2) +')',
			        	'-o-transform':'scale('+ (1 + (1 - now)/2) +')',
			        	'transform':'scale('+ (1 + (1 - now)/2) +')'
			        });
			    },
			    duration:localDuration
			}, localEase);
	}
