<?php
//Fonction d'envoi de requête POST
//$url : [String] de l'url complète à appeler
//$params : [Array] POST. Nul si aucune option
//Return : [Decoded JSON] Retour de l'appel le cas échéant
//TODO (peut-être) : adapter pour le GET

define('SERV_ROOT','http://imedia.emn.fr'); 

function sendRequest($url, $params = array()){
	
	$postdata = http_build_query($params);
	$opts = array('http' =>	array(
		'method' =>'POST',
		'header' =>'Content-type: application/x-www-form-urlencoded',
		'content' =>$postdata
	));
	
	$context = stream_context_create($opts);
	return json_decode(file_get_contents($url, false, $context),true);
}

//Fonction permettant de traduire un terme suivant la langue de l'app
//$key : la clé du terme voulu
//$langueApp : parametre POST de la langue de l'application. Anglais si elle n'xiste pas
function getLocalize($key) {
	static $translations = NULL;
  	if (is_null($translations)) {
  		$langueApp = 'en';
  		if(isset($_POST['langueApp']))
  			$langueApp = $_POST['langueApp'];
    	$lang_file = 'http://imedia.emn.fr/SCWeb/dyn/res/json/'. $langueApp .'/strings.json';
    	$lang_file_content = file_get_contents($lang_file);
    	$translations = json_decode($lang_file_content, true);
  	}

  	if($translations[$key] != null)
  		return $translations[$key];
  	else
  		return $key;
}

?>