<?php include_once("../fonctions.php"); ?>

<!-- <div class="btn-profil-search-fermer btn-cross-fermer" title="Fermer ce panneau">x</div> -->
<section id="profil-search-wrapper" class="panel-wrapper">
	<div class="btn-cross-fermer" id="btn-profil-search-fermer">x</div>
	<section id="profil-search-field">
		<section id="profil-search-actions">
			<input type="search" results="5" name="username" autosave="fr.emn.pairform" placeholder="<?php echo getLocalize('label_pseudo'); ?>">
			<div id="btn-profil-search-trigger" class="btn-action"><?php echo getLocalize('web_button_rechercher'); ?></div>
			<!-- <div id="btn-profil-search-fermer" class="btn-action">Fermer</div> -->
		</section>
	</section>
	<section id="profil-search-results">
		<h1 class="profil-search-not-found"><?php echo getLocalize('web_label_aucun_resultat'); ?></h1>
		<ul id="profil-search-list-results">
		</ul>
	</section>
	<li class="profil-search-result-user" id="profil-search-result-user-template">
		<img src="" alt="" class="profil-search-img">
		<div class="profil-search-inner">
			<h1 class="profil-search-username"><?php echo getLocalize('web_label_utilisateur'); ?></h1>
			<div class="profil-search-etablissement"><?php echo getLocalize('label_etablissement'); ?></div>
		</div>
		<div class="reseaux-add profil-search-add-user" title="<?php echo getLocalize('button_ajouter'); ?>">+</div>
	</li>
</section>