<?php 
	
	include_once("../fonctions.php");

	$liste_reseaux = isset($_POST['liste_reseaux']) ? $_POST['liste_reseaux'] : '';
	//Récupère $cercles, $classes et $classesRejointes
	foreach ($liste_reseaux as $key => $value) {
		$$key = $value;
	}

?>
<section class="panel-wrapper" id="reseaux-wrapper">
<div class="btn-cross-fermer" id="btn-reseaux-fermer">x</div>
	<section class="panel-head" id="reseaux-head">
			<h1 class='panel-title' id='reseaux-user'><?php echo getLocalize('title_cercles'); ?></h1>
		<!-- 
		<section class="panel-actions" id="reseaux-actions">
			<div id="btn-reseaux-fermer" class="btn-action">Fermer ce panneau</div>
		</section> -->
	</section>
	<section class="reseaux-container" id="cercles-container">
		<span class="sous-section-label"><?php echo getLocalize('title_mes_cercles'); ?></span>
		<span class="reseaux-add reseaux-add-item" title="<?php echo getLocalize('title_creer_cercle'); ?>">+</span>
		<section class="reseaux-add-item-form">
			<input type="text" placeholder="<?php echo getLocalize('label_nom'); ?>" name="cercle_name">
			<span class="reseaux-cancel reseaux-cancel-item" title="<?php echo getLocalize('button_annuler'); ?>">&#10008;</span>
			<span class="reseaux-valid reseaux-valid-item" data-type="cercle" title="<?php echo getLocalize('web_button_valider'); ?>">&#10004;</span>
		</section>
		<?php 
			if (count($cercles)) {
		?>
			<ul class="reseaux-liste">
			<?php
				foreach ($cercles as $index => $cercle) {
			?>
				<li class="reseaux-item" data-id_collection="<?php echo $cercle['id']; ?>">
					<span class="reseaux-item-label"><?php echo $cercle['name'];?></span>
					<div class="reseaux-valid reseaux-select-item" title="<?php echo getLocalize('web_button_selectionner');?>">&#10004;</div>
					<div class="reseaux-add reseaux-add-to-item" title="<?php echo getLocalize('web_button_ajouter_a_ce_cercle');?>">+</div>
					<div class="reseaux-cancel reseaux-delete-item" title="<?php echo getLocalize('web_button_supprimer_ce_cercle');?>">&#10008;</div>
					<ul class="reseaux-liste-user">
						<div class="reseaux-add reseaux-add-user" title="<?php echo getLocalize('web_button_ajouter_une_personne_cercle');?>">+</div>
						<?php
							if (count($cercle['members'])) {
									
								foreach ($cercle['members'] as $key => $user) {
							?>
								<li class="reseaux-user" data-id_utilisateur="<?php echo $user['id_utilisateur'];?>">
									<div class="reseaux-cancel reseaux-delete-user" title="<?php echo getLocalize('web_button_supprimer_cet_utilisateur');?>">&#10008;</div>
									<img src="<?php echo $user['image'];?>" alt=""/>
									<figcaption><?php echo $user['name'];?></figcaption>
								</li>
															
							<?php
								}
							}
						?>
					</ul>
				</li>
			<?php
				}
			?>
			</ul>
		<?php
			}
		?>
		
	</section>
	<section class="reseaux-container" id="classes-container">
		<span class="sous-section-label"><?php echo getLocalize('title_classes');?></span>
		<span class="reseaux-add reseaux-add-item" data-type="classe" title="<?php echo getLocalize('web_button_creer_classe');?>">+</span>
		<section class="reseaux-add-item-form">
			<input type="text" placeholder="<?php echo getLocalize('web_label_nom_classe');?>" name="cercle_name">
			<span class="reseaux-cancel reseaux-cancel-item" title="<?php echo getLocalize('button_annuler'); ?>">&#10008;</span>
			<span class="reseaux-valid reseaux-valid-item" data-type="classe" title="<?php echo getLocalize('web_button_valider'); ?>">&#10004;</span>
		</section>
		<?php 
			if (count($classes)) {
		?>
			<ul class="reseaux-liste">
			<?php
				foreach ($classes as $index => $classe) {
			?>
				<li class="reseaux-item" data-id_collection="<?php echo $classe['id']; ?>">
					<span class="reseaux-item-label"><?php echo $classe['name'];?></span>
					<div class="reseaux-valid reseaux-select-item" title="<?php echo getLocalize('web_button_selectionner'); ?>">&#10004;</div>
					<div class="reseaux-add reseaux-add-to-item" title="<?php echo getLocalize('web_button_ajouter_a_cette_classe'); ?>">+</div>
					<div class="reseaux-cancel reseaux-delete-item" title="<?php echo getLocalize('web_button_supprimer_cette_classe'); ?>">&#10008;</div>
					<span class="reseaux-item-cle-label">Clé : <?php echo $classe['cle'];?></span>
					<ul class="reseaux-liste-user">
						<div class="reseaux-add reseaux-add-user" title="<?php echo getLocalize('web_button_ajouter_une_personne_classe');?>">+</div>
						<?php
							if (count($classe['members'])) {
								
								foreach ($classe['members'] as $key => $user) {
							?>
								<li class="reseaux-user" data-id_utilisateur="<?php echo $user['id_utilisateur'];?>">
									<img src="<?php echo $user['image'];?>" alt=""/>
									<figcaption><?php echo $user['name'];?></figcaption>
								</li>
															
							<?php
								}
							}
						?>
					</ul>
				</li>
			<?php
				}
			?>
			</ul>
		<?php
			}
		?>
	</section>
	<section class="reseaux-container" id="classes-rejointes-container">
		<span class="sous-section-label"><?php echo getLocalize('title_classes_rejointes');?></span>
		<span class="reseaux-add reseaux-add-item reseaux-rejoindre-item" data-type="rejoindre_classe" title="<?php echo getLocalize('title_rejoindre_classe');?>">+</span>
		<section class="reseaux-add-item-form">
			<input type="text" placeholder="<?php echo getLocalize('label_clef_classe');?>" name="cercle_name">
			<span class="reseaux-cancel reseaux-cancel-item" title="<?php echo getLocalize('button_annuler');?>">&#10008;</span>
			<span class="reseaux-valid reseaux-valid-item" data-type="classeRejoindre" title="<?php echo getLocalize('web_button_valider');?>">&#10004;</span>
		</section>
		<?php 
			if (count($classesRejointes)) {
		?>
			<ul class="reseaux-liste">
			<?php
				foreach ($classesRejointes as $index => $classeRejointe) {
			?>
				<li class="reseaux-item" data-id_collection="<?php echo $classeRejointe['id']; ?>">
					<span class="reseaux-item-label"><?php echo $classeRejointe['name'];?></span>
					<div class="reseaux-valid reseaux-select-item" title="<?php echo getLocalize('web_button_selectionner');?>">&#10004;</div>
					<span class="reseaux-item-cle-label"><?php echo getLocalize('label_clef');?> : <?php echo $classeRejointe['cle'];?></span>
					<ul class="reseaux-liste-user">
						<?php
							if (count($classeRejointe['members'])) {
								
								foreach ($classeRejointe['members'] as $key => $user) {
							?>
								<li class="reseaux-user" data-id_utilisateur="<?php echo $user['id_utilisateur'];?>">
									<img src="<?php echo $user['image'];?>" alt=""/>
									<figcaption><?php echo $user['name'];?></figcaption>
								</li>
															
							<?php
								}
							}
						?>
					</ul>
				</li>
			<?php
				}
			?>
			</ul>
		<?php
			}
		?>
	</section>
</section>