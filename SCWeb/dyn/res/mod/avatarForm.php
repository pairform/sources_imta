<?php include_once("../fonctions.php"); ?>

<div class="popin formBox">
	<section id="loginBox">
		<h1><?php echo getLocalize('title_modifier_avatar'); ?></h1>
		<form action="http://imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/compte/editerAvatar.php" enctype="multipart/form-data" method="post" id="formAvatar">
			<input type="file"  name="avatar"/><br>
			<input class="btn-action" type="submit" value="<?php echo getLocalize('button_utiliser_image'); ?>" />
		</form>
	</section>
</div>