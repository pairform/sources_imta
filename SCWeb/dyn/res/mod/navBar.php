<!-- Module de barre de navigation -->
<div id="btn-menu">
	<!-- <a href="http://imedia.emn.fr/PairForm/home.php"> -->
		<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/PairformFullFinal@2x.png">
	<!-- </a> -->
	<div id="panel-menu">
		<ul>
			<li id="btn-menu-messages"><?php echo getLocalize('web_button_messages_ressource'); ?></li>
			<li id="btn-menu-ressources"><?php echo getLocalize('web_button_autres_ressources'); ?></li>
			<li id="btn-menu-recherche"><?php echo getLocalize('web_button_recherche_profil'); ?></li>
			<li id="btn-menu-succes"><?php echo getLocalize('web_button_liste_succes'); ?></li>
			<li id="btn-menu-presentation"><?php echo getLocalize('button_aide'); ?></li>
			<a href="http://www.pairform.fr/CGU_user.html" target="CGU"><li id="btn-menu-cgu"><?php echo getLocalize('button_cgu_2'); ?></li></a>
		</ul>
	</div>
</div>

<div id="btn-wrapper-left" >
	<div class="btn-bar" id="btn-bar-messages-section" title="Messages sur cette section"></div>
	<div class="btn-bar" id="btn-bar-aide" title="<?php echo getLocalize('button_aide'); ?>"></div>
	<div class="btn-bar" id="btn-bar-langue" title="<?php echo getLocalize('label_langue'); ?>">
		<img id="img-langueApp" src="">
		<div id="panel-Langue-App">
			<ul></ul>
		</div>
	</div>
</div>
<div id="label-title">
	<h3></h3>
</div>
<div id="btn-login">
	<span><?php echo getLocalize('button_connexion'); ?></span>
</div>
<div id="btn-wrapper-right">
	<div class="btn-bar btn-bar-notifications" id="btn-bar-notifications-user" data-count="0" title="Notifications">
	<ul class="arrow_box">
		<li class="notification notification-empty notification-header"><?php echo getLocalize('web_label_aucune_notification_recente'); ?></li>
	</ul>
	</div>
	<div class="btn-bar btn-bar-notifications" id="btn-bar-notifications-score" data-count="0" title="Notifications">
	<ul class="arrow_box">
		<li class="notification notification-empty notification-header"><?php echo getLocalize('web_label_aucune_notification_recente'); ?></li>
	</ul>
	</div>
	<div id="btn-profil">
		
		<img src="">
		<h5 id="profil-name"><?php echo getLocalize('button_profil'); ?></h5>
		<span id="profil-pts"></span>
		<span id="profil-pts-label"> <?php echo getLocalize('label_pts'); ?></span>
		<div id="wsIndicator" class="unsure" title="<?php echo getLocalize('web_label_maj_temps_reel'); ?>"></div>
		<div id="panel-Profil">
			<ul>
				<li id="btn-mon-profil"><?php echo getLocalize('button_profil'); ?></li>
				<li id="btn-menu-notifications"></li>
				<li id="btn-menu-noms-reels"></li>
				<li id="btn-reinitmesslus"><?php echo getLocalize('web_button_reinit_messages_lus'); ?></li>
				<li id="btn-refreshPage"><?php echo getLocalize('button_recuperer_nouveaux_messages'); ?></li>
				<li id="btn-logout"><?php echo getLocalize('button_deconnexion'); ?></li>
			</ul>
		</div>
	</div>
</div>