<?php include_once("../fonctions.php"); ?>

	<section class="panel-wrapper" id="profil-config-wrapper">
	<div class="btn-cross-fermer" id="btn-profil-config-fermer">x</div>
		<section class="panel-head" id="profil-config-head">
				<h1 class='panel-title' id='profil-config-user'><?php echo getLocalize('title_modifier_compte'); ?></h1>
		</section>
		<section class="profil-config-container formBox">
			<form method="post" id="profil-config-form">
				<h3><?php echo getLocalize('label_mot_de_passe'); ?> :</h3>
				<span><?php echo getLocalize('label_mot_de_passe_ancien'); ?> : </span><input placeholder="" type="password" value="" id="register-password-old" name="current_password" ><br>
				<span><?php echo getLocalize('label_mot_de_passe_nouveau'); ?> : </span><input placeholder="6 <?php echo getLocalize('web_label_caracteres_min'); ?>" type="password" value="" id="register-password" name="password" ><br>
				<span><?php echo getLocalize('label_mot_de_passe_confirmation'); ?> : </span><input placeholder="6 <?php echo getLocalize('web_label_caracteres_min'); ?>" type="password" value="" id="register-password2" name="password2" ><br>
				<h3><?php echo getLocalize('web_label_infos_perso'); ?> :</h3>
				<span><?php echo getLocalize('label_nom'); ?> / <?php echo getLocalize('label_prenom'); ?> : </span><input placeholder="<?php echo getLocalize('web_label_nom_maj') .' '. getLocalize('label_prenom'); ?>" type="text" id="register-name" name="name" value=""><br>
				<span><?php echo getLocalize('label_etablissement'); ?> : </span><input placeholder="<?php echo getLocalize('web_label_ecole_entreprise'); ?>" type="text" id="register-etablissement" name="etablissement" value=""><br>
				<h3><?php echo getLocalize('label_langues'); ?> :</h3>
				<span><?php echo getLocalize('label_langue_principale'); ?> : </span><select id="register-langue-principale" name="langue"></select><br>
				<span><?php echo getLocalize('label_autres_langues'); ?> : </span><span id="btn-profil-autres-langues" class="btn-action"><?php echo getLocalize('button_choisir'); ?>...</span><br>
				<span id="btn-profil-config-valider"  class="btn-action"><?php echo getLocalize('button_enregistrer'); ?></span>
			</form>
		</section>
	</section>

	<div class="popin formBox" id="popin-choix-autres-langues">
		<section id="choiceOtherLanguagesBox">
			<h1><?php echo getLocalize('title_autres_langues'); ?></h1>
			<section id="sec-other-languages">
				<ul id="list-other-languages"></ul>
			</section>
			<span id="btn-ok-choice-other-languages"  class="btn-action">OK</span>
		</section>
	</div>
	<div class="popin formBox" id="popin-change-langue-app-profil">
		<section id="changeLanguageAppBoxProfil">
			<h1><?php echo getLocalize('title_langue_application'); ?></h1>
			<p><?php echo getLocalize('label_changer_langue_app'); ?></p>
			<span id="btn-yes-change-langue-app-profil"  class="btn-action"><?php echo getLocalize('button_non'); ?></span>
			<span id="btn-no-change-langue-app-profil"  class="btn-action"><?php echo getLocalize('button_oui'); ?></span>
		</section>
	</div>

