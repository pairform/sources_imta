<?php include_once("../fonctions.php"); ?>

<div class="popin formBox" id="popin-connexion">
	<section id="loginBox">
		<h1><?php echo getLocalize('title_connexion'); ?></h1>
		<form action="" method="post" id="formLog">
			<label><?php echo getLocalize('label_pseudo'); ?> : </label><input type="text" name="username"/><br>
			<label><?php echo getLocalize('label_mot_de_passe'); ?> :</label><input type="password" name="password"/>
			<a id="passwordForgotSwitch" title="<?php echo getLocalize('web_button_mdp_oublie'); ?>"></a><br>
			<a class="logSwitch btn-action"><?php echo getLocalize('button_creer_un_compte'); ?></a>

			<input class="btn-action" type="submit" value="<?php echo getLocalize('web_button_valider'); ?>" />
		</form>
	</section>
	<section id="passwordForgotBox">
		<h1><?php echo getLocalize('title_reinitialiser_mot_de_passe'); ?></h1>
		<form action="" method="post" id="formPasswordForgot">
			<label><?php echo getLocalize('label_pseudo'); ?> / <?php echo getLocalize('label_email'); ?> : </label><input type="text" name="username"/><br>
			
			<input class="btn-action" type="submit" value="<?php echo getLocalize('web_button_valider'); ?>" />
		</form>
	</section>
	<section id="registerBox">
		<h1><?php echo getLocalize('title_nouveau_compte'); ?></h1>
		<form method="post" id="formRegister" >
			<label for="register-username"><?php echo getLocalize('label_pseudo'); ?> : </label><input placeholder="4 <?php echo getLocalize('web_label_caracteres_min'); ?>" type="text" id="register-username" name="username" ><br>
			<label for="register-password"><?php echo getLocalize('label_mot_de_passe'); ?> : </label><input placeholder="6 <?php echo getLocalize('web_label_caracteres_min'); ?>" type="password" value="" id="register-password" name="password" ><br>
			<label for="register-password2"><?php echo getLocalize('label_mot_de_passe_confirmation'); ?> : </label><input placeholder="6 <?php echo getLocalize('web_label_caracteres_min'); ?>" type="password" value="" id="register-password2" name="password2" ><br>
			<label for="register-email"><?php echo getLocalize('label_email'); ?> : </label><input placeholder="mail@example.com" type="text" id="register-email" name="email" ><br>
			<label for="register-name"><?php echo getLocalize('label_nom'); ?> / <?php echo getLocalize('label_prenom'); ?> : </label><input placeholder="<?php echo getLocalize('web_label_nom_maj') .' '. getLocalize('label_prenom'); ?>" type="text" id="register-name" name="name" ><br>
			<label for="register-langue"><?php echo getLocalize('label_langue_principale_2'); ?> : </label><select id="register-langue" name="langue"></select><br>
			<label for="register-langue"><?php echo getLocalize('web_label_autres_langues'); ?> : </label><span id="btn-register-autres-langues" class="btn-action"><?php echo getLocalize('button_choisir'); ?>...</span><br>
			<label for="register-etablissement"><?php echo getLocalize('label_etablissement'); ?> (<?php echo getLocalize('web_label_optionnel'); ?>) : </label><input placeholder="<?php echo getLocalize('web_label_ecole_entreprise'); ?>" type="text" id="register-etablissement" name="etablissement" ><br>
			<input type="checkbox" name="register-cgu" id="register-cgu"> <label class="label-small" for="register-cgu"><?php echo getLocalize('web_label_cocher_case'); ?> <a href="http://www.pairform.fr/CGU_user.html" target="_blank" style="color:#822;"><?php echo getLocalize('web_label_conditions_generales_utilisation'); ?></a></label>
			<a class="logSwitch btn-action"><?php echo getLocalize('button_connexion'); ?></a>
			<input class="btn-action" type="submit" name="submit" value="<?php echo getLocalize('button_enregistrer'); ?>">
		</form>
	</section>
	<section id="postRegisterBox">
		<h1><?php echo getLocalize('web_title_validation'); ?></h1>
		<div>
			<h3><?php echo getLocalize('label_creation_de_compte_reussi'); ?></h3>

			<span>
				<?php echo getLocalize('web_label_clique_dehors'); ?>.
			</span>
		</div>
	</section>
	<section id="postDeposantBox">
		<h1><?php echo getLocalize('web_tite_autorisation_depot'); ?></h1>
		<div>
			<h3><?php echo getLocalize('web_label_valider_vos_infos'); ?></h3>

			<span>
				<?php echo getLocalize('web_label_attendre_reponse_mail'); ?>.
			</span>
			<span>
				<?php echo getLocalize('web_label_clique_dehors'); ?>.
			</span>
		</div>
	</section>
</div>
<div class="popin formBox" id="popin-choix-autres-langues-register">
	<section id="choiceOtherLanguagesBoxRegister">
		<h1><?php echo getLocalize('title_autres_langues'); ?></h1>
		<section id="sec-other-languages">
			<ul id="list-other-languages-register"></ul>
		</section>
		<span id="btn-ok-choice-other-languages-register"  class="btn-action">OK</span>
	</section>
</div>
<div class="popin formBox" id="popin-change-langue-app-register">
	<section id="changeLanguageAppBoxRegister">
		<h1><?php echo getLocalize('title_langue_application'); ?></h1>
		<p><?php echo getLocalize('label_changer_langue_app'); ?></p>
		<span id="btn-yes-change-langue-app-register"  class="btn-action"><?php echo getLocalize('button_non'); ?></span>
		<span id="btn-no-change-langue-app-register"  class="btn-action"><?php echo getLocalize('button_oui'); ?></span>
	</section>
</div>