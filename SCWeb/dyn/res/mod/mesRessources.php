<?php

	$ressources = isset($_POST['ressources']) ? $_POST['ressources'] : false;

	$messages = isset($_POST['messages']) ? $_POST['messages'] : false;

	echo '<div id="my-ressources">';

	if ($ressources == false) {
		echo '<h1>Vous n\'avez pas encore ajouté de ressource.</h1>';
		echo '<h2>Cliquez sur le bouton en bas à droite pour ajouter votre première ressource</h2>';
	}
	else{
		foreach ($ressources as $cle => $res) {
			// error_log(print_r($res,true));
			foreach ($res as $key => $value) {
				$$key = $value;
			}

			echo '<section data-hash="'.$hash_capsule.'" data-id="'.$id_capsule.'">
					<div class="column res-icon" data-count="'.(array_key_exists($id_capsule, $messages) ? $messages[$id_capsule] : '0').'">
						<img alt="'.$nom_court.'" src="data:image/png;base64,'.$icone_blob.'">
					</div>
					<div class="column res-info-wrapper">
						<h2>'.$nom_court.'</h2>
						<div>Mis à jour le '.$date_update.'</div>
						<div>Créé par <a href="mailto:'.$deposeur_email.'">'.$deposeur_username.'</a></div>
					</div>
					<div class="column">
						<div>Visibilité de la ressource : '.($visible ? 'Public' : 'Privée').'</div>
						<div>Hash de la ressource : '.$hash_capsule.'</div>
						'.((trim($url_web) != "") ? '<div class="my-ressources-web-label">Version web</div>' : '').'
						'.((trim($url_mob) != "") ? '<div class="my-ressources-mob-label">Version mobile</div>' : '').'
					</div>
					<div class="column">
						<div class="btn-action">
							<a href="'.$url_web.'">
								Accéder à la ressource
							</a>
						</div>
						<div class="btn-action btn-modifier">
							Modifier les métadonnées
						</div>
						<div class="btn-action btn-supprimer">
							Supprimer tous les messages
						</div>
					</div>
				</section>';

		}
	}
	echo '</div>';

	// Bouton de visibilité
	// <div class="btn-action btn-visibilite">
	// 	Gérer les cercles de visibilité
	// </div>
?>