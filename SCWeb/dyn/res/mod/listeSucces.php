<?php
	include_once('../fonctions.php');
	if(isset($_POST['id_utilisateur']))
		$post = array('os' => 'web', 'version' => '1.0', 'id_utilisateur' => $_POST['id_utilisateur'], 'langue_app' => $_POST['idLangueApp']);
	else
		$post = array('os' => 'web', 'version' => '1.0', 'langue_app' => $_POST['idLangueApp']);
	
	$data = sendRequest(SERV_ROOT."/SCElgg/elgg-1.8.13/webServicesYvesse/accomplissement/listeSucces.php", $post);
	
	foreach ($data as $key => $value) {
		$$key = $value;
	}

	$output = array();

	foreach ($allSuccess as $index => $succes) {
		foreach ($succes as $key => $value) {
			$$key = $value;
		}

		array_push($output, "
						<li class='success-result'>
							<img src='http://imedia.emn.fr/SCWeb/dyn/res/img/succes/$image.png' class='success-img'>
							<div class='success-inner'>
								<h2 class='success-titre'>$nom</h2>
								<div class='success-description'>$description</div>
							</div>
						</li>");
		
	}
?>
<section id="success-wrapper" class="panel-wrapper">
	<section id="success-head">
	<div class="btn-cross-fermer" id="btn-succes-fermer">x</div>
		<?php 
		if($mode == "user")
			echo "<h1 class='success-label' id='success-user'>".getLocalize('web_label_succes_debloques')." ($userSuccessCount/$allSuccessCount)</h1>";
		else
			echo "<h1 class='success-label' id='success-global'>".getLocalize('web_label_liste_des')." $allSuccessCount ".getLocalize('web_label_succes')."</h1>";
		?>
	</section>
	<section id="success-results">
		<ul id="success-list-results">
			<?php
				for ($i=0; $i < count($output); $i++) { 
					echo $output[$i];
				}
			?>
		</ul>
	</section>
</section>