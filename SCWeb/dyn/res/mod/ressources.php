	<?php include_once("../fonctions.php"); ?>
	
	<section class="panel-wrapper" id="ressources-wrapper">
		<div class="btn-cross-fermer" id="btn-ressources-fermer">x</div>
		<section class="panel-head" id="ressources-head">
				<h1 class='panel-title'><?php echo getLocalize('web_title_ressources_pairform'); ?></h1>
			<section class="panel-actions" id="ressources-actions">				
				<div id="btn-ressources-switch" class="btn-action"><?php echo getLocalize('web_label_trier_par_etablissement'); ?></div>
				<!-- <div id="btn-ressources-fermer" class="btn-action">Fermer ce panneau</div> -->
			</section>
		</section>
		<section class="ressources-container">
			<?php
			require_once("../fonctions.php");
			//TODO:Faire le module de construction de la liste de ressources.
			// //WebServ : listeApplications.php
			// $retour = sendRequest("http://imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/ressources/listerRessources.php",array('os'=>'web','version'=>'1.5'));
			// $messages = sendRequest("http://imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/messages/recupererNombreMessages.php",array('os'=>'web','version'=>'1.5'));

			$ressources = isset($_POST['ressources']) ? $_POST['ressources'] : false;
			$messages = isset($_POST['messages']) ? $_POST['messages'] : false;
			// $ressources = $retour['ressources'];

			// error_log(print_r($ressources));
			$resParDomaine = array();
			$resParEtablissement = array();

			foreach ($ressources as $key => $res) {
				if(!isset($resParEtablissement[$res['etablissement']]))
				{
					$resParEtablissement[$res['etablissement']] = array();
				}
				array_push($resParEtablissement[$res['etablissement']],$res);

				if(!isset($resParDomaine[$res['theme']]))
				{
					$resParDomaine[$res['theme']] = array();
				}
				array_push($resParDomaine[$res['theme']],$res);
				
			}
			echo '<div id="ressources-wrapper">';



			foreach ($resParEtablissement as $key => $etablissement) {
				echo '<div class="ressources ressources-etablissement">
							<h2 class="ressources-title">'.$key.'</h2>
								<div class="ressources-items-container">';

				foreach ($etablissement as $cle => $res) {
					// echo '<div data-count="'.(array_key_exists($res['id_capsule'], $messages) ? ''.$messages[$res['id_capsule']] : '0').'" title="'.$res['description'].'&#013;&#013;Auteurs : '.$res['auteur'].'"><a href="'.$res ==$res['url_web'].'"><img class="ressources-item-icon" alt="'.$res['nom_court'].'" src="data:image/png;base64,'.$res['icone_blob'].'"><span>'.$res['nom_court'].'</span></a></div>';
					echo '<div data-count="'.(array_key_exists($res['id_capsule'], $messages) ? ''.$messages[$res['id_capsule']] : '0').'" title="'.$res['description'].'&#013;&#013;Auteurs : '.$res['auteur'].'" class="'.($res['visible'] == '1' ? '' : 'ressource-locked').'">';
					// echo '<a href="'.$res ==$res['url_web'].'">';

					if ($res['visible'] == '1') {
						echo '<a href="'.$res['url_web'].'" title="Cliquez pour accéder à la ressource">';
					}
					echo '<img class="ressources-item-icon" alt="'.$res['nom_court'].'" src="data:image/png;base64,'.$res['icone_blob'].'"><span>'.$res['nom_court'].'</span>';
					echo '<img class="icone_drap_res" src="http://imedia.emn.fr/SCWebYvesse/dyn/res/img/langue/icone_drapeau_rond_'.$res['code_langue'].'.png" alt="'.$res['code_langue'].'"/>';
					if ($res['visible'] == '1') {
						echo '</a>';
					}
					echo '</div>';
				}
				echo '</div>
				</div>';
			}
			
			foreach ($resParDomaine as $key => $domaine) {
				echo '<div class="ressources ressources-domaine">
							<h2 class="ressources-title">'.$key.'</h2>
								<div class="ressources-items-container">';
				foreach ($domaine as $cle => $res) {
					echo '<div data-count="'.(array_key_exists($res['id_capsule'], $messages) ? ''.$messages[$res['id_capsule']] : '0').'" title="'.$res['description'].'&#013;&#013;Auteurs : '.$res['auteur'].'" class="'.($res['visible'] == '1' ? '' : 'ressource-locked').'">';
					// echo '<a href="'.$res ==$res['url_web'].'">';

					if ($res['visible'] == '1') {
						echo '<a href="'.$res['url_web'].'" title="Cliquez pour accéder à la ressource">';
					}
					echo '<img class="ressources-item-icon" alt="'.$res['nom_court'].'" src="data:image/png;base64,'.$res['icone_blob'].'"><span>'.$res['nom_court'].'</span>';
					echo '<img class="icone_drap_res" src="http://imedia.emn.fr/SCWebYvesse/dyn/res/img/langue/icone_drapeau_rond_'.$res['code_langue'].'.png" alt="'.$res['code_langue'].'"/>';
					if ($res['visible'] == '1') {
						echo '</a>';
					}
					echo '</div>';
				}
				echo '</div>
				</div>';
			}
			echo '</div>';
			?>
		</section>
	</section>
