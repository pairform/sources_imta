
<?php
	try {
		$dbh = new PDO('mysql:host=localhost;dbname=SCElgg', 'root', 'So6son7', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));	
	} catch (Exception $e) {
		error_log($e->getMessage());
	}
?>
<div class="popin formBox formBoxRes">
	<h1>Enregistrement de capsule</h1>
	<form id="formRes" action="" method="post">
		<input placeholder="Identifiant à 6 caractères" type="hidden" name="hash_capsule"/>
		<span id="hash_label"></span>
		<section>
				
			<input type="hidden" name="icone_blob"/>
			<article id="dropWrapper">
				<article id="dropfile">100x100 .png</article> 
				<article id="spanWrapper">
					<h1>Déposez une image.</h1> 
					<h4>Aucun fichier</h4>
				</article>
			</article><br>
			<span>Nom long *: </span> <input placeholder="100 caractères maximum" type="text" name="nom_long"/><br>
			<span>Nom court *: </span> <input placeholder="20 caractères maximum" type="text" name="nom_court"/><br>
			<span>Description : </span> <textarea placeholder="Décrivez votre ressource" name="description"></textarea><br>
		</section>
		<section>
			<span>Url mob : </span> <input placeholder="http://www.ex.fr/capsule/mob" type="text" name="url"/><br>
			<span>Url web : </span> <input placeholder="http://www.ex.fr/capsule/web" type="text" name="url_web"/><br>
			<span>Taille (Mo) : </span> <input placeholder="Taille de votre génération mobile" type="text" name="taille"/><br><br>
			<span>Etablissement *: </span> <input placeholder="Choix dans la liste / champ libre" list="etablissement" type="text" name="etablissement"/>
			<datalist id="etablissement">
				<?php
					$query = $dbh->query("SELECT * FROM cape_etablissements");

					while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
						echo '<option value="'.$row['nom_long'].'" label="'.$row['nom_court'].'" data-id="'.$row['id_etablissement'].'"></option>';
					}
				?>
			</datalist><br>
			<span>Thème *: </span> <input placeholder="Choix dans la liste / champ libre" list="theme" type="text" name="theme"/>
			<datalist id="theme">
				<?php
					$query = $dbh->query("SELECT * FROM cape_themes");
					

					while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
						echo '<option value="'.$row['nom_long'].'" data-id="'.$row['id_theme'].'"></option>';
					}
				?>
			</datalist><br>
			<span>Ressource *: </span> <input placeholder="Choix dans la liste / champ libre" list="ressource" type="text" name="ressource"/>
			<datalist id="ressource">
				<?php
					$query = $dbh->query("SELECT * FROM cape_ressources");

					while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
						echo '<option value="'.$row['nom'].'" data-id="'.$row['id_ressource'].'"></option>';
					}
					$dbh = null;
				?>
			</datalist><br>
			<span>Licence : </span> <input placeholder="Ex: CC BY-NC-SA" type="text" name="licence"/><br>
			<span>Auteur(s) : </span> <input placeholder="Auteurs, séparés par des virgules" type="text" name="auteur"/><br><br>
			<span>Visibilité : </span> 
			<select name="visible">
				<option value="0">Ne pas afficher sur le store</option>
				<option value="1">Afficher sur le store</option>
			</select><br>
		</section>
		
		<btn class="btn-action" id="formResCancel">Annuler</btn>
		<btn class="btn-action" id="formResSubmit">Envoyer</btn>
	</form>
	<div id="success" style="display:none">
		<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/checkmark_288.png" alt="success">
		<h2>Informations mises à jour!</h2>
		<div id="success_div_hash">
			<h4>Identifiant : </h4><h1 id="success_hash">xxxxxx</h1><br>
			<span>Entrez ce code dans les méta-données de votre module Opale, dans le champ 'ID de la ressource'.</span><br>
			<span>Cela actionnera l'utilisation de PairForm sur votre ressource.</span>
			<br>
			<span>Veuillez actualiser la page pour visualiser le résultat.</span>
		</div>
				
	</div>
</div>