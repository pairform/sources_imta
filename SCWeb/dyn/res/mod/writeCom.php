<?php include_once("../fonctions.php"); ?>

<div class="ui-tooltip-top">
	<form action="" method="post" id="formComInline">
		<div id="message-options" title="<?php echo getLocalize('web_label_modififer_optinos_message'); ?>">
			<div class="message-options-cog"></div>
		</div>
		<textarea name="message"></textarea>
		<input type="hidden" name="est_defi" value="-1"/>
		<input type="hidden" name="visibilite" value=""/>
		<input type="submit"/>
	</form>
</div>