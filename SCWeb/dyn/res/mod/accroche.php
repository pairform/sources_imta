<?php include_once("../fonctions.php"); ?>

		<div class="popin">
			<div class="btn-cross-fermer" title="<?php echo getLocalize('web_label_fermer_panneau'); ?>">x</div>
			<section id="presBox">

				<h1><?php echo getLocalize('web_label_site_utilise_pairform'); ?> :</h1>
				<ul id="pres-thumbnails-list">
					<li>
						<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/accroche_etudiez.png" alt="<?php echo getLocalize('web_label_etudiez'); ?>" title="<?php echo getLocalize('web_label_etudiez'); ?>">
						<div class="pres-thumb-label"><h2><?php echo getLocalize('web_label_etudiez'); ?></h2><?php echo getLocalize('web_label_notions_contenus_ressources'); ?></div>	
					</li>
					
					<li>
						<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/accroche_echangez.png" alt="<?php echo getLocalize('web_label_partagez'); ?>" title="<?php echo getLocalize('web_label_partagez'); ?>">
						<div class="pres-thumb-label"><h2><?php echo getLocalize('web_label_partagez'); ?></h2><?php echo getLocalize('web_label_questions_idees'); ?></div>	
					</li>
					
					<li>
						<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/accroche_beneficiez.png" alt="<?php echo getLocalize('web_label_benificiez'); ?>" title="<?php echo getLocalize('web_label_benificiez'); ?>">
						<div class="pres-thumb-label"><h2><?php echo getLocalize('web_label_benificiez'); ?></h2><?php echo getLocalize('web_label_retour_auteurs_experts_participants'); ?></div>	
					</li>
					
					<li>
						<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/accroche_decouvrez.png" alt="<?php echo getLocalize('web_label_poursuivez'); ?>" title="<?php echo getLocalize('web_label_poursuivez'); ?>">
						<div class="pres-thumb-label"><h2><?php echo getLocalize('web_label_poursuivez'); ?></h2><?php echo getLocalize('web_label_avec_autres_ressources'); ?></div>	
					</li>
					
				</ul>
				<footer>
					<span><?php echo getLocalize('web_label_former_avec_vos_pairs'); ?>, <div class="btn-action" id="btn-accr-creer-compte"><?php echo getLocalize('web_button_creez_vous_un_compte'); ?></div></span>
					<br>
					<span><?php echo getLocalize('web_label_en_savoir_plus'); ?>, <div class="btn-action" id="btn-afficher-plus"><?php echo getLocalize('web_button_cliquez_ici'); ?>.</div></span>
				</footer>
			</section>
		</div> 