<?php include_once("../fonctions.php"); ?>

<main id="pres-container">
	<div class="btn-pres-fermer btn-cross-fermer" title="<?php echo getLocalize('web_label_fermer_panneau'); ?>">x</div>
	<section class="pres-header">
		<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/PairformFullFinal@2x.png" alt="<?php echo getLocalize('web_label_logo_pairform'); ?>" title="<?php echo getLocalize('web_label_logo_pairform'); ?>">
		<p>
			<?php echo getLocalize('web_label_naviguer_site_pairform'); ?> <i><?php echo getLocalize('web_label_au_coeur_ressource'); ?></i>.
		</p>
		<h1><?php echo getLocalize('web_label_change'); ?></h1>
	</section>
	<hr>
	<section class="pres-section">
		<div class="pres-content">
			<h2>1 - <?php echo getLocalize('web_label_echanger_avec_pairs'); ?></h2>
			<p>
				<?php echo getLocalize('web_label_commentaires_bulle_rouge'); ?>
				<br><br>
				<?php echo getLocalize('web_label_utilisez_le'); ?> <b><?php echo getLocalize('web_label_clic_droit'); ?></b> <?php echo getLocalize('web_label_votre_souris_visualiser'); ?>.
			</p>
		</div>
		<aside class="pres-content-illustration">
			<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/pres/lire0.png" alt="<?php echo getLocalize('web_label_clic_droit_commentaire'); ?>" title="<?php echo getLocalize('web_label_clic_droit_commentaire'); ?>">
		</aside>

		<div class="pres-content">
			<h3><?php echo getLocalize('web_label_et_voila'); ?></h3>
			<p>
				<?php echo getLocalize('web_label_vous_pouvez'); ?> : 
				<ul>
					<li><?php echo getLocalize('web_label_ecrire_nouveau_message'); ?>,</li>
					<li><?php echo getLocalize('web_label_repondre_message_existant'); ?>,</li>
					<li><?php echo getLocalize('web_label_voter_utilite_message'); ?>,</li>
					<li><?php echo getLocalize('web_label_autres_actions'); ?>...</li>
				</ul>
			</p>
		</div>
		<aside class="pres-content-illustration">
			<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/pres/lire1.png" alt="<?php echo getLocalize('web_label_messages_attaches'); ?>" title="<?php echo getLocalize('web_label_messages_attaches'); ?>">
		</aside>
	</section>
	<hr>
	<section class="pres-section">
		<div class="pres-content">
			<h2>2 - <?php echo getLocalize('web_label_prenez_notoriete'); ?></h2>
			<p>
				<?php echo getLocalize('web_label_action_reseau_social'); ?>.
				<br><br>
				<?php echo getLocalize('web_label_par_exemple'); ?> : 
				<div>
					<div class="pres-score-container"><div class="pres-score-value">+10</div><div class="pres-score-action"><?php echo getLocalize('web_label_ecrire_message'); ?></div></div>
					<div class="pres-score-container"><div class="pres-score-value">+1</div><div class="pres-score-action"><?php echo getLocalize('web_label_voter_message'); ?></div></div>
					<div class="pres-score-container"><div class="pres-score-value">+30</div><div class="pres-score-action"><?php echo getLocalize('web_label_gagner_defi'); ?></div></div>
					<div class="pres-score-container"><div class="pres-score-value pres-score-value-neg">-30</div><div class="pres-score-action"><?php echo getLocalize('web_label_se_faire_sup_msg'); ?></div></div>
				</div>
			</p>
		</div>
		<aside class="pres-content-illustration no-slide">
			<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/pres/rep0.png" alt="<?php echo getLocalize('web_label_profil_utilisateur'); ?>" title="<?php echo getLocalize('web_label_profil_utilisateur'); ?>">
		</aside>
		<div class="pres-content">
				<br>
				<?php echo getLocalize('web_label_propres_a_chaque_ressource'); ?>
				<br><br>
				<?php echo getLocalize('web_label_points_droits'); ?>
				<br><br><br><br>
				<?php echo getLocalize('web_label_par_exemple'); ?> : 
				<ul>
					<li><?php echo getLocalize('web_label_sup_msg_autres'); ?>,</li>
					<li><?php echo getLocalize('web_label_donner_medailles'); ?>,</li>
					<li><?php echo getLocalize('web_label_attribuer_tags'); ?>.</li>
				</ul>
		</div>
		<aside class="pres-content-illustration">
			<img src="http://imedia.emn.fr/SCWeb/dyn/res/img/pres/rep1.png" alt="<?php echo getLocalize('web_label_nouveaux_droits'); ?>" title="<?php echo getLocalize('web_label_nouveaux_droits'); ?>">
		</aside>
	</section>
	<hr>
	<section class="pres-section">
		<div class="pres-content-full">
			<h2>3 - <?php echo getLocalize('web_label_creez_un_compte'); ?></h2>
			<p>
				<?php echo getLocalize('web_label_participer_reseau_social_creer_compte'); ?>
			</p>
			<div class="btn-action" id="btn-pres-creer-compte"><?php echo getLocalize('button_creer_un_compte'); ?></div>
		</div>
	</section>
	<i class="pres-footer">
		<div class="btn-action btn-pres-fermer"><?php echo getLocalize('web_label_cliquez_ici'); ?></div> <?php echo getLocalize('web_label_pour_fermer_fenetre'); ?>.
		<br>
		<?php echo getLocalize('web_label_afficher_a_propos'); ?>
	</i>
</main>