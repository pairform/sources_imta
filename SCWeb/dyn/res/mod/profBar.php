<?php
	include_once("../fonctions.php");
	$id_utilisateur = isset($_POST['id_utilisateur']) ? $_POST['id_utilisateur'] : '';
	$profil = isset($_POST['profil']) ? $_POST['profil'] : '';
	$is_connected_user = isset($_POST['is_connected_user']) ? $_POST['is_connected_user'] : false;
	// $profil = sendRequest(SERV_ROOT."/SCElgg/elgg-1.8.13/webServices/profil/afficherProfil.php", array('os' => 'web', 'version' => '1.0', 'id_utilisateur' => $id_utilisateur));

	foreach ($profil as $key => $value) {
		$$key = $value;
	}

	$labelStatus = getLocalize('web_label_en_ligne');

	if ($offline != "false") {

		if ($offline < 60) {
			$offline = round($offline);
			$labelStatus = getLocalize('web_label_inactif_depuis')." $offline ".getLocalize('web_label_minutes');
		}
		elseif(($offline > 60) && ($offline < 1440)){
			$offline = round($offline / 60);
			$labelStatus = getLocalize('web_label_inactif_depuis')." $offline ".getLocalize('web_label_heures');
		}
		elseif(($offline > 1440) && ($offline < 43200)){
			$offline = round($offline / 1440);
			$labelStatus = getLocalize('web_label_inactif_depuis')." $offline ".getLocalize('web_label_jours');
		}
		elseif(($offline > 43200) && ($offline < 20000000)){
			$offline = round($offline / 43200);
			$labelStatus = getLocalize('web_label_inactif_depuis')." $offline ".getLocalize('web_label_mois');
		}
		elseif ($offline > 20000000) {
			$labelStatus = getLocalize('web_label_inactif');
		}
	}
?>
<section id="profil-wrapper" class="panel-wrapper">
	<div class="btn-cross-fermer" id="btn-profil-fermer">x</div>

	<section id="profil-info">
		<div id="profil-info-avatar">
			<img src="<?php echo $image; ?>" alt="" id="profil-img">
			<img src="http://imedia.emn.fr/SCWebYvesse/dyn/res/img/langue/icone_drapeau_rond_<?php echo $langue_principale; ?>.png" alt="" id="profil-icone-langue">
			<?php if($is_connected_user == "true"): ?>
				<div id="profil-info-avatar-label"><?php echo getLocalize('title_modifier_avatar'); ?></div>
			<?php endif; ?>
		</div>
		<div id="profil-info-inner">
			<h1 id="profil-username"><?php echo $name; ?>

				<?php if($is_connected_user == "true"): ?>
					<div id="btn-profil-config" class="options-cog" title="<?php echo getLocalize('title_modifier_compte'); ?>"></div>
				<?php endif; ?>
			</h1>
			<div id="profil-etablissement"><?php echo $etablissement; ?></div>
			<div id="profil-status" class="<?php echo ($offline != "false") ? 'offline' : "online"; ?>"><?php echo $labelStatus;?></div>
		</div>	
	</section>
	<section class="panel-actions">
		<?php 
			if ($is_connected_user == "true") {
		?>
		<div id="btn-profil-messages" class="btn-action"><?php echo getLocalize('button_mes_messages'); ?></div>
		<div id="btn-profil-mon-reseau" class="btn-action"><?php echo getLocalize('button_cercles'); ?></div>
		<?php 
			}else{
		?>
		<div id="btn-profil-messages" class="btn-action"><?php echo getLocalize('button_ses_messages'); ?></div>
		<div id="btn-profil-ajouter-reseau" class="btn-action"><?php echo getLocalize('button_ajouter'); ?></div>
		<?php
			}
		?>
		<!-- <div id="btn-profil-fermer" class="btn-action">Fermer profil</div> -->
	</section>
	<section id="profil-succes">
		<span id="profil-succes-label">
			<?php echo getLocalize('title_derniers_succes'); ?> (<?php echo $nombreSucces; ?>/74)
		</span>
		<div id="profil-succes-container">
			<?php 
				$count = $nombreSucces <= 5 ? $nombreSucces : 5;
				if ($count > 0) {
					for ($i=0; $i < $count; $i++) { 
						$suc = $succes[$i];
						echo "<img src='data:image/png;base64,".$suc['image_blob']."' alt='".$suc['description']."' title='".$suc['succes']."'>";
					}
				}
			?>
		</div>
	</section>
	<section id="profil-niveaux">
		<?php 
			if ($is_connected_user == "true") { echo "<span id=\"profil-niveaux-label\">".getLocalize('title_mes_ressources')."</span>"; }
			else { echo "<span id=\"profil-niveaux-label\">".getLocalize('title_ses_ressources')."</span>"; }
		?>
		<ul id="profil-ressource-container">
			<?php
			if (count($res) && isset($res)) {
				foreach ($res as $index => $ressource) {

					if($is_connected_user == "true")
						$ressource['visible'] = '1';

					echo '<li data-id_ressource="'.$ressource['id_ressource'].'" class="profil-ressource'.($ressource['visible'] == '1' ? '' : ' profil-ressource-locked').'">';

					if ($ressource['visible'] == '1') {
						echo '<a href="'.$ressource['url_web'].'" title="'.getLocalize('web_label_cliquez_acceder_ressource').'">';
					}
					echo '<img src="data:image/png;base64,'.$ressource['icone_blob'].'" class="profil-ressource-image">';
					if ($ressource['visible'] == '1') {
						echo '</a>';
					}
					echo '
						<div class="profil-ressource-inner">
							<div class="profil-ressource-nom">'.$ressource['nom'].'</div>
							<div class="profil-ressource-rang">'.getLocalize('label_user_'.strtolower($ressource['categorie'])).'</div>
							<div class="profil-ressource-points">'.$ressource['score'].' pts</div>
						</div>
					</li>';

				}
			}
			
			?>
		</ul>
	</section>
</section>