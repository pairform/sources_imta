<?php include_once("../fonctions.php"); ?>

<div id="comAction">
	<ul>
		<li id="writeCom" class="btn-action">
			<span><?php echo getLocalize('web_button_ecrire_message'); ?></span>
		</li>
		<li id="com-detach" class="btn-action">
			<span><?php echo getLocalize('web_button_detacher_panneau'); ?></span>
		</li>
		<li id="com-sort" class="btn-action">	
			<span><?php echo getLocalize('web_button_trier_par'); ?> <?php echo $_POST['next_sort_type'];?> </span>
		</li>
	</ul>
</div>

<div id="comContainer">
	<form style="display:none;" method="post" id="formComBar">
		<div id="message-langue" title="<?php echo getLocalize('button_changer_langue_message'); ?>">
			<img id="message-langue-img" src="" alt=""/>
		</div>
		<div id="message-options" title="<?php echo getLocalize('button_publier_message_options'); ?>">
			<div class="message-options-cog"></div>
		</div>
		<textarea name="message" data-id_blog=""></textarea>
		<input type="hidden" name="est_defi" value="-1"/>
		<input type="hidden" name="visibilite" value=""/>
		<input type="hidden" name="langue" value=""/>
		<input type="submit" value="<?php echo getLocalize('button_publier_message'); ?>"/>
	</form>
	<?php
	date_default_timezone_set('Europe/Berlin');
	// error_log($_POST);
	// error_log(print_r($_POST, true));
	// error_log(print_r($_POST,true));
	$array_messages = isset($_POST['messages_array']) ? $_POST['messages_array'] : false;
	$mode_transversal = $_POST['transversal'];

	//http://stackoverflow.com/a/10002262/1437016
	function linkify($str)
	{
		$pattern = '(?xi)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))';
		return preg_replace_callback("#$pattern#i", function($matches) {
		    $input = $matches[0];
		    $url = preg_match('!^https?://!i', $input) ? $input : "http://$input";
		    return '<a href="' . $url . '" rel="nofollow" target="_blank">' . "$input</a>";
		}, $str); 
	}
	
	if($array_messages)
	{
		// error_log(print_r($array_messages, true));
		foreach ($array_messages as $id => $content) {
			if (isset($content['tags']) && $content['tags'] != "") {
				
				if (is_string($content['tags']))
					$content['tags'] = json_decode($content['tags']);

				$tagOutput = '<div class="comTagsContainer">';
				
				foreach ($content['tags'] as $index => $tag) {
					 $tagOutput .= '<span class="comTagsLabel">'.$tag.'</span>';
				}

				$tagOutput .= '</div>';
			}
			else
				$tagOutput = '';

			if ($mode_transversal == "true") {
				//Mode automatique AUTO
				if (isset($content['nom_page'])) {
					$nom_page = $content['nom_page'] != "" ? $content['nom_page'].'.html' : "";
					// $anchor = $content['nom_page'] != "" ? $content['nom_tag'].'-'.$content['num_occurence'] : "RES";
					$anchor = $content['nom_page'] != "" ? "page" : "res";
					
				}
				//Mode identifié OA
				else if (isset($content['uid_page'])) {
					$nom_page = $content['uid_page'] != "" ? $content['uid_page'].'.html' : "";
					// $anchor = $content['uid_page'] != "" ? $content['nom_tag'].'-'.$content['num_occurence'] : "RES";
					$anchor = $content['uid_page'] != "" ? "page" : "res";
				}
				$relativeLink = $nom_page.'#'.$anchor.'-'.$content['id_message'];
				$linkToRes = '<div class="comLinkToRes">';

				$linkToRes .= '<a href="'.$relativeLink.'">'.getLocalize('web_button_context').'</a>';
				$linkToRes .= '</div>';
			}
			else
				$linkToRes = '';

			$content['contenu'] = linkify($content['contenu']);

			echo '
				<div class="comBox '
				.($content['supprime_par'] != '0' ? 'supprime' : '')
				.' rank-'.$content["id_role_auteur"].''
				.($content['id_message_parent'] != '' ? ' reponse' : '')
				.($content['est_defi'] >= 1 ? ' defi' : '')
				.'" data-id_message="'.$content["id_message"]
				.'" data-id_auteur="'.$content["id_auteur"]
				.'" data-id_role_auteur="'.$content["id_role_auteur"]
				.'" data-supprime_par="'.$content['supprime_par']
				.'" data-id_langue="'.$content['id_langue']
				.'" data-prive="'.$content['prive']
				.($content['est_defi'] >= 1 ? '" data-est_defi="'.$content['est_defi'] :'')
				.($content['defi_valide'] == "true" ? '" data-defi_valide="'.$content['defi_valide'] :'')
				.'"'.($content['id_message_parent'] != '' ? ' data-id_message_parent="'.$content['id_message_parent'].'"' : '').'>
					<div class="comProfil">
						<img class="imgProfil" src="'.$content["url_avatar_auteur"].'" title="'.getLocalize('web_label_clique_visualiser_profil').'."/>
						<div class="voteSection">				
							<div class="vote voteNo'.($content["utilisateur_a_vote"] == "-1" ? ' active' :'').'" title="'.getLocalize('web_label_commentaire_inutile').'."></div>
							<div class="voteCount">'.$content["somme_votes"].'</div>
							<div class="vote voteYes'.($content["utilisateur_a_vote"] == "1" ? ' active' :'').'" title="'.getLocalize('web_label_commentaire_utile').'."></div>
						</div>
						'.($content['medaille'] ? '<img src="//imedia.emn.fr/SCWeb/dyn/res/img/medaille_'.$content['medaille'].'.png" class="comMedaille" title="'.getLocalize('web_label_message_pertinant_collaborateur').'."/>' :'').'
						'.($content['defi_valide'] == "true" ? '<img src="//imedia.emn.fr/SCWeb/dyn/res/img/DefiValid.png" class="comDefi" title="'.getLocalize('web_label_reponse_validee_par_defieur').'."/>' :'').'
						'.($content['est_defi'] == 1 ? '<img src="//imedia.emn.fr/SCWeb/dyn/res/img/DefiActif.png" class="comDefi" title="'.getLocalize('web_label_message_defi_actif').'."/>' :'').'
						'.($content['est_defi'] == 2 ? '<img src="//imedia.emn.fr/SCWeb/dyn/res/img/DefiFini.png" class="comDefi" title="'.getLocalize('web_label_message_defi_termine').'."/>' :'').'
					</div>
					<div class="comContent" ">
						<div class="comTitle">
							<h5 class="comUserName">'.$content["pseudo_auteur"].'</h5>
							<span class="comUserRank"> - '.getLocalize('label_user_'.strtolower($content["role_auteur"])).'</span>
							<div class="timeCreated">'.date(getLocalize('web_format_date_heure'),$content["date_creation"]).'</div>
						</div>
						<div class="comText">'.$content["contenu"].'</div>
						<div class="actionSection">
						'.$linkToRes.'
						</div>
					</div>
					'.$tagOutput.'
				</div>';
		}
	}
	else{
		echo "<div class='comBar-no-element'>"
				."<div class='comBar-arrow-placeholder'>"
					."<img src='//imedia.emn.fr/SCWeb/dyn/res/img/comBar-arrow.png'/>"
				."</div>"
				."<div class='comBar-label'>"
					."<h2>".getLocalize('label_liste_messages_vide')."</h2>"
					."<div>".getLocalize('web_label_element_pas_de_contenu').".</div>"
					."<h3>".getLocalize('web_label_reagir_sur_partie')."</h3>"
				."</div>"
			."</div>";
	}

	?>
</div>
<div class="btn-cross-fermer" id="comCloser">x</div>