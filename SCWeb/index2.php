<!DOCTYPE html>
<html>
<head>
	<title>SupCast Web</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="res/css/reset.css">
	<link rel="stylesheet/less" type="text/css" href="res/css/general.css">
	<link rel="stylesheet/less" type="text/css" href="res/css/header.css">
	<script type="text/javascript" src="res/js/less.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script type="text/javascript" src="res/js/main.js"></script>

	<script src="//imedia.emn.fr//SCWeb/res/js/import.js" type="text/JavaScript"></script>
</head>
<body>
	<?php 
		require_once("fonctions.php");
		include_once("res/mod/navBar.php");
	?>
<section id="content">
	<div>
		<div id="paper-top">
			<h1>Pourquoi SupCast?</h1>
			<a title="Téléchargez l'application" href="http://itunes.apple.com/fr/app/supcast/id507452786?mt=8"><img src="res/img/appstore" alt="SupCast sur l'AppStore"></a>
		</div>
		<div id="paper-middle">
			<div id="presentation">
			<p>
				<div id="teaser">
					<img title="SupCast" src="res/img/icone_supcast.png" >
					Après l’expérience de “Parlez-vous chinois ?” sur iPhone et iPad, nous avons voulu généraliser le concept. L’objectif du projet est de pouvoir porter semi-automatiquement des ressources réalisées avec Scenari Opale sur smartphone et tablette numérique. Une application est gratuitement disponible sur l’AppStore. Elle permet de télécharger sur son iBidule une série de supports pédagogiques adaptées à la physionomie des appareils mobiles. L’utilisateur, l’étudiant qui est la personne visée en priorité, a accès à des ressources médiatisées. Il peut de plus profiter d’un réseau social intégré à chaque ressource afin d’échanger avec ses pairs en vue d’améliorer son apprentissage.
				</div>
			</p>
			<p>
				 
			</p>
			<h2>Le portage des ressources</h2>
			<p>
				“SupCast converter” analyse des ressources Scenari Opale pour identifier les médias incompatibles avec les appareils mobiles (ex : animation flash). À partir de la liste des incompatibilités, l’auteur modifie sa ressource en proposant des médias de substitution.
			    “SupCast converter” effectue certaines actions automatiquement comme le redimensionnement des images. Ensuite, “SupCast converter” produit une archive contenant à la fois la structure de la ressource Scenari Opale et les pages Web produites. Cette archive
			    compatible avec l’application mobile est déposée sur Internet, les informations attachées à la ressource sont intégrées dans une base de données unique. Toutes les ressources converties ainsi sont visibles et téléchargeables depuis l’application mobile.
			</p>
			<p>Les établissements d’enseignement supérieur intéressés par le portage de leurs ressources pédagogiques en version mobile peuvent nous contacter (voir contact ci-dessous). Nous leur fournirons le convertisseur et les accompagnerons pendant la migration
			    afin qu’ils deviennent autonomes. De par la nature de SupCast, seules les ressources libres (creative commons ou équivalent) peuvent être portées sans problème de droits d’auteur.
			</p>
			    <h2>L’application</h2>
			    <p>
			    	SupCast est une application disponible sur l’AppStore pour les iPhones et iPad. Elle permet de voir toutes les ressources mises en ligne par les établissements. L’usager peut télécharger toutes celles qu’il souhaite. Cinq ressources sont ou seront disponibles
			        très prochainement (Droit d’auteur ; Présentation en anglais scientifique et technique ; Optique pour l’ingénieur ; e-diagnostic ;  Initiation à la modélisation des systèmes automatisés). D’autres ressources seront progressivement mises en ligne
			        sachant qu’en 2013 une trentaine devrait l’être.
			    </p>
			    <p>
			    	Grâce à “SupCast Converter” les ressources Scenari Opale ont été adaptées à la taille de l’écran de l’iPhone et de l’iPad (voir images ci-dessous). L’affichage d’une ressource Scenari en version Web via le navigateur Safari n’est pas une expérience satisfaisante.
			        La mise en page, la place occupée par le sommaire prennent trop de place, surtout sur un smartphone. Les applications mobiles existantes ne font pas beaucoup mieux. L’application SupCast met le contenu de la ressource sur la totalité de l’écran pour un
			        confort de lecture.
			    </p>
			    <div class="dual-screenshots">
			    	<div>
		        
			            <img title="Droit-auteur-iphone" src="http://imedia.emn.fr/wp/wp-content/uploads/2012/09/photo-300x225.png" alt="" width="200" height="225">
			        
				        <p>Version ipad
				        </p>
				    </div>
				    <div>
			        
			            <img title="droit-auteur-iphone" src="http://imedia.emn.fr/wp/wp-content/uploads/2012/09/photo1-200x300.png" alt="" width="160" height="240">
			        
				        <p>Version iPhone
				        </p>
				    </div>
			    </div>
			    
			    <p>
			    	SupCast peut fonctionner hors ligne : une fois les ressources téléchargées, nul besoin de réseau pour les consulter. Pratique en l’absence de réseau ou pour les petits forfaits data. Le réseau est utilisé pour télécharger de nouveaux supports ou pour
			        dialoguer avec ses pairs.
			    </p>
			    <p>
			    	Réseau social : SupCast intègre des fonctionnalités de réseau social au cœur des ressources. Toutes les personnes qui ont un même support peuvent voir les messages que les autres ont accroché sur les grains pédagogiques. SupCast utilise son propre moteur
			        de réseau social car il est essentiel de connaître le contexte du message dans un acte d’apprentissage pour donner du sens. Les grands réseaux (twitter, facebook) n’offre pas cela. Aussi, SupCast a intégré un fonctionnement interne à la twitter. Le réseau
			        relie les usagers de l’application, il permet à chacun de pouvoir consulter des messages d’autres individus, d’y réagir, de rédiger ses propres messages qui seront lus par la communauté. Par ce biais, de nombreux usages sont possibles :
			    </p>
		        <ul>
		            <li>Poser des questions, obtenir/rechercher/apporter de l’aide.</li>
		            <li>Fournir une explication à un grain pédagogique qui ne semble pas clair au départ et du coup faciliter la compréhension des autres personnes.</li>
		            <li>Echanger du contenu (complément de vocabulaire ou de grammaire, exercices, informations culturelles, …). Le matériau pédagogique devient vivant et évolutif grâce à la communauté.</li>
		            <li>Rapprocher les personnes en fonction de points communs et/ou de leur proximité géographique. Diminuer l’isolement des apprenants.</li>
		            <li>Renforcer la motivation à apprendre.</li>
		            <li>Pointer des ressources externes intéressantes et complémentaires</li>
		            <li>Établir un lien entre les usagers et les auteurs par exemple pour améliorer ou enrichir le support pédagogique,</li>
		            <li>…</li>
		        </ul>
			    <p>
			    	Au fur et à mesure du développement de SupCast, de nouvelles fonctionnalités seront ajoutées. Toutes les ressources pédagogiques en bénéficieront automatiquement. C’est aussi un des intérêts d’avoir une application dédiée à cet usage.
			    </p>

			    <h2>Les prochaines versions</h2>
			    
		        <ul>
		            <li>la version 1.5 permettra d’utiliser indifféremment une ressource sur iPhone, sur iPad ou avec un navigateur Web sur ordinateur. Les messages et les fonctionnalités du réseau social seront les mêmes sur ces 3 appareils. Les usagers changent de matériel
		                selon le lieu et le temps (en mobilité ils auront par exemple un smartphone, ailleurs un ordinateur et un iPad dans leur lit). SupCast leur offrira un confort d’utilisation avec une continuité de service.</li>
		            <li>Avec cette version 1.5, une expérimentation est menée pour évaluer l’impact du réseau social sur l’apprentissage. Il s’agit avec quelques enseignants identifiés, partenaires, d’utiliser SupCast avec leurs élèves dans leur propre enseignement avec une
		                stratégie pédagogique orientée réseau social. Un chercheur analysera tous les échanges sur le réseau social pour déterminer ce que les utilisateurs en font et comment, et si ces échanges au sein de la communauté ont ou un impact sur le processus d’apprendre.</li>
		            <li>la version 2 visera en priorité le portage d’un grand nombre de ressources pédagogiques (plus de 500 h de cours). Elle permettra également de s’approprier les supports grâce à de nouvelles fonctionnalités d’annotation textuelles et graphiques comme on
		                peut le faire sur une version papier. De même, l’application offrira la possibilité de passer d’un appareil à un autre en conservant les informations saisies sur le premier, ou encore les pages qui n’ont pas encore été lues.</li>
		            <li>La version pour Android est prévue pour 2013. À ce jour, les tablettes Android ne sont pas prédominantes. C’est pourquoi la première version est consacrée à l’iPhone et à l’iPad.</li>
		        </ul>
			    
			    <h2>Partenaires</h2>
			    
			    Institut Mines Télécom (Mines Nantes), Union des Professeurs de Sciences et Techniques Industrielles, Université de Valenciennes et du Haut-Cambresis, Université du Maine. Ce projet est financé par UNIT et l’Institut Mines Télécom
				</p>
			    
			    <h2>Prix</h2>
			    
			    <p>

			        <!-- <img title="Trophées des Technologies Educatives, Educatec-Educatice" src="http://imedia.emn.fr/wp/wp-content/uploads/2012/11/trophees-244x300.jpg" alt="" width="90"> -->
			    
			    </p>
			    <p>
			    	SupCast a reçu le Grand Prix des “Trophées des Technologies Educatives” Educatec-Educatice 2012 (catégorie enseignement supérieur).
			    </p>
			        
			    <h2>Contact :</h2>  <a title="Christian Colin" href="mailto:Christian.Colin@mines-nantes.fr">Christian Colin</a>
			    
			</div>
		</div>
		<div id="paper-bottom"></div>
	</div>

	<h1 class="Ressources">Ressources disponibles sur SupCast :</h1>
	<!--<div id="switch">Classer par etablissement</div>-->
	<?php
			include_once("res/mod/shelf.php");
		?>
</section>
<footer>
</footer>
</body>
</html>