
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <link rel=stylesheet type="text/css" href="global.css">
        <link rel=stylesheet type="text/css" href="form.css">
        <link rel=stylesheet type="text/css" href="content.css">
        <style type="text/css">
        	body{
        		background-color:rgb(223, 223, 223);	
        	}

        	#body-wrapper{
        		min-height: 240px;
        		width: 550px;
        	}
        </style>
</head>
<body>
<div id="body-wrapper">
	<section id="content"> 
		<section>
			<h2>Vous devez être authentifié pour accéder à cette page. </h2>
			<form action="LoginInfo.php" method="POST" class="loginForm">
				Nom d'utilisateur : <input type="text" name="user" placeholder="Utilisateur..."/></br>
				Mot de passe : <input type="password" name="pass" placeholder="Mot de passe..."/>		
				<input type="submit" value="Login">
			</form>
		</section>
	</section>
<?php
	if(isset($_GET['log']))
		echo '<div class="warning warning-fail"><img src="croix.jpeg"/><h2>Erreur d\'authentitification.</h2></div><br/>';
?>
</div>
</body>
</html>