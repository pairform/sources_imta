<?php
	$accueil = $_POST['accueil_items'];
	$ressources = $_POST['ressources_items'];


	$filename = "informations.plist";
	 $domimpl = new DOMImplementation();
    // <!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    $dtd = $domimpl->createDocumentType('plist', '-//Apple Computer//DTD PLIST 1.0//EN', 'http://www.apple.com/DTDs/PropertyList-1.0.dtd');
    $doc = $domimpl->createDocument(null, "plist", $dtd);
    $doc->encoding = "UTF-8";

    // get documentElement and set attribs
    $plist = $doc->documentElement;
    $plist->setAttribute('version', '1.0');

    $arrayRoot = $doc->createElement('array');
    $plist->appendChild($arrayRoot);

    $arrayNodeAccueil = $doc->createElement('array');
    $arrayRoot->appendChild($arrayNodeAccueil);
    // add PropertyList's children
    for ($i=0; $i < count($accueil); $i++) { 
    	$item = $doc->createElement('string', $accueil[$i]);
    	$arrayNodeAccueil->appendChild($item);
    }

    $arrayNodeAccueil = $doc->createElement('array');
    $arrayRoot->appendChild($arrayNodeAccueil);
    // add PropertyList's children
    for ($i=0; $i < count($ressources); $i++) { 
    	$item = $doc->createElement('string', $ressources[$i]);
    	$arrayNodeAccueil->appendChild($item);
    }

	$content = $doc->saveXML();	

    $fh = fopen($filename, 'wb');
    fwrite($fh,$content);
    fclose($fh);
		
	$pagePrecédente = $_SERVER['HTTP_REFERER'];
	if(strpos($pagePrecédente,"?") !== FALSE)
	{
		$exploded = explode("?", $pagePrecédente);
		$pagePrecédente = $exploded[0];
	}

	header('Location: '.$pagePrecédente.'?enr=ok');


?>
