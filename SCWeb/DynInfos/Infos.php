<?php
	session_start();
	if(!isset($_SESSION['id']))
	{
		error_log('Redir . id = '.session_id());
		header('Location: Login.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Infos PairForm</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <link rel=stylesheet type="text/css" href="global.css">
        <link rel=stylesheet type="text/css" href="form.css">
        <link rel=stylesheet type="text/css" href="content.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript">
        function newElem(a,b,c){a=a.nodeType?a:document.createElement(a),b&&b.insertBefore(a,c||null);return a}
			//CONTROLLING EVENTS IN jQuery
			$(document).ready(function()
			{
				
				$(".ajouterItem").click(
											   
					function()
					{
						var item = newElem('section',this.parentNode);
						
						item.className = "draggable";
						item.setAttribute("draggable",true);
						item.style.display = 'none';
						if(this.parentNode.id == "section_accueil")
							item.innerHTML = '<img src="croix.jpeg" class="supprimerItem"><h3>Item </h3><textarea type="text" name="accueil_items[]" class="formDescription"></textarea></br>'
						if(this.parentNode.id == "section_ressources")
							item.innerHTML = '<img src="croix.jpeg" class="supprimerItem"><h3>Item </h3><textarea type="text" name="accueil_items[]" class="formDescription"></textarea></br>'
						$(this).appendTo(this.parentNode);
						$(item).fadeIn("medium");
						
						var rows = document.querySelectorAll('.draggable');
						[].forEach.call(rows, function(row) {
						  row.addEventListener('dragstart', handleDragStart, false);
						  row.addEventListener('dragenter', handleDragEnter, false);
						  row.addEventListener('dragover', handleDragOver, false);
						  row.addEventListener('dragleave', handleDragLeave, false);
						  row.addEventListener('drop', handleDrop, false);
						  row.addEventListener('dragend', handleDragEnd, false);
						});
					}
				);
				$('body').on('click', '.supprimerItem', function() {
						$(this.parentNode).fadeOut("slow");
						
						setTimeout(function(){$(this.parentNode).remove()}, 1000);
					}
				);

			var dragSrcEl = null;
			function handleDragStart(e) {
			  this.style.opacity = '0.4';  // this / e.target is the source node.

			  dragSrcEl = this;

			  e.dataTransfer.effectAllowed = 'move';
			  e.dataTransfer.setData('text/html', this.innerHTML);
			}

			function handleDragOver(e) {
			  if (e.preventDefault) {
			    e.preventDefault(); // Necessary. Allows us to drop.
			  }

			  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

			  return false;
			}

			//var hoveredSection = null;
			function handleDragEnter(e) {
			  // this / e.target is the current hover target.
					this.classList.add('over');
				
			  	
			  
			}

			function handleDragLeave(e) { // this / e.target is previous target element.
				
			  	this.classList.remove('over'); 
			  
			}
			function handleDrop(e) {
			  // this / e.target is current target element.

			  if (e.stopPropagation) {
			    e.stopPropagation(); // stops the browser from redirecting.
			  }

			  // See the section on the DataTransfer object.

			  // Don't do anything if dropping the same column we're dragging.
			  if (dragSrcEl != this) {
				    // Set the source column's HTML to the HTML of the column we dropped on.
				    dragSrcEl.innerHTML = this.innerHTML;
			    	this.innerHTML = e.dataTransfer.getData('text/html');
				
			  }
			  return false;
			}

			function handleDragEnd(e) {
			  // this/e.target is the source node.

			  [].forEach.call(rows, function (row) {
			    row.classList.remove('over');
			  });

			  this.style.opacity = "1";
			}

			var rows = document.querySelectorAll('.draggable');
			[].forEach.call(rows, function(row) {
			  row.addEventListener('dragstart', handleDragStart, false);
			  row.addEventListener('dragenter', handleDragEnter, false);
			  row.addEventListener('dragover', handleDragOver, false);
			  row.addEventListener('dragleave', handleDragLeave, false);
			  row.addEventListener('drop', handleDrop, false);
			  row.addEventListener('dragend', handleDragEnd, false);
			});
		});
        </script>
        <style type="text/css">
        	h3{
        		font-size: 15px;
        	}
        	.supprimerItem{
        		position: relative;
				float: right;
				right: -45px;
				top: -45px;
				display: block;
        	}
        	.draggable{
        		padding: 10px;
        		margin: 20px 0;
        		border: 1px solid lightgrey;
        		border-radius: 8px;
        	}

			.draggable .over {
    			box-shadow: 0px 0px 25px #008;
			}
        </style>
</head>
<body>
	<?php
		
		require_once("pListParser.php");

		$path = "informations.plist";
	    $plistDocument = new DOMDocument();
	    $plistDocument->load($path);

	    $plistParsed = parsePlist($plistDocument);
	    
	    /*
 		require_once(dirname(__FILE__).'/cfpropertylist/CFPropertyList.php');
 		$plistParsed  = new CFPropertyList( dirname(__FILE__).'/informations.plist' );
 		print_r($plistParsed);*/
	?>

<section id="body-dnd-trash">
<section id="body-wrapper">
	<section id="content"> 
	<h1>Infos dynamiques de PairForm</h1>
	<?php 
		if(isset($_GET['enr']))
		if($_GET['enr'] == "ok") 
			echo '<div class="warning "><img src="valider.jpeg"/><h2>Enregistrement Réussi !</h2></div><br/>';
	?>
	<section class="info">
		<h2>Info</h2></br>
			<span>Vous pouvez réorganiser la position des items : pour cela, faites glisser un item à la position souhaitée.</span>
	</section>
	<form action="MAJInfos.php" method="POST" class="contentForm">
		<section id="section_accueil">
			<h2>Ecran d'accueil :</h2>

			<?php
				for($i = 0; $i < count($plistParsed[0]); $i++)
				{
					$textarea = '<textarea type="text" name="accueil_items[]" class="formDescription">'.$plistParsed[0][$i].'</textarea>';
					$button = '<img src="croix.jpeg" class="supprimerItem">';
					echo '<section class="draggable" draggable=true>'.$button.'<h3>Item '.$i.' : </h3>'.$textarea.'</br></section>';
				}
			?>
			<img src="plus.jpeg" class="ajouterItem"/>
		</section>
		<section id="section_ressources">
			<h2>Ecran des ressources :</h2>
			<?php
				for($i = 0; $i < count($plistParsed[1]); $i++)
				{
					$textarea = '<textarea type="text" name="ressources_items[]" class="formDescription">'.$plistParsed[1][$i].'</textarea>';
					$button = '<img src="croix.jpeg" class="supprimerItem">';
					echo '<section class="draggable" draggable=true>'.$button.'<h3>Item '.$i.' : </h3>'.$textarea.'</br></section>';
				}
			?>

			<img src="plus.jpeg" class="ajouterItem"/>
		</section>
		<button ><img src="valider.jpeg"></button>
	</form>
	</section>
</section>
</section>
</body>
</html>