<?php

//error_log("**********************************************");
//error_log("RecupInfo.php appellé"); 
//error_log("**********************************************");

	//Webserv qui permet d'envoyer, s'il y a besoin, les informations dynamiques affichées dans SupCast.
	//Ce webservice est apellé par l'AppDelegate de SupCast au démarrage.

	//Recup de la date / heure en post
	//Recup des infos de informations.plist
	//Si la date/heure de mise à jour de informations.plist est supérieure à celle envoyée par l'appli
	//On envoie informations.plist
	//Sinon, on renvoie un flag mort

	//**************************************************************************************************//
	//Recup de la date / heure en post
	$timestamp = $_POST['timestamp'];

	//error_log("timestamp = $timestamp");
	//Recup des infos de informations.plist

	//Url de l'emplacement de la ressource
	$url = "informations.plist";
	$timestampModif = filemtime($url);

	//error_log("timestampModif = $timestampModif");


	//Si la date/heure de mise à jour de informations.plist est supérieure à celle envoyée par l'appli

	if($timestampModif > $timestamp)
	{
		//On envoie informations.plist
		$plist = file_get_contents($url);

		//error_log("plist = " .$plist);

		print($plist);
	}
	else
	{
		//Sinon, on renvoie un flag mort
		print(false);
	}
	
?>
