<?php
//Fonction d'envoi de requête POST
//$url : [String] de l'url complète à appeler
//$params : [Array] POST. Nul si aucune option
//Return : [Decoded JSON] Retour de l'appel le cas échéant
//TODO (peut-être) : adapter pour le GET

function sendRequest($url, $params = array()){
	
	$postdata = http_build_query($params);

	$opts = array('http' =>	array(
		'method' =>'POST',
		'header' =>'Content-type: application/x-www-form-urlencoded',
		'content' =>$postdata
	));

	
	$context = stream_context_create($opts);
	return json_decode(file_get_contents($url, false, $context),true);
}
?>