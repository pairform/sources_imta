var web_services = require("../webServices/lib/webServices"),
		 express = require("express"),
		  CONFIG = require("CONFIG"),
		  	  fs = require('fs'),
	pairform_dao = require("../webServices/node_modules/pairform_dao/lib/pairformDAO");


app = exports = module.exports = express();

/*
 * Point d'entrée de pairform web app
 * Renvoie import.js / import.min.js (permet d'alterner entre fichier minifié ou pas sans changer l'url pointé dans les ressources web)
*/

// Récupère les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
app.get("/app", function(req, res) {
	var path;
	//Si on est sur la prod
	if (CONFIG.app.urls.serveur.match(/(pairform.fr)/))
		//On renvoie la version minifiée
		path = "js/import.min.js";
	//Sinon, renvoi de la version simple
	else
		path = "js/import.js";

	//Test dév
	// path = "js/import.min.js";

	res.sendFile(path, {root : __dirname + "/private/"});
});

/*
 * Traitement des erreurs (peut être appellé par Apache pour afficher des pages d'erreurs uniques)
*/


app.get("/404", function (req, res) {		
	res.render("404");
});

app.get("/500", function (req, res) {		
	res.render("500");
});

/*
 * Toutes les vues de la web App
 * Renvoie tous les .jade / html dans views (voir layer.jade)
*/

//View contenant tous les élements composants l'interface de l'app wzeb
app.get("/app/layer", function (req, res) {		
	res.render(__dirname + "/views/layer");
});

//Page autonome de login

// app.get("/login", function (req, res) {		
// 	res.render(__dirname + "/views/login",{
// 		url_to_follow : "",
// 		// unauthorized : true
// 	});
// });

//Gestion des documents privés
//Url de forme http://imedia.emn.fr:3000/doc/1-12-980/Graphe/web/co/Accueil.html, avec tout ce qui suit id_capsule optionnel
app.get("/doc/:id_espace-:id_ressource-:id_capsule*", function (req, res) {
	
	//Récuperation des paramètres contenus dans l'URL
	var url_of_ressource = req.param(0),
		url_to_follow = req.url,
		id_capsule = req.params.id_capsule,
		id_ressource = req.params.id_ressource;

	//Si on est là, on est déjà en train d'essayer à une ressource privée
	//Si la ressource demandée n'est pas privée, on a rien à faire ici, malgré le fait que ça va marcher.

	//Si l'utilisateur est déjà connecté
	if (req.isAuthenticated()) {
		//On vérifie son accès à la capsule demandée
		web_services.capsule.getAutorisationAcces(
			id_capsule,
			req.user.email,
			function(autorisation) {
				// S'il est autorisé (au moins une autorisation trouvée dans le retour SQL) || admin pairform
				if (autorisation.length || req.user.est_admin_pairform) {
					var path_to_res = CONFIG.app.urls.url_repertoire_capsule + url_of_ressource;

					//On essaie d'append index.html derriere s'il n'y avait pas de fichier spécifié (par exemple, nom_doc/web/)
					if (!path_to_res.match(/(\/[a-zA-Z0-9\-\_\s]*\.[a-zA-Z0-9]{2,4})$/))
						path_to_res += path_to_res.substr(-1) == "/" ? "index.html" : "/index.html";
						
					//On envoie la page demandée
					if (fs.existsSync(path_to_res))
						res.sendFile(path_to_res);
					else{
						res.status(404).render("404");
					}

				} else {
					//Sinon, on l'envoie vers la page de login (en français par défaut), en le notifiant qu'il n'est pas autorisé (dernier param)
					redirigerVersLogin(id_ressource, 3, true);
				}
			}
		);
	} else {
		//Sinon, on l'envoie vers la page de login (en français par défaut)
		redirigerVersLogin(id_ressource, 3, false);
	}	

	function redirigerVersLogin (id_ressource, id_langue, unauthorized) {
		//On récupère quelques infos sur la ressource concernée
		pairform_dao.getConnexionBDD(function (_connexion_courante){
			pairform_dao.ressource_dao.selectRessourceForWeb(
				id_ressource,
				id_langue,
				function (ressource) {
					pairform_dao.libererConnexionBDD(_connexion_courante);
					//Si la ressource concernée n'existe pas
					if (!ressource) {
						//On envoie une 404
						return res.status(404).render("404");
					}
					//Sinon, on envoie la page jade login avec les infos de la ressource
					res.render(__dirname + "/views/login",{
						url_to_follow : url_to_follow,
						espace_nom_long : ressource["espace_nom_long"],
						url_logo : CONFIG.app.urls.alias_serveur + ":" + CONFIG.app.port + "/" + ressource["url_logo"],
						url_logo_espace : CONFIG.app.urls.alias_serveur + ":" + CONFIG.app.port + "/" + ressource["url_logo_espace"],
						unauthorized : unauthorized
					});
				},
				//S'il y a une erreur pendant la requete
				function () {
					//404
					return res.status(404).render("404");
				}
			);
		});
	}
});
