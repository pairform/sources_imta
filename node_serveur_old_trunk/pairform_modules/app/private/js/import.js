function PF () {};

//Global vars
PF.globals = {};
PF.globals.url = {};

// PF.globals.url.ws = "https://imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/";
PF.globals.url.root = "http://imedia.emn.fr:3000/";
PF.globals.url.ws = PF.globals.url.root+"webServices/";
// PF.globals.url.ws = "http://imedia.emn.fr/SCElgg/elgg-1.8.13/webServices/";
PF.globals.url.views = PF.globals.url.root + "app/";

PF.globals.url.private = PF.globals.url.root + "private/";
PF.globals.url.public = PF.globals.url.root + "public/";

PF.globals.os = "web";

PF.includeHead = function (url, callback)
{
    var head = window.document.getElementsByTagName('head')[0];
    var type = url.slice(url.lastIndexOf('.'));
    switch (type)
    {
        case '.js':
        var addedHead = window.document.createElement('script');
        addedHead.setAttribute('src', url);

        if(callback)
        {
            var completed = false;
            addedHead.onload = addedHead.onreadystatechange = function () {
                if (!completed && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                    completed = true;
                    callback();
                    addedHead.onload = addedHead.onreadystatechange = null;
                    head.removeChild(addedHead);
                }
            };
        }
        break;
        case '.css':
        var addedHead = window.document.createElement('link');
        addedHead.setAttribute('href', url);
        addedHead.setAttribute('type', 'text/css');
        addedHead.setAttribute('rel', 'stylesheet');
        break;
        case '.less':
        var addedHead = window.document.createElement('link');
        addedHead.setAttribute('href', url);
        addedHead.setAttribute('type', 'text/css');
        addedHead.setAttribute('rel', 'stylesheet/less');
        break;
        default:
        var addedHead = '<!--[if lte IE 8]>    <script type="text/javascript"      src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>    <sc     // Le code conditionnel qui check si Google Chrome Frame est déjà installé     // Il ouvre une iFrame pour proposer le téléchargement.     window.attachEvent("onload", function() {       CFInstall.check({         mode: "overlay" // the default       });     });    </script>        //La balise indiquant à IE d`utiliser GCF si présent    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->'
        break;

    }
    addedHead.async =true;
    head.appendChild(addedHead);
}

//Récupération du titre de la ressource
PF.getHash = function () {
   
    var hash_ressource = document.querySelector('meta[name="hash_ressource"]');
    if(hash_ressource)
        return hash_ressource.content;
    else 
        return false;
    
}

//Le site est-il construit avec Scenari
PF.isScenari = function () {
    //"<meta name='generator' content='* scenari || SCENARI *'>"
    var meta_tag_content = document.querySelector('meta[name="generator"]','head');
    if(!meta_tag_content)
        return false;

    if(meta_tag_content.content.match(/scenari/i))
        return true;
    else
        return false;
}
//Le site est-il dans un cas spécial, ou le sélécteur est défini directement dans la page
PF.isSpecialCase = function () {
    var meta_tag_content = document.querySelector('meta[name="generator"]','head');
    if(!meta_tag_content)
        return false;

    if(meta_tag_content.content == "special")
        return true;
    else
        return false;
}


//Le site est-il généré depuis LaTeX
PF.isFromLatex = function () {
    //"<meta name='generator' content='* scenari || SCENARI *'>"
    var meta_tag_content = document.querySelector('meta[name="generator"]','head');
    if(!meta_tag_content)
        return false;

    if(meta_tag_content.content == "http://www.nongnu.org/elyxer/")
        return true;
    else
        return false;
}


PF.isWebVersion = function () {
    var meta_tag_content = document.querySelector('meta[name="platform"]','head');
    if(!meta_tag_content)
        return true;
    else if(meta_tag_content.content === "web")
        return true;
    else 
        return false;
}

PF.isMobVersion = function () {
    var meta_tag_content = document.querySelector('meta[name="platform"]','head');
    if(!meta_tag_content)
        return false;
    else if(meta_tag_content.content === "mob")
        return true;
    else 
        return false;
}

PF.hasLocalStorage = function (callback_success, callback_error) {
     try {
        localStorage.setItem("_pf_test", "_pf_test");
        localStorage.removeItem("_pf_test");
        return callback_success();
    } catch(e) {
        return callback_error(e);
    }
}
//Le site est-il intégré dans SupCast?
PF.isPairForm = function (callback) {
    // if(isScenari())
    // {
        
        var hash_ressource = PF.getHash();
        if(hash_ressource)
        {
            var http = new XMLHttpRequest();
            var url = PF.globals.url.ws + 'ressource/verifier';
            var post = 'hash_ressource=' + hash_ressource;
            http.open("POST", url, true);

            //Send the proper header information along with the request
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            http.onreadystatechange = function() {//Call a function when the state changes.
                if(http.readyState == 4 && http.status == 200) {
                    var dataJSON = JSON.parse(http.responseText);
                    if(dataJSON.id_capsule)
                    {    
                        if(PF.isWebVersion())
                        {
                            PF.globals.capsule = {
                                'id_ressource' : dataJSON.id_ressource,
                                'id_capsule' : dataJSON.id_capsule,
                                'id_langue_capsule' : dataJSON.id_langue,
                                'nom_long' : dataJSON.nom_long,
                                'url_logo' : dataJSON.url_logo,
                            }
                            callback();
                        }
                        else if(PF.isMobVersion())
                        {
                            console.log("Enregistrée.");
                            callback();
                        }
                    }
                    else
                    {
                        //TODO: Gérer l'importation d'un formulaire de validation et d'upload de la ressource.
                        if(PF.isWebVersion())
                        {   
                            alert("Cette ressource n'est pas encore enregistrée sur les serveurs PairForm.");
                            console.log("Pas enregistrée.");
                        }
                        else if(PF.isMobVersion())
                        {
                            alert("Cette ressource n'est pas encore enregistrée sur les serveurs PairForm.");
                            console.log("Pas enregistrée.");
                        }
                    }
                }
            }
            http.send(post);
        }
    // }
}
PF.hasLocalStorage(function callback_success () {
    PF.includeHead(PF.globals.url.private + "js/require.js", function(){
        PF.includeHead(PF.globals.url.private + "css/supcast.css");
        // PF.includeHead(PF.globals.url.private + "css/dev.css");
        PF.includeHead(PF.globals.url.private + "css/toaster.css");
        PF.includeHead(PF.globals.url.private + "css/angular-loading-bar.css");
        PF.includeHead(PF.globals.url.public + "css/outdatedbrowser.min.css");
        PF.includeHead(PF.globals.url.public + "js/outdated-browser-1.1.0/outdatedbrowser.min.js");
        require.config({
            baseUrl: PF.globals.url.private + 'js/',
            paths: {
                jquery: "jquery-1.9.0",
                jquerycookie: "jquery.cookie",
                jqueryuiposition: "jquery.ui.position",
                // angular: "angular.min",
                angular: "angular",
                angular_loading: "angular-loading-bar",
                angular_cookies: "angular-cookies",
                angular_animate: "angular-animate.min",
                angular_toaster: "angular_toaster",
                angular_route: "angular-route.min",
                angular_translate: "angular-translate.min",
                angular_translate_loader: "angular-translate-loader-static-files",
                angular_ui_router:"angular-ui-router.min",
                angular_file_upload:'angular-file-upload.min',
                ngstorage:"ngStorage.min",
                J42R: "J42R",
                languageManager: "languageManager",
                main_controller: "main_controller",
                main_model: "main_model",
                old_main: "old_main",
                main: "main"
            },
            shim: {
                'jquerycookie' : ['jquery'],
                'angular_loading' : ['angular'],
                'angular_cookies' : ['angular'],
                'angular_animate' : ['angular'],
                'angular_toaster' : ['angular_animate'],
                'angular_route' : ['angular'],
                'angular_translate' : ['angular'],
                'angular_translate_loader' : ['angular_translate'],
                'angular_ui_router' : ['angular'],
                'angular_file_upload' : ['angular'],
                'ngstorage' : ['angular'],
                'old_main' : ["jqueryuiposition","jquerycookie", "J42R","languageManager"],
                'main_model' : ["angular",
                                "angular_cookies",
                                "angular_toaster",
                                "angular_translate",
                                "angular_translate_loader",
                                 "ngstorage"],
                'main_controller' : ["main_model",
                                     "jquery",
                                     "angular_cookies",
                                     "angular_loading",
                                     "angular_toaster",
                                     "angular_route",
                                     "angular_translate",
                                     "angular_translate_loader",
                                     "angular_file_upload",
                                      "ngstorage"]
            }
        });
        PF.isPairForm(function () {
            if(PF.isWebVersion())
            {
                // require(["jquery","angular"],function () {
                //     require(["jquerycookie", "angular_translate", "angular_translate_loader"], function () {
                //          require([ "main_controller","jqueryui", "jqueryuiposition"]);
                //     });
                // });
                require(['main_controller']);
            }
        });
    });
}, function callback_error (e) {
    var warn = document.createElement("div");
    warn.style.cssText = "position:fixed; z-index: 10000; top:0px; right: 0px; background:rgba(255, 126, 3, 0.61); box-shadow: 0 -10px 10px rgba(237, 3, 3, 0.16) inset; color:rgb(237, 237, 237); font-weight: 800; padding: 8px; border-bottom-left-radius: 20px";
    warn.id = "pf-incompatible-warning";
    warn.innerHTML = 'Votre navigateur n\'est pas compatible avec PairForm.';
    var warn_link = document.createElement("a");
    warn_link.href = "http://www.pairform.fr/compatibilite.html";
    warn_link.innerHTML = "Cliquez ici pour plus d\'information";
    warn.insertBefore(warn_link);
    document.querySelector('body').insertBefore(warn);
    // alert("Les paramètres de votre navigateur ne permettent pas d'utliser PairForm. Cliquez ici pour en savoir plus.");
});