$(function () {
  window.setTimeout(function(){
    $('html').removeClass("animated");
  }, 700);

  toastr.options.preventDuplicates = true;

  if (PF.unauthorized){
    toastr["error"]("Vous n'êtes pas autorisé à consulter ce document avec vos identifiants actuels.");
    $.post("/webServices/utilisateur/logout", function(data){
      console.log(data);
    });
  }
});
var pf_login = angular.module('pf_login', []);

var request_pending = false,
    number_of_attempts = 0;


pf_login.controller('controller_connexion', ["$scope", function ($scope) {
    var defaults = {
      "visible" : false,
      "mode" : "connexion",
      "mot_de_passe_oublie" : {}
    };

    angular.extend($scope, defaults);

    $scope.connecter = function (){
      if (request_pending) {
        toastr["warning"]("Veuillez patienter, l'enregistrement est en cours...");
        return false;
      }
      request_pending = true;
      number_of_attempts++;

      toastr["info"]("Connexion en cours...");
      // var postData = $(this).serialize()+"&os=web&version=2";
      angular.extend($scope.connexion, {"os" : "web", "version" : "2"});
      $.post("/webServices/utilisateur/login", $scope.connexion, function(data){
        request_pending = false;
        var retour = data;
        if (retour['status'] == 'ko')
        {
          if (retour['message'] == "empty")
            toastr["error"]("Tous les champs obligatoires du formulaire n'ont pas &eacute;t&eacute; renseign&eacute;s.");
          else{
            toastr["error"]("Identifiants invalides : veuillez réessayer.");
            if (number_of_attempts >= 2) {
              window.setTimeout(function(){
                var old_timeout = toastr.options.timeOut;
                toastr.options.timeOut = 0;
                // toastr.options.positionClass = "toast-top-left";
                toastr["warning"]("Mot de passe oublié ? <div class='btn-action' onclick='afficherResetPass()'>Cliquez ici</div>");
                toastr.options.timeOut = old_timeout;
                // toastr.options.positionClass = "toast-top-right";
              }, 7000);
            }

          }
        }
        else
        {              
          toastr["success"]("Connexion r&eacute;ussie, redirection vers la page en cours...");
          //On stocke dans le localstorage l'utilisateur
          //Avec le flag connecté
          var user = new Utilisateur(retour);

          user.est_connecte = true;

          localStorage["ngStorage-utilisateur_local"] = JSON.stringify(user);
          $('html').addClass('animated');
          $('body').addClass('fade-out');

          window.setTimeout(function(){
            // window.location.replace(PF.url_to_follow);
            window.location.reload();
          }, 1000);
        }
      });
      return false; // ne change pas de page
    }

    $scope.renvoyerMotDePasse = function (){
      
      var _scope = $scope;
      angular.extend(this.mot_de_passe_oublie, {"os" : "web", "version" : "2"});
      $.post('/webServices/utilisateur/reset', this.mot_de_passe_oublie, function callback_success (data) {
        var retour = data;
        if (retour['status'] == 'ok') {
          toastr.success('Un e-mail de réinitialisation a été envoyé à l\'adresse indiquée.');
        }
        else {
          toastr.error(retour['message']);
        }
      });
    }
    $scope.enregistrer = function (){
      //Attention : register_cgu à la place de register-cgu
      var _scope = $scope;
      angular.extend(this.enregistrement, {"os" : "web", "version" : "2", "langue" : 3});
      $.ajax({
        url : '/webServices/utilisateur/enregistrer',
        type : "PUT",
        data : this.enregistrement,
        success : function callback_success (data) {
          var retour = data;
          if (retour['status'] == 'ok') {
            toastr.success('Votre compte a été créé avec succès!');
            _scope.$apply(function () {
              _scope.mode = "enregistrement-1";
            });
          }
          else {
            switch (retour['message']){
              case "label_erreur_saisie_champ_vide" :
                toastr.error("Tous les champs obligatoires doivent être remplis.");
                break;
              case "label_erreur_saisie_mot_de_passe_confirmation" :
                toastr.error("Le mot de passe de confirmation doit être identique au mot de passe.");
                break;
              case "web_label_erreur_mail_utilise" :
                toastr.error("Votre adresse mail est déjà utilisée.");
                break;
              case "web_label_erreur_nom_utilise" :
                toastr.error("Votre nom d'utilisateur est déjà utilisé.");
                break;
              default:
                toastr.error(retour['message']);
                break;
            }
          }
        }
      });
    }

  }]);

function afficherResetPass () {
  var _scope = angular.element($("[ng-controller=controller_connexion]")).scope();
  _scope.$apply(function () {
    _scope.mode = 'mot_de_passe';
  })
}

function Utilisateur (options) {

  this.id_utilisateur = 0;
  this.username = "";
  this.name = "";
  this.avatar_url = "";
  this.etablissement = "";
  this.email = "";
  this.rank = {};
  
  this.estExpert = false;
  this.elggperm = "";
  this.langue_principale = "fr";
  this.autres_langues = ["1","2"];
  this.categorie = {};
  this.est_admin = false;

  this.est_connecte = false;
  this.preferences = {
    "expanded" : false,
    "ordreTri" : 0,
  };

  angular.extend(this,options);
  this.rank_ressources = {};

  var _this = this;
  $.each(this.rank, function (id_capsule, categorie) {
    if(!_this.rank_ressources[categorie.id_ressource])
      _this.rank_ressources[categorie.id_ressource] = categorie;
  });
  

  //Au cas où il n'y a pas le bon champ renvoyé par le webServices
  if (this.username == "")
    this.username = this.name;
  else if (this.username == "")
    this.name = this.username;
  
}