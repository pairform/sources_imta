// 'use strict';

/*
 * Définition des classes
*/

var LARGEUR_PANNEAU_PX = "400px",
	LARGEUR_PANNEAU = 400,
	TEMPS_ANIMATION = 200,
	ID_RES_TEST = "51";

function Panneau (options) {
	//Au cas où il n'y a pas d'option, on met un objet vide
	options = options || {};
	this.nom_controller = "";
	this.controller_scope = undefined;
	this.view = undefined;
	this.position = "left";
	this.superpose = false;
	this.should_toggle = true;
	this.should_reload = false;

	angular.extend(this,options);

	//S'il doit se recharger, il ne peut pas se toggler
	if (this.should_reload)
		this.should_toggle = false;


	this.attacherView(options.view);
	this.attacherScope();
}

Panneau.pile = {
	"left":[],
	"right":[],
	"bottom":[],
	"top":[],
	"center":[],
	"globale":[]
}
Panneau.cacherDernierDeLaPile = function (position, callback) {
	if(Panneau.pile[position].length != 0){
		var _last_controller_scope = PF.getScopeOfController(Panneau.pile[position][Panneau.pile[position].length -1]);
		
		return _last_controller_scope.panneau.cacherPanneau(callback);
	}
	if (typeof(callback) == "function")
		return callback();
	else
		return false;
}

Panneau.enleverDernierDeLaPile = function () {
	return Panneau.pile[this.position].pop();
}

Panneau.prototype.attacherView = function  (view) {
	this.view = this.view || view;
		//Récuperation de la vue correspondante, si l'élément n'est pas passé en paramètre
	if (typeof(this.view) == "undefined"){
		var _view = PF.getController(this.nom_controller);
		if (_view)
			this.view = _view;
		else
			this.view = null;
	}
}
Panneau.prototype.attacherScope = function  (controller_scope) {
	this.controller_scope = this.controller_scope || controller_scope;
	//Récuperation du controller correspondant
	if ((typeof(this.controller_scope) == "undefined") && (this.nom_controller != "")){
		var _scope = PF.getScopeOfController(this.nom_controller);
		if (_scope){
			this.controller_scope = _scope;
			this.controller_scope.init();
			//Référence au panneau passé au controller
			this.controller_scope.panneau = this;
		}
		else
			this.controller_scope = null;
	}
}

Panneau.prototype.enleverDeLaPile = function () {
	//On enlève de la pile globale également
	Panneau.pile["globale"].splice(this.indexDansPileGlobale())
	return Panneau.pile[this.position].splice(this.indexDansPile());
}
Panneau.prototype.ajouterDansPile = function () {
	//On la met dans la pile globale également
	Panneau.pile["globale"].push(this.nom_controller)
	return Panneau.pile[this.position].push(this.nom_controller);
}
Panneau.prototype.indexDansPile = function () {
	return Panneau.pile[this.position].indexOf(this.nom_controller);
}
Panneau.prototype.indexDansPileGlobale = function () {
	return Panneau.pile["globale"].indexOf(this.nom_controller);
}
Panneau.prototype.estAffiche = function () {
	return (this.indexDansPile() >= 0);
}
Panneau.prototype.togglePanneau = function (callback) {
	var index = this.indexDansPile();
	//S'il est affiché
	//S'il ne doit pas se recharger
	if (this.estAffiche() && this.should_toggle) {
		this.cacherPanneau(callback);
	}
	//S'il doit se recharger
	else if (this.estAffiche() && this.should_reload) {
		var that = this;
		Panneau.cacherDernierDeLaPile(this.position,function () {
			that.afficherPanneau(callback);	
		});
	}
	//S'il n'est pas encore affiché
	else if(!this.estAffiche()){
		//S'il ne doit pas se superposer
		if (!this.superpose) {
			var that = this;
			Panneau.cacherDernierDeLaPile(this.position,function () {
				that.afficherPanneau(callback);	
			});
		}
		else{
			this.afficherPanneau(callback);
		}
	}
	//S'il ne doit rien faire, et updater les informations direct dans le panneau -> on fait rien
}

Panneau.prototype.afficherPanneau = function (callback) {
	//On le flag comme visible
	this.controller_scope.visible = true;
	//S'il y a déjà un panneau avec cet identifiant
	if (this.estAffiche()){
		//On le cache, et on rappelle la fonction
		this.cacherPanneau(this.afficherPanneau);
	}
	//On pousse le panneau dans la pile
	this.ajouterDansPile();

	//Nombre de panneaux encastrés, pour définir l'offset
	var index = this.indexDansPile();
	
	//S'il n'y a qu'un élément
	if (index == 0){

	}
	else if (index == 1){
		$(this.view).addClass('panel-container-'+this.position+'-second-level');
	}
	else {
		//Pas normal, on planque!
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}

	if (typeof(callback) == "function"){
		return callback();
	}
	return this;
}

Panneau.prototype.cacherPanneau = function (callback) {
	//On le flag comme visible

	//Nombre de panneaux encastrés, pour définir l'offset
	var index = this.indexDansPile();

	//On le vire de la pile
	this.enleverDeLaPile();;
	//S'il n'y a qu'un élément
	if (index == 0){
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}
	else if (index == 1){
		$(this.view).removeClass('panel-container-'+this.position+'-second-level');
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}
	else {
		//Pas normal, on planque!
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}

	if (typeof(callback) == "function"){
		return callback();
	}
	return this;
}

function PanneauModal (options) {
	// Panneau.call(this);
	var _this = new Panneau(options)
	// angular.extend(this,options);
	_this.attacherScope();
	_this.attacherView();
	_this.position = "center";

	_this.controller_scope.panneau_modal = _this;
	
	return _this;
}
// PanneauModal.prototype = new Panneau();
PanneauModal.prototype.constructor = PanneauModal;

PanneauModal.prototype.togglePanneauModal = function (callback) {
	var index = this.indexDansPile();
	//S'il est affiché
	//S'il doit être unique
	if (this.estAffiche() && this.should_toggle) {
		this.cacherPanneau(callback);
	}
	//S'il ne l'est pas encore
	else{
		//S'il ne doit pas se superposer
		if (!this.superpose) {
			Panneau.cacherDernierDeLaPile(this.position);
			this.afficherPanneau(callback);	
		}
		else{
			this.afficherPanneau(callback);
		}
	}
	
}

function Utilisateur (options) {

	this.id_utilisateur = 0;
	this.username = "";
	this.name = "";
	this.avatar_url = "";
	this.etablissement = "";
	this.email = "";
	this.rank = {};
	this.rank[PF.globals.capsule.id_capsule] = {
		"id_categorie" : 0,
		"nom_categorie": "Lecteur",
		"score": 0
	};
	this.estExpert = false;
	this.elggperm = "";
	this.langue_principale = "fr";
	this.autres_langues = ["1","2"];
	this.categorie = {};
	this.est_admin = false;

	this.est_connecte = false;
	this.preferences = {
		"expanded" : false,
		"ordreTri" : 0,
	};

	angular.extend(this,options);
	this.rank_ressources = {};

	var _this = this;
	$.each(this.rank, function (id_capsule, categorie) {
	 	if(!_this.rank_ressources[categorie.id_ressource])
	 		_this.rank_ressources[categorie.id_ressource] = categorie;
	});
	

	//Au cas où il n'y a pas le bon champ renvoyé par le webServices
	if (this.username == "")
		this.username = this.name;
	else if (this.username == "")
		this.name = this.username;
	
}

Utilisateur.prototype.getRankForRessource = function (id_ressource){
	for (var i = this.rank.length - 1; i >= 0; i--) {
	 	if(id_ressource == this.rank[i].id_ressource)
	 		return parseInt(this.rank[i].id_categorie);
	 }; 
}

function Notification (options) {
	this.id_notification = 0;
	this.id_message = 0;
	this.id_utilisateur = 0;
	this.titre = "";
	this.sous_type = "";
	this.contenu = "";
	this.date_creation = "";
	this.date_vue = "";
	this.image = "";
	this.label = "";
	this.points = "";
	this.type_points = "";

	angular.extend(this,options);
}
Notification.setUpdateTimer = function () {
	if (!PF.globals.timer_notifications){
		Notification.updateAll();
		PF.globals.timer_notifications = setInterval(Notification.updateAll, 30000);
	}
}

Notification.removeUpdateTimer = function () {
	if (PF.globals.timer_notifications)
		clearInterval(PF.globals.timer_notifications);
}

Notification.updateAll = function () {
	var param = {"id_capsule" : PF.globals.capsule.id_capsule};

	PF.get('notification', param, function (data, status, headers, config){
			   
		var retour = data;
		var scope = PF.getScopeOfController('controller_nav_bar');
		//Si ya pas de soucis
		if (retour['status'] == 'ok')
		{
			scope.utilisateur_local.rank[scope.capsule.id_capsule].score = retour['score'];
			scope.notifications.us = retour['us'];
			scope.notifications.un = retour['un'];
			scope.notifications.count_us = retour['count_us'];
			scope.notifications.count_un = retour['count_un'];
		}
		else{
			if (retour['message'] == "utilisateur_invalide") {
				scope.deconnecter("web_label_session_expiree");
			}
		}
	});
}
Notification.updateAllWithDelay = function (delay){
	delay = delay || 1000;
	setTimeout(Notification.updateAll, delay);
}
Notification.prototype.voirContexte = function () {
	//Score utilisateur
	if (this.type == "us"){
		//Si on est dans le cas d'un succes, il n'y a pas de message attaché
		if (!this.id_message){
			displayProfil();
		}
		else{
			PF.redirigerVersMessage(this.id_message);
		}
	}
	//Notification utilisateur
	else{
		switch(this.sous_type){
			// dc : défi créé 
			case "dc" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// dt : défi terminé 
			case "dt" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// dv : défi validé 
			case "dv" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// ar : ajout réseau
			case "ar" : {
				displayProfil(this.contenu);
				break;
			}
			// cr : classe rejointe
			case "cr" : {
				displayProfil(this.contenu);
				break;
			}
			// ru : réponse d'utilisateur
			case "ru" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// gm : gagné médaille
			case "gm" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
		}
	}
}

function Reseau (options) {
	this.id_collection = "0";
	this.nom = "Sans titre";
	this.profils = [];

	angular.extend(this,options);
}

function Classe (options) {
	Reseau.call(this);

	this.cle = "";

	angular.extend(this,options);
}

Classe.prototype = new Reseau();
Classe.prototype.constructor = Classe;



/*
* Utilitaire
*/
PF.langue = {
	"tableau" : {
		"1" : "en",
		"2" : "es",
		"3" : "fr"
	},
	"tableau_labels" : [
		{"id" :"1", "label": "English"},
		{"id" :"2", "label": "Español"},
		{"id" :"3", "label": "Français"}
	],
	"tableau_inverse" : {
		"en" : "1",
		"es" : "2",
		"fr" : "3"
	}
};

PF.langue.codeFromId = function (id_langue) {
	return PF.langue.tableau[id_langue] || "fr";
}

PF.langue.idFromCode = function (code) {
	return PF.langue.tableau_inverse[code] || "3";
}
PF.langue.initLangueNavigateur = function() {
	if (typeof (PF.langue.langueNavigateur = navigator.language) === 'string');
	else if (typeof (PF.langue.langueNavigateur = navigator.userLanguage) === 'string');
	else PF.langue.langueNavigateur = "en";

	if (PF.langue.langueNavigateur.length>2)
		PF.langue.langueNavigateur=PF.langue.langueNavigateur.charAt(0)+PF.langue.langueNavigateur.charAt(1);

	if (PF.langue.tableau_inverse[PF.langue.langueNavigateur] == undefined) {
		PF.langue.langueNavigateur = "en";
	}
}

PF.LanguageM = {
	langueApp: "en",
	langueNavigateur: "en",
	langueUserPrincipale: "fr",
	languesUserAutres: [],
	arrayLanguesValuesName: [],
	arrayRealLanguesValuesName: [],
	arrayLanguesValuesCode: [],
	arrayLanguesValuesId: [],

	init: function() {
		this.initLanguageArrays();
		this.getLangueNavigateur();
	},

	initLanguageArrays: function() {
		var tmp_arrays;

		$.ajaxSetup({ async: false });
		$.getJSON(globals.dynRes + "json/arrays.json", function(data) {
			tmp_arrays = data;
		});
		$.ajaxSetup({ async: true });

		this.arrayRealLanguesValuesName = tmp_arrays.langue_values_string;
		this.arrayLanguesValuesCode = tmp_arrays.langue_values_code;
		this.arrayLanguesValuesId = tmp_arrays.langue_values_id;

		return this;
	},

	setLangueValuesNameArray: function() {
		var tmp_arrays;

		$.ajaxSetup({ async: false });
		$.getJSON(globals.dynRes + "json/"+ this.langueApp +"/arrays.json", function(data) {
			tmp_arrays = data;
		});
		$.ajaxSetup({ async: true });

		this.arrayLanguesValuesName = tmp_arrays.langue_values_string;
	},

	getLangueNavigateur: function() {
		if (typeof (this.langueNavigateur = navigator.language) === 'string');
		else if (typeof (this.langueNavigateur = navigator.userLanguage) === 'string');
		else this.langueNavigateur = "en";

		if (this.langueNavigateur.length>2)
			this.langueNavigateur=this.langueNavigateur.charAt(0)+this.langueNavigateur.charAt(1);

		if ($.inArray(this.langueNavigateur, this.arrayLanguesValuesCode) == -1) {
			this.langueNavigateur = "en";
		}
	},

	stringLanguesAffichageForBDD: function(isConnected) {
		var tmpStringLangues = "";
		if(isConnected) {
			tmpStringLangues += this.idLangueWithCode(this.langueUserPrincipale).toString();

			for(var i=0; i<this.languesUserAutres.length; i++) {
				tmpStringLangues += "," + this.idLangueWithCode(this.languesUserAutres[i]);
			}

			return tmpStringLangues;
		} else {
			tmpStringLangues += this.arrayLanguesValuesId[0].toString();
			
			for(var i=1; i<this.arrayLanguesValuesId.length; i++) {
				tmpStringLangues += "," + this.arrayLanguesValuesId[i];
			}

			return tmpStringLangues;
		}
	},

	arrayFromBDDOtherLanguages: function(array) {
		var tmpArray = new Array();
		
		for(var i=0; i<array.length; i++) {
			tmpArray.push(array[i]["code_langue"]);
		}

		return tmpArray;
	},

	arrayLanguageIdWithArrayLanguageCode: function(arrayLanguageCode) {
		for(var i=0; i<arrayLanguageCode.length; i++) {
			arrayLanguageCode[i] = this.idLangueWithCode(arrayLanguageCode[i]);
		}

		return arrayLanguageCode;
	},

	idLangueWithCode: function(codeLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesCode.indexOf(codeLangue);
		return this.arrayLanguesValuesId[tmpArrayIndex];
	},

	idLangueWithName: function(nomLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesName.indexOf(nomLangue);
		return this.arrayLanguesValuesId[tmpArrayIndex];
	},

	nameLangueWithCode: function(codeLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesCode.indexOf(codeLangue);
		return this.arrayLanguesValuesName[tmpArrayIndex];
	},

	nameRealLangueWithCode: function(codeLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesCode.indexOf(codeLangue);
		return this.arrayRealLanguesValuesName[tmpArrayIndex];
	},

	codeLangueWithId: function(idLangue) {
		return this.arrayLanguesValuesCode[this.arrayIndexWithId(idLangue)];
	},

	codeLangueWithName: function(nomLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesName.indexOf(nomLangue);
		return this.arrayLanguesValuesCode[tmpArrayIndex];
	},

	arrayIndexWithId: function(idLangue) {
		return this.arrayLanguesValuesId.indexOf(idLangue);
	}
};
PF.matches = function(el, selector) {
  el = el[0] || el;
  return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
}
PF.getElement = function (query, parent){
	if (parent)
		query = parent + " " + query;
	return angular.element(document.querySelectorAll(query));
}
PF.init = function (){

	//S'il y a des OA definis sur la page, et qu'on a un id de page
	if ((($('[data-oauid]:not([data-oauid=""])').length != 0) || ($('meta[name="uid_page"]').not('[content=""]').length != 0))
		|| !(PF.isScenari() || PF.isSpecialCase() || PF.isFromLatex()))
	{
		//On utilisera la version de base d'attachement de messages
		PF.globals.version = "1.5";
		PF.globals.styleAttacheMessages = "OA";
		PF.globals.uid_page = $('meta[name="uid_page"]').attr('content');
	}
	//S'il n'y a pas d'OA definis sur la page
	else
	{
		//On utilisera la version avance d'attachement des messages par identifiants uniques
		PF.globals.version =  "2";
		PF.globals.styleAttacheMessages = "AUTO";
		var nom_page = window.location.pathname.split("/")[window.location.pathname.split("/").length - 1];
		//S'il n'y a pas de nom de page (cas d'un chemin finissant par "/"), on met "index"
		PF.globals.nom_page = nom_page.split(".")[0] || "index";
	}


	if (PF.globals.id_res == ID_RES_TEST)
		PF.displayAlert("Attention, vous êtes dans un environnement de test ; les messages ont une durée d'expiration d'une heure.","info");

	//Selecteur d'éléments
	if(PF.isScenari())
	{
		PF.globals.selector = ".mainContent_co p, .resInFlow";
		PF.globals.selectorRes = "#label-title";
		PF.globals.selectorPage = ".mainContent_ti";
		PF.globals.selectorContent = ".mainContent_co";
	}
	else if(PF.isFromLatex()){
		PF.globals.selector = "#globalWrapper span";
		PF.globals.selectorRes = "#label-title";
		PF.globals.selectorPage = "#globalWrapper";
		PF.globals.selectorContent = "body";
	}
	else if(PF.isSpecialCase()){
		//On ne fait rien, les selecteurs sont déclarés dans la page
	}
	//Sinon, c'est qu'on est sur profeci
	else
	{
		PF.globals.selector = "*[data-oauid]:not(body)";
		// PF.globals.selector = ".feedback, .inner, div.object-properties, a.diagram_link, table.display, table.html-grid";
		PF.globals.selectorRes = "#label-title";
		PF.globals.selectorPage = ".inner > h2";
		PF.globals.selectorContent = "body";
	}
	PF.globals.selectorAll = PF.globals.selector + ", " + PF.globals.selectorPage + ", " + PF.globals.selectorRes;

	$(PF.globals.selectorAll).attr('pf-commentable', '');

	// $(document).on('click', PF.globals.selectorAll, function handlePFClick(event) {
	// 	// PF.addFocusedItem($(this));
	// 	Message.afficher($(this)); 
	// 	event.stopPropagation();
	// });
	// localStorage.array_messages_lus = "[]";

	//Pas d'analytics en dev
	if (PF.globals.url.root.match(/imedia.emn.fr/))
	{
		ga = function () {};
	}
	else{
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-40687872-2', 'pairform.fr');
		ga('send', 'pageview');
	}
}

PF.normaliserURL = function (url) {
	return url.split('/')[url.split('/').length -1].split('.')[0];
}
PF.displayAlert = function () {}

PF.toObject = function (array) {
	var rv = {};
	for (var i = 0; i < array.length; ++i)
	{
		var id_post = array[i].id_message;
		rv[id_post] = array[i];
	}	  	
	return rv;
}
PF.pageIsMenu = function (url) {
	if(typeof(url) == "undefined")
		//Defaut : page courante	
		var url = window.location.pathname.split('/')[window.location.pathname.split('/').length -1 ];
	
	//On test s'il est de classe ueDiv.
	var result = $('.mnu li div[id="'+ url +'"].mnu_b');
	
	return result.length == 0 ? false : true;
}

PF.getController = function (nom_controller){
	var controller = document.querySelector('[ng-controller="'+ nom_controller +'"]');
	return typeof(controller) != "undefined" ? angular.element(controller) : null;
}

PF.getScopeOfController = function (nom_controller){
	var controller = PF.getController(nom_controller);
	return controller ? controller.scope() : null;
}
PF.getMainScope = function (){
	var controller = PF.getController('main_controller');
	return controller ? controller.scope() : null;
}
PF.redirigerVersMessage = function(id_message){
	window.location.replace(PF.globals.url.ws + "message/rediriger?id_message=" + id_message);
}
PF.http = function (argument) {
	if (typeof(PF._http) == "undefined"){
		PF._http = angular.element(document).injector().get('$http');	
	}
	return PF._http;
} 

PF.get = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('get', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.post = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('post', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.put = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('put', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.delete = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('delete', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.request_with_file = function (methode, nom_webservice, file, callback_succes, callback_echec) {
	var xhr = PF.http(),
		url_ws = PF.globals.url.ws + nom_webservice,
		http_config = {
	        method: methode,
	        url: url_ws,
	        contentType: false,
			processData: false
	    };

	var formData = new FormData();
	formData.append("file", file); 
	formData.append("os", PF.globals.os);
	formData.append("version", PF.globals.version);


	if ((methode == "post") || (methode == "put"))
		http_config.data = formData;
	else
		http_config.params = formData;


	var request = xhr(http_config);

	if (typeof(callback_succes) == "function")
    	request.success(callback_succes);
	if (typeof(callback_echec) == "function")
    	request.error(callback_echec);
}

PF.request = function (methode, nom_webservice, data_requete, callback_succes, callback_echec) {
	var xhr = PF.http(),
		url_ws = PF.globals.url.ws + nom_webservice,
		data_requete_plus = {"os" : PF.globals.os, "version" : PF.globals.version},
		http_config = {
	        method: methode,
	        url: url_ws
	    };

	// var elggperm = localStorage['ngStorage-session'] ? JSON.parse(localStorage['ngStorage-session']).elggperm : ""; 
	// if(elggperm)
	// 	data_requete_plus.elggperm = elggperm;

    if (typeof(data_requete) == "object")
    	angular.extend(data_requete_plus, data_requete);

	if ((methode == "post") || (methode == "put"))
		http_config.data = data_requete_plus;
	else
		http_config.params = data_requete_plus;


	var request = xhr(http_config);

	if (typeof(callback_succes) == "function")
    	request.success(callback_succes);
	if (typeof(callback_echec) == "function")
    	request.error(callback_echec);
} 

function Message (options) {
    this.contenu = "";
    this.date_creation = Math.round(Date.now() / 1000);
    this.date_modification = Math.round(Date.now() / 1000);
    this.defi_valide = "0";
    this.est_defi = "0";
    this.est_lu = "0";
    this.geo_lattitude = "";
    this.geo_longitude = "";
    this.id_auteur = "0";
    this.id_capsule = "0";
    this.id_langue = "3";
    this.id_message = "0";
    this.id_message_parent = "";
    this.id_role_auteur = "1";
    this.medaille = "";
    this.nom_auteur = "Anonyme";
    this.nom_page = "";
    this.nom_tag = "";
    this.noms_auteurs_tags = {};
    this.num_occurence = "0";
    this.prive = "0";
    this.pseudo_auteur = "Anonyme";
    this.role_auteur = "Lecteur";
    this.somme_votes = "0";
    this.supprime_par = "0";
    this.tags = JSON.stringify([]);
    this.url_avatar_auteur = "//imedia.emn.fr/SCElgg/elgg-1.8.13/_graphics/icons/default/medium.png";
    this.utilisateur_a_vote = "0";
    this.uid_page = "";
    this.uid_oa = "";

	angular.extend(this,options);
	if (this.nom_page == "")
		this.nom_page = "_pf_res";
	if (this.uid_page == "")
		this.uid_page = "_pf_res";
}
//On passe le compile injecté en référence aux deux fonctions ci-dessous
//Parce qu'elles ne seront pas forcément capable de récuperer l'injector() de l'app
Message.contextualiserMessage = function ($compile) {
	//S'il y a des messages à remettre dans le contexte 
	if(window.location.hash)
	{
		//Tableau avec les deux paramètres
		var params = window.location.hash.replace("#/", "").split('-');
		var emplacement = params[0];
		var id_message = params[1];

		//Si c'est sur la ressource
		if (emplacement == "res") {
			displayMessages($(globals.selectorRes), id_message);
			addFocusedItem($(globals.selectorRes));		
		}
		//Si c'est sur la page
		else if (emplacement == "page")
		{
			
			var array_messages = JSON.parse(localStorage["ngStorage-array_messages"])[PF.globals.capsule.id_capsule];
			//Flag 
			var elementFound = false;

			var $compile = $compile || angular.element(document).injector().get('$compile'),
				main_controller_scope = PF.getScopeOfController('main_controller');

			//Dans tous les messages de la page
			$.each(array_messages[PF.globals.nom_page], function (index, message) {
				//Si le message existe
				if (message['id_message'] == id_message)
				{
					//On essaie de retrouver l'élément DOM concerné
					var elementWithMess;

					//En fonction de la facon d'attacher les messages
					//Si on est dans le cas d'objet d'apprentissage identifiés
					if (PF.globals.styleAttacheMessages == "OA") {
						//S'il est attaché à la page
						if (message['uid_oa'] == "")
						{
							elementWithMess = $(PF.globals.selectorPage); //Profeci
						}
						//Sinon, c'est qu'il est sur un grain
						else
						{
							elementWithMess = $('[data-oauid='+message['uid_oa']+']');
						}
					}
					//Si on est dans le cas d'attache automatique sur élément bas niveau
					else{
						if (PF.isScenari() || PF.isFromLatex() || PF.isSpecialCase())
						{
							//S'il est attaché à la page
							if (message['nom_tag'] == "PAGE")
								elementWithMess = $(PF.globals.selectorPage);
							//Sinon, c'est qu'il est sur un grain
							else
								elementWithMess = $(message['nom_tag'] ,PF.globals.selectorContent).get(message['num_occurence']);						
						}
						// else
							// PF.displayAlert(J42R.get('web_label_aucun_objet_apprentissage'), "info");
					}

					//Si on l'a trouvé
					if ($(elementWithMess).length)
					{
						//On l'affiche, et on le met en valeur (true en second parametre, idMessToFocus)
						// Message.afficher($(elementWithMess), id_message);
						// angular.element('[ng-controller=main_]').injector().get('$scope').$apply(function () {

						$(elementWithMess).attr('highlighted',id_message);
						if(main_controller_scope)
							$compile(elementWithMess)(main_controller_scope);
						// });
						// addFocusedItem($(elementWithMess));

				        // $('#tplCo').animate({ 
				        //     scrollTop: $(elementWithMess).position().top 
				        // }, 600);
						elementFound = true;
					}
					
				}
			});

			// if(!elementFound)
				// displayAlert(J42R.get('web_label_erreur_msg_inexistant'),'error');
		}
		
	}
}
//On passe le localStorage injecté en référence aux deux fonctions ci-dessous
//Parce qu'elles ne seront pas forcément capable de récuperer l'injector() de l'app
Message.recupererMessages = function (wipe_cache, callback, $localStorage) {
	
		if(!$localStorage){
			try{
				$localStorage = angular.element(document).injector().get('$localStorage');

			}
			catch(e){
				console.log("Injector not ready : " + e);
			}
		}

		var id_capsule = PF.globals.capsule.id_capsule,
			params,
			wipe_cache = wipe_cache || false;

		//Si on n'a pas encore de message du tout
		//Ou que l'on souhaite supprimer le cache
		if (typeof($localStorage.array_messages) == "undefined" 
			|| typeof($localStorage.array_messages[PF.globals.capsule.id_capsule]) == "undefined"
			|| wipe_cache)
			params = {'id_capsule' : id_capsule, "langues_affichage" : 3};
		//Sinon, on envoie le timestamp le plus récent
		else{
			var filtre_timestamp = {};
			filtre_timestamp[id_capsule] = $localStorage.array_messages[PF.globals.capsule.id_capsule]['_timestamp_last_message'] || 0;
			params = {"filtre_timestamp" : filtre_timestamp, "langues_affichage" : 3};
		}


		PF.get("message",params,function (data, status, headers, config)
		{			
			var array_messages = data;

			Message.importerMessages(wipe_cache, array_messages['messages']);

			//Callback function
			if(typeof(callback) === "function")
			{
				callback();
			}
		});		
}
Message.importerMessages = function  (wipe_cache, nouveaux_messages) {
	//Récuperation du localstorage angular
	var $localStorage = angular.element(document).injector().get('$localStorage');
	// $localStorage.$reset();
	//Création des tableaux de messages dans le localStorage au cas ou
	if (typeof($localStorage.array_messages) == "undefined"){
		$localStorage.array_messages = {};
	}
	
	//Si le localstorage commence a devenir fat, et peut ralentir l'utilisation de la ressource, on purge les messages stockés pour les autres sections
	else if(JSON.stringify($localStorage.array_messages).length > 100000){
		var autres_capsules = Object.keys($localStorage.array_messages);
		for (var i = 0; i < autres_capsules.length; i++) {
			if (autres_capsules[i] != PF.globals.capsule.id_capsule)
				delete $localStorage.array_messages[autres_capsules[i]];
		};
	}
		//Reset du tableau des messages dans le localstorage
		// $localStorage.array_messages = {};
	
	//Si on n'a pas encore de message du tout
	//Ou que l'on souhaite supprimer le cache
	if (typeof($localStorage.array_messages[PF.globals.capsule.id_capsule]) == "undefined"
		|| wipe_cache){
		$localStorage.array_messages[PF.globals.capsule.id_capsule] = {};
	}

	var	current_id_user = PF.globals.utilisateur_local.id_utilisateur,
		array_messages = $localStorage.array_messages[PF.globals.capsule.id_capsule],
		_timestamp_last_message = array_messages['_timestamp_last_message'] || 0,
		array_messages_lus = $localStorage.array_messages_lus = $localStorage.array_messages_lus || [],
		key_id_page = "nom_page";

	//Si on est dans le cas d'objet d'apprentissage identifiés
	if (PF.globals.styleAttacheMessages == "OA") {
		key_id_page = "uid_page";
	}

	//Pour chaque message
	$.each(nouveaux_messages, function () {
		var _this = new Message(this),
			//Présence & index du message dans le localstorage
			index_of_message = (typeof(array_messages[_this[key_id_page]]) == 'undefined') ? -1 : array_messages[_this[key_id_page]].map(function (e) {return(e.id_message)}).indexOf(_this.id_message);

		//Si le message n'est pas encore là
		if (index_of_message == -1)
		{
			//Messages sur la page courante
			//Sinon, message sur les autres pages / ressource
			//Stockage dans le $localstorage
			if(typeof(array_messages[_this[key_id_page]]) == 'undefined')
					array_messages[_this[key_id_page]] = [];

			array_messages[_this[key_id_page]].push(_this);
		}
		//Si le message est déjà enregistré, mais a été modifié depuis
		else if(array_messages[_this[key_id_page]][index_of_message]['date_modification'] < _this.date_modification)
		{
			array_messages[_this[key_id_page]][index_of_message] = _this;
		}

		//Dans tous les cas, on garde en stock le dernier timestamp du message le plus récent
		if(_this.date_modification > _timestamp_last_message)
			_timestamp_last_message = _this.date_modification;
	});

	//On réenregistre le timestamp dans le localStorage
	$localStorage.array_messages[PF.globals.capsule.id_capsule]['_timestamp_last_message'] = _timestamp_last_message;

	//Important : ajouter des bulles pour tous les liens
	var a = $('a[href]:not([href^=#])').filter(function (index, object) {
		var normalized = PF.normaliserURL($(this).attr('href'));
		return typeof(array_messages[normalized]) !== "undefined";
	});
	a.attr('pf-commentable', '');

	var $compile = angular.element(document).injector().get('$compile');
	$compile(a)(PF.getScopeOfController('main_controller'));
}

Message.getSavedSortForType = function (display_type) {
	//Récupération des valeurs actuelles
	var sorts_per_displays = Message.getLocalStorageSortType();

	//On renvoie le mode correspondant
	return sorts_per_displays !== false ? sorts_per_displays[display_type] : 'default';

}
/*
	* LocalStorage sorts_per_displays :
	* {
	*   'transversal' : 'date' || 'votes',
	*   'user' : 'date' || 'votes',
	*   'normal' : 'date' || 'votes'
	* }
	*
	* 'default' quand erreur / rien de trouvé / stocké.
	*/
Message.getLocalStorageSortType = function  () {
	if (typeof(localStorage) != "undefined")
	{
		//S'il n'y a pas encore de type, on met les valeurs par défaut
		if (typeof(localStorage.sorts_per_displays) == 'undefined') {
			var sorts_per_displays = {'transversal' : 'date', 'user' : 'date', 'normal' : 'votes'};
			localStorage.sorts_per_displays = JSON.stringify(sorts_per_displays);
		}
		else{
			//Récupération de la valeur stockée
			var sorts_per_displays = JSON.parse(localStorage.sorts_per_displays);
		}
		return sorts_per_displays;

	}
	else 
		return false;
}

Message.setSavedSortForType = function (sort_type, display_type) {
	//Récupération des valeurs actuelles
	var sorts_per_displays = getLocalStorageSortType();

	//Si les valeurs sont bien récupérées
	if(sorts_per_displays !== false){
		//On enregistre le type
		sorts_per_displays[display_type] = sort_type;

		localStorage.sorts_per_displays = JSON.stringify(sorts_per_displays);

		return true;
	}
	else 
		return false;
}
Message.getNextSortType = function (current_sort_type) {
	var sorts_array = ['date', 'votes'];

	//On récupère l'index du prochain mode de tri, et si on sort de la limite du tableau, on revient à 0
	var next_index = (sorts_array.indexOf(current_sort_type) +1) >= sorts_array.length ? 0 : sorts_array.indexOf(current_sort_type) +1;

	return sorts_array[next_index];
}
Message.isVisible = function  (message, callbackTrue, callbackFalse) {
	if(message.supprime_par == 0)
	{
		if (typeof(callbackTrue) == "function")
			callbackTrue();
	}
	else{
		//Si c'est le commentaire de l'utilisateur courant, ou qu'il est admin, ou qu'il a un rang supérieur à 3
		if((message.id_user == "isConnected()") || "isAdmin()" || ("getRank()" >= 3))
		{
			if (typeof(callbackTrue) == "function")
				callbackTrue();
		}
		else{
			if (typeof(callbackFalse) == "function")
				callbackFalse();
		}
	}
}

Message.afficher = function (idMessToFocus, idUser) {
		
	PF.getScopeOfController('controller_messages').afficherMessagesTransversal(idMessToFocus, idUser);

}
