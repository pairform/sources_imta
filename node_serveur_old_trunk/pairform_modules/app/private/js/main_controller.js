'use strict';


//Injection de tous les modules
$.get(PF.globals.url.views + 'layer', function(layer) {
	$('body').prepend(layer);
	// var dom_layer = $("[ng-app='pf_app']");
	// angular.injector(['ng']).invoke(function($compile) {
	//var scope = angular.element(dom_layer).scope();
	//$compile(dom_layer)(scope);
	//});

	/*
	 *	Main Controller
	 */

	var pf_app = angular.module('pf_app',[
		"pascalprecht.translate"
		,"ngStorage"
		,"ngAnimate"
		,"toaster"
		,"ngCookies"
		,"angular-loading-bar"
		,"angularFileUpload"
		,"ngRoute"
	]);

	//Si on est sur le site vitrine
	if(document.querySelector('meta[name="pf_vitrine"]','head'))
		PF.globals.is_vitrine = true;

	if (PF.globals.is_vitrine) {
		PF.initVitrine(pf_app);
	}
	/*
	 *	Internationalisation
	 */

	pf_app.config(["$translateProvider", function ($translateProvider) {
		$translateProvider.useStaticFilesLoader({
			prefix: PF.globals.url.private + 'json/',
			suffix: '/strings.json'
		});
		$translateProvider.preferredLanguage("fr");
	}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
		cfpLoadingBarProvider.includeSpinner = false;
	}]).config(['$httpProvider', function($httpProvider) {
		// $httpProvider.defaults.useXDomain = true;
		$httpProvider.defaults.withCredentials = true;
		// $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		}
	]);


	/*
	* Filtres
	*/

	pf_app.filter('date_timestamp', ["$filter", function myDateFormat($filter){
		return function(timestamp){
		//Conversion en millisecondes
		var  tempdate= new Date(timestamp *1000);
		return $filter('date')(tempdate, "dd/MM/yy \à HH:mm");
	}
	}]);

	pf_app.filter('search', ["$filter", function ($filter) {
		return function(input, term, fields, operator) {
			if (!term) {
				return input;
			}

			fields || (fields = []);

			if (!fields.length) {
				return $filter('filter')(input, term);
			}

			operator || (operator = false); // true=OR, false=AND

			var filtered = [], valid;

			angular.forEach(input, function(value, key) {
				valid = !operator;
				for(var i in fields) {
					var index = value[fields[i]].toLowerCase().indexOf(term.toLowerCase());
					// OR : found any? valid
					if (operator && index >= 0) { 
						valid = true; break;
					} 
					// AND: not found once? invalid 
					else if (!operator && index < 0) { 
						valid = false; break;
					}
				}
				if (valid) {
					this.push(value);
				}
			}, filtered);

			return filtered;
		};
	}]);

	pf_app.filter('orderMessages', function() {
		return function(messages_array_before_sort, _scope, reverse) {
			_scope = _scope || $scope;
			messages_array_before_sort = messages_array_before_sort || [];
			var messages_array = [];
			var messages_array_reponse = [];
			//Tableau des posts réarrangés
			var messages_object = {};

			//Stockage des messages lus
			var array_messages_lus = [];
			//On ajoute les messages lus à ceux éventuellements déjà lus
			if (typeof(localStorage.array_messages_lus) != "undefined")
				array_messages_lus = JSON.parse(localStorage.array_messages_lus);

			//S'il n'y a pas de message sur la page, ça sert a rien de faire la suite
			if (messages_array_before_sort.length) {
				//Tableau des messages de l'OA
				$.each(messages_array_before_sort, function (index,message) {
					
					if(PF.globals.display_type == "user"){
						if(message['id_auteur'] == _scope.id_utilisateur){
							messages_array.push(message);
						}	
					}
					else if(PF.globals.display_type == "transversal"){
						messages_array.push(message);
					}
					else{
						if(message['id_message_parent'] == "")	
							messages_array.push(message);
						else
							messages_array_reponse.push(message);
					}
				});

				
				//Si c'est temporel
				// if ((_scope.sort_type == 'date') || (_scope.sort_type == 'default')) {
					//Ordre temporel décroissant
					messages_array.sort(function (a,b) {
						//Si a est créé avant
						if (a.date_creation > b.date_creation)
							return -1;
						if (a.date_creation < b.date_creation)
							return 1;
						return 0;

					});

					//Ordre temporel décroissant
					messages_array_reponse.sort(function (a,b) {
						//Si a est créé avant
						if (a.date_creation > b.date_creation)
							return -1;
						if (a.date_creation < b.date_creation)
							return 1;
						return 0;
					});
				// }
				//Si c'est par votes
				if (_scope.sort_type == 'votes') {
					//Plus voté d'abord
					messages_array.sort(function (a,b) {
						//Si a est plus utile
						if (parseInt(a.somme_votes) > parseInt(b.somme_votes))
							return -1;
						if (parseInt(a.somme_votes) < parseInt(b.somme_votes))
							return 1;
						return 0;

					});

					//Plus voté d'abord
					messages_array_reponse.sort(function (a,b) {
						//Attention, ordre inversé!
						if (parseInt(a.somme_votes) > parseInt(b.somme_votes))
							return 1;
						if (parseInt(a.somme_votes) < parseInt(b.somme_votes))
							return -1;
						return 0;
					});
				}
				// _controller_messages_scope.next_sort_type = Message.getNextSortType(sort_type);

				//Si on est dans le cas de message d'un grain, les réponses sont déjà triés dans un tableau, sinon, non
				if((PF.globals.display_type == "user") || (PF.globals.display_type == "transversal")){

					var messages_object = PF.toObject(messages_array_before_sort);
					//On tri les messages qui sont des réponses de défi
					var temp_messages_array = messages_array.filter(function (message, index) {
						//Si le message est une réponse
						if((message['id_message_parent'] != ""))
						{
							var parent_message = messages_object[message['id_message_parent']];
							//S'il se passe quelque chose de louche
							if(!parent_message)
								//On ne laisse pas passer la réponse vacante
								return false;

							if (parent_message.est_defi == 1 || parent_message.est_defi == "1") {
								//Si c'est le commentaire de l'utilisateur courant
								//Ou que l'utilisateur courant est initiateur du défi
								if((message.id_auteur != _scope.utilisateur_local.id_utilisateur) 
									&& (parent_message.id_auteur != _scope.utilisateur_local.id_utilisateur) 
									&& (_scope.utilisateur_local.rank[_scope.capsule.id_capsule].id_categorie < 4)){
									
									return false;
								}
							}
						}
						return true;
					});

					return temp_messages_array;
				}
				else{
					var messages_object = PF.toObject(messages_array);
					//On tri les messages qui sont des réponses de défi
					$.each(messages_array_reponse, function (index,message) {
						//Si le message est une réponse
						if((message['id_message_parent'] != ""))	
						{
							var parent_message = messages_object[message['id_message_parent']];
							message.message_parent = parent_message;
							var new_index = messages_array.indexOf(parent_message);

							//S'il se passe quelque chose de louche	//On ne laisse pas passer la réponse vacante
							if(parent_message && new_index != -1){
								if (parent_message.est_defi == 1 || parent_message.est_defi == "1") {
									//Si c'est le commentaire de l'utilisateur courant
									//Ou que l'utilisateur courant est initiateur du défi
									if((this.id_auteur == _scope.utilisateur_local.id_utilisateur) 
										|| (parent_message.id_auteur == _scope.utilisateur_local.id_utilisateur) 
										|| (_scope.utilisateur_local.rank[_scope.capsule.id_capsule].id_categorie >= 4)){
										
										messages_array.splice(new_index+1, 0, this);
									}
								}
								else
									messages_array.splice(new_index+1, 0, this);
							}
							else{
								messages_array.splice(0, 0, this);	
								this.id_message_parent = 0;
							}
							
						}
							
					});	

					return messages_array;
				}

			}
			else
				return [];
		};
	});


	/*
	 *	Directives
	 */

	pf_app.directive('stopEvent', function () {
		return {
			restrict: 'A',
			link: function (scope, element, attr) {
				element.bind(attr.stopEvent, function (e) {
					e.stopPropagation();
				});
			}
		};
	});

	pf_app.directive('pfMessage', function () {
		return {
			restrict: 'E',
			link: function (scope, element, attr) {
				scope.init = function (message_updated) {
					var msg = message_updated || scope.message;

					element.addClass('rank-' + msg.id_role_auteur);

					if(msg.supprime_par != '0'){
						element.addClass('supprime'); 
					}
					else
						element.removeClass('supprime'); 	

					if((msg.id_message_parent != '') && !scope.transversal)
						element.addClass('reponse'); 
					else
						element.removeClass('reponse');

					if(msg.est_defi >= 1)
						element.addClass('est_defi');
					
					element.data({
						"id_message" : msg.id_message,
						"id_auteur" : msg.id_auteur,
						"id_role_auteur" : msg.id_role_auteur,
						"supprime_par" : msg.supprime_par,
						"id_langue" : msg.id_langue,
						"prive" : msg.prive,
						"est_defi" : msg.est_defi >= 1 ? msg.est_defi : '',
						"defi_valide" : msg.defi_valide == true ? msg.defi_valide : '',
						"id_message_parent" : msg.id_message_parent != '' ? msg.id_message_parent : ''
					});
				}
				scope.init();

				scope.$watch('message',function (message_updated) {
					scope.init(message_updated);
				}, true)
			}
		};
	});
	pf_app.directive('pfMenuMedaille', function () {
	    return {
	        restrict: 'E',
	        template: '<div class="comDonnerMedaille-container ui-tooltip-bottom">' +
				        	'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'or\')" ng-src="{{res_url_private}}img/medaille_or.png" title="Attribuer une médaille d\'or">' +
				        	'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'argent\')" ng-src="{{res_url_private}}img/medaille_argent.png" title="Attribuer une médaille d\'argent">' +
				        	'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'bronze\')" ng-src="{{res_url_private}}img/medaille_bronze.png" title="Attribuer une médaille de bronze">' +
				        	'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'enlever\')" ng-src="{{res_url_private}}img/error.png" title="Supprimer la médaille">'+
			        	'</div>'
	    };
	});
	pf_app.directive('pfMenuTags', function () {
	    return {
	        restrict: 'E',
	        template: '<div class="ui-tooltip-bottom">' +
	        				'<input ng-model="new_tags" placeholder="Tags séparés par des virgules">' +
	        				'<div class="btn-action" ng-click="ajouterTags(message, new_tags); new_tags = \'\'; ">{{"button_ajouter" | translate}}</div>' +
			        	'</div>'
	    };
	});
	pf_app.directive('pfMenuLangue', function () {
	    return {
	        restrict: 'E',
	        template: 	'<div class="comModifierLangue-container ui-tooltip-bottom">'+
							'<select ng-select="nouveau.code_langue" id="message-set-language-list">'+
								'<option value="1">English </option>'+
								'<option value="2">Español </option>'+
								'<option value="3">Français </option>'+
							'</select>'+
						'</div>'
	    };
	});
	pf_app.directive('pfMenuPartager', function () {
	    return {
	        restrict: 'E',
	        template: 	'<div class="ui-tooltip-bottom comPartager-container">'+
							'<span>{{message_permalien}}</span>'+
						'</div>'
	    };
	});
	pf_app.directive('pfMenuOptionsMessage', function () {
	    return {
	        restrict: 'E',
	        template: 	'<div class="message-options-container ui-tooltip-bottom">'+
							'<div class="message-visibilite-container">'+
								'<span class="message-visibilite-label">Visibilité :</span>'+
								'<ul class="message-visibilite-list">'+
									'<li ng-show="nouveau.liste_reseaux.length == 0" class="message-visibilite-item message-visibilite-item-public" title="Par défaut">{{(nouveau.message_parent_prive && nouveau.message_parent_prive != \'0\') ? "Restreinte" : "Publique"}}</li>'+
									'<li ng-repeat="reseau in nouveau.liste_reseaux track by reseau.id_collection" ng-click="enleverReseauVisibilite(reseau)" class="message-visibilite-item message-visibilite-item-public" title="Par défaut">{{reseau.nom}}</li>'+
									'<li ng-if="!nouveau.message_parent_prive || nouveau.message_parent_prive == \'0\'" class="message-visibilite-item message-visibilite-add-item" ng-click="afficherReseau(\'usage\')" title="Changer la visibilité">+</li>'+								
								'</ul>'+
							'</div>'+
							'<div ng-if="((utilisateur_local.rank[capsule.id_capsule].id_categorie >= 4) || (utilisateur_local.est_admin))" class="message-defi-container">'+
								'<span>Défi : </span>'+
								'<div class="onoffswitch">'+
									'<input type="checkbox" ng-model="nouveau.est_defi" class="onoffswitch-checkbox" id="myonoffswitch">'+
									'<label class="onoffswitch-label" for="myonoffswitch">'+
										'<div class="onoffswitch-inner"></div>'+
										'<div class="onoffswitch-switch"></div>'+
									'</label>'+
								'</div>'+
							'</div>'+
						'</div>'
	    };
	});
	pf_app.directive('pfMenuChangerRole', function () {
	    return {
	        restrict: 'E',
	        template: 	
				'<ul class="rank-upgrade-list">' +
					'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 4)">{{"label_user_expert" | translate}}' +
					'</li>' +
					'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 3)">{{"label_user_animateur" | translate}}' +
					'</li>' +
					'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 2)">{{"label_user_collaborateur" | translate}}' +
					'</li>' +
					'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 1)">{{"label_user_participant" | translate}}' +
					'</li>' +
					'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 0)">{{"web_label_revenir_role_normal" | translate}}' +
					'</li>' +
				'</ul>'
	    }
	});
	pf_app.directive('pfObjectifApprentissage', function() {
		return {
			restrict: 'A',
			link: function (scope, element) {
				scope.updateModel = function (new_value, old_value) {
					if(new_value != old_value)
						scope.$parent.updateObjectif(scope.objectif);
				}
				scope.$watch('objectif.nom', scope.updateModel);
				scope.$watch('objectif.est_valide', scope.updateModel);
			}
		}
	});
	pf_app.directive('pfQuestionProfilApprentissage', function() {
		return {
			restrict: 'A',
			link: function (scope, element) {
				scope.updateModel = function (new_value, old_value) {
					if(new_value != old_value)
						scope.$parent.updateProfilQuestion(scope.question);
				}
				scope.$watch('question.id_reponse_valide', scope.updateModel);
			}
		}
	});
	pf_app.directive('pfCommentable', ['$localStorage', function($localStorage) {
		return {
			restrict: 'A',
			scope:{
				"highlighted": "@"
			},
			link: function (scope, element) {
				//Ajout de la classe commentable
				element.addClass('pf-commentable');
				
				scope.$storage = $localStorage;
				scope.messages_local = [];
				scope.messages_lus = scope.$storage.array_messages_lus;
				
				// scope.$watch($localStorage.array_messages, scope.updateLocalMessages);
				scope.init = function () {
					//Type de l'élément : 
					//Ressource
					if (PF.matches(element, PF.globals.selectorRes)) {
						scope.nom_tag = "";
						scope.num_occurence = "";
						scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule + "']['_pf_res']";
					}
					//Page
					else if (PF.matches(element, PF.globals.selectorPage)) {
						scope.nom_tag = "PAGE";
						scope.num_occurence = "";
						scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule +"']['" + PF.globals.nom_page + "']";
					}
					//Grain
					else if (PF.matches(element, PF.globals.selector)) {

						scope.nom_tag = element[0].nodeName;
						scope.num_occurence = $(scope.nom_tag, PF.globals.selectorContent).index(element[0]);
						scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule +"']['" + PF.globals.nom_page + "']";
					}
					//Autre page
					else {
						scope.nom_tag = "PAGE";
						scope.num_occurence = "";
						var _nom_page = PF.normaliserURL($(element).attr('href'));
						scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule +"']['" + _nom_page + "']";
					}
					//Initialisation du tableau de messages
					scope.messages_local = [];
				};

				scope.updateLocalMessages = function (array_messages) {
					if (!array_messages) {
						return;
					}
					//Type de l'élément : 
					//Ressource
					if (PF.matches(element, PF.globals.selectorRes)) {
						scope.messages_local = array_messages || [];
					}
					//Page
					else if (PF.matches(element, PF.globals.selectorPage)) {
						if (typeof(array_messages) != "undefined") {
							scope.messages_local = $.grep(array_messages,function (message) {
								return (message.nom_tag == "PAGE");
							});
						}
						else
							scope.messages_local = [];
					}
					//Grain
					else if (PF.matches(element, PF.globals.selector)) {
						if (typeof(array_messages) != "undefined") {

							scope.messages_local = $.grep(array_messages,function (message) {
								return ((message.nom_tag == scope.nom_tag) && (message.num_occurence == scope.num_occurence));
							});
						}
						else
							scope.messages_local = [];
					}
					//Autre page
					else {
						scope.messages_local = array_messages || [];
					}

					scope.updateCount();
				}

				scope.updateCount = function () {
					if (scope.messages_local.length){
						var count;

						if (scope.messages_lus){
							count = scope.messages_local.map(function (message) {
								return message.id_message;
							}).filter(function(n) {
								return scope.messages_lus.indexOf(n) === -1
							}).length;
						}
						else
							count = scope.messages_local.length;

						element.addClass('hasComment');
						if (count == 0) {
							element.addClass('readed');
							element.attr('data-count', scope.messages_local.length);
						}
						else{
							element.attr('data-count', count);
							element.removeClass('readed');
						}
					}
					else{
						element.removeAttr('data-count');
						element.removeClass('hasComment readed');
					}
				}

				scope.afficherDepuisOA = function () {
					$('.focusedItem').removeClass('focusedItem');
					element.addClass('focusedItem');
					PF.getScopeOfController('controller_messages').afficherMessagesDepuisOA(scope.messages_local, scope.nom_tag, scope.num_occurence, scope.highlighted);
				}

				function handleDragOver(e) {
					if (e.preventDefault) {
					  e.preventDefault(); // Allows us to drop.
					}

					element.addClass('pf-droppable-hover');

					return false;
				};

				function handleDragEnter(e) {
					element.addClass('pf-droppable-hover');
				};

				function handleDragLeave(e) {
					// this/e.target is previous target element.
					element.removeClass('pf-droppable-hover');
				};

				function handleDrop(e) {
					// this/e.target is current target element.

					if (e.stopPropagation) {
					  e.stopPropagation(); // stops the browser from redirecting.
					}

					if (e.target == element[0] || e.target.parentElement == element[0]) {

						var data_string = e.originalEvent.dataTransfer.getData("text/javascript");
						if (data_string && data_string != "undefined") {
							var data = JSON.parse(data_string);

							//Tableau de messages
							if (data instanceof Array) {
								var array_messages = data;
								for (var i = 0; i < array_messages.length; i++) {
									var message = array_messages[i],
										post_data = {
											"id_message" : message.id_message,
											"nom_tag" : scope.nom_tag,
											"num_occurence" : scope.num_occurence,
											"id_capsule" : PF.globals.capsule.id_capsule,
											"nom_page" : (scope.nom_tag == "" && scope.num_occurence == "") ? "" : PF.globals.nom_page
										}

									element.removeClass("pf-droppable pf-droppable-hover");
									PF.post("message/deplacer", post_data, function (data) {
										//Si on est sur le dernier des messages à bouger
										if (i == array_messages.length ) {
											var retour = data;
											if (retour['status'] == 'ok')
											{
												scope.$root.$broadcast("pf:dropped", message);
												scope.highlighted = message.id_message;
												scope.afficherDepuisOA(scope.highlighted);
											}
											else{
												scope.$root.$broadcast("pf:dragend");
											}
										}
									});
								};
							}
							//Message unique
							else if(data instanceof Object){
								var message = data,
									post_data = {
										"id_message" : message.id_message,
										"nom_tag" : scope.nom_tag,
										"num_occurence" : scope.num_occurence,
										"id_capsule" : PF.globals.capsule.id_capsule,
										"nom_page" : (scope.nom_tag == "" && scope.num_occurence == "") ? "" : PF.globals.nom_page
									}

								element.removeClass("pf-droppable pf-droppable-hover");
								PF.post("message/deplacer", post_data, function (data) {
									var retour = data;
									if (retour['status'] == 'ok')
									{
										scope.$root.$broadcast("pf:dropped", message);
										scope.highlighted = message.id_message;
										scope.afficherDepuisOA(scope.highlighted);
									}
									else{
										scope.$root.$broadcast("pf:dragend");
									}
								});
							}

						}
					}
				};

				scope.$on("pf:dragstart", function () {
					element.on('dragenter', handleDragEnter);
					element.on('dragover', handleDragOver);
					element.on('dragleave', handleDragLeave);
					element.on('drop', handleDrop);
					
					element.addClass("pf-droppable");


				});

				scope.$on("pf:dragend", function () {
					// body...
					element.removeClass("pf-droppable");

					element.off('dragenter');
					element.off('dragover');
					element.off('dragleave');
					element.off('drop');
				});
				// scope.updateLocalMessages();
				scope.init();

				if (scope.highlighted)
					scope.afficherDepuisOA(scope.highlighted);

				scope.$watch('messages_lus', scope.updateCount, true);
				scope.$watch(scope.watcher_messages, function (new_value, old_value) {
					if (new_value != scope.messages_local) {
					// if (!angular.equals(new_value[PF.globals.capsule.id_capsule], old_value[PF.globals.capsule.id_capsule])) {
						scope.updateLocalMessages(new_value);
						if (element.hasClass('focusedItem')){
							PF.getScopeOfController('controller_messages').afficherMessagesDepuisOA(scope.messages_local, scope.nom_tag, scope.num_occurence);		
						}
					}
				}, true);

				//Affichage du message, uniquement si l'élément n'est pas un lien vers une autre page
				if(PF.matches(element, PF.globals.selectorRes) || PF.matches(element, PF.globals.selectorPage) || PF.matches(element, PF.globals.selector))
					element.bind('click', scope.afficherDepuisOA);
				// element.attr('style',"background-color: red;");
			}

		}
	}]);

	pf_app.directive('draggable', function($document) {
		return {
			restrict: 'A',
			link: function(scope, element, attr) {

				function handleDragStart(e) {
					//On empèche l'event de bubbler 
					//Sur un DnD d'un message, ca bubble au btn-bar
					e.stopPropagation();

	                e.originalEvent.dataTransfer.effectAllowed = 'move';
	                var data;
	                
	                //Si on déplace un seul message
	                if (scope.message)
	                	data = JSON.stringify(scope.message);
	                //Sinon, on déplace tous les messages
	                else
	                	data = JSON.stringify(scope.array_messages_deplacement);

	                e.originalEvent.dataTransfer.setData('text/javascript', data);
					// this/e.target is the source node.
					// this.addClassName('moving');
					scope.$root.$broadcast("pf:dragstart");
				};


				function handleDragEnd(e) {
					scope.$root.$broadcast("pf:dragend");
				};

				// col.setAttribute('draggable', 'true');  // Enable columns to be draggable.
				element.on('dragstart', handleDragStart);
				element.on('dragend', handleDragEnd);


			}
		}
	});
	/*
	 *	Controllers
	 */

	/*
		TOASTER : 
		'success'
		'error', 
		'wait', 
		'warning'
		'note', 
	*/
	pf_app.controller('main_controller', ["$scope", "$localStorage", "toaster", "$translate", function ($scope, $localStorage, toaster, $translate) {
		
		$scope.capsule = PF.globals.capsule;

		//S'il n'y a pas d'utilisateur du tout, ou que l'utilisateur non connecté n'est pas encore venu sur cette capsule
		if (!$localStorage.utilisateur_local 
		|| (!$localStorage.utilisateur_local.est_connecte
		&& !$localStorage.utilisateur_local.rank[$scope.capsule.id_capsule]))
			//On regénère un utilisateur, pour qu'il ait le rang pour cette capsule
			$localStorage.utilisateur_local = new Utilisateur();
		else
			//Sinon, on le récupère
			$localStorage.utilisateur_local = $localStorage.utilisateur_local;

		$scope.utilisateur_local = $localStorage.utilisateur_local;
		PF.globals.utilisateur_local = $localStorage.utilisateur_local;
		$scope.codeFromId = PF.langue.codeFromId;
		$scope.idFromCode = PF.langue.idFromCode;
		$scope.res_url_private = PF.globals.url.private;
		$scope.res_url_public = PF.globals.url.public;
		$scope.res_url_root = PF.globals.url.root;

		//On récupère les messages en déplacement depuis le localStorage dès qu'on peut
		$scope.$root.array_messages_deplacement = $localStorage.array_messages_deplacement;


		//Vérification que l'utilisateur a un rôle pour cette ressource
		$scope.verifierRole = function () {
			PF.post("utilisateur/categorie/verifier", {
					"id_ressource" : $scope.capsule.id_ressource,
					"id_capsule" : $scope.capsule.id_capsule
			}, function  (retour) {
				var status = retour["status"];
				if (status == "ko" && retour['message'] == "utilisateur_invalide"
					|| retour["id_utilisateur"] != $scope.utilisateur_local.id_utilisateur) {
					//Conflit d'identité : on déconnecte
					$scope.deconnecter("web_label_session_expiree");
				}
				//S'il y a mise à jour
				else if(status == "up"){
					//S'il faut mettre à jour la categorie
					if (typeof(retour["categorie_ressource"]) != "undefined") {
						//S'il n'y avait rien, ni rôle ni parametres pour cette capsule
						if (retour["categorie_ressource"] == false) {
							//Mise à jour de la catégorie avec les parametres par défaut
							$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
								"id_ressource" : $scope.capsule.id_ressource,
								"score" : "0",
								"notification_mail" : "1",
								"afficher_noms_reels" : "0",
								"nom_categorie" : "Lecteur",
								"id_categorie" : "0"
							}
						}
						//Sinon, l'utilisateur n'avait pas visité cette capsule, mais avait déjà un rôle dans cette ressource
						else{
							//Mise à jour de la catégorie avec les parametres revenant du serveur
							$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
								"id_ressource" : $scope.capsule.id_ressource,
								"score" : retour["categorie_ressource"]["score"],
								"notification_mail" : "1",
								"afficher_noms_reels" : "0",
								"nom_categorie" : retour["categorie_ressource"]["nom_categorie"],
								"id_categorie" : retour["categorie_ressource"]["id_categorie"]
							}
						}
					}
					//Sinon, il y avait déjà les rôles .
					else if (typeof(retour["utilisateur_parametres"]) != "undefined") {
						//Mise à jour des paramètres utilisateurs
						$scope.utilisateur_local.rank[$scope.capsule.id_capsule].notification_mail = "1";
						$scope.utilisateur_local.rank[$scope.capsule.id_capsule].afficher_noms_reels = "0";
					}
				}
			});
		}


		$scope.afficherConnexion = function (mode){
			var scope_connexion = PF.getScopeOfController('controller_connexion');
			if (scope_connexion){
				// scope_connexion.$parent.setVisible();
				// scope_connexion.$parent.popin_visible = true;
				scope_connexion.visible = true;
				// scope_connexion.popin.visible = true;
				scope_connexion.mode = mode || 'connexion';
			}	
		}

		$scope.setSession = function (){

			// var date = new Date();
			// date.setTime(date.getTime() + (24 * 60 * 1000));

			$scope.utilisateur_local = $localStorage.utilisateur_local;
			PF.globals.utilisateur_local = $scope.utilisateur_local;
			$scope.utilisateur_local.est_connecte = true;
			// $localStorage.session = {"elggperm" : $scope.utilisateur_local.elggperm, "expiration" : date};
		}

		$scope.removeSession = function (){
			$scope.utilisateur_local = $localStorage.utilisateur_local;
			PF.globals.utilisateur_local = $scope.utilisateur_local;
			$scope.utilisateur_local.est_connecte = false;
			$localStorage.session = {};
		}

		$scope.afficherMessagesUtilisateur = function (id_utilisateur) {
			Message.afficher(undefined, id_utilisateur);
		}
		$scope.afficherMessagesTransversal = function () {
			Message.afficher(undefined, undefined);
		}
		$scope.afficherProfil = function (id_utilisateur) {
			if (typeof(id_utilisateur) == "undefined")
				var id_utilisateur = $localStorage.utilisateur_local.id_utilisateur;
			//On passe la valeur
			PF.getScopeOfController('controller_profil').utilisateur.id_utilisateur = id_utilisateur;
			//Et on affiche le panneau après, qui appelle init()
			var _profil = new Panneau({'nom_controller' : 'controller_profil', 'position' : 'right', 'should_toggle' : false});
			_profil.togglePanneau();
		}
		$scope.afficherProfilConfig = function () {
			// if (typeof(id_utilisateur) == "undefined")
			var utilisateur_local = $localStorage.utilisateur_local;
			//On passe la valeur
			PF.getScopeOfController('controller_profil_config').utilisateur = utilisateur_local;
			//Et on affiche le panneau après, qui appelle init()
			var _profil_config = new Panneau({'nom_controller' : 'controller_profil_config', 'position' : 'right', 'superpose' : true});
			_profil_config.togglePanneau();
		}
		$scope.afficherChangerAvatar = function () {
			// if (typeof(id_utilisateur) == "undefined")
			var utilisateur_local = $localStorage.utilisateur_local;
			//On passe la valeur
			PF.getScopeOfController('controller_avatar_upload').utilisateur = utilisateur_local;
			//Et on affiche le panneau après, qui appelle init()
			var _changer_avatar = new Panneau({'nom_controller' : 'controller_avatar_upload', 'position' : 'right', 'superpose' : true});
			_changer_avatar.togglePanneau();
		}
		$scope.afficherSucces = function (id_utilisateur, succes) {
			if (typeof(id_utilisateur) == "undefined")
				var id_utilisateur = undefined;
			//On passe la valeur
			PF.getScopeOfController('controller_succes').id_utilisateur = id_utilisateur;
			PF.getScopeOfController('controller_succes').succes = succes || undefined;
			//Et on affiche le panneau après, qui appelle init()
			var _succes = new Panneau({'nom_controller' : 'controller_succes', 'position' : 'left'});
			_succes.togglePanneau();
		}
		$scope.afficherReseau = function (mode, utilisateur_ajout) {
			PF.getScopeOfController('controller_reseau').mode = mode || "gestion";
			PF.getScopeOfController('controller_reseau').utilisateur_ajout = utilisateur_ajout || undefined;

			var _reseau = new Panneau({'nom_controller' : 'controller_reseau', 'position' : 'right', 'superpose' : true});
			_reseau.togglePanneau();
			
		}
		$scope.afficherDashboard = function () {
			// var _dashboard = new Panneau({'nom_controller' : 'controller_dashboard', 'position' : 'left'});
			// _dashboard.togglePanneau();
			$scope.fonctionPasDisponible();
		}
		$scope.afficherRessources = function () {
			var _ressources = new Panneau({'nom_controller' : 'controller_ressources', 'position' : 'left'});
			_ressources.togglePanneau();
		}

		$scope.afficherRecherche = function (mode) {
			PF.getScopeOfController('controller_recherche_profil').mode = mode || "recherche_profil";
			var _recherche_profil = new Panneau({'nom_controller' : 'controller_recherche_profil', 'position' : 'left'});
			_recherche_profil.togglePanneau();
		}
		$scope.afficherPreferences = function () {
			var _preferences = new Panneau({'nom_controller' : 'controller_preferences', 'position' : 'left'});
			_preferences.togglePanneau();
		}
		$scope.afficherPresentation = function () {
			var _presentation = new Panneau({'nom_controller' : 'controller_presentation', 'position' : 'left'});
			_presentation.togglePanneau();
		}

		$scope.$root.afficherFicheRessource = function (id_ressource) {
			PF.getScopeOfController('controller_fiche_ressource').id_ressource = id_ressource || PF.globals.capsule.id_ressource;
			var _fiche_ressource = new Panneau({'nom_controller' : 'controller_fiche_ressource', 'position' : 'left', "should_toggle" : false, "superpose" : true});
			
			_fiche_ressource.togglePanneau();
		}

		$scope.deconnecter = function (toast, callback){

			var _scope = $scope;
			PF.post('utilisateur/logout', {}, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok')
				{
					//Reset de l'utilisateur
					$localStorage.utilisateur_local = new Utilisateur();
					//On enlève la session stockée
					_scope.removeSession();
					//On arrête les notifications
					Notification.removeUpdateTimer();

					//On rechope tous les messages
					Message.recupererMessages(true);
					//On toast grave
					if (typeof(toast) != "undefined"){
						if(toast != ""){
							toaster.clear();
							toaster.pop("warning",toast);
						}
						if(typeof(callback) == "function")
							callback();
					}
					else{
						toaster.pop("success","label_deconnexion_reussi");
						if(typeof(callback) == "function")
							callback();
					}
				}
				else
				{
					toaster.pop("error", "title_erreur_identifiants");
				}

			}, function callback_error (data, status, headers, config) {
				toaster.pop("error", "title_erreur_identifiants");
			});
		}
		$scope.safeApply = function(fn) {
			var phase = this.$root.$$phase;
			if(phase == '$apply' || phase == '$digest') {
				if(fn && (typeof(fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply(fn);
			}
		}
		//Init par défaut
		$scope.init = function () {
			this;
		}

		//Remove par défaut
		$scope.remove = function () {
			this.panneau.cacherPanneau();
		}
		//Fonctionnalité pas encore disponible...
		$scope.fonctionPasDisponible = function () {
			toaster.pop("note", "web_label_fonctionnalité_indisponible");
		}
		//Fonctionnalité pas encore disponible...
		$scope.changerLangue = function (code_langue) {
			code_langue = code_langue || "fr";
			$translate.uses(code_langue);
		}
		//Fonctionnalité pas encore disponible...
		$scope.changerLangue = function (code_langue) {
			code_langue = code_langue || "fr";
			$translate.uses(code_langue);
		}

	
		if ($scope.utilisateur_local.est_connecte){
			//Vérification que l'utilisateur a un rôle pour cette ressource	
			$scope.verifierRole();
			//Lancement du timer de polling des notifications
			Notification.setUpdateTimer();
		}

		//Configuration des raccourcis clavier
		angular.element(document).on("keyup", function (e) {
			if (e.keyCode == 27){
				$scope.safeApply(function () {
					// body...
					Panneau.cacherDernierDeLaPile("globale");
				})
			}
		});
	}]);

	pf_app.controller('controller_nav_bar', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
		var defaults = {
			"visible" : true,
			"sidebar_visible" : false,
			"messages_deplacement_visible" : false,
			"notifications" : {'us' : [], 'un' : [], 'count_us' : 0, 'count_un' : 0, 'visible' : ''}
		};
		angular.extend($scope, defaults);

		$scope.actualiser = function () {
			Message.recupererMessages();
			toaster.pop('success',"web_label_messages_actualises");
			Notification.updateAll();
		}

		$scope.afficherNotification = function (type) {
			if ($scope.notifications.visible != type){
				$scope.notifications.visible = type;

				//Récupération des ids des notifications non vues
				var notifications_non_vues = $scope.notifications[type].map(function (notification, index) {
					if(notification.date_vue == null){
						notification.date_vue = Math.round(Date.now() / 1000);
						return notification.id_notification;
					}
				}).filter(function(n){ return n != undefined });

		
				//Gestion de la synchro des vues des notifications
				if (notifications_non_vues.length > 0)
				{
					var old_count = ""+$scope.notifications['count_'+type];
					$scope.notifications['count_'+type] = 0;

					var post = {"id_notifications" : notifications_non_vues};

					PF.post("notification/vue", post , function (retour){
						
						if (retour['status'] == 'ko')
						{
							if (retour['message'] == "utilisateur_invalide") {
								$scope.deconnecter("web_label_session_expiree");
							}
							else
								toaster.pop('error', retour['message']);
							
							$scope.notifications['count_'+type] = old_count;
						}
					});
					
				}
			}
			else
				$scope.notifications.visible = '';
		}

		$scope.actionNotification = function (notification, notification_type) {
			//Score utilisateur
			if (notification_type == "us"){
				//Si on est dans le cas d'un succes, il n'y a pas de message attaché
				if (!notification.id_message){
					$scope.afficherProfil();
				}
				else{
					PF.redirigerVersMessage(notification.id_message);
				}
			}
			//Notification utilisateur
			else{
				switch(notification.sous_type){
					// dc : défi créé 
					case "dc" : {
						PF.redirigerVersMessage(notification.id_message);
						break;
					}
					// dt : défi terminé 
					case "dt" : {
						PF.redirigerVersMessage(notification.id_message);
						break;
					}
					// dv : défi validé 
					case "dv" : {
						PF.redirigerVersMessage(notification.id_message);
						break;
					}
					// ar : ajout réseau
					case "ar" : {
						$scope.afficherProfil(notification.contenu);
						break;
					}
					// cr : classe rejointe
					case "cr" : {
						$scope.afficherProfil(notification.contenu);
						break;
					}
					// ru : réponse d'utilisateur
					case "ru" : {
						PF.redirigerVersMessage(notification.id_message);
						break;
					}
					// gm : gagné médaille
					case "gm" : {
						PF.redirigerVersMessage(notification.id_message);
						break;
					}
					// gs : gagné succès
					case "gs" : {
						$scope.afficherProfil();
						break;
					}
					// cc : changé catégorie
					case "cc" : {
						$scope.afficherProfil();
						break;
					}
				}
			}
		}

		$scope.afficherMessagesDeplacement = function () {
			$scope.messages_deplacement_visible = !$scope.messages_deplacement_visible;

			if ($scope.messages_deplacement_visible)
				toaster.pop("info", "web_label_deplacez_com_sur_grain");
		}
		$scope.supprimerDeplacementMessage = function (message) {

			var index = $scope.$root.array_messages_deplacement.indexOf(message);

			if (index != -1){
				$scope.$root.array_messages_deplacement.splice(index, 1);
				//On fait pointer une variable globale a tous les controllers sur le local storage,
				//Pour pouvoir watcher ca dans le controller nav_bar
				$localStorage.array_messages_deplacement = $scope.$root.array_messages_deplacement;
			}
		}

		$scope.$on("pf:dropped", function (event, message) {
			var index = -1;

			$localStorage.array_messages_deplacement.some(function (msg, idx, arr) {
				if (msg.id_message == message.id_message)
					return index = idx;
			});
			if (index !== -1)
				$localStorage.array_messages_deplacement.splice(index, 1);

			$scope.$root.array_messages_deplacement = $localStorage.array_messages_deplacement;

			toaster.pop("success", "web_label_msg_deplace_avec_succes");
			Message.recupererMessages(true);
			
		});
	}]);

	pf_app.controller('controller_connexion', ["$scope", "$http", "$localStorage", "toaster", "$translate", function ($scope, $http, $localStorage, toaster, $translate) {
		var defaults = {
			"visible" : false,
			"mode" : "connexion",
			"tableau_langues_label" : PF.langue.tableau_labels,
			// "connexion" : {"username" : "Bigood", "password" : "azerty"},
			"mot_de_passe_oublie" : {},
			"enregistrement" : {
				"id_capsule" : PF.globals.capsule.id_capsule
			}
		};
		// $scope.$watch('visible', function (new_value, old_value) {
		// 	$scope.$parent.visible = new_value;
		// });

		angular.extend($scope, defaults);

		$scope.connecter = function (){
			console.log(this.connexion);
			var _scope = $scope;
			PF.post('utilisateur/login', this.connexion, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok')
				{
					var user = new Utilisateur(retour);
					//On cale l'ID elgg et le nom dans le stockage local
					if($localStorage)
					{
						$localStorage.utilisateur_local = user;
					}
					
					
					PF.getScopeOfController("main_controller").setSession();
					_scope.verifierRole();
					Notification.setUpdateTimer();

					// updatePostOs();

					toaster.pop("success","label_connexion_reussi");
					_scope.visible = false;
					//Mise à jour des messages de la page
					//IMPORTANT : on efface le cache des messages
					Message.recupererMessages(true);
					ga('send', 'event', 'user', 'login_success', user.username);

				}
				else
				{
					PF.getScopeOfController('controller_connexion').connexion.password = "";
					// $('#popin-connexion').effect("shake");
					
					
					if(retour['message'] == "utilisateur_invalide")
						toaster.pop("error", "title_erreur_identifiants");
					else
						toaster.pop("error", retour['message']);

					ga('send', 'event', 'user', 'login_fail');
				}

			}, function callback_error (data, status, headers, config) {
				
			});
		}

		$scope.checkForLTIToken = function () {
			//Récuperation des parametres passés en GET dans l'URL depuis la passerelle LTI
			var url_params = getSearchParameters();
			//On récupère le token == elggperm
			var token = url_params['token']
			//Si on est bien dans le cas d'une connexion LTI (et qu'on a le localStorage)
			if (typeof(token) != "undefined"){

				$localStorage.premiere_visite = true;

				//Important : prévenir le WS qu'on est dans le cas de LTI, en changeant la valeur de version
				var post_params = {
					"version" : "LTI",
					"elggperm" : token
				}
				var _scope = $scope;
				_scope.deconnecter('', function(){
					//Et log de l'utilisateur.
					PF.post('utilisateur/login', post_params, function callback_success (data, status, headers, config) {
						var retour = data;
						if (retour['status'] == 'ok')
						{
							var user = new Utilisateur(retour);
							//On cale l'ID elgg et le nom dans le stockage local
							if($localStorage)
							{
								$localStorage.utilisateur_local = user;
							}
							
							
							PF.getScopeOfController("main_controller").setSession();
							_scope.verifierRole();
							Notification.setUpdateTimer();

							// updatePostOs();

							toaster.pop("success","label_connexion_reussi");
							//Mise à jour des messages de la page
							//IMPORTANT : on efface le cache des messages
							Message.recupererMessages(true);
							ga('send', 'event', 'user', 'login_success', user.username);

						}
						else
						{
							toaster.pop("error", retour['message']);
							ga('send', 'event', 'user', 'login_fail', user.username);
						}

					});
				});
			}
		}
		
		function getSearchParameters () {
			var prmstr = window.location.search.substr(1);
			return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
		}

		function transformToAssocArray ( prmstr ) {
			var params = {};
			var prmarr = prmstr.split("&");
			for ( var i = 0; i < prmarr.length; i++) {
				var tmparr = prmarr[i].split("=");
				params[tmparr[0]] = tmparr[1];
			}
			return params;
		}
		
		$scope.renvoyerMotDePasse = function (){
			
			var _scope = $scope;
			PF.post('utilisateur/reset', this.mot_de_passe_oublie, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok') {
					_scope.mode = "enregistrement-1";
				}
				else {
					toaster.pop("error", retour['message']);
				}
			});
		}
		$scope.enregistrer = function (){
			//Attention : register_cgu à la place de register-cgu
			var _scope = $scope;
			PF.put('utilisateur/enregistrer', this.enregistrement, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok') {
					_scope.mode = "enregistrement-1";
					ga('send', 'event', 'user', 'register', this.enregistrement.id_capsule);
				}
				else {
					toaster.pop("error", retour['message']);
				}
			});
		}

		//Vérification LTI
		$scope.checkForLTIToken();
	}]);

	pf_app.controller('controller_accroche', ["$scope", "$localStorage", function ($scope, $localStorage) {
		var defaults = {
			"visible" : false
		};

		angular.extend($scope, defaults);
		
		if (!$localStorage.premiere_visite && !PF.globals.is_vitrine && !/(android|ipad|iphone|ipod|mobile)/i.test(navigator.userAgent||navigator.vendor||window.opera)){
			var _accroche = new PanneauModal({'nom_controller' : 'controller_accroche'});
			_accroche.togglePanneau();
		}

		$scope.remove = function () {
			$localStorage.premiere_visite = true;
			this.panneau.cacherPanneau();
		}
		$scope.fermerEtAfficherConnexion = function () {
			$scope.remove();
			$scope.afficherConnexion("enregistrement-0");
		}
		$scope.fermerEtAfficherPresentation = function () {
			$scope.remove();
			$scope.afficherPresentation();
		}

	}]);

	pf_app.controller('controller_presentation', ["$scope", function ($scope) {
		var defaults = {
			"visible" : false
		};

		angular.extend($scope, defaults);

	}]);
	pf_app.controller('controller_avatar_upload', ["$scope", "$http", "toaster", "$upload", function ($scope, $http, toaster, $upload) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"utilisateur" : {}
		};

		angular.extend($scope, defaults);

		// $scope.init = function () {
		// 	//...
		// }
		// récupération des informations sur le champ "Logo"
		$scope.recupererLogo = function($files) {
			//S'il n'y a pas de fichier choisi (annulation de l'utilisateur)
			if ($files.length == 0)
				return;
			
			// si le fichier n'est pas une image dans le bon format
			if (!$files[0].type.match(/(image)/))
				return toaster.pop("error", "Le fichier selectionné n'est pas une image.");

			var _URL = window.URL || window.webkitURL;
			var hauteur, largeur;
			var image = new Image();

			// transformation du fichier en image, la fonction onload sera appelé à la fin
			image.src = _URL.createObjectURL($files[0]);

			image.onload = function() {
			
				// affichage du nom du logo sélectionné
				$scope.nom_logo = $files[0].name;

				// affichage du logo selectionné
				var file_reader = new FileReader();

				file_reader.readAsDataURL($files[0]);

				file_reader.onload = function (e) {
					$scope.$applyAsync(function () {
						// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
						$scope.src_logo = e.target.result;
					})
				};			
			}	
		}
		$scope.uploadAvatar = function () {


			PF.request_with_file("post", "utilisateur/avatar", document.querySelector("#avatar_upload form input").files[0], function(data, status, headers, config) {
			// $.ajax({
   //              url: PF.globals.url.ws + 'utilisateur/avatar',
   //              type: 'POST',
   //              async: true,
   //              cache: false,
   //              contentType: false,
   //              processData: false,
   //              data: form
   //          }).done(function(data) { 
				if (data['status'] == "ok") {
					
				}
				else{
					if (data['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop("error", data['message']);
						// toaster.pop("error", "web_label_erreur_contacter_pairform");
				}
			})
		}

	}]);

	pf_app.controller('controller_fiche_ressource', ["$scope", function ($scope) {
		var defaults = {
			"id_ressource" : null,
			"loaded" : null,
			"panneau_affiche" : 2,							//Panneau affiché par défaut
			"objectif_apprentissage_affiche" : 1,			//Objectif affiché par défaut
			"profil_apprentissage_affiche" : 0,			//Profil caché par défaut
			"objectif_apprentissage_edition" : 0,			//Mode edition off
			"ressource" :{
				"nom_court" : "Chargement...",
				"nom_long" : "Chargement...",
				"etablissement" : "Chargement...",
				"url_logo" : PF.globals.url.root + "public/img/logo_defaut.png",
				"theme" : "Chargement...",
				"auteurs" : "Chargement...",
				"licence" : "Chargement...",
				"date_creation" : "Chargement...",
				"date_modification" : "Chargement...",
				"description" : "Chargement...",
				"capsules" : [
				],
				"objectifs_apprentissage" : {
					"liste_objectifs_apprentissage": [
					],
					"nb_objectifs_valides": 0
				}
			}
		};

		angular.extend($scope, defaults);

		$scope.init = function () {
			//Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
			if ($scope.loaded != $scope.id_ressource) {
				var _scope = $scope;
				PF.get('ressource', {"id_ressource": $scope.id_ressource, "id_utilisateur": $scope.utilisateur_local.id_utilisateur} , function(data, status, headers, config) {
					if (data['status'] == "ok") {
						data.ressource.url_logo = PF.globals.url.root + data.ressource.url_logo;
						
						angular.extend(_scope, data);
						_scope.loaded = _scope.id_ressource;
					}
					
				});
			}
		}
		
		//Update du nombre d'objectif d'apprentissage validés
		$scope.$watch('ressource.objectifs_apprentissage.liste_objectifs_apprentissage', function (liste_objectifs_apprentissage_updated, old_value) {
			var count = 0;
			for (var i = liste_objectifs_apprentissage_updated.length - 1; i >= 0; i--) {
				count += +liste_objectifs_apprentissage_updated[i].est_valide;
			}
			$scope.ressource.objectifs_apprentissage.nb_objectifs_valides = count;
		},true);

		// $scope.panneau_affiche = 1;
		
		
		$scope.objectif_apprentissage_affiche = 1;
		
		$scope.objectif_apprentissage_edition = 0;

		$scope.ajouterObjectif = function () {
			PF.put('objectif', {"id_ressource": $scope.id_ressource, "nom" : ""} , function(data, status, headers, config) {
				if (data['status'] == "ok") {
					$scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.push({"id_objectif" : data['id_objectif'], "nom" : "", "est_valide" : 0});
				}
				else{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop("error", "web_label_erreur_contacter_pairform");
				}
				
			});
		}

		$scope.updateObjectif = function (objectif) {
			var id_objectif = objectif.id_objectif,
				nom = objectif.nom,
				est_valide = objectif.est_valide;

			PF.post('objectif', {"id_objectif": id_objectif, "nom" : nom, "est_valide" : est_valide} , function(data, status, headers, config) {
				if (data['status'] == "ok") {
					//Cast de est_valide en -1 / 1 por false / true
					// var new_count = -1 + 2* (+est_valide);
					//Changement du décompte d'objectifs d'apprentissage valides 
					// $scope.ressource.objectifs_apprentissage.nb_objectifs_valides += new_count;
				}
				else{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop("error", "web_label_erreur_contacter_pairform");
				}
				
			});
		}

		$scope.enleverObjectif = function (objectif) {
			PF.delete('objectif', {"id_objectif" : objectif.id_objectif} , function(data, status, headers, config) {
				if (data['status'] == "ok") {
					var index_objectif = $scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.indexOf(objectif);
					$scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.splice(index_objectif,1);

					//Cast de est_valide en -1 / 1 por false / true
					// var new_count = -1 + 2* (+objectif.est_valide);
					//Changement du décompte d'objectifs d'apprentissage valides 
					// $scope.ressource.objectifs_apprentissage.nb_objectifs_valides -= new_count;
				}
				else{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop("error", "web_label_erreur_contacter_pairform");
				}
				
			});
		}
		$scope.editerObjectifs = function () {
			//Mode edition off / on
			$scope.objectif_apprentissage_edition = +!$scope.objectif_apprentissage_edition;
		}

		$scope.updateProfilQuestion = function (question) {
			var id_question = question.id_question,
				id_reponse_valide = question.id_reponse_valide,
				param = {"id_utilisateur" : $scope.utilisateur_local.id_utilisateur, 
					"liste_reponses_valides" : [
						{
							"id_question": id_question,
							"id_reponse_valide" : id_reponse_valide
						}
					]
				};

			PF.post('profil', param , function(data, status, headers, config) {
				if (data['status'] == "ok") {
					//Rien a faire
				}
				else{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop("error", "web_label_erreur_contacter_pairform");
				}
				
			});
		}
		$scope.afficherSousSection = function (index) {
			$scope.panneau_affiche = index;
		}
		$scope.changerAffichageObjectif = function () {
			$scope.objectif_apprentissage_affiche = +!$scope.objectif_apprentissage_affiche;
		}
		$scope.changerAffichageProfil = function () {
			$scope.profil_apprentissage_affiche = +!$scope.profil_apprentissage_affiche;
		}
	}]);



	pf_app.controller('controller_succes', ["$scope", function ($scope) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"id_utilisateur" : null,
			"succes" : [],
		};

		angular.extend($scope, defaults);

		$scope.init = function () {
			//S'il y a déjà des succès, on laisse tomber
			if ($scope.succes && $scope.id_utilisateur)
				return;

			//Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
			if ($scope.loaded != $scope.id_utilisateur) {
				var _scope = $scope;
				PF.get('accomplissement/succes', {"id_utilisateur": $scope.id_utilisateur} , function(data, status, headers, config) {
					var retour = data;
					angular.extend(_scope, retour);

					_scope.loaded = _scope.id_utilisateur;
					
				});
			}
		}
	}]);

	pf_app.controller('controller_preferences', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"preferences" : {
				"connexion_automatique" : false,
				"notification_mail" : false,
				"afficher_noms_reels" : false
			}
		};
		angular.extend($scope, defaults);
 		angular.extend($scope, $scope.utilisateur_local.rank[$scope.capsule.id_capsule]);

		$scope.init = function () {
			// Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
			// if ($scope.loaded != $scope.id_utilisateur) {
				var _scope = $scope;
				PF.get('utilisateur/preferences', {"id_capsule" : $scope.capsule.id_capsule} , function(data, status, headers, config) {
					var retour = data;
					if (retour["status"] == "ko") {
						//Rien de neuf
						if (retour['message'] == "utilisateur_invalide") {
							$scope.deconnecter("web_label_session_expiree");
						};
					}
					else{
						angular.extend(_scope, retour);
					}
					// _scope.loaded = _scope.id_utilisateur;
					
				});
			// }
		}
		$scope.switchNotificationMail = function () {

			var post = {'id_utilisateur' : $scope.utilisateur_local.id_utilisateur, 'id_capsule' : $scope.capsule.id_capsule};
			ga('send', 'event', 'user', 'change_notifications_level', post);

			//On inverse la valeur (cast booléen puis string)
			var notif_mail = $scope.preferences.notification_mail = +!$scope.preferences.notification_mail;

			PF.post('utilisateur/notification/mail',post , function (retour){
				//Si ya pas de soucis
				if (retour['status'] == 'ok')
				{
					var alert = notif_mail ? 'web_label_activer' : 'web_label_desactiver';
					toaster.pop('success', "'web_label_notification_par_mail' + '"+alert+ "' + 'web_label_sur_cette_ressource'");
			  	}
			  	else{
			  		if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
			  	}
			});
		}
		$scope.switchNomsReels = function () {
			$scope.fonctionPasDisponible();
		}

		$scope.reinitialiserMessagesLus = function () {
			delete $localStorage.array_messages_lus;
			toaster.pop('success',"web_label_messages_lus_reinitialises");

			setTimeout(function () {
				location.reload();
			},1000);
		}

		$scope.effacerToutesDonnees = function () {
			$localStorage.$reset();
			localStorage.clear();

			setTimeout(function () {
				location.reload();
			},400);
		}
		
	}]);


	pf_app.controller('controller_messages', ["$scope", "$localStorage", "$timeout", "toaster", function ($scope, $localStorage, $timeout, toaster) {
		var defaults = {
			"sort_types":["date", "votes"],
			"messages_array":[],
			"transversal":false,
			"nom_tag":"",
			"num_occurence":"",
			"langueApp":"fr",
			"visible_menu_medaille" : undefined,
			"visible_menu_tags" : undefined,
			"nouveau" : {
				"contenu" : "",
				"est_defi" : "",
				"visibilite" : "",
				"id_langue" : "3",
				"code_langue" : "fr",
				"id_message_parent" : "",
				"visibilite_message_parent" : "",
				"liste_reseaux" : []
			}
		};

		angular.extend($scope, defaults);
		// $scope.$watch('visible_menu_options_message', function (new_value, old_value) {
		// 	if (new_value == true)
		// 		$('#comBar').attr('style', 'overflow:visible;');
		// 	else
		// 		$('#comBar').attr('style', '');
		// });

		$scope.init = function  () {
			$scope.sort_type = Message.getSavedSortForType(PF.globals.display_type);
			$scope.next_sort_type = $scope.sort_types.indexOf($scope.sort_type) + 1 >= $scope.sort_types.length ? $scope.sort_types[0] : $scope.sort_types[$scope.sort_types.indexOf($scope.sort_type) + 1];
		}
		
		$scope.repondre = function (message) {
			//Si le message est déjà une réponse
			if (message.id_message_parent != "") 
				$scope.nouveau.id_message_parent = message.id_message_parent;
			else
				$scope.nouveau.id_message_parent = message.id_message;

			//On donne le focus au textarea
			$('#formComBar textarea').focus();
			//On met le pseudo
			$scope.nouveau.contenu = "@"+message.pseudo_auteur+" ";
			$scope.nouveau.message_parent_prive = message.prive;
			//On met en avant le message
			$scope.highlight(message.id_message);
		}
		$scope.envoyer = function  (params) {
			if ($scope.utilisateur_local.est_connecte || PF.globals.is_vitrine) {
				var params = params || this.nouveau;
				//On trime le contenu du message
				params.contenu = params.contenu.trim();
				//S'il n'y a rien à envoyer
				if (!!params.contenu) {

					//On ne peut envoyer qu'un message à la fois
					if (typeof(PF.globals.sendingMessage) == "undefined")
					{
						//Flag pour éviter les doubles post
						PF.globals.sendingMessage = true;
						var nom_page_temp = ($scope.nom_tag == "" && $scope.num_occurence == "") ? "" : PF.globals.nom_page;

						//Cas spécial pour les démos
						if(PF.globals.is_vitrine){
							//On met un id random directement sur le message
							//Assez haut pour qu'il n'y ait pas de conflit futur
							params.id_message = Math.round(Math.random() * 10000000);
							//On enlève le flag de double post, puisqu'il n'y a pas d'échange avec le serveur
							delete PF.globals.sendingMessage;

							//Si c'est bien l'utilisateur qui a écrit un post,
							//Il n'y a pas de flag demo_reponse_automatique
							if (!params.demo_reponse_automatique) {
								//Si on est sur le site vitrine, on met un compte démo qui n'existe pas
								$scope.utilisateur_local = new Utilisateur({"username" : "Vous", "avatar_url" : "https://secure.gravatar.com/avatar/?d=mm"});
							
								//On garde en mémoire le nombre de message écrit par l'utilisateur
								//TODO: mettre ça dans le localstorage
								if (!PF.globals.demo_nombre_message_postes)
									PF.globals.demo_nombre_message_postes = 0;

								PF.globals.demo_nombre_message_postes++;
								//On écrit une réponse automatiquement à ce poste dans 2 secondes
								setTimeout(function(){
									//On stocke l'utilisateur démo
									var _utilisateur_demo = $scope.utilisateur_local;
									var contenu_reponse = [
									 "@Vous Bravo, vous avez posté votre premier commentaire! \r\n A présent, essayez de répondre à ce commentaire...",
									 "@Vous Vous pouvez également voter pour ou contre un commentaire. Plus un message est voté, plus il est visible. \r\n Essayez de voter pour ce commentaire!",
									 "@Vous Pour continuer, peut-être voulez-vous créer un compte? Cliquez sur \"Connexion\", en haut de votre écran!",
									 "@Vous Si vous voulez tester toutes les fonctionalités de PairForm, vous pouvez aller dans la ressource Bac à sable. \r\n Pour cela, cliquez sur le bouton menu (en haut à gauche), puis sélectionnez le document Bac à Sable."];
									

									//Flag de réponse automatique pour ne pas répondre à une réponse automatique
									var index_contenu_reponse = PF.globals.demo_nombre_message_postes > contenu_reponse.length ? contenu_reponse.length - 1 : PF.globals.demo_nombre_message_postes -1,
										demo_reponse = {
										"demo_reponse_automatique" : true,
										"contenu": contenu_reponse[index_contenu_reponse],
										"est_defi":"",
										"visibilite":"",
										"id_langue":"3",
										"code_langue":"fr",
										"id_message_parent": params.id_message_parent ? params.id_message_parent : params.id_message,
										"visibilite_message_parent":"",
										"liste_reseaux":[],
										"message_parent_prive":0
									};

									//Si on a posté déjà un message, on est censé essayer de répondre au message de PairForm
									//Si l'utilisateur ne l'a pas fait
									if (index_contenu_reponse == 1 && !params.id_message_parent)
									{
										//On reset le nombre de messages postés, pour que l'utilisateur n'avance pas plus dans les étapes
										//(cette variable est incrémenté quand l'utlisateur juste après dans le code, au moment de l'envoi du message.)
										PF.globals.demo_nombre_message_postes--;
										demo_reponse.contenu = "@Vous Pour répondre à un commentaire, cliquez sur le bouton \"répondre\" sur ce dernier!";
									}

									//On publie la réponse
									$scope.envoyer(demo_reponse);

								}, Math.round(Math.random() * 2000) + 1000);
								//Random sur le timing, pour que ça à l'air naturel (avec minimum une seconde de temps de réponse)
							}
							//Dans le cas d'une réponse automatique
							else{
								//On connecte l'utilisateur PairForm, qu'on va déconnecter par la suite
								if(PF.globals.url.root.match(/imedia.emn.fr/)){
									$scope.utilisateur_local = new Utilisateur({
									  "id_utilisateur": 8482,
									  "username": "PairForm",
									  "avatar_url": "http://imedia.emn.fr/PairForm_elgg/mod/profile/icondirect.php?lastcache=1390906173&joindate=1377509608&guid=8482&size=medium",
									  "rank": {
									    "31": {
									      "id_categorie": "0",
									      "nom_categorie": "Administrateur"
									    }
									  }
									});
								}
								else{
									$scope.utilisateur_local = new Utilisateur({
									  "id_utilisateur": 35,
									  "username": "PairForm",
									  "avatar_url": "https://podcast.mines-nantes.fr/PairForm_Elgg/avatar/view/PairForm/medium/0",
									  "rank": {
									    "112": {
									      "id_categorie": "0",
									      "nom_categorie": "Administrateur"
									    }
									  }
									});
								}
							}

						}

						angular.extend(params, {
							"id_auteur" : $scope.utilisateur_local.id_utilisateur,
							"id_capsule" : $scope.capsule.id_capsule,
							"id_role_auteur" : $scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie,
							"est_defi" : +params.est_defi,
							"nom_auteur" : $scope.utilisateur_local.name,
							"nom_page" : nom_page_temp,
							"nom_tag" : $scope.nom_tag,
							"num_occurence" : $scope.num_occurence,
							"pseudo_auteur" : $scope.utilisateur_local.username,
							"role_auteur" : $scope.utilisateur_local.rank[$scope.capsule.id_capsule].nom_categorie,
							"url_avatar_auteur" : $scope.utilisateur_local.avatar_url,
							"prive" : ((params.message_parent_prive && (params.message_parent_prive != "0")) || (params.liste_reseaux.length > 0) ? 1 : 0),
							"visibilite" : JSON.stringify(params.liste_reseaux.map(function (reseau) { return reseau.id_collection}))
						});
					

						var nouveau_message = new Message(params);

						if(typeof($localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page]) == "undefined")
							$localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page] = [];
						
						$localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page].push(nouveau_message);
						$scope.messages_array.push(nouveau_message);
						//On highlight le nouveau message
						
						$scope.highlight("0", 200);

						//Si c'est un message posté par l'utilisateur, et pas une réponse simulée
						if (!params.demo_reponse_automatique){
							toaster.pop('success', "web_label_commentaire_poste");
						}
						
						this.nouveau.contenu = "";
						//Si on est sur le site vitrine, on quitte direct : on ne veut pas que les messages soient vraiment postés sur le serveur.
						if(PF.globals.is_vitrine){

							//Si on est sur le site vitrine, on re-met un compte démo qui n'existe pas
							$scope.utilisateur_local = new Utilisateur({"username" : "Vous", "avatar_url" : "https://secure.gravatar.com/avatar/?d=mm"});
							return;
						}

						PF.put('message', nouveau_message , function(data, status, headers, config) {
							var retour = data;

							if (retour['status'] == "ok") {	
								
								//Obliger de rafraichir dans un timeout, pour éviter le digest en cours
								// $timeout(function () {
									//Important : cast en string de l'identifiant
									nouveau_message.id_message = retour['id_message'];
									// nouveau_message.id_message = ""+retour['id_message'];
								// }, 200);
								$localStorage.array_messages_lus.push(nouveau_message.id_message);
								// ga('send', 'event', 'message_sent', 'page', retour['id_message']);
							}
							else{
								if (retour['message'] == "utilisateur_invalide") {
									$scope.deconnecter("web_label_session_expiree");
								}
								else
									toaster.pop('error', retour['message']);
								
								$localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page].pop();
								$scope.messages_array.pop();
							}
							//Update manuel des notifications
							Notification.updateAllWithDelay();
							PF.globals.sendingMessage = undefined;
						});
					}
				}
				else
					toaster.pop('error', 'label_erreur_saisie_message');

			}
			else
				$scope.afficherConnexion();

		}
		$scope.voter = function (message, vote) {

			//Si on est pas connecté, on quitte (Sauf si on est sur le site vitrine)
			if (!$scope.utilisateur_local.est_connecte && !PF.globals.is_vitrine) {	
				$scope.afficherConnexion();
				return false;
			}
			//On check si l'utilisateur essaie de voter sur son message
			if($scope.utilisateur_local.id_utilisateur == message.id_auteur)
			{
				toaster.pop('error', 'web_label_pas_voter_votre_message');	
				return;
			}

			//Récupération du type de vote
			//True pour up, false pour down
			var vote_up = vote == 1 ? true : false;

			//Annulation du vote
			if(vote == message.utilisateur_a_vote)
			{
				//Update du décompte
				message.somme_votes = (parseInt(message.somme_votes) + (vote_up ? -1 : 1)) + "";
				message.utilisateur_a_vote = "0";
			}
			else
			{		
				//Nouveau vote	
				if(message.utilisateur_a_vote == 0)
				{
					//Update du décompte
					message.somme_votes = (parseInt(message.somme_votes) + (vote_up ? 1 : -1)) + "";
					message.utilisateur_a_vote = ""+vote;
				}
				//Changement de vote
				else
				{
					//Update du décompte
					message.somme_votes = (parseInt(message.somme_votes) + (vote_up ? 2 : -2)) + "";
					message.utilisateur_a_vote = ""+vote;
				}
			}
			//Si on est sur le site vitrine,
			if(PF.globals.is_vitrine){
				//On stope ici, pas de communication avec le serveur
				return;
			}
			//Variable de post de message
			var post_vote = {'up' : vote_up , "id_message" : message.id_message};

			ga('send', 'event', 'message_vote', vote == 1 ? "up" : "down", $scope.utilisateur_local.id_utilisateur);

			PF.post("message/voter", post_vote, function (retour) {
				if (retour['status'] == "ko") {
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop('note', retour["message"]);
				}
				else{
					Notification.updateAllWithDelay();
				}
			});

			//Highlight du message
			// $scope.highlight(message.id_message);
		}
		$scope.afficherVotes = function (message) {
			
			//Si on est pas connecté, on quitte (Sauf si on est sur le site vitrine)
			if (!$scope.utilisateur_local.est_connecte) {	
				$scope.afficherConnexion();
				return false;
			}
			if ($scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie < 2) {
				toaster.pop('note', "web_label_erreur_suppression");
				return;
			}

			message.toggle_somme_votes = !message.toggle_somme_votes;
			if (message.toggle_somme_votes) {
				PF.get("message/votes", {"id_message" : message.id_message}, function (retour) {
					if (retour['status'] == "ko") {
						if (retour['message'] == "utilisateur_invalide") {
							$scope.deconnecter("web_label_session_expiree");
						}
						else
							toaster.pop('note', retour["message"]);
					}
					else{
						angular.extend(message, retour.data[0]);
					}
				});
			}

		}
		$scope.afficherMessagesDepuisOA = function (_messages_array, _nom_tag, _num_occurence, highlight){
			$scope.nouveau = {
				"contenu" : "",
				"est_defi" : "",
				"visibilite" : "",
				"id_langue" : "3",
				"code_langue" : "fr",
				"id_message_parent" : "",
				"visibilite_message_parent" : "",
				"liste_reseaux" : []
			}
			$scope.nom_tag = _nom_tag;
			$scope.num_occurence = _num_occurence;
			$scope.messages_array = _messages_array;
			// $scope.messages_array = messagesFilter(_messages_array, $scope);
			PF.globals.display_type = "normal";

			$scope.transversal = false;

			var ids_messages_lus = $.map($scope.messages_array, function (message) {
				return message.id_message;
			});
			//MAJ des messages lus
			for (var i = ids_messages_lus.length - 1; i >= 0; i--) {
				if($localStorage.array_messages_lus.indexOf(ids_messages_lus[i]))
					$localStorage.array_messages_lus.push(ids_messages_lus[i]);
			}
			
			$scope.safeApply(function (argument) {
				if (!$scope.panneau) {
					var panneau = new Panneau({'nom_controller' : 'controller_messages', 'position' : 'bottom'});
					panneau.togglePanneau();
				}
				else if (!$scope.panneau.estAffiche()){
					$scope.panneau.togglePanneau();
				}
				//On rajoute la classe sticky, qu'on enlève quelque seconde après.
				//Pour éviter que le panneau ne disparaisse si l'utilisateur n'était pas en hover dessus
				$scope.panneau.view.addClass("sticky");
				setTimeout(function () {
					$scope.panneau.view.removeClass("sticky");
				}, 4000);
			});

				// body...

			if (highlight)
				$scope.highlight(highlight, 200, true);
			ga('send', 'event', 'messages_seen', 'grain', $scope.utilisateur_local.id_utilisateur ? $scope.utilisateur_local.username : "anonymous");
		}
		$scope.afficherMessagesTransversal = function (idMessToFocus, idUser){
			//Inutile
			//var post = JSON.stringify(array_messages[focusedItem.data("nom_tag")][focusedItem.data("num_occurence")]['messages']);
			var messages_array_before_sort = {};

			$scope.nom_tag = "*";
			$scope.num_occurence = "*";
			$scope.uid_page = "*";
			$scope.uid_oa = "*";

			PF.globals.display_type = "normal";

			//On vérifie que l'utilisateur ne commente pas sur un sous menu s'il ne visionne pas les messages de la ressource
			// if(PF.pageIsMenu() && focusedItem.attr('id') !== PF.globals.selectorRes.split("#")[1])
			// {
			// 	toastr.pop('error','web_label_erreur_commenter_sous_menu');
			// 	return false;
			// }

			if (typeof(idUser) != "undefined")
			{
				PF.globals.display_type = "user";
				$scope.id_utilisateur = idUser;
				// recupererMessagesRessourceWithoutLanguageFilter();
			}
			else{
				PF.globals.display_type = "transversal";
			}

			//Tableau de tous les messages de la page
			// var messages_page =JSON.parse(localStorage.array_messages);
			//Tableau de tous les messages de toutes les autres pages
			var messages_pages = $localStorage.array_messages[PF.globals.capsule.id_capsule];
			var messages_array_before_sort = [];

			$.each(messages_pages, function (page, messages_of_pages) {
				if(page != "_timestamp_last_message")
					messages_array_before_sort = messages_array_before_sort.concat(messages_of_pages);
			});
			
			
			ga('send', 'event', 'messages_seen', 'transversal', $scope.utilisateur_local.id_utilisateur ? $scope.utilisateur_local.username : "anonymous");
		

			$scope.messages_array = messages_array_before_sort || [];
			// $scope.messages_array = messagesFilter(messages_array_before_sort, $scope) || [];

			//Transversal
			$scope.transversal = true;


			// $scope.langueApp = LanguageM.langueApp;

			$scope.safeApply(function (argument) {

				if (!$scope.panneau) {
					var panneau = new Panneau({'nom_controller' : 'controller_messages', 'position' : 'bottom'});
					panneau.togglePanneau();
				}
				else if (!$scope.panneau.estAffiche()){
					$scope.panneau.togglePanneau();
				}
				//On rajoute la classe sticky, qu'on enlève quelque seconde après.
				//Pour éviter que le panneau ne disparaisse si l'utilisateur n'était pas en hover dessus
				$scope.panneau.view.addClass("sticky");
				setTimeout(function () {
					$scope.panneau.view.removeClass("sticky");
				}, 4000);
			});

			// $scope.$apply();

			if (typeof(idMessToFocus) != "undefined")
			{
				$scope.highlight(idMessToFocus, 200, true);
			}
			$('.focusedItem').removeClass('focusedItem');
		}

		$scope.supprimer = function (message, definitif) {
			var params = {'id_message' : message.id_message.toString(),
								'supprime_par' : message.supprime_par.toString(),
								'id_user_op' : message.id_auteur.toString(),
								'id_rank_user_op' : message.id_role_auteur.toString(),
								'id_user_moderator' : $scope.utilisateur_local.id_utilisateur,
								'id_rank_user_moderator' : $scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie,
								'id_capsule' : PF.globals.capsule.id_capsule};

			//Si la suppression est définitive
			if (typeof(definitif) != "undefined"){
				//Si oui, on cast definitif en int et on le rajoute aux paramètres
				params['supprime_def'] = +definitif;
			
				toaster.pop('success','web_label_msg_supprime_definitivement');
				var index = $localStorage.array_messages[message.id_capsule][message.nom_page].indexOf(message);
				$localStorage.array_messages[message.id_capsule][message.nom_page].splice(index, 1);
			}
			else{
				if(message.supprime_par == "0")
				{
					//On set le data et on ajoute la classe
					message.supprime_par = $scope.utilisateur_local.id_utilisateur;
					toaster.pop('success','web_label_msg_supprimer_avec_succes');
				}
				else
				{
					//Si le message a été supprimé par quelqu'un d'autre, et que l'utilisateur n'a pas au moins un petit coeur (<3) d'animateur
					if (($scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie <3) && (message.supprime_par != $scope.utilisateur_local.id_utilisateur)) {
						//On empêche la réactivation
						return toaster.pop('success','web_label_avoir_rang_eleve');
					}
					//Sinon c'est cool
					else{
						//On set le data et on ajoute la classe
						message.supprime_par = "0";
						toaster.pop('success','web_label_msg_retabli_avec_succes');
					}
				}
			}

			PF.post("message/supprimer", params, function(data){
			   
			   var retour = data;

				if(retour['status'] == "ok")
				{
					//Si la suppression est définitive
					
				}
				else
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop('error',retour['message']);

					if (typeof(definitif) != "undefined"){
						$localStorage.array_messages[message.id_capsule][message.nom_page].push(message);
					}
					else{
						//On set le data et on ajoute la classe
						if(message.supprime_par == "0")
							message.supprime_par = $scope.utilisateur_local.id_utilisateur;
						//On set le data et on ajoute la classe
						else
							message.supprime_par = "0";
					}
				}
			});
		}
		$scope.donnerMedaille = function (message, type_medaille) {
			var post = {'id_message' : message.id_message};

			//Pour ne pas garder de référence
			var _old_medaille = ""+message.medaille;
			
			if (type_medaille != "enlever"){
				post.type_medaille = type_medaille;
				message.medaille = type_medaille;
			}
			else
				message.medaille = "";


			PF.post('message/donnerMedaille',post, function (retour) {
				if (retour['status'] == "ko") 
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else 
						toaster.pop('error', retour['message']);

					message.medaille = _old_medaille;
				}
			});
		}

		$scope.ajouterTags = function (message, tags) {
			var tags_array = tags.replace(/\s/g, "").split(",");
			var post = {"id_message" : message.id_message, "tags" : tags_array};
			// $scope.fonctionPasDisponible();
			PF.post('message/tags',post, function (retour) {
				if (retour['status'] == "ko") 
				{
					toaster.pop('error', retour['message']);
				}
				else if (retour['status'] == "up") {
					var tags_to_merge = retour['data'];
					toaster.pop('success', "Tags ajoutés avec succès.");
					//Rajout des tags dans le message
					var old_tags = JSON.parse(message.tags);
					message.tags = JSON.stringify(old_tags.concat(tags_to_merge));
				}
				else if (retour['status'] == "ok") {
					toaster.pop('note', "Aucune modification.");
				}
			});
		}
		$scope.supprimerTag = function (message, tag) {
			var post = {"id_message" : message.id_message, "tag" : tag};
			// $scope.fonctionPasDisponible();
			PF.delete('message/tag', post, function (retour) {
				if (retour['status'] == "ko") 
				{
					toaster.pop('error', retour['message']);
				}
				else if (retour['status'] == "ok") {
					toaster.pop('success', "Tags supprimé.");
					//Rajout des tags dans le message
					var old_tags = JSON.parse(message.tags),
						index = old_tags.indexOf(tag);
					old_tags.splice(index, 1);
					message.tags = JSON.stringify(old_tags);
				}
			});
		}
		$scope.validerMessageDefi = function (message) {
			var old_value = message.defi_valide;

			if (old_value == "0") {
				message.defi_valide = "1";
			}
			else{
				message.defi_valide = "0";	
			}
			PF.post('message/defi/valider', {"id_message" : message.id_message, "defi_valide" : message.defi_valide}, function (retour) {
				if (retour['status'] == "ko") 
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						toaster.pop('error', "web_label_erreur_contacter_pairform");

					message.defi_valide = old_value;
				}
			});
		}
		$scope.terminerDefi = function (message) {
			var old_value = message.est_defi;
			//On reverifie bien que le message est un défi
			if (old_value == "1") {
				message.est_defi = "2";

				PF.post('message/defi/terminer',{"id_message" : message.id_message, "id_capsule" : $scope.capsule.id_capsule}, function (retour) {
					if (retour['status'] == "ko") 
					{
						if (retour['message'] == "utilisateur_invalide") {
							$scope.deconnecter("web_label_session_expiree");
						}
						else 
							toaster.pop('error', "web_label_erreur_contacter_pairform");
						message.est_defi = old_value;
					}
				});
			}	
		}
		$scope.partagerMessage = function (message) {
			if($scope.visible_menu_partager == message.id_message)
				return $scope.visible_menu_partager = undefined;

			$scope.visible_menu_partager = message.id_message;
			$scope.message_permalien = $scope.res_url_root + 'webServices/message/rediriger?id_message=' + message.id_message;
			
			//timeout, pour qu'angular ait le temps de créer le menu
			setTimeout(function () {
				var text = document.querySelector("pf-menu-partager span"), 
					range, 
					selection;

			    if (document.body.createTextRange) {
			        range = document.body.createTextRange();
			        range.moveToElementText(text);
			        range.select();
			    } else if (window.getSelection) {
			        selection = window.getSelection();        
			        range = document.createRange();
			        range.selectNodeContents(text);
			        selection.removeAllRanges();
			        selection.addRange(range);
			    }
			}, 250);
		}

		$scope.deplacerMessage = function (message) {

			//Récupération des réponses du messages s'il y en a
			if (!$localStorage.array_messages_deplacement || !$localStorage.array_messages_deplacement.length) {
				$localStorage.array_messages_deplacement = [message];	
			}
			else{
				var index = -1;

				$localStorage.array_messages_deplacement.some(function (msg, idx, arr) {
					if (msg.id_message == message.id_message)
						return index = idx;
				});
				if (index === -1)
					$localStorage.array_messages_deplacement.push(message);
				else
					$localStorage.array_messages_deplacement.splice(index, 1);
			}
			//On fait pointer une variable globale a tous les controllers sur le local storage,
			//Pour pouvoir watcher ca dans le controller nav_bar
			$scope.$root.array_messages_deplacement = $localStorage.array_messages_deplacement;
		}

		$scope.ajouterReseauVisibilite = function (reseau) {
			if ($scope.nouveau.liste_reseaux.indexOf(reseau) == -1)
				$scope.nouveau.liste_reseaux.push(reseau);
		}
		$scope.enleverReseauVisibilite = function (reseau) {
			var index = $scope.nouveau.liste_reseaux.indexOf(reseau);
			$scope.nouveau.liste_reseaux.splice(index, 1);
		}
		$scope.highlight = function(idMessToFocus, delay, bounce){
			bounce = bounce || false;
			delay = delay || 0;
			if (idMessToFocus.length)
			{
				$timeout(function () {
					var comBoxToFocus = $('pf-message').filter(function  () {
						return angular.element(this).data('id_message') == idMessToFocus;
					});
					if (comBoxToFocus.length){
						$('#comBar').animate({ 
							scrollTop: comBoxToFocus.position().top - 145
						}, 1000, "swing", function () {
							if (bounce) {
								comBoxToFocus.addClass('highlighted');
								setTimeout(function () {
									comBoxToFocus.removeClass('highlighted');
								},1000);
							}
						});
					}
				},delay);
			}
		}
		$scope.linkify = function (string)
		{
			var pattern = '(?xi)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))';
			return string.replace("#"+pattern+"#i", function(matches) {
				var input = matches[0];
				var url = preg_match('!^https?://!i', input) ? input : "http://" + input;
				return '<a href="' + url + '" rel="nofollow" target="_blank">' + input +"</a>";
			}); 
		}
		$scope.changerOrdreTri = function (){
			var current_index = $scope.sort_types.indexOf($scope.sort_type);
			if (current_index + 1 >= $scope.sort_types.length){
				$scope.sort_type = $scope.sort_types[0];
				$scope.next_sort_type = $scope.sort_types[1];
			}
			else {
				$scope.sort_type = $scope.sort_types[current_index + 1];

				if (current_index + 2 >= $scope.sort_types.length)
					$scope.next_sort_type = $scope.sort_types[0];
				else
					$scope.next_sort_type = $scope.sort_types[current_index + 2];
			}
		}
		if(!$scope.utilisateur_local.preferences)
			$scope.utilisateur_local.preferences = {"expanded" : false};

		$scope.expanded = $scope.utilisateur_local.preferences.expanded || false;
		$scope.changerAffichage = function (){
			$scope.expanded = !$scope.expanded;
			$scope.utilisateur_local.preferences.expanded = $scope.expanded;
			document.querySelector("#comBar").style.cssText = "";
		}
		$scope.parseJSON = function (json) {
			return JSON.parse(json);
		}
	}]);
	

	pf_app.controller('controller_profil', ["$scope", "$http", "$translate", "toaster", function ($scope, $http, $translate, toaster) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"upgrade_ressource" : false,
			"is_connected_user" : false,
			"utilisateur" : {}
		};

		angular.extend($scope, defaults);

		$scope.init = function () {
			//Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
			if ($scope.loaded != $scope.utilisateur.id_utilisateur) {
				var _scope = $scope;
				PF.get('utilisateur/profil', {"id_utilisateur": $scope.utilisateur.id_utilisateur} , function(retour, status, headers, config) {
					var datas = retour['datas'];
					datas.id_utilisateur = $scope.utilisateur.id_utilisateur;
					var utilisateur = new Utilisateur(datas);

					angular.extend(_scope.utilisateur, utilisateur);
					_scope.loaded = _scope.utilisateur.id_utilisateur;
					_scope.is_connected_user = (_scope.utilisateur.id_utilisateur == _scope.utilisateur_local.id_utilisateur);
					_scope.determinerDerniereConnexion();
					// var post = {'id_utilisateur':id_user, 'profil': retour, 'is_connected_user': is_connected_user, 'langueApp': LanguageM.langueApp};

					
				});
			}
		}

		$scope.determinerDerniereConnexion = function  () {
			var lb_st = $scope.labelStatus;
			var t_off = $scope.utilisateur.offline;
			$translate(['web_label_en_ligne', 'web_label_inactif_depuis', 'web_label_minutes', 'web_label_heures', 'web_label_jours', 'web_label_mois']).then(function (translation_object) {
				lb_st = translation_object['web_label_en_ligne'];
				if (t_off != false) {

					if (t_off < 60) {
						t_off = Math.round(t_off);
						lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_minutes'];
					}
					else if((t_off > 60) && (t_off < 1440)){
						t_off = Math.round(t_off / 60);
						lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_heures'];
					}
					else if((t_off > 1440) && (t_off < 43200)){
						t_off = Math.round(t_off / 1440);
						lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_jours'];
					}
					else if((t_off > 43200) && (t_off < 20000000)){
						t_off = Math.round(t_off / 43200);
						lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_mois'];
					}
					else if (t_off > 20000000) {
						lb_st = translation_object['web_label_inactif'];
					}
				}
				$scope.offline = t_off;
				$scope.labelStatus = lb_st;
			});
		}

		$scope.changerRoleUtilisateur = function (categorie_ressource, id_categorie_temp, nom_categorie_temp) {
			PF.post('utilisateur/role', {"id_utilisateur_cible": $scope.utilisateur.id_utilisateur, "id_ressource": categorie_ressource.id_ressource, "id_categorie_temp": id_categorie_temp} , function(retour, status, headers, config) {
				if (retour["status"] == "ok"){
					categorie_ressource.id_categorie = retour["data"]["id_categorie"];
					categorie_ressource.nom_categorie = retour["data"]["nom_categorie"];
					$scope.upgrade_ressource = false;
				}
				else
					toaster.pop("error", "web_label_erreur_contacter_pairform");
			});
		}

	}]);


	pf_app.controller('controller_profil_config', ["$scope", "toaster", function ($scope, toaster) {
		var defaults = {
			"visible" : false,
			"tableau_langues_label" : PF.langue.tableau_labels,
			"informations" : {
				"current_password" : "",
				"password" : "",
				"password2" : "",
				"name" : "",
				"etablissement" : "",
				"langue" : "3"
			}
		};
		angular.extend($scope, defaults);

		$scope.init = function () {
			$scope.informations.name = $scope.utilisateur.name;
			$scope.informations.etablissement = $scope.utilisateur.etablissement;
			$scope.informations.langue = $scope.utilisateur.id_langue;
		}

		$scope.enregistrerModifications = function ()  {
			var _scope = $scope;

			//Si un des champs est rempli
			if(typeof($scope.informations.current_password) != "undefined" 
				|| typeof($scope.informations.password) != "undefined" 
				|| typeof($scope.informations.password2) != "undefined")
			{
				//Il faut absolument que tous les champs soient remplis
				if(typeof($scope.informations.current_password) == "undefined" 
				|| typeof($scope.informations.password) == "undefined" 
				|| typeof($scope.informations.password2) == "undefined")
				{
					//Sinon, return
					return toaster.pop('error', "web_label_renseigner_champ");
				}
			}
			//Si ça roule
			PF.post('utilisateur/editer', $scope.informations, function(retour, status, headers, config) {
				if (retour['status'] == 'ok'){
					_scope.utilisateur_local.name = _scope.informations.name;
					_scope.utilisateur_local.etablissement = _scope.informations.etablissement;
					_scope.utilisateur_local.langue = _scope.informations.langue;
					_scope.panneau.togglePanneau();
					toaster.pop('success', "web_label_info_modifiee_avec_succes");
				}
				//Ajout refusé
				else
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						//Display de l'erreur correspondante
						toaster.pop('error', retour['message']);
				}
			});

		}

	}]);

	pf_app.controller('controller_recherche_profil', ["$scope", "$http", function ($scope, $http) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"mode" : 'recherche_profil',
			"utilisateurs_array" : []
		};
		angular.extend($scope, defaults);

		$scope.init = function () {
			if (!$scope.loaded) {
				var _scope = $scope;
				PF.get('utilisateur/liste', {}, function(data, status, headers, config) {
					var retour = data;
					_scope.utilisateurs_array = retour['profils'];
					_scope.loaded = true;
				});
			}
		}
		$scope.ajouterUtilisateurDansReseauDepuisRecherche = function (utilisateur) {
			PF.getScopeOfController('controller_reseau').ajouterUtilisateurDansReseauDepuisRecherche(utilisateur);
		}
	}]);

	pf_app.controller('controller_reseau', ["$scope", "toaster", function ($scope, toaster) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"mode" : "gestion", //ajout || usage
			"sous_mode" : false,
			"cercles":[],
			"classes":[],
			"classesRejointes":[],
			"utilisateur_ajout" : undefined,
			"reseau_selected" : undefined
		};

		angular.extend($scope, defaults);

		var resParDomaine = {};
		var resParEtablissement = {};

		$scope.init = function () {
			if ($scope.loaded != $scope.id_utilisateur) {
				var _scope = $scope;
				PF.get('reseau/liste', {}, function(data, status, headers, config) {
					var retour = data;	
					if (retour["status"] == "ok") {
						_scope.loaded = _scope.id_utilisateur;
						_scope.cercles = retour['cercles'];
						_scope.classes = retour['classes'];
						_scope.classesRejointes = retour['classesRejointes'];
					}
					else{
						if (retour['message'] == "utilisateur_invalide") {
							$scope.deconnecter("web_label_session_expiree");
						}
					}
				});
			}
		}
		$scope.ajouterReseau = function (type) {
			if (!$scope.utilisateur_local.est_connecte)
				return;

			//Récupération du nom de la collection
			var name = $scope.nouveau_reseau;

			//Si champ vide
			if ($.trim(name) == ''){
				toaster.pop('error', 'web_label_erreur_renseigner_nom');
				return;
			}

			var post = {'nom' : name};

			var message = 'web_label_votre ' + type + ' web_label_creee_avec_succes';
			var ws = '',
				post,
				nouveau_reseau,
				array_reseaux;

			if (type == "classe"){
				nouveau_reseau = new Classe({'nom' : name});
				array_reseaux = $scope.classes;
				ws = 'reseau/classe';
				//Si on est pas au moins expert
				if ($scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie < 4){
					//On saute
					toaster.pop('error', 'web_label_erreur_role_creer_classe');
					return;
				}
			}
			else if (type == "cercle"){
				nouveau_reseau = new Reseau({'nom' : name});
				array_reseaux = $scope.cercles;
				ws = 'reseau/cercle';
			}
			//Si on est dans le cas d'un élève qui rejoint une classe
			else if (type == "classeRejoindre"){
				nouveau_reseau = new Classe({'nom' : name});
				array_reseaux = $scope.classesRejointes;
				//On change l'intitulé du paramètre et le WS
				post.cle = name;
				message = 'web_label_classe_rejointe';
				ws = 'reseau/classe/utilisateur';
			}
			//Ajout dans le tableau
			array_reseaux.splice(0,0,nouveau_reseau);
			//Notif
			// toaster.pop('success', message);

			ga('send', 'event', 'user', 'create_network', $scope.utilisateur_local.id_utilisateur);
			//Envoi de la requête d'ajout
			PF.put(ws, post, function (retour) {
				//Ajout validé
				if (retour['status'] == 'ok'){
					//Update de l'affichage & notification
					nouveau_reseau.id_collection = retour['id_collection'];
					//Si on est dans le cas d'une classe
					if(type != "cercle"){
						//Ajout de la clé
						nouveau_reseau.cle = retour['cle'];
					}
				}
				//Ajout refusé
				else
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						//Display de l'erreur correspondante
						toaster.pop('error', retour['message']);

					//Suppression dans le tableau
					array_reseaux.splice(0,1);
				}
			});
		}
		$scope.afficherFormReseau = function (type_reseau) {
			$scope.sous_mode = type_reseau;
			
		}
		$scope.utiliserReseau = function (reseau) {
			PF.getScopeOfController('controller_messages').ajouterReseauVisibilite(reseau);
			
		}
		$scope.ajouterUtilisateurDansReseauDepuisReseau = function (reseau) {
			if ($scope.utilisateur_ajout)
				$scope.ajouterUtilisateurDansReseau(reseau, $scope.utilisateur_ajout);
			else
				toaster.pop("error", "Pas d'utilisateur sélectionné");

		}
		$scope.ajouterUtilisateurDansReseauDepuisRecherche = function (utilisateur) {
			if ($scope.reseau_selected)
				$scope.ajouterUtilisateurDansReseau($scope.reseau_selected, utilisateur);
			else
				toaster.pop("error", 'Pas de réseau sélectionné');
		}
		$scope.ajouterUtilisateurDansReseau = function (reseau, utilisateur) {
			//S'il est déjà dedans, on coupe court
			if (reseau.profils.map(function(e){return e.id_utilisateur.toString();}).indexOf(utilisateur.id_utilisateur) >= 0) {
				//Display de l'erreur correspondante
				return toaster.pop('error', 'web_label_erreur_ajout_reseau_deja_existant');

			}
			//Envoi de la requête d'ajout
			var post = {'id_collection' : reseau.id_collection, 'id_utilisateur_concerne' : utilisateur.id_utilisateur};
			reseau.profils.splice(0,0,utilisateur);
			ga('send', 'event', 'user', 'add_to_network', $scope.utilisateur_local.id_utilisateur);

			PF.put('reseau/utilisateur', post,function (retour) {		
				if (retour['status'] == 'ok'){
					//Update de l'affichage & notification
					toaster.pop('success', 'web_label_utilisateur_ajouté_avec_succes');

				}
				//Ajout refusé
				else
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						//Display de l'erreur correspondante
						toaster.pop('error', retour['message']);

					reseau.profils.splice(0,1);
				}
			});
		}
		$scope.selectionUtilisateurPourReseau = function (reseau) {
			$scope.afficherRecherche('reseau');
			$scope.reseau_selected = reseau;
		}
		$scope.supprimerUtilisateurReseau = function (utilisateur, reseau) {
			var post = {'id_collection' : reseau.id_collection, 'id_utilisateur_concerne' : utilisateur.id_utilisateur};
			var index = reseau.profils.indexOf(utilisateur);
			reseau.profils.splice(index, 1);

			PF.delete('reseau/utilisateur', post,function (retour) {		
				if (retour['status'] == 'ok'){
					//Update de l'affichage & notification
					toaster.pop('success', 'web_label_utilisateur_sup_reseau');
					
				}
				//Ajout refusé
				else
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						//Display de l'erreur correspondante
						toaster.pop('error', retour['message']);

					reseau.profils.splice(index,0,reseau);
				}
			});
		}
		$scope.supprimerReseau = function (reseau, array_reseaux) {
			var post = {'id_collection': reseau.id_collection};
			var index = array_reseaux.indexOf(reseau);
			array_reseaux.splice(index, 1);

			PF.delete('reseau', post,function (retour) {		
				if (retour['status'] == 'ok'){
					//Update de l'affichage & notification
					toaster.pop('success', 'web_label_sup_reseau');
				}
				//Ajout refusé
				else
				{
					if (retour['message'] == "utilisateur_invalide") {
						$scope.deconnecter("web_label_session_expiree");
					}
					else
						//Display de l'erreur correspondante
						toaster.pop('error', retour['message']);

					array_reseaux.splice(index,0,reseau);
				}
			});
		}
	}]);


	pf_app.controller('controller_ressources', ["$scope", "$http", "toaster", function ($scope, $http, toaster) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"switch_sort_mode" : 0
		};
		angular.extend($scope, defaults);

		var resParDomaine = {};
		var resParEspace = {};

		$scope.init = function () {
			if (!$scope.loaded) {
				var _scope = $scope;
				PF.get('ressource/liste', {}, function(data, status, headers, config) {
					var retour = data;
					// S'il y a des ressources
					if (retour['status'] == 'ok')
					{
						var ressources = retour['ressources'];
						//On va chercher le nombre de messages / On enlève le & du début de post_OS
						_scope.ressources = ressources;
						//Tri des ressources, on commence à afficher
						_scope.sortRessources();
						//Parallèllement, on chope le nombre de messages pour les afficher sur les ressources
						PF.get("message/nombre", {"id_utilisateur" : $scope.utilisateur_local.id_utilisateur, "langues_affichage" : [1,2,3]}, function(retour, status, headers, config){
						
							if (retour['status'] == "ok")
							{
								_scope.nombre_messages = retour['nombre_messages'];
								// var postLoad = {'messages' : messages, 'ressources' : ressources, 'langueApp': LanguageM.langueApp };
							   	
							}
						});
						_scope.loaded = true;
					}
				});
			}
		}

		$scope.sortRessources = function () {

			resParDomaine = {};
			resParEspace = {};
			angular.forEach($scope.ressources,function (ressource) {
				ressource.url_logo = PF.globals.url.root + ressource.url_logo;
				if(typeof(resParEspace[ressource.espace_nom_court]) == "undefined")
				{
					resParEspace[ressource.espace_nom_court] = [];
				}
				resParEspace[ressource.espace_nom_court].push(ressource);

				if(typeof(resParDomaine[ressource.theme]) == "undefined")
				{
					resParDomaine[ressource.theme] = [];
				}
				resParDomaine[ressource.theme].push(ressource);
				
			});

			$scope.switch_sort_mode = 0;
			$scope.res_sorted = [];
			$scope.res_sorted[0] = resParEspace;
			$scope.res_sorted[1] = resParDomaine;
		}
		
	}]);

	//Obligé d'utiliser un bloc run, au cas ou angular n'a pas fini de bootstrapper
	//On passe le localStorage injecté en référence aux deux fonctions ci-dessous
	//Parce qu'elles ne seront pas forcément capable de récuperer l'injector() de l'app
	pf_app.run(["$localStorage", "$compile", function ($localStorage, $compile) {
		// Obligé de mettre un mini-time out, je sais pas pourquoi.
		setTimeout(function () {
			Message.recupererMessages(false, undefined, $localStorage);
			Message.contextualiserMessage($compile);
		},50);
	}]);

	angular.element(document).ready(function() {
		PF.init();
		angular.bootstrap(document, ['pf_app']);
		$('div[ng-controller=main_controller]').fadeIn(200);
		outdatedBrowser({
			bgColor: '#f25648',
			color: '#ffffff',
			lowerThan: 'transform',
			languagePath: PF.globals.url.public + 'js/outdated-browser-1.1.0/lang/fr.html'
		});

		// Message.recupererMessages();
		// Message.contextualiserMessage();
	});
});