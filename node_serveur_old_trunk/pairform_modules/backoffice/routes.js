var express = require("express");


module.exports = function(app, passport) {

	app.get("/backoffice", function (req, res) {
		res.render(__dirname + "/views/index", {utilisateur: req.user ? req.user : "-1"});
	});

	app.get("/backoffice/utilisateur/connexion", function (req, res) {
		res.render(__dirname + "/views/pages/connexion");
	});

	app.get("/backoffice/:branche/:nom_view", function (req, res) {
		res.render(__dirname + "/views/pages/" + req.params.nom_view);
	});
}
