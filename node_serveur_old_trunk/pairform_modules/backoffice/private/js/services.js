"use strict";

/*
 * Services utilisés dans le backoffice
 */
angular.module("backofficeServices", [])

	// service conservant les informations sur l'utilisateur connecté
	.service("connexion", function() {
		var utilisateur_connecte;

		return {
			setUtilisateurConnecte: function(utilisateur_connecte) {
				this.utilisateur_connecte = utilisateur_connecte;
			},
			getUtilisateurConnecte: function() {
				return this.utilisateur_connecte;
			}
		};
	});