"use strict";


/*
 * Controleurs des statistiques des ressources
 */



// Page d'édition d'une ressource
backoffice_controlleurs.controller("controleurStatistiquesRessource", ["$scope", "$http", "$routeParams", "$translate", function($scope, $http, $routeParams, $translate){
	var erreur_requete;

	// traduction des labels de la page
	$translate('LABEL.STATISTIQUE.STATISTIQUES').then(function (traduction) {
		$scope.titre = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;

	// récupération des statistiques sur la ressource
	$http.get("webServices/statistiques/espace/"+ $routeParams.id_espace +"/ressource/"+ $routeParams.id_ressource +"/"+ $routeParams.ressource_cree_par)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.statistiques_ressource = retour_json.statistiques_ressource;
			} else {
				toastr.error(erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/statistiques/espace/:id_espace/ressource/:id_ressource/:ressource_cree_par");
			toastr.error(erreur_requete);
		});
}]);