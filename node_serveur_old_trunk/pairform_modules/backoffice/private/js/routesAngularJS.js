"use strict";

// ajout des modules
var app = angular.module("backoffice", [
	"ngRoute",						// module de gestion des routes
	"ngStorage",					// module de gestion du local storage
	"pascalprecht.translate",		// module internationalisation
	"angularFileUpload",			// module permettant l'upload de fichier dans les formulaires
	"backofficeFiltres",			// module des filtres
	"backofficeServices",			// module des filtres
	"backofficeControlleurs"		// module des controllers
]);

var backoffice_controlleurs = angular.module("backofficeControlleurs", []);

// position des toasts de notification
toastr.options.positionClass = "toast-bottom-right";


/*
 * déclaration des constantes
 */

var OS_BACKOFFICE = "backoffice";		// type de la plateforme - envoyé en paramètre de chaque requete aux webservices PairForm
var VERSION_BACKOFFICE = "0.9.002"		// version courante du backoffice (cf. package.json) - envoyé en paramètre de chaque requete aux webservices PairForm
var RETOUR_JSON_OK = "ok";				// Status d'une requete JSON s'étant bien déroulée
var RETOUR_JSON_KO = "ko";				// Status d'une requete JSON s'étant mal déroulée (erreur SQL, connexion internet suspendu, utilisateur n'ayant pas les droits sur l'action, etc.)
var ERREUR_DOUBLON = "ER_DUP_ENTRY"									// code d'erreur correspondant à un champ déjà existant en BDD (ex : nom d'un espace)
var ERREUR_UTILISATEUR_NON_CONNECTE = "utilisateur_invalide";		// code d'erreur correspondant à un couple login / mot de passe inexistant lors de la connexion
var ERREUR_UTILISATEUR_NON_AUTORISE = "utilisateur_non_autorise";	// code d'erreur correspondant à une action non autorisé pour un utilisateur
var ERREUR_EMAIL_INEXISTANT = "Cet utilisateur / e-mail n'existe pas : veuillez réessayer."		// code d'erreur correspondant à une reinitialisation de mot de passe via un email inexistant  
var ARCHIVES_ZIP_TYPES = [											// liste des types possible pour une archive zip			
	"application/zip", 
	"application/x-zip", 
	"application/x-zip-compressed", 
	"application/x-compressed"
];
var IMG_DATA_TYPE = "data:image/png;base64,";						// informations préfixant l'attribut "src" des balises html "img" - permet l'affichage d'une image à partir de son encodage en base64
var DIMENSION_LOGO_ESPACE = 400;									// Dimensions (longueur et largeur) de l'image correspondant au logo de l'espace
var DIMENSION_LOGO_RESSOURCE = 100;									// Dimensions (longueur et largeur) de l'image correspondant au logo de la ressource

// liste des droits existants dans le Backoffice 
// Rq commentaires : cf. fichier webservices/lib/constantes.js
var ADMIN_ESPACE = "admin_espace";
var EDITER_ESPACE = "editer_espace";
var GERER_GROUPES = "gerer_groupes";
var GERER_ROLES = "gerer_roles";
var ATTRIBUER_ROLES = "attribuer_roles";
var GERER_RESSOURCES_ET_CAPSULES = "gerer_ressources_et_capsules";
var EDITER_TOUTES_RESSOURCES_ET_CAPSULES = "editer_toutes_ressources_et_capsules";
var GERER_VISIBILITE_CAPSULE = "gerer_visibilite_capsule";
var GERER_VISIBILITE_TOUTES_CAPSULES = "gerer_visibilite_toutes_capsules";

// Regex
var REGEX_EMAIL = /^[a-zA-Z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/;
var REGEX_NOM_DE_DOMAINE = /^[a-z0-9._-]+\.[a-z]{2,6}$/;

// Visibilités possibles d'une capsule pour un groupe
var VISIBILITE_ACCESSIBLE = 1;
var VISIBILITE_INACCESSIBLE = 2;
var VISIBILITE_INVISIBLE = 3;


/*
 * Internationalisation
 */
app.config(["$translateProvider", function ($translateProvider) {
	$translateProvider.useStaticFilesLoader({
    	prefix: 'private/json/backofficeLangue_',
    	suffix: '.json'
	});

	$translateProvider.preferredLanguage("fr");
}]);

/*
 * gestion de l"association routes - templates
 */

// vérifie si l'utilisateur est connecté ou non
var utilisateurEstConnecte = function($q, $timeout, $location, $rootScope, connexion) {
	var deferred = $q.defer();

	// quand l'utilisateur n'est pas connecte, window.utilisateur est initialisé avec "-1"
	// si l'utilisateur est connecté
	if( window.utilisateur != "-1" || connexion.getUtilisateurConnecte() ) {
		var utilisateur_connecte = connexion.getUtilisateurConnecte() ? connexion.getUtilisateurConnecte() : window.utilisateur;

		// si l'utilisateur connecté est un admin PairForm ou s'il a au moins 1 rôle dans le Backoffice
		if (utilisateur_connecte.est_admin_pairform || Object.keys(utilisateur_connecte.liste_roles).length > 0) {
			if (!connexion.getUtilisateurConnecte()) {
				connexion.setUtilisateurConnecte(window.utilisateur);	// mise à jour de l'utilisateur connecté pour que ses infos soient accessible dans l'application
				$rootScope.$broadcast("connexion");						// broadcast d'un evenement pour mettre à jour l'affichage avec les infos sur l'utilisateur connecté (ex: le menu)
			}
			$timeout(deferred.resolve, 0);
		} else {
			// sinon, on redirige vers l'écran de connexion
			connexion.setUtilisateurConnecte(null);
			$timeout(deferred.reject, 0);
			$location.path("/utilisateur/connexion").search({});		// redirection vers l'écran de connexion, search({}) permet de supprimer les paramètres stockés dans l'URL
		}
	} else {
		// sinon, on redirige vers l'écran de connexion
		$timeout(deferred.reject, 0);
		$location.path("/utilisateur/connexion").search({});			// redirection vers l'écran de connexion, search({}) permet de supprimer les paramètres stockés dans l'URL
	}

	return deferred.promise;
};

app.config(["$routeProvider", function($routeProvider) {
	$routeProvider
		// tableau de bord
		.when("/tableauDeBord", {
			templateUrl: "backoffice/tableauDeBord/tableauDeBord",
			controller: "controleurTableauDeBord",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})

		// utilisateur
		.when("/utilisateur/connexion", {
			templateUrl: "backoffice/utilisateur/connexion",
			controller: "controleurConnexion"
		})

		// espaces
		.when("/espaces/creationEspace", {
			templateUrl: "backoffice/espaces/gestionEspace",
			controller: "controleurCreationEspace",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})
		.when("/espaces/gestionEspace/:id_espace", {
			templateUrl: "backoffice/espaces/gestionEspace",
			controller: "controleurGestionEspace",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})

		// ressources
		.when("/ressources/creationRessource/:id_espace", {
			templateUrl: "backoffice/ressources/gestionRessource",
			controller: "controleurCreationRessource",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})
		.when("/ressources/gestionRessource/:id_ressource", {
			templateUrl: "backoffice/ressources/gestionRessource",
			controller: "controleurGestionRessource",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})

		// capsules
		.when("/capsules/creationCapsule/:id_ressource", {
			templateUrl: "backoffice/capsules/gestionCapsule",
			controller: "controleurCreationCapsule",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})
		.when("/capsules/gestionCapsule/:id_capsule", {
			templateUrl: "backoffice/capsules/gestionCapsule",
			controller: "controleurGestionCapsule",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})
		.when("/capsules/gestionVisibiliteCapsule/:id_capsule", {
			templateUrl: "backoffice/capsules/gestionVisibiliteCapsule",
			controller: "controleurGestionVisibiliteCapsule",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})

		// groupes
		.when("/groupes/gestionGroupes/:id_espace", {
			templateUrl: "backoffice/groupes/gestionGroupes",
			controller: "controleurGestionGroupes",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})

		// rôles
		.when("/roles/gestionRoles/:id_espace", {
			templateUrl: "backoffice/roles/gestionRoles",
			controller: "controleurGestionRoles",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})
		.when("/roles/attributionRoles/:id_espace", {
			templateUrl: "backoffice/roles/attributionRoles",
			controller: "controleurAttributionRoles",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})

		// statistiques
		.when("/statistiques/statistiquesRessource/:id_ressource", {
			templateUrl: "backoffice/statistiques/statistiquesRessource",
			controller: "controleurStatistiquesRessource",
			resolve: {
				connected: utilisateurEstConnecte
			}
		})

		// redirection des routes inconnues vers l'accueil
		.otherwise({
			redirectTo: "/tableauDeBord",
			resolve: {
				connected: utilisateurEstConnecte
			}
		});
}]);
