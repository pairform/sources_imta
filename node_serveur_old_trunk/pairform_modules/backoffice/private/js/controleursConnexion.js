"use strict";


/*
 * Controleurs gérant la connexion
 */

// Page de connexion
backoffice_controlleurs.controller("controleurConnexion", ["$scope", "$http", "$location", "$translate", "$rootScope", "connexion", function($scope, $http, $location, $translate, $rootScope, connexion) {
	var succes_identification, succes_reinitialisation_mot_de_passe;
	var erreur_requete, erreur_acces_utilisateur_non_autorise, erreur_email_inexistant;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.IDENTIFICATION').then(function (traduction) {
		succes_identification = traduction;
	});
	$translate('LABEL.SUCCES.REINITIALISATION_MOT_DE_PASSE').then(function (traduction) {
		succes_reinitialisation_mot_de_passe = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});
	$translate('LABEL.ERREUR.ACCES_UTILISATEUR_NON_AUTORISE').then(function (traduction) {
		erreur_acces_utilisateur_non_autorise = traduction;
	});
	$translate('LABEL.ERREUR.EMAIL_INEXISTANT').then(function (traduction) {
		erreur_email_inexistant = traduction;
	});

	$scope.connecterUtilisateur = function(formulaire_valide) {
		$scope.formulaire_soumis = true;
		$scope.utilisateur_non_connecte = false;
		// si tout les champs du formulaire sont valides
		if(formulaire_valide) {
			// On envoie une demande de connexion pour l'utilisateur pour récupérer ses informations (id, droits, etc.)
			$http.post("webservices/backoffice/connexion", {
					os: OS_BACKOFFICE,
					version: VERSION_BACKOFFICE,
					username: $scope.login,
					password: window.btoa(encodeURIComponent( escape($scope.mot_de_passe) ))	// encodage base64 du mot de passe
				})
				.success(function(retour_json){
					console.log(retour_json);
					if (retour_json.status === RETOUR_JSON_OK) {
						// mise à jour de l'utilisateur connecté pour que ses infos soient accessible dans l'application
						connexion.setUtilisateurConnecte(retour_json.utilisateur_connecte);
						$location.path("/tableauDeBord");
						
						toastr.success(succes_identification);
						// broadcast d'un evenement pour mettre à jour l'affichage avec les infos sur l'utilisateur connecté (ex: le menu)
						$rootScope.$broadcast("connexion");
					} else {
						// Si le couple login / mot de passe ne correspond à aucuns utilisateurs
						if (retour_json.message === ERREUR_UTILISATEUR_NON_CONNECTE) {
							$scope.utilisateur_non_connecte = true;
							$scope.formulaire_soumis = false;
						}
						else if (retour_json.message === ERREUR_UTILISATEUR_NON_AUTORISE) {
							$scope.formulaire_soumis = false;
							toastr.error(erreur_acces_utilisateur_non_autorise);
						}
						else {
							toastr.error(erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /backoffice/connexion");
					toastr.error(erreur_requete);
				});
		}
	}

	$scope.reinitialiserMotDePasse = function(email_valide) {
		// si tout les champs du formulaire sont valides
		if(email_valide) {
			// On envoie une demande de reinitialisation du mot de passe de l'utilisateur
			$http.post("webservices/backoffice/MotDePasse/reinitialiser", {
					os: OS_BACKOFFICE,
					version: VERSION_BACKOFFICE,
					username: $scope.email
				})
				.success(function(retour_json){
					console.log(retour_json);
					if (retour_json.status === RETOUR_JSON_OK) {						
						toastr.success(succes_reinitialisation_mot_de_passe);
					} else {
						if (retour_json.message === ERREUR_EMAIL_INEXISTANT) {
							toastr.error(erreur_email_inexistant);
						}
						else {
							toastr.error(erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /backoffice/MotDePasse/reinitialiser");
					toastr.error(erreur_requete);
				});
		}
	}
}]);
