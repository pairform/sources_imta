"use strict";


/*
 * Controleurs du menu
 */

backoffice_controlleurs.controller("controlleurMenu", ["$scope", "$http", "$location", "$translate", "$localStorage", "connexion", function($scope, $http, $location, $translate, $localStorage, connexion) {
	var succes_deconnexion, erreur_requete;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.DECONNEXION').then(function (traduction) {
		succes_deconnexion = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})

	// si c'est la première connexion
	if(! $localStorage.language) {
		//Recuperation de la langue du navigateur
		var langue_courante = window.navigator.userLanguage || window.navigator.language;

		if(langue_courante.indexOf('fr') != -1) { // Detecte la langue française
			$localStorage.language = 'fr';
			$translate.use('fr');
		}
		else { // Detecte la langue anglaise (langue par défaut)
			$localStorage.language = 'en';
		}
	}
	else { // Recuperation de la langue sauvegardée dans le LocalStorage
		$translate.use($localStorage.language);
	}

	// lorsque l'event connexion est recu, on récupère l'utilisateur connecté pour mettre à jour l'affichage du menu
	$scope.$on("connexion", function(event) {
		$scope.utilisateur_connecte = connexion.getUtilisateurConnecte();
	});

	// Permet de changer de langue lors de la selection d'une langue
	$scope.changerLangue = function (nouvelle_langue) {
		$translate.use(nouvelle_langue);
		$localStorage.language = nouvelle_langue; // Mise à jour de la langue sauvegardé dans le LocalStorage
	};

	$scope.deconnecterUtilisateur = function() {
		// On envoie une demande de deconnexion de l'utilisateur
		$http.post("webservices/backoffice/deconnexion", {
				os: OS_BACKOFFICE,
				version: VERSION_BACKOFFICE
			})
			.success(function(retour_json){
				console.log(retour_json);
				if (retour_json.status === RETOUR_JSON_OK) {
					// suppression de l'utilisateur
					connexion.setUtilisateurConnecte(null);
					$scope.utilisateur_connecte = false;
					$location.path("/utilisateur/connexion").search({});		// redirection vers l'écran de connexion, search({}) permet de supprimer les paramètres stockés dans l'URL
					
					toastr.success(succes_deconnexion);
				} else {
					toastr.error(erreur_requete);
				}
			})
			.error(function(){
				console.log("erreur requete http post /backoffice/deconnexion");
				toastr.error(erreur_requete);
			});
	}
}]);
