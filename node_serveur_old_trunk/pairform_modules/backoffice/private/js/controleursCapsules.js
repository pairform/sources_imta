"use strict";


/*
 * Controleurs de gestion des capsules
 */

// Page de création d'une capsule
backoffice_controlleurs.controller("controleurCreationCapsule", ["$scope", "$http", "$routeParams", "$location", "$translate", "$timeout", "$upload", "connexion", function($scope, $http, $routeParams, $location, $translate, $timeout, $upload, connexion){
	var succes_enregistrement, erreur_requete;
	var parametres_requete;
	var archive_mobile, archive_web;
	$scope.formulaire_soumis = false;
	$scope.archive_mobile_selectionne = false;	// false tant qu'aucune archive mobile respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.archive_web_selectionne = false;		// false tant qu'aucune archive web respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.format_mobile_incorrect = false;		// false si le format de l'archive mobile de la ressource n'est pas le bon
	$scope.format_web_incorrect = false;		// false si le format de l'archive web de la ressource n'est pas le bon

	// traduction des labels de la page
	$translate('LABEL.CAPSULE.CREER').then(function (traduction) {
		$scope.titre = traduction;
	});
	$translate('LABEL.CREER').then(function (traduction) {
		$scope.bouton_formulaire = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;

	$scope.capsule = {};
	$scope.capsule.id_ressource = parseInt($routeParams.id_ressource, 10);
	
	// récupération de l'archive de la capsule (version mobile)
	$scope.recupererArchiveMobile = function($files) {
		// si le fichier n'est pas dans le format zip
		if ( ARCHIVES_ZIP_TYPES.indexOf($files[0].type) < 0 ) {
			$scope.format_mobile_incorrect = true;
			$scope.archive_mobile_selectionne = false;
		} else {
			$scope.format_mobile_incorrect = false;
			$scope.archive_mobile_selectionne = true;

			// récupération du binaire de l'archive
			var file_reader = new FileReader();

			file_reader.onload = function (e) {
				// le timeout permet d'attendre que le binaire soit chargée avant de mettre à jour à l'écran, sinon, le chargement est trop rapide
				$timeout(function () {
					$scope.nom_archive_mobile = $files[0].name;
					archive_mobile = e.target.result;
				}, 500);
			};

			file_reader.readAsDataURL($files[0]);
		}
	}

	// récupération de l'archive de la capsule (version web)
	$scope.recupererArchiveWeb = function($files) {
		// si le fichier n'est pas dans le format zip
		if ( ARCHIVES_ZIP_TYPES.indexOf($files[0].type) < 0 ) {
			$scope.format_web_incorrect = true;
			$scope.archive_web_selectionne = false;
		} else {
			$scope.format_web_incorrect = false;
			$scope.archive_web_selectionne = true;

			// récupération du binaire de l'archive
			var file_reader = new FileReader();

			file_reader.onload = function (e) {
				// le timeout permet d'attendre que le binaire soit chargée avant de mettre à jour à l'écran, sinon, le chargement est trop rapide
				$timeout(function () {
					$scope.nom_archive_web = $files[0].name;
					archive_web = e.target.result;
				}, 500);
			};

			file_reader.readAsDataURL($files[0]);
		}
	}

	$scope.gererCapsule = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			parametres_requete = $scope.capsule;
			parametres_requete.id_espace = $routeParams.id_espace;
			parametres_requete.ressource_cree_par = $routeParams.ressource_cree_par;
						
			// si une archive mobile a été séléctionné et que la checkbox demandant d'uploader l'archive est cochée
			if($scope.archive_mobile_selectionne && $scope.upload_archive_mobile) {
				parametres_requete.archive_mobile = archive_mobile.substring(archive_mobile.indexOf(","));	// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé
			}
			// si une archive web a été séléctionné et que la checkbox demandant d'uploader l'archive est cochée
			if($scope.archive_web_selectionne && $scope.upload_archive_web) {
				parametres_requete.archive_web = archive_web.substring(archive_web.indexOf(","));
			}

			// on ajoute la capsule en BDD
			$upload.http({
				url: "webServices/capsule",
				method: "PUT",
				data: parametres_requete
			}).success(function(retour_json){
				if (retour_json.status === RETOUR_JSON_OK) {
					$location.path("/tableauDeBord").search({
						id_espace: $scope.arborescence.id_espace,
						id_ressource: $scope.arborescence.id_ressource,
						id_capsule: retour_json.id_capsule
					});
					toastr.success(succes_enregistrement);
				} else {
					// SI le nom_ourt est identique à celui d'une capsule existante
					if (retour_json.message === ERREUR_DOUBLON) {
						// on indique la précesence du doublon
						$scope.capsule_doublon = true;
					} else {
						toastr.error(erreur_requete);
					}
				}
			})
			.error(function(){
				console.log("erreur requete http put /webServices/capsule");
				toastr.error(erreur_requete);
			});
		}
	}
}]);


// Page d'édition d'une capsule
backoffice_controlleurs.controller("controleurGestionCapsule", ["$scope", "$http", "$routeParams", "$location", "$translate", "$timeout", "$upload", function($scope, $http, $routeParams, $location, $translate, $timeout, $upload){
	var succes_mise_a_jour, erreur_requete;
	var parametres_requete;
	var archive_mobile, archive_web;
	var url_web_capsule;
	$scope.formulaire_soumis = false;
	$scope.archive_mobile_selectionne = false;	// false tant qu'aucune archive mobile respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.archive_web_selectionne = false;		// false tant qu'aucune archive web respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.format_mobile_incorrect = false;		// false si le format de l'archive mobile de la ressource n'est pas le bon
	$scope.format_web_incorrect = false;		// false si le format de l'archive web de la ressource n'est pas le bon

	// traduction des labels de la page
	$translate('LABEL.CAPSULE.EDITER').then(function (traduction) {
		$scope.titre = traduction;
	});
	$translate('LABEL.EDITER').then(function (traduction) {
		$scope.bouton_formulaire = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.MISE_A_JOUR').then(function (traduction) {
		succes_mise_a_jour = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})	

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;
	$scope.arborescence.nom_capsule = $routeParams.nom_capsule;
	$scope.arborescence.id_capsule = $routeParams.id_capsule;

	// récupération des informations sur la capsule à éditer
	$http.get("webServices/espace/"+ $routeParams.id_espace +"/capsule/" + $routeParams.id_capsule +"/"+ $routeParams.cree_par)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.capsule = retour_json.capsule;
				url_web_capsule = retour_json.capsule.url_web;
			} else {
				toastr.error(erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/espace/:id_espace/capsule/:id_capsule/:cree_par");
			toastr.error(erreur_requete);
		});

	// récupération de l'archive de la capsule (version mobile)
	$scope.recupererArchiveMobile = function($files) {
		// si le fichier n'est pas dans le format zip
		if ( ARCHIVES_ZIP_TYPES.indexOf($files[0].type) < 0 ) {
			$scope.format_mobile_incorrect = true;
			$scope.archive_mobile_selectionne = false;
		} else {
			$scope.format_mobile_incorrect = false;
			$scope.archive_mobile_selectionne = true;

			// récupération du binaire de l'archive
			var file_reader = new FileReader();

			file_reader.onload = function (e) {
				// le timeout permet d'attendre que le binaire soit chargée avant de mettre à jour à l'écran, sinon, le chargement est trop rapide
				$timeout(function () {
					$scope.nom_archive_mobile = $files[0].name;
					archive_mobile = e.target.result;
				}, 500);
			};

			file_reader.readAsDataURL($files[0]);
		}
	}

	// récupération de l'archive de la capsule (version web)
	$scope.recupererArchiveWeb = function($files) {
		// si le fichier n'est pas dans le format zip
		if ( ARCHIVES_ZIP_TYPES.indexOf($files[0].type) < 0 ) {
			$scope.format_web_incorrect = true;
			$scope.archive_web_selectionne = false;
		} else {
			$scope.format_web_incorrect = false;
			$scope.archive_web_selectionne = true;

			// récupération du binaire de l'archive
			var file_reader = new FileReader();

			file_reader.onload = function (e) {
				// le timeout permet d'attendre que le binaire soit chargée avant de mettre à jour à l'écran, sinon, le chargement est trop rapide
				$timeout(function () {
					$scope.nom_archive_web = $files[0].name;
					archive_web = e.target.result;
				}, 500);
			};

			file_reader.readAsDataURL($files[0]);
		}
	}

	$scope.gererCapsule = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			parametres_requete = $scope.capsule;
			parametres_requete.id_espace = $routeParams.id_espace;
			
			// si une archive mobile a été séléctionné et que la checkbox demandant d'uploader l'archive est cochée
			if($scope.archive_mobile_selectionne && $scope.upload_archive_mobile) {
				parametres_requete.archive_mobile = archive_mobile.substring(archive_mobile.indexOf(","));	// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé
			}
			// si une archive web a été séléctionné et que la checkbox demandant d'uploader l'archive est cochée
			if($scope.archive_web_selectionne && $scope.upload_archive_web) {
				parametres_requete.archive_web = archive_web.substring(archive_web.indexOf(","));
			}

			// on modifie la capsule en BDD
			$upload.http({
				url: "webServices/capsule",
				method: "POST",
				data: parametres_requete
			}).success(function(retour_json){
				if (retour_json.status === RETOUR_JSON_OK) {
					$location.path("/tableauDeBord").search({
						id_espace: $scope.arborescence.id_espace,
						id_ressource: $scope.arborescence.id_ressource,
						id_capsule: $routeParams.id_capsule,
						ancienne_url_web_capsule: url_web_capsule			// en envoyant l'url web actuelle de la capsule, on pourra vérifier si cette url a été mise à jour (et si oui, prévenir l'utilisateur) 
					});
					toastr.success(succes_mise_a_jour);
				} else {
					// SI le nom_ourt est identique à celui d'une capsule existante
					if (retour_json.message === ERREUR_DOUBLON) {
						// on indique la précesence du doublon
						$scope.capsule_doublon = true;
						$scope.gestion_capsule_form.nom_court.$invalid = false;
					} else {
						toastr.error(erreur_requete);
					}
				}
			})
			.error(function(){
				console.log("erreur requete http post /webServices/capsule");
				toastr.error(erreur_requete);
			});
		}
	}
}]);


// page de gestion de la visibilité d'une capsule par les Groupes
backoffice_controlleurs.controller("controleurGestionVisibiliteCapsule", ["$scope", "$http", "$routeParams", "$location", "$translate", function($scope, $http, $routeParams, $location, $translate) {
	var succes_enregistrement, erreur_requete;
	var a_une_visibilite;
	var groupe_selectionne;
	var initialisation_finie = false;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})

	// traduction des labels de la page
	$translate('LABEL.CAPSULE.VISIBILITE.A_UNE_VISIBILITE').then(function (traduction) {
		a_une_visibilite = traduction;
	})

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;
	$scope.arborescence.nom_capsule = $routeParams.nom_capsule;
	$scope.arborescence.id_capsule = $routeParams.id_capsule;

	// récupération des informations à afficher sur la page
	$http.get("webServices/espace/" + $routeParams.id_espace +"/capsule/" + $routeParams.id_capsule +"/"+ $routeParams.cree_par +"/visibilite")
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				// on divise la liste des groupes en 3 : sphere publique, espace (les 2 groupes obligatoires) et une liste contenant les groupes restants
				$scope.groupe_sphere_publique = retour_json.liste_visibilite_groupe[0];
				$scope.groupe_espace = retour_json.liste_visibilite_groupe[1];
				$scope.liste_groupes = retour_json.liste_visibilite_groupe.slice(2);

				$scope.liste_visibilites_sphere_publique = retour_json.liste_visibilites;
				$scope.liste_visibilites_espace = retour_json.liste_visibilites.slice(0);
				$scope.liste_visibilites_groupe = retour_json.liste_visibilites.slice(0, 2);	// version de la liste des visibilité sans "Invisible" (utilisé pour le groupe car, pour lui, une visibilité Invisible n'a pas de sens)
				
				$scope.sous_titre_visibilite = $scope.arborescence.nom_capsule + a_une_visibilite;
				
				// Affichage sur la page de la visibilité associée au groupe Sphère publique 
				switch ($scope.groupe_sphere_publique.id_visibilite) {
					case retour_json.liste_visibilites[0].id_visibilite:
						$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[0];
						break
					case retour_json.liste_visibilites[1].id_visibilite:
						$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[1];
						break
					case retour_json.liste_visibilites[2].id_visibilite:
						$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[2];
						break
				}

				// Affichage sur la page de la visibilité associée au groupe Espace
				switch ($scope.groupe_espace.id_visibilite) {
					case retour_json.liste_visibilites[0].id_visibilite:
						$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[0];
						break
					case retour_json.liste_visibilites[1].id_visibilite:
						$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[1];
						break
					case retour_json.liste_visibilites[2].id_visibilite:
						$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[2];
						break
				}
				
				// Affichage sur la page du groupe selectionné (s'il y en a un) et de sa visibilité
				for(var clef in $scope.liste_groupes) {
					groupe_selectionne = $scope.liste_groupes[clef];

					if (groupe_selectionne.id_visibilite) {
						$scope.groupe_selectionne = groupe_selectionne;

						// Affichage sur la page de la visibilité associée au groupe groupe_selectionne
						switch (groupe_selectionne.id_visibilite) {
							case retour_json.liste_visibilites[0].id_visibilite:
								$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[0];
								break
							case retour_json.liste_visibilites[1].id_visibilite:
								$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[1];
								break
						}
						break;
					}
				}
				initialisation_finie = true;

			} else {
				toastr.error(erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/espace/:id_espace/capsule/:id_capsule/:cree_par/visibilite");
				toastr.error(erreur_requete);
		});

	// Edite les visibilités de l'Espace et du groupe quand la visibilité de la Sphère publique a été modifiée
	$scope.editerVisibilitesViaSpherePublique = function() {
		// si l'initialisation de l'écran est terminé
		if (initialisation_finie) {
			// Si la visibilité de la Sphere publique devient accessible, alors, les visibilités de l'Espace et du groupe deviennent accessible
			if ($scope.sphere_publique_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[0];
				$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[0];
			}
			// Si la visibilité de la Sphere publique devient inaccessible ET que la visibilité de l'Espace est invisible, alors, la visibilité de l'Espace devient inaccessible
			else if ($scope.sphere_publique_visibilite_selectionne.id_visibilite == VISIBILITE_INACCESSIBLE && $scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_INVISIBLE) {
				$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[1];
			}
		}
	}

	// Edite les visibilités de Sphere publique et du groupe quand la visibilité de l'Espace a été modifiée
	$scope.editerVisibilitesViaEspace = function() {
		// si l'initialisation de l'écran est terminé
		if (initialisation_finie) {
			// Si la visibilité de l'Espace devient accessible, alors, la visibilités du groupe devient accessible
			if ($scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[0];
			}
			// Si la visibilité de l'Espace devient inaccessible ET que la visibilité de la Sphere publique est accessible, alors, la visibilités de la Sphere publique devient inaccessible
			else if ($scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_INACCESSIBLE && $scope.sphere_publique_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[1];
			}
			// Si la visibilité de l'Espace devient invisible, alors, la visibilités de la Sphere publique devient invisible
			else if ($scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_INVISIBLE) {
				$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[2];
			}
		}
	}

	// Edite les visibilités de l'Espace et de Sphere publique quand la visibilité du groupe a été modifiée
	$scope.editerVisibilitesViaGroupe = function() {
		// si l'initialisation de l'écran est terminé
		if (initialisation_finie) {
			// Si la visibilité du groupe devient inaccessible ET que la visibilité de l'Espace est accessible, alors, la visibilités de l'Espace devient inaccessible et celle de la Sphere publique invisible
			if ($scope.groupe_visibilite_selectionne.id_visibilite == VISIBILITE_INACCESSIBLE && $scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[1];
				$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[2];
			}
		}
	}

	$scope.editerCapsuleVisibilite = function() {
		// récupération des paramètres pour la requete
		var parametres_requete = {};
		parametres_requete.id_espace = $routeParams.id_espace;
		parametres_requete.cree_par = $routeParams.cree_par;

		// ajout des groupes obligatoires, mis à jour, aux parametres
		parametres_requete.sphere_publique = $scope.groupe_sphere_publique;
		parametres_requete.sphere_publique.id_visibilite = $scope.sphere_publique_visibilite_selectionne.id_visibilite;
		parametres_requete.espace = $scope.groupe_espace;
		parametres_requete.espace.id_visibilite = $scope.espace_visibilite_selectionne.id_visibilite;

		// si un groupe non-obligatoire était sélectionné avant l'édition
		if (groupe_selectionne) {
			// on l'ajoute aux parametres
			parametres_requete.id_ancien_groupe_selectionne = groupe_selectionne.id_groupe;
		}
		// si un nouveau groupe non-obligatoire a été selectionne et qu'une visibilité lui est associé
		if ($scope.groupe_selectionne && $scope.groupe_visibilite_selectionne) {
			// on l'ajoute aux parametres
			parametres_requete.nouveau_groupe_selectionne = {
				id_groupe: $scope.groupe_selectionne.id_groupe,
				id_capsule: $routeParams.id_capsule,
				id_visibilite: $scope.groupe_visibilite_selectionne.id_visibilite
			};
		}

		parametres_requete.id_capsule = $routeParams.id_capsule;

		// on edite la visibilité de la capsule en BDD
		$http.post("webServices/capsule/visibilite", parametres_requete)
			.success(function(retour_json){
				if (retour_json.status === RETOUR_JSON_OK) {
					$location.path("/tableauDeBord").search({
						id_espace: $scope.arborescence.id_espace,
						id_ressource: $scope.arborescence.id_ressource,
						id_capsule: $routeParams.id_capsule
					});
					toastr.success(succes_enregistrement);
				} else {
					toastr.error(erreur_requete);
				}
			})
			.error(function(){
				console.log("erreur requete http post /webServices/capsule/visibilite");
				toastr.error(erreur_requete);
			});		
	}
}]);
