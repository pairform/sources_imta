"use strict";


/*
 * Controleurs de gestion des espaces
 */

// Page de création d'un espace
backoffice_controlleurs.controller("controleurCreationEspace", ["$scope", "$http", "$location", "$translate", "$timeout", function($scope, $http, $location, $translate, $timeout){
	var succes_enregistrement;
	var erreur_requete;
	$scope.formulaire_soumis = false;
	$scope.espace_doublon = false;
	$scope.logo_selectionne = false;		// false tant qu'aucun logo respectant les contraintes (format, taille, etc.) n'a été associé à la ressource
	$scope.format_incorrect = false;		// false si le format du logo de la ressource n'est pas le bon
	$scope.dimensions_incorrectes = false;	// false si les dimensions du logo de la ressource ne sont pas les bonnes


	// traduction des labels de la page
	$translate('LABEL.ESPACE.CREER').then(function (traduction) {
		$scope.titre = traduction;
	});
	$translate('LABEL.CREER').then(function (traduction) {
		$scope.bouton_formulaire = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})

	// récupération des informations sur le champ "Logo"
	$scope.recupererLogo = function($files) {
		// si le fichier n'est pas une image dans le bon format
		if ($files[0].type != "image/png") {
			$scope.format_incorrect = true;
			$scope.logo_selectionne = false;
			$scope.dimensions_incorrectes = false;

		} else {
			var _URL = window.URL || window.webkitURL;
			var hauteur, largeur;
			var image = new Image();

			image.onload = function() {
				// si l'image du logo n'a pas les bonnes dimensions
				if (this.width != DIMENSION_LOGO_ESPACE || this.height != DIMENSION_LOGO_ESPACE) {
					// le timeout permet d'attendre que les valeurs soient chargées avant de les mettre à jour à l'écran, sinon, le chargement est trop rapide et les valeurs ne sont pas initalisées
					$timeout(function () {
						$scope.dimensions_incorrectes = true;
						$scope.logo_selectionne = false;
						$scope.format_incorrect = false;
					}, 500);

				} else {
					// on affiche les infos sur le logo
					$scope.logo_selectionne = true;
					$scope.format_incorrect = false;
					$scope.dimensions_incorrectes = false;

					// affichage du nom du logo sélectionné
					$scope.nom_logo = $files[0].name;

					// affichage du logo selectionné
					var file_reader = new FileReader();

					file_reader.onload = function (e) {
						// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
						$timeout(function () {
							$scope.src_logo = e.target.result;
						}, 500);
					};

					file_reader.readAsDataURL($files[0]);
				}
			}

			// transformation du fichier en image, la fonction onload sera appelé à la fin
			image.src = _URL.createObjectURL($files[0]);
		}
	}

	$scope.gererEspace = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			// si un logo a été sélectionné
			var logo;
			if($scope.logo_selectionne) {
				logo = $scope.src_logo.substring(IMG_DATA_TYPE.length)		// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé
			}

			// on ajoute l'espace en BDD
			$http.put("webServices/espace", {
					nom_court: $scope.nom_court,
					nom_long: $scope.nom_long,
					logo: logo
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						$location.path("/tableauDeBord").search({id_espace: retour_json.id_espace});
						toastr.success(succes_enregistrement);
					} else {
						// SI le nom_ourt est identique à celui d'un espace existant
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la présence du doublon
							$scope.espace_doublon = true;
							$scope.gestion_espace_form.nom_court.$invalid = false;
						} else {
							toastr.error(erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http put /webServices/espace");
					toastr.error(erreur_requete);
				});
		}
	}
}]);


// Page d'édition d'un espace
backoffice_controlleurs.controller("controleurGestionEspace", ["$scope", "$http", "$routeParams", "$location", "$translate", "$timeout", function($scope, $http, $routeParams, $location, $translate, $timeout){
	var succes_mise_a_jour;
	var erreur_requete;
	$scope.formulaire_soumis = false;
	$scope.espace_doublon = false;
	$scope.logo_selectionne = false;		// false tant qu'aucun logo respectant les contraintes (format, taille, etc.) n'a été associé à la ressource
	$scope.format_incorrect = false;		// false si le format du logo de la ressource n'est pas le bon
	$scope.dimensions_incorrectes = false;	// false si les dimensions du logo de la ressource ne sont pas les bonnes

	// traduction des labels de la page
	$translate('LABEL.ESPACE.EDITER').then(function (traduction) {
		$scope.titre = traduction;
	});
	$translate('LABEL.EDITER').then(function (traduction) {
		$scope.bouton_formulaire = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.MISE_A_JOUR').then(function (traduction) {
		succes_mise_a_jour = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})	

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;

	// récupération des informations sur l'espace à éditer
	$http.get("webServices/espace/"+ $routeParams.id_espace)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {				
				$scope.id_espace = retour_json.espace.id_espace;
				$scope.nom_court = retour_json.espace.nom_court;
				$scope.nom_long = retour_json.espace.nom_long;

				// si l'espace possède un logo
				if (retour_json.espace.logo) {
					$scope.logo_selectionne = true;
					$scope.src_logo = IMG_DATA_TYPE + retour_json.espace.logo;
				}
			} else {
				toastr.error(erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/espace/:id_espace");
			toastr.error(erreur_requete);
		});

	// récupération des informations sur le champ "Logo"
	$scope.recupererLogo = function($files) {
		// si le fichier n'est pas une image dans le bon format
		if ($files[0].type != "image/png") {
			$scope.format_incorrect = true;
			$scope.logo_selectionne = false;
			$scope.dimensions_incorrectes = false;

		} else {
			var _URL = window.URL || window.webkitURL;
			var hauteur, largeur;
			var image = new Image();

			image.onload = function() {
				// si l'image du logo n'a pas les bonnes dimensions
				if (this.width != DIMENSION_LOGO_ESPACE || this.height != DIMENSION_LOGO_ESPACE) {
					// le timeout permet d'attendre que les valeurs soient chargées avant de les mettre à jour à l'écran, sinon, le chargement est trop rapide et les valeurs ne sont pas initalisées
					$timeout(function () {
						$scope.dimensions_incorrectes = true;
						$scope.logo_selectionne = false;
						$scope.format_incorrect = false;
					}, 500);

				} else {
					// on affiche les infos sur le logo
					$scope.logo_selectionne = true;
					$scope.format_incorrect = false;
					$scope.dimensions_incorrectes = false;

					// affichage du nom du logo sélectionné
					$scope.nom_logo = $files[0].name;

					// affichage du logo selectionné
					var file_reader = new FileReader();

					file_reader.onload = function (e) {
						// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
						$timeout(function () {
							$scope.src_logo = e.target.result;
						}, 500);
					};

					file_reader.readAsDataURL($files[0]);
				}
			}

			// transformation du fichier en image, la fonction onload sera appelé à la fin
			image.src = _URL.createObjectURL($files[0]);
		}
	}

	$scope.gererEspace = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			// si un logo a été sélectionné
			var logo;
			if($scope.logo_selectionne) {
				logo = $scope.src_logo.substring(IMG_DATA_TYPE.length)		// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé
			}

			// on modifie l'espace en BDD
			$http.post("webServices/espace", {
					id_espace: $scope.id_espace,
					nom_court: $scope.nom_court,
					nom_long: $scope.nom_long,
					logo: logo
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						$location.path("/tableauDeBord").search({id_espace: $routeParams.id_espace});
						toastr.success(succes_mise_a_jour);
					} else {
						// SI le nom_ourt est identique à celui d'un espace existant
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la présence du doublon
							$scope.espace_doublon = true;
							$scope.gestion_espace_form.nom_court.$invalid = false;
						} else {
							toastr.error(erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /webServices/espace");
					toastr.error(erreur_requete);
				});
		}
	}
}]);
