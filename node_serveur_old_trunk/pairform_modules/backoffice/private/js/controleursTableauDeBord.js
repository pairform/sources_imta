"use strict";


/*
 * Controleurs du tableau de bord
 */

// Page d'accueil
backoffice_controlleurs.controller("controleurTableauDeBord", ["$scope", "$routeParams", "$http", "$translate", "$location", "connexion", function($scope, $routeParams, $http, $translate, $location, connexion) {
	var succes_suppression, erreur_requete, attention_modification_url_web_capsule;
	var supprimer_espace, supprimer_ressource, supprimer_capsule, supprimer_messages_capsule;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.SUPPRESSION').then(function (traduction) {
		succes_suppression = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});
	$translate('LABEL.ESPACE.SUPPRIMER').then(function (traduction) {
		supprimer_espace = traduction;
	});
	$translate('LABEL.RESSOURCE.SUPPRIMER').then(function (traduction) {
		supprimer_ressource = traduction;
	});
	$translate('LABEL.CAPSULE.SUPPRIMER').then(function (traduction) {
		supprimer_capsule = traduction;
	});
	$translate('LABEL.CAPSULE.SUPPRIMER_MESSAGES_DE').then(function (traduction) {
		supprimer_messages_capsule = traduction;
	});
	$translate('LABEL.CAPSULE.MODIFICATION_URL_WEB_CAPSULE').then(function (traduction) {
		attention_modification_url_web_capsule = traduction;
	});

	// initialisation des boutons de sélection des espaces / ressources / capsules
	$translate('LABEL.ESPACE.SELECTIONNER').then(function (traduction) {
		$scope.nom_espace_selectionne = traduction;
		$scope.nom_espace_selectionne_defaut = traduction;
	});
	$translate('LABEL.RESSOURCE.SELECTIONNER').then(function (traduction) {
		$scope.nom_ressource_selectionne = traduction;
		$scope.nom_ressource_selectionne_defaut = traduction;
	});
	$translate('LABEL.CAPSULE.SELECTIONNER').then(function (traduction) {
		$scope.nom_capsule_selectionne = traduction;
		$scope.nom_capsule_selectionne_defaut = traduction;
	});

	var utilisateur_connecte = connexion.getUtilisateurConnecte();
	// si l'utilisateur connecté est admin 
	$scope.est_admin_pairform = utilisateur_connecte.est_admin_pairform;

	$scope.arborescence = {};

	// récupération des informations à afficher sur la page
	$http.get("webServices/infosTableauDeBord")
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.liste_espaces = retour_json.liste_espaces; 
				$scope.liste_ressources = retour_json.liste_ressources;    	
				$scope.liste_capsules = retour_json.liste_capsules;
				
				// Si l'utilisateur arrive sur la tableau de bord via un clic sur un élément de l'arborescence Espace > Ressource > Capsule, on pré-sélectionne les bons éléments
				// SI un espace doit être pré-selectionné
				if ($routeParams.id_espace) {
					$scope.espace_selectionne = trouverElementListe($routeParams.id_espace, $scope.liste_espaces, "id_espace");
					$scope.nom_espace_selectionne = $scope.espace_selectionne.nom_court;
				}
				// SI une ressource doit être pré-selectionnée
				if ($routeParams.id_ressource) {
					$scope.ressource_selectionne = trouverElementListe($routeParams.id_ressource, $scope.liste_ressources, "id_ressource");
					$scope.nom_ressource_selectionne = $scope.ressource_selectionne.nom_court;
					// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord)
					$scope.arborescence.nom_espace = trouverElementListe($scope.ressource_selectionne.id_espace, $scope.liste_espaces, "id_espace").nom_court;
				}
				// SI une capsule doit être pré-selectionnée
				if ($routeParams.id_capsule) {
					$scope.capsule_selectionne = trouverElementListe($routeParams.id_capsule, $scope.liste_capsules, "id_capsule");
					$scope.nom_capsule_selectionne = $scope.capsule_selectionne.nom_court;
					// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord)
					var arborescence_ressource = trouverElementListe($scope.capsule_selectionne.id_ressource, $scope.liste_ressources, "id_ressource");
					$scope.arborescence.nom_ressource = arborescence_ressource.nom_court;
					$scope.arborescence.id_espace = arborescence_ressource.id_espace;
					$scope.arborescence.nom_espace = trouverElementListe(arborescence_ressource.id_espace, $scope.liste_espaces, "id_espace").nom_court;

					// si l'url web de la capsule a changé, on informe l'utilisateur
					if ($routeParams.ancienne_url_web_capsule && $routeParams.ancienne_url_web_capsule != $scope.capsule_selectionne.url_web)
						toastr.info(attention_modification_url_web_capsule + $scope.capsule_selectionne.url_web);
				}
			} else {
				toastr.error(erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/infosTableauDeBord");
			toastr.error(erreur_requete);
		});

	// trouve un élement dans une liste via son id
	function trouverElementListe(id, liste, clef_id) {
		for(var clef in liste) {
			if (id == liste[clef][clef_id]) {
				return liste[clef];
			}
		}
	}

	// selectionne l'espace choisi par l'utilisateur
	$scope.selectionnerEspace = function (espace) {
		$scope.espace_selectionne = espace;
		$scope.nom_espace_selectionne = espace.nom_court;		
	}

	// selectionne la ressource choisi par l'utilisateur
	$scope.selectionnerRessource = function (ressource) {
		$scope.ressource_selectionne = ressource;
		$scope.nom_ressource_selectionne = ressource.nom_court;
		// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord)
		$scope.arborescence.nom_espace = trouverElementListe(ressource.id_espace, $scope.liste_espaces, "id_espace").nom_court;
	}

	// selectionne la capsule choisi par l'utilisateur
	$scope.selectionnerCapsule = function (capsule) {
		$scope.capsule_selectionne = capsule;
		$scope.nom_capsule_selectionne = capsule.nom_court;
		// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord)
		var arborescence_ressource = trouverElementListe(capsule.id_ressource, $scope.liste_ressources, "id_ressource");
		$scope.arborescence.nom_ressource = arborescence_ressource.nom_court;
		$scope.arborescence.id_espace = arborescence_ressource.id_espace;
		$scope.arborescence.nom_espace = trouverElementListe(arborescence_ressource.id_espace, $scope.liste_espaces, "id_espace").nom_court;
	}

	// supprime l'espace séléctionné par l'utilisateur
	$scope.supprimerEspace = function (espace) {
		// popup de confirmation de l'action
		if( confirm(supprimer_espace +" "+ espace.nom_court +" ?") ) {
			// suppression en BDD
			$http.delete("webServices/espace/"+ espace.id_espace)
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toastr.success(succes_suppression);
						// on recharge l'écran
						$location.path("/tableauDeBord").search({});
					} else {
						toastr.error(erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http delete /webServices/espace/:id_espace");
					toastr.error(erreur_requete);
				});
		}
	}

	// supprime la ressource séléctionnée par l'utilisateur
	$scope.supprimerRessource = function (ressource) {
		// popup de confirmation de l'action
		if( confirm(supprimer_ressource +" "+ ressource.nom_court +" ?") ) {
			// suppression en BDD
			$http.delete("webServices/ressource/"+ ressource.id_ressource +"/"+ ressource.id_espace)
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toastr.success(succes_suppression);
						// on recharge l'écran
						$location.path("/tableauDeBord").search({});
					} else {
						toastr.error(erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http delete /webServices/ressource/:id_ressource/:id_espace");
					toastr.error(erreur_requete);
				});
		}
	}

	// supprime la capsule séléctionnée par l'utilisateur
	$scope.supprimerCapsule = function (capsule, id_espace) {
		// popup de confirmation de l'action
		if( confirm(supprimer_capsule +" "+ capsule.nom_court +" ?") ) {
			// suppression en BDD
			$http.delete("webServices/capsule/"+ capsule.id_capsule +"/"+ capsule.id_ressource +"/"+ id_espace)
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toastr.success(succes_suppression);
						// on recharge l'écran
						$location.path("/tableauDeBord").search({});
					} else {
						toastr.error(erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http delete /webServices/capsule/:id_capsule/:id_ressource/:id_espace");
					toastr.error(erreur_requete);
				});
		}
	}

	// supprime les messages de la capsule séléctionnée par l'utilisateur
	$scope.supprimerMessagesCapsule = function (capsule, id_espace) {
		// popup de confirmation de l'action
		if( confirm(supprimer_messages_capsule +" "+ capsule.nom_court +" ?") ) {
			// suppression en BDD
			$http.delete("webServices/messages/capsule/"+ capsule.id_capsule +"/"+ capsule.cree_par +"/espace/"+ id_espace)
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toastr.success(succes_suppression);
						// on recharge l'écran
						$location.path("/tableauDeBord").search({});
					} else {
						toastr.error(erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http delete /webServices/messages/capsule/:id_capsule/:cree_par/espace/:id_espace");
					toastr.error(erreur_requete);
				});
		}
	}


	/*
	 * Gestion de la visibilité des différents boutons du tableau de bord en fonction des droits de l'utilisateur connecté
	 */
	$scope.aAccesBoutonEditerEspace = function () {
		if ($scope.espace_selectionne) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][EDITER_ESPACE];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonCreerRessource = function () {
		if ($scope.espace_selectionne) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][GERER_RESSOURCES_ET_CAPSULES];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererGroupes = function () {
		if ($scope.espace_selectionne) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][GERER_GROUPES] ||
				utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][GERER_VISIBILITE_CAPSULE] || 
				utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][GERER_VISIBILITE_TOUTES_CAPSULES];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererRoles = function () {
		if ($scope.espace_selectionne) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][GERER_ROLES] ||
				utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][ATTRIBUER_ROLES];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonAttribuerRoles = function () {
		if ($scope.espace_selectionne) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.espace_selectionne.id_espace][ATTRIBUER_ROLES];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonEditerRessource = function () {
		if ($scope.ressource_selectionne) {
			return utilisateur_connecte.est_admin_pairform || 
				utilisateur_connecte.liste_roles[$scope.ressource_selectionne.id_espace][EDITER_TOUTES_RESSOURCES_ET_CAPSULES] ||
				( utilisateur_connecte.liste_roles[$scope.ressource_selectionne.id_espace][GERER_RESSOURCES_ET_CAPSULES] && $scope.ressource_selectionne.cree_par == utilisateur_connecte.id_utilisateur );
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonCreerCapsule = function () {
		if ($scope.ressource_selectionne) {
			return utilisateur_connecte.est_admin_pairform || 
				( utilisateur_connecte.liste_roles[$scope.ressource_selectionne.id_espace][GERER_RESSOURCES_ET_CAPSULES] && $scope.ressource_selectionne.cree_par == utilisateur_connecte.id_utilisateur ) ||
				utilisateur_connecte.liste_roles[$scope.ressource_selectionne.id_espace][ADMIN_ESPACE];
		} else {
			return false;
		}
	}	
	$scope.aAccesBoutonAfficherStatistiquesRessource = function () {
		if ($scope.ressource_selectionne) {
			return utilisateur_connecte.est_admin_pairform || 
				$scope.ressource_selectionne.cree_par == utilisateur_connecte.id_utilisateur ||
				utilisateur_connecte.liste_roles[$scope.ressource_selectionne.id_espace][ADMIN_ESPACE];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonEditerCapsule = function () {
		if ($scope.capsule_selectionne) {
			return utilisateur_connecte.est_admin_pairform || 
				utilisateur_connecte.liste_roles[$scope.arborescence.id_espace][EDITER_TOUTES_RESSOURCES_ET_CAPSULES] ||
				( utilisateur_connecte.liste_roles[$scope.arborescence.id_espace][GERER_RESSOURCES_ET_CAPSULES] && $scope.capsule_selectionne.cree_par == utilisateur_connecte.id_utilisateur );
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonSupprimerMessagesCapsule = function () {
		if ($scope.capsule_selectionne) {
			return utilisateur_connecte.est_admin_pairform || 
				( utilisateur_connecte.liste_roles[$scope.arborescence.id_espace][GERER_RESSOURCES_ET_CAPSULES] && $scope.capsule_selectionne.cree_par == utilisateur_connecte.id_utilisateur ) ||
				utilisateur_connecte.liste_roles[$scope.arborescence.id_espace][ADMIN_ESPACE];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererVisibiliteCapsule = function () {
		if ($scope.capsule_selectionne) {
			return utilisateur_connecte.est_admin_pairform || 
				utilisateur_connecte.liste_roles[$scope.arborescence.id_espace][GERER_VISIBILITE_TOUTES_CAPSULES] ||
				( utilisateur_connecte.liste_roles[$scope.arborescence.id_espace][GERER_VISIBILITE_CAPSULE] && $scope.capsule_selectionne.cree_par == utilisateur_connecte.id_utilisateur );
		} else {
			return false;
		}
	}
}]);
