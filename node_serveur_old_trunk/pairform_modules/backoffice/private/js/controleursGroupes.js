"use strict";


/*
 * Controleurs de gestion des Groupes
 */

// page de gestion du contenu des groupes
backoffice_controlleurs.controller("controleurGestionGroupes", ["$scope", "$http", "$routeParams", "$translate", "connexion", function($scope, $http, $routeParams, $translate, connexion) {
	var succes_enregistrement, succes_mise_a_jour, succes_suppression;
	var erreur_requete, erreur_email_invalide, erreur_nom_domaine_invalide, erreur_ajout_dans_espace_avant_groupe;
	var supprimer_groupe;

	// détermine l'index d'un membre dans une liste de membre
	// Rq: liste.indexOf ne fonctionne pas, AngularJS rajoute la clef du membre dans l'objet membre ; 2 membres identiques de 2 listes différentes apparaissent comme différents car leur clef est différente
	var indexOf = function(email_ou_nom_domaine_membre, liste_membres) {
		for (var i in liste_membres) {
			if (liste_membres[i].nom == email_ou_nom_domaine_membre) {
				return i;
			}
		}
		return -1;
	};

	$scope.formulaire_edition_membres_groupe_soumis = false;
	$scope.formulaire_soumis = false;
	$scope.groupe_doublon = false;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	});
	$translate('LABEL.SUCCES.MISE_A_JOUR').then(function (traduction) {
		succes_mise_a_jour = traduction;
	});
	$translate('LABEL.SUCCES.SUPPRESSION').then(function (traduction) {
		succes_suppression = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});
	$translate('LABEL.ERREUR.NOM_DE_DOMAINE_INVALIDE').then(function (traduction) {
		erreur_nom_domaine_invalide = traduction;
	})
	$translate('LABEL.ERREUR.EMAIL_INVALIDE').then(function (traduction) {
		erreur_email_invalide = traduction;
	})	
	$translate('LABEL.ERREUR.AJOUTER_MEMBRE_DANS_ESPACE_AVANT_GROUPE').then(function (traduction) {
		erreur_ajout_dans_espace_avant_groupe = traduction;
	})	
	$translate('LABEL.GROUPE.SUPPRIMER').then(function (traduction) {
		supprimer_groupe = traduction;
	});
	
	$scope.nouvelle_liste_membres = {};
	$scope.nouvelle_liste_membres_email = {};
	$scope.nouvelle_liste_membres_nom_domaine = {};

	// si l'utilisateur connecté est admin ou s'il a le droit gerer_groupes
	$scope.droit_gerer_groupes = connexion.getUtilisateurConnecte().est_admin_pairform || connexion.getUtilisateurConnecte().liste_roles[$routeParams.id_espace][GERER_GROUPES];
	
	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	
	// récupération des informations à afficher sur la page
	$http.get("webServices/espace/"+ $routeParams.id_espace +"/listeGroupes")
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.liste_groupes = retour_json.liste_groupes;

				// ajout de listes fictives pour le groupe Sphère publique (fictives, car elles ne seront jamais affichées)
				// permet d'éviter des erreurs JS lors d'utilisation de "concat()" sur les listes
				$scope.liste_groupes[0].liste_membres_email = [];
				$scope.liste_groupes[0].liste_membres_nom_domaine = [];
			} else {
				toastr.error(erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/espace/:id_espace/listeGroupes");
			toastr.error(erreur_requete);
		});

	$scope.creerGroupe = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			// on ajoute le nouveau groupe en BDD
			$http.put("webServices/groupe", {
					id_espace: $routeParams.id_espace,
					nom: $scope.nom_nouveau_groupe,
					obligatoire: false
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toastr.success(succes_enregistrement);
						// ajout du nouveau groupe à la liste
						$scope.liste_groupes.push({
							id_groupe: retour_json.id_groupe,
							nom: $scope.nom_nouveau_groupe,
							id_espace: $routeParams.id_espace,
							liste_membres_email: [],
							liste_membres_nom_domaine: []
						});
						// mise à jour de l'interface
						$scope.formulaire_soumis = false;
						$scope.nom_nouveau_groupe = "";
					} else {
						// SI le nom du groupe est identique à celui d'un groupe existant
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la présence du doublon
							$scope.groupe_doublon = true;
							$scope.creation_groupe_form.nom_nouveau_groupe.$invalid = false;
						} else {
							toastr.error(erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http put /webServices/groupe");
					toastr.error(erreur_requete);
				});
		}
	};

	$scope.editerNomGroupe = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			// on edite le groupe en BDD
			$http.post("webServices/groupe", {
					id_espace: $routeParams.id_espace,
					nom: $scope.nom_groupe_selectionne,
					id_groupe: $scope.groupe_selectionne.id_groupe
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toastr.success(succes_mise_a_jour);
						// modification du groupe venant d'être édité
						$scope.groupe_selectionne.nom = $scope.nom_groupe_selectionne;
						// mise à jour de l'interface
						$scope.formulaire_soumis = false;
					} else {
						// SI le nom du groupe est identique à celui d'un groupe existant
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la présence du doublon
							$scope.groupe_doublon = true;
							$scope.edition_groupe_form.nom_groupe_selectionne.$invalid = false;
						} else {
							toastr.error(erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /webServices/groupe");
					toastr.error(erreur_requete);
				});
		}
	};

	$scope.supprimerGroupe = function() {
		// popup de confirmation de l'action
		if( confirm(supprimer_groupe +" "+ $scope.groupe_selectionne.nom +" ?") ) {
			// on ajoute le nouveau groupe en BDD
			$http.delete("webServices/espace/"+ $routeParams.id_espace +"/groupe/" + $scope.groupe_selectionne.id_groupe)
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toastr.success(succes_suppression);
						// mise à jour de l'interface
						var index_groupe = $scope.liste_groupes.indexOf($scope.groupe_selectionne);
						$scope.liste_groupes.splice(index_groupe, 1);
						$scope.groupe_selectionne = null;
					} else {
						toastr.error(erreur_requete);
					}
				})
				.error(function(){
					console.log("erreur requete http delete /webServices/espace/:id_espace/groupe/:id_groupe");
					toastr.error(erreur_requete);
				});
		}
	};

	// Actualisation de la liste des membres du groupe sélectionné (cette liste contient la liste des email et la liste des noms de domaine)
	$scope.rechercherMembreGroupe = function() {
		if ($scope.groupe_selectionne) {
			$scope.groupe_selectionne.liste_membres = $scope.groupe_selectionne.liste_membres_email.concat( $scope.groupe_selectionne.liste_membres_nom_domaine );
		}
	}

	// configuration des listes affichées dans la fenetre  permettant d'ajouter / supprimer les membres du groupe sélectionné
	$scope.gererMembresGroupe = function() {
		// si le groupe sélectionné n'est pas un groupe obligatoire (ni Espace, ni Sphère publique)
		if (!$scope.groupe_selectionne.obligatoire) {
			$scope.groupe_espace = $scope.liste_groupes[1];
			// configuration des titres des colonnes de membres
			$translate('LABEL.GROUPE.MEMBRE.MEMBRES_DE').then(function (traduction) {
				$scope.legende_groupe_selectionne = traduction + $scope.groupe_selectionne.nom;
			})
			$translate('LABEL.GROUPE.MEMBRE.MEMBRES_DE_L').then(function (traduction) {
				$scope.legende_groupe_espace = traduction + $scope.groupe_espace.nom;
			})

			// liste de tous les membres du groupe espace (concatenation des 2 listes, email et noms de domaine)
			$scope.liste_membres_groupe_espace = $scope.groupe_espace.liste_membres_email.concat( $scope.groupe_espace.liste_membres_nom_domaine) ;
		}
	
		// on créé une copie éditable des différentes liste de membres du groupe sélectionné
		$scope.nouvelle_liste_membres_email = angular.copy( $scope.groupe_selectionne.liste_membres_email );						// liste des email des membres
		$scope.nouvelle_liste_membres_nom_domaine = angular.copy( $scope.groupe_selectionne.liste_membres_nom_domaine );			// liste des noms de domaine des membres du groupe
		$scope.nouvelle_liste_membres = $scope.nouvelle_liste_membres_email.concat( $scope.nouvelle_liste_membres_nom_domaine );	// liste de tous les membres du groupe (concatenation des 2 listes)
	};

	// créé une liste de nouveaux membres (email ou nom de domaine) dans le groupe Espace
	$scope.creerMembresGroupeEspace = function(email_ou_nom_domaine_nouveaux_membres, liste_membres, nom_de_domaine) {
		email_ou_nom_domaine_nouveaux_membres = email_ou_nom_domaine_nouveaux_membres.replace(/\s+/g, '');	// suppression des espaces
		email_ou_nom_domaine_nouveaux_membres = email_ou_nom_domaine_nouveaux_membres.split(",");			// transformation du string en array : les virgules sont utilisées comme éléments de séparation
		var membre_valide;

		for (var i in email_ou_nom_domaine_nouveaux_membres) {
			membre_valide = false;

			if (email_ou_nom_domaine_nouveaux_membres[i]) {
				// si le membre à ajouter est constituer d'un nom de domaine
				if(nom_de_domaine) {
					if ( REGEX_NOM_DE_DOMAINE.test(email_ou_nom_domaine_nouveaux_membres[i]) ) {
						membre_valide = true;
					} else {
						toastr.warning(erreur_nom_domaine_invalide + email_ou_nom_domaine_nouveaux_membres[i]);
					}
				} else {
					// sinon, le membre est constituer d'un email
					if ( REGEX_EMAIL.test(email_ou_nom_domaine_nouveaux_membres[i]) ) {
						membre_valide = true;
					} else {
						toastr.warning(erreur_email_invalide + email_ou_nom_domaine_nouveaux_membres[i]);
					}
				}

				// si le membre à ajouter est valide et ne fait pas déjà parti du groupe, on l'ajoute au groupe
				if(membre_valide && indexOf(email_ou_nom_domaine_nouveaux_membres[i], liste_membres) < 0) {
					liste_membres.push({
						nom: email_ou_nom_domaine_nouveaux_membres[i],
						nom_de_domaine: nom_de_domaine
					});
					$scope.nouvelle_liste_membres.push({
						nom: email_ou_nom_domaine_nouveaux_membres[i],
						nom_de_domaine: nom_de_domaine
					});
				}
			}
		}
	};

	// ajoute un membre (du groupe Espace) dans un groupe
	$scope.ajouterMembreGroupe = function(nouveau_membre, liste_membres) {
		// si le membre à ajouter ne fait pas déjà parti du groupe, on l'ajoute au groupe
		if (indexOf(nouveau_membre.nom, liste_membres) < 0) {
			liste_membres.push(nouveau_membre);
			$scope.nouvelle_liste_membres.push(nouveau_membre);
		}
	};

	// ajoute une liste de membres dans un groupe
	$scope.ajouterMembresGroupe = function(email_ou_nom_domaine_nouveaux_membres, liste_membres, liste_membres_espace, nom_de_domaine) {
		email_ou_nom_domaine_nouveaux_membres = email_ou_nom_domaine_nouveaux_membres.replace(/\s+/g, '');	// suppression des espaces
		email_ou_nom_domaine_nouveaux_membres = email_ou_nom_domaine_nouveaux_membres.split(",");			// transformation du string en array : les virgules sont utilisées comme éléments de séparation

		var index_membre_liste_membres_espace;

		for (var i in email_ou_nom_domaine_nouveaux_membres) {
			// si le membre à ajouter n'est pas vide ET s'il ne fait pas déjà parti du groupe
			if (email_ou_nom_domaine_nouveaux_membres[i] && indexOf(email_ou_nom_domaine_nouveaux_membres[i], liste_membres) < 0) {
				// récupération de l'index du membre dans la la liste de membres du groupe Espace
				index_membre_liste_membres_espace = indexOf(email_ou_nom_domaine_nouveaux_membres[i], liste_membres_espace);

				// si le membre à ajouter fait parti du groupe Espace
				if (index_membre_liste_membres_espace >= 0) {
					// ajout du membre au groupe
					liste_membres.push( liste_membres_espace[index_membre_liste_membres_espace] );
					$scope.nouvelle_liste_membres.push( liste_membres_espace[index_membre_liste_membres_espace] );
				} else {
					toastr.warning(erreur_ajout_dans_espace_avant_groupe);
				}
			}
		}
	};

	// supprime un membre dans un groupe
	$scope.supprimerMembreGroupe = function(membre_a_supprime, liste_membres) {
		liste_membres.splice( liste_membres.indexOf(membre_a_supprime), 1);
		$scope.nouvelle_liste_membres.splice( $scope.nouvelle_liste_membres.indexOf(membre_a_supprime), 1);
	};

	// supprime tout les membres d'un groupe
	$scope.viderMembresGroupe = function(liste_membres) {
		for (var i in liste_membres) {
			$scope.nouvelle_liste_membres.splice( $scope.nouvelle_liste_membres.indexOf(liste_membres[i]), 1);
		}
		while(liste_membres.length) {
			liste_membres.pop();
		}
	}

	// Mise à jour des membres du groupe sélectionné avec enregistrement en BDD
	$scope.editerMembresGroupe = function() {
		$scope.formulaire_edition_membres_groupe_soumis = true;

		var liste_membres_supprimes = [];
		var liste_nouveaux_membres = [];
		var membre;

		// on récupère les membres du groupe qui ne sont plus présent dans la nouvelle liste de membres
		for (var i in $scope.groupe_selectionne.liste_membres) {
			membre = $scope.groupe_selectionne.liste_membres[i];
			// si le membre de l'ancienne liste n'existe plus dans la nouvelle liste 
			if (indexOf(membre.nom, $scope.nouvelle_liste_membres) < 0) {
				liste_membres_supprimes.push(membre);
			}
		}
		// on récupère les nouveaux membres du groupe
		for (var i in $scope.nouvelle_liste_membres) {
			membre = $scope.nouvelle_liste_membres[i];
			// si le membre de la nouvelle liste n'existait pas dans l'ancienne 
			if (indexOf(membre.nom, $scope.groupe_selectionne.liste_membres) < 0) {
				liste_nouveaux_membres.push(membre);
			}
		}

		// si des membres ont été ajoutés ou supprimés
		if(liste_membres_supprimes.length > 0 || liste_nouveaux_membres.length > 0) {
			// on edite le groupe en BDD
			$http.post("webServices/groupe/listeMembres", {
					id_espace: $routeParams.id_espace,
					groupe_espace: $scope.groupe_selectionne.nom === "Espace",
					id_groupe: $scope.groupe_selectionne.id_groupe,
					liste_nouveaux_membres: liste_nouveaux_membres,
					liste_membres_supprimes: liste_membres_supprimes
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						// Si le groupe selectionné est l'Espace
						if ($scope.groupe_selectionne.nom === "Espace") {
							$scope.groupe_selectionne.liste_membres_email = retour_json.liste_membres_email;
							$scope.groupe_selectionne.liste_membres_nom_domaine = retour_json.liste_membres_nom_domaine;

							//  Si des membres du groupe Espace ne sont plus présent dans sa nouvelle liste de membres
							if(liste_membres_supprimes.length > 0) {
								// on supprime ces membres dans tout les sous-groupes
								for (var i=2; i < $scope.liste_groupes.length; i++) {
									for (var j in liste_membres_supprimes) {
										// si le membre a supprimé fait partie des membres du sous-groupe, on le supprime
										if (indexOf(liste_membres_supprimes[j].nom, $scope.liste_groupes[i].liste_membres_email) >= 0) {
											$scope.liste_groupes[i].liste_membres_email.splice(
												indexOf(liste_membres_supprimes[j].nom, $scope.liste_groupes[i].liste_membres_email),
												1
											);
										}
										if (indexOf(liste_membres_supprimes[j].nom, $scope.liste_groupes[i].liste_membres_nom_domaine) >= 0) {
											$scope.liste_groupes[i].liste_membres_nom_domaine.splice(
												indexOf(liste_membres_supprimes[j].nom, $scope.liste_groupes[i].liste_membres_nom_domaine),
												1
											);
										}
									}
								};
							}
						} else {
							$scope.groupe_selectionne.liste_membres_email = $scope.nouvelle_liste_membres_email;
							$scope.groupe_selectionne.liste_membres_nom_domaine = $scope.nouvelle_liste_membres_nom_domaine;
						}
						$scope.rechercherMembreGroupe();

						toastr.success(succes_enregistrement);
					} else {
						toastr.error(erreur_requete);
					}
					$scope.formulaire_edition_membres_groupe_soumis = false;
					$scope.editer_membres = false; 
				})
				.error(function(){
					$scope.formulaire_edition_membres_groupe_soumis = false;
					$scope.editer_membres = false; 
					console.log("erreur requete http post /webServices/groupe/listeMembres");
					toastr.error(erreur_requete);
				});
		} else {
			$scope.formulaire_edition_membres_groupe_soumis = false;
			$scope.editer_membres = false;
			toastr.success(succes_enregistrement);
		}
	};
}]);
