"use strict";

module.exports = function(app, passport) {
	require("./routes/routesBackoffice")(app, passport);
	require("./routes/routesApplications")(app, passport);
	require("./routes/routesMigrations")(app, passport);
}