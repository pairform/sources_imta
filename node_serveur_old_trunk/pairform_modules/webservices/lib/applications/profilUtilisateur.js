"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes");


// affiche les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
exports.getProfilUtilisateur = function(parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var version = parametres.version,
		os = parametres.os,
		id_utilisateur = parametres.id_utilisateur,
		id_langue = parametres.id_langue ? parametres.id_langue : 3;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		selectProfilUtilisateur();
	});

	// Récupère les infos sur le profil de l'utilisateur (pseudo, avatar, email, etablissement, langue principale, etc.)
	function selectProfilUtilisateur() {
		pairform_dao.utilisateur_dao.selectProfilUtilisateur(
			id_utilisateur,
			function (retour_sql) {
				retour_json.datas = {}
				retour_json.datas.username = retour_sql.username;
				retour_json.datas.email = retour_sql.email;
				retour_json.datas.etablissement = retour_sql.etablissement;
				retour_json.datas.langue_principale = retour_sql.langue_principale;
				retour_json.datas.avatar_url = retour_sql.url_avatar;
				retour_json.datas.score_max = retour_sql.score_max;
			
				selectRessourcesByUtilisateur();
			},
			callbackError
		);
	}

	function selectRessourcesByUtilisateur() {
		pairform_dao.ressource_dao.selectRessourcesByUtilisateur(
			id_utilisateur,
			id_langue,
			function (liste_ressources) {
				retour_json.datas.ressources = liste_ressources;
				selectSuccesByUtilisateur();
			},
			callbackError
		);
	}

	// Récupère les succès d'un utilisateur
	function selectSuccesByUtilisateur() {
		pairform_dao.succes_dao.selectSuccesByUtilisateur(
			id_utilisateur,
			id_langue,
			function (liste_succes) {
				retour_json.datas.succes = liste_succes;
				retour_json.datas.nb_succes = liste_succes.length;
				selectDernierSuccesGagnesByUtilisateur();
			},
			callbackError
		);
	}

	// Récupère les 5 derniers succès gagnés par l'utilisateur
	function selectDernierSuccesGagnesByUtilisateur() {
		pairform_dao.succes_dao.selectDernierSuccesGagnesByUtilisateur(
			id_utilisateur,
			function (liste_derniers_succes_gagnes) {
				retour_json.datas.nb_succes_gagnes = liste_derniers_succes_gagnes.length;

				// on récupère seulement les 5 derniers succès gagnés (ou moins si l'utilisateur à moins de 5 succès gagnés)
				switch (liste_derniers_succes_gagnes.length) {
					case 0:
						retour_json.datas.derniers_succes = [];
						break;
					case 1:
						retour_json.datas.derniers_succes = liste_derniers_succes_gagnes.slice(0, 1);
						break;
					case 2:
						retour_json.datas.derniers_succes = liste_derniers_succes_gagnes.slice(0, 2);
						break;
					case 3:
						retour_json.datas.derniers_succes = liste_derniers_succes_gagnes.slice(0, 3);
						break;
					case 4:
						retour_json.datas.derniers_succes = liste_derniers_succes_gagnes.slice(0, 4);
						break;
					default:
						retour_json.datas.derniers_succes = liste_derniers_succes_gagnes.slice(0, 5);
				}	

				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);								
			},
			callbackError
		);
	}
};
