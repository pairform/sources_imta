"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		 request = require("request"),
			 log = require('metalogger')(),
		  api_rs = require("../api_rs");


//TODO
exports.ajouterTags = function(param, callback) {		
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		new_tags = param.tags;

	//Verification que la limite de tags ne soit pas excédée
	if (new_tags.length > 5) {
		callback({"status" : "ko", "message" : "label_erreur_maximum_tags"});
	}
	else {
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.message_dao.selectAllTags(id_message, function callback_succes (old_tags) {
				//Verification que la limite de tags ne soit pas excédée
				if (old_tags.length + new_tags.length > 5) {
					callback({"status" : "ko", "message" : "label_erreur_maximum_tags"});
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
				else {
					//Lowercase des tags, pour éviter le mismatch lié aux majuscules
					var old_tags_lowercase = old_tags.map(function (value, index) {
						return value["tag"].toLowerCase();
					});

					var tags_to_add = [];
					//Vérification que chaque tag ne soit pas déjà en base
					for (var i = 0; i < new_tags.length; i++) {
						var ind = old_tags_lowercase.indexOf(new_tags[i].toLowerCase());
						//Si le tag ne l'est pas
						if (ind == -1 && new_tags[i] != "")
							//On va le rajouter
							tags_to_add.push(new_tags[i]);
					};
					//S'il y a des tags à rajouter
					if (tags_to_add.length > 0) {
						pairform_dao.message_dao.insertMultipleTags(id_message, id_utilisateur, tags_to_add, function () {
							pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
								pairform_dao.libererConnexionBDD(_connexion_courante);
								callback({
									"status" : "up",
									"data" : tags_to_add
								});
							}, callbackError);
						}, callbackError);
					}
					//Sinon, bye bye
					else {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({"status" : "ok"}); 
					}

				}
			}, callbackError);
		});
	}

};
//TODO
exports.supprimerTag = function(param, callback) {		
	function callbackError () {
		callback({"status" : "ko", "message" : "web_label_erreur_contacter_pairform"});
	}	
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		tag = param.tag;

	//Verification que la limite de tags ne soit pas excédée
	if (tag == "") {
		callback({"status" : "ko", "message" : "web_label_erreur_contacter_pairform"});
	}
	else {
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.message_dao.deleteTag(id_message, id_utilisateur, tag, function callback_succes (retour_sql) {
				if (retour_sql.affectedRows > 0) {
					pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({"status" : "ok"});			
					}, callbackError);		
				}
				else {
					callback({"status" : "ko", "message" : "web_label_erreur_suppression_tag_autre"});
				}
			}, 
			callbackError);
		});
	}

};

//TODO
exports.recupererVotesMessage = function(param, callback) {		
	function callbackError () {
		callback({"status" : "ko", "message" : "web_label_erreur_contacter_pairform"});
	}	
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.vote_dao.selectVotesMessage(id_message, function callback_succes (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			callback({
				"status" : "ok", 
				"data" : retour_sql
			});
		}, 
		callbackError);
	});


};
// deplacerMessage.php
exports.deplacer = function(param, callback) {
	function callbackError () {
		callback({"status" : "ko", "message" : "web_label_erreur_contacter_pairform"});
	}	
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		id_capsule = param.id_capsule,
		nom_page = param.nom_page,
		nom_tag = param.nom_tag,
		num_occurence = param.num_occurence;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updatePosition(
			id_message,
			id_capsule,
			nom_page,
			nom_tag,
			num_occurence,
			function callback_succes (retour_sql) {
			if (retour_sql.affectedRows > 0) {
				pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback({"status" : "ok"});			
				}, callbackError);		
			}
			else {
				callback({"status" : "ko", "message" : "web_label_erreur_suppression_tag_autre"});
			}
		}, 
		callbackError);
	});
};


// donnerMedaille.php
exports.donnerMedaille = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	var medaille = param.medaille || param.type_medaille || "", //String vide pour l'enlever
		id_message = param.id_message,
		id_utilisateur = param.id_utilisateur;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaMedaille(id_message, medaille, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.donnerMedaille(id_utilisateur, id_message);
			}, callbackError);
		}, callbackError);
	});
};


// enregistrerMessage.php
exports.envoyer = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	var id_utilisateur = param["id_utilisateur"],
		uid_page = param["uid_page"] != "_pf_res" ? param["uid_page"] : "",
		uid_page = param["uid_page"] || "",
		uid_oa = param["uid_oa"] || "", 
		id_capsule = param["id_capsule"] || param["id_ressource"] || "DEFAULT", 
		nom_page = param["nom_page"] != "_pf_res" ? param["nom_page"] : "", 
		nom_tag = param["nom_tag"] || "", 
		num_occurence = param["num_occurence"] || "DEFAULT", 
		geo_latitude = param["geo_latitude"] || "DEFAULT", 
		geo_longitude = param["geo_longitude"] || "DEFAULT", 
		id_langue = param["id_langue"] || param["langue"] || "DEFAULT", 
		id_message_parent = param["id_message_parent"] || param["id_message_original"] || param["parent"] || "DEFAULT", 
		est_defi = param["est_defi"] || "DEFAULT",
		contenu = param["contenu"] || param["value"] || "",
		tags = [],
		visibilite = "0";

	//Moche selection de parametre
	//Albert : je ne sais pas si tu envoie tag ou tags comme parametre, et json.parse sur unedefined crash direct.
	//Dans le doute, je teste les parametres comme ça, mais redis moi à l'occasion
	if (param["tags"])		
		tags = JSON.parse(param["tags"]);
	else if(param["tag"])
		tags = JSON.parse(param["tag"]);
	else
		tags = [];

	if(param["visibilite"])
		visibilite = JSON.parse(param["visibilite"]).length > 0 ? JSON.parse(param["visibilite"]).toString() : "0";
	else
		visibilite="0"

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectProchainIdMessage(
			function callback_succes (retour_sql) {
				var id_message = retour_sql[0]['AUTO_INCREMENT'];
				pairform_dao.message_dao.insertMessage(
					id_utilisateur,
					uid_page,
					uid_oa,
					id_capsule,
					nom_page,
					nom_tag,
					num_occurence,
					geo_latitude,
					geo_longitude,
					id_langue,
					id_message_parent,
					est_defi,
					visibilite,
					contenu,
					function callback_succes () {
						//S'il y a des tags à mettre
						pairform_dao.libererConnexionBDD(_connexion_courante);
						if (tags.length) {
							exports.ajouterTags({"id_utilisateur" : id_utilisateur, "id_message" : id_message, "tags" : tags}, function callbackTags (retour) {
								if (retour == "ok") {
									callback({
										"status" : "ok",
										"id_message" : id_message
									});
								}
								else {
									callback({
										"status" : "ok",
										"id_message" : id_message,
										"data" : retour
									});
								}
							});
						}
						else {
							callback({
								"status" : "ok",
								"id_message" : id_message
							});
						}

						api_rs.ecrireMessage(id_utilisateur, id_message);
				}, callbackError);
			},
			callbackError
		);
	});
};


// modifierLangue.php
exports.modifierLangue = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	var id_langue = param.id_langue || param.langue || 3, //Français par défaut
		id_message = param.id_message;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaLangue(id_message, id_langue, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
			}, callbackError);
		}, callbackError);
	});
};



// recupererMessagesV2.php
exports.getMessages = function(param, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};
	// log.notice("Parametres " + JSON.stringify(param));

	var id_utilisateur = param.id_utilisateur,
	 filtre_timestamp = param.filtre_timestamp ? JSON.parse(param.filtre_timestamp) : undefined,
	 // filtre_ressource = param.filtre_ressource ? JSON.parse(param.filtre_ressource) : undefined,
		   id_capsule = param.id_capsule || param.id_ressource,
	langues_affichage = param.langues_affichage,
			 nom_page = param.nom_page,
			  nom_tag = param.nom_tag,
		num_occurence = param.num_occurence,
			 uid_page = param.uid_page,
			   uid_oa = param.uid_oa,
		   id_message = param.id_message;

	//Compatibilité avec les anciennes versions
	//Transformation du filtre ressource sans timestamp en un filtre avec timestamp à 0
	// if (filtre_ressource && !filtre_timestamp) {
	// 	filtre_timestamp = {};
	// 	for(id_capsule in filtre_ressource){
	// 		filtre_timestamp[id_capsule] = 0;
	// 	}
	// }

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
		function callbackError() {
			callback(constantes.RETOUR_JSON_KO);
		}

		function callbackSucces (messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			//Cast en string de toutes les valeurs pour les mobiles
			if (param.os != "web") {
				messages = JSON.parse(JSON.stringify(messages, function (key, value) {
					if (typeof(value) == "number") {
						return value + "";
					}
					else 
						return value;
				}));
			}
			callback({"status" : "ok", "messages" : messages});
		}

		/**************************************************************************************************************************/
		/****************************************** Toutes les sections filtrées **************************************************/
		/**************************************************************************************************************************/
		if (filtre_timestamp && !id_capsule && (!nom_page && !uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && !id_message){
			pairform_dao.message_dao.selectMessageWithFiltre(
				id_utilisateur, 
				filtre_timestamp, 
				callbackSucces, 
				callbackError
			);

		}
		/**************************************************************************************************************************/
		/************************************************ Une section *************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && id_capsule && (!nom_page && !uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && !id_message){
			pairform_dao.message_dao.selectAllMessagesOfCapsule(
				id_utilisateur,
				id_capsule,
				callbackSucces,
				callbackError
			);
		}
		/**************************************************************************************************************************/
		/************************************************ Une page ****************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && id_capsule && (nom_page || uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && !id_message){
			if(nom_page){
				pairform_dao.message_dao.selectMessageOfPage(
					id_utilisateur, 
					id_capsule, 
					nom_page, 
					callbackSucces, 
					callbackError
				);
			}
			else {
				pairform_dao.message_dao.selectMessageOfPageOA(
					id_utilisateur, 
					id_capsule, 
					uid_page, 
					callbackSucces, 
					callbackError
				);
			}
		}
		/**************************************************************************************************************************/
		/************************************************ Un grain ***************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && id_capsule && (nom_page || uid_page) && ((nom_tag  && num_occurence) || uid_oa) && !id_message){
			if(nom_page && num_occurence){
				pairform_dao.message_dao.selectMessageOfGrain(
					id_utilisateur, 
					id_capsule, 
					nom_page,
					nom_tag, 
					num_occurence, 
					callbackSucces, 
					callbackError
				);
			}
			else {
				pairform_dao.message_dao.selectMessageOfGrain(
					id_utilisateur, 
					id_capsule, 
					uid_page,
					uid_oa, 
					callbackSucces, 
					callbackError
				);
			}

		}
		/**************************************************************************************************************************/
		/************************************************ Un message **************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && !id_capsule && (!nom_page && !uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && id_message){
			pairform_dao.message_dao.selectMessageFromId(
				id_utilisateur, 
				id_message, 
				callbackSucces, 
				callbackError
			);
		}
		else {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			callbackError();
		}
	});

	// parametres.id_utilisateur = parametres.id_utilisateur || req.user.id_utilisateur;
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
};



exports.nombre = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000,
		langues_affichage = param.langues_affichage || [1,2,3];

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectNombreAllRessources(id_utilisateur, langues_affichage, function callback_succes (nombre_messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_messages_objet = {};
			for (var i = 0; i < nombre_messages.length; i++) {
				nombre_messages_objet[nombre_messages[i]['id_ressource']] = nombre_messages[i]['count'];
			};
			var retour = {
				"status" : "ok",
				"nombre_messages" : nombre_messages_objet
			};
			callback(retour);
		}, callbackError)
	});
};

exports.nombreParCapsules = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000,
		langues_affichage = param.langues_affichage || [1,2,3];

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectNombreAllRessourcesByCapsules(id_utilisateur, langues_affichage, function callback_succes (nombre_messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_messages_objet = {};
			for (var i = 0; i < nombre_messages.length; i++) {
				nombre_messages_objet[nombre_messages[i]['id_capsule']] = nombre_messages[i]['count'];
			};
			
			callback(nombre_messages_objet);
		}, callbackError)
	});
};
exports.nombreDansRessource = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000,
		langues_affichage = param.langues_affichage || [1,2,3],
		id_ressource = param.id_ressource;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectNombreFromRessource(id_utilisateur, langues_affichage, id_ressource, function callback_succes (nombre_messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_messages_objet = {};
			for (var i = 0; i < nombre_messages.length; i++) {
				nombre_messages_objet[nombre_messages[i]['id_capsule']] = nombre_messages[i]['count'];
			};
			var retour = {
				"status" : "ok",
				"nombre_messages" : nombre_messages_objet
			};
			callback(retour);
		}, callbackError)
	});
};
// redirigerVersMessage.php
exports.rediriger = function(param, callback) {
	
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/messages/redirigerVersMessage.php",
		method: "GET",
		form: param
	});
};


// supprimerMessage.php
exports.supprimer = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		supprime_par_precedent = +param.supprime_par, //Moche cast en int pour être sûr d'avoir un entier (parseInt bug si c'est deja un int)
		id_message = param.id_message;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaSupprime(id_message, id_utilisateur, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.supprimerMessage(id_utilisateur, id_message, supprime_par_precedent);
			}, callbackError);
		}, callbackError);
	});
};

exports.supprimerDefinitivement = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		id_rank_user_moderator = param.id_rank_user_moderator,
		est_admin_pairform = param.est_admin_pairform;

	//Si c'est un admin
	//On vérifie quand même côté serveur si c'est un admin (système anti-petit-malin)
	if(id_rank_user_moderator == "5" && est_admin_pairform){
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.message_dao.deleteMessage(id_message, function callback_succes () {
				pairform_dao.notification_dao.deleteNotificationsLinkedToMessage(id_message, function () {
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback({
						"status" : "ok"
					});
				}, callbackError);
			}, callbackError);	
		});
		
	}
	else {
		callback({
			"status" : "ko",
			"message" : "web_label_erreur_suppression"
		});
	}
};

// terminerDefi.php
exports.terminerDefi = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	
	var id_message = param.id_message,
		id_utilisateur = param.id_utilisateur;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaEstDefi(id_message, 2, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.terminerDefi(id_utilisateur, id_message);
			}, callbackError);
		}, callbackError);
	});
};


// validerDefi.php
exports.validerReponseDefi = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
		
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		defi_valide = param.defi_valide || 1;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaDefiValide(id_message, defi_valide, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.validerReponseDefi(id_utilisateur, id_message);
			}, callbackError);
		}, callbackError);
	});
};


// voterUtilite.php
exports.voter = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		vote = param.vote || param.up;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.vote_dao.selectVote(id_message, id_utilisateur, function callback_succes (a_vote) {

			//Conversion en int si c'est pas encore le cas
			if (typeof(vote) == "string") 
				vote = vote == "true" ? 1 : -1;
			else
				//Empecher des éventuels petits malins d'ajouter +2836394 à leur potes en tronquant à 1 / -1
				vote = (vote ? 1 : -1);
			//Copie avec + pour eviter un pointeur, pour s'en servir dans la notification
			var nouveau_vote = +vote;
			// }
			if (a_vote.length) {
				//Annulation du vote le cas échéant
				if (a_vote[0]["vote"] == vote)
					vote = 0;

				pairform_dao.vote_dao.updateVote(id_message, id_utilisateur, vote, function callback_succes () {
					pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({
							"status" : "ok"
						});
						api_rs.evaluerMessage(id_utilisateur, id_message, nouveau_vote, a_vote[0]["vote"]);
					}, callbackError);
				}, callbackError);
			}
			else {
				pairform_dao.vote_dao.insertVote(id_message, id_utilisateur, vote, function callback_succes () {
					pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({
							"status" : "ok"
						});
						api_rs.evaluerMessage(id_utilisateur, id_message, nouveau_vote, undefined);
					}, callbackError);
				}, callbackError);	
			}
		}, callbackError);
	});
};
