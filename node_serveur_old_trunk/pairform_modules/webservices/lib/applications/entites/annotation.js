"use strict";

var pairform_dao = require("pairform_dao"),
			 log = require('metalogger')(),
	  constantes = require("../../constantes");


exports.getAnnotations = function(id_utilisateur, id_ressource, location, lastUpdate, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		// selectAnnotations();
		getAnnotationsPagesCourantes();		
	});

	function getAnnotationsPagesCourantes(){
		pairform_dao.annotation_dao.selectAnnotationsPagesCourantes(
			id_utilisateur,
			id_ressource,
			location,
			lastUpdate,
			function (annotations) {
				retour_json.annotations = annotations;
				selectDataGroupe();
			},
			callbackError
		);
	}

	function selectDataGroupe(){
		pairform_dao.annotation_dao.selectDataGroupe(
			id_utilisateur,
			function (dataGroupe) {
				retour_json.dataGroupe = dataGroupe;
				
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


exports.setAnnotationsGuidees = function(list_parametres,callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);		
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		log.error(list_parametres.id_annotation);
		insertAnnotations(list_parametres.type, _connexion_courante);		
	});

	function insertAnnotations(type, _connexion_courante) {
		if(type == "guidees")
			pairform_dao.annotation_dao.insertAnnotationGuidees(
				list_parametres.objet.id_utilisateur,
				list_parametres.objet.id_ressource,
				list_parametres.objet.location,
				list_parametres.objet.position,
				list_parametres.objet.guidees_design,
				list_parametres.objet.guidees_contenu,
				list_parametres.objet.guidees_groupe,
				list_parametres.objet.guidees_couleur,
				list_parametres.objet.guidees_anchorOffset,
				list_parametres.objet.guidees_focusOffset,
				list_parametres.id_annotation,
				function (annotations) {
					retour_json.annotations = annotations;
					getDate(type, _connexion_courante);
				},
				callbackError
			);
		else if(type == "pointeur")
			pairform_dao.annotation_dao.insertAnnotationPointeur(
				list_parametres.objet.id_utilisateur,
				list_parametres.objet.id_ressource,
				list_parametres.objet.location,
				list_parametres.objet.position,
				list_parametres.objet.pointeur_image,
				list_parametres.objet.pointeur_texte,
				list_parametres.id_annotation,
				function (annotations) {
					retour_json.annotations = annotations;
					getDate(type, _connexion_courante);
				},
				callbackError
			);
		else if(type == "croquis")
			pairform_dao.annotation_dao.insertAnnotationCroquis(
				list_parametres.objet.id_utilisateur,
				list_parametres.objet.id_ressource,
				list_parametres.objet.location,
				list_parametres.objet.position,
				list_parametres.objet.croquis_taille,
				list_parametres.objet.croquis_data_url,
				list_parametres.id_annotation,
				function (annotations) {
					retour_json.annotations = annotations;
					getDate(type, _connexion_courante);
				},
				callbackError
			);
	}

	function getDate(type, _connexion_courante){
		pairform_dao.annotation_dao.selectDate(
			list_parametres.objet.id_utilisateur,
			type,
			list_parametres.id_annotation,
			function (date) {
				var date_creation = date[0].date_creation;
				retour_json.date_creation = date_creation;
				var date_update = date[0].date_update;
				retour_json.date_update = date_update;
				
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


exports.deleteAnnotation = function(list_parametres,callback){
	var retour_json = {status: constantes.STATUS_JSON_OK};

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);	
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		deleteAnnotation(list_parametres.type, _connexion_courante);
	});

	function deleteAnnotation(type, _connexion_courante){
		if(type=='guidees'){
			pairform_dao.annotation_dao.deleteAnnotationGuidees(
				list_parametres.id,
				list_parametres.id_utilisateur,
				function (annotations) {
					retour_json.annotations = annotations;
					getDateGuidees(_connexion_courante)
				},
				callbackError
			);	
		}else if(type == 'pointeur'){
			pairform_dao.annotation_dao.deleteAnnotationPointeur(
				list_parametres.id,
				list_parametres.id_utilisateur,
				function (annotations) {
					retour_json.annotations = annotations;
					getDate(type, _connexion_courante);
				},
				callbackError
			);	
		}else if(type == 'croquis'){
			pairform_dao.annotation_dao.deleteAnnotationCroquis(
				list_parametres.id,
				list_parametres.id_utilisateur,
				function (annotations) {
					retour_json.annotations = annotations;
					getDate(type, _connexion_courante);
				},
				callbackError
			);	
		}
	}

	function getDate(type, _connexion_courante){
		pairform_dao.annotation_dao.selectDate(
			list_parametres.id_utilisateur,
			type,
			list_parametres.id,
			function (date) {
				var date_creation = date[0].date_creation;
				retour_json.date_creation = date_creation;
				var date_update = date[0].date_update;
				retour_json.date_update = date_update;
				
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}

	function getDateGuidees(_connexion_courante){
		pairform_dao.annotation_dao.selectDateGuidees(
			list_parametres.id_utilisateur,
			list_parametres.id,
			function (date) {
				retour_json.date = date; 
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
}


exports.setTexte = function(list_parametres,callback){
	var retour_json = {status: constantes.STATUS_JSON_OK};

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);	
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		setPointeurTexte(_connexion_courante);
	});

	function setPointeurTexte(_connexion_courante){
		pairform_dao.annotation_dao.updateTexte(
			list_parametres.id_utilisateur,
			list_parametres.id_annotation,
			list_parametres.pointeur_texte,
			function (annotations) {
				retour_json.annotations = annotations;
				getDate('pointeur', _connexion_courante);
			},
			callbackError
		);	
	}

	function getDate(type, _connexion_courante){
		pairform_dao.annotation_dao.selectDate(
			list_parametres.id_utilisateur,
			type,
			list_parametres.id_annotation,
			function (date) {
				var date_creation = date[0].date_creation;
				retour_json.date_creation = date_creation;
				var date_update = date[0].date_update;
				retour_json.date_update = date_update;
				
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
}


exports.setImage = function(list_parametres,callback){
	var retour_json = {status: constantes.STATUS_JSON_OK};

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		setPointeurImage(_connexion_courante);
	});

	function setPointeurImage(_connexion_courante){
		pairform_dao.annotation_dao.updateImage(
			list_parametres.id_utilisateur,
			list_parametres.id_annotation,
			list_parametres.pointeur_image,
			function (annotations) {
				retour_json.annotations = annotations;
				getDate('pointeur', _connexion_courante);
			},
			callbackError
		);
	}

	function getDate(type, _connexion_courante){
		pairform_dao.annotation_dao.selectDate(
			list_parametres.id_utilisateur,
			type,
			list_parametres.id_annotation,
			function (date) {
				var date_creation = date[0].date_creation;
				retour_json.date_creation = date_creation;
				var date_update = date[0].date_update;
				retour_json.date_update = date_update;
				
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
}
