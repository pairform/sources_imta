"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		 request = require("request");

// listeSucces.php
exports.liste = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/accomplissement/listeSucces.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			// var retour_json = JSON.parse(body);
			callback(body);
		}
	});
};

