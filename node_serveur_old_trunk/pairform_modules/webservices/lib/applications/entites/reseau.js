"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		 request = require("request");



// afficherListeCercles.php
exports.liste = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/cercle/afficherListeCercles.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};
// ajouterMembre.php
exports.ajouterMembre = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/cercle/ajouterMembre.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};
// creerCercle.php
exports.creerCercle = function(param, callback) {
	// var id_utilisateur = param.id_utilisateur;
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/cercle/creerCercle.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
			// api_rs.creerReseau(id_utilisateur);
		}
	});
};
// creerClasse.php
exports.creerClasse = function(param, callback) {
	// var id_utilisateur = param.id_utilisateur;
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/cercle/creerClasse.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
			// api_rs.creerReseau(id_utilisateur);
		}
	});
};
// rejoindreClasse.php
exports.rejoindreClasse = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/cercle/rejoindreClasse.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};
// supprimerCercle.php
exports.supprimerCercle = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/cercle/supprimerCercle.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};
// supprimerMembre.php
exports.supprimerMembre = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/cercle/supprimerMembre.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};