"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		 request = require("request");


// changerVueNotification.php
exports.changerVueNotification = function(param, callback) {
	var id_notifications = param.id_notifications || [0];
	
	//Si on est sur mobile, c'est un tableau JSON qui arrive
	if (param.os != "web") {
		id_notifications = JSON.parse(id_notifications);
	}
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.notification_dao.updateNotificationsVues(
		id_notifications,
		function () {
			callback(constantes.RETOUR_JSON_OK);
		},
		callbackError
	);

};

// recupererNotifications.php
exports.recuperer = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000, 
		id_capsule = param.id_capsule  || null,
		retour_json_final = {"status" : constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.notification_dao.selectNotifications(
			"un",
			id_utilisateur,
			function (un_notifications) {
				retour_json_final.un = un_notifications;

				pairform_dao.notification_dao.selectNotificationsCount(
					"un",
					id_utilisateur,
					function (count_un) {
						retour_json_final.count_un = count_un[0]['count'];
						
						pairform_dao.notification_dao.selectNotifications(
							"us",
							id_utilisateur,
							function (us_notifications) {
								retour_json_final.us = us_notifications;

								pairform_dao.notification_dao.selectNotificationsCount(
									"us",
									id_utilisateur,
									function (count_us) {
										retour_json_final.count_us = count_us[0]['count'];
										
										//Si on a passé en parametre une capsule, c'est que le client veut une mise à jour du score pour cette capsule
										if (id_capsule){
											pairform_dao.utilisateur_dao.selectScoreCapsule(
												id_utilisateur,
												id_capsule,
												function (score) {
													if (score[0])
														retour_json_final.score = score[0]['score'];
													
													pairform_dao.libererConnexionBDD(_connexion_courante);
													callback(retour_json_final);
												},
												callbackError
											);
										}
										else{
											pairform_dao.libererConnexionBDD(_connexion_courante);
											callback(retour_json_final);
										}

									},
									callbackError
								);

							},
							callbackError
						);

					},
					callbackError
				);
			},
			callbackError
		);
	});
};