"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
	  	  api_rs = require("../api_rs"),
			 log = require('metalogger')(),
		 request = require("request");


// creerAvatarParDefaut.php
exports.creerAvatarParDefaut = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/creerAvatarParDefaut.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);

			if (retour_json.status == "ok") {
				callback(retour_json);
			} else {
				callbackError();
			}
		}
	});
};


// editerAvatar.php
exports.editerAvatar = function(param, callback) {
	var id_utilisateur = param.id_utilisateur;
	
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/editerAvatar.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);

			if (retour_json.status == "ok") {
				callback(retour_json);
			} else {
				callbackError();
			}
			// api_rs.changerAvatar(id_utilisateur);
		}
	});
};


// editerCompte.php
exports.editerCompte = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/editerCompte.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};


// editerLangues.php
exports.editerLangues = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/editerLangues.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);

			if (retour_json.status == "ok") {
				callback(retour_json);
			} else {
				callbackError();
			}
		}
	});
};


// registerCompte.php
exports.registerCompte = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/registerCompte.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};


// resetPassword.php
exports.resetPassword = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/resetPassword.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);

			callback(retour_json);
		}
	});
};


// resetPasswordStep2.php
exports.resetPasswordStep2 = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/resetPasswordStep2.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);

			if (retour_json.status == "ok") {
				callback(retour_json);
			} else {
				callbackError();
			}
		}
	});
};



//afficherListeDeProfils.php
exports.liste = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/profil/afficherListeDeProfils.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		// log.error("Réponse liste profil : "  + JSON.stringify(body));
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};


//changerNewsParMail.php
exports.changerNewsParMail = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/profil/changerNewsParMail.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);

			callback(retour_json);
		}
	});
};


//changerNomsReels.php
exports.changerNomsReels = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/profil/changerNomsReels.php",
		method: "POST",
		form: param
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);
			callback(retour_json);
		}
	});
};


//changerRole.php
exports.changerRole = function(param, callback) {
	var id_utilisateur_cible = param.id_utilisateur_cible, 
		id_ressource = param.id_ressource, 
		id_categorie_temp = param.id_categorie_temp;

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.updateCategorieTempUtilisateur(
			id_utilisateur_cible,
			id_ressource,
			id_categorie_temp,
			function (retour_sql) {
				pairform_dao.utilisateur_dao.selectCategorieRessource(
					id_utilisateur_cible,
					id_ressource,
					function (retour_sql) {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({"status" : constantes.STATUS_JSON_OK,
								"data" : retour_sql[0]});
						api_rs.changerCategorie(id_utilisateur_cible, retour_sql[0]["nom_categorie"], id_ressource);
					},
					callbackError
				);
			},
			callbackError
		);
		
	});
};

//Verification que l'utilisateur a bien un rôle (implique qu'il a déjà visité la ressource)
exports.verifierCategorie = function(param, callback_final) {
	var id_utilisateur = param.id_utilisateur, 
		id_ressource = param.id_ressource, 
		id_capsule = param.id_capsule;

	function callbackError() {
		callback_final(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.selectParametres(
			id_utilisateur,
			id_capsule,
			function (utilisateur_parametres) {
				// var retour_json_final = {'status': constantes.STATUT_JSON_OK};
				var retour_json_final = {"id_utilisateur" : id_utilisateur};

				//Si l'utilisateur a les paramêtres configurés
				if (utilisateur_parametres.length) {
					retour_json_final.status = constantes.STATUS_JSON_OK;
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback_final(retour_json_final);
				}
				//Sinon, on va les créer
				else{

					retour_json_final.status = constantes.STATUS_JSON_UPDATE;
					//Insertion des parametres par défaut
					pairform_dao.utilisateur_dao.insertParametres(id_utilisateur, id_capsule, function (utilisateur_parametres) {

						//Flag pour indiquer qu'il faut updater au niveau client
						retour_json_final.utilisateur_parametres = false;
						
						//Recupération du rôle de l'utilisateur, pour être sûr
						pairform_dao.utilisateur_dao.selectCategorieRessource(id_utilisateur, id_ressource, function (categorie_ressource) {

							//Si l'utilisateur a déjà un rôle
							if (categorie_ressource.length) {
								//Flag pour indiquer qu'il faut updater au niveau client
								retour_json_final.categorie_ressource = categorie_ressource[0];
								pairform_dao.libererConnexionBDD(_connexion_courante);
								callback_final(retour_json_final);
							}
							else{
								//Insertion du rôle par défaut
								pairform_dao.utilisateur_dao.insertCategorieUtilisateur(id_utilisateur, id_ressource, constantes.CATEGORIE_LECTEUR, function (categorie_ressource) {
									
									//Flag pour indiquer qu'il faut updater au niveau client
									retour_json_final.categorie_ressource = false;
									pairform_dao.libererConnexionBDD(_connexion_courante);
									callback_final(retour_json_final);
									//Trigger du succès qui va bien
									api_rs.visiterRessource(id_utilisateur);
								}, 
								callbackError);
							}
						}, 
						callbackError);
					}, 
					callbackError);
				}
			},
			callbackError
		);
		
	});
};

//Verification que l'utilisateur a bien un rôle (implique qu'il a déjà visité la ressource)
exports.getPreferences = function(param, callback_final) {
	var id_utilisateur = param.id_utilisateur,
		id_capsule = param.id_capsule;

	function callbackError() {
		callback_final(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.selectParametres(
			id_utilisateur,
			id_capsule,
			function (utilisateur_parametres) {
				// var retour_json_final = {'status': constantes.STATUT_JSON_OK};
				var retour_json_final = {"status" : constantes.STATUS_JSON_OK};

				//Si l'utilisateur a les paramêtres configurés
				if (utilisateur_parametres.length) {
					retour_json_final.preferences = utilisateur_parametres[0];
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback_final(retour_json_final);
				}
				else
					callbackError();
			},
			callbackError
		);
		
	});
};



