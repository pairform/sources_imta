var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes"),
			 log = require('metalogger')(),
		 request = require("request");


// demande de connexion d'un utilisateur
exports.interfaceConnexion = function(req, parametres, callback) {

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/loginCompte.php",
		method: "POST",
		form: parametres
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var utilisateur_connecte_json = JSON.parse(body);

			if (utilisateur_connecte_json.status == "ok") {
				// connexion avec passport, une session node.js sera crée
				req.login(
					utilisateur_connecte_json,
					function(erreur) {
						if (erreur) {
							log.error("passport login erreur : "+ erreur);
							return callbackError();
						} else {
							return callback(utilisateur_connecte_json);
						}
					}
				);
			} else {
				callback(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
			}
		}
	});
};


// demande de déconnexion d'un utilisateur
exports.interfaceDeconnexion = function(req, parametres, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/logoutCompte.php",
		method: "POST",
		form: parametres
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError();
		} else {
			var retour_json = JSON.parse(body);

			if (retour_json.status == "ok") {
				req.logout();
				callback(constantes.RETOUR_JSON_OK);
			} else {
				callbackError();
			}
		}
	});
};
