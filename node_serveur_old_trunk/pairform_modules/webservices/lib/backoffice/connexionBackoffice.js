var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes"),
			 log = require('metalogger')(),
		 request = require("request");


// demande de connexion d'un utilisateur
exports.interfaceConnexion = function(req, liste_parametres, callback) {
	var _connexion_courante;
	var utilisateur_connecte;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/loginCompte.php",
		method: "POST",
		form: liste_parametres
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			log.notice("retour_json webwervice PHP : " + body);
			var retour_json = JSON.parse(body);

			if (retour_json.status == "ok") {
				utilisateur_connecte = {
					id_utilisateur: retour_json.id_utilisateur,
					pseudo: retour_json.pseudo,
					avatar_url: retour_json.avatar_url,
					email: retour_json.email,
					est_admin_pairform: retour_json.est_admin_pairform
				};

				pairform_dao.getConnexionBDD(function (connexion) {
					_connexion_courante = connexion;
					selectRolesByEmail(retour_json.email);
				});
			} else {
				callback(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
			}
		}
	});

	// récupère les roles de l'utilisateur connecté dans chaque espace
	function selectRolesByEmail(email) {
		pairform_dao.role_dao.selectRolesByEmail(
			email,
			function (liste_roles) {
				// si l'utilisateur à au moins un rôle dans le Backoffice
				if(liste_roles.length > 0) {
					utilisateur_connecte.liste_roles = {};
					// pour chaque rôle de l'utilisateur
					for (var clef in liste_roles) {
						utilisateur_connecte.liste_roles[liste_roles[clef].id_espace] = liste_roles[clef];
					};
				}

				// si l'utilisateur à au moins un rôle dans le Backoffice OU est un admin PairForm 
				if(liste_roles.length > 0 || utilisateur_connecte.est_admin_pairform) {
					// connexion avec passport, une session node.js sera crée
					req.login(
						utilisateur_connecte,
						function(erreur) {
							if (erreur) {
								log.error("passport login erreur : " + erreur);
								return callbackError();
							} else {
								pairform_dao.libererConnexionBDD(_connexion_courante);
								return callback({status: constantes.STATUS_JSON_OK, utilisateur_connecte: req.user});
							}
						}
					);
				} else {
					// sinon, l'utilisateur n'est pas autorisé à accèder au Backoffice
					callback(constantes.RETOUR_JSON_UTILISATEUR_NON_AUTORISE);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}
};


// demande de déconnexion d'un utilisateur
exports.interfaceDeconnexion = function(req, liste_parametres, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/logoutCompte.php",
		method: "POST",
		form: liste_parametres
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError();
		} else {
			log.notice("retour_json webservice PHP : " + body);
			var retour_json = JSON.parse(body);

			if (retour_json.status == "ok") {
				req.logout();
				callback(constantes.RETOUR_JSON_OK);
			} else {
				callbackError();
			}
		}
	});
};


// reinitialisation du mot de passe d'un utilisateur
exports.reinitialisationMotDePasse = function(liste_parametres, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// envoie d'une requete au webservice PHP pour une connexion avec Elgg
	request({
		uri: constantes.URL_WEBSERVICES_PHP +"/compte/resetPassword.php",
		method: "POST",
		form: liste_parametres
	}, function(erreur, response, body) {
		if (erreur) {
			callbackError
		} else {
			var retour_json = JSON.parse(body);

			callback(retour_json);
		}
	});
};
