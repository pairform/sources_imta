"use strict";

var pairform_dao = require("pairform_dao"),
	web_services = require("../../webServices"),
	  constantes = require("../../constantes"),
			  fs = require("fs");

var LISTE_GROUPES_OBLIGATOIRES = [constantes.GROUPE_SPHERE_PUBLIQUE, constantes.GROUPE_ESPACE];													// liste des groupes obligatoires dans un espace
var LISTE_ROLES_PAR_DEFAUT = [constantes.ROLE_RESPONSABLE_CAPSULE, constantes.ROLE_RESPONSABLE_RESSOURCE, constantes.ROLE_RESPONSABLE_ESPACE];	// liste des roles par défaut d'un espace


// Récupération d'un espace
exports.getEspace = function(id_espace, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		selectEspace();
	});

	function selectEspace() {
		pairform_dao.espace_dao.selectEspace(
			id_espace,
			function (espace) {
				if(espace.logo) {
					// encodage du logo en base 64
					espace.logo = new Buffer( espace.logo ).toString("base64");
				} else {
					// ajout d'un logo par défaut, encodé en base64
					espace.logo = fs.readFileSync(constantes.URL_IMG_LOGO_PAR_DEFAUT, "base64");
				}

				retour_json.espace = espace;
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Création d'un nouvelle espace
exports.putEspace = function(liste_parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		insertEspace();
	});

	function insertEspace() {
		pairform_dao.espace_dao.insertEspace(
			liste_parametres.nom_court,
			liste_parametres.nom_long,
			function (reponse_sql) {
				// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
				EnregistrementLogo(reponse_sql.id_espace);
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function EnregistrementLogo(id_espace) {
		var logo_blob, url_logo;

		// si un logo est défini pour l'espace
		if (liste_parametres.logo) {
			logo_blob = new Buffer(liste_parametres.logo, "base64");					// on transforme sa représentation base64 en hexadécimale
			url_logo = constantes.REPERTOIRE_LOGOS_ESPACES + id_espace +".png";
			fs.writeFileSync(url_logo, logo_blob);										// stockage du logo dans le repertoire dédié
		} else {
			url_logo = constantes.URL_IMG_LOGO_PAR_DEFAUT;
		}
		
		pairform_dao.espace_dao.updateLogoEspace(
			id_espace,
			url_logo,
			logo_blob,
			function () {
				// on insert le premier groupe de la liste des groupes obligatoires dans le nouvelle espace
				insertGroupe(0, id_espace);
			},
			callbackError
		);
	}

	// création des groupes obligatoires pour le nouvelle espace
	function insertGroupe(index_groupe, id_espace) {
		pairform_dao.groupe_dao.insertGroupe(
			LISTE_GROUPES_OBLIGATOIRES[index_groupe],
			id_espace,
			true,
			function () {
				index_groupe++;
				// SI il reste des groupes obligatoires à insérer
				if (index_groupe < LISTE_GROUPES_OBLIGATOIRES.length) {
					insertGroupe(index_groupe, id_espace);
				} else {
					// on insert le premier role de la liste des roles par défaut dans le nouvelle espace
					insertRole(0, id_espace);
				}
			},
			callbackError
		);
	}

	// création des rôles par défaut pour le nouvelle espace
	function insertRole(index_role, id_espace) {
		pairform_dao.role_dao.insertRole(
			id_espace,
			LISTE_ROLES_PAR_DEFAUT[index_role].admin_espace,
			LISTE_ROLES_PAR_DEFAUT[index_role].nom,
			LISTE_ROLES_PAR_DEFAUT[index_role].editer_espace,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_groupes,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_roles,
			LISTE_ROLES_PAR_DEFAUT[index_role].attribuer_roles,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_ressources_et_capsules,
			LISTE_ROLES_PAR_DEFAUT[index_role].editer_toutes_ressources_et_capsules,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_visibilite_capsule,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_visibilite_toutes_capsules,
			function () {
				index_role++;
				// SI il reste des rôles par défaut à insérer
				if (index_role < LISTE_ROLES_PAR_DEFAUT.length) {
					insertRole(index_role, id_espace);
				} else {
					retour_json.id_espace = id_espace;					
					callback(retour_json);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}
};


// Mise à jour d'un espace
exports.postEspace = function(liste_parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_KO};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		updateEspace();
	});

	function updateEspace() {		
		pairform_dao.espace_dao.updateEspace(
			liste_parametres.id_espace,
			liste_parametres.nom_court,
			liste_parametres.nom_long,
			function () {
				EnregistrementLogo();
			},
			function (erreur_sql) {
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function EnregistrementLogo() {
		var logo_blob, url_logo;

		logo_blob = new Buffer(liste_parametres.logo, "base64");		// on transforme la représentation base64 en hexadécimale
		url_logo = constantes.REPERTOIRE_LOGOS_ESPACES + liste_parametres.id_espace +".png";
		fs.writeFileSync(url_logo, logo_blob);							// stockage du logo dans le repertoire dédié
		
		pairform_dao.espace_dao.updateLogoEspace(
			liste_parametres.id_espace,
			url_logo,
			logo_blob,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Suppression d'un espace
exports.deleteEspace = function(id_espace, callback) {
	var _connexion_courante;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		deleteRessources();
	});

	// supprime les ressources contenues dans l'espace à supprimer
	function deleteRessources() {
		pairform_dao.ressource_dao.selectRessourcesByEspaces([{
				id_espace: id_espace
			}],
			function (liste_ressources) {
				if (liste_ressources.length > 0) {
					web_services.ressource_backoffice.deleteRessources({
							liste_ressources: liste_ressources,
							id_espace: id_espace
						},
						function () {
							deleteGroupes();
						},
						callbackError
					);
				}
				else {
					deleteGroupes();
				}
			},
			callbackError
		);
	}

	// supprime les groupes de l'espace et leurs membres
	function deleteGroupes() {
		// recupère les membres des groupes de l'espace
		pairform_dao.membre_dao.selectMembresByIdEspace(
			id_espace,
			null,
			function (liste_membres) {
				if (liste_membres.length > 0) {
					// supprime les membres des groupes
					pairform_dao.groupe_dao.deleteMembresGroupeEspace(
						liste_membres,
						function () {
							// supprime les membres
							pairform_dao.membre_dao.deleteMembres(
								liste_membres,
								function () {
									// supprime les groupes
									pairform_dao.groupe_dao.deleteGroupesByEspace(
										id_espace,
										function () {
											deleteRoles();
										},
										callbackError
									);
								},
								callbackError
							);
						},
						callbackError
					);
				}
				else {
					// supprime les groupes
					pairform_dao.groupe_dao.deleteGroupesByEspace(
						id_espace,
						function () {
							deleteRoles();
						},
						callbackError
					);
				}
			},
			callbackError
		);
	}

	// supprime les roles backoffice de l'espace
	function deleteRoles() {
		pairform_dao.role_dao.deleteRolesByEspace(
			id_espace,
			function () {
				deleteEspace();
			},
			callbackError
		);
	}

	// supprime l'espace
	function deleteEspace() {
		pairform_dao.espace_dao.deleteEspace(
			id_espace,
			function () {
				// si un fichier logo existe pour l'espace, on le supprime
				if ( fs.existsSync(constantes.REPERTOIRE_LOGOS_RESSOURCES + id_espace +".png") )
					fs.unlinkSync(constantes.REPERTOIRE_LOGOS_RESSOURCES + id_espace +".png");

				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};