"use strict";

var pairform_dao = require("pairform_dao"),
	web_services = require("../../webServices"),
	  constantes = require("../../constantes"),
			  fs = require("fs");


// Récupération d'un ressource
exports.getRessource = function(id_ressource, code_langue, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		selectRessource();
	});

	function selectRessource() {
		pairform_dao.ressource_dao.selectRessource(
			id_ressource,
			function (ressource) {
				if(ressource.logo) {
					// encodage du logo en base 64
					ressource.logo = new Buffer( ressource.logo ).toString("base64");
				} else {
					// ajout d'un logo par défaut, encodé en base64
					ressource.logo = fs.readFileSync(constantes.URL_IMG_LOGO_PAR_DEFAUT, "base64");
				}

				retour_json.ressource = ressource;
				selectAllThemes();
			},
			callbackError
		);
	}

	// Récupére toutes les thèmes (traduit dans la langue choisi)
	function selectAllThemes() {
		pairform_dao.theme_dao.selectAllThemes(
			code_langue,
			function (liste_themes) {
				retour_json.liste_themes = liste_themes;
				selectAllUsages();
			},
			callbackError
		);
	}

	// Récupére toutes les usages (traduits dans la langue choisie)
	function selectAllUsages() {
		pairform_dao.usage_dao.selectAllUsages(
			function (liste_usages) {
				retour_json.liste_usages = liste_usages;
				selectAllLangues();
			},
			callbackError
		);
	}

	// Récupére toutes les langues (traduit dans la langue choisi)
	function selectAllLangues() {
		pairform_dao.langue_dao.selectAllLangues(
			code_langue,
			function (liste_langues) {
				retour_json.liste_langues = liste_langues;
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Création d'un nouvelle ressource
exports.putRessource = function(liste_parametres, cree_par, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		insertRessource();
	});

	function insertRessource() {
		var logo_blob;

		// si un logo est défini pour la ressource, on transforme sa représentatio base64 en hexadécimale
		if (liste_parametres.logo) {
			logo_blob = new Buffer(liste_parametres.logo, "base64");
		}

		pairform_dao.ressource_dao.insertRessource(
			liste_parametres.nom_court,
			liste_parametres.id_espace,
			liste_parametres.nom_long,
			liste_parametres.description,
			liste_parametres.id_theme,
			liste_parametres.id_usage,
			liste_parametres.id_langue,
			cree_par,
			function (reponse_sql) {
				retour_json.id_ressource = reponse_sql.id_ressource;
				
				EnregistrementLogo(reponse_sql.id_ressource);				
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;

				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function EnregistrementLogo(id_ressource) {
		var logo_blob, url_logo;

		// si un logo est défini pour la ressource
		if (liste_parametres.logo) {
			logo_blob = new Buffer(liste_parametres.logo, "base64");					// on transforme sa représentation base64 en hexadécimale
			url_logo = constantes.REPERTOIRE_LOGOS_RESSOURCES + id_ressource +".png";
			fs.writeFileSync(url_logo, logo_blob);										// stockage du logo dans le repertoire dédié
		} else {
			url_logo = constantes.URL_IMG_LOGO_PAR_DEFAUT;
		}
		
		pairform_dao.ressource_dao.updateLogoRessource(
			id_ressource,
			url_logo,
			logo_blob,
			function () {
				// si le createur de la ressource n'est pas l'admin pairform
				if (cree_par != constantes.ID_UTILISATEUR_ADMIN_PAIRFORM) {
					insertCreateurRessource(id_ressource);
				} else {
					insertAdmin(id_ressource);
				}
			},
			callbackError
		);
	}

	// le créateur de la ressource est défini comme expert de cette nouvelle ressource
	function insertCreateurRessource(id_ressource) {
		pairform_dao.utilisateur_dao.insertCategorieUtilisateur(
			cree_par,
			id_ressource,
			constantes.CATEGORIE_EXPERT,
			function () {
				insertAdmin(id_ressource)
			},
			callbackError
		);
	}

	// l'admin pairform est défini comme admin de la nouvelle ressource
	function insertAdmin(id_ressource) {
		pairform_dao.utilisateur_dao.insertCategorieUtilisateur(
			constantes.ID_UTILISATEUR_ADMIN_PAIRFORM,
			id_ressource,
			constantes.CATEGORIE_ADMIN,
			function () {
				// si la ressource est dédié à un usage de formation, on associe à la ressource la liste de questions/réponses du profil d'apprentissage
				if (liste_parametres.id_usage == constantes.USAGE_FORMATION) {
					insertQuestion(id_ressource, 0);
				} else {
					callback(retour_json);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// créé une question dans le profil d'apprentissage d'une ressource
	function insertQuestion(id_ressource, index_question) {
		pairform_dao.profil_apprentissage_dao.insertQuestion(
			id_ressource,
			function (reponse_sql) {
				insertTraductionQuestion(id_ressource, index_question, reponse_sql.id_question);
			},
			callbackError
		);
	}

	// créé la traduction d'une question du profil d'apprentissage d'une ressource
	function insertTraductionQuestion(id_ressource, index_question, id_question) {
		pairform_dao.profil_apprentissage_dao.insertTraductionQuestion(
			id_question,
			constantes.PROFIL_APPRENTISSAGE[index_question].id_langue,
			constantes.PROFIL_APPRENTISSAGE[index_question].question,
			function () {
				insertReponse(id_ressource, index_question, id_question, 0);
			},
			callbackError
		);
	}

	// créé une réponse à une reponse du profil d'apprentissage d'une ressource
	function insertReponse(id_ressource, index_question, id_question, index_reponse) {
		pairform_dao.profil_apprentissage_dao.insertReponse(
			id_question,
			function (reponse_sql) {
				insertTraductionReponse(id_ressource, index_question, id_question, index_reponse, reponse_sql.id_reponse);
			},
			callbackError
		);
	}

	// créé la traduction d'une reponse du profil d'apprentissage d'une ressource
	function insertTraductionReponse(id_ressource, index_question, id_question, index_reponse, id_reponse) {
		pairform_dao.profil_apprentissage_dao.insertTraductionReponse(
			id_reponse,
			constantes.PROFIL_APPRENTISSAGE[index_question].id_langue,
			constantes.PROFIL_APPRENTISSAGE[index_question].reponses[index_reponse],
			function () {
				index_reponse++;
				// s'il y a encore des réponses à ajouter
				if(index_reponse < constantes.PROFIL_APPRENTISSAGE[index_question].reponses.length) {
					insertReponse(id_ressource, index_question, id_question, index_reponse);
				} else {
					index_question++;
					// s'il y a encore des questions à ajouter
					if (index_question < constantes.PROFIL_APPRENTISSAGE.length) {
						insertQuestion(id_ressource, index_question);
					} else {
						callback(retour_json);
						pairform_dao.libererConnexionBDD(_connexion_courante);
					}
				}
			},
			callbackError
		);
	}
};


// Mise à jour d'un ressource
exports.postRessource = function(liste_parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_KO};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		updateRessource();
	});

	function updateRessource() {
		var logo_blob;

		// si un logo est défini pour la ressource, on transforme sa représentation base64 en hexadécimale
		if (liste_parametres.logo) {
			logo_blob = new Buffer(liste_parametres.logo, "base64");
		}
		
		pairform_dao.ressource_dao.updateRessource(
			liste_parametres.id_ressource,
			liste_parametres.nom_court,
			liste_parametres.id_espace,
			liste_parametres.nom_long,
			liste_parametres.description,
			liste_parametres.id_theme,
			liste_parametres.id_langue,
			function () {
				EnregistrementLogo();
			},
			function (erreur_sql) {
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function EnregistrementLogo() {
		var logo_blob, url_logo;

		logo_blob = new Buffer(liste_parametres.logo, "base64");		// on transforme la représentation base64 en hexadécimale
		url_logo = constantes.REPERTOIRE_LOGOS_RESSOURCES + liste_parametres.id_ressource +".png";
		fs.writeFileSync(url_logo, logo_blob);							// stockage du logo dans le repertoire dédié
		
		pairform_dao.ressource_dao.updateLogoRessource(
			liste_parametres.id_ressource,
			url_logo,
			logo_blob,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Suppression d'une liste de ressources
exports.deleteRessources = function(liste_parametres, callback) {
	var _connexion_courante;
	var liste_ressources = liste_parametres.liste_ressources,
			   id_espace = liste_parametres.id_espace;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		deleteCapsules();
	});

	// supprime les capsules contenues dans les ressources à supprimer
	function deleteCapsules() {
		pairform_dao.capsule_dao.selectCapsulesByRessources(
			liste_ressources,
			function (liste_capsules) {
				if (liste_capsules.length > 0) {
					web_services.capsule_backoffice.deleteCapsules({
							liste_capsules: liste_capsules,
							id_espace: id_espace
						},
						function () {
							deleteUtilisateursCategorie();
						},
						callbackError
					);
				}
				else {
					deleteUtilisateursCategorie();
				}
			},
			callbackError
		);
	}	

	// supprime les role des utilisateurs associés aux ressources
	function deleteUtilisateursCategorie() {
		pairform_dao.utilisateur_dao.deleteUtilisateursCategorieByRessources(
			liste_ressources,
			function () {
				deleteObjectifsApprentissage();
			},
			callbackError
		);
	}

	// supprime les objectifs d'apprentissage
	function deleteObjectifsApprentissage() {
		pairform_dao.objectif_apprentissage_dao.deleteObjectifsApprentissageByRessources(
			liste_ressources,
			function () {
				deleteProfilsApprentissage();
			},
			callbackError
		);
	}

	// supprime les profils d'apprentissage
	function deleteProfilsApprentissage() {
		// supprime les profils d'apprentissage des utilisateurs sur une liste de ressources
		pairform_dao.profil_apprentissage_dao.deleteProfilsApprentissageUtilisateursByRessources(
			liste_ressources,
			function () {
				// supprime les traductions des réponses au profil d'apprentissage des utilisateurs pour une liste de ressources
				pairform_dao.profil_apprentissage_dao.deleteTraductionsReponsesByRessources(
					liste_ressources,
					function () {
						// supprime les réponses au profil d'apprentissage des utilisateurs pour une liste de ressources
						pairform_dao.profil_apprentissage_dao.deleteReponsesByRessources(
							liste_ressources,
							function () {
								// supprime les traductions des questions au profil d'apprentissage des utilisateurs pour une liste de ressources
								pairform_dao.profil_apprentissage_dao.deleteTraductionsQuestionsByRessources(
									liste_ressources,
									function () {
										// supprime les questions au profil d'apprentissage des utilisateurs pour une liste de ressources
										pairform_dao.profil_apprentissage_dao.deleteQuestionsByRessources(
											liste_ressources,
											function () {
												deleteRessources();
											},
											callbackError
										);
									},
									callbackError
								);
							},
							callbackError
						);
					},
					callbackError
				);
			},
			callbackError
		);
	}

	// supprime les ressources
	function deleteRessources() {
		pairform_dao.ressource_dao.deleteRessources(
			liste_ressources,
			function () {
				// suppression des logos
				for (var i in liste_ressources) {
					// si un fichier logo existe pour la ressource courante, on le supprime
					if ( fs.existsSync(constantes.REPERTOIRE_LOGOS_RESSOURCES + liste_ressources[i].id_ressource +".png") )
						fs.unlinkSync(constantes.REPERTOIRE_LOGOS_RESSOURCES + liste_ressources[i].id_ressource +".png");
				}

				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};
