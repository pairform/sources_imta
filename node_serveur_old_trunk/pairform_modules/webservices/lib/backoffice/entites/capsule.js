"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
			 log = require('metalogger')(),
		 adler32 = require('adler32'),
		  AdmZip = require("adm-zip"),
			  fs = require("fs"),
		 request = require("request");


// Récupération d'une capsule
exports.getCapsule = function(id_capsule, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		selectCapsule();
	});

	function selectCapsule() {
		pairform_dao.capsule_dao.selectCapsule(
			id_capsule,
			function (capsule) {
				retour_json.capsule = capsule;

				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// récupèration des informations sur la visibilité d'une capsule (pour chaque groupe de l'espace)
exports.getCapsuleVisibilite = function(id_capsule, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		selectCapsuleVisibilite();
	});

	function selectCapsuleVisibilite() {
		pairform_dao.capsule_dao.selectCapsuleVisibilite(
			id_capsule,
			function (liste_visibilite_groupe) {
				retour_json.liste_visibilite_groupe = liste_visibilite_groupe;
				selectAllVisibilites();
			},
			callbackError
		);
	}

	function selectAllVisibilites() {
		pairform_dao.capsule_dao.selectAllVisibilites(
			function (liste_visibilites) {
				retour_json.liste_visibilites = liste_visibilites;

				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Création d'un nouvelle capsule
exports.putCapsule = function(liste_parametres, cree_par, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		insertCapsule();
	});

	function insertCapsule() {
		pairform_dao.capsule_dao.insertCapsule(
			liste_parametres.id_ressource,
			liste_parametres.nom_court,
			liste_parametres.nom_long,
			liste_parametres.url_mobile,
			liste_parametres.url_web,
			liste_parametres.poid,
			liste_parametres.description,
			liste_parametres.licence,
			liste_parametres.auteurs,
			cree_par,
			function (reponse_sql) {
				retour_json.id_capsule = reponse_sql.id_capsule;
				updateClefPairFormCapsule(retour_json.id_capsule);
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;

				callback(retour_json);
			}
		);
	}

	// ajoute la clef pairform de la capsule et met à jour les urls mobile et web (elles peuvent avoir été modifié si une capsule a été uploadé)
	function updateClefPairFormCapsule(id_capsule) {
		var buffer_id_capsule = new Buffer(id_capsule);
		var clef_pairform = adler32.sum(buffer_id_capsule).toString(16);	// hash de l'id avec Adler-32 pour obtenir la clef pairform

		pairform_dao.capsule_dao.updateClefPairFormCapsule(
			id_capsule,
			clef_pairform,
			function () {
				// si des archives doivent être uploadés
				if (liste_parametres.archive_mobile || liste_parametres.archive_web) {
					dezipperArchives(id_capsule)
				} else {
					selectGroupesObligatoiresByIdRessource(id_capsule)
				}
			},
			callbackError
		);
	}

	// dezippe les archives (mob, puis web) de la capsule uploadé
	function dezipperArchives(id_capsule) {
		dezipperArchive(liste_parametres.archive_mobile, liste_parametres.id_espace, liste_parametres.id_ressource, retour_json.id_capsule, "mob", liste_parametres.url_mobile, function(url_mobile) {
			dezipperArchive(liste_parametres.archive_web, liste_parametres.id_espace, liste_parametres.id_ressource, retour_json.id_capsule, "web", liste_parametres.url_web, function(url_web) {
				pairform_dao.capsule_dao.updateUrlsCapsule(
					id_capsule,
					url_mobile,
					url_web,
					function () {
						selectGroupesObligatoiresByIdRessource(id_capsule);
					},
					callbackError
				);
			});
		});	
	}
	
	// Récupère les groupes obligatoires d'un espace via l'id d'une ressource de cet espace (groupes obligatoires : sphère publique, espace...)
	function selectGroupesObligatoiresByIdRessource(id_capsule) {
		pairform_dao.groupe_dao.selectGroupesObligatoiresByIdRessource(
			liste_parametres.id_ressource,
			function (liste_groupes_obligatoires) {
				// on insert la visibilité par défaut pour le premier groupe de la liste des groupes obligatoires
				insertCapsuleVisibilite(liste_groupes_obligatoires, 0, id_capsule);
			},
			callbackError
		);
	}

	function insertCapsuleVisibilite(liste_groupes_obligatoires, index_groupe, id_capsule) {
		pairform_dao.capsule_dao.insertCapsuleVisibilite(
			id_capsule,
			liste_groupes_obligatoires[index_groupe].id_groupe,
			constantes.VISIBILITE_INVISIBLE,
			function () {
				index_groupe++;
				// SI il reste des groupes obligatoires à insérer
				if (index_groupe < liste_groupes_obligatoires.length) {
					insertCapsuleVisibilite(liste_groupes_obligatoires, index_groupe, id_capsule);
					callback(retour_json);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				} else {					
				}
			},
			callbackError
		);
	}
};


// Mise à jour d'un capsule
exports.postCapsule = function(liste_parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		updateCapsule();
	});

	function updateCapsule() {
		pairform_dao.capsule_dao.updateCapsule(
			liste_parametres.id_capsule,
			liste_parametres.id_ressource,
			liste_parametres.nom_court,
			liste_parametres.nom_long,
			liste_parametres.url_mobile,
			liste_parametres.url_web,
			liste_parametres.poid,
			liste_parametres.description,
			liste_parametres.licence,
			liste_parametres.auteurs,
			function (reponse_sql) {
				// si des archives doivent être uploadés
				if (liste_parametres.archive_mobile || liste_parametres.archive_web) {
					dezipperArchives()				
				} else {
					callback(retour_json);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}				
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;

				callback(retour_json);
			}
		);
	}

	// dezippe les archives (mob, puis web) de la capsule uploadé
	function dezipperArchives() {
		dezipperArchive(liste_parametres.archive_mobile, liste_parametres.id_espace, liste_parametres.id_ressource, liste_parametres.id_capsule, "mob", liste_parametres.url_mobile, function(url_mobile) {
			dezipperArchive(liste_parametres.archive_web, liste_parametres.id_espace, liste_parametres.id_ressource, liste_parametres.id_capsule, "web", liste_parametres.url_web, function(url_web) {
				pairform_dao.capsule_dao.updateUrlsCapsule(
					liste_parametres.id_capsule,
					url_mobile,
					url_web,
					function () {
						callback(retour_json);
						pairform_dao.libererConnexionBDD(_connexion_courante);
					},
					callbackError
				);
			});
		});	
	}
};


// Mise à jour de la visibilité d'une capsule
exports.postCapsuleVisibilite = function(visibilite_capsule, callback) {
	var _connexion_courante;
	
	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		updateCapsuleVisibiliteSpherePublique();
	});

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// Mise à jour de la visibilité de la capsule pour le groupe obligatoire Sphere publique
	function updateCapsuleVisibiliteSpherePublique() {
		pairform_dao.capsule_dao.updateCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.sphere_publique.id_groupe,
			visibilite_capsule.sphere_publique.id_visibilite,
			function () {
				updateCapsuleVisibiliteEspace();
			},
			callbackError
		);
	}

	// Mise à jour de la visibilité de la capsule pour le groupe obligatoire Sphere publique
	function updateCapsuleVisibiliteEspace() {
		pairform_dao.capsule_dao.updateCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.espace.id_groupe,
			visibilite_capsule.espace.id_visibilite,
			function () {
				// si un groupe non-obligatoire avait déjà une visibilité sur la capsule
				if (visibilite_capsule.id_ancien_groupe_selectionne) {
					deleteCapsuleVisibilite();
				}
				// sinon, si un nouveau groupe non-obligatoire doit avoir une visibilité sur la capsule
				else if (visibilite_capsule.nouveau_groupe_selectionne) {
					insertCapsuleVisibilite();
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// Suppression la visibilité de la capsule pour un groupe
	function deleteCapsuleVisibilite() {
		pairform_dao.capsule_dao.deleteCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.id_ancien_groupe_selectionne,
			function () {
				// si un nouveau groupe non-obligatoire doit avoir une visibilité sur la capsule
				if (visibilite_capsule.nouveau_groupe_selectionne) {
					insertCapsuleVisibilite();
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// Ajout de la visibilité de la capsule pour un nouveau groupe
	function insertCapsuleVisibilite() {
		pairform_dao.capsule_dao.insertCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.nouveau_groupe_selectionne.id_groupe,
			visibilite_capsule.nouveau_groupe_selectionne.id_visibilite,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
}


// Suppression d'une liste de capsules
exports.deleteCapsules = function(liste_parametres, callback) {
	var _connexion_courante;
	var liste_capsules = liste_parametres.liste_capsules,
			 id_espace = liste_parametres.id_espace;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		deleteCapsulesVisibilite();
	});

	// supprime les visibilités
	function deleteCapsulesVisibilite() {
		pairform_dao.capsule_dao.deleteCapsulesVisibilite(
			liste_capsules,
			function () {
				deleteUtilisateursCapsulesParametres();
			},
			callbackError
		);
	}

	// supprime les parametres des utilisateurs
	function deleteUtilisateursCapsulesParametres() {
		pairform_dao.utilisateur_dao.deleteUtilisateursCapsulesParametresByCapsules(
			liste_capsules,
			function () {
				deleteMessagesCapsules();
			},
			callbackError
		);
	}

	// supprime les messages (donc, supprime aussi les notifications, les votes et les tags liés à ces messages) - pyramid of doom !
	function deleteMessagesCapsules() {
		// suppression notifications
		pairform_dao.notification_dao.deleteNofificationsByCapsules(
			liste_capsules,
			function () {
				// suppression votes
				pairform_dao.vote_dao.deleteVotesByCapsules(
					liste_capsules,
					function () {
						// suppression tags
						pairform_dao.tag_dao.deleteTagsByCapsules(
							liste_capsules,
							function () {
								// suppression messages
								pairform_dao.message_dao.deleteMessagesByCapsules(
									liste_capsules,
									function () {
										deleteCapsules();
									},
									callbackError
								);
							},
							callbackError
						);
					},
					callbackError
				);
			},
			callbackError
		);
	}

	// supprime les capsules
	function deleteCapsules() {
		// suppression en BDD des capsules
		pairform_dao.capsule_dao.deleteCapsules(
			liste_capsules,
			function () {
				// suppression physique des capsules = supprime les fichiers stockés sur le serveur (logo, pages html, etc.)
				for (var i in liste_capsules) {
					// si la capsule existe, on la supprime (version web et version mobile)
					if (fs.existsSync(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ liste_capsules[i].id_ressource +"/"+ liste_capsules[i].id_capsule)) {
						// suppression du repertoire contenant la capsule
						supprimerRepertoire(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ liste_capsules[i].id_ressource +"/"+ liste_capsules[i].id_capsule);
					}					
				}

				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// dezippe l'archive de la capsule uploadé
function dezipperArchive(archive, id_espace, id_ressource, id_capsule, version, url, callback) {
	// si une archive a été uploadé
	if (archive) {
		var buffer_archive = new Buffer(archive, "base64");
		var zip = new AdmZip(buffer_archive);

		// si la capsule existe déjà, on la supprime
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version)) {
			// Suppression de l'ancienne version de la capsule
			supprimerRepertoire(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version);
			// suppression de l'archive zip de la capsule
			if (fs.existsSync(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version +".zip")) {
				fs.unlinkSync(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version +".zip");
			}
		}

		// dézippage de la capsule capsule dans : constantes.REPERTOIRE_CAPSULE_ABSOLUE id_espace / id_ressource / id_capsule / version
		zip.extractAllTo(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version, true);
		
		if (version == "mob") {
			fs.writeFileSync(constantes.REPERTOIRE_CAPSULE_ABSOLUE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version +".zip", buffer_archive);
		}
		url = constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version;
	}
	callback(url);
}


// Supprime récursivement un repertoire
function supprimerRepertoire(chemin) {
	var fichiers = [];

	if( fs.existsSync(chemin) ) {
		fichiers = fs.readdirSync(chemin);

		fichiers.forEach( function(fichier,index) {
			var chemin_courant = chemin + "/" + fichier;
			
			if(fs.lstatSync(chemin_courant).isDirectory()) {
				supprimerRepertoire(chemin_courant);
			} else {
				// supprimer fichier associé au chemin courant
				fs.unlinkSync(chemin_courant);
			}
		});
		fs.rmdirSync(chemin);
	}
};
