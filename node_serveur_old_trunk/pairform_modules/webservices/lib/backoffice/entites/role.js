"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes");


// Récupération de la liste des roles via son espace
exports.getRolesByIdEspace = function(id_espace, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		selectRolesByIdEspace();
	});

	function selectRolesByIdEspace() {
		pairform_dao.role_dao.selectRolesByIdEspace(
			id_espace,
			function (liste_roles) {
				retour_json.liste_roles = liste_roles;
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Mise à jour de la liste des roles d'un espace
exports.postRoles = function(liste_parametres, callback) {
	var _connexion_courante;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;

		if(liste_parametres.liste_roles_modifies.length > 0) {				// s'il y a des anciens rôles modifies
			updateRole(0);
		} else if(liste_parametres.liste_roles_supprimes.length > 0) {		// sinon, s'il y a des anciens rôles à supprimer
			updateMembresRoleByIdRole(0);
		} else if(liste_parametres.liste_nouveaux_roles.length > 0) {		// sinon, s'il y a des nouveaux rôles à ajouter
			insertRole(0);
		} else {
			callback(constantes.RETOUR_JSON_OK);
			pairform_dao.libererConnexionBDD(_connexion_courante);
		}
	});

	// Mise à jour des roles modifiés
	function updateRole(index_role) {
		pairform_dao.role_dao.updateRole(
			liste_parametres.liste_roles_modifies[index_role].id_role,
			liste_parametres.liste_roles_modifies[index_role].nom,
			liste_parametres.liste_roles_modifies[index_role].editer_espace,
			liste_parametres.liste_roles_modifies[index_role].gerer_groupes,
			liste_parametres.liste_roles_modifies[index_role].gerer_roles,
			liste_parametres.liste_roles_modifies[index_role].attribuer_roles,
			liste_parametres.liste_roles_modifies[index_role].gerer_ressources_et_capsules,
			liste_parametres.liste_roles_modifies[index_role].editer_toutes_ressources_et_capsules,
			liste_parametres.liste_roles_modifies[index_role].gerer_visibilite_capsule,
			liste_parametres.liste_roles_modifies[index_role].gerer_visibilite_toutes_capsules,
			function () {
				index_role++;
				// S'il reste des nouveaux rôles à ajouter en BDD
				if (index_role < liste_parametres.liste_roles_modifies.length) {
					updateRole(index_role);
				} else {
					if(liste_parametres.liste_roles_supprimes.length > 0) {				// s'il y a des anciens rôles à supprimer
						updateMembresRoleByIdRole(0);
					} else if(liste_parametres.liste_nouveaux_roles.length > 0) {		// sinon, s'il y a des nouveaux rôles à ajouter
						insertRole(0);
					} else {
						callback(constantes.RETOUR_JSON_OK);
						pairform_dao.libererConnexionBDD(_connexion_courante);
					}
				}
			},
			callbackError
		);
	}

	// retire un rôle à une liste de membres
	function updateMembresRoleByIdRole(index_role) {
		pairform_dao.membre_dao.updateMembresRoleByIdRole(
			liste_parametres.liste_roles_supprimes[index_role].id_role,
			null,
			function () {
				index_role++;
				// S'il reste des rôles à retirés à des membres en BDD
				if (index_role < liste_parametres.liste_roles_supprimes.length) {
					updateMembresRoleByIdRole(index_role);
				} else {
					deleteRoles();
				}
			},
			callbackError
		);
	}

	// suppression d'anciens rôles de l'espace
	function deleteRoles() {
		pairform_dao.role_dao.deleteRoles(
			liste_parametres.liste_roles_supprimes,
			function () {
				// S'il y a des nouveaux rôles à ajouter
				if (liste_parametres.liste_nouveaux_roles.length > 0) {
					insertRole(0);
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// ajout des nouveaux roles dans l'espace
	function insertRole(index_role) {
		pairform_dao.role_dao.insertRole(
			liste_parametres.id_espace,
			false,
			liste_parametres.liste_nouveaux_roles[index_role].nom,
			liste_parametres.liste_nouveaux_roles[index_role].editer_espace,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_groupes,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_roles,
			liste_parametres.liste_nouveaux_roles[index_role].attribuer_roles,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_ressources_et_capsules,
			liste_parametres.liste_nouveaux_roles[index_role].editer_toutes_ressources_et_capsules,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_visibilite_capsule,
			liste_parametres.liste_nouveaux_roles[index_role].gerer_visibilite_toutes_capsules,
			function () {
				index_role++;
				// S'il reste des nouveaux rôles à ajouter en BDD
				if (index_role < liste_parametres.liste_nouveaux_roles.length) {
					insertRole(index_role);
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}
};


// Mise à jour de la liste des membres nommé à un rôle
exports.postMembresRole = function(liste_parametres, callback) {
	var _connexion_courante;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;

		if (liste_parametres.liste_membres_supprimes.length > 0) {			// S'il y a des membres qui ne sont plus nommés
			updateMembresRoleRetire();
		} else if (liste_parametres.liste_nouveaux_membres.length > 0) {	// Sinon, s'il y a des membres nommés
			updateMembresRoleNomme();
		} else {
			callback(constantes.RETOUR_JSON_OK);
			pairform_dao.libererConnexionBDD(_connexion_courante);
		}
	});

	// retire le rôle d'une liste de membres - le paramètre id_role vaut NULL si l'on veut retirer un role à une liste de membres
	function updateMembresRoleRetire() {
		pairform_dao.membre_dao.updateMembresRole(
			null,
			liste_parametres.liste_membres_supprimes,
			function () {
				// s'il y a des membres nommés
				if (liste_parametres.liste_nouveaux_membres.length > 0) {
					updateMembresRoleNomme();
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// nomme à un rôle une liste de membres
	function updateMembresRoleNomme() {
		pairform_dao.membre_dao.updateMembresRole(
			liste_parametres.id_role,
			liste_parametres.liste_nouveaux_membres,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};