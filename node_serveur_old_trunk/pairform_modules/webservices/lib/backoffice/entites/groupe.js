"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes");


// Récupération de la liste des groupes via son espace
exports.getGroupesByIdEspace = function(id_espace, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		selectGroupesByIdEspace();
	});

	function selectGroupesByIdEspace() {
		pairform_dao.groupe_dao.selectGroupesByIdEspace(
			id_espace,
			function (liste_groupes) {
				retour_json.liste_groupes = liste_groupes;
				// On démarre l'index à 1 car on ne récupère pas les membres du groupe Sphère publique
				selectEmailMembresByIdGroupe(1);
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres d'un groupe
	function selectEmailMembresByIdGroupe(index_groupe) {
		pairform_dao.membre_dao.selectMembresByIdGroupe(
			retour_json.liste_groupes[index_groupe].id_groupe,
			false,
			function (liste_membres) {
				retour_json.liste_groupes[index_groupe].liste_membres_email = liste_membres;
				index_groupe++;
				
				if (index_groupe < retour_json.liste_groupes.length) {
					selectEmailMembresByIdGroupe(index_groupe);
				} else {
					// On démarre l'index à 1 car on ne récupère pas les membres du groupe Sphère publique
					selectNomDomaineMembresByIdGroupe(1);
				}
			},
			callbackError
		);
	}

	// Récupère la liste des noms de domaines des membres d'un groupe (le nom de domaine est compté comme un membre mais peut faire référence à plusieurs utilisateurs)
	function selectNomDomaineMembresByIdGroupe(index_groupe) {
		pairform_dao.membre_dao.selectMembresByIdGroupe(
			retour_json.liste_groupes[index_groupe].id_groupe,
			true,
			function (liste_membres) {
				retour_json.liste_groupes[index_groupe].liste_membres_nom_domaine = liste_membres;
				index_groupe++;
				
				if (index_groupe < retour_json.liste_groupes.length) {
					selectNomDomaineMembresByIdGroupe(index_groupe);
				} else {
					callback(retour_json);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}
};


// Création d'un nouveau groupe
exports.putGroupe = function(liste_parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		insertGroupe();
	});

	function insertGroupe() {
		pairform_dao.groupe_dao.insertGroupe(
			liste_parametres.nom,
			liste_parametres.id_espace,
			liste_parametres.obligatoire,
			function (id_groupe) {
				retour_json.id_groupe = id_groupe;
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			function (erreur_sql) {
				retour_json = constantes.RETOUR_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}
};


// Mise à jour d'un groupe
exports.postGroupe = function(liste_parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_KO};

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		updateGroupe();
	});

	function updateGroupe() {
		pairform_dao.groupe_dao.updateGroupe(
			liste_parametres.nom,
			liste_parametres.id_groupe,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			function (erreur_sql) {
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}
};


// Mise à jour de la liste des membres d'un groupe
exports.postMembres = function(liste_parametres, callback) {
	var _connexion_courante;
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		if (liste_parametres.liste_membres_supprimes.length > 0) {	// S'il y a des membres à supprimer dans le groupe
			// si le groupe concerné est l'espace
			if(liste_parametres.groupe_espace) {
				deleteMembresGroupeEspace();
			} else {
				deleteMembresGroupeByIdGroupe();
			}
		} else if (liste_parametres.liste_nouveaux_membres.length > 0) {		// Sinon, s'il y a des membres à ajouter dans le groupe
			// si le groupe concerné est l'espace
			if(liste_parametres.groupe_espace) {
				insertMembre();
			} else {
				insertMembreGroupe(liste_parametres.liste_nouveaux_membres);
			}
		} else {
			callback(constantes.RETOUR_JSON_OK);
			pairform_dao.libererConnexionBDD(_connexion_courante);
		}
	});

	// cas spécifique au groupe Espace
	// supprime une liste de membres dans le groupe Espace et dans ses sous-groupes
	function deleteMembresGroupeEspace() {
		pairform_dao.groupe_dao.deleteMembresGroupeEspace(
			liste_parametres.liste_membres_supprimes,
			function () {
				deleteMembres();
			},
			callbackError
		);
	}

	// cas spécifique au groupe Espace
	// supprime une liste de membres
	function deleteMembres() {
		pairform_dao.membre_dao.deleteMembres(
			liste_parametres.liste_membres_supprimes,
			function () {
				// S'il y a des membres à ajouter dans le groupe
				if (liste_parametres.liste_nouveaux_membres.length > 0) {
					insertMembre();
				} else {
					selectEmailMembresByIdGroupe();
				}
			},
			callbackError
		);
	}

	// supprime une liste de membres d'un groupe
	function deleteMembresGroupeByIdGroupe() {
		pairform_dao.groupe_dao.deleteMembresGroupeByIdGroupe(
			liste_parametres.id_groupe,
			liste_parametres.liste_membres_supprimes,
			function () {
				// S'il y a des membres à ajouter dans le groupe
				if (liste_parametres.liste_nouveaux_membres.length > 0) {
					insertMembreGroupe(liste_parametres.liste_nouveaux_membres);
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// cas spécifique au groupe Espace
	// créé un membre
	function insertMembre() {
		pairform_dao.membre_dao.insertMembre(
			liste_parametres.liste_nouveaux_membres,
			function (liste_id_membre) {
				insertMembreGroupe(liste_id_membre)
			},
			callbackError
		);
	}

	// ajoute un membre à un groupe
	function insertMembreGroupe(liste_id_nouveaux_membres) {
		pairform_dao.groupe_dao.insertMembreGroupe(
			liste_parametres.id_groupe,
			liste_id_nouveaux_membres,
			function () {				
				// si le groupe concerné est l'espace
				if(liste_parametres.groupe_espace) {
					selectEmailMembresByIdGroupe();
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres de l'espace
	function selectEmailMembresByIdGroupe() {
		pairform_dao.membre_dao.selectMembresByIdGroupe(
			liste_parametres.id_groupe,
			false,
			function (liste_membres) {
				retour_json.liste_membres_email = liste_membres;
				selectNomDomaineMembresByIdGroupe();
			},
			callbackError
		);
	}

	// Récupère la liste des noms de domaines des membres de l'espace
	function selectNomDomaineMembresByIdGroupe() {
		pairform_dao.membre_dao.selectMembresByIdGroupe(
			liste_parametres.id_groupe,
			true,
			function (liste_membres) {				
				retour_json.liste_membres_nom_domaine = liste_membres;

				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Suppression d'un groupe (qui ne peut pas être un groupe obligatoire)
exports.deleteGroupe = function(id_groupe, callback) {
	var _connexion_courante;
	
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		deleteAllMembresGroupeByIdGroupe();
	});

	// on vide le groupe de ses membres
	function deleteAllMembresGroupeByIdGroupe() {
		pairform_dao.groupe_dao.deleteAllMembresGroupeByIdGroupe(
			id_groupe,
			function () {
				sqlDeleteGroupe();
			},
			callbackError
		);
	}

	function sqlDeleteGroupe() {
		pairform_dao.groupe_dao.deleteGroupe(
			id_groupe,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};