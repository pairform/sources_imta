"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes"),
	  async = require("async");


// récupèration des statistiques d'une ressource
exports.getStatistiquesRessource = function(id_ressource, callback) {
	var retour_json = {
		status: constantes.STATUS_JSON_OK, 
		statistiques_ressource: {capsules: {}}
	};
	var liste_nb_messages_capsule, liste_nb_reponses_capsule, liste_nb_votes_positifs_capsule, liste_nb_votes_negatifs_capsule;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		async.parallel([
			// Récupère le nombre d'utilisateurs utilisant une ressource
			function selectNbUtilisateurs(callback_parallel) {
				pairform_dao.utilisateur_dao.selectNbUtilisateursByRessource(
					id_ressource,
					null,
					function (nb_utilisateurs) {
						retour_json.statistiques_ressource.utilisateurs_sur_ressource = nb_utilisateurs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de lecteurs utilisant une ressource
			function selectNbLecteurs(callback_parallel) {
				pairform_dao.utilisateur_dao.selectNbUtilisateursByRessource(
					id_ressource,
					constantes.CATEGORIE_LECTEUR,
					function (nb_utilisateurs) {
						retour_json.statistiques_ressource.lecteurs_sur_ressource = nb_utilisateurs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de participants utilisant une ressource
			function selectNbParticipants(callback_parallel) {
				pairform_dao.utilisateur_dao.selectNbUtilisateursByRessource(
					id_ressource,
					constantes.CATEGORIE_PARTICIPANT,
					function (nb_utilisateurs) {
						retour_json.statistiques_ressource.participants_sur_ressource = nb_utilisateurs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de collaborateurs utilisant une ressource
			function selectNbCollaborateurs(callback_parallel) {
				pairform_dao.utilisateur_dao.selectNbUtilisateursByRessource(
					id_ressource,
					constantes.CATEGORIE_COLLABORATEUR,
					function (nb_utilisateurs) {
						retour_json.statistiques_ressource.collaborateurs_sur_ressource = nb_utilisateurs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre d'animateurs utilisant une ressource
			function selectNbAnimateurs(callback_parallel) {
				pairform_dao.utilisateur_dao.selectNbUtilisateursByRessource(
					id_ressource,
					constantes.CATEGORIE_ANIMATEUR,
					function (nb_utilisateurs) {
						retour_json.statistiques_ressource.animateurs_sur_ressource = nb_utilisateurs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre d'experts utilisant une ressource
			function selectNbExperts(callback_parallel) {
				pairform_dao.utilisateur_dao.selectNbUtilisateursByRessource(
					id_ressource,
					constantes.CATEGORIE_EXPERT,
					function (nb_utilisateurs) {
						retour_json.statistiques_ressource.experts_sur_ressource = nb_utilisateurs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de messages d'une ressource
			function selectNbMessages(callback_parallel) {
				pairform_dao.message_dao.selectNbMessagesByRessource(
					id_ressource,
					false,				// true si on veut uniquement le nombre de réponse, false sinon
					function (nb_messages) {
						retour_json.statistiques_ressource.messages_sur_ressource = nb_messages;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de réponses aux messages d'une ressource
			function selectNbReponsesMessage(callback_parallel) {
				pairform_dao.message_dao.selectNbMessagesByRessource(
					id_ressource,
					true,				// true si on veut uniquement le nombre de réponse, false sinon
					function (nb_messages) {
						retour_json.statistiques_ressource.reponses_sur_ressource = nb_messages;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de votes positifs sur les messages d'une ressource
			function selectNbVotesPositifs(callback_parallel) {
				pairform_dao.vote_dao.selectNbVotesByRessource(
					id_ressource,
					1,
					function (nb_votes) {
						retour_json.statistiques_ressource.votes_positifs_sur_ressource = nb_votes;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de votes négatifs sur les messages d'une ressource
			function selectNbVotesNegatifs(callback_parallel) {
				pairform_dao.vote_dao.selectNbVotesByRessource(
					id_ressource,
					-1,
					function (nb_votes) {
						retour_json.statistiques_ressource.votes_negatifs_sur_ressource = nb_votes;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de médailles d'or sur les messages d'une ressource
			function selectNbMedaillesOrMessage(callback_parallel) {
				pairform_dao.message_dao.selectNbMedaillesByRessource(
					id_ressource,
					constantes.MEDAILLE_OR,
					function (nb_medailles) {
						retour_json.statistiques_ressource.medailles_or_sur_ressource = nb_medailles;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de médailles d'argent sur les messages d'une ressource
			function selectNbMedaillesArgentMessage(callback_parallel) {
				pairform_dao.message_dao.selectNbMedaillesByRessource(
					id_ressource,
					constantes.MEDAILLE_ARGENT,
					function (nb_medailles) {
						retour_json.statistiques_ressource.medailles_argent_sur_ressource = nb_medailles;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de médailles de bronze sur les messages d'une ressource
			function selectNbMedaillesBronzeMessage(callback_parallel) {
				pairform_dao.message_dao.selectNbMedaillesByRessource(
					id_ressource,
					constantes.MEDAILLE_BRONZE,
					function (nb_medailles) {
						retour_json.statistiques_ressource.medailles_bronze_sur_ressource = nb_medailles;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre d'objectifs d'apprentissage dans une ressource
			function selectNbObjectifsApprentissage(callback_parallel) {
				pairform_dao.objectif_apprentissage_dao.selectNbObjectifsApprentissageByRessource(
					id_ressource,
					false,				// true si on veut seulement les objectifs validés, false sinon
					function (nb_objectifs) {
						retour_json.statistiques_ressource.objectifs_apprentissage_sur_ressource = nb_objectifs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre d'objectifs d'apprentissage validés dans une ressource
			function selectNbObjectifsApprentissageValides(callback_parallel) {
				pairform_dao.objectif_apprentissage_dao.selectNbObjectifsApprentissageByRessource(
					id_ressource,
					true,				// true si on veut seulement les objectifs validés, false sinon
					function (nb_objectifs) {
						retour_json.statistiques_ressource.objectifs_apprentissage_valides_sur_ressource = nb_objectifs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre d'utilisateurs ayant défini au moins un objectif d'apprentissage
			function selectNbUtilisateursUtilisantObjectifsApprentissage(callback_parallel) {
				pairform_dao.objectif_apprentissage_dao.selectNbUtilisateursUtilisantObjectifsApprentissageByResssource(
					id_ressource,
					function (nb_utilisateurs) {
						retour_json.statistiques_ressource.utilisateurs_utilisant_objectifs_apprentissage_sur_ressource = nb_utilisateurs;
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de messages de chaque capsule d'une ressource
			function selectCapsulesByRessource(callback_parallel) {
				pairform_dao.capsule_dao.selectCapsulesByRessources(
					[{id_ressource: id_ressource}],
					function (capsules) {
						// pour chaque capsule de la ressources
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].nom_court = capsules[clef].nom_court;
						}
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de messages de chaque capsule d'une ressource
			function selectNbMessagesByCapsule(callback_parallel) {
				pairform_dao.message_dao.selectNbMessagesCapsuleByRessource(
					id_ressource,
					false,				// true si on veut uniquement le nombre de réponse, false sinon
					function (capsules) {
						// pour chaque capsule de la ressources ayant des messages
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].messages_sur_capsule = capsules[clef].nb_messages;
						}
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de réponses aux messages de chaque capsule d'une ressource
			function selectNbReponsesMessageByCapsule(callback_parallel) {
				pairform_dao.message_dao.selectNbMessagesCapsuleByRessource(
					id_ressource,
					true,				// true si on veut uniquement le nombre de réponse, false sinon
					function (capsules) {
						// pour chaque capsule de la ressources ayant des réponses à des messages
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].reponses_sur_capsule = capsules[clef].nb_messages;
						}
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de votes positifs sur les messages de chaque capsule d'une ressource
			function selectNbVotesPositifsByCapsule(callback_parallel) {
				pairform_dao.vote_dao.selectNbVotesCapsulesByRessource(
					id_ressource,
					1,
					function (capsules) {
						// pour chaque capsule de la ressources ayant des votes positifs sur des messages
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].votes_positifs_sur_capsule = capsules[clef].nb_votes;
						}
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de votes négatifs sur les messages de chaque capsule d'une ressource
			function selectNbVotesNegatifsByCapsule(callback_parallel) {
				pairform_dao.vote_dao.selectNbVotesCapsulesByRessource(
					id_ressource,
					-1,
					function (capsules) {
						// pour chaque capsule de la ressources ayant des votes négatifs sur des messages
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].votes_negatifs_sur_capsule = capsules[clef].nb_votes;
						}
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de médailles d'or sur les messages de chaque capsule d'une ressource
			function selectNbMedaillesOrMessageByCapsule(callback_parallel) {
				pairform_dao.message_dao.selectNbMedaillesCapsulesByRessource(
					id_ressource,
					constantes.MEDAILLE_OR,
					function (capsules) {
						// pour chaque capsule de la ressources ayant au moins une médaille d'or sur un message
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].medailles_or_sur_capsule = capsules[clef].nb_medailles;
						}
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de médailles d'argent sur les messages de chaque capsule d'une ressource
			function selectNbMedaillesArgentMessageByCapsule(callback_parallel) {
				pairform_dao.message_dao.selectNbMedaillesCapsulesByRessource(
					id_ressource,
					constantes.MEDAILLE_ARGENT,
					function (capsules) {
						// pour chaque capsule de la ressources ayant au moins une médaille d'argent sur un message
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].medailles_argent_sur_capsule = capsules[clef].nb_medailles;
						}
						callback_parallel();
					},
					callbackError
				);
			},
			// Récupère le nombre de médailles de bronze sur les messages de chaque capsule d'une ressource
			function selectNbMedaillesBronzeMessageByCapsule(callback_parallel) {
				pairform_dao.message_dao.selectNbMedaillesCapsulesByRessource(
					id_ressource,
					constantes.MEDAILLE_BRONZE,
					function (capsules) {
						// pour chaque capsule de la ressources ayant au moins une médaille de bronze sur un message
						for (var clef in capsules) {
							// si la capsule n'existe pas encore dans le retour JSON, on la crée
							if (!retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule])
								retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule] = {};

							retour_json.statistiques_ressource.capsules[capsules[clef].id_capsule].medailles_bronze_sur_capsule = capsules[clef].nb_medailles;
						}
						callback_parallel();
					},
					callbackError
				);
			},],
			function callbackFinal () {
				// Récupère le nombre d'utilisateurs utilisant une ressource
				pairform_dao.utilisateur_dao.selectNbUtilisateursActifsByRessource(
					id_ressource,
					function (nb_utilisateurs) {
						// calcul des stats utilisant le nb d'utilisateurs utilisant la ressource
						retour_json.statistiques_ressource.utilisateurs_actifs_sur_ressource = nb_utilisateurs;
						retour_json.statistiques_ressource.messages_par_utilisateur_actif_sur_ressource = retour_json.statistiques_ressource.messages_sur_ressource / nb_utilisateurs
						retour_json.statistiques_ressource.messages_par_utilisateur_actif_sur_ressource = Math.round( retour_json.statistiques_ressource.messages_par_utilisateur_actif_sur_ressource );
						retour_json.statistiques_ressource.votes_par_utilisateur_actif_sur_ressource = (retour_json.statistiques_ressource.votes_positifs_sur_ressource + retour_json.statistiques_ressource.votes_negatifs_sur_ressource) / nb_utilisateurs
						retour_json.statistiques_ressource.votes_par_utilisateur_actif_sur_ressource = Math.round( retour_json.statistiques_ressource.votes_par_utilisateur_actif_sur_ressource );
						
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback(retour_json);
					},
					callbackError
				);
			}			
		);
	});
};
