
/*
	/!\ Attention : pour le Backoffice, les webservices du dossier "entites" 
					doivent être suffixés avec "_backoffice"
					pour éviter les conflits avec les webservices des Applications
*/


/*
 * Backoffice
 */
exports.tableau_de_bord = require("./backoffice/tableauDeBord");
exports.gestion_ressource = require("./backoffice/gestionRessource");
exports.attribution_roles = require("./backoffice/attributionRoles");
exports.connexion_backoffice = require("./backoffice/connexionBackoffice");
exports.statistiques = require("./backoffice/statistiques");

exports.espace_backoffice = require("./backoffice/entites/espace");
exports.ressource_backoffice = require("./backoffice/entites/ressource");
exports.capsule_backoffice = require("./backoffice/entites/capsule");
exports.groupe_backoffice = require("./backoffice/entites/groupe");
exports.role_backoffice = require("./backoffice/entites/role");
exports.message_backoffice = require("./backoffice/entites/message");


/*
 * Applications (iOs, web, Android, ...)
 */
exports.profil_utilisateur = require("./applications/profilUtilisateur");
exports.connexion_application = require("./applications/connexionApplication");

// exports.annotation = require("./applications/entites/annotation");
exports.objectif_apprentissage = require("./applications/entites/objectifApprentissage");
exports.profil_apprentissage = require("./applications/entites/profilApprentissage");
exports.accomplissement = require("./applications/entites/accomplissement");
exports.message = require("./applications/entites/message");
exports.notification = require("./applications/entites/notification");
exports.reseau = require("./applications/entites/reseau");
exports.ressource = require("./applications/entites/ressource");
exports.capsule = require("./applications/entites/capsule");
exports.utilisateur = require("./applications/entites/utilisateur");
exports.api_rs = require("./applications/api_rs");


/*
 * Migrations
 */
exports.migration_profils_apprentissage = require("./migrations/migrationProfilsApprentissage");
