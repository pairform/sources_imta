"use strict";

var CONFIG = require("config");

function definirConstantes(nom, valeur) {
	Object.defineProperty(exports, nom, {
		value: valeur,
		enumerable: true
	});
}


/*   
 *             o
 *             |
 *            / \  
 *           |   |
 *           | P |
 *           | A |  
 *           | I |  
 *           | R |  
 *           | - |
 *           | F |  
 *           | O |  
 *           | R |  
 *           | M |
 *           |   |  
 *          /  |  \ 
 *         o   o   o
 *
 *            @
 *              @
 *           @ @
 *             @
 *        @   @   @
 *         @ FIRE !
 *       @   @  @@ @
 *    ------------------
 *
 * Liste des constantes du module wbeservices
 *
 */

// Groupes obligatoires créés dès la création d'un espace
definirConstantes("GROUPE_ESPACE", "Espace");
definirConstantes("GROUPE_SPHERE_PUBLIQUE", "Sphère publique");			// groupe contenant tout les utilisateurs de PairForm


// Visibilités possibles d'une capsule pour un groupe
definirConstantes("VISIBILITE_ACCESSIBLE", 1);
definirConstantes("VISIBILITE_INACCESSIBLE", 2);
definirConstantes("VISIBILITE_INVISIBLE", 3);


// Usages possibles d'une ressource
definirConstantes("USAGE_FORMATION", 1);
definirConstantes("USAGE_PROFESSIONEL", 2);
definirConstantes("USAGE_DEBAT", 3);


// Type de médaille pouvant être attribué à un message
/*definirConstantes("MEDAILLE_OR", 1);
definirConstantes("MEDAILLE_ARGENT", 2);
definirConstantes("MEDAILLE_BRONZE", 3);*/
definirConstantes("MEDAILLE_OR", "or");
definirConstantes("MEDAILLE_ARGENT", "argent");
definirConstantes("MEDAILLE_BRONZE", "bronze");


// Catégorie possibles pour un utilisateur
definirConstantes("CATEGORIE_LECTEUR", 0);
definirConstantes("CATEGORIE_PARTICIPANT", 1);
definirConstantes("CATEGORIE_COLLABORATEUR", 2);
definirConstantes("CATEGORIE_ANIMATEUR", 3);
definirConstantes("CATEGORIE_EXPERT", 4);
definirConstantes("CATEGORIE_ADMIN", 5);


// id_utilisateur en BDD de l'admin PairForm
definirConstantes("ID_UTILISATEUR_ADMIN_PAIRFORM", 35);


// code et id des langues disponible dans PairForm
definirConstantes("LANGUES", {
	en: 1,
	es: 2,
	fr: 3
});


// liste des droits existants dans le Backoffice 
// Rq commentaires ci-dessous : "ressource / capsule ajoutée", il faut comprendre, "ressource / capsule crée dans le backoffice par l'utilisateur connecté"
definirConstantes("ADMIN_ESPACE", "admin_espace");											// L'admin d'un espace peut tout voir et faire dans cet espace. Il posséde tout les autres droits - ce n'est pas l'admin PairForm
definirConstantes("EDITER_ESPACE", "editer_espace");										// droit permettant : consulter et de modifier un espace
definirConstantes("GERER_GROUPES", "gerer_groupes");										// droit permettant : consulter, ajouter, modifier et supprimer les groupes + consulter, ajouter, modifier et supprimer les membres des groupes d'un espace
definirConstantes("GERER_ROLES", "gerer_roles");											// droit permettant : consulter, ajouter, modifier et supprimer les rôles d'un espace
definirConstantes("ATTRIBUER_ROLES", "attribuer_roles");									// droit permettant : consulter, les rôles d'un espace + consulter, ajouter, modifier et supprimer les membres dans un rôle d'un espace 
definirConstantes("GERER_RESSOURCES_ET_CAPSULES", "gerer_ressources_et_capsules");			// droit permettant : ajouter des ressources dans un espace + consulter, modifier les ressources ajoutées + ajouter des capsules dans une ressource ajoutée + consulter, modifier les capsules ajoutées
definirConstantes("EDITER_TOUTES_RESSOURCES_ET_CAPSULES", "editer_toutes_ressources_et_capsules"); // droit permettant : consulter et modifier toutes les ressources et capsules d'un espace
definirConstantes("GERER_VISIBILITE_CAPSULE", "gerer_visibilite_capsule");					// droit permettant : consulter les groupes d'un espace + consulter et modifier la visibilité d'une capsule ajoutée d'un espace
definirConstantes("GERER_VISIBILITE_TOUTES_CAPSULES", "gerer_visibilite_toutes_capsules");	// droit permettant : consulter les groupes d'un espace + consulter et modifier la visibilité de toutes les capsules d'un espace


// Roles par défaut créés à la création de l'espace (à chaque role est associé un nom et une liste de droits : 1 pour autorisé, 0 pour non-autorisé)
definirConstantes("ROLE_RESPONSABLE_CAPSULE", {
	nom: "Auteur",
	admin_espace: 0,
	editer_espace: 0,
	gerer_groupes: 0,
	gerer_roles: 0,
	attribuer_roles: 0,
	gerer_ressources_et_capsules: 1,
	editer_toutes_ressources_et_capsules: 0,
	gerer_visibilite_capsule: 1,
	gerer_visibilite_toutes_capsules: 0
});
definirConstantes("ROLE_RESPONSABLE_RESSOURCE", {
	nom: "Responsable de la publication",
	admin_espace: 0,
	editer_espace: 0,
	gerer_groupes: 1,
	gerer_roles: 0,
	attribuer_roles: 0,
	gerer_ressources_et_capsules: 0,
	editer_toutes_ressources_et_capsules: 0,
	gerer_visibilite_capsule: 0,
	gerer_visibilite_toutes_capsules: 1
});
definirConstantes("ROLE_RESPONSABLE_ESPACE", {
	nom: "Administrateur",
	admin_espace: 1,
	editer_espace: 1,
	gerer_groupes: 1,
	gerer_roles: 1,
	attribuer_roles: 1,
	gerer_ressources_et_capsules: 1,
	editer_toutes_ressources_et_capsules: 1,
	gerer_visibilite_capsule: 1,
	gerer_visibilite_toutes_capsules: 1
});


// Constantes lié aux retours JSON
definirConstantes("STATUS_JSON_OK", "ok");									// Status JSON pour une requete s'étant bien déroulée
definirConstantes("STATUS_JSON_UPDATE", "up");								// Status JSON pour une requete s'étant bien déroulée, mais nécessitant une mise à jour niveau client
definirConstantes("STATUS_JSON_KO", "ko");									// Status JSON pour une requete s'étant mal déroulée (erreur SQL, connexion internet suspendu, utilisateur n'ayant pas les droits sur l'action, etc.)
definirConstantes("UTILISATEUR_NON_CONNECTE", "utilisateur_invalide");		// message JSON indiquant que l'utilisateur n'est pas connecté à PairForm
definirConstantes("UTILISATEUR_NON_AUTORISE", "utilisateur_non_autorise");	// message JSON indiquant que l'utilisateur n'est autorisé à effectuer une action
definirConstantes("RETOUR_JSON_OK", {status: this.STATUS_JSON_OK});			// JSON par défaut lorsqu'une requête s'est bien déroulée
definirConstantes("RETOUR_JSON_UPDATE", {status: this.STATUS_JSON_UP});		// JSON par défaut lorsqu'une requete s'étant bien déroulée, mais nécessitant une mise à jour niveau client
definirConstantes("RETOUR_JSON_KO", {status: this.STATUS_JSON_KO});			// JSON par défaut lorsqu'une requête s'est mal déroulée (erreur SQL, connexion internet suspendu, utilisateur n'ayant pas les droits sur l'action, etc.)
definirConstantes("RETOUR_JSON_UTILISATEUR_NON_CONNECTE", {status: this.STATUS_JSON_KO, message: this.UTILISATEUR_NON_CONNECTE});		// JSON renvoyé quand l'utilisateur n'est pas connecté  et en cas de connexion avec des identifiants invalides
definirConstantes("RETOUR_JSON_UTILISATEUR_NON_AUTORISE", {status: this.STATUS_JSON_KO, message: this.UTILISATEUR_NON_AUTORISE});		// JSON renvoyé quand l'utilisateur n'est pas autorisé à effectuer une action


// Adresses et URL
definirConstantes("URL_WEBSERVICES_PHP", CONFIG.app.urls.serveur + CONFIG.app.urls.web_services_php);	// adresse des webservices PHP
definirConstantes("REPERTOIRE_CAPSULE_ABSOLUE", CONFIG.app.urls.url_repertoire_capsule +"/");			// Chemin relatif vers le répertoire de stockage des capsules
definirConstantes("URL_REPERTOIRE_CAPSULE", CONFIG.app.urls.alias_serveur + CONFIG.app.urls.repertoire_capsule +"/");		// Chemin absolue vers le répertoire de stockage des capsules
definirConstantes("REPERTOIRE_LOGOS_ESPACES", "public/img/logos_espaces/");								// Chemin relatif vers le répertoire de stockage des logos des espaces
definirConstantes("REPERTOIRE_LOGOS_RESSOURCES", "public/img/logos_ressources/");						// Chemin relatif vers le répertoire de stockage des logos des ressources
definirConstantes("URL_IMG_LOGO_PAR_DEFAUT", "public/img/logo_defaut.png");								// Chemin relatif vers le logo par défaut des ressources / espaces


// Questions et réponses par défaut du profil d'apprentissage associées à une ressource lors de sa création
definirConstantes("PROFIL_APPRENTISSAGE", [
	{
		id_langue: 1,
		question: "Je cherche à atteindre le niveau",
		reponses: [
			"initiation",
			"intermédiaire",
			"avancé"
		]
	},
	{
		id_langue: 1,
		question: "Je travaille",
		reponses: [
			"seul(e)",
			"avec des groupes restreints",
			"avec la communauté ouverte"
		]
	},
	{
		id_langue: 1,
		question: "Je participe surtout",
		reponses: [
			"pour le plaisir que je ressens à apprendre",
			"pour le plaisir de me sentir plus efficace plus compétent",
			"pour la valorisation personnelle ou professionnelle que cela me procure",
			"par pression professionnelle ou personnelle"
		]
	},
	{
		id_langue: 1,
		question: "J’appliquerai les acquis",
		reponses: [
			"immédiatement",
			"dans moins de 3 mois",
			"dans plus de 3 mois"
		]
	},
	{
		id_langue: 1,
		question: "Je vise surtout à",
		reponses: [
			"Acquérir des connaissances",
			"Maintenir mon niveau",
			"Partager des pratiques",
			"Faire de la veille",
			"Créer/élargir mon réseau",
			"Valoriser ma compétence"
		]
	},
	{
		id_langue: 2,
		question: "Je cherche à atteindre le niveau",
		reponses: [
			"initiation",
			"intermédiaire",
			"avancé"
		]
	},
	{
		id_langue: 2,
		question: "Je travaille",
		reponses: [
			"seul(e)",
			"avec des groupes restreints",
			"avec la communauté ouverte"
		]
	},
	{
		id_langue: 2,
		question: "Je participe surtout",
		reponses: [
			"pour le plaisir que je ressens à apprendre",
			"pour le plaisir de me sentir plus efficace plus compétent",
			"pour la valorisation personnelle ou professionnelle que cela me procure",
			"par pression professionnelle ou personnelle"
		]
	},
	{
		id_langue: 2,
		question: "J’appliquerai les acquis",
		reponses: [
			"immédiatement",
			"dans moins de 3 mois",
			"dans plus de 3 mois"
		]
	},
	{
		id_langue: 2,
		question: "Je vise surtout à",
		reponses: [
			"Acquérir des connaissances",
			"Maintenir mon niveau",
			"Partager des pratiques",
			"Faire de la veille",
			"Créer/élargir mon réseau",
			"Valoriser ma compétence"
		]
	},
	{
		id_langue: 3,
		question: "Je cherche à atteindre le niveau",
		reponses: [
			"initiation",
			"intermédiaire",
			"avancé"
		]
	},
	{
		id_langue: 3,
		question: "Je travaille",
		reponses: [
			"seul(e)",
			"avec des groupes restreints",
			"avec la communauté ouverte"
		]
	},
	{
		id_langue: 3,
		question: "Je participe surtout",
		reponses: [
			"pour le plaisir que je ressens à apprendre",
			"pour le plaisir de me sentir plus efficace plus compétent",
			"pour la valorisation personnelle ou professionnelle que cela me procure",
			"par pression professionnelle ou personnelle"
		]
	},
	{
		id_langue: 3,
		question: "J’appliquerai les acquis",
		reponses: [
			"immédiatement",
			"dans moins de 3 mois",
			"dans plus de 3 mois"
		]
	},
	{
		id_langue: 3,
		question: "Je vise surtout à",
		reponses: [
			"Acquérir des connaissances",
			"Maintenir mon niveau",
			"Partager des pratiques",
			"Faire de la veille",
			"Créer/élargir mon réseau",
			"Valoriser ma compétence"
		]
	}
]);


//Constantes de points
definirConstantes("PTSPARTICIPANT", 30);
definirConstantes("PTSCOLLABORATEUR", 500);
definirConstantes("PTSANIMATEUR", 1200);
definirConstantes("PTSECRIREMESSAGE", 10);
definirConstantes("PTSSUPPRIMERMESSAGE", -10);
definirConstantes("PTSEVALUERMESSAGE", 1);
definirConstantes("PTSGAGNERSUCCES", 10);
definirConstantes("PTSREUSSIDEFI", 20);
definirConstantes("PTSREUSSIDEFI_INVERSION", -20);
definirConstantes("PTSVOTEPOSITIF", 1);
definirConstantes("PTSVOTENEGATIF", -1);
definirConstantes("PTSVOTEPOSITIF_INVERSION", 2);
definirConstantes("PTSVOTENEGATIF_INVERSION", -2);
definirConstantes("PTSCOMMENTAIRESUPPRIME", -30);
definirConstantes("PTSCOMMENTAIREACTIVE", 30);


//Constantes des succès
definirConstantes("PARTICIPANT1", 1);
definirConstantes("PARTICIPANT3", 4);
definirConstantes("PARTICIPANT5", 5);
definirConstantes("PARTICIPANT10", 10);
definirConstantes("COLLABORATEUR1", 6);
definirConstantes("COLLABORATEUR3", 7);
definirConstantes("COLLABORATEUR5", 8);
definirConstantes("COLLABORATEUR10", 24);
definirConstantes("ANIMATEUR1", 9);
definirConstantes("ANIMATEUR3", 10);
definirConstantes("ANIMATEUR5", 11);
definirConstantes("ANIMATEUR10", 25);
definirConstantes("POINTSMONO100", 13);
definirConstantes("POINTSMULTI200", 14);
definirConstantes("TOUTLESSUCCES", 15); 
definirConstantes("AVATAR", 12);
definirConstantes("RESSOURCE1", 17);
definirConstantes("RESSOURCE2", 18);
definirConstantes("RESSOURCE3", 19);
definirConstantes("RESSOURCE5", 20);
definirConstantes("RESSOURCE10", 21);
definirConstantes("RESSOURCE25", 22);
definirConstantes("CERCLE", 16);
definirConstantes("MESSAGE5", 26);
definirConstantes("MESSAGE10", 27);
definirConstantes("MESSAGE20", 28);
definirConstantes("MESSAGE30", 29);
definirConstantes("MESSAGE50", 30);
definirConstantes("MESSAGE75", 31);
definirConstantes("MESSAGE100", 32);
definirConstantes("VOTE5", 33);
definirConstantes("VOTE10", 34);
definirConstantes("VOTE20", 35);
definirConstantes("VOTE30", 36);
definirConstantes("VOTE50", 37);
definirConstantes("VOTE75", 38);
definirConstantes("VOTE100", 39);
definirConstantes("REPONDRE5", 40);
definirConstantes("REPONDRE10", 41);
definirConstantes("REPONDRE20", 42);
definirConstantes("REPONDRE30", 43);
definirConstantes("REPONDRE50", 44);
definirConstantes("REPONDRE75", 45);
definirConstantes("REPONDRE100", 46);
definirConstantes("RECEVOIRREPONSES5", 47);
definirConstantes("RECEVOIRREPONSES10", 48);
definirConstantes("RECEVOIRREPONSES25", 49);
definirConstantes("DEFI5", 50);
definirConstantes("DEFI10", 51);
definirConstantes("DEFI20", 52);
definirConstantes("DEFI30", 53);
definirConstantes("DEFI50", 54);
definirConstantes("DEFI75", 55);
definirConstantes("DEFI100", 56);
definirConstantes("UTILITEREPANDUE5", 57);
definirConstantes("UTILITEREPANDUE10", 58);
definirConstantes("UTILITEREPANDUE20", 59);
definirConstantes("UTILITEREPANDUE30", 60);
definirConstantes("UTILITEREPANDUE50", 61);
definirConstantes("UTILE5", 62);
definirConstantes("UTILE10", 63);
definirConstantes("UTILE20", 64);
definirConstantes("UTILE30", 65);
definirConstantes("UTILE50", 66);
definirConstantes("UTILE75", 67);
definirConstantes("UTILE100", 68);
definirConstantes("UTILE1003", 69);
definirConstantes("UTILE1005", 70);
definirConstantes("UTILE10010", 71);
definirConstantes("CLASSE", 72);
definirConstantes("MEDAILLE", 73);
definirConstantes("MEDAILLE3", 74);
definirConstantes("MEDAILLE5", 75);
definirConstantes("MEDAILLE10", 76);