"use strict";

var web_services = require("../lib/webServices"),
		 express = require("express"),
			 log = require('metalogger')(),
	  constantes = require("../lib/constantes");


module.exports = function(app, passport) {

	// si l'utilisateur n'est pas connecté, on ne traite pas la requête
	function utilisateurEstConnecte(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
		}
	}

	// si l'utilisateur n'est pas autorisé à réaliser l'action, on ne traite pas la requête
	function utilisateurEstAutorise(res, autorisation, traiterRequete) {
		if (autorisation) {
			traiterRequete();
		} else {
			log.error("autorisation action : pas ok");
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_AUTORISE);
		}
	}


	//--------------------------------- Backoffice ---------------------------------

	/*
	 * Tableau de bord
	 */

	// Récupération des données du tableau de bord (liste des espaces, liste des ressources, liste des capsules)
	app.get("/webServices/infosTableauDeBord", utilisateurEstConnecte, function(req, res) {
		// si l'utilisateur connecté est l'admin de PairForm, on renvoit tout les espaces, toutes les ressources et toutes les capsules
		if(req.user.est_admin_pairform) {
			web_services.tableau_de_bord.getTableauDeBordAdminPairForm( function(retour_json) {
				res.json(retour_json);
			});
		}
		else if( Object.keys(req.user.liste_roles).length > 0) {
			// sinon, si l'utilisateur à des roles dans le backoffice
			// on renvoit les espaces dans lesquels l'utilisateur à un rôle, ainsi que les ressources et capsules que ces espaces contiennent
			web_services.tableau_de_bord.getTableauDeBord(
				req.user.email,
				function(retour_json) {
					res.json(retour_json);
				}
			);
		}
		else {
			// sinon, l'utilisateur n'a aucun role, il n'est pas autorisé à accéder au backoffice
			log.error("autorisation action : pas ok");
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_AUTORISE);
		}
	});


	/*
	 * Gestion ressource
	 */

	// récupèration des données liées à la page de création d'une ressource (liste des thèmes, listes des langues ; traduit dans la langue choisi)
	app.get("/webServices/espace/:id_espace/infosCreationRessource/langue/:code_langue", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.gestion_ressource.getCreationRessource(
				req.params.code_langue,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Attribution roles
	 */

	// récupèration des données lièes à la page dattribution des rôles aux utilisateurs d'un espace
	app.get("/webServices/espace/:id_espace/attributionRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.ATTRIBUER_ROLES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.attribution_roles.getAttributionRoles(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Connexion
	 */

	// connexion d'un utilisateur au Backoffice
	app.post("/webServices/backoffice/connexion", function(req, res) {
		web_services.connexion_backoffice.interfaceConnexion(
			req,
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// déconnexion d'un utilisateur au Backoffice
	app.post("/webServices/backoffice/deconnexion", function(req, res) {
		web_services.connexion_backoffice.interfaceDeconnexion(
			req,
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// reinitialisation du mot de passe d'un utilisateur
	app.post("/webServices/backoffice/MotDePasse/reinitialiser", function(req, res) {
		web_services.connexion_backoffice.reinitialisationMotDePasse(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	//--------------------------------- Entités ---------------------------------

	/*
	 * Espaces
	 */

	// récupèration d'un espace
	app.get("/webServices/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.EDITER_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.getEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'un nouvelle espace
	app.put("/webServices/espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform;

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.putEspace(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour d'un espace
	app.post("/webServices/espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.EDITER_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.postEspace(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// suppression d'un espace
	app.delete("/webServices/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform;

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.deleteEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Ressources
	 */

	// récupèration d'une ressource
	app.get("/webServices/espace/:id_espace/ressource/:id_ressource/:cree_par/langue/:code_langue", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.EDITER_TOUTES_RESSOURCES_ET_CAPSULES] || 
			(req.user.liste_roles[req.params.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES] && req.user.id_utilisateur == req.params.cree_par);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.getRessource(
				req.params.id_ressource,
				req.params.code_langue,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'une nouvelle ressource
	app.put("/webServices/ressource", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.putRessource(
				req.body,
				req.user.id_utilisateur,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour d'une ressource
	app.post("/webServices/ressource", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.EDITER_TOUTES_RESSOURCES_ET_CAPSULES] || 
			(req.user.liste_roles[req.body.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES] && req.user.id_utilisateur == req.body.cree_par);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.postRessource(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// suppression d'une ressource
	app.delete("/webServices/ressource/:id_ressource/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform;
		req.params.liste_ressources = [{id_ressource: req.params.id_ressource}];		// le paramètre envoyé doit être un tableau contenant les id des capsules à supprimer

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.deleteRessources(
				req.params,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Capsules
	 */


	// récupèration d'une capsule
	app.get("/webServices/espace/:id_espace/capsule/:id_capsule/:cree_par", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.EDITER_TOUTES_RESSOURCES_ET_CAPSULES] || 
			(req.user.liste_roles[req.params.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES] && req.user.id_utilisateur == req.params.cree_par);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.getCapsule(
				req.params.id_capsule,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// récupèration des informations sur la visibilité d'une capsule
	app.get("/webServices/espace/:id_espace/capsule/:id_capsule/:cree_par/visibilite", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_VISIBILITE_TOUTES_CAPSULES] ||
			(req.user.liste_roles[req.params.id_espace][constantes.GERER_VISIBILITE_CAPSULE] && req.user.id_utilisateur == req.params.cree_par);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.getCapsuleVisibilite(
				req.params.id_capsule,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'une nouvelle capsule
	// un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js /!\ 
	app.put("/webServices/capsule", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.ADMIN_ESPACE] ||
			(req.user.liste_roles[req.body.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES] && req.user.id_utilisateur == req.body.ressource_cree_par);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.putCapsule(
				req.body,
				req.user.id_utilisateur,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour d'une capsule
	// un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js /!\ 
	app.post("/webServices/capsule", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.EDITER_TOUTES_RESSOURCES_ET_CAPSULES] || 
			(req.user.liste_roles[req.body.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES] && req.user.id_utilisateur == req.body.cree_par);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.postCapsule(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour de la visibilité d'une capsule
	app.post("/webServices/capsule/visibilite", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_VISIBILITE_TOUTES_CAPSULES] ||
			(req.user.liste_roles[req.body.id_espace][constantes.GERER_VISIBILITE_CAPSULE] && req.user.id_utilisateur == req.body.cree_par);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.postCapsuleVisibilite(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// suppression d'une capsule
	app.delete("/webServices/capsule/:id_capsule/:id_ressource/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform;
		req.params.liste_capsules = [{			// le paramètre envoyé doit être un tableau contenant les id des capsules à supprimer et l'id de la ressource associé
			id_capsule: req.params.id_capsule,
			id_ressource: req.params.id_ressource
		}];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.deleteCapsules(
				req.params,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Groupes
	 */

	// récupèration de la liste des groupes d'un espace
	app.get("/webServices/espace/:id_espace/listeGroupes", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_GROUPES] || 
			req.user.liste_roles[req.params.id_espace][constantes.GERER_VISIBILITE_CAPSULE] || req.user.liste_roles[req.params.id_espace][constantes.GERER_VISIBILITE_TOUTES_CAPSULES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.getGroupesByIdEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'un nouveau groupe
	app.put("/webServices/groupe", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.putGroupe(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour d'un groupe
	app.post("/webServices/groupe", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.postGroupe(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour de la liste des membres d'un groupe
	app.post("/webServices/groupe/listeMembres", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.postMembres(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// suppression d'un groupe
	app.delete("/webServices/espace/:id_espace/groupe/:id_groupe", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.deleteGroupe(
				req.params.id_groupe,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Roles
	 */

	// récupèration de la liste des roles d'un espace
	app.get("/webServices/espace/:id_espace/listeRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_ROLES] || req.user.liste_roles[req.params.id_espace][constantes.ATTRIBUER_ROLES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.getRolesByIdEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour des roles d'un espace
	app.post("/webServices/listeRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_ROLES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.postRoles(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour de la liste des membres d'un role
	app.post("/webServices/role/listeMembres", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.ATTRIBUER_ROLES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.postMembresRole(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Messages
	 */

	// suppression des messages d'une capsule
	app.delete("/webServices/messages/capsule/:id_capsule/:cree_par/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.ADMIN_ESPACE] || 
			(req.user.liste_roles[req.params.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES] && req.user.id_utilisateur == req.params.cree_par);
		
		utilisateurEstAutorise(res, autorisation, function() {
			web_services.message_backoffice.deleteCapsuleMessages(
				req.params.id_capsule,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Statistiques
	 */

	// récupèration des statistiques d'une ressource
	app.get("/webServices/statistiques/espace/:id_espace/ressource/:id_ressource/:ressource_cree_par", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || 
			req.user.liste_roles[req.params.id_espace][constantes.ADMIN_ESPACE] ||
			req.user.id_utilisateur == req.params.ressource_cree_par;

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.statistiques.getStatistiquesRessource(
				req.params.id_ressource,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});
}