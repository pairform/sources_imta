"use strict";

var web_services = require("../lib/webServices"),
		 express = require("express"),
	  constantes = require("../lib/constantes"),
		  CONFIG = require("config");


module.exports = function(app, passport) {

	// si l'utilisateur n'est pas connecté, on ne traite pas la requête
	function utilisateurEstConnecte(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
		}
	}

	//--------------------------------- Application ---------------------------------

	/*
	 * Connexion
	 */

	// connexion d'un utilisateur à l'application web
	app.post("/webServices/utilisateur/login", function(req, res) {
		web_services.connexion_application.interfaceConnexion(
			req,
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// déconnexion d'un utilisateur de l'application web
	app.post("/webServices/utilisateur/logout", function(req, res) {
		web_services.connexion_application.interfaceDeconnexion(
			req,
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Profil utilisateur
	 */
	
	// Récupère les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
	app.get("/webServices/utilisateur/profil", function(req, res) {
		web_services.profil_utilisateur.getProfilUtilisateur(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	//--------------------------------- Entités ---------------------------------

	/*
	 * Ressource
	 */

	// Vérification de la présence de la ressource en BDD
	app.post("/webServices/ressource/verifier", function(req, res) {
		web_services.ressource.verifier(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// downloadRessource
	app.all("/webServices/ressource/downloadRessource", function(req, res) {
		web_services.ressource.downloadRessource(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Récuperer toutes les infos d'une ressource
	app.get("/webServices/ressource", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		req.query.email = req.isAuthenticated() ? req.user.email : undefined;
		web_services.ressource.getRessource(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// listerRessources
	app.get("/webServices/ressource/liste", function(req, res) {
		req.query.email = req.isAuthenticated() ? req.user.email : undefined;
		web_services.ressource.liste(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	app.get("/webServices/ressource/kiosque", function(req, res) {
		req.query.email = req.isAuthenticated() ? req.user.email : undefined;
		web_services.ressource.kiosque(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Utilisateur
	 */

	// Vérification du role de l'utilisateur
	app.post("/webServices/utilisateur/categorie/verifier", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.verifierCategorie(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	
	// Recuperation des préférences 
	app.get("/webServices/utilisateur/preferences", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.getPreferences(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	// editerAvatar
	app.post("/webServices/utilisateur/avatar", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.editerAvatar(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// editerCompte
	app.post("/webServices/utilisateur/editer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.editerCompte(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// editerLangues
	app.all("/webServices/utilisateur/editerLangues", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.editerLangues(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// registerCompte
	app.all("/webServices/utilisateur/enregistrer", function(req, res) {
		web_services.utilisateur.registerCompte(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// resetPassword
	app.post("/webServices/utilisateur/reset", function(req, res) {
		web_services.utilisateur.resetPassword(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// resetPasswordStep2
	app.all("/webServices/utilisateur/resetPasswordStep2", function(req, res) {
		web_services.utilisateur.resetPasswordStep2(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// afficherListeDeProfils
	app.get("/webServices/utilisateur/liste", function(req, res) {
		web_services.utilisateur.liste(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// activer / désactiver la notification par email
	app.post("/webServices/utilisateur/notification/mail", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.changerNewsParMail(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// changerNomsReels
	app.post("/webServices/utilisateur/noms/reels", utilisateurEstConnecte, function(req, res) {
		web_services.utilisateur.changerNomsReels(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// changerRole
	app.post("/webServices/utilisateur/role", utilisateurEstConnecte, function(req, res) {
		web_services.utilisateur.changerRole(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Messages
	 */

	 // Suppression d'un tag
	app.get("/webServices/message/votes", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.message.recupererVotesMessage(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Suppression d'un tag
	app.delete("/webServices/message/tag", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.message.supprimerTag(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// ajouter une liste de Tags
	app.post("/webServices/message/tags", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.ajouterTags(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// deplacer un message
	app.post("/webServices/message/deplacer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.deplacer(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// donner une médaille à un message
	app.post("/webServices/message/donnerMedaille", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.donnerMedaille(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// enregistrer un nouveau message
	app.put("/webServices/message", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.envoyer(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	
	// modifierLangue
	app.all("/webServices/message/modifierLangue", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.modifierLangue(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// recuperer une liste de messages (filtrée)
	app.get("/webServices/message", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.getMessages(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// recupererNombreMessages
	app.get("/webServices/message/nombre", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.nombre(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Pour le bridge avec les anciennes version.
	app.get("/webServices/message/nombre/capsules", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.nombreParCapsules(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	//
	app.get("/webServices/message/nombre/ressource", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.nombreDansRessource(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// redirigerVersMessage
	app.get("/webServices/message/rediriger", function(req, res) {
    	res.redirect(CONFIG.app.urls.serveur + CONFIG.app.urls.web_services_php +'/messages/redirigerVersMessage.php?id_message='+req.query.id_message);
	});

	// supprimer un message
	app.post("/webServices/message/supprimer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		if (req.body.supprime_def) {
			req.body.est_admin_pairform = req.user.est_admin_pairform;
			web_services.message.supprimerDefinitivement(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			);
		}
		else{
			web_services.message.supprimer(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			);
		}
	});

	// terminer un défi
	app.post("/webServices/message/defi/terminer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.terminerDefi(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// valider la reponse à un défi
	app.post("/webServices/message/defi/valider", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.validerReponseDefi(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	
	// vote +1 / -1 sur un message
	app.post("/webServices/message/voter", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.voter(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Acccomplissement
	 */

	//
	app.get("/webServices/accomplissement/succes", function(req, res) {
		web_services.accomplissement.liste(
			req.query,
			function(retour_json) {
				res.send(retour_json);
			}
		);
	});


	/*
	 * Notifications
	 */

	//
	app.get("/webServices/notification", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.notification.recuperer(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	//
	app.post("/webServices/notification/vue", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.notification.changerVueNotification(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Réseau
	 */

	// afficherListeCercles
	app.get("/webServices/reseau/liste", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.liste(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// ajouter un membre
	app.put("/webServices/reseau/utilisateur", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.ajouterMembre(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// creer un ercle
	app.put("/webServices/reseau/cercle", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.creerCercle(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// creer une classe
	app.put("/webServices/reseau/classe", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.creerClasse(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// rejoindre une classe
	app.put("/webServices/reseau/classe/utilisateur", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.rejoindreClasse(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// supprimer un cercle
	app.delete("/webServices/reseau", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.supprimerCercle(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// supprimer un membre d'une classe/cercle (réseau)
	app.delete("/webServices/reseau/utilisateur", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.supprimerMembre(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Objectifs d'apprentissage
	 */

	// création d'un nouvelle objectif d'apprentissage
	app.put("/webServices/objectif", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.objectif_apprentissage.putObjectifApprentissage(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Mise à jour d'un objectif d'apprentissage
	app.post("/webServices/objectif", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.objectif_apprentissage.postObjectifApprentissage(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// suppression d'un objectif d'apprentissage
	app.delete("/webServices/objectif", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.objectif_apprentissage.deleteObjectifApprentissage(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	/*
	 * Profils d'apprentissage
	 */

	// Mise à jour d'un profil d'apprentissage
	app.post("/webServices/profil", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.profil_apprentissage.postProfilApprentissage(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

}