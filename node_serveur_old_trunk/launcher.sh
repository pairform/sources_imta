#!/bin/bash

if [[ $1 == 'dev' ]]; then
	
	sudo -u node ./dev_start.sh

else 
	sudo -u node ./start.sh	
fi