"use strict";

var express = require("express"),
	compression = require("compression"),
	body_parser = require("body-parser"),
  cookie_parser = require("cookie-parser"),
		   cors = require("cors"),
method_override = require("method-override"),
 cookie_session = require("cookie-session"),
   errorhandler = require("errorhandler"),
less_middleware = require("less-middleware"),
	 		log = require("metalogger")(),
		 CONFIG = require("config"),
	   passport = require("passport"),
	    favicon = require('serve-favicon');

require("../passport/passport")(passport);


exports = module.exports;

exports.setup = function(app) {

	// configuration des nom de domaine autorisés à effectuer du cross-domaine avec le serveur Node.js de PairForm
	var liste_blanche = CONFIG.app.liste_blanche_cross_domaine;

	var cors_options = {
		origin: function(origin, callback) {
			var origin_est_dans_liste_blanche = liste_blanche.indexOf(origin) !== -1;
			callback(null, origin_est_dans_liste_blanche);
		}
	};

	app.use(function(req, res, next) {
		res.setHeader('Access-Control-Allow-Credentials', 'true');		// permission de stocker et d'accèder aux infos (ex: cookies) du header des requetes 
		if(req.originalUrl.match(/.*?(webServices)/))
			res.setHeader('Cache-Control', 'no-cache');					// par défaut, le réponse du serveur Node ne seront pas mises en cache sur les clients pour les webservices
		return next();
	});
	app.use(cors(cors_options));

	app.use(compression());

	// Jade est le moteur de template des applications web PairForm
	app.set("view engine", "jade");

	// modification de la taille limite de certaines requetes
	app.use("/webServices/espace", body_parser({ limit: 1024 * 1024 * 1024 }));				// route avec de l'upload d'image (logo)
	app.use("/webServices/capsule", body_parser({ limit: 1024 * 1024 * 1024 }));				// route avec de l'upload de fichier
	app.use("/webServices/groupe/listeMembres", body_parser({ limit: 1024 * 1024 * 1024 }));	// import d'une multitude d'e-mails

	app.use("/", body_parser());
	app.use(cookie_parser());
	app.use(method_override());
	app.use(express.query());

	var root_dir = require("path").dirname(module.parent.filename);
	// This is not needed if you handle static files with, say, Nginx (recommended in production!)
	// Additionally you should probably pre-compile your LESS stylesheets in production
	// Last, but not least: Express" default error handler is very useful in dev, but probably not in prod.
	if (("NODE_SERVE_STATIC" in process.env) && process.env["NODE_SERVE_STATIC"] == 1) {
		var pub_dir = root_dir + CONFIG.app.pub_dir;

		app.use(less_middleware( pub_dir ));
		app.use("/public", express.static(pub_dir));
		app.use(errorhandler({ dumpExceptions: true, showStack: true }));
	}

	app.use("/private", express.static(root_dir + "/pairform_modules/backoffice/private"));
	app.use("/private", express.static(root_dir + "/pairform_modules/vitrine/private"));
	app.use("/private", express.static(root_dir + "/pairform_modules/app/private"));
	
	//Favicon
	app.use(favicon(root_dir + '/public/ico/favicon.png'));

	// on stock les sessions avec cookie_session et passport
	app.use(cookie_session( { 
		key: "session.pf",
		secret: CONFIG.app.sessions.secret,
		cookie: {
			maxAge: 30 * 60 * 1000
		}
	} ));
	app.use(passport.initialize());
	app.use(passport.session());

	//---- Mounting application modules
	app.use(require("../../pairform_modules/vitrine"));
	app.use(require("../../pairform_modules/app"));
	require("../../pairform_modules/backoffice")(app, passport);
	require("../../pairform_modules/webservices")(app, passport);

	app.use(function notFoundHandler(req, res) {
		log.error("404 - " + req.url);
		res.status(404);
		// Renvoi de la page 404.jade
		if (req.url.match(/.htm|.html|.php/) || req.accepts('html')) {
			res.render('404');
			return;
		}
		//Si on demande une image
		// if (req.accepts(['image/png', 'image/webp', 'image/jpeg', 'image/gif'])) {
		if (req.url.match(/.png|.jpg|.gif/) && req.accepts('image')) {
			res.sendFile(root_dir + "public/img/logo_defaut.png");
			return;
		}

		// Renvoi d'une réponse JSON 
		if (req.accepts('json')) {
			res.send({ retour : "ko", message : '404' });
			return;
		}

		// Au cas où, on renvoie du texte brut
		res.type('txt').send('404');
	});
	//--- End of Internal modules

	//-- ATTENTION: make sure errorHandler are the very last app.use() call
	//-- ATTENTION: and in the sequence they are in, or it won't work!!!
	
	// Catch-all error handler. Modify as you see fit, but don't overuse.
	// Throwing exceptions is not how we normally handle errors in Node.
	app.use(function catchAllErrorHandler(err, req, res, next) {
		// Emergency: means system is unusable
		log.emergency(err.stack);
		res.status(500).render('500');

		// We aren't in the business of hiding exceptions under the rug. It should
		// still crush the process. All we want is: to properly log the error before
		// that happens.
		//
		// Clustering code in the lib/clustering module will restart the crashed process.
		// Make sure to always run clustering in production!
		setTimeout(function() { // Give a chance for response to be sent, before killing the process
			process.exit(1);
		}, 10);
	});
};
