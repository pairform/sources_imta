"use strict";

var mysql = require('mysql'),
   CONFIG = require('config'),
	  log = require('metalogger')(),
  request = require("request");

var LocalStrategy = require('passport-local').Strategy;
var pool = mysql.createPool(CONFIG.app.bdd_mysql);		// pool permettant de récupérer une connexion existante


module.exports = function(passport) {

	// serialize un utilisateur pour lui attribuer une session
	passport.serializeUser( function(utilisateur_connecte, done) {
		done(null, utilisateur_connecte.id_utilisateur);
	});

	// deserialize l'utilisateur connecte
	passport.deserializeUser( function(id_utilisateur, done) {
		var connexionCourante;									// connexion courante avec la BDD
		var utilisateur_connecte;

		// récupération d'une connexion
		pool.getConnection( function(erreur, connexion) {
			if (erreur) {
				log.error("erreur connexion BDD : " + erreur);
				setTimeout(getConnexionBDD, 2000);
			} else {
				connexionCourante = connexion;
				selectUtilisateur();
			}
		});

		// récupération des informations sur l'utilisateur connecté
		function selectUtilisateur() {
			pool.query(
				"SELECT guid AS id_utilisateur, username AS pseudo, email, (CASE admin WHEN 'yes' THEN 1 ELSE 0 END) AS est_admin_pairform, "+
				"CONCAT('"+ CONFIG.app.urls.serveur + CONFIG.app.urls.elgg_php + "/avatar/view/', username, '/medium/0') AS avatar_url "+
				"FROM sce_users_entity "+
				"WHERE guid = "+ id_utilisateur,
				function(erreur, utilisateurs){
					// Si la requete SQL a déclanchée une erreur
					if (erreur) {
						log.error("erreur requete SQL : " + erreur.code);
						connexionCourante.release();
						done(erreur, null);
					} else {
						utilisateur_connecte = {
							id_utilisateur: utilisateurs[0].id_utilisateur,
							pseudo: utilisateurs[0].pseudo,
							avatar_url: utilisateurs[0].avatar_url,
							email: utilisateurs[0].email,
							est_admin_pairform: utilisateurs[0].est_admin_pairform
						};
						selectRolesByEmail(utilisateurs[0].email);
					}
				}
			);
		}

		// récupère les roles de l'utilisateur connecté dans chaque espace
		function selectRolesByEmail(email) {
			// récupération du nom de domaine de l'email
			var nom_de_domaine = email.substring( email.indexOf("@") + 1 );

			pool.query(
				"SELECT cr.id_role, cr.nom, cr.id_espace, cr.admin_espace, cr.editer_espace, cr.gerer_groupes, cr.gerer_roles, cr.attribuer_roles, cr.gerer_ressources_et_capsules, cr.editer_toutes_ressources_et_capsules, cr.gerer_visibilite_capsule, cr.gerer_visibilite_toutes_capsules "+
				"FROM cape_roles AS cr "+
				"JOIN cape_membres AS cm ON cr.id_role = cm.id_role "+
				"WHERE cm.nom='"+ email +"' OR (cm.nom='"+ nom_de_domaine +"' AND cm.nom_de_domaine=1);",
				function(erreur, liste_roles) {
					// Si la requete SQL a déclanchée une erreur
					if (erreur) {
						log.error("erreur requete SQL : " + erreur.code);
					} else {
						utilisateur_connecte.liste_roles = {};
						// pour chaque rôle de l'utilisateur
						for (var clef in liste_roles) {
							utilisateur_connecte.liste_roles[liste_roles[clef].id_espace] = liste_roles[clef];
						};
					}
					connexionCourante.release();
					done(erreur, utilisateur_connecte);
				}
			);
		}
	});
};