<?php

  //********************************************//
  //******** Structure de base de WS ***********//
  //********************************************//
  
  try {
    $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET CHARACTER SET utf8");
    // $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

  } catch (Exception $e) {
    error_log($e->getMessage());
  }
  print('Migration des messages : <br>');
  print('Phase 1 - metadonnées 1-to-1 <br>');
      	
  $sql_query_messages = "SELECT 
    `cm`.`id_message`,
    `sceoe`.`description` AS 'contenu',
    `e`.`time_created` AS 'date_creation',
    CAST((CASE  WHEN (`msn_tag`.`string` = 'time_updated_meta' AND !ISNULL(`msv_tag`.`string`)) THEN (CASE WHEN `msv_tag`.`string` > `e`.`time_updated` THEN `msv_tag`.`string` ELSE `e`.`time_updated` END) ELSE `e`.`time_updated` END) AS SIGNED INTEGER) AS 'date_modification',
    `cm`.`id_capsule`,
    `cm`.`nom_page`,
    `cm`.`nom_tag`,
    `cm`.`num_occurence`,

    IF(`e`.`access_id` IN(1,2), 0, `e`.`access_id`) AS 'visibilite',
    GROUP_CONCAT(DISTINCT(CASE `n`.`string` WHEN 'parent' THEN v.`string` ELSE '' END) SEPARATOR '') AS 'id_message_parent',
    `cm`.`id_langue`,
    0 AS 'est_lu',
    `cm`.`supprime_par`,
    GROUP_CONCAT(DISTINCT(CASE `n`.`string` WHEN 'medaille' THEN v.`string` ELSE '' END) SEPARATOR '') AS 'medaille', 
    (CASE `n`.`string` WHEN 'est_defi' THEN v.`string` ELSE 0 END) AS 'est_defi', 
    SUM(CASE `n`.`string` WHEN 'defi_valide' THEN v.`string` ELSE 0 END) AS 'defi_valide', 
    (SUM(CASE `n`.`string` WHEN 'likes' THEN 1 ELSE 0 END) - SUM(CASE `n`.`string` WHEN 'dislikes' THEN 1 ELSE 0 END)) AS 'somme_votes', 
    CONCAT('[',GROUP_CONCAT(DISTINCT(CONCAT('\"', (CASE WHEN (`msn_tag`.`string` = 'tags' AND !ISNULL(`msv_tag`.`string`)) THEN `msv_tag`.`string` END), '\"')) SEPARATOR ', '), ']') AS 'tags', 
    CONCAT('[',GROUP_CONCAT(DISTINCT(CONCAT('\"', (CASE WHEN (`msn_tag`.`string` = 'tags' AND !ISNULL(`ue_tag`.`username`)) THEN `ue_tag`.`username` END), '\"')) SEPARATOR ', '), ']') AS 'noms_auteurs_tags',
    `cm`.`geo_lattitude`,
    `cm`.`geo_longitude`,

    `e`.`owner_guid` AS 'id_auteur'

    FROM `cape_messages` AS `cm`

    JOIN `sce_entities` `e` ON `cm`.`id_message` = `e`.`guid`  

    LEFT OUTER JOIN `sce_annotations` `n_table` ON `cm`.`id_message` = `n_table`.`entity_guid`
    LEFT OUTER JOIN `sce_metastrings` `n` on `n_table`.`name_id` = `n`.`id`  
    LEFT OUTER JOIN `sce_metastrings` `v` on `n_table`.`value_id` = `v`.`id`  
    LEFT OUTER JOIN `sce_users_entity` `ue` ON `e`.`owner_guid` = `ue`.`guid`  

    JOIN `cape_utilisateurs_categorie` AS `cuc` ON `cuc`.`id_utilisateur` = `e`.`owner_guid`
    JOIN `cape_categories` AS `cc` ON `cc`.`id_categorie` = `cuc`.`id_categorie`
    JOIN `sce_objects_entity` AS `sceoe` ON `sceoe`.`guid` = `cm`.`id_message`

    LEFT OUTER JOIN `sce_metadata` `md_tag` on `md_tag`.`entity_guid` = `e`.`guid`  
    LEFT OUTER JOIN `sce_users_entity` `ue_tag` on `ue_tag`.`guid` = `md_tag`.`owner_guid`  
    LEFT OUTER JOIN `sce_metastrings` `msv_tag` on `msv_tag`.`id` = `md_tag`.`value_id`  
    LEFT OUTER JOIN `sce_metastrings` `msn_tag` on `md_tag`.`name_id` = `msn_tag`.`id`

    GROUP BY cm.id_message";
  //Récupération de toutes les messages
  $query_cm = $dbh->query($sql_query_messages);
  //Préparation de l'update
  $update_cm = $dbh->prepare('UPDATE cape_messages SET 
      id_utilisateur = :id_utilisateur,
      id_message_parent = :id_message_parent,
      est_defi = :est_defi,
      defi_valide = :defi_valide,
      medaille = :medaille,
      visibilite = :visibilite,
      contenu = :contenu,
      date_creation = :date_creation,
      date_modification = :date_modification
      WHERE 
      id_message = :id_message');

  //Pour chaque message
  while ($message = $query_cm->fetch()) {

    $resultat = $update_cm->execute(array(
      ":id_utilisateur" => $message["id_auteur"],
      ":id_message_parent" => $message["id_message_parent"],
      ":est_defi" => $message["est_defi"],
      ":defi_valide" => $message["defi_valide"],
      ":medaille" => $message["medaille"],
      ":visibilite" => $message["visibilite"],
      ":contenu" => $message["contenu"],
      ":date_creation" => $message["date_creation"],
      ":date_modification" => $message["date_modification"],
      ":id_message" => $message["id_message"]
    ));
    
    print("<br>Message #{$message["id_message"]} : " .($resultat ? "mis à jour" : "erreur"));

    error_log("Message #{$message["id_message"]} : " .($resultat ? "mis à jour" : "erreur"));
  }

  print('Phase 2 - metadonnées n-to-n <br>');
  print('Phase 2.1 - Votes <br>');

  $sql_query_votes = "SELECT 
    n_table.owner_guid AS id_utilisateur,
    n_table.entity_guid AS id_message,
    (CASE `n`.`string` WHEN 'likes' THEN 1 WHEN 'dislikes' THEN -1 ELSE 0 END) AS 'vote',
    n_table.time_created AS date_creation,
    n_table.time_created AS date_modification

    FROM `sce_annotations` `n_table`
    JOIN `cape_messages` `cm` on `cm`.`id_message` = `n_table`.`entity_guid`
    LEFT OUTER JOIN `sce_metastrings` `n` on `n_table`.`name_id` = `n`.`id`  
    LEFT OUTER JOIN `sce_metastrings` `v` on `n_table`.`value_id` = `v`.`id` 
    WHERE n.string IN('likes','dislikes')";

  //Récupération de toutes les messages
  $query_votes = $dbh->query($sql_query_votes);
  //Préparation de l'update
  $insert_cmv = $dbh->prepare('INSERT IGNORE INTO cape_messages_votes 
      VALUES 
      (:id_utilisateur, :id_message, :vote, :date_creation, :date_modification)');

  //Pour chaque message
  while ($vote = $query_votes->fetch()) {

    $resultat = $insert_cmv->execute(array(
      "id_utilisateur" => $vote["id_utilisateur"],
      "id_message" => $vote["id_message"],
      "vote" => $vote["vote"],
      "date_creation" => $vote["date_creation"],
      "date_modification" => $vote["date_modification"]
    ));
    
    print("<br>Vote sur message #{$vote["id_message"]} de @{$vote["id_utilisateur"]} valeur {$vote["vote"]} : " .($resultat ? "mis à jour" : "erreur"));

    error_log("Vote sur message #{$vote["id_message"]} de @{$vote["id_utilisateur"]} valeur {$vote["vote"]} : " .($resultat ? "mis à jour" : "erreur"));
  }

  print('Phase 2.2 - Tags <br>');

  $sql_query_tags = "SELECT 
    n_table.owner_guid AS id_utilisateur,
    n_table.entity_guid AS id_message,
    `v`.`string` AS 'tag',
    n_table.time_created AS date_creation,
    n_table.time_created AS date_modification

    FROM `sce_metadata` `n_table`
    JOIN `cape_messages` `cm` on `cm`.`id_message` = `n_table`.`entity_guid` 
    LEFT OUTER JOIN `sce_metastrings` `n` on `n_table`.`name_id` = `n`.`id`  
    LEFT OUTER JOIN `sce_metastrings` `v` on `n_table`.`value_id` = `v`.`id` 
    WHERE n.string = 'tags'
    AND n_table.owner_guid != 0
    AND n_table.entity_guid != 0
    AND v.string != ''";

  //Récupération de toutes les messages
  $query_tags = $dbh->query($sql_query_tags);
  //Préparation de l'update
  $insert_cmt = $dbh->prepare('INSERT IGNORE INTO cape_messages_tags 
      VALUES 
      (:id_utilisateur, :id_message, :tag, :date_creation, :date_modification)');

  //Pour chaque message
  while ($tag = $query_tags->fetch()) {

    $resultat = $insert_cmt->execute(array(
      "id_utilisateur" => $tag["id_utilisateur"],
      "id_message" => $tag["id_message"],
      "tag" => $tag["tag"],
      "date_creation" => $tag["date_creation"],
      "date_modification" => $tag["date_modification"]
    ));
    
    print("<br>Tag sur message #{$tag["id_message"]} de @{$tag["id_utilisateur"]} valeur {$tag["tag"]} : " .($resultat ? "mis à jour" : "erreur"));

    error_log("Tag sur message #{$tag["id_message"]} de @{$tag["id_utilisateur"]} valeur {$tag["tag"]} : " .($resultat ? "mis à jour" : "erreur"));
  }
?>