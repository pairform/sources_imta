<?php

  //********************************************//
  //******** Structure de base de WS ***********//
  //********************************************//
  
  try {
    $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET CHARACTER SET utf8");
    // $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

  } catch (Exception $e) {
    error_log($e->getMessage());
  }
  print('Migration des niveaux utilisateurs :<br>');
      	
  //Récupération de toutes les entrées de cape_utilisateurs_categorie
  $query_cuc = $dbh->query('SELECT * FROM cape_utilisateurs_categorie ORDER BY score DESC');
  //Préparation de query sur cape_utilisateurs_categorie
  $query_cucc = $dbh->prepare('INSERT IGNORE INTO cape_utilisateurs_categorie_collection VALUES (
    :id_ressource, 
    :id_utilisateur, 
    :id_categorie, 
    :id_categorie_temp, 
    :score)');
  //Préparation de query sur cape_utilisateurs_categorie
  $query_curp = $dbh->prepare('INSERT IGNORE INTO cape_utilisateurs_capsules_parametres VALUES (
    :id_capsule, 
    :id_utilisateur, 
    :notification_mail)');
  //Préparation de query sur cape_capsules
  $query_cc = $dbh->prepare('SELECT id_ressource FROM cape_capsules WHERE id_capsule=:id_capsule');
  
  //Pour chaque niveau par ressource
  while ($niveau_ressource = $query_cuc->fetch()) {
    //On récupère la capsule correspondante
    $query_cc->bindParam(':id_capsule', $niveau_ressource['id_ressource']);
    $query_cc->execute();
    $ressource_collection = $query_cc->fetch();

    //Et on insère dans la nouvelle table cape_utilisateurs_categorie
    $resultat = $query_cucc->execute(array(
      ':id_ressource' => $ressource_collection['id_ressource'],
      ':id_utilisateur' => $niveau_ressource['id_utilisateur'],
      ':id_categorie' => $niveau_ressource['id_categorie'],
      ':id_categorie_temp' => $niveau_ressource['id_categorie_temp'],
      ':score' => $niveau_ressource['score']));
    
    $resultat2 = $query_curp->execute(array(
      ':id_capsule' => $niveau_ressource['id_ressource'],
      ':id_utilisateur' => $niveau_ressource['id_utilisateur'],
      ':notification_mail' => $niveau_ressource['notification_mail']
      ));

    print('<br>Ligne - id_ressource '.$ressource_collection['id_ressource'].' / id_ressource'.$niveau_ressource['id_ressource'].' / id_utilisateur'.$niveau_ressource['id_utilisateur'].' : '.($resultat2 ? 'succes.' : 'erreur!'));

    error_log('Ligne - id_ressource '.$ressource_collection['id_ressource'].' / id_ressource'.$niveau_ressource['id_ressource'].' / id_utilisateur'.$niveau_ressource['id_utilisateur'].' : '.($resultat2 ? 'succes.' : 'erreur!'));
}
?>