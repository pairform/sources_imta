<?php
	//********************************************//
	//************** editerAvatar.php *************//
	//********************************************//

	/*
	*
	* Edite l'avatar
	*
	*  le param est dans $_FILES['avatar']
	*

	*/

	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
	include_once("../accomplissement/gagnerSucces.php");

	// error_log('$_FILE : ' . print_r($_FILE, true));
	// error_log('$_POST : ' . print_r($_POST, true));
	// error_log('$_GET : ' . print_r($_GET, true));

  	$os = $_POST['os'];
  	$version = $_POST['version'];

	if($os == "web" && $version >= "2"){
		$id_utilisateur = $_POST['id_utilisateur'];
		$utilisateur_connecte = get_user($id_utilisateur);

	}
	else{
		$id_utilisateur = elgg_get_logged_in_user_guid();
		$utilisateur_connecte = get_entity($id_utilisateur);
	}

	// error_log(print_r($_FILES, true));
	if (!$utilisateur_connecte || !($utilisateur_connecte instanceof ElggUser) || !$utilisateur_connecte->canEdit()) {
		register_error(elgg_echo('avatar:upload:fail'));
		error_log("avatar:upload:fail1");
		echo json_encode(array('status' => "ko",'message' => "L'upload a échoué, veuillez réessayer."));

		exit();
	}

	if ($_FILES['avatar']['error'] != 0) {
		register_error(elgg_echo('avatar:upload:fail'));
		error_log("avatar:upload:fail2");
		echo json_encode(array('status' => "ko",'message' => "L'upload a échoué, veuillez réessayer."));
		exit();
	}

	$icon_sizes = elgg_get_config('icon_sizes');

	// get the images and save their file handlers into an array
	// so we can do clean up if one fails.
	$files = array();
	foreach ($icon_sizes as $name => $size_info) {
		$resized = get_resized_image_from_uploaded_file('avatar', $size_info['w'], $size_info['h'], $size_info['square'], $size_info['upscale']);
		if ($resized) {
			//@todo Make these actual entities.  See exts #348.
			$file = new ElggFile();
			$file->owner_guid = $id_utilisateur;
			$file->setFilename("profile/{$id_utilisateur}{$name}.jpg");
			$file->open('write');
			$file->write($resized);
			$file->close();
			$files[] = $file;
		} else {
			// cleanup on fail
			foreach ($files as $file) {
				$file->delete();
			}

			register_error(elgg_echo('avatar:resize:fail'));
			error_log("avatar:resize:fail");

			if ($os == 'and') {
				echo json_encode(array('status' => "ko",'message' => "erreur_taille_avatar"));
			} else {
				echo json_encode(array('status' => "ko",'message' => "Échec du redimensionnement, l'image est trop grande. Veuillez réessayer avec une image plus petite."));
			}
			
			exit();
		}
	}


	// reset crop coordinates
	$utilisateur_connecte->x1 = 0;
	$utilisateur_connecte->x2 = 0;
	$utilisateur_connecte->y1 = 0;
	$utilisateur_connecte->y2 = 0;

	$utilisateur_connecte->icontime = time();
	

	elgg_trigger_event('profileiconupdate', $utilisateur_connecte->type, $utilisateur_connecte);

	//Renvoi de la nouvelle url pour le web, pour qu'on puisse updater la valeur en local
  	$avatar_url = $utilisateur_connecte->getIconURL("tiny");

	echo  json_encode(array('status' => "ok", "avatar_url" => $avatar_url));
	gagnerSuccesAvatar();

?>