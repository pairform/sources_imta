<?php
error_reporting(E_ALL);
  //********************************************//
  //*********** gagnerSucces.php ***********//
  //********************************************//
  
  /* Fait gagner des succes a l'utilisateur .
   *
   * Est a utiliser en tant que library
   * On appelle les fonctions quand des succes peuvent être gagnés à cet endroit.
   * 
   * Voir Test
   *


  gagnerSuccesParticipant(); // Devenir participant
  gagnerSuccesCollaborateur(); // Devenir collaborateur
  gagnerSuccesAnimateur(); // Devenir animateur
  gagnerSuccesPoints(); // Gagner des points
  gagnerSuccesRessource(); // Acceder/participer à des ressources.
  gagnerSuccesAvatar(); // Changer d’avatar
  gagnerSuccesMessage(); // Ecrire un message
  gagnerSuccesVote(); // Voter un message
  gagnerSuccesRepondre(); // Repondre à un message
  gagnerSuccesRecevoirReponses(id_utilisateur); // Recevoir une reponse.
  gagnerSuccesParticiperDefi(); // Participer à un défi
  gagnerSuccesUtiliteRecu(id_utilisateur); // Recevoir un vote.
  gagnerSuccesRejoindreClasse(); // Rejoindre une classe
  gagnerSuccesMedaille(id_utilisateur); // Recevoir une medaille.

   * 
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once('../ws/Pusher.php');

  include_once("../push/api/api.php");



  

  define("PARTICIPANT1", 1);
  define("PARTICIPANT3", 4);
  define("PARTICIPANT5", 5);
  define("PARTICIPANT10", 10);

  define("COLLABORATEUR1", 6);
  define("COLLABORATEUR3", 7);
  define("COLLABORATEUR5", 8);
  define("COLLABORATEUR10", 24);

  define("ANIMATEUR1", 9);
  define("ANIMATEUR3", 10);
  define("ANIMATEUR5", 11);
  define("ANIMATEUR10", 25);

  define("POINTSMONO100", 13);

  define("POINTSMULTI200", 14);

  define("TOUTLESSUCCES", 15); 

  define("AVATAR", 12);

  define("RESSOURCE1", 17);
  define("RESSOURCE2", 18);
  define("RESSOURCE3", 19);
  define("RESSOURCE5", 20);
  define("RESSOURCE10", 21);
  define("RESSOURCE25", 22);

  define("CERCLE", 16);

  define("MESSAGE5", 26);
  define("MESSAGE10", 27);
  define("MESSAGE20", 28);
  define("MESSAGE30", 29);
  define("MESSAGE50", 30);
  define("MESSAGE75", 31);
  define("MESSAGE100", 32);

  define("VOTE5", 33);
  define("VOTE10", 34);
  define("VOTE20", 35);
  define("VOTE30", 36);
  define("VOTE50", 37);
  define("VOTE75", 38);
  define("VOTE100", 39);

  define("REPONDRE5", 40);
  define("REPONDRE10", 41);
  define("REPONDRE20", 42);
  define("REPONDRE30", 43);
  define("REPONDRE50", 44);
  define("REPONDRE75", 45);
  define("REPONDRE100", 46);

  define("RECEVOIRREPONSES5", 47);
  define("RECEVOIRREPONSES10", 48);
  define("RECEVOIRREPONSES25", 49);

  define("DEFI5", 50);
  define("DEFI10", 51);
  define("DEFI20", 52);
  define("DEFI30", 53);
  define("DEFI50", 54);
  define("DEFI75", 55);
  define("DEFI100", 56);

  define("UTILITEREPANDUE5", 57);
  define("UTILITEREPANDUE10", 58);
  define("UTILITEREPANDUE20", 59);
  define("UTILITEREPANDUE30", 60);
  define("UTILITEREPANDUE50", 61);

  define("UTILE5", 62);
  define("UTILE10", 63);
  define("UTILE20", 64);
  define("UTILE30", 65);
  define("UTILE50", 66);
  define("UTILE75", 67);
  define("UTILE100", 68);

  define("UTILE1003", 69);
  define("UTILE1005", 70);
  define("UTILE10010", 71);

  define("CLASSE", 72);

  define("MEDAILLE", 73);
  define("MEDAILLE3", 74);
  define("MEDAILLE5", 75);
  define("MEDAILLE10", 76);


  
  // TEST
  
  /*gagnerSuccesParticipant();
  gagnerSuccesCollaborateur();
  gagnerSuccesAnimateur();
  gagnerSuccesPoints();
  gagnerSuccesRessource();
  gagnerSuccesAvatar();
  gagnerSuccesMessage();
  gagnerSuccesVote();
  gagnerSuccesRepondre();
  gagnerSuccesRecevoirReponses(74);
  gagnerSuccesParticiperDefi();
  gagnerSuccesUtiliteRecu(74);
  gagnerSuccesRejoindreClasse();
  gagnerSuccesMedaille(35);
  gagnerToutLesSucces();*/



  function gagnerSuccesParticipant($id_utilisateur = null){
    gagnerSuccesRang(1,PARTICIPANT1,PARTICIPANT3,PARTICIPANT5,PARTICIPANT10,$id_utilisateur);

  }

  function gagnerSuccesCollaborateur($id_utilisateur = null){
    gagnerSuccesRang(2,COLLABORATEUR1,COLLABORATEUR3,COLLABORATEUR5,COLLABORATEUR10,$id_utilisateur);
  }

  function gagnerSuccesAnimateur($id_utilisateur = null){
    gagnerSuccesRang(3,ANIMATEUR1,ANIMATEUR3,ANIMATEUR5,ANIMATEUR10,$id_utilisateur);
  }

  function gagnerSuccesPoints($id_utilisateur= null){
    if ( $id_utilisateur == null ){
      if($os == "web" && $version >= "2"){
        global $_POST;
        $id_utilisateur = $_POST['id_utilisateur'];
      }
      else
        $id_utilisateur = elgg_get_logged_in_user_guid();
    }
    if ( !$id_utilisateur )return;
    // MONO
    $result = mysql_query("SELECT MAX(score) 
        FROM  `cape_utilisateurs_categorie` 
        WHERE  `id_utilisateur` =$id_utilisateur");

    if ( $result){
      $row =mysql_fetch_row($result);
      $number = $row[0] ; 
      if ( $number >= 100 ) gagnerSucces($id_utilisateur,POINTSMONO100);
    }
    //MULTI
    $result2 = mysql_query("SELECT SUM(score) 
        FROM  `cape_utilisateurs_categorie` 
        WHERE  `id_utilisateur` =$id_utilisateur");

    if ( $result2){
      $row2 =mysql_fetch_row($result2);
      $number2 = $row2[0] ; 
      if ( $number2 >= 200 ) gagnerSucces($id_utilisateur,POINTSMULTI200);
    }

  }

  function gagnerSuccesAvatar($id_utilisateur = null){
    if ( $id_utilisateur == null ){
      if($os == "web" && $version >= "2"){
        global $_POST;
        $id_utilisateur = $_POST['id_utilisateur'];
      }
      else
        $id_utilisateur = elgg_get_logged_in_user_guid();
    }
    if ( !$id_utilisateur )return;
    gagnerSucces($id_utilisateur,AVATAR);
  }


  function gagnerSuccesRessource($id_utilisateur = null){
        if ( $id_utilisateur == null ){
          if($os == "web" && $version >= "2"){
            global $_POST;
        $id_utilisateur = $_POST['id_utilisateur'];
          }
      else
        $id_utilisateur = elgg_get_logged_in_user_guid();
        }
        if ( !$id_utilisateur )return;
    // MONO
    $result = mysql_query("SELECT COUNT(*) 
        FROM  `cape_utilisateurs_categorie` 
        WHERE  `id_utilisateur` =$id_utilisateur");

    if ( $result){
      $row =mysql_fetch_row($result);
      $number = $row[0] ; 
      if ( $number >= 1 ) gagnerSucces($id_utilisateur,RESSOURCE1);
      if ( $number >= 2 ) gagnerSucces($id_utilisateur,RESSOURCE2);
      if ( $number >= 3 ) gagnerSucces($id_utilisateur,RESSOURCE3);
      if ( $number >= 5 ) gagnerSucces($id_utilisateur,RESSOURCE5);
      if ( $number >= 10 ) gagnerSucces($id_utilisateur,RESSOURCE10);
      if ( $number >= 25 ) gagnerSucces($id_utilisateur,RESSOURCE25);
    }
  }

  function gagnerSuccesCercle($id_utilisateur = null){
    if ( $id_utilisateur == null ){
      if($os == "web" && $version >= "2"){
        global $_POST;
        $id_utilisateur = $_POST['id_utilisateur'];
      }
      else
        $id_utilisateur = elgg_get_logged_in_user_guid();
    }
    if ( !$id_utilisateur )return;
    gagnerSucces($id_utilisateur,CERCLE);
  }

  function gagnerSuccesMessage(){
    if($os == "web" && $version >= "2"){
      global $_POST;
      $id_utilisateur = $_POST['id_utilisateur'];
    }
    else
      $id_utilisateur = elgg_get_logged_in_user_guid();
    if ( !$id_utilisateur )return;
    $options               = array(
                'owner_guid' => $id_utilisateur,
                'type' => 'object',
                'limit' => 0,
                'subtypes' => array('blog')
            );    
    $number = count(elgg_get_entities($options));
    if ( $number >= 5 ) gagnerSucces($id_utilisateur,MESSAGE5);
    if ( $number >= 10 ) gagnerSucces($id_utilisateur,MESSAGE10);
    if ( $number >= 20 ) gagnerSucces($id_utilisateur,MESSAGE20);
    if ( $number >= 30 ) gagnerSucces($id_utilisateur,MESSAGE30);
    if ( $number >= 50 ) gagnerSucces($id_utilisateur,MESSAGE50);
    if ( $number >= 75 ) gagnerSucces($id_utilisateur,MESSAGE75);
    if ( $number >= 100 ) gagnerSucces($id_utilisateur,MESSAGE100);
   
  }

  function gagnerSuccesVote(){
    if($os == "web" && $version >= "2"){
      global $_POST;
      $id_utilisateur = $_POST['id_utilisateur'];
    }
    else
      $id_utilisateur = elgg_get_logged_in_user_guid();
    if ( !$id_utilisateur )return;
    $options               = array(
                'owner_guid' => $id_utilisateur,
                'annotation_names' => array('likes','dislikes'),
                'limit' => 0
            );    
    $number = count(elgg_get_annotations($options));
    if ( $number >= 5 ) gagnerSucces($id_utilisateur,VOTE5);
    if ( $number >= 10 ) gagnerSucces($id_utilisateur,VOTE10);
    if ( $number >= 20 ) gagnerSucces($id_utilisateur,VOTE20);
    if ( $number >= 30 ) gagnerSucces($id_utilisateur,VOTE30);
    if ( $number >= 50 ) gagnerSucces($id_utilisateur,VOTE50);
    if ( $number >= 75 ) gagnerSucces($id_utilisateur,VOTE75);
    if ( $number >= 100 ) gagnerSucces($id_utilisateur,VOTE100);
   
  }


  function gagnerSuccesRepondre(){
    if($os == "web" && $version >= "2"){
      global $_POST;
      $id_utilisateur = $_POST['id_utilisateur'];
    }
    else
      $id_utilisateur = elgg_get_logged_in_user_guid();
    if ( !$id_utilisateur )return;
    $options               = array(
                'owner_guid' => $id_utilisateur,
                'annotation_names' => array('parent'),
                'limit' => 0

            );    
    $number = count(elgg_get_annotations($options));
    if ( $number >= 5 ) gagnerSucces($id_utilisateur,REPONDRE5);
    if ( $number >= 10 ) gagnerSucces($id_utilisateur,REPONDRE10);
    if ( $number >= 20 ) gagnerSucces($id_utilisateur,REPONDRE20);
    if ( $number >= 30 ) gagnerSucces($id_utilisateur,REPONDRE30);
    if ( $number >= 50 ) gagnerSucces($id_utilisateur,REPONDRE50);
    if ( $number >= 75 ) gagnerSucces($id_utilisateur,REPONDRE75);
    if ( $number >= 100 ) gagnerSucces($id_utilisateur,REPONDRE100);
   
  }

  function gagnerSuccesRecevoirReponses($id_utilisateur){
    if ( !$id_utilisateur )return;
    $maxReponseAvecAuteurUnique;
    // Sur tout les messages.
    $options               = array(
                'owner_guid' => $id_utilisateur,
                'type' => 'object',
                'limit' => 0,
                'subtypes' => array('blog')
            );  
    $messages = elgg_get_entities($options);
    // Je prend toute les réponses.
    foreach ($messages as &$message) {
      $id_message = $message->__get("guid");
       $optionsAnnotation               = array(
                'annotation_names' => array('parent'),
                'limit' => 0,
                'annotation_values' => $id_message
            );    
      $annotations = elgg_get_annotations($optionsAnnotation);
      $owner_guids= array();
      // Je prend tout les auteurs distincts.
      foreach ($annotations as &$annotation) {
        $owner_guids[] = $annotation->__get("owner_guid");
      }

      $number = count(array_unique($owner_guids));
      $maxReponseAvecAuteurUnique = max($number,$maxReponseAvecAuteurUnique);
      
    }  
    if ( $maxReponseAvecAuteurUnique >= 5 ) gagnerSucces($id_utilisateur,RECEVOIRREPONSES5);
    if ( $maxReponseAvecAuteurUnique >= 10 ) gagnerSucces($id_utilisateur,RECEVOIRREPONSES10);
    if ( $maxReponseAvecAuteurUnique >= 25 ) gagnerSucces($id_utilisateur,RECEVOIRREPONSES25);
   
  }

  function gagnerSuccesParticiperDefi(){
    if($os == "web" && $version >= "2"){
      global $_POST;
      $id_utilisateur = $_POST['id_utilisateur'];
    }
    else
      $id_utilisateur = elgg_get_logged_in_user_guid();
    if ( !$id_utilisateur )return;
    $number = 0;
    // Sur tout les defis
    $options               = array(
                'annotation_names' => array('est_defi'),
                'limit' => 0
            );  
    $annotations = elgg_get_annotations($options);
    // Je prend tout mes reponses de defi , defi par defi.
    foreach ($annotations as &$annotation) {
      if ( $annotation->__get("owner_guid") == $id_utilisateur) continue;
      $id_annotation = $annotation->__get("entity_guid");

       $optionsAnnotation               = array(
                'owner_guid' => $id_utilisateur,
                'annotation_names' => array('parent'),
                'limit' => 0,
                'annotation_values' => $id_annotation
            );    
       $annotationsPerso = elgg_get_annotations($optionsAnnotation);
       // si j'ai des reponses sur ce defi , +1 defi fait
       if (!empty($annotationsPerso)) $number++;
       
     }
    

    if ( $number >= 5 ) gagnerSucces($id_utilisateur,DEFI5);
    if ( $number >= 10 ) gagnerSucces($id_utilisateur,DEFI10);
    if ( $number >= 20 ) gagnerSucces($id_utilisateur,DEFI0);
    if ( $number >= 30 ) gagnerSucces($id_utilisateur,DEFI30);
    if ( $number >= 50 ) gagnerSucces($id_utilisateur,DEFI50);
    if ( $number >= 75 ) gagnerSucces($id_utilisateur,DEFI75);
    if ( $number >= 100 ) gagnerSucces($id_utilisateur,DEFI100);
   
  }

  function gagnerSuccesUtiliteRecu($id_utilisateur){
    if ( !$id_utilisateur )return;
    $numberVotesSurMessagesDifferents = 0;
    $maxVoteRecu = 0;
    $numberVote100 = 0;


    // Sur tout mes messages
        $options               = array(
                'owner_guid' => $id_utilisateur,
                'type' => 'object',
                'limit' => 0,
                'subtypes' => array('blog')
            );  
    $messages = elgg_get_entities($options);


    foreach ($messages as &$message) {
      $id_message = $message->__get("guid");
      $optionsAnnotation               = array(
                'annotation_names' => array('likes'),
                'limit' => 0,
                'guids' => $id_message
            );    
      $annotations = elgg_get_annotations($optionsAnnotation);

      if (!empty($annotations)) {
        $numberVotesSurMessagesDifferents++;
        $maxVoteRecu = max($maxVoteRecu ,count($annotations));
        if ( count($annotations) >= 100) $numberVote100 ++;
      }
       
     }


    
    if ( $numberVotesSurMessagesDifferents >= 5 ) gagnerSucces($id_utilisateur,UTILITEREPANDUE5);
    if ( $numberVotesSurMessagesDifferents >= 10 ) gagnerSucces($id_utilisateur,UTILITEREPANDUE10);
    if ( $numberVotesSurMessagesDifferents >= 20 ) gagnerSucces($id_utilisateur,UTILITEREPANDUE20);
    if ( $numberVotesSurMessagesDifferents >= 30 ) gagnerSucces($id_utilisateur,UTILITEREPANDUE30);
    if ( $numberVotesSurMessagesDifferents >= 50 ) gagnerSucces($id_utilisateur,UTILITEREPANDUE50);

    if ( $maxVoteRecu >= 5 ) gagnerSucces($id_utilisateur,UTILE5);
    if ( $maxVoteRecu >= 10 ) gagnerSucces($id_utilisateur,UTILE510);
    if ( $maxVoteRecu >= 20 ) gagnerSucces($id_utilisateur,UTILE520);
    if ( $maxVoteRecu >= 30 ) gagnerSucces($id_utilisateur,UTILE530);
    if ( $maxVoteRecu >= 50 ) gagnerSucces($id_utilisateur,UTILE550);
    if ( $maxVoteRecu >= 75 ) gagnerSucces($id_utilisateur,UTILE575);
    if ( $maxVoteRecu >= 100 ) gagnerSucces($id_utilisateur,UTILE5100);

    if ( $numberVote100 >= 3) gagnerSucces($id_utilisateur,UTILE1003);
    if ( $numberVote100 >= 5 ) gagnerSucces($id_utilisateur,UTILE1005);
    if ( $numberVote100 >= 10 ) gagnerSucces($id_utilisateur,UTILE10010);
   
  }

  function gagnerSuccesRejoindreClasse(){
    if($os == "web" && $version >= "2"){
      global $_POST;
      $id_utilisateur = $_POST['id_utilisateur'];
    }
    else
      $id_utilisateur = elgg_get_logged_in_user_guid();
    gagnerSucces($id_utilisateur,CLASSE);
  }


  function gagnerSuccesMedaille($id_utilisateur){
    if ( !$id_utilisateur )return;
    $options               = array(
                'owner_guid' => $id_utilisateur,
                'annotation_names' => array('medaille'),
                'limit' => 0
            );    
    $number = count(elgg_get_annotations($options));
    if ( $number >= 1 ) gagnerSucces($id_utilisateur,MEDAILLE);
    if ( $number >= 3 ) gagnerSucces($id_utilisateur,MEDAILLE3);
    if ( $number >= 5 ) gagnerSucces($id_utilisateur,MEDAILLE5);
    if ( $number >= 10 ) gagnerSucces($id_utilisateur,MEDAILLE10);
   
  }

  function gagnerToutLesSucces(){
    if($os == "web" && $version >= "2"){
      global $_POST;
      $id_utilisateur = $_POST['id_utilisateur'];
    }
    else
      $id_utilisateur = elgg_get_logged_in_user_guid();
    if ( !$id_utilisateur )return;
   $result = mysql_query("SELECT COUNT(*)
    FROM `cape_succes`
    WHERE (id_succes) NOT IN
      (SELECT `cape_succes`.`id_succes`
      FROM `cape_succes`,`cape_utilisateurs_succes` 
      WHERE `cape_succes`.`id_succes` = `cape_utilisateurs_succes`.`id_succes`
      AND `id_utilisateur` = $id_utilisateur)");
   $row =mysql_fetch_row($result);
    $number = $row[0] ; 
    if ($number ==1) gagnerSucces($id_utilisateur,TOUTLESSUCCES);

  }

  //Privé


  function gagnerSuccesRang($rang,$succes1,$succes2,$succes3,$succes4,$id_utilisateur){
    if ( $id_utilisateur == null ){
      if($os == "web" && $version >= "2"){
        global $_POST;
        $id_utilisateur = $_POST['id_utilisateur'];
      }
      else
        $id_utilisateur = elgg_get_logged_in_user_guid();
    }
    if ( !$id_utilisateur )return;
    $result = mysql_query("SELECT COUNT(*) 
        FROM  `cape_utilisateurs_categorie` 
        WHERE  `id_utilisateur` =$id_utilisateur
        AND  `id_categorie` >= $rang");

    if ( $result){
      $row =mysql_fetch_row($result);
      $number = $row[0] ; 
      if ( $number >= 1 ) gagnerSucces($id_utilisateur,$succes1);
      if ( $number >= 3 ) gagnerSucces($id_utilisateur,$succes2);
      if ( $number >= 5 ) gagnerSucces($id_utilisateur,$succes3);
      if ( $number >= 10 ) gagnerSucces($id_utilisateur,$succes4);
    }
  }

  function gagnerSucces($id_utilisateur,$id_succes){
    $result = mysql_query("SELECT * 
      FROM  `cape_utilisateurs_succes` 
      WHERE  `id_utilisateur` =$id_utilisateur
      AND  `id_succes` =$id_succes");
    if (!$result) return;
    if (!mysql_fetch_assoc($result)){
      $time = time();
      mysql_query("INSERT INTO  `cape_utilisateurs_succes` (  `id_utilisateur` ,  `id_succes` , `date_gagne`) 
        VALUES ('$id_utilisateur',  '$id_succes', $time)");


      $result = mysql_query("SELECT * 
        FROM `cape_succes_langues` 
        WHERE `id_succes` =$id_succes");

      // if(!$pusher)
      //   $pusher = new Pusher('0b0142b03d240425e90f', '2ae0897085fc6c512871', '55928');

      //Pour ce succès
      while($row = mysql_fetch_assoc($result))
      {
        // $pusher->trigger('pairform', 'new_success', array('id_utilisateur' => $id_utilisateur, 'nom' => $row['nom'],'description' => $row['description'],'image_blob' => base64_encode($row['image_blob'])));
        global $api;
        error_log(print_r($api,true));
        $api->sendMessage($id_utilisateur, "us", "Vous avez gagné un succès!", array("c" => $row['nom'], "st" => "PTSGAGNERSUCCES"));
      }
    }
    gagnerToutLesSucces();

  }

?>

