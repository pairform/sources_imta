<?php

  //********************************************//
  //******** changerVueNotification.php ********//
  //********************************************//
  
  /*
  * Met la / les notification(s) à vue(s)
  * 
  */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");


  if($os == "web" && $version >= "2")
    $id_utilisateur = $_POST['id_utilisateur'];
  else
    $id_utilisateur = elgg_get_logged_in_user_guid();
  $id_notifications = isset($_POST['id_notifications']) ? implode(",",json_decode($_POST['id_notifications'])) : null;

  if ($id_utilisateur && $id_notifications) {
    try {
      $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $dbh->exec("SET CHARACTER SET utf8");
          $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

    } catch (Exception $e) {
      error_log($e->getMessage());
    }
    error_log($id_notifications);

    //Récuperation des notifications utilisateur
    $ret = $dbh->exec("UPDATE `cape_notifications` 
                SET `date_vue` = NOW()
                WHERE `id_notification` IN($id_notifications)");

    
      echo(json_encode(array("status" => "ok")));
  }
  else
    echo(json_encode(array("status" => "ko", "message" => "user_logged_out")));
      
?>