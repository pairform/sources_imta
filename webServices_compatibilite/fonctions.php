<?php
//Fonction d'envoi de requête POST
//$url : [String] de l'url complète à appeler
//$params : [Array] POST. Nul si aucune option
//Return : [JSON] Retour de l'appel le cas échéant


function request($url, $method = "POST", $params = array()){
	
	//Si l'URL n'est pas absolue
	if (!preg_match("/^(http)/", $url)) {
		//On détermine l'environnement actuel
		//Dev
		if (preg_match("/(imedia.emn.fr)/", $_SERVER["HTTP_HOST"]))
			$url = "http://imedia.emn.fr:3000" . $url;
		//Prod
		else
			$url = "https://www.pairform.fr/node" . $url;

	}
	$serialized_params = http_build_query($params);

	if ($method == "POST" || $method == "PUT") {
		$opts = array('http' =>	array(
			'method' => $method,
			'header' =>'Content-type: application/x-www-form-urlencoded',
			'content' =>$serialized_params
		));
	}
	else{
		$opts = array('http' =>	array(
			'method' => $method
		));
		$url = $url ."?" .$serialized_params;
	}
	
	$context = stream_context_create($opts);
	return file_get_contents($url, false, $context);
}

?>