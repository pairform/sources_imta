<?php

  require_once("../fonctions.php");
  echo request("/webServices/reseau/liste","GET", $_REQUEST);

// //********************************************//
// //*********** afficherListeCercle.php ***********//
// //********************************************//

// /* affiche la liste des cercles de l'utilisateur connecte.
//  *
//  * 
//  * Si Android :
//  *
//   {
//     "cercles":[
//       {
//       "id_collection":"4",
//       "nom":"classe1",
//       "profils":[
//         {
//           "name":"Admin",
//           "id_utilisateur":35,
//           "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
//         },
//         {
//           "name":"Azerty",
//           "id_utilisateur":68,
//           "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
//         }
//       ]}
//       {...}
//     ]
//     "classes":[
//       {
//       "id_collection":"4",
//       "nom":"classe1",
//       "cle":"abcd"
//       "profils":[
//         {
//           "name":"Admin",
//           "id_utilisateur":35,
//           "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
//         }
//       ]}
//       {...}
//     ]
//     "classesRejointes":[
//       ...
//     ]
//   }
//  *
//  * 
//  * Si iOs ou web :
//  *
//   {
//     "cercles":[
//       {
//       "id_collection":"4",
//       "nom":"classe1",
//       "profils":[
//         {
//           "name":"Admin",
//           "id_utilisateur":35,
//           "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
//         },
//         {
//           "name":"Azerty",
//           "id_utilisateur":68,
//           "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
//         }
//       ]}
//       {...}
//     ]
//     "classes":[
//       {
//       "id":"4",
//       "nam":"classe1",
//       "cle":"abcd"
//       "members":[
//         {
//           "name":"Admin",
//           "id_utilisateur":35,
//           "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=small"
//         }
//       ]}
//       {...}
//     ]
//     "classesRejointes":[
//       ...
//     ]
//   }
//  */

//   include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
//   $os = $_POST['os'];
//   $version = $_POST['version'];

//   if($os == "web" && $version >= "2")
//     $id_utilisateur = $_POST['id_utilisateur'];
//   else
//     $id_utilisateur = elgg_get_logged_in_user_guid();

//   $user = get_user($id_utilisateur);

// // Utilisateur pas connecté
//   if ( empty($user)) {
//     $return = json_encode(array("cercles"=>array()));
//   }
//   else{
//     $id_utilisateur = $user->guid;

//   //Première phase :
//   //On récupère toutes les collections appartenant à l'utilisateur actif (cad cercles et classes)
//   //C'est donc lui qui les a créé : ce sont soit ses propres cercles, soit les classes qu'il a créé.

//     $collections = get_user_access_collections ($id_utilisateur);

//   //Pour chacune
//     foreach ($collections as &$collection) {
//     //On récupère tous les membres de cette collection
//       $members = get_members_of_access_collection($collection->id);
//       $members_final = array();
//       foreach ($members as &$member) 
//       {
//         if (($os == 'ios') && ($version == '1.0.202')) {
//           $members_final[] = (object) array('name' => $member->username, 'guid' => $member->guid,'image' => $member->getIconURL('small'));
//         } else if( $os == "and" || ($os == "web" && $version == "2")) {
//           $members_final[] = (object) array('username' => $member->username, 'name' => $member->name, 'id_utilisateur' => $member->guid,'image' => $member->getIconURL('medium'));
//         } else {
//           $members_final[] = (object) array('name' => $member->username, 'id_utilisateur' => $member->guid,'image' => $member->getIconURL('small'));
//         }
//     // Dissociation classe et cercle. avec l'id du cercle/classe/
//       }
//       $collection_id = $collection->id;
//       $requete = mysql_query("SELECT * FROM `cape_classes` WHERE `id_classe` = $collection_id");
//       $row_message   = mysql_fetch_assoc($requete);  

//       if( $os == "and" || ($os == "web" && $version == "2")) {
//       // Si resultat vide , c'est un cercle normal 
//         if(empty($row_message)) {
//           $cercles[] = (object) array('id_collection'=>$collection->id,'nom'=>$collection->name,'profils'=>$members_final);
//           $namesCercles[] = $collection->name;
//         } else {
//           $classes[] = (object) array('id_collection'=>$collection->id,'nom'=>$collection->name,'profils'=>$members_final,'cle'=>$row_message['cle']);
//           $namesClasses[] = $collection->name;
//         }
//       } else {   
//       // Si resultat vide , c'est un cercle normal 
//         if(empty($row_message)) {
//           $cercles[] = (object) array('id'=>$collection->id,'name'=>$collection->name,'members'=>$members_final);
//           $namesCercles[] = $collection->name;
//         } else {
//           $classes[] = (object) array('id'=>$collection->id,'name'=>$collection->name,'members'=>$members_final,'cle'=>$row_message['cle']);
//           $namesClasses[] = $collection->name;
//         }
//       }
//     // On range par odre alphhabetique.
//       if ( !empty($namesCercles)) array_multisort($namesCercles,$cercles);
//       if ( !empty($namesClasses)) array_multisort($namesClasses,$classes);
//     }
//     if ( empty($cercles) ) $cercles = array();
//     if ( empty($classes) ) $classes = array();


//   //Deuxième phase :
//   //On récupère toutes les classes auxquelles l'utilisateur appartient
//   //Elles sont stockées dans la table cape_utilisateurs_classes
//     $query = mysql_query("SELECT cc.id_classe, cc.cle FROM cape_utilisateurs_classes AS cuc, cape_classes AS cc WHERE cuc.id_utilisateur = $id_utilisateur AND cc.id_classe = cuc.id_classe");

//   //Pour chaque classe trouvée
//     while ($row = mysql_fetch_assoc($query)) { 
//       $id_classe = $row['id_classe'];
//       $cle = $row['cle'];

//     //On récupère tous les membres de cette classe
//       $members = get_members_of_access_collection($id_classe);
//       $members_final = array();

//       foreach ($members as &$member) {
//         if( $os == "and" || ($os == "web" && $version == "2")) {
//           $members_final[] = (object) array('username' => $member->username, 'name' => $member->name, 'id_utilisateur' => $member->guid,'image' => $member->getIconURL('medium'));
//         } else {
//           $members_final[] = (object) array('name' => $member->username, 'id_utilisateur' => $member->guid,'image' => $member->getIconURL('small'));
//         }
//       }
//       $collection = get_access_collection($id_classe);

//       if( $os == "and" || ($os == "web" && $version == "2")) {
//         $classesRejointes[] = (object) array('id_collection'=>$id_classe,'nom'=>$collection->name,'profils'=>$members_final,'cle'=>$cle);
//       } else {
//         $classesRejointes[] = (object) array('id'=>$id_classe,'name'=>$collection->name,'members'=>$members_final,'cle'=>$cle);
//       }
//     }

//     if ( empty($classesRejointes) ) $classesRejointes = array();

//     if($os == "web" && $version == "2")
//       $return = json_encode(array("status" => "ok", "cercles"=>$cercles,"classes"=>$classes, "classesRejointes"=>$classesRejointes));
//     else
//       $return = json_encode(array("cercles"=>$cercles,"classes"=>$classes, "classesRejointes"=>$classesRejointes));
    
//     echo $return;
//   }

?>