<?php
    /*
     * Liste les ressources téléchargeables
    * Paramètres :
    *
    * id_elgg (int) - id de l'utilisateur (obsolete)
    * langues_affichage (string) - liste des id langues des ressources à afficher
    *
    * Retour
    "ressources":[
    {
    "id_ressource":"2",
    "nom_court":"E-diagnostic",
    "nom_long":"E-diag - Cours de sensibilisation aux principes de diagnostic",
    "url_mob":"http://imedia.emn.fr/SupCast/Ressources/E-diag",
    "icone_blob":"iVBORw0KGgoJggg==",
    "description":"Ce cours est une sensibilisation à des techniques de diagnostic de fiabilité et/ou d'erreur dans le contrôle de procédés industriels. Il illustre par de nombreux exemples différentes démarches du diagnostic, en précisant ce que l'on doit diagnostiquer et comment on le fait. Les aspects homme-machine y sont traités. ",
    "taille":"87",
    "etablissement":"UVHC",
    "theme":"Diagnostic",
    "auteur":"Dark Vador",
    "licence":"GPL", (car Dark Vador aime le libre et la choucroute)
    "date_release":"2012-03-09",
    "date_update":"2012-11-15",
    "langue":""
    },


     */
    
    // error_log("**********************************************");
    // error_log("listerRessources.php appelé"); 
    // error_log("**********************************************");

    
	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
    
	$os = $_POST['os'];
  $version = $_POST['version'];
  // $id_elgg = isset($_POST['id_elgg']) ? mysql_real_escape_string($_POST['id_elgg']) : 0;
  $id_deposeur = isset($_POST['id_deposeur']) ? intval(mysql_real_escape_string($_POST['id_deposeur'])) : 0;
  $liste_langues = isset($_POST['langues_affichage']) ? mysql_real_escape_string($_POST['langues_affichage']) : "1,2,3";
  $langue_app = isset($_POST['langue_app']) ? mysql_real_escape_string($_POST['langue_app']) : "3";
  

  define("ADMIN", 35);  

  // error_log(print_r($_POST,true));
  
  $query = sprintf("SELECT 
      `id_capsule`,
      `hash_capsule`,
      `cape_capsules`.`nom_court`,
      `cape_capsules`.`nom_long`,
      `url`,
      `url_web`,
      `cape_capsules`.`icone_blob`,
      `description`,
      `visible`,
      `cape_etablissements`.`nom_court` etablissement,
      `cape_etablissements`.`icone_blob` icone_etablissement,
      `cape_themes_langues`.`nom_long` theme,
      `auteur`,
      `licence`,
      `ajoute_par`,
      `date_update`,
      `date_release`,
      `taille`,
      `langue`,
      `code_langue`,
      `users`.`username`,
      `users`.`email`
      FROM `cape_capsules`, 
      `cape_etablissements`,
      `cape_themes_langues`,
      `cape_langues`,
      `sce_users_entity` AS `users`
      WHERE 
      `cape_themes_langues`.`id_theme` = `cape_capsules`.`theme` 
      and `cape_themes_langues`.`id_langue` = $langue_app 
      and `users`.`guid` = `cape_capsules`.`ajoute_par`
      and `cape_langues`.`id_langue` = `cape_capsules`.`langue` 
      and `cape_etablissements`.`id_etablissement` = `cape_capsules`.`etablissement` 
      and `cape_capsules`.`langue` in ($liste_langues)");   
                   
  //Si on est dans le cas de l'affichage des ressources ajouté par un utilisateur
  //Cas de la gestion des ressources dans le back-office
  if (($id_deposeur != 0) && ($id_deposeur != ADMIN)) {
    $query .= "AND `cape_capsules`.`ajoute_par` = " . $id_deposeur;
  }

  $resultQuery = mysql_query($query);
  if(!$resultQuery)
  {
    error_log(mysql_error());
    exit();
  }
    
                   
  $rows = array();
  

  
  while ($row = mysql_fetch_assoc($resultQuery)) 
  {
    // error_log('Icone Blob : ' . $row['icone_blob']); 
    //error_log('Ressource : ' . $row['nom']); 
    $visible = $row['visible'];
    
    //error_log('$visible (default) : '.$visible);
        //On checke si la ressource est en développement
    //Visible == false?
    if($visible == 0)
    {
              //Gestion des ressources en développement : récupération de l'ID user
      // $req = "SELECT id_ressource FROM cape_utilisateurs_ressources WHERE id_elgg = '$id_elgg'";
      // $res = mysql_query($req);
      
      // while($row = mysql_fetch_assoc($res))
      // {
      //   //Si il a le droit, on dit à l'application web que la ressource est visible.
      //   if($row['id_ressource'] == $id_elgg)
      //     $visible = 1;
          
      //   //error_log('$visible (modifié) : '.$visible);
      // }
      //Si on est sur le web
      if ($os == 'web')
        //On renvoie tout, visibile ou invisible si on est dans le cas d'ajout de ressource
        if ($id_deposeur != 0) 
          $visible = 1;
    }

      //Si on est sur le web
      if ($os == 'web') {
        //On envoie les ressources qui ont une url web
        //On renvoie tout, version web ou pas, si on est dans le cas d'ajout de ressource
        if ((trim($row['url_web']) != "") || ($id_deposeur != 0)) {
          array_push($rows, array('id_capsule' => $row['id_capsule'],
           'hash_capsule' => $row['hash_capsule'],
           'nom_court' => $row['nom_court'],
           'nom_long' => $row['nom_long'],
           'url_mob' => $row['url'],
           'url_web' => $row['url_web'],
           'icone_blob' => base64_encode($row['icone_blob']),
           'icone_etablissement' => base64_encode($row['icone_etablissement']),
           'description' => $row['description'],
           'taille' => $row['taille'],
           'code_langue' => $row['code_langue'],
           'etablissement' => $row['etablissement'],
           'auteur' => $row['auteur'],
           'licence' => $row['licence'],
           'visible' => $row['visible'],
           'theme' => $row['theme'],
           'date_release' => $row['date_release'],
           'date_update' => $row['date_update'],
           'deposeur_username' => $row['username'],
           'deposeur_email' => $row['email']));
        }    
      }
      //Si on est sur mobile
      else{
        //On envoie les ressources qui ont une url mobile
        if (trim($row['url']) != "") {
          $should_return = false;
          
          if (($os == "ios" && $version < "1.0.300") || $os == "and") {
            $should_return = $visible;
          }
          else{
            $should_return = true;
          }
          if ($should_return) {
            array_push($rows, array('id_ressource' => $row['id_capsule'],
             'nom_court' => $row['nom_court'],
             'nom_long' => $row['nom_long'],
             'url_mob' => $row['url'],
             'url_web' => $row['url_web'],
             'icone_blob' => base64_encode($row['icone_blob']),
             'icone_etablissement' => base64_encode($row['icone_etablissement']),
             'description' => $row['description'],
             'taille' => $row['taille'],
             'langue' => $row['langue'],
             'etablissement' => $row['etablissement'],
             'auteur' => $row['auteur'],
             'licence' => $row['licence'],
             'visible' => $row['visible'],
             'theme' => $row['theme'],
             'date_release' => $row['date_release'],
             'date_update' => $row['date_update']));
          }
        }    
      }
  }
  // error_log("Rows : " . print_r($rows,true));
  $return = json_encode(array('ressources' => $rows));
  print ($return);
  // error_log(print_r($return,true));
                       
?>