<?php

  //********************************************//
  //******** Structure de base de WS ***********//
  //********************************************//

include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
include_once("../accomplissement/gagnerSucces.php");

$os = $_POST['os'];
$version = $_POST['version'];
  //$id = $_POST['id_elgg'];

if($os == "web" && $version >= "2"){
  $id_utilisateur = $_POST['id_utilisateur'];
}
else{
  $id_utilisateur = elgg_get_logged_in_user_guid();
}

if (isset($id_utilisateur) && $id_utilisateur != 0) {

  //$id_utilisateur = 8328;
  $id_capsule = $_POST['id_capsule'];
    //On teste s'il a déjà des parametres sur cette capsule
  $result_has_capsule = mysql_query("SELECT * 
    FROM `cape_utilisateurs_capsules_parametres` 
    WHERE `id_utilisateur` = $id_utilisateur
    AND `id_capsule` = $id_capsule");
  //Sinon
  if ($result_has_capsule && !mysql_fetch_assoc($result_has_capsule)){
    //On insère les parametres, du coup, on peut considérer qu'il a désormais la capsule ;
    mysql_query("INSERT INTO `cape_utilisateurs_capsules_parametres` VALUES ($id_capsule, $id_utilisateur, 1, 0)");

    //Maintenant, a-t-il déjà des points sur cette ressource (en ayant participé sur une capsule ayant la même ressource par exemple),
    $result_has_level_for_capsule = mysql_query("SELECT * 
      FROM `cape_utilisateurs_categorie` 
      WHERE `id_utilisateur` = $id_utilisateur
      AND `id_ressource` = (SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_capsule)");
    //Sinon
    if ($result_has_level_for_capsule && !mysql_fetch_assoc($result_has_level_for_capsule)) {
      //On insère un rôle
      mysql_query("INSERT IGNORE INTO `cape_utilisateurs_categorie` (`id_ressource` , `id_utilisateur`, `id_categorie`,  `score`) 
        VALUES ((SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_capsule), $id_utilisateur, 0, 0)");
    }

    gagnerSuccesRessource();

    //S'il n'avait pas ce rang là avant, on renvoie les infos concernant le rang
    $result_query_rank = mysql_query("SELECT curp.id_capsule, ccl.id_ressource, cuc.score, curp.notification_mail, curp.afficher_noms_reels, cc.id_categorie, cc.nom
    FROM cape_utilisateurs_categorie AS cuc, cape_utilisateurs_capsules_parametres AS cucp, cape_capsules AS ccl, cape_categories AS cc, cape_utilisateurs AS cu
    WHERE cuc.id_utilisateur='$id_utilisateur'
    AND cu.id_elgg = '$id_utilisateur'
    AND `ccl`.`id_capsule` = $id_capsule
    AND `ccl`.`id_capsule` = `cucp`.`id_capsule`
    AND `cuc`.`id_ressource` = `ccl`.`id_ressource`
    AND `cucp`.`id_utilisateur` = '$id_utilisateur'
    AND  
        ((cuc.`id_categorie_temp` > cuc.`id_categorie` AND cc.`id_categorie` = cuc.`id_categorie_temp`)
        OR (cuc.`id_categorie_temp` <= cuc.`id_categorie` AND cc.`id_categorie` = cuc.`id_categorie` )
        )
    ORDER BY cuc.id_ressource ASC");

    $rank_for_capsule = mysql_fetch_assoc($result_query_rank);
    $rank_of_capsules_formatted = array();

    if($os == 'and') {
      $rank_of_capsules_formatted[] = array(
        'id_capsule' => $rank_for_capsule['id_capsule'],
        'id_categorie' => $rank_for_capsule['id_categorie'],
        'id_ressource' => $rank_for_capsule['id_ressource'],
        'nom_categorie' => $rank_for_capsule['nom'],
        'score' =>$rank_for_capsule['score'],
        'notification_email' =>$rank_for_capsule['notification_mail'],
        'afficher_noms_reels' =>$rank_for_capsule['afficher_noms_reels']);
    } else {
      $rank_of_capsules_formatted[$rank_for_capsule['id_capsule']] = array(
        'id_categorie' => $rank_for_capsule['id_categorie'],
        'id_ressource' => $rank_for_capsule['id_ressource'],
        'nom_categorie' => $rank_for_capsule['nom'],
        'score' =>$rank_for_capsule['score'],
        'notification_mail' =>$rank_for_capsule['notification_mail'],
        'afficher_noms_reels' =>$rank_for_capsule['afficher_noms_reels']);
    }

    echo json_encode(array("status" => "add", "rank" => $rank_of_capsules_formatted));
  }
  else {
    echo json_encode(array("status" => "ok"));
  }
} else {
  echo json_encode(array("status" => "ko", "message" => "erreur"));
}
?>

