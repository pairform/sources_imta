<?php

	require_once("../fonctions.php");
	echo request("/webServices/message/nombre/capsules","GET", $_REQUEST);

  //********************************************//
  //*********** recupererNombreMessages.php ***********//
  //********************************************//
  
  /* Recuperation du nombre de messages a un niveau particulier.
   *
   * Paramètres :
   * nom_page (string) - nom de la page concernée
   * id_ressource (int) - id de la ressource concernée
   * vue_trans (void) - option pour afficher en vue transversales
   * langues_affichage (string) - ids des langues à afficher (id séparés par des virgules)
   *
   * Utilisation :
   *
   * aucun argument : vue globale
   * id_ressource : vue au niveau de la ressource 
   * id_ressource et nom_page : vue au niveau de la page ( chaque object d'apprentissage )
   * 
   *
   *
   * Retour : 
   * {"nombreDeMessages":
   *  {
   *    "7":12
   *   }
   * }
   * 
   */
  
  
//   include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");



// //Connexion à la base de données
//   try {
//     $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
//     $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//     $dbh->exec("SET CHARACTER SET utf8");
//     // $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
//   } catch (Exception $e) {
//     error_log($e->getMessage());
//   }


//   $os = $_POST['os'];
//   $version = $_POST['version'];
//   $langues_affichage = isset($_POST['langues_affichage']) ? $_POST['langues_affichage'] : "1,2,3";


// //Si on a un utilisateur loggé 
// if($os == "web" && $version >= "2"){
//   $id_utilisateur = $_POST['id_utilisateur'];

// }
// else{
//   $id_utilisateur = elgg_get_logged_in_user_guid();
// }
// //Sinon, on met -1 pour ne choper aucun vote de qui que ce soit
// $id_utilisateur = isset($id_utilisateur) ? $id_utilisateur : -1000;


// $sql_select = "SELECT 
// id_ressource, 
// COUNT(id_message) AS count 
// FROM `cape_messages` `cm`
// JOIN `sce_entities` `e` ON `cm`.`id_message` = `e`.`guid` 

// WHERE `e`.`owner_guid` != 0
// AND `cm`.`langue` IN ($langues_affichage) 
//   AND 
//   (
//     (
//      (
//       (`e`.access_id IN 
//         (
//           SELECT CONCAT('2,', 
//             (SELECT GROUP_CONCAT(am.access_collection_id) 
//               FROM sce_access_collection_membership am 
//               LEFT JOIN sce_access_collections ag ON ag.id = am.access_collection_id 
//               WHERE am.user_guid = $id_utilisateur )
//             ,',',
//             (SELECT GROUP_CONCAT(ag.id) 
//               FROM sce_access_collections ag 
//             WHERE ag.owner_guid = $id_utilisateur)
//           ) 
//         )
//       )
//         OR `e`.access_id = 2
//         OR (e.owner_guid = $id_utilisateur)
//         OR IF($id_utilisateur != -1000, e.access_id = 1, NULL)  
//     )
//     AND (
//       `cm`.`supprime_par` = 0
//       OR `cm`.`supprime_par` = $id_utilisateur
//     )
//     AND `e`.`enabled`='yes'
//    ) OR $id_utilisateur = 35
//   )
//   GROUP BY id_ressource";


// // error_log($sql_select);
// $result = $dbh->query($sql_select);
// $key_value_array = $result->fetchAll(PDO::FETCH_KEY_PAIR);
// // error_log($key_value_array);
// $return = json_encode($key_value_array);

// die($return);

?>