<?php


  //********************************************//
  //********* cronPurgerMessageBAS.php *********//
  //********************************************//

/*
 * Supprime tous les messages du bac à sable. Absolument pas sécurisé, mais en même temps, pourquoi faire?
 * 
 * 
 * Retourne:
 *   - status (string)
 *   - message (string)
*/  

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  error_log("######## Tache cronPurgerMessageBAS.");

 //Suppression des messages dans la base et dans elgg
  $one_hour_before = time() - 3600;
  $result_query_messages = mysql_query("SELECT id_message 
                          FROM `cape_messages` `cm`
                            JOIN `sce_entities` `se` ON `se`.`guid` = `cm`.`id_message`
                          WHERE `cm`.`id_ressource` = 51 
                            AND `se`.`time_created` <= $one_hour_before");
  if ($result_query_messages) {
      $result_delete_messages = mysql_query("DELETE `cm`, `cn` 
                              FROM `cape_messages` AS `cm`,
                                   `sce_entities` AS `se`,
                                   `cape_notifications` AS `cn`
                              WHERE `cm`.`id_ressource` = 51 
                                AND `se`.`guid` = `cm`.`id_message`
                                AND `se`.`time_created` <= $one_hour_before
                                AND `cn`.`id_message` = `cm`.`id_message`");
      mysql_error();

    //Override des autorisations propres à chaque message
    elgg_set_ignore_access(true);
    while ($row = mysql_fetch_assoc($result_query_messages)) {
      if ($message = get_entity($row['id_message'])) {
        // $timestamp = $message->time_created;
        if (!delete_entity($message->guid, true)) {
          $errors[] = "Impossible de supprimer le message n°$message->guid.";
        }
      }
      else
          $warnings[] = "Message n°{$row['id_message']} déjà supprimé";
    }
    //Remise en place de l'override
    elgg_set_ignore_access(false);

    if ((count($errors) == 0) && $row) {
      if (!$result_delete_messages) {
        error_log(mysql_error());
        echo json_encode(array("status" => "ko", "message" => mysql_error()));
      }
      else
      {
        mysql_query("UPDATE `cape_utilisateurs_categorie` 
                      SET `id_categorie_temp`= 5,`score`= 0 
                      WHERE  `id_ressource` = 27");


        mysql_query("UPDATE `cape_utilisateurs_capsules_parametres` SET notification_mail = 0, afficher_noms_reels = 0 WHERE `id_capsule` = 51");

        echo json_encode(array("status" => "ok", "message" => "Tous les messages de cette ressource ont été définitivement supprimés.")); 
      }
    }
    else{
        error_log('Erreur suppression messages : ' . print_r($errors, true));
        echo json_encode(array("status" => "ko", "message" => "Une erreur est survenue pendant la suppression des messages. L'équipe PairForm en a été alerté."));
    }
  }
  else {
    error_log(mysql_error());
    echo json_encode(array("status" => "ko", "message" => mysql_error()));
  }
  if (count($warnings)) {
    error_log("Warnings : ".print_r($warnings,true));
  }
?>