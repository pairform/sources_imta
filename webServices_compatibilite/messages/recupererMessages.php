<?php
//error_log('//********************************************//');
//error_log('//*********** recupererMessages.php **********//');
//error_log('//********************************************//');
//Recuperation des valeurs des messages
/*
 * Paramètres :
   * nom_page (string) - nom de la page concernée.
   * id_ressource (int) - id de la ressource concernée.
   * nom_tag ( string ) - nom  du tag ( PAGE ou autre ).
   * num_occurence (int) - le numero de l'occurence de l'objet pedagogique.
   * langues_affichage (string) - ids des langues à afficher
   * filtre_ressource (array)(int) - tableau json d'id de ressource que possède l'utilisateur sur son iOS / Android (?)

     "messages":[
      {
          "id_message":117,
          "owner_guid":"35",
          "owner_username":"Admin",
          "owner_avatar_medium":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1362507644&joindate=1359553679&guid=35&size=medium",
          "owner_rank_id":null,
          "owner_rank_name":null,
          "value":"Test sur l'actualité au 15 janvier",
          "geo_lattitude":"",
          "geo_longitude":"",
          "supprime_par":"0", ( id de la personne qui l'a supprimé)
          "utilite":0,
          "tags":{ superTag
            },
          "tags_auteurs":[ superTag => bast],  
          "est_defi": 0 ( pas defi ) 1 ( defi ) 2 ( defi terminé" ,
          "defi_valide" 0 ( pas validé )  1 ( validé )
          "parent" : 59
          "medaille":"or" / "argent" / "bronze",
          "id_ressource" : SQL['id_ressource'],
          "nom_page" : SQL['nom_page'],
          "nom_tag" : SQL['nom_tag'],
          "num_occurence" : SQL['num_occurence'],
          "defi_valide" : 0 / 1,
          "user_a_vote" : 0 -> normal , 1 positif , -1 negatif,
          "time_created" : ( timestamp),
          "time_updated" : ( timestamp),
          "langue" : 3
      }
 */

include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
include_once("../accomplissement/gagnerSucces.php");
$os      = $_POST['os'];
$version = $_POST['version'];
$langues_affichage = isset($_POST['langues_affichage']) ? $_POST['langues_affichage'] : "1,2,3";


//Impossible d'utiliser cette fonction sans être connecté
// if (!elgg_get_logged_in_user_guid())
// {
    //Creation d'une session php par Elggme
    //$user   = get_user_by_username("sc_bot");
    //$result = login($user, true);
// }
    

//Si on a un utilisateur loggé 
$user = elgg_get_logged_in_user_entity();
// error_log(print_r($_POST,true));
if ($os == 'web') {
    if (isset($_POST['id_ressource'])) {
      $id_ressource = mysql_real_escape_string($_POST['id_ressource']);
      $_POST['filtre_ressource'] = array($id_ressource);
    }
}
else{
    // Si l'utilisateur n'a pas encore cette ressource dans sa liste de ressource.
    $filtre_ressource = json_decode($_POST['filtre_ressource']) ? json_decode($_POST['filtre_ressource']) : $_POST['filtre_ressource'];
    
}
    // error_log("1 - Parsed : " . print_r($filtre_ressource, true) ." - Row : ". $_POST['filtre_ressource']);
if (!empty($user)) {
  $id_utilisateur = $user->getGUID();
  //On va checker qu'il a bien un niveau sur la ressource
  if ($os == 'web') {
      $result = mysql_query("SELECT * 
                FROM `cape_utilisateurs_capsules_parametres` 
                WHERE `id_utilisateur` = $id_utilisateur
                AND `id_capsule` = (SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_ressource)");
      if ($result && !mysql_fetch_assoc($result)){
          $result = mysql_query("INSERT IGNORE INTO `cape_utilisateurs_categorie` (`id_ressource` , `id_utilisateur`, `id_categorie`,  `score`) 
            VALUES ((SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_ressource), $id_utilisateur, 0, 0)");

          $result2 = mysql_query("INSERT INTO `cape_utilisateurs_capsules_parametres` VALUES ($id_ressource, $id_utilisateur, 1, 0)");
          gagnerSuccesRessource();
      }
  }
  else
  {
    if (!empty($filtre_ressource)) {
      foreach ($filtre_ressource as $id_ressource) {
          $id_utilisateur = mysql_real_escape_string($id_utilisateur);
          $result = mysql_query("SELECT * 
                FROM `cape_utilisateurs_capsules_parametres` 
                WHERE `id_utilisateur` = $id_utilisateur
                AND `id_capsule` = (SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_ressource)");
          if ($result && !mysql_fetch_assoc($result)){
              $result = mysql_query("INSERT IGNORE INTO `cape_utilisateurs_categorie` (`id_ressource` , `id_utilisateur`, `id_categorie`,  `score`) 
                VALUES ((SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_ressource), $id_utilisateur, 0, 0)");
          $result2 = mysql_query("INSERT INTO `cape_utilisateurs_capsules_parametres` VALUES ($id_ressource, $id_utilisateur, 1, 0)");
              gagnerSuccesRessource();
          }
      }
    }  
  }
}

 

// N'affiche pas les reponse au niveau 1
// function getMessage($row_message_sc, $affiche_reponse = false)
function getMessage($row_message_sc, $array_capsules, $os, $version)
{

    //On chope les variables réutilisées souvent, pour éviter les accès au tableau et pour la lisibilité
    $message_id_ressource  = $row_message_sc['id_ressource'];
    $message_nom_page      = $row_message_sc['nom_page'];
    $message_nom_tag       = $row_message_sc['nom_tag'];
    $message_num_occurence = $row_message_sc['num_occurence'];
    //Options passées 
    $options               = array(
        'guid' => $row_message_sc['id_message'],
        'type' => 'object'
    );                                                    
    $messages_elgg         = elgg_get_entities($options);
    // error_log('$messages_elgg : '.print_r($messages_elgg,true));
    $message_elgg          = $messages_elgg[0];
    // error_log('$message_elgg : '.print_r($message_elgg,true));
    // Recupere l'entity objet 
    $message_entity        = get_entity($row_message_sc['id_message']);
    // error_log('$message_entity : '.print_r($message_entity,true));
    // Si l'objet est inconnu on arrete.
    if ($messages_elgg == null || $message_entity == null)
        return null;
    // Ne pas afficher si nous n'avons pas l'acces.
    $user = elgg_get_logged_in_user_entity();
    // error_log('User : '.print_r($user,true));
    // Si l'utilisateur est l'admin, on affiche qd même
    if (!has_access_to_entity($message_entity, $user) && ($user->guid != 35))
        return null;

    // Ne pas afficher si c'est une reponse d'un defi et que nous ne somme pas l'auteur-expert.


    
    $arrayParent    = $message_entity->getAnnotations("parent");
    if (!empty($arrayParent))
    {
        $parentGuid = $arrayParent[0]->__get("value");
        $parentEntity = get_entity($parentGuid);
        
        if ( !empty($parentEntity)){

            // si le parent est un defi , et que je suis pas l'auteur de ce defi
            $arrayDefi = $parentEntity->getAnnotations("est_defi");
            if (!empty($arrayDefi) && ($parentEntity->getOwnerEntity() != $user)){
                // et que le defi n'est pas fini
                if ( $arrayDefi[0]->__get("value") == 1)    return null;
            }
        }

        
    }
      
    if ( $user ){
         $user_guid = $user->getGUID();
    }

    //On store ces valeurs, parce que les crochets passent mal dans la formation de la requête SQL
    $id_message_temp = $message_elgg->guid;
    $id_owner        = $message_elgg->owner_guid;
    $date            = $message_elgg->time_created;
    $time_updated=(($message_elgg->time_updated)>($message_elgg->time_updated_meta))?($message_elgg->time_updated):("$message_elgg->time_updated_meta");
    //$time_updated    = $message_elgg->time_updated;
    $user            = elgg_get_entities(array(
        "type" => "user",
        "guid" => $id_owner
    ));
    if ( empty($user[0])  )  return null;
    //error_log('IDOwner : '. $id_owner .' : '.print_r($user,true));
    $iconURL         = $user[0]->getIconURL("medium");
    // donne l'utilité
    $likes           = elgg_get_annotations(array(
        'guid' => $id_message_temp,
        'annotation_name' => 'likes'
    ));
    $dislikes        = elgg_get_annotations(array(
        'guid' => $id_message_temp,
        'annotation_name' => 'dislikes'
    ));
    $utile           = count($likes) - count($dislikes);

    $user_a_vote     = 0;
    //Renvoie 1 pour vote positif
    foreach ($likes as $key => $like)
    {
        //error_log("Like guid : " . $like['owner_guid']);
        if ($like['owner_guid'] == $user_guid)
            $user_a_vote = 1;
    }
    //Renvoie -1 pour vote négatif
    foreach ($dislikes as $key => $dislike)
    {
        //error_log("dislike guid : " . $dislike['owner_guid']);
        if ($dislike['owner_guid'] == $user_guid)
            $user_a_vote = -1;
    }
    
    //Tag
    $tags            = $message_entity->getTags();
    if (empty($tags))
     {   $tags = "";}
    else{
        $tagsHash  = (array)json_decode($message_entity->tagsHash);
        foreach ($tags as $value) {
            if ( $tagsHash[$value] ){
                $user_tag =get_entity($tagsHash[$value]);
                $name = $user_tag->__get('name');
                $tags_auteurs[] = $name;
            }else{
                $tags_auteurs[] = null;
            }
            
        }
    }

    
    
    if (empty($tagsHash))
        $tagsHash = "";

    // Defi
    $est_defi = $message_entity->getAnnotations("est_defi");
    if ($est_defi)  
        $est_defi = $est_defi[0]->__get("value");
    else 
        $est_defi = 0;

    // Defi valide
    $array_defi_valide = $message_entity->getAnnotations("defi_valide");
    if (!empty($array_defi_valide)) 
        $defi_valide = true;
    else 
        $defi_valide = false;

    // Medaille
    $arrayMedaille         = $message_entity->getAnnotations("medaille");
    if (!empty($arrayMedaille))
    {
        $medaille = $arrayMedaille[0]->__get("value");
    }
    
    // parent d'une reponse
    $arrayParent         = $message_entity->getAnnotations("parent");
    if (!empty($arrayParent))
    {
        $parent = $arrayParent[0]->__get("value");
    }
    
    if (($os == 'web') && ($version == '1.5')) {
      $result_messages_enrichis = mysql_query("SELECT `uid_page`, `uid_oa`, `cape_utilisateurs_categorie`.`id_categorie`, `cape_categories`.`nom`
  FROM `cape_messages`,  `cape_utilisateurs_categorie`, `cape_categories` 
  WHERE `cape_messages`.`id_message` = $id_message_temp 
  AND `cape_utilisateurs_categorie`.`id_ressource` = (SELECT id_ressource FROM cape_capsules WHERE id_capsule=$message_id_ressource)
  AND `cape_utilisateurs_categorie`.`id_utilisateur` = $id_owner
  AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie`");  
    }
    else{
      $result_messages_enrichis = mysql_query("SELECT `geo_lattitude`, `geo_longitude`, `supprime_par`, `cape_utilisateurs_categorie`.`id_categorie`, `cape_categories`.`nom`
  FROM `cape_messages`,  `cape_utilisateurs_categorie`, `cape_categories` 
  WHERE `cape_messages`.`id_message` = $id_message_temp 
  AND `cape_utilisateurs_categorie`.`id_ressource` = (SELECT id_ressource FROM cape_capsules WHERE id_capsule=$message_id_ressource)
  AND `cape_utilisateurs_categorie`.`id_utilisateur` = $id_owner
  AND `cape_categories`.`id_categorie` = `cape_utilisateurs_categorie`.`id_categorie`");
    }
    if ($result_messages_enrichis)
    {

        $row_message_enrichis   = mysql_fetch_assoc($result_messages_enrichis);

        
        if($array_capsules){
          if (array_key_exists($message_id_ressource, $array_capsules))
            $display_name = $user[0]->name;
          else
            $display_name = !empty($user[0]->username) ? $user[0]->username : $user[0]->name;
        }
        else
          $display_name = !empty($user[0]->username) ? $user[0]->username : $user[0]->name;

        if (($os == 'web') && ($version == '1.5'))
        {
          //On forme le message        
          $array_message = array(
              "id_message" => "$id_message_temp",
              "owner_guid" => $id_owner,
              "owner_username" => $display_name,
              "owner_avatar_medium" => $iconURL,
              "owner_rank_id" => $row_message_enrichis['id_categorie'],
              "owner_rank_name" => $row_message_enrichis['nom'],
              "value" => $message_elgg->description,
              "geo_lattitude" => $row_message_sc['geo_lattitude'],
              "geo_longitude" => $row_message_sc['geo_longitude'],
              "supprime_par" => $row_message_sc['supprime_par'],
              "utilite" => "$utile",
              "tags" => $message_entity->getTags(),
              "tags_auteurs" => $tags_auteurs,
              "est_defi" => $est_defi,
              "defi_valide" => $defi_valide,
              "parent" => "$parent",
              "medaille" => $medaille,
              "id_ressource" => $row_message_sc['id_ressource'],
              "uid_oa" => $row_message_enrichis['uid_oa'],
              "uid_page" => $row_message_enrichis['uid_page'],
              "time_created" => $date,
              "time_updated" => $time_updated,
              "user_a_vote" => "$user_a_vote",
              "langue" => $row_message_sc['langue']
          );

        } else if ($os == 'and') {
          //On forme le message        
          $array_message = array(
              "id_message" => "$id_message_temp",
              "id_ressource" => $row_message_sc['id_ressource'],
              "id_utilisateur" => $id_owner,
              "username" => $user[0]->username,
              "name" => $user[0]->name,
              "image" => $iconURL,
              "owner_rank_id" => $row_message_enrichis['id_categorie'],
              "rank" => $row_message_enrichis['nom'],
              "value" => $message_elgg->description,
              "geo_lattitude" => $row_message_sc['geo_lattitude'],
              "geo_longitude" => $row_message_sc['geo_longitude'],
              "supprime_par" => $row_message_sc['supprime_par'],
              "utilite" => "$utile",
              "tags" => $message_entity->getTags(),
              "tags_auteurs" => $tags_auteurs,
              "est_defi" => $est_defi,
              "defi_valide" => $defi_valide,
              "parent" => "$parent",
              "medaille" => $medaille,
              "nom_page" => $row_message_sc['nom_page'],
              "nom_tag" => $row_message_sc['nom_tag'],
              "num_occurence" => $row_message_sc['num_occurence'],
              "time_created" => $date,
              "time_updated" => $time_updated,
              "user_a_vote" => "$user_a_vote",
              "langue" => $row_message_sc['langue']
          );
        
        } else {
          //On forme le message        
          $array_message = array(
              "id_message" => "$id_message_temp",
              "owner_guid" => $id_owner,
              "owner_username" => $display_name,
              "owner_avatar_medium" => $iconURL,
              "owner_rank_id" => $row_message_enrichis['id_categorie'],
              "owner_rank_name" => $row_message_enrichis['nom'],
              "value" => $message_elgg->description,
              "geo_lattitude" => $row_message_sc['geo_lattitude'],
              "geo_longitude" => $row_message_sc['geo_longitude'],
              "supprime_par" => $row_message_sc['supprime_par'],
              "utilite" => "$utile",
              "tags" => $message_entity->getTags(),
              "tags_auteurs" => $tags_auteurs,
              "est_defi" => $est_defi,
              "defi_valide" => $defi_valide,
              "parent" => "$parent",
              "medaille" => $medaille,
              "id_ressource" => $row_message_sc['id_ressource'],
              "nom_page" => $row_message_sc['nom_page'],
              "nom_tag" => $row_message_sc['nom_tag'],
              "num_occurence" => $row_message_sc['num_occurence'],
              "time_created" => $date,
              "time_updated" => $time_updated,
              "user_a_vote" => "$user_a_vote",
              "langue" => $row_message_sc['langue']
          );
        }

    }
    else 
        error_log(mysql_error());

    $arrayFinal    = $array_message;
    return $arrayFinal;
}


$granularite = ''; //globale, ressource ou page

if (($os == 'web') && ($version == '1.5')) {

  if (isset($_POST['id_ressource']) && isset($_POST['uid_page']))
  {
      $id_ressource    = mysql_real_escape_string($_POST['id_ressource']);
      $uid_page        = mysql_real_escape_string($_POST['uid_page']);
      $granularite     = 'page';
      $result_messages = mysql_query("SELECT * FROM `cape_messages` 
    WHERE `id_ressource` = $id_ressource AND `uid_page` = '$uid_page' 
    ORDER BY `cape_messages`.`uid_page` ASC,
    `cape_messages`.`uid_oa` ASC ");

      if (!$result_messages)
          error_log(mysql_error());
  }
  else if (isset($_POST['id_ressource']) && !isset($_POST['uid_page']))
  {
      $id_ressource    = mysql_real_escape_string($_POST['id_ressource']);
      $granularite     = 'ressource';
      $result_messages = mysql_query("SELECT * FROM `cape_messages` 
    WHERE `id_ressource` = $id_ressource 
    ORDER BY `cape_messages`.`uid_page` ASC,
    `cape_messages`.`uid_oa` ASC");

      if (!$result_messages)
          error_log(mysql_error());
  }
}
else
{
  //En fonction de ce que l'on envoie au webservice, on va faire une sélection plus fine des messages
  if (isset($_POST['id_ressource']) && isset($_POST['nom_page']) && isset($_POST['nom_tag']) && isset($_POST['num_occurence']))
  {
      $id_ressource    = mysql_real_escape_string($_POST['id_ressource']);
      $nom_page        = mysql_real_escape_string($_POST['nom_page']);
      $message_nom_tag    = mysql_real_escape_string($_POST['nom_tag']);
      $message_num_occurence        = mysql_real_escape_string($_POST['num_occurence']);
      $granularite     = 'message';
      $result_messages = mysql_query("SELECT * FROM `cape_messages` 
    WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' AND `nom_tag` = '$message_nom_tag' AND `num_occurence` = $message_num_occurence AND `langue` IN ($langues_affichage) 
    ORDER BY `id_ressource` ASC,
    `nom_page` ASC,
    `nom_tag` ASC,
    `num_occurence` ASC ");
      if (!$result_messages)
          error_log(mysql_error());
  }
  else if (isset($_POST['id_ressource']) && isset($_POST['nom_page']))
  {
      $id_ressource    = mysql_real_escape_string($_POST['id_ressource']);
      $nom_page        = mysql_real_escape_string($_POST['nom_page']);
      $granularite     = 'page';
      $result_messages = mysql_query("SELECT * FROM `cape_messages` 
    WHERE `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' AND `cape_messages`.`langue` IN ($langues_affichage) 
    ORDER BY `cape_messages`.`nom_tag` ASC,
    `cape_messages`.`num_occurence` ASC ");
      if (!$result_messages)
          error_log(mysql_error());
  }
  else if (isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
  {
      $id_ressource    = mysql_real_escape_string($_POST['id_ressource']);
      $granularite     = 'ressource';
      $result_messages = mysql_query("SELECT * FROM `cape_messages` 
    WHERE `id_ressource` = $id_ressource AND `cape_messages`.`langue` IN ($langues_affichage) 
    ORDER BY `cape_messages`.`id_ressource` ASC,
    `cape_messages`.`nom_page` ASC,
    `cape_messages`.`nom_tag` ASC,
    `cape_messages`.`num_occurence` ASC ");
      if (!$result_messages)
          error_log(mysql_error());
  }
  else if (!isset($_POST['id_ressource']) && !isset($_POST['nom_page']) && !isset($_POST['vue_trans']) && !isset($_POST['id_message']))
  {
      $granularite     = 'globale';
      $query = "SELECT * FROM `cape_messages` WHERE `cape_messages`.`langue` IN ($langues_affichage)";


      if (isset($_POST['filtre_ressource'])) {
       // Filtre plusieurs ressources : On ne prend pas les messages qui ne sont pas dans la liste
        $filtre_ressource = json_decode($_POST['filtre_ressource']);
        //Si le tableau de filtre n'est pas vide
        if (!empty($filtre_ressource)) {
          // $filtre_timestamp = json_encode($_POST['filtre_timestamp']);
          $filtre_timestamp = json_decode($_POST['filtre_timestamp']);
          //Si le tableau de filtre n'est pas vide (ce qui serait VRAIMENT idéal)
          if (!empty($filtre_timestamp)) {

            $query = "SELECT * FROM `cape_messages`, `sce_entities` WHERE `cape_messages`.`id_message` = `sce_entities`.`guid` AND `cape_messages`.`langue` IN ($langues_affichage)";
            //On construit le where
            $where_clause = " AND (";

            //Pour chaque ressource dans la liste
            foreach ($filtre_timestamp as $id_res => $timestamp) {
              //On rajoute l'id dans la condition
              $where_clause .= "(`cape_messages`.`id_ressource` = $id_res AND `sce_entities`.`time_updated` > $timestamp) OR ";
            }
            //On vire le dernier " OR "
            $where_clause = substr($where_clause, 0, -4);
            $where_clause.=")";
            //On append ça à la query
            $query .= $where_clause;
          }
          //Sinon, old school
          else{
            //On construit le where
            $where_clause = " AND `id_ressource` IN (";
            //Pour chaque ressource dans la liste
            foreach ($filtre_ressource as $index => $id_res) {
              //On rajoute l'id dans le IN
              $where_clause .= mysql_real_escape_string($id_res) . ',';
            }
            //On vire la dernière ","
            $where_clause = substr($where_clause, 0, -1);
            $where_clause.=")";
            //On append ça à la query
            $query .= $where_clause;
            
          }
        }
        //S'il est vide, par contre,
        else{
          //On ne veut rien retourner
          $return = json_encode(array('messages' => array()));
          //On die hard
          die($return);
        }
      } 
      //Si on a même pas de filtre de ressource (ne devrait jamais arriver)
      else{
        //On ne veut rien retourner
        $return = json_encode(array('messages' => array()));
        //On die encore plus hard
        die($return);
      }
      //On met le reste de la query. L'ordre des instructions SQL est important, WHERE doit être avant ORDER BY.
      $query .= " ORDER BY `cape_messages`.`id_ressource` ASC,
    `cape_messages`.`nom_page` ASC,
    `cape_messages`.`nom_tag` ASC,
    `cape_messages`.`num_occurence` ASC ";
      

      $result_messages = mysql_query($query);
      if (!$result_messages)
          error_log(mysql_error());
  }
  else if (isset($_POST['id_message']))
  {
      $id_message = mysql_real_escape_string($_POST['id_message']);
      $granularite     = 'message';
      $result_messages = mysql_query("SELECT * FROM `cape_messages` 
          WHERE `id_message` = $id_message AND `cape_messages`.`langue` IN ($langues_affichage)");
      if (!$result_messages)
          error_log(mysql_error());
  }else{
      $granularite     = 'transversale';
      $result_messages = mysql_query("SELECT  `id_message` , `id_ressource` , `nom_page` , `nom_tag` , `num_occurence` , `geo_lattitude` , `geo_longitude` , `supprime_par`   
    FROM `cape_messages` , `sce_entities`
    WHERE  `cape_messages`.`id_message` = `sce_entities`.`guid` AND `cape_messages`.`langue` IN ($langues_affichage) 
    ORDER BY `sce_entities`.`time_created` DESC   ");
      if (!$result_messages)
          error_log(mysql_error());
  }
}
// error_log("Granularite : " . $granularite);

function userWantsRealNameForCapsules ($id_utilisateur = null)
{
  //Si pas d'utilisateur specifié
  if ($id_utilisateur === null) {
    $id_utilisateur = elgg_get_logged_in_user_guid();
    
    //Si ya pas d'utilisateur connecté
    if (!$id_utilisateur) {
      //On quitte, ça sert à rien
      return false;
    }
  }

  $query = mysql_query("SELECT `id_capsule` FROM `cape_utilisateurs_capsules_parametres` WHERE `afficher_noms_reels` = 1 AND `id_utilisateur` = $id_utilisateur");

  while ($row = mysql_fetch_assoc($query)) {
    $array_capsules[$row['id_capsule']] = true;
  }

  //Retour d'un tableau avec tous les ids de capsules dont l'utilisateur veut voir les prénoms des utilisateurs.
  return $array_capsules;
}

//Si l'utilisateur actuel souhaite utiliser les vrais noms 
$array_capsules = userWantsRealNameForCapsules();

//Pour chaque message
while ($row_message_sc = mysql_fetch_assoc($result_messages))
{
    $message = getMessage($row_message_sc, $array_capsules, $os, $version);
    // error_log('Apres traitement :'.print_r($message,true));
    if ($message)
    {
        $arrayFinal[] = $message;
    }
}
if (!isset($arrayFinal)) {
  $arrayFinal = array();
}
// error_log("Messages Envoyé");
//error_log('Array_grains : ' . print_r($array_grains, true));
$return = json_encode(array('messages' => $arrayFinal));
//error_log('Retour JSON : ' . $return);
//Retour JSON
print($return);

?>