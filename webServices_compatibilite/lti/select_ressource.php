<!DOCTYPE html>
<html>
<head>
<title>Selection de la ressource PairForm</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<link rel="stylesheet" href="//www.pairform.fr/SCWeb/dyn/res/css/supcast.css">
<style type="text/css">
	body{
		background: url("//www.pairform.fr/img/bg.png") 50% 0% no-repeat rgb(50,50,50);
		font-family: 'Raleway';
		height: 100%;
		margin: 0%;
	}

	h1, h2{
		font-weight:100;
	}
	@font-face {
	  font-family: 'Raleway';
	  font-style: normal;
	  font-weight: 100;
	  src: local('Raleway Thin'), local('Raleway-Thin'), url('//www.pairform.fr/SCWeb/dyn/res/fonts/Raleway Thin.woff') format('woff');
	}
	@font-face {
	  font-family: 'Raleway';
	  font-style: normal;
	  font-weight: 200;
	  src: local('Raleway ExtraLight'), local('Raleway-ExtraLight'), url('//www.pairform.fr/SCWeb/dyn/res/fonts/Raleway ExtraLight.woff') format('woff');
	}
	.popin.formBox {
		position: absolute;
		left: 0;
		right: 0;
		margin-left: auto;
		margin-right: auto;
		width: 300px;
		margin-top: 20px;
	}
	pre{
		color: white;
	}
	h1, h2 {
		margin: 0 0 10px 0;
	}
	/*@media screen and (max-width: 480px) {
		h1{
			font-size: 2.5em;
		}
	}*/
</style>


</head>
<body>
<script>
	$(function () {
		var request_pending = false;

		$('#form_ressource').submit(function () {
			var new_url = $('input[name=url]').val();

			if (new_url.match(/^(http):\/\//))
				new_url = new_url.replace("http://", "https://");
			if (!new_url.match(/^(https?):\/\//))
				new_url = "https://" + new_url;
			

			$('input[name=url]').val("https://podcast.mines-nantes.fr/SCElgg/elgg-1.8.13/webServices/lti/lti_wrapper.php?url=" + new_url);
		});
	});
</script>
	<div class="popin formBox pf">
		<section id="loginBox">
			<h1>Entrez le lien à inserer :</h1>
			<form action="<?php echo $_POST['launch_presentation_return_url']; ?>" method="get" id="form_ressource" enctype="application/x-www-form-urlencoded">
				<section>
					<label for="url">Url de la ressource : </label><input type="text" name="url" value="" />
				</section>
				<section>
					<label for="text">Titre du lien : </label><input type="text" name="text" value="" />
				</section>
				<input type="hidden" name="return_type" value="lti_launch_url"/>
				<input class="btn-action" type="submit" value="Intégrer"/>
			</form>
		</section>
	</div> 

</body>
</html>