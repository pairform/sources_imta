<?php

  //********************************************//
  //*********** creerCercle.php ***********//
  //********************************************//
  
  /* Creer un cercle.
   *
   * Paramètres :
   *
   * nom (string) nom du cercle.
   * 
   * Retour : 
   * {"status":"ok"
   * “message” : “message d’erreur “ // uniquement si status est “ko”
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerSucces.php");
  $os = $_POST['os'];
  $version = $_POST['version'];

  $nom = $_POST['nom'];

  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
  }
  
  // Nom identiques
  $collections = get_user_access_collections ($id_utilisateur);
  foreach ($collections as &$collection) {
      if ($collection->name ==  $nom) {
        $return = json_encode(array('status' => "ko" ,'message' => "Ce nom est déjà utilisé."));
        echo $return;
        exit();

      };
  }


  if ($id_collection = create_access_collection  ( $nom, $id_utilisateur )){
    $return = json_encode(array('status' => "ok", 'id_collection' => $id_collection));
    gagnerSuccesCercle();
  }
  else
  {
    $return = json_encode(array('status' => "ko" ,'message' => "La création a échoué"));
  }
  echo $return;
  

?>