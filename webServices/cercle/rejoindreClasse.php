<?php

  //********************************************//
  //*********** rejoindreClasse.php ***********//
  //********************************************//
  
  /* rejoindre une classe
   *
   * Paramètres :
   *
   * cle (string) cle de la classe.
   * 
   * Retour : 
   * {"status":"ok"
   * “message” : “message d’erreur “ // uniquement si status est “ko”
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../push/api/api.php");
  $os = $_POST['os'];
  $version = $_POST['version'];

  $cle = mysql_real_escape_string($_POST['cle']);
  
  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
    $utilisateur_connecte = get_user($id_utilisateur);

  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
    $utilisateur_connecte = elgg_get_logged_in_user_entity();
  }

  $result = mysql_query("SELECT `cape_classes`.`id_classe`,`sce_access_collections`.`name` 
    FROM `cape_classes`,`sce_access_collections` 
    WHERE `cle` = '$cle' 
    AND id_classe = id ");
  $row_message = mysql_fetch_assoc($result);
  
   if($row_message){
      
      $id_classe = $row_message['id_classe'];
      $name = $row_message['name'];
      if ( get_access_collection($id_classe)->owner_guid == $id_utilisateur){
        $return = json_encode(array('status' => "ko" ,'message' => "Vous être le propriétaire de cette classe."));
      } else {
        if (add_user_to_access_collection ($id_utilisateur, $id_classe)) {

          $result = mysql_query("INSERT INTO cape_utilisateurs_classes VALUES ($id_utilisateur, $id_classe)");
          if ($result) {
            $api->sendMessage(get_access_collection($id_classe)->owner_guid, "un", ucfirst($utilisateur_connecte->username)." a rejoint votre classe", array("c" => (string)$id_utilisateur, "su" => "cr"));
            //On l'ajoute dans la table cape_utilisateurs_classes
            if( $os == "and" || (isset($_POST['id_utilisateur'])) || ($os == "ios" && $version >= "1.0.300")) {
              $return = json_encode(array('status' => "ok"));
            } else {
              $return = json_encode(array('status' => "ok" ,'message' => "Clé acceptée."));
            }
          } else {
            error_log("######Erreur - Utilisateur #".$id_utilisateur. " rejoinds une classe : ".mysql_error());
            if( $os == "and" || (isset($_POST['id_utilisateur'])) || ($os == "ios" && $version >= "1.0.300")) {
              $return = json_encode(array('status' => "ko",'message' => "erreur_serveur"));
            } else {
              $return = json_encode(array('status' => "ko",'message' => "Erreur du serveur : veuillez réessayer."));
            }
          }
        } else {
          if( $os == "and" || (isset($_POST['id_utilisateur'])) || ($os == "ios" && $version >= "1.0.300")) {
            $return = json_encode(array('status' => "ko",'message' => "utilisateur_deja_membre"));
          } else {
            $return = json_encode(array('status' => "ko",'message' => "Vous êtes déjà dans cette classe."));
          }
        }
      }
      
    } else{
      if( $os == "and" || (isset($_POST['id_utilisateur'])) || ($os == "ios" && $version >= "1.0.300")) {
        $return = json_encode(array('status' => "ko",'message' => "cle_invalide"));
      } else {
        $return =  json_encode(array('status' => "ko",'message' => "Clé invalide."));
      }
    }
  echo $return;
  


?>