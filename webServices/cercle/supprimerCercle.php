<?php

   //********************************************//
  //*********** supprimerCercle.php ***********//
  //********************************************//
  
  /* supprimer un cercle ou une classe
   *
   * Paramètres :
   *
   * id_collection (string) id du cercle/classe.
   * 
   * Retour : 
   * {"status":"ok"
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  $os = $_POST['os'];
  $version = $_POST['version'];

  $id_collection = mysql_real_escape_string($_POST['id_collection']);
  error_log(print_r($_POST, true));
  if(delete_access_collection($id_collection)){
    $return = json_encode(array('status' => "ok"));
    mysql_query("DELETE FROM `SCElgg`.`cape_classes` WHERE `cape_classes`.`id_classe` = $id_collection");
  }
  else
  {
    $return = json_encode(array('status' => "ko", 'message' => "Erreur de suppression de cercle. Veuillez réessayer."));
  }

  echo $return;
  
?>