<?php

  //********************************************//
  //*********** ajouterMembre.php ***********//
  //********************************************//
  
  /* affiche la liste des membre d'un cercle.
   *
   * Paramètres :
   *
   * id_utilisateur (int) - l'id de l'utilisateur
   * id_collection (int) - l'id de la collection
   * 
   * Retour : 
   * {
   * "status":"ko"
   * 'message' : 'erreur'
   * }
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../push/api/api.php");
  $os = $_POST['os'];
  $version = $_POST['version'];

  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
    $utilisateur_connecte = get_user($id_utilisateur);
    $id_utilisateur_ajoute = $_POST['id_utilisateur_concerne'];
  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
    $utilisateur_connecte = elgg_get_logged_in_user_entity();
    $id_utilisateur_ajoute = $_POST['id_utilisateur'];
  }

  $id_collection= $_POST['id_collection'];

  //Vérification anti-doublon
  $members = get_members_of_access_collection($id_collection);
  //Si l'utilisateur est déjà dans la collection
  if (!empty($members)) {
    foreach ($members as &$member) 
    {
      if($member->guid == $id_utilisateur_ajoute)
      {
        $return = json_encode(array('status' => "ko", 'message' => "web_label_erreur_ajout_reseau_deja_existant"));
      }
    }
  }
  if (empty($return)) {
    if (add_user_to_access_collection ($id_utilisateur_ajoute, $id_collection)){
      $api->sendMessage($id_utilisateur_ajoute, "un", ucfirst($utilisateur_connecte->username)." vous a ajouté dans son réseau", array("c" => (string)$id_utilisateur, "su" => "ar"));
      $return = json_encode(array('status' => "ok"));
    }
    else
    {
      $return = json_encode(array('status' => "ko", 'message' => "web_label_erreur_ajout_reseau"));
    }
    
  }
  echo $return;

?>