<?php 
// Load up the Basic LTI Support code
require_once 'ims-blti/blti.php';

include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

// Initialize, all secrets are 'secret', do not set session, and do not redirect
$context = new BLTI("PairForm", false, false);

//error_log(print_r($_REQUEST, true));

$url_to_follow = isset($_REQUEST["url"]) ? $_REQUEST["url"] : $_REQUEST["custom_url"] ;


if ( $context->valid  && $url_to_follow != "") {
	$user_email = $context->getUserEmail();
	$users_with_that_email = get_user_by_email($user_email);

	if (!empty($users_with_that_email)) {
		logout();
		$user = $users_with_that_email[0];

		$code = (md5($user->name . $user->username . time() . rand()));
		elgg_set_ignore_access(true);
		$user->code = md5($code);
		if ($user->save() || elgg_trigger_event('login', 'user', $user)) {
			elgg_set_ignore_access(false);
			$guid = $user->guid;
			$token = $code;
			header("Location:".$url_to_follow."?token=".$token);
		}
	}
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Authentification PairForm</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<link rel="stylesheet" href="//www.pairform.fr/SCWeb/dyn/res/css/supcast.css">
<style type="text/css">
	body{
		background: url("//www.pairform.fr/img/bg.png") 50% 0% no-repeat rgb(50,50,50);
		font-family: 'Raleway';
		height: 100%;
		margin: 0%;
	}

	h1, h2{
		font-weight:100;
	}
	@font-face {
	  font-family: 'Raleway';
	  font-style: normal;
	  font-weight: 100;
	  src: local('Raleway Thin'), local('Raleway-Thin'), url('//www.pairform.fr/SCWeb/dyn/res/fonts/Raleway Thin.woff') format('woff');
	}
	@font-face {
	  font-family: 'Raleway';
	  font-style: normal;
	  font-weight: 200;
	  src: local('Raleway ExtraLight'), local('Raleway-ExtraLight'), url('//www.pairform.fr/SCWeb/dyn/res/fonts/Raleway ExtraLight.woff') format('woff');
	}
	.popin.formBox {
		position: absolute;
		left: 0;
		right: 0;
		margin-left: auto;
		margin-right: auto;
		width: 420px;
		margin-top: 20px;
	}
	pre{
		color: white;
	}
	h1, h2 {
		margin: 0 0 10px 0;
	}
	#explication{
		line-height: 14px;
	}
	/*@media screen and (max-width: 480px) {
		h1{
			font-size: 2.5em;
		}
	}*/
</style>


</head>
<body>

<?php



if ( $context->valid  && $url_to_follow != "") {

echo <<<JS_SCRIPT
<script>
	$(function () {
		var request_pending = false;

		$('#form_confirm_login').submit(function () {

			if (request_pending) {
				toastr["warning"]("Veuillez patienter, l'enregistrement est en cours...");
				return false;
			}
		   	request_pending = true;

			toastr["info"]("Enregistrement en cours...");
			var postData = $(this).serialize()+"&os=web&version=LTI";

			$.post("https://podcast.mines-nantes.fr/PairForm_elgg/webServices/compte/registerCompte.php", postData, function(data){

		   		request_pending = false;
			   var retour = $.parseJSON(data);
			   if (retour['status'] == 'ko')
			   {
					if (retour['message'] == "empty") 
						toastr["error"]("Tous les champs obligatoires du formulaire n'ont pas &eacute;t&eacute; renseign&eacute;s.");

					else if (retour['message'] == "username") 
						toastr["error"]("Votre nom d'utilisateur est d&eacute;jà utilis&eacute;. Veuillez en choisir un autre.");

					else if (retour['message'] == "email")
						toastr["error"]("Votre adresse mail est d&eacute;jà utilis&eacute;e. Veuillez en saisir une autre.");
					else if (retour['message'] == "registerbad")
						toastr["error"]("Erreur d'enregistrement : veuillez contacter le CAPE de l'Ecole des Mines de Nantes.");
					else
						toastr["error"](retour['message']);
			   }
			   else
			   {
			   		var retour = $.parseJSON(data);
					var token = retour['token'];
			   		toastr["success"]("Enregistrement r&eacute;ussi, redirection vers la page en cours...");
			   		window.setTimeout(function(){
			   			window.location.replace("$url_to_follow?token="+token);
			   		}, 2000);
			   		
			   }
			});

			return false; // ne change pas de page
		});
	});
</script>
JS_SCRIPT;

	$user_image = $context->getUserImage();
	$real_name = $context->getUserName();
 	$user_name = str_replace(' ', '_', trim($context->getUserName()));
	$user_email = $context->getUserEmail();
	// $langue = isset($_REQUEST['launch_presentation_locale']) ? $_REQUEST['launch_presentation_locale'] : "LTI";
	$consumer_name = isset($_REQUEST['tool_consumer_info_product_family_code']) ? $_REQUEST['tool_consumer_info_product_family_code'] : "LTI";
	$user_role = isset($_REQUEST['roles']) ? $_REQUEST['roles'] : "defaut";
	$name = isset($_REQUEST['lis_person_name_family']) ? $_REQUEST['lis_person_name_family'] : "Nom";
	$surname = isset($_REQUEST['lis_person_name_given']) ? $_REQUEST['lis_person_name_given'] : "Prenom";
	$user_id = isset($_REQUEST['custom_canvas_user_id']) ? $_REQUEST['custom_canvas_user_id'] : -1;

echo <<<CONFIRM_LOGIN
			<div class="popin formBox pf">
				<section id="loginBox">
					<h1>Vous êtes connecté(e) en tant que :</h1>
					<form action="$url_to_follow" method="post" id="form_confirm_login">
						<section id="profil-info">
							<section id="profil-info-avatar" style="height: 128px;">
								<img src="$user_image" alt="Profile picture" width="128" height="128" style="border-radius:50%;"/>
							</section>
							<section id="profil-info-inner">
								<h2 id="profil-username">$real_name</h2>
								<h2 id="profil-etablissement">$user_email</h2>
							</section>	
						</section>
						<section>
							<div id="explication">Vous devez créer un compte PairForm pour accéder à ce document.</div>
							<label for="username">Choisissez un pseudo (choix définitif): </label><input type="text" name="username" value="$user_name" />
						</section>
						<input type="hidden" name="user_image" value="$user_image" />
						<input type="hidden" name="name" value="$name" />
						<input type="hidden" name="surname" value="$surname" />
						<input type="hidden" name="email" value="$user_email" />
						<input type="hidden" name="consumer_name" value="$consumer_name" />
						<input type="hidden" name="langue" value="3" />
						<input type="hidden" name="user_role" value="$user_role" />
						<input type="hidden" name="user_id" value="$user_id" />
						<input class="btn-action" type="submit" value="Accéder au document"/>
					</form>
				</section>
			</div> 
CONFIRM_LOGIN;

} else if($url_to_follow == "")
{
echo <<<NO_URL
			<div class="popin formBox">
				<section id="loginBox">
					<h1>Vous n'avez pas spécifié d'URL.</h1>
					<h2>Vérifiez la configuration de l'outil LTI PairForm.</h2>
				</section>
			</div> 
NO_URL;
}	else {

echo <<<DENY_LOGIN
		<section>
			<h1>Utilisation refusée</h1>
			<h2>Vous devez utiliser l'application LTI en l'intégrant directement dans un LMS.</h2>
		</section>
DENY_LOGIN;
 }
?>
	
</body>
</html>