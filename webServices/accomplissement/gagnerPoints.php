<?php


  //********************************************//
  //*********** gagnerPoints.php ***********//
  //********************************************//

  /* Fait gagner des points à l'utilisateur .
   *
   * Paramètres :
   * 
   * Est a utiliser en tant que library
   * 
   * On appelle les fonctions quand des points peuvent être gagnés à cet endroit.

  gagnerPointsEcrireMessage($entity) // Ecrire un message , $entity du message
  gagnerPointsEvaluerMessage($entity,$bPositif) // Evaluer un message , $entity du message , $bPositif vrai si vote positif
  gagnerPointsSupprimerCommentaire($entity) // Supprimer un message , $entity du message
  gagnerPointsActiverCommentaire($entity) // Activer un message , $entity du message
  gagnerPointsEvaluerMessagePassif($entity,$bPositif,$id_utilisateur) // Recevoir une evalutation de message , $entity du message , $bPositif vrai si vote positif , $id_utilisateur id de l’utilisateur.
  gagnerPointsReussirDefi($entity,$id_utilisateur) // Reussir un défi ,$entity du message ,$id_utilisateur id de l’utilisateur.
   *
   */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("gagnerSucces.php");
     include_once("../push/api/api.php");
  


  define("PTSPARTICIPANT", 30);
  define("PTSCOLLABORATEUR", 500);
  define("PTSANIMATEUR", 1200);


  //Actions simples , actives
  
  define("PTSECRIREMESSAGE", 10);
  define("PTSSUPPRIMERMESSAGE", -10);
  define("PTSEVALUERMESSAGE", 1);
  define("PTSGAGNERSUCCES", 10);

  //Actions omplexes , passives

  define("PTSREUSSIDEFI", 20);
  define("PTSVOTEPOSITIF", 1);
  define("PTSVOTENEGATIF", -1);
  define("PTSCOMMENTAIRESUPPRIME", -30);
  define("PTSCOMMENTAIREACTIVE", 30);

  //Actions simples , actives
  
  // define("PTSECRIREMESSAGE", 2);
  // define("PTSEVALUERMESSAGE", 1);
  // define("PTSGAGNERSUCCES", 15);

  // //Actions omplexes , passives

  // define("PTSREUSSIDEFI", 25);
  // define("PTSVOTEPOSITIF", 10);
  // define("PTSVOTENEGATIF", -10);
  // define("PTSCOMMENTAIRESUPPRIME", -50);



  //Actions simples


  function gagnerPointsEcrireMessage($entity){
    $id_capsule = getIdRessourceFromEntity($entity);
    $id_utilisateur = $entity->owner_guid;

    global $api;
    $api->sendMessage($id_utilisateur, "us", "Message posté", array("im" => $entity->guid, "c" => $id_capsule, "st" => "PTSECRIREMESSAGE"), false);
    gagnePoints($id_capsule,PTSECRIREMESSAGE, $id_utilisateur);
  }

  function gagnerPointsEvaluerMessage($entity,$bPositif, $id_utilisateur){
    $id_capsule = getIdRessourceFromEntity($entity);

    if ( $bPositif){
      $pts = PTSEVALUERMESSAGE;
    }else{
      $pts = -PTSEVALUERMESSAGE;
    }
    global $api;
    $api->sendMessage($id_utilisateur, "us", "Vote sur un message", array("im" => $entity->guid, "c" => $id_capsule, "st" => "PTSVOTEPOSITIF"), false);
    gagnePoints($id_capsule,$pts, $id_utilisateur);
  }

  // StandBy : ou mettre les points?
  /*function gagnerPointsGagnerSucces(){
      gagnePoints($id_capsule,ptsGagnerSucces);
    }*/




  //Actions complexes , passives

    function perdrePointsSupprimerCommentaire($entity){
      $id_capsule = getIdRessourceFromEntity($entity);
      $id_utilisateur = $entity->getOwnerGUID();
      // error_log('Valeur des constantes : '.PTSCOMMENTAIRESUPPRIME);
      global $api;
      $api->sendMessage($id_utilisateur, "us", "Message modéré", array("im" => $entity->guid, "c" => $id_capsule, "st" => "PTSCOMMENTAIRESUPPRIME"));
      gagnePoints($id_capsule,PTSCOMMENTAIRESUPPRIME,$id_utilisateur);
    }

    function perdrePointsMessageSupprime($entity){
      $id_capsule = getIdRessourceFromEntity($entity);
      $id_utilisateur = $entity->getOwnerGUID();
      // error_log('Valeur des constantes : '.PTSSUPPRIMERMESSAGE);

      global $api;
      $api->sendMessage($id_utilisateur, "us", "Message supprimé", array("im" => $entity->guid, "c" => $id_capsule, "st" => "PTSSUPPRIMERMESSAGE"));
      gagnePoints($id_capsule,PTSSUPPRIMERMESSAGE,$id_utilisateur);
    }

    function gagnerPointsActiverCommentaire($entity){
      $id_capsule = getIdRessourceFromEntity($entity);
      $id_utilisateur = $entity->getOwnerGUID();
      global $api;
      $api->sendMessage($id_utilisateur, "us", "Message réactivé", array("im" => $entity->guid, "c" => $id_capsule, "st" => "PTSCOMMENTAIREACTIVE"));
      gagnePoints($id_capsule,PTSCOMMENTAIREACTIVE,$id_utilisateur);
    }
    
    function gagnerPointsMessageActive($entity){
      $id_capsule = getIdRessourceFromEntity($entity);
      $id_utilisateur = $entity->getOwnerGUID();
      global $api;
      $api->sendMessage($id_utilisateur, "us", "Message revu", array("im" => $entity->guid, "c" => $id_capsule, "st" => "PTSECRIREMESSAGE"));
      gagnePoints($id_capsule,PTSECRIREMESSAGE,$id_utilisateur);
    }

    function gagnerPointsEvaluerMessagePassif($entity,$bPositif,$id_utilisateur){
      $id_capsule = getIdRessourceFromEntity($entity);
      if ( $bPositif){
        $pts = PTSVOTEPOSITIF;
        $score_type = "PTSVOTEPOSITIF";
      }else{
        $pts = PTSVOTENEGATIF;
        $score_type = "PTSVOTENEGATIF";
      }
      
      global $api;
      $api->sendMessage($id_utilisateur, "us", "Vote d'un utilisateur", array("im" => $entity->guid, "c" => $id_capsule, "st" => $score_type));
      gagnePoints($id_capsule,$pts,$id_utilisateur);
    }

    function gagnerPointsReussirDefi($entity,$id_utilisateur){
      $id_capsule = getIdRessourceFromEntity($entity);
      gagnePoints($id_capsule,PTSREUSSIDEFI,$id_utilisateur);

      global $api;
      $api->sendMessage($id_utilisateur, "us", "Vous avez gagné un défi!", array("im" => $entity->guid, "c" => $id_capsule, "st" => "PTSREUSSIDEFI"));
    }

    
    
  // functions "privates"  


    function getIdRessourceFromEntity($entity){
      $entityGuid = $entity->getGUID();

    //#####
    // DEBUT REQUETE getRessource
    //######
      $result = mysql_query("SELECT `id_capsule` 
        FROM `cape_messages` 
        WHERE `id_message`=$entityGuid");

      if(!$result)
        error_log(mysql_error());
      
      $row_message_sc = mysql_fetch_assoc($result);
      if ($row_message_sc == null) return null;
      $id_capsule = $row_message_sc['id_capsule'];

    //#####
    // FIN REQUETE  getRessource
    //######
      return $id_capsule;


      
    }

    function gagnePoints($id_capsule,$points,$id_utilisateur=null){

      if ( $id_capsule == null ) return;

    //#####
    // DEBUT REQUETE Ajout de points
    //######


    // Si l'utilisateur n'a pas encore de points dans la ressource on ajoute.
      $res = mysql_fetch_assoc(mysql_query("SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_capsule"));
      $id_ressource = $res['id_ressource'];

      $result = mysql_query("SELECT * 
        FROM `cape_utilisateurs_categorie` 
        WHERE `id_utilisateur` = $id_utilisateur
        AND `id_ressource` = $id_ressource");
      if (!mysql_fetch_assoc($result)){

        $result = mysql_query("INSERT INTO  `cape_utilisateurs_categorie` (  `id_ressource` , `id_utilisateur` , `id_categorie` ,  `score` ) 
          VALUES ($id_ressource, $id_utilisateur, 0, 0)");
      }

    // On ajoute les points
      // error_log("Points réels : $points. UPDATE `cape_utilisateurs_categorie` SET score = score+$points WHERE `id_utilisateur` = $id_utilisateur AND `id_ressource` = $id_ressource");

      $result = mysql_query("UPDATE `cape_utilisateurs_categorie`
        SET score = score+$points
        WHERE `id_utilisateur` = $id_utilisateur
        AND `id_ressource` = $id_ressource");

      if(!$result){
        error_log(mysql_error());
      }

    //#####
    // FIN REQUETE  Ajout de points
    //######

      gagnerSuccesPoints();
      gagnerSuccesRessource();

   //#####
    // DEBUT REQUETE recuperer points
    //######
      $result = mysql_query("SELECT *
        FROM `cape_utilisateurs_categorie`
        WHERE `id_utilisateur` = $id_utilisateur
        AND `id_ressource` = $id_ressource");

      if(!$result)
        error_log(mysql_error());
      
      while($row_message_sc = mysql_fetch_assoc($result))
      {
        $score = $row_message_sc['score'];
        $id_categorie = $row_message_sc['id_categorie'];
      }


    //#####
    // FIN REQUETE  recuperer points
    //######

      $old_id_caterogie = $id_categorie;
      if (($id_categorie == 0) && ($score >=PTSPARTICIPANT)  ) 
        $id_categorie = 1;
      else if (($id_categorie == 1) && ($score >=PTSCOLLABORATEUR)  ) 
        $id_categorie = 2;
      else if (($id_categorie == 2) && ($score >=PTSANIMATEUR)  ) 
        $id_categorie = 3;

    /*if ($id_categorie == 2 & $score < 30  ) $id_categorie = 1;
    if ($id_categorie == 3 & $score < 75  ) $id_categorie = 2;*/

    if ($old_id_caterogie != $id_categorie){
      //#####
      // DEBUT REQUETE changement de status
      //######


      $result = mysql_query("UPDATE `cape_utilisateurs_categorie`
        SET id_categorie = $id_categorie
        WHERE `id_utilisateur` = $id_utilisateur
        AND `id_ressource` = $id_ressource");

      if(!$result)
        error_log(mysql_error());
      

      gagnerSuccesParticipant();
      gagnerSuccesCollaborateur();
      gagnerSuccesAnimateur();

      //#####
      // FIN REQUETE  changement de status
      //######
    }

    // require_once('../ws/Pusher.php');

    // if(!$pusher)
    //   $pusher = new Pusher('0b0142b03d240425e90f', '2ae0897085fc6c512871', '55928');
    // $pusher->trigger('pairform', 'new_points', array('id_ressource' => $id_ressource, 'id_categorie' => $id_categorie, 'score' => $score, 'id_utilisateur' => $id_utilisateur));

  }
  ?>