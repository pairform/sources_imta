<?php
  //********************************************//
  //************* listeSucces **************//
  //********************************************//
  /*
   * Liste les succès d'un utilisateur, ou tous les succès si aucun id n'est passé.
   * Paramètres : 
   * id_utilisateur : (int) id_utilisateur
   *
   * Retour : 
   * {
   * 	'allSuccess':  (array) Tous les succès,
   *	'mode': (string) "all" || "user",
   *	'userSuccessCount' :  (int) decompte de succes gagnés,
   *	'allSuccessCount' :  (int) nombre total de succes
   * }
  */
	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
	//Flag de mode d'affichage
	$mode = "all";
	$id_langue = isset($_POST['id_langue']) ? $_POST['id_langue'] : 3;
	//Récupération de tous les succès
	$allSuccessResult = mysql_query("SELECT cs.id_succes, cs.place, csl.nom, csl.description, cs.nom_logo FROM cape_succes cs JOIN cape_succes_langues csl ON csl.id_succes = cs.id_succes WHERE csl.id_langue = $id_langue");
	if (!$allSuccessResult) {
		error_log(mysql_error());
	}
	$allSuccess = array();

	//Si on est dans le cas de l'affichage des succès d'un utilisateur
	if (isset($_POST['id_utilisateur']))) {
		$id_utilisateur = mysql_real_escape_string($_POST['id_utilisateur']);
		$mode = "user";
		//Récupération des succès de l'user
		$userSuccessResult = mysql_query("SELECT * FROM cape_utilisateurs_succes WHERE id_utilisateur = $id_utilisateur ORDER BY date_gagne DESC");
		if (!$userSuccessResult) {
			error_log(mysql_error());
		}
		$userSuccesses = array();
		
		while ($userSuccess = mysql_fetch_assoc($userSuccessResult))
		{
			$userSuccesses[$userSuccess['id_succes']] = $userSuccess['date_gagne'];
		}

		$userSuccessCount = count($userSuccesses);
		$userSuccessGained = array();
	}

	while ($success = mysql_fetch_assoc($allSuccessResult)) {
		//Si on affiche les succes d'un utilisateur
		if ($mode == "user") {			
			//Si l'utilisateur a remporté ce succès
			if ($userSuccesses[$success['id_succes']]) {
				//On ne change pas l'image,
				//On le store dans le tableau des succes acquis, qui se retrouvera au début
				array_push($userSuccessGained, $success);
			}
			//Sinon, on change l'image par défaut
			else{
				$success['image'] = 'default';
				//Et on le push normalement dans le tableau
				array_push($allSuccess, $success);
			}
		}
		else
		{
			array_push($allSuccess, $success);
		}

	}
	if ($mode == "user") {
		$return = array_merge($userSuccessGained, $allSuccess);
		$allSuccessCount = count($return);
		print(json_encode(array('allSuccess'=> $return,'mode'=>$mode, 'userSuccessCount' => $userSuccessCount, 'allSuccessCount' => $allSuccessCount)));
		// print(json_encode(array("status" => "ok", "datas" => array('allSuccess'=> $return,'mode'=>$mode, 'userSuccessCount' => $userSuccessCount, 'allSuccessCount' => $allSuccessCount))));
	}
	else
	{
		$allSuccessCount = count($allSuccess);
		print(json_encode(array('allSuccess'=> $allSuccess,'mode'=>$mode, 'allSuccessCount' => $allSuccessCount)));	
	}
	
?>