<?php

  //********************************************//
  //******** recupererNotifications.php ********//
  //********************************************//
  
  /*
  * Renvoie les notifications de l'utilisateur connecté
  *
  * Paramètre :
  * (optionnel) [web] id_capsule (int) : id de la capsule en cours, pour récupération du score
  *
  * Retour : 
  * un -> user notifications (notifications concernants l'utilisateur)
  *   id_notification (int) : Id de la notification
  *   id_message (int) : Id du message concerné par la notification - peut être null dans le cas d'une notif ne concernant pas un message en particulier
  *   id_utilisateur (int) : Id de l'utilisateur concerné par la notification - peut être null dans le cas d'une notif broadcastée
  *   titre (string) : Titre principal de la notification (non internationalisé)
  *   sous_type (string) : Sous type de notification 
  *     dc : défi créé 
  *       - Cible : broadcast de la notification (pas d'utilisateur cible)
  *       - Contenu associé : Nom court de la ressource concernée
  *       - Action : Renvoyer vers le message
  *     dt : défi terminé 
  *       - Cible : broadcast de la notification (pas d'utilisateur cible)
  *       - Contenu associé : Nom court de la ressource concernée
  *       - Action : Renvoyer vers le message
  *     dv : défi validé 
  *       - Cible : broadcast de la notification (pas d'utilisateur cible)
  *       - Contenu associé : Nom court de la ressource concernée
  *       - Action : Renvoyer vers le message
  *     ar : ajout réseau
  *       - Cible : Utilisateur ajouté au réseau de quelqu'un
  *       - Contenu associé : ID de l'utilisateur qui a fait l'action
  *       - Action : Renvoyer vers le profil utilisateur
  *     cr : classe rejointe
  *       - Cible : Utilisateur qui a créé la classe
  *       - Contenu associé : ID de l'utilisateur qui a fait l'action
  *       - Action : Renvoyer vers le profil utilisateur
  *     ru : réponse d'utilisateur
  *       - Cible : Auteur du post original
  *       - Contenu associé : Nom court de la ressource concernée
  *       - Action : Renvoyer vers le message
  *     gm : gagné médaille
  *       - Cible : Utilisateur ayant gagné la médaille
  *       - Contenu associé : Nom court de la ressource concernée
  *       - Action : Renvoyer vers le message
  *     nan : défaut (à gérer)
  *       - Cible : Utilisateur concerné
  *       - Contenu associé : Nom court de la ressource concernée
  *       - Action : Renvoyer vers le message
  *   contenu (string) : Dépend du sub-type 
  *   date_creation (int - timestamp) : Timestamp de création
  *   date_vue (int - timestamp) : Timestamp de vision (peut être null si pas encore vu)
  *   image (string) : Nom de l'image (sans extension), déterminé en fonction du sous type
  *   label (string) : Sous-titre, déterminé en fonction du sous type
  *
  * us -> user score (notifications concernants les points / succès gagnés par l'utilisateur)
  * 
  *   id_notification (int) : Id de la notification
  *   id_message (int) : Id du message concerné par la notification - peut être null dans le cas d'une notif ne concernant pas un message en particulier
  *   titre (string) : Titre principal de la notification (non internationalisé)
  *   label (string) : Nom court de la ressource concernée
  *   points (string) : Nombre de points (négatif ou positif), récupéré dans la base cape_definitions_points à partir de la constante
  *   contenu (string) : Dépend du sub-type 
  *   date_creation (int - timestamp) : Timestamp de création
  *   date_vue (int - timestamp) : Timestamp de vision (peut être null si pas encore vu)
  * 
  * score : (int) Score de l'utilisateur pour une capsule donnée
  */
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  if(isset($_POST['id_utilisateur']))
    $id_utilisateur = $_POST['id_utilisateur'];
  else
    $id_utilisateur = elgg_get_logged_in_user_guid();

  if ($id_utilisateur) {
    try {
      $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $dbh->exec("SET CHARACTER SET utf8");
          $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

    } catch (Exception $e) {
      error_log($e->getMessage());
    }
    
    $return = array("status" => "ok", "un" => array(), "us" => array());

    //Récuperation des notifications utilisateur
    //Note: il faut mettre sous_type dans le WHEN, et pas au niveau du CASE pour pouvoir factoriser en utilisant IN()
    $un_stmt = $dbh->prepare("SELECT id_notification, id_message, id_utilisateur, titre, sous_type, contenu, date_creation, date_vue, 
                                (CASE 
                                  WHEN `sous_type` = 'dc'
                                    THEN 'DefiActif'
                                  WHEN `sous_type` = 'dt'
                                    THEN 'DefiFini'
                                  WHEN `sous_type` = 'dv'
                                    THEN 'DefiValid'
                                  WHEN `sous_type` IN('ar','cr')
                                    THEN 'reseau_notif'
                                  WHEN `sous_type` = 'ru'
                                    THEN 'comment_unreaded_64'
                                  WHEN `sous_type` = 'gm'
                                    THEN CONCAT('medaille_', REPLACE(SUBSTRING(SUBSTRING_INDEX(`contenu`, ' - ', 3), LENGTH(SUBSTRING_INDEX(`contenu`, ' - ', 3 - 1)) + 1), ' - ', ''))
                                  END
                                  ) AS 'image',
                                (CASE 
                                  WHEN `sous_type` IN('dc', 'dt', 'dv', 'ru')
                                    THEN (SELECT `nom_court` FROM cape_capsules WHERE `id_capsule` = `contenu`)
                                  WHEN `sous_type` IN('ar','cr')
                                    THEN 'notification_clic_profil'
                                  WHEN `sous_type` = 'gm'
                                    THEN REPLACE(SUBSTRING(SUBSTRING_INDEX(`contenu`, '-', 3), LENGTH(SUBSTRING_INDEX(`contenu`, '-', 3 - 1)) + 1), ',', '')
                                  END
                                  ) AS 'label'
                              FROM `cape_notifications` 
                              WHERE (`id_utilisateur` = ? OR ISNULL(`id_utilisateur`)) AND type_notification = 'un' 
                              ORDER BY date_creation DESC
                              LIMIT 20");

    $un_stmt->execute(array($id_utilisateur));
    $return["un"] = $un_stmt->fetchAll(PDO::FETCH_ASSOC);

    //Récuperation des notifications score
    $us_stmt = $dbh->prepare("SELECT id_notification, id_message, titre, contenu, 
                               (CASE 
                                  WHEN !ISNULL(id_message)
                                    THEN (SELECT `nom_court` FROM cape_capsules WHERE `id_capsule` = `contenu`)
                                  ELSE 
                                    contenu
                                  END
                                  ) AS label, 
      (SELECT nombre_points FROM cape_definitions_points WHERE code_definition_point = type_points) AS points, `type_points`, date_creation, date_vue 
                              FROM `cape_notifications` 
                              WHERE `id_utilisateur` = ? AND type_notification = 'us' 
                              ORDER BY date_creation DESC
                              LIMIT 20");

    $us_stmt->execute(array($id_utilisateur));
    $return["us"] = $us_stmt->fetchAll(PDO::FETCH_ASSOC);

    //La version web fait du pulling pour récuperer les notifications : le score peut donc évoluer à chaque requête, d'où l'envoi du score
    $id_capsule = isset($_POST['id_capsule']) ? $_POST['id_capsule'] : null;

    if ($id_capsule) {
      //Récuperation du score total actuel
      $score_stmt = $dbh->prepare("SELECT cuc.score
                                FROM cape_utilisateurs_categorie AS cuc
                                  JOIN cape_capsules AS ccl 
                                    ON `cuc`.`id_ressource` = `ccl`.`id_ressource`
                                WHERE cuc.id_utilisateur = ? 
                                AND `ccl`.`id_capsule` = ?");

      $score_stmt->execute(array($id_utilisateur, $id_capsule));
      
      $return["score"] = $score_stmt->fetch(PDO::FETCH_COLUMN);

    }
    $count_us = $dbh->prepare("SELECT COUNT(id_notification)
                              FROM `cape_notifications` 
                              WHERE `id_utilisateur` = ? 
                                AND type_notification = 'us' 
                                AND ISNULL(date_vue)");

    $count_us->execute(array($id_utilisateur));

    $return["count_us"] = $count_us->fetch(PDO::FETCH_COLUMN);

    $count_un = $dbh->prepare("SELECT COUNT(id_notification)
                              FROM `cape_notifications` 
                              WHERE `id_utilisateur` = ? 
                                AND type_notification = 'un' 
                                AND ISNULL(date_vue)");

    $count_un->execute(array($id_utilisateur));

    $return["count_un"] = $count_un->fetch(PDO::FETCH_COLUMN);
  

    echo(json_encode($return));
  }
  else
    echo(json_encode(array("status" => "ko", "message" => "user_logged_out")));
      
?>