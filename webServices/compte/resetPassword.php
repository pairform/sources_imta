<?php

	//********************************************//
	//******** Structure de base de WS ***********//
	//********************************************//

	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

	$os = $_POST['os'];
	$version = $_POST['version'];
	$username = $_POST['username'];

	// allow email addresses
	if (strpos($username, '@') !== false && ($users = get_user_by_email($username))) {
		$username = $users[0]->username;
	}

	$user = get_user_by_username($username);
	if ($user) {
		if (send_new_password_request($user->guid)) {
			die(json_encode(array('status'=>'ok')));
		} else {
			die(json_encode(array('status'=>'ko','message'=>'Erreur système : veuillez réessayer.')));
		}
	} else {
		die(json_encode(array('status'=>'ko','message'=>'Cet utilisateur / e-mail n\'existe pas : veuillez réessayer.')));
	}

?>
