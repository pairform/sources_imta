<?php

  //********************************************//
  //***************** logOut *******************//
  //********************************************//
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");


  // Log out
  $result = logout();

  // Renvoi message
  if ($result) {
    die(json_encode(array("status" => 'ok')));
  } else {
    die(json_encode(array("status" => 'ko')));
  }



?>