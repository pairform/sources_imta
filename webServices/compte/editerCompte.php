<?php

  //********************************************//
  //************** editerCompte.php *************//
  //********************************************//

  /*
  *
  * Edite le compte
  *
  *  Paramètres :
  *
  * name (string) - nom et prenom de l'utilisateur
  * etablissement (string) - établissement de l'utilisateur
  * current_password (string) - ancien mot de passe
  * password (string) - nouveau mot de passe
  * password2 (string) - confirmation du nouveau mot de passe
  *
  * Ancien paramètre (actif sur d'ancienne version de PairForm mais à ne plus utiliser)
  * email (string) - email de l'utilisateur
  * 
  * Retour : 
  * {
  *  "status":"ko", "message" : "message d'erreur"
  * }
  *
  * 
  */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");


  $os = $_POST['os'];
  $version = $_POST['version'];
  
  error_log(print_r($_POST,true));
  
  if($os == "web" && $version == "2"){
    $id_utilisateur = $_POST['id_utilisateur'];
    $user = get_user($id_utilisateur);
  }
  else{
    $user = elgg_get_logged_in_user_entity();
    $id_utilisateur = $user->guid;
  }

  if (!$user ) {
    if($os == "web" && $version == "2")
      print(json_encode(array('status' => 'ko', 'message' => 'web_label_session_expiree')));
    else
      print(json_encode(array('status' => 'ko', 'message' => 'non_connect')));
     exit();
  }

elgg_set_ignore_access(true);
  //Gestion du changement de mot de passe
  if ((isset($_POST['current_password']) && $_POST['current_password'] != "")
   && (isset($_POST['password']) && $_POST['password'] != "")
   && (isset($_POST['password2']) && $_POST['password2'] != ""))
  {
      //Nouveau : décodage du mdp lors du transit entre l'app et le webservice en base64
      if (($os == "ios" && $version >= "1.0.300") || ($os == "and" && $version >= "1.0.003")) {
        $current_password = base64_decode($_POST['current_password']);
        $password = base64_decode($_POST['password']);
        $password2 = base64_decode($_POST['password2']);
      }
      else {
        $current_password = $_POST['current_password'];
        $password = $_POST['password'];
        $password2 = $_POST['password2'];
      }

       $credentials = array(
        'username' => $user->username,
        'password' => $current_password
      );

      try {
        pam_auth_userpass($credentials);
      } catch (LoginException $e) {
        if($os == "web" && $version == "2")
          print(json_encode(array('status' => 'ko', 'message' => 'label_erreur_saisie_mot_de_passe')));
        else
          print(json_encode(array('status' => 'ko', 'message' => 'current_password')));
        exit();

      }


      try {
        $result = validate_password($password);
      } catch (RegistrationException $e) {
        if($os == "web" && $version == "2")
          print(json_encode(array('status' => 'ko', 'message' => 'web_label_nouveau_mdp_invalide')));
        else
          print(json_encode(array('status' => 'ko', 'message' => 'password')));
        exit();
      }



      if ($result) {
        if ($password == $password2) {
          $user->salt = generate_random_cleartext_password(); // Reset the salt
          $user->password = generate_user_password($user, $password);
        } else {
          if($os == "web" && $version == "2")
            print(json_encode(array('status' => 'ko', 'message' => 'label_erreur_saisie_mot_de_passe_confirmation')));
          else
            print(json_encode(array('status' => 'ko', 'message' => 'password2')));
          exit();
        }
      } else {
        if($os == "web" && $version == "2")
          print(json_encode(array('status' => 'ko', 'message' => 'web_label_nouveau_mdp_invalide')));
        else
          print(json_encode(array('status' => 'ko', 'message' => 'password')));
        exit();
      }
  }

  // if (isset($_POST['email']) && !empty($_POST['email'])) {
  //  $email = $_POST['email'];
  //   if (!is_email_address($email)) {
  //     if($os == "web" && $version == "2")
  //       print(json_encode(array('status' => 'ko', 'message' => 'email')));
  //     else
  //       print(json_encode(array('status' => 'ko', 'message' => 'email')));
  //     exit();
  //   }

  //   if (strcmp($email, $user->email) != 0) {
  //     if (!get_user_by_email($email)) {
  //         $user->email = $email;
          
  //     } else {
  //       if($os == "web" && $version == "2")
  //         print(json_encode(array('status' => 'ko', 'message' => 'email')));
  //       else
  //         print(json_encode(array('status' => 'ko', 'message' => 'email')));
  //       exit();
  //     }
  //   } 
  // }

  if (isset($_POST['name']) && !empty($_POST['name'])) {
   $name = $_POST['name'];
    
    if (strcmp($name, $user->name) != 0) {
      $user->name = $name;
    } 
  }

  // Etablissement
  if (isset($_POST['etablissement']) && !empty($_POST['etablissement'])) {
    $etablissement = mysql_real_escape_string($_POST['etablissement']);
    $result = mysql_query("SELECT * FROM `cape_utilisateurs` WHERE `id_utilisateur` = $id_utilisateur");

    if(!$result){
      if($os == "web" && $version == "2")
            print(json_encode(array('status' => 'ko', 'message' => 'web_label_erreur_reesayer')));
          else
            print(json_encode(array('status' => 'ko', 'message' => 'erreur_reseau')));
          exit();
    }
    while($row_message_sc = mysql_fetch_assoc($result))
    {
      $etablissementBdd = $row_message_sc['etablissement'];
    }
    
    if (strcmp($etablissement, $etablissementBdd) != 0) {
      $result = mysql_query("UPDATE  `cape_utilisateurs` SET  `etablissement` =  '$etablissement' WHERE `id_utilisateur` = $id_utilisateur");

      if(!$result)
      {
        if($os == "web" && $version == "2")
            print(json_encode(array('status' => 'ko', 'message' => 'web_label_erreur_contacter_pairform')));
          else
            print(json_encode(array('status' => 'ko', 'message' => 'erreur_enregistrement')));
          exit();
      }
    }
  }

  // Save final
  if (!$user->save()){
    if($os == "web" && $version == "2")
      print(json_encode(array('status' => 'ko', 'message' => 'web_label_erreur_reesayer')));
    else
      print(json_encode(array('status' => 'ko', 'message' => 'erreur_reseau')));
    exit();
  }

elgg_set_ignore_access(false);
  print(json_encode(array('status' => 'ok')));

?>