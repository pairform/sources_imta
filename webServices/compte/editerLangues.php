<?php

  //********************************************//
  //*********** editerLangues.php ***********//
  //********************************************//
  
  /* Modifier les langues d'un utilisateur
   *
   * Paramètres :
   * langue_principale (string) - id de la langue principale
   * autres_langues (array)(int) - tableau des id des autres langues d'un utilisateur
   * id_utilisateur(int) - l'id de l'utilisateur 
   *
   * Retour : 
   * {"status":"ok"
   * }
   * 
   */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $os = $_POST['os'];
  $version = $_POST['version'];
  $id_utilisateur = isset($_POST['id_utilisateur']) ? $_POST['id_utilisateur'] : false;
  $langue_principale = isset($_POST['langue_principale']) ? $_POST['langue_principale'] : false;
  $autres_langues = isset($_POST['autres_langues']) ? $_POST['autres_langues'] : false;

  try {
      $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $dbh->exec("SET CHARACTER SET utf8");
          $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

    } catch (Exception $e) {
      error_log($e->getMessage());
  }

  if ($id_utilisateur && $langue_principale && $autres_langues) {
    $id_utilisateur = intval($id_utilisateur);
    $langue_principale = intval($langue_principale);
    $autres_langues = json_decode($autres_langues);

    // Suppression des entrées existantes
    $remove_langues_utilisateur = $dbh->prepare("DELETE FROM cape_utilisateurs_langues WHERE id_utilisateur = :id_utilisateur");
    $remove_langues_utilisateur->execute(array(":id_utilisateur" => $id_utilisateur));

    // Ajout de la langue principale de l'utilisateur
    $add_langue_principale = $dbh->prepare("INSERT INTO cape_utilisateurs_langues (id_utilisateur, id_langue, principale) VALUES (:id_utilisateur, :id_langue, 1)");
    $add_langue_principale->execute(array(":id_utilisateur" => $id_utilisateur, ":id_langue" => $langue_principale));

    // Ajout des autres langues de l'utilisateur
    foreach ($autres_langues as $id_langue) {
      $id_langue = intval($id_langue);
      $add_autre_langue = $dbh->prepare("INSERT INTO cape_utilisateurs_langues (id_utilisateur, id_langue, principale) VALUES (:id_utilisateur, :id_langue, 0)");
      $add_autre_langue->execute(array(":id_utilisateur" => $id_utilisateur, ":id_langue" => $id_langue));
    }

    if(!$add_langue_principale)
    {
      echo json_encode(array('statut' => 'ko', 'message' => 'Erreur de changement des langues de l\'utilisateur'));
      error_log('Erreur de changement des langues de l\'utilisateur');  
    }
    else {
      echo json_encode(array('status' => "ok"));
    }
  }
?>