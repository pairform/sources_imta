<?php

  //********************************************//
  //*********** RegisterCompte.php *************//
  //********************************************//
  
  /*
  *
  * Enregistre un compte
  *
  *  Paramètres :
  *
  * version (string) - numéro de la version ATTENTION : valeur "LTI" possible, dans le cas d'une utilisation par un LMS.
  * -> Création de compte avec mot de passe aléatoire, envoyé à l'utilisateur par e-mail.
  * username (string) - pseudo de l'utilisateur
  * name (string) - nom de l'utilisateur
  * surname (string) - prénom de l'utilisateur
  * etablissement (string) - établissement de l'utilisateur
  * password (string) - mot de passe
  * password2 (string) - confirmation du mot de passe
  * email (string) - email de l'utilisateur
  * langue (int) - langue de l'utilisateur
  * 
  * Retour : 
  * {
  *  "status":"ko", "message" : "message d'erreur"
  * }
  * Ou :
  * {
  *  "status":"ok", "id_utilisateur" : "id de l'utilisateur créé"
  * }
  *
  */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerSucces.php");
  error_reporting(E_ALL);
  $os = $_POST['os'];
  $version = $_POST['version'];
  
  //Elgg values
  $username = mysql_real_escape_string($_POST['username']);
  $email = mysql_real_escape_string($_POST['email']);

  $depositeur = isset($_POST['depositeur']) ? -1 : 0;

  //Cas spécial d'une utilisation par un LMS
  if ($version == "LTI") {
    //Création de compte avec mot de passe aléatoire, envoyé à l'utilisateur par e-mail a posteriori.
     $password = $password2 = substr(str_shuffle(MD5(microtime())), 0, 10);
  }

  else{
    //Nouveau : décodage du mdp lors du transit entre l'app et le webservice en base64
    if (($os == "ios" && $version >= "1.0.300") || ($os == "and" && $version >= "1.0.003")) {
      $password = base64_decode( mysql_real_escape_string($_POST['password']) );
      $password2 = base64_decode( mysql_real_escape_string($_POST['password2']) );
    }
    else {
      $password = mysql_real_escape_string($_POST['password']);
      $password2 = mysql_real_escape_string($_POST['password2']);
    }
  }

  //PairForm values
  $etablissement = isset($_POST['etablissement']) ? mysql_real_escape_string($_POST['etablissement']) : "";
  $langue = isset($_POST['langue']) ? intval(mysql_real_escape_string($_POST['langue'])) : 3;
  //Strtoupper enlevé : sur le web, tout était en majuscule = pas cool
  $name = isset($_POST['name']) ? mysql_real_escape_string($_POST['name']) : $username;
    
  //Tests avant la création de comtpe par Elgg
      
  if (trim($password) == "" || trim($password2) == "" || trim($email) == "" || trim($username) == "") {
    if($os == "web" && $version == "2")
      $return = json_encode(array('status' => 'ko', 'message' => 'label_erreur_saisie_champ_vide'));
    else
      $return = json_encode(array('status' => 'ko', 'message' => 'empty'));

    // error_log($return);
    die($return);
  }
      
  if (strcmp($password, $password2) != 0) {
    if($os == "web" && $version == "2")
      $return = json_encode(array('status' => 'ko', 'message' => 'label_erreur_saisie_mot_de_passe_confirmation'));
    else
      $return = json_encode(array('status' => 'ko', 'message' => 'password2'));
    // error_log($return);
    die($return);
  }

  

  //Check que ce n'est pas un mail jetable
  //Les adresses mail doivent être uniques
  if($usedEmail = get_user_by_email($email) || preg_match("/^\w+@(0815.ru0clickemail.com|0-mail.com|0wnd.net|0wnd.org|10minutemail.com|20minutemail.com|2prong.com|3d-painting.com|4warding.com|4warding.net|4warding.org|9ox.net|a-bc.net|ag.us.to|amilegit.com|anonbox.net|anonymbox.com|antichef.com|antichef.net|antispam.de|baxomale.ht.cx|beefmilk.com|binkmail.com|bio-muesli.net|bobmail.info|bodhi.lawlita.com|bofthew.com|brefmail.com|bsnow.net|bugmenot.com|bumpymail.com|casualdx.com|chogmail.com|cool.fr.nf|correo.blogos.net|cosmorph.com|courriel.fr.nf|courrieltemporaire.com|curryworld.de|cust.in|dacoolest.com|dandikmail.com|deadaddress.com|despam.it|despam.it|devnullmail.com|dfgh.net|digitalsanctuary.com|discardmail.com|discardmail.de|disposableaddress.com|disposeamail.com|disposemail.com|dispostable.com|dm.w3internet.co.ukexample.com|dodgeit.com|dodgit.com|dodgit.org|dontreg.com|dontsendmespam.de|dump-email.info|dumpyemail.com|e4ward.com|email60.com|emailias.com|emailias.com|emailinfive.com|emailmiser.com|emailtemporario.com.br|emailwarden.com|enterto.com|ephemail.net|explodemail.com|fakeinbox.com|fakeinformation.com|fansworldwide.de|fastacura.com|filzmail.com|fixmail.tk|fizmail.com|frapmail.com|garliclife.com|gelitik.in|get1mail.com|getonemail.com|getonemail.net|girlsundertheinfluence.com|gishpuppy.com|goemailgo.com|great-host.in|greensloth.com|greensloth.com|gsrv.co.uk|guerillamail.biz|guerillamail.com|guerillamail.net|guerillamail.org|guerrillamail.biz|guerrillamail.com|guerrillamail.de|guerrillamail.net|guerrillamail.org|guerrillamailblock.com|haltospam.com|hidzz.com|hotpop.com|ieatspam.eu|ieatspam.info|ihateyoualot.info|imails.info|inboxclean.com|inboxclean.org|incognitomail.com|incognitomail.net|ipoo.org|irish2me.com|jetable.com|jetable.fr.nf|jetable.net|jetable.org|jnxjn.com|junk1e.com|kasmail.com|kaspop.com|klzlk.com|kulturbetrieb.info|kurzepost.de|kurzepost.de|lifebyfood.com|link2mail.net|litedrop.com|lookugly.com|lopl.co.cc|lr78.com|maboard.com|mail.by|mail.mezimages.net|mail4trash.com|mailbidon.com|mailcatch.com|maileater.com|mailexpire.com|mailin8r.com|mailinator.com|mailinator.net|mailinator2.com|mailincubator.com|mailme.lv|mailmetrash.com|mailmoat.com|mailnator.com|mailnull.com|mailzilla.org|mbx.cc|mega.zik.dj|meltmail.com|mierdamail.com|mintemail.com|mjukglass.nu|mobi.web.id|moburl.com|moncourrier.fr.nf|monemail.fr.nf|monmail.fr.nf|mt2009.com|mx0.wwwnew.eu|mycleaninbox.net|myspamless.com|mytempemail.com|mytrashmail.com|netmails.net|neverbox.com|no-spam.ws|nobulk.com|noclickemail.com|nogmailspam.info|nomail.xl.cx|nomail2me.com|nospam.ze.tc|nospam4.us|nospamfor.us|nowmymail.com|objectmail.com|obobbo.com|odaymail.com|onewaymail.com|ordinaryamerican.net|owlpic.com|pookmail.com|privymail.de|proxymail.eu|punkass.com|putthisinyourspamdatabase.com|quickinbox.com|rcpt.at|recode.me|recursor.net|regbypass.comsafe-mail.net|safetymail.info|sandelf.de|saynotospams.com|selfdestructingmail.com|sendspamhere.com|sharklasers.com|shieldedmail.com|shiftmail.com|skeefmail.com|slopsbox.com|slushmail.com|smaakt.naar.gravel|smellfear.com|snakemail.com|sneakemail.com|sofort-mail.de|sogetthis.com|soodonims.com|spam.la|spamavert.com|spambob.net|spambob.org|spambog.com|spambog.de|spambog.ru|spambox.info|spambox.us|spamcannon.com|spamcannon.net|spamcero.com|spamcorptastic.com|spamcowboy.com|spamcowboy.net|spamcowboy.org|spamday.com|spamex.com|spamfree.eu|spamfree24.com|spamfree24.de|spamfree24.eu|spamfree24.info|spamfree24.net|spamfree24.org|spamgourmet.com|spamgourmet.net|spamgourmet.org|spamherelots.com|spamhereplease.com|spamhole.com|spamify.com|spaminator.de|spamkill.info|spaml.com|spaml.de|spammotel.com|spamobox.com|spamspot.com|spamthis.co.uk|spamthisplease.com|speed.1s.fr|suremail.info|tempalias.com|tempe-mail.com|tempemail.biz|tempemail.com|tempemail.net|tempinbox.co.uk|tempinbox.com|tempomail.fr|temporaryemail.net|temporaryinbox.com|tempymail.com|thankyou2010.com|thisisnotmyrealemail.com|throwawayemailaddress.com|tilien.com|tmailinator.com|tradermail.info|trash-amil.com|trash-mail.at|trash-mail.com|trash-mail.de|trash2009.com|trashmail.at|trashmail.com|trashmail.me|trashmail.net|trashmailer.com|trashymail.com|trashymail.net|trillianpro.com|tyldd.com|tyldd.com|uggsrock.com|wegwerfmail.de|wegwerfmail.net|wegwerfmail.org|wh4f.org|whyspam.me|willselfdestruct.com|winemaven.info|wronghead.com|wuzupmail.net|xoxy.net|yogamaven.com|yopmail.com|yopmail.fr|yopmail.net|yuurok.com|zippymail.info|zoemail.com)$/i", 
    $email))
  {
    if($os == "web" && $version == "2")
      $return = json_encode(array('status' => 'ko', 'message' => 'web_label_erreur_mail_utilise'));
    else
      $return = json_encode(array('status' => 'ko', 'message' => "email"));
    // error_log($return);
    die($return);
  }

  //On vérifie si le pseudo est disponible
  if($usedUserName = get_user_by_username($username))
  {
    if($os == "web" && $version == "2")
      $return = json_encode(array('status' => 'ko', 'message' => 'web_label_erreur_nom_utilise'));
    else
      $return = json_encode(array('status' => 'ko', 'message' => "username"));
    // error_log($return);
    die($return);
  }

  //Valeurs par défaut pour qu'elgg soit content
  $friend_guid = 0; 
  $invitecode = "";
  
  //Remettre ça pour utiliser le param d'ELGG : && elgg_get_config('allow_registration')
  //Elgg registration
  if (!$usedEmail && !$usedUserName ) {
    try {
      
      if ($version == "LTI") {
        elgg_unregister_plugin_hook_handler('register', 'user', 'uservalidationbyemail_disable_new_user');
        $id_utilisateur = register_user($username, $password, $name, $email, false, $friend_guid, $invitecode);
      }
      else
        $id_utilisateur = register_user($username, $password, $name, $email, false, $friend_guid, $invitecode);
      
      if ($id_utilisateur) {
        $new_user = get_entity($id_utilisateur);

        // allow plugins to respond to self registration
        // note: To catch all new users, even those created by an admin,
        // register for the create, user event instead.
        // only passing vars that aren't in ElggUser.
        $params = array(
          'user' => $new_user,
          'password' => $password,
          'friend_guid' => $friend_guid,
          'invitecode' => $invitecode
        );

        // @todo should registration be allowed no matter what the plugins return?
        if (!elgg_trigger_plugin_hook('register', 'user', $params, TRUE)) {
          $new_user->delete();
          // @todo this is a generic messages. We could have plugins
          // throw a RegistrationException, but that is very odd
          // for the plugin hooks system.
          
          if($os == "web" && $version == "2")
            $return = json_encode(array('status' => 'ko', 'message' => 'web_label_erreur_enregistrement'));
          else
            $return = json_encode(array('status' => 'ko', 'message' => 'registerbad'));
          
          die($return);
        }
        // elgg_clear_sticky_form('register');
        system_message(elgg_echo("registerok", array(elgg_get_site_entity()->name)));

        //Enregistrement des niveaux dans la base PairForm
        $result = mysql_query("INSERT INTO cape_utilisateurs (id_utilisateur, etablissement) VALUES ('$id_utilisateur','$etablissement')");

        if($result) {
          //WARNING : Ne pas oublier les ressources par défaut.
          //On ne créé plus tous les niveaux par ressources, mais on met quand même les deux par défauts.

          //OPI et C2i D2
          // $valuesOutput = "(4,".$id_utilisateur.",0), (9,".$id_utilisateur.",0)";

          //Création du niveau 
          /*if(isset($_POST['id_capsule']) && ($os == "web")) {
            $id_capsule = mysql_real_escape_string($_POST['id_capsule']);
            
            $result = mysql_query("INSERT INTO `cape_utilisateurs_categorie` (`id_ressource` , `id_utilisateur`, `id_categorie`,  `score`) 
            VALUES ((SELECT id_capsule FROM cape_capsules WHERE id_capsule=$id_capsule), $id_utilisateur, 0, 0)");
            $result2 = mysql_query("INSERT INTO `cape_utilisateurs_capsules_parametres` VALUES ($id_capsule, $id_utilisateur, 1, 0)");
            //On passe l'id en paramètre, car il n'est pas forcément encore connecté (voir definition fonction)
            gagnerSuccesRessource($id_utilisateur);
          }*/

          //Ajout de la langue
          if(isset($_POST['langue'])) {
            $id_langue = mysql_real_escape_string($_POST['langue']);
            
            $result = mysql_query("INSERT INTO `cape_utilisateurs_langues` (`id_utilisateur`, `id_langue`,  `principale`) 
            VALUES ($id_utilisateur, $id_langue, 1)");
          }
         
           //Cas spécial d'une utilisation par un LMS
          if ($version == "LTI") {
           elgg_register_plugin_hook_handler('register', 'user', 'uservalidationbyemail_disable_new_user');
            logout();
            //Activation du profil
            //Nom de la plateforme émettrice
            $consumer_name = ucfirst($_POST['consumer_name']);

            $access = access_get_show_hidden_status();
            access_show_hidden_entities(TRUE);
            $new_user->enable();
            $new_user->validated = true;

            //Si ya une image, on la balance
            $user_image = isset($_POST['user_image']) ? $_POST['user_image'] : "";
            error_log("user_image : $user_image");
            error_log("Match default image : ".preg_match("/(avatar-50\.png)/", $user_image));
            if ($user_image != "" && preg_match("/(avatar-50\.png)/", $user_image) == false) {
              setUserAvatarFromURL($new_user, $user_image);
            }
            $new_user->save();
            //Pas besoin de validation par e-mail, l'identité de la personne est déjà certaine, avec le nom de la plateforme d'accès (LMS)
            error_log("Validation : " . elgg_set_user_validation_status($id_utilisateur, true, $consumer_name) ? "Yeah" : "Fuck");
            // error_log(print_r($new_user, true));
            //Login de l'utilisateur (création du code elggperm)
            login($new_user, true);
            access_show_hidden_entities($access);

            //Enregistrement des paramètres de contexte
            $new_user->user_role = $_POST['user_role'];
            $new_user->user_id = $_POST['user_id'];

            //Envoi à l'utilisateur de son mot de passe par e-mail.
            $headers = "From: register@pairform.fr\r\n";
            $headers .= "Reply-To: contact@pairform.fr\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

            $subject = "PairForm - Inscription depuis $consumer_name";
            $body = '<html><body>';
            $body .= '<style>';
            $body .= "body {font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; background: #ffffff; /* Old browsers */ background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%); /* FF3.6+ */ background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5)); /* Chrome,Safari4+ */ background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Chrome10+,Safari5.1+ */ background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Opera 11.10+ */ background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* IE10+ */ background: linear-gradient(to bottom,  #ffffff 0%,#e5e5e5 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 ); /* IE6-9 */ } h1{font-size: 1.8em; } h1, h2, h3{font-weight: 100; clear: both; float: left; } #signature{text-align: center; width: 100%; } #bodyWrapper{padding: 0 20px; min-width: 700px; width: 80%; margin: 0 auto; } #logoPairForm {float: right; } #comContainer .comBox {padding: 20px; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3); margin-bottom: 20px; border-radius: 4px; z-index: 25; clear: both; } #comContainer .comBox.defi {background: rgb(249,198,103); background: -moz-linear-gradient(top, rgba(249,198,103,0.1) 0%, rgba(247,150,33,0.5) 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(249,198,103,0.5)), color-stop(100%,rgba(247,150,33,0.5))); background: -webkit-linear-gradient(top, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); background: -o-linear-gradient(top, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); background: -ms-linear-gradient(top, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); background: linear-gradient(to bottom, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 ); } #comContainer .comBox .comProfil {margin-right: 16px; display: inline-block; vertical-align: top; width:60px; height:60px; } #comContainer .comBox .comProfil .imgProfil {border-radius: 20%; border: 1px solid rgba(0, 0, 0, 0.2); overflow: hidden; width: 60px; height: 60px; cursor: pointer; } #comContainer .comBox .comContent {display: inline-block; width: 100%; } #comContainer .comBox .comContent .comTitle {border-bottom: 1px solid rgba(0, 0, 0, 0.1); box-shadow: 0 1px 0 rgba(0, 0, 0, 0.05); } #comContainer .comBox .comContent .comTitle .comUserName {font-weight: bold; font-size: 16px; color: #444; font-variant: small-caps; } #comContainer .comBox .comContent .comTitle .comUserRank {font-size: 12px; font-style: italic; color: grey; } #comContainer .comBox .comContent .comTitle .timeCreated {font-style: italic; font-size: 12px; color: #444; float: right; padding-top: 4px; } #comContainer .comBox .comContent .comText {min-height: 40px; margin: 10px 0; white-space: pre-line; color: rgb(49, 49, 49); } #comContainer .comBox .comContent .textEdit {width: 100%; min-height: 40px; margin-top: 10px; border: none; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3); border-radius: 4px; } #comContainer .comBox .comContent .actionSection {font-size: 12px; color: grey; text-align: right; } #comContainer .comBox .comContent .actionSection div {display: inline-block; border-right: 1px solid #AAA; padding: 0 5px; cursor: pointer; -webkit-transition: all ease-in-out 0.4s; -moz-transition: all ease-in-out 0.4s; transition: all ease-in-out 0.4s; } #comContainer .comBox .comContent .actionSection div:hover {color: white; background-color: grey; } #comContainer .comBox .comContent .actionSection div:last-child {border-right: 0px solid #FFF; } #comContainer .supprime {opacity: 0.4; } #comContainer .comBox.reponse, #comContainer #formComBar.reponse{margin-left: 50px; } #comContainer .comBox.reponse:before , #comContainer #formComBar.reponse:before{content: '\21b5'; position: absolute; font-size: 3em; color: rgb(236, 236, 236); text-shadow: -1px 1px 2px rgb(175, 175, 175); margin-left: -70px; display: block; -moz-transform: scaleX(-1); -o-transform: scaleX(-1); -webkit-transform: scaleX(-1); transform: scaleX(-1); filter: FlipH; } #comContainer .comBox .comMedaille {width: 32px; height: auto; position: absolute; right: 36px; } #comContainer .comBox .comDefiValide {width: 32px; height: auto; position: absolute; right: 36px; margin-top: 70px; } #comContainer .comBox .comTagsLabel {background: rgb(206, 206, 206); padding: 2px 8px; margin-right: 8px; border-radius: 5px; color: #666; }";
            $body .= '</style>';
            $to = $email;

            $user_body = "<section id='bodyWrapper'>      
                    <h1>Bonjour $name!</h1>
                    <a href='http://www.pairform.fr'><img id='logoPairForm' src='http://www.pairform.fr/img/PairFormFullFinal.png'></a>
              
                    <h2>Nous vous remercions d'avoir créé un compte sur PairForm depuis $consumer_name!</h2>
                    <br>
                    <h3>Vous pouvez dès à présent commenter n'importe quel paragraphe à l'intérieur des documents. <br>
                     Nous vous invitons également à visiter les autres documents présents sur PairForm!</h3>
                    <br>
                    <h2>Votre identifiant : <strong>$username</strong></h2>
                    <h2>Votre mot de passe (généré automatiquement) : <strong>$password</strong></h2>
                    <br>
                    <h3>Vous pouvez changer votre mot de passe à tout moment dans l'interface de PairForm Web, en cliquant sur votre nom, puis sur la roue dentée.</h3>
                    <h1 id='signature'>L'&eacute;quipe PairForm.</h1>
                </section>
              </body>
            </html>";

            mail($to, $subject, $body.$user_body, $headers);
            error_log("Mail envoyé à $to ($username)");

            $return = json_encode(array('status' => "ok", 'id_utilisateur' => $id_utilisateur, 'token' => $_SESSION['code']));
            
            die($return);
            
            
          }
          //error_log("JSON : " . $return);
          $return = json_encode(array('status' => "ok", 'id_utilisateur' => $id_utilisateur));
          
          die($return);
          
        }
        else 
          error_log('Erreur Enregistrement cape_utilisateurs : ' . mysql_error());

      } else {
        if($os == "web" && $version == "2")
          $return = json_encode(array('status' => 'ko', 'message' => 'web_label_erreur_enregistrement'));
        else
          $return = json_encode(array('status' => 'ko', 'message' => 'registerbad'));

        
        register_error(elgg_echo("registerbad"));
        die($return);
      }
    } catch (RegistrationException $r) {
      register_error($r->getMessage());
      
      $return = json_encode(array('status' => 'ko', 'message' => $r->getMessage()));
      
      die($return);
    }
  }

function setUserAvatarFromURL($owner, $url)
{
  $id_utilisateur = $owner->getGUID();
  $url = elgg_normalize_url($url);
  $img_data = file_get_contents($url);
  if (!$img_data) {
    // try curl
    if (function_exists('curl_init')) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $img_data = curl_exec($ch);
      curl_close($ch);
    }

    if (!$img_data) {
      register_error(elgg_echo('avatar:upload:fail'));
      return false;
    }
  }
    // if we have img data, save it
  if ($img_data) {
    $filehandler = new ElggFile();
    $filehandler->owner_guid = $owner->getGUID();
    $filehandler->setFilename("profile/" . $owner->guid . "master.jpg");
    $filehandler->open("write");
    if (!$filehandler->write($img_data)) {
      register_error(elgg_echo("avatar:upload:fail"));
      return false;
    }
    $filename = $filehandler->getFilenameOnFilestore();
    $filehandler->close();
    
    $icon_sizes = elgg_get_config('icon_sizes');

    // get the images and save their file handlers into an array
    // so we can do clean up if one fails.
    $files = array();
    foreach ($icon_sizes as $name => $size_info) {
      if ($upload) {
        $resized = get_resized_image_from_uploaded_file('avatar', $size_info['w'], $size_info['h'], $size_info['square'], $size_info['upscale']);
      } else {
        $resized = get_resized_image_from_existing_file(
            $filename,
            $size_info['w'],
            $size_info['h'],
            $size_info['square']
        );
      }

      if ($resized) {
        //@todo Make these actual entities.  See exts #348.
        $file = new ElggFile();
        $file->owner_guid = $id_utilisateur;
        $file->setFilename("profile/{$id_utilisateur}{$name}.jpg");
        $file->open('write');
        $file->write($resized);
        $file->close();
        $files[] = $file;
      } else {
        // cleanup on fail
        foreach ($files as $file) {
          $file->delete();
        }

        register_error(elgg_echo('avatar:resize:fail'));
        return false;
      }
    }

    // reset crop coordinates
    $owner->x1 = 0;
    $owner->x2 = 0;
    $owner->y1 = 0;
    $owner->y2 = 0;

    $owner->icontime = time();
    if (elgg_trigger_event('profileiconupdate', $owner->type, $owner)) {
      system_message(elgg_echo("avatar:upload:success"));

      $view = 'river/user/default/profileiconupdate';
      elgg_delete_river(array('subject_guid' => $owner->guid, 'view' => $view));
      add_to_river($view, 'update', $owner->guid, $owner->guid);
    }

    elgg_trigger_event('profileiconupdate', $owner->type, $owner);

    //Renvoi de la nouvelle url pour le web, pour qu'on puisse updater la valeur en local
      $avatar_url = $owner->getIconURL("tiny");
    return $avatar_url;
  }


  return false;
}
?>