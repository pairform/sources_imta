<?php

	//********************************************//
	//******** Structure de base de WS ***********//
	//********************************************//

	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

	$user_guid = $_GET['u'];
	$code = $_GET['c'];

	if (execute_new_password_request($user_guid, $code)) {
		system_message(elgg_echo('user:password:success'));
	} else {
		register_error(elgg_echo('user:password:fail'));
	}

	forward('http://www.pairform.fr/reset.html');
?>
