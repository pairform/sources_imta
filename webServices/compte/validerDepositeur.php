<?php

  //********************************************//
  //********** validerDepositeur.php ***********//
  //********************************************//


  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $id_utilisateur = isset($_GET['id_utilisateur']) ? $_GET['id_utilisateur'] : false;
  $validation = isset($_GET['validation']) ? $_GET['validation'] : 0;

  try {
      $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $dbh->exec("SET CHARACTER SET utf8");
          $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

    } catch (Exception $e) {
      error_log($e->getMessage());
  } 

  if ($id_utilisateur) {
  	$update_user = $dbh->prepare("UPDATE cape_utilisateurs SET depositeur = :validation WHERE id_elgg = :id_utilisateur");
  	$update_user->execute(array(":validation" => $validation, ":id_utilisateur" => $id_utilisateur));

  	if(!$update_user)
  	{
  		echo json_encode(array('statut' => 'ko', 'message' => 'Erreur de changement de statut dépositeur pour l\'user ' .$id_utilisateur));
  		error_log('Erreur de changement de statut dépositeur pour l\'user ' .$id_utilisateur);	
  	}
  	//En cas de réussite
  	else
  	{
  		$query_user = $dbh->prepare('SELECT `email`,`username` FROM sce_users_entity WHERE `guid` = :id_utilisateur');
      $query_user->execute(array(":id_utilisateur" => $id_utilisateur));

  		$row = $query_user->fetch();

  		//On envoie un mail
      $to = $row['email'];

      $headers = "From: register@pairform.com\r\n";
      $headers .= "Reply-To: contact@pairform.com\r\n";
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
      
      $message = '<html><head>';
      $message .= '<style>';
      $message .= "body {font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; background: #ffffff; /* Old browsers */ background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%); /* FF3.6+ */ background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5)); /* Chrome,Safari4+ */ background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Chrome10+,Safari5.1+ */ background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Opera 11.10+ */ background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* IE10+ */ background: linear-gradient(to bottom,  #ffffff 0%,#e5e5e5 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 ); /* IE6-9 */ } h1{font-size: 1.8em; } h1, h2, h3{font-weight: 100; clear: both; float: left; } #signature{text-align: center; width: 100%; } #bodyWrapper{padding: 0 20px; min-width: 700px; width: 80%; margin: 0 auto; } #logoPairForm {float: right; } #comContainer .comBox {padding: 20px; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3); margin-bottom: 20px; border-radius: 4px; z-index: 25; clear: both; } #comContainer .comBox.defi {background: rgb(249,198,103); background: -moz-linear-gradient(top, rgba(249,198,103,0.1) 0%, rgba(247,150,33,0.5) 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(249,198,103,0.5)), color-stop(100%,rgba(247,150,33,0.5))); background: -webkit-linear-gradient(top, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); background: -o-linear-gradient(top, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); background: -ms-linear-gradient(top, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); background: linear-gradient(to bottom, rgba(249,198,103,0.1) 0%,rgba(247,150,33,0.5) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 ); } #comContainer .comBox .comProfil {margin-right: 16px; display: inline-block; vertical-align: top; width:60px; height:60px; } #comContainer .comBox .comProfil .imgProfil {border-radius: 20%; border: 1px solid rgba(0, 0, 0, 0.2); overflow: hidden; width: 60px; height: 60px; cursor: pointer; } #comContainer .comBox .comContent {display: inline-block; width: 100%; } #comContainer .comBox .comContent .comTitle {border-bottom: 1px solid rgba(0, 0, 0, 0.1); box-shadow: 0 1px 0 rgba(0, 0, 0, 0.05); } #comContainer .comBox .comContent .comTitle .comUserName {font-weight: bold; font-size: 16px; color: #444; font-variant: small-caps; } #comContainer .comBox .comContent .comTitle .comUserRank {font-size: 12px; font-style: italic; color: grey; } #comContainer .comBox .comContent .comTitle .timeCreated {font-style: italic; font-size: 12px; color: #444; float: right; padding-top: 4px; } #comContainer .comBox .comContent .comText {min-height: 40px; margin: 10px 0; white-space: pre-line; color: rgb(49, 49, 49); } #comContainer .comBox .comContent .textEdit {width: 100%; min-height: 40px; margin-top: 10px; border: none; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3); border-radius: 4px; } #comContainer .comBox .comContent .actionSection {font-size: 12px; color: grey; text-align: right; } #comContainer .comBox .comContent .actionSection div {display: inline-block; border-right: 1px solid #AAA; padding: 0 5px; cursor: pointer; -webkit-transition: all ease-in-out 0.4s; -moz-transition: all ease-in-out 0.4s; transition: all ease-in-out 0.4s; } #comContainer .comBox .comContent .actionSection div:hover {color: white; background-color: grey; } #comContainer .comBox .comContent .actionSection div:last-child {border-right: 0px solid #FFF; } #comContainer .supprime {opacity: 0.4; } #comContainer .comBox.reponse, #comContainer #formComBar.reponse{margin-left: 50px; } #comContainer .comBox.reponse:before , #comContainer #formComBar.reponse:before{content: '\21b5'; position: absolute; font-size: 3em; color: rgb(236, 236, 236); text-shadow: -1px 1px 2px rgb(175, 175, 175); margin-left: -70px; display: block; -moz-transform: scaleX(-1); -o-transform: scaleX(-1); -webkit-transform: scaleX(-1); transform: scaleX(-1); filter: FlipH; } #comContainer .comBox .comMedaille {width: 32px; height: auto; position: absolute; right: 36px; } #comContainer .comBox .comDefiValide {width: 32px; height: auto; position: absolute; right: 36px; margin-top: 70px; } #comContainer .comBox .comTagsLabel {background: rgb(206, 206, 206); padding: 2px 8px; margin-right: 8px; border-radius: 5px; color: #666; }";
      $message .= '</style>';
      $message .= '</head><body>';
      
      error_log("Validation : $validation. Test : ".($validation == 1));

      //Si l'utilisateur est validé
      if ($validation == 1) {
        $subject = '[PairForm] Demande depositeur validée pour '.$row['username'];

        $message .= '<h1>Validation de la demande de '.$row['username'].'</h1>';
        $message .= '<h2>Votre demande d\'autorisation de dépôt des ressources sur PairForm a été accordée. <br><h2>';
        $message .= '<span>Vous avez désormais accès au formulaire de soumission de ressources.</span><br><br>';
        $message .= '<span>Rendez vous sur cette adresse : http://www.pairform.fr/backoffice.php</span><br>';
        $message .= "<span>L'équipe PairForm</span>";
      }
      //Sinon
      else{
        $subject = '[PairForm] Demande depositeur refusée pour '.$row['username'];

        $message .= '<h1>Refus de la demande de '.$row['username'].'</h1>';
        $message .= '<h2>Votre demande d\'autorisation de dépôt des ressources sur PairForm a été déclinée.<br><h2>';
        $message .= '<span>Il est possible que : </span><br>';
        $message .= "<ul>";
        $message .= "<li>Votre profil ne contient pas suffisament d'information permettant de vous identifier : précisez vos informations (nom réel, établissement) en modifiant votre profil</li>";
        $message .= "<li>Votre adresse e-mail a été reconnue comme étant une adresse e-mail \"jetable\" : dans ce cas, contactez-nous</li>";
        $message .= "</ul>";
        $message .= "<span>Si vous pensez qu'il s'agit d'une erreur, vous pouvez nous contacter en <a href='mailto:contact@pairform.fr'>cliquant ici.</a></span><br>";
        $message .= "<span>Vous pouvez refaire votre demande en vous connectant à nouveau sur le back-office.</span><br><br>";
        $message .= "<span>L'équipe PairForm</span>";
      }

      $message .= '</body></html>';
      
      mail($to, $subject, $message, $headers);

  		echo json_encode(array('statut' => 'ok'));
  	}
  }
  else{
  	echo json_encode(array('statut' => 'ko', 'message' => 'Erreur de changement de statut dépositeur pour l\'user : pas d\'id.'));
	error_log('Erreur de changement de statut dépositeur pour l\'user : pas d\'id.');
  } 
 ?>