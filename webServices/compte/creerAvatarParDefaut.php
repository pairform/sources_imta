<?php

/**
 * Crée l'avatar par défaut avec Gravatar
 *
 * La fonction est appelé dans la fonction getIconURL() de la classe engine/classes/ElggEntity.php
 *
 * @param string $size taille de l'icon: tiny, small, medium, large
 * @param int $guid id de l'utilisateur
 *
 * @return string l'URL
 */
if (!function_exists('creerAvatarParDefaut')){
function creerAvatarParDefaut($size, $guid) {		
	$icon_sizes = elgg_get_config('icon_sizes');
	$size = $icon_sizes[$size]['w'];
	
	$md5 = md5( $guid );
	$url = 'http://www.gravatar.com/avatar/' .$md5. '.jpg?d=identicon&s=' . $size;
	return $url;
}

}

?>