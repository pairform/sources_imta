<?php

  //********************************************//
  //************* ajouterTags **************//
  //********************************************//
  /*
   * Ajoute des tags à un message.
   * Paramètres : 
   * tags : 
   * 	iOs et web : string séparé par des virgules
   * 	android : array
   * id_message : id du message à traiter
   *
   * Retour : 
   * {
   *   "status":"ok"
   *   "message" : "message" ( uniquement en cas d'erreur )
   * }
  */
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $os = $_POST['os'];
  $version = $_POST['version'];
  $id_message = mysql_real_escape_string($_POST['id_message']);
  
  if ($os == "and") {
  		$newTags = (array)json_decode($_POST['tags']);
  		
  		if (!is_array($newTags)) {
			echo json_encode(array('status' => 'ko', 'message' => 'tag_vide'));
			exit();
		}
  } else {
		$tags = $_POST['tags'];
		if ( empty($tags)){
			echo json_encode(array('status' => 'ko', 'message' => 'Champs vide'));
			exit();
		}
  }

  $message_entity = get_entity($id_message);
  
  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
  }  


  
  // Recupere les tags  
  if ($os == "and") {		
		$newTagsHash = array();
		foreach ($newTags as $tag) {	 	
			$newTagsHash[$tag] = $id_utilisateur;
		}
  } else {
		$newTags = explode(",", $tags);
		$newTagsHash = array();
		foreach ($newTags as $tag) {
			$newTagsHash[$tag] = $id_utilisateur;
		}
  }
  
  // TagsHash - liaison entre les tags et les auteurs.
  $oldTagsHash = (array)json_decode($message_entity->tagsHash);
  if ( empty($oldTagsHash) ) {
    $message_entity->tagsHash = json_encode($newTagsHash);
  } else {
    //if ( !is_array($oldTagsHash)) $oldTagsHash = array($oldTagsHash);
    $message_entity->tagsHash = json_encode(array_merge($oldTagsHash,$newTagsHash));
  }

   // ajoute les tags
   $oldTags = $message_entity->tags;
      
  if ( empty($oldTags) ){  	
     if (count($newTags) > 5) {
    	if ($os == "and") {
			$return = json_encode(array('status' => 'ko', 'message' => 'max_tags'));
		} else {
			$return = json_encode(array('status' => 'ko', 'message' => 'Limite de 5 tags dépassée'));
		}
        echo $return;
        exit();
     }
	
     $message_entity->tags = $newTags;
    
  }else{  	
    if ( !is_array($oldTags)) $oldTags = array($oldTags);
    if (count(array_merge($oldTags,$newTags)) > 5) {
       if ($os == "and") {
			$return = json_encode(array('status' => 'ko', 'message' => 'max_tags'));
		} else {
			$return = json_encode(array('status' => 'ko', 'message' => 'Limite de 5 tags dépassée'));
		}
      echo $return;
      exit();
    }
   
    $message_entity->tags = array_merge($oldTags,$newTags);
  }  
  $message_entity->time_updated_meta = time();

  $return = json_encode(array('status' => 'ok'));
  echo $return;


?>