<?php

  //********************************************//
  //*********** supprimerMessage.php ***********//
  //********************************************//
  
  /* Inscription du message comme supprimé dans la table cape_messages
   *
   * Paramètres :
   * id_message (int) - id du message à supprimer
   * id_user_op (int) - id de l'auteur du message
   * supprime_par (int) - id du modérateur ayant supprimé le message (defaut : 0 si non-supprimé)
   * supprime_def (int) - booléen - suppression définitive du message
   * id_user_moderator (int) - id du moderateur du message (triggerer de l'action)
   * id_ressource (int) - id de la ressource concernée
   *
   * Si version web ou iOs :
   * id_rank_user_op (int) - id de la categorie de l'auteur du message
   * id_rank_user_moderator (int) - id de la categorie du moderateur du message (triggerer de l'action)
   *
   * Retour : 
   * {
   *	"status":"ok"
   *	"message" : "message" ( uniquement en cas d'erreur )
   * }
   */
   
   
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerPoints.php");

  require_once('../ws/Pusher.php');

  if(!$pusher)
    $pusher = new Pusher('0b0142b03d240425e90f', '2ae0897085fc6c512871', '55928');
  
  
  //Connexion à la base de données
  try {
		$dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');	
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$dbh->exec("SET CHARACTER SET utf8");
  } catch (Exception $e) {
		error_log($e->getMessage());
  }
  
   //Override des autorisations propres à chaque message
  elgg_set_ignore_access(true);
  
  $os = $_POST['os'];
  $version = $_POST['version'];

  //Ids elgg
  $id_message = (int)mysql_real_escape_string($_POST['id_message']);
  $supprime_par = (int)mysql_real_escape_string($_POST['supprime_par']);
  $supprime_def = isset($_POST['supprime_def']) ? (int)mysql_real_escape_string($_POST['supprime_def']) : null;
  $id_user_op = (int)mysql_real_escape_string($_POST['id_user_op']);  
  $id_user_moderator = (int)mysql_real_escape_string($_POST['id_user_moderator']);
  $id_capsule = (int)mysql_real_escape_string($_POST['id_capsule']);
  
  error_log('***** Supp *******');
  
  //Switch : si suppression temporaire
  if ($supprime_def === null) {
    if ($os == "and") {
      $result = $dbh->query("SELECT id_categorie FROM cape_utilisateurs_categorie WHERE id_utilisateur='$id_user_op' AND id_ressource=(SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_capsule)");
      
      if($result) {
        $row = $result->fetch();
        $id_rank_user_op = (int)$row['id_categorie'];
      }
      else error_log(mysql_error());
      
      $result = $dbh->query("SELECT id_categorie FROM cape_utilisateurs_categorie WHERE id_utilisateur='$id_user_moderator' AND id_ressource=(SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_capsule)");
      
      if($result) {
        $row = $result->fetch();
        $id_rank_user_moderator = (int)$row['id_categorie'];
        
        error_log('id_rank_user_moderator :' . $id_rank_user_moderator);
      }
      else error_log(mysql_error());

    } else {
    	  $id_rank_user_op = (int)mysql_real_escape_string($_POST['id_rank_user_op']);
      $id_rank_user_moderator = (int)mysql_real_escape_string($_POST['id_rank_user_moderator']);
    }  


    //Trois cas de figure : 
            
    //Le message n'a pas encore été supprimé ;
    if($supprime_par == 0)
    {
    	
      //On check si le modo est l'op
      //OU que le rang du modo est supérieur ou égal à celui du posteur
      if(($id_user_moderator == $id_user_op) || ($id_rank_user_moderator >= $id_rank_user_op))
      {
        // error_log('Tentative de suppression');
        //On ajoute l'enregistrement à la table
        $result = $dbh->query("UPDATE cape_messages SET supprime_par ='$id_user_moderator' WHERE id_message='$id_message'");
            
        if($result)
        {
          $entity = get_entity($id_message);
          
          //On enlève 30 points que si c'est un autre utilisateur qui supprime le message
          if($id_user_moderator != $id_user_op)
            perdrePointsSupprimerCommentaire($entity);

          //On enlève les 10 points du message de toute façon.
          perdrePointsMessageSupprime($entity);
          $entity->time_updated_meta = time();
          $entity->save();
          $pusher->trigger('pairform', 'suppr_message', array('id_ressource' => $id_capsule, 'id_message' => $id_message, 'id_user_op' => $id_user_op, 'supprime_par' => $id_user_moderator));
          
        //Remise en place de l'override
        elgg_set_ignore_access(false);
          print( json_encode(array('status' => "ok")) );
          exit();
        }
        else error_log(mysql_error());
      }
      else 
        echo json_encode(array('status' => "ko","message" => "Vous n'avez pas l'autorisation de supprimer ce message."));
    }

    //Le message est déjà supprimé par un autre utilisateur de rang inférieur
    //Possibilité d'annuler la suppression
    else
    { 
      $result = $dbh->query("SELECT id_categorie FROM cape_utilisateurs_categorie WHERE id_utilisateur='$supprime_par' AND id_ressource=(SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_capsule)");
      
      if($result) {
        $row = $result->fetch();
        $id_rank_supprime_par = (int)$row['id_categorie'];
        
    	error_log('id_rank_supprime_par :' . $id_rank_supprime_par);
      }
      else error_log(mysql_error());

      //Si le rang du modo est supérieur à celui du modo précédent
      //OU que le modo est le propriétaire du post et celui qui a supprimé le message
      if(($id_rank_user_moderator >= $id_rank_supprime_par) || ($id_user_op == $supprime_par & $id_user_op == $id_user_moderator))
      {
        //On ajoute l'enregistrement à la table
        $result = $dbh->query("UPDATE cape_messages SET supprime_par = 0 WHERE id_message='$id_message'");
        
        if($result)
        {
          $entity = get_entity($id_message);
          $entity->time_updated_meta = time();
          $entity->save();
          //On récupère des points que si c'est un autre utilisateur qui avait supprime le message
          if($supprime_par != $id_user_op)
            gagnerPointsActiverCommentaire($entity);

          //On remet les 10 points du message 
          gagnerPointsMessageActive($entity);
          
          $pusher->trigger('pairform', 'suppr_message', array('id_ressource' => $id_capsule, 'id_message' => $id_message, 'id_user_op' => $id_user_op, 'supprime_par' => 0));
          
        //Remise en place de l'override
        elgg_set_ignore_access(false);
          print(  json_encode(array('status' => "ok")));
          exit();
        }
        else error_log(mysql_error());
      }

      //Le message est déjà supprimé par un autre utilisateur de rang supérieur
      else  {
        echo json_encode(array('status' => "ko","message" => "Vous n'avez pas l'autorisation de supprimer ce message."));
      }
    }
  }
  //Si c'est une suppression définitive
  else{
    //On fait gaffe
    //Vérification de son rang
    $id_rank_user_moderator = (int)mysql_real_escape_string($_POST['id_rank_user_moderator']);
    if (($id_rank_user_moderator >= 4) && ($os == "web")) {
      //On récupère le déposant de la ressource
      // $result = $dbh->query("SELECT ajoute_par FROM cape_capsules WHERE id_capsule = $id_capsule")->fetch();

      //Soit l'utilisateur est admin, soit c'est lui qui a déposé la ressource (pour l'instant)
      if ($id_user_moderator == 35) {

        //Si on récupère bien le messsage elgg
        if ($message = get_entity($id_message)) {

          //On enlève les 10 points gagnés avec le message
          //perdrePointsMessageSupprime($message);
          
          //Et qu'on arrive à le supprimer
          if (delete_entity($message->guid, true)) {
            
            // On va le supprimer dans la base PF
            $result = mysql_query("DELETE FROM `cape_messages` WHERE `id_capsule` = $id_capsule AND id_message = $id_message");
            // Si on y arrive
            if ($result) {
              
              // On renvoie un statut positif
              echo json_encode(array("status" => "ok")); 
            }
            else
            {
              // Sinon, on renvoie l'erreur SQL
              error_log(mysql_error());
              echo json_encode(array("status" => "ko", "message" => mysql_error()));
            }
          }
          else{
            //On remet les points
            //gagnerPointsMessageActive($message);
            echo json_encode(array('status' => "ko","message" => "Vous n'avez pas l'autorisation de supprimer ce message."));
          }
        }
        else{
          echo json_encode(array('status' => "ko","message" => "Ce message a déjà été supprimé, et n'existe plus."));
        }
      
        //Remise en place de l'override
        elgg_set_ignore_access(false);

      }
      else {
        echo json_encode(array('status' => "ko","message" => "Vous n'avez pas l'autorisation de supprimer ce message."));
      }
      
    }
    else  {
      echo json_encode(array('status' => "ko","message" => "Vous n'avez pas l'autorisation de supprimer ce message."));
    }
    
  }


?>