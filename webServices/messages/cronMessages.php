<?php
	/*
	*  Envoie un mail résumant les messages durant les 24 dernières heures
	*  Utilise le logiciel Cronnix pour la planification CRON (sur le serveur)
	*/

	//Connexion à la base de données
	try {
		$dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');	
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$dbh->exec("SET CHARACTER SET utf8");
		// $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
	} catch (Exception $e) {
		error_log($e->getMessage());
	}
	//Nombre de messages à envoyer
	$number_of_messages_to_show = 10;
	//On store la date actuelle
	$current_time = time();
	//On récupère les messages des dernières 24h
	//Pour ça, on calcule le timestamp correspondant à t -24h
	$one_day_before = $current_time - 3600 * 24;
	// $one_day_before = $current_time - 3600 * 24 * 7;

	//Ensuite, on requête la base sce_elgg_entity croisée avec cape_messages
	$query_messages = $dbh->prepare("SELECT 
		`cm`.`id_message`,
		`cr`.`nom_court` AS 'nom_court_ressource',
		`ccl`.`nom_court` AS 'nom_court_capsule',
		`cm`.`id_utilisateur` AS 'id_auteur',
		`cm`.`contenu`,
		`cm`.`date_creation`,
		`cm`.`date_modification`,
		`cm`.`id_capsule`,
		`cm`.`nom_page`,
		`cm`.`nom_tag`,
		`cm`.`num_occurence`,
		`cm`.`geo_lattitude`,
		`cm`.`geo_longitude`,

		`cm`.`id_message_parent`,
		`cm`.`id_langue`,
		`cm`.`supprime_par`,
		`cm`.`medaille`,
		`cm`.`est_defi`,
		`cm`.`defi_valide`,
		IF(`cm`.`visibilite` != 0, 1, 0) AS 'prive',

		`ue`.`username` AS 'pseudo_auteur',
		`ue`.`name` AS 'nom_auteur',
		`cuc`.`id_categorie` AS 'id_role_auteur',
		`cc`.`nom` AS `role_auteur`,

		IFNULL(SUM(`cmv`.`vote`), 0) AS 'somme_votes', 
		CONCAT('http://podcast.mines-nantes.fr/PairForm_Elgg/avatar/view/',`ue`.`username`,'/medium/0') AS 'url_avatar_auteur'

		FROM `cape_messages` AS `cm`

		JOIN `sce_users_entity` `ue` ON `ue`.`guid` = `cm`.`id_utilisateur`
		JOIN `cape_capsules` AS `ccl` ON `ccl`.`id_capsule` = `cm`.`id_capsule`
		JOIN `cape_ressources` AS `cr` ON `cr`.`id_ressource` = `ccl`.`id_ressource`

		JOIN `cape_utilisateurs_categorie` AS `cuc` ON (`cuc`.`id_utilisateur` = `cm`.`id_utilisateur` AND `cuc`.`id_ressource` = `cr`.`id_ressource`)
		JOIN `cape_categories` AS `cc` ON `cc`.`id_categorie` = `cuc`.`id_categorie`
	
		LEFT OUTER JOIN `cape_messages_votes` AS `cmv` ON `cmv`.`id_message` = `cm`.`id_message`

		WHERE cm.date_creation > ? AND cm.supprime_par = 0 AND cm.visibilite = 0
		
		GROUP BY `cm`.`id_message`

		ORDER BY `somme_votes` DESC");

	$query_messages->execute(array($one_day_before));
	//On store le tableau
	$last_messages = $query_messages->fetchAll(PDO::FETCH_ASSOC);
	//On store le nombre de messages concernés
	$messages_count = $query_messages->rowCount();
	error_log("Tache cronMessage : ".$messages_count." messages concernés.");
	//Si on a un résultat,
	if($messages_count > 0)
	{

  		include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
		//On récupère l'id et mail des users intéressés
		//$query_mail = $dbh->prepare("SELECT u.guid, u.username, u.email FROM sce_users_entity AS u, cape_utilisateurs AS cu WHERE cu.notification_mail = 1 AND u.guid = cu.id_elgg");

  		$array_capsules = array();

  		//On regarde les capsules concernées, et on stocke dans un tableau
  		foreach ($last_messages as $key => $value) {
  			if (!in_array($value['id_capsule'], $array_capsules)) {
				array_push($array_capsules, $value['id_capsule']);
			}	
  		}
		$number_params = str_repeat('?,', count($array_capsules) - 1) . '?';
		//On prépare la query rank
		$query_rank = $dbh->prepare("SELECT DISTINCT u.username, u.email, u.guid, GROUP_CONCAT(cucp.id_capsule) AS 'capsules'  FROM cape_utilisateurs_capsules_parametres AS cucp, sce_users_entity AS u WHERE cucp.notification_mail = 1 AND cucp.id_utilisateur = u.guid AND cucp.id_capsule IN ($number_params) GROUP BY cucp.id_utilisateur");
		$query_rank->execute($array_capsules);

        $headers = "From: notifications@pairform.fr\r\n";
        $headers .= "Reply-To: contact@pairform.fr\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$subject = 'PairForm - Derniers messages sur vos ressources';
        $body = '<html>';
        $body .= '<body style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-size: 14px; background-image: linear-gradient(to bottom,  #ffffff 0%,#e5e5e5 100%);';
        
//#comBar #comContainer .comBox {padding: 20px; display: block; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3); margin-bottom: 20px; border-radius: 4px; clear: both;   }
		//Pour chaque utilisateur
		while ($user = $query_rank->fetch(PDO::FETCH_ASSOC)) {
			
				$user_pseudo = $user['username'];
				
				//Composition du mail : 

		        $to = $user['email'];
		        $user_body = "<section style=\"min-width: 700px; width: 80%; margin: 0 auto; padding: 0 20px;\" id='bodyWrapper'>			
			        			<h1 style=\"font-size: 1.8em; font-weight: 100; clear: both; float: left;\">Bonjour $user_pseudo!</h1>
			        			<a href='http://www.pairform.fr'><img  style=\"float: right;\" align=\"right\" src='http://www.pairform.fr/img/PairFormFullFinal.png'></a>
			        
			        			<h2 style=\"font-weight: 100; clear: both; float: left;\">Voici les messages les plus populaires (sur $messages_count nouveaux messages) pendant les derni&egrave;res 24 heures, sur les ressources que vous suivez : </h2>
			        			<div id='comContainer'>";
	
				//Flag pour voir si l'utilisateur est concerné par au moins un des messages :
				$isConcernedByMessages = false;
				//Récupération des capsules suivies par l'utilisateur en cours
				$followed_capsules = explode(",", $user['capsules']);

				error_log($to . " -> " . print_r($followed_capsules,true));
				// foreach ($last_messages as $index => $message) {
				for ($i=0; $i < min(count($last_messages), $number_of_messages_to_show); $i++) { 
					$message = $last_messages[$i];

					
					if(in_array($message['id_capsule'], $followed_capsules)){

						$isConcernedByMessages = true;
						foreach ($message as $key => $value) {
							$$key = $value;
						}

						
						$user_body .=
						"<div style=\"box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3); border: 1px solid rgba(215, 215, 215, 1); margin-bottom: 20px; margin-left: auto; margin-right: auto; border-radius: 4px; border-top-left-radius: 30px; border-bottom-right-radius: 40px; z-index: 25; clear: both; padding: 20px; width:600px;\" class='comBox'>
							<div class='comProfil' style='display: inline-block; vertical-align: top; margin: 0 10px;'>
								<img class='imgProfil' src='$url_avatar_auteur' style='width: 60px; height: 60px; background: white; margin-bottom:5px; border-radius: 8px;'>
								<div class='voteSection'>				
									<div class='voteCount'style=\"text-align: center; \">$somme_votes vote".($somme_votes > 1 ? "s" : "") ."</div>
								</div>
							</div>
							<div class='comContent'  style=\"display: inline-block; width: 500px;\">
								<div class=\'comTitle\" style=\"border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: rgba(0, 0, 0, 0.1); box-shadow: 0 1px 0 rgba(0, 0, 0, 0.05);\">
									<span class=\"comUserName\" style=\"font-weight: bold; font-size: 16px; color: #444;\">$pseudo_auteur</span>
									<span class=\"comUserRank\" style=\"font-size: 12px; color: gray;\"> - $role_auteur</span>
									<div class=\"timeCreated\" style=\"font-style: italic; font-size: 12px; color: #444; float: right; padding-top: 4px;\">Le ".date('d/m/y à G:i', $date_creation)."</div>
								</div>
								<div class=\"comText\" style=\"min-height: 40px; white-space: pre-line; color: rgb(49, 49, 49); margin: 10px 0;\">$contenu</div>
								<div class=\"actionSection\" style=\"font-size: 12px; color: grey; text-align: right;\" align=\"right\">
									 ".($nom_page == '' ? 'Sur' : 'Dans')." la ressource $nom_court_ressource - $nom_court_capsule <div style=\"display: inline-block; border-right-style: solid; border-right-width: 0px; border-right-color: #FFF; cursor: pointer; -webkit-transition: all ease-in-out 0.4s; -moz-transition: all ease-in-out 0.4s; transition: all ease-in-out 0.4s; padding: 0 5px;\"><a href=\"https://www.pairform.fr/node/webServices/message/rediriger?id_message=".$id_message."\">(cliquez pour voir le contexte)</a></div>
								</div>
							</div>
						</div>";
					}
				}
				
				$user_body .= "</div>
							<h3 style=\"font-weight: 100; clear: both; float: left;\">Si vous ne souhaitez plus recevoir de notification par rapport à une ressource, rendez-vous dans la ressource en question, allez dans \"Préférences\" dans le menu latéral, puis cliquez sur \"Désactiver les notifications\"</h3>
							<h1 style=\"font-size: 1.8em; font-weight: 100; clear: both; float: left; text-align: center; width: 100%;\" align=\"center\">L'équipe PairForm.</h1>
						</section>
					</body>
				</html>";

				if ($isConcernedByMessages)
				{
					// echo $headers.$body.$user_body;	
					mail($to, $subject, $body.$user_body, $headers);
					error_log("Mail envoyé à $to ($user_pseudo)");
				}
		}
	}
	//Sinon, on ne fait rien.
	// print($body.$user_body);

?>