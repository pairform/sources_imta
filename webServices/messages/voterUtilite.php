<?php

  //********************************************//
  //************* voterUtilite **************//
  //********************************************//
  /*
   * Change l’utilité d’un message
   * 
   * Paramètres : 
   * up : booléen. True pour vote positif, false pour vote négatif.
   * id_message : id du message à traiter
   *
   * Retour : 
   * {"status":"ok"
   * "message" : "message" ( uniquement en cas d'erreur )
   * }
  */
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerPoints.php");
  require_once('../ws/Pusher.php');
  // if(!$pusher)
  //   $pusher = new Pusher('0b0142b03d240425e90f', '2ae0897085fc6c512871', '55928');

	$os = $_POST['os'];
  	$version = $_POST['version'];
  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
  }  
  error_log("Id user : " . $id_utilisateur);
    if(!isset($id_utilisateur) || $id_utilisateur == 0)
    {
      $os == 'web' ? print(json_encode(array('status' => 'ko', 'message' => 'web_label_session_expiree'))) : print(json_encode(array('status' => 'ko', 'message' =>"Erreur d'enregistrement du message.")));
      exit;
    }
    // Valeurs d'entrees 
    if(isset($_POST['up']))
    {
        $bUp = $_POST['up'] == "true" ? true : false;
        $id_message = (int)mysql_real_escape_string($_POST['id_message']);

        $message = get_entity($id_message);

        // l'utilisateur ne peux pas voter ses propres messages
        if ($message->owner_guid == $id_utilisateur) {
          if (isset($_POST['id_utilisateur'])) 
            print(json_encode(array('status' => 'ko', 'message' => 'web_label_pas_voter_votre_message')));
          else
            print(json_encode(array('status' => 'ko', 'message' => 'Vous ne pouvez pas voter pour vos messages!')));
            exit();
        }
        // check si l'utilisateur a déjà le même vote
        if (elgg_annotation_exists($id_message, getTypes($bUp), $id_utilisateur))
        {
            deleteLike($bUp, $id_message, $id_utilisateur);
        }
        else
        {     //S'il a fait le vote inverse
            if(elgg_annotation_exists($id_message, getTypes(!$bUp), $id_utilisateur))
            {
                deleteLike(!$bUp, $id_message, $id_utilisateur);
                createLike($bUp, $id_message, $id_utilisateur);
            }
            else
            {
                createLike($bUp, $id_message, $id_utilisateur);
            }

        }
        print(json_encode(array('status' => 'ok')));
        
        // $likes           = elgg_get_annotations(array(
        //   'guid' => $id_message,
        //   'annotation_name' => 'likes'
        // ));
        // $dislikes        = elgg_get_annotations(array(
        //     'guid' => $id_message,
        //     'annotation_name' => 'dislikes'
        // ));
        // $voteCount = count($likes) - count($dislikes);

        // $pusher->trigger('pairform', 'new_vote', array('id_message' => $id_message, 'voteCount' => $voteCount));
      }
     //error_log("entity_guid : $id_message ,typeLike : ".getTypes(!$bUp).", !bUp : !$bUp, user->guid : $user->guid, annotation : $annotation");
  
    function getTypes($up, $type = '')
    {
        //Si on est dans le cas d'un ajout,
        if($up)
        {
            $typeLike = 'likes';
            $typeLikeQuote = "likes";
        }
        else
        {
            $typeLike = 'dislikes';
            $typeLikeQuote = "dislikes";
        }

        return ($type == "quote" ? $typeLikeQuote : $typeLike);
    }

    //Fonctions de créations / suppression
    function createLike($up, $id_message, $id_utilisateur)
    {
        $annotation = create_annotation($id_message,
            getTypes($up),
            getTypes($up, "quote"),
            "",
            $id_utilisateur,
            2);
        $message = get_entity($id_message);
        // error_log(__LINE__.' — $message = ' . print_r($message, true));
        //$message->set("time_updated",time());
        $message->time_updated_meta = time();
        $message->save();
        gagnerPointsEvaluerMessage($message,true, $id_utilisateur);
        if ( $up)  {
            gagnerPointsEvaluerMessagePassif($message,true,$message->getOwnerGUID());
            gagnerSuccesUtiliteRecu($message->getOwnerGUID());
        }
        else gagnerPointsEvaluerMessagePassif($message,false,$message->getOwnerGUID());
        gagnerSuccesVote();
    }
    
    function deleteLike($up, $id_message, $id_utilisateur)
    {
        $likes = elgg_get_annotations(array(
            'guid' => $id_message,
            'annotation_owner_guid' => $id_utilisateur,
            'annotation_name' => getTypes($up) ));

        $like = $likes[0];

        if ($like && $like->canEdit())
        {
            $like->delete();
        }
        $message = get_entity($id_message);
         // error_log(__LINE__.' — $message = ' . print_r($message, true));
        $message->time_updated_meta = time();
        $message->save();
        gagnerPointsEvaluerMessage($message,false, $id_utilisateur);
        if ( $up)  gagnerPointsEvaluerMessagePassif($message,false,$message->getOwnerGUID());
        else gagnerPointsEvaluerMessagePassif($message,true,$message->getOwnerGUID());
    }


?>