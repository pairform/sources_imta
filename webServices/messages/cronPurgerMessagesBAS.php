<?php


  //********************************************//
  //********* cronPurgerMessageBAS.php *********//
  //********************************************//

/*
 * Supprime tous les messages du bac à sable. Absolument pas sécurisé, mais en même temps, pourquoi faire?
 * 
 * 
 * Retourne:
 *   - status (string)
 *   - message (string)
*/  

  try {
    $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET CHARACTER SET utf8");
    // $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
  } catch (Exception $e) {
    error_log($e->getMessage());
  }

 //Suppression des messages dans la base et dans elgg

  $result_query_messages = $dbh->query("SELECT COUNT(id_message) AS 'count' FROM `cape_messages` WHERE date_creation <= (UNIX_TIMESTAMP() - 3600) AND id_capsule = 51");
  if ($result_query_messages) {
    error_log("######## Tache cronPurgerMessageBAS ");
      $result_delete_messages = $dbh->exec("DELETE `cm`, `cn` 
                              FROM `cape_messages` AS `cm` JOIN `cape_notifications` AS `cn` ON `cn`.`id_message` = `cm`.`id_message`
                              WHERE `cm`.`id_capsule` = 51 
                              AND cm.date_creation <= (UNIX_TIMESTAMP() - 3600)");
      // mysql_error();


    if (!$result_delete_messages) {
      echo json_encode(array("status" => "ko", "message" => "Ahahah!"));
    }
    else
    {
      $dbh->exec("UPDATE `cape_utilisateurs_categorie` 
                    SET `id_categorie_temp`= 5,`score`= 0 
                    WHERE  `id_ressource` = 27");


      $dbh->exec("UPDATE `cape_utilisateurs_capsules_parametres` SET notification_mail = 0, afficher_noms_reels = 0 WHERE `id_capsule` = 51");

      echo json_encode(array("status" => "ok", "message" => "Tous les messages de cette ressource ont été définitivement supprimés.")); 
    }
  }
  else {
    echo json_encode(array("status" => "ko", "message" => "Ahahah!"));
  }
?>