<?php

  //********************************************//
  //*********** recupererNombreMessages.php ***********//
  //********************************************//
  
  /* Recuperation du nombre de messages a un niveau particulier.
   *
   * Paramètres :
   * nom_page (string) - nom de la page concernée
   * id_ressource (int) - id de la ressource concernée
   * vue_trans (void) - option pour afficher en vue transversales
   * langues_affichage (string) - ids des langues à afficher (id séparés par des virgules)
   *
   * Utilisation :
   *
   * aucun argument : vue globale
   * id_ressource : vue au niveau de la ressource 
   * id_ressource et nom_page : vue au niveau de la page ( chaque object d'apprentissage )
   * 
   *
   *
   * Retour : 
   * {"nombreDeMessages":
   *  {
   *    "7":12
   *   }
   * }
   * 
   */
  
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");





  $os = $_POST['os'];
  $version = $_POST['version'];
  $langues_affichage = isset($_POST['langues_affichage']) ? $_POST['langues_affichage'] : "1,2,3";
  //Exemple de message lu
  //$listeMessagesLus = array(155);
  //$id = $_POST['id_message'];

  




// N'affiche pas les reponse au niveau 1
function getNombreDeMessages($result_messages,$granularite){
      
      $user = elgg_get_logged_in_user_entity();

      if (gettype($user) == 'object')
        $user_guid = $user->guid;
      else
        $user_guid = -1;

      while($row_message_sc = mysql_fetch_assoc($result_messages))
      {
        
        $id_message = $row_message_sc['id_message'];
        // Recupere l'entity objet 
        $message_entity = get_entity($id_message);

        //Sécurité, pour être sûr que le message référencé en base Elgg existe bien dans Elgg
        //S'il existe pas, get_entity renvoie un booléen, donc on passe pas.
        if (gettype($message_entity) == 'object') {
          $message_id_ressource = $row_message_sc['id_ressource'];
          $message_nom_page = $row_message_sc['nom_page'];
          $message_nom_tag = $row_message_sc['nom_tag'];
          $message_num_occurence = $row_message_sc['num_occurence'];
          $message_supprime_par = (int) $row_message_sc['supprime_par'];
          
          $count = 1;

          //Message supprimé
          if($message_supprime_par != 0)
          {
            //Et ce n'est pas l'user qui a supprimé son propre message
            if($message_supprime_par != $user_guid)
            {
              //On le décompte du nombre total.
                $count = 0;
            }         
          }

          //Vérification que l'utilisateur a accès à ce message
          if($user_guid) 
          {
            // $user = elgg_get_logged_in_user_entity();
            if (!has_access_to_entity($message_entity, $user) && ($user->guid != 35))
              $count = 0;
          }
          //S'il n'y a pas d'user connecté, et que le message n'est pas public
          else if ($message_entity->access_id != 2) {         
            $count = 0;
          }

          switch ($granularite) {
            case 'globale':

            if ( !isset($result[$message_id_ressource])) $result[$message_id_ressource] = 0;
            $result[$message_id_ressource] += $count;
              break;
            case 'ressource':

            $result[$message_nom_page] += $count;
             break;

            case 'page':
            $result[$message_nom_tag][$message_num_occurence] += $count;
              break;

            default:
              # La réponse D.
              break;
          }
        }
      }

      return $result;
  }


     

      $granularite = ''; //globale, ressource ou page

      //En fonction de ce que l'on envoie au webservice, on va faire une sélection plus fine des messages
      if(isset($_POST['id_ressource']) && isset($_POST['nom_page']))
      {      
        $id_ressource = mysql_real_escape_string($_POST['id_ressource']);
        $nom_page = mysql_real_escape_string($_POST['nom_page']);
        $granularite = 'page';

        $result_messages = mysql_query("SELECT * FROM `cape_messages` , `sce_entities`
            WHERE `sce_entities`.`owner_guid` != 0
            AND  `cape_messages`.`id_message` = `sce_entities`.`guid`
            AND `id_ressource` = $id_ressource AND `nom_page` = '$nom_page' AND `cape_messages`.`langue` IN ($langues_affichage) 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }

      else if(isset($_POST['id_ressource']) && !isset($_POST['nom_page']))
      {
        $id_ressource = mysql_real_escape_string($_POST['id_ressource']);
        $granularite = 'ressource';
        $result_messages = mysql_query("SELECT * FROM `cape_messages` , `sce_entities`
            WHERE `sce_entities`.`owner_guid` != 0
            AND  `cape_messages`.`id_message` = `sce_entities`.`guid`
            AND `id_ressource` = $id_ressource AND `cape_messages`.`langue` IN ($langues_affichage) 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }


      else if(!isset($_POST['id_ressource']) && !isset($_POST['nom_page']) && !isset($_POST['vue_trans']))
      {        
        $granularite = 'globale';

        $result_messages = mysql_query("SELECT * FROM `cape_messages`, `sce_entities`
            WHERE `sce_entities`.`owner_guid` != 0
            AND  `cape_messages`.`id_message` = `sce_entities`.`guid`
            AND `cape_messages`.`langue` IN ($langues_affichage) 
          ORDER BY `cape_messages`.`id_ressource` ASC,
          `cape_messages`.`nom_page` ASC,
          `cape_messages`.`nom_tag` ASC,
          `cape_messages`.`num_occurence` ASC ");

        if(!$result_messages)
          error_log(mysql_error());
      }

      else 
      {      
        $granularite = 'globale';

          $result_messages = mysql_query("SELECT  `id_message` , `id_ressource` , `nom_page` , `nom_tag` , `num_occurence` , `geo_lattitude` , `geo_longitude` , `supprime_par`   
            FROM `cape_messages` , `sce_entities`
            WHERE `sce_entities`.`owner_guid` != 0
            AND  `cape_messages`.`id_message` = `sce_entities`.`guid`
            AND `cape_messages`.`langue` IN ($langues_affichage) 
            ORDER BY `sce_entities`.`time_created` DESC   ");

        if(!$result_messages)
          error_log(mysql_error());
      }

  switch ($os) {
    case 'web':    
      $result = getNombreDeMessages($result_messages,$granularite);
      $return = json_encode($result);
  
      //Retour JSON
      print($return);
      break;

    case 'ios':
      $result = getNombreDeMessages($result_messages,$granularite);
      $return = json_encode($result);
  
      //Retour JSON
      print($return);
      break;

    case 'and':
      # code...
      break;
    
    default:
      print('La réponse D');
      break;
  }

?>