<?php

  //********************************************//
  //******** redirigerVersMessage.php **********//
  //********************************************//
  
  /*
  * Redirige vers un message sur la plateforme web, à partir de l'id du message.
  *
  * Paramètre (GET || POST) :
  *   id_message (int || string) - id du message
  *
  * TODOs: 
  *   - trouver une solution pour identifier les ressources scenari pour rajouter le /co
  */
	
  //S'il n'y a pas d'id de message, no use!
  if(!isset($_REQUEST["id_message"])){
    die(json_encode(array("status" => "ko", "message" => "no_message_id")));
  }

  //Récuperation de l'id de message en int
  $id_message = gettype($_REQUEST["id_message"]) == "string" ? (int)$_REQUEST["id_message"] : $_REQUEST["id_message"];

  try {
      $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $dbh->exec("SET CHARACTER SET utf8");
      $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
  } catch (Exception $e) {
    error_log($e->getMessage());
  }

  //Récuperation des notifications utilisateur
  $sel_stmt = $dbh->prepare("SELECT 
                              `cm`.`id_message`,
                              `cm`.`uid_page`,
                              `cm`.`uid_oa`,
                              `cm`.`id_capsule`,
                              `cm`.`nom_page`,
                              `cm`.`nom_tag`,
                              `cc`.`url_web`
                            FROM  `cape_messages` `cm` 
                              JOIN `cape_capsules` `cc` ON `cm`.`id_capsule` = `cc`.`id_capsule`
                            WHERE `cm`.`id_message` = ?");

  $sel_stmt->execute(array($id_message));


  if ($sel_stmt->rowCount() > 0) {
    $ret_stmt = $sel_stmt->fetch(PDO::FETCH_ASSOC);
    $base_url = $ret_stmt['url_web'];

    //Mode automatique AUTO
    if (trim($ret_stmt['nom_tag']) != "") {
      //Sur la ressource
      if (trim($ret_stmt['nom_page']) == "") {
        $nom_page = "index.html";
        $anchor = "res";
        
      }
      //Sur une page
      else{
        $nom_page = $ret_stmt['nom_page'].'.html';
        $anchor = "page";
        //TODO: trouver une solution pour identifier les ressources scenari pour rajouter le /co
        $base_url .= "/co";

      }
    }
    //Mode identifié OA
    else if (isset($ret_stmt['uid_page'])) {
      $nom_page = $ret_stmt['uid_page'] != "" ? $ret_stmt['uid_page'].'.html' : "";
      $anchor = $ret_stmt['uid_page'] != "" ? "page" : "res";
    }

    $final_url = $base_url.'/'.$nom_page.'#'.$anchor.'-'.$ret_stmt['id_message'];
    
    if (isset($_POST["id_message"])) {
      die(json_encode(array("status" => "ok", "url" => $final_url)));
    }
    else if (isset($_GET["id_message"])) {
      // header("HTTP/1.1 301 Moved Permanently"); 
      // header("Location : ".$final_url);
      header('Location: ' . $final_url, true, 301);
      exit;
    }
    else{
      die(json_encode(array("status" => "ko", "message" => "unknown_protocol")));
    }
    
  }
  else
    die(json_encode(array("status" => "ko", "message" => "no_message")));
  
?>