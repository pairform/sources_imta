<?php

error_log("//********************************************//");
error_log("//******** EnregistrerMessage.php ************//");
error_log("//********************************************//");

/* Enregister un message
*
* Paramètres :
* nom_page (string) - nom de la page concernée.
* id_capsule (int) - id de la ressource concernée.
* nom_tag ( string ) - nomdu tag ( PAGE ou autre ).
* num_occurence (int) - le numero de l'occurence de l'objet pedagogique.
* id_message_parent (int) - id du message original , seulement si c'est une reponse
* value (string) - le texte du message.
* parent (int) - id du message parent, seulement si c'est une reponse
* est_defi (void) - indique si le message est un defi.
* (optionel) visibilite ( tableau int ) - indique les id de visibilité si different de defaut ex [ 4,5,6];
* (optionel) tags ( tableau string ) - indique tags associés au message;
* langue (int) - id de la langue du message
*
* Dans les cas ou les messages sont accrochés sur OA :
* uid_page (int) - numéro de page unique
* uid_oa (int) - numéro de l'OA unique dans la page
*
* Retour : 
* {
*	"statut":"ok",
*	"id_message" : (int) - id du message créé
* }
* 
*/

include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
include_once("../accomplissement/gagnerPoints.php");
include_once("../accomplissement/gagnerSucces.php");

include_once("../push/api/api.php");

require_once('../ws/Pusher.php');
// require_once('../push/API.php');

if(!$pusher)
	$pusher = new Pusher('0b0142b03d240425e90f', '2ae0897085fc6c512871', '55928', true);


$os = $_POST['os'];
$version = $_POST['version'];
// error_log(print_r($_POST,true));

$id_ressource = isset($_POST['id_ressource']) ? mysql_real_escape_string($_POST['id_ressource']) : mysql_real_escape_string($_POST['id_capsule']);

//Gros fuck
if (isset($_POST['id_message_parent'])) {
	$_POST['id_message_original'] = $_POST['id_message_parent'];
}
//Cas de messages accrochés sur OA
if (($os == 'web') && ($version == '1.5'))
{
	$uid_page = isset($_POST['uid_page']) ? mysql_real_escape_string($_POST['uid_page']) : '';
	$uid_oa = isset($_POST['uid_oa']) ? mysql_real_escape_string($_POST['uid_oa']) : '';
}
else
{
	$nom_page = mysql_real_escape_string($_POST['nom_page']);

	//Si on est dans le cas d'un message sur une ressource
	$nom_tag= isset($_POST['nom_tag']) ? mysql_real_escape_string($_POST['nom_tag']) : 'PAGE';
	$num_occurence= isset($_POST['num_occurence']) ? (int)mysql_real_escape_string($_POST['num_occurence']) : 0;
}         

//Recuperation des variables envoyé pour le message
if ($os == "and") {
	$message = $_POST['value'];
} else  {
	$message = $_POST['contenu'];
}

if ($os == "and") {
	$tagarray = json_decode($_POST['tags']);
} else  {
	$tagarray = json_decode($_POST['tag']);
}

$visibilitearray = json_decode($_POST['visibilite']);

if(isset($_POST['langue'])){
	$langue = intval(json_decode($_POST['langue']));
} 
else{
	$langue = isset($_POST['id_langue']) ? intval($_POST['id_langue']) : 3;	
}

//Si tableau de visiblité est vide, ou élément string vide (cas du web)
//On met la visibilité à null
if (empty($visibilitearray) || $visibilitearray[0] == "") {
	$visibilitearray = null;
}

$path_message = $id_ressource.'/'.$nom_page.'/'.$nom_tag.'/'.$num_occurence;

if(isset($_POST['id_utilisateur'])){
	$id_auteur = $_POST['id_utilisateur'];
	$utilisateur_connecte = get_user($id_auteur);

}
else{
	$id_auteur = elgg_get_logged_in_user_guid();
	$utilisateur_connecte = elgg_get_logged_in_user_entity();
}

if(!$id_auteur)
	die(json_encode(array('status' => "ko",'message' => 'not_logged_in')));

//Login
//Test si le blog existe déjà
//Sinon, on enregistre le blog dans la base
//On génère un ID sous forme /id_ressource/nom_de_la_page.html/type_de_tag/occurence_du_tag
//Ensuite, on enregistre le couple de cet ID et l'id Elgg dans la base 


if(isset($_POST['id_message']) && ($_POST['id_message'] != "") && ($_POST['id_message'] != "0"))
{
	$id_message = mysql_real_escape_string($_POST['id_message']);
	$options = array('owner_guid' => $id_auteur, 'type' => 'object', 'guid' => $id_message);
	$messages_elgg = elgg_get_entities($options);

	$blog = $messages_elgg[0];

	$blog->description = $message;
// Heure de modification
	$blog->time_updated = date("dd/mm/yy");
// Now save the object
	if (!$blog->save()) 
	{
		register_error(elgg_echo("blog:error"));
//forward($_SERVER['HTTP_REFERER']);
	}
	// error_log('Edition message (user #'.$id_auteur.'): ' . print_r($_POST,true));
	print(json_encode(array('status' => "ok")));
	$pusher->trigger('pairform', 'new_message', array('id_ressource' => $id_ressource, 'nom_page' => $nom_page, 'nom_tag' => $nom_tag, 'num_occurence' => $num_occurence, 'modified' => true));
}
else
{


elgg_set_ignore_access(true);
	// Initialise a new ElggObject
	$blog = new ElggObject();
	// Tell the system it's a blog post
	$blog->subtype = "blog";
	// Set its owner to the current user
	$blog->owner_guid = $id_auteur;
	// For now, set its access
	//$blog->access_id = 1; public access

	// Gestion des acces.

	// Creation d'un acces special si plusieurs cercle/classe sont choisis , cf doc.

	//Si le message est une reponse on prend la visibilité du parent
	if ($os == "and") 
	{
		if ( isset($_POST['parent']) && $_POST['parent'] != "" ) {
			$parent = get_entity($_POST['parent']);
			$blog->access_id = $parent->access_id;
		} 
		else
		{
			//ATTENTION : Patch pour MSIT (Expérimentation avec HEC)
			//Tableau des utilisateurs concernés
			// $patchArrayMSIT2014 = array(351, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429);

			// if (in_array($id_auteur, $patchArrayMSIT2014) && (intval($id_ressource) == 20)) {
			// 	$new_access_id = access_plus_parse_access(array(31));
			// // error_log('Nouvel access id:'.$new_access_id);
			// 	$blog->access_id = $new_access_id;
			// }
			// else
			// {
			if ( $visibilitearray )
			{
				$new_access_id = access_plus_parse_access($visibilitearray);
				$blog->access_id = $new_access_id;
			}
			else
			{
				$blog->access_id = 2;
			}
			// }
		}
	} 
	else 
	{
		if ( isset($_POST['id_message_original']) && $_POST['id_message_original'] != "" ) 
		{
			$parent = get_entity($_POST['id_message_original']);
			$blog->access_id = $parent->access_id;
			if ($parent->owner_guid != $blog->owner_guid) {
				// $api->sendMessage($parent->owner_guid, "user_notification", "Reponse de ".elgg_get_logged_in_user_entity()->username, array("content" => "Ressource n°".$id_ressource, "id_message" => $_POST['id_message_original']));
				$api->sendMessage($parent->owner_guid, "un", "Réponse de ".ucfirst($utilisateur_connecte->username), array("c" => $id_ressource, "im" => $_POST['id_message_original'], "su" => "ru"));

			}
      
			// notifyUserOfAnswer('Bigood', 'Trebla', 'C2i-D2');
		} 
		else
		{
			if ( $visibilitearray ){

				$new_access_id = access_plus_parse_access($visibilitearray);
				$blog->access_id = $new_access_id;
			}
			else{
				$blog->access_id = 2;
			}
		}
	}


	// Set its title and description appropriately
	$blog->title = $path_message;

	// $blog->container_guid = "35";

	// $blog->site_guid = 1;
	// Description
	$blog->description = $message;

	// Heure de création
	$blog->time_created = time();

	// Now let's add tags. We can pass an array directly to the object property! Easy.
	if (is_array($tagarray)) 
	{
		$blog->tags = $tagarray;
	}
	//whether the user wants to allow comments or not on the blog post
	$blog->comments_on = false;
	// error_log(print_r($blog, true));
	// Now save the object
	if (!$blog->save()) 
	{

		register_error(elgg_echo("blog:error"));
	//forward($_SERVER['HTTP_REFERER']);
	}
	//Systeme de reponses
	if ($os == "and") {
		if ( isset($_POST['parent']) && $_POST['parent'] != "" ) {
			$blog->annotate('parent',$_POST['parent'],2, $id_auteur);
		} 
	} else {
		if ( isset($_POST['id_message_original']) && $_POST['id_message_original'] != "" ) {
			$blog->annotate('parent',$_POST['id_message_original'],2, $id_auteur);
		} 
	}

	//systeme de defi
	if ( isset($_POST['est_defi'])){
		if (((int)$_POST['est_defi']) == 1) {
			$blog->annotate('est_defi',1,2, $id_auteur); // 1 pour défi actif. 2 pour annotation public.
		}
	}


	//ID Elgg du blog
	$id_message = $blog->guid;
	// Success message
	system_message(elgg_echo("blog:posted"));
	// add to river
	add_to_river('river/object/blog/create', 'create', $id_auteur, $blog->guid);
	// Remove the blog post cache
	//unset($_SESSION['blogtitle']); unset($_SESSION['blogbody']); unset($_SESSION['blogtags']);
	
	//Cas de messages accrochés sur OA
	if (($os == 'web') && ($version == '1.5'))
	{
		$result = mysql_query("INSERT INTO `cape_messages` (`id_message`, `id_capsule`, `uid_page`, `uid_oa`, `geo_lattitude`,`geo_longitude`,`supprime_par`,`id_langue`) 
			VALUES ($id_message, $id_ressource, '$uid_page','$uid_oa','','', 0, $langue)");
	}
	else
	{
		$result = mysql_query("INSERT INTO `cape_messages` (`id_message`, `id_capsule`,`nom_page`,`nom_tag`,`num_occurence`, `geo_lattitude`,`geo_longitude`,`supprime_par`,`id_langue`) 
			VALUES ($id_message, $id_ressource,'$nom_page','$nom_tag',$num_occurence, '','', 0, $langue)");
	}

	//Points
	if ($result){
		gagnerPointsEcrireMessage($blog);
		gagnerSuccesMessage();
		gagnerSuccesRepondre();
		
		if ($os == "and") {
			if ( isset($_POST['parent']) && $_POST['parent'] != "" ) {
				$entity = get_entity ($_POST['parent']);
				$guid_owner = $entity->getOwnerGUID();
				gagnerSuccesRecevoirReponses($guid_owner);

				//Participation defi
				$entityGuid = $entity->getGUID();
				$optionsAnnotation = array(
					'guids'=> $entityGuid,
					'annotation_names' => array('est_defi'),
					'limit' => 0,
					);
				$annotations = elgg_get_annotations($optionsAnnotation);
				if (!empty($annotations)){
					gagnerSuccesParticiperDefi();
				}
			}
		} else {
			if ( isset($_POST['id_message_parent']) && $_POST['id_message_parent'] != "" ) {
				$entity = get_entity ($_POST['id_message_parent']);
				$guid_owner = $entity->getOwnerGUID();
				gagnerSuccesRecevoirReponses($guid_owner);

				//Participation defi
				$entityGuid = $entity->getGUID();
				$optionsAnnotation = array(
					'guids'=> $entityGuid,
					'annotation_names' => array('est_defi'),
					'limit' => 0,
					);
				$annotations = elgg_get_annotations($optionsAnnotation);
				if (!empty($annotations)){
					gagnerSuccesParticiperDefi();
				}
			}
		}
		//systeme de defi
		if ( isset($_POST['est_defi'])){
			if (((int)$_POST['est_defi']) == 1) {
				$api->sendMessage(0, "un", "Un nouveau défi a été créé!", array("c" => $id_ressource, "im" => $blog->guid, "su" => "dc"));
			}
		}
		print(json_encode(array('status' => "ok", "id_message" => $id_message)));
		$pusher->trigger('pairform', 'new_message', array('id_ressource' => $id_ressource, 'nom_page' => $nom_page, 'nom_tag' => $nom_tag, 'num_occurence' => $num_occurence));

		
	error_log('Nouveau message (user #'.$id_auteur.'): ' . print_r($_POST,true));
	}

	else {
		error_log(mysql_error());
		print(json_encode(array('status' => "ko", 'message' => "erreur")));
	}
	
elgg_set_ignore_access(false);
}
?>