<?php
//Connexion à la base de données
  try {
    $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET CHARACTER SET utf8");
    // $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
  } catch (Exception $e) {
    error_log($e->getMessage());
  }

include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
include_once("../accomplissement/gagnerSucces.php");
$os      = $_POST['os'];
$version = $_POST['version'];
$langues_affichage = isset($_POST['langues_affichage']) ? $_POST['langues_affichage'] : "1,2,3";
$filtre_ressource = isset($_POST['filtre_ressource']) ? json_decode($_POST['filtre_ressource']) : NULL;
$filtre_timestamp = isset($_POST['filtre_timestamp']) ? json_decode($_POST['filtre_timestamp']) : NULL;
$id_capsule = isset($_POST['id_capsule']) ? $_POST['id_capsule'] : NULL;
$nom_page = isset($_POST['nom_page']) ? $_POST['nom_page'] : NULL;
$nom_tag = isset($_POST['nom_tag']) ? $_POST['nom_tag'] : NULL;
$num_occurence = isset($_POST['num_occurence']) ? $_POST['num_occurence'] : 0;
$uid_page = isset($_POST['uid_page']) ? $_POST['uid_page'] : NULL;
$uid_oa = isset($_POST['uid_oa']) ? $_POST['uid_oa'] : NULL;
$id_message = isset($_POST['id_message']) ? $_POST['id_message'] : 0;


//Si on a un utilisateur loggé 
if(isset($_POST['id_utilisateur'])){
  $id_utilisateur = $_POST['id_utilisateur'];

}
else{
  $id_utilisateur = elgg_get_logged_in_user_guid();
}
//Sinon, on met -1 pour ne choper aucun vote de qui que ce soit
$id_utilisateur = isset($id_utilisateur) ? $id_utilisateur : -1000;

$sql_select = "SELECT 
`cm`.`id_message`,
`sceoe`.`description` AS 'contenu',
`e`.`time_created` AS 'date_creation',
CAST((CASE  WHEN (`msn_tag`.`string` = 'time_updated_meta' AND !ISNULL(`msv_tag`.`string`)) THEN (CASE WHEN `msv_tag`.`string` > `e`.`time_updated` THEN `msv_tag`.`string` ELSE `e`.`time_updated` END) ELSE `e`.`time_updated` END) AS SIGNED INTEGER) AS 'date_modification',
`cm`.`id_capsule`,
`cm`.`nom_page`,
`cm`.`nom_tag`,
`cm`.`num_occurence`,

IF(`e`.`access_id` NOT IN (2,1), 1, 0) AS 'prive',
GROUP_CONCAT(DISTINCT(CASE `n`.`string` WHEN 'parent' THEN v.`string` ELSE '' END) SEPARATOR '') AS 'id_message_parent',
`cm`.`id_langue`,
0 AS 'est_lu',
`cm`.`supprime_par`,
GROUP_CONCAT(DISTINCT(CASE `n`.`string` WHEN 'medaille' THEN v.`string` ELSE '' END) SEPARATOR '') AS 'medaille', 
(CASE `n`.`string` WHEN 'est_defi' THEN v.`string` ELSE 0 END) AS 'est_defi', 
SUM(CASE `n`.`string` WHEN 'defi_valide' THEN v.`string` ELSE 0 END) AS 'defi_valide', 
(SUM(CASE `n`.`string` WHEN 'likes' THEN 1 ELSE 0 END) - SUM(CASE `n`.`string` WHEN 'dislikes' THEN 1 ELSE 0 END)) AS 'somme_votes', 
(CASE WHEN (`n_table`.owner_guid = $id_utilisateur AND `n`.`string` = 'likes') THEN 1 WHEN (`n_table`.owner_guid = $id_utilisateur AND `n`.`string` = 'dislikes') THEN -1 ELSE 0 END) AS 'utilisateur_a_vote', 
CONCAT('[',GROUP_CONCAT(DISTINCT(CONCAT('\"', (CASE WHEN (`msn_tag`.`string` = 'tags' AND !ISNULL(`msv_tag`.`string`)) THEN `msv_tag`.`string` END), '\"')) SEPARATOR ', '), ']') AS 'tags', 
CONCAT('[',GROUP_CONCAT(DISTINCT(CONCAT('\"', (CASE WHEN (`msn_tag`.`string` = 'tags' AND !ISNULL(`ue_tag`.`username`)) THEN `ue_tag`.`username` END), '\"')) SEPARATOR ', '), ']') AS 'noms_auteurs_tags',
`cm`.`geo_lattitude`,
`cm`.`geo_longitude`,

`e`.`owner_guid` AS 'id_auteur',
`ue`.`username` AS 'pseudo_auteur',
`ue`.`name` AS 'nom_auteur',
GREATEST(`cuc`.`id_categorie`,`cuc`.`id_categorie_temp`) AS 'id_role_auteur',
`cc`.`nom` AS `role_auteur`,
CONCAT('http://{$_SERVER['HTTP_HOST']}/PairForm_Elgg/avatar/view/',`ue`.`username`,'/medium/', (CASE `n`.`string` WHEN 'icontime' THEN v.`string` ELSE 0 END)) AS 'url_avatar_auteur'

FROM `cape_messages` AS `cm`

JOIN `sce_entities` `e` ON `cm`.`id_message` = `e`.`guid`  

LEFT OUTER JOIN `sce_annotations` `n_table` ON `cm`.`id_message` = `n_table`.`entity_guid`
LEFT OUTER JOIN `sce_metastrings` `n` on `n_table`.`name_id` = `n`.`id`  
LEFT OUTER JOIN `sce_metastrings` `v` on `n_table`.`value_id` = `v`.`id`  
LEFT OUTER JOIN `sce_users_entity` `ue` ON `e`.`owner_guid` = `ue`.`guid`  

JOIN `cape_utilisateurs_categorie` AS `cuc` ON `cuc`.`id_utilisateur` = `e`.`owner_guid`
JOIN `cape_categories` AS `cc` ON `cc`.`id_categorie` = GREATEST(`cuc`.`id_categorie`,`cuc`.`id_categorie_temp`)
JOIN `sce_objects_entity` AS `sceoe` ON `sceoe`.`guid` = `cm`.`id_message`

LEFT OUTER JOIN `sce_metadata` `md_tag` on `md_tag`.`entity_guid` = `e`.`guid`  
LEFT OUTER JOIN `sce_users_entity` `ue_tag` on `ue_tag`.`guid` = `md_tag`.`owner_guid`  
LEFT OUTER JOIN `sce_metastrings` `msv_tag` on `msv_tag`.`id` = `md_tag`.`value_id`  
LEFT OUTER JOIN `sce_metastrings` `msn_tag` on `md_tag`.`name_id` = `msn_tag`.`id`

WHERE  `cuc`.`id_ressource` = (SELECT `cape_capsules`.`id_ressource` FROM `cape_capsules` WHERE `cape_capsules`.`id_capsule`=`cm`.`id_capsule`)
AND `cm`.`id_langue` IN ($langues_affichage) ";

/**************************************************************************************************************************/
/************************************ Tous les messages de toutes les sections ********************************************/
/**************************************************************************************************************************/
if (!$filtre_ressource && !$id_capsule && (!$nom_page && !$uid_page) && (!$nom_tag  && !$num_occurence && !$uid_oa) && !$id_message){

  //On filtre rien, on chope tous les messages de la table cape_messages
}
/**************************************************************************************************************************/
/****************************************** Toutes les sections filtrées **************************************************/
/**************************************************************************************************************************/
else if ($filtre_ressource && !$id_capsule && (!$nom_page && !$uid_page) && (!$nom_tag  && !$num_occurence && !$uid_oa) && !$id_message){

    //Si le tableau de filtre par timestamp n'est pas vide (ce qui serait VRAIMENT idéal)
    if (!empty($filtre_timestamp)) {
      //On construit le where
      $where_clause = " AND (";

      //Pour chaque ressource dans la liste
      foreach ($filtre_timestamp as $id_res => $timestamp) {
        //On rajoute l'id dans la condition
        $where_clause .= "(`cm`.`id_capsule` = $id_res AND `e`.`time_updated` > $timestamp) OR ";
      }
      //On vire le dernier " OR "
      $where_clause = substr($where_clause, 0, -4);
      $where_clause.=") ";
      //On append ça à la query
      $sql_select .= $where_clause;
    }
    //Sinon, old school
    else{
      //On construit le where
      $where_clause = " AND `cm`.`id_capsule` IN (";
      //Pour chaque ressource dans la liste
      foreach ($filtre_ressource as $index => $id_res) {
        //On rajoute l'id dans le IN
        $where_clause .= mysql_real_escape_string($id_res) . ',';
      }
      //On vire la dernière ","
      $where_clause = substr($where_clause, 0, -1);
      $where_clause.=") ";
      //On append ça à la query
      $sql_select .= $where_clause;
      
    }
}
/**************************************************************************************************************************/
/************************************************ Une section *************************************************************/
/**************************************************************************************************************************/
else if (!$filtre_ressource && $id_capsule && (!$nom_page && !$uid_page) && (!$nom_tag  && !$num_occurence && !$uid_oa) && !$id_message){
$sql_select .= "AND `cm`.`id_capsule` = $id_capsule ";
}
/**************************************************************************************************************************/
/************************************************ Une page ****************************************************************/
/**************************************************************************************************************************/
else if (!$filtre_ressource && $id_capsule && ($nom_page || $uid_page) && (!$nom_tag  && !$num_occurence && !$uid_oa) && !$id_message){
if ($nom_page)
  $sql_select .= "AND `cm`.`id_capsule` = $id_capsule AND `cm`.`nom_page` = '$nom_page' ";

else
  $sql_select .= "AND `cm`.`id_capsule` = $id_capsule AND `cm`.`uid_page` = '$uid_page' ";
}
/**************************************************************************************************************************/
/************************************************ Un grain ***************************************************************/
/**************************************************************************************************************************/
else if (!$filtre_ressource && $id_capsule && ($nom_page || $uid_page) && (($nom_tag  && $num_occurence) || $uid_oa) && !$id_message){
if ($nom_tag  && $num_occurence)
  $sql_select .= "AND `cm`.`id_capsule` = $id_capsule AND `cm`.`nom_page` = '$nom_page' AND `cm`.`nom_tag` = '$nom_tag' AND `cm`.`num_occurence` = $num_occurence ";
else
  $sql_select .= "AND `cm`.`id_capsule` = $id_capsule AND `cm`.`uid_page` = '$uid_page' AND `cm`.`uid_oa` = '$uid_oa' ";
}
/**************************************************************************************************************************/
/************************************************ Un message **************************************************************/
/**************************************************************************************************************************/
else if (!$filtre_ressource && !$id_capsule && (!$nom_page && !$uid_page) && (!$nom_tag  && !$num_occurence && !$uid_oa) && $id_message){
$sql_select .= "AND `cm`.`id_message` = $id_message ";
}
else 
  die(json_encode(array('messages' => array())));


$sql_select .= " AND  ((`n_table`.`id` IS NULL) OR   
  (
   (
    (`n`.`string` IN ('medaille','est_defi', 'defi_valide', 'likes', 'dislikes', 'time_updated_meta', 'parent', 'icontime')) 
    AND (
     (`n`.`id` != `v`.`id`) AND
     (`n_table`.`access_id` IN (2) 
      OR (`n_table`.`owner_guid` = -1)
      OR (`n_table`.`access_id` = 0 
       AND `n_table`.`owner_guid` = -1
      )
     ) 
     AND `n_table`.`enabled`='yes'
    )
   )
   OR  
   (
    (
     (`msn_tag`.`string` IN ('tags', 'time_updated_meta')) 
     AND  `msv_tag`.`string` != '' 
  
     AND  ((`e`.`type` = 'object' 
     AND `e`.`subtype` IN (4))) 
     AND  (`e`.`site_guid` IN (1)) )
     AND 
     (
      (`md_tag`.`access_id` IN (2) 
       OR (`md_tag`.`owner_guid` = -1)
       OR (`md_tag`.`access_id` = 0 
        AND `md_tag`.`owner_guid` = -1
       )
      ) 
      AND `md_tag`.`enabled`='yes'
     )
  )
  ) 
)
AND 
(
  (
   (
      `e`.access_id IN(SELECT am.access_collection_id  
                        FROM sce_access_collection_membership am 
                        LEFT JOIN sce_access_collections ag ON ag.id = am.access_collection_id 
                        WHERE am.user_guid = $id_utilisateur GROUP BY am.access_collection_id)
      OR `e`.access_id IN(SELECT ag.id FROM sce_access_collections ag 
                            WHERE ag.owner_guid = $id_utilisateur GROUP BY ag.id)
      OR `e`.access_id = 2
      OR (e.owner_guid = $id_utilisateur)
      OR IF($id_utilisateur != -1000, e.access_id = 1, NULL)  
    )
  AND `e`.`enabled`='yes'
 ) OR $id_utilisateur = 35
)

GROUP BY `cm`.`id_message`
ORDER BY `e`.`time_updated` ";

// error_log($sql_select);
$result = $dbh->query($sql_select);
$return = json_encode(array('messages' => $result->fetchAll(PDO::FETCH_ASSOC)));

die($return);

?>