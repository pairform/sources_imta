<?php

  //********************************************//
  //*********** validerDefi.php ***********//
  //********************************************//
  
  /* Valider une reponse de défi.
   *
   * Paramètres :
   * id_message (int) - l'id du message reponde de défi
   *
   * 
   *
   *
   * Retour : 
   * {"status":"ok"
   * }
   * 
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
  include_once("../accomplissement/gagnerPoints.php");



  $id_message = $_POST['id_message'];
  $defi = get_entity($id_message);
  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
  }
  $arrayMedaille = $defi->getAnnotations("defi_valide");
  if ( empty($arrayMedaille)){
    $defi->annotate('defi_valide',1,2, $id_utilisateur);
    $return = json_encode(array('status' => "ok"));
    $defi->time_updated_meta = time();
    $defi->save();
    gagnerPointsReussirDefi($defi,$defi->getOwnerGUID());
  }else{
login(get_user($id_utilisateur), true);
    $defi->deleteAnnotations("defi_valide");
    $return = json_encode(array('status' => "ok"));
    $defi->time_updated_meta = time();
    $defi->save();
    logout();
  }
  
  echo $return; 
?>