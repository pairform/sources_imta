<?php

  //********************************************//
  //************ purgerMessage.php *************//
  //********************************************//

/*
 * Supprime tous les messages d'une ressource. Requiert une session valide.
 * Paramètres :
 *   - id_capsule (int)
 *   - hash_capsule (string)
 *   - id_deposeur (int)
 * 
 * Retourne:
 *   - status (string)
 *   - message (string)
*/  

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");


	$os = $_POST['os'];
  $version = $_POST['version'];

  if (isset($_POST['id_capsule']) && isset($_POST['hash_capsule']) && isset($_POST['id_deposeur'])) {
    $id_capsule = intval(mysql_real_escape_string($_POST['id_capsule']));
    $hash_capsule = mysql_real_escape_string($_POST['hash_capsule']);
    $id_deposeur = intval(mysql_real_escape_string($_POST['id_deposeur']));
  }
  else{
    error_log("Suppression message ressource : mauvais paramètres d'entrée");
    die(json_encode(array("status" => "ko", "message" => "Erreur dans la requête. Contactez l'équipe PairForm.")));
  }

  //Phase 1

  //Vérification que ce n'est pas une suppression frauduleuse.
  //On check si l'id utilisateur client est le même que celui serveur
  $id_utilisateur = elgg_get_logged_in_user_guid();
  
  if ($id_utilisateur != $id_deposeur){
    error_log("Suppression message ressource : mismatch id client ($id_deposeur) & id_serveur ($id_utilisateur)");
    die(json_encode(array("status" => "ko", "message" => "Erreur dans la requête. Contactez l'équipe PairForm.")));
  }
  
  

  //On check la conformité entre id de ressource et hash
  $sql_query = "SELECT * FROM `cape_capsules` WHERE `id_capsule` = $id_capsule 
    AND `hash_capsule` = '$hash_capsule'";

  //On check si l'utilisateur en cours est bien l'utilisateur qui a ajouté la ressource (si c'est pas l'admin)
  if ($id_utilisateur != 35) {
    $sql_query .= "AND `ajoute_par` = $id_utilisateur";
  }
  $result = mysql_query($sql_query);
	
  if (!$result) {
    error_log("Suppression message ressource : erreur SQL ".mysql_error());
    die(json_encode(array("status" => "ko", "message" => "Erreur dans la requête. Contactez l'équipe PairForm.")));
  }
  else{
    //S'il n'y a aucune ressource qui correspond
    if (!mysql_fetch_row($result)) {
      error_log("Suppression message ressource : aucune ressource ne satisfait les paramètres suivant (`id_ressource` = $id_capsule 
    AND `hash_capsule` = $hash_capsule 
    AND `ajoute_par` = $id_utilisateur )");
      die(json_encode(array("status" => "ko", "message" => "Erreur dans la requête. Contactez l'équipe PairForm.")));
    }
  }

  //Si on arrive la, c'est que c'est bon.

  //Phase 2

  //Suppression des messages dans la base et dans elgg
  $result = mysql_query("SELECT id_message FROM `cape_messages` WHERE `id_ressource` = $id_capsule");

  if ($result) {
    //Override des autorisations propres à chaque message
    elgg_set_ignore_access(true);
    while ($row = mysql_fetch_assoc($result)) {
      if ($message = get_entity($row['id_message'])) {
        if (!delete_entity($message->guid, true)) {
          $errors[] = "Impossible de supprimer le message n°$message->guid.";
        }
      }
      else
          $warnings[] = "Message n°{$row['id_message']} déjà supprimé";
    }
    //Remise en place de l'override
    elgg_set_ignore_access(false);
    if (count($errors) == 0) {
      $result = mysql_query("DELETE FROM `cape_messages` WHERE `id_ressource` = $id_capsule");
      if (!$result) {
        error_log(mysql_error());
        echo json_encode(array("status" => "ko", "message" => mysql_error()));
      }
      else
      {
        echo json_encode(array("status" => "ok", "message" => "Tous les messages de cette ressource ont été définitivement supprimés.")); 
      }
    }
    else{
        error_log('Erreur suppression messages : ' . print_r($errors, true));
        echo json_encode(array("status" => "ko", "message" => "Une erreur est survenue pendant la suppression des messages. L'équipe PairForm en a été alerté."));
    }
  }
  else {
    error_log(mysql_error());
    echo json_encode(array("status" => "ko", "message" => mysql_error()));
  }
  if (count($warnings)) {
    error_log("Warnings : ".print_r($warnings,true));
  }
?>