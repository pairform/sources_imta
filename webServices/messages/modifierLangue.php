<?php

  //********************************************//
  //*********** modifierLangue.php ***********//
  //********************************************//
  
  /* Modifier la langue d'un message.
   *
   * Paramètres :
   * langue (string) - id de la langue du message
   * id_message (int) - id du messages
   * 
   *
   * Retour : 
   * {"status":"ok"
   * }
   * 
   */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $os = $_POST['os'];
  $version = $_POST['version'];
  $id_langue = isset($_POST['langue']) ? $_POST['langue'] : false;
  $id_message = isset($_POST['id_message']) ? $_POST['id_message'] : false;
  $entity_guid = $_POST['id_message'];
  $entity = get_entity($entity_guid);

  try {
      $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $dbh->exec("SET CHARACTER SET utf8");
          $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

    } catch (Exception $e) {
      error_log($e->getMessage());
  }

  if ($id_langue) {
    $id_langue = intval($id_langue);
    $id_message = intval($id_message);
    $update_msg = $dbh->prepare("UPDATE cape_messages SET langue = :id_langue WHERE id_message = :id_message");
    $update_msg->execute(array(":id_langue" => $id_langue, ":id_message" => $id_message));

    $entity->time_updated_meta = time();
    $entity->save();

    if(!$update_msg)
    {
      echo json_encode(array('statut' => 'ko', 'message' => 'Erreur de changement de langue du message ' .$id_message));
      error_log('Erreur de changement de langue du message ' .$id_message);  
    }
    else {
      echo json_encode(array('status' => "ok"));
    }
  }

?>