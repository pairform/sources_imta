<?php

  //*********************************************************************************//
  //******** Recherche la date de la dernière mise à jour d'une ressource ***********//
  //*********************************************************************************//
  /*
  * paramétre :
  * 
  * id_ressource (int) - id de la ressource concernée
  *
  * Retour :
  * 
  * {
  *   "date_update" : 123456789 (timestamp)
  * }
  *
  */
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

	$os = $_POST['os'];
  $version = $_POST['version'];
  $id_capsule = mysql_real_escape_string($_POST['id_ressource']);
	// error_log(print_r($_POST,true));
  
  $query = sprintf("SELECT 
    `date_update`
    FROM `cape_capsules` 
    WHERE 
    `id_capsule` = $id_capsule");   
                 
  $resultQuery = mysql_query($query);
  if(!$resultQuery)
  {
    error_log(mysql_error());
    exit();
  }
  $row = mysql_fetch_assoc($resultQuery);
  // error_log(print_r($row, true));

  $date_update = $row['date_update'];

  $return = json_encode(array("date_update" => $date_update));

  print($return);
?>