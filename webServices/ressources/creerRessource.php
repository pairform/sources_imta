<?php

	class PseudoCrypt {
	 
	    /* Key: Next prime greater than 62 ^ n / 1.618033988749894848 */
	    /* Value: modular multiplicative inverse */
	    private static $golden_primes = array(
	        '1'                  => '1',
	        '41'                 => '59',
	        '2377'               => '1677',
	        '147299'             => '187507',
	        '9132313'            => '5952585',
	        '566201239'          => '643566407',
	        '35104476161'        => '22071637057',
	        '2176477521929'      => '294289236153',
	        '134941606358731'    => '88879354792675',
	        '8366379594239857'   => '7275288500431249',
	        '518715534842869223' => '280042546585394647'
	    );
	 
	    /* Ascii :                    0  9,         A  Z,         a  z     */
	    /* $chars = array_merge(range(48,57), range(65,90), range(97,122)) */
	    private static $chars62 = array(
	        0=>48,1=>49,2=>50,3=>51,4=>52,5=>53,6=>54,7=>55,8=>56,9=>57,10=>65,
	        11=>66,12=>67,13=>68,14=>69,15=>70,16=>71,17=>72,18=>73,19=>74,20=>75,
	        21=>76,22=>77,23=>78,24=>79,25=>80,26=>81,27=>82,28=>83,29=>84,30=>85,
	        31=>86,32=>87,33=>88,34=>89,35=>90,36=>97,37=>98,38=>99,39=>100,40=>101,
	        41=>102,42=>103,43=>104,44=>105,45=>106,46=>107,47=>108,48=>109,49=>110,
	        50=>111,51=>112,52=>113,53=>114,54=>115,55=>116,56=>117,57=>118,58=>119,
	        59=>120,60=>121,61=>122
	    );
	 
	    public static function base62($int) {
	        $key = "";
	        while(bccomp($int, 0) > 0) {
	            $mod = bcmod($int, 62);
	            $key .= chr(self::$chars62[$mod]);
	            $int = bcdiv($int, 62);
	        }
	        return strrev($key);
	    }
	 
	    public static function hash($num, $len = 5) {
	        $ceil = bcpow(62, $len);
	        $primes = array_keys(self::$golden_primes);
	        $prime = $primes[$len];
	        $dec = bcmod(bcmul($num, $prime), $ceil);
	        $hash = self::base62($dec);
	        $ret = str_pad($hash, $len, "0", STR_PAD_LEFT);
	        return (strlen($ret) > 6) ? substr($ret, -6) : $ret;
	    }
	 
	    public static function unbase62($key) {
	        $int = 0;
	        foreach(str_split(strrev($key)) as $i => $char) {
	            $dec = array_search(ord($char), self::$chars62);
	            $int = bcadd(bcmul($dec, bcpow(62, $i)), $int);
	        }
	        return $int;
	    }
	 
	    public static function unhash($hash) {
	        $len = strlen($hash);
	        $ceil = bcpow(62, $len);
	        $mmiprimes = array_values(self::$golden_primes);
	        $mmi = $mmiprimes[$len];
	        $num = self::unbase62($hash);
	        $dec = bcmod(bcmul($num, $mmi), $ceil);
	        return $dec;
	    }
	 
	
	}
	/************************************************/
	/************** creerRessource.php **************/
	/************************************************/

	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
    
	try 
	{
		try {
			$dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');	
  	      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  	      $dbh->exec("SET CHARACTER SET utf8");
  	      $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

		} catch (Exception $e) {
			error_log($e->getMessage());
		}
	    

		// $id_capsule = isset($_POST['id_ressource']) ? $dbh->quote($_POST['id_ressource'];)
		$hash_capsule = isset($_POST['hash_capsule']) ? $_POST['hash_capsule'] : '';

		//Si l'utilisateur demande les paramètres liés à la ressource
		if (isset($_POST['get_params']) && $hash_capsule) {
			$query = $dbh->prepare("SELECT 
				visible,
				id_capsule,
				id_ressource,
				nom_court,
				nom_long,
				url,
				url_web,
				taille,
				description,
				icone_blob,
				etablissement,
				theme,
				licence,
				auteur,
				date_update,
				date_release
			 FROM cape_capsules WHERE hash_capsule= ?");
			$query->execute(array($hash_capsule));

			if($query->rowCount())
			{
				$row = $query->fetch(PDO::FETCH_ASSOC);
				$row['icone_blob'] = base64_encode($row['icone_blob']);
				die(json_encode(array('status'=>'ok', 'row'=>$row)));

			}
				
			else{
				die(json_encode(array('status'=>'ko', 'message' => "Ce hash n'existe pas")));
			}
		}
		else
		{

			// error_log('Récup du post: '. print_r($_POST,true));

			//Sinon, on continue le script

			$icone_blob = isset($_POST['icone_blob']) ? base64_decode($_POST['icone_blob']) : '';
			$nom_court = isset($_POST['nom_court']) ? $_POST['nom_court'] : '';
			$nom_long = isset($_POST['nom_long']) ? $_POST['nom_long'] : '';
			$description = isset($_POST['description']) ? $_POST['description'] : '';
			$url = isset($_POST['url']) ? $_POST['url'] : '';
			$url_web = isset($_POST['url_web']) ? $_POST['url_web'] : '';
			$taille = isset($_POST['taille']) ? $_POST['taille'] : '';
			$etablissement = isset($_POST['etablissement']) ? $_POST['etablissement'] : '';
			$theme = isset($_POST['theme']) ? $_POST['theme'] : '';
			$ressource = isset($_POST['ressource']) ? $_POST['ressource'] : '';
			$licence = isset($_POST['licence']) ? $_POST['licence'] : '';
			$auteur = isset($_POST['auteur']) ? $_POST['auteur'] : '';
			$visible = isset($_POST['visible']) ? $_POST['visible'] : '';

			$ajoute_par = elgg_get_logged_in_user_guid();
			// $temp_blob = tmpfile();
			
			// //Ecriture dans un fichier temporaire
			// fwrite($temp_blob, $icone_blob);
			// fclose($temp_blob); // this removes the file

			//Si l'utilisateur n'est pas connecté
			if (!$ajoute_par) {
				//On die hard

	    		die (json_encode(array('status' => 'ko','message' => 'Vous n\'êtes pas connecté!')));

			}
			// die (json_encode(array('status' => 'ko','message' => 'Succes')));

			//On vérifie encore que l'utilisateur est autorisé à uploader des ressources
			// $result = $dbh->query("SELECT depositeur FROM cape_utilisateurs WHERE id_elgg =$ajoute_par");
			// $autorisation = $result->fetchColumn();

			// switch ($autorisation) {
			// 	case -1:
	  //   			die (json_encode(array('status' => 'ko','message' => 'Vous n\'êtes pas encore autorisé à déposer des ressources. <br>Veuillez attendre la confirmation par e-mail.')));
			// 		break;
			// 	case 0:
	  //   			die (json_encode(array('status' => 'ko','message' => 'Vous n\'êtes pas autorisé à déposer des ressources. Faites la demande à contact@pairform.fr')));
			// 		break;
			// 	case 1:
			// 		break;
			// 	default:
			// 		die (json_encode(array('status' => 'ko','message' => 'Vous n\'êtes pas autorisé à déposer des ressources. Faites la demande à contact@pairform.fr')));
			// 		break;
			// }

			//On check l'état des variables à ID (theme, etablissement et ressource)
			// error_log('Theme : '.$theme.' ; type : '.gettype($theme));
			// error_log('Etablissement : '.$etablissement.' ; type : '.gettype($etablissement));
			// error_log('Ressource : '.$ressource.' ; type : '.gettype($ressource));

			if ((gettype($theme) == "string") && (strpos($theme, '#') === 0)) {
				$theme = substr($theme, 1);
				//Insert dans la base de donnée de l'élement
				$insert = $dbh->prepare("INSERT INTO cape_themes (nom_long) VALUES (:value)");
				$insert->bindParam(':value', $theme);
				$insert->execute();

				//Récup de l'id thème correspondant
				$theme= $dbh->lastInsertId();
			}

			if ((gettype($etablissement) == "string") && (strpos($etablissement, '#') === 0)) {
				$etablissement = substr($etablissement, 1);
				//Insert dans la base de donnée de l'élement
				$insert = $dbh->prepare("INSERT INTO cape_etablissements (nom_long, nom_court) VALUES (:value,:value_bis)");
				$insert->bindParam(':value', $etablissement);
				$insert->bindParam(':value_bis', $etablissement);
				$insert->execute();

				//Récup de l'id thème correspondant
				$etablissement= $dbh->lastInsertId();
			}

			if ((gettype($ressource) == "string") && (strpos($ressource, '#') === 0)) {
				$ressource = substr($ressource, 1);
				$insert = $dbh->prepare("INSERT INTO cape_ressources (nom, ajoute_par) VALUES (:nom, :ajoute_par)");
				//Insert dans la base de donnée de l'élement
				$insert->bindParam(':nom', $ressource);
				$insert->bindParam(':ajoute_par', $ajoute_par);
				$insert->execute();

				//Récup de l'id thème correspondant
				$ressource= $dbh->lastInsertId();
			}
			// error_log('Après — Theme : '.$theme.' ; type : '.gettype($theme));
			// error_log('Après — Etablissement : '.$etablissement.' ; type : '.gettype($etablissement));
			// error_log('Après — Ressource : '.$ressource.' ; type : '.gettype($ressource));
			//Si on est toujours pas dead() jusqu'ici :
			//Si on a un hash de ressource, on est dans le cas d'une MAJ
			if ($hash_capsule) {
				error_log('Update avec Hash #' . $hash_capsule);

				$date_update = date('Y-m-d');
				$query = $dbh->prepare("UPDATE cape_capsules
					SET 
					 `id_ressource`=:id_ressource,
					 `icone_blob`=:icone_blob,
					 `nom_court`=:nom_court,
					 `nom_long`=:nom_long,
					 `description`=:description,
					 `url`=:url,
					 `url_web`=:url_web,
					 `taille`=:taille,
					 `etablissement`=:etablissement,
					 `theme`=:theme,
					 `licence`=:licence,
					 `auteur`=:auteur,
					 `visible`=:visible,
					 `date_update`=:date_update
					WHERE hash_capsule=:hash_capsule");

				$query->bindParam(':id_ressource', $ressource);
				$query->bindParam(':icone_blob', $icone_blob, PDO::PARAM_LOB);
				$query->bindParam(':nom_court', $nom_court);
				$query->bindParam(':nom_long', $nom_long);
				$query->bindParam(':description', $description);
				$query->bindParam(':url', $url);
				$query->bindParam(':url_web', $url_web);
				$query->bindParam(':taille', $taille);
				$query->bindParam(':etablissement', $etablissement);
				$query->bindParam(':theme', $theme);
				$query->bindParam(':licence', $licence);
				$query->bindParam(':auteur', $auteur);
				$query->bindParam(':visible', $visible);
				// $query->bindParam(':ajoute_par', $ajoute_par);
				$query->bindParam(':date_update', $date_update);
				$query->bindParam(':hash_capsule', $hash_capsule);
				$query->execute();
				
				// mysql_query("UPDATE cape_capsules
				// 		SET 
				// 		 `icone_blob`=$icone_blob
				// 		WHERE hash_capsule=$hash_capsule");

				die (json_encode(array('status' => 'ok')));
			}
			//Sinon, c'est que l'on créé la ressource
			else{
				$date_update = date('Y-m-d');
				$date_release = date('Y-m-d');

				$query = $dbh->query("SHOW TABLE STATUS LIKE 'cape_capsules'");
				$row = $query->fetch();
				// error_log(print_r($row,true));

				$hash_capsule = PseudoCrypt::hash($row['Auto_increment'], 6);

				error_log('Création avec Hash #' . $hash_capsule);

				$query = $dbh->prepare("INSERT INTO cape_capsules 
					(hash_capsule,
					 id_ressource,
					 icone_blob,
					 visible,
					 nom_court,
					 nom_long,
					 url,
					 url_web,
					 taille,
					 description,
					 etablissement,
					 theme,
					 licence,
					 auteur,
					 ajoute_par,
					 date_update,
					 date_release) 
					VALUES (:hash_capsule,
					 :id_ressource,
					 :icone_blob,
					 :visible,
					 :nom_court,
					 :nom_long,
					 :url,
					 :url_web,
					 :taille,
					 :description,
					 :etablissement,
					 :theme,
					 :licence,
					 :auteur,
					 :ajoute_par,
					 :date_update,
					 :date_release)");
				
				$query->bindParam(':hash_capsule', $hash_capsule);
				$query->bindParam(':id_ressource', $ressource);
				$query->bindParam(':icone_blob', $icone_blob, PDO::PARAM_LOB);
				$query->bindParam(':nom_court', $nom_court);
				$query->bindParam(':nom_long', $nom_long);
				$query->bindParam(':description', $description);
				$query->bindParam(':url', $url);
				$query->bindParam(':url_web', $url_web);
				$query->bindParam(':taille', $taille);
				$query->bindParam(':etablissement', $etablissement);
				$query->bindParam(':theme', $theme);
				$query->bindParam(':licence', $licence);
				$query->bindParam(':auteur', $auteur);
				$query->bindParam(':visible', $visible);
				$query->bindParam(':ajoute_par', $ajoute_par);
				$query->bindParam(':date_update', $date_update);
				$query->bindParam(':date_release', $date_release);
				$query->execute();

				$id_capsule = $dbh->lastInsertId();

				//Si ce n'est pas un admin qui ajoute la ressource
				if ($ajoute_par != 35) {
					//On ajoute le niveau admin et le niveau expert de l'ajouteur
					$insert = $dbh->prepare("INSERT IGNORE INTO cape_utilisateurs_categorie VALUES (:id_ressource, 35, 5, 0, 0), (:id_ressource_bis, :ajoute_par, 4, 0, 0)");
					$insert2 = $dbh->prepare("INSERT IGNORE INTO cape_utilisateurs_capsules_parametres VALUES (:id_capsule, 35, 1, 0),(:id_capsule_bis, :ajoute_par, 1, 0)");
					$insert->bindParam(':id_ressource_bis', $ressource);
					$insert2->bindParam(':id_capsule_bis', $id_capsule);
					$insert->bindParam(':ajoute_par', $ajoute_par);
					$insert2->bindParam(':ajoute_par', $ajoute_par);

				}
				else{
					//On n'ajoute que le niveau expert de l'ajouteur
					$insert = $dbh->prepare("INSERT IGNORE INTO cape_utilisateurs_categorie VALUES (:id_ressource, 35, 5, 0, 0)");
					$insert2 = $dbh->prepare("INSERT IGNORE INTO cape_utilisateurs_capsules_parametres VALUES (:id_capsule, 35, 1, 0)");
				}
				
				$insert->bindParam(':id_ressource', $ressource);
				$insert2->bindParam(':id_capsule', $id_capsule);

				$insert->execute();
				$insert2->execute();
				die (json_encode(array('status' => 'code', 'message' => $hash_capsule)));
			}
			// $query->debugDumpParams();
		}
	} 
	catch (PDOException $e) 
	{
	    error_log("Erreur : " . $e->getMessage());
	    die (json_encode(array('status' => 'ko','message' => $e->getMessage())));
	    
	}

?>