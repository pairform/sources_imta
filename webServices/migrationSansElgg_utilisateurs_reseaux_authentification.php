<?php

  set_time_limit(0);
  //********************************************//
  //******** Structure de base de WS ***********//
  //********************************************//
  
  try {
    $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET CHARACTER SET utf8");
    // $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

  } catch (Exception $e) {
    error_log($e->getMessage());
  }
  /*****************************************************************************/
  /*****************************************************************************/
  /*****************************************************************************/
  
  print('Migration des utilisateurs : <br>');

  $create_table_utilisateurs = $dbh->exec("
    SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

    CREATE TABLE IF NOT EXISTS `cape_utilisateurs_v2` (
      `id_utilisateur` bigint(20) NOT NULL auto_increment,
      `etablissement` varchar(250) NOT NULL,
      `email` varchar(200) NOT NULL,
      `nom_prenom` varchar(200) NOT NULL,
      `pseudo` varchar(128) NOT NULL,
      `password` varchar(200) NOT NULL,
      `salt` varchar(200) NOT NULL,
      `avatar_url` varchar(400) NOT NULL default 'public/img/avatar/0.png',
      `token_validation` varchar(200) NOT NULL,
      `token_reset` varchar(200) NOT NULL,
      `token_reset_expiration` int(11) NOT NULL,
      `token_oauth` varchar(200) NOT NULL,
      `token_oauth_expiration` int(11) NOT NULL,
      `email_est_valide` tinyint(1) NOT NULL default '0',
      `est_banni` tinyint(1) NOT NULL default '0',
      `est_admin` tinyint(1) NOT NULL default '0',
      `date_creation` int(11) NOT NULL default '0',
      `date_actif` int(11) NOT NULL default '0',      `os_mobile` varchar(20) NOT NULL,
      `token_mobile` varchar(250) NOT NULL,
      PRIMARY KEY  (`id_utilisateur`),
      UNIQUE KEY `pseudo` (`pseudo`),
      UNIQUE KEY `email` (`email`(50)),
      KEY `password` (`password`),
      KEY `code` (`token_reset`),
      KEY `date_creation` (`date_creation`),
      KEY `date_actif` (`date_actif`),
      KEY `est_admin` (`est_admin`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9724 ;
  ");


/*
SELECT TABLE_NAME, CONSTRAINT_NAME FROM information_schema.table_constraints
WHERE table_schema = 'PairForm_V2'
AND constraint_type='FOREIGN KEY'
AND (CONSTRAINT_NAME LIKE '%users%' OR CONSTRAINT_NAME LIKE '%utilisateur%');
*/


  $sql_insert_utilisateurs = "INSERT INTO cape_utilisateurs_v2
    (`id_utilisateur`,
    `etablissement`,
    `email`,
    `nom_prenom`,
    `pseudo`,
    `password`,
    `salt`,
    `email_est_valide`,
    `token_oauth`,
    `date_creation`,
    `date_actif`,
    `token_mobile`)
    SELECT 
      guid AS 'id_utilisateur',
      etablissement,
      email,
      name AS 'nom_prenom',
      username AS 'pseudo',
      password,
      salt,
      1,
      code AS 'token_oauth',
      last_login AS 'date_creation',
      last_action AS 'date_actif',
      device_token AS 'token_mobile'
    FROM `cape_utilisateurs` 
    JOIN sce_users_entity ON guid = id_utilisateur
  ";

  //Insertion de tous les utilisateurs
  $dbh->query($sql_insert_utilisateurs);

  print("Recuperation des avatars des utilisateurs...");

  $utilisateurs = $dbh->query("SELECT DISTINCT id_utilisateur, pseudo FROM cape_utilisateurs_v2");
  
  while ($row = $utilisateurs->fetch()) {
    $pseudo = $row["pseudo"];
    $id_utilisateur = $row["id_utilisateur"];
    $url = "https://podcast.mines-nantes.fr/PairForm_Elgg/avatar/view/".$pseudo."/medium/0";
    $fp = fopen(dirname(__FILE__) .'/avatars/'. $id_utilisateur .'.png', 'w+');

    $ch = curl_init(str_replace(" ","%20",$url));
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
    curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
  }

  print('Migration des reseaux : <br>');
  $create_table_reseaux = $dbh->exec("

    SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

    CREATE TABLE IF NOT EXISTS `cape_reseaux` (
      `id_reseau` BIGINT(20) NOT NULL auto_increment,
      `id_utilisateur` BIGINT(20) NOT NULL,
      `nom` varchar(200) NOT NULL,
      `cle` varchar(200) NOT NULL,
      PRIMARY KEY  (`id_reseau`, `id_utilisateur`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cercles et classes, et leur métadonnées' AUTO_INCREMENT=1 ;

     
  ");
  $create_table_reseaux_constraints = $dbh->exec("
    ALTER TABLE  `cape_reseaux` ADD FOREIGN KEY (  `id_utilisateur` ) REFERENCES  `PairForm_V2`.`cape_utilisateurs` (`id_utilisateur` ) ON DELETE CASCADE
  ");

  $create_table_reseaux_utilisateurs = $dbh->exec("
  
    SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

    CREATE TABLE IF NOT EXISTS `cape_reseaux_utilisateurs` (
      `id_reseau` BIGINT(20) NOT NULL,
      `id_utilisateur` BIGINT(20) NOT NULL,
      PRIMARY KEY  (`id_reseau`,`id_utilisateur`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table croisée des utilisateurs et de leurs cercles';
    ");
$create_table_reseaux_utilisateurs_constraints = $dbh->exec("
    ALTER TABLE `cape_reseaux_utilisateurs` ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_1` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE;
  ");

  
  /*****************************************************************************/
  /*****************************************************************************/
  /*****************************************************************************/

  print('Phase 1 - metadonnées 1-to-1 <br>');
        
  $sql_query_reseaux = "SELECT 
      id,
      owner_guid,
      name,
      IFNULL(cle, '') AS 'cle'
    FROM  `sce_access_collections` 
    LEFT JOIN `cape_classes` cc ON cc.id_classe = id
    LEFT JOIN `cape_utilisateurs` cu ON cu.id_utilisateur = owner_guid
    WHERE owner_guid != 0
    AND cu.id_utilisateur IS NOT NULL";

  $query_cr = $dbh->query($sql_query_reseaux);
  //Préparation de l'update
  $insert_cr = $dbh->prepare('INSERT INTO cape_reseaux (id_reseau, id_utilisateur, nom, cle) VALUES 
      (:id_reseau, :id_utilisateur, :nom, :cle)');

  //Pour chaque reseau
  while ($reseau = $query_cr->fetch()) {

    $resultat = $insert_cr->execute(array(
      ":id_reseau" => $reseau["id"],
      ":id_utilisateur" => $reseau["owner_guid"],
      ":nom" => $reseau["name"],
      ":cle" => $reseau["cle"]
    ));
    
    print("<br>Reseau inséré #{$reseau["id"]} : " .($resultat ? "mis à jour" : "erreur"));

    error_log("Reseau inséré #{$reseau["id"]} : " .($resultat ? "mis à jour" : "erreur"));
  }

  /*****************************************************************************/
  /*****************************************************************************/
  /*****************************************************************************/
  
  print('Phase 2 - metadonnées n-to-n <br>');

  $sql_query_utilisateurs = "SELECT 
      sacm.access_collection_id,
      sacm.user_guid 
    FROM  `sce_access_collection_membership` sacm
    LEFT JOIN `cape_utilisateurs` cu ON cu.id_utilisateur = sacm.user_guid
    LEFT JOIN `cape_reseaux` cr ON cr.id_reseau = sacm.access_collection_id
    WHERE cu.id_utilisateur IS NOT NULL
    AND cr.id_reseau IS NOT NULL";

  //Récupération de toutes les reseaux
  $query_utilisateurs = $dbh->query($sql_query_utilisateurs);
  //Préparation de l'update
  $insert_cru = $dbh->prepare('INSERT INTO cape_reseaux_utilisateurs 
      VALUES 
      (:id_reseau, :id_utilisateur)');

  //Pour chaque message
  while ($reseau_utilisateur = $query_utilisateurs->fetch()) {

    $resultat = $insert_cru->execute(array(
      "id_reseau" => $reseau_utilisateur["access_collection_id"],
      "id_utilisateur" => $reseau_utilisateur["user_guid"]
    ));
    
    print("<br>Utilisateur #{$reseau_utilisateur["user_guid"]} ajoute dans reseau {$reseau_utilisateur["access_collection_id"]} : " .($resultat ? "mis à jour" : "erreur"));

    error_log("Utilisateur #{$reseau_utilisateur["user_guid"]} ajoute dans reseau {$reseau_utilisateur["access_collection_id"]} : " .($resultat ? "mis à jour" : "erreur"));
  }

  print('Création des tables LTI  : <br>');
  print('Phase 1 - source authentiication <br>');
  $create_table_source_authentification = $dbh->exec("

    CREATE TABLE IF NOT EXISTS `cape_sources_authentification` (
      `id_source_auth` int(20) NOT NULL auto_increment,
      `id_source_auth_client` varchar(200) NOT NULL,
      `context_id` varchar(200) NOT NULL,
      `context_title` varchar(400) NOT NULL,
      `nom_client` varchar(200) NOT NULL,
      `mail_administrateur` varchar(200) NOT NULL,
      `est_autorise` int(1) NOT NULL default '0',
      `oauth_consumer_key` varchar(200) NOT NULL,
      `oauth_consumer_secret` varchar(200) NOT NULL,
      `date_creation` int(11) NOT NULL,
      PRIMARY KEY  (`id_source_auth`),
      UNIQUE KEY `oauth_consumer_key` (`oauth_consumer_key`),
      UNIQUE KEY `id_source_auth_client` (`id_source_auth_client`,`context_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;
     
  ");
  $insert_table_source_authentification = $dbh->exec("
    
    INSERT INTO `cape_sources_authentification` (`id_source_auth`, `id_source_auth_client`, `context_id`, `context_title`, `nom_client`, `mail_administrateur`, `est_autorise`, `oauth_consumer_key`, `oauth_consumer_secret`, `date_creation`) VALUES
    (6, '7db438071375c02373713c12c73869ff2f470b68.canvas.instructure.com', 'b8a872e1919d01fa18c58a4b6707f5ab5c74ba92', 'SANDBOX-COPY impAct de la décision sur la santé et la sécurité au travail ', 'canvas', 'maen.juganaikloo@mines-nantes.fr', 1, 'MoocImpact', 'impact2015', 1429710589),
    (7, 'campusneo.mines-nantes.fr', '9', 'Bac à sable', 'moodle', 'Maen.JUGANAIKLOO@mines-nantes.fr', 1, 'Moodle', 'mootmoot', 1429710608),
    (8, '', 'Institut-Mines-Telecom/IMT-SST/Novembre-2014', '', '', 'mjuganaikloo+fun@gmail.com', 1, 'test_fun', 'secret', 1429792749);

  ");

  print('Phase 2 - insertions utilisateurs autorisés <br>');
  $create_table_source_authentification_utilisateurs = $dbh->exec("
    CREATE TABLE IF NOT EXISTS `cape_sources_authentification_utilisateurs` (
      `id_log` int(20) NOT NULL auto_increment,
      `id_utilisateur` int(20) NOT NULL,
      `id_source_auth` int(20) NOT NULL,
      `est_valide` int(1) NOT NULL default '0',
      `date_connexion` int(11) NOT NULL,
      PRIMARY KEY  (`id_log`,`id_utilisateur`),
      UNIQUE KEY `id_utilisateur` (`id_utilisateur`,`id_source_auth`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Log des connexions LTI & autorisation utilisation' AUTO_INCREMENT=8 ;
  ");

  print('Changement dans la table cape_messages : "latitude" au lieu de "lattitude" <br>');
  $dbh->exec("
    ALTER TABLE  `cape_messages` CHANGE  `geo_lattitude`  `geo_latitude` VARCHAR( 80 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
  ");
?>