<?php

  //********************************************//
  //*********** afficherListeDeProfils.php ***********//
  //********************************************//

  /* Affiche la liste des utilisateurs.
   *
   * Paramètres :
   * (Optionel) id_ressource(int) - l'id de la ressource pour filtrer les profils.
   *
   * 
   *
   *
        "profils":[
                  {
                  "name":"Colin",
                  "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/_graphics/icons/user/defaultlarge.gif"
                  },
                  {
                  "name":"sc_bot",
                  "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/_graphics/icons/user/defaultlarge.gif"
                  }
                ]
        }
   * 
   */

	include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

	$os = $_POST['os'];
	$id_ressource = mysql_real_escape_string($_POST['id_ressource']);

	//Pour le web, on veut que l
	if ($os == "ios" || $os == "and") 
	{
		$opt = array('types' => 'user' ,'limit' => 0);
		$array = elgg_get_entities($opt);
		foreach ($array as &$value) {

			$otherGuid = $value->getGUID();

			// Le filtre par ressource
			if ( !empty($id_ressource)){
				$result = mysql_query("SELECT * FROM `cape_utilisateurs_categorie` 
					WHERE `id_utilisateur` = $otherGuid
					AND `id_ressource` = (SELECT id_ressource FROM cape_capsules WHERE id_capsule=$id_ressource)");
				if (!$result || !mysql_fetch_assoc($result) ) continue;
			}

			// Commentaires pour la fonction d'affichage des membres qui ont une ressource en commun avec soi.
			/*$result = mysql_query("SELECT * FROM `cape_utilisateurs_categorie` 
			WHERE `id_utilisateur` = $myGuid 
			AND (`id_ressource` 
			IN 
			(SELECT `id_ressource` 
			FROM `cape_utilisateurs_categorie`
			WHERE `id_utilisateur` = $otherGuid))");

			if (!$result) continue;*/

			if ($os == 'and') {
				$object[] = (object) array('name' => $value->__get('name'), 'username' => $value->__get('username'), 'image' => $value->getIconURL('medium'), 'id_utilisateur' => $value->getGUID(), 'langue_utilisateur' => $code_langue);

			} else {
				$object[] = (object) array('name' => $value->__get('username'), 'image' => $value->getIconURL('medium'), 'id_utilisateur' => $value->getGUID());
			}
		}

	}
	else{
	
		$result = mysql_query("SELECT u.guid, u.username, c.etablissement FROM sce_users_entity u, cape_utilisateurs c WHERE c.id_utilisateur = u.guid");
		
			
		
		while ($userMatched = mysql_fetch_assoc($result)) {
			
			$userEntity = get_user(intval($userMatched['guid']));


			if (gettype($userEntity) == "object") {		

				$last_action = $userEntity->get("last_action");
				$minutes_before_last_action = (time() - $last_action) / 60;

				//Si ça fait moins de dix minutes qu'il a fait quoi que ce soit
				if ($minutes_before_last_action <= 10) 
					$offline = false;
				else
					$offline = $minutes_before_last_action;	
				// error_log(print_r($userEntity,true));
				// error_log($userEntity->getIconURL('medium'));
				$object[] = (object) array('username' => $userMatched['username'], 'image' => $userEntity->getIconURL('medium'), 'offline' => $offline,'id_utilisateur' => $userMatched['guid'], 'etablissement' => $userMatched['etablissement']);	
			}
		}
		//On créé un objet vide pour éviter une valeur null
		if (empty($object)) {
			$object = array();
		}
	}
	$return = json_encode( (object) array('profils' => $object) );
	print $return;
  // Commentaires pour la fonction d'affichage des membres qui ont une ressource en commun avec soi.
  /*if (!empty($filtre_ressource) && !in_array($message_id_ressource, $filtre_ressource))  {
     return null;
 }*/

    /*$myGuid = elgg_get_logged_in_user_guid();
    if(!$myGuid){
      $return = json_encode( (object) array('profils' => array() ) );
      print $return;
      exit();
  }*/




?>