<?php

  //********************************************//
  //******** ChangerNomsReels.php  *************//
  //********************************************//
  
  /* 
   * Permet d'affichager les noms et prénoms des utilisateurs à la place des pseudo, et vice versa  
   *
   *
   * Paramètres :
   * id_utilisateur (int) - l'id de l'utilisateur
   *
   *
   * retour JSON : 
   * {
   *  'status':"ko", "message" : "message d'erreur"
   * }
   *
   * Mascotte pour PairForm ?
   *  
   *       ()_()
   *       (o.o)        @
   *      /('"')\     @@ @@
   *--------"-"------@  @  @
   *-----------------@@   @
   *                @  @@
   *
   * Très bonne idée.
                                          .....'',;;::cccllllllllllllcccc:::;;,,,''...'',,'..
                            ..';cldkO00KXNNNNXXXKK000OOkkkkkxxxxxddoooddddddxxxxkkkkOO0XXKx:.
                      .':ok0KXXXNXK0kxolc:;;,,,,,,,,,,,;;,,,''''''',,''..              .'lOXKd'
                 .,lx00Oxl:,'............''''''...................    ...,;;'.             .oKXd.
              .ckKKkc'...'',:::;,'.........'',;;::::;,'..........'',;;;,'.. .';;'.           'kNKc.
           .:kXXk:.    ..       ..................          .............,:c:'...;:'.         .dNNx.
          :0NKd,          .....''',,,,''..               ',...........',,,'',,::,...,,.        .dNNx.
         .xXd.         .:;'..         ..,'             .;,.               ...,,'';;'. ...       .oNNo
         .0K.         .;.              ;'              ';                      .'...'.           .oXX:
        .oNO.         .                 ,.              .     ..',::ccc:;,..     ..                lXX:
       .dNX:               ......       ;.                'cxOKK0OXWWWWWWWNX0kc.                    :KXd.
     .l0N0;             ;d0KKKKKXK0ko:...              .l0X0xc,...lXWWWWWWWWKO0Kx'                   ,ONKo.
   .lKNKl...'......'. .dXWN0kkk0NWWWWWN0o.            :KN0;.  .,cokXWWNNNNWNKkxONK: .,:c:.      .';;;;:lk0XXx;
  :KN0l';ll:'.         .,:lodxxkO00KXNWWWX000k.       oXNx;:okKX0kdl:::;'',;coxkkd, ...'. ...'''.......',:lxKO:.
 oNNk,;c,'',.                      ...;xNNOc,.         ,d0X0xc,.     .dOd,           ..;dOKXK00000Ox:.   ..''dKO,
'KW0,:,.,:..,oxkkkdl;'.                'KK'              ..           .dXX0o:'....,:oOXNN0d;.'. ..,lOKd.   .. ;KXl.
;XNd,;  ;. l00kxoooxKXKx:..ld:         ;KK'                             .:dkO000000Okxl;.   c0;      :KK;   .  ;XXc
'XXdc.  :. ..    '' 'kNNNKKKk,      .,dKNO.                                   ....       .'c0NO'      :X0.  ,.  xN0.
.kNOc'  ,.      .00. ..''...      .l0X0d;.             'dOkxo;...                    .;okKXK0KNXx;.   .0X:  ,.  lNX'
 ,KKdl  .c,    .dNK,            .;xXWKc.                .;:coOXO,,'.......       .,lx0XXOo;...oNWNXKk:.'KX;  '   dNX.
  :XXkc'....  .dNWXl        .';l0NXNKl.          ,lxkkkxo' .cK0.          ..;lx0XNX0xc.     ,0Nx'.','.kXo  .,  ,KNx.
   cXXd,,;:, .oXWNNKo'    .'..  .'.'dKk;        .cooollox;.xXXl     ..,cdOKXXX00NXc.      'oKWK'     ;k:  .l. ,0Nk.
    cXNx.  . ,KWX0NNNXOl'.           .o0Ooldk;            .:c;.':lxOKKK0xo:,.. ;XX:   .,lOXWWXd.      . .':,.lKXd.
     lXNo    cXWWWXooNWNXKko;'..       .lk0x;       ...,:ldk0KXNNOo:,..       ,OWNOxO0KXXNWNO,        ....'l0Xk,
     .dNK.   oNWWNo.cXK;;oOXNNXK0kxdolllllooooddxk00KKKK0kdoc:c0No        .'ckXWWWNXkc,;kNKl.          .,kXXk,
      'KXc  .dNWWX;.xNk.  .kNO::lodxkOXWN0OkxdlcxNKl,..        oN0'..,:ox0XNWWNNWXo.  ,ONO'           .o0Xk;
      .ONo    oNWWN0xXWK, .oNKc       .ONx.      ;X0.          .:XNKKNNWWWWNKkl;kNk. .cKXo.           .ON0;
      .xNd   cNWWWWWWWWKOkKNXxl:,'...;0Xo'.....'lXK;...',:lxk0KNWWWWNNKOd:..   lXKclON0:            .xNk.
      .dXd   ;XWWWWWWWWWWWWWWWWWWNNNNNWWNNNNNNNNNWWNNNNNNWWWWWNXKNNk;..        .dNWWXd.             cXO.
      .xXo   .ONWNWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWNNK0ko:'..OXo          'l0NXx,              :KK,
      .OXc    :XNk0NWXKNWWWWWWWWWWWWWWWWWWWWWNNNX00NNx:'..       lXKc.     'lONN0l.              .oXK:
      .KX;    .dNKoON0;lXNkcld0NXo::cd0NNO:;,,'.. .0Xc            lXXo..'l0NNKd,.              .c0Nk,
      :XK.     .xNX0NKc.cXXl  ;KXl    .dN0.       .0No            .xNXOKNXOo,.               .l0Xk;.
     .dXk.      .lKWN0d::OWK;  lXXc    .OX:       .ONx.     . .,cdk0XNXOd;.   .'''....;c:'..;xKXx,
     .0No         .:dOKNNNWNKOxkXWXo:,,;ONk;,,,,,;c0NXOxxkO0XXNXKOdc,.  ..;::,...;lol;..:xKXOl.
     ,XX:             ..';cldxkOO0KKKXXXXXXXXXXKKKKK00Okxdol:;'..   .';::,..':llc,..'lkKXkc.
     :NX'    .     ''            ..................             .,;:;,',;ccc;'..'lkKX0d;.
     lNK.   .;      ,lc,.         ................        ..,,;;;;;;:::,....,lkKX0d:.
    .oN0.    .'.      .;ccc;,'....              ....'',;;;;;;;;;;'..   .;oOXX0d:.
    .dN0.      .;;,..       ....                ..''''''''....     .:dOKKko;.
     lNK'         ..,;::;;,'.........................           .;d0X0kc'.
     .xXO'                                                 .;oOK0x:.
      .cKKo.                                    .,:oxkkkxk0K0xc'.
        .oKKkc,.                         .';cok0XNNNX0Oxoc,.
          .;d0XX0kdlc:;,,,',,,;;:clodkO0KK0Okdl:,'..
              .,coxO0KXXXXXXXKK0OOxdoc:,..
                        ...
   */

  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  //Connexion à la base de données
  try {
    $dbh = new PDO('mysql:host=localhost;dbname=PairForm_V2', 'root', 'So6son7');  
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET CHARACTER SET utf8");
    // $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
  } catch (Exception $e) {
    error_log($e->getMessage());
  }

	$os = $_POST['os'];
  $version = $_POST['version'];

  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
  }
  // if ($os == 'and') {
    //On reverifie le statut minimum d'expert de l'utilisateur pour utiliser cette foncionnalité
    $query_is_expert = $dbh->prepare("SELECT COUNT(*) FROM `cape_utilisateurs_categorie` WHERE `id_utilisateur`= :id_utilisateur AND `id_categorie` >= 4");
    $query_is_expert->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_INT);
    $query_is_expert->execute();

  // } else {
  //   $id_ressource =$_POST['id_ressource'];

  //   //On reverifie le statut minimum d'expert de l'utilisateur pour utiliser cette foncionnalité
  //   $query_is_expert = $dbh->prepare("SELECT COUNT(*) FROM `cape_utilisateurs_categorie` WHERE `id_utilisateur`= :id_utilisateur AND `id_ressource` = (SELECT id_ressource FROM cape_capsules WHERE id_capsule=:id_capsule) AND `id_categorie` >= 4");
  //   $query_is_expert->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_INT);
  //   $query_is_expert->bindParam(':id_capsule', $id_ressource, PDO::PARAM_INT);
  //   $query_is_expert->execute();
  // }

  //Bool
  $is_expert = $query_is_expert->fetchColumn();

  if ($is_expert) {
    // if ($os == 'and') {
      $query_update = $dbh->prepare("UPDATE cape_utilisateurs_capsules_parametres as cucp
        LEFT JOIN cape_capsules as cc ON cucp.id_capsule= cc.id_capsule 
        LEFT JOIN cape_utilisateurs_categorie as cuc ON cc.id_ressource= cuc.id_ressource
        SET cucp.afficher_noms_reels = IF (cucp.afficher_noms_reels, 0, 1) 
        WHERE cucp.id_utilisateur = :id_utilisateur AND cuc.id_utilisateur = :id_utilisateur AND cuc.id_categorie >= 4");
      $query_update->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_INT);
      $worked = $query_update->execute();
    // } else {
    //   $query_update = $dbh->prepare("UPDATE `cape_utilisateurs_capsules_parametres` SET `afficher_noms_reels` = IF (`afficher_noms_reels`, 0, 1) WHERE `id_utilisateur`= :id_utilisateur AND `id_capsule` = :id_capsule");
    //   $query_update->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_INT);
    //   $query_update->bindParam(':id_capsule', $id_ressource, PDO::PARAM_INT);
    //   $worked = $query_update->execute();
    // }

    if ($worked) {
       echo json_encode(array('status' => 'ok'));
    }
    else{
      echo json_encode(array('status' => 'ko', 'message' => 'Erreur de mise à jour de statut : veuillez réessayer.'));
    }
  }
  else{
    echo json_encode(array('status' => 'ko', 'message' => 'Vous n\'avez pas un rang assez élevé pour utiliser cette fonctionnalité.'));
  }
?>