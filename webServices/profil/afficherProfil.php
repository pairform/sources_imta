<?php

  //********************************************//
  //*********** afficherProfil.php ***********//
  //********************************************//
  
  /* Affiche le profil d'un membre.
   *
   * Paramètres :
   * id_utilisateur(int) - l'id de l'utilisateur , s'il est absent , prend l'utilisateur connecté
   *
   * 
   * Retour JSON
   *
      {
      "username":"Luke Skywalker",
      "name":"LukeSK",
      "offline":false || minutes, // Minutes depuis dernière activité
      "image":"http://imedia.emn.fr/SCElgg/elgg-1.8.13/mod/profile/icondirect.php?lastcache=1363189869&joindate=1363095006&guid=74&size=medium",
      "email":"email@email.com",
      "etablissement" : "EMN",
      "res":[
              {
              "id_ressource":"123"
              "nom":"E-diagnostic",
              "score":"4",
              "max_score":"1200",
              "categorie":"Participant"
              }
          ],
      "nombreSucces":2,
      "succes":[
                {
                "succes":"Participant",
                "description":"Devenir participant",
                "image":"1.jpg"
                },
                {
                "succes":"SuperCaster 100+",
                "description":"Obtenir plus de 100 points",
                "image":"2.jpg"
                }
      ],
      "succesNonGagnes":[
                {
                "succes":"Petite plume",
                "description":"Ecrire 5 messages",
                "image":"succes_default"
                }
      ],
      "succesDerniers":[
                {
                "image":"image.jpg"
                }
      ],
      "isOwner":"yes"
      }
   * 
   */
  
 
  
  include_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

  $os = $_POST['os'];
  $version = $_POST['version'];
  $langue_app = isset($_POST['langue_app']) ? mysql_real_escape_string($_POST['langue_app']) : 3;


  if(isset($_POST['id_utilisateur'])){
    $id_utilisateur = $_POST['id_utilisateur'];
    $utilisateur = get_user($id_utilisateur);

  }
  else{
    $id_utilisateur = elgg_get_logged_in_user_guid();
    $utilisateur = elgg_get_logged_in_user_entity();
  }

  $isOwner = "A quoi sert cette variable?";

  error_log("GET : " . print_r($_GET, true));
  error_log("Post : " . print_r($_POST, true));
  error_log("REQUEST : " . print_r($_REQUEST, true));

  $id_utilisateur = $utilisateur->guid;
	$name = $utilisateur->username;
	$image = $utilisateur->getIconURL('medium');
  $email = $utilisateur->get("email"); 
  //Ajout flag online
  $last_action = $utilisateur->get("last_action");
  $minutes_before_last_action = (time() - $last_action) / 60;

  //Si ça fait moins de dix minutes qu'il a fait quoi que ce soit
  if ($minutes_before_last_action <= 10) {
    $offline = false;
  }
  else
    $offline = $minutes_before_last_action;


  // initialisation des variables tableaux
  if ($os == 'and') {
    $arrayRes = array();
    $arraySucces = array();
  }


  //######
  // Affichage des noms réels 
  //######

function userWantsRealNameForCapsules ($id_utilisateur = null)
  {
    //Si pas d'utilisateur specifié
    if ($id_utilisateur === null) {
      $id_utilisateur = elgg_get_logged_in_user_guid();
      
      //Si ya pas d'utilisateur connecté
      if (!$id_utilisateur) {
        //On quitte, ça sert à rien
        return false;
      }
    }

    $query = mysql_query("SELECT `id_capsule` FROM `cape_utilisateurs_capsules_parametres` WHERE `afficher_noms_reels` = 1 AND `id_utilisateur` = $id_utilisateur");

    while ($row = mysql_fetch_assoc($query)) {
      $array_capsules[$row['id_capsule']] = true;
    }

    //Retour d'un tableau avec tous les ids de capsules dont l'utilisateur veut voir les prénoms des utilisateurs.
    return $array_capsules;
  }

  //Si l'utilisateur actuel souhaite utiliser les vrais noms 
  $array_capsules_noms_reels = userWantsRealNameForCapsules($id_utilisateur);
  //Affichage des ressources
  $flag_expert_ressource = false;

	//#####
  // DEBUT REQUETE RES
  //######
  $result = mysql_query(
    "SELECT  `ccl`.`id_capsule`,
      `ccl`.`visible`,
      `ccl`.`url_web`,
      `ccl`.`icone_blob`,
      `ccl`.`nom_court`,
      `cc`.`nom`,
      `cc`.`id_categorie`,
      `score`,
      `cuc`.`id_categorie_temp`
    FROM `cape_utilisateurs_categorie` AS `cuc`,
     `cape_utilisateurs_capsules_parametres` AS `cucp`,
     `cape_capsules` AS `ccl`,
     `cape_categories` AS `cc`
    WHERE `cuc`.`id_utilisateur` = $id_utilisateur
    AND `ccl`.`id_capsule` = `cucp`.`id_capsule`
    AND `cuc`.`id_ressource` = `ccl`.`id_ressource`
    AND `cucp`.`id_utilisateur` = '$id_utilisateur'
    AND  
      ((`cuc`.`id_categorie_temp` > `cuc`.`id_categorie` AND `cc`.`id_categorie` = `cuc`.`id_categorie_temp`)
      OR (`cuc`.`id_categorie_temp` <= `cuc`.`id_categorie` AND `cc`.`id_categorie` = `cuc`.`id_categorie` )
      )
  ORDER BY `score` DESC
  ");

  if(!$result)
      error_log(mysql_error());

  //Pour chaque ressouces
  while($row_message_sc = mysql_fetch_assoc($result))
  {
    if ($os == 'web') 
      $arrayRes[] = (object) array('id_ressource' => $row_message_sc['id_capsule'],'url_web' => $row_message_sc['url_web'],'icone_blob' => base64_encode($row_message_sc['icone_blob']),'nom' => $row_message_sc['nom_court'],'score' => $row_message_sc['score'],'max_score' => 1200,'categorie' => $row_message_sc['nom'],'id_categorie' => $row_message_sc['id_categorie'], 'visible' => $row_message_sc['visible']);
    
    else
      $arrayRes[] = (object) array('id_ressource' => $row_message_sc['id_capsule'],'nom' => $row_message_sc['nom_court'],'score' => $row_message_sc['score'],'max_score' => 1200,'categorie' => $row_message_sc['nom'],'id_categorie' => $row_message_sc['id_categorie']);
    
    //Si l'utilisateur possède une ressource, pour laquelle l'utilisateur actuelle détient le rôle d'expert & qu'il veut afficher les noms réels
    if (!$flag_expert_ressource && $array_capsules_noms_reels) {
      if (array_key_exists($row_message_sc['id_capsule'], $array_capsules_noms_reels)) {
       $flag_expert_ressource = true;
      }
     } 
  }
	

  // Si l'utilisateur veut afficher les noms réels, on affiche le nom à la place du pseudo dans le profil
  if ($flag_expert_ressource) {
    $name = $utilisateur->name;
  }

	
  //#####
  // FIN REQUETE RES
  //######


  // Affichage des succès gagnés dans l'ordre logique.

  //#####
  // DEBUT REQUETE succes
  //######


  $result = mysql_query("SELECT `cape_succes_langues`.`nom`, `cape_succes_langues`.`description`, `cape_succes`.`image_blob`, `cape_succes`.`image` 
  FROM `cape_succes`,`cape_utilisateurs_succes`,`cape_succes_langues` 
  WHERE `cape_succes`.`id_succes` = `cape_utilisateurs_succes`.`id_succes` 
  AND `cape_succes`.`id_succes` = `cape_succes_langues`.`id_succes` 
  AND `cape_succes_langues`.`id_langue` = $langue_app 
  AND `cape_utilisateurs_succes`.`id_utilisateur` = $id_utilisateur 
  ORDER BY `cape_succes`.`place` ASC");

  if(!$result)
      error_log(mysql_error());
    

  //Pour chaque message
  while($row_message_sc2 = mysql_fetch_assoc($result))
  {
    if ($os == 'web') 
      $arraySucces[] = (object) array('succes' => $row_message_sc2['nom'],'description' => $row_message_sc2['description'],'image_blob'=> base64_encode($row_message_sc2['image_blob']));
    else
      $arraySucces[] = (object) array('succes' => $row_message_sc2['nom'],'description' => $row_message_sc2['description'],'image' => $row_message_sc2['image']/*,'image_blob'=> base64_encode($row_message_sc2['image_blob'])*/);
  }
  
  
  // error_log(print_r($arraySucces, true)); 
  //#####
  // FIN REQUETE succes
  //######

  // Affichage des succès non gagnés.

  //#####
  // DEBUT REQUETE succes NON GAGNE
  //######

  $result = mysql_query("SELECT `cape_succes_langues`.`nom`, `cape_succes_langues`.`description`, `cape_succes`.`image_blob`, `cape_succes`.`image` 
  FROM `cape_succes`, `cape_succes_langues` 
  WHERE `cape_succes`.`id_succes` = `cape_succes_langues`.`id_succes` 
  AND `cape_succes_langues`.`id_langue` = $langue_app 
  AND (`cape_succes`.`id_succes`) 
  NOT IN
    (SELECT `cape_succes`.`id_succes` 
    FROM `cape_succes`,`cape_utilisateurs_succes`,`cape_succes_langues` 
    WHERE `cape_succes`.`id_succes` = `cape_utilisateurs_succes`.`id_succes` 
    AND `cape_succes`.`id_succes` = `cape_succes_langues`.`id_succes` 
    AND `cape_succes_langues`.`id_langue` = $langue_app 
    AND `cape_utilisateurs_succes`.`id_utilisateur` = $id_utilisateur) 
  ORDER BY place ASC");

  if(!$result)
      error_log(mysql_error());
    

  //Pour chaque message
  while($row_message_sc3 = mysql_fetch_assoc($result))
  {
    $arraySuccesNonGagnes[] = (object) array('succes' => $row_message_sc3['nom'],'description' => $row_message_sc3['description'],'image' => "succes_default");
  }
  
  
  //#####
  // FIN REQUETE succes NON GAGNES
  //######


  //Affichage des succès gagnés en l'ordre chronologique.


  //#####
  // DEBUT REQUETE derniers succes
  //######

  $result = mysql_query("SELECT `cape_succes`.`image` 
  FROM `cape_succes`,`cape_utilisateurs_succes`,`cape_succes_langues` 
  WHERE `cape_succes`.`id_succes` = `cape_utilisateurs_succes`.`id_succes` 
  AND `cape_succes`.`id_succes` = `cape_succes_langues`.`id_succes` 
  AND `cape_succes_langues`.`id_langue` = $langue_app
  AND `cape_utilisateurs_succes`.`id_utilisateur` = $id_utilisateur
  ORDER BY time DESC");

  if(!$result)
      error_log(mysql_error());
    

  //Pour chaque message
  while($row_message_sc3 = mysql_fetch_assoc($result))
  {
    $arraySuccesDerniers[] = (object) array('image' => $row_message_sc3['image']);
  }
  
  
  //#####
  // FIN REQUETE derniers succes
  //######

  $result = mysql_query("SELECT `cu`.`etablissement`, `cl`.`code_langue`
  FROM `cape_utilisateurs` AS `cu`, `cape_langues` AS `cl`, `cape_utilisateurs_langues` AS `cul`
  WHERE `cl`.`id_langue` = `cul`.`id_langue`
  AND `cul`.`principale` = 1
  AND `cu`.`id_elgg` = `cul`.`id_utilisateur`
  AND `cu`.`id_elgg` = $id_utilisateur");
  $row_message_sc = mysql_fetch_assoc($result);
  $etablissement = $row_message_sc['etablissement'];
  $codeLangue = $row_message_sc['code_langue'];

  if ($os == 'and') {
    $return = json_encode( (object) array('name' => $name, 'afficher_noms_reels' => $flag_expert_ressource, 'image' => $image ,'email' => $email,'etablissement' => $etablissement, 'res' => $arrayRes,'nombreSucces' => count($arraySucces),'succes' => $arraySucces,'succesNonGagnes'=>$arraySuccesNonGagnes,'succesDerniers'=>$arraySuccesDerniers,'isOwner'=>$isOwner, 'langue_principale'=>$codeLangue) );
  } else {
    $return = json_encode( (object) array('name' => $name, 'offline' => $offline, 'image' => $image ,'email' => $email,'etablissement' => $etablissement, 'res' => $arrayRes,'nombreSucces' => count($arraySucces),'succes' => $arraySucces,'succesNonGagnes'=>$arraySuccesNonGagnes,'succesDerniers'=>$arraySuccesDerniers,'isOwner'=>$isOwner, 'langue_principale'=>$codeLangue) );
	}
	print $return;

?>